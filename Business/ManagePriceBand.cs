﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class ManagePriceBand
    {
        db_1TrackEntities db = new db_1TrackEntities();

        #region PriceBand
        public List<tblPriceBand> GetPriceBandList()
        {
            try
            {
                return db.tblPriceBands.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblPriceBand GetPriceBandById(int id)
        {
            try
            {
                return db.tblPriceBands.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddEditPriceBand(tblPriceBand obj)
        {
            try
            {
                var id = obj.ID;
                if (id == 0)
                {
                    db.tblPriceBands.AddObject(new tblPriceBand
                    {
                        ID = id,
                        Name = obj.Name,
                        Notes = obj.Notes,
                        IsActive = obj.IsActive,
                        CreatedOn = obj.CreatedOn,
                        CreatedBy = obj.CreatedBy
                    });
                }
                else
                {
                    var rec = db.tblPriceBands.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.Name = obj.Name;
                        rec.Notes = obj.Notes;
                        rec.IsActive = obj.IsActive;
                        rec.ModifiedBy = obj.ModifiedBy;
                        rec.ModifiedOn = obj.ModifiedOn;
                    }
                }

                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeletePriceBand(int id)
        {
            try
            {
                var rec = db.tblPriceBands.FirstOrDefault(x => x.ID == id);
                db.tblPriceBands.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactivePriceBand(int id)
        {
            try
            {
                var rec = db.tblPriceBands.FirstOrDefault(x => x.ID == id);
                if (rec != null) rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
