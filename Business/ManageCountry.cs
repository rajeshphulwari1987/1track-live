﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageCountry
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public List<tblCountriesMst> GetCountryDetail(Guid GId)
        {
            return _db.tblCountriesMsts.Where(ty => ty.CountryID == GId).ToList();
        }
        public List<tblProductEurailCountire> GetEurailCountryList()
        {
            try
            {
                return _db.tblProductEurailCountires.OrderBy(t => t.Country).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblProductEurailCountire> GetEurailCountryLinkedList(Guid EurailCountryID)
        {
            try
            {
                var list = (from tbl1 in _db.tblProductEurailCountiresLinkedCountires
                            join tbl2 in _db.tblProductEurailCountires on tbl1.EurailCountryLinkedID equals tbl2.EurailCountryID
                            where tbl1.EurailCountryID == EurailCountryID
                            select new { tbl2.Country, tbl2.EurailCountryID }).OrderBy(t => t.Country).ToList().Select(t => new tblProductEurailCountire
                            {
                                Country = t.Country
                            }).ToList();

                if (list != null && list.Count() > 0)
                    return list;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetEurailCountryLinkedList(tblProductEurailCountiresLinkedCountire obj)
        {
            try
            {
                var data = _db.tblProductEurailCountiresLinkedCountires.FirstOrDefault(t => t.EurailCountryID == obj.EurailCountryID && t.EurailCountryLinkedID == obj.EurailCountryLinkedID);
                if (data == null)
                {
                    _db.tblProductEurailCountiresLinkedCountires.AddObject(obj);
                    _db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddEurailCountry(tblProductEurailCountire obj)
        {
            try
            {
                var data = _db.tblProductEurailCountires.FirstOrDefault(t => t.Country.ToLower().Contains(obj.Country.ToLower()));
                if (data == null)
                {
                    _db.tblProductEurailCountires.AddObject(obj);
                    _db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddEurailCountryPassList(List<tblProductEurailCountryProductLookup> lst, Int32 Code)
        {
            try
            {
                var data = _db.tblProductEurailCountryProductLookups.FirstOrDefault(t => t.Code == Code);
                if (data == null)
                {
                    foreach (var item in lst)
                    {
                        _db.tblProductEurailCountryProductLookups.AddObject(item);
                        _db.SaveChanges();
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JourneyList GetJourneyById(Guid id)
        {
            try
            {
                var olist = (from sj in _db.tblJourneys
                             join sl in _db.tblJourneySiteLookups
                             on sj.ID equals sl.JourneyID
                             where sj.ID == id
                             select new { sj, sl }).ToList();

                var list = olist.Distinct().Select(x => new JourneyList
                {
                    ID = x.sj.ID,
                    From = x.sj.From,
                    To = x.sj.To,
                    FromFlag = x.sj.FromFlag,
                    ToFlag = x.sj.ToFlag,
                    Price = x.sj.Price,
                    DurationHr = x.sj.DurationHr,
                    DurationMin = x.sj.DurationMin,
                    BannerImg = x.sj.BannerImg,
                    Description = x.sj.Description,
                    IsActive = x.sj.IsActive,
                    SiteId = Guid.Parse(x.sl.SiteID.ToString())
                }).ToList().ToList();

                return list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<JourneyList> GetTopJourneyByCntryID(Guid cntryID, Guid siteId)
        {
            try
            {
                var olist = (from sj in _db.tblJourneys
                             join sl in _db.tblJourneySiteLookups
                             on sj.ID equals sl.JourneyID
                             join s in _db.tblSites
                             on sl.SiteID equals s.ID
                             where sj.CountryID == cntryID
                             && sj.IsActive == true
                             && sj.IsPopular == true
                             && sl.SiteID == siteId
                             select new { sj, s }).ToList();

                var list = olist.Distinct().Select(x => new JourneyList
                {
                    ID = x.sj.ID,
                    From = x.sj.From,
                    To = x.sj.To,
                    FromFlag = x.sj.FromFlag,
                    ToFlag = x.sj.ToFlag,
                    Price = x.sj.Price,
                    DurationHr = x.sj.DurationHr,
                    DurationMin = x.sj.DurationMin,
                    BannerImg = x.sj.BannerImg,
                    Description = x.sj.Description
                }).ToList().ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class JourneyList : tblJourney
        {
            public string SiteName { get; set; }
            public Guid SiteId { get; set; }
        }

        public tblCountriesMst GetCountryNameById(Guid CuId)
        {
            try
            {
                return _db.tblCountriesMsts.Single(v => v.CountryID == CuId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TrainDetails> GetTrainDtlByCId(Guid cId, Guid siteId)
        {
            try
            {
                var tdList = (from train in _db.tblTrainDetails
                              join cntlook in _db.tblTrainDetailCntryLookups
                                  on train.ID equals cntlook.TrainID
                              join sitelook in _db.tblTrainDetailSiteLookups
                                  on train.ID equals sitelook.TrainID
                              where cntlook.CountryID == cId &&
                                    sitelook.SiteID == siteId
                                    && train.IsActive
                              select new { train }).ToList();

                var list = tdList.Distinct().Select(x => new TrainDetails
                    {
                        ID = x.train.ID,
                        BannerImage = x.train.BannerImage,
                        Name = x.train.Name,
                        NavUrl = x.train.NavUrl
                    }).ToList().ToList();

                return list.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class TrainDetails : tblTrainDetail
        {
        }
        public class CountryProduct : tblProduct
        {
            public string ImagePrdt { get; set; }
        }
    }
}
