﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class ManageSeo
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region SEO Breadcrumbs
        public List<SeoDropdown> Getproductlist(Guid Siteid)
        {
            try
            {
                var data = (from tp in _db.tblProducts
                            join tpn in _db.tblProductNames on tp.ID equals tpn.ProductID
                            join tpcl in _db.tblProductCategoriesLookUps on tpn.ProductID equals tpcl.ProductID
                            join tcn in _db.tblCategoriesNames on tpcl.CategoryID equals tcn.CategoryID
                            join tc in _db.tblCategories on tpcl.CategoryID equals tc.ID
                            join tcsl in _db.tblCategorySiteLookUps on tpcl.CategoryID equals tcsl.CategoryID
                            join tpsl in _db.tblProductSiteLookUps on tpcl.ProductID equals tpsl.ProductID
                            where tp.IsActive && tc.IsActive &&
                            (tp.ProductEnableFromDate <= DateTime.Now && tp.ProductEnableToDate >= DateTime.Now) &&
                            tcsl.SiteID == Siteid && tpsl.SiteID == Siteid
                            select new SeoDropdown { Id = tpn.ProductID, Name = tpn.Name }).OrderBy(a => a.Name).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SeoDropdown> Getcountrylist(Guid Siteid)
        {
            try
            {
                var data = _db.tblCountrySiteLookUps.Where(x => x.SiteID == Siteid && x.tblCountriesMst.IsActive == true && x.tblCountriesMst.IsVisible == true).Select(x => new SeoDropdown
                {
                    Id = x.tblCountriesMst.CountryID,
                    Name = x.tblCountriesMst.CountryName
                }).OrderBy(a => a.Name).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SeoDropdown> Getspecialtrainlist(Guid Siteid)
        {
            try
            {
                var countryIdList = _db.tblCountrySiteLookUps.Where(x => x.SiteID == Siteid).Select(x => x.CountryID).ToList();
                var data = _db.tblSpecialTrains.Where(v => v.IsActive == true && countryIdList.Contains(v.CountryID)).Select(y => new SeoDropdown
                {
                    Id = y.ID,
                    Name = y.Name,
                }).OrderBy(a => a.Name).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SeoDropdown> Getotherpagelist(Guid Siteid)
        {
            try
            {
                var data = _db.tblWebMenus.Where(x => x.IsActive == true && x.OtherPageSeo).Select(y => new SeoDropdown
                {
                    Id = y.ID,
                    Name = y.Name,
                }).OrderBy(a => a.Name).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSeoBreadcrumb Getlinkbody(Guid Id, Guid Siteid, bool IsCategory = false)
        {
            try
            {
                return _db.tblSeoBreadcrumbs.FirstOrDefault(x => x.SiteId == Siteid && x.Linkid == Id && x.IsCategory == IsCategory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSeoBreadcrumb GetlinkbodyCategory(Guid Id, Guid Siteid)
        {
            try
            {
                return _db.tblSeoBreadcrumbs.FirstOrDefault(x => x.SiteId == Siteid && x.Linkid == Id && x.IsCategory == true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SeoDropdownList> GetListDataSEO(List<SeoDropdown> datalist, Guid Siteid, bool IsCategory = false)
        {
            try
            {
                var data = (from a in datalist
                            join b in _db.tblSeoBreadcrumbs.Where(x => x.SiteId == Siteid && x.IsCategory == IsCategory) on a.Id equals b.Linkid into temp
                            from j in temp.DefaultIfEmpty()
                            select new SeoDropdownList { Id = a.Id, Name = a.Name, Url = (j != null ? j.SourceCode : ""), IsActive = (j != null ? j.IsActive : false) }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetlinkbodySiteURL(Guid Siteid)
        {
            try
            {
                var data = _db.tblSites.FirstOrDefault(x => x.ID == Siteid);
                if (data != null)
                    return data.SiteURL;
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Savelinkbody(string SourceCode, Guid Id, Guid Siteid, Boolean Active, bool IsCategory = false)
        {
            try
            {
                var data = _db.tblSeoBreadcrumbs.FirstOrDefault(x => x.Linkid == Id && x.SiteId == Siteid && x.IsCategory == IsCategory);
                if (data != null)
                {
                    data.SourceCode = SourceCode;
                    data.IsActive = Active;
                    data.ModifyedBy = AdminuserInfo.UserID;
                    data.ModifyedOn = DateTime.Now;
                }
                else
                {
                    var obj = new tblSeoBreadcrumb
                    {
                        Linkid = Id,
                        SiteId = Siteid,
                        SourceCode = SourceCode,
                        IsActive = Active,
                        CreatedBy = AdminuserInfo.UserID,
                        CreatedOn = DateTime.Now,
                        IsCategory = IsCategory
                    };
                    _db.tblSeoBreadcrumbs.AddObject(obj);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Activelinkbody(Guid Id, Guid Siteid)
        {
            try
            {
                var data = _db.tblSeoBreadcrumbs.FirstOrDefault(x => x.Linkid == Id && x.SiteId == Siteid);
                if (data != null)
                {
                    data.IsActive = !data.IsActive;
                    data.ModifyedBy = AdminuserInfo.UserID;
                    data.ModifyedOn = DateTime.Now;
                    _db.SaveChanges();
                }
                return (data == null) ? "Please make breadcrumbs for this pass." : "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SpecialTrainMetaInfo
        public List<tblSpecialTrain> GetSpecialTrainList()
        {
            try
            {
                return _db.tblSpecialTrains.Where(x => x.IsActive == true).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SpecialTrainMetaInfo> GetSpecialTrainSeoList(Guid siteID)
        {
            try
            {
                var list = (from sp in _db.tblSpecialTrainMetaInfoes
                            join st in _db.tblSites
                                on sp.SiteID equals st.ID
                            join spt in _db.tblSpecialTrains
                            on sp.SpecialTrainID equals spt.ID
                            where sp.SiteID == siteID
                            orderby sp.CreatedOn
                            select new { sp, st, spt }).ToList();
                return list.Select(x => new SpecialTrainMetaInfo
                    {
                        ID = x.sp.ID,
                        SpTrainName = x.spt.Name,
                        Title = x.sp.Title,
                        SiteName = x.st.DisplayName
                    }).OrderBy(x => x.SpTrainName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSpecialTrainMetaInfo GetspTrainSeoById(Guid id)
        {
            try
            {
                return _db.tblSpecialTrainMetaInfoes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddEditSpecialTrainSeo(tblSpecialTrainMetaInfo odata)
        {
            try
            {
                var data = _db.tblSpecialTrainMetaInfoes.FirstOrDefault(x => x.SpecialTrainID == odata.SpecialTrainID && x.SiteID == odata.SiteID);
                if (data != null)
                {
                    data.SpecialTrainID = odata.SpecialTrainID;
                    data.SiteID = odata.SiteID;
                    data.Title = odata.Title;
                    data.Keywords = odata.Keywords;
                    data.Description = odata.Description;
                    data.ModifyOn = DateTime.Now;
                    data.ModifyBy = odata.CreatedBy;
                }
                else
                {
                    odata.ID = Guid.NewGuid();
                    _db.AddTotblSpecialTrainMetaInfoes(odata);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteSpTrainSeo(Guid id)
        {
            try
            {
                var rec = _db.tblSpecialTrainMetaInfoes.FirstOrDefault(x => x.ID == id);
                _db.tblSpecialTrainMetaInfoes.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class SpecialTrainMetaInfo : tblSpecialTrainMetaInfo
        {
            public string SpTrainName { get; set; }
            public string SiteName { get; set; }
        }
        #endregion

        #region CountryMetaInfo
        public List<tblCountriesMst> GetCountryList()
        {
            try
            {
                return _db.tblCountriesMsts.Where(x => x.IsActive == true).OrderBy(x => x.CountryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CountryMetaInfo> GetCountrySeoList(Guid siteID)
        {
            try
            {
                var list = (from cm in _db.tblCountryMetaInfoes
                            join st in _db.tblSites
                                on cm.SiteID equals st.ID
                            join ct in _db.tblCountriesMsts
                            on cm.CountryID equals ct.CountryID
                            where cm.SiteID == siteID
                            orderby cm.CreatedOn
                            select new { cm, st, ct }).ToList();
                return list.Select(x => new CountryMetaInfo
                {
                    ID = x.cm.ID,
                    CountryName = x.ct.CountryName,
                    Title = x.cm.Title,
                    SiteName = x.st.DisplayName
                }).OrderBy(x => x.CountryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCountryMetaInfo GetCountrySeoById(Guid id)
        {
            try
            {
                return _db.tblCountryMetaInfoes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddEditCountrySeo(tblCountryMetaInfo odata)
        {
            try
            {
                var data = _db.tblCountryMetaInfoes.FirstOrDefault(x => x.CountryID == odata.CountryID && x.SiteID == odata.SiteID);
                if (data != null)
                {
                    data.CountryID = odata.CountryID;
                    data.SiteID = odata.SiteID;
                    data.Title = odata.Title;
                    data.Keywords = odata.Keywords;
                    data.Description = odata.Description;
                    data.ModifyOn = DateTime.Now;
                    data.ModifyBy = odata.CreatedBy;
                }
                else
                {
                    odata.ID = Guid.NewGuid();
                    _db.AddTotblCountryMetaInfoes(odata);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCountrySeo(Guid id)
        {
            try
            {
                var rec = _db.tblCountryMetaInfoes.FirstOrDefault(x => x.ID == id);
                _db.tblCountryMetaInfoes.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CountryMetaInfo : tblCountryMetaInfo
        {
            public string CountryName { get; set; }
            public string SiteName { get; set; }
        }
        #endregion

        #region WebMenuMetaInfo
        public List<tblWebMenu> GetWebMenuList()
        {
            try
            {
                return _db.tblWebMenus.Where(x => x.IsActive == true && x.IsSeo && x.IsCorporate == false).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WebMenuMetaInfo> GetWebMenuSeoList(Guid siteID)
        {
            try
            {
                var list = (from web in _db.tblWebMenuMetaInfoes
                            join st in _db.tblSites
                                on web.SiteID equals st.ID
                            join menu in _db.tblWebMenus
                            on web.WebMenuID equals menu.ID
                            where web.SiteID == siteID
                            orderby web.CreatedOn
                            select new { web, st, menu }).ToList();
                return list.Select(x => new WebMenuMetaInfo
                {
                    ID = x.web.ID,
                    WebMenuName = x.menu.Name,
                    Title = x.web.Title,
                    SiteName = x.st.DisplayName
                }).OrderBy(x => x.WebMenuName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblWebMenuMetaInfo GetWebMenuSeoById(Guid id)
        {
            try
            {
                return _db.tblWebMenuMetaInfoes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddEditWebMenuSeo(tblWebMenuMetaInfo odata)
        {
            try
            {
                var data = _db.tblWebMenuMetaInfoes.FirstOrDefault(x => x.WebMenuID == odata.WebMenuID && x.SiteID == odata.SiteID);
                if (data != null)
                {
                    data.WebMenuID = odata.WebMenuID;
                    data.SiteID = odata.SiteID;
                    data.Title = odata.Title;
                    data.Keywords = odata.Keywords;
                    data.Description = odata.Description;
                    data.ModifyOn = DateTime.Now;
                    data.ModifyBy = odata.CreatedBy;
                }
                else
                {
                    odata.ID = Guid.NewGuid();
                    _db.AddTotblWebMenuMetaInfoes(odata);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteWebMenuSeo(Guid id)
        {
            try
            {
                var result = _db.tblWebMenuMetaInfoes.FirstOrDefault(x => x.ID == id);
                _db.tblWebMenuMetaInfoes.DeleteObject(result);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class WebMenuMetaInfo : tblWebMenuMetaInfo
        {
            public string WebMenuName { get; set; }
            public string SiteName { get; set; }
        }

        #endregion

        #region CMS Pages

        public List<tblPagesCM> GetCMSPageList(Guid SiteId)
        {
            try
            {
                return _db.tblPagesCMS.Where(x => x.SiteId == SiteId).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CMSPageMetaInfo> GetCMSPageSeoList(Guid siteID)
        {
            try
            {
                var list = (from cm in _db.tblCMSPageMetaInfoes
                            join st in _db.tblSites on cm.SiteID equals st.ID
                            join cms in _db.tblPagesCMS on cm.CMSPageID equals cms.ID
                            where cm.SiteID == siteID
                            orderby cm.CreatedOn
                            select new { cm, st, cms }).ToList();
                return list.Select(x => new CMSPageMetaInfo
                {
                    ID = x.cm.ID,
                    Name = x.cms.Name,
                    Title = x.cm.Title,
                    SiteName = x.st.DisplayName
                }).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCMSPageMetaInfo GetCMSPageSeoById(Guid id)
        {
            try
            {
                return _db.tblCMSPageMetaInfoes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCMSPageSeo(Guid id)
        {
            try
            {
                var result = _db.tblCMSPageMetaInfoes.FirstOrDefault(x => x.ID == id);
                _db.tblCMSPageMetaInfoes.DeleteObject(result);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddEditCMSPageSeo(tblCMSPageMetaInfo odata)
        {
            try
            {
                var data = _db.tblCMSPageMetaInfoes.FirstOrDefault(x => x.CMSPageID == odata.CMSPageID && x.SiteID == odata.SiteID);
                if (data != null)
                {
                    data.Title = odata.Title;
                    data.Keywords = odata.Keywords;
                    data.Description = odata.Description;
                    data.ModifyOn = DateTime.Now;
                    data.ModifyBy = odata.CreatedBy;
                }
                else
                {
                    odata.ID = Guid.NewGuid();
                    _db.AddTotblCMSPageMetaInfoes(odata);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CMSPageMetaInfo : tblCMSPageMetaInfo
        {
            public string Name { get; set; }
            public string SiteName { get; set; }
        }

        #endregion

        #region CorporateSiteMetaInfo
        public List<CorporateMetaInfo> GetCorporateSeo(Guid siteid)
        {
            try
            {
                var list = (from cm in _db.tblCorporateMetaInfoes
                            join nv in _db.tblWebMenus
                                on cm.NavID equals nv.ID
                            join st in _db.tblSites
                                on cm.SiteID equals st.ID
                            where cm.SiteID == siteid
                            orderby cm.CreatedBy
                            select new { cm, nv, st });
                return list.Select(x => new CorporateMetaInfo
                    {
                        ID = x.cm.ID,
                        PageName = x.nv.Name,
                        Title = x.cm.Title,
                        SiteName = x.st.DisplayName
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCorporateMetaInfo GetCorpSeoById(Guid id)
        {
            try
            {
                return _db.tblCorporateMetaInfoes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddEditCorpSeo(tblCorporateMetaInfo odata)
        {
            try
            {
                var data = _db.tblCorporateMetaInfoes.FirstOrDefault(x => x.NavID == odata.NavID && x.SiteID == odata.SiteID);
                if (data != null)
                {
                    data.NavID = odata.NavID;
                    data.SiteID = odata.SiteID;
                    data.Title = odata.Title;
                    data.Keywords = odata.Keywords;
                    data.Description = odata.Description;
                    data.ModifiedOn = DateTime.Now;
                    data.ModifiedBy = odata.CreatedBy;
                }
                else
                {
                    odata.ID = Guid.NewGuid();
                    _db.AddTotblCorporateMetaInfoes(odata);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpSeo(Guid id)
        {
            try
            {
                var rec = _db.tblCorporateMetaInfoes.FirstOrDefault(x => x.ID == id);
                _db.tblCorporateMetaInfoes.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorporateMetaInfo : tblCorporateMetaInfo
        {
            public string PageName { get; set; }
            public string SiteName { get; set; }
        }
        #endregion

        #region FrontEnd SEO management
        public SeoDetail GetSeoDetails(string seoType, Guid siteid, Guid pageid)
        {
            try
            {
                switch (seoType)
                {
                    case "page":
                        var resultA = (from page in _db.tblPages
                                       where page.SiteID == siteid &&
                                             page.NavigationID == pageid
                                       select new { page });
                        return resultA.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.MetaTitle,
                            MetaKeyword = x.page.Keyword,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "specialTrain":
                        var resultB = (from page in _db.tblSpecialTrainMetaInfoes
                                       where page.SiteID == siteid &&
                                        page.SpecialTrainID == pageid
                                       select new { page });
                        return resultB.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "country":
                        var resultC = (from page in _db.tblCountryMetaInfoes
                                       where page.SiteID == siteid &&
                                        page.CountryID == pageid
                                       select new { page });
                        return resultC.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "category":
                        var resultD = (from page in _db.tblCategoryMetaInfoes
                                       where page.SiteID == siteid &&
                                        page.CategoryID == pageid
                                       select new { page });
                        return resultD.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "product":
                        var resultE = (from page in _db.tblProductMetaInfoes
                                       where page.SiteID == siteid &&
                                        page.ProductID == pageid
                                       select new { page });
                        return resultE.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "productdetails":
                        var resultPD = (from page in _db.tblProductDetailsMetaInfoes
                                        where page.SiteID == siteid &&
                                         page.ProductID == pageid
                                        select new { page });
                        return resultPD.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "other":
                        var resultF = (from page in _db.tblWebMenuMetaInfoes
                                       where page.SiteID == siteid &&
                                        page.WebMenuID == pageid
                                       select new { page });
                        return resultF.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                        break;
                    case "cms":
                        var resultG = (from page in _db.tblCMSPageMetaInfoes
                                       where page.SiteID == siteid &&
                                        page.CMSPageID == pageid
                                       select new { page });
                        return resultG.Select(x => new SeoDetail
                        {
                            MetaTitle = x.page.Title,
                            MetaKeyword = x.page.Keywords,
                            MetaDescription = x.page.Description
                        }).FirstOrDefault();
                    default:
                        return null;
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Countries HrefLangTag
        public string GetHrefLangTag(Guid SiteId)
        {
            try
            {
                var data = _db.tblSeoStieTags.FirstOrDefault(x => x.SiteId == SiteId);
                if (data != null)
                    return data.hreflang;
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }

    public class SeoDetail
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
    }

    public class SeoDropdown
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class SeoDropdownList
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }

    }
}
