﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Business;

namespace Business
{
    public class ManageTrainDetails
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public List<tblCountriesMst> GetCountryDetail()
        {
            try
            {
                var list = _db.tblCountriesMsts.Where(x => x.IsActive == true).OrderBy(x => x.CountryName).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblNationality> GetNationality()
        {
            try
            {
                var list = _db.tblNationalities.Where(x => x.IsActive).OrderBy(x => x.Adjective).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetEurailCountryNames(string guids)
        {
            try
            {
                string countryName = string.Empty;
                string[] ids = guids.Split(',').ToArray();
                for (int i = 0; i < ids.Count(); i++)
                {
                    if (!String.IsNullOrEmpty(ids[i]))
                    {
                        Guid id = Guid.Parse(ids[i]);
                        var rec = _db.tblProductEurailCountires.FirstOrDefault(x => x.EurailCountryID == id && x.SelectPass == true);
                        if (rec != null)
                            if (i == 0)
                                countryName += rec.Country;
                            else
                                countryName += ", " + rec.Country;
                    }
                }
                if (countryName.Length > 0)
                    countryName = countryName.Remove(countryName.Length - 1);
                return countryName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<TrainDetailFields> GetTrainDetailList(Guid siteID, int start, int end)
        {
            try
            {
                return _db.tblTrainDetails.Where(p => p.tblTrainDetailSiteLookups.Any(z => z.SiteID == siteID)).Select(x => new TrainDetailFields
                {
                    ID = x.ID,
                    //Country = x.tblCountriesMst.CountryName + "(" + x.tblCountriesMst.CountryCode + ")",
                    BannerImage = x.BannerImage,
                    IsActive = x.IsActive,
                    ShortDescription = x.ShortDescription,
                    Name = x.Name,
                }).OrderBy(x => x.Name).Skip(start).Take(end).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public tblTrainDetail GetTrainDetialById(Guid id)
        {
            try
            {
                return _db.tblTrainDetails.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Guid> GetTrainDetailSiteIdListById(Guid id)
        {
            try
            {
                return _db.tblTrainDetailSiteLookups.Where(y => y.TrainID == id).Select(x => x.SiteID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblTrainDetailCntryLookup> GetTrainDetailCntryIdListById(Guid id)
        {
            return _db.tblTrainDetailCntryLookups.Where(x => x.TrainID == id).ToList();
        }

        public List<tblTrainDetailGallery> GetGalleryListByTrainID(Guid id)
        {
            try
            {
                return (from tg in _db.tblTrainDetailGalleries
                        join td in _db.tblTrainDetails
                            on tg.TrainDetailID equals td.ID
                        where td.ID == id
                        select tg).OrderBy(x => x.tblTrainDetail.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public int TotalNumberOfRecord(Guid siteID)
        {
            return _db.tblTrainDetails.Where(y => y.tblTrainDetailSiteLookups.Any(x => x.SiteID == siteID)).Count();
        }

        public Guid AddEditTrainDetail(tblTrainDetail trdetail)
        {
            try
            {
                if (trdetail.ID == new Guid())
                {
                    if (_db.tblTrainDetails.Any(x => x.Name == trdetail.Name.Trim()))
                        throw new Exception("Train already exists.");

                    trdetail.ID = Guid.NewGuid();
                    _db.AddTotblTrainDetails(trdetail);
                }
                else
                {
                    var rec = _db.tblTrainDetails.FirstOrDefault(x => x.ID == trdetail.ID);
                    rec.ID = trdetail.ID;
                    rec.Name = trdetail.Name;
                    rec.NavUrl = trdetail.NavUrl;
                    if (!string.IsNullOrEmpty(trdetail.BannerImage))
                        rec.BannerImage = trdetail.BannerImage;
                    rec.ShortDescription = trdetail.ShortDescription;
                    rec.LongDescription = trdetail.LongDescription;
                    rec.IPAddress = trdetail.IPAddress;
                    rec.IsActive = trdetail.IsActive;
                    rec.ModifiedBy = trdetail.CreatedBy;
                    rec.ModifiedOn = trdetail.CreatedOn;
                }
                _db.SaveChanges();
                return trdetail.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddGalry(tblTrainDetailGallery TrnGlry)
        {
            try
            {
                if (TrnGlry.ID == new Guid())
                {
                    TrnGlry.ID = Guid.NewGuid();
                    _db.AddTotblTrainDetailGalleries(TrnGlry);
                }
                _db.SaveChanges();
                return TrnGlry.TrainDetailID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditTrainDetailSiteLookup(List<tblTrainDetailSiteLookup> oLookup)
        {
            try
            {
                Guid id = oLookup.FirstOrDefault().TrainID;
                _db.tblTrainDetailSiteLookups.Where(x => x.TrainID == id).ToList().ForEach(_db.tblTrainDetailSiteLookups.DeleteObject);
                foreach (var item in oLookup)
                {
                    item.ID = Guid.NewGuid();
                    _db.tblTrainDetailSiteLookups.AddObject(item);
                }
                _db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddCntLookUp(List<tblTrainDetailCntryLookup> lookUpList, Guid trainId)
        {
            try
            {
                _db.tblTrainDetailCntryLookups.Where(x => x.TrainID == trainId).ToList().ForEach(_db.tblTrainDetailCntryLookups.DeleteObject);
                if (lookUpList != null)
                    foreach (var lookUp in lookUpList)
                    {
                        lookUp.ID = Guid.NewGuid();
                        _db.tblTrainDetailCntryLookups.AddObject(lookUp);
                    }
                _db.SaveChanges();
                return trainId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteGalleryImagesById(Guid id)
        {
            try
            {
                var rec = _db.tblTrainDetailGalleries.FirstOrDefault(x => x.ID == id);
                string imgPath = rec.GalleryImage;
                _db.DeleteObject(rec);
                _db.SaveChanges();
                return imgPath;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteTrain(Guid id)
        {
            try
            {
                _db.tblTrainDetailCntryLookups.Where(x => x.TrainID == id).ToList().ForEach(_db.tblTrainDetailCntryLookups.DeleteObject);
                _db.tblTrainDetailSiteLookups.Where(x => x.TrainID == id).ToList().ForEach(_db.tblTrainDetailSiteLookups.DeleteObject);
                _db.tblTrainDetailGalleries.Where(x => x.TrainDetailID == id).ToList().ForEach(_db.tblTrainDetailGalleries.DeleteObject);
                var rec = _db.tblTrainDetails.FirstOrDefault(x => x.ID == id);
                _db.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactive(Guid id)
        {
            try
            {
                var rec = _db.tblTrainDetails.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblRailCard> GetRailCardList()
        {
            try
            {
                return _db.tblRailCards.Where(x => x.DisplayOnly == false).OrderBy(x => x.RailCardName).ToList();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
public class TrainDetailFields : tblTrainDetail
{
    public string Country { get; set; }
}