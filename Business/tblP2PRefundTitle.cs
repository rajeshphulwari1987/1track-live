//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblP2PRefundTitle
    {
        public System.Guid ID { get; set; }
        public Nullable<System.Guid> P2PRefundId { get; set; }
        public Nullable<int> TitleNumber { get; set; }
        public decimal Amount { get; set; }
        public string DeductedAmtInPercent { get; set; }
        public decimal DeductedAmount { get; set; }
        public decimal RefundedAmount { get; set; }
        public string RefundCause { get; set; }
    
        public virtual tblP2PRefund tblP2PRefund { get; set; }
    }
}
