//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblMailTemplateSiteLookUp
    {
        public System.Guid Id { get; set; }
        public System.Guid MailTemplateId { get; set; }
        public System.Guid SiteId { get; set; }
    
        public virtual tblMailTemplate tblMailTemplate { get; set; }
    }
}
