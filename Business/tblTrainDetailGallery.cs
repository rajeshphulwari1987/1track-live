//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblTrainDetailGallery
    {
        public System.Guid ID { get; set; }
        public System.Guid TrainDetailID { get; set; }
        public string GalleryImage { get; set; }
    
        public virtual tblTrainDetail tblTrainDetail { get; set; }
    }
}
