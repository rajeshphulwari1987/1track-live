﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageCookie
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public List<tblSite> GetSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region
        public List<GeneralInformation> GetGeneralInformation(Guid siteId)
        {
            try
            {
                var list = _db.tblGeneralInformation_.Where(y => y.SiteId == siteId).Select(x => new GeneralInformation
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = x.tblSite.DisplayName
                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public class GeneralInformation : tblGeneralInformation_
        {
            public string SiteName { get; set; }
        }
        public bool DeleteGeneralInformation(Guid id)
        {
            try
            {
                var rec = _db.tblGeneralInformation_.FirstOrDefault(x => x.ID == id);
                _db.tblGeneralInformation_.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddGeneralInformation(tblGeneralInformation_ odata)
        {
            try
            {
                var _information = _db.tblGeneralInformation_.FirstOrDefault(x => x.ID == odata.ID);
                if (_information != null)
                {
                    _information.Name = odata.Name;
                    _information.Description = odata.Description;
                    _information.Keywords = odata.Keywords;
                    _information.Imagepath = odata.Imagepath;
                    _information.ModifiedBy = odata.CreatedBy;
                    _information.ModifiedOn = odata.CreatedOn;
                    _information.SiteId = odata.SiteId;
                }
                else
                {
                    var _info = _db.tblGeneralInformation_.FirstOrDefault(x => x.SiteId == odata.SiteId);
                    if (_info == null)
                    {
                        _db.AddTotblGeneralInformation_(odata);
                    }
                }
                _db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblGeneralInformation_ GetGeneralInformationById(Guid id)
        {
            try
            {
                return _db.tblGeneralInformation_.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Cookies
        public List<CookiePolicyList> GetCookieList(Guid siteId)
        {
            try
            {
                var list = _db.tblCookies.Where(y => y.SiteId == siteId).Select(x => new CookiePolicyList
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = x.tblSite.DisplayName

                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public class CookiePolicyList : tblCookie
        {
            public string SiteName { get; set; }
        }
        public tblCookie GetCookiesBySiteId(Guid siteID)
        {
            return _db.tblCookies.FirstOrDefault(x => x.SiteId == siteID);
        }

        public bool DeleteCookiePolicy(Guid id)
        {
            try
            {
                var rec = _db.tblCookies.FirstOrDefault(x => x.ID == id);
                _db.tblCookies.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCookie GetCookieById(Guid id)
        {
            try
            {
                return _db.tblCookies.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddCookie(tblCookie oCookie)
        {
            try
            {
                var _privacy = _db.tblCookies.FirstOrDefault(x => x.ID == oCookie.ID);
                if (_privacy != null)
                {
                    _privacy.Name = oCookie.Name;
                    _privacy.Description = oCookie.Description;
                    _privacy.Keywords = oCookie.Keywords;
                    _privacy.ModifiedBy = oCookie.CreatedBy;
                    _privacy.ModifiedOn = oCookie.CreatedOn;
                    _privacy.SiteId = oCookie.SiteId;
                }
                else
                {
                    var _test = _db.tblCookies.FirstOrDefault(x => x.SiteId == oCookie.SiteId);
                    if (_test == null)
                    {
                        _db.AddTotblCookies(oCookie);
                    }
                }
                _db.SaveChanges();
                return oCookie.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion
    }
}
