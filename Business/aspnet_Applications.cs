//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class aspnet_Applications
    {
        public aspnet_Applications()
        {
            this.aspnet_Membership = new HashSet<aspnet_Membership>();
            this.aspnet_Membership1 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership2 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership3 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership4 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership5 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership6 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership7 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership8 = new HashSet<aspnet_Membership>();
            this.aspnet_Membership9 = new HashSet<aspnet_Membership>();
            this.aspnet_Paths = new HashSet<aspnet_Paths>();
            this.aspnet_Paths1 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths2 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths3 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths4 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths5 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths6 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths7 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths8 = new HashSet<aspnet_Paths>();
            this.aspnet_Paths9 = new HashSet<aspnet_Paths>();
            this.aspnet_Roles = new HashSet<aspnet_Roles>();
            this.aspnet_Roles1 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles2 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles3 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles4 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles5 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles6 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles7 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles8 = new HashSet<aspnet_Roles>();
            this.aspnet_Roles9 = new HashSet<aspnet_Roles>();
            this.aspnet_Users = new HashSet<aspnet_Users>();
            this.aspnet_Users1 = new HashSet<aspnet_Users>();
            this.aspnet_Users2 = new HashSet<aspnet_Users>();
            this.aspnet_Users3 = new HashSet<aspnet_Users>();
            this.aspnet_Users4 = new HashSet<aspnet_Users>();
            this.aspnet_Users5 = new HashSet<aspnet_Users>();
            this.aspnet_Users6 = new HashSet<aspnet_Users>();
            this.aspnet_Users7 = new HashSet<aspnet_Users>();
            this.aspnet_Users8 = new HashSet<aspnet_Users>();
            this.aspnet_Users9 = new HashSet<aspnet_Users>();
        }
    
        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public System.Guid ApplicationId { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<aspnet_Membership> aspnet_Membership { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership1 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership2 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership3 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership4 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership5 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership6 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership7 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership8 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership9 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths1 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths2 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths3 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths4 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths5 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths6 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths7 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths8 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths9 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles1 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles2 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles3 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles4 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles5 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles6 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles7 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles8 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles9 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users1 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users2 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users3 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users4 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users5 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users6 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users7 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users8 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users9 { get; set; }
    }
}
