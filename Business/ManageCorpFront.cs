﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageCorpFront
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();

        public Guid GetPageIdByURL(string url)
        {
            var rec = _db.tblCorpCms.FirstOrDefault(x => x.Url.ToLower() == url.ToLower());
            return rec == null ? new Guid() : rec.ID;
        }

        public Guid GetSiteIdByURL(string url)
        {
            var rec = _db.tblCorpCms.FirstOrDefault(x => x.Url.ToLower() == url.ToLower());
            return rec == null ? new Guid() : rec.SiteId;
        }

        public Guid GetSiteIdBySiteURL(string url)
        {
            //var rec = _db.tblSites.FirstOrDefault(x => x.SiteURL.ToLower() == url.ToLower());
            //return rec == null ? new Guid() : rec.ID;
            var rec = _db.tblSites.FirstOrDefault(x => x.SiteURL.ToLower().Contains(url.ToLower()));
            return rec == null ? new Guid() : rec.ID;
        }

        public string GetSiteUrlBySiteID(Guid siteId)
        {
            var rec = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            return rec == null ? null : rec.SiteURL;
        }

        public bool IsCorpBySiteUrl(string url)
        {
            var rec = _db.tblSites.FirstOrDefault(x => x.SiteURL == url);
            return rec != null && rec.IsCorporate;
        }

        public Guid GetWebPageIdByURL(string url)
        {
            var rec = _db.tblWebMenus.FirstOrDefault(x => x.PageName.ToLower() == url.ToLower());
            return rec == null ? new Guid() : rec.ID;
        }

        #region ContctUs
        public bool AddContactUs(tblCorpContactU odata)
        {
            try
            {
                var data = _db.tblCorpContactUs.FirstOrDefault(x => x.ID == odata.ID);

                if (data != null)
                {
                    data.CreatedOn = System.DateTime.Now;
                    data.Name = odata.Name;
                    data.Phone = odata.Phone;
                    data.Description = odata.Description;
                    data.SiteID = odata.SiteID;
                }
                else
                {
                    odata.CreatedOn = System.DateTime.Now;
                    _db.AddTotblCorpContactUs(odata);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteContactUs(int id)
        {
            try
            {
                var data = _db.tblCorpContactUs.FirstOrDefault(x => x.ID == id);
                if (data != null)
                {
                    _db.tblCorpContactUs.DeleteObject(data);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCorpContactU GetContactUsById(int id)
        {
            try
            {
                return _db.tblCorpContactUs.FirstOrDefault(x => x.ID == id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCorpContactU> GetContactUsList(Guid siteId)
        {
            try
            {
                return _db.tblCorpContactUs.Where(x => x.SiteID == siteId).OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Journey
        public List<tblCorpTicketType> GetTicketTypeList()
        {
            try
            {
                return _db.tblCorpTicketTypes.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCorpTicketDelivered> GetTicketDeliverList()
        {
            try
            {
                return _db.tblCorpTicketDelivereds.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCorpLoyaltyCard> GetLoyaltyCardList()
        {
            try
            {
                return _db.tblCorpLoyaltyCards.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCorpClassMst> GetClassList()
        {
            try
            {
                return _db.tblCorpClassMsts.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCorpAccomodation> GetAccomodationsList()
        {
            try
            {
                return _db.tblCorpAccomodations.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddJourneyPassenger(tblCorpJourneyPassenger obj)
        {
            try
            {
                _db.tblCorpJourneyPassengers.AddObject(obj);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddJourney(tblCorpJourney obj)
        {
            try
            {
                _db.tblCorpJourneys.AddObject(obj);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateJourneyPassenger(tblCorpJourneyPassenger obj)
        {
            try
            {

                var _jp = _db.tblCorpJourneyPassengers.FirstOrDefault(x => x.Id == obj.Id);
                if (_jp != null)
                {
                    _jp.Firstname = obj.Firstname;
                    _jp.Lastname = obj.Lastname;
                    _jp.Email = obj.Email;
                    _jp.Phone = obj.Phone;
                    _jp.Agencyname = obj.Agencyname;
                    _jp.IATAno = obj.IATAno;
                    _jp.Requesttype = obj.Requesttype;
                    _jp.Tickettype = obj.Tickettype;
                    _jp.Ticketdelivered = obj.Ticketdelivered;
                    _jp.Children = obj.Children;
                    _jp.Adults = obj.Adults;
                    _jp.Youths = obj.Youths;
                    _jp.Seniors = obj.Seniors;
                    _jp.LPTitle = obj.LPTitle;
                    _jp.LPFname = obj.LPFname;
                    _jp.LPLname = obj.LPLname;
                    _jp.LPSuffix = obj.LPSuffix;
                    _jp.Dob = obj.Dob;
                    _jp.Discountcardtype = obj.Discountcardtype;
                    _jp.Discountcardno = obj.Discountcardno;
                    _jp.Discountexp = obj.Discountexp;
                    _jp.SiteId = obj.SiteId;
                    _jp.Note = obj.Note;
                    _jp.Status = obj.Status;
                    //_jp.ModifiedBy = obj.ModifiedBy;
                    _jp.ModifiedOn = obj.ModifiedOn;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateJourney(tblCorpJourney obj)
        {
            try
            {
                var _journey = _db.tblCorpJourneys.FirstOrDefault(x => x.Id == obj.Id);
                if (_journey != null)
                {
                    _journey.From = obj.From;
                    _journey.To = obj.To;
                    _journey.Via = obj.Via;
                    _journey.Date = obj.Date;
                    _journey.Deptime = obj.Deptime;
                    _journey.Arrtime = obj.Arrtime;
                    _journey.Trainno = obj.Trainno;
                    _journey.Class = obj.Class;
                    _journey.Accomodation = obj.Accomodation;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Faqs
        public List<tblCorpFaq> GetCorpFaqList(Guid siteId)
        {
            return _db.tblCorpFaqs.Where(x => x.SiteID == siteId).OrderByDescending(x => x.ModifyOn).ToList();
        }
        #endregion

        #region Passenger info
        public List<tblCorpPassergerInfo> GetCorpPassList(Guid countryID)
        {
            return _db.tblCorpPassergerInfoes.Where(x => x.CountryID == countryID && x.IsActive).ToList();
        }
        #endregion

        #region Cancel/change
        public void AddChangeCancelOrder(tblCorpChangeCancel odata)
        {
            try
            {
                _db.AddTotblCorpChangeCancels(odata);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region SiteLogo
        public tblCorpLogo GetSiteLogo(Guid siteId)
        {
            try
            {
                return _db.tblCorpLogoes.FirstOrDefault(x => x.SiteID == siteId && x.IsActive);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get SEO detail
        public tblCorpCm GetPagesSeoDetail(Guid siteID, Guid pageID)
        {
            return _db.tblCorpCms.FirstOrDefault(x => x.SiteId == siteID && x.ID == pageID);
        }

        public tblCorporateMetaInfo GetSeoDetail(Guid siteID, Guid pageID)
        {
            return _db.tblCorporateMetaInfoes.FirstOrDefault(x => x.SiteID == siteID && x.NavID == pageID);
        }
        #endregion

        #region Booking
        public bool AddJourneyBookingPassenger(tblCorpJourneyPassengerBooked obj)
        {
            try
            {
                _db.tblCorpJourneyPassengerBookeds.AddObject(obj);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddJourneyBooking(tblCorpJourneyBooked obj)
        {
            try
            {
                _db.tblCorpJourneyBookeds.AddObject(obj);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
