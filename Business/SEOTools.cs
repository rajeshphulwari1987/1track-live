﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class SEOTools
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        /***************************************************/
        #region Product Deatils SEO
        public bool DeleteDataProductDetails(Guid ID)
        {
            try
            {
                _db.tblProductDetailsMetaInfoes.Where(t => t.ID == ID).ToList().ForEach(_db.tblProductDetailsMetaInfoes.DeleteObject);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblProductDetailsMetaInfo EditDataProductDetails(Guid ID)
        {
            try
            {
                return _db.tblProductDetailsMetaInfoes.FirstOrDefault(t => t.ID == ID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddEditProductDetails(tblProductDetailsMetaInfo Obj)
        {
            try
            {
                bool result = true;
                var data = _db.tblProductDetailsMetaInfoes.FirstOrDefault(t => t.SiteID == Obj.SiteID && t.CategoryID == Obj.CategoryID && t.ProductID == Obj.ProductID);
                if (data != null)
                {
                    data.CategoryID = Obj.CategoryID;
                    data.ProductID = Obj.ProductID;
                    data.Title = Obj.Title;
                    data.Keywords = Obj.Keywords;
                    data.Description = Obj.Description;
                    data.ModifyOn = Obj.CreatedOn;
                    data.ModifyBy = Obj.CreatedBy;
                    result = false;
                }
                else
                {
                    _db.tblProductDetailsMetaInfoes.AddObject(Obj);
                }
                _db.SaveChanges();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductDetailsSEO> GetProductDetails(Guid _stie)
        {
            try
            {
                var data = _db.tblProductDetailsMetaInfoes.Where(t => t.SiteID == _stie).AsEnumerable().Select(t => new ProductDetailsSEO
                {
                    ID = t.ID,
                    CategoryID = t.CategoryID,
                    ProductID = t.ProductID,
                    SiteID = t.SiteID,
                    Title = t.Title,
                    Keywords = t.Keywords,
                    Description = t.Description,
                    CategoryName = GetProductCategoryName(t.CategoryID),
                    SiteName = GetSiteName(t.SiteID),
                    ProductName = GetProductName(t.ProductID),
                }).OrderBy(x => x.ProductName).ToList();
                return data;
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
        /***************************************************/
        #region DML Operation Product Category

        public bool DeleteDataCategory(Guid ID)
        {
            try
            {
                _db.tblCategoryMetaInfoes.Where(t => t.ID == ID).ToList().ForEach(_db.tblCategoryMetaInfoes.DeleteObject);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblCategoryMetaInfo EditDataCategory(Guid ID)
        {
            try
            {
                return _db.tblCategoryMetaInfoes.FirstOrDefault(t => t.ID == ID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddEditCategory(tblCategoryMetaInfo Obj)
        {
            try
            {
                bool result = true;
                var data = _db.tblCategoryMetaInfoes.FirstOrDefault(t => t.SiteID == Obj.SiteID && t.CategoryID == Obj.CategoryID);
                if (data != null)
                {
                    data.Keywords = Obj.Keywords;
                    data.CategoryID = Obj.CategoryID;
                    data.Title = Obj.Title;
                    data.Keywords = Obj.Keywords;
                    data.Description = Obj.Description;
                    data.ModifyOn = Obj.CreatedOn;
                    data.ModifyBy = Obj.CreatedBy;
                    result = false;
                }
                else
                {
                    _db.tblCategoryMetaInfoes.AddObject(Obj);
                }
                _db.SaveChanges();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CategorySEO> GetCategory(Guid _stie)
        {
            try
            {
                var data = _db.tblCategoryMetaInfoes.Where(t => t.SiteID == _stie).AsEnumerable().Select(t => new CategorySEO
                {
                    ID = t.ID,
                    CategoryID = t.CategoryID,
                    SiteID = t.SiteID,
                    Title = t.Title,
                    Keywords = t.Keywords,
                    Description = t.Description,
                    CategoryName = GetProductCategoryName(t.CategoryID),
                    SiteName = GetSiteName(t.SiteID)
                }).OrderBy(x => x.CategoryName).ToList();
                return data;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
        /***************************************************/
        #region For get data by id [cat,site]

        public string GetProductCategoryName(Guid CatID)
        {
            try
            {
                var data = _db.tblCategoriesNames.FirstOrDefault(t => t.CategoryID == CatID);
                if (data != null)
                    return data.Name;
                else
                    return "";
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetSiteName(Guid Site)
        {
            try
            {
                var data = _db.tblSites.FirstOrDefault(t => t.ID == Site);
                if (data != null)
                    return data.DisplayName;
                else
                    return "";
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetProductName(Guid ProductID)
        {
            try
            {
                var data = _db.tblProductNames.FirstOrDefault(t => t.ProductID == ProductID);
                if (data != null)
                    return data.Name;
                else
                    return "";
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
        /***************************************************/
        #region DML operation Product

        public bool DeleteDataProduct(Guid ID)
        {
            try
            {
                _db.tblProductMetaInfoes.Where(t => t.ID == ID).ToList().ForEach(_db.tblProductMetaInfoes.DeleteObject);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblProductMetaInfo EditDataProduct(Guid ID)
        {
            try
            {
                return _db.tblProductMetaInfoes.FirstOrDefault(t => t.ID == ID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddEditProduct(tblProductMetaInfo Obj)
        {
            try
            {
                bool result = true;
                var data = _db.tblProductMetaInfoes.FirstOrDefault(t => t.SiteID == Obj.SiteID && t.CategoryID == Obj.CategoryID && t.ProductID == Obj.ProductID);
                if (data != null)
                {
                    data.CategoryID = Obj.CategoryID;
                    data.ProductID = Obj.ProductID;
                    data.Title = Obj.Title;
                    data.Keywords = Obj.Keywords;
                    data.Description = Obj.Description;
                    data.ModifyOn = Obj.CreatedOn;
                    data.ModifyBy = Obj.CreatedBy;
                    result = false;
                }
                else
                {
                    _db.tblProductMetaInfoes.AddObject(Obj);
                }
                _db.SaveChanges();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductSEO> GetProduct(Guid _stie)
        {
            try
            {
                var data = _db.tblProductMetaInfoes.Where(t => t.SiteID == _stie).AsEnumerable().Select(t => new ProductSEO
                {
                    ID = t.ID,
                    CategoryID = t.CategoryID,
                    ProductID = t.ProductID,
                    SiteID = t.SiteID,
                    Title = t.Title,
                    Keywords = t.Keywords,
                    Description = t.Description,
                    CategoryName = GetProductCategoryName(t.CategoryID),
                    SiteName = GetSiteName(t.SiteID),
                    ProductName = GetProductName(t.ProductID),
                }).OrderBy(x => x.ProductName).ToList();
                return data;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
        /***************************************************/
    }
    public class CategorySEO : tblCategoryMetaInfo
    {
        public String SiteName { get; set; }
        public String CategoryName { get; set; }
    }

    public class ProductSEO : tblProductMetaInfo
    {
        public String SiteName { get; set; }
        public String CategoryName { get; set; }
        public String ProductName { get; set; }
    }
    public class ProductDetailsSEO : tblProductDetailsMetaInfo
    {
        public String SiteName { get; set; }
        public String CategoryName { get; set; }
        public String ProductName { get; set; }
    }
}

