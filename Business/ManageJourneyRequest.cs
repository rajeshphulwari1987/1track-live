﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Business
{

    public class ManageJourneyRequest
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public long AddJourneyRequests(tblJourneyRequest objJourney)
        {

            try
            {
                _db.tblJourneyRequests.AddObject(objJourney);
                _db.SaveChanges();
                return objJourney.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long AddTrainResultInfo(TrainResultField trainresult)
        {

            try
            {
                return _db.InsertTrainResultInfo(trainresult.siteID, trainresult.ipAddress, trainresult.apiName, trainresult.dateOfDepart, trainresult.dateOfArrival, trainresult.from, trainresult.to, trainresult.adult, trainresult.youth, trainresult.child, trainresult.senior, trainresult.Class, trainresult.departureTime, trainresult.arrivalTime, trainresult.Title, trainresult.FName, trainresult.LName, trainresult.email, trainresult.phone, trainresult.FIPNumber, trainresult.FIPClass, trainresult.DeptTNo, trainresult.ReturnTNo, trainresult.Notes, trainresult.IsReturn).First().Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblJourneyRequest> GetAllJourneyRequests()
        {
            try
            {
                List<tblJourneyRequest> lstJR = new List<tblJourneyRequest>();
                lstJR = _db.tblJourneyRequests.ToList();
                return lstJR;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public class TrainResultField
    {
        public Guid siteID { get; set; }
        public String ipAddress { get; set; }
        public String apiName { get; set; }
        public DateTime dateOfDepart { get; set; }
        public DateTime dateOfArrival { get; set; }
        public String from { get; set; }
        public String to { get; set; }
        public Int32 adult { get; set; }
        public Int32 youth { get; set; }
        public Int32 child { get; set; }
        public Int32 senior { get; set; }
        public string Class { get; set; }
        public string departureTime { get; set; }
        public string arrivalTime { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Title { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string FIPNumber { get; set; }
        public string FIPClass { get; set; }
        public string DeptTNo { get; set; }
        public string ReturnTNo { get; set; }
        public string Notes { get; set; }
        public bool IsReturn { get; set; }

    }


}
