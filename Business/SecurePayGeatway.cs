﻿using System;
using System.Net.Http;
using System.Xml;
using System.Text;
using System.Net;
namespace Business
{
    public class SecurePayGeatway
    {
        db_1TrackEntities db = new db_1TrackEntities();
        public SecurePayResponse PaymentProcess(SecurePayRequest srequest, string paymentUrl, long OrderId)
        {
            try
            {
                var data = new SecurePayResponse();
                string result = "";
                string baseUrl = paymentUrl;
                string xmldata = @"<SecurePayMessage>";
                xmldata += "<MessageInfo> <messageID>" + srequest.MessageInfo.messageID + "</messageID><messageTimestamp>" + srequest.MessageInfo.messageTimestamp + "</messageTimestamp><timeoutValue>" + srequest.MessageInfo.timeoutValue + "</timeoutValue><apiVersion>" + srequest.MessageInfo.apiVersion + "</apiVersion></MessageInfo>";
                xmldata += "<MerchantInfo><merchantID>" + srequest.MerchantInfo.MerchantID + "</merchantID><password>" + srequest.MerchantInfo.password + "</password></MerchantInfo><RequestType>" + srequest.RequestType + "</RequestType>";
                xmldata += "<Payment> <TxnList count=\"1\"><Txn ID=\"1\"><txnType>" + srequest.Payment.TxnList.txnType + "</txnType><txnSource>" + srequest.Payment.TxnList.txnSource + "</txnSource><amount>" + srequest.Payment.TxnList.amount + "</amount><currency>" + srequest.Payment.TxnList.currency + "</currency><purchaseOrderNo>" + srequest.Payment.TxnList.purchaseOrderNo + "</purchaseOrderNo>";
                xmldata += "<CreditCardInfo><cardNumber>" + srequest.Payment.TxnList.CreditCardInfo.cardNumber + "</cardNumber><expiryDate>" + srequest.Payment.TxnList.CreditCardInfo.expiryDate + "</expiryDate><cvv>" + srequest.Payment.TxnList.CreditCardInfo.cvv + "</cvv></CreditCardInfo>";
                xmldata += "<BuyerInfo><firstName>" + srequest.ByerInfo.firstName + "</firstName><lastName>" + srequest.ByerInfo.lastName + "</lastName><zipCode>" + srequest.ByerInfo.zipCode + "</zipCode><town>" + srequest.ByerInfo.town + "</town><billingCountry>" + srequest.ByerInfo.billingCountry + "</billingCountry><deliveryCountry>" + srequest.ByerInfo.deliveryCountry + "</deliveryCountry><emailAddress>" + srequest.ByerInfo.emailAddress + "</emailAddress><ip>" + srequest.ByerInfo.ip + "</ip></BuyerInfo>";
                xmldata += "</Txn></TxnList></Payment></SecurePayMessage>";
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)SPType.Tls | (SecurityProtocolType)SPType.Tls11 | (SecurityProtocolType)SPType.Tls12;
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new System.Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/xml"));
                System.Net.Http.HttpContent content = new StringContent(xmldata, UTF8Encoding.UTF8, "application/xml");
                HttpResponseMessage messge = client.PostAsync(baseUrl, content).Result;
                if (messge.IsSuccessStatusCode)
                {
                    result = messge.Content.ReadAsStringAsync().Result;
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(result);
                    XmlNode xnr;
                    string statusCode = "0", statusDescription = string.Empty, approved = string.Empty;
                    xnr = xDoc.SelectSingleNode("/SecurePayMessage/Status/statusCode");
                    if (xnr != null)
                        statusCode = xnr.InnerText.Trim();
                    xnr = xDoc.SelectSingleNode("/SecurePayMessage/Status/statusDescription");
                    if (xnr != null)
                        statusDescription = xnr.InnerText.Trim();

                    xnr = xDoc.SelectSingleNode("/SecurePayMessage/Payment/TxnList/Txn/approved");
                    if (xnr != null)
                        approved = xnr.InnerText.Trim();
                    data = new SecurePayResponse
                    {
                        statusCode = Convert.ToInt32(statusCode),
                        statusDescription = statusDescription,
                        approved = approved
                    };
                }

                db.tblPaymentGatwayLogs.AddObject(new tblPaymentGatwayLog
                {
                    GateWayName = "securepay Request/response",
                    OrderId = OrderId,
                    Request = xmldata,
                    Response = result,
                    CratedeOn = DateTime.Now
                });
                db.SaveChanges();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class SecurePayRequest
    {
        public MessageInfo MessageInfo { get; set; }
        public MerchantInfo MerchantInfo { get; set; }
        public string RequestType { get; set; }
        public Payment Payment { get; set; }
        public ByerInfo ByerInfo { get; set; }
    }

    public class ByerInfo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string zipCode { get; set; }
        public string billingCountry { get; set; }
        public string deliveryCountry { get; set; }
        public string emailAddress { get; set; }
        public string ip { get; set; }
        public string town { get; set; }
    }

    public class MessageInfo
    {
        public string messageID { get; set; }
        public string messageTimestamp { get; set; }
        public int timeoutValue { get; set; }
        public string apiVersion { get; set; }
    }

    public class MerchantInfo
    {
        public string MerchantID { get; set; }
        public string password { get; set; }
    }

    public class Payment
    {
        public Txn TxnList { get; set; }
    }

    public class Txn
    {
        public int ID { get; set; }
        public int txnType { get; set; } //0- for standard payment
        public int txnSource { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string purchaseOrderNo { get; set; } //--IR 1110
        public CreditCardInfo CreditCardInfo { get; set; }
    }

    public class CreditCardInfo
    {
        public string cardNumber { get; set; }
        public string expiryDate { get; set; }
        public string cvv { get; set; }
        public string pan { get; set; }
        public string recurring { get; set; }
    }

    public class SecurePayResponse
    {
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public string approved { get; set; }
    }
    /*Tls version*/
    public enum SPType
    {
        Ssl3 = 48,
        Tls = 192,
        Tls11 = 768,
        Tls12 = 3072,
    }
}
