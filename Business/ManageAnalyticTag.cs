﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageAnalyticTag
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();

        public tblAnalyticTag GetTagBySiteId(Guid siteid)
        {
            return _db.tblAnalyticTags.FirstOrDefault(x => x.SiteID == siteid);
        }

        public List<tblAnalyticTag> GetListTagBySiteId(Guid siteid)
        {
            return _db.tblAnalyticTags.Where(x => x.SiteID == siteid).ToList();
        }

        public tblAnalyticTag GetTagById(Guid id)
        {
            return _db.tblAnalyticTags.FirstOrDefault(x => x.ID == id);
        }

        public bool RomoveTag(Guid id)
        {
            var rec = _db.tblAnalyticTags.FirstOrDefault(x => x.ID == id);
            if (rec != null)
            {
                _db.tblAnalyticTags.DeleteObject(rec);
                _db.SaveChanges();
            }
            return true;
        }

        public bool AddAnalyticTag(tblAnalyticTag analytictag)
        {
            try
            {
                if (analytictag.ID == new Guid())
                {

                    if (_db.tblAnalyticTags.Any(x => x.SiteID == analytictag.SiteID))
                        throw new Exception("Google Analytic Tags already added for this site.");

                    analytictag.ID = Guid.NewGuid();
                    _db.tblAnalyticTags.AddObject(analytictag);
                }
                else
                {
                    var rec = _db.tblAnalyticTags.FirstOrDefault(x => x.ID == analytictag.ID);
                    rec.SiteID = analytictag.SiteID;
                    rec.VisitTracking = analytictag.VisitTracking;
                    rec.VisitTrackingPageView = analytictag.VisitTrackingPageView;
                    rec.EventTracking = analytictag.EventTracking;
                    rec.Ecommerce = analytictag.Ecommerce;
                    rec.addImpression = analytictag.addImpression;
                    rec.ImpressionPageView = analytictag.ImpressionPageView;
                    rec.Actions = analytictag.Actions;
                    rec.SetActions = analytictag.SetActions;
                    rec.RmvActions = analytictag.RmvActions;
                    rec.RmvSetActions = analytictag.RmvSetActions;
                    rec.ChkOutActions = analytictag.ChkOutActions;
                    rec.ChkOutSetActions = analytictag.ChkOutSetActions;
                    rec.ChkOutActionsPageView = analytictag.ChkOutActionsPageView;
                    rec.PurchaseActions = analytictag.PurchaseActions;
                    rec.PurchaseSetActions = analytictag.PurchaseSetActions;
                    rec.PurchaseActionsPageView = analytictag.PurchaseActionsPageView;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class Pass
        {
            public string searchTxt { get; set; }
            public string searchResNo { get; set; }
        }

        public class P2P
        {
            public string origin { get; set; }
            public string destination { get; set; }
            public string outboundDate { get; set; }
            public string pax { get; set; }
            public string paxAdult { get; set; }
            public string paxU26 { get; set; }
            public string paxChild { get; set; }
            public string paxSenior { get; set; }
            public string Class { get; set; }
            public string maxTransfers { get; set; }
        }
    }

    public class ManageScriptingTag
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();

        public tblScriptingTag GetScriptingTagSiteId(Guid siteid)
        {
            try
            {
                return _db.tblScriptingTags.FirstOrDefault(x => x.SiteID == siteid);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblScriptingTag> GetListScriptingTagBySiteId(Guid SiteId)
        {
            try
            {
                return _db.tblScriptingTags.Where(x => x.SiteID == SiteId).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public tblScriptingTag GetScriptingTagById(Guid ID)
        {
            try
            {
                return _db.tblScriptingTags.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool RomoveScriptingTag(Guid ID)
        {
            try
            {
                var rec = _db.tblScriptingTags.FirstOrDefault(x => x.ID == ID);
                if (rec != null)
                {
                    _db.tblScriptingTags.DeleteObject(rec);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ActiveScriptingTag(Guid ID)
        {
            try
            {
                var rec = _db.tblScriptingTags.FirstOrDefault(x => x.ID == ID);
                if (rec != null)
                {
                    rec.IsActive = !rec.IsActive;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddScriptingTag(tblScriptingTag _ScriptingTag)
        {
            try
            {
                if (_ScriptingTag.ID == new Guid())
                {
                    if (_db.tblScriptingTags.Any(x => x.SiteID == _ScriptingTag.SiteID && x.PageType == _ScriptingTag.PageType))
                        throw new Exception("Scripting Tag already added for this site.");

                    _ScriptingTag.ID = Guid.NewGuid();
                    _db.tblScriptingTags.AddObject(_ScriptingTag);
                }
                else
                {
                    var rec = _db.tblScriptingTags.FirstOrDefault(x => x.ID == _ScriptingTag.ID);
                    rec.SiteID = _ScriptingTag.SiteID;
                    rec.Script = _ScriptingTag.Script;
                    rec.IsActive = _ScriptingTag.IsActive;
                    rec.PageType = _ScriptingTag.PageType;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
