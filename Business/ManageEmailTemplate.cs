﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageEmailTemplate
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();

        public void AddEditTemplate(List<MailTemplate> listmailtemp)
        {
            try
            {
                foreach (var item in listmailtemp)
                {
                    if (!db.tblMailTemplateSiteLookUps.Any(x => x.SiteId == item.SiteId && x.MailTemplateId == item.MailTemplateId))
                    {
                        db.tblMailTemplateSiteLookUps.AddObject(new tblMailTemplateSiteLookUp
                        {
                            Id = Guid.NewGuid(),
                            MailTemplateId = item.MailTemplateId,
                            SiteId = item.SiteId
                        });
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<MailTemplate> GetMailTemplatesWithSiteName()
        {
            try
            {
                var data = from tmtl in db.tblMailTemplates
                           join tmtlsl in db.tblMailTemplateSiteLookUps on tmtl.Id equals tmtlsl.MailTemplateId
                           join ts in db.tblSites on tmtlsl.SiteId equals ts.ID
                           select new MailTemplate()
                           {
                               Id = tmtlsl.Id,
                               MailTemplateName = tmtl.Name,
                               SiteName = ts.DisplayName,
                               SiteId = ts.ID,
                               templateUrl = ts.SiteURL + tmtl.PhyscialPath.Replace("~/", "")
                           };
                return data.ToList().OrderBy(x => x.SiteName).ThenBy(x => x.MailTemplateName).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<MailTemplate> GetMailTemplates()
        {
            try
            {
                return db.tblMailTemplates.Where(x => x.IsActive == true).Select(x => new MailTemplate
                {
                    Id = x.Id,
                    MailTemplateName = x.Name
                }).ToList().OrderBy(x => x.MailTemplateName).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetMailTemplateBySiteId(Guid siteid)
        {
            try
            {
                var MailTemplate = db.tblMailTemplateSiteLookUps.FirstOrDefault(y => y.SiteId == siteid);
                if (MailTemplate != null)
                {

                    var result = db.tblMailTemplates.FirstOrDefault(x => x.Id == MailTemplate.MailTemplateId);
                    return result != null ? result.PhyscialPath : null;
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool deleteTemplatesFromLookUp(Guid SiteId)
        {
            try
            {
                var data = db.tblMailTemplateSiteLookUps.FirstOrDefault(x => x.SiteId == SiteId);
                if (data != null)
                {
                    db.tblMailTemplateSiteLookUps.DeleteObject(data);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblMailTemplateSiteLookUp GetMailTemplateSiteLookUpBySiteId(Guid siteid)
        {
            try
            {
                return db.tblMailTemplateSiteLookUps.FirstOrDefault(x => x.SiteId == siteid);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblMailTemplateSiteLookUp> GetMailTemplateSiteLookUpByTemplateId(Guid TemplateId)
        {
            try
            {
                return db.tblMailTemplateSiteLookUps.Where(x => x.MailTemplateId == TemplateId).ToList();
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public class MailTemplate
    {
        public Guid Id { get; set; }
        public Guid SiteId { get; set; }
        public string SiteName { get; set; }
        public Guid MailTemplateId { get; set; }
        public string MailTemplateName { get; set; }
        public string templateUrl { get; set; }

    }
}
