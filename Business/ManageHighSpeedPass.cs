﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class ManageHighSpeedPass
    {
        db_1TrackEntities db = new db_1TrackEntities();
        public Boolean AddHighSpeedLink(tblHighSpeedPass obj)
        {
            db.tblHighSpeedPasses.AddObject(obj);
            db.SaveChanges();
            return true;
        }
        public Boolean updateHighSpeedLink(tblHighSpeedPass obj)
        {
            tblHighSpeedPass objInfo = db.tblHighSpeedPasses.Where(a => a.ID == obj.ID).FirstOrDefault();
            objInfo.Name = obj.Name;
            objInfo.Description = obj.Description;
            objInfo.Image = obj.Image;
            objInfo.ThumbImg = obj.ThumbImg;
            db.SaveChanges();
            return true;
        }
        public Boolean AddHighSpeedLinkDetail(tblHighSpeedPassDetail obj)
        {
            db.tblHighSpeedPassDetails.AddObject(obj);
            db.SaveChanges();
            return true;
        }
        public Boolean DeleteHighSpeedLinkDetail(Guid Id)
        {
            tblHighSpeedPassDetail obj = db.tblHighSpeedPassDetails.Where(a => a.ID == Id).FirstOrDefault();
            if (obj != null)
            {
                db.tblHighSpeedPassDetails.DeleteObject(obj);
                db.SaveChanges();
            }
            return true;
        }
        public HighSpeedPassInfo getHighSpeedPassInfo(Guid Id)
        {
            HighSpeedPassInfo obj = new HighSpeedPassInfo();

            var objInfo = (from a in db.tblHighSpeedPasses
                           join b in db.tblSites on a.SiteId equals b.ID
                           join c in db.tblCurrencyMsts on b.DefaultCurrencyID equals c.ID
                           where a.ID == Id
                           select new { a, b, c }).ToList().Select(ty => new HighSpeedPassInfo
                           {
                               ID = ty.a.ID,
                               Name = ty.a.Name,
                               Description = ty.a.Description,
                               Image = ty.a.Image,
                               ThumbImg = ty.a.ThumbImg,
                               CreatedBy = ty.a.CreatedBy,
                               CretedDate = ty.a.CretedDate,
                               SiteId = ty.a.SiteId,
                               SiteName = ty.b.DisplayName,
                               SiteUrl = ty.b.SiteURL,
                               Root = ty.b.Root,
                               CurrencySymbol = ty.c.Symbol
                           }).ToList();
            obj = objInfo.FirstOrDefault();
            return obj;
        }
        public List<HighSpeedPassInfo> getHighSpeedPassInfoList(Guid SiteId)
        {
            List<HighSpeedPassInfo> lst = new List<HighSpeedPassInfo>();

            var objInfo = (from a in db.tblHighSpeedPasses
                           join b in db.tblSites on a.SiteId equals b.ID
                           join c in db.tblCurrencyMsts on b.DefaultCurrencyID equals c.ID
                           where a.SiteId == SiteId
                           select new { a, b, c }).ToList().Select(ty => new HighSpeedPassInfo
                           {
                               ID = ty.a.ID,
                               Name = ty.a.Name,
                               Description = ty.a.Description,
                               Image = ty.a.Image,
                               ThumbImg = ty.a.ThumbImg,
                               CreatedBy = ty.a.CreatedBy,
                               CretedDate = ty.a.CretedDate,
                               SiteId = ty.a.SiteId,
                               SiteName = ty.b.DisplayName,
                               SiteUrl = ty.b.SiteURL,
                               Root = ty.b.Root,
                               CurrencySymbol = ty.c.Symbol,
                               Passes = getPasses(ty.a.ID),
                               totalPrice = getTotalPrice(ty.a.ID)
                           }).ToList();
            lst = objInfo.ToList();
            return lst;
        }
        public string getPasses(Guid Id)
        {
            List<GetHighSpeedPassDetailInfo_Result> lst = getHighSpeedPassDetailInfo(Id);
            string strPasses = "";
            foreach (GetHighSpeedPassDetailInfo_Result li in lst)
            {
                strPasses = strPasses + li.RailPassName + ", " + li.PassDesc + ", " + li.ClassType + ", " + li.PassCount.ToString() + "&nbsp;" + li.TrvClass + "<br>";
            }
            return strPasses;
        }
        public decimal getTotalPrice(Guid Id)
        {
            List<GetHighSpeedPassDetailInfo_Result> lst = getHighSpeedPassDetailInfo(Id);
            decimal PricePasses = 0;
            foreach (GetHighSpeedPassDetailInfo_Result obj in lst)
            {
                PricePasses = PricePasses + (obj.totalPrice.HasValue ? (obj.totalPrice.Value) : 0);
            }
            return PricePasses;
        }
        public List<GetHighSpeedPassDetailForFront_Result> GetHighSpeedPassDetailForFront(Guid Id)
        {
            return db.GetHighSpeedPassDetailForFront(Id).ToList();
        }
        public List<GetHighSpeedPassDetailInfo_Result> getHighSpeedPassDetailInfo(Guid Id)
        {
            return db.GetHighSpeedPassDetailInfo(Id).ToList();
        }
        public void DeleteHighSpeedPass(Guid ID)
        {
            try
            {
                db.tblHighSpeedPassDetails.Where(a => a.HighSpeedPassID == ID).ToList().ForEach(db.tblHighSpeedPassDetails.DeleteObject);
                db.tblHighSpeedPasses.Where(a => a.ID == ID).ToList().ForEach(db.tblHighSpeedPasses.DeleteObject);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
    public class HighSpeedPassInfo : tblHighSpeedPass
    {
        public string SiteName { get; set; }
        public string SiteUrl { get; set; }
        public string Root { get; set; }
        public string CurrencySymbol { get; set; }
        public string Passes { get; set; }
        public decimal totalPrice { get; set; }


    }
}
