//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOrderTraveller
    {
        public System.Guid ID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.Guid> Country { get; set; }
        public string PassportNo { get; set; }
        public Nullable<System.DateTime> PassStartDate { get; set; }
        public Nullable<bool> LeadPassenger { get; set; }
        public long OrderIdentity { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Nationality { get; set; }
        public string MiddleName { get; set; }
    }
}
