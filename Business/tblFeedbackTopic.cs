//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblFeedbackTopic
    {
        public tblFeedbackTopic()
        {
            this.tblFeedbacks = new HashSet<tblFeedback>();
        }
    
        public System.Guid ID { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<tblFeedback> tblFeedbacks { get; set; }
    }
}
