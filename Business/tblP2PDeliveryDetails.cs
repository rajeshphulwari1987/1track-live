//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblP2PDeliveryDetails
    {
        public System.Guid ID { get; set; }
        public Nullable<long> OrderID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public Nullable<System.Guid> CountryID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> StateID { get; set; }
    }
}
