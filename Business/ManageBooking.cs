﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.Xml;

namespace Business
{
    public class ManageBooking
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        public decimal GetCategoryOrProductBookingFeeById(Guid siteId, Guid CId, Guid PId, decimal Price)
        {
            try
            {
                var Cdata = _db.tblBookingFeeCategories.FirstOrDefault(x => x.CategoryId == CId && x.SiteId == siteId);
                var Pdata = _db.tblBookingFeeProducts.FirstOrDefault(x => x.ProductId == PId && x.SiteId == siteId);
                if (Pdata != null && Pdata.Price > 0)
                {
                    bool BookingFromDate = Pdata.BookingFromDate.HasValue ? Pdata.BookingFromDate.Value <= DateTime.Now : true;
                    bool BookingToDate = Pdata.BookingToDate.HasValue ? Pdata.BookingToDate.Value >= DateTime.Now : true;
                    bool PriceRange = ((Pdata.AmountFrom <= Price && Pdata.AmountTo >= Price) || (Pdata.AmountTo == 0 && Pdata.AmountFrom == 0));
                    if (BookingFromDate && BookingToDate && PriceRange)
                        return Pdata.Price;
                }
                else if (Cdata != null && Pdata.Price > 0)
                {
                    bool BookingFromDate = Cdata.BookingFromDate.HasValue ? Cdata.BookingFromDate.Value <= DateTime.Now : true;
                    bool BookingToDate = Cdata.BookingToDate.HasValue ? Cdata.BookingToDate.Value >= DateTime.Now : true;
                    bool PriceRange = ((Cdata.AmountFrom <= Price && Cdata.AmountTo >= Price) || (Cdata.AmountTo == 0 && Cdata.AmountFrom == 0));
                    if (BookingFromDate && BookingToDate && PriceRange)
                        return Cdata.Price;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region BritRailStockReturnReport
        public List<GetEurailPrintQueue_Result> GetEurailPrintqueue(Guid Userid)
        {
            try
            {
                return _db.GetEurailPrintQueue(Userid).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EURAILSTOCKRETURNREPORT_Result> EurailStockReturnReport(string Queueids, DateTime startdate, DateTime enddate)
        {
            try
            {
                return _db.EURAILSTOCKRETURNREPORT(Queueids, startdate, enddate).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public void FillShippingDefaultData(long Orderid, string ShipMethod)
        {
            string txtdata = "";
            try
            {
                var passdata = _db.tblPassSales.Where(x => x.OrderID == Orderid).Select(x => x.ProductID).Distinct().ToList();
                bool isShippingAllow = _db.tblProducts.Any(x => x.IsShippingAplicable == true && passdata.Contains(x.ID));

                var OrderData = _db.tblOrders.FirstOrDefault(x => x.OrderID == Orderid);
                if (OrderData != null && isShippingAllow)
                {
                    var CountryName = _db.tblOrderBillingAddresses.FirstOrDefault(o => o.OrderID == Orderid).CountryShpg;
                    Guid CountryID = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName == CountryName).CountryID;
                    var lstShip = getAllPassShippingDetail(OrderData.SiteID.Value, CountryID);
                    var ShipcurrentData = new shippingData();
                    txtdata += "BookingCart => OrderID: " + OrderData.OrderID + "; CountryId: " + CountryID + "; ";

                    if (lstShip.Any() && !string.IsNullOrEmpty(ShipMethod))
                    {
                        ShipcurrentData = lstShip.FirstOrDefault(x => x.ShippingName.ToLower() == ShipMethod.ToLower());
                        if (ShipcurrentData != null)
                        {
                            OrderData.ShippingAmount = ShipcurrentData.Price;
                            OrderData.ShippingMethod = ShipcurrentData.ShippingName;
                            OrderData.ShippingDescription = ShipcurrentData.Description;
                            _db.SaveChanges();
                            txtdata += "Method:" + ShipMethod + "; Amount: " + ShipcurrentData.Price + "; Description" + ShipcurrentData.Description + "; ";
                        }
                        else
                        {
                            ShipcurrentData = lstShip.OrderBy(x => x.Price).FirstOrDefault();
                            OrderData.ShippingAmount = ShipcurrentData.Price;
                            OrderData.ShippingMethod = ShipcurrentData.ShippingName;
                            OrderData.ShippingDescription = ShipcurrentData.Description;
                            _db.SaveChanges();
                            txtdata += "Method:" + ShipMethod + "; Amount: " + ShipcurrentData.Price + "; Description" + ShipcurrentData.Description + "; ";
                        }
                    }
                    else
                    {
                        var lstDefaultShip = getDefaultShippingDetail(OrderData.SiteID.Value);
                        if (lstDefaultShip.Any())
                        {
                            ShipcurrentData = lstDefaultShip.Select(x => new shippingData
                            {
                                ID = x.ID,
                                ShippingName = x.ShippingName,
                                Description = x.Description,
                                Price = x.Price
                            }).OrderBy(x => x.Price).FirstOrDefault();
                            OrderData.ShippingAmount = ShipcurrentData.Price;
                            OrderData.ShippingMethod = ShipcurrentData.ShippingName;
                            OrderData.ShippingDescription = ShipcurrentData.Description;
                            _db.SaveChanges();
                            txtdata += "Method:" + ShipMethod + "; Amount: " + ShipcurrentData.Price + "; Description" + ShipcurrentData.Description + "; ";
                        }
                    }
                }
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, "Payment shipping cost=> " + txtdata);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddInterrailCoefficients(Guid passSaleID, int InterrailPassType, decimal OriginalPrice, Guid ClassId, int CountryStartCode)
        {
            try
            {
                bool FirstClass = _db.tblClassMsts.FirstOrDefault(t => t.ID == ClassId).EurailCode == 1;
                if (InterrailPassType == 1)//Global Pass
                {
                    foreach (var item in _db.tblInterrailGlobalPasses)
                    {
                        decimal ExCharge = 0;
                        if (FirstClass)
                            ExCharge = item.Class_1st;
                        else
                            ExCharge = item.Class_2nd;
                        tblInterrailCoefficient obj = new tblInterrailCoefficient();
                        obj.PassSaleId = passSaleID;
                        obj.LinkId = item.Id;
                        obj.IsGlobalPass = true;
                        obj.Price = (ExCharge * OriginalPrice) / 100;
                        obj.OriginalPrice = OriginalPrice;
                        _db.tblInterrailCoefficients.AddObject(obj);
                    }
                    _db.SaveChanges();
                }
                else if (InterrailPassType == 2)//One Country Pass
                {
                    foreach (var item in _db.tblInterrailOneCountryPasses.Where(t => t.OneCountryCode == CountryStartCode).ToList())
                    {
                        decimal ExCharge = 0;
                        if (FirstClass)
                            ExCharge = item.Class_1st;
                        else
                            ExCharge = item.Class_2nd;
                        tblInterrailCoefficient obj = new tblInterrailCoefficient();
                        obj.PassSaleId = passSaleID;
                        obj.LinkId = item.Id;
                        obj.IsGlobalPass = false;
                        obj.Price = (ExCharge * OriginalPrice) / 100;
                        obj.OriginalPrice = OriginalPrice;
                        _db.tblInterrailCoefficients.AddObject(obj);
                    }
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetCategoryIsInterrailById(Guid ID)
        {
            try
            {
                return _db.tblCategories.FirstOrDefault(t => t.ID == ID).IsInterRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetIsInterrail(long orderId)
        {
            try
            {
                return _db.tblPassSales.FirstOrDefault(t => t.OrderID == orderId).tblCategory.IsInterRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetRefundAmountByID(Guid PassSaleId, Guid P2PPassSaleId, string ProductType)
        {
            try
            {
                if (ProductType == "P2P")
                {
                    var data = _db.tblOrderRefunds.FirstOrDefault(t => t.ProductID == P2PPassSaleId);
                    if (data != null)
                        return data.TotalProductRefund.Value;
                    else
                        return 0;
                }
                else
                {
                    var data = _db.tblOrderRefunds.FirstOrDefault(t => t.ProductID == PassSaleId);
                    if (data != null)
                        return data.TotalProductRefund.Value;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<getRailPassData> GetUserOrderInCreateStatusByOrderID(int OrderID)
        {
            try
            {
                List<getRailPassData> DataList = new List<getRailPassData>();
                var rec = _db.tblOrders.Where(t => t.OrderID == OrderID).OrderByDescending(t => t.CreatedOn).FirstOrDefault();
                if (rec == null)
                    return null;
                else
                {
                    long OrderId = rec.OrderID;
                    var list = (from tps in _db.tblPassSales
                                join tpsl in _db.tblPassP2PSalelookup on tps.ID equals tpsl.PassSaleID
                                join tot in _db.tblOrderTravellers on tpsl.OrderTravellerID equals tot.ID
                                join tcm in _db.tblClassMsts on tps.ClassID equals tcm.ID
                                join tp in _db.tblProductNames on tps.ProductID equals tp.ProductID
                                join ttm in _db.tblTravelerMsts on tps.TravellerID equals ttm.ID
                                join tpvn in _db.tblProductValidUpToNames on tps.ValidityID equals tpvn.ID
                                join tc in _db.tblCategories on tps.CategoryID equals tc.ID
                                where tps.OrderID == OrderId
                                select new { tps, IsBritrail = tc.IsBritRailPass, ClassName = tcm.Name, ProductName = tp.Name, ProductId = tps.ProductID, TrvlName = ttm.Name, ValidityName = tpvn.Name, FirstName = tot.FirstName, LastName = tot.LastName, PassportNo = tot.PassportNo, PassStartDate = tot.PassStartDate, Country = tot.Country, DOB = tot.DOB }).ToList();
                    foreach (var data in list)
                    {
                        getRailPassData obj = new getRailPassData();

                        obj.CategoryID = data.tps.CategoryID.ToString();
                        obj.ClassID = data.tps.ClassID.ToString();
                        obj.ClassName = data.ClassName;
                        obj.Commission = data.tps.Commition.ToString();
                        obj.CountryEndCode = data.tps.CountryEndCode.ToString();
                        obj.CountryStartCode = data.tps.CountryStartCode.ToString();
                        obj.Id = data.tps.ID;
                        obj.MarkUp = data.tps.MarkUp.ToString();
                        obj.OriginalPrice = getOriginalPricePassSale(data.tps.ProductXML).ToString();
                        obj.PrdtId = data.tps.ProductID.ToString();
                        obj.PrdtName = data.ProductName;
                        obj.Price = data.tps.Price.ToString();//-----------
                        obj.Qty = "1";
                        obj.SalePrice = getCostPriceInProductXMLPassSale(data.tps.ProductXML);
                        obj.TravellerID = data.tps.TravellerID.ToString();
                        obj.TravellerName = data.TrvlName;
                        obj.ValidityID = data.tps.ValidityID.ToString();
                        obj.ValidityName = data.ValidityName;
                        obj.PassportIsVisible = GetProductPassportValidityById(data.ProductId.Value);
                        obj.CountryLevelIDs = data.tps.Country1 + "," + data.tps.Country2 + "," + data.tps.Country3 + "," + data.tps.Country4 + "," + data.tps.Country5;
                        obj.FirstName = data.FirstName;
                        obj.LastName = data.LastName;
                        obj.PassportNo = data.PassportNo;
                        obj.Country = data.Country;
                        obj.DOB = data.DOB.HasValue ? data.DOB.Value.ToString("dd/MMM/yyyy") : null;
                        obj.PassStartDate = data.PassStartDate;
                        obj.MonthValidity = new ManageProduct().GetMonthValidity(data.ProductId.Value);
                        obj.OrderIdentity = data.tps.OrderID.Value;
                        obj.IsBritrail = data.IsBritrail;
                        DataList.Add(obj);
                    }
                    return DataList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getUserP2PPassDesc(Guid? ProductID, string Type)
        {
            string PassDetail = "";
            if (Type != "P2P")
            {

                var objPS = (from tp in _db.tblPassSales
                             join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                             join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                             join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                             join c in _db.tblClassMsts on tp.ClassID equals c.ID
                             join od in _db.tblOrders on tp.OrderID equals od.OrderID
                             where tp.ID == ProductID
                             orderby t.Name
                             select new { Productname = p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
                if (objPS != null)
                {
                    string country =
                        (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                    PassDetail = objPS.Productname + country + ", " +
                        objPS.PassName + ", " +
                        objPS.PassDesc + ", " +
                        objPS.ClassType;
                }
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.FirstOrDefault(a => a.ID == ProductID);
                if (objP2P != null)
                {
                    string status = "Issue";
                    if (objP2P.Status == true)
                        status = "Confirm";
                    string str = string.Empty;
                    if (!string.IsNullOrEmpty(objP2P.ReservationCode))
                    {
                        string apiname = "PNR Number :";
                        if (!string.IsNullOrEmpty(objP2P.ApiName))
                            if (objP2P.ApiName.Trim().ToLower() == "evolvi")
                                apiname = "TOD Ref :";

                        str = "<tr><td width='50%'>" + apiname + "</td><td width='50%'><b>" + objP2P.ReservationCode + "</b></td></tr>" +
                            "<tr><td width='50%'>Status:</td><td width='50%'>" + status + "</td></tr>";
                    }
                    PassDetail = "Rail Tickets</td></tr><tr><td colspan='3'><table width='100%'>" +
                         "<tr><td width='50%'>Train Number:</td><td width='50%'>" + objP2P.TrainNo.ToString() + "</td></tr>" + str +
                         "<tr><td width='50%'>From:</td><td width='50%'>" + objP2P.From + "</td></tr>" +
                         "<tr><td width='50%'>To:</td><td width='50%'>" + objP2P.To + "</td></tr>" +
                         "<tr><td width='50%'>Departure Date:</td><td width='50%' class='dateofDeparture'>" + Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MM/yyyy") + "</td></tr>" +
                         "<tr><td width='50%'>Departure Time:</td><td width='50%'>" + Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") + "</td></tr>" +
                         "<tr><td width='50%'>Arrival Date:</td><td width='50%'>" + Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MM/yyyy") + "</td></tr>" + "<tr><td width='50%'>Arrival Time:</td><td width='50%'>" + Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm") + "</td></tr>" + "</table>";
                }
            }
            return PassDetail;
        }

        public string GetP2PRefundAmount(long orderId)
        {
            try
            {
                var data = (from TPR in _db.tblP2PRefund
                            join TPRT in _db.tblP2PRefundTitle on TPR.ID equals TPRT.P2PRefundId
                            where TPR.OrderNo == orderId && TPR.IsRefunded == true
                            select new { TPRT.Amount, TPRT.DeductedAmount, TPRT.RefundedAmount }).ToList().GroupBy(t => t).Sum(t => t.Key.RefundedAmount);
                return data.ToString("F2");
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateP2PJourneyPrice(long OrderId, decimal AdminFee, decimal BookingFee)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (objOrder != null)
                {
                    objOrder.AdminFee = AdminFee;
                    objOrder.BookingFee = BookingFee;
                    _db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateP2PSalePrice(Guid Id, decimal Price)
        {
            try
            {
                tblP2PSale objp2psale = _db.tblP2PSale.FirstOrDefault(x => x.ID == Id);
                if (objp2psale != null)
                {
                    objp2psale.Price = Price;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblSite> GetSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int updateP2PBookingFeeIsApplicable(tblP2PBookingFee booking)
        {
            try
            {
                var list = _db.tblP2PBookingFee.FirstOrDefault(t => t.SiteId == booking.SiteId);
                if (list != null)
                    list.IsApplicable = !list.IsApplicable;
                else
                    _db.AddTotblP2PBookingFee(booking);
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateP2PBookingFee(tblP2PBookingFee booking)
        {
            try
            {
                var list = _db.tblP2PBookingFee.FirstOrDefault(t => t.SiteId == booking.SiteId);
                if (list != null)
                {
                    list.Fee = booking.Fee;
                    list.IsPercentage = booking.IsPercentage;
                    list.ModifiedBy = booking.CreatedBy;
                    list.ModifiedOn = booking.CreatedOn;
                }
                else
                    _db.AddTotblP2PBookingFee(booking);
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DelpasssaleData(Guid ID)
        {
            try
            {
                _db.tblPassSales.Where(t => t.ID == ID).ToList().ForEach(_db.tblPassSales.DeleteObject);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrderStatusForManualBooking(int Status, long orderid, Guid agentId)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                if (objOrder != null)
                {
                    if (Status == 3)
                        objOrder.PaymentDate = DateTime.Now;

                    objOrder.Status = Status;
                    objOrder.AgentID = agentId;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetAdminFeeByOrderId(long orderId)
        {
            try
            {
                var refund = _db.tblOrders.FirstOrDefault(t => t.OrderID == orderId);
                return refund.AdminFee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<P2PbookingFee> GetP2PAPIBookingFee(Guid SiteId)
        {
            try
            {
                var list = _db.tblP2PBookingFee.Where(x => x.SiteId == SiteId && x.ApiName != null && x.IsSite == false).AsEnumerable().Select(x => new P2PbookingFee
                {
                    ID = Convert.ToString(x.ID),
                    SiteID = Convert.ToString(x.SiteId),
                    IsApplicable = Convert.ToString(x.IsApplicable),
                    Name = Convert.ToString(x.ApiName),
                    Fee = Convert.ToString(x.Fee),
                    IsPercentage = Convert.ToString(x.IsPercentage)
                }).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateP2PAPIBookingFee(tblP2PBookingFee booking, int Caseno)
        {
            try
            {
                var list = _db.tblP2PBookingFee.FirstOrDefault(t => t.SiteId == booking.SiteId && t.IsSite == false && t.ApiName == booking.ApiName);
                if (list != null)
                {
                    if (Caseno == 1)
                        list.IsApplicable = !list.IsApplicable;
                    else if (Caseno == 2)
                    {

                        list.Fee = booking.Fee;
                        list.IsPercentage = booking.IsPercentage;
                        list.ModifiedBy = booking.CreatedBy;
                        list.ModifiedOn = booking.CreatedOn;
                    }
                }
                else
                    _db.AddTotblP2PBookingFee(booking);

                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<P2PbookingFee> GetP2PBookingFee()
        {
            try
            {
                var list = (from u in _db.tblSites
                            join bg in _db.tblP2PBookingFee on u.ID equals bg.SiteId into temp
                            from j in temp.DefaultIfEmpty()
                            where u.IsDelete == false
                            select new
           {
               SiteID = (Guid?)u.ID,
               IsApplicable = (bool?)j.IsApplicable,
               u.DisplayName,
               ID = (Guid?)j.ID,
               Fee = (decimal?)j.Fee,
               IsPercentage = (decimal?)j.IsPercentage
           }).ToList().Select(ty => new P2PbookingFee
                             {
                                 ID = Convert.ToString(ty.ID),
                                 SiteID = Convert.ToString(ty.SiteID),
                                 IsApplicable = Convert.ToString(ty.IsApplicable),
                                 Name = Convert.ToString(ty.DisplayName),
                                 Fee = Convert.ToString(ty.Fee),
                                 IsPercentage = Convert.ToString(ty.IsPercentage)

                             }).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSiteAliasByID(Guid SiteId)
        {
            try
            {
                return _db.tblSites.FirstOrDefault(x => x.ID == SiteId).SiteAlias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean UpdatePaymentReferenceId(long OrderNo, string ReferenceId)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == OrderNo).FirstOrDefault();
                objOrder.PaymentId = ReferenceId;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<P2PCommissionFee> GetP2PCommissionFee()
        {
            try
            {
                var list = (from S in _db.tblSites
                            join CF in _db.tblP2PCommissionFee on S.ID equals CF.SiteId into x
                            from xx in x.DefaultIfEmpty()
                            where S.IsDelete == false
                            select new
                            {
                                ID = (Guid?)xx.ID,
                                SiteID = (Guid?)S.ID,
                                SiteName = S.DisplayName,
                                IsApplicable = (bool?)xx.IsApplicable,
                                Name = S.DisplayName,
                                IsPercentage = (decimal?)xx.IsPercentage,
                            }).OrderBy(t => t.Name).ToList().Select(t => new P2PCommissionFee
                            {
                                ID = t.ID,
                                SiteID = t.SiteID,
                                IsApplicable = t.IsApplicable ?? false,
                                Name = t.SiteName,
                                IsPercentage = t.IsPercentage ?? 0
                            }).ToList();

                return list;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int updateP2PBookingCommissionFeeStatus(tblP2PCommissionFee CF)
        {
            try
            {
                var list = _db.tblP2PCommissionFee.FirstOrDefault(t => t.SiteId == CF.SiteId);
                if (list != null)
                {
                    if (list.IsApplicable)
                        list.IsApplicable = false;
                    else
                        list.IsApplicable = true;
                }
                else
                    _db.AddTotblP2PCommissionFee(CF);
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateP2PCommissionFee(P2PCommissionFee CommissionFee, Guid ID)
        {
            //tblP2PCommissionFee CF = new tblP2PCommissionFee();
            try
            {

                tblP2PCommissionFee CF = _db.tblP2PCommissionFee.FirstOrDefault(t => t.SiteId == CommissionFee.SiteID);
                if (CF == null)
                    CF = new tblP2PCommissionFee();
                CF.ID = CommissionFee.ID.Value;
                CF.SiteId = CommissionFee.SiteID.Value;
                CF.IsPercentage = CommissionFee.IsPercentage;
                CF.ModifiedBy = CommissionFee.ModifiedBy;
                CF.ModifiedOn = CommissionFee.ModifiedOn;
                CF.CreatedBy = CommissionFee.CreatedBy;
                CF.CreatedOn = CommissionFee.CreatedOn;
                if (ID == Guid.Empty)
                    _db.AddTotblP2PCommissionFee(CF);
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid? GetCurrencyIdBySiteId(Guid id)
        {
            return _db.tblSites.FirstOrDefault(x => x.ID == id).DefaultCurrencyID;
        }

        public Guid? GetCurrencyIdByProductId(Guid id)
        {
            return _db.tblProducts.FirstOrDefault(x => x.ID == id).CurrencyID;
        }

        /*get printresponse for P2POrderDetails*/
        public List<PrintResponse> GetPrintResponse(Guid passSaleId)
        {
            var st = (from p2psale in _db.tblP2PSale
                      where p2psale.ID == passSaleId
                      select p2psale).FirstOrDefault();

            var prntResp = new List<PrintResponse>();
            if (st != null)
            {
                prntResp.Add(new PrintResponse
                {
                    ReservationCode = st.ReservationCode,
                    status = st.Status.ToString(),
                    URL = st.PdfURL
                });
            }
            return prntResp;
        }

        public void UpdateIsRegional(bool IsRegional, long orderid)
        {
            try
            {
                var objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                if (objOrder != null)
                {
                    objOrder.IsRegional = IsRegional;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrderData(decimal ShippingCost, string ShipMethod, string ShipDesc, string CollectionStation, long orderid)
        {
            try
            {
                var objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                if (objOrder != null)
                {
                    objOrder.ShippingAmount = ShippingCost;
                    objOrder.ShippingMethod = ShipMethod;
                    objOrder.ShippingDescription = ShipDesc;
                    objOrder.CollectionStation = CollectionStation;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetDiscountCodeByOrderId(int OrderId)
        {
            try
            {
                var data = _db.tblOrderDiscounts.FirstOrDefault(t => t.OrderId == OrderId);
                if (data != null)
                    return data.DiscountCode;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblOrderBillingAddress GetBookingDetailsByOrderId(long OrderID)
        {
            try
            {
                return _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            }
            catch (Exception ex) { throw ex; }
        }

        public string getP2PDetailsForEmail(Guid? ProductID, List<PrintResponse> PrintUrl, string trainType, bool isEvolvi, bool isAgent, string EvolviTandC = "")
        {
            string PassDetail = "";
            tblP2PSale objP2P = _db.tblP2PSale.FirstOrDefault(a => a.ID == ProductID);
            if (objP2P != null)
            {
                string passenger = "";
                if (Convert.ToInt32(objP2P.Adult) > 0)
                    passenger += objP2P.Adult + " Adult";
                if (Convert.ToInt32(objP2P.Youth) > 0)
                    passenger += "," + objP2P.Youth + " Youth";
                if (Convert.ToInt32(objP2P.Senior) > 0)
                    passenger += "," + objP2P.Senior + " Senior";
                if (Convert.ToInt32(objP2P.Children) > 0)
                    passenger += "," + objP2P.Children + " Child";

                PassDetail += "<tr><td colspan='2' align='left' valign='top' style='font-size:14px; color:#5c5c5c;  font-family:Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>" + objP2P.From + " - " + objP2P.To + "</strong></td><td width='35%' align='right' valign='top' style='font-size:14px; color:#5c5c5c;  font-family:Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>##Price##</strong></td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Traveller name:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>##Name##</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>##NetAmount##</td><tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Departs :</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") + " " + Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MMM/yyyy") + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'></td><tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Arrives :</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm") + " " + Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MMM/yyyy") + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'></td><tr>";
                if (isEvolvi && !isAgent)
                    PassDetail += "";
                else
                    PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Train Number:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objP2P.TrainNo.ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td><tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Passengers :</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + passenger + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td><tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Class Of Travel :</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objP2P.Class + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td><tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Ticket Type :</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objP2P.SeviceName + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td><tr>";
            }
            return PassDetail;
        }

        public string getPassDetailsForEmail(Guid? ProductID, List<PrintResponse> PrintUrl, string trainType, string EvolviTandC = "")
        {
            string PassDetail = "";
            var objPS = (from tp in _db.tblPassSales
                         join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                         join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                         join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                         join c in _db.tblClassMsts on tp.ClassID equals c.ID
                         join od in _db.tblOrders on tp.OrderID equals od.OrderID
                         where tp.ID == ProductID
                         orderby t.Name
                         select new { Productname = p.Name, ProductUserFriendlyName = !string.IsNullOrEmpty(p.UserFriendlyName) ? p.UserFriendlyName : p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
            if (objPS != null)
            {
                string country =
                    (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                    country = country + ")";

                PassDetail += "<tr><td colspan='2' align='left' valign='top' style='font-size:14px; color:#5c5c5c;  font-family:Arial, Helvetica, sans-serif;'><strong>" + objPS.ProductUserFriendlyName + "</strong></td><td width='35%' align='right' valign='top' style='font-size:14px; color:#5c5c5c;  font-family:Arial, Helvetica, sans-serif;'><strong>##Price##</strong></td></tr>";
                if (!string.IsNullOrEmpty(country))
                    PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Countries:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + country + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>##NetAmount##</td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Traveller name:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>##Name##</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + (string.IsNullOrEmpty(country) ? "##NetAmount##" : "<strong></strong>") + "</td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Type:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objPS.PassName + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Validity:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objPS.PassDesc + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Class:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objPS.ClassType + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
            }
            return PassDetail;
        }

        public string getPassDetailsForEmailReceipt(Guid? ProductID, List<PrintResponse> PrintUrl, string trainType, string EvolviTandC = "")
        {
            string PassDetail = "";
            var objPS = (from tp in _db.tblPassSales
                         join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                         join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                         join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                         join c in _db.tblClassMsts on tp.ClassID equals c.ID
                         join od in _db.tblOrders on tp.OrderID equals od.OrderID
                         where tp.ID == ProductID
                         orderby t.Name
                         select new { Productname = p.Name, ProductUserFriendlyName = !string.IsNullOrEmpty(p.UserFriendlyName) ? p.UserFriendlyName : p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
            if (objPS != null)
            {
                string country =
                    (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                   (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                    country = country + ")";

                PassDetail += "<tr><td colspan='2' align='left' valign='top' style='font-size:14px; color:#5c5c5c;  font-family:Arial, Helvetica, sans-serif;'><strong>" + objPS.ProductUserFriendlyName + "</strong></td><td width='35%' align='right' valign='top' style='font-size:14px; color:#5c5c5c;  font-family:Arial, Helvetica, sans-serif;'><strong>##Price##</strong></td></tr>";
                if (!string.IsNullOrEmpty(country))
                    PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Countries:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + country + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>##NetAmount##</td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Traveller name:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>##Name##</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + (string.IsNullOrEmpty(country) ? "##NetAmount##" : "<strong></strong>") + "</td></tr>";
                PassDetail += "##DOB##";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Type:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objPS.PassName + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Validity:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objPS.PassDesc + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                PassDetail += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Class:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + objPS.ClassType + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
            }
            return PassDetail;
        }

        public List<ManageP2PSaleData> GetP2PSaleListByOrderID(long OrderID)
        {
            try
            {
                var result = (from A in _db.tblP2PSale
                              join B in _db.tblPassP2PSalelookup on A.ID equals B.PassSaleID
                              join C in _db.tblOrders on B.OrderID equals C.OrderID
                              where C.OrderID == OrderID
                              select new { A, B, C }).OrderBy(x => x.B.ShortOrder).ToList();
                if (result != null && result.Count > 0)
                {
                    return result.Select(x => new ManageP2PSaleData
                    {
                        OrderID = x.C.OrderID,
                        ID = x.A.ID,
                        From = x.A.From,
                        To = x.A.To,
                        TrainNo = !string.IsNullOrEmpty(x.A.TrainNo) ? x.A.TrainNo : "",
                        Passenger = x.A.Passenger,
                        Price = x.A.Price.HasValue ? x.A.Price.Value : 0,
                        Class = x.A.Class,
                        SeviceName = !string.IsNullOrEmpty(x.A.SeviceName) ? x.A.SeviceName : "",
                        TicketProtection = x.A.TicketProtection.HasValue ? x.A.TicketProtection.Value : 0,
                        ReservationCode = !string.IsNullOrEmpty(x.A.ReservationCode) ? x.A.ReservationCode : "",
                        PdfURL = !string.IsNullOrEmpty(x.A.PdfURL) ? x.A.PdfURL : "",
                        DeliveryOption = !string.IsNullOrEmpty(x.A.DeliveryOption) ? x.A.DeliveryOption : "",
                        ApiName = x.A.ApiName,
                        EvSleeper = !string.IsNullOrEmpty(x.A.EvSleeper) ? x.A.EvSleeper : ""
                    }).ToList();
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public string getP2PDetailsForEmailTicket(long OrderID, string EvolviTandC, string PdfImageUrl)
        {
            string PassDetail = "";
            var result = (from A in _db.tblP2PSale
                          join B in _db.tblPassP2PSalelookup on A.ID equals B.PassSaleID
                          where B.OrderID == OrderID
                          select new { A, B }).OrderBy(x => x.B.ShortOrder).ToList();
            if (result != null && result.Count > 0)
            {
                PassDetail += "<table width='100%' border='0' cellspacing='0' cellpadding='5' align='left' class='deviceWidth'>";
                if (result.Any(x => x.A.ApiName.ToUpper() == "BENE"))
                {
                    foreach (var item in result)
                    {
                        if (!string.IsNullOrEmpty(item.A.PdfURL))
                            PassDetail += "<tr><td colspan='2' align='center' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><img width='32' width='32' src='" + PdfImageUrl + "'/>" + "  " + "<a target='_blank' href='" + item.A.PdfURL + "' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-decoration: underline;color: blue;'>(Download)</a></td></tr>";
                    }
                    PassDetail += "<tr><td width='35%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>DNR Number:</td><td width='65%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + (!string.IsNullOrEmpty(result.FirstOrDefault().A.ReservationCode) ? result.FirstOrDefault().A.ReservationCode : "") + "</strong></td></tr>";
                }
                else if (result.Any(x => x.A.ApiName.ToUpper() == "ITALIA"))
                {
                    foreach (var item in result)
                    {
                        if (!string.IsNullOrEmpty(item.A.PdfURL))
                            PassDetail += "<tr><td colspan='2' align='center' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><img width='32' width='32' src='" + PdfImageUrl + "'/>" + "  " + "<a target='_blank' href='" + item.A.PdfURL + "' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-decoration: underline;color: blue;'>(Download)</a></td></tr>";
                    }
                    PassDetail += "<tr><td width='35%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>PNR Number:</td><td width='65%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + (!string.IsNullOrEmpty(result.FirstOrDefault().A.ReservationCode) ? result.FirstOrDefault().A.ReservationCode : "") + "</strong></td></tr>";
                }
                else if (result.Any(x => x.A.ApiName.ToUpper() == "EVOLVI"))
                {
                    PassDetail += "<tr><td colspan='2' align='center' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + (!string.IsNullOrEmpty(result.FirstOrDefault().A.ReservationCode) ? result.FirstOrDefault().A.ReservationCode : "") + "</strong></td></tr>";
                    PassDetail += "<tr><td colspan='2' align='center' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><a target='_blank' href='" + EvolviTandC + "' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-decoration: underline;color: blue;'>(How to collect your ticket)</a></td></tr>";
                }
                PassDetail += "</table>";
            }
            return PassDetail;
        }

        #region Booking list
        public List<BookingConditionList> GetBookingConditionList(Guid siteId)
        {
            try
            {
                var olist = (from sBook in _db.tblBookingConditions
                             join s in _db.tblSites
                             on sBook.SiteID equals s.ID
                             where sBook.SiteID == siteId
                             select new { s, sBook }).ToList();

                var list = olist.Distinct().Select(x => new BookingConditionList
                {
                    ID = x.sBook.ID,
                    Title = x.sBook.Title.Trim(),
                    SiteName = x.s.DisplayName,
                    IsActive = x.sBook.IsActive
                }).ToList().ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblBookingCondition GetBookingCondById(Guid id)
        {
            try
            {
                return _db.tblBookingConditions.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblBookingCondition> GetBookingBySiteid(Guid siteId)
        {
            try
            {
                return _db.tblBookingConditions.Where(x => x.SiteID == siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveBooking(Guid id)
        {
            try
            {
                var objBook = _db.tblBookingConditions.FirstOrDefault(x => x.ID == id);
                if (objBook != null)
                    objBook.IsActive = !objBook.IsActive;
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBookingCondition(Guid id)
        {
            try
            {
                var rec = _db.tblBookingConditions.FirstOrDefault(x => x.ID == id);
                _db.tblBookingConditions.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddBooking(tblBookingCondition oBook)
        {
            try
            {
                var book = new tblBookingCondition
                {
                    ID = oBook.ID,
                    SiteID = oBook.SiteID,
                    Title = oBook.Title,
                    Description = oBook.Description,
                    Contract = oBook.Contract,
                    BookingsReservations = oBook.BookingsReservations,
                    Validity = oBook.Validity,
                    FaresPayment = oBook.FaresPayment,
                    TicketRefunds = oBook.TicketRefunds,
                    LostDamagedTickets = oBook.LostDamagedTickets,
                    Baggage = oBook.Baggage,
                    ForceMajeure = oBook.ForceMajeure,
                    GoverningLaw = oBook.GoverningLaw,
                    BoardingTimes = oBook.BoardingTimes,
                    TimetableInformation = oBook.TimetableInformation,
                    Variations = oBook.Variations,
                    IsActive = oBook.IsActive
                };

                _db.AddTotblBookingConditions(book);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateBookingCondition(tblBookingCondition book)
        {
            try
            {
                var _book = _db.tblBookingConditions.FirstOrDefault(x => x.ID == book.ID);
                if (_book != null)
                {
                    _book.Title = book.Title;
                    _book.Description = book.Description;
                    _book.SiteID = book.SiteID;
                    _book.Contract = book.Contract;
                    _book.BookingsReservations = book.BookingsReservations;
                    _book.Validity = book.Validity;
                    _book.FaresPayment = book.FaresPayment;
                    _book.TicketRefunds = book.TicketRefunds;
                    _book.LostDamagedTickets = book.LostDamagedTickets;
                    _book.Baggage = book.Baggage;
                    _book.ForceMajeure = book.ForceMajeure;
                    _book.GoverningLaw = book.GoverningLaw;
                    _book.BoardingTimes = book.BoardingTimes;
                    _book.TimetableInformation = book.TimetableInformation;
                    _book.Variations = book.Variations;
                    _book.IsActive = book.IsActive;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class BookingConditionList : tblBookingCondition
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region PrivacyPolicy

        public List<tblPrivacyPolicy> GetPrivacyBySiteid(Guid siteId)
        {
            try
            {
                return _db.tblPrivacyPolicies.Where(x => x.SiteID == siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PrivacyPolicyList> GetPrivacyList(Guid siteId)
        {
            try
            {
                var olist = (from sp in _db.tblPrivacyPolicies
                             join s in _db.tblSites
                             on sp.SiteID equals s.ID
                             where sp.SiteID == siteId
                             select new { s, sp }).ToList();

                var list = olist.Distinct().Select(x => new PrivacyPolicyList
                {
                    ID = x.sp.ID,
                    Title = x.sp.Title.Trim(),
                    SiteName = x.s.DisplayName,
                    IsActive = x.sp.IsActive
                }).ToList().ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class PrivacyPolicyList : tblPrivacyPolicy
        {
            public string SiteName { get; set; }
        }

        public void ActiveInactivePrivacy(Guid id)
        {
            try
            {
                var objBook = _db.tblPrivacyPolicies.FirstOrDefault(x => x.ID == id);
                if (objBook != null)
                    objBook.IsActive = !objBook.IsActive;
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeletePrivacyPolicy(Guid id)
        {
            try
            {
                var rec = _db.tblPrivacyPolicies.FirstOrDefault(x => x.ID == id);
                _db.tblPrivacyPolicies.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPrivacyPolicy GetPrivacyById(Guid id)
        {
            try
            {
                return _db.tblPrivacyPolicies.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddPrivacy(tblPrivacyPolicy oPriv)
        {
            try
            {
                var privacy = new tblPrivacyPolicy
                {
                    ID = oPriv.ID,
                    SiteID = oPriv.SiteID,
                    Title = oPriv.Title,
                    Description = oPriv.Description,
                    YourConsent = oPriv.YourConsent,
                    InfoCollected = oPriv.InfoCollected,
                    UseInfo = oPriv.UseInfo,
                    DisclosureInfo = oPriv.DisclosureInfo,
                    Person18 = oPriv.Person18,
                    Cookies = oPriv.Cookies,
                    SecurityInfo = oPriv.SecurityInfo,
                    PersonalData = oPriv.PersonalData,
                    AlterationsPrivacy = oPriv.AlterationsPrivacy,
                    IsActive = oPriv.IsActive
                };

                _db.AddTotblPrivacyPolicies(privacy);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdatePrivacy(tblPrivacyPolicy oPriv)
        {
            try
            {
                var _privacy = _db.tblPrivacyPolicies.FirstOrDefault(x => x.ID == oPriv.ID);
                if (_privacy != null)
                {
                    _privacy.Title = oPriv.Title;
                    _privacy.Description = oPriv.Description;
                    _privacy.SiteID = oPriv.SiteID;
                    _privacy.YourConsent = oPriv.YourConsent;
                    _privacy.InfoCollected = oPriv.InfoCollected;
                    _privacy.UseInfo = oPriv.UseInfo;
                    _privacy.DisclosureInfo = oPriv.DisclosureInfo;
                    _privacy.Person18 = oPriv.Person18;
                    _privacy.Cookies = oPriv.Cookies;
                    _privacy.SecurityInfo = oPriv.SecurityInfo;
                    _privacy.PersonalData = oPriv.PersonalData;
                    _privacy.AlterationsPrivacy = oPriv.AlterationsPrivacy;
                    _privacy.IsActive = oPriv.IsActive;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Conditions of use

        public List<tblConditionsofUse> GetConditionsBySiteid(Guid siteId)
        {
            try
            {
                return _db.tblConditionsofUses.Where(x => x.SiteID == siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ConditionsList> GetConditionsList(Guid siteId)
        {
            try
            {
                var olist = (from sp in _db.tblConditionsofUses
                             join s in _db.tblSites
                             on sp.SiteID equals s.ID
                             where sp.SiteID == siteId
                             select new { s, sp }).ToList();

                var list = olist.Distinct().Select(x => new ConditionsList
                {
                    ID = x.sp.ID,
                    Title = x.sp.Title.Trim(),
                    SiteName = x.s.DisplayName,
                    IsActive = x.sp.IsActive
                }).ToList().ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveConditions(Guid id)
        {
            try
            {
                var objBook = _db.tblConditionsofUses.FirstOrDefault(x => x.ID == id);
                if (objBook != null)
                    objBook.IsActive = !objBook.IsActive;
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteConditions(Guid id)
        {
            try
            {
                var rec = _db.tblConditionsofUses.FirstOrDefault(x => x.ID == id);
                _db.tblConditionsofUses.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddConditions(tblConditionsofUse oCond)
        {
            try
            {
                var conditions = new tblConditionsofUse
                {
                    ID = oCond.ID,
                    SiteID = oCond.SiteID,
                    Title = oCond.Title,
                    Description = oCond.Description,
                    WarrantyLiability = oCond.WarrantyLiability,
                    Products = oCond.Products,
                    UseWebsite = oCond.UseWebsite,
                    AntiViralSoftware = oCond.AntiViralSoftware,
                    Copyright = oCond.Copyright,
                    IsActive = oCond.IsActive
                };

                _db.AddTotblConditionsofUses(conditions);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateConditions(tblConditionsofUse oCond)
        {
            try
            {
                var _conditions = _db.tblConditionsofUses.FirstOrDefault(x => x.ID == oCond.ID);
                if (_conditions != null)
                {
                    _conditions.Title = oCond.Title;
                    _conditions.Description = oCond.Description;
                    _conditions.SiteID = oCond.SiteID;
                    _conditions.WarrantyLiability = oCond.WarrantyLiability;
                    _conditions.Products = oCond.Products;
                    _conditions.UseWebsite = oCond.UseWebsite;
                    _conditions.AntiViralSoftware = oCond.AntiViralSoftware;
                    _conditions.Copyright = oCond.Copyright;
                    _conditions.IsActive = oCond.IsActive;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblConditionsofUse GetConditionsById(Guid id)
        {
            try
            {
                return _db.tblConditionsofUses.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ConditionsList : tblConditionsofUse
        {
            public string SiteName { get; set; }
        }

        #endregion

        #region TextTemplate
        public List<tblTextContent> GetTextTemplateBySiteid(Guid siteId)
        {
            try
            {
                return _db.tblTextContents.Where(x => x.SiteID == siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TextTemplate> GetTextTemplate(Guid siteId)
        {
            try
            {
                var olist = (from sp in _db.tblTextContents
                             join s in _db.tblSites
                             on sp.SiteID equals s.ID
                             where sp.SiteID == siteId
                             select new { s, sp }).ToList();

                var list = olist.Distinct().Select(x => new TextTemplate
                {
                    ID = x.sp.ID,
                    Title = x.sp.Title.Trim(),
                    SiteName = x.s.DisplayName,
                }).ToList().ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddTextTemplate(tblTextContent oTxt)
        {
            try
            {
                var txt = new tblTextContent
                {
                    ID = oTxt.ID,
                    SiteID = oTxt.SiteID,
                    Title = oTxt.Title,
                    Description = oTxt.Description,
                    CreatedBy = oTxt.CreatedBy,
                    CreatedOn = oTxt.CreatedOn
                };

                _db.AddTotblTextContents(txt);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateTextTemplate(tblTextContent oTxt)
        {
            try
            {
                var _txt = _db.tblTextContents.FirstOrDefault(x => x.ID == oTxt.ID);
                if (_txt != null)
                {
                    _txt.Title = oTxt.Title;
                    _txt.Description = oTxt.Description;
                    _txt.SiteID = oTxt.SiteID;
                    _txt.ModifyBy = oTxt.ModifyBy;
                    _txt.ModifyOn = oTxt.ModifyOn;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteTextTemplate(Guid id)
        {
            try
            {
                var rec = _db.tblTextContents.FirstOrDefault(x => x.ID == id);
                _db.tblTextContents.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblTextContent GetTextTemplateById(Guid id)
        {
            try
            {
                return _db.tblTextContents.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public class TextTemplate : tblTextContent
        {
            public string SiteName { get; set; }
        }

        #endregion

        #region Booking Cart Region
        public tblOrderBillingAddress GetBillingShippingAddress(long OrderId)
        {
            try
            {
                var list = _db.tblOrderBillingAddresses.FirstOrDefault(ty => ty.OrderID == OrderId);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateShippingAddress(long OrderId, tblOrderBillingAddress Obj)
        {
            try
            {
                tblOrderBillingAddress gObj = _db.tblOrderBillingAddresses.FirstOrDefault(ty => ty.OrderID == OrderId);
                gObj.Address1Shpg = Obj.Address1Shpg;
                gObj.Address2Shpg = Obj.Address2Shpg;
                gObj.CityShpg = Obj.CityShpg;
                gObj.EmailAddressShpg = Obj.EmailAddressShpg;
                gObj.FirstNameShpg = Obj.FirstNameShpg;
                gObj.LastNameShpg = Obj.LastNameShpg;
                gObj.StateShpg = Obj.StateShpg;
                gObj.PostcodeShpg = Obj.PostcodeShpg;
                gObj.TitleShpg = Obj.TitleShpg;
                gObj.CountryShpg = Obj.CountryShpg;
                gObj.PhoneShpg = Obj.PhoneShpg;
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUpdateShipBillAddress(long OrderId, tblOrderBillingAddress Obj)
        {
            try
            {
                tblOrderBillingAddress gObj = _db.tblOrderBillingAddresses.FirstOrDefault(ty => ty.OrderID == OrderId);
                if (gObj != null)
                {
                    gObj.Address1 = Obj.Address1;
                    gObj.Address2 = Obj.Address2;
                    gObj.City = Obj.City;
                    gObj.EmailAddress = Obj.EmailAddress;
                    gObj.FirstName = Obj.FirstName;
                    gObj.LastName = Obj.LastName;
                    gObj.State = Obj.StateShpg;
                    gObj.Postcode = Obj.Postcode;
                    gObj.Title = Obj.Title;
                    gObj.Country = Obj.Country;

                    gObj.Address1Shpg = Obj.Address1Shpg;
                    gObj.Address2Shpg = Obj.Address2Shpg;
                    gObj.CityShpg = Obj.CityShpg;
                    gObj.EmailAddressShpg = Obj.EmailAddressShpg;
                    gObj.FirstNameShpg = Obj.FirstNameShpg;
                    gObj.LastNameShpg = Obj.LastNameShpg;
                    gObj.StateShpg = Obj.StateShpg;
                    gObj.PostcodeShpg = Obj.PostcodeShpg;
                    gObj.TitleShpg = Obj.TitleShpg;
                    gObj.CountryShpg = Obj.CountryShpg;
                    _db.SaveChanges();
                }
                else
                {
                    Obj.OrderID = OrderId;
                    Obj.ID = Guid.NewGuid();
                    _db.tblOrderBillingAddresses.AddObject(Obj);
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblOrderTraveller GetP2POrderTravellerDetailByP2PSId(Guid ID)
        {
            tblPassP2PSalelookup obj = _db.tblPassP2PSalelookup.FirstOrDefault(a => a.ID == ID);
            tblOrderTraveller objT = new tblOrderTraveller();
            if (obj != null)
            {
                if (obj.OrderTravellerID != null)
                {
                    objT = _db.tblOrderTravellers.FirstOrDefault(a => a.ID == obj.OrderTravellerID);
                }
                else
                {
                    objT = null;
                }
            }
            return objT;
        }

        public tblOrderTraveller GetTraveller(Guid ID)
        {
            try
            {
                var list = _db.tblOrderTravellers.FirstOrDefault(ty => ty.ID == ID);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTraveller(Guid Id, tblOrderTraveller obj)
        {
            try
            {
                tblOrderTraveller gobj = _db.tblOrderTravellers.FirstOrDefault(ty => ty.ID == Id);
                if (gobj != null)
                {
                    gobj.Title = obj.Title;
                    gobj.FirstName = obj.FirstName;
                    gobj.MiddleName = obj.MiddleName;
                    gobj.LastName = obj.LastName;
                    gobj.Country = obj.Country;
                    gobj.PassportNo = obj.PassportNo;
                    gobj.Nationality = obj.Nationality;
                    gobj.PassStartDate = obj.PassStartDate;
                    gobj.LeadPassenger = obj.LeadPassenger;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddEditLeadPassengerByPassP2PSaleId(Guid Id, tblOrderTraveller obj)
        {
            try
            {
                tblPassP2PSalelookup objPP = _db.tblPassP2PSalelookup.FirstOrDefault(a => a.ID == Id);
                if (objPP != null)
                {
                    if (objPP.OrderTravellerID != null)
                    {
                        tblOrderTraveller gobj = _db.tblOrderTravellers.FirstOrDefault(ty => ty.ID == objPP.OrderTravellerID);
                        gobj.Country = obj.Country;
                        gobj.FirstName = obj.FirstName;
                        gobj.LastName = obj.LastName;
                        gobj.LeadPassenger = obj.LeadPassenger;
                        _db.SaveChanges();
                    }
                    else
                    {
                        obj.ID = Guid.NewGuid();
                        _db.tblOrderTravellers.AddObject(obj);
                        _db.SaveChanges();
                        objPP.OrderTravellerID = obj.ID;
                        _db.SaveChanges();

                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getCountryNameByIDs(Guid? IDs)
        {
            try
            {
                var list = _db.tblProductEurailCountires.FirstOrDefault(ty => ty.EurailCountryID == IDs);
                if (list != null)
                    return list.Country;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetsiteCountryCode(long? OrderId)
        {
            try
            {
                var siteid = _db.tblOrders.FirstOrDefault(t => t.OrderID == OrderId).SiteID;

                var list = from a in _db.tblCountriesMsts
                           join tcs in _db.tblCountrySiteLookUps on a.CountryID equals tcs.CountryID
                           join ts in _db.tblSites on tcs.SiteID equals ts.ID
                           where ts.ID == siteid
                           select a.EurailCode;
                return (list.FirstOrDefault().HasValue ? list.FirstOrDefault().Value.ToString() : null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetCurrencyMultiplier(string shortCode, Guid productId, long OrderId)
        {
            try
            {
                Guid siteId = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId).SiteID.Value;
                return _db.GetCurrencyMultiplier(shortCode, productId, siteId).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddPassSale(tblPassSale PSale, Guid LookUpID, string SalePrice, string OriginalPrice)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<Product>");
                sb.Append("<ID>" + PSale.ProductID + "</ID>");
                sb.Append("<ProductTypeID>" + PSale.CategoryID + "</ProductTypeID>");
                var ProductName = _db.tblProductNames.FirstOrDefault(a => a.ProductID == PSale.ProductID);
                var ValidityName = _db.tblProductValidUpToNames.FirstOrDefault(a => a.ID == PSale.ValidityID);
                var tValidity = _db.tblTravelerMsts.FirstOrDefault(a => a.ID == PSale.TravellerID);
                var classT = _db.tblClassMsts.FirstOrDefault(a => a.ID == PSale.ClassID);
                var CateGoryName = _db.tblCategoriesNames.FirstOrDefault(a => a.CategoryID == PSale.CategoryID);

                #region Added Extra Parameter
                Guid validityID = _db.tblProductValidUpToNames.FirstOrDefault(x => x.ID == PSale.ValidityID).ProductValidUpToID;
                var rec = _db.tblProductValidUpToes.FirstOrDefault(x => x.ID == validityID);
                bool IsFlexi = rec != null ? rec.IsFlexi : false;
                var recPr = _db.tblProducts.FirstOrDefault(x => x.ID == PSale.ProductID);
                bool? isPromoPass = recPr != null ? recPr.IsPromoPass : false;
                string passcode = "0";
                decimal Databaseprice = 0;
                if (!string.IsNullOrEmpty(OriginalPrice))
                {
                    Databaseprice = Convert.ToDecimal(OriginalPrice);
                }
                var pp = _db.tblProductPrices.FirstOrDefault(x => x.ClassID == PSale.ClassID && x.TravelerID == PSale.TravellerID && x.ValidityID == validityID && x.ProductID == PSale.ProductID && x.Price == Databaseprice);
                if (pp != null)
                    passcode = pp.PassTypeCode.ToString();
                #endregion
                sb.Append("<Name>" + ProductName.Name.Replace("&", "and") + "</Name>");
                sb.Append("<PassType>" + CateGoryName.Name + "</PassType>");
                sb.Append("<Vailidity>" + ValidityName.Name + "</Vailidity>");
                if (tValidity != null)
                {
                    sb.Append("<Traveller>" + tValidity.Name + "</Traveller>");
                    sb.Append("<AgeFrom>" + tValidity.FromAge + "</AgeFrom>");
                    sb.Append("<AgeTo>" + tValidity.ToAge + "</AgeTo>");
                }
                else
                {
                    sb.Append("<Traveller></Traveller>");
                    sb.Append("<AgeFrom></AgeFrom>");
                    sb.Append("<AgeTo></AgeTo>");
                }
                sb.Append("<SiteCountryCode>" + GetsiteCountryCode(PSale.OrderID) + "</SiteCountryCode>");
                sb.Append("<OriginalPrice>" + OriginalPrice + "</OriginalPrice>");
                sb.Append("<Class>" + classT.Name + "</Class>");
                sb.Append("<CountryCodeStart>" + PSale.CountryStartCode + "</CountryCodeStart>");
                sb.Append("<CountryCodeEnd>" + PSale.CountryEndCode + "</CountryCodeEnd>");

                List<CountryName> listCountry = new List<CountryName>();
                if (PSale.Country1 != new Guid())
                    listCountry.Add(new CountryName { Country = GetCountryNameByCountryID(PSale.Country1) });
                if (PSale.Country2 != new Guid())
                    listCountry.Add(new CountryName { Country = GetCountryNameByCountryID(PSale.Country2) });
                if (PSale.Country3 != new Guid())
                    listCountry.Add(new CountryName { Country = GetCountryNameByCountryID(PSale.Country3) });
                if (PSale.Country4 != new Guid())
                    listCountry.Add(new CountryName { Country = GetCountryNameByCountryID(PSale.Country4) });
                if (PSale.Country5 != new Guid())
                    listCountry.Add(new CountryName { Country = GetCountryNameByCountryID(PSale.Country5) });
                string countryName = "";
                foreach (var item in listCountry.OrderBy(x => x.Country).ToList())
                    countryName = string.IsNullOrEmpty(countryName) ? item.Country : countryName + " - " + item.Country;
                if (PSale.CountryStartCode > 1000 && PSale.CountryStartCode < 3000)
                    countryName = GetCountryNameByCountryCode(Convert.ToInt32(PSale.CountryStartCode));
                sb.Append("<Countries>" + countryName + "</Countries>");
                sb.Append("<Tarrif></Tarrif>");
                string IsFlexiDay = ValidityName.Name.ToLower();
                if (IsFlexi && (IsFlexiDay.Contains("in") || IsFlexiDay.Contains("within")) && IsFlexiDay.Split(' ').Where(t => t.Contains("day")).Count() > 1)
                {
                    int array = System.Text.RegularExpressions.Regex.Replace(IsFlexiDay, "[^0-9]", " ").Split(' ').Where(s => !string.IsNullOrWhiteSpace(s)).Select(Int32.Parse).Max();
                    sb.Append("<LastDateDaysIn>" + array + "</LastDateDaysIn>");
                }
                var PData = _db.tblProducts.FirstOrDefault(a => a.ID == PSale.ProductID);
                bool IsItxPass = PData != null ? PData.IsItxPass : false;
                sb.Append("<DaysIn>" + rec.Day + "</DaysIn>");
                sb.Append("<Months>" + rec.Month + "</Months>");
                sb.Append("<IsFlexi>" + IsFlexi + "</IsFlexi>");
                sb.Append("<IsPromo>" + isPromoPass + "</IsPromo>");
                sb.Append("<IsItxPass>" + PData.IsItxPass + "</IsItxPass>");
                sb.Append("<PassTypeCode>" + passcode + "</PassTypeCode>");
                sb.Append("<TravellerCat>" + tValidity.EurailCode.ToString().Trim() + "</TravellerCat>");
                sb.Append("<ClassCode>" + classT.EurailCode + "</ClassCode>");
                var currency = (from a in _db.tblOrders join b in _db.tblSites on a.SiteID equals b.ID where a.OrderID == PSale.OrderID select b.DefaultCurrencyID).SingleOrDefault();
                sb.Append("<CostCurrencyID>" + currency.Value + "</CostCurrencyID>");
                sb.Append("<SupplierCurrencyID>" + GetSupplierCurerencyIDByProduct(PSale.ProductID) + "</SupplierCurrencyID>");
                string price = Convert.ToDecimal(PSale.Price).ToString();
                sb.Append("<Cost>" + SalePrice + "</Cost>");
                sb.Append("</Product>");
                tblPassP2PSalelookup objPSLookupForUpdate = _db.tblPassP2PSalelookup.Where(a => a.PassSaleID == PSale.ID).FirstOrDefault();
                if (objPSLookupForUpdate != null)
                {
                    tblPassSale objPS = _db.tblPassSales.Where(a => a.ID == PSale.ID).FirstOrDefault();
                    objPS.OrderID = PSale.OrderID;
                    objPS.ProductID = PSale.ProductID;
                    objPS.TravellerID = PSale.TravellerID;
                    objPS.ValidityID = PSale.ValidityID;
                    objPS.Price = objPS.Price;
                    objPS.ClassID = PSale.ClassID;
                    objPS.CategoryID = PSale.CategoryID;
                    objPS.Commition = PSale.Commition;
                    objPS.MarkUp = PSale.MarkUp;
                    objPS.ProductXML = sb.ToString();
                    objPS.TicketProtection = PSale.TicketProtection;
                    objPS.CountryStartCode = PSale.CountryStartCode;
                    objPS.CountryEndCode = PSale.CountryEndCode;
                    objPS.Country1 = PSale.Country1;
                    objPS.Country2 = PSale.Country2;
                    objPS.Country3 = PSale.Country3;
                    objPS.Country4 = PSale.Country4;
                    objPS.Country5 = PSale.Country5;
                    objPS.CPBookingFee = PSale.CPBookingFee;
                    objPS.IsBestseller = PData.BestSeller;
                    objPS.IsSpecialOffer = PData.IsSpecialOffer;
                }
                else
                {
                    var savePassSale = new tblPassSale
                    {
                        ID = PSale.ID,
                        OrderID = PSale.OrderID,
                        ProductID = PSale.ProductID,
                        TravellerID = PSale.TravellerID,
                        ValidityID = PSale.ValidityID,
                        Price = PSale.Price,
                        ClassID = PSale.ClassID,
                        Commition = PSale.Commition,
                        MarkUp = PSale.MarkUp,
                        CategoryID = PSale.CategoryID,
                        TicketProtection = PSale.TicketProtection,
                        ProductXML = sb.ToString(),
                        CountryStartCode = PSale.CountryStartCode,
                        CountryEndCode = PSale.CountryEndCode,
                        Country1 = PSale.Country1,
                        Country2 = PSale.Country2,
                        Country3 = PSale.Country3,
                        Country4 = PSale.Country4,
                        Country5 = PSale.Country5,
                        Site_MP = PSale.Site_MP,
                        USD_MP = PSale.USD_MP,
                        SEU_MP = PSale.SEU_MP,
                        SBD_MP = PSale.SBD_MP,
                        GBP_MP = PSale.GBP_MP,
                        EUR_MP = PSale.EUR_MP,
                        INR_MP = PSale.INR_MP,
                        SEK_MP = PSale.SEK_MP,
                        NZD_MP = PSale.NZD_MP,
                        CAD_MP = PSale.CAD_MP,
                        JPY_MP = PSale.JPY_MP,
                        AUD_MP = PSale.AUD_MP,
                        CHF_MP = PSale.CHF_MP,
                        EUB_MP = PSale.EUB_MP,
                        EUT_MP = PSale.EUT_MP,
                        GBB_MP = PSale.GBB_MP,
                        THB_MP = PSale.THB_MP,
                        SGD_MP = PSale.SGD_MP,
                        Site_USD = PSale.Site_USD,
                        Site_SEU = PSale.Site_SEU,
                        Site_SBD = PSale.Site_SBD,
                        Site_GBP = PSale.Site_GBP,
                        Site_EUR = PSale.Site_EUR,
                        Site_INR = PSale.Site_INR,
                        Site_SEK = PSale.Site_SEK,
                        Site_NZD = PSale.Site_NZD,
                        Site_CAD = PSale.Site_CAD,
                        Site_JPY = PSale.Site_JPY,
                        Site_AUD = PSale.Site_AUD,
                        Site_CHF = PSale.Site_CHF,
                        Site_EUT = PSale.Site_EUT,
                        Site_EUB = PSale.Site_EUB,
                        Site_GBB = PSale.Site_GBB,
                        Site_THB = PSale.Site_THB,
                        Site_SGD = PSale.Site_SGD,
                        IsHighSpeedPass = PSale.IsHighSpeedPass,
                        CPBookingFee = PSale.CPBookingFee,
                        IsBestseller = PData.BestSeller,
                        IsSpecialOffer = PData.IsSpecialOffer
                    };
                    _db.AddTotblPassSales(savePassSale);
                    string PassType = "Pass";
                    var OrderData = _db.tblOrders.FirstOrDefault(t => t.OrderID == PSale.OrderID);
                    if (OrderData != null)
                        OrderData.TrvType = PassType;
                    _db.tblPassP2PSalelookup.AddObject(new tblPassP2PSalelookup { ID = Guid.NewGuid(), OrderID = PSale.OrderID, PassSaleID = PSale.ID, ProductType = PassType });
                }
                _db.SaveChanges();
                return PSale.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid GetSupplierCurerencyIDByProduct(Guid? id)
        {
            try
            {
                var rec = _db.tblProducts.FirstOrDefault(x => x.ID == id);
                return rec != null ? rec.CurrencyID : new Guid();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCountryNameByCountryID(Guid? id)
        {
            try
            {
                var rec = _db.tblProductEurailCountires.FirstOrDefault(x => x.EurailCountryID == id);
                return rec != null ? rec.Country : string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCountryNameByCountryCode(int code)
        {
            try
            {
                List<CountryName> list = new List<CountryName>();

                var rec = _db.tblEurailCountries.FirstOrDefault(x => x.CountryCode == code);
                list.Add(new CountryName { Country = rec.Country1 });
                if (!string.IsNullOrWhiteSpace(rec.Country2))
                    list.Add(new CountryName { Country = rec.Country2 });

                string str = "";
                foreach (var item in list.OrderBy(x => x.Country).ToList())
                    if (item.Country.Trim() != "-")
                        str = string.IsNullOrEmpty(str) ? item.Country : str + "," + item.Country;
                return str;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddOrderTraveller(tblOrderTraveller objTraveller, Guid passSaleID)
        {
            try
            {
                tblPassP2PSalelookup objPSLookupForUpdate = _db.tblPassP2PSalelookup.Where(a => a.OrderTravellerID == objTraveller.ID).FirstOrDefault();
                if (objPSLookupForUpdate != null)
                {
                    tblOrderTraveller objT = _db.tblOrderTravellers.Where(a => a.ID == objTraveller.ID).FirstOrDefault();
                    objT.PassStartDate = objTraveller.PassStartDate;
                    objT.Title = objTraveller.Title;
                    objT.FirstName = objTraveller.FirstName;
                    objT.MiddleName = objTraveller.MiddleName;
                    objT.LastName = objTraveller.LastName;
                    objT.Country = objTraveller.Country;
                    objT.PassportNo = objT.PassportNo;
                    objT.Nationality = objT.Nationality;
                    objT.DOB = objT.DOB;
                    _db.SaveChanges();
                }
                else
                {

                    _db.AddTotblOrderTravellers(objTraveller);
                    _db.SaveChanges();
                }
                //Update P2P sale info in Lookup table
                tblPassP2PSalelookup objPSLookup = _db.tblPassP2PSalelookup.Where(a => a.PassSaleID == passSaleID).FirstOrDefault();
                objPSLookup.OrderTravellerID = objTraveller.ID;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long UpdateBrowserDetails(long OrderId, string BrowserDetails)
        {
            try
            {
                var objOrder = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId);
                if (objOrder != null)
                {
                    objOrder.BrowserDetails = BrowserDetails;
                    _db.SaveChanges();
                }
                return OrderId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long updateExistsOrder(string AffiliateCode, Guid AgentID, Guid USERID, Guid SiteId, long OrderId, string BrowserDetails = "")
        {
            try
            {
                var objOrder = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId);
                if (objOrder != null)
                {
                    objOrder.CreatedOn = DateTime.Now;
                    objOrder.Status = 1;
                    objOrder.SiteID = SiteId;
                    objOrder.AffiliateCode = AffiliateCode; ;
                    objOrder.UserID = USERID;
                    objOrder.AgentID = AgentID;
                    objOrder.IpAddress = GetIpAddress();
                    objOrder.BookingFee = 0;
                    objOrder.ShippingAmount = 0;
                    objOrder.BrowserDetails = BrowserDetails;
                    _db.SaveChanges();
                }
                return OrderId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long CreateOrder(string AffiliateCode, Guid AgentID, Guid USERID, Guid SiteId, string Page_Accept, string BrowserDetails = "")
        {
            try
            {
                tblOrder objOrder = new tblOrder();
                objOrder.CreatedOn = DateTime.Now;
                objOrder.Status = 1;
                objOrder.SiteID = SiteId;
                objOrder.AffiliateCode = AffiliateCode; ;
                objOrder.UserID = USERID;
                objOrder.AgentID = AgentID;
                objOrder.TrvType = "Pass";
                objOrder.IpAddress = GetIpAddress();
                objOrder.AgentReferenceNo = "0";
                objOrder.page_accept = Page_Accept;
                objOrder.BrowserDetails = BrowserDetails;
                _db.tblOrders.AddObject(objOrder);
                _db.SaveChanges();
                long OrderId = objOrder.OrderID;
                return OrderId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsDOB(Guid ProductId)
        {
            try
            {
                var data = _db.tblProductCategoriesLookUps.FirstOrDefault(x => x.ProductID == ProductId);
                return data != null ? data.tblCategory.IsDOB : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetAffiliateCode(Guid ID)
        {
            try
            {
                var data = _db.tblAffiliateUsers.FirstOrDefault(t => t.ID == ID);
                if (data != null)
                    return data.Code;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long CreateOrder(string AffiliateCode, Guid AgentID, Guid USERID, Guid SiteId, string TrvType, string AgentReferenceNo, string Page_Accept)
        {
            try
            {
                tblOrder objOrder = new tblOrder();
                objOrder.CreatedOn = DateTime.Now;
                objOrder.Status = 1;
                objOrder.SiteID = SiteId;
                objOrder.AffiliateCode = AffiliateCode; ;
                objOrder.UserID = USERID;
                objOrder.AgentID = AgentID;
                objOrder.TrvType = TrvType;
                objOrder.IpAddress = GetIpAddress();
                objOrder.AgentReferenceNo = AgentReferenceNo;
                objOrder.page_accept = Page_Accept;
                _db.tblOrders.AddObject(objOrder);
                _db.SaveChanges();
                long OrderId = objOrder.OrderID;
                return OrderId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean DeleteOldOrderPassSales(long OrderID)
        {
            try
            {
                var list = _db.tblPassP2PSalelookup.Where(t => t.OrderID == OrderID).ToList();
                if (list != null && list.Count > 0)
                {
                    foreach (var data in list)
                    {
                        var list_data = _db.tblOrderTravellers.FirstOrDefault(ty => ty.ID == data.OrderTravellerID);
                        if (list_data != null)
                            _db.DeleteObject(list_data);
                    }

                    _db.tblPassSales.Where(t => t.OrderID == OrderID).ToList().ForEach(_db.tblPassSales.DeleteObject);
                    _db.tblOrderBillingAddresses.Where(t => t.OrderID == OrderID).ToList().ForEach(_db.tblOrderBillingAddresses.DeleteObject);
                    _db.tblPassP2PSalelookup.Where(t => t.OrderID == OrderID).ToList().ForEach(_db.tblPassP2PSalelookup.DeleteObject);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<getRailPassData> GetLastAgentOrderInCreateStatus(Guid AgentId)
        {
            try
            {

                List<getRailPassData> DataList = new List<getRailPassData>();
                var rec = _db.tblOrders.Where(t => t.AgentID == AgentId && t.Status == 1).OrderByDescending(t => t.CreatedOn).FirstOrDefault();
                if (rec == null)
                    return null;
                else
                {
                    long OrderId = rec.OrderID;
                    var list = (from tps in _db.tblPassSales
                                join tpsl in _db.tblPassP2PSalelookup on tps.ID equals tpsl.PassSaleID
                                join tot in _db.tblOrderTravellers on tpsl.OrderTravellerID equals tot.ID
                                join tcm in _db.tblClassMsts on tps.ClassID equals tcm.ID
                                join tp in _db.tblProductNames on tps.ProductID equals tp.ProductID
                                join ttm in _db.tblTravelerMsts on tps.TravellerID equals ttm.ID
                                join tpvn in _db.tblProductValidUpToNames on tps.ValidityID equals tpvn.ID
                                where tps.OrderID == OrderId
                                select new { tps, ClassName = tcm.Name, ProductName = tp.Name, ProductId = tps.ProductID, TrvlName = ttm.Name, ValidityName = tpvn.Name, FirstName = tot.FirstName, LastName = tot.LastName, PassportNo = tot.PassportNo, PassStartDate = tot.PassStartDate, Country = tot.Country, DOB = tot.DOB }).ToList();
                    foreach (var data in list)
                    {
                        getRailPassData obj = new getRailPassData();
                        obj.CategoryID = data.tps.CategoryID.ToString();
                        obj.ClassID = data.tps.ClassID.ToString();
                        obj.ClassName = data.ClassName;
                        obj.Commission = data.tps.Commition.ToString();
                        obj.CountryEndCode = data.tps.CountryEndCode.ToString();
                        obj.CountryStartCode = data.tps.CountryStartCode.ToString();
                        obj.Id = data.tps.ID;
                        obj.MarkUp = data.tps.MarkUp.ToString();
                        obj.OriginalPrice = getOriginalPricePassSale(data.tps.ProductXML).ToString();
                        obj.PrdtId = data.tps.ProductID.ToString();
                        obj.PrdtName = data.ProductName;
                        obj.Price = data.tps.Price.ToString();//-----------
                        obj.Qty = "1";
                        obj.SalePrice = getCostPriceInProductXMLPassSale(data.tps.ProductXML);
                        obj.TravellerID = data.tps.TravellerID.ToString();
                        obj.TravellerName = data.TrvlName;
                        obj.ValidityID = data.tps.ValidityID.ToString();
                        obj.ValidityName = data.ValidityName;
                        obj.PassportIsVisible = GetProductPassportValidityById(data.ProductId.Value);
                        obj.CountryLevelIDs = data.tps.Country1 + "," + data.tps.Country2 + "," + data.tps.Country3 + "," + data.tps.Country4 + "," + data.tps.Country5;
                        obj.FirstName = data.FirstName;
                        obj.LastName = data.LastName;
                        obj.PassportNo = data.PassportNo;
                        obj.Country = data.Country;
                        obj.DOB = data.DOB.HasValue ? data.DOB.Value.ToString("dd/MMM/yyyy") : null;
                        obj.PassStartDate = data.PassStartDate;
                        obj.MonthValidity = new ManageProduct().GetMonthValidity(data.ProductId.Value);
                        obj.OrderIdentity = data.tps.OrderID.Value;
                        DataList.Add(obj);
                    }
                    return DataList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getCostPriceInProductXMLPassSale(string ProductXML)
        {
            try
            {
                string price = "0.00";
                if (ProductXML.Contains("Cost"))
                {
                    XDocument doc = XDocument.Parse(ProductXML);
                    price = doc.Descendants("Product").Select(s => new { Cost = s.Element("Cost").Value }).FirstOrDefault().Cost;
                }
                return price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool GetProductPassportValidityById(Guid id)
        {
            var Passport = _db.tblProducts.FirstOrDefault(x => x.ID == id).PassportValid;
            return Passport.HasValue ? Passport.Value : false;
        }

        public decimal getOriginalPricePassSale(string ProductXML)
        {
            try
            {
                decimal price = 0;
                if (ProductXML.Contains("OriginalPrice"))
                {
                    XDocument doc = XDocument.Parse(ProductXML);
                    price = Convert.ToDecimal(doc.Descendants("Product").Select(s => new { OriginalProductprice = s.Element("OriginalPrice").Value }).FirstOrDefault().OriginalProductprice);
                }
                return price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean UpdateSiteToOrder(long OrderID, Guid SiteID)
        {

            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == OrderID).FirstOrDefault();
                objOrder.SiteID = SiteID;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetIpAddress()
        {
            string result = string.Empty;
            string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ip))
            {
                string[] ipRange = ip.Split(',');
                int le = ipRange.Length - 1;
                result = ipRange[0];
            }
            else
            {
                result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return result;
        }

        public Boolean IsFraudIPorCard(Guid SiteId, String CardNumber, String CardHolderName)
        {
            try
            {
                String IPAddress = GetIpAddress();
                Boolean isfraud = _db.tblBlockFraudUsers.Any(x => x.IPAddress == IPAddress);
                if (!isfraud && !string.IsNullOrEmpty(CardHolderName))
                    isfraud = _db.tblBlockFraudUsers.ToList().Any(x => x.CardHolderName.ToLower() == CardHolderName.ToLower());
                if (!isfraud && !string.IsNullOrEmpty(CardNumber))
                    isfraud = _db.tblBlockFraudUsers.Any(x => x.CardNumber == CardNumber);
                return isfraud;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddP2PBookingOrder(tblP2PSale objP2PSale, long OrderId)
        {
            try
            {
                //Add P2P Sale Product information
                objP2PSale.ID = Guid.NewGuid();
                _db.tblP2PSale.AddObject(objP2PSale);
                _db.SaveChanges();

                tblPassP2PSalelookup objP2PLookup = new tblPassP2PSalelookup();
                objP2PLookup.ID = Guid.NewGuid();
                objP2PLookup.OrderID = OrderId;
                objP2PLookup.PassSaleID = objP2PSale.ID;
                objP2PLookup.ProductType = "P2P";
                _db.tblPassP2PSalelookup.AddObject(objP2PLookup);
                _db.SaveChanges();

                return objP2PLookup.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Guid AddP2PBookingOrder(tblP2PSale objP2PSale, long OrderId, out Guid P2PSaleId)
        {
            try
            {
                //Add P2P Sale Product information
                objP2PSale.ID = Guid.NewGuid();
                _db.tblP2PSale.AddObject(objP2PSale);
                _db.SaveChanges();
                P2PSaleId = objP2PSale.ID;

                tblPassP2PSalelookup objP2PLookup = new tblPassP2PSalelookup();
                objP2PLookup.ID = Guid.NewGuid();
                objP2PLookup.OrderID = OrderId;
                objP2PLookup.PassSaleID = objP2PSale.ID;
                objP2PLookup.ProductType = "P2P";
                _db.tblPassP2PSalelookup.AddObject(objP2PLookup);
                _db.SaveChanges();

                return objP2PLookup.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public tblAdminUser GetAdminUserbyId(Guid ID)
        {
            try
            {
                tblAdminUser objAUser = _db.tblAdminUsers.FirstOrDefault(a => a.ID == ID);

                return objAUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AddTraveller(tblOrderTraveller objTraveller, Guid LookUpID)
        {
            try
            {
                tblPassP2PSalelookup objPSLookupForUpdate = _db.tblPassP2PSalelookup.Where(a => a.OrderTravellerID == objTraveller.ID).FirstOrDefault();
                if (objPSLookupForUpdate != null)
                {
                    tblOrderTraveller objT = _db.tblOrderTravellers.Where(a => a.ID == objTraveller.ID).FirstOrDefault();
                    objT.PassStartDate = objTraveller.PassStartDate;
                    objT.Title = objTraveller.Title;
                    objT.FirstName = objTraveller.FirstName;
                    objT.LastName = objTraveller.LastName;
                    objT.Country = objTraveller.Country;
                    objT.PassportNo = objT.PassportNo;
                    _db.SaveChanges();
                }
                else
                {
                    _db.tblOrderTravellers.AddObject(objTraveller);
                    _db.SaveChanges();

                    //Update P2P sale info in Lookup table
                    tblPassP2PSalelookup objPSLookup = _db.tblPassP2PSalelookup.Where(a => a.ID == LookUpID).FirstOrDefault();
                    objPSLookup.OrderTravellerID = objTraveller.ID;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteCartPass(Guid? TID, Guid? LID, Guid? ProductID)
        {

            try
            {
                tblOrderTraveller objT = _db.tblOrderTravellers.Where(a => a.ID == TID).FirstOrDefault();
                _db.tblOrderTravellers.DeleteObject(objT);
                _db.SaveChanges();

                tblPassSale objPass = _db.tblPassSales.Where(a => a.ID == ProductID).FirstOrDefault();
                _db.tblPassSales.DeleteObject(objPass);
                _db.SaveChanges();

                tblPassP2PSalelookup objPPSL = _db.tblPassP2PSalelookup.Where(a => a.ID == LID).FirstOrDefault();
                _db.tblPassP2PSalelookup.DeleteObject(objPPSL);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<GetAllCartData_Result> GetAllCartData(long orderId)
        {
            try
            {
                List<GetAllCartData_Result> lst = _db.GetAllCartData(orderId).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetTicketProtectionAmt(Guid PassSaleID, decimal TktPtAmt)
        {
            try
            {
                var list = _db.tblPassSales.SingleOrDefault(ty => ty.ID == PassSaleID);
                if (list != null)
                {
                    list.TicketProtection = TktPtAmt;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetorderUserid(long OrdrId, Guid UserID)
        {
            try
            {
                var list = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrdrId);
                if (list != null)
                {
                    list.UserID = UserID;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<getbookingcartdata> GetAllPassSale(long OrderID, string Type, decimal tkPrice)
        {
            try
            {
                List<getbookingcartdata> lstPassDetail = new List<getbookingcartdata>();

                if (Type != "")
                {
                    List<GetAllCartData_Result> lstCartItem = _db.GetAllCartData(OrderID).ToList();
                    if (lstCartItem.Count() > 0)
                    {
                        var list = lstCartItem.Select(x => new getbookingcartdata
                        {
                            LID = x.LID,
                            OrderID = x.OrderID,
                            ProductID = x.PassSaleID,
                            ProductType = x.ProductType,
                            TravellerID = x.OrderTravellerID,
                            TicketProtection = Convert.ToBoolean(x.TicketProtection.HasValue && x.TicketProtection > 0 ? true : false),
                            PassDesc = getPassDesc(x.PassSaleID, x.ProductType),
                            Price = getPrice(x.PassSaleID, x.ProductType),
                            Traveller = (x.Title != null ? (x.Title + " " + x.FirstName + " " + x.MiddleName + " " + x.LastName) : ""),
                            DateOfJourney = JourneyDate(x.PassSaleID, x.ProductType, x.PassStartDate),
                            TicketProtectionPrice = tkPrice,
                            PassSaleTicketProtectionPrice = x.TicketProtection.HasValue ? x.TicketProtection.Value : 0,
                            OrderIdentity = x.OrderIdentity.HasValue ? x.OrderIdentity.Value : 0
                        }).ToList();
                        lstPassDetail = list.OrderBy(a => a.OrderIdentity).ToList();
                    }
                }
                else
                {
                    List<GetAllCartData_Result> lstCartItem = _db.GetAllCartData(OrderID).ToList();
                    if (lstCartItem.Count() > 0)
                    {
                        var list = lstCartItem.Select(x => new getbookingcartdata
                        {
                            LID = x.LID,
                            OrderID = x.OrderID,
                            ProductID = x.PassSaleID,
                            ProductType = x.ProductType,
                            TravellerID = x.OrderTravellerID,
                            TicketProtection = Convert.ToBoolean(x.TicketProtection.HasValue && x.TicketProtection > 0 ? true : false),
                            PassDesc = getPassDesc(x.PassSaleID, x.ProductType),
                            Price = getPrice(x.PassSaleID, x.ProductType),
                            Traveller = (x.Title != null ? (x.Title + " " + x.FirstName + " " + x.MiddleName + " " + x.LastName) : ""),
                            DateOfJourney = JourneyDate(x.PassSaleID, x.ProductType, x.PassStartDate),
                            TicketProtectionPrice = tkPrice,
                            PassSaleTicketProtectionPrice = x.TicketProtection.HasValue ? x.TicketProtection.Value : 0,
                            OrderIdentity = x.OrderIdentity.HasValue ? x.OrderIdentity.Value : 0
                        }).ToList();
                        lstPassDetail = list.OrderBy(a => a.OrderIdentity).ToList();
                    }
                }
                return lstPassDetail.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetBookingFee_Result> GetBooingFees(decimal amount, Guid siteid)
        {
            try
            {
                List<GetBookingFee_Result> lstBookingFee = new List<GetBookingFee_Result>();
                lstBookingFee = _db.GetBookingFee(amount, siteid).ToList();
                return lstBookingFee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal P2PGetBooingFees(decimal OrderId, String ApiName)
        {
            try
            {
                decimal bookingAmount = 0;
                decimal bookingfee = 0;
                decimal IsPercentage = 0;
                Guid? SiteId = _db.tblOrders.FirstOrDefault(t => t.OrderID == OrderId).SiteID;

                var APIBooking = _db.tblP2PBookingFee.FirstOrDefault(t => t.SiteId == SiteId && t.IsSite == false && t.ApiName == ApiName && t.IsApplicable);
                if (APIBooking != null)
                {
                    IsPercentage = APIBooking.IsPercentage / 100;
                    bookingfee = APIBooking.Fee;
                }
                else
                {
                    var SiteBooking = _db.tblP2PBookingFee.FirstOrDefault(t => t.SiteId == SiteId && t.IsSite && t.IsApplicable);
                    if (SiteBooking != null)
                    {
                        IsPercentage = SiteBooking.IsPercentage / 100;
                        bookingfee = SiteBooking.Fee;
                    }
                }

                var list = _db.tblPassP2PSalelookup.Where(t => t.OrderID == OrderId && t.ProductType.ToUpper() == "P2P").ToList();
                if (list != null && list.Count() > 0)
                    foreach (var data in list)
                    {
                        var p2pdata = _db.tblP2PSale.FirstOrDefault(t => t.ID == data.PassSaleID);
                        if (p2pdata != null)
                        {
                            p2pdata.BookingFee = bookingfee + (p2pdata.NetPrice.Value * IsPercentage);
                            bookingAmount += decimal.Truncate(p2pdata.BookingFee * 100m) / 100m;
                        }
                        _db.SaveChanges();
                    }
                return bookingAmount / (list != null ? list.Count : 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSiteID(Guid siteID, long orderid)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                if (objOrder != null)
                {
                    objOrder.SiteID = siteID;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*GetCommissionFee*/
        public decimal P2PGetCommissionFee(decimal OrderId)
        {
            try
            {
                decimal bookingAmount = 0;
                decimal IsPercentage = 0;
                var SiteId = _db.tblOrders.FirstOrDefault(t => t.OrderID == OrderId).SiteID;
                var amountCommission = _db.tblP2PCommissionFee.FirstOrDefault(t => t.SiteId == SiteId);
                if (amountCommission != null && amountCommission.IsApplicable)
                {
                    IsPercentage = amountCommission.IsPercentage / 100;
                }
                var list = _db.tblPassP2PSalelookup.Where(t => t.OrderID == OrderId && t.ProductType.ToUpper() == "P2P").ToList();
                if (list != null && list.Count() > 0)
                    foreach (var data in list)
                    {
                        var p2pdata = _db.tblP2PSale.FirstOrDefault(t => t.ID == data.PassSaleID);
                        if (p2pdata != null)
                        {
                            p2pdata.CommissionFee = (p2pdata.NetPrice.Value * IsPercentage);
                            bookingAmount += decimal.Truncate(p2pdata.CommissionFee * 100m) / 100m;
                        }
                        _db.SaveChanges();
                    }
                return bookingAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime? JourneyDate(Guid? ProductID, string Type, DateTime? PassStartDate)
        {
            if (Type == "Pass")
            {
                return PassStartDate;
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == ProductID).FirstOrDefault();
                return objP2P.DateTimeDepature;
            }
            return null;
        }

        public tblP2PSale getP2PSingleRowData(Guid ProductID)
        {
            tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == ProductID).FirstOrDefault();
            return objP2P;
        }

        public string getPassDesc(Guid? ProductID, string Type)
        {
            string PassDetail = "";
            if (Type == "Pass")
            {
                var objPS = (from tp in _db.tblPassSales
                             join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                             join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                             join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                             join c in _db.tblClassMsts on tp.ClassID equals c.ID
                             join od in _db.tblOrders on tp.OrderID equals od.OrderID
                             where tp.ID == ProductID
                             orderby t.Name
                             select new { Productname = p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
                if (objPS != null)
                {
                    string country =
                        (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                    PassDetail = objPS.Productname + country + ", " +
                        objPS.PassName + ", " +
                        objPS.PassDesc + ", " +
                        objPS.ClassType;
                }
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.FirstOrDefault(a => a.ID == ProductID);
                if (objP2P != null)
                {
                    string status = "Issue";
                    if (objP2P.Status == true)
                        status = "Confirm";
                    string str = string.Empty;
                    if (!string.IsNullOrEmpty(objP2P.ReservationCode))
                    {
                        str = "<tr><td width='60%'>PNR Number:</td><td width='40%'><b>" + objP2P.ReservationCode + "</b></td></tr>" +
                            "<tr><td width='60%'>Status:</td><td width='40%'>" + status + "</td></tr>";
                    }
                    PassDetail = "Rail Tickets</td></tr><tr><td colspan='3'><table width='100%'>" +
                         "<tr><td width='60%'>Train Number:</td><td width='40%'>" + objP2P.TrainNo.ToString() + "</td></tr>" + str +
                         "<tr><td width='60%'>From:</td><td width='40%'>" + objP2P.From + "</td></tr>" +
                         "<tr><td width='60%'>To:</td><td width='40%'>" + objP2P.To + "</td></tr>" +
                         "<tr><td width='60%'>Departure Date:</td><td width='40%' class='dateofDeparture'>" + Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MM/yyyy") + "</td></tr>" +
                         "<tr><td width='60%'>Departure Time:</td><td width='40%'>" + Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") + "</td></tr>" +
                         "<tr><td width='60%'>Arrival Date:</td><td width='40%'>" + Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MM/yyyy") + "</td></tr>" + "<tr><td width='60%'>Arrival Time:</td><td width='40%'>" + Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm") + "</td></tr>" + "</table>";
                }
            }
            return PassDetail;
        }

        /*PassDescType*/

        public List<GetP2PSaleData> GetP2PSaleDetail(long orderid)
        {

            return _db.GetP2PSaleData(orderid).ToList();
        }

        public List<GetP2PSaleRefundsData> GetP2PSaleRefundsDetail(long orderid)
        {
            return _db.GetP2PSaleRefundsData(orderid).ToList();
        }

        public string getPassDescMail(Guid? ProductID, string Type, List<PrintResponse> PrintUrl, string trainType, string EvolviTandC = "")
        {
            string PassDetail = "";
            if (Type != "P2P")
            {
                var objPS = (from tp in _db.tblPassSales
                             join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                             join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                             join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                             join c in _db.tblClassMsts on tp.ClassID equals c.ID
                             join od in _db.tblOrders on tp.OrderID equals od.OrderID
                             where tp.ID == ProductID
                             orderby t.Name
                             select new { Productname = p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
                if (objPS != null)
                {
                    string country =
                        (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                    PassDetail = objPS.Productname + country + ", " +
                        objPS.PassName + ", " +
                        objPS.PassDesc + ", " +
                        objPS.ClassType;
                }
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.FirstOrDefault(a => a.ID == ProductID);
                if (objP2P != null)
                {
                    Int32 orderid = (Int32)_db.tblPassP2PSalelookup.FirstOrDefault(a => a.PassSaleID == ProductID).OrderID;
                    string status = objP2P.Status == true ? "Confirm" : "Issue";
                    string str = string.Empty; string trainNO = string.Empty;
                    string pnrType = "PNR Number";
                    if (!string.IsNullOrEmpty(objP2P.ReservationCode))
                    {
                        if (!string.IsNullOrEmpty(trainType))
                            if (trainType == "BE")
                                pnrType = "DNR Number";

                        if (!string.IsNullOrEmpty(objP2P.ApiName))
                            if (objP2P.ApiName.Trim().ToLower() == "evolvi")
                                pnrType = "TOD Ref";

                        //var objurl = PrintUrl != null ? (objP2P.PdfURL) : null;
                        if (pnrType == "TOD Ref")
                            str = "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>" + pnrType + " :</b></td><td width='50%'><b>" + objP2P.ReservationCode + "</b></td></tr>" +
                                "<tr><td width='50%'></td><td width='50%'><a target='_blank' href='" + EvolviTandC + "' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif; text-decoration: underline;color: blue;'>How to collect your ticket</a></td></tr>";
                        else
                            str = "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>" + pnrType + " :</b></td><td width='50%'><b>" + objP2P.ReservationCode + "</b></td></tr>";
                        str = str +
                            (!string.IsNullOrEmpty(objP2P.PdfURL) ? "<tr><td colspan='2' width='100%'>" + GetUrls(objP2P.PdfURL, orderid) + "" : "") + "</td></tr>" +
                            "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>Status:</b></td><td width='50%'>" + status + "</td><tr>";
                    }
                    if (objP2P.ApiName != "Evolvi")
                        trainNO = "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>Train Number:</b></td><td width='50%'>" + objP2P.TrainNo.ToString() + "</td><tr>";

                    PassDetail = trainNO + str +
                         "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>From:</b></td><td width='50%'>" + objP2P.From + "</td><tr>" +
                         "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>To:</b></td><td width='50%'>" + objP2P.To + "</td><tr>" +
                         "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>Departure Date:</b></td><td width='50%'>" + Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MM/yyyy") + "</td><tr>" +
                         "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>Departure Time:</b></td><td width='50%'>" + Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") + "</td><tr>" +
                         "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>Arrival Date:</b></td><td width='50%'>" + Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MM/yyyy") + "</td><tr>" +
                         "<tr><td width='50%' style='font-size: 12px;font-family: Arial, Helvetica, sans-serif;color='#000'><b>Arrival Time:</b></td><td width='50%'>" + Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm") + "</td><tr>";
                    //"</table>";
                }
            }
            return PassDetail;
        }

        public string GetElectronicNote(Guid PassSaleId)
        {
            try
            {
                var datapasssale = _db.tblPassSales.FirstOrDefault(x => x.ID == PassSaleId);
                if (datapasssale != null)
                {
                    var data = _db.tblProducts.FirstOrDefault(x => x.ID == datapasssale.ProductID);
                    if (data != null && data.IsElectronicPass)
                        return data.ElectronicPassNote;
                    else
                        return string.Empty;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetPassengerDetailsByOrderId(Int32 orderid)
        {
            try
            {
                return (from tot in _db.tblOrderTravellers
                        join tpotl in _db.tblP2POtherTravellerLookup on tot.ID equals tpotl.OrderTravellerID
                        join tppsl in _db.tblPassP2PSalelookup on tpotl.PassSaleID equals tppsl.PassSaleID
                        where tppsl.OrderID == orderid
                        orderby tot.OrderIdentity
                        select tot.Title + " " + tot.FirstName + " " + tot.LastName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // TrenItalia Regional Urls for each passengers else will work previous
        public string GetUrls(string url, Int32 orderid)
        {
            List<string> passengers = GetPassengerDetailsByOrderId(orderid);
            int count = 0;
            string tablestruc = "<table width='100%' style='font-size: 12px; font-family: Arial, Helvetica, sans-serif;'>";
            tablestruc += "<tr><td style='color:#000; padding-right: 5px; font-weight: bold' >Print Ticket:</td>";
            if (!string.IsNullOrEmpty(url))
            {
                string[] urls = url.Split('#');
                foreach (var item in urls)
                {
                    if (item.Length > 32)
                    {
                        if (count == 0)
                            tablestruc += "<td style='text-align:right'>" + ((passengers != null && passengers.Count > 0) ? ((count >= passengers.Count) ? "" : passengers[count]) : "") + " </td><td>" + "<b> <a href='" + item + "'>Click Here</a></b></td></tr>";
                        else
                            tablestruc += "<tr><td></td><td style='text-align:right'>" + ((passengers != null && passengers.Count > 0) ? ((count >= passengers.Count) ? "" : passengers[count]) : "") + " </td><td>" + "<b> <a href='" + item + "'>Click Here</a></b></td></tr>";
                        count++;
                    }
                }
            }
            tablestruc += "</table>";
            return string.IsNullOrEmpty(tablestruc) ? url : tablestruc;
        }

        public bool GetP2PHavePDF(Guid? ProductID, List<PrintResponse> PrintUrl)
        {
            try
            {
                tblP2PSale objP2P = _db.tblP2PSale.FirstOrDefault(a => a.ID == ProductID);
                if (objP2P != null && PrintUrl != null)
                    return PrintUrl.Any(x => x.ReservationCode == objP2P.ReservationCode);
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getPassDescForQbit(Guid? ProductID, string Type, string RType)
        {
            string PassDetail = "";
            if (Type == "Pass")
            {

                var objPS = (from tp in _db.tblPassSales
                             join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                             join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                             join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                             join c in _db.tblClassMsts on tp.ClassID equals c.ID
                             join od in _db.tblOrders on tp.OrderID equals od.OrderID
                             where tp.ID == ProductID
                             orderby t.Name
                             select new { Productname = p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
                if (objPS != null)
                {
                    string country =
                        (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                    if (RType == "Category")
                    {
                        PassDetail = objPS.Productname;
                    }
                    else
                    {
                        PassDetail = objPS.Productname + country + ", " +
                            objPS.PassName + ", " +
                            objPS.PassDesc + ", " +
                            objPS.ClassType;
                    }
                }
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == ProductID).FirstOrDefault();
                if (RType == "Category")
                {
                    PassDetail = "P2P";
                }
                else
                {
                    PassDetail = "Train Number:" + objP2P.TrainNo.ToString() +
                         ", From:" + objP2P.From +
                         ", To:" + objP2P.To +
                         ", Departure Date:" + Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MMM/yyyy") +
                         ", Departure Time:" + Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") +
                         ", Arrival Date:" + Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MMM/yyyy") +
                         ", Arrival Time:" + Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm");
                }
            }
            return PassDetail;
        }

        public void UpdateTicketProtection(Guid P2PID, Decimal Protection)
        {
            try
            {
                tblP2PSale obj = _db.tblP2PSale.Where(a => a.ID == P2PID).FirstOrDefault();
                if (obj != null)
                {
                    obj.TicketProtection = Protection;
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckDilveryOptionType(Int64 orderId)
        {
            tblP2PSale oSale = _db.tblP2PSale.FirstOrDefault(x => x.ID == _db.tblPassP2PSalelookup.FirstOrDefault(y => y.OrderID == orderId).PassSaleID);
            return oSale != null ? oSale.DeliveryOption.Contains("TrenItaila") || oSale.DeliveryOption.Contains("Home") : false;
        }

        public string getDeliveryOption(Int64 orderId)
        {
            tblP2PSale oSale = _db.tblP2PSale.FirstOrDefault(x => x.ID == _db.tblPassP2PSalelookup.FirstOrDefault(y => y.OrderID == orderId).PassSaleID);
            return oSale.DeliveryOption;
        }

        public bool UpdateReservationCode(Int64 orderId, string ReservationCode, bool status)
        {
            try
            {
                if (!string.IsNullOrEmpty(ReservationCode))
                {
                    var obj = _db.tblP2PSale.Where(x => x.ReservationCode == ReservationCode).ToList();
                    if (obj != null && obj.Count > 0)
                    {
                        obj.ForEach(x => x.Status = status);
                        _db.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdatePinNumberByReservationCode(string ReservationCode, string pinnumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(ReservationCode))
                {
                    var obj = _db.tblP2PSale.Where(x => x.ReservationCode == ReservationCode).ToList();
                    if (obj != null && obj.Count > 0)
                    {
                        obj.ForEach(x => x.PinNumber = pinnumber);
                        _db.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateTicketUrlByReservationCode(string ReservationCode, string url)
        {
            try
            {
                if (!string.IsNullOrEmpty(ReservationCode))
                {
                    var obj = _db.tblP2PSale.Where(x => x.ReservationCode == ReservationCode).ToList();
                    if (obj != null && obj.Count > 0)
                    {
                        obj.ForEach(x => x.PdfURL = url);
                        _db.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateReservationCodebyP2PID(Guid P2PID, Int64 orderId, string ReservationCode, string pinnumber, string delivery, bool status, string DepStNm, bool isRegional)
        {
            try
            {
                tblP2PSale obj = null;
                if (ReservationCode.Length == 6)
                    obj = _db.tblP2PSale.Where(a => a.ID == P2PID && a.From == DepStNm).FirstOrDefault();
                else
                    obj = _db.tblP2PSale.Where(a => a.ID == P2PID).FirstOrDefault();

                if (isRegional == false)
                {
                    if (obj != null)
                    {
                        obj.ReservationCode = ReservationCode;
                        obj.PinNumber = pinnumber;
                        obj.DeliveryOption = delivery;
                        obj.Status = status;
                        _db.SaveChanges();
                    }
                }
                else
                {
                    var passSaleID = (from TPSL in _db.tblPassP2PSalelookup
                                      join TPS in _db.tblP2PSale on TPSL.PassSaleID equals TPS.ID
                                      join TO in _db.tblOrders on TPSL.OrderID equals TO.OrderID
                                      where TO.OrderID == orderId
                                      select TPS.ID).ToList();
                    if (passSaleID != null && passSaleID.Count > 0)
                    {
                        foreach (var item in passSaleID)
                        {
                            var newObj = _db.tblP2PSale.Where(a => a.ID == item).FirstOrDefault();
                            if (newObj != null)
                            {
                                newObj.ReservationCode = ReservationCode;
                                newObj.PinNumber = pinnumber;
                                newObj.DeliveryOption = delivery;
                                newObj.Status = status;
                                _db.SaveChanges();
                            }
                        }
                    }

                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
        public bool UpdateReservationCodebyP2PID(Guid P2PID, string ReservationCode, string pinnumber, string delivery, bool status)
        {
            try
            {
                tblP2PSale obj = _db.tblP2PSale.Where(a => a.ID == P2PID).FirstOrDefault();
                if (obj != null)
                {
                    obj.ReservationCode = ReservationCode;
                    obj.PinNumber = pinnumber;
                    obj.DeliveryOption = delivery;
                    obj.Status = status;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public decimal getPrice(Guid? ProductID, string Type)
        {
            decimal i = 0;
            if (Type == "Pass")
            {
                tblPassSale objPS = _db.tblPassSales.Where(a => a.ID == ProductID).FirstOrDefault();
                if (objPS != null)
                    i = Convert.ToDecimal(objPS.Price);
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == ProductID).FirstOrDefault();
                if (objP2P != null)
                    i = Convert.ToDecimal(objP2P.NetPrice);

            }
            return i;
        }

        public void AddOrderBillingAddress(tblOrderBillingAddress obj)
        {
            try
            {
                tblOrderBillingAddress objBillingAddress = _db.tblOrderBillingAddresses.Where(a => a.OrderID == obj.OrderID).FirstOrDefault();
                if (objBillingAddress != null)
                {
                    objBillingAddress.Title = obj.Title;
                    objBillingAddress.FirstName = obj.FirstName;
                    objBillingAddress.LastName = obj.LastName;
                    objBillingAddress.Address1 = obj.Address1;
                    objBillingAddress.Address2 = obj.Address2;
                    objBillingAddress.EmailAddress = obj.EmailAddress;
                    objBillingAddress.Phone = obj.Phone;
                    objBillingAddress.City = obj.City;
                    objBillingAddress.State = obj.State;
                    objBillingAddress.Country = obj.Country;
                    objBillingAddress.Postcode = obj.Postcode;
                    objBillingAddress.IsVisibleEmailAddress = obj.IsVisibleEmailAddress;

                    objBillingAddress.TitleShpg = obj.TitleShpg;
                    objBillingAddress.FirstNameShpg = obj.FirstNameShpg;
                    objBillingAddress.LastNameShpg = obj.LastNameShpg;
                    objBillingAddress.Address1Shpg = obj.Address1Shpg;
                    objBillingAddress.Address2Shpg = obj.Address2Shpg;
                    objBillingAddress.EmailAddressShpg = obj.EmailAddressShpg;
                    objBillingAddress.PhoneShpg = obj.PhoneShpg;
                    objBillingAddress.CityShpg = obj.CityShpg;
                    objBillingAddress.StateShpg = obj.StateShpg;
                    objBillingAddress.CountryShpg = obj.CountryShpg;
                    objBillingAddress.PostcodeShpg = obj.PostcodeShpg;
                    _db.SaveChanges();
                }
                else
                {
                    _db.tblOrderBillingAddresses.AddObject(obj);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateDepatureDate(long orderid, DateTime DoD)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                if (objOrder != null)
                {
                    objOrder.DateOfDepart = DoD;
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrderBookingFee(decimal bookingAmount, long orderid)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                objOrder.BookingFee = bookingAmount;
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatetblP22BookingFee(decimal bookingAmount, long orderid)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == orderid).FirstOrDefault();
                List<Guid?> passSaleID = _db.tblPassP2PSalelookup.Where(a => a.OrderID == orderid).Select(x => x.PassSaleID).ToList();
                if (passSaleID != null)
                {
                    objOrder.BookingFee = bookingAmount;
                    foreach (Guid id in passSaleID)
                    {
                        tblP2PSale p2pSale = _db.tblP2PSale.FirstOrDefault(x => x.ID == id);
                        p2pSale.BookingFee = bookingAmount;
                        _db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AgentDetails> GetListUpdateOrderStatusLog(long orderid)
        {
            try
            {
                return _db.tblOrderStatusUpdateLogs.Where(x => x.Orderid == orderid).AsEnumerable().Select(x => new AgentDetails
                 {
                     AgentName = x.tblAdminUser.Forename + " " + x.tblAdminUser.Surname,
                     Date = x.CreatedOn.ToString("dd/MMM/yyyy"),
                     Status = x.tblOrderStatu.Name,
                     Time = x.CreatedOn.ToString("HH:mm:ss")
                 }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrderStatusLog(int Status, long orderid)
        {
            if (orderid > 0)
            {
                Guid UserId = AdminuserInfo.UserID;
                if (AgentuserInfo.UserID != Guid.Empty)
                    UserId = AgentuserInfo.UserID;
                if (UserId == Guid.Empty)
                    UserId = Guid.Parse("98D39398-D622-4FD0-80A1-B725FC05BC0F");
                try
                {
                    var obj = new tblOrderStatusUpdateLog();
                    obj.Orderid = orderid;
                    obj.AgentId = UserId;
                    obj.StatusId = Status;
                    obj.CreatedOn = DateTime.Now;
                    _db.tblOrderStatusUpdateLogs.AddObject(obj);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void UpdateOrderStatus(int Status, long orderid)
        {
            if (orderid > 0)
            {
                try
                {
                    //3:Approved; 5:On Hold (After Approved); 7:Completed; 9:Refund Complete;
                    UpdateOrderStatusLog(Status, orderid);
                    tblOrder objOrder = _db.tblOrders.FirstOrDefault(a => a.OrderID == orderid);
                    if (objOrder != null)
                    {

                        if (Status == 3 || Status == 5 || Status == 7 || Status == 9)
                            objOrder.PaymentDate = objOrder.PaymentDate.HasValue ? objOrder.PaymentDate.Value : DateTime.Now;
                        objOrder.ModifyedBy = AdminuserInfo.UserID;
                        objOrder.ModifyedOn = DateTime.Now;
                        objOrder.Status = Status;
                        _db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void UpdateAgentReferenceNumber(string agentRef, long orderid)
        {
            try
            {
                var objOrder = _db.tblOrders.FirstOrDefault(a => a.OrderID == orderid);
                if (objOrder != null)
                {
                    if (agentRef != null)
                    {
                        objOrder.AgentReferenceNo = agentRef;
                        _db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<spGetP2PRefund> GetP2PRefund(long orderid)
        {
            return _db.spGetP2PRefund(orderid).ToList();
        }

        public void UpdateBillingAddress(long OrderId, tblOrderBillingAddress Obj)
        {
            try
            {
                var gObj = _db.tblOrderBillingAddresses.FirstOrDefault(ty => ty.OrderID == OrderId);
                if (gObj != null)
                {
                    gObj.Address1 = Obj.Address1;
                    gObj.Address2 = Obj.Address2;
                    gObj.City = Obj.City;
                    gObj.EmailAddress = Obj.EmailAddress;
                    gObj.FirstName = Obj.FirstName;
                    gObj.LastName = Obj.LastName;
                    gObj.State = Obj.State;
                    gObj.Postcode = Obj.Postcode;
                    gObj.Title = Obj.Title;
                    gObj.Country = Obj.Country;
                    gObj.Phone = Obj.Phone;

                }
                else
                {
                    Obj.ID = Guid.NewGuid();
                    Obj.OrderID = OrderId;
                    _db.AddTotblOrderBillingAddresses(Obj);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBillingEmailAddressVisible(long OrderId, bool IsVisibleEmailAddress)
        {
            try
            {
                var gObj = _db.tblOrderBillingAddresses.FirstOrDefault(ty => ty.OrderID == OrderId);
                if (gObj != null)
                    gObj.IsVisibleEmailAddress = IsVisibleEmailAddress;
                _db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }

        public bool GetP2PSaleDataById(tblP2PSale objdata)
        {
            var data = _db.tblP2PSale.FirstOrDefault(t => t.ID == objdata.ID);
            if (data != null)
            {
                data.From = objdata.From;
                data.To = objdata.To;
                data.DateTimeDepature = objdata.DateTimeDepature;
                data.DepartureTime = objdata.DepartureTime;
                data.DateTimeArrival = objdata.DateTimeArrival;
                data.ArrivalTime = objdata.ArrivalTime;
                data.TrainNo = objdata.TrainNo;
                data.Senior = objdata.Senior;
                data.Adult = objdata.Adult;
                data.Children = objdata.Children;
                data.Youth = objdata.Youth;
                data.Passenger = objdata.Passenger;
                data.P2PType = objdata.P2PType;
                data.Class = objdata.Class;
                _db.SaveChanges();
            }
            return true;
        }

        public void UpdateP2PSaleDataById(tblP2PSale obj, Int32 OrderID)
        {
            var data = _db.tblP2PSale.FirstOrDefault(t => t.ID == obj.ID);
            if (data != null)
            {
                data.TrainNo = obj.TrainNo;
                data.From = obj.From;
                data.To = obj.To;
                data.DateTimeDepature = obj.DateTimeDepature;
                data.DepartureTime = obj.DepartureTime;
                data.DateTimeArrival = obj.DateTimeArrival;
                data.ArrivalTime = obj.ArrivalTime;
                data.Adult = obj.Adult;
                data.Children = obj.Children;
                data.Senior = obj.Senior;
                data.Youth = obj.Youth;
                data.Class = obj.Class;
                data.NetPrice = obj.NetPrice;
                data.FareName = obj.FareName;
                data.DeliveryOption = obj.DeliveryOption;
                data.Terms = obj.Terms;
                data.Passenger = obj.Passenger;
                data.TicketProtection = obj.TicketProtection;
                data.BookingFee = obj.BookingFee;
                data.CommissionFee = obj.CommissionFee;
                data.FIPClass = obj.FIPClass;
                data.FIPNumber = obj.FIPNumber;
                data.ApiName = obj.ApiName;
                data.Via = obj.Via;
                _db.SaveChanges();
            }
            else
            {
                _db.tblP2PSale.AddObject(obj);
                var Result = _db.tblPassP2PSalelookup.FirstOrDefault(t => t.OrderID == OrderID && t.PassSaleID == Guid.Empty);
                if (Result != null)
                {
                    Result.PassSaleID = obj.ID;
                }
                else
                {
                    var AData = _db.tblPassP2PSalelookup.FirstOrDefault(t => t.OrderID == OrderID);
                    tblPassP2PSalelookup objdata = new tblPassP2PSalelookup();
                    objdata.ID = Guid.NewGuid();
                    objdata.OrderID = OrderID;
                    objdata.PassSaleID = obj.ID;
                    objdata.ProductType = "P2P";
                    objdata.OrderTravellerID = AData.OrderTravellerID;
                    _db.tblPassP2PSalelookup.AddObject(objdata);
                }
                _db.SaveChanges();
            }
        }

        public void DeleteP2PSaleDataById(Guid ID)
        {
            var Data = _db.tblPassP2PSalelookup.FirstOrDefault(t => t.PassSaleID == ID);
            if (Data != null)
                Data.PassSaleID = Guid.Empty;
            _db.tblP2PSale.Where(t => t.ID == ID).ToList().ForEach(_db.tblP2PSale.DeleteObject);
            _db.SaveChanges();
        }

        public void UpdateP2PSaleDataById(tblP2PSale obj)
        {
            var data = _db.tblP2PSale.FirstOrDefault(t => t.ID == obj.ID);
            if (data != null)
            {
                data.TrainNo = obj.TrainNo;
                data.From = obj.From;
                data.To = obj.To;
                data.DateTimeDepature = obj.DateTimeDepature;
                data.DepartureTime = obj.DepartureTime;
                data.DateTimeArrival = obj.DateTimeArrival;
                data.ArrivalTime = obj.ArrivalTime;
                data.Adult = obj.Adult;
                data.Children = obj.Children;
                data.Senior = obj.Senior;
                data.Youth = obj.Youth;
                data.Class = obj.Class;
                data.NetPrice = obj.NetPrice;
                data.FareName = obj.FareName;
                data.DeliveryOption = obj.DeliveryOption;
                data.Terms = obj.Terms;
                data.Passenger = obj.Passenger;
            }
            _db.SaveChanges();
        }

        public tblP2PSale GetP2PSaleDataById(Guid ID)
        {
            return _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
        }

        public void DeleteDiscountByOrderId(int orderid)
        {
            try
            {
                _db.tblOrderDiscounts.Where(t => t.OrderId == orderid).ToList().ForEach(_db.tblOrderDiscounts.DeleteObject);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidDiscountCode(string discountCode, Guid siteid, int OrderId)
        {
            try
            {
                bool Result = false;
                var data = _db.tblPassSales.Where(t => t.OrderID == OrderId).ToList();
                List<Guid> DataPassProduct = data.Select(t => t.ProductID.Value).ToList();
                List<Guid> DataPassCategory = data.Select(t => t.CategoryID.Value).ToList();
                DateTime CurrentDate = DateTime.Now;
                DateTime ToDate = DateTime.Now.AddDays(1);
                Result = _db.tblDiscountProducts.Any(t => DataPassProduct.Contains(t.ProductId) && discountCode == t.DiscountCode && siteid == t.SiteId &&
                    ((t.discountFromDate.HasValue ? t.discountFromDate.Value : CurrentDate) <= CurrentDate && (t.discountToDate.HasValue ? t.discountToDate.Value : ToDate) >= CurrentDate));
                if (!Result)
                    Result = _db.tblDiscountCategories.Any(t => DataPassCategory.Contains(t.CategoryId) && discountCode == t.DiscountCode && siteid == t.SiteId && ((t.discountFromDate.HasValue ? t.discountFromDate.Value : CurrentDate) <= CurrentDate && (t.discountToDate.HasValue ? t.discountToDate.Value : ToDate) >= CurrentDate));
                if (!Result)
                    Result = (from tpds in _db.tblProductDiscountSiteLookups
                              join tpd in _db.tblProductDiscounts on tpds.ProductDiscountID equals tpd.ID
                              where tpds.SiteID == siteid && tpd.IsActive && tpd.Code == discountCode &&
                              (tpd.discountFromDate.HasValue ? tpd.discountFromDate.Value : CurrentDate) <= CurrentDate && (tpd.discountToDate.HasValue ? tpd.discountToDate.Value : ToDate) >= CurrentDate
                              select new { tpd.Code }).ToList().Any();

                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOrderDiscountType(string discountCode, Guid siteid)
        {
            try
            {
                var data = (from tpds in _db.tblProductDiscountSiteLookups
                            join tpd in _db.tblProductDiscounts on tpds.ProductDiscountID equals tpd.ID
                            where tpds.SiteID == siteid && tpd.IsActive && tpd.Code == discountCode
                            select new { tpd.IsPercentage, tpd.Amount }).ToList().FirstOrDefault();
                if (data != null)
                {
                    if (data.IsPercentage)
                        return (data.Amount ?? 0) + "%";
                    else
                        return (data.Amount ?? 0).ToString();
                }
                return "0.00";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddOrderDiscountData(string discountCode, Guid siteid, long orderid, decimal TotalAmount)
        {
            try
            {
                string Result = "0.00";
                bool IsPercent = false;
                decimal DiscountAmount = 0;
                List<Guid> DataPassProduct = _db.tblPassSales.Where(t => t.OrderID == orderid).Select(t => t.ProductID.Value).ToList();
                List<Guid> DataPassCategory = _db.tblPassSales.Where(t => t.OrderID == orderid).Select(t => t.CategoryID.Value).ToList();

                var ProductDiscount = _db.tblDiscountProducts.FirstOrDefault(t => DataPassProduct.Contains(t.ProductId) && discountCode == t.DiscountCode && siteid == t.SiteId);
                if (ProductDiscount == null)
                {
                    var CategoryDiscount = _db.tblDiscountCategories.FirstOrDefault(t => DataPassCategory.Contains(t.CategoryId) && discountCode == t.DiscountCode && siteid == t.SiteId);

                    if (CategoryDiscount == null)
                    {
                        var data = (from tpds in _db.tblProductDiscountSiteLookups
                                    join tpd in _db.tblProductDiscounts on tpds.ProductDiscountID equals tpd.ID
                                    where tpds.SiteID == siteid && tpd.IsActive && tpd.Code == discountCode
                                    select new { IsPercent = tpd.IsPercentage, DiscountPrice = tpd.Amount.Value }).FirstOrDefault();
                        if (data != null)
                        {
                            DiscountAmount = data.DiscountPrice;
                            IsPercent = data.IsPercent;
                        }
                    }
                    else
                    {
                        DiscountAmount = CategoryDiscount.DiscountPrice;
                        IsPercent = CategoryDiscount.IsPercent;
                    }
                }
                else
                {
                    DiscountAmount = ProductDiscount.DiscountPrice;
                    IsPercent = ProductDiscount.IsPercent;
                }
                if (DiscountAmount > 0)
                {
                    var objDis = new tblOrderDiscount();
                    objDis.Id = Guid.NewGuid();
                    objDis.OrderId = orderid;
                    objDis.DiscountCode = discountCode;
                    objDis.OrderTotalAmount = TotalAmount;
                    objDis.DiscountAmount = DiscountAmount;
                    objDis.IsPercentage = IsPercent;
                    if (IsPercent)
                    {
                        TotalAmount = ((objDis.DiscountAmount * TotalAmount) / 100);
                        Result = objDis.DiscountAmount + "%";
                    }
                    else
                    {
                        TotalAmount = TotalAmount - objDis.DiscountAmount;
                        Result = objDis.DiscountAmount.ToString();
                    }
                    objDis.OrderDiscountAmount = TotalAmount;
                    var firstdefault = _db.tblOrderDiscounts.FirstOrDefault(t => t.OrderId == orderid);
                    if (firstdefault != null)
                    {
                        firstdefault.IsPercentage = objDis.IsPercentage;
                        firstdefault.OrderDiscountAmount = objDis.OrderDiscountAmount;
                        firstdefault.OrderTotalAmount = objDis.OrderTotalAmount;
                        firstdefault.DiscountCode = objDis.DiscountCode;
                        firstdefault.DiscountAmount = objDis.DiscountAmount;
                    }
                    else
                        _db.tblOrderDiscounts.AddObject(objDis);
                    _db.SaveChanges();
                }
                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrderAmexPrice(long OrderNo, decimal AmexPrice)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == OrderNo).FirstOrDefault();
                if (objOrder != null)
                {
                    objOrder.AmexPrice = AmexPrice;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Shipping Detail Admin
        public List<shippingData> getAllShippingDetailAdmin(Guid siteId, Guid countryId)
        {
            try
            {
                var list = (from ts in _db.tblShippings
                            join tsLook in _db.tblShippingOptionCountries
                                on ts.ID equals tsLook.ShippingID
                            where ts.SiteID == siteId && ts.IsActive && tsLook.CountryID == countryId
                            select new { ts }).ToList();

                return list.Select(ty => new shippingData
                {
                    ID = ty.ts.ID,
                    ShippingName = ty.ts.ShippingName,
                    Description = ty.ts.Description,
                    Price = ty.ts.Price
                }).OrderBy(x => x.Price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblShipping> getDefaultShippingDetailAdmin(Guid siteId)
        {
            try
            {
                return _db.tblShippings.Where(ty => ty.SiteID == siteId && ty.IsActive && ty.DefaultWhenMissing).OrderBy(ty => ty.Price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public decimal GetCommitionP2PManualBooking(Guid ID, decimal Price)
        {
            try
            {
                var data = _db.tblManualP2PCommition.FirstOrDefault(t => t.Id == ID);
                if (data != null)
                    return data.Ispercentage ? ((Price / 100) * data.Commition) : data.Commition;
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateShippingMethodForManualBooking(string shipMethod, string shipDesp, decimal ShipAmount, long orderid)
        {
            try
            {
                var objOrder = _db.tblOrders.FirstOrDefault(a => a.OrderID == orderid);
                if (objOrder != null)
                {
                    objOrder.ShippingMethod = shipMethod;
                    objOrder.ShippingDescription = shipMethod;
                    objOrder.ShippingAmount = ShipAmount;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AllPassPrintedForThisOrder(long orderid)
        {
            try
            {
                Guid Queued = Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b");
                Guid Assigned = Guid.Parse("405AB5E3-51C6-49E8-B605-1655EBC74AAD");
                List<Guid?> listID = new List<Guid?> { Queued, Assigned };
                return !_db.tblPrintQueueItems.Any(a => a.OrderID == orderid && listID.Contains(a.Status));
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int GetItemOrderCount(long OrderId)
        {
            try
            {
                int count = _db.tblPassP2PSalelookup.Where(a => a.OrderID == OrderId).Count();
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean IsErurailPass(Guid? CategoryID)
        {
            try
            {
                var count = _db.EurailCount(CategoryID).Single();
                if (Convert.ToInt32(count) > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<spGetOrderReciptData_Result> GetOrderReciptData(long OrderID)
        {
            try
            {
                List<spGetOrderReciptData_Result> lst = _db.GetOrderReciptData(OrderID).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean UpdateOrderPaymentDetail(long OrderNo, string CN, string CARDNO, string PID, string ExpDate, string Brand)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.Where(a => a.OrderID == OrderNo).FirstOrDefault();
                objOrder.CardholderName = CN;
                objOrder.CardNumber = CARDNO;
                objOrder.PaymentId = PID;
                objOrder.Brand = Brand;
                objOrder.ExpDate = ExpDate;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Shipping Detail
        public List<shippingData> getAllShippingDetail(Guid siteId, Guid countryId)
        {
            try
            {
                var list = (from ts in _db.tblShippings
                            join tsLook in _db.tblShippingOptionCountries
                                on ts.ID equals tsLook.ShippingID
                            where ts.SiteID == siteId && ts.IsActive && tsLook.CountryID == countryId
                            && ts.IsVisibleFront
                            select new { ts }).ToList();

                return list.Select(ty => new shippingData
                {
                    ID = ty.ts.ID,
                    ShippingName = ty.ts.ShippingName,
                    Description = ty.ts.Description,
                    Price = ty.ts.Price
                }).OrderBy(x => x.Price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<shippingData> getAllPassShippingDetail(Guid siteId, Guid countryId)
        {
            try
            {
                var list = (from ts in _db.tblShippings
                            join tsLook in _db.tblPassShippingOptionCountries
                                on ts.ID equals tsLook.ShippingID
                            where ts.SiteID == siteId && ts.IsActive == true && tsLook.CountryID == countryId
                            && ts.IsVisibleFront == true
                            select new { ts }).ToList();

                return list.Select(ty => new shippingData
                {
                    ID = ty.ts.ID,
                    ShippingName = ty.ts.ShippingName,
                    Description = ty.ts.Description,
                    Price = ty.ts.Price,
                    DefaultWhenMissing = ty.ts.DefaultWhenMissing
                }).OrderByDescending(x => x.DefaultWhenMissing).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblShipping> getDefaultShippingDetail(Guid siteId)
        {
            try
            {
                return _db.tblShippings.Where(ty => ty.SiteID == siteId && ty.IsActive == true && ty.DefaultWhenMissing == true && ty.IsVisibleFront == true).OrderBy(ty => ty.Price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Login User
        public bool AddLoginUSer(tblUserLogin saveuser)
        {
            try
            {
                var EmailChk = _db.tblUserLogins.FirstOrDefault(ty => ty.Email == saveuser.Email);

                if (EmailChk != null)
                {
                    return true;
                }
                else
                {
                    _db.AddTotblUserLogins(saveuser);
                    _db.SaveChanges();
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get classid and validityid
        public sp_GetPassSaleDetail_Result GetClassAndValidityID(string price, string validityname, string classname, string TravellerName, Guid ProductID, Guid TravellerID, Guid SiteID)
        {
            try
            {
                var list = _db.GetPassSaleDetail(price, validityname, classname, TravellerName, ProductID, TravellerID, SiteID, Guid.NewGuid()).ToList();
                return list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public sp_GetPassSaleDetail_Result GetClassAndValidityID(string price, string validityname, string classname, string TravellerName, Guid ProductID, Guid TravellerID, Guid SiteID, Guid AgentID)
        {
            try
            {
                var list = _db.GetPassSaleDetail(price, validityname, classname, TravellerName, ProductID, TravellerID, SiteID, AgentID).ToList();
                return list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Data Dump Operation
        public List<GetDataDump> GetDataDump(Guid siteids, DateTime startdate, DateTime enddate, string OfficeId, int Status, Boolean IsSta)
        {
            try
            {
                return _db.GetDataDump(siteids, startdate, enddate, OfficeId, Status, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Data Dump Operation
        public List<NewGetDatadump_Result> NewGetDataDump(Guid siteids, DateTime startdate, DateTime enddate, string OfficeId, int Status, Boolean IsSta)
        {
            try
            {
                return _db.NewGetDatadump(siteids, startdate, enddate, OfficeId, Status, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Data Dump Operation
        public List<spPartnersReportEuRail_Result> GetPartnersReportEuRail()
        {
            try
            {
                return _db.PartnersReportEuRail().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region EurailStockReport
        public List<EurailStockReport_Result> GetEurailStockReport(Guid SiteId, DateTime startdate, DateTime enddate, string OfficeId, Boolean IsSta)
        {
            try
            {
                return _db.EurailStockReport(SiteId, OfficeId, startdate, enddate, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region EurailStockReport
        public List<EurailStock> GetEurailStockRpt(Guid siteId)
        {
            try
            {
                var olist = (from tsau in _db.tblStockAllocationUsages
                             join tsq in _db.tblStockQueues on tsau.StockAllocationID equals tsq.ID
                             join tpq in _db.tblPrintQueueItems on tsau.PrintQueueItemID equals tpq.ID
                             join tpqs in _db.tblPrintQueueStatus on tpq.Status equals tpqs.ID
                             join tps in _db.tblPassSales on tpq.PassSaleID equals tps.ID
                             join ttm in _db.tblTravelerMsts on tps.TravellerID equals ttm.ID
                             join tppsl in _db.tblPassP2PSalelookup on tps.ID equals tppsl.PassSaleID
                             join tot in _db.tblOrderTravellers on tppsl.OrderTravellerID equals tot.ID
                             join tpn in _db.tblProductNames on tps.ProductID equals tpn.ProductID
                             join todr in _db.tblOrders on tps.OrderID equals todr.OrderID
                             join ts in _db.tblSites on todr.SiteID equals ts.ID
                             join tcurrm in _db.tblCurrencyMsts on ts.DefaultCurrencyID equals tcurrm.ID
                             where tpq.SiteID == siteId
                             select new { tsau, tsq, tpq, tot, tpn, tps, tpqs, tcurrm, ts, todr, ttm }).ToList();

                var list = olist.Select(x => new EurailStock
                {
                    StockNo = Convert.ToInt64(x.tsau.StockNumber),
                    Location = GetOfficeName(x.tsq.BranchID) + " (" + x.tsq.PrinterCode + ")",
                    Date = x.tsau.CreatedOn,
                    OrderNo = Convert.ToInt64(x.tpq.OrderID),
                    Name = x.tot.Title + ' ' + x.tot.FirstName + ' ' + x.tot.LastName,
                    Product = (x.ttm.Name.ToLower().Contains("saver")) ? "Control Voucher" : x.tpn.Name,
                    //SalePrice = (x.tps.Price.HasValue ? x.tps.Price.Value : 0) + (x.tps.TicketProtection.HasValue ? x.tps.TicketProtection.Value : 0),
                    SalePrice = (x.tps.Price.HasValue ? x.tps.Price.Value : 0),
                    SaleCurrency = x.tcurrm.ShortCode,
                    //NetEURPrice = getEURCurrPrice((x.tps.Price.HasValue ? x.tps.Price.Value : 0) + (x.tps.TicketProtection.HasValue ? x.tps.TicketProtection.Value : 0), x.todr.SiteID, x.ts.DefaultCurrencyID),
                    NetEURPrice = getEURCurrPrice((x.tps.Price.HasValue ? x.tps.Price.Value : 0), x.todr.SiteID, x.ts.DefaultCurrencyID),
                    //SaleEURPrice = (x.tps.Price.HasValue ? x.tps.Price.Value : 0) + (x.tps.TicketProtection.HasValue ? x.tps.TicketProtection.Value : 0),
                    SaleEURPrice = getEURCurrPrice((x.tps.Price.HasValue ? x.tps.Price.Value : 0), x.todr.SiteID, x.ts.DefaultCurrencyID) + (x.tps.MarkUp.HasValue ? x.tps.MarkUp.Value : 0),
                    Status = (x.tpqs.Name.ToLower() == "distroy") ? "VOID" : "SALE"
                }).OrderBy(ty => ty.StockNo).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public List<GetStockAllocationEurail> GetStockAllocationEurail(Guid siteId)
        //{
        //    try
        //    {
        //        var list = _db.GetStockAllocationEurail(siteId).Where(a => a.SiteID == siteId);
        //        return list.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public decimal getEURCurrPrice(decimal Price, Guid? SiteID, Guid? CurrID)
        {
            try
            {
                var EURdata = new ManageOrder().getEURCurrencyAndSiteid();
                Guid OrderCurrencyID = CurrID.HasValue ? CurrID.Value : Guid.Empty;
                Guid OrderSiteID = SiteID.HasValue ? SiteID.Value : Guid.Empty;
                return FrontEndManagePass.GetPriceAfterConversion(Price, OrderSiteID, EURdata.ID, OrderCurrencyID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOfficeName(Guid id)
        {
            try
            {
                string name = string.Empty;
                var list = (from stk in _db.tblBranches
                            where stk.ID == id
                            select new
                            {
                                parentID = stk.ParentBranchID,
                                name = stk.OfficeName,
                            }).ToList();

                foreach (var item in list)
                {
                    name = GetOfficeName((Guid)item.parentID) + " - " + item.name;
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region TicketProtection

        public tblTicketProtection bindTicketProtection(Guid SiteID)
        {
            try
            {
                return _db.tblTicketProtections.SingleOrDefault(ty => ty.SiteID == SiteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddTicketProtection(tblTicketProtection data)
        {
            try
            {
                var list = _db.tblTicketProtections.SingleOrDefault(ty => ty.SiteID == data.SiteID);
                if (list != null)
                {
                    list.CurrencyID = data.CurrencyID;
                    list.Amount = data.Amount;
                    list.Description = data.Description;
                    list.IsActive = data.IsActive;
                    list.ModifiedOn = data.CreatedOn;
                    list.ModifiyedBy = data.CreatedBy;
                }
                else
                {
                    _db.AddTotblTicketProtections(data);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Station List
        public List<getStationList_Result> GetAllStaionList()
        {
            try
            {
                return _db.getStationList().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public GetOldStationName_Result GetOldStationNameFromNew(string stationName)
        {
            try
            {
                GetOldStationName_Result obj = new GetOldStationName_Result();
                obj = _db.GetOldStationName(stationName).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GetStationDetailsByStationName GetStationDetailsByStationName(string stationName)
        {
            try
            {
                GetOldStationName_Result obj = new GetOldStationName_Result();
                return _db.GetStationDetailsByStationName(stationName).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public GetNewStaionName_Result GetNewStationNameFromOld(string stationName)
        {
            try
            {
                GetNewStaionName_Result obj = new GetNewStaionName_Result();
                obj = _db.GetNewStaionName(stationName).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GetAllGroupStationName_Result> GetAllGroupStationName(string stationName)
        {
            try
            {
                List<GetAllGroupStationName_Result> lst = new List<GetAllGroupStationName_Result>();
                lst = _db.GetAllGroupStationName(stationName).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region StockAssignedTo
        public List<StockAssignedToReport_Result> GetStockAssignedTo(int PrinterType, Guid StockStatus, Guid BranchId)
        {
            try
            {
                return _db.StockAssignedToReport(PrinterType, StockStatus, BranchId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ALLPRINTERS
        public List<ALLPRINTERS_Result> GetAllPrinters()
        {
            try
            {
                return _db.ALLPRINTERS().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public bool GetClassAndValidityID(Guid CatId)
        {
            try
            {
                return _db.tblCategories.FirstOrDefault(t => t.ID == CatId).IsBritRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetIsBritrailPassByCatID(Guid CatId)
        {
            try
            {
                return _db.tblCategories.FirstOrDefault(t => t.ID == CatId).IsBritRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetIsNationaltyByProdId(Guid ProdId)
        {
            try
            {
                return _db.tblProducts.FirstOrDefault(t => t.ID == ProdId).NationalityValid ?? false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetIsEurailPassByCatID(Guid CatId)
        {
            try
            {
                return _db.tblCategories.FirstOrDefault(t => t.ID == CatId).IsPrintQueueEnable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool Delp2psalePayConfirm(Guid ID)
        {
            try
            {
                var data = _db.tblP2PSale.Where(t => t.ID == ID).FirstOrDefault();
                var lookup = _db.tblPassP2PSalelookup.FirstOrDefault(t => t.PassSaleID == ID);
                if (lookup != null)
                    _db.tblOrderTravellers.Where(t => t.ID == lookup.OrderTravellerID).ToList().ForEach(_db.tblOrderTravellers.DeleteObject);
                _db.tblPassP2PSalelookup.Where(t => t.PassSaleID == ID).ToList().ForEach(_db.tblPassP2PSalelookup.DeleteObject);
                _db.tblP2PSale.Where(t => t.ID == ID).ToList().ForEach(_db.tblP2PSale.DeleteObject);
                _db.SaveChanges();
                if (data != null)
                {
                    return _db.tblPassP2PSalelookup.Any(t => t.OrderID == lookup.OrderID);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DelpasssalePayConfirm(Guid ID)
        {
            try
            {
                var data = _db.tblPassSales.Where(t => t.ID == ID).FirstOrDefault();
                var lookup = _db.tblPassP2PSalelookup.FirstOrDefault(t => t.PassSaleID == ID);
                if (lookup != null)
                    _db.tblOrderTravellers.Where(t => t.ID == lookup.OrderTravellerID).ToList().ForEach(_db.tblOrderTravellers.DeleteObject);
                _db.tblPassP2PSalelookup.Where(t => t.PassSaleID == ID).ToList().ForEach(_db.tblPassP2PSalelookup.DeleteObject);
                _db.tblPassSales.Where(t => t.ID == ID).ToList().ForEach(_db.tblPassSales.DeleteObject);
                _db.SaveChanges();
                if (data != null)
                {
                    return _db.tblPassSales.Any(t => t.OrderID == data.OrderID);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Shipping Data DDL Operation
        public Guid AddUpdateShippingData(tblShipping shipping)
        {
            int casevalid = 0;
            try
            {
                var list = _db.tblShippings.FirstOrDefault(ty => ty.ID == shipping.ID);
                if (list != null)
                {
                    list.ShippingName = shipping.ShippingName;
                    list.SiteID = shipping.SiteID;
                    list.Price = shipping.Price;
                    list.IsActive = shipping.IsActive;
                    list.Description = shipping.Description;
                    list.ModifyedBy = shipping.CreatedBy;
                    list.ModifyedOn = shipping.CreatedOn;
                    casevalid = 1;
                }
                else
                {
                    _db.AddTotblShippings(shipping);
                    casevalid = 2;
                }
                _db.SaveChanges();
                return shipping.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddUpdateShippingCountryLookup(Guid id, List<tblShippingOptionCountry> oLookup)
        {
            try
            {
                _db.tblShippingOptionCountries.Where(x => x.ShippingID == id).ToList().ForEach(_db.tblShippingOptionCountries.DeleteObject);
                foreach (var item in oLookup)
                {
                    item.ID = Guid.NewGuid();
                    _db.tblShippingOptionCountries.AddObject(item);
                }
                _db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddUpdatePassShippingCountryLookup(Guid id, List<tblPassShippingOptionCountry> oLookup)
        {
            try
            {
                _db.tblPassShippingOptionCountries.Where(x => x.ShippingID == id).ToList().ForEach(_db.tblPassShippingOptionCountries.DeleteObject);
                foreach (var item in oLookup)
                {
                    item.ID = Guid.NewGuid();
                    _db.tblPassShippingOptionCountries.AddObject(item);
                }
                _db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<shippingData> GetShippingData(Guid siteId)
        {
            try
            {
                var list = _db.tblShippings.Where(ty => ty.SiteID == siteId).ToList();
                return list.Select(ty => new shippingData
                {
                    ID = ty.ID,
                    ShippingName = ty.ShippingName,
                    SiteName = ty.tblSite.DisplayName,
                    Price = ty.Price,
                    Description = ty.Description,
                    IsActive = ty.IsActive,
                    DefaultWhenMissing = ty.DefaultWhenMissing,
                    IsVisibleFront = ty.IsVisibleFront
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveShippingByID(Guid id, bool IsActive, Guid UID, DateTime date)
        {
            try
            {
                var list = _db.tblShippings.FirstOrDefault(ty => ty.ID == id);
                if (list != null)
                {
                    list.IsActive = (IsActive != true);
                    list.ModifyedBy = UID;
                    list.ModifyedOn = date;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool VisibleShippingByID(Guid id, bool IsVisible, Guid UID, DateTime date)
        {
            try
            {
                var list = _db.tblShippings.FirstOrDefault(ty => ty.ID == id);
                if (list != null)
                {
                    list.IsVisibleFront = (IsVisible != true);
                    list.ModifyedBy = UID;
                    list.ModifyedOn = date;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<shippingData> getAllPassShippingDetailAdminSection(Guid siteId, Guid countryId)
        {
            try
            {
                var list = (from ts in _db.tblShippings
                            join tsLook in _db.tblPassShippingOptionCountries
                                on ts.ID equals tsLook.ShippingID
                            where ts.SiteID == siteId && ts.IsActive && tsLook.CountryID == countryId
                            select new { ts }).ToList();

                return list.Select(ty => new shippingData
                {
                    ID = ty.ts.ID,
                    ShippingName = ty.ts.ShippingName,
                    Description = ty.ts.Description,
                    Price = ty.ts.Price
                }).OrderBy(x => x.Price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblShipping> getDefaultShippingDetailAdminSection(Guid siteId)
        {
            try
            {
                return _db.tblShippings.Where(ty => ty.SiteID == siteId && ty.IsActive && ty.DefaultWhenMissing).OrderBy(ty => ty.Price).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DefaultShippingByID(Guid id, bool IsDefault, Guid UID, DateTime date)
        {
            try
            {
                var list = _db.tblShippings.FirstOrDefault(ty => ty.ID == id);
                if (list != null)
                {
                    list.DefaultWhenMissing = (IsDefault != true);
                    list.ModifyedBy = UID;
                    list.ModifyedOn = date;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblShipping GetShippingByID(Guid id)
        {
            try
            {
                return _db.tblShippings.FirstOrDefault(ty => ty.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblShippingOptionCountry> GetShippingCountriesById(Guid id)
        {
            return _db.tblShippingOptionCountries.Where(x => x.ShippingID == id).ToList();
        }

        public List<tblPassShippingOptionCountry> GetPassShippingCountriesById(Guid id)
        {
            return _db.tblPassShippingOptionCountries.Where(x => x.ShippingID == id).ToList();
        }

        public bool DeleteShippingByID(Guid id)
        {
            try
            {
                _db.tblShippingOptionCountries.Where(x => x.ShippingID == id).ToList().ForEach(_db.tblShippingOptionCountries.DeleteObject);
                _db.tblPassShippingOptionCountries.Where(x => x.ShippingID == id).ToList().ForEach(_db.tblPassShippingOptionCountries.DeleteObject);
                var rec = _db.tblShippings.FirstOrDefault(x => x.ID == id);
                _db.tblShippings.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Check Validate Date For booking
        public int getOneHubServiceDayCount(string serviceName)
        {
            int rday = 0;
            try
            {
                tblOneHubBookingDay obj = _db.tblOneHubBookingDays.Where(a => a.Name.ToLower() == serviceName.ToLower()).FirstOrDefault();
                if (obj != null)
                {
                    rday = obj.DaysCount.HasValue ? obj.DaysCount.Value : 0;
                }
                return rday;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetIsBritrailByProductID(Guid ProductID)
        {
            try
            {
                return _db.tblProductCategoriesLookUps.FirstOrDefault(t => t.ProductID == ProductID).tblCategory.IsBritRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Central Settelement
        public List<CentralSettlement_Result> GetCentralSettlement(DateTime startdate, DateTime enddate, string OfficeId, Boolean IsSta, string Supplier)
        {
            try
            {
                return _db.CentralSettlement(OfficeId, startdate, enddate, IsSta, Supplier).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Sale Breakdown Report
        public List<SalesBreakDown> GetSaleBreakdownReport(Guid? SiteID, string OfficeId, DateTime D1, DateTime D2, Boolean IsSta)
        {
            try
            {
                return _db.SalesBreakDown(SiteID, OfficeId, D1, D2, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeliveryMailDetails
        public Guid AddDeliveryDetails(tblP2PDeliveryDetails details)
        {
            try
            {
                var _detail = new tblP2PDeliveryDetails
                {
                    ID = details.ID,
                    OrderID = details.OrderID,
                    FirstName = details.FirstName,
                    LastName = details.LastName,
                    Department = details.Department,
                    Address = details.Address,
                    Postcode = details.Postcode,
                    City = details.City,
                    CountryID = details.CountryID,
                    StateID = details.StateID,
                    CreatedDate = details.CreatedDate
                };

                _db.AddTotblP2PDeliveryDetails(_detail);
                _db.SaveChanges();
                return _detail.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCountyMst> GetCountyList(Guid countryID)
        {
            try
            {
                return _db.tblCountyMsts.Where(x => x.CountryId == countryID && x.CountyIsActive == true).OrderBy(y => y.County).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateTerms&Condition
        public Boolean UpdateTermsConditionToOrder(long OrderID, string terms)
        {

            try
            {
                var objOrder = _db.tblOrders.FirstOrDefault(a => a.OrderID == OrderID);
                if (objOrder != null) objOrder.Note = terms;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get P2P Sale Information
        public List<getbookingcartdata> GetP2PSale(long OrderID, string Type, decimal tkPrice)
        {

            try
            {
                List<getbookingcartdata> lstPassDetail = new List<getbookingcartdata>();
                if (Type != "")
                {
                    List<GetAllCartData_Result> lstCartItem = _db.GetAllCartData(OrderID).ToList();
                    if (lstCartItem.Count() > 0)
                    {
                        var list = lstCartItem.AsEnumerable().Select(x => new getbookingcartdata
                        {
                            LID = x.LID,
                            OrderID = x.OrderID,
                            ProductID = x.PassSaleID,
                            ProductType = x.ProductType,
                            TravellerID = x.OrderTravellerID,
                            TicketProtection = Convert.ToBoolean(x.TicketProtection.HasValue && x.TicketProtection > 0 ? true : false),
                            P2PPassDetail = GetP2PPassDescription((Guid)x.PassSaleID, x.ProductType),
                            Price = getPrice(x.PassSaleID, x.ProductType),
                            Traveller = (x.Title != null ? (x.Title + " " + x.FirstName + " " + GetTraveller(x.LastName.Replace("Passenger:", ""))) : ""),
                            DateOfJourney = JourneyDate(x.PassSaleID, x.ProductType, x.PassStartDate),
                            TicketProtectionPrice = tkPrice,
                            PassSaleTicketProtectionPrice = x.TicketProtection.HasValue ? x.TicketProtection.Value : 0,
                            OrderIdentity = x.OrderIdentity.HasValue ? x.OrderIdentity.Value : 0
                        }).ToList();
                        lstPassDetail = list.OrderBy(a => a.OrderIdentity).ToList();
                    }
                }
                else
                {
                    List<GetAllCartData_Result> lstCartItem = _db.GetAllCartData(OrderID).ToList();
                    if (lstCartItem.Count() > 0)
                    {
                        var list = lstCartItem.AsEnumerable().Select(x => new getbookingcartdata
                        {
                            LID = x.LID,
                            OrderID = x.OrderID,
                            ProductID = x.PassSaleID,
                            ProductType = x.ProductType,
                            TravellerID = x.OrderTravellerID,
                            TicketProtection = Convert.ToBoolean(x.TicketProtection.HasValue && x.TicketProtection > 0 ? true : false),
                            P2PPassDetail = GetP2PPassDescription((Guid)x.PassSaleID, x.ProductType),
                            Price = getPrice(x.PassSaleID, x.ProductType),
                            Traveller = (x.Title != null ? (x.Title + " " + x.FirstName + " " + GetTraveller(x.LastName.Replace("Passenger:", ""))) : ""),
                            DateOfJourney = JourneyDate(x.PassSaleID, x.ProductType, x.PassStartDate),
                            TicketProtectionPrice = tkPrice,
                            PassSaleTicketProtectionPrice = x.TicketProtection.HasValue ? x.TicketProtection.Value : 0,
                            OrderIdentity = x.OrderIdentity.HasValue ? x.OrderIdentity.Value : 0
                        }).ToList();
                        lstPassDetail = list.OrderBy(a => a.OrderIdentity).ToList();
                    }
                }
                return lstPassDetail.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string GetTraveller(string traveller)
        {
            string[] travller = traveller.Split(',');
            string trav = "";
            foreach (var item in travller)
            {
                if (!item.Contains("0"))
                    trav += item + ", ";
            }
            return trav;
        }
        public P2PPassDetail GetP2PPassDescription(Guid? ProductID, string Type)
        {
            if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == ProductID).FirstOrDefault();
                return new P2PPassDetail
                {
                    TrainNo = objP2P.TrainNo.ToString(),
                    From = objP2P.From,
                    To = objP2P.To,
                    DateTimeDepature = Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd MMM yyyy"),
                    DepartureTime = Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm"),
                    DateTimeArrival = Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd MMM yyyy"),
                    ArrivalTime = Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm")
                };
            }
            return null;
        }
        #endregion

        public bool GetIsBritrail(long orderId)
        {
            try
            {
                return _db.tblPassSales.FirstOrDefault(t => t.OrderID == orderId).tblCategory.IsBritRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region New Desgin

        public string GetPassValidity(Guid? PassSaleID)
        {
            var obj = (from tp in _db.tblPassSales
                       join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                       where tp.ID == PassSaleID
                       select new { Name = pn.Name }).FirstOrDefault();
            if (obj != null)
                return obj.Name;
            return string.Empty;
        }
        public string GetP2PClass(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.Class;
            return string.Empty;
        }
        public string GetP2PService(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.SeviceName;
            return string.Empty;
        }
        public string GetP2PTrain(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.TrainNo;
            return string.Empty;
        }
        public string GetP2PArrives(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.ArrivalTime.Substring(0, 5) + " | " + obj.DateTimeArrival.Value.Date.ToString("dd MMM yyyy");
            return string.Empty;
        }
        public string GetP2PDeparts(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.DepartureTime.Substring(0, 5) + " | " + obj.DateTimeDepature.Value.Date.ToString("dd MMM yyyy");
            return string.Empty;
        }

        public string GetP2PArrivesStation(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.To;
            return string.Empty;
        }

        public string GetP2PDepartsStation(Guid? ID)
        {
            var obj = _db.tblP2PSale.FirstOrDefault(t => t.ID == ID);
            if (obj != null)
                return obj.From;
            return string.Empty;
        }

        public string GetP2PTravellerName(Guid? ID)
        {
            try
            {
                string TName = string.Empty;
                var data = _db.tblP2PSale.FirstOrDefault(ty => ty.ID == ID);
                if (data != null)
                    TName = (data.Adult != "0" ? data.Adult + " " + "Adult" : string.Empty) +
                    (data.Children != "0" ? data.Children + " " + "Child" : string.Empty) +
                    (data.Senior != "0" ? data.Senior + " " + "Senior" : string.Empty) +
                    (data.Youth != "0" ? data.Youth + " " + "Youth" : string.Empty);
                return TName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCountryByIDs(Guid? ID)
        {
            try
            {
                var list = _db.tblCountriesMsts.FirstOrDefault(ty => ty.CountryID == ID);
                if (list != null)
                    return list.CountryName;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetPassCountries(Guid? PassSaleID)
        {
            string country = string.Empty;
            var obj = _db.tblPassSales.FirstOrDefault(t => t.ID == PassSaleID);
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(obj.ProductXML);
                XmlNode xnr = xDoc.SelectSingleNode("Product/Countries");
                if (xnr != null)
                    country = xnr.InnerText;
                return country;
            }
            catch
            {
                if (obj != null)
                {
                    country = (!string.IsNullOrEmpty(getCountryNameByIDs(obj.Country1)) ? "(" + getCountryNameByIDs(obj.Country1) : string.Empty) +
                            (!string.IsNullOrEmpty(getCountryNameByIDs(obj.Country2)) ? ", " + getCountryNameByIDs(obj.Country2) : string.Empty) +
                            (!string.IsNullOrEmpty(getCountryNameByIDs(obj.Country3)) ? ", " + getCountryNameByIDs(obj.Country3) : string.Empty) +
                            (!string.IsNullOrEmpty(getCountryNameByIDs(obj.Country4)) ? ", " + getCountryNameByIDs(obj.Country4) : string.Empty) +
                            (!string.IsNullOrEmpty(getCountryNameByIDs(obj.Country5)) ? ", " + getCountryNameByIDs(obj.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                }
                return country;
            }

        }



        public string GetProductName(Guid? PassSaleID)
        {
            var obj = (from tp in _db.tblPassSales
                       join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                       where tp.ID == PassSaleID
                       select new { Productname = p.Name }).SingleOrDefault();
            if (obj != null)
                return obj.Productname;
            return string.Empty;
        }
        public string GetTravellerName(Guid? PassSaleID)
        {
            var obj = (from tp in _db.tblPassSales
                       join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                       where tp.ID == PassSaleID
                       select new { t.Name, t.FromAge, t.ToAge }).SingleOrDefault();
            if (obj != null)
                return obj.Name + " (" + obj.FromAge + " - " + obj.ToAge + " Years)";
            return string.Empty;
        }

        public string getPassDescType(Guid? ProductID, string Type, string trainType)
        {
            string PassDetail = "";
            if (Type != "P2P")
            {
                var objPS = (from tp in _db.tblPassSales
                             join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                             join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                             join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                             join c in _db.tblClassMsts on tp.ClassID equals c.ID
                             join od in _db.tblOrders on tp.OrderID equals od.OrderID
                             where tp.ID == ProductID
                             orderby t.Name
                             select new { Productname = p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
                if (objPS != null)
                {
                    string country =
                        (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                    PassDetail = objPS.Productname + country + ", " +
                        objPS.PassName + ", " +
                        objPS.PassDesc + ", " +
                        objPS.ClassType;
                }
            }
            else if (Type == "P2P")
            {
                var objP2P = _db.tblP2PSale.Where(a => a.ID == ProductID).FirstOrDefault();
                string status = "Issue";
                if (objP2P.Status == true)
                    status = "Confirm";

                string str = string.Empty;
                string pnrType = "PNR";
                if (!string.IsNullOrEmpty(objP2P.ReservationCode))
                {
                    if (!string.IsNullOrEmpty(trainType))
                        if (trainType == "BE")
                            pnrType = "DNR";
                    str = "<tr><td width='50%'>" + pnrType + " Number:</td><td width='50%'><b>" + objP2P.ReservationCode + "</b></td><tr>" +
                        "<tr><td width='50%'>Status:</td><td width='50%'>" + status + "</td><tr>";
                }

                string ClassType = string.Empty;
                if (objP2P.Class == "1")
                    ClassType = "1st Class";
                else if (objP2P.Class == "2")
                    ClassType = "2nd Class";
                else
                    ClassType = objP2P.Class;

                PassDetail = "Rail Tickets</td></tr><tr><td colspan='3'><table width='100%'>" +
                     "<tr><td width='50%'>Train Number:</td><td width='50%'>" + objP2P.TrainNo.ToString() + "</td><tr>" + str +
                     "<tr><td width='50%'>Class:</td><td width='50%'>" + ClassType + "</td><tr>" +
                     "<tr><td width='50%'>From:</td><td width='50%'>" + objP2P.From + "</td><tr>" +
                     "<tr><td width='50%'>To:</td><td width='50%'>" + objP2P.To + "</td><tr>" +
                     "<tr><td width='50%'>Departure Date:</td><td width='50%' class='dateofDeparture'>" + Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MM/yyyy") + "</td><tr>" +
                     "<tr><td width='50%'>Departure Time:</td><td width='50%'>" + Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") + "</td><tr>" +
                     "<tr><td width='50%'>Arrival Date:</td><td width='50%'>" + Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MM/yyyy") + "</td><tr>" +
                     "<tr><td width='50%'>Arrival Time:</td><td width='50%'>" + Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm") + "</td><tr>" +
                     "</table>";
            }
            return PassDetail;
        }
        #endregion

        #region Manual P2P booking
        public bool CheckIfAgent(Guid agentID)
        {
            var user = _db.tblAdminUsers.FirstOrDefault(x => x.ID == agentID && x.IsAgent == true && x.IsDeleted == false);
            return user != null;
        }
        #endregion

        public List<OrderReceipt> GetReceiptPassDescMail(Guid? ProductID, string Type, List<PrintResponse> PrintUrl, string trainType)
        {
            var datalist = new List<OrderReceipt>();
            string PassDetail = "";
            if (Type != "P2P")
            {
                var objPS = (from tp in _db.tblPassSales
                             join p in _db.tblProductNames on tp.ProductID equals p.ProductID
                             join t in _db.tblTravelerMsts on tp.TravellerID equals t.ID
                             join pn in _db.tblProductValidUpToNames on tp.ValidityID equals pn.ID
                             join c in _db.tblClassMsts on tp.ClassID equals c.ID
                             join od in _db.tblOrders on tp.OrderID equals od.OrderID
                             where tp.ID == ProductID
                             orderby t.Name
                             select new { Productname = p.Name, RailPassName = p.Name, PassName = t.Name, PassDesc = pn.Name, ClassType = c.Name, TrvClass = od.TrvType, tp.Price, tp.Country1, tp.Country2, tp.Country3, tp.Country4, tp.Country5 }).SingleOrDefault();
                if (objPS != null)
                {
                    string country =
                        (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country1)) ? "(" + getCountryNameByIDs(objPS.Country1) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country2)) ? ", " + getCountryNameByIDs(objPS.Country2) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country3)) ? ", " + getCountryNameByIDs(objPS.Country3) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country4)) ? ", " + getCountryNameByIDs(objPS.Country4) : string.Empty) +
                       (!string.IsNullOrEmpty(getCountryNameByIDs(objPS.Country5)) ? ", " + getCountryNameByIDs(objPS.Country5) : string.Empty);
                    if ((country.LastIndexOf(',') - (country.Length - 1)) <= 0 && country.Length > 0)
                        country = country + ")";
                    PassDetail = objPS.Productname + country + ", " +
                        objPS.PassName + ", " +
                        objPS.PassDesc + ", " +
                        objPS.ClassType;

                    datalist.Add(new OrderReceipt { Id = 7, Name = "Journey ", Value = PassDetail });
                }
            }
            else if (Type == "P2P")
            {
                tblP2PSale objP2P = _db.tblP2PSale.FirstOrDefault(a => a.ID == ProductID);
                if (objP2P != null)
                {
                    Int32 orderid = (Int32)_db.tblPassP2PSalelookup.FirstOrDefault(a => a.PassSaleID == ProductID).OrderID;
                    string status = objP2P.Status == true ? "Confirm" : "Issue";
                    string str = string.Empty; string trainNO = string.Empty;
                    string pnrType = "PNR Number";
                    if (!string.IsNullOrEmpty(objP2P.ReservationCode))
                    {
                        if (!string.IsNullOrEmpty(trainType) && trainType == "BE")
                            pnrType = "DNR Number";

                        if (!string.IsNullOrEmpty(objP2P.ApiName) && objP2P.ApiName.Trim().ToLower() == "evolvi")
                            pnrType = "TOD Ref";

                        datalist.Add(new OrderReceipt { Id = 8, Name = pnrType, Value = objP2P.ReservationCode });
                        //datalist.Add(new OrderReceipt { Id = 9, Name = "Status", Value = status });
                    }
                    if (objP2P.ApiName != "Evolvi")
                        datalist.Add(new OrderReceipt { Id = 7, Name = "Train Number", Value = objP2P.TrainNo });

                    datalist.Add(new OrderReceipt { Id = 10, Name = "From", Value = objP2P.From });
                    datalist.Add(new OrderReceipt { Id = 11, Name = "To", Value = objP2P.To });
                    datalist.Add(new OrderReceipt { Id = 12, Name = "Departure Date", Value = Convert.ToDateTime(objP2P.DateTimeDepature).ToString("dd/MMM/yyyy") });
                    datalist.Add(new OrderReceipt { Id = 13, Name = "Departure Time", Value = Convert.ToDateTime(objP2P.DepartureTime).ToString("HH:mm") });
                    datalist.Add(new OrderReceipt { Id = 14, Name = "Arrival Date", Value = Convert.ToDateTime(objP2P.DateTimeArrival).ToString("dd/MMM/yyyy") });
                    datalist.Add(new OrderReceipt { Id = 15, Name = "Arrival Time", Value = Convert.ToDateTime(objP2P.ArrivalTime).ToString("HH:mm") });
                }
            }
            return datalist;
        }

        public string GetPassengerDetails(Guid PassSaleId)
        {
            try
            {
                string name = "";
                var data = _db.tblP2PSale.FirstOrDefault(x => x.ID == PassSaleId);
                if (data != null)
                {
                    if (Convert.ToInt32(data.Adult) > 0)
                        name = (name == "" ? data.Adult + " Adult" : data.Adult + " Adult");
                    if (Convert.ToInt32(data.Children) > 0)
                        name = (name == "" ? data.Children + " Children" : data.Children + " Children");
                    if (Convert.ToInt32(data.Youth) > 0)
                        name = (name == "" ? data.Youth + " Youth" : data.Youth + " Youth");
                    if (Convert.ToInt32(data.Senior) > 0)
                        name = (name == "" ? data.Senior + " Senior" : data.Senior + " Senior");
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetExtraCharge(Guid PassSaleId, string ProductType)
        {
            try
            {
                if (ProductType == "P2P")
                    return _db.tblP2PSale.FirstOrDefault(t => t.ID == PassSaleId).AdminExtraCharge;
                else
                    return _db.tblPassSales.FirstOrDefault(t => t.ID == PassSaleId).AdminExtraCharge;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetExtraChargeByOrderId(long OrderId)
        {
            try
            {
                var data = (_db.tblPassP2PSalelookup.FirstOrDefault(t => t.OrderID == OrderId && t.PassSaleID != Guid.Empty));
                if (data != null)
                {
                    if (data.ProductType == "P2P")
                        return _db.tblP2PSale.Where(t => _db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderId).Select(x => x.PassSaleID).Contains(t.ID)).Sum(t => t.AdminExtraCharge);
                    else
                        return _db.tblPassSales.Where(t => t.OrderID == OrderId).Sum(t => t.AdminExtraCharge);
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateExtraCharge(decimal Amount, Guid PassSaleId)
        {
            try
            {
                var data = _db.tblPassSales.FirstOrDefault(t => t.ID == PassSaleId);
                if (data != null)
                    data.AdminExtraCharge = Amount;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateExtraChargeP2P(decimal Amount, Guid PassSaleId)
        {
            try
            {
                var data = _db.tblP2PSale.FirstOrDefault(t => t.ID == PassSaleId);
                if (data != null)
                    data.AdminExtraCharge = Amount;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePnrNoByPassSaleId(Guid PassSaleId, string ReservationCode)
        {
            try
            {
                var data = _db.tblP2PSale.FirstOrDefault(x => x.ID == PassSaleId);
                if (data != null)
                {
                    data.OldReservationCode = data.ReservationCode;
                    data.ReservationCode = ReservationCode;
                    _db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetOldReservationCode(Guid P2PSaleId)
        {
            try
            {
                var data = _db.tblP2PSale.FirstOrDefault(x => x.ID == P2PSaleId);
                if (data != null)
                    return !string.IsNullOrEmpty(data.OldReservationCode) ? data.OldReservationCode : "";
                return "";
            }
            catch (Exception ex) { throw ex; }
        }

        #region Evolvi Methods

        public string GetCountryISOCode(Guid SiteId)
        {
            try
            {
                var objCounty = (from TCM in _db.tblCountriesMsts
                                 join TS in _db.tblSites on TCM.CountryID equals TS.DefaultCountryID
                                 where TS.ID == SiteId && TS.IsActive == true && TCM.IsActive == true
                                 select new { TCM }).FirstOrDefault();
                if (objCounty != null)
                    return objCounty.TCM.IsoCode.ToString();
                return "";
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetCurrencyISOCode(Guid SiteId)
        {
            try
            {
                var objCurrency = (from TCM in _db.tblCurrencyMsts
                                   join TS in _db.tblSites on TCM.ID equals TS.DefaultCurrencyID
                                   where TS.ID == SiteId && TS.IsActive == true && TCM.IsActive == true
                                   select new { TCM }).FirstOrDefault();
                if (objCurrency != null)
                    return objCurrency.TCM.ShortCode.ToString();
                return "";
            }
            catch (Exception ex) { throw ex; }
        }

        public tblEvolviOtherChargesLookUp GetEvolviChargesByOrderId(long OrderId)
        {
            try
            {
                return _db.tblEvolviOtherChargesLookUps.FirstOrDefault(x => x.OrderId == OrderId);
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid AddP2POrderTraveller(tblOrderTraveller objtblOrderTraveller)
        {
            try
            {
                tblOrderTraveller objOTravell = new tblOrderTraveller();
                objOTravell.ID = Guid.NewGuid();
                objOTravell.Title = objtblOrderTraveller.Title;
                objOTravell.FirstName = objtblOrderTraveller.FirstName;
                objOTravell.LastName = objtblOrderTraveller.LastName;
                objOTravell.Country = objtblOrderTraveller.Country;
                objOTravell.LeadPassenger = objtblOrderTraveller.LeadPassenger;
                _db.tblOrderTravellers.AddObject(objOTravell);
                _db.SaveChanges();
                return objOTravell.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public void UpdateP2POrderTraveller(long OrderId, Guid TravellerId)
        {
            try
            {
                _db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderId).ToList().ForEach(x => x.OrderTravellerID = TravellerId);
                _db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }

        public void AddP2POtherTravellerLookup(long OrderId, Guid TravellerId)
        {
            try
            {
                var objP2PPasssale = _db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderId).OrderBy(x => x.ShortOrder).FirstOrDefault();
                if (objP2PPasssale != null)
                {
                    tblP2POtherTravellerLookup objP2POtherLookUp = new tblP2POtherTravellerLookup();
                    objP2POtherLookUp.ID = Guid.NewGuid();
                    objP2POtherLookUp.OrderID = OrderId;
                    objP2POtherLookUp.PassSaleID = objP2PPasssale.PassSaleID;
                    objP2POtherLookUp.OrderTravellerID = TravellerId;
                    _db.tblP2POtherTravellerLookup.AddObject(objP2POtherLookUp);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddUpdateEvolviCharges(tblEvolviOtherChargesLookUp objCharges)
        {
            try
            {
                var charge = _db.tblEvolviOtherChargesLookUps.FirstOrDefault(x => x.OrderId == objCharges.OrderId);
                if (charge != null)
                {
                    charge.TicketCost = objCharges.TicketCost;
                    charge.Discount = objCharges.Discount;
                    charge.TicketChange = objCharges.TicketChange;
                    charge.CreditCardCharge = objCharges.CreditCardCharge;
                    charge.BookingRef = objCharges.BookingRef;
                    charge.BookingItemRef = objCharges.BookingItemRef;
                }
                else
                {
                    _db.tblEvolviOtherChargesLookUps.AddObject(new tblEvolviOtherChargesLookUp
                    {
                        ID = objCharges.ID,
                        SiteId = objCharges.SiteId,
                        OrderId = objCharges.OrderId,
                        TicketCost = objCharges.TicketCost,
                        Discount = objCharges.Discount,
                        TicketChange = objCharges.TicketChange,
                        CreditCardCharge = objCharges.CreditCardCharge,
                        BookingRef = objCharges.BookingRef,
                        BookingItemRef = objCharges.BookingItemRef
                    });
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddEvolviSleeper(tblEvolviSleeperPriceLookUp objCharges)
        {
            try
            {
                _db.tblEvolviSleeperPriceLookUps.AddObject(new tblEvolviSleeperPriceLookUp
                {
                    ID = Guid.NewGuid(),
                    SiteID = objCharges.SiteID,
                    OrderId = objCharges.OrderId,
                    Price = objCharges.Price,
                    PassSaleId = objCharges.PassSaleId
                });
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateSleeperPrice(long OrderID, decimal SleeperOutCharge, decimal SleeperInCharge)
        {
            try
            {
                var data = (from A in _db.tblP2PSale
                            join B in _db.tblPassP2PSalelookup
                            on A.ID equals B.PassSaleID
                            where B.OrderID == OrderID
                            select new { Id = A.ID, P2PID = A.P2PId }).OrderBy(x => x.P2PID).ToList();
                if (data != null && data.Count > 0)
                {
                    Guid OutId = data.OrderBy(x => x.P2PID).FirstOrDefault().Id;
                    var updateOutboundPrice = _db.tblP2PSale.FirstOrDefault(x => x.ID == OutId);
                    if (updateOutboundPrice != null)
                    {
                        updateOutboundPrice.Price = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == OutId && x.OrderId == OrderID).Price) + SleeperOutCharge;
                        updateOutboundPrice.NetPrice = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == OutId && x.OrderId == OrderID).Price) + SleeperOutCharge;
                        _db.SaveChanges();
                    }
                    if (data.Count > 1)
                    {
                        Guid InId = data.OrderByDescending(x => x.P2PID).FirstOrDefault().Id;
                        var updateInboundPrice = _db.tblP2PSale.FirstOrDefault(x => x.ID == InId);
                        if (updateInboundPrice != null)
                        {
                            updateInboundPrice.Price = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == InId && x.OrderId == OrderID).Price) + SleeperInCharge;
                            updateInboundPrice.NetPrice = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == InId && x.OrderId == OrderID).Price) + SleeperInCharge;
                            _db.SaveChanges();
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateSleeperPriceZero(long OrderID, decimal SleeperOutCharge, decimal SleeperInCharge, bool isOutBoundZero, bool isInboundZero)
        {
            try
            {
                var data = (from A in _db.tblP2PSale
                            join B in _db.tblPassP2PSalelookup
                            on A.ID equals B.PassSaleID
                            where B.OrderID == OrderID
                            select new { Id = A.ID, P2PID = A.P2PId }).OrderBy(x => x.P2PID).ToList();
                if (data != null && data.Count > 0)
                {
                    Guid OutId = data.OrderBy(x => x.P2PID).FirstOrDefault().Id;
                    var updateOutboundPrice = _db.tblP2PSale.FirstOrDefault(x => x.ID == OutId);
                    if (updateOutboundPrice != null && isOutBoundZero)
                    {
                        updateOutboundPrice.Price = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == OutId && x.OrderId == OrderID).Price) + SleeperOutCharge;
                        updateOutboundPrice.NetPrice = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == OutId && x.OrderId == OrderID).Price) + SleeperOutCharge;
                        _db.SaveChanges();
                    }
                    if (data.Count > 1)
                    {
                        Guid InId = data.OrderByDescending(x => x.P2PID).FirstOrDefault().Id;
                        var updateInboundPrice = _db.tblP2PSale.FirstOrDefault(x => x.ID == InId);
                        if (updateInboundPrice != null && isInboundZero)
                        {
                            updateInboundPrice.Price = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == InId && x.OrderId == OrderID).Price) + SleeperInCharge;
                            updateInboundPrice.NetPrice = (_db.tblEvolviSleeperPriceLookUps.FirstOrDefault(x => x.PassSaleId == InId && x.OrderId == OrderID).Price) + SleeperInCharge;
                            _db.SaveChanges();
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateEvolviBookingRef(long OrderId, string BookingRef, string BookingItemRef, string TODRef)
        {
            try
            {
                var charge = _db.tblEvolviOtherChargesLookUps.FirstOrDefault(x => x.OrderId == OrderId);
                if (charge != null)
                {
                    charge.BookingRef = BookingRef;
                    charge.BookingItemRef = BookingItemRef;
                    charge.TODRef = TODRef;
                }
                var objp2pLU = _db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderId).Select(x => x.PassSaleID).ToList();
                if (objp2pLU != null && objp2pLU.Count > 0)
                {
                    _db.tblP2PSale.Where(x => objp2pLU.Contains(x.ID)).ToList().ForEach(x => x.ReservationCode = TODRef);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddSleeperSeat(long OrderId, string OutSleeper, string InSleeper)
        {
            try
            {
                var data = (from A in _db.tblP2PSale
                            join B in _db.tblPassP2PSalelookup
                            on A.ID equals B.PassSaleID
                            where B.OrderID == OrderId
                            select new { Id = A.ID, P2PID = A.P2PId }).OrderBy(x => x.P2PID).ToList();
                if (data != null && data.Count > 0)
                {
                    Guid OutId = data.OrderBy(x => x.P2PID).FirstOrDefault().Id;
                    var updateOutboundPrice = _db.tblP2PSale.FirstOrDefault(x => x.ID == OutId);
                    if (updateOutboundPrice != null)
                    {
                        updateOutboundPrice.EvSleeper = OutSleeper;
                        _db.SaveChanges();
                    }
                    if (data.Count > 1)
                    {
                        Guid InId = data.OrderByDescending(x => x.P2PID).FirstOrDefault().Id;
                        var updateInboundPrice = _db.tblP2PSale.FirstOrDefault(x => x.ID == InId);
                        if (updateInboundPrice != null)
                        {
                            updateInboundPrice.EvSleeper = InSleeper;
                            _db.SaveChanges();
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblRailCard> GetRailCardList()
        {
            try
            {
                return _db.tblRailCards.Where(x => x.DisplayOnly == false).OrderBy(x => x.RailCardName).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateP2PSaleStatus(Guid p2pSaleId)
        {
            try
            {
                var objp2p = _db.tblP2PSale.FirstOrDefault(x => x.ID == p2pSaleId);
                if (objp2p != null)
                    objp2p.Status = true;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public ManageApiType GetAllApiType(long OrderID)
        {
            try
            {
                ManageApiType _ManageApiType = new ManageApiType();
                var result = (from A in _db.tblP2PSale
                              join B in _db.tblPassP2PSalelookup on A.ID equals B.PassSaleID
                              join C in _db.tblOrders on B.OrderID equals C.OrderID
                              where C.OrderID == OrderID && C.TrvType == "P2P"
                              select new { A }).ToList();

                if (result != null && result.Count > 0)
                {
                    if (!string.IsNullOrEmpty(result.FirstOrDefault().A.ApiName))
                    {
                        _ManageApiType.isEvolvi = result.FirstOrDefault().A.ApiName.ToUpper() == "EVOLVI" ? true : false;
                        _ManageApiType.isNTV = result.FirstOrDefault().A.ApiName.ToUpper() == "NTV" ? true : false;
                        _ManageApiType.isBene = result.FirstOrDefault().A.ApiName.ToUpper() == "BENE" ? true : false;
                        _ManageApiType.isTI = result.FirstOrDefault().A.ApiName.ToUpper() == "ITALIA" ? true : false;
                    }
                    else
                        _ManageApiType.isNTV = _ManageApiType.isEvolvi = _ManageApiType.isBene = _ManageApiType.isTI = false;
                }
                else
                    _ManageApiType.isNTV = _ManageApiType.isEvolvi = _ManageApiType.isBene = _ManageApiType.isTI = false;
                return _ManageApiType;
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region P2P Country
        public List<tblP2PCountry> GetP2PCountryBySiteid(Guid siteId)
        {
            try
            {
                return _db.tblP2PCountry.Where(x => x.SiteId == siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<P2PCountry> GetP2PCountry(Guid siteId)
        {
            try
            {
                var olist = (from sp in _db.tblP2PCountry
                             join s in _db.tblSites
                             on sp.SiteId equals s.ID
                             where sp.SiteId == siteId
                             select new { s, sp }).ToList();

                var list = olist.Distinct().Select(x => new P2PCountry
                {
                    Id = x.sp.Id,
                    CountryName = x.sp.CountryName.Trim(),
                    SiteName = x.s.DisplayName,
                    IsActive = x.sp.IsActive,
                    PageContent = x.sp.PageContent,
                    Logo = x.sp.Logo
                }).ToList().ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddP2PCountry(tblP2PCountry oTxt)
        {
            try
            {
                var txt = new tblP2PCountry
                {
                    Id = oTxt.Id,
                    SiteId = oTxt.SiteId,
                    CountryName = oTxt.CountryName,
                    PageContent = oTxt.PageContent,
                    IsActive = oTxt.IsActive,
                    Logo = oTxt.Logo
                };
                _db.AddTotblP2PCountry(txt);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateP2PCountry(tblP2PCountry oTxt)
        {
            try
            {
                var _txt = _db.tblP2PCountry.FirstOrDefault(x => x.Id == oTxt.Id);
                if (_txt != null)
                {
                    _txt.CountryName = oTxt.CountryName;
                    _txt.IsActive = oTxt.IsActive;
                    _txt.PageContent = oTxt.PageContent;
                    _txt.SiteId = oTxt.SiteId;
                    if (!string.IsNullOrEmpty(oTxt.Logo))
                        _txt.Logo = oTxt.Logo;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteP2PCountry(Guid id)
        {
            try
            {
                var rec = _db.tblP2PCountry.FirstOrDefault(x => x.Id == id);
                _db.tblP2PCountry.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblP2PCountry GetP2PCountryById(Guid id)
        {
            try
            {
                return _db.tblP2PCountry.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveP2PCountry(Guid id)
        {
            try
            {
                var rec = _db.tblP2PCountry.FirstOrDefault(x => x.Id == id);
                rec.IsActive = !rec.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public class P2PCountry : tblP2PCountry
        {
            public string SiteName { get; set; }
        }

        #endregion
    }

    public class AgentDetails
    {
        public string AgentName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
    }


    public class EurailStock
    {
        public long StockNo { get; set; }
        public string Location { get; set; }
        public DateTime Date { get; set; }
        public long OrderNo { get; set; }
        public string Name { get; set; }
        public string Product { get; set; }
        public decimal SalePrice { get; set; }
        public string Status { get; set; }
        public string SaleCurrency { get; set; }
        public decimal NetEURPrice { get; set; }
        public decimal SaleEURPrice { get; set; }
    }
    public class P2PbookingFee
    {
        public string IsApplicable { get; set; }
        public string ID { get; set; }
        public string SiteID { get; set; }
        public string Name { get; set; }
        public string Fee { get; set; }
        public string IsPercentage { get; set; }
    }
    public class shippingData : tblShipping
    {
        public string SiteName { get; set; }
    }
    public class PassDetails
    {
        public Guid LID { get; set; }
        public long OrderID { get; set; }
        public Guid? ProductID { get; set; }
        public string ProductType { get; set; }
        public Guid? TravellerID { get; set; }
        public Boolean TicketProtection { get; set; }
        public string PassDesc { get; set; }
        public P2PPassDetail P2PPassDetail { get; set; }
        public decimal Price { get; set; }
        public string Traveller { get; set; }
        public Guid? ProductCurID { get; set; }
        public DateTime? DateOfJourney { get; set; }
    }
    public class getRailPassData : tblOrderTraveller
    {

        public String DOB { get; set; }
        public Guid Id { get; set; }
        public string PrdtId { get; set; }
        public string PrdtName { get; set; }
        public string TravellerID { get; set; }
        public string TravellerName { get; set; }
        public string ValidityID { get; set; }
        public string ValidityName { get; set; }
        public string ClassID { get; set; }
        public string ClassName { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string SalePrice { get; set; }
        public string OriginalPrice { get; set; }
        public string CategoryID { get; set; }
        public string Commission { get; set; }
        public string MarkUp { get; set; }
        public string CountryStartCode { get; set; }
        public string CountryEndCode { get; set; }
        public Boolean IsBritrail { get; set; }
        public Boolean IsEurail { get; set; }
        public Boolean PassportIsVisible { get; set; }
        public Boolean NationalityIsVisible { get; set; }
        public string CountryLevelIDs { get; set; }
        public string MonthValidity { get; set; }
        public Boolean IsHighSpeedPass { get; set; }
        public string CountryName { get; set; }
        public int PassTypeCode { get; set; }
        public decimal BookingFee { get; set; }
    }

    public class getbookingcartdata : PassDetails
    {
        public decimal TicketProtectionPrice { get; set; }
        public decimal PassSaleTicketProtectionPrice { get; set; }
        public long OrderIdentity { get; set; }
    }

    public class CountryName
    {
        public string Country { get; set; }
    }
    public class PrintResponse
    {
        public string URL { get; set; }
        public string status { get; set; }
        public string ReservationCode { get; set; }
    }

    public class P2PCommissionFee
    {
        public Guid? ID { get; set; }
        public Guid? SiteID { get; set; }
        public bool IsApplicable { get; set; }


        public string Name { get; set; }
        public decimal IsPercentage { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid ModifiedBy { get; set; }
    }
    public class P2PPassDetail
    {

        public string TrainNo { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string DateTimeDepature { get; set; }
        public string DepartureTime { get; set; }
        public string DateTimeArrival { get; set; }
        public string ArrivalTime { get; set; }
    }
    public class BritrailSettlement
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Amount { get; set; }
        public string Other { get; set; }
    }

    public class OrderReceipt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ManageApiType
    {
        public bool isEvolvi { get; set; }
        public bool isNTV { get; set; }
        public bool isTI { get; set; }
        public bool isBene { get; set; }
    }

    public class ManageP2PSaleData : tblP2PSale
    {
        public long OrderID { get; set; }
    }

    public class SelectDDLList
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
