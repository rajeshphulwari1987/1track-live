//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class aa_old
    {
        public string Name { get; set; }
        public System.Guid ID { get; set; }
        public System.Guid CurrencyID { get; set; }
        public int CountryStartCode { get; set; }
        public int CountryEndCode { get; set; }
        public string Description { get; set; }
        public string Eligibility { get; set; }
        public string EligibilityforSaver { get; set; }
        public string Discount { get; set; }
        public string Validity { get; set; }
        public string TermCondition { get; set; }
        public bool IsPromoPass { get; set; }
        public bool IsActive { get; set; }
        public bool IsShippingAplicable { get; set; }
        public bool IsTopRailPass { get; set; }
        public string CounrtyImage { get; set; }
        public string BannerImage { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> PassportValid { get; set; }
        public string ProductImage { get; set; }
        public Nullable<int> MonthValidity { get; set; }
        public System.DateTime ProductValidFromDate { get; set; }
        public System.DateTime ProductValidToDate { get; set; }
        public System.DateTime ProductEnableFromDate { get; set; }
        public System.DateTime ProductEnableToDate { get; set; }
        public bool IsItxPass { get; set; }
        public string TemplateNote { get; set; }
        public bool Istwinpass { get; set; }
        public Nullable<bool> NationalityValid { get; set; }
        public bool IsSpecialOffer { get; set; }
        public string ProductImageSecond { get; set; }
        public string GreatPassDescription { get; set; }
        public string HomePageProductImage { get; set; }
        public bool BestSeller { get; set; }
        public string SpecialOfferText { get; set; }
        public string AnnouncementTitle { get; set; }
        public string Announcement { get; set; }
        public string StartDayText { get; set; }
        public string ProductAltTag1 { get; set; }
        public string ProductAltTag2 { get; set; }
        public Nullable<System.DateTime> BritrailPromoFrom { get; set; }
        public Nullable<System.DateTime> BritrailPromoTo { get; set; }
        public string IrTitle { get; set; }
        public string IrDesciption { get; set; }
        public bool IsElectronicPass { get; set; }
        public string ElectronicPassNote { get; set; }
        public bool IsAllowAdminPass { get; set; }
        public bool IsAllowMiddleName { get; set; }
        public bool IsAllowRefund { get; set; }
        public Nullable<System.Guid> SupplierId { get; set; }
    }
}
