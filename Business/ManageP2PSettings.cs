﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business
{
    public class ManageP2PSettings
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public bool AddP2PSetting(tblP2PSetting setting)
        {
            try
            {
                if (setting.ID == Guid.Empty)
                {
                    setting.ID = Guid.NewGuid();
                    if (_db.tblP2PSetting.Any(m => m.SiteID == setting.SiteID))
                        throw new Exception("Setting is already exist for this setting.");
                    _db.tblP2PSetting.AddObject(setting);
                }
                else
                {
                    var tblP2PSetting = _db.tblP2PSetting.FirstOrDefault(m => m.ID == setting.ID);
                    tblP2PSetting.ModifyBy = setting.CreatedBy;
                    tblP2PSetting.ModifyOn = setting.CreatedOn;
                    tblP2PSetting.IsActive = setting.IsActive;
                    tblP2PSetting.P2PWidgetPage = setting.P2PWidgetPage;
                    tblP2PSetting.P2pTrainResultsPage = setting.P2pTrainResultsPage;
                    tblP2PSetting.P2PLoyaltyCardInfoPage = setting.P2PLoyaltyCardInfoPage;
                    tblP2PSetting.DirectoryPath = setting.DirectoryPath;
                    tblP2PSetting.SiteID = setting.SiteID;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteP2PSetting(Guid id)
        {
            try
            {
                var v = _db.tblP2PSetting.FirstOrDefault(x => x.ID == id);
                _db.tblP2PSetting.DeleteObject(v);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ActiveInactiveP2PSetting(Guid id)
        {
            try
            {
                tblP2PSetting setting = _db.tblP2PSetting.FirstOrDefault(m => m.ID == id);
                setting.IsActive = !setting.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<P2PSettingField> GetP2PSettingList(Guid SiteId)
        {
            try
            {
                return _db.tblP2PSetting.Where(x => x.SiteID == SiteId).Select(p2p => new P2PSettingField
                {
                    ID = p2p.ID,
                    SiteName = p2p.tblSite.DisplayName,
                    SiteID = p2p.SiteID,
                    P2PWidgetPage = p2p.P2PWidgetPage,
                    P2pTrainResultsPage = p2p.P2pTrainResultsPage,
                    P2PLoyaltyCardInfoPage = p2p.P2PLoyaltyCardInfoPage,
                    DirectoryPath = p2p.DirectoryPath,
                    IsActive = p2p.IsActive
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblP2PSetting GetP2PSettingById(Guid id)
        {
            try
            {
                return _db.tblP2PSetting.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblP2PSetting GetP2PSettingBySiteId(Guid SiteId)
        {
            try
            {
                return _db.tblP2PSetting.FirstOrDefault(x => x.SiteID == SiteId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public class P2PSettingField : tblP2PSetting
        {
            public string SiteName { get; set; }
        }

        public static string URLPath()
        {
            try
            {
                HttpContext context = HttpContext.Current;
                return context.Request.RawUrl;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

