//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSite
    {
        public tblSite()
        {
            this.HashtagMappings = new HashSet<HashtagMapping>();
            this.tblAdminFees = new HashSet<tblAdminFee>();
            this.tblAffiliateUserLookupSites = new HashSet<tblAffiliateUserLookupSite>();
            this.tblAnalyticTags = new HashSet<tblAnalyticTag>();
            this.tblApiLoginDetailSiteLookups = new HashSet<tblApiLoginDetailSiteLookup>();
            this.tblBannerLookups = new HashSet<tblBannerLookup>();
            this.tblBookingConditions = new HashSet<tblBookingCondition>();
            this.tblBookingFeeSiteLookUps = new HashSet<tblBookingFeeSiteLookUp>();
            this.tblBranchLookupSites = new HashSet<tblBranchLookupSite>();
            this.tblCategorySiteLookUps = new HashSet<tblCategorySiteLookUp>();
            this.tblConditionsofUses = new HashSet<tblConditionsofUse>();
            this.tblContacts = new HashSet<tblContact>();
            this.tblCookies = new HashSet<tblCookie>();
            this.tblCorpOfficeSiteLookUps = new HashSet<tblCorpOfficeSiteLookUp>();
            this.tblCountrySiteLookUps = new HashSet<tblCountrySiteLookUp>();
            this.tblCrossSaleAdsenseSiteLookUps = new HashSet<tblCrossSaleAdsenseSiteLookUp>();
            this.tblDeliveryOptionSiteLookUps = new HashSet<tblDeliveryOptionSiteLookUp>();
            this.tblEmailSettingLookups = new HashSet<tblEmailSettingLookup>();
            this.tblEQOEmailTemplates = new HashSet<tblEQOEmailTemplate>();
            this.tblEQOEnquiries = new HashSet<tblEQOEnquiry>();
            this.tblEurailPromotionSiteLookups = new HashSet<tblEurailPromotionSiteLookup>();
            this.tblEurailPromotionSortOrderLookups = new HashSet<tblEurailPromotionSortOrderLookup>();
            this.tblFaqs = new HashSet<tblFaq>();
            this.tblFeedbacks = new HashSet<tblFeedback>();
            this.tblFooterMenuSiteLookups = new HashSet<tblFooterMenuSiteLookup>();
            this.tblGeneralInformation_ = new HashSet<tblGeneralInformation_>();
            this.tblJourneySiteLookups = new HashSet<tblJourneySiteLookup>();
            this.tblManageAdsenceLinks = new HashSet<tblManageAdsenceLink>();
            this.tblMandatorySiteLookups = new HashSet<tblMandatorySiteLookup>();
            this.tblNewsLetterUsers = new HashSet<tblNewsLetterUser>();
            this.tblOrders = new HashSet<tblOrder>();
            this.tblP2PDeliveryChargesMst = new HashSet<tblP2PDeliveryChargesMst>();
            this.tblP2PSetting = new HashSet<tblP2PSetting>();
            this.tblPaymentCallCosts = new HashSet<tblPaymentCallCost>();
            this.tblPrivacyPolicies = new HashSet<tblPrivacyPolicy>();
            this.tblProductDiscountSiteLookups = new HashSet<tblProductDiscountSiteLookup>();
            this.tblProductSiteLookUps = new HashSet<tblProductSiteLookUp>();
            this.tblQubitScripts = new HashSet<tblQubitScript>();
            this.tblRailPassSec1 = new HashSet<tblRailPassSec1>();
            this.tblRailPassSec2 = new HashSet<tblRailPassSec2>();
            this.tblRailPassSec3 = new HashSet<tblRailPassSec3>();
            this.tblSecuritySiteLookups = new HashSet<tblSecuritySiteLookup>();
            this.tblSeoStieTags = new HashSet<tblSeoStieTag>();
            this.tblShippings = new HashSet<tblShipping>();
            this.tblSiteCurrencyLookUps = new HashSet<tblSiteCurrencyLookUp>();
            this.tblSiteThemes = new HashSet<tblSiteTheme>();
            this.tblSocialMediaScripts = new HashSet<tblSocialMediaScript>();
            this.tblSpecialTrainSiteLookUps = new HashSet<tblSpecialTrainSiteLookUp>();
            this.tblTermsandConditionSiteLookups = new HashSet<tblTermsandConditionSiteLookup>();
            this.tblTextContents = new HashSet<tblTextContent>();
            this.tblTicketProtections = new HashSet<tblTicketProtection>();
            this.tblTrainDetailSiteLookups = new HashSet<tblTrainDetailSiteLookup>();
            this.tblUserLogins = new HashSet<tblUserLogin>();
            this.tblWebMenuSiteLookups = new HashSet<tblWebMenuSiteLookup>();
        }
    
        public System.Guid ID { get; set; }
        public string DisplayName { get; set; }
        public Nullable<System.Guid> ThemeID { get; set; }
        public Nullable<System.Guid> DefaultCurrencyID { get; set; }
        public Nullable<System.Guid> DefaultLanguageID { get; set; }
        public Nullable<System.Guid> DefaultCountryID { get; set; }
        public string SiteURL { get; set; }
        public Nullable<bool> IsUS { get; set; }
        public Nullable<bool> IsSTA { get; set; }
        public Nullable<bool> IsAgent { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Root { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<bool> IsTopJourney { get; set; }
        public Nullable<bool> HavRailPass { get; set; }
        public Nullable<bool> IsVisibleNewsLetter { get; set; }
        public Nullable<bool> EnableP2P { get; set; }
        public Nullable<bool> EnableContactEmail { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<bool> EnableTicketProtection { get; set; }
        public int BookingDayLimit { get; set; }
        public string JourneyEmail { get; set; }
        public Nullable<bool> IsVisibleP2PWidget { get; set; }
        public string BccEmail { get; set; }
        public bool IsBccEmail { get; set; }
        public Nullable<bool> IsVisibleJRF { get; set; }
        public Nullable<bool> IsVisibleGeneralInfo { get; set; }
        public Nullable<bool> IsVisibleImages { get; set; }
        public bool IsWholeSale { get; set; }
        public string SiteHeading { get; set; }
        public string GoogleAnalytics { get; set; }
        public string GoogleManager { get; set; }
        public bool IsTrainTickets { get; set; }
        public string SiteHeadingTitle { get; set; }
        public string SiteAlias { get; set; }
        public bool IsCorporate { get; set; }
        public byte DateFormat { get; set; }
        public string FaviconPath { get; set; }
        public string OrderPrefix { get; set; }
        public string LogoPath { get; set; }
        public bool EnableAffiliateTracking { get; set; }
        public byte PaymentType { get; set; }
        public int LayoutType { get; set; }
        public bool IsAttentionVisible { get; set; }
        public string AttentionTitle { get; set; }
        public string AttentionMsg { get; set; }
        public string AttentionImg { get; set; }
        public string BDRColor { get; set; }
        public string BGColor { get; set; }
        public bool IsEnableCookie { get; set; }
        public bool IsVisibleFeed { get; set; }
    
        public virtual ICollection<HashtagMapping> HashtagMappings { get; set; }
        public virtual ICollection<tblAdminFee> tblAdminFees { get; set; }
        public virtual ICollection<tblAffiliateUserLookupSite> tblAffiliateUserLookupSites { get; set; }
        public virtual ICollection<tblAnalyticTag> tblAnalyticTags { get; set; }
        public virtual ICollection<tblApiLoginDetailSiteLookup> tblApiLoginDetailSiteLookups { get; set; }
        public virtual ICollection<tblBannerLookup> tblBannerLookups { get; set; }
        public virtual ICollection<tblBookingCondition> tblBookingConditions { get; set; }
        public virtual ICollection<tblBookingFeeSiteLookUp> tblBookingFeeSiteLookUps { get; set; }
        public virtual ICollection<tblBranchLookupSite> tblBranchLookupSites { get; set; }
        public virtual ICollection<tblCategorySiteLookUp> tblCategorySiteLookUps { get; set; }
        public virtual ICollection<tblConditionsofUse> tblConditionsofUses { get; set; }
        public virtual ICollection<tblContact> tblContacts { get; set; }
        public virtual ICollection<tblCookie> tblCookies { get; set; }
        public virtual ICollection<tblCorpOfficeSiteLookUp> tblCorpOfficeSiteLookUps { get; set; }
        public virtual ICollection<tblCountrySiteLookUp> tblCountrySiteLookUps { get; set; }
        public virtual ICollection<tblCrossSaleAdsenseSiteLookUp> tblCrossSaleAdsenseSiteLookUps { get; set; }
        public virtual ICollection<tblDeliveryOptionSiteLookUp> tblDeliveryOptionSiteLookUps { get; set; }
        public virtual ICollection<tblEmailSettingLookup> tblEmailSettingLookups { get; set; }
        public virtual ICollection<tblEQOEmailTemplate> tblEQOEmailTemplates { get; set; }
        public virtual ICollection<tblEQOEnquiry> tblEQOEnquiries { get; set; }
        public virtual ICollection<tblEurailPromotionSiteLookup> tblEurailPromotionSiteLookups { get; set; }
        public virtual ICollection<tblEurailPromotionSortOrderLookup> tblEurailPromotionSortOrderLookups { get; set; }
        public virtual ICollection<tblFaq> tblFaqs { get; set; }
        public virtual ICollection<tblFeedback> tblFeedbacks { get; set; }
        public virtual ICollection<tblFooterMenuSiteLookup> tblFooterMenuSiteLookups { get; set; }
        public virtual ICollection<tblGeneralInformation_> tblGeneralInformation_ { get; set; }
        public virtual ICollection<tblJourneySiteLookup> tblJourneySiteLookups { get; set; }
        public virtual ICollection<tblManageAdsenceLink> tblManageAdsenceLinks { get; set; }
        public virtual ICollection<tblMandatorySiteLookup> tblMandatorySiteLookups { get; set; }
        public virtual ICollection<tblNewsLetterUser> tblNewsLetterUsers { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual ICollection<tblP2PDeliveryChargesMst> tblP2PDeliveryChargesMst { get; set; }
        public virtual ICollection<tblP2PSetting> tblP2PSetting { get; set; }
        public virtual ICollection<tblPaymentCallCost> tblPaymentCallCosts { get; set; }
        public virtual ICollection<tblPrivacyPolicy> tblPrivacyPolicies { get; set; }
        public virtual ICollection<tblProductDiscountSiteLookup> tblProductDiscountSiteLookups { get; set; }
        public virtual ICollection<tblProductSiteLookUp> tblProductSiteLookUps { get; set; }
        public virtual ICollection<tblQubitScript> tblQubitScripts { get; set; }
        public virtual ICollection<tblRailPassSec1> tblRailPassSec1 { get; set; }
        public virtual ICollection<tblRailPassSec2> tblRailPassSec2 { get; set; }
        public virtual ICollection<tblRailPassSec3> tblRailPassSec3 { get; set; }
        public virtual ICollection<tblSecuritySiteLookup> tblSecuritySiteLookups { get; set; }
        public virtual ICollection<tblSeoStieTag> tblSeoStieTags { get; set; }
        public virtual ICollection<tblShipping> tblShippings { get; set; }
        public virtual ICollection<tblSiteCurrencyLookUp> tblSiteCurrencyLookUps { get; set; }
        public virtual ICollection<tblSiteTheme> tblSiteThemes { get; set; }
        public virtual ICollection<tblSocialMediaScript> tblSocialMediaScripts { get; set; }
        public virtual ICollection<tblSpecialTrainSiteLookUp> tblSpecialTrainSiteLookUps { get; set; }
        public virtual ICollection<tblTermsandConditionSiteLookup> tblTermsandConditionSiteLookups { get; set; }
        public virtual ICollection<tblTextContent> tblTextContents { get; set; }
        public virtual ICollection<tblTicketProtection> tblTicketProtections { get; set; }
        public virtual ICollection<tblTrainDetailSiteLookup> tblTrainDetailSiteLookups { get; set; }
        public virtual ICollection<tblUserLogin> tblUserLogins { get; set; }
        public virtual ICollection<tblWebMenuSiteLookup> tblWebMenuSiteLookups { get; set; }
    }
}
