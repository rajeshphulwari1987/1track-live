﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace Business
{
    public class ManageHashTag
    {
        public readonly db_1TrackEntities db = new db_1TrackEntities();

        #region HashTag Method

        public bool AddHashTag(Hashtag objHashtag)
        {
            try
            {
                db.Hashtags.AddObject(objHashtag);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteHashTag(int Id)
        {
            try
            {
                var data = db.Hashtags.FirstOrDefault(x => x.Id == Id);
                if (data != null)
                {
                    db.DeleteObject(data);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateHashTag(Hashtag objHashtag)
        {
            try
            {
                var data = db.Hashtags.FirstOrDefault(x => x.Id == objHashtag.Id);
                if (data != null)
                {
                    data.Title = objHashtag.Title;
                    data.IsActive = objHashtag.IsActive;
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Hashtag> GetHashtagList()
        {
            try
            {
                return db.Hashtags.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtag GetHashtagById(int Id)
        {
            try
            {
                return db.Hashtags.FirstOrDefault(x => x.Id == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveHashTag(int ID)
        {
            try
            {
                var rec = db.Hashtags.FirstOrDefault(x => x.Id == ID);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UploadHashTagFromExcel(string sPath)
        {
            try
            {
                string ConStringOLEDB = string.Empty;
                ConStringOLEDB = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + sPath + "';Extended Properties=Excel 12.0;";
                OleDbConnection MyConnection = null;

                object missing = System.Reflection.Missing.Value;
                MyConnection = new OleDbConnection(ConStringOLEDB);
                MyConnection.Open();

                DataTable objDT = new DataTable();
                objDT = MyConnection.GetSchema("Tables");

                for (int j = 0; j < objDT.Rows.Count; j++)
                {
                    string strWorksheetName = objDT.Rows[j]["TABLE_NAME"].ToString();
                    OleDbDataAdapter objDataAdpter = null;
                    try
                    {
                        objDataAdpter = new OleDbDataAdapter("select * from [" + strWorksheetName + "]  ", MyConnection);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    DataSet objDS = new DataSet();
                    objDataAdpter.Fill(objDS);
                    MyConnection.Close();

                    for (int i = 0; i < objDS.Tables.Count; i++)
                    {
                        foreach (DataRow item in objDS.Tables[i].Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item[1])))
                            {
                                string Isactive = item[1].ToString().ToLower() == "yes" ? "TRUE" : "FALSE";
                                Hashtag objHashtag = new Hashtag
                                {
                                    Title = Convert.ToString(item[0]),
                                    IsActive = Convert.ToBoolean(Isactive)
                                };
                                db.Hashtags.AddObject(objHashtag);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region HashTagMapping Method
        public List<Hashtag> GetHashtagsList()
        {
            try
            {
                return db.Hashtags.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HasTagProductList> GetProductList(Guid SiteId)
        {
            var objProduct = (from tp in db.tblProducts
                              join tpn in db.tblProductNames on tp.ID equals tpn.ProductID
                              join tpclp in db.tblProductCategoriesLookUps on tpn.ProductID equals tpclp.ProductID
                              join tpcolp in db.tblProductCountryLookUps on tpn.ProductID equals tpcolp.ProductID
                              join tcn in db.tblCategoriesNames on tpclp.CategoryID equals tcn.CategoryID
                              //join tpslp in db.tblProductSiteLookUps on tp.ID equals tpslp.ProductID 
                              where tp.IsActive == true
                              //&& tpslp.SiteID == SiteId
                              select new HasTagProductList
                              {
                                  ProductId = tp.ID,
                                  ProductName = tpn.Name,
                                  CategoryName = tcn.Name,
                                  CategoryID = tpclp.CategoryID,
                                  CountryID = tpcolp.CountryID,
                                  CountryStartCode = tp.CountryStartCode
                              }).Union(from tp in db.tblProducts //union is for passes other then global <5000
                                       join tpn in db.tblProductNames on tp.ID equals tpn.ProductID
                                       join tpclp in db.tblProductCategoriesLookUps on tpn.ProductID equals tpclp.ProductID
                                       //join tpcolp in db.tblProductCountryLookUps on tpn.ProductID equals tpcolp.ProductID
                                       join tcn in db.tblCategoriesNames on tpclp.CategoryID equals tcn.CategoryID
                                       //join tpslp in db.tblProductSiteLookUps on tp.ID equals tpslp.ProductID 
                                       where tp.IsActive == true
                                       //&& tpslp.SiteID == SiteId
                                       select new HasTagProductList
                                       {
                                           ProductId = tp.ID,
                                           ProductName = tpn.Name,
                                           CategoryName = tcn.Name,
                                           CategoryID = tpclp.CategoryID,
                                           CountryID = Guid.Empty,
                                           CountryStartCode = tp.CountryStartCode
                                       });
            return objProduct.Distinct().ToList();
        }

        public List<HasTagSpecialTrainList> GetSpecialTrainList(Guid SiteId)
        {
            var objProduct = from st in db.tblSpecialTrains
                             join stsl in db.tblSpecialTrainSiteLookUps
                             on st.ID equals stsl.SpecialTrainID
                             where st.IsActive == true && stsl.SiteID == SiteId
                             select new HasTagSpecialTrainList
                             {
                                 Id = st.ID,
                                 Name = st.Name,
                                 CountryID = st.CountryID
                             };
            return objProduct.Distinct().ToList().OrderBy(x => x.Name).ToList();
        }

        public bool AddHashtagMapping(HashtagMapping objHashtagMapping)
        {
            try
            {
                db.HashtagMappings.AddObject(objHashtagMapping);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Delete hash tag by mapping id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteHashtagMapping(int id)
        {
            try
            {
                var data = db.HashtagMappings.FirstOrDefault(x => x.Id == id);
                if (data == null) return false;
                db.DeleteObject(data);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete hashtag by hashtagId
        /// </summary>
        /// <param name="hashtagId"></param>
        /// <returns></returns>
        public bool DeleteHashtagMappingByHashId(int hashtagId)
        {
            try
            {
                db.HashtagMappings.Where(x => x.HashtagId == hashtagId).ToList().ForEach(db.HashtagMappings.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HashTagcategory> GetCategoryList(Guid SiteId)
        {
            try
            {
                var data = from tc in db.tblCategories
                           join tcn in db.tblCategoriesNames on tc.ID equals tcn.CategoryID
                           join tcsl in db.tblCategorySiteLookUps on tc.ID equals tcsl.CategoryID
                           where tcsl.SiteID == SiteId && tc.IsActive == true
                           select new { tc, tcn };
                return data.Select(x => new HashTagcategory()
                {
                    Id = x.tc.ID,
                    Name = x.tcn.Name
                }).ToList().OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex) { throw ex; }
        }
        public List<HashTagcategory> GetCountryList(Guid SiteId)
        {
            try
            {
                var data = from tc in db.tblCountriesMsts
                           join tcsl in db.tblCountrySiteLookUps on tc.CountryID equals tcsl.CountryID
                           where tcsl.SiteID == SiteId && tc.IsActive == true
                           select new { tc, tcsl };
                return data.Select(x => new HashTagcategory()
                {
                    Id = x.tc.CountryID,
                    Name = x.tc.CountryName
                }).ToList().OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
    }

    public class HasTagProductList
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }

        public Guid CategoryID { get; set; }
        public string CategoryIDs { get; set; }
        public Guid CountryID { get; set; }
        public string CountryIDs { get; set; }

        public int CountryStartCode { get; set; }
    }

    public class HasTagSpecialTrainList
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Guid CountryID { get; set; }
    }

    public class HashTagcategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
