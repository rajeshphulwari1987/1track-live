//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    
    public partial class GetUserOrderList
    {
        public Nullable<int> TOTALRECORD { get; set; }
        public Nullable<long> ROWNO { get; set; }
        public string PRODUCTTYPE { get; set; }
        public long ORDERID { get; set; }
        public string TRAVELLERNAME { get; set; }
        public string ORDERSTATUS { get; set; }
        public Nullable<System.DateTime> CREATEDON { get; set; }
        public Nullable<System.DateTime> DATEOFDEPART { get; set; }
    }
}
