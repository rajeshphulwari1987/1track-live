﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Xml.Linq;

namespace Business
{
    public class ManagePrintQueue
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();

        public decimal GetActualpriceByPassSaleId(List<Guid> PassSaleID)
        {
            try
            {
                decimal price = 0;
                var PassData = db.tblPassSales.Where(x => PassSaleID.Contains(x.ID)).Select(x => x.ProductXML).ToList();
                foreach (var ProductXML in PassData)
                {
                    if (ProductXML.Contains("Cost"))
                    {
                        XDocument doc = XDocument.Parse(ProductXML);
                        price += Convert.ToDecimal(doc.Descendants("Product").Select(s => new { Cost = s.Element("Cost").Value }).FirstOrDefault().Cost);
                    }
                }
                return price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PrintQueueAgentList> GetPrintQueueAgentListByQueueId(Guid SiteId, Guid PrintQueueId)
        {
            try
            {
                List<Guid> AgentLists = db.tblAdminUserLookupSites.Where(x => x.SiteID == SiteId).Select(x => x.AdminUserID.Value).ToList();
                return db.tblAdminUserStockQueueLookups.Where(x => AgentLists.Contains(x.AdminUserID) && x.StockQueueID == PrintQueueId).ToList().Select(x =>
                     new PrintQueueAgentList
                     {
                         Id = x.tblAdminUser.ID,
                         Name = (string.IsNullOrEmpty(x.tblAdminUser.Forename) ? x.tblAdminUser.Surname : (x.tblAdminUser.Forename + (string.IsNullOrEmpty(x.tblAdminUser.Surname) ? string.Empty : " " + x.tblAdminUser.Surname))) + "(" + x.tblAdminUser.UserName + ")"
                     }).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //INTERRAIL
        public tblSite SiteGetById(Guid SiteId)
        {
            try
            {
                return db.tblSites.FirstOrDefault(x => x.ID == SiteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePrintingURLInterrail(long stockno, string URL)
        {
            try
            {
                var data = db.tblStockAllocationUsageInterrails.FirstOrDefault(x => x.StockNumber == stockno);
                if (data != null)
                {
                    data.URL = URL;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateInterrailStockNo(Int64 StockNo, Int32 status, Guid AdminUserID)
        {
            try
            {
                int? result = db.spUpdateStatusInterrailStockNo(StockNo, status, AdminUserID).FirstOrDefault().Value;//1.printing 2.printed 3.Destroy 4.Used 5.Void
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetProductsuppliercategory(Guid ID)
        {
            try
            {
                var data = db.tblProducts.FirstOrDefault(t => t.ID == ID);
                if (data != null)
                {
                    var SupplierData = db.tblSuppliers.FirstOrDefault(x => x.Id == data.SupplierId);
                    if (SupplierData != null)
                        return SupplierData.Category;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertStockInUsageInterrail(Guid catID, Guid stkQueueID, Guid pqItemID, Int32 stkNumber, string voucherXML, Guid createdBy, bool Saver, string OtherCase = "")
        {
            try
            {
                //db.DeletestockallocatinousagePQitem(pqItemID);
                if (Saver)
                {
                    Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                    db.spInsertStockUsageInterrail(catID, pqItemID, stkQueueID, stkNumber, voucherXML, createdBy, OtherCase);
                }
                else if (!db.tblStockAllocationUsageBritrails.Any(ty => ty.StockNumber == stkNumber))
                {
                    Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                    db.spInsertStockUsageInterrail(catID, pqItemID, stkQueueID, stkNumber, voucherXML, createdBy, OtherCase);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<spGetPrintQueueItemsInJobVoucherXMLInterrail_Result> GetPrintQueueItemsInJobListInterrail(Guid adminid, string pqItemID, Guid QueueID, long stockfrom, long stockto)
        {
            try
            {
                return db.spGetPrintQueueItemsInJobVoucherXMLInterrail(adminid, pqItemID, QueueID, stockfrom, stockto).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<spGetInJobedPrintQueueItemsInterrail_Result> GetPrintQueueInJobListInterrail(Guid QueueID, string CatIds, Guid SiteID, Guid AdminUserId)
        {
            try
            {
                Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                return db.spGetInJobedPrintQueueItemsInterrail(QueueID, CatIds, SiteID, langId, AdminUserId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public Guid AddEditStockInterrail(tblStockInterrail queue)
        {
            try
            {
                var branch = db.tblBranches.FirstOrDefault(x => x.ID == queue.BranchID);
                queue.ParentBranchID = (Guid)branch.ParentBranchID;

                var stbranch = db.tblStockInterrails.FirstOrDefault(x => x.StockNoTo >= queue.StockNoFrom && x.StockNoFrom <= queue.StockNoFrom && x.BranchID == queue.ParentBranchID);
                if (stbranch != null)
                    queue.TreeParentBranchID = stbranch.TreeID;
                else
                {
                    var IDdata = db.tblStockInterrails.FirstOrDefault(x => x.ID == queue.ID);
                    if (IDdata != null)
                        queue.TreeParentBranchID = IDdata.TreeParentBranchID;
                }

                Boolean data = db.InsertNewStockInterrail((long?)queue.StockNoFrom, (long?)queue.StockNoTo, (Guid?)queue.BranchID, (Guid?)queue.ParentBranchID, (Guid?)queue.CreatedBy, (Guid?)queue.StatusID, (Guid?)queue.ID, (long?)queue.TreeID, (long?)queue.TreeParentBranchID, queue.IPAddress).FirstOrDefault().Value;
                if (!data)
                    throw new Exception("Stock not avalable this range.");
                return queue.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetStockInterrailToDelivered(Guid userID, Guid branchId, Int32 stkFrom, Int32 stkTo)
        {
            try
            {
                return db.UpdateInterrailStatusOfHeldStock(branchId, userID, stkFrom, stkTo).FirstOrDefault().Value > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteStockNumberInterrail(Guid id, int level, string stockRangeFrom, string stockRangeTo)
        {
            try
            {
                return db.RemoveStockInterrail(id, level, stockRangeFrom, stockRangeTo).FirstOrDefault().Value > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblStockInterrail GetStockInterrailForEdit(Guid id)
        {
            return db.tblStockInterrails.FirstOrDefault(x => x.ID == id);
        }

        public List<stockdata> GetStatusOfStockNumberInterrailByParentBranchID(Guid parentBranchID)
        {
            try
            {
                var list = db.GetStatusOfStockNumberInterrailByParentBranch(parentBranchID).ToList();
                List<stockdata> stock = new List<stockdata>();
                var spdata = list.FirstOrDefault();
                if (spdata != null)
                {
                    decimal start = spdata.StockNo;
                    decimal rowno = 1;
                    string match = spdata.Status;
                    foreach (var data in list)
                    {
                        stockdata obj = new stockdata();
                        if (start == data.StockNo && match == data.Status)
                        {
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.BranchID = data.BranchID;
                            obj.ParentBranchID = data.ParentBranchID;
                            obj.Status = data.Status;
                            obj.StatusID = data.StatusID;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        else
                        {
                            rowno++;
                            match = data.Status;
                            start = data.StockNo;
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.BranchID = data.BranchID;
                            obj.ParentBranchID = data.ParentBranchID;
                            obj.Status = data.Status;
                            obj.StatusID = data.StatusID;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        start++;
                        stock.Add(obj);
                    }
                    stock = stock.GroupBy(t => new { t.row, t.Allocated_From, t.AllocatedTo, t.BranchID, t.ParentBranchID, t.Status, t.StatusID }).Select(grp => new stockdata
                    {
                        row = grp.Key.row,
                        StockFrom = grp.Min(t => t.StockNo),
                        StockTo = grp.Max(t => t.StockNo),
                        Allocated_From = grp.Key.Allocated_From,
                        AllocatedTo = grp.Key.AllocatedTo,
                        BranchID = grp.Key.BranchID,
                        ParentBranchID = grp.Key.ParentBranchID,
                        StatusID = grp.Key.StatusID,
                        Status = grp.Key.Status
                    }).ToList();
                }
                return stock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public StockOfficeDetail GetInterrailStockBranchInfo(Guid id)
        {
            var rec = db.tblStockBritrails.FirstOrDefault(x => x.ParentBranchID == id);
            if (rec == null)
                rec = db.tblStockBritrails.FirstOrDefault(x => x.BranchID == id);
            if (rec != null)
                return new StockOfficeDetail
                {
                    OfficeTreeName = GetOfficeName(id),
                    CreatedBy = db.tblAdminUsers.FirstOrDefault(y => y.ID == rec.CreatedBy).UserName,
                    CreatedOn = rec.CreatedOn.ToShortDateString(),
                    OfficeName = rec.tblBranch.OfficeName,
                    StockNumberFrom = rec.StockNoFrom,
                    StockNumberTo = rec.StockNoTo,
                    IPAddress = rec.IPAddress
                };
            return null;
        }

        public List<GetAllStockNumberInterrailByBranch_Result> GetAllStockNumberInterrailByBranch(Guid branchID)
        {
            try
            {
                return db.GetAllStockNumberInterrailByBranch(branchID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 UpdateInterrailStockNoReuse(long StockNo)
        {
            try
            {
                return db.spReusedStockNoInterrail(StockNo).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //END INTERRAIL

        public List<GetPrintQueueItemsInJobVoucherXMLBritrailFreeDayPromo> GetPrintQueueItemsInJobListBritrailFreeDayPromo(Guid adminid, string pqItemID, Guid QueueID, long stockfrom, long stockto)
        {
            try
            {
                return db.GetPrintQueueItemsInJobVoucherXMLBritrailFreeDayPromo(adminid, pqItemID, QueueID, stockfrom, stockto).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetITXFlagByPassSaleId(Guid id)
        {
            try
            {
                bool IsItxPass = false;
                var data = db.tblPassSales.FirstOrDefault(t => t.ID == id);
                if (data != null && data.ProductXML.Contains("IsItxPass"))
                {
                    XDocument doc = XDocument.Parse(data.ProductXML);
                    IsItxPass = Convert.ToBoolean(doc.Descendants("Product").Select(s => new { IsItxPass = s.Element("IsItxPass").Value }).FirstOrDefault().IsItxPass);
                }
                return IsItxPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Isvalidforbritrailfreedaypassprinting(Guid ProductId)
        {
            try
            {
                bool Result = false;
                var data = db.tblProducts.FirstOrDefault(x => x.ID == ProductId);
                if (data != null)
                {
                    DateTime From = data.BritrailPromoFrom ?? DateTime.Now.AddDays(-1);
                    DateTime To = data.BritrailPromoTo ?? DateTime.Now.AddDays(-1);
                    Result = (From <= DateTime.Now && To >= DateTime.Now);
                }
                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetIsBritRailFreeDayPromoByPassSaleId(Guid id)
        {
            try
            {
                bool IsBritRailFreeDayPromo = false;
                var data = db.tblPassSales.FirstOrDefault(t => t.ID == id);
                if (data != null && data.ProductXML.Contains("IsPromo"))
                {
                    XDocument doc = XDocument.Parse(data.ProductXML);
                    IsBritRailFreeDayPromo = Convert.ToBoolean(doc.Descendants("Product").Select(s => new { IsBritRailFreeDayPromo = s.Element("IsPromo").Value }).FirstOrDefault().IsBritRailFreeDayPromo);
                }
                return IsBritRailFreeDayPromo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /*Stock Queue*/
        public List<tblStockQueueType> GetStockQueueListType()
        {
            try
            {
                return db.tblStockQueueTypes.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetOfficeFullName> GetOfficeList()
        {
            try
            {
                return db.GetOfficeFullName().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblStockQueue GetStockQueueById(Guid id)
        {
            try
            {
                return db.tblStockQueues.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<AgentIDs> GetAgentNameByStockQueueId(Guid id)
        {
            try
            {
                return db.tblAdminUserStockQueueLookups.Where(x => x.StockQueueID == id).AsEnumerable().Select(ty => new AgentIDs { Id = ty.AdminUserID }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockQueueField> GetStockQueueList()
        {
            try
            {
                List<StockQueueField> list = (from stk in db.tblStockQueues
                                              group stk by new { stk.ID, stk.BranchID, stk.PrinterCode, stk.tblStockQueueType.Name, stk.IsActive } into cms

                                              select new StockQueueField
                                                  {
                                                      ID = cms.Key.ID,
                                                      BranchID = cms.Key.BranchID,
                                                      PrinterCode = cms.Key.PrinterCode,
                                                      QueueType = cms.Key.Name,
                                                      IsActive = cms.Key.IsActive
                                                  }
                                             ).AsEnumerable().Select(x => new StockQueueField
                                             {
                                                 ID = x.ID,
                                                 PrinterCode = x.PrinterCode,
                                                 QueueType = x.QueueType,
                                                 Name = GetOfficeName(x.BranchID),
                                                 IsActive = x.IsActive
                                             }).OrderBy(x => x.Name).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockQueueField> GetStockList(Guid AdminId)
        {
            try
            {

                //--Queue Type Id =>status mode is "Queue" 
                Guid qtypId = Guid.Parse("9899686a-6ec4-4b32-a813-d9287d7e03e1");
                var list = (from stk in db.tblStockQueues
                            join adminstk in db.tblAdminUserStockQueueLookups on stk.ID equals adminstk.StockQueueID
                            where stk.tblStockQueueType.ID == qtypId && stk.IsActive == true && adminstk.AdminUserID == AdminId
                            select new StockQueueField
                            {
                                ID = stk.ID,
                                BranchID = stk.BranchID,
                                PrinterCode = stk.PrinterCode,
                                AdminUserID = adminstk.AdminUserID,
                                QueueType = stk.tblStockQueueType.Name,
                                IsInterrailDefault = adminstk.InterrailDefaultPrinter,
                                IsBritrailDefault = adminstk.BritrailDefaultPrinter,
                                IsEurailDefault = adminstk.EurailDefaultPrinter,
                            }).AsEnumerable().Select(x => new StockQueueField
                            {
                                ID = x.ID,
                                AdminUserID = x.AdminUserID,
                                Name = GetOfficeName(x.BranchID) + " (" + x.PrinterCode + ")",
                                IsInterrailDefault = x.IsInterrailDefault,
                                IsBritrailDefault = x.IsBritrailDefault,
                                IsEurailDefault = x.IsEurailDefault,
                            }).Distinct().OrderBy(x => x.Name).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCategoriesName> GetCategoryName(Guid siteID)
        {
            try
            {
                var lst = from cat in db.tblCategoriesNames
                          join clp in db.tblCategorySiteLookUps
                              on cat.CategoryID equals clp.CategoryID
                          where clp.SiteID == siteID
                          select cat;

                return lst.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOfficeName(Guid id)
        {
            try
            {
                string name = string.Empty;
                var list = (from stk in db.tblBranches
                            where stk.ID == id
                            select new
                                {
                                    parentID = stk.ParentBranchID,
                                    name = stk.OfficeName,
                                }).ToList();

                foreach (var item in list)
                {
                    name = GetOfficeName((Guid)item.parentID) + " - " + item.name;
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditStockQueue(tblStockQueue queue)
        {
            try
            {
                if (queue.ID == new Guid())
                {
                    if (db.tblStockQueues.Any(x => x.BranchID == queue.BranchID))
                        throw new Exception("Print queue is alredy exist.");

                    queue.ID = Guid.NewGuid();
                    queue.BranchID = (queue.BranchID == new Guid()) ? Guid.NewGuid() : queue.BranchID;
                    db.tblStockQueues.AddObject(queue);
                }
                else
                {
                    var rec = db.tblStockQueues.FirstOrDefault(x => x.ID == queue.ID);
                    rec.BranchID = queue.BranchID;
                    rec.IsActive = queue.IsActive;
                    rec.ModifyBy = queue.CreatedBy;
                    rec.ModifyOn = queue.CreatedOn;
                    rec.PrinterCode = queue.PrinterCode;
                    rec.StockQueueTypeID = queue.StockQueueTypeID;
                }
                db.SaveChanges();
                db.InsertPrintingTemplate(queue.ID);

                return queue.ID;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Guid AddAdminUserStockQueueLookups(tblAdminUserStockQueueLookup queue)
        {
            try
            {
                if (!db.tblAdminUserStockQueueLookups.Any(x => x.StockQueueID == queue.StockQueueID && x.AdminUserID == queue.AdminUserID))
                    db.AddTotblAdminUserStockQueueLookups(new tblAdminUserStockQueueLookup
                    {
                        ID = Guid.NewGuid(),
                        AdminUserID = queue.AdminUserID,
                        StockQueueID = queue.StockQueueID,
                        InterrailDefaultPrinter = queue.InterrailDefaultPrinter,
                        BritrailDefaultPrinter = queue.BritrailDefaultPrinter,
                        EurailDefaultPrinter = queue.EurailDefaultPrinter,
                    });
                db.SaveChanges();
                return queue.ID;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool DeleteStockQueue(Guid id)
        {
            try
            {
                db.tblAdminUserStockQueueLookups.Where(x => x.StockQueueID == id).ToList().ForEach(db.tblAdminUserStockQueueLookups.DeleteObject);
                var rec = db.tblStockQueues.FirstOrDefault(x => x.ID == id);
                db.tblStockQueues.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Stock Queue is already in used.");
            }
        }

        public void ActiveInactiveStockQueue(Guid id)
        {
            try
            {
                var rec = db.tblStockQueues.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*Stock Allocation*/
        public List<GetAllPrintQueueListByAdminId_Result> GetActiveStockListByAdminId(Guid AdminID)
        {
            try
            {
                return db.GetAllPrintQueueListByAdminId(AdminID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class StockQueueList
        {
            public Guid ID { get; set; }
            public string Name { get; set; }
            public string PCode { get; set; }
        }

        public List<StockQueueField> GetParentNameOfficeList(Guid Id)
        {
            try
            {
                if (Id == Guid.Empty)
                {
                    return db.tblBranches.Where(z => z.TreeParentBranchID == null && z.IsActive == true).Select(x => new StockQueueField
                    {
                        ID = x.ID,
                        IsActive = (bool)x.IsActive,
                        Name = x.OfficeName
                    }).OrderBy(x => x.Name).ToList();
                }
                else
                {
                    return db.tblBranches.Where(x => x.ParentBranchID == Id && x.IsActive == true).Select(x => new StockQueueField
                    {
                        ID = x.ID,
                        IsActive = (bool)x.IsActive,
                        Name = x.OfficeName
                    }).OrderBy(x => x.Name).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblStockAllocationStatu> GetStockAllocationStatus()
        {
            try
            {
                return db.tblStockAllocationStatus.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditStockAllocation(tblStockAllocation queue)
        {
            try
            {
                var branch = db.tblBranches.FirstOrDefault(x => x.ID == queue.BranchID);
                queue.ParentBranchID = (Guid)branch.ParentBranchID;

                var stbranch = db.tblStockAllocations.FirstOrDefault(x => x.StockNoTo >= queue.StockNoFrom && x.StockNoFrom <= queue.StockNoFrom && x.BranchID == queue.ParentBranchID);
                if (stbranch != null)
                    queue.TreeParentBranchID = stbranch.TreeID;

                Boolean data = db.InsertNewStock((long?)queue.StockNoFrom, (long?)queue.StockNoTo, (Guid?)queue.BranchID, (Guid?)queue.ParentBranchID, (Guid?)queue.CreatedBy, (Guid?)queue.StatusID, (Guid?)queue.ID, (long?)queue.TreeID, (long?)queue.TreeParentBranchID, queue.IPAddress).FirstOrDefault().Value;
                if (!data)
                    throw new Exception("Stock not avalable this range.");
                return queue.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditStockBritrail(tblStockBritrail queue)
        {
            try
            {
                var branch = db.tblBranches.FirstOrDefault(x => x.ID == queue.BranchID);
                queue.ParentBranchID = (Guid)branch.ParentBranchID;

                var stbranch = db.tblStockBritrails.FirstOrDefault(x => x.StockNoTo >= queue.StockNoFrom && x.StockNoFrom <= queue.StockNoFrom && x.BranchID == queue.ParentBranchID);
                if (stbranch != null)
                    queue.TreeParentBranchID = stbranch.TreeID;

                Boolean data = db.InsertNewStockBritrail((long?)queue.StockNoFrom, (long?)queue.StockNoTo, (Guid?)queue.BranchID, (Guid?)queue.ParentBranchID, (Guid?)queue.CreatedBy, (Guid?)queue.StatusID, (Guid?)queue.ID, (long?)queue.TreeID, (long?)queue.TreeParentBranchID, queue.IPAddress).FirstOrDefault().Value;
                if (!data)
                    throw new Exception("Stock not avalable this range.");
                return queue.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblStockAllocation GetStockAllocationForEdit(Guid id)
        {
            return db.tblStockAllocations.FirstOrDefault(x => x.ID == id);
        }

        public tblStockBritrail GetStockBritrailForEdit(Guid id)
        {
            return db.tblStockBritrails.FirstOrDefault(x => x.ID == id);
        }

        public tblStockAllocation GetStockAllocationByBranchID(Guid id)
        {
            return db.tblStockAllocations.FirstOrDefault(x => x.ID == id);
        }

        public tblStockBritrail GetStockBritrailByBranchID(Guid id)
        {
            return db.tblStockBritrails.FirstOrDefault(x => x.ID == id);
        }

        public string GetOfficeStockNumber(Guid id)
        {
            try
            {
                string name = string.Empty;
                var list = (from stk in db.tblBranches
                            where stk.ID == id
                            select new
                            {
                                parentID = stk.ParentBranchID,
                                name = stk.OfficeName
                            }).ToList();

                foreach (var item in list)
                {
                    name = GetOfficeName((Guid)item.parentID) + " - " + item.name;
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteStockNumber(Guid id, int level, string stockRangeFrom, string stockRangeTo)
        {
            try
            {
                int res = db.RemoveStockAlloction(id, level, stockRangeFrom, stockRangeTo).FirstOrDefault().Value;
                if (res > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteStockNumberBritrail(Guid id, int level, string stockRangeFrom, string stockRangeTo)
        {
            try
            {
                int res = db.RemoveStockBritrail(id, level, stockRangeFrom, stockRangeTo).FirstOrDefault().Value;
                if (res > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /*Stock Allocation Details*/
        public StockOfficeDetail GetStockBranchInfo(Guid id)
        {
            var rec = db.tblStockAllocations.FirstOrDefault(x => x.ParentBranchID == id);
            if (rec == null)
                rec = db.tblStockAllocations.FirstOrDefault(x => x.BranchID == id);
            if (rec != null)
                return new StockOfficeDetail
                {
                    OfficeTreeName = GetOfficeName(id),
                    CreatedBy = db.tblAdminUsers.FirstOrDefault(y => y.ID == rec.CreatedBy).UserName,
                    CreatedOn = rec.CreatedOn.ToShortDateString(),
                    OfficeName = rec.tblBranch.OfficeName,
                    StockNumberFrom = rec.StockNoFrom,
                    StockNumberTo = rec.StockNoTo,
                    IPAddress = rec.IPAddress
                };
            return null;
        }

        public StockOfficeDetail GetBritrailStockBranchInfo(Guid id)
        {
            var rec = db.tblStockBritrails.FirstOrDefault(x => x.ParentBranchID == id);
            if (rec == null)
                rec = db.tblStockBritrails.FirstOrDefault(x => x.BranchID == id);
            if (rec != null)
                return new StockOfficeDetail
                {
                    OfficeTreeName = GetOfficeName(id),
                    CreatedBy = db.tblAdminUsers.FirstOrDefault(y => y.ID == rec.CreatedBy).UserName,
                    CreatedOn = rec.CreatedOn.ToShortDateString(),
                    OfficeName = rec.tblBranch.OfficeName,
                    StockNumberFrom = rec.StockNoFrom,
                    StockNumberTo = rec.StockNoTo,
                    IPAddress = rec.IPAddress
                };
            return null;
        }

        public List<StockDetails> GetStockDetails(Guid branchId)
        {
            if (branchId == new Guid())
                return db.tblStockAllocations.Where(x => x.TreeParentBranchID == null).Select(x => new StockDetails
                {
                    ParentBranchID = x.ParentBranchID == null ? new Guid() : x.ParentBranchID,
                    AllocatedFrom = db.tblBranches.FirstOrDefault(y => y.ID == x.ParentBranchID).OfficeName,

                    BranchID = x.BranchID,
                    AllocatedTo = x.tblBranch.OfficeName,

                    StockNumberFrom = x.StockNoFrom,
                    StockNumberTo = x.StockNoTo,
                    Status = x.tblStockAllocationStatu.Name,
                    IsParentExist = true
                }).OrderBy(x => x.StockNumberFrom).ToList();
            else if (db.tblStockAllocations.Any(x => x.ParentBranchID == branchId))
                return db.tblStockAllocations.Where(x => x.ParentBranchID == branchId).Select(x => new StockDetails
                {
                    ParentBranchID = x.ParentBranchID == null ? new Guid() : x.ParentBranchID,
                    AllocatedFrom = db.tblBranches.FirstOrDefault(y => y.ID == x.ParentBranchID).OfficeName,

                    BranchID = x.BranchID,
                    AllocatedTo = x.tblBranch.OfficeName,

                    StockNumberFrom = x.StockNoFrom,
                    StockNumberTo = x.StockNoTo,
                    Status = x.tblStockAllocationStatu.Name,
                    IsParentExist = true
                }).OrderBy(x => x.StockNumberFrom).ToList();
            else
            {
                //var rec = db.tblStockAllocations.FirstOrDefault(x => x.BranchID == branchId);
                var recList = (from stUsage in db.tblStockAllocationUsages
                               join stQ in db.tblStockQueues on stUsage.StockAllocationID equals stQ.ID
                               join pq in db.tblPrintQueueItems on stUsage.PrintQueueItemID equals pq.ID
                               join status in db.tblPrintQueueStatus on pq.Status equals status.ID
                               where stQ.BranchID == branchId
                               select new { stUsage, stQ, status }).ToList();

                var listStockDetails = new List<StockDetails>();
                foreach (var item in recList)
                //for (Int32 i = Convert.ToInt32(rec.StockNoFrom); i <= Convert.ToInt32(rec.StockNoTo); i++)
                {
                    listStockDetails.Add(new StockDetails
                      {
                          ID = item.stUsage.ID,
                          StockNumber = Convert.ToInt32(item.stUsage.StockNumber),
                          DatePrinted = item.stUsage.CreatedOn != null ? item.stUsage.CreatedOn.ToString("dd, MMM yyyy") : "",
                          PrintedBy = adminUserNameById(item.stUsage.CreatedBy != null ? item.stUsage.CreatedBy : Guid.Empty),
                          Status = item.status.Name
                      });
                }
                return listStockDetails.OrderBy(x => x.StockNumber).ToList();
            }
        }

        public string adminUserNameById(Guid ID)
        {
            try
            {
                return ID != Guid.Empty ? db.tblAdminUsers.FirstOrDefault(ty => ty.ID == ID).UserName : "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<stockdata> GetStatusOfStockNumberByParentBranchID(Guid parentBranchID)
        {
            try
            {
                var list = db.GetStatusOfStockNumberByParentBranch(parentBranchID).ToList();
                List<stockdata> stock = new List<stockdata>();
                var spdata = list.FirstOrDefault();
                if (spdata != null)
                {
                    decimal start = spdata.StockNo;
                    decimal rowno = 1;
                    string match = spdata.Status;
                    foreach (var data in list)
                    {
                        stockdata obj = new stockdata();
                        if (start == data.StockNo && match == data.Status)
                        {
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.BranchID = data.BranchID;
                            obj.ParentBranchID = data.ParentBranchID;
                            obj.Status = data.Status;
                            obj.StatusID = data.StatusID;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        else
                        {
                            rowno++;
                            match = data.Status;
                            start = data.StockNo;
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.BranchID = data.BranchID;
                            obj.ParentBranchID = data.ParentBranchID;
                            obj.Status = data.Status;
                            obj.StatusID = data.StatusID;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        start++;
                        stock.Add(obj);
                    }
                    stock = stock.GroupBy(t => new { t.row, t.Allocated_From, t.AllocatedTo, t.BranchID, t.ParentBranchID, t.Status, t.StatusID }).Select(grp => new stockdata
                    {
                        row = grp.Key.row,
                        StockFrom = grp.Min(t => t.StockNo),
                        StockTo = grp.Max(t => t.StockNo),
                        Allocated_From = grp.Key.Allocated_From,
                        AllocatedTo = grp.Key.AllocatedTo,
                        BranchID = grp.Key.BranchID,
                        ParentBranchID = grp.Key.ParentBranchID,
                        StatusID = grp.Key.StatusID,
                        Status = grp.Key.Status
                    }).ToList();
                }
                return stock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<stockdata> GetPendingDeliveryStatusOfStockNumber()
        {
            try
            {
                var list = db.GetPendingDeliveryStatusOfStockNumber().ToList();
                List<stockdata> stock = new List<stockdata>();
                var spdata = list.FirstOrDefault();
                if (spdata != null)
                {
                    decimal start = spdata.StockNo;
                    decimal rowno = 1;
                    string match = spdata.Status;
                    foreach (var data in list)
                    {
                        stockdata obj = new stockdata();
                        if (start == data.StockNo && match == data.Status)
                        {
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.Status = data.Status;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        else
                        {
                            rowno++;
                            match = data.Status;
                            start = data.StockNo;
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.Status = data.Status;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        start++;
                        stock.Add(obj);
                    }
                    stock = stock.GroupBy(t => new { t.row, t.Allocated_From, t.AllocatedTo, t.Status }).Select(grp => new stockdata
                    {
                        row = grp.Key.row,
                        StockFrom = grp.Min(t => t.StockNo),
                        StockTo = grp.Max(t => t.StockNo),
                        Allocated_From = grp.Key.Allocated_From,
                        AllocatedTo = grp.Key.AllocatedTo,
                        Status = grp.Key.Status
                    }).ToList();
                }
                return stock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<stockdata> GetStatusOfStockNumberBritrailByParentBranchID(Guid parentBranchID)
        {
            try
            {
                var list = db.GetStatusOfStockNumberBritrailByParentBranch(parentBranchID).ToList();
                List<stockdata> stock = new List<stockdata>();
                var spdata = list.FirstOrDefault();
                if (spdata != null)
                {
                    decimal start = spdata.StockNo;
                    decimal rowno = 1;
                    string match = spdata.Status;
                    foreach (var data in list)
                    {
                        stockdata obj = new stockdata();
                        if (start == data.StockNo && match == data.Status)
                        {
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.BranchID = data.BranchID;
                            obj.ParentBranchID = data.ParentBranchID;
                            obj.Status = data.Status;
                            obj.StatusID = data.StatusID;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        else
                        {
                            rowno++;
                            match = data.Status;
                            start = data.StockNo;
                            obj.Allocated_From = data.Allocated_From;
                            obj.AllocatedTo = data.AllocatedTo;
                            obj.BranchID = data.BranchID;
                            obj.ParentBranchID = data.ParentBranchID;
                            obj.Status = data.Status;
                            obj.StatusID = data.StatusID;
                            obj.StockNo = data.StockNo;
                            obj.row = rowno;
                        }
                        start++;
                        stock.Add(obj);
                    }
                    stock = stock.GroupBy(t => new { t.row, t.Allocated_From, t.AllocatedTo, t.BranchID, t.ParentBranchID, t.Status, t.StatusID }).Select(grp => new stockdata
                    {
                        row = grp.Key.row,
                        StockFrom = grp.Min(t => t.StockNo),
                        StockTo = grp.Max(t => t.StockNo),
                        Allocated_From = grp.Key.Allocated_From,
                        AllocatedTo = grp.Key.AllocatedTo,
                        BranchID = grp.Key.BranchID,
                        ParentBranchID = grp.Key.ParentBranchID,
                        StatusID = grp.Key.StatusID,
                        Status = grp.Key.Status
                    }).ToList();
                }
                return stock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetAllStockNumberByBranch> GetAllStockNumberByBranch(Guid branchID)
        {
            try
            {
                return db.GetAllStockNumberByBranch(branchID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetAllStockNumberBritrailByBranch> GetAllStockNumberBritrailByBranch(Guid branchID)
        {
            try
            {
                return db.GetAllStockNumberBritrailByBranch(branchID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetStockToDelivered(Guid userID, Guid branchId, Int32 stkFrom, Int32 stkTo)
        {
            try
            {
                var XX = db.UpdateStatusOfHeldStock(branchId, userID, stkFrom, stkTo).FirstOrDefault();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetStockBritrailToDelivered(Guid userID, Guid branchId, Int32 stkFrom, Int32 stkTo)
        {
            try
            {
                return db.UpdateBritrailStatusOfHeldStock(branchId, userID, stkFrom, stkTo).FirstOrDefault().Value > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /*Order Details Stock to queue*/
        public bool updateOrderAgentIdForPublicSite(long OrderID, Guid AgentID)
        {
            try
            {
                var list = db.tblOrders.FirstOrDefault(t => t.OrderID == OrderID);
                if (list != null)
                {
                    list.AgentID = AgentID;
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUpdatePrintQ(int OrderId, Guid PassSaleId, Guid SiteId)
        {
            try
            {
                if (!db.tblPrintQueues.Any(x => x.OrderID == OrderId && x.PassSaleID == PassSaleId))
                {
                    var prnQ = new tblPrintQueue
                        {
                            ID = Guid.NewGuid(),
                            SiteID = SiteId,
                            PassSaleID = PassSaleId,
                            AdminUserID = AgentuserInfo.UserID,
                            OrderID = OrderId,
                            DateTimeStamp = DateTime.Now
                        };
                    db.AddTotblPrintQueues(prnQ);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblPassSale> GetTPSQueueData(int OrderId)
        {
            try
            {
                return (from tps in db.tblPassSales
                        join tc in db.tblCategories on tps.CategoryID equals tc.ID
                        where tps.OrderID == OrderId && (tc.IsPrintQueueEnable || tc.IsBritRailPass || tc.IsInterRailPass)
                        select tps).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddPrintQItems(tblPrintQueueItem objPQ)
        {
            try
            {
                Guid Destroy = Guid.Parse("7100B83E-07ED-418B-97E6-E5C6B746DA3B");
                var data = db.tblPrintQueueItems.FirstOrDefault(t => t.OrderID == objPQ.OrderID && t.PassSaleID == objPQ.PassSaleID && t.QueueID == Guid.Empty);
                if (data != null)
                    data.QueueID = objPQ.QueueID;
                else
                    if (!db.tblPrintQueueItems.Any(t => t.OrderID == objPQ.OrderID && t.PassSaleID == objPQ.PassSaleID && t.Status != Destroy))
                        db.AddTotblPrintQueueItems(objPQ);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateSaverTravellerXML(int stockno, string passsaleids)
        {
            try
            {
                db.UpdateSaverTravellerXML(stockno, passsaleids);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*Printing/PrintingOrders*/
        public List<GetRCT2Template> GetRCT2Template(Guid branchId, int PassType)
        {
            try
            {
                return db.GetRCT2Template(branchId, PassType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeletePrintQItem(Guid id, long OrderId)
        {
            try
            {
                bool isnotSaver = true;
                var list = (from a in db.tblPassSales
                            join b in db.tblTravelerMsts on a.TravellerID equals b.ID
                            join c in db.tblPrintQueueItems on a.ID equals c.PassSaleID
                            where a.OrderID == OrderId
                            select new { c.ID, a.Price, b.EurailCode }).ToList();
                if (list != null && list.Count() > 0)
                {
                    var slist = list.Where(ty => ty.EurailCode == 51 || ty.EurailCode == 53 || ty.EurailCode == 74 || ty.EurailCode == 75).ToList();
                    if (slist != null)
                    {
                        var sumlist = slist.Where(ty => ty.ID == id);
                        if (sumlist != null && sumlist.Count() > 0)
                        {
                            isnotSaver = false;
                            foreach (var item in slist)
                            {
                                var _prnt = db.tblPrintQueueItems.FirstOrDefault(x => x.ID == item.ID);
                                db.tblPrintQueueItems.DeleteObject(_prnt);
                            }
                        }
                    }
                }
                if (isnotSaver)
                {
                    var _prnt = db.tblPrintQueueItems.FirstOrDefault(x => x.ID == id);
                    db.tblPrintQueueItems.DeleteObject(_prnt);
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdatePrintQStatus(Guid id, long OrderId)
        {
            try
            {
                Guid Queued = Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b");
                Guid Assigned = Guid.Parse("405AB5E3-51C6-49E8-B605-1655EBC74AAD");
                //Guid Void = Guid.Parse("DE05D0A6-785C-4DA0-B650-CB3CAFCB5330");
                bool isnotSaver = true;
                var list = (from a in db.tblPassSales
                            join b in db.tblTravelerMsts on a.TravellerID equals b.ID
                            join c in db.tblPrintQueueItems on a.ID equals c.PassSaleID
                            where a.OrderID == OrderId && (c.Status == Queued || c.Status == Assigned)
                            select new { c.ID, a.Price, b.EurailCode }).ToList();
                if (list != null && list.Count() > 0)
                {
                    var slist = list.Where(ty => ty.EurailCode == 51 || ty.EurailCode == 53 || ty.EurailCode == 74 || ty.EurailCode == 75).ToList();
                    if (slist != null)
                    {
                        var sumlist = slist.Where(ty => ty.ID == id);
                        if (sumlist != null && sumlist.Count() > 0)
                        {
                            isnotSaver = false;
                            foreach (var item in slist)
                            {
                                var _prnt = db.tblPrintQueueItems.FirstOrDefault(x => x.ID == item.ID);
                                if (_prnt != null)
                                {
                                    _prnt.AdminUserID = AgentuserInfo.UserID;
                                    Guid statusID = (Guid)_prnt.Status;
                                    if (statusID == Queued || statusID == Assigned)
                                        _prnt.Status = statusID == Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b") ? Guid.Parse("405AB5E3-51C6-49E8-B605-1655EBC74AAD") : Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b");
                                }

                            }
                        }
                    }
                }
                if (isnotSaver)
                {
                    var _prnt = db.tblPrintQueueItems.FirstOrDefault(x => x.ID == id);
                    if (_prnt != null)
                    {
                        _prnt.AdminUserID = AgentuserInfo.UserID;
                        Guid statusID = (Guid)_prnt.Status;
                        if (statusID == Queued || statusID == Assigned)
                            _prnt.Status = statusID == Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b") ? Guid.Parse("405AB5E3-51C6-49E8-B605-1655EBC74AAD") : Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b");
                    }
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePrintingURL(long stockno, string URL)
        {
            try
            {
                var data = db.tblStockAllocationUsages.FirstOrDefault(x => x.StockNumber == stockno);
                if (data != null)
                {
                    data.URL = URL;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdatePrintingURLBritrail(long stockno, string URL)
        {
            try
            {
                var data = db.tblStockAllocationUsageBritrails.FirstOrDefault(x => x.StockNumber == stockno);
                if (data != null)
                {
                    data.URL = URL;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdatePrintQStausFromAssigenToInJob(Guid QueueID, Guid catID, Guid SiteID)
        {
            try
            {
                db.SetPrintQueueItemsAssigendStatusToInJob(QueueID, catID, SiteID);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdatePrintQStausFromInJobToPrinting(Guid QueueID, Guid catID, Guid SiteID)
        {
            try
            {
                db.UpdatePrintQStausFromInJobToPrinting(QueueID, catID, SiteID);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int CheckBritrailPassAny(List<CategoryIds> CatID)
        {
            try
            {
                List<Guid> list = CatID.Select(t => t.Id).ToList();
                if (db.tblCategories.Any(ty => list.Contains(ty.ID) && ty.IsBritRailPass == true))
                    return 1;//Britrail
                else if (db.tblCategories.Any(ty => list.Contains(ty.ID) && ty.IsInterRailPass == true))
                    return 2;//Interrail
                return 3;//Eurial
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdatePrintQPandingStatus(Guid ID)
        {
            try
            {
                Guid Assigned = Guid.Parse("405AB5E3-51C6-49E8-B605-1655EBC74AAD");
                Guid PendingDelivery = Guid.Parse("82DE1DBB-05D2-4AC2-BE59-569DDC9BE292");
                var data = db.tblPrintQueueItems.FirstOrDefault(ty => ty.ID == ID && ty.Status == Assigned);
                if (data != null)
                    data.Status = PendingDelivery;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DateTime GetProductValiditydate(Guid ID)
        {
            try
            {
                var data = db.tblProducts.FirstOrDefault(t => t.ID == ID);
                if (data != null)
                {
                    return data.ProductValidToDate;
                }
                return DateTime.Now;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetProductPrintingNote(Guid ID)
        {
            try
            {
                var data = db.tblProducts.FirstOrDefault(t => t.ID == ID);
                if (data != null && !string.IsNullOrEmpty(data.TemplateNote))
                {
                    return data.TemplateNote;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetFreechil(string strid)
        {
            try
            {
                return db.GetAdultSaverFreeChild(strid).FirstOrDefault().Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GetPrintQueueItems_Result> GetPrintQList(Guid QueueID, Guid catID, Guid SiteID, Guid Agentid)
        {
            try
            {
                Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                // return db.GetPrintQueueItems(QueueID, catID, SiteID, langId).ToList();
                List<Guid> listCatId = new List<Guid>();
                List<Guid> listIds = new List<Guid> { catID };

                if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                {

                    listIds = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                    listCatId.AddRange(listIds);
                    if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                    {
                        listCatId = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                        listCatId.AddRange(listIds);
                    }
                }
                else
                    listCatId = listIds;

                var list = db.GetPrintQueueItems(QueueID, string.Join(",", listCatId), SiteID, langId, Agentid).ToList();

                return list.OrderBy(t => t.PrintStatus).ThenBy(t => t.OrderNo).ThenBy(t => t.shorting).ThenBy(t => t.shortingtrav).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //--Select Those Print Queue Items Which are in jobed
        public List<GetPrintQueueItemsInPrintingStatus> GetPrintQueueInPrintingList(Guid QueueID)
        {
            try
            {
                return db.GetPrintQueueItemsInPrintingStatus(QueueID).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<GetInJobedPrintQueueItems> GetPrintQueueInJobList(Guid QueueID, string CatIds, Guid SiteID, Guid AdminUserId)
        {
            try
            {
                Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                return db.GetInJobedPrintQueueItems(QueueID, CatIds, SiteID, langId, AdminUserId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<GetInJobedPrintQueueItemsBritrail> GetPrintQueueInJobListBritrail(Guid QueueID, string CatIds, Guid SiteID, Guid AdminUserId)
        {
            try
            {
                Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                return db.GetInJobedPrintQueueItemsBritrail(QueueID, CatIds, SiteID, langId, AdminUserId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public tblPrintQueueItem getPQdata(Guid PQID)
        {
            try
            {
                var data = db.tblPrintQueueItems.FirstOrDefault(ty => ty.ID == PQID);
                return data != null ? data : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Insertqueueitems(Guid PrintQueueIdNew, tblPrintQueueItem PQData)
        {
            try
            {
                db_1TrackEntities db = new db_1TrackEntities();
                tblPrintQueueItem item = new tblPrintQueueItem();
                Guid Void = Guid.Parse("DE05D0A6-785C-4DA0-B650-CB3CAFCB5330");
                item.ID = PrintQueueIdNew;
                item.PassSaleID = PQData.PassSaleID;
                item.SiteID = PQData.SiteID;
                item.OrderID = PQData.OrderID;
                item.ProductID = PQData.ProductID;
                item.QueueID = PQData.QueueID;
                item.AdminUserID = PQData.AdminUserID;
                item.CategoryID = PQData.CategoryID;
                item.DateTimeStamp = DateTime.Now;
                item.Status = Void;
                db.AddTotblPrintQueueItems(item);
                db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool chkIsEuraiPromo(Guid PQId)
        {
            try
            {
                bool EuraiPromo = false;
                var data = (from tpq in db.tblPrintQueueItems
                            join tps in db.tblPassSales on tpq.PassSaleID equals tps.ID
                            join tp in db.tblProducts on tps.ProductID equals tp.ID
                            where tpq.ID == PQId
                            select new { tp.IsPromoPass }).ToList().FirstOrDefault();
                if (data != null)
                    EuraiPromo = data.IsPromoPass;
                return EuraiPromo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool chkIsFlexi(Guid PQId)
        {
            try
            {
                bool IsFlexi = false;
                var data = (from tpq in db.tblPrintQueueItems
                            join tps in db.tblPassSales on tpq.PassSaleID equals tps.ID
                            join tpvn in db.tblProductValidUpToNames on tps.ValidityID equals tpvn.ID
                            join tpvt in db.tblProductValidUpToes on tpvn.ProductValidUpToID equals tpvt.ID
                            where tpq.ID == PQId
                            select new { tpvt.IsFlexi }).ToList().FirstOrDefault();
                if (data != null)
                    IsFlexi = data.IsFlexi;
                return IsFlexi;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CountSaverTraveller(Guid Passid)
        {
            try
            {
                string Name = string.Empty;
                var data = db.tblPassSales.FirstOrDefault(t => t.ID == Passid);
                if (data != null)
                {
                    var list = (from tps in db.tblPassSales
                                join ttm in db.tblTravelerMsts on tps.TravellerID equals ttm.ID
                                where new[] { 51, 53, 74, 75 }.Contains(ttm.EurailCode) && tps.OrderID == data.OrderID && tps.ProductID == data.ProductID
                                select new { name = ttm.Name }).ToList().GroupBy(t => t).Select(t => new { Count = t.Count(), name = t.Key.name }).ToList().OrderBy(t => t.name);
                    foreach (var aa in list)
                    {

                        Name = Name + aa.Count + " " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(aa.name.ToLower().Replace("saver", string.Empty)) + " ";
                    }
                    Name = "Saver " + Name;
                }
                return Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertStockInUsage(Guid catID, Guid stkQueueID, Guid pqItemID, Int32 stkNumber, string voucherXML, Guid createdBy, bool Saver, string OtherCase = "")
        {
            try
            {
                //db.DeletestockallocatinousagePQitem(pqItemID);
                if (Saver)
                {
                    Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                    db.InsertStockUsage(catID, pqItemID, stkQueueID, stkNumber, voucherXML, createdBy, OtherCase);
                }
                else
                {
                    if (!db.tblStockAllocationUsages.Any(ty => ty.StockNumber == stkNumber))
                    {
                        Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                        db.InsertStockUsage(catID, pqItemID, stkQueueID, stkNumber, voucherXML, createdBy, OtherCase);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertStockInUsageBritrail(Guid catID, Guid stkQueueID, Guid pqItemID, Int32 stkNumber, string voucherXML, Guid createdBy, bool Saver)
        {
            try
            {
                //db.DeletestockallocatinousagePQitem(pqItemID);
                if (Saver)
                {
                    Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                    db.InsertStockUsageBritrail(catID, pqItemID, stkQueueID, stkNumber, voucherXML, createdBy);
                }
                else
                {
                    if (!db.tblStockAllocationUsageBritrails.Any(ty => ty.StockNumber == stkNumber))
                    {
                        Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                        db.InsertStockUsageBritrail(catID, pqItemID, stkQueueID, stkNumber, voucherXML, createdBy);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int32 UpdateEurailStockNoReuse(long StockNo)
        {
            try
            {
                return db.ReusedEurailStockNo(StockNo).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int32 UpdateBritrailStockNoReuse(long StockNo)
        {
            try
            {
                return db.ReusedEurailStockNoBritrail(StockNo).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteEurailStockNo(Int64 StockNo)
        {
            try
            {
                db.tblStockAllocationUsages.Where(ty => ty.StockNumber == StockNo).ToList().ForEach(db.tblStockAllocationUsages.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteBritrailStockNo(Int64 StockNo)
        {
            try
            {
                db.tblStockAllocationUsageBritrails.Where(ty => ty.StockNumber == StockNo).ToList().ForEach(db.tblStockAllocationUsageBritrails.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool PrintingStatusPrintQueueItems(Guid PQIid)
        {
            try
            {
                Guid Void = Guid.Parse("DE05D0A6-785C-4DA0-B650-CB3CAFCB5330");
                Guid Printing = Guid.Parse("1B91ACA5-A339-4608-BAF6-90D55F195D5C");
                var data = db.tblPrintQueueItems.FirstOrDefault(ty => ty.ID == PQIid && ty.Status != Void);
                if (data != null)
                {
                    data.Status = Printing;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool deleteVoidPrintQueueItems(Guid PQIid)
        {
            try
            {
                Guid Void = Guid.Parse("DE05D0A6-785C-4DA0-B650-CB3CAFCB5330");
                db.tblPrintQueueItems.Where(ty => ty.ID == PQIid && ty.Status == Void).ToList().ForEach(db.tblPrintQueueItems.DeleteObject);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateBritrailStockNo(Int64 StockNo, Int32 status, Guid AdminUserID)
        {
            try
            {
                int? result = db.UpdateStatusEurailStockNoBritrail(StockNo, status, AdminUserID).FirstOrDefault().Value;//1.printing 2.printed 3.Destroy 4.Used 5.Void
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateEurailStockNo(Int64 StockNo, Int32 status, Guid AdminUserID)
        {
            try
            {
                int? result = db.UpdateStatusEurailStockNo(StockNo, status, AdminUserID).FirstOrDefault().Value;//1.printing 2.printed 3.Destroy 4.Used 5.Void
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool InsertNewPrintQueueOrderBypasssaleid(Guid id)
        {
            try
            {
                Guid Printed = Guid.Parse("AF1D088B-03B9-4657-B231-C2787BA53425");
                Guid Queued = Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b");
                var list = db.tblPrintQueueItems.FirstOrDefault(t => t.ID == id && t.Status == Printed);
                if (list != null)
                {
                    tblPrintQueueItem obj = new tblPrintQueueItem();
                    obj.ID = Guid.NewGuid();
                    obj.PassSaleID = list.PassSaleID;
                    obj.SiteID = list.SiteID;
                    obj.OrderID = list.OrderID;
                    obj.CategoryID = list.CategoryID;
                    obj.ProductID = list.ProductID;
                    obj.Status = Queued;
                    obj.AdminUserID = list.AdminUserID;
                    obj.DateTimeStamp = list.DateTimeStamp;
                    obj.QueueID = list.QueueID;
                    db.tblPrintQueueItems.AddObject(obj);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdatePrintQConfirmedStatus(Guid id, int status)
        {
            try
            {
                db.UpdateStatusTicketPrinted(id, status);//1.printing 2.printed 3.Distroy 4.Queued 5.Void
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetProductITXFlagByProductId(Guid id)
        {
            try
            {
                bool IsItxPass = false;
                var data = db.tblPassSales.FirstOrDefault(t => t.ID == id);
                if (data != null && data.ProductXML.Contains("IsItxPass"))
                {
                    XDocument doc = XDocument.Parse(data.ProductXML);
                    IsItxPass = Convert.ToBoolean(doc.Descendants("Product").Select(s => new { IsItxPass = s.Element("IsItxPass").Value }).FirstOrDefault().IsItxPass);
                }
                return IsItxPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetCurrencySourceCode(Guid id)
        {
            try
            {
                return db.tblCurrencyMsts.FirstOrDefault(t => t.ID == id).ShortCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GetPrintQueueItemsInJobVoucherXML> GetPrintQueueItemsInJobList(Guid adminid, string pqItemID, Guid QueueID, long stockfrom, long stockto)
        {
            try
            {
                return db.GetPrintQueueItemsInJobVoucherXML(adminid, pqItemID, QueueID, stockfrom, stockto).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GetPrintQueueItemsInJobVoucherXMLBritrail> GetPrintQueueItemsInJobListBritrail(Guid adminid, string pqItemID, Guid QueueID, long stockfrom, long stockto)
        {
            try
            {
                return db.GetPrintQueueItemsInJobVoucherXMLBritrail(adminid, pqItemID, QueueID, stockfrom, stockto).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<RetrieveEurailProductXML> GetRetrieveEurailProductXML(Guid passSaleId, string PassSaleIDin)
        {
            try
            {
                return db.GetRetrieveEurailProductXML(passSaleId, PassSaleIDin).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CancelPrintProcess(Guid id, Int32 stockNo, Guid stokcQueueId)
        {
            try
            {
                return db.CancelPrintProcess(id, stockNo, stokcQueueId).FirstOrDefault().Value > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid? GetPassSaleIDByPrintQueueID(Guid pqId)
        {
            try
            {
                //Guid InJobStatus = Guid.Parse("1B91ACA5-A339-4608-BAF6-90D55F195D5C");//notuse
                Guid InJobStatus = Guid.Parse("5D3E09AB-AFDB-400D-A7C7-8DBF6F6A5F87");//injob use
                var rec = db.tblPrintQueueItems.FirstOrDefault(x => x.ID == pqId && x.Status == InJobStatus);
                return rec != null ? rec.PassSaleID : new Guid();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MovePrintQItem(Guid id, Guid newStockQueueId)
        {
            try
            {
                var _prnt = db.tblPrintQueueItems.FirstOrDefault(x => x.ID == id);
                _prnt.QueueID = newStockQueueId;
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetTravellerName(long id)
        {
            var res = (from pl in db.tblPassP2PSalelookup
                       join ot in db.tblOrderTravellers
                       on pl.OrderTravellerID equals ot.ID
                       where pl.OrderID == id
                       select new { ot.FirstName, ot.LastName }).ToList();
            return res.Aggregate("", (current, item) => current + ("- " + item.FirstName + " " + item.LastName + "<br/>"));
        }

        public class PrintQueueLst : tblPrintQueueItem
        {
            public string LeadPassenger { get; set; }
            public string PrdName { get; set; }
            public string StatusName { get; set; }
        }

        /*Printing/PrintingAssigned*/
        public GetStockNumberByStockQueueId GetRange(Guid queueID, Guid userID)
        {
            try
            {
                return db.GetStockNumberByStockQueueId(queueID, userID).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class StockQueueField
    {
        public Guid ID { get; set; }
        public Guid BranchID { get; set; }
        public Guid AdminUserID { get; set; }
        public string Name { get; set; }
        public string PrinterCode { get; set; }
        public string QueueType { get; set; }
        public bool IsActive { get; set; }
        public bool IsInterrailDefault { get; set; }
        public bool IsEurailDefault { get; set; }
        public bool IsBritrailDefault { get; set; }
    }

    public class StockAllocationField
    {
        public int TreeID { get; set; }
        public Guid ID { get; set; }
        public Guid BranchID { get; set; }
        public Guid ParentBranchID { get; set; }
        public int? TreeParentBranchID { get; set; }

        public decimal StockNoFrom { get; set; }
        public decimal StockNoTo { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
    }

    public class StockDetails
    {
        public Guid ID { get; set; }
        public Guid BranchID { get; set; }
        public decimal StockNumberFrom { get; set; }
        public decimal StockNumberTo { get; set; }
        public string AllocatedTo { get; set; }
        public string AllocatedFrom { get; set; }

        public Int32 StockNumber { get; set; }

        public string DatePrinted { get; set; }
        public string PrintedBy { get; set; }

        public Guid ParentBranchID { get; set; }
        public string Status { get; set; }
        public bool IsParentExist { get; set; }
    }

    public class CategoryIds
    {
        public Guid Id { get; set; }
    }

    public class AgentIDs
    {
        public Guid Id { get; set; }
    }
    public class PrintQueueAgentList
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
    }
    public class StockOfficeDetail
    {
        public string OfficeTreeName { get; set; }
        public string OfficeName { get; set; }
        public decimal StockNumberFrom { get; set; }
        public decimal StockNumberTo { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string IPAddress { get; set; }
    }
    public class stockdata
    {
        public decimal row { get; set; }
        public decimal StockFrom { get; set; }
        public decimal StockTo { get; set; }
        public string Allocated_From { get; set; }
        public string AllocatedTo { get; set; }
        public Guid? BranchID { get; set; }
        public Guid? ParentBranchID { get; set; }
        public string Status { get; set; }
        public Guid StatusID { get; set; }
        public decimal StockNo { get; set; }
    }
}
