﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Business
{
    public class ManageHolidays
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public Boolean AddHoliday(tblSiteHoliday obj)
        {
            try
            {
                if (_db.tblSiteHolidays.Where(b => b.SiteID == obj.SiteID && b.DateofHoliday == obj.DateofHoliday).Count() == 0)
                {
                    _db.tblSiteHolidays.AddObject(obj);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception("Holyday already available.");
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }
        public int UpdateHoliday(tblSiteHoliday obj)
        {
            try
            {
                var _txt = _db.tblSiteHolidays.FirstOrDefault(x => x.ID == obj.ID);
                if (_txt != null)
                {
                    _txt.HolidayName = obj.HolidayName;
                    _txt.DateofHoliday = obj.DateofHoliday;
                    _txt.SiteID = obj.SiteID;
                    _txt.IsActive = obj.IsActive;

                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblSiteHoliday> GetAllHolydaysBySite(Guid sid)
        {
            try
            {
                return _db.tblSiteHolidays.Where(a => a.SiteID == sid).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblSiteHoliday GetHolydayByID(long ID)
        {
            try
            {
                return _db.tblSiteHolidays.Where(a => a.ID == ID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool DeleteHolyDay(long id)
        {
            try
            {
                var rec = _db.tblSiteHolidays.FirstOrDefault(x => x.ID == id);
                _db.tblSiteHolidays.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
