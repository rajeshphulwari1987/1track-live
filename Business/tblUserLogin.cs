//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUserLogin
    {
        public tblUserLogin()
        {
            this.tblEQOEnquiries = new HashSet<tblEQOEnquiry>();
        }
    
        public System.Guid ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public System.Guid Country { get; set; }
        public System.Guid SiteId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public bool IsNewsEmail { get; set; }
        public bool IsEQOUser { get; set; }
        public bool IsContactUsUser { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifyOn { get; set; }
    
        public virtual tblCountriesMst tblCountriesMst { get; set; }
        public virtual ICollection<tblEQOEnquiry> tblEQOEnquiries { get; set; }
        public virtual tblSite tblSite { get; set; }
    }
}
