﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageCMSPage
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();



        public List<tblPagesCM> GetPageCMSList(Guid siteId)
        {
            try
            {
                var list = _db.tblPagesCMS.Where(ty => ty.SiteId == siteId).OrderBy(x => x.Name).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCMSPageMetaInfo GetPageMetaInfoCMSByPageId(Guid CMSPageID, Guid SiteId)
        {
            try
            {
                return _db.tblCMSPageMetaInfoes.FirstOrDefault(x => x.CMSPageID == CMSPageID && x.SiteID == SiteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePageMetaInfoCMS(tblCMSPageMetaInfo PageMeta)
        {
            try
            {
                if (!string.IsNullOrEmpty(PageMeta.Title))
                {
                    var data = _db.tblCMSPageMetaInfoes.FirstOrDefault(x => x.CMSPageID == PageMeta.CMSPageID && x.SiteID == PageMeta.SiteID);
                    if (data != null)
                    {
                        data.Title = PageMeta.Title;
                        data.Keywords = PageMeta.Keywords;
                        data.Description = PageMeta.Description;
                        data.ModifyBy = PageMeta.ModifyBy;
                        data.ModifyOn = PageMeta.ModifyOn;
                    }
                    else
                    {
                        _db.tblCMSPageMetaInfoes.AddObject(PageMeta);
                    }
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddPageMetaInfoCMS(tblCMSPageMetaInfo PageMeta)
        {
            //int validQaInsert = 0;
            try
            {
                if (!string.IsNullOrEmpty(PageMeta.Title))
                {
                    _db.tblCMSPageMetaInfoes.AddObject(PageMeta);
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddCMS(tblPagesCM oPage)
        {
            //int validQaInsert = 0;
            try
            {
                var Faq = new tblPagesCM
                {
                    ID = oPage.ID,
                    Title = oPage.Title,
                    Name = oPage.Name,
                    Description = oPage.Description,
                    Image = oPage.Image,
                    Keyword = oPage.Keyword,
                    CreatedOn = oPage.CreatedOn,
                    CreatedBy = oPage.CreatedBy,
                    SiteId = oPage.SiteId,
                    IsWidgetVisible = oPage.IsWidgetVisible,
                };
                var _test = _db.tblPagesCMS.FirstOrDefault(x => x.SiteId == oPage.SiteId && x.ID == oPage.ID);
                if (_test == null)
                {
                    _db.tblPagesCMS.AddObject(Faq);
                    //validQaInsert = 1;
                }
                _db.SaveChanges();
                return oPage.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateCMS(tblPagesCM oPage)
        {
            try
            {
                var _privacy = _db.tblPagesCMS.FirstOrDefault(x => x.ID == oPage.ID);
                if (_privacy != null)
                {
                    _privacy.Name = oPage.Name;
                    _privacy.Title = oPage.Title;
                    _privacy.Description = oPage.Description;
                    _privacy.Image = oPage.Image;
                    _privacy.Keyword = oPage.Keyword;
                    _privacy.ModifyBy = oPage.ModifyBy;
                    _privacy.ModifyOn = oPage.ModifyOn;
                    _privacy.SiteId = oPage.SiteId;
                    _privacy.IsWidgetVisible = oPage.IsWidgetVisible;
                }
                _db.SaveChanges();
                return oPage.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPagesCM GetCMSById(Guid id)
        {
            try
            {
                return _db.tblPagesCMS.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCMS(Guid id)
        {
            try
            {
                _db.tblPagesCMS.Where(x => x.ID == id).ToList().ForEach(_db.tblPagesCMS.DeleteObject);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactive(Guid id)
        {
            try
            {
                tblPagesCM page = _db.tblPagesCMS.FirstOrDefault(x => x.ID == id);
                page.IsWidgetVisible = !page.IsWidgetVisible;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddPageGlry(tblPagesCMSGallery pageGlry)
        {
            try
            {
                if (pageGlry.ID == new Guid())
                {
                    pageGlry.ID = Guid.NewGuid();
                    _db.AddTotblPagesCMSGalleries(pageGlry);
                }
                _db.SaveChanges();
                return pageGlry.PageID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblPagesCMSGallery> GetGalleryListByPageID(Guid id)
        {
            try
            {
                return (from tg in _db.tblPagesCMSGalleries
                        join td in _db.tblPagesCMS
                            on tg.PageID equals td.ID
                        where td.ID == id
                        select tg).OrderBy(x => x.tblPagesCM.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteGalleryImagesById(Guid id)
        {
            try
            {
                var rec = _db.tblPagesCMSGalleries.FirstOrDefault(x => x.ID == id);
                string imgPath = rec.GalleryImage;
                _db.DeleteObject(rec);
                _db.SaveChanges();
                return imgPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ImagePath> GetBannerImgByID(Guid pageID)
        {
            try
            {
                return
                    _db.tblPagesCMSGalleries.Where(x => x.PageID == pageID).Select(
                        x => new ImagePath { ImgUrl = x.GalleryImage, AltTag = x.Alt_Tag, ImageTitle = x.Image_Title }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ImagePath
        {
            public string ImgUrl { get; set; }
            public string AltTag { get; set; }
            public string ImageTitle { get; set; }
        }
    }
}
