﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class ManageFrontWebsitePage
    {
        db_1TrackEntities db = new db_1TrackEntities();
        public List<MenuFields> GetActiveParentWebNavigationList(Guid siteId)
        {
            try
            {
                var olist = db.tblWebMenus.Where(x => x.IsActive == true && x.IsTop == true && x.IsAllowTopBottom == true).OrderBy(x => x.PSortOrder).ToList();
                var result = olist.Select(x => new MenuFields
                {
                    Name = x.Name,
                    Url = x.Name,
                    GrpName = x.GroupName
                    //SubMenuList = db.tblWebMenus.Where(y => x.web.IsActive == true).OrderBy(y => x.web.PSortOrder).Select(y => new SubMenuFields
                    //  {
                    //      Name = x.web.Name,
                    //      Url = x.web.PageName,
                    //  }).AsEnumerable().Select(z => z),
                }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblSite GetSiteDetailsById(Guid siteId)
        {
            try
            {
                return db.tblSites.FirstOrDefault(x => x.ID == siteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPhoneNumber(Guid siteId)
        {
            try
            {
                return db.tblSites.FirstOrDefault(x => x.ID == siteId).PhoneNumber;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetBookingDayLimitBySiteId(Guid id)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == id);
            return rec != null ? rec.BookingDayLimit : 0;

        }
        public List<NavFields> GetFooterMenu(Guid siteId)
        {
            var olist = db.tblWebMenus.Where(x => x.IsActive == true && x.IsBottom == true).OrderBy(x => x.PSortOrder).ToList();
            var result = olist.Select(x => new NavFields
                {
                    Name = x.Name,
                    Url = x.Name.Replace(" ", "-")
                }).ToList();

            return result;
        }

        public List<NavFields> GetLegalInfoMenu()
        {
            var olist = db.tblLegalInfoMenus.Where(x => x.IsActive == true).OrderBy(x => x.PSortOrder).ToList();
            var result = olist.Select(x => new NavFields
            {
                Name = x.Name,
                Url = x.PageName
            }).ToList();

            return result;
        }

        public Guid GetSiteIdByURL(string url)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.SiteURL.ToLower().Trim() == url.ToLower().Trim());
            return rec == null ? new Guid() : rec.ID;
        }

        public bool GetSiteActiveInactiveBySiteId(Guid id)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == id);
            return rec == null ? false : (bool)rec.IsActive;
        }
        public bool IsVisibleHourOfSite(Guid ID)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == ID);
            return rec == null ? false : rec.IsAttentionVisible;
        }
        public Boolean GetSiteIsDeleted(Guid ID)
        {
            List<tblSite> lst = db.tblSites.Where(x => x.ID == ID).ToList();
            Boolean rval = false;
            if (lst.Count() > 0)
            {
                if (lst.FirstOrDefault().IsDelete != null)
                    rval = Convert.ToBoolean(lst.FirstOrDefault().IsDelete);
            }
            return rval;
        }

        public String GetSiteURLbySiteId(Guid id)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == id);
            return rec == null ? "#" : rec.SiteURL;
        }
        public tblSite GetSiteDatabySiteId(Guid id)
        {
            return db.tblSites.FirstOrDefault(x => x.ID == id);
        }
        public String GetSietAliasSiteId(Guid id)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == id);
            if (rec != null)
                return string.IsNullOrEmpty(rec.SiteAlias) ? "us" : rec.SiteAlias.Trim();
            return "us";
        }

        public bool? IsUsSite(Guid id)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == id);
            return rec != null ? rec.IsUS : false;

        }
        public bool? IsVisibleP2PWidget(Guid id)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == id);
            return rec != null ? rec.IsVisibleP2PWidget : false;

        }

        public List<CountryMenuFields> GetCountryNavigationList(Guid siteId)
        {
            List<Guid?> countryIdList = db.tblCountrySiteLookUps.Where(x => x.SiteID == siteId).Select(x => x.CountryID).ToList();

            var results = from p in db.tblCountriesMsts
                          group p.CountryName by p.CountryName into g
                          where g.Count() > 1
                          select new
                          {
                              CountryName = g.Key
                          };


            return new List<CountryMenuFields>
                {
                    new CountryMenuFields
                        {
                            Name = "Countries",
                            MenuList = db.tblContinents.AsEnumerable().AsEnumerable().Select(x => new MenuFields
                                {
                                    Name = x.Name,
                                    Url = "",
                                    SubMenuList = x.tblCountriesMsts.Where(v => v.IsActive == true && v.IsVisible == true && countryIdList.Contains(v.CountryID)).AsEnumerable().Select(y => new SubMenuFields
                                            {
                                                Id = y.CountryID,
                                                Name = y.CountryName,
                                                Url = y.CountryID.ToString().Substring(0, 4) + "-" + y.CountryName.Replace(" - ", "-").Replace("/", "-").Replace("&", "and").Replace(" ", "-").ToLower()
                                            }).OrderBy(a => a.Name).ToList()
                                }).ToList().Where(x => x.SubMenuList.Any())
                        }
                };
        }

        public List<SpecialMenuFields> GetSpecialTrainList(Guid siteId)
        {
            var countryIdList = db.tblCountrySiteLookUps.Where(x => x.SiteID == siteId).Select(x => x.CountryID).ToList();
            var list = new List<SpecialMenuFields>();
            var item = new SpecialMenuFields
            {
                Name = "Special Trains",
                MenuList = db.tblContinents.Where(x => x.tblSpecialTrains.Any(y => y.ContinentID == x.ID)).AsEnumerable().Select(x => new spMenuFields
                {
                    Name = x.Name,
                    Url = "",
                    SubMenuList = x.tblSpecialTrains.Where(v => v.IsActive == true && countryIdList.Contains(v.CountryID)).Select(y => new spSubMenuFields
                    {
                        Id = y.ID,
                        Name = y.Name,
                        Url = y.ID.ToString().Substring(0, 4) + "-" + y.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower(),
                        CountryCode = y.tblCountriesMst.CountryCode
                    }).OrderBy(a => a.Name).AsEnumerable()
                }).ToList()
            };

            if (item.MenuList != null)
                list.Add(item);
            return list;
        }

        public CookiesCollectionValue GetCookiesValue(Guid siteId)
        {
            var rec = db.tblSites.FirstOrDefault(x => x.ID == siteId);
            return new CookiesCollectionValue
            {
                CurrencyId = rec.DefaultCurrencyID,
                CountryId = rec.DefaultCountryID,
                LangId = rec.DefaultLanguageID

            };
        }

        public int GetSiteLayoutType(Guid SiteId)
        {
            try
            {
                var data = db.tblSites.FirstOrDefault(x => x.ID == SiteId && x.IsActive == true);
                if (data != null)
                    return data.LayoutType;
                else
                    return 0;
            }
            catch (Exception ex) { throw ex; }
        }

        public IEnumerable<spSubMenuFields> GetRailPassesProductList(Guid SiteId, int StartCode, int EndCode, Guid AgentId)
        {
            try
            {

                bool IsFOCAD75 = false;
                if (AgentId != Guid.Empty)
                    IsFOCAD75 = (from tau in db.tblAdminUsers
                                 join rd in db.ap_RoleMenuDetail on tau.RoleID equals rd.RoleId
                                 join tnv in db.tblNavigations on rd.MenuId equals tnv.ID
                                 where tau.IsActive.Value && tau.ID == AgentId && tnv.Name == "FOC & AD75 EURAIL TICKETS"
                                 select tau.IsActive).FirstOrDefault().Value;

                var result = (from tp in db.tblProducts
                              join tpn in db.tblProductNames on tp.ID equals tpn.ProductID
                              join tpcl in db.tblProductCategoriesLookUps on tpn.ProductID equals tpcl.ProductID
                              join tcn in db.tblCategoriesNames on tpcl.CategoryID equals tcn.CategoryID
                              join tc in db.tblCategories on tpcl.CategoryID equals tc.ID
                              join tcsl in db.tblCategorySiteLookUps on tpcl.CategoryID equals tcsl.CategoryID
                              join tpsl in db.tblProductSiteLookUps on tpcl.ProductID equals tpsl.ProductID
                              where tp.IsActive && tc.IsActive && !tp.IsAllowAdminPass && (IsFOCAD75 ? tc.IsFOCAD75 == tc.IsFOCAD75 : tc.IsFOCAD75 == IsFOCAD75) &&
                              (tp.ProductEnableFromDate <= DateTime.Now && tp.ProductEnableToDate >= DateTime.Now) &&
                              tcsl.SiteID == SiteId && tpsl.SiteID == SiteId &&
                              (tp.CountryStartCode >= StartCode && tp.CountryStartCode <= EndCode)
                              select new { tpn.ProductID, ProductName = tpn.Name }).AsEnumerable().Select(x => new spSubMenuFields
                              {
                                  Id = x.ProductID,
                                  Name = x.ProductName,
                                  Url = x.ProductID.ToString().Substring(0, 4) + "-" + x.ProductName.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower()
                              }).OrderBy(a => a.Name).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RailPassesMenuFields> GetRailPassesList(Guid siteId, Guid AgentId)
        {
            try
            {
                List<RailPassMenu> MenuListData = new List<RailPassMenu>();
                MenuListData.Add(new RailPassMenu { Name = "Global Rail Pass", StartCode = 9999, EndCode = 9999 });
                MenuListData.Add(new RailPassMenu { Name = "One Country Rail Pass", StartCode = 1001, EndCode = 2000 });
                MenuListData.Add(new RailPassMenu { Name = "Two Country Pass", StartCode = 2001, EndCode = 3000 });
                MenuListData.Add(new RailPassMenu { Name = "Three Country Pass", StartCode = 3001, EndCode = 4000 });
                MenuListData.Add(new RailPassMenu { Name = "Four Country Pass", StartCode = 4001, EndCode = 5000 });
                MenuListData.Add(new RailPassMenu { Name = "Other Pass", StartCode = 0, EndCode = 1000 });

                var countryIdList = db.tblCountrySiteLookUps.Where(x => x.SiteID == siteId).Select(x => x.CountryID).ToList();
                var list = new List<RailPassesMenuFields>();
                var item = new RailPassesMenuFields
                {
                    Name = "Rail Passes",
                    MenuList = MenuListData.AsEnumerable().Select(x => new spMenuFields
                    {
                        Name = x.Name,
                        Url = "",
                        SubMenuList = GetRailPassesProductList(siteId, x.StartCode, x.EndCode, AgentId)
                    }).Where(x => x.SubMenuList.Count() > 0).ToList()
                };

                if (item.MenuList != null)
                    list.Add(item);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region RobotsFile
        public string GetRobotsFilePathBySiteId(Guid siteId)
        {
            try
            {
                var robotfile = db.tblRobotsFiles.FirstOrDefault(x => x.SiteId == siteId && x.IsActive == true);
                return robotfile == null ? "Robots.txt" : robotfile.RobotsFilePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion RobotsFile

        #region SitemapFile
        public string GetSitemapFilePathBySiteId(Guid siteId)
        {
            try
            {
                var sitemap = db.tblSitemapFiles.FirstOrDefault(x => x.SiteId == siteId && x.IsActive == true);
                return sitemap == null ? "sitemap.xml" : sitemap.SiteMapFilePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion SitemapFile


        #region Foc/AD75
        public bool IsFocAD75(Guid agentID)
        {
            try
            {
                var isAd = (from ad in db.tblAdminUsers
                            join role in db.ap_RoleMenuDetail on ad.RoleID equals role.RoleId
                            join nav in db.tblNavigations on role.MenuId equals nav.ID
                            where ad.ID == agentID && nav.Name == "FOC & AD75 Eurail tickets"
                            select ad);
                if (isAd.Any())
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ShowFocTab
        public bool ShowIsFoc(Guid productID)
        {
            try
            {
                var result = (from cl in db.tblProductCategoriesLookUps
                              join cat in db.tblCategories on cl.CategoryID equals cat.ID
                              where cl.ProductID == productID
                              select new { cat }).FirstOrDefault();
                if (result != null)
                {
                    var isfoc = result.cat.IsFOCAD75;
                    if (isfoc != null)
                        return (bool)isfoc;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ShowHavRaiPass
        public bool HavRailPass(Guid siteId)
        {
            try
            {
                var result = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (result != null)
                {
                    var railPass = result.HavRailPass;
                    if (railPass != null)
                        return (bool)railPass;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ShowP2PWidget
        public bool EnableP2P(Guid siteId)
        {
            try
            {
                var result = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (result != null)
                {
                    var enableP2P = result.EnableP2P;
                    if (enableP2P != null)
                        return (bool)enableP2P;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetJourneyEmail
        public string GetJourneyEmail(Guid siteId)
        {
            try
            {
                var result = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (result != null)
                {
                    var journeyEmail = result.JourneyEmail;
                    return journeyEmail;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetBccEmail
        public string GetBccEmail(Guid siteId)
        {
            try
            {
                tblSite objsite = db.tblSites.FirstOrDefault(x => x.ID == siteId && x.IsBccEmail == true);
                return objsite != null ? objsite.BccEmail : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Rail Pass section1
        public List<RailPassSec1> GetRailPassSec1(Guid siteId)
        {
            try
            {
                var list = db.tblRailPassSec1.Where(y => y.SiteId == siteId).Select(x => new RailPassSec1
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = x.tblSite.DisplayName
                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblRailPassSec1 GetRailPassSec1ById(Guid id)
        {
            try
            {
                return db.tblRailPassSec1.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteRailPassSec1(Guid id)
        {
            try
            {
                var rec = db.tblRailPassSec1.FirstOrDefault(x => x.ID == id);
                db.tblRailPassSec1.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddRailPassSec1(tblRailPassSec1 odata)
        {
            try
            {
                var info = db.tblRailPassSec1.FirstOrDefault(x => x.ID == odata.ID);
                if (info != null)
                {
                    info.Name = odata.Name;
                    info.Imagepath = odata.Imagepath;
                    info.Description = odata.Description;
                    info.ModifiedBy = odata.CreatedBy;
                    info.ModifiedOn = odata.CreatedOn;
                    info.SiteId = odata.SiteId;
                }
                else
                {
                    var _info = db.tblRailPassSec1.FirstOrDefault(x => x.SiteId == odata.SiteId);
                    if (_info == null)
                    {
                        db.AddTotblRailPassSec1(odata);
                    }
                }
                db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblSite> GetSiteList()
        {
            try
            {
                return db.tblSites.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class RailPassSec1 : tblRailPassSec1
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region Rail Pass section2
        public List<RailPassSec2> GetRailPassSec2(Guid siteId)
        {
            try
            {
                var list = db.tblRailPassSec2.Where(y => y.SiteId == siteId).Select(x => new RailPassSec2
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = x.tblSite.DisplayName
                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblRailPassSec2 GetRailPassSec2ById(Guid id)
        {
            try
            {
                return db.tblRailPassSec2.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteRailPassSec2(Guid id)
        {
            try
            {
                var rec = db.tblRailPassSec2.FirstOrDefault(x => x.ID == id);
                db.tblRailPassSec2.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddRailPassSec2(tblRailPassSec2 odata)
        {
            try
            {
                var info = db.tblRailPassSec2.FirstOrDefault(x => x.ID == odata.ID);
                if (info != null)
                {
                    info.Name = odata.Name;
                    info.Imagepath = odata.Imagepath;
                    info.Description = odata.Description;
                    info.ModifiedBy = odata.CreatedBy;
                    info.ModifiedOn = odata.CreatedOn;
                    info.SiteId = odata.SiteId;
                }
                else
                {
                    var _info = db.tblRailPassSec2.FirstOrDefault(x => x.SiteId == odata.SiteId);
                    if (_info == null)
                    {
                        db.AddTotblRailPassSec2(odata);
                    }
                }
                db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class RailPassSec2 : tblRailPassSec2
        {
            public string SiteName { get; set; }
        }
        #endregion    }

        #region Rail Pass section3
        public List<RailPassSec3> GetRailPassSec3(Guid siteId)
        {
            try
            {
                var list = db.tblRailPassSec3.Where(y => y.SiteId == siteId).Select(x => new RailPassSec3
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = x.tblSite.DisplayName
                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblRailPassSec3 GetRailPassSec3ById(Guid id)
        {
            try
            {
                return db.tblRailPassSec3.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteRailPassSec3(Guid id)
        {
            try
            {
                var rec = db.tblRailPassSec3.FirstOrDefault(x => x.ID == id);
                db.tblRailPassSec3.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddRailPassSec3(tblRailPassSec3 odata)
        {
            try
            {
                var info = db.tblRailPassSec3.FirstOrDefault(x => x.ID == odata.ID);
                if (info != null)
                {
                    info.Name = odata.Name;
                    info.Imagepath = odata.Imagepath;
                    info.Description = odata.Description;
                    info.ModifiedBy = odata.CreatedBy;
                    info.ModifiedOn = odata.CreatedOn;
                    info.SiteId = odata.SiteId;
                }
                else
                {
                    var _info = db.tblRailPassSec3.FirstOrDefault(x => x.SiteId == odata.SiteId);
                    if (_info == null)
                    {
                        db.AddTotblRailPassSec3(odata);
                    }
                }
                db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class RailPassSec3 : tblRailPassSec3
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region SiteTheme
        public List<SiteTheme> GetSiteTheme()
        {
            try
            {
                var list = db.tblSiteThemes.Select(x => new SiteTheme
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = x.tblSite.DisplayName,
                    Theme = x.tblTheme.ThemeName
                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSiteTheme GetSiteThemeById(Guid id)
        {
            try
            {
                return db.tblSiteThemes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteSiteTheme(Guid id)
        {
            try
            {
                var rec = db.tblSiteThemes.FirstOrDefault(x => x.ID == id);
                db.tblSiteThemes.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddSiteTheme(tblSiteTheme odata)
        {
            try
            {
                var info = db.tblSiteThemes.FirstOrDefault(x => x.ID == odata.ID);
                if (info != null)
                {
                    info.Name = odata.Name;
                    info.CssFolderName = odata.CssFolderName;
                    info.CssHeaderFooterName = odata.CssHeaderFooterName;
                    info.SiteID = odata.SiteID;
                    info.ThemeID = odata.ThemeID;
                    info.HeaderHtml = odata.HeaderHtml;
                    info.FooterHtml = odata.FooterHtml;
                }
                else
                {
                    var _info = db.tblSiteThemes.FirstOrDefault(x => x.SiteID == odata.SiteID);
                    if (_info == null)
                    {
                        db.AddTotblSiteThemes(odata);
                    }
                }
                db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class SiteTheme : tblSiteTheme
        {
            public string SiteName { get; set; }
            public string Theme { get; set; }
        }
        #endregion

        #region Footer Menus
        public List<FooterMenuFields> GetActiveFooterMeuList(Guid siteId)
        {
            try
            {
                var olist = (from fm in db.tblFooterMenus
                             join fmlook in db.tblFooterMenuSiteLookups on fm.ID equals fmlook.MenuID
                             where fmlook.SiteID == siteId && fm.IsActive
                             select new { fm }).ToList();
                var result = olist.Select(x => new FooterMenuFields
                {
                    ID = x.fm.ID,
                    MenuName = x.fm.MenuName,
                    MenuList = db.tblFooterItems.Where(y => y.IsActive && y.MenuID == x.fm.ID).OrderBy(u => u.ShortOrder).Select(z => new FooterSubMenuFields
                    {
                        Name = z.Name,
                        Url = z.NavUrl
                    }).AsEnumerable().ToList(),
                    SortOrder = (int)x.fm.SortOrder
                }).ToList();

                return result.OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    public class FooterMenuFields
    {
        public Guid ID { get; set; }
        public string MenuName { get; set; }
        public int SortOrder { get; set; }
        public IEnumerable<FooterSubMenuFields> MenuList { get; set; }
    }
    public class FooterSubMenuFields
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
    }

    public class NavFields
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }

    public class MenuFields
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public string GrpName { get; set; }
        public IEnumerable<SubMenuFields> SubMenuList { get; set; }
    }
    public class SubMenuFields
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
    }

    /*special train menu*/
    public class spMenuFields
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public string GrpName { get; set; }
        public IEnumerable<spSubMenuFields> SubMenuList { get; set; }
    }
    public class spSubMenuFields
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
    }
    public class CountryMenuFields
    {
        public string Name { get; set; }
        public IEnumerable<MenuFields> MenuList { get; set; }
    }

    public class SpecialMenuFields
    {
        public string Name { get; set; }
        public List<spMenuFields> MenuList { get; set; }
    }

    public class SpecialSubMenuFields
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public Guid CountryID { get; set; }
        public string CountryCode { get; set; }
    }

    public class CookiesCollectionValue
    {
        public Guid? LangId { get; set; }
        public Guid? CountryId { get; set; }
        public Guid? CurrencyId { get; set; }
    }
    public class RailPassMenu
    {
        public string Name { get; set; }
        public int StartCode { get; set; }
        public int EndCode { get; set; }
    }

    public class RailPassesMenuFields
    {
        public string Name { get; set; }
        public List<spMenuFields> MenuList { get; set; }
    }
}
