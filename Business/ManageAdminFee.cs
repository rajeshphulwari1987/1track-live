﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageAdminFee
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region For Admin Areas
        public List<TBLAdminFee> GetAdminFee()
        {
            try
            {
                List<TBLAdminFee> list = (from ts in _db.tblSites
                                          join taf in _db.tblAdminFees on ts.ID equals taf.SiteId into temp
                                          from admin in temp.DefaultIfEmpty()
                                          where ts.IsDelete == false
                                          select new { admin, ts }).Select
                            (t => new TBLAdminFee
                            {
                                ID = (t.admin.ID == null ? Guid.NewGuid() : t.admin.ID),
                                SiteId = t.ts.ID,
                                SiteName = t.ts.DisplayName,
                                AdminFee = t.admin.AdminFee == null ? (decimal)0.00 : t.admin.AdminFee,
                                IsPercentage = t.admin.IsPercentage == null ? false : t.admin.IsPercentage,
                                IsApplicable = t.admin.IsApplicable == null ? false : t.admin.IsApplicable
                            }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateAdminFee(tblAdminFee admin)
        {
            try
            {
                var list = _db.tblAdminFees.FirstOrDefault(t => t.SiteId == admin.SiteId);
                if (list != null)
                {
                    list.AdminFee = admin.AdminFee;
                    list.IsPercentage = admin.IsPercentage;
                    list.ModifiedBy = admin.CreatedBy;
                    list.ModifiedOn = admin.CreatedOn;
                }
                else
                    _db.AddTotblAdminFees(admin);
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateIsApplicable(Guid ID)
        {
            try
            {
                var data = _db.tblAdminFees.FirstOrDefault(t => t.ID == ID);
                if (data != null)
                    data.IsApplicable = !data.IsApplicable;
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region For Front Areas
        public string GetAdminFeeBySiteID(Guid SiteId)
        {
            try
            {
                string AdminFee = string.Empty;
                var data = _db.tblAdminFees.FirstOrDefault(x => x.SiteId == SiteId && x.IsApplicable);
                if (data != null)
                {
                    if (data.IsPercentage)
                        AdminFee = "mult" + (data.AdminFee / 100);
                    else
                        AdminFee = "sum" + data.AdminFee;
                }
                return AdminFee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrderAdminFeeByOrderID(long OrderID, Decimal Amount)
        {
            try
            {
                var data = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderID);
                if (data != null)
                    data.AdminFee = Amount;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetOrderAdminFeeByOrderID(int OrderID)
        {
            try
            {
                return _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderID).AdminFee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
    public class TBLAdminFee : tblAdminFee
    {
        public string SiteName { get; set; }
    }
}