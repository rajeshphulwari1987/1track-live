//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    
    public partial class GetStatusOfStockNumberByParentBranch
    {
        public decimal StockNo { get; set; }
        public string AllocatedTo { get; set; }
        public string Allocated_From { get; set; }
        public string Status { get; set; }
        public System.Guid BranchID { get; set; }
        public Nullable<System.Guid> ParentBranchID { get; set; }
        public System.Guid StatusID { get; set; }
    }
}
