﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Business
{
    public class ManageP2PBooking
    {
        db_1TrackEntities db = new db_1TrackEntities();

        public Guid InsertRefund(tblP2PRefund refund)
        {
            try
            {
                if (refund.ID == Guid.Empty)
                {
                    refund.ID = Guid.NewGuid();
                    refund.RefundDate = System.DateTime.Now;
                    db.tblP2PRefund.AddObject(refund);
                    db.SaveChanges();
                }
                return refund.ID;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid InsertRefundTitle(tblP2PRefundTitle refund)
        {
            try
            {
                if (refund.ID == Guid.Empty)
                {
                    refund.ID = Guid.NewGuid();
                    db.tblP2PRefundTitle.AddObject(refund);
                    db.SaveChanges();
                }
                return refund.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ExtraRefundField GetByReservationCode(string resCode, long orderNo)
        {

            Guid siteID = (Guid)db.tblOrders.FirstOrDefault(x => x.OrderID == orderNo).SiteID;
            Guid srcCurrID = (Guid)db.tblSites.FirstOrDefault(x => x.ID == siteID).DefaultCurrencyID;
            Guid trgCurId = FrontEndManagePass.GetCurrencyID("EUT");
            Decimal ExchangeRate = (Decimal)db.tblCurrencyConversions.FirstOrDefault(x => x.SiteID == siteID && x.SrcCurrID == srcCurrID && x.TrgCurrID == trgCurId).Multiplier;
            tblP2PSale p2pSale = db.tblP2PSale.FirstOrDefault(x => x.ReservationCode == resCode);

            if (p2pSale != null)
                return new ExtraRefundField
                   {
                       ExchangeRate = ExchangeRate,
                       ArrivalStationName = p2pSale.From,
                       DepartureStationName = p2pSale.To
                   };

            return null;

        }

        public bool IsRefundedReservation(string resCode)
        {
            return db.tblP2PRefund.Any(x => x.ReservationCode == resCode);
        }

    }
    public class ExtraRefundField
    {
        public decimal ExchangeRate { set; get; }
        public string DepartureStationName { set; get; }
        public string ArrivalStationName { set; get; }

    }
}
