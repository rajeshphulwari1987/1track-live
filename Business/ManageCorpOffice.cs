﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{

    public class ManageCorpOffice
    {
        db_1TrackEntities db = null;

        public ManageCorpOffice()
        {
            this.db = new db_1TrackEntities();
        }

        public Guid AddEditCorpOffice(tblBranch office)
        {
            try
            {
                if (db.tblBranches.Any(x => x.ID == office.ID))
                {
                    tblBranch officeObj = db.tblBranches.FirstOrDefault(x => x.ID == office.ID);
                    officeObj.OfficeName = office.OfficeName;
                    officeObj.Address1 = office.Address1;
                    officeObj.Address2 = office.Address2;
                    officeObj.Town = office.Town;
                    officeObj.Country = office.Country;
                    officeObj.IsActive = office.IsActive;
                    officeObj.Telephone = office.Telephone;
                    officeObj.Email = office.Email;
                    officeObj.Postcode = office.Postcode;
                    officeObj.IsCorporate = true;
                    officeObj.IsVirtualOffice = false;
                    officeObj.chkEmail = false;
                    officeObj.chkSecondaryEmail = false;
                    officeObj.City = office.City;
                    officeObj.Description = office.Description;
                    officeObj.County = office.County;
                    officeObj.ParentBranchID = office.ParentBranchID;
                    officeObj.LogoTitle = office.LogoTitle;
                    officeObj.Color = office.Color;
                    if (office.LogoPath != null)
                    {
                        officeObj.LogoPath = office.LogoPath;
                    }
                    db.SaveChanges();
                    return officeObj.ID;
                }
                else
                {
                    tblBranch officeObj = new tblBranch();
                    officeObj.ID = office.ID;
                    officeObj.OfficeName = office.OfficeName;
                    officeObj.Address1 = office.Address1;
                    officeObj.Address2 = office.Address2;
                    officeObj.Town = office.Town;
                    officeObj.Country = office.Country;
                    officeObj.IsActive = office.IsActive;
                    officeObj.Telephone = office.Telephone;
                    officeObj.Email = office.Email;
                    officeObj.Postcode = office.Postcode;
                    officeObj.IsCorporate = true;
                    officeObj.IsVirtualOffice = false;
                    officeObj.chkEmail = false;
                    officeObj.chkSecondaryEmail = false;
                    officeObj.City = office.City;
                    officeObj.Description = office.Description;
                    officeObj.County = office.County;
                    officeObj.ParentBranchID = office.ParentBranchID;
                    officeObj.LogoTitle = office.LogoTitle;
                    officeObj.Color = office.Color;
                    if (office.LogoPath != null)
                    {
                        officeObj.LogoPath = office.LogoPath;
                    }
                    db.tblBranches.AddObject(officeObj);
                    db.SaveChanges();
                    return officeObj.ID;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public int AddCorpBranchLookupSites(tblBranchLookupSite BranchLookupSite)
        {
            try
            {
                var _tblBranchLookupSite = new tblBranchLookupSite
                {
                    ID = BranchLookupSite.ID,
                    BranchID = BranchLookupSite.BranchID,
                    SiteID = BranchLookupSite.SiteID
                };
                db.AddTotblBranchLookupSites(_tblBranchLookupSite);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblBranch GetOfficeDetailById(Guid officeid)
        {
            try
            {
                return db.tblBranches.FirstOrDefault(x => x.ID == officeid);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<Office> GetOfficeDDLList(Guid siteid, Guid ParentId)
        {
            try
            {
                if (ParentId != Guid.Empty)
                {
                    return (from x in db.tblBranches
                            join clkp in db.tblBranchLookupSites on x.ID equals clkp.BranchID
                            where clkp.SiteID == siteid && x.ID != ParentId && x.IsCorporate
                            orderby x.OfficeName
                            select new Office
                            {
                                ID = x.ID,
                            }).AsEnumerable().Select(x => new Office
                            {
                                ID = x.ID,
                                OfficeName = string.IsNullOrEmpty(GetOfficeNameDDL(x.ID)) ? "" : GetOfficeNameDDL(x.ID).Substring(0, GetOfficeNameDDL(x.ID).Length - 4),
                            }).ToList();
                }
                else
                {
                    return (from x in db.tblBranches
                            join clkp in db.tblBranchLookupSites on x.ID equals clkp.BranchID
                            where clkp.SiteID == siteid && x.IsCorporate
                            orderby x.OfficeName
                            select new Office
                            {
                                ID = x.ID,
                            }).AsEnumerable().Select(x => new Office
                            {
                                ID = x.ID,
                                OfficeName = string.IsNullOrEmpty(GetOfficeNameDDL(x.ID)) ? "" : GetOfficeNameDDL(x.ID).Substring(0, GetOfficeNameDDL(x.ID).Length - 4),
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOfficeNameDDL(Guid id)
        {
            try
            {
                string name = string.Empty;
                var list = (from stk in db.tblBranches
                            where stk.ID == id
                            select new
                            {
                                id = stk.ID,
                                parentID = stk.ParentBranchID,
                                name = stk.OfficeName,
                            }).ToList();

                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        name = GetOfficeNameDDL((Guid)item.parentID) + " " + item.name + " --> ";
                    }
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Office> GetOfficeList(Guid siteid)
        {
            try
            {
                var result = (from TB in db.tblBranches
                              join TBSL in db.tblBranchLookupSites on TB.ID equals TBSL.BranchID
                              where TBSL.SiteID == siteid && TB.IsCorporate
                              select TB).ToList();
                if (result != null && result.Count > 0)
                {
                    return result.ToList().Select(x => new Office
                    {
                        Address = x.Address1 + " " + x.Address2 + " " + x.Town + " " + x.City + " " + x.County + " " + (db.tblCountriesMsts.FirstOrDefault(y => y.CountryID == x.Country.Value).CountryName) + "," + x.Postcode,
                        ID = x.ID,
                        Telephone = x.Telephone,
                        LogoTitle = x.LogoTitle,
                        LogoPath = "../" + x.LogoPath,
                        IsActive = x.IsActive.HasValue ? x.IsActive.Value : false,
                        OfficeName = string.IsNullOrEmpty(GetOfficeName(x.ID)) ? "" : GetOfficeName(x.ID).Substring(0, GetOfficeName(x.ID).Length - 4),
                    }).ToList();
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetOfficeName(Guid id)
        {
            try
            {
                string name = string.Empty;
                var list = (from stk in db.tblBranches
                            where stk.ID == id
                            select new
                            {
                                parentID = stk.ParentBranchID,
                                name = stk.OfficeName,
                            }).ToList();

                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        name = GetOfficeName((Guid)item.parentID) + " " + item.name + " --> ";
                    }
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpSiteOffice(Guid officeid)
        {
            try
            {
                db.tblBranchLookupSites.Where(w => w.BranchID == officeid).ToList().ForEach(db.tblBranchLookupSites.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ActiveInActiveOffice(Guid officeid)
        {

            try
            {
                tblBranch officeObj = db.tblBranches.FirstOrDefault(x => x.ID == officeid);
                officeObj.IsActive = !officeObj.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteOffice(Guid officeid)
        {

            try
            {
                tblBranch officeObj = db.tblBranches.FirstOrDefault(x => x.ID == officeid);
                if (officeObj != null)
                {
                    DeleteCorpSiteOffice(officeid);
                    db.tblBranches.DeleteObject(officeObj);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblBranchLookupSite> GetBranchLookupSites(Guid BranchId)
        {
            try
            {
                return db.tblBranchLookupSites.Where(x => x.BranchID == BranchId).ToList();
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public class Office : tblBranch
    {
        public string Address { get; set; }
        public string Telephone { get; set; }
    }
}
