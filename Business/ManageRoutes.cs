﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageRoutes
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        public void DoneRouteProductUpdateUrl()
        {
            try
            {
                var data = _db.tblRouteTables.FirstOrDefault();
                if (data != null)
                    data.IsUpdate = false;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetRouteProductUpdateUrl()
        {
            try
            {
                var result = _db.tblRouteTables.FirstOrDefault();
                if (result != null)
                    return result.IsUpdate;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetRouteCount(Route route)
        {
            try
            {
                var result = _db.tblRouteTables.FirstOrDefault();
                switch (route)
                {
                    case Route.CategoryRoute:
                        return result.CategoryCount;
                    case Route.ProductRoute:
                        return result.ProductCount;
                    case Route.CountryRoute:
                        return result.CountryCount;
                    case Route.SpecialTrainRoute:
                        return result.SpecialTrainCount;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateRouteCount(Route route, int count)
        {
            try
            {
                var result = _db.tblRouteTables.FirstOrDefault();
                switch (route)
                {
                    case Route.CategoryRoute:
                        result.CategoryCount = count;
                        break;
                    case Route.ProductRoute:
                        result.ProductCount = count;
                        break;
                    case Route.CountryRoute:
                        result.CountryCount = count;
                        break;
                    case Route.SpecialTrainRoute:
                        result.SpecialTrainCount = count;
                        break;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public enum Route
    {
        CategoryRoute,
        ProductRoute,
        SpecialTrainRoute,
        CountryRoute,
    }
}
