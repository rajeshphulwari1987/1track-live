﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using ResizeImage;
using System.Web.UI;

namespace Business
{
    public class Common
    {
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static void FillDropDown(IEnumerable objList, DropDownList ddl, string strDisplayText, string strDisplayValue, string strDefaultText, string strDefaultValue)
        {
            ListItem lstDefault = new ListItem(strDefaultText, strDefaultValue);
            ddl.DataTextField = strDisplayText;
            ddl.DataValueField = strDisplayValue;
            ddl.DataSource = objList;
            ddl.DataBind();
            ddl.Items.Insert(0, lstDefault);
        }

        public static void FillCheckBoxList(IEnumerable objList, CheckBoxList cbl, string strDisplayText, string strDisplayValue)
        {
            cbl.DataTextField = strDisplayText;
            cbl.DataValueField = strDisplayValue;
            cbl.DataSource = objList;
            cbl.DataBind();
        }
        public string CreateRandomPassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*_";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file

            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }


        public string CropImage(FileUpload FileUpload1, string Location, float height, float width)
        {
            string strImage = FileUpload1.PostedFile.FileName;
            if (!string.IsNullOrEmpty(strImage))
            {
                try
                {

                    System.Drawing.Image myImage = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);

                    float imgHeight = myImage.Height;
                    float imgWidth = myImage.Width;
                    float NewHeight = 0;
                    float NewWidth = 0;
                    float factor = 0;

                    string FileExt = Path.GetExtension(strImage).ToString();
                    strImage = Guid.NewGuid() + "_" + Path.GetFileNameWithoutExtension(strImage) + FileExt;
                    string strFilePathTemp = HttpContext.Current.Server.MapPath(Location + strImage);
                    FileUpload1.PostedFile.SaveAs(strFilePathTemp);

                    if (imgHeight < height)
                    {
                        NewHeight = imgHeight;
                    }
                    else
                    {
                        NewHeight = height;
                    }
                    factor = imgHeight / NewHeight;
                    NewWidth = imgWidth / factor;
                    NewWidth = NewWidth > width ? width : NewWidth;

                    string strNewImagename = Guid.NewGuid() + FileExt;
                    ResizeImage(strFilePathTemp, HttpContext.Current.Server.MapPath(Location) + strNewImagename, Convert.ToInt32(NewWidth), Convert.ToInt32(NewHeight));
                    strImage = strNewImagename;

                    if (File.Exists(strFilePathTemp))
                        File.Delete(strFilePathTemp);
                }
                catch
                {
                    strImage = "";
                }
            }
            else
            {
                strImage = "";
            }
            return strImage;
        }

        public string CropImage(Guid newfileName, FileUpload FileUpload1, string Location, float height, float width)
        {
            string strImage = FileUpload1.PostedFile.FileName;
            if (!string.IsNullOrEmpty(strImage))
            {
                try
                {

                    System.Drawing.Image myImage = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);

                    float imgHeight = myImage.Height;
                    float imgWidth = myImage.Width;
                    float NewHeight = 0;
                    float NewWidth = 0;
                    float factor = 0;

                    string FileExt = Path.GetExtension(strImage).ToString();
                    strImage = newfileName + "_" + Path.GetFileNameWithoutExtension(strImage) + FileExt;
                    string strFilePathTemp = HttpContext.Current.Server.MapPath(Location + strImage);
                    FileUpload1.PostedFile.SaveAs(strFilePathTemp);

                    if (imgHeight < height)
                    {
                        NewHeight = imgHeight;
                    }
                    else
                    {
                        NewHeight = height;
                    }
                    factor = imgHeight / NewHeight;
                    NewWidth = imgWidth / factor;
                    NewWidth = NewWidth > width ? width : NewWidth;

                    string strNewImagename = newfileName + FileExt;
                    ResizeImage(strFilePathTemp, HttpContext.Current.Server.MapPath(Location) + strNewImagename, Convert.ToInt32(NewWidth), Convert.ToInt32(NewHeight));
                    strImage = strNewImagename;

                    if (File.Exists(strFilePathTemp))
                        File.Delete(strFilePathTemp);
                }
                catch
                {
                    strImage = "";
                }
            }
            else
            {
                strImage = "";
            }
            return strImage;
        }

        public void ResizeImage(string sourceFile, string targetFile, int outputWidth, int outputHeight)
        {
            ImageResize.ResizeFix(sourceFile, targetFile, outputWidth, outputHeight);
        }



    }
}
