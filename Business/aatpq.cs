//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class aatpq
    {
        public System.Guid ID { get; set; }
        public Nullable<System.Guid> PassSaleID { get; set; }
        public Nullable<System.Guid> SiteID { get; set; }
        public Nullable<long> OrderID { get; set; }
        public Nullable<System.Guid> CategoryID { get; set; }
        public Nullable<System.Guid> ProductID { get; set; }
        public Nullable<System.Guid> Status { get; set; }
        public Nullable<System.Guid> AdminUserID { get; set; }
        public Nullable<System.DateTime> DateTimeStamp { get; set; }
        public Nullable<System.Guid> QueueID { get; set; }
    }
}
