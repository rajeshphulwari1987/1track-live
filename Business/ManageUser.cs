﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Collections.Generic;
using System.Web;
using System.Net;

namespace Business
{
    public class ManageUser
    {
        db_1TrackEntities db = new db_1TrackEntities();

        public Guid SaveEQOcontactusUser(tblUserLogin obj)
        {
            try
            {
                var objaffUser = db.tblUserLogins.Where(o => o.Email == obj.Email && o.SiteId == obj.SiteId).FirstOrDefault();
                if (objaffUser != null)
                {
                    objaffUser.FirstName = obj.FirstName;
                    objaffUser.LastName = obj.LastName;
                    objaffUser.Phone = obj.Phone;
                    objaffUser.Email = obj.Email;
                    objaffUser.Dob = obj.Dob;
                    objaffUser.Country = obj.Country;
                    objaffUser.City = obj.City;
                    objaffUser.State = obj.State;
                    objaffUser.PostCode = obj.PostCode;
                    objaffUser.IsActive = obj.IsActive;
                    objaffUser.SiteId = obj.SiteId;
                    objaffUser.Address = obj.Address;
                    objaffUser.Address2 = obj.Address2;
                    objaffUser.State = obj.State;
                    objaffUser.IsNewsEmail = obj.IsNewsEmail;
                    if (!string.IsNullOrEmpty(obj.Password))
                        objaffUser.Password = obj.Password;
                    objaffUser.IsContactUsUser = obj.IsContactUsUser == null ? false : true;
                    objaffUser.IsEQOUser = obj.IsEQOUser == null ? false : true;


                    /**hold exists id*/
                    obj.ID = objaffUser.ID;
                }
                else if (!db.tblUserLogins.Any(x => x.Email == obj.Email && x.SiteId == obj.SiteId))
                    db.tblUserLogins.AddObject(obj);
                else
                    return Guid.Empty;
                db.SaveChanges();
                return obj.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblUseCancelOrder UserCancelOrderByOrderID(long OrderID)
        {
            try
            {
                return db.tblUseCancelOrders.FirstOrDefault(x => x.OrderId == OrderID);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<UseCancelOrder> GetusercancelorderList(Guid SiteID)
        {
            try
            {
                var datalist = (from tul in db.tblUserLogins
                                join tuco in db.tblUseCancelOrders on tul.ID equals tuco.UserId
                                join tod in db.tblOrders on tuco.OrderId equals tod.OrderID
                                join tos in db.tblOrderStatus on tod.Status equals tos.ID
                                where tul.SiteId == SiteID
                                select new UseCancelOrder
                                {
                                    FirstName = tul.FirstName,
                                    LastName = tul.LastName,
                                    Email = tul.Email,
                                    OrderId = tuco.OrderId,
                                    StatusName = tos.Name,
                                    Status = tuco.Status,
                                    IsActive = tuco.IsActive,
                                    Id = tuco.Id,
                                    CreatedOn = tuco.CreatedOn
                                }).OrderByDescending(x => x.CreatedOn).ToList();
                return datalist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUseCancelOrderChat(int OrderId, Guid AdminId, string Commitment, int Status)
        {
            try
            {
                var data = db.tblUseCancelOrders.FirstOrDefault(x => x.OrderId == OrderId);
                if (data != null && data.Status != Status) data.Status = Status;

                var dataOrder = db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (dataOrder != null && dataOrder.Status != Status) dataOrder.Status = Status;

                if (data != null && !string.IsNullOrEmpty(Commitment))
                    db.tblUseCancelOrderChats.AddObject(
                        new tblUseCancelOrderChat
                        {
                            OrderId = OrderId,
                            ChatUserId = AdminId,
                            Commitment = Commitment,
                            CreatedOn = DateTime.Now
                        });
                db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }
        //Get Admin Menu Role Wise
        public List<GetAdminMenuByRoleWise_Result> GetMenuRoleWise(Guid Roleid)
        {
            try
            {
                return db.GetAdminMenuByRoleWise(Roleid).ToList();
                //return db.ExecuteStoreQuery<tblNavigation>("exec GetAdminMenuByRoleWise '" + Roleid + "'").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblOrder OrderByOrderID(long OrderID)
        {
            try
            {
                return db.tblOrders.FirstOrDefault(x => x.OrderID == OrderID);
            }
            catch (Exception ex) { throw ex; }
        }

        public void GetusercancelorderActive(int Id)
        {
            try
            {
                var data = db.tblUseCancelOrders.FirstOrDefault(x => x.Id == Id);
                data.IsActive = !data.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UseCancelOrderChatHistory> GetUseCancelOrderChatList(int OrderId)
        {
            try
            {
                var data = (from tucoc in db.tblUseCancelOrderChats
                            join tau in db.tblAdminUsers on tucoc.ChatUserId equals tau.ID into tempTAU
                            from Atau in tempTAU.DefaultIfEmpty()
                            join tul in db.tblUserLogins on tucoc.ChatUserId equals tul.ID into tempTUL
                            from Btul in tempTUL.DefaultIfEmpty()
                            where tucoc.OrderId == OrderId
                            select new { AFirstName = Atau.Forename, ALastName = Atau.Surname, UFirstName = Btul.FirstName, ULastName = Btul.LastName, OrderId = tucoc.OrderId, Commitment = tucoc.Commitment, CreatedOn = tucoc.CreatedOn }).AsEnumerable()
                            .Select(x => new UseCancelOrderChatHistory
                            {
                                Name = string.IsNullOrEmpty(x.UFirstName) ? x.AFirstName + (string.IsNullOrEmpty(x.ALastName) ? "" : " " + x.ALastName) : x.UFirstName + (string.IsNullOrEmpty(x.ULastName) ? "" : " " + x.ULastName),
                                Commitment = x.Commitment,
                                OrderId = x.OrderId.Value,
                                CreatedOn = x.CreatedOn,
                                IsUser = string.IsNullOrEmpty(x.UFirstName) ? "clsAdmin" : "clsUser"
                            }).ToList();
                return data;
            }
            catch (Exception ex) { throw ex; }
        }

        //Login 
        public tblAdminUser AdminLogin(string UserName, string Password)
        {
            try
            {
                tblAdminUser user = db.tblAdminUsers.FirstOrDefault(x => x.UserName == UserName && x.Password == Password && x.IsActive == true);
                if (user == null || (user.AssignTo != 2 && user.AssignTo != 3))
                    return null;
                return db.tblBranches.Any(x => x.ID == user.BranchID && x.IsActive == true) == true ? user : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Guid GetSiteIdByUser(Guid userid)
        {
            try
            {
                tblAdminUserLookupSite user = (from ts in db.tblSites
                                               join tauls in db.tblAdminUserLookupSites on ts.ID equals tauls.SiteID
                                               where tauls.AdminUserID.Value == userid && ts.IsActive == true && ts.IsDelete == false
                                               select tauls).FirstOrDefault();
                return user == null ? Guid.Empty : (Guid)user.SiteID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblUserLogin GetUserLogin(string EmailAddress, string Password, Guid SiteID)
        {
            return db.tblUserLogins.FirstOrDefault(x => x.Email == EmailAddress && x.SiteId == SiteID && x.Password == Password);
        }

        public List<UserCancelOrderChats> GetUseCancelOrderChatList(long OrderID, Guid SiteID, Guid UserID)
        {
            try
            {
                var data = (from TU in db.tblUserLogins
                            join TUCO in db.tblUseCancelOrders on TU.ID equals TUCO.UserId
                            join TOS in db.tblOrderStatus on TUCO.Status equals TOS.ID
                            join TUCOM in db.tblUseCancelOrderChats on TUCO.OrderId equals TUCOM.OrderId
                            where TU.SiteId == SiteID && TUCO.OrderId == OrderID
                            select new UserCancelOrderChats
                            {
                                OrderId = TUCOM.OrderId,
                                ChatUserId = TUCOM.ChatUserId,
                                CreatedOn = TUCOM.CreatedOn,
                                Commitment = TUCOM.Commitment,
                                Name = TUCOM.ChatUserId == UserID ? TU.FirstName + " " + (string.IsNullOrEmpty(TU.LastName) ? "" : TU.LastName) : "Admin",
                                IsUser = TUCOM.ChatUserId == UserID ? "clsUser" : "clsAdmin"
                            }).OrderByDescending(x => x.CreatedOn).ToList();
                return data;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddUserCancelOrder(tblUseCancelOrder UseCancelOrder, tblUseCancelOrderChat UseCancelOrderChat)
        {
            try
            {
                if (!db.tblUseCancelOrders.Any(x => x.OrderId == UseCancelOrder.OrderId))
                    db.tblUseCancelOrders.AddObject(UseCancelOrder);

                db.tblUseCancelOrderChats.AddObject(UseCancelOrderChat);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ChangeAgentPassword(Guid id, string newPass, string firstName, string lastName)
        {
            try
            {
                var user = db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                if (user != null)
                {
                    user.Password = newPass;
                    user.Forename = firstName;
                    user.Surname = lastName;
                    db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ChangePassword(Guid id, string oldPass, string newPass)
        {
            try
            {
                if (!db.tblAdminUsers.Any(x => x.ID == id && x.Password == oldPass))
                    throw new Exception("Old Password does not matched!");
                else
                {
                    tblAdminUser user = db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                    user.Password = newPass;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblAffiliateUser AffliateLogin(string UserName, string Password, Guid siteID)
        {
            try
            {
                tblAffiliateUser result = db.tblAffiliateUsers.FirstOrDefault(x => x.UserName == UserName && x.Password == Password);
                if (result != null)
                {
                    if (db.tblAffiliateUserLookupSites.Any(x => x.AffiliateUserID == result.ID && x.SiteID == siteID))
                        return result;
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public AdminUser AgentLogin(string UserName, string Password, Guid siteID)
        {
            try
            {
                var user = (from ad in db.tblAdminUsers
                            join st in db.tblAdminUserLookupSites
                                on ad.ID equals st.AdminUserID
                            where
                                ad.UserName == UserName && ad.Password == Password && ad.IsAgent == true &&
                                ad.IsActive == true && ad.IsDeleted == false
                                && st.SiteID == siteID
                            select new AdminUser { ID = ad.ID, RoleID = ad.RoleID, UserName = ad.UserName, BranchID = ad.BranchID, AssignTo = ad.AssignTo }).FirstOrDefault();
                if (user == null || (user.AssignTo != 1 && user.AssignTo != 3))
                    return null;
                return db.tblBranches.Any(x => x.ID == user.BranchID && x.IsActive == true) == true ? user : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public tblUserLogin GetUserByEmailAndSiteID(string EmailAddress, Guid SiteID)
        {
            var data = db.tblUserLogins.FirstOrDefault(x => x.Email == EmailAddress && x.SiteId == SiteID);
            return data;
        }

        public tblUserLogin GetUserByEmailID(string EmailAddress)
        {
            return db.tblUserLogins.FirstOrDefault(x => x.Email == EmailAddress);
        }

        public tblAdminUser AgentLoginById(Guid ID)
        {
            try
            {
                tblAdminUser user = db.tblAdminUsers.FirstOrDefault(x => x.ID == ID && x.IsAgent == true && x.IsActive == true);
                if (user == null || (user.AssignTo != 1 && user.AssignTo != 3))
                    return null;
                return db.tblBranches.Any(x => x.ID == user.BranchID && x.IsActive == true) == true ? user : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public tblBranch AgentDetailsById(Guid ID)
        {
            try
            {
                tblAdminUser user = db.tblAdminUsers.FirstOrDefault(x => x.ID == ID);
                return db.tblBranches.FirstOrDefault(x => x.ID == user.BranchID && x.IsActive == true);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public tblAdminUser AgentDetailsByUserName(string UserName)
        {
            try
            {
                return db.tblAdminUsers.FirstOrDefault(x => x.UserName == UserName && x.IsActive == true);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public tblAdminUser AgentNameEmailById(Guid ID)
        {
            try
            {
                tblAdminUser user = db.tblAdminUsers.FirstOrDefault(x => x.ID == ID && x.IsAgent == true && x.IsActive == true);
                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public tblAdminUser GetAdminUserDetails(Guid ID)
        {
            try
            {
                return db.tblAdminUsers.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //Check Email Address
        public tblAdminUser CheckEmail(string username, string Email)
        {
            try
            {
                return db.tblAdminUsers.FirstOrDefault(x => x.UserName == username && x.EmailAddress == Email && x.IsDeleted == false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Send Mail
        public static bool SendMailForPswRest(string Subject, string Body, string ToMail, Guid siteId)
        {
            try
            {
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var result = new Masters().GetEmailSettingDetail(siteId);
                if (result != null)
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToMail);
                    message.Subject = string.IsNullOrEmpty(Subject) ? "Forgot Password!" : Subject;
                    message.IsBodyHtml = true;
                    message.Body = Body;
                    smtpClient.Send(message);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }

        //Send Mail
        public static bool SendMail(string Subject, string Body, string ToMail, Guid siteId)
        {
            try
            {
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                string Bcc = "tim@internationalrail.com,rahul.verma@dotsquares.com";
                var result = new Masters().GetEmailSettingDetail(siteId);
                if (result != null)
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToMail);
                    message.Bcc.Add(Bcc);
                    message.Subject = string.IsNullOrEmpty(Subject) ? "Forgot Password!" : Subject;
                    message.IsBodyHtml = true;
                    message.Body = Body;
                    smtpClient.Send(message);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }

        //SendAdmin email
        public static bool SendAdminMail(string Subject, string Body, string ToMail)
        {
            try
            {
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                // host Address
                string SmtpHost = System.Configuration.ConfigurationSettings.AppSettings["SmtpHost"];
                // user name
                string SmtpUser = System.Configuration.ConfigurationSettings.AppSettings["SmtpUser"];
                //Password 
                string SmtpPassword = System.Configuration.ConfigurationSettings.AppSettings["SmtpPassword"];
                int SmtpPort = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SmtpPort"]);
                // Address from where send the mail
                string FromEmail = System.Configuration.ConfigurationSettings.AppSettings["FromEmail"];
                MailAddress fromAddres = new MailAddress(FromEmail, FromEmail);

                smtpClient.Host = SmtpHost;
                smtpClient.Port = SmtpPort;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential(SmtpUser, SmtpPassword);

                message.From = fromAddres;
                message.To.Add(ToMail);
                message.Subject = "Forgot Password!";
                message.IsBodyHtml = true;
                message.Body = Body;
                smtpClient.Send(message);
                return true;
            }
            catch (Exception ee)
            {
                return false;
            }
        }


        #region tblUserlogin Operation
        public tblUserLogin UserLogin(string UserName, string Password, bool IsActive)
        {
            try
            {
                return db.tblUserLogins.FirstOrDefault(x => (x.Email == UserName && x.Password == Password && x.IsActive == IsActive));
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public tblUserLogin CheckEmailUser(string Email)
        {
            try
            {
                return db.tblUserLogins.FirstOrDefault(x => x.Email == Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LoginUserFields> GetAllUserLogin(Guid SiteId)
        {
            try
            {
                var list = (from Au in db.tblUserLogins
                            where Au.SiteId == SiteId
                            select Au).ToList();
                return (list.Select(x => new LoginUserFields
                {
                    ContactEmail = x.Email,
                    Country = x.tblCountriesMst.CountryName,
                    ID = x.ID,
                    IsActive = x.IsActive,
                    Name = x.FirstName + " " + x.LastName,
                    SiteName = GetSiteName(x.ID)
                })).AsEnumerable().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetSiteName(Guid? id)
        {
            var res = (from catSi in db.tblUserLogins
                       join ts in db.tblSites on catSi.SiteId equals ts.ID
                       where catSi.ID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";

            }
            return siteName;
        }
        public bool DeleteLoginUser(Guid ID)
        {
            try
            {
                var rec = db.tblUserLogins.FirstOrDefault(x => x.ID == ID);
                db.tblUserLogins.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool? ActivateInActivateUser(Guid ID)
        {
            bool? result = false;
            var affuser = db.tblUserLogins.Where(x => x.ID == ID).FirstOrDefault();
            if (affuser != null)
            {
                result = affuser.IsActive = !affuser.IsActive;

                db.SaveChanges();
            }
            return result;
        }
        public tblUserLogin GetUserbyID(Guid ID)
        {
            return db.tblUserLogins.FirstOrDefault(x => x.ID == ID);
        }

        public Guid SaveUser(tblUserLogin affUser)
        {
            try
            {
                var objaffUser = db.tblUserLogins.Where(o => o.ID == affUser.ID).FirstOrDefault();
                if (objaffUser != null)
                {
                    objaffUser.FirstName = affUser.FirstName;
                    objaffUser.LastName = affUser.LastName;
                    objaffUser.Phone = affUser.Phone;
                    objaffUser.Email = affUser.Email;
                    objaffUser.Dob = affUser.Dob;
                    objaffUser.Country = affUser.Country;
                    objaffUser.City = affUser.City;
                    objaffUser.State = affUser.State;
                    objaffUser.PostCode = affUser.PostCode;
                    objaffUser.IsActive = affUser.IsActive;
                    objaffUser.SiteId = affUser.SiteId;
                    objaffUser.Address = affUser.Address;
                    objaffUser.Address2 = affUser.Address2;
                    objaffUser.State = affUser.State;
                    objaffUser.IsNewsEmail = affUser.IsNewsEmail;
                    if (affUser.Password.Trim() != "")
                        objaffUser.Password = affUser.Password;
                }
                else if (!db.tblUserLogins.Any(x => x.Email == affUser.Email && x.SiteId == affUser.SiteId))
                    db.tblUserLogins.AddObject(affUser);
                else
                    return Guid.Empty;
                db.SaveChanges();
                return affUser.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region OneHubLogin Area
        public List<GetApiLogingDetail> GetApiLogingDetail(Guid siteId)
        {
            try
            {
                return db.GetApiLogingDetail(siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<GetApiLogingBySiteId> GetApiLogingBySiteId(Guid siteId)
        {
            try
            {
                return db.GetApiLogingBySiteId(siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInActiveApiLogingDetail(tblApiLoginDetailSiteLookup apiloginsite)
        {
            try
            {
                if (db.tblApiLoginDetailSiteLookups.Any(x => x.ApiAccountID == apiloginsite.ApiAccountID && x.SiteID == apiloginsite.SiteID))
                {
                    tblApiLoginDetailSiteLookup rec = db.tblApiLoginDetailSiteLookups.FirstOrDefault(x => x.ApiAccountID == apiloginsite.ApiAccountID && x.SiteID == apiloginsite.SiteID);
                    db.tblApiLoginDetailSiteLookups.DeleteObject(rec);
                }
                else
                {
                    //--Restricted if train name is exist then only one a/c is posible test ot producion
                    if (db.tblApiLoginDetailSiteLookups.Any(x => x.TrainName == apiloginsite.TrainName && x.SiteID == apiloginsite.SiteID))
                    {
                        tblApiLoginDetailSiteLookup rec = db.tblApiLoginDetailSiteLookups.FirstOrDefault(x => x.TrainName == apiloginsite.TrainName && x.SiteID == apiloginsite.SiteID);
                        db.tblApiLoginDetailSiteLookups.DeleteObject(rec);
                    }

                    db.tblApiLoginDetailSiteLookups.AddObject(apiloginsite);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public List<tblP2PSale> GetExistingUserOrderInfo(long OrderID)
        {
            try
            {
                var result = from A in db.tblP2PSale
                             join B in db.tblPassP2PSalelookup on A.ID equals B.PassSaleID
                             where B.OrderID == OrderID
                             select new { A, B };
                return result.ToList().Select(x => new tblP2PSale
                {
                    ID = x.A.ID,
                    From = x.A.From,
                    To = x.A.To,
                    DateTimeDepature = x.A.DateTimeDepature,
                    DepartureTime = x.A.DepartureTime,
                    P2PId = x.A.P2PId,
                    FromStation = x.A.FromStation,
                    ToStation = x.A.ToStation,
                    Adult = x.A.Adult,
                    Children = x.A.Children,
                    Youth = x.A.Youth,
                    Senior = x.A.Senior
                }).OrderBy(x => x.P2PId).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteExistingP2PSale(long OrderID)
        {
            try
            {
                var result = db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderID).ToList();
                if (result != null && result.Count > 0)
                {
                    var result5 = db.tblOrders.FirstOrDefault(x => x.OrderID == OrderID);
                    if (result5 != null)
                    {
                        result5.CreatedOn = DateTime.Now;
                        db.SaveChanges();
                    }

                    Guid[] Ids = result.ToList().Select(x => x.PassSaleID.Value).ToArray();
                    Guid TravellerId = result.ToList().FirstOrDefault().OrderTravellerID.Value;
                    db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderID).ToList().ForEach(db.tblPassP2PSalelookup.DeleteObject);
                    db.SaveChanges();

                    var result2 = db.tblP2PSale.Where(x => Ids.Contains(x.ID)).ToList();
                    if (result2 != null && result2.Count > 0)
                    {
                        db.tblP2PSale.Where(x => Ids.Contains(x.ID)).ToList().ForEach(db.tblP2PSale.DeleteObject);
                        db.SaveChanges();
                    }

                    var result3 = db.tblP2POtherTravellerLookup.Where(x => x.OrderID == OrderID).ToList();
                    if (result3 != null && result3.Count > 0)
                    {
                        Guid[] OrderTravellerIDs = result3.ToList().Select(x => x.OrderTravellerID.Value).ToArray();
                        db.tblP2POtherTravellerLookup.Where(x => x.OrderID == OrderID).ToList().ForEach(db.tblP2POtherTravellerLookup.DeleteObject);
                        db.SaveChanges();

                        var result4 = db.tblOrderTravellers.Where(x => OrderTravellerIDs.Contains(x.ID)).ToList();
                        if (result4 != null && result4.Count > 0)
                        {
                            db.tblOrderTravellers.Where(x => OrderTravellerIDs.Contains(x.ID)).ToList().ForEach(db.tblOrderTravellers.DeleteObject);
                            db.SaveChanges();
                        }
                    }
                    else if (TravellerId != Guid.Empty)
                    {
                        db.tblOrderTravellers.Where(x => x.ID == TravellerId).ToList().ForEach(db.tblOrderTravellers.DeleteObject);
                        db.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteExistingP2PSleeperEvolvi(long OrderID)
        {
            try
            {
                var result = db.tblEvolviSleeperPriceLookUps.Where(x => x.OrderId == OrderID).ToList();
                if (result != null && result.Count > 0)
                {
                    db.tblEvolviSleeperPriceLookUps.Where(x => x.OrderId == OrderID).ToList().ForEach(db.tblEvolviSleeperPriceLookUps.DeleteObject);
                    db.SaveChanges();
                }
                var result2 = db.tblEvolviOtherChargesLookUps.Where(x => x.OrderId == OrderID).ToList();
                if (result != null && result.Count > 0)
                {
                    db.tblEvolviOtherChargesLookUps.Where(x => x.OrderId == OrderID).ToList().ForEach(db.tblEvolviOtherChargesLookUps.DeleteObject);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public static class AdminuserInfo
    {
        public static string Username
        {
            get
            {
                if (HttpContext.Current.Session["Username"] != null)
                    return Convert.ToString(HttpContext.Current.Session["Username"].ToString());
                else
                    return "";
            }
            set { HttpContext.Current.Session["Username"] = value; }
        }

        public static Guid? RoleId
        {
            get
            {
                if (HttpContext.Current.Session["RoleId"] != null)
                    return Guid.Parse(HttpContext.Current.Session["RoleId"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["RoleId"] = value; }
        }
        public static Guid? BranchID
        {
            get
            {
                if (HttpContext.Current.Session["BranchID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["BranchID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["BranchID"] = value; }
        }

        public static Guid UserID
        {
            get
            {
                if (HttpContext.Current.Session["UserID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["UserID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["UserID"] = value; }
        }

        public static Guid SiteID
        {
            get
            {
                if (HttpContext.Current.Session["SiteID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["SiteID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["SiteID"] = value; }
        }
    }

    public static class USERuserInfo
    {
        public static string Username
        {
            get
            {
                if (HttpContext.Current.Session["USERUsername"] != null)
                    return Convert.ToString(HttpContext.Current.Session["USERUsername"].ToString());
                else
                    return "";
            }
            set { HttpContext.Current.Session["USERUsername"] = value; }
        }
        public static string UserEmail
        {
            get
            {
                if (HttpContext.Current.Session["USEREmailId"] != null)
                    return (HttpContext.Current.Session["USEREmailId"].ToString());
                else
                    return string.Empty;
            }
            set { HttpContext.Current.Session["USEREmailId"] = value; }
        }
        public static Guid ID
        {
            get
            {
                if (HttpContext.Current.Session["USERUserID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["USERUserID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["USERUserID"] = value; }
        }
        public static Guid SiteID
        {
            get
            {
                if (HttpContext.Current.Session["USERSiteID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["USERSiteID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["USERSiteID"] = value; }
        }
    }

    public static class AgentuserInfo
    {
        public static string Username
        {
            get
            {
                if (HttpContext.Current.Session["AgentUsername"] != null)
                    return Convert.ToString(HttpContext.Current.Session["AgentUsername"].ToString());
                else
                    return "";
            }
            set { HttpContext.Current.Session["AgentUsername"] = value; }
        }

        public static Guid? RoleId
        {
            get
            {
                if (HttpContext.Current.Session["AgentRoleId"] != null)
                    return Guid.Parse(HttpContext.Current.Session["AgentRoleId"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["AgentRoleId"] = value; }
        }

        public static Guid UserID
        {
            get
            {
                if (HttpContext.Current.Session["AgentUserID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["AgentUserID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["AgentUserID"] = value; }
        }

        public static Guid SiteID
        {
            get
            {
                if (HttpContext.Current.Session["AgentSiteID"] != null)
                    return Guid.Parse(HttpContext.Current.Session["AgentSiteID"].ToString());
                else
                    return Guid.Empty;
            }
            set { HttpContext.Current.Session["AgentSiteID"] = value; }
        }


        public static bool IsAffliate
        {
            get
            {
                if (HttpContext.Current.Session["IsAffliate"] != null)
                    return Boolean.Parse(HttpContext.Current.Session["IsAffliate"].ToString());
                else
                    return false;
            }
            set { HttpContext.Current.Session["IsAffliate"] = value; }
        }

    }

    public class AdminUser : tblAdminUser
    {
        public string Name { get; set; }
    }

    public class UserCancelOrderChats : tblUseCancelOrderChat
    {
        public string Name { get; set; }
        public string IsUser { get; set; }
    }

    public class LoginUserFields
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string ContactEmail { get; set; }
        public string Country { get; set; }
        public bool? IsActive { get; set; }
        public string SiteName { get; set; }
    }

    public class UseCancelOrderChatHistory
    {
        public int OrderId { get; set; }
        public string Name { get; set; }
        public string Commitment { get; set; }
        public DateTime CreatedOn { get; set; }
        public string IsUser { get; set; }
    }

    public class UseCancelOrder : tblUseCancelOrder
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string StatusName { get; set; }
        public int OrderId { get; set; }
        public int Status { get; set; }
        public bool IsActive { get; set; }
    }

    public class ApiLoginDetailSiteLookup : tblApiLoginDetailSiteLookup
    {
        public string ApiName { get; set; }
        public string SiteName { get; set; }
    }
}

