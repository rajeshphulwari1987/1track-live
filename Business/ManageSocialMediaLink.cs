﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{

    public class ManageSocialMediaLink
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        public string GetScriptBySiteID(Guid siteid)
        {
            try
            {
                var result = _db.tblSocialMediaScripts.FirstOrDefault(x => x.SiteID == siteid && x.IsActive);
                return result == null ? "" : result.Script;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SocialMediaSite> GetScriptsList()
        {
            try
            {
                var result = from s in _db.tblSites
                             join m in _db.tblSocialMediaScripts on s.ID equals m.SiteID
                             into temp
                             from left in temp.DefaultIfEmpty()
                             where s.IsActive == true
                             orderby s.DisplayName
                             select new SocialMediaSite
                             {
                                 IsActive = left == null ? false : left.IsActive,
                                 Script = left == null ? "" : left.Script,
                                 SiteID = s.ID,
                                 SiteName = s.DisplayName

                             };
                return result.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveScirpt(Guid siteId)
        {
            try
            {
                var result = _db.tblSocialMediaScripts.FirstOrDefault(t => t.SiteID == siteId);
                if (result != null)
                    result.IsActive = !result.IsActive;
                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddScirpts(tblSocialMediaScript script)
        {
            try
            {
                var result = _db.tblSocialMediaScripts.FirstOrDefault(t => t.SiteID == script.SiteID);

                if (result != null)
                {
                    result.Script = script.Script;
                    result.ModifiedBy = script.CreatedBy;
                    result.ModifiedOn = script.CreatedOn;
                }
                else
                    _db.AddTotblSocialMediaScripts(script);

                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public class SocialMediaSite
    {
        public string SiteName { get; set; }
        public Guid SiteID { get; set; }
        public string Script { get; set; }
        public bool IsActive { get; set; }
    }
}


