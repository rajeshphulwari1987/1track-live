﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageInterRailNew
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region Manage Help Link
        public bool AddHelp(tblHelp objtblHelp)
        {
            try
            {
                _db.tblHelps.AddObject(objtblHelp);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddUpdateHelp(tblHelp objtblHelp)
        {
            try
            {
                if (objtblHelp.Id == Guid.Empty)
                {
                    objtblHelp.Id = Guid.NewGuid();
                    _db.tblHelps.AddObject(objtblHelp);
                }
                else
                {
                    var tblHelp = _db.tblHelps.FirstOrDefault(m => m.Id == objtblHelp.Id);
                    tblHelp.Name = objtblHelp.Name;
                    tblHelp.Email = objtblHelp.Email;
                    tblHelp.Phone = objtblHelp.Phone;
                    tblHelp.Contact = objtblHelp.Contact;
                    tblHelp.SiteId = objtblHelp.SiteId;
                    tblHelp.CreatedOn = objtblHelp.CreatedOn;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteHelp(Guid id)
        {
            try
            {
                var v = _db.tblHelps.FirstOrDefault(x => x.Id == id);
                _db.tblHelps.DeleteObject(v);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblHelp> GettblHelpList(Guid SiteId)
        {
            try
            {
                return _db.tblHelps.Where(m => m.SiteId == SiteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblHelp GettblHelpById(Guid id)
        {
            try
            {
                return _db.tblHelps.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Manage Pass Query
        public bool AddPassQuery(tblPassQuery objtblPassQuery)
        {
            try
            {
                _db.tblPassQueries.AddObject(objtblPassQuery);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddUpdatetblPassQuery(tblPassQuery objtblPassQuery)
        {
            try
            {
                if (objtblPassQuery.Id == Guid.Empty)
                {
                    objtblPassQuery.Id = Guid.NewGuid();
                    _db.tblPassQueries.AddObject(objtblPassQuery);
                }
                else
                {
                    var tblPassQuery = _db.tblPassQueries.FirstOrDefault(m => m.Id == objtblPassQuery.Id);
                    tblPassQuery.Name = objtblPassQuery.Name;
                    tblPassQuery.Email = objtblPassQuery.Email;
                    tblPassQuery.Phone = objtblPassQuery.Phone;
                    tblPassQuery.Dob = objtblPassQuery.Dob;
                    tblPassQuery.TellUs = objtblPassQuery.TellUs;
                    tblPassQuery.IsEmail = objtblPassQuery.IsEmail;
                    tblPassQuery.SiteId = objtblPassQuery.SiteId;
                    tblPassQuery.CreatedOn = objtblPassQuery.CreatedOn;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeletetblPassQuery(Guid id)
        {
            try
            {
                var v = _db.tblPassQueries.FirstOrDefault(x => x.Id == id);
                _db.tblPassQueries.DeleteObject(v);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblPassQuery> GettblPassQueryList(Guid SiteId)
        {
            try
            {
                return _db.tblPassQueries.Where(m => m.SiteId == SiteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPassQuery GettblPassQueryById(Guid id)
        {
            try
            {
                return _db.tblPassQueries.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEmailAddress(Guid SiteId)
        {
            try
            {
                var result = _db.tblSites.FirstOrDefault(x => x.ID == SiteId);
                if (result != null)
                    return result.JourneyEmail;
                return "";
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region Manage Country
        public List<CountryClassNew> GetAllContinent(Guid SiteId)
        {
            try
            {
                var v = from tc in _db.tblContinents
                        join tcm in _db.tblCountriesMsts on tc.ID equals tcm.ContinentID
                        join tcsl in _db.tblCountrySiteLookUps on tcm.CountryID equals tcsl.CountryID
                        where tcsl.SiteID == SiteId && tc.IsActive == true
                        group tc by tc.Name into tcc
                        select new CountryClassNew
                        {
                            ContinentID = tcc.FirstOrDefault().ID,
                            ContinentName = tcc.Key
                        };
                return v.ToList().OrderBy(tc => tc.ContinentName).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<CountryClassNew> GetAllCountry(Guid SiteId, Guid ContinentId)
        {
            try
            {
                var result = new List<CountryClassNew>();
                var countrylist = from tcm in _db.tblCountriesMsts
                                  join tcsl in _db.tblCountrySiteLookUps on tcm.CountryID equals tcsl.CountryID
                                  where tcsl.SiteID == SiteId && tcm.IsActive == true && tcm.IsVisible == true && tcm.ContinentID == ContinentId
                                  select new CountryClassNew
                                  {
                                      CountryID = tcm.CountryID,
                                      CountryName = tcm.CountryName,
                                      CountryCode = tcm.CountryCode,
                                  };
                result = countrylist.ToList().OrderBy(tcm => tcm.CountryName).ToList();

                var results = from p in _db.tblCountriesMsts
                              group p.CountryName by p.CountryName into g
                              where g.Count() > 1
                              select new
                              {
                                  CountryName = g.Key
                              };


                var list = new List<CountryClassNew>();
                foreach (var item in result)
                {
                    list.Add(new CountryClassNew
                    {
                        CountryID = item.CountryID,
                        CountryName = item.CountryName,
                        CountryCode = item.CountryCode,
                        Url = results.Any(t => t.CountryName == item.CountryName) ? item.CountryID.ToString().Substring(0, 4) + "-" + item.CountryName.Replace(" - ", "-").Replace("/", "-").Replace("&", "and").Replace(" ", "-").Replace("'", "").ToLower() : item.CountryName.Replace(" - ", "-").Replace("/", "-").Replace("&", "and").Replace(" ", "-").Replace("'", "").ToLower()
                    });
                }
                return list;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<CountryList> GetHomePageCountryList(Guid SiteId)
        {
            try
            {
                return _db.GetHomePageAllCountryBySiteId(SiteId).Select(t => new CountryList { Name = t.NAME }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CountryList> GetHomePageCountryListFilterByContinent(string Name, Guid SiteID)
        {
            try
            {
                return _db.tblCountriesMsts.Where(x => x.tblContinent.Name == Name && x.tblCountrySiteLookUps.Any(o => o.CountryID == x.CountryID && o.SiteID == SiteID)).Select(t => new CountryList { Name = t.CountryName }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Manage Product

        public List<ProductPass> GetAllProductBySiteIdHashtagAdd(Guid siteId, string SearchText)
        {
            var currId = _db.tblSites.FirstOrDefault(x => x.ID == siteId).DefaultCurrencyID;

            List<ProductPass> result = _db.HashTagAdditionalSearch(siteId, SearchText, AgentuserInfo.UserID).Select(item => new ProductPass
            {
                IsFocAD75 = item.IsFOCAD75,
                ProductCode = item.ID.ToString(),
                ID = item.ID,
                CategoryName = item.CATEGORYNAME,
                Name = item.NAME,
                ProductImage = item.PRODUCTIMAGE,
                ProductCurrencyId = (Guid)item.PRODUCTCURRENCYID,
                Desciption = item.DESCIPTION,
                Price = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion((Decimal)item.PPRICE, siteId, item.PRODUCTCURRENCYID, currId.Value))),
                IsFlag = Convert.ToBoolean(item.ISFLAG),
                IsSpecialOffer = Convert.ToBoolean(item.ISSPECIALOFFER),
                BestSeller = Convert.ToBoolean(item.BESTSELLER),
                //IsTopPass = (bool)item.ISTOPRAILPASS
            }).ToList();

            var results = from p in _db.tblProductNames
                          group p.Name by p.Name into g
                          where g.Count() > 1
                          select new
                          {
                              ProductName = g.Key
                          };
            List<ProductPass> list = result.Select(t => new ProductPass
            {
                IsTopPass = t.IsTopPass,
                IsFocAD75 = t.IsFocAD75,
                ProductCode = t.ID.ToString(),
                ID = t.ID,
                Name = t.Name,
                ProductCurrencyId = t.ProductCurrencyId,
                CategoryName = t.CategoryName,
                ProductImage = t.ProductImage,
                Desciption = t.Desciption,
                Price = t.Price,
                IsFlag = t.IsFlag,
                IsSpecialOffer = t.IsSpecialOffer,
                BestSeller = t.BestSeller,
                Url = t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower()
            }).ToList();
            return list;
        }
        public List<ProductPass> GetAllProductBySiteIdHashtagNew(Guid siteId, string SearchText)
        {
            var currId = _db.tblSites.FirstOrDefault(x => x.ID == siteId).DefaultCurrencyID;

            List<ProductPass> result = _db.GetAllProductBySiteIdHashtag(siteId, SearchText, AgentuserInfo.UserID).Select(item => new ProductPass
            {
                IsFocAD75 = item.IsFOCAD75,
                ProductCode = item.ID.ToString(),
                ID = item.ID,
                CategoryName = item.CATEGORYNAME,
                Name = item.NAME,
                ProductImage = item.PRODUCTIMAGE,
                ProductCurrencyId = (Guid)item.PRODUCTCURRENCYID,
                Desciption = item.DESCIPTION,
                Price = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion((Decimal)item.PPRICE, siteId, item.PRODUCTCURRENCYID, currId.Value))),
                IsFlag = Convert.ToBoolean(item.ISFLAG),
                IsSpecialOffer = Convert.ToBoolean(item.ISSPECIALOFFER),
                BestSeller = Convert.ToBoolean(item.BESTSELLER),
                IsTopPass = (bool)item.ISTOPRAILPASS
            }).ToList();


            List<ProductPass> list = result.Select(t => new ProductPass
            {
                IsTopPass = t.IsTopPass,
                IsFocAD75 = t.IsFocAD75,
                ProductCode = t.ID.ToString(),
                ID = t.ID,
                Name = t.Name,
                ProductCurrencyId = t.ProductCurrencyId,
                CategoryName = t.CategoryName,
                ProductImage = t.ProductImage,
                Desciption = t.Desciption,
                Price = t.Price,
                IsFlag = t.IsFlag,
                IsSpecialOffer = t.IsSpecialOffer,
                BestSeller = t.BestSeller,
                Url = t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower()
            }).ToList();
            return list;
        }
        public List<ProductPass> GetAllProductPass(Guid siteId, string SearchText,int Filter=0)
        {
            var result = new List<ProductPassName>();
            foreach (var item in _db.GetAllProductBySiteId(siteId, SearchText, AgentuserInfo.UserID, Filter))
            {
                result.Add(new ProductPassName
                {
                    CategoryID = item.CATEGORYID,
                    CategoryName = item.CATEGORYNAME,
                    ID = item.ID,
                    Name = item.NAME,
                    ProductImage = item.PRODUCTIMAGE,
                    Desciption = item.DESCIPTION,
                    Price = item.PPRICE.ToString(),
                });
            }

            var results = from p in _db.tblCategoriesNames
                          group p.Name by p.Name into g
                          where g.Count() > 1
                          select new
                          {
                              CatName = g.Key
                          };

            var list = new List<ProductPass>();
            foreach (var item in result)
            {
                list.Add(new ProductPass
                {
                    CategoryID = item.CategoryID,
                    CategoryName = item.CategoryName,
                    ID = item.ID,
                    Name = item.Name,
                    ProductImage = item.ProductImage,
                    Desciption = item.Desciption,
                    Price = item.Price.ToString(),
                    Url = results.Any(y => y.CatName == item.CategoryName) ?
                          item.CategoryID.ToString().Substring(0, 4) + "-" + item.CategoryName.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                          (item.ID.ToString().Substring(0, 4) + "-" + item.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower())
                           : item.CategoryName.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                          (item.ID.ToString().Substring(0, 4) + "-" + item.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower())
                });
            }
            return list;
        }
        public List<ProductPass> GetAllORSelectedCountryProductPass(Guid siteId, string SearchCountrys)
        {
            var currId = _db.tblSites.FirstOrDefault(x => x.ID == siteId).DefaultCurrencyID;
            List<ProductPass> result = _db.AllCountryHomeProductBySiteId(siteId, SearchCountrys, AgentuserInfo.UserID).Select(item => new ProductPass
            {
                ProductCode = item.ID.ToString(),
                ID = item.ID,
                CategoryName = item.CATEGORYNAME,
                Name = item.NAME,
                ProductImage = item.PRODUCTIMAGE,
                ProductCurrencyId = (Guid)item.PRODUCTCURRENCYID,
                Desciption = item.DESCIPTION,
                Price = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion((Decimal)item.PPRICE, siteId, item.PRODUCTCURRENCYID.Value, currId.Value))),
                IsFlag = Convert.ToBoolean(item.ISFLAG),
                IsSpecialOffer = Convert.ToBoolean(item.ISSPECIALOFFER),
                BestSeller = Convert.ToBoolean(item.BESTSELLER)
            }).ToList();

            List<ProductPass> list = result.Select(t => new ProductPass
            {
                ProductCode = t.ID.ToString(),
                ID = t.ID,
                Name = t.Name,
                ProductCurrencyId = t.ProductCurrencyId,
                CategoryName = t.CategoryName,
                ProductImage = t.ProductImage,
                Desciption = t.Desciption,
                Price = t.Price,
                IsFlag = t.IsFlag,
                IsSpecialOffer = t.IsSpecialOffer,
                BestSeller = t.BestSeller,
                Url = t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "").ToLower()
            }).ToList();
            return list;
        }
        public List<ProductPass> GetAllProductPassNew(Guid siteId, string SearchText, int Filter=0)
        {
            var currId = _db.tblSites.FirstOrDefault(x => x.ID == siteId).DefaultCurrencyID;

            List<ProductPass> result = _db.GetAllProductBySiteId(siteId, SearchText, AgentuserInfo.UserID, Filter).Select(item => new ProductPass
            {
                IsFocAD75 = item.IsFOCAD75,
                ProductCode = item.ID.ToString(),
                ID = item.ID,
                CategoryName = item.CATEGORYNAME,
                Name = item.NAME,
                ProductImage = item.PRODUCTIMAGE,
                ProductCurrencyId = (Guid)item.PRODUCTCURRENCYID,
                Desciption = item.DESCIPTION,
                Price = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion((Decimal)item.PPRICE, siteId, item.PRODUCTCURRENCYID, currId.Value))),
                IsFlag = Convert.ToBoolean(item.ISFLAG),
                IsSpecialOffer = Convert.ToBoolean(item.ISSPECIALOFFER),
                BestSeller = Convert.ToBoolean(item.BESTSELLER),
                IsTopPass = (bool)item.ISTOPRAILPASS
            }).ToList();


            List<ProductPass> list = result.Select(t => new ProductPass
                {
                    IsTopPass = t.IsTopPass,
                    IsFocAD75 = t.IsFocAD75,
                    ProductCode = t.ID.ToString(),
                    ID = t.ID,
                    Name = t.Name,
                    ProductCurrencyId = t.ProductCurrencyId,
                    CategoryName = t.CategoryName,
                    ProductImage = t.ProductImage,
                    Desciption = t.Desciption,
                    Price = t.Price,
                    IsFlag = t.IsFlag,
                    IsSpecialOffer = t.IsSpecialOffer,
                    BestSeller = t.BestSeller,
                    Url = t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower()
                }).ToList();
            return list;
        }
        public ProductDetails GetProductByProductId(Guid ProductId, Guid siteId)
        {
            var result = (from tp in _db.tblProducts
                          join tpn in _db.tblProductNames on tp.ID equals tpn.ProductID
                          join tpclp in _db.tblProductCategoriesLookUps on tpn.ProductID equals tpclp.ProductID
                          join tcn in _db.tblCategoriesNames on tpclp.CategoryID equals tcn.CategoryID
                          join tpslp in _db.tblProductSiteLookUps on siteId equals tpslp.SiteID
                          where tpn.ProductID == ProductId
                          select new ProductDetails
                          {
                              Id = tpn.ProductID,
                              Name = tpn.Name,
                              CurrencyID = tp.CurrencyID,
                              CategoryId = tcn.CategoryID,
                              CategoryName = tcn.Name,
                              Description = tp.Description,
                              ProductImage = tp.ProductImage,
                              ProductImageSecond = tp.ProductImageSecond,
                              BannerImage = tp.BannerImage,
                              SpecialOfferText = tp.SpecialOfferText,
                              Announcement = tp.Announcement,
                              AnnouncementTitle = tp.AnnouncementTitle,
                              ProductAltTag1 = tp.ProductAltTag1,
                              ProductAltTag2 = tp.ProductAltTag2,
                              IrTitle = tp.IrTitle,
                              IrDescription = tp.IrDesciption
                          }).FirstOrDefault();

            var results = from p in _db.tblCategoriesNames
                          group p.Name by p.Name into g
                          where g.Count() > 1
                          select new
                          {
                              CatName = g.Key
                          };


            return new ProductDetails
            {
                ProductAltTag1 = result.ProductAltTag1,
                ProductAltTag2 = result.ProductAltTag2,
                CategoryId = result.CategoryId,
                CategoryName = result.CategoryName,
                Id = result.Id,
                Name = result.Name,
                CurrencyID = result.CurrencyID,
                ProductImage = result.ProductImage,
                ProductImageSecond = result.ProductImageSecond,
                Description = result.Description,
                BannerImage = result.BannerImage,
                SpecialOfferText = result.SpecialOfferText,
                Announcement = result.Announcement,
                AnnouncementTitle = result.AnnouncementTitle,
                IrTitle = result.IrTitle,
                IrDescription = result.IrDescription,
                Price = (decimal)_db.HomeCountryPriceById(siteId, result.Id).FirstOrDefault().RESULT, //result.Price,
                Url = results.Any(y => y.CatName == result.CategoryName) ?
                         result.CategoryId.ToString().Substring(0, 4) + "-" + result.CategoryName.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "") + "/" +
                         (result.Id.ToString().Substring(0, 4) + "-" + result.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "").Replace("&", "and").ToLower())
                          : result.CategoryName.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "") + "/" +
                         (result.Id.ToString().Substring(0, 4) + "-" + result.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "").Replace("&", "and").ToLower())
            };
        }
        public ProductPass GetSpecialBestSeller(Guid ProductId)
        {
            var data = _db.tblProducts.Where(x => x.ID == ProductId).Select(x => new ProductPass
            {
                IsSpecialOffer = x.IsSpecialOffer,
                BestSeller = x.BestSeller
            }).FirstOrDefault();
            return data;
        }
        #endregion

        #region Manage Contact Us Link
        public List<ManageAdsenceLinkField> GetManageAdsenceLinkList(Guid SiteId)
        {
            try
            {
                return _db.tblManageAdsenceLinks.Where(x => x.SiteId == SiteId).Select(m => new ManageAdsenceLinkField
                {
                    Id = m.Id,
                    SiteName = m.tblSite.DisplayName,
                    SiteId = m.SiteId,
                    Name = m.Name,
                    Url = m.Url,
                    IsActive = m.IsActive
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddManageAdsenceLink(tblManageAdsenceLink setting)
        {
            try
            {
                if (setting.Id == Guid.Empty)
                {
                    setting.Id = Guid.NewGuid();
                    if (_db.tblManageAdsenceLinks.Any(m => m.LinkId == setting.LinkId && m.SiteId == setting.SiteId))
                        throw new Exception("Name is already exist for this setting.");
                    _db.tblManageAdsenceLinks.AddObject(setting);
                }
                else
                {
                    var tblManageAdsenceLink = _db.tblManageAdsenceLinks.FirstOrDefault(m => m.Id == setting.Id);
                    tblManageAdsenceLink.ModifyBy = setting.CreatedBy;
                    tblManageAdsenceLink.ModifyOn = setting.CreatedOn;
                    tblManageAdsenceLink.IsActive = setting.IsActive;
                    tblManageAdsenceLink.Name = setting.Name;
                    tblManageAdsenceLink.Url = setting.Url;
                    tblManageAdsenceLink.SiteId = setting.SiteId;
                    tblManageAdsenceLink.LinkId = setting.LinkId;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteManageAdsenceLink(Guid id)
        {
            try
            {
                var v = _db.tblManageAdsenceLinks.FirstOrDefault(x => x.Id == id);
                _db.tblManageAdsenceLinks.DeleteObject(v);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveManageAdsenceLink(Guid id)
        {
            try
            {
                tblManageAdsenceLink setting = _db.tblManageAdsenceLinks.FirstOrDefault(m => m.Id == id);
                setting.IsActive = !setting.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblManageAdsenceLink GetManageAdsenceLinkById(Guid id)
        {
            try
            {
                return _db.tblManageAdsenceLinks.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblManageAdsenceLink GetManageAdsenceLinkBySiteId(Guid SiteId, int LinkId)
        {
            try
            {
                return _db.tblManageAdsenceLinks.FirstOrDefault(x => x.SiteId == SiteId && x.LinkId == LinkId && x.IsActive == true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public class ManageAdsenceLinkField : tblManageAdsenceLink
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region Cross sale Adsense
        public Guid AddCrossSaleAdsense(tblCrossSaleAdsense CrossSaleAdsense)
        {
            try
            {
                _db.AddTotblCrossSaleAdsenses(CrossSaleAdsense);
                _db.SaveChanges();
                return CrossSaleAdsense.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddCrossSaleAdsenseSiteLookUp(tblCrossSaleAdsenseSiteLookUp CrossSaleAdsenseSiteLookUp)
        {
            try
            {
                _db.tblCrossSaleAdsenseSiteLookUps.AddObject(CrossSaleAdsenseSiteLookUp);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateCrossSaleAdsense(tblCrossSaleAdsense CrossSaleAdsense)
        {
            try
            {
                var _CrossSaleAdsense = _db.tblCrossSaleAdsenses.FirstOrDefault(x => x.Id == CrossSaleAdsense.Id);
                if (_CrossSaleAdsense != null)
                {
                    _CrossSaleAdsense.URLLink = CrossSaleAdsense.URLLink;
                    _CrossSaleAdsense.ClassId = CrossSaleAdsense.ClassId;
                    _CrossSaleAdsense.Name = CrossSaleAdsense.Name;
                    _CrossSaleAdsense.Price = CrossSaleAdsense.Price;
                    _CrossSaleAdsense.ShortDescription = CrossSaleAdsense.ShortDescription;
                    _CrossSaleAdsense.LongDescription = CrossSaleAdsense.LongDescription;
                    _CrossSaleAdsense.ModifyBy = CrossSaleAdsense.ModifyBy;
                    _CrossSaleAdsense.ModifyOn = CrossSaleAdsense.ModifyOn;
                    _CrossSaleAdsense.IsActive = CrossSaleAdsense.IsActive;
                    if (!string.IsNullOrEmpty(CrossSaleAdsense.AdsenceImage))
                        _CrossSaleAdsense.AdsenceImage = CrossSaleAdsense.AdsenceImage;
                }
                _db.SaveChanges();
                return _CrossSaleAdsense.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCrossSaleAdsense GetCrossSaleAdsenseListEdit(Guid ID)
        {
            try
            {
                return _db.tblCrossSaleAdsenses.FirstOrDefault(x => x.Id == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveCrossSaleAdsense(Guid id)
        {
            try
            {
                var rec = _db.tblCrossSaleAdsenses.FirstOrDefault(x => x.Id == id);
                rec.IsActive = !rec.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CrossSaleAdsenseInfo> GetCrossSaleAdsenseInfoList(Guid SiteId, Guid ProductId)
        {
            var result = (from tcs in _db.tblCrossSaleAdsenses
                          join tcsl in _db.tblCrossSaleAdsenseSiteLookUps on tcs.Id equals tcsl.AdsenseId
                          join tcspl in _db.tblCrossSaleProductLookups on tcs.Id equals tcspl.CrossSaleAdsenseId
                          join tc in _db.tblClassMsts on tcs.ClassId equals tc.ID
                          where tcsl.SiteId == SiteId && tcs.IsActive && tcspl.ProductId == ProductId
                          select new { tcs, tc }).Select(t => new CrossSaleAdsenseInfo
                                           {
                                               URLLink = t.tcs.URLLink,
                                               ClassName = t.tc.Name,
                                               Name = t.tcs.Name,
                                               Price = t.tcs.Price,
                                               ShortDescription = t.tcs.ShortDescription,
                                               LongDescription = t.tcs.LongDescription,
                                               BannerImg = t.tcs.AdsenceImage
                                           }).OrderBy(x => x.ClassName).ToList();
            return result;
        }
        #endregion
    }
    #region Manage Classes
    public class CountryList
    {
        public string Name { get; set; }
    }
    public class CountryClassNew
    {
        public Guid CountryID { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public Guid? ContinentID { get; set; }
        public string ContinentName { get; set; }
        public string Url { get; set; }
    }
    public class ProductPassClass
    {
        public Guid Id { get; set; }
        public Guid CountryId { get; set; }
        public Guid ContinentId { get; set; }
        public string Name { get; set; }
        public string ContinentName { get; set; }
        public string CountryName { get; set; }
        public string ProductImage { get; set; }
    }
    public class HelpClass
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Guid SiteId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Contact { get; set; }
    }
    public class PassQueryClass
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Guid SiteId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TellUs { get; set; }
        public string DOB { get; set; }
        public Boolean IsEmail { get; set; }
    }

    public class ProductPass
    {
        public bool IsTopPass { get; set; }
        public bool IsFocAD75 { get; set; }
        public int Count { get; set; }
        public string ProductID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid ProductCurrencyId { get; set; }
        public String CategoryName { get; set; }
        public Guid ID { get; set; }
        public String Name { get; set; }
        public Guid ContinentId { get; set; }
        public String Price { get; set; }
        public String Desciption { get; set; }
        public String ProductImage { get; set; }
        public String Url { get; set; }
        public String ProductCode { get; set; }
        public Boolean IsFlag { get; set; }
        public Boolean IsSpecialOffer { get; set; }
        public Boolean BestSeller { get; set; }
    }
    public class ProductPassName
    {
        public Guid CategoryID { get; set; }
        public String CategoryName { get; set; }
        public Guid ID { get; set; }
        public String Name { get; set; }
        public String Price { get; set; }
        public String Desciption { get; set; }
        public String ProductImage { get; set; }
    }

    public class ProductDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CategoryId { get; set; }
        public Guid CurrencyID { get; set; }
        public string CategoryName { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageSecond { get; set; }
        public string BannerImage { get; set; }
        public string Description { get; set; }
        public string SpecialOfferText { get; set; }
        public string AnnouncementTitle { get; set; }
        public string Announcement { get; set; }
        public string Url { get; set; }
        public decimal Price { get; set; }
        public string ProductAltTag1 { get; set; }
        public string ProductAltTag2 { get; set; }
        public string IrTitle { get; set; }
        public string IrDescription { get; set; }
    }

    public class CrossSaleAdsenseInfo
    {
        public string URLLink { get; set; }
        public string ClassName { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string BannerImg { get; set; }
    }
    #endregion
}
