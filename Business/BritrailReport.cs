﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class BritrailReport
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region BritrailSummaryTwoReport
        public List<BritrailSummaryTwoReport> GetBritrailSummaryTwoReport(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSummaryTwoReport(startdate, enddate, IsSta, siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementTwoReport
        public List<BritrailSettlement> GetBritrailSettlementTwoReport(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSettlementTwoReport(startdate, enddate, IsSta, siteId).
                    Select(t => new BritrailSettlement
                    {
                        Id = t.ID.Value,
                        Title = t.TITLE,
                        Amount = t.AMOUNT.Value
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailPassSaleTwoReport
        public List<BritrailPassSaleTwoReport> GetBritrailPassSaleTwoReport(Guid siteid, DateTime From, DateTime To, Int32 IsSta, String CategoryName, String PassCode, String Defination, String Class, String duration, String type)
        {
            try
            {
                return _db.BritrailPassSaleTwoReport(siteid, From, To, IsSta, CategoryName, PassCode, Defination, Class, duration, type).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritRailStockReturnReport
        public List<GetBritrailPrintqueue> GetBritrailPrintqueue(Guid Userid)
        {
            try
            {
                return _db.GetBritrailPrintqueue(Userid).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BritrailStockReturnReports> BritRailStockReturnReport(string Queueids, DateTime startdate, DateTime enddate)
        {
            try
            {
                return _db.BritrailStockReturnReports(Queueids, startdate, enddate).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSummaryReport
        public List<Britrailsummaryreport> GetBritrailsummaryreport(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.Britrailsummaryreport(startdate, enddate, IsSta, siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSummaryReport 2016
        public List<BritrailSummaryReport2016> GetBritrailsummaryreport2016(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSummaryReport2016(startdate, enddate, IsSta, siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementReport 2016
        public List<BritrailSettlement> GetBritrailSettlementReport2016(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSettlementReport2016(startdate, enddate, IsSta, siteId).
                    Select(t => new BritrailSettlement
                    {
                        Id = t.ID.Value,
                        Title = t.TITLE,
                        Amount = t.AMOUNT.Value
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementReport 2016
        public List<BRITRAILPASSSALE2016> GetBritrailPassSale2016(Guid siteid, DateTime From, DateTime To, Int32 IsSta)
        {
            try
            {
                return _db.BRITRAILPASSSALE2016(siteid, From, To, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSummaryReport 2017
        public List<BritrailSummaryReport2017_Result> GetBritrailsummaryreport2017(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSummaryReport2017(startdate, enddate, IsSta, siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementReport 2017
        public List<BritrailSettlement> GetBritrailSettlementReport2017(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSettlementReport2017(startdate, enddate, IsSta, siteId).
                    Select(t => new BritrailSettlement
                    {
                        Id = t.ID.Value,
                        Title = t.TITLE,
                        Amount = t.AMOUNT.Value
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementReport 2017
        public List<BRITRAILPASSSALE2017_Result> GetBritrailPassSale2017(Guid siteid, DateTime From, DateTime To, Int32 IsSta)
        {
            try
            {
                return _db.BRITRAILPASSSALE2017(siteid, From, To, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementReport
        public List<BritrailSettlement> GetBritrailSettlementReport(DateTime startdate, DateTime enddate, Int32 IsSta, Guid siteId)
        {
            try
            {
                return _db.BritrailSettlementReport(startdate, enddate, IsSta, siteId).
                    Select(t => new BritrailSettlement
                    {
                        Id = t.ID.Value,
                        Title = t.TITLE,
                        Amount = t.AMOUNT.Value
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailEurailStockReport
        public List<BritrailStockReport> GetBritrailStockReport(Guid SiteId, DateTime startdate, DateTime enddate, string OfficeId, Boolean IsSta)
        {
            try
            {
                return _db.BritrailStockReport(SiteId, OfficeId, startdate, enddate, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region BritrailSettlementReport
        public List<BritrailPassSale> GetBritrailPassSale(Guid siteid, DateTime From, DateTime To, Int32 IsSta, String CategoryName, String PassCode, String Defination, String Class, String duration, String type)
        {
            try
            {
                return _db.BritrailPassSale(siteid, From, To, IsSta, CategoryName, PassCode, Defination, Class, duration, type).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public String GetSiteCurrCode(Guid ID)
        {
            try
            {
                var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == ID);
                if (currencyID != null)
                    return new FrontEndManagePass().GetCurrency(currencyID.DefaultCurrencyID.Value);
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
