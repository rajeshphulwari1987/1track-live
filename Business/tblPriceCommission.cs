//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPriceCommission
    {
        public System.Guid ID { get; set; }
        public decimal Commission { get; set; }
        public System.Guid OfficeID { get; set; }
        public Nullable<System.Guid> CategoryID { get; set; }
        public Nullable<System.Guid> ProductID { get; set; }
        public Nullable<System.Guid> ProductPriceID { get; set; }
    
        public virtual tblBranch tblBranch { get; set; }
        public virtual tblCategory tblCategory { get; set; }
        public virtual tblProduct tblProduct { get; set; }
    }
}
