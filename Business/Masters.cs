﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Data.OleDb;
using System.Data;
using System.Net.Mail;
using System.Net;

namespace Business
{
    public class Masters
    {
        db_1TrackEntities db = new db_1TrackEntities();

        public bool isBritRailPromoPass(Guid ProductID)
        {
            try
            {
                var result = (from TC in db.tblCategories
                              join TPCL in db.tblProductCategoriesLookUps on TC.ID equals TPCL.CategoryID
                              join TP in db.tblProducts on TPCL.ProductID equals TP.ID
                              where TP.ID == ProductID && TC.IsBritRailPass == true && TP.IsPromoPass == true
                              select new { TC, TP, TPCL }).FirstOrDefault();
                if (result != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) { throw ex; }
        }
        public bool GetSystemReportByUserId(Guid ID)
        {
            try
            {
                return db.aspnet_Roles.FirstOrDefault(t => t.RoleId == (db.tblAdminUsers.FirstOrDefault(x => x.ID == ID).RoleID)).SystemReport;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCountriesMst> GetPassShippingAllCountryList(Guid SiteID)
        {
            try
            {
                var result = (from TS in db.tblShippings
                              join TSC in db.tblPassShippingOptionCountries
                                   on TS.ID equals TSC.ShippingID
                              join TCM in db.tblCountriesMsts
                                   on TSC.CountryID equals TCM.CountryID
                              where TS.IsActive == true && TS.IsVisibleFront == true && TS.SiteID == SiteID
                              select new { TCM }).ToList();

                return result.GroupBy(x => new { x.TCM.CountryID, x.TCM.CountryName }).Select(y => new tblCountriesMst
                {
                    CountryID = y.Key.CountryID,
                    CountryName = y.Key.CountryName
                }).ToList().OrderBy(x => x.CountryName).ToList(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region WorldPay Setting
        public List<tblWorldPayMst> GetWorldPayList(Guid siteID)
        {
            try
            {
                var list = (from world in db.tblWorldPayMsts
                            where world.SiteID == siteID
                            select world).OrderBy(x => x.CreatedOn).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddWorldPaySetting(tblWorldPayMst world)
        {
            try
            {
                var _world = new tblWorldPayMst
                {
                    ID = world.ID,
                    SiteID = world.SiteID,
                    MerchantCode = world.MerchantCode,
                    InstallationId = world.InstallationId,
                    UserName = world.UserName,
                    Password = world.Password,
                    WorldPayURL = world.WorldPayURL,
                    Language = world.Language,
                    Currency = world.Currency,
                    CreatedBy = world.CreatedBy,
                    CreatedOn = System.DateTime.Now,
                    IsActive = world.IsActive,
                    IsEnableThreeD = world.IsEnableThreeD
                };
                db.AddTotblWorldPayMsts(_world);
                db.SaveChanges();
                return _world.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblWorldPayMst GetWorldPayEdit(Guid ID)
        {
            try
            {
                return db.tblWorldPayMsts.FirstOrDefault(x => x.ID == ID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateWorldPay(tblWorldPayMst world)
        {
            try
            {
                var _world = db.tblWorldPayMsts.FirstOrDefault(x => x.ID == world.ID);
                if (_world != null)
                {
                    _world.ID = world.ID;
                    _world.MerchantCode = world.MerchantCode;
                    _world.InstallationId = world.InstallationId;
                    _world.UserName = world.UserName;
                    _world.Password = world.Password;
                    _world.WorldPayURL = world.WorldPayURL;
                    _world.Language = world.Language;
                    _world.Currency = world.Currency;
                    _world.ModifiedBy = world.ModifiedBy;
                    _world.ModifiedOn = world.ModifiedOn;
                    _world.IsActive = world.IsActive;
                    _world.IsEnableThreeD = world.IsEnableThreeD;
                };
                db.SaveChanges();
                return _world.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveWorldPay(Guid id)
        {
            try
            {
                var objWorldPay = db.tblWorldPayMsts.FirstOrDefault(x => x.ID == id);
                if (objWorldPay != null)
                    objWorldPay.IsActive = !objWorldPay.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteWorldPay(Guid id)
        {
            try
            {
                var rec = db.tblWorldPayMsts.FirstOrDefault(x => x.ID == id);
                db.tblWorldPayMsts.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblWorldPayMst GetWorldPayDetails(Guid Id)
        {
            try
            {
                return db.tblWorldPayMsts.FirstOrDefault(x => x.SiteID == Id && (bool)x.IsActive);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
        #region Table Layout
        public List<tblLayout> GetLayoutList()
        {
            try
            {
                return db.tblLayouts.Where(y => y.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblPageLayout> GetPageLayoutList()
        {
            try
            {
                return db.tblPageLayouts.Where(y => y.IsActive == true).OrderBy(y => y.LayoutName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public PageLayoutList GetPageLayout(int SiteType, string SiteName, Guid LayoutId)
        {
            try
            {
                var pageLayout = from TL in db.tblPageLayouts
                                 join TLN in db.tblPageLayoutNames on TL.ID equals TLN.LayoutId
                                 where TL.IsActive == true && TLN.SiteName == SiteName && TLN.SiteType == SiteType && TLN.LayoutId == LayoutId
                                 select new PageLayoutList()
                                 {
                                     ID = TLN.ID,
                                     Height = (int)TLN.Height,
                                     Width = (int)TLN.Width,
                                     LayoutName = TL.LayoutName,
                                     LayoutPath = TLN.LayoutPath,
                                     LayoutId = TL.ID,
                                     SiteName = TLN.SiteName,
                                     SiteType = (int)TLN.SiteType,
                                     ImageUrl = TLN.ImageUrl
                                 };
                return pageLayout.FirstOrDefault();
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region Documents
        public int AddFile(tblDocument tdoc)
        {
            try
            {
                var doc = new tblDocument
                {
                    CreatedBy = tdoc.CreatedBy,
                    CreatedOn = DateTime.Now,
                    FileName = tdoc.FileName,
                    FileType = tdoc.FileType,
                    NewFileName = tdoc.NewFileName
                };

                db.AddTotblDocuments(doc);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblDocument> GetFileList()
        {
            try
            {
                return db.tblDocuments.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblDocument GetFileById(Int32 id)
        {
            try
            {
                return db.tblDocuments.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool DeleteFile(Int32 id)
        {
            try
            {
                tblDocument doc = db.tblDocuments.FirstOrDefault(x => x.ID == id);
                if (doc != null)
                    db.tblDocuments.DeleteObject(doc);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion

        #region Currency Master Section
        public int AddCurrency(Currency currency)
        {
            try
            {
                tblCurrencyMst _currency = new tblCurrencyMst
                {
                    Name = currency.Name,
                    Symbol = currency.Symbol,
                    HTMLCode = currency.HTMLCode,
                    ShortCode = currency.ShortCode,
                    IsActive = currency.IsActive,
                    ID = currency.ID
                };
                db.AddTotblCurrencyMsts(_currency);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateCurrency(Currency currency)
        {
            try
            {
                var _currency = db.tblCurrencyMsts.FirstOrDefault(x => x.ID == currency.ID);
                if (_currency != null)
                {
                    _currency.Name = currency.Name;
                    _currency.Symbol = currency.Symbol;
                    _currency.HTMLCode = currency.HTMLCode;
                    _currency.ShortCode = currency.ShortCode;
                    _currency.IsActive = currency.IsActive;
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void DeleteCrossSaleProductLookup(Guid ID)
        {
            try
            {
                db.tblCrossSaleProductLookups.Where(t => t.Id == ID).ToList().ForEach(db.tblCrossSaleProductLookups.DeleteObject);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CrossSaleProduct> GetCrossSaleProductLookup(Guid Productid)
        {
            try
            {
                return db.tblCrossSaleProductLookups.Join(db.tblCrossSaleAdsenses, o => o.CrossSaleAdsenseId, i => i.Id, (t1, t2) => new { t1.Id, t1.CrossSaleAdsenseId, t1.ProductId, t2.IsActive, t2.Name, t2.ClassId, t2.Price }).Where(t => t.ProductId == Productid).Select(t => new CrossSaleProduct
                {
                    Id = t.Id,
                    CrossSaleAdsenseId = t.CrossSaleAdsenseId,
                    ProductId = t.ProductId,
                    IsActive = t.IsActive,
                    Class = db.tblClassMsts.FirstOrDefault(x => x.ID == t.ClassId).Name,
                    Name = t.Name,
                    Price = (decimal)t.Price
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddCrossSaleProductLookup(tblCrossSaleProductLookup obj)
        {
            try
            {
                db.tblCrossSaleProductLookups.Where(t => t.ProductId == obj.ProductId && t.CrossSaleAdsenseId == obj.CrossSaleAdsenseId).ToList().ForEach(db.tblCrossSaleProductLookups.DeleteObject);
                db.tblCrossSaleProductLookups.AddObject(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCrossSaleAdsense> GetCrossSaleAdsense()
        {
            try
            {
                return db.tblCrossSaleAdsenses.Where(t => t.IsActive).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCurrencyMst> GetCurrencyList()
        {
            try
            {
                return db.tblCurrencyMsts.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblCurrencyMst GetCurrencyListEdit(Guid ID)
        {
            try
            {
                return db.tblCurrencyMsts.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInActiveCurrency(Guid ID)
        {
            var currency = db.tblCurrencyMsts.Where(x => x.ID == ID).FirstOrDefault();
            if (currency != null)
            {
                currency.IsActive = !(currency.IsActive);
                db.SaveChanges();
            }

        }
        #endregion

        #region Currency Conversion Section
        public List<tblCurrencyMst> GetActiveCurrencyList()
        {
            try
            {
                return db.tblCurrencyMsts.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public List<tblCurrencyMst> GetCurrencyExcptList(Guid ID)
        //{
        //    try
        //    {
        //        return db.tblCurrencyMsts.Where(x => x.ID != ID && x.IsActive == true).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public List<tblCurrencyConversion> GetCurrencyEditList(Guid ID)
        //{
        //    try
        //    {
        //        return db.tblCurrencyConversions.Where(x => x.SrcCurrID == ID).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public List<tblCurrencyConversion> GetCurrencyConvList()
        //{
        //    try
        //    {
        //        return db.tblCurrencyConversions.OrderBy(x => x.SrcCurrID).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<tblCurrencyConversion> GetCurrencyConvInnerList(Guid ID)
        {
            try
            {
                return db.tblCurrencyConversions.Where(x => x.ID == ID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCurrencyConversion> GetCurrencyConvInnerListWithSiteId(Guid ID, Guid SiteID)
        {
            try
            {
                return db.tblCurrencyConversions.Where(x => x.ID == ID && x.SiteID == SiteID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblCurrencyConversion GetCurrencyConvListEdit(Guid ID)
        {
            try
            {
                return db.tblCurrencyConversions.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddCurrencyConv(CurrencyConv currCon)
        {
            try
            {
                tblCurrencyConversion _checkConv = db.tblCurrencyConversions.FirstOrDefault(a => a.SrcCurrID == currCon.SrcCurrID && a.TrgCurrID == currCon.TrgCurrID && a.SiteID == currCon.SiteID);
                if (_checkConv != null)
                {
                    List<tblCurrencyConversion> lst = db.tblCurrencyConversions.Where(a => a.SrcCurrID == _checkConv.SrcCurrID && a.TrgCurrID == _checkConv.TrgCurrID).ToList();
                    foreach (tblCurrencyConversion obj in lst)
                    {
                        var _currCon1 = db.tblCurrencyConversions.FirstOrDefault(x1 => x1.ID == obj.ID);
                        if (_currCon1 != null)
                        {
                            _currCon1.Multiplier = currCon.Multiplier;
                            db.SaveChanges();
                        }
                    }
                }
                else
                {

                    tblCurrencyConversion _currConv = new tblCurrencyConversion
                    {
                        SiteID = currCon.SiteID,
                        SrcCurrID = currCon.SrcCurrID,
                        TrgCurrID = currCon.TrgCurrID,
                        Multiplier = currCon.Multiplier,
                        ID = currCon.ID
                    };
                    db.AddTotblCurrencyConversions(_currConv);
                    db.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateCurrencyConv(CurrencyConv currCon)
        {
            try
            {
                var _currCon = db.tblCurrencyConversions.FirstOrDefault(x => x.ID == currCon.ID);
                List<tblCurrencyConversion> lst = db.tblCurrencyConversions.Where(a => a.SrcCurrID == _currCon.SrcCurrID && a.TrgCurrID == _currCon.TrgCurrID).ToList();
                foreach (tblCurrencyConversion obj in lst)
                {
                    var _currCon1 = db.tblCurrencyConversions.FirstOrDefault(x1 => x1.ID == obj.ID);
                    if (_currCon1 != null)
                    {
                        _currCon1.Multiplier = currCon.Multiplier;
                        db.SaveChanges();
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Country Master Section
        public Guid AddCountry(Country country)
        {
            try
            {
                tblCountriesMst _country = new tblCountriesMst
                {
                    OtherCountry = country.OtherCountry,
                    CountryName = country.CountryName,
                    CountryCode = country.CountryCode,
                    DespatchExpressCountryCode = country.DespatchExpressCountryCode,
                    IsoCode = country.IsoCode,
                    EurailCode = country.EurailCode,
                    RBSCode = country.RBSCode,
                    IsActive = country.IsActive,
                    IsVisible = country.IsVisible,
                    CountryImg = country.CountryImg,
                    BannerImg = country.BannerImg,
                    CountryFlagImg = country.CountryFlagImg,
                    CountryID = country.CountryID,
                    RegionID = country.RegionID,
                    ContinentID = country.ContinentID,
                    SortOrder = country.SortOrder
                };
                db.AddTotblCountriesMsts(_country);
                db.SaveChanges();
                return _country.CountryID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCountriesMst> GetShippingAllCountryList(Guid SiteID)
        {
            try
            {
                var result = (from TS in db.tblShippings
                              join TSC in db.tblShippingOptionCountries
                                   on TS.ID equals TSC.ShippingID
                              join TCM in db.tblCountriesMsts
                                   on TSC.CountryID equals TCM.CountryID
                              where TS.IsActive == true && TS.IsVisibleFront == true && TS.SiteID == SiteID
                              select new { TCM }).ToList();

                return result.GroupBy(x => new { x.TCM.CountryID, x.TCM.CountryName }).Select(y => new tblCountriesMst
                {
                    CountryID = y.Key.CountryID,
                    CountryName = y.Key.CountryName
                }).ToList().OrderBy(x => x.CountryName).ToList(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid UpdateCountry(Country country)
        {
            try
            {
                var _country = db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == country.CountryID);
                if (_country != null)
                {
                    _country.CountryName = country.CountryName;
                    _country.OtherCountry = country.OtherCountry;
                    _country.CountryCode = country.CountryCode;
                    _country.DespatchExpressCountryCode = country.DespatchExpressCountryCode;
                    _country.IsoCode = country.IsoCode;
                    _country.EurailCode = country.EurailCode;
                    _country.RBSCode = country.RBSCode;
                    _country.IsActive = country.IsActive;
                    _country.IsVisible = country.IsVisible;

                    if (!string.IsNullOrEmpty(country.CountryFlagImg))
                        _country.CountryFlagImg = country.CountryFlagImg;
                    if (!string.IsNullOrEmpty(country.CountryImg))
                        _country.CountryImg = country.CountryImg;
                    if (!string.IsNullOrEmpty(country.BannerImg))
                        _country.BannerImg = country.BannerImg;

                    _country.CountryID = country.CountryID;
                    _country.RegionID = country.RegionID;
                    _country.ContinentID = country.ContinentID;
                }
                db.SaveChanges();
                return _country.CountryID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetvalidCountryId(Guid SiteId, Guid CountryId)
        {
            try
            {
                return db.tblCountrySiteLookUps.Any(x => x.SiteID == SiteId && x.CountryID == CountryId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool ActiveInactiveCountry(Guid id)
        {
            try
            {
                var rec = db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCountriesMst> GetCountryList()
        {
            try
            {
                return db.tblCountriesMsts.Where(x => x.IsActive == true).OrderBy(x => x.CountryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCountriesMst> GetCountryListByContinetId(Guid id)
        {
            try
            {
                return db.tblCountriesMsts.Where(x => x.tblContinent.ID == id && x.IsActive == true).OrderBy(x => x.CountryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCountriesMst GetcountryListEdit(Guid ID)
        {
            try
            {
                return db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblRegionMst> GetRegionList()
        {
            try
            {
                return db.tblRegionMsts.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Languanges Master Section
        public List<tblLanguagesMst> GetLanguangesList()
        {
            try
            {
                return db.tblLanguagesMsts.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddLanguage(Languages _Languages)
        {
            try
            {
                tblLanguagesMst _tblLanguagesMst = new tblLanguagesMst
                {
                    Name = _Languages.Name,
                    Logo = _Languages.Logo,
                    IsActive = _Languages.IsActive,
                    ID = _Languages.ID
                };
                db.AddTotblLanguagesMsts(_tblLanguagesMst);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateLanguage(Languages _Languages)
        {
            try
            {
                var _chkLanguages = db.tblLanguagesMsts.FirstOrDefault(x => x.ID == _Languages.ID);
                if (_chkLanguages != null)
                {
                    _chkLanguages.Name = _Languages.Name;
                    _chkLanguages.Logo = _Languages.Logo;
                    _chkLanguages.IsActive = _Languages.IsActive;

                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public tblLanguagesMst GetLanguageListEdit(Guid ID)
        {
            try
            {
                return db.tblLanguagesMsts.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveLanguage(Guid id)
        {
            try
            {
                var rec = db.tblLanguagesMsts.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region Navigation Master Section
        public int AddNavigation(Navigation _Navigation)
        {
            try
            {
                tblNavigation _tblNavigation = new tblNavigation
                {
                    Name = _Navigation.Name,
                    PageName = _Navigation.PageName,
                    PSortOrder = _Navigation.PSortOrder,
                    CSortOrder = _Navigation.CSortOrder,
                    IsCorporate = _Navigation.IsCorporate,
                    IsActive = _Navigation.IsActive,
                    ID = _Navigation.ID,
                    ParentID = _Navigation.ParentID,
                    IsTop = _Navigation.IsTop,
                    IsBottom = _Navigation.IsBottom
                };
                db.AddTotblNavigations(_tblNavigation);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblNavigation> GetParentNavigationList(Guid id)
        {
            try
            {
                return db.tblNavigations.Where(i => i.ParentID == id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblNavigation GetNavigationListEdit(Guid ID)
        {
            try
            {
                return db.tblNavigations.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateNavigation(Navigation _Navigation)
        {
            try
            {
                var _ckhNavigation = db.tblNavigations.FirstOrDefault(x => x.ID == _Navigation.ID);
                if (_ckhNavigation != null)
                {
                    _ckhNavigation.Name = _Navigation.Name;
                    _ckhNavigation.PageName = _Navigation.PageName;
                    _ckhNavigation.PSortOrder = _Navigation.PSortOrder;
                    _ckhNavigation.CSortOrder = _Navigation.CSortOrder;
                    _ckhNavigation.IsCorporate = _Navigation.IsCorporate;
                    _ckhNavigation.IsActive = _Navigation.IsActive;
                    _ckhNavigation.ID = _Navigation.ID;
                    _ckhNavigation.ParentID = _Navigation.ParentID;
                    _ckhNavigation.IsTop = _Navigation.IsTop;
                    _ckhNavigation.IsBottom = _Navigation.IsBottom;
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<NavigationList> GetNavMenulistbyRoleID(Guid roleId)
        {
            try
            {
                var list = (from role in db.ap_RoleMenuDetail
                            join menu in db.tblNavigations
                                on role.MenuId equals menu.ID
                            where role.RoleId == roleId
                            orderby menu.Name
                            select new { menu, role }).ToList();
                return list.Select(x => new NavigationList
                {
                    Name = x.menu.Name,
                    Permission = x.role.Permission,
                    Visible = x.role.Visible
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public class NavigationList
        {
            public string Name { get; set; }
            public bool? Permission { get; set; }
            public bool? Visible { get; set; }
        }

        public List<tblWebMenu> GetCorporateSiteMenu()
        {
            //return db.tblNavigations.Where(x => x.IsCorporate && x.PageName != string.Empty).OrderBy(x => x.Name).ToList();
            return db.tblWebMenus.Where(x => x.IsSeo && x.IsCorporate).OrderBy(x => x.Name).ToList();
        }
        #endregion

        #region Branch
        public void ManageOfficeCommission(int OfficeID, Decimal Commission)
        {

            List<tblBranch> lstBranch = db.tblBranches.Where(a => a.TreeParentBranchID == OfficeID).ToList();
            foreach (tblBranch tb in lstBranch)
            {
                ManageOfficeCommission(tb.TreeID, Commission);
            }

            tblBranch objBr = db.tblBranches.Where(a => a.TreeID == OfficeID).FirstOrDefault();
            if (objBr != null)
            {
                db.tblOfficeCommissionLookups.Where(x => x.OfficeID == objBr.ID).ToList().ForEach(db.tblOfficeCommissionLookups.DeleteObject);
                tblOfficeCommissionLookup objCLookup = new tblOfficeCommissionLookup();
                objCLookup.ID = Guid.NewGuid();
                objCLookup.Commission = Commission;
                objCLookup.OfficeID = objBr.ID;
                db.tblOfficeCommissionLookups.AddObject(objCLookup);
                db.SaveChanges();
            }
        }
        public decimal GetOfficeCommission(int TreeId)
        {
            try
            {
                decimal rval = 0;
                tblBranch objBr = db.tblBranches.Where(a => a.TreeID == TreeId).FirstOrDefault();
                if (objBr != null)
                {
                    tblOfficeCommissionLookup objCommission = db.tblOfficeCommissionLookups.Where(a => a.OfficeID == objBr.ID).FirstOrDefault();
                    if (objCommission != null)
                        rval = objCommission.Commission;
                }
                return rval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean CheckBranchID(Guid bid)
        {
            try
            {
                Boolean rval = false;
                tblBranch objBr = db.tblBranches.Where(a => a.ID == bid).FirstOrDefault();
                if (objBr != null)
                {
                    rval = true;
                }
                return rval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid GetBranchGUID(int id)
        {
            try
            {
                return db.tblBranches.FirstOrDefault(x => x.TreeID == id).ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddBranch(Branch _Branch)
        {
            try
            {
                var _chkBranch = db.tblBranches.FirstOrDefault(x => x.TreeID == _Branch.TreeID);
                if (_chkBranch == null)
                {
                    tblBranch _tblBranch = new tblBranch
                    {
                        OfficeName = _Branch.OfficeName,
                        ID = _Branch.ID,
                        ParentBranchID = _Branch.ParentBranchID,
                        TreeParentBranchID = _Branch.TreeParentBranchID,
                        Title = _Branch.Title,
                        FirstName = _Branch.FirstName,
                        LastName = _Branch.LastName,
                        Address1 = _Branch.Address1,
                        Address2 = _Branch.Address2,
                        Town = _Branch.Town,
                        County = _Branch.County,
                        Country = _Branch.Country,
                        Postcode = _Branch.Postcode,
                        Telephone = _Branch.Telephone,
                        Email = _Branch.Email,
                        SecondaryEmail = _Branch.SecondaryEmail,
                        chkEmail = _Branch.chkEmail,
                        chkSecondaryEmail = _Branch.chkSecondaryEmail,
                        IsActive = _Branch.IsActive,
                        IsVirtualOffice = _Branch.IsVirtualOffice,
                    };
                    db.AddTotblBranches(_tblBranch);
                    db.SaveChanges();
                    return _tblBranch.ID;
                }
                else
                {
                    _chkBranch.OfficeName = _Branch.OfficeName;
                    _chkBranch.Title = _Branch.Title;
                    _chkBranch.IsVirtualOffice = _Branch.IsVirtualOffice;
                    _chkBranch.FirstName = _Branch.FirstName;
                    _chkBranch.LastName = _Branch.LastName;
                    _chkBranch.Address1 = _Branch.Address1;
                    _chkBranch.Address2 = _Branch.Address2;
                    _chkBranch.Town = _Branch.Town;
                    _chkBranch.County = _Branch.County;
                    _chkBranch.Country = _Branch.Country;
                    _chkBranch.Postcode = _Branch.Postcode;
                    _chkBranch.Telephone = _Branch.Telephone;
                    _chkBranch.Email = _Branch.Email;
                    _chkBranch.SecondaryEmail = _Branch.SecondaryEmail;
                    _chkBranch.chkEmail = _Branch.chkEmail;
                    _chkBranch.chkSecondaryEmail = _Branch.chkSecondaryEmail;
                    _chkBranch.IsActive = _Branch.IsActive;
                }
                db.SaveChanges();
                return _chkBranch.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddBranchLookupSites(tblBranchLookupSite BranchLookupSite)
        {
            try
            {
                var _tblBranchLookupSite = new tblBranchLookupSite
                {
                    ID = BranchLookupSite.ID,
                    BranchID = BranchLookupSite.BranchID,
                    SiteID = BranchLookupSite.SiteID
                };
                db.AddTotblBranchLookupSites(_tblBranchLookupSite);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetAgentOfficePhone(Guid agentId)
        {
            try
            {
                var result = (from user in db.tblAdminUsers
                              join office in db.tblBranches on user.BranchID equals office.ID
                              where user.ID == agentId
                              select new { office.Telephone }).FirstOrDefault();
                if (result != null)
                    return result.Telephone;
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BranchListToEdit_Result BranchListforEdit(int id)
        {
            try
            {
                return db.BranchListToEdit(id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteBranch(int id)
        {
            try
            {
                var bid = db.tblBranches.FirstOrDefault(x => x.TreeID == id).ID;
                if (db.tblAdminUsers.Any(x => x.BranchID == bid) || db.tblStockAllocationEurails.Any(t => t.BranchID == bid || t.ParentBranchID == bid) || db.tblStockAllocations.Any(t => t.BranchID == bid || t.ParentBranchID == bid) || db.tblStockQueues.Any(t => t.BranchID == bid))
                    throw new Exception("Office is already in used.");

                db.tblBranchLookupSites.Where(w => w.BranchID == bid).ToList().ForEach(db.tblBranchLookupSites.DeleteObject);

                db.tblBranches.Where(x => x.TreeID == id || x.TreeParentBranchID == id).ToList().ForEach(db.tblBranches.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Office is already in used.");
            }
        }

        public int UpdateBranch(Branch _Branch)
        {
            try
            {
                var _chkBranch = db.tblBranches.FirstOrDefault(x => x.TreeID == _Branch.TreeID);
                if (_chkBranch != null)
                {
                    //_chkBranch.BranchName = _Branch.BranchName;
                    //_chkBranch.Salutation = _Branch.Salutation;
                    //_chkBranch.Forename = _Branch.Forename;
                    //_chkBranch.Surname = _Branch.Surname;
                    //_chkBranch.Address1 = _Branch.Address1;
                    //_chkBranch.Address2 = _Branch.Address2;
                    //_chkBranch.Town = _Branch.Town;
                    //_chkBranch.County = _Branch.County;
                    //_chkBranch.Country = _Branch.Country;
                    //_chkBranch.Postcode = _Branch.Postcode;
                    //_chkBranch.Telephone = _Branch.Telephone;
                    //_chkBranch.Email = _Branch.Email;
                    //_chkBranch.IsActive = _Branch.IsActive;
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid GetBranchParentID(int TreeID)
        {
            try
            {
                var _chk = db.tblBranches.FirstOrDefault(x => x.TreeID == TreeID);
                return _chk.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblBranch GetRootNode()
        {
            return db.tblBranches.Where(x => x.ParentBranchID == null).FirstOrDefault();
        }

        #region
        public int UploadBranchesFromExcel(string sPath, int ParentTreeId, Guid? CreatedBy, string fileExt)
        {
            try
            {
                string ConStringOLEDB = string.Empty;
                if (fileExt.ToUpper() == ".XLS")
                    ConStringOLEDB = "provider=Microsoft.Jet.OLEDB.4.0; data source='" + sPath + "';Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
                else
                    ConStringOLEDB = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + sPath + "';Extended Properties='Excel 12.0 Xml;HDR=YES'";
                OleDbConnection MyConnection;

                object missing = System.Reflection.Missing.Value;
                MyConnection = new OleDbConnection(ConStringOLEDB);
                MyConnection.Open();

                var objDT = new DataTable();
                objDT = MyConnection.GetSchema("Tables");
                var dr = objDT.Rows[0];
                string strWorksheetName = objDT.Rows[0]["TABLE_NAME"].ToString(); //"Shee1$";

                OleDbDataAdapter objDataAdpter;
                try
                {
                    objDataAdpter = new OleDbDataAdapter("select * from [" + strWorksheetName + "]  ", MyConnection);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                var objDS = new DataSet();
                objDataAdpter.Fill(objDS);
                MyConnection.Close();

                foreach (DataRow item in objDS.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[0])))
                    {
                        var oBranch = new tempBranch
                        {
                            BranchName = Convert.ToString(item[0]),
                            Address = Convert.ToString(item[1]),
                            City = Convert.ToString(item[2]),
                            PostCode = Convert.ToString(item[3]),
                            CountryCode = Convert.ToString(item[4]),
                            WorkPhone = Convert.ToString(item[5]),
                            Email = Convert.ToString(item[6]),
                        };
                        db.tempBranches.AddObject(oBranch);
                        db.SaveChanges();
                    }
                }
                int res = db.spImportBranches(CreatedBy, ParentTreeId).FirstOrDefault().Value;
                return 1;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

        #endregion

        #endregion

        #region Theme
        public List<tblTheme> GetThemeList()
        {
            try
            {
                return db.tblThemes.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region EnableTicketProtection
        public bool IsTicketProtection(Guid siteId)
        {
            try
            {
                var result = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (result != null)
                {
                    var tck = result.EnableTicketProtection;
                    if (tck != null)
                        return (bool)tck;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Site

        public void AddEditSiteCurrency(Guid siteid, List<Guid> currencyids)
        {
            try
            {
                if (db.tblSiteCurrencyLookUps.Any(x => x.SiteID == siteid))
                {
                    db.tblSiteCurrencyLookUps.Where(x => x.SiteID == siteid).ToList().ForEach(db.tblSiteCurrencyLookUps.DeleteObject);
                    db.SaveChanges();
                }
                foreach (Guid currencyid in currencyids)
                {
                    db.tblSiteCurrencyLookUps.AddObject(new tblSiteCurrencyLookUp
                    {
                        ID = Guid.NewGuid(),
                        SiteID = siteid,
                        CurrencyID = currencyid
                    });
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<SiteCurrency> GetCurrencyBySiteId(Guid siteid)
        {
            try
            {
                return db.tblSiteCurrencyLookUps.Where(x => x.SiteID == siteid).Select(x => new SiteCurrency
                {
                    CurrencyID = x.CurrencyID,
                    Name = x.tblCurrencyMst.Name
                }).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateAttentionMsg(Guid ID, string AttentionTitle, string AttentionMsg, string AttentionImg, string BGColor, string BDRColor)
        {
            try
            {
                var data = db.tblSites.FirstOrDefault(t => t.ID == ID);
                if (data != null)
                {
                    data.AttentionTitle = AttentionTitle;
                    data.AttentionMsg = AttentionMsg;
                    data.AttentionImg = (AttentionImg == "images/Seasonal.jpg" && data.AttentionImg != "images/Seasonal.jpg") ? data.AttentionImg : AttentionImg;
                    data.BGColor = BGColor;
                    data.BDRColor = BDRColor;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsVisibleNewsLetter(Guid siteId)
        {
            try
            {
                var rt = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                return rt != null && (rt.IsVisibleNewsLetter != null && ((bool)rt.IsVisibleNewsLetter));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsVisibleJRF(Guid siteId)
        {
            try
            {
                var rt = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                return rt != null && (rt.IsVisibleJRF != null && ((bool)rt.IsVisibleJRF));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsVisibleGeneralInfo(Guid siteId)
        {
            try
            {
                var rt = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                return rt != null && (rt.IsVisibleGeneralInfo != null && ((bool)rt.IsVisibleGeneralInfo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsVisibleRightPanelImages(Guid siteId)
        {
            try
            {
                var rt = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                return rt != null && (rt.IsVisibleImages != null && ((bool)rt.IsVisibleImages));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string GetSiteNameByID(Guid siteId)
        {
            try
            {
                return db.tblSites.FirstOrDefault(x => x.ID == siteId).DisplayName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool IsExistSiteURL(string stieUrl)
        {
            return db.tblSites.Any(x => x.SiteURL == stieUrl);
        }
        public Guid AddSite(Sites _Sites)
        {
            try
            {
                var _tblSite = new tblSite
                {
                    DisplayName = _Sites.DisplayName,
                    ID = _Sites.ID,
                    ThemeID = _Sites.ThemeID,
                    SiteAlias = _Sites.SiteAlias,
                    OrderPrefix = _Sites.OrderPrefix,
                    DefaultCurrencyID = _Sites.DefaultCurrencyID,
                    DefaultLanguageID = _Sites.DefaultLanguageID,
                    DefaultCountryID = _Sites.DefaultCountryID,
                    SiteURL = _Sites.SiteURL,
                    IsSTA = _Sites.IsSTA,
                    IsAgent = _Sites.IsAgent,
                    IsActive = _Sites.IsActive,
                    IsCorporate = _Sites.IsCorporate,
                    IsUS = _Sites.IsUS,
                    IsTopJourney = _Sites.IsTopJourney,
                    HavRailPass = _Sites.HavRailPass,
                    IsAttentionVisible = _Sites.IsAttentionVisible,
                    IsDelete = _Sites.IsDelete,
                    IsVisibleNewsLetter = _Sites.IsVisibleNewsLetter,
                    IsVisibleJRF = _Sites.IsVisibleJRF,
                    IsVisibleGeneralInfo = _Sites.IsVisibleGeneralInfo,
                    IsVisibleImages = _Sites.IsVisibleImages,
                    EnableP2P = _Sites.EnableP2P,
                    EnableContactEmail = _Sites.EnableContactEmail,
                    EnableTicketProtection = _Sites.EnableTicketProtection,
                    PhoneNumber = _Sites.PhoneNumber,
                    SiteHeading = _Sites.SiteHeading,
                    BookingDayLimit = _Sites.BookingDayLimit,
                    JourneyEmail = _Sites.JourneyEmail,
                    BccEmail = _Sites.BccEmail,
                    IsBccEmail = _Sites.IsBccEmail,
                    IsVisibleP2PWidget = _Sites.IsVisibleP2PWidget,
                    IsTrainTickets = _Sites.IsTrainTickets,
                    LogoPath = _Sites.LogoPath,
                    GoogleAnalytics = _Sites.GoogleAnalytics,
                    GoogleManager = _Sites.GoogleManager,
                    DateFormat = _Sites.DateFormat,
                    FaviconPath = _Sites.FaviconPath,
                    EnableAffiliateTracking = _Sites.EnableAffiliateTracking,
                    PaymentType = _Sites.PaymentType,
                    LayoutType = _Sites.LayoutType,
                    IsEnableCookie = _Sites.IsEnableCookie,
                    EurailPrinter = _Sites.EurailPrinter,
                    BritrailPrinter = _Sites.BritrailPrinter,
                    InterrailPrinter = _Sites.InterrailPrinter,
                    GmtTimeZone = _Sites.GmtTimeZone
                };
                if (!IsExistSiteURL(_Sites.SiteURL.Trim()))
                {
                    db.AddTotblSites(_tblSite);
                    db.SaveChanges();
                    return _tblSite.ID;
                }
                else
                    return Guid.Parse("00000000-0000-0000-0000-000000000000");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SiteList_Result> Branchlist()
        {
            try
            {
                return db.SiteList().OrderBy(x => x.DisplayName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<UserSiteList_Result> UserSitelist()
        {
            try
            {
                return db.UserSiteList().OrderBy(x => x.DISPLAYNAME).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblSite> GetSiteList()
        {
            try
            {
                return db.tblSites.OrderBy(x => x.SiteURL).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblSite> GetActiveSiteList()
        {
            try
            {
                return db.tblSites.Where(x => x.IsActive == true && x.IsDelete != true).OrderBy(x => x.DisplayName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateSite(Sites _Sites)
        {
            try
            {
                var _chkSites = db.tblSites.FirstOrDefault(x => x.ID == _Sites.ID);
                if (_chkSites != null)
                {
                    _chkSites.DisplayName = _Sites.DisplayName;
                    _chkSites.ID = _Sites.ID;
                    _chkSites.SiteAlias = _Sites.SiteAlias;
                    _chkSites.OrderPrefix = _Sites.OrderPrefix;
                    _chkSites.ThemeID = _Sites.ThemeID;
                    _chkSites.DefaultCurrencyID = _Sites.DefaultCurrencyID;
                    _chkSites.DefaultLanguageID = _Sites.DefaultLanguageID;
                    _chkSites.DefaultCountryID = _Sites.DefaultCountryID;
                    _chkSites.SiteURL = _Sites.SiteURL;
                    _chkSites.IsSTA = _Sites.IsSTA;
                    _chkSites.IsAgent = _Sites.IsAgent;
                    _chkSites.IsActive = _Sites.IsActive;
                    _chkSites.IsAttentionVisible = _Sites.IsAttentionVisible;
                    _chkSites.IsCorporate = _Sites.IsCorporate;
                    _chkSites.IsUS = _Sites.IsUS;
                    _chkSites.IsTopJourney = _Sites.IsTopJourney;
                    _chkSites.HavRailPass = _Sites.HavRailPass;
                    _chkSites.IsDelete = _Sites.IsDelete;
                    _chkSites.IsVisibleNewsLetter = _Sites.IsVisibleNewsLetter;
                    _chkSites.IsVisibleJRF = _Sites.IsVisibleJRF;
                    _chkSites.IsVisibleGeneralInfo = _Sites.IsVisibleGeneralInfo;
                    _chkSites.IsVisibleImages = _Sites.IsVisibleImages;
                    _chkSites.EnableP2P = _Sites.EnableP2P;
                    _chkSites.PhoneNumber = _Sites.PhoneNumber;
                    _chkSites.SiteHeading = _Sites.SiteHeading;
                    _chkSites.EnableContactEmail = _Sites.EnableContactEmail;
                    _chkSites.BookingDayLimit = _Sites.BookingDayLimit;
                    _chkSites.EnableTicketProtection = _Sites.EnableTicketProtection;
                    _chkSites.JourneyEmail = _Sites.JourneyEmail;
                    _chkSites.BccEmail = _Sites.BccEmail;
                    _chkSites.IsBccEmail = _Sites.IsBccEmail;
                    _chkSites.IsVisibleP2PWidget = _Sites.IsVisibleP2PWidget;
                    _chkSites.IsTrainTickets = _Sites.IsTrainTickets;
                    _chkSites.IsWholeSale = _Sites.IsWholeSale;
                    _chkSites.GoogleAnalytics = _Sites.GoogleAnalytics;
                    _chkSites.GoogleManager = _Sites.GoogleManager;
                    _chkSites.SiteHeadingTitle = _Sites.SiteHeadingTitle;
                    _chkSites.DateFormat = _Sites.DateFormat;
                    _chkSites.EnableAffiliateTracking = _Sites.EnableAffiliateTracking;
                    _chkSites.PaymentType = _Sites.PaymentType;
                    _chkSites.LayoutType = _Sites.LayoutType;
                    _chkSites.IsEnableCookie = _Sites.IsEnableCookie;
                    _chkSites.EurailPrinter = _Sites.EurailPrinter;
                    _chkSites.BritrailPrinter = _Sites.BritrailPrinter;
                    _chkSites.InterrailPrinter = _Sites.InterrailPrinter;
                    _chkSites.GmtTimeZone = _Sites.GmtTimeZone;

                    if (_Sites.LogoPath != null)
                        _chkSites.LogoPath = _Sites.LogoPath;
                    if (_Sites.FaviconPath != null)
                        _chkSites.FaviconPath = _Sites.FaviconPath;
                }
                db.SaveChanges();
                return _Sites.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblSite GetSiteListEdit(Guid ID)
        {
            try
            {
                return db.tblSites.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActivateInActivateSite(Guid ID)
        {
            var _site = db.tblSites.FirstOrDefault(x => x.ID == ID);
            if (_site != null)
            {
                _site.IsActive = !(_site.IsActive);
                db.SaveChanges();
            }
        }
        public void DeleteSite(Guid ID)
        {
            var _site = db.tblSites.FirstOrDefault(x => x.ID == ID);
            if (_site != null)
            {
                _site.IsDelete = true;
                db.SaveChanges();
            }
        }
        public void ChangeSTAStatus(Guid ID)
        {
            var _site = db.tblSites.FirstOrDefault(x => x.ID == ID);
            if (_site != null)
            {
                _site.IsSTA = !(_site.IsSTA);
                db.SaveChanges();
            }
        }
        public void ChangeAgentStatus(Guid ID)
        {
            var _site = db.tblSites.FirstOrDefault(x => x.ID == ID);
            if (_site != null)
            {
                _site.IsAgent = !(_site.IsAgent);
                db.SaveChanges();
            }
        }

        public void DeleteSiteDeletionRightForUser(Guid ID)
        {
            try
            {
                db.tblSiteDeletionRights.Where(t => t.AssignTo == ID).ToList().ForEach(db.tblSiteDeletionRights.DeleteObject);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddSiteDeletionRight(tblSiteDeletionRight obj)
        {
            try
            {
                db.tblSiteDeletionRights.AddObject(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblSiteDeletionRight> GetUserSiteDeletionRight(Guid id)
        {
            try
            {
                List<tblSiteDeletionRight> lst = db.tblSiteDeletionRights.Where(a => a.AssignTo == id).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<UserSiteDeletion> GetUsersWithSiteDeletionRight()
        {
            try
            {
                var list = (from Au in db.tblSiteDeletionRights select new { Au.AssignTo }).Distinct().ToList();
                return (list.Select(x => new UserSiteDeletion
                {
                    ID = x.AssignTo,
                    UserName = db.tblAdminUsers.Where(a => a.ID == x.AssignTo).Select(b => b.UserName).SingleOrDefault(),
                    SiteName = GetSiteNameForDeletion(x.AssignTo)
                })).AsEnumerable().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetSiteNameForDeletion(Guid? id)
        {
            var res = (from catSi in db.tblSiteDeletionRights
                       join ts in db.tblSites on catSi.SiteID equals ts.ID
                       where catSi.AssignTo == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;

        }
        public Boolean RoleDeleteHaveUser(Guid userid, Guid siteid)
        {
            try
            {
                if (db.tblSiteDeletionRights.Where(a => a.SiteID == siteid && a.AssignTo == userid).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsExistUserName(String UserName, Guid ID)
        {
            try
            {
                return db.tblAdminUsers.Any(t => t.UserName == UserName && t.ID != ID && t.IsDeleted == false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool IsExistUserName(String UserName)
        {
            try
            {
                return db.tblAdminUsers.Any(t => t.UserName == UserName && t.IsDeleted == false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RoleIsAdmin(Guid adminId)
        {
            try
            {
                var result = (from admin in db.tblAdminUsers
                              join role in db.aspnet_Roles
                                  on admin.RoleID equals role.RoleId
                              where role.RoleName == "Admin"
                              && admin.ID == adminId
                              select new { admin });
                if (result.Any())
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid GetDefaultCountryBySiteId(Guid SiteId)
        {
            try
            {
                var data = db.tblSites.FirstOrDefault(x => x.ID == SiteId && x.IsActive == true);
                return data == null ? Guid.Empty : data.DefaultCountryID.HasValue ? data.DefaultCountryID.Value : Guid.Empty;
            }
            catch (Exception ex) { throw ex; }
        }

        public StationNameList GetLongitudeLatitude(string StationCode, string RailName)
        {
            try
            {
                return db.StationNameLists.FirstOrDefault(x => x.StationCode.Contains(StationCode) && x.RailName == RailName);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region Web Menu Section
        public IEnumerable<tblWebMenu> GetParentWebNavigationList(Guid id)
        {
            try
            {
                return db.tblWebMenus.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<tblWebMenu> GetActiveParentWebNavigationList(Guid siteId)
        {
            try
            {
                return db.tblWebMenus.Where(i => i.IsActive == true && i.IsAllowTopBottom == true).OrderBy(x => x.PSortOrder).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public int AddWebMenu(Navigation _Navigation)
        //{
        //    try
        //    {
        //        tblWebMenu _tblNavigation = new tblWebMenu
        //        {
        //            Name = _Navigation.Name,
        //            PageName = _Navigation.PageName,
        //            CSortOrder = _Navigation.CSortOrder,
        //            PSortOrder = _Navigation.PSortOrder,
        //            IsActive = _Navigation.IsActive,
        //            ID = _Navigation.ID,
        //            ParentID = _Navigation.ParentID,
        //            IsTop = _Navigation.IsTop,
        //            IsBottom = _Navigation.IsBottom,
        //            SiteID = _Navigation.SiteId

        //        };
        //        db.AddTotblWebMenus(_tblNavigation);
        //        db.SaveChanges();
        //        return 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //public int UpdateWebMenu(Navigation _Navigation)
        //{
        //    try
        //    {
        //        var _ckhWebMenu = db.tblWebMenus.FirstOrDefault(x => x.ID == _Navigation.ID);
        //        if (_ckhWebMenu != null)
        //        {
        //            _ckhWebMenu.Name = _Navigation.Name;
        //            _ckhWebMenu.PageName = _Navigation.PageName;
        //            _ckhWebMenu.CSortOrder = _Navigation.CSortOrder;
        //            _ckhWebMenu.PSortOrder = _Navigation.PSortOrder;
        //            _ckhWebMenu.IsActive = _Navigation.IsActive;
        //            _ckhWebMenu.ID = _Navigation.ID;
        //            _ckhWebMenu.ParentID = _Navigation.ParentID;
        //            _ckhWebMenu.IsTop = _Navigation.IsTop;
        //            _ckhWebMenu.IsBottom = _Navigation.IsBottom;
        //            _ckhWebMenu.SiteID = _Navigation.SiteId;
        //        }
        //        return db.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}
        public tblWebMenu GetWebMenuEdit(Guid ID)
        {
            try
            {
                return db.tblWebMenus.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ChangeISTop(Guid ID)
        {
            var _webMenu = db.tblWebMenus.FirstOrDefault(x => x.ID == ID);
            if (_webMenu != null)
            {
                _webMenu.IsTop = !(_webMenu.IsTop);
                db.SaveChanges();
            }
        }

        public void ChangeISBottom(Guid ID)
        {
            var _webMenu = db.tblWebMenus.FirstOrDefault(x => x.ID == ID);
            if (_webMenu != null)
            {
                _webMenu.IsBottom = !(_webMenu.IsBottom);
                db.SaveChanges();
            }
        }

        public void ActivateInActivateWebMenu(Guid ID)
        {
            var _webMenu = db.tblWebMenus.FirstOrDefault(x => x.ID == ID);
            if (_webMenu != null)
            {
                _webMenu.IsActive = !(_webMenu.IsActive);
                db.SaveChanges();
            }
        }

        #endregion

        #region  Role Menu Details
        public int AddRoleMenu(Ap_RoleMenuDetail _Ap_RoleMenuDetail)
        {
            try
            {
                ap_RoleMenuDetail _ap_RoleMenuDetail = new ap_RoleMenuDetail
                {
                    Id = _Ap_RoleMenuDetail.Id,
                    MenuId = _Ap_RoleMenuDetail.MenuId,
                    RoleId = _Ap_RoleMenuDetail.RoleId,
                    Permission = _Ap_RoleMenuDetail.Permission,
                    Visible = _Ap_RoleMenuDetail.Visible
                };
                db.AddToap_RoleMenuDetail(_ap_RoleMenuDetail);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Country Info
        public int AddCountryInfo(CountriesInfo _CountriesInfo)
        {
            try
            {
                tblCountriesInfo _tblCountriesInfo = new tblCountriesInfo
                {
                    ID = _CountriesInfo.ID,
                    Capital = _CountriesInfo.Capital,
                    Population = _CountriesInfo.Population,
                    Language = _CountriesInfo.Language,
                    Currency = _CountriesInfo.Currency,
                    TrainOperator = _CountriesInfo.TrainOperator,
                    TouristBoard = _CountriesInfo.TouristBoard,
                    TimeZone = _CountriesInfo.TimeZone,
                    CallingCode = _CountriesInfo.CallingCode,
                    CountryInfo = _CountriesInfo.CountryInfo,
                    Drives = _CountriesInfo.Drives,
                    Visas = _CountriesInfo.Visas,
                    CountryID = _CountriesInfo.CountryID,
                    InternetTLD = _CountriesInfo.InternetTLD,
                    Title = _CountriesInfo.Title,
                    IrCountryInfo = _CountriesInfo.IrCountryInfo
                };
                db.AddTotblCountriesInfoes(_tblCountriesInfo);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblCountriesInfo GetCountryInfoEdit(Guid ID)
        {
            try
            {
                return db.tblCountriesInfoes.FirstOrDefault(x => x.CountryID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateCountryInfo(CountriesInfo _CountriesInfo)
        {
            try
            {
                var _ckhCountryInfo = db.tblCountriesInfoes.FirstOrDefault(x => x.CountryID == _CountriesInfo.CountryID);
                if (_ckhCountryInfo != null)
                {
                    _ckhCountryInfo.Capital = _CountriesInfo.Capital;
                    _ckhCountryInfo.Population = _CountriesInfo.Population;
                    _ckhCountryInfo.Language = _CountriesInfo.Language;
                    _ckhCountryInfo.Currency = _CountriesInfo.Currency;
                    _ckhCountryInfo.TrainOperator = _CountriesInfo.TrainOperator;
                    _ckhCountryInfo.TouristBoard = _CountriesInfo.TouristBoard;
                    _ckhCountryInfo.TimeZone = _CountriesInfo.TimeZone;
                    _ckhCountryInfo.CallingCode = _CountriesInfo.CallingCode;
                    _ckhCountryInfo.CountryInfo = _CountriesInfo.CountryInfo;
                    _ckhCountryInfo.Drives = _CountriesInfo.Drives;
                    _ckhCountryInfo.Visas = _CountriesInfo.Visas;
                    _ckhCountryInfo.CountryID = _CountriesInfo.CountryID;
                    _ckhCountryInfo.InternetTLD = _CountriesInfo.InternetTLD;
                    _ckhCountryInfo.Title = _CountriesInfo.Title;
                    _ckhCountryInfo.IrCountryInfo = _CountriesInfo.IrCountryInfo;
                }

                ManageRoutes objroute = new ManageRoutes();
                objroute.UpdateRouteCount(Business.Route.CountryRoute, 0);

                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Role Master

        public List<aspnet_Roles> GetRolesList()
        {
            try
            {
                return db.aspnet_Roles.OrderBy(x => x.RoleName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Admin User Section

        public List<GetAllBranchList_Result> GetAllBranchlist()
        {
            try
            {
                return db.GetAllBranchList().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EmailCheck(string email)
        {
            try
            {
                var EmailChk = db.tblAdminUsers.Any(ty => ty.EmailAddress == email && ty.IsDeleted == false);
                if (EmailChk)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Guid AddAdminUser(AdminUser AdminUser)
        {
            try
            {
                bool result = db.tblAdminUsers.Any(x => x.UserName == AdminUser.UserName && x.Password == AdminUser.Password) || db.tblAffiliateUsers.Any(x => x.UserName == AdminUser.UserName && x.Password == AdminUser.Password);
                if (result)
                    throw new Exception("User Name is already exist. Please try different!");

                var _tblAdminUser = new tblAdminUser
                {
                    ID = AdminUser.ID,
                    UserName = AdminUser.UserName,
                    Password = AdminUser.Password,
                    Salutation = AdminUser.Salutation,
                    Forename = AdminUser.Forename,
                    Surname = AdminUser.Surname,
                    EmailAddress = AdminUser.EmailAddress,
                    Note = AdminUser.Note,
                    BranchID = AdminUser.BranchID,
                    RoleID = AdminUser.RoleID,
                    IsActive = AdminUser.IsActive,
                    IsAgent = AdminUser.IsAgent,
                    AssignTo = AdminUser.AssignTo,
                    CreatedOn = AdminUser.CreatedOn,
                    IsDeleted = AdminUser.IsDeleted,
                    MenualP2PIsActive = AdminUser.MenualP2PIsActive,
                    ManualPassIsActive = AdminUser.ManualPassIsActive,
                };
                db.AddTotblAdminUsers(_tblAdminUser);
                db.SaveChanges();
                return _tblAdminUser.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AdminUser> GetAdminUserListByBranchId(Guid branchid)
        {
            try
            {
                return db.tblAdminUsers.Where(t => t.BranchID == branchid && t.IsDeleted == false && t.IsActive == true && t.IsAgent == true).Select(x => new AdminUser { ID = x.ID, Name = x.UserName + " (" + x.Forename + " " + x.Surname + ")" }).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddUserLookupSites(AdminUserLookSites AdminUserLookSites)
        {
            try
            {
                tblAdminUserLookupSite _tblAdminUserLookupSite = new tblAdminUserLookupSite
                {
                    ID = AdminUserLookSites.ID,
                    AdminUserID = AdminUserLookSites.AdminUserID,
                    SiteID = AdminUserLookSites.SiteID
                };
                db.AddTotblAdminUserLookupSites(_tblAdminUserLookupSite);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*Country*/
        public int AddCountryLookupSites(tblCountrySiteLookUp countrySiteLookUp)
        {

            try
            {
                db.AddTotblCountrySiteLookUps(countrySiteLookUp);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public tblAdminUser GetUserListEdit(Guid ID)
        {
            try
            {
                return db.tblAdminUsers.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblAdminUser> GetUserList()
        {
            try
            {
                return db.tblAdminUsers.Where(x => x.IsAgent == true && x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateAdminUser(AdminUser AdminUser)
        {
            try
            {
                var _tblAdminUser = db.tblAdminUsers.FirstOrDefault(x => x.ID == AdminUser.ID);
                if (_tblAdminUser != null)
                {
                    _tblAdminUser.ID = AdminUser.ID;
                    _tblAdminUser.UserName = AdminUser.UserName;
                    _tblAdminUser.Password = AdminUser.Password;
                    _tblAdminUser.Salutation = AdminUser.Salutation;
                    _tblAdminUser.Forename = AdminUser.Forename;
                    _tblAdminUser.Surname = AdminUser.Surname;
                    _tblAdminUser.EmailAddress = AdminUser.EmailAddress;
                    _tblAdminUser.Note = AdminUser.Note;
                    _tblAdminUser.BranchID = AdminUser.BranchID;
                    _tblAdminUser.RoleID = AdminUser.RoleID;
                    _tblAdminUser.IsActive = AdminUser.IsActive;
                    _tblAdminUser.IsAgent = AdminUser.IsAgent;
                    _tblAdminUser.AssignTo = AdminUser.AssignTo;
                    _tblAdminUser.ModifiedOn = AdminUser.ModifiedOn;
                    _tblAdminUser.MenualP2PIsActive = AdminUser.MenualP2PIsActive;
                    _tblAdminUser.ManualPassIsActive = AdminUser.ManualPassIsActive;
                }

                db.SaveChanges();
                return _tblAdminUser.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActivateInActivateAdminUser(Guid ID)
        {
            var _adminuser = db.tblAdminUsers.FirstOrDefault(x => x.ID == ID);
            if (_adminuser != null)
            {
                _adminuser.IsActive = !(_adminuser.IsActive);
                db.SaveChanges();
            }
        }
        public void AgentNotAgentAdminUser(Guid ID)
        {
            var _adminuser = db.tblAdminUsers.FirstOrDefault(x => x.ID == ID);
            if (_adminuser != null)
            {
                _adminuser.IsAgent = !(_adminuser.IsAgent);
                db.SaveChanges();
            }
        }
        public bool DeleteAdminUser(Guid id)
        {
            try
            {
                var _adminuser = db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                if (_adminuser != null)
                {
                    _adminuser.IsActive = false;
                    _adminuser.IsDeleted = true;
                    _adminuser.DeletedOn = DateTime.Now;
                    _adminuser.DeletedBy = AdminuserInfo.UserID;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Banner
        public tblBanner GetBannerByID(Guid id)
        {
            try
            {
                return db.tblBanners.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BannerList> GetBanner(Guid siteID)
        {
            try
            {
                var res = db.tblBanners.Where(x => x.tblBannerLookups.Any(y => y.SiteID == siteID)).AsEnumerable().Select(x => new BannerList
                {
                    SiteName = GetBannerSiteName(x.ID),
                    ID = x.ID,
                    Title = x.Title,
                    IsActive = x.IsActive,
                    Description = x.Description,
                    NavUrl = x.NavUrl,
                    ImageUrl = x.ImageUrl,
                    sortorder = GetBannerSortingorderByIdandSiteId(siteID, x.ID)
                }).ToList();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetBannerSortingorderByIdandSiteId(Guid siteID, Guid ID)
        {
            try
            {
                var data = db.tblBannerLookups.FirstOrDefault(t => t.SiteID == siteID && t.BannerId == ID);
                if (data != null)
                    return Convert.ToString(data.SortOrder);
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updateBannerSortingorderByIdandSiteId(Guid siteID, Guid ID, int SortOrder)
        {
            try
            {
                var exists = db.tblBannerLookups.Any(t => t.SiteID == siteID && t.SortOrder == SortOrder && t.BannerId != ID);
                if (exists)
                    throw new Exception("Short order already in use.");
                var data = db.tblBannerLookups.FirstOrDefault(t => t.SiteID == siteID && t.BannerId == ID);
                if (data != null)
                    data.SortOrder = SortOrder;
                else
                {

                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetBannerSiteName(Guid id)
        {
            var res = (from tdosl in db.tblBannerLookups
                       join ts in db.tblSites on tdosl.SiteID equals ts.ID
                       where tdosl.BannerId == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";
            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }

        public class BannerList : tblBanner
        {
            public string SiteName { get; set; }
            public string sortorder { get; set; }
        }

        public Guid UpdateBanner(tblBanner Banner)
        {
            try
            {
                var _Banner = db.tblBanners.FirstOrDefault(x => x.ID == Banner.ID);
                if (_Banner != null)
                {
                    _Banner.Title = Banner.Title;
                    _Banner.NavUrl = Banner.NavUrl;
                    _Banner.Description = Banner.Description;
                    _Banner.IsActive = Banner.IsActive;
                    if (!string.IsNullOrEmpty(Banner.ImageUrl))
                        _Banner.ImageUrl = Banner.ImageUrl;
                }
                db.SaveChanges();
                return _Banner.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddBanner(tblBanner Banner)
        {
            try
            {
                var _Banner = new tblBanner
                {
                    ID = Banner.ID,
                    Title = Banner.Title,
                    NavUrl = Banner.NavUrl,
                    Description = Banner.Description,
                    ImageUrl = Banner.ImageUrl,
                    IsActive = Banner.IsActive,
                };
                db.AddTotblBanners(_Banner);
                db.SaveChanges();
                return _Banner.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddBannerLookupSites(tblBannerLookup BannerSiteLookUp)
        {
            try
            {
                db.AddTotblBannerLookups(BannerSiteLookUp);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveBanner(Guid id)
        {
            try
            {
                var rec = db.tblBanners.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBanner(Guid id)
        {
            try
            {
                db.tblBannerLookups.Where(w => w.BannerId == id).ToList().ForEach(db.tblBannerLookups.DeleteObject);
                db.SaveChanges();
                var rec = db.tblBanners.FirstOrDefault(x => x.ID == id);
                db.tblBanners.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblBanner GetBannerListEdit(Guid ID)
        {
            try
            {
                return db.tblBanners.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Page Section

        public tblPage GetPageListByID(Guid ID)
        {
            try
            {
                return db.tblPages.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPage GetPageByNavID(Guid navID, Guid siteID)
        {
            try
            {
                return db.tblPages.FirstOrDefault(x => x.NavigationID == navID && x.SiteID == siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPage GetPageListByName(string pageName)
        {
            try
            {
                return db.tblPages.FirstOrDefault(x => x.PageName == pageName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblPage> GetPageList(Guid ID)
        {
            try
            {
                return db.tblPages.Where(x => x.SiteID == ID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPage GetPageEdit(Guid ID)
        {
            try
            {
                return db.tblPages.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPage GetPageDetailsByUrl(string url)
        {
            try
            {
                return db.tblPages.FirstOrDefault(x => x.Url.ToLower().Trim() == url.ToLower().Trim());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ImagePath> GetBannerImgByID(List<int> idList)
        {
            try
            {
                return (from img in db.tblImages
                        join cat in db.tblImgCategories
                            on img.CategoryID equals cat.ID
                        where idList.Contains(img.ID)
                        select new ImagePath
                        {
                            ImgUrl = img.ImagePath
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ImagePath> GetRightImgByID(List<int> idList)
        {
            try
            {
                return (from img in db.tblImages
                        join cat in db.tblImgCategories
                            on img.CategoryID equals cat.ID
                        where idList.Contains(img.ID) && cat.CategoryName == "Right Panel"
                        select new ImagePath
                        {
                            ImgUrl = img.ImagePath
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ImagePath
        {
            public string ImgUrl { get; set; }
        }

        public int AddPage(tblPage page)
        {
            try
            {
                db.AddTotblPages(page);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdatePage(tblPage page)
        {
            try
            {
                var _page = db.tblPages.FirstOrDefault(x => x.ID == page.ID);
                if (_page != null)
                {
                    _page.PageName = page.PageName;
                    _page.Url = page.Url;
                    _page.NavigationID = page.NavigationID;
                    _page.SiteID = page.SiteID;
                    _page.LayoutID = page.LayoutID;
                    _page.IsActive = page.IsActive;

                    if (page.BannerIDs != "")
                        _page.BannerIDs = page.BannerIDs;

                    _page.PageHeading = page.PageHeading;
                    _page.PageContent = page.PageContent;

                    if (page.FooterImg != "")
                        _page.FooterImg = page.FooterImg;

                    if (page.RightPanelImgID != "")
                        _page.RightPanelImgID = page.RightPanelImgID;
                    //if (page.RightPanelNavUrl != "")
                    _page.RightPanelNavUrl = page.RightPanelNavUrl;
                    if (page.RightPanel1 != "")
                        _page.RightPanel1 = page.RightPanel1;
                    if (page.RightPanel2 != "")
                        _page.RightPanel2 = page.RightPanel2;

                    if (page.ContactPanel != "")
                        _page.ContactPanel = page.ContactPanel;
                    if (page.ContactCall != "")
                        _page.ContactCall = page.ContactCall;

                    if (page.FooterHeader != "")
                        _page.FooterHeader = page.FooterHeader;
                    _page.MetaTitle = page.MetaTitle;
                    _page.Description = page.Description;
                    _page.Keyword = page.Keyword;

                    _page.HomeSearchText = page.HomeSearchText;
                    _page.HomeCountryText = page.HomeCountryText;
                    _page.SiteType = page.SiteType;

                    _page.HomePageHeading1 = page.HomePageHeading1;
                    _page.HomePageHeading2 = page.HomePageHeading2;
                    _page.HomePageDescription1 = page.HomePageDescription1;
                    _page.HomePageDescription2 = page.HomePageDescription2;

                    _page.VEbanner = page.VEbanner;
                    _page.VEptpimg = page.VEptpimg;
                    _page.VEmap = page.VEmap;
                    _page.VEmaps1 = page.VEmaps1;
                    _page.VEmaps2 = page.VEmaps2;

                    _page.VEmaplink = page.VEmaplink;
                    _page.VEmaps1link = page.VEmaps1link;
                    _page.VEmaps2link = page.VEmaps2link;

                    _page.VEpass = page.VEpass;
                    _page.VEtrain = page.VEtrain;
                    _page.VEnewadventure = page.VEnewadventure;
                    _page.VEbtncontactcall = page.VEbtncontactcall;
                    _page.VEbtncontactbook = page.VEbtncontactbook;
                    _page.VEbtncontactlive = page.VEbtncontactlive;
                    _page.VEbtncontactemail = page.VEbtncontactemail;
                    _page.VEbtntext1 = page.VEbtntext1;
                    _page.VEbtntext2 = page.VEbtntext2;
                    _page.VEbtntext3 = page.VEbtntext3;
                    _page.VEbtntext4 = page.VEbtntext4;
                    _page.VEbtntext5 = page.VEbtntext5;
                    _page.VEbtntext6 = page.VEbtntext6;
                    _page.VEbtntext7 = page.VEbtntext7;
                    _page.VEbtntext8 = page.VEbtntext8;

                    _page.RailTicketInnerHeading = page.RailTicketInnerHeading;
                    _page.RailTicketSubInnerLower = page.RailTicketSubInnerLower;
                    if (page.LeftBannerImage != "")
                        _page.LeftBannerImage = page.LeftBannerImage;
                    if (page.RightBanner != "")
                        _page.RightBanner = page.RightBanner;
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactivePage(Guid id)
        {
            try
            {
                var objPage = db.tblPages.FirstOrDefault(x => x.ID == id);
                if (objPage != null)
                    objPage.IsActive = !objPage.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeletePage(Guid id)
        {
            try
            {
                var objPage = db.tblPages.FirstOrDefault(x => x.ID == id);
                if (objPage != null)
                    db.tblPages.DeleteObject(objPage);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Class Section

        public int AddClass(tblClassMst classMst)
        {
            try
            {
                if (classMst.ID == Guid.Empty)
                {
                    classMst.ID = Guid.NewGuid();
                    db.tblClassMsts.AddObject(classMst);
                }
                {
                    var ocls = db.tblClassMsts.FirstOrDefault(x => x.ID == classMst.ID);
                    if (ocls != null)
                    {
                        ocls.IsActive = classMst.IsActive;
                        ocls.ModifiedBy = classMst.ModifiedBy;
                        ocls.ModifiedOn = classMst.ModifiedOn;
                        ocls.Name = classMst.Name;
                        ocls.EurailCode = classMst.EurailCode;
                    }
                }
                return db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveClass(Guid id)
        {
            try
            {
                var rec = db.tblClassMsts.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool DeleteClass(Guid id)
        {
            try
            {
                var ocls = db.tblClassMsts.FirstOrDefault(x => x.ID == id);
                if (ocls != null) db.tblClassMsts.DeleteObject(ocls);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblClassMst> GetClassList()
        {
            try
            {
                return db.tblClassMsts.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblClassMst GetClassById(Guid id)
        {
            try
            {
                return db.tblClassMsts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Product Valid Up To
        public Guid AddProductValidUpTo(tblProductValidUpTo validUpTo)
        {
            try
            {

                if (validUpTo.ID == Guid.Empty)
                {
                    validUpTo.ID = Guid.NewGuid();
                    db.tblProductValidUpToes.AddObject(validUpTo);
                }
                else
                {
                    var oVal = db.tblProductValidUpToes.FirstOrDefault(x => x.ID == validUpTo.ID);
                    if (oVal != null)
                    {
                        oVal.IsActive = validUpTo.IsActive;
                        oVal.EurailCode = validUpTo.EurailCode;
                        oVal.PromoPassText = validUpTo.PromoPassText;
                        oVal.Note = validUpTo.Note;
                        oVal.Day = validUpTo.Day;
                        oVal.Month = validUpTo.Month;
                        oVal.Year = validUpTo.Year;
                        oVal.ModifiedBy = validUpTo.CreatedBy;
                        oVal.ModifiedOn = validUpTo.CreatedOn;

                    }
                }
                db.SaveChanges();
                return validUpTo.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddProductValidUpToName(tblProductValidUpToName upToName)
        {
            try
            {

                if (upToName.ID == Guid.Empty)
                {
                    upToName.ID = Guid.NewGuid();
                    db.tblProductValidUpToNames.AddObject(upToName);
                }
                else
                {
                    var oVal = db.tblProductValidUpToNames.FirstOrDefault(x => x.ID == upToName.ID);
                    if (oVal != null)
                    {
                        oVal.DefaultWhenMissing = upToName.DefaultWhenMissing;
                        oVal.DefaultWhenMissing = upToName.DefaultWhenMissing;
                        oVal.LanguageID = upToName.LanguageID;
                        oVal.Name = upToName.Name;
                        oVal.ProductValidUpToID = upToName.ProductValidUpToID;
                    }

                }
                db.SaveChanges();
                return upToName.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactivecProductValidUpTo(Guid id)
        {
            try
            {
                var rec = db.tblProductValidUpToes.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void IsFlexiProductValidUpTo(Guid id)
        {
            try
            {
                var rec = db.tblProductValidUpToes.FirstOrDefault(x => x.ID == id);
                rec.IsFlexi = !rec.IsFlexi;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool DeleteProductValidUpTo(Guid id)
        {
            try
            {
                db.tblProductValidUpToNames.Where(t => t.ProductValidUpToID == id).ToList().ForEach(db.tblProductValidUpToNames.DeleteObject);
                var oProd = db.tblProductValidUpToes.FirstOrDefault(t => t.ID == id);
                db.tblProductValidUpToes.DeleteObject(oProd);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Traveller validity already in used.");
            }
        }
        public bool DeleteProductValidUpToName(Guid id, Guid idLang, string name)
        {
            try
            {
                var oProd = db.tblProductValidUpToNames.FirstOrDefault(t => t.ProductValidUpToID == id && t.LanguageID == idLang && t.Name == name);
                if (oProd != null)
                    db.tblProductValidUpToNames.DeleteObject(oProd);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ValidUpTo> GetProductValidUpToNameList()
        {
            try
            {
                Guid langId = GetLanguageId();
                return (from pName in db.tblProductValidUpToNames
                        join p in db.tblProductValidUpToes
                            on pName.ProductValidUpToID equals p.ID
                        where pName.LanguageID == langId
                        select new ValidUpTo
                        {
                            ID = p.ID,
                            Name = pName.Name,
                            PromoPassText = p.PromoPassText,
                            IsActive = p.IsActive,
                            EurailCode = p.EurailCode,
                            IsFlexi = p.IsFlexi,
                            Note = p.Note
                        }).ToList().OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblProductValidUpToName> GetProductValidUpToNameById(Guid id)
        {
            try
            {
                return db.tblProductValidUpToNames.Where(x => x.ProductValidUpToID == id).Select(x => x).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblProductValidUpTo GetProductValidUpToById(Guid id)
        {
            try
            {
                return db.tblProductValidUpToes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //--Get Language Id
        public Guid GetLanguageId()
        {
            Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
            var oLng = db.tblLanguagesMsts.FirstOrDefault(x => x.ID == langid);
            return oLng != null ? oLng.ID : new Guid();
        }
        #endregion

        #region Traveler
        public Guid AddTraveler(tblTravelerMst oTravelerMst)
        {
            try
            {
                if (oTravelerMst.ID == new Guid())
                {
                    oTravelerMst.ID = Guid.NewGuid();
                    var oTrav = db.tblTravelerMsts.Where(x => x.SortOrder == oTravelerMst.SortOrder);
                    if (oTrav.Count() == 0)
                        db.tblTravelerMsts.AddObject(oTravelerMst);
                    else
                        throw new Exception("Sort order already assign.");
                }
                else
                {
                    var oTrav = db.tblTravelerMsts.FirstOrDefault(x => x.ID == oTravelerMst.ID);
                    var oTravRank = db.tblTravelerMsts.Where(x => x.ID != oTravelerMst.ID && x.SortOrder == oTravelerMst.SortOrder);
                    if (oTravRank.Count() == 0)
                    {
                        if (oTrav != null)
                        {
                            oTrav.SortOrder = oTravelerMst.SortOrder;
                            oTrav.FromAge = oTravelerMst.FromAge;
                            oTrav.EurailCode = oTravelerMst.EurailCode;
                            oTrav.IsActive = oTravelerMst.IsActive;
                            oTrav.ModifiedBy = oTravelerMst.CreatedBy;
                            oTrav.ModifiedOn = oTravelerMst.CreatedOn;
                            oTrav.Name = oTravelerMst.Name;
                            oTrav.ToAge = oTravelerMst.ToAge;
                            oTrav.Note = oTravelerMst.Note;
                        }
                    }
                    else
                    {
                        throw new Exception("Sort order already assign.");
                    }
                }
                db.SaveChanges();
                return oTravelerMst.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveTraveler(Guid id)
        {
            try
            {
                var rec = db.tblTravelerMsts.FirstOrDefault(x => x.ID == id);
                if (rec != null) rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteTraveler(Guid id)
        {
            try
            {
                var oTrav = db.tblTravelerMsts.FirstOrDefault(x => x.ID == id);
                if (oTrav != null)
                    db.DeleteObject(oTrav);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblTravelerMst> GetTravelerList()
        {
            try
            {
                return db.tblTravelerMsts.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblTravelerMst> GetTravelerListWithType()
        {
            try
            {
                var lst = db.tblTravelerMsts.OrderBy(x => x.Name).ToList();
                return lst.Select(ty => new tblTravelerMst { ID = ty.ID, Name = (ty.Name + (ty.Note == null ? "" : " ( " + ty.Note + " )")), IsActive = ty.IsActive }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblTravelerMst GetTravelerById(Guid id)
        {
            try
            {
                return db.tblTravelerMsts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Image Category
        public List<tblImgCategory> GetImageCategoryList()
        {
            try
            {
                return db.tblImgCategories.OrderBy(x => x.ID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblImgCategory GetImageCategoryListEdit(int ID)
        {
            try
            {
                return db.tblImgCategories.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddImgCategory(tblImgCategory category)
        {
            try
            {
                tblImgCategory _category = new tblImgCategory
                {
                    CategoryName = category.CategoryName,
                    Width = category.Width,
                    Height = category.Height,
                    IsActive = category.IsActive
                    //ID = category.ID
                };
                db.AddTotblImgCategories(_category);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateImgCategory(tblImgCategory category)
        {
            try
            {
                var _category = db.tblImgCategories.FirstOrDefault(x => x.ID == category.ID);
                if (_category != null)
                {
                    _category.CategoryName = category.CategoryName;
                    _category.Width = category.Width;
                    _category.Height = category.Height;
                    _category.IsActive = category.IsActive;
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Images
        public List<ImageList> GetImageList()
        {
            try
            {
                return (from i in db.tblImages
                        join iC in db.tblImgCategories
                            on i.CategoryID equals iC.ID
                        select new ImageList
                        {
                            ID = i.ID,
                            Name = i.ImageName,
                            Path = i.ImagePath,
                            CategoryName = iC.CategoryName,
                            Size = i.ImageSize,
                            Type = i.ImageType,
                            Width = (int)iC.Width,
                            Height = (int)iC.Height,
                            IsActive = (bool)i.IsActive
                        }).ToList().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblImage> GetImageListByID(int ID)
        {
            try
            {
                return db.tblImages.Where(x => x.CategoryID == ID && x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblImage GetImageListEdit(int ID)
        {
            try
            {
                return db.tblImages.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddImage(tblImage img)
        {
            try
            {
                tblImage _img = new tblImage
                {
                    CategoryID = img.CategoryID,
                    ImageName = img.ImageName,
                    ImagePath = img.ImagePath,
                    ImageSize = img.ImageSize,
                    ImageType = img.ImageType,
                    IsActive = img.IsActive
                };
                db.AddTotblImages(_img);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateImage(tblImage img)
        {
            try
            {
                var _img = db.tblImages.FirstOrDefault(x => x.ID == img.ID);
                if (_img != null)
                {
                    _img.CategoryID = img.CategoryID;
                    if (img.ImagePath != null)
                    {
                        _img.ImagePath = img.ImagePath;
                    }
                    _img.ImageSize = img.ImageSize;
                    _img.ImageType = img.ImageType;
                    _img.ImageName = img.ImageName;
                    _img.IsActive = img.IsActive;
                }
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveImage(int id)
        {
            try
            {
                var objImg = db.tblImages.FirstOrDefault(x => x.ID == id);
                if (objImg != null)
                    objImg.IsActive = !objImg.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveImageCat(int id)
        {
            try
            {
                var objImg = db.tblImgCategories.FirstOrDefault(x => x.ID == id);
                if (objImg != null)
                    objImg.IsActive = !objImg.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Homepage
        public tblPage GetPageByName(string pageName)
        {
            try
            {
                return db.tblPages.FirstOrDefault(x => x.PageName == pageName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteImage(int id)
        {
            try
            {
                var objImage = db.tblImages.FirstOrDefault(x => x.ID == id);
                if (objImage != null)
                    db.tblImages.DeleteObject(objImage);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delivery Option
        public Guid AddDeliveryOption(FieldsDeliveryOption option)
        {
            try
            {
                Guid id = option.ID == Guid.Empty ? Guid.NewGuid() : option.ID;

                if (option.ID == Guid.Empty)
                {
                    db.tblDeliveryOptions.AddObject(new tblDeliveryOption
                        {
                            ID = id,
                            BasePrice = option.BasePrice,
                            CreatedBy = option.CreatedBy,
                            CreatedOn = DateTime.Now,
                            Description = option.Description,
                            IsActive = option.IsActive,
                            IsActiveForAdmin = option.IsActiveForAdmin,
                            Name = option.Name
                        });
                }
                else
                {
                    var rec = db.tblDeliveryOptions.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.IsActive = option.IsActive;
                        rec.BasePrice = option.BasePrice;
                        rec.Name = option.Name;
                        rec.Description = option.Description;
                        rec.ModifiedBy = option.CreatedBy;
                        rec.ModifiedOn = DateTime.Now;
                    }
                }

                db.tblDeliveryOptionCountries.Where(x => x.DeliveryOptionID == id).ToList().ForEach(db.tblDeliveryOptionCountries.DeleteObject);
                foreach (var item in option.ListCountryId)
                {
                    db.tblDeliveryOptionCountries.AddObject(new tblDeliveryOptionCountry
                    {
                        CountryID = item,
                        ID = Guid.NewGuid(),
                        DeliveryOptionID = id,
                    });
                }

                db.tblDeliveryOptionSiteLookUps.Where(x => x.DeliveryOptionID == id).ToList().ForEach(db.tblDeliveryOptionSiteLookUps.DeleteObject);
                foreach (var item in option.ListSiteId)
                {
                    db.tblDeliveryOptionSiteLookUps.AddObject(new tblDeliveryOptionSiteLookUp
                    {
                        SiteID = item,
                        ID = Guid.NewGuid(),
                        DeliveryOptionID = id,
                    });
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteDeliveryOption(Guid id)
        {
            try
            {
                db.tblDeliveryOptions.Where(x => x.ID == id).ToList().ForEach(db.tblDeliveryOptions.DeleteObject);
                db.tblDeliveryOptionSiteLookUps.Where(x => x.DeliveryOptionID == id).ToList().ForEach(db.tblDeliveryOptionSiteLookUps.DeleteObject);
                db.tblDeliveryOptionCountries.Where(x => x.DeliveryOptionID == id).ToList().ForEach(db.tblDeliveryOptionCountries.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInActiveDeliveryOption(Guid id)
        {
            try
            {
                var rec = db.tblDeliveryOptions.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                {
                    rec.IsActive = !rec.IsActive;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInActiveDeliveryOptionForAdmin(Guid id)
        {
            try
            {
                var rec = db.tblDeliveryOptions.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                {
                    rec.IsActiveForAdmin = !rec.IsActiveForAdmin;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FieldsDeliveryOption> GetDeliveryOptions(Guid siteID)
        {
            try
            {
                var res = db.tblDeliveryOptions.Where(x => x.tblDeliveryOptionSiteLookUps.Any(y => y.SiteID == siteID)).Select(x => new FieldsDeliveryOption
                    {
                        BasePrice = x.BasePrice,
                        ID = x.ID,
                        IsActive = x.IsActive,
                        IsActiveForAdmin = x.IsActiveForAdmin,
                        Name = x.Name
                    }).AsEnumerable().Select(x => new FieldsDeliveryOption
                        {
                            SiteName = GetSiteName(x.ID),
                            BasePrice = x.BasePrice,
                            ID = x.ID,
                            IsActive = x.IsActive,
                            IsActiveForAdmin = x.IsActiveForAdmin,
                            Name = x.Name
                        }).ToList();

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FieldsDeliveryOption GetDeliveryOptionsById(Guid id)
        {
            try
            {
                var rec = db.tblDeliveryOptions.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new FieldsDeliveryOption
                        {
                            BasePrice = rec.BasePrice,
                            IsActive = rec.IsActive,
                            Name = rec.Name,
                            Description = rec.Description,
                            ListSiteId = rec.tblDeliveryOptionSiteLookUps.Where(x => x.DeliveryOptionID == id).Select(x => x.SiteID).ToList(),
                            ListCountryId = rec.tblDeliveryOptionCountries.Where(x => x.DeliveryOptionID == id).Select(x => (Guid)x.CountryID).ToList()
                        };
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSiteName(Guid id)
        {
            var res = (from tdosl in db.tblDeliveryOptionSiteLookUps
                       join ts in db.tblSites on tdosl.SiteID equals ts.ID
                       where tdosl.DeliveryOptionID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";

            }
            return siteName;

        }

        #endregion

        #region Eurail Country Section

        public int AddEurailCountry(tblEurailCountry country)
        {
            try
            {
                if (country.ID == Guid.Empty)
                {
                    country.ID = Guid.NewGuid();
                    db.tblEurailCountries.AddObject(country);
                }
                {
                    var ocls = db.tblEurailCountries.FirstOrDefault(x => x.ID == country.ID);
                    if (ocls != null)
                    {
                        ocls.IsActive = country.IsActive;
                        ocls.CountryCode = country.CountryCode;
                        ocls.Country1 = country.Country1;
                        ocls.Country2 = country.Country2;
                        ocls.Country3 = country.Country3;
                        ocls.Country4 = country.Country4;
                        ocls.Country5 = country.Country5;
                        ocls.OtherCountry = country.OtherCountry;
                        ocls.PriceBindLevel = country.PriceBindLevel;
                    }
                }
                return db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveEurailCountry(Guid id)
        {
            try
            {
                var rec = db.tblEurailCountries.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool DeleteEurailCountry(Guid id)
        {
            try
            {
                var ocls = db.tblEurailCountries.FirstOrDefault(x => x.ID == id);
                if (ocls != null) db.tblEurailCountries.DeleteObject(ocls);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<EurailCountries> GetEurailCountryList()
        {
            try
            {
                List<EurailCountries> list = (from tec in db.tblEurailCountries
                                              join tpb in db.tblPriceBands on tec.PriceBindLevel equals tpb.ID
                                              select new EurailCountries { ID = tec.ID, Country1 = tec.Country1, Country2 = tec.Country2, Country3 = tec.Country3, Country4 = tec.Country4, Country5 = tec.Country5, CountryCode = tec.CountryCode, IsActive = tec.IsActive, OtherCountry = tec.OtherCountry, Level = (tec.CountryCode > 2000 ? (tpb.ID > 1 ? tpb.Name : "---") : string.Empty) }).OrderBy(t => t.CountryCode).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblEurailCountry GetEurailCountryById(Guid id)
        {
            try
            {
                return db.tblEurailCountries.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region NewsletterUsers
        public Guid AddNewsletterUser(tblNewsLetterUser news)
        {
            try
            {
                tblNewsLetterUser _news = new tblNewsLetterUser
                {
                    ID = news.ID,
                    Email = news.Email,
                    Name = news.Name,
                    SiteID = news.SiteID,
                    IsActive = news.IsActive,
                    CreatedOn = news.CreatedOn,
                    IsSubscribed = news.IsSubscribed
                };
                db.AddTotblNewsLetterUsers(_news);
                db.SaveChanges();
                return _news.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblNewsArea> GetNewsArea()
        {
            try
            {
                return db.tblNewsAreas.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddNewsAreaLookupSites(tblNewsLetterUserAreaLookup newsAreaLookUp)
        {

            try
            {
                db.AddTotblNewsLetterUserAreaLookups(newsAreaLookUp);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SubscribedUsers> GetSubscribedUserList(Guid siteId)
        {
            try
            {
                var olist = (from sUser in db.tblNewsLetterUsers
                             join s in db.tblSites on sUser.SiteID equals s.ID
                             join sLook in db.tblNewsLetterUserAreaLookups
                             on sUser.ID equals sLook.UserID
                             select new { s, sUser, sLook }).ToList();

                List<SubscribedUsers> list = new List<SubscribedUsers>();

                list = olist.Distinct().Select(x => new SubscribedUsers
                    {
                        ID = x.sUser.ID,
                        IsActive = x.sUser.IsActive,
                        Name = x.sUser.Name.Trim(),
                        Email = x.sUser.Email.Trim(),
                        SiteName = x.s.DisplayName,
                        AreaName = GetAreaName(x.sUser.ID),
                        IsSubscribed = x.sUser.IsSubscribed
                    }).ToList().GroupBy(y => new { y.ID, y.IsActive, y.Name, y.Email, y.SiteName, y.AreaName, y.IsSubscribed }).Select(z => new SubscribedUsers { ID = z.Key.ID, Name = z.Key.Name, Email = z.Key.Email, IsActive = z.Key.IsActive, SiteName = z.Key.SiteName, AreaName = z.Key.AreaName, IsSubscribed = z.Key.IsSubscribed }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetAreaName(Guid userId)
        {
            var res = (from slook in db.tblNewsLetterUserAreaLookups
                       join a in db.tblNewsAreas on slook.AreaID equals a.ID
                       where slook.UserID == userId
                       select new { a.Name }).ToList();
            string areaName = "";
            foreach (var item in res)
            {
                areaName += "- " + item.Name + "<br/>";
            }
            return areaName;
        }

        public void ActiveInactiveSubsUsers(Guid id)
        {
            try
            {
                var objUser = db.tblNewsLetterUsers.FirstOrDefault(x => x.ID == id);
                if (objUser != null)
                    objUser.IsActive = !objUser.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SubscribeUnsubscribeUsers(Guid id)
        {
            try
            {
                var objUser = db.tblNewsLetterUsers.FirstOrDefault(x => x.ID == id);
                if (objUser != null)
                    objUser.IsSubscribed = !objUser.IsSubscribed;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteSubscribedUser(Guid id)
        {
            try
            {
                db.tblNewsLetterUserAreaLookups.Where(x => x.UserID == id).ToList().ForEach(db.tblNewsLetterUserAreaLookups.DeleteObject);
                var rec = db.tblNewsLetterUsers.FirstOrDefault(x => x.ID == id);
                db.tblNewsLetterUsers.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Continent
        public int AddContinent(tblContinent conty)
        {
            try
            {
                if (conty.ID == new Guid())
                {
                    conty.ID = Guid.NewGuid();
                    if (!db.tblContinents.Any(x => x.Name == conty.Name))
                        db.tblContinents.AddObject(conty);
                    else
                        throw new Exception("Name is alredy exist.");
                }
                {
                    var ocls = db.tblContinents.FirstOrDefault(x => x.ID == conty.ID);
                    if (ocls != null)
                    {
                        ocls.IsActive = conty.IsActive;
                        ocls.Name = conty.Name;
                        ocls.ModifyBy = conty.CreatedBy;
                        ocls.ModifyOn = conty.CreatedOn;
                    }
                }
                return db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveContinent(Guid id)
        {
            try
            {
                var rec = db.tblContinents.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool DeleteContinent(Guid id)
        {
            try
            {
                var ocls = db.tblContinents.FirstOrDefault(x => x.ID == id);
                if (ocls != null) db.tblContinents.DeleteObject(ocls);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblContinent> GetContinentList()
        {
            try
            {
                return db.tblContinents.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblContinent GetContinentById(Guid id)
        {
            try
            {
                return db.tblContinents.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Contactus
        public int AddContact(tblContact oContact)
        {
            try
            {
                var contact = new tblContact
                {
                    ID = oContact.ID,
                    SiteID = oContact.SiteID,
                    Name = oContact.Name,
                    Email = oContact.Email,
                    Phone = oContact.Phone,
                    OrderNumber = oContact.OrderNumber,
                    Description = oContact.Description
                };

                db.AddTotblContacts(contact);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblContact> GetContactsList(Guid siteId, int startIndex, int lastIndex)
        {
            try
            {
                return db.tblContacts.Where(x => x.SiteID == siteId).OrderBy(x => x.Name).Skip(startIndex).Take(lastIndex).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteContactInfo(Guid id)
        {
            try
            {
                db.tblContacts.DeleteObject(db.tblContacts.FirstOrDefault(x => x.ID == id));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int TotalNumberOfContacts()
        {
            return db.tblContacts.Count();
        }

        //Sendcontactus email
        public bool SendCancelOrderMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail)
        {
            try
            {
                var masterPage = new Masters();
                var message = new MailMessage();
                var smtpClient = new SmtpClient();
                var result = masterPage.GetEmailSettingDetail(siteId);
                if (result != null)
                {
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                    message.From = new MailAddress(FromEmail, FromEmail);
                    message.To.Add(ToMail);
                    string BccEmail = new ManageFrontWebsitePage().GetBccEmail(siteId);
                    if (!string.IsNullOrEmpty(BccEmail))
                        message.Bcc.Add(BccEmail);
                    //message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = Body;
                    smtpClient.Send(message);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }

        //Sendcontactus email
        public bool SendContactUsMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail)
        {
            try
            {
                var masterPage = new Masters();
                var message = new MailMessage();
                var smtpClient = new SmtpClient();
                var result = masterPage.GetEmailSettingDetail(siteId);
                if (result != null)
                {
                    var fromAddres = new MailAddress(FromEmail, FromEmail);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                    message.From = fromAddres;
                    message.To.Add(ToMail);
                    message.Subject = "User Query!";
                    message.IsBodyHtml = true;
                    message.Body = Body;
                    smtpClient.Send(message);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }
        public bool EnableContactEmail(Guid siteId)
        {
            try
            {
                var result = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (result != null)
                    return (bool)result.EnableContactEmail;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SendEmailMessage(Guid siteId, string FromName, string FromEmail, string ToEmail, string CC, string BCC, string Subject, string Content, Attachment[] Attachement, string MailType, string Status, string Id)
        {
            bool retVal = false;
            string MailPrefix = "IR_EQO";
            string replyto = "navratantest.1track@dotsquares.com";
            var masterPage = new Masters();
            var message = new MailMessage();
            var smtpClient = new SmtpClient();
            var smtpDetails = masterPage.GetEmailSettingDetail(siteId);
            try
            {
                MailAddress fromAddres = new MailAddress(FromEmail, "1Track EQO");
                smtpClient.Host = smtpDetails.SmtpHost;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(smtpDetails.SmtpUser, smtpDetails.SmtpPass);
                message.From = fromAddres;

                if (ToEmail != null)
                {
                    message.To.Add(ToEmail);
                    if (!string.IsNullOrEmpty(CC))
                        message.To.Add(CC);

                    if (!string.IsNullOrEmpty(BCC))
                        message.To.Add(BCC);

                    if (Attachement != null)
                        foreach (Attachment att in Attachement)
                        {
                            message.Attachments.Add(att);
                        }

                    message.Bcc.Add(smtpDetails.SmtpUser);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = Content;
                    message.ReplyToList.Add(new MailAddress(replyto, MailPrefix + "_" + MailType + "_" + Status.Replace(' ', '_') + "_" + Id));
                    smtpClient.Send(message);
                }
                retVal = true;
            }
            catch (Exception ex)
            {
                throw;
            }

            return retVal;
        }
        #endregion

        #region Feedback
        public int AddFeedback(tblFeedback oFeedback)
        {
            try
            {
                var feedback = new tblFeedback
                {
                    ID = oFeedback.ID,
                    SiteID = oFeedback.SiteID,
                    Name = oFeedback.Name,
                    Email = oFeedback.Email,
                    Phone = oFeedback.Phone,
                    TopicID = oFeedback.TopicID,
                    Description = oFeedback.Description
                };

                db.AddTotblFeedbacks(feedback);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FeedbackInfo> GetFeedbackList(Guid siteId, int startIndex, int lastIndex)
        {
            try
            {
                var list = (from fb in db.tblFeedbacks
                            join fbT in db.tblFeedbackTopics
                                on fb.TopicID equals fbT.ID
                            where fb.SiteID == siteId
                            select new { fb, fbT });

                return list.Select(x => new FeedbackInfo
                    {
                        ID = x.fb.ID,
                        Name = x.fb.Name,
                        Email = x.fb.Email,
                        Phone = x.fb.Phone,
                        TopicName = x.fbT.Name,
                        Description = x.fb.Description
                    }).OrderBy(x => x.Name).Skip(startIndex).Take(lastIndex).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteFeedback(Guid id)
        {
            try
            {
                db.tblFeedbacks.DeleteObject(db.tblFeedbacks.FirstOrDefault(x => x.ID == id));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblFeedbackTopic> GetTopicList()
        {
            try
            {
                return db.tblFeedbackTopics.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int TotalNumberOfFeedbacks()
        {
            return db.tblFeedbacks.Count();
        }

        //Send Mail
        public bool SendFeebackMail(string Subject, string Body, string FrmAddress, string ToAddress, Guid siteId)
        {
            try
            {
                string fromAddress = string.Empty, toAddress = string.Empty, sSubject = string.Empty, sBody = string.Empty;
                fromAddress = FrmAddress;

                var result = GetEmailSettingDetail(siteId);
                if (result != null)
                {
                    var message = new MailMessage();
                    message.From = new MailAddress(fromAddress);
                    message.To.Add(new MailAddress(result.Email));
                    sSubject = Subject;
                    sBody = Body;
                    message.Subject = sSubject;
                    message.Body = sBody;
                    message.IsBodyHtml = true;

                    var smtpClient = new SmtpClient();
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    smtpClient.Send(message);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }

        public class FeedbackInfo : tblFeedback
        {
            public string TopicName { get; set; }
        }
        #endregion

        #region Booking Fee
        public List<tblBookingFeeRule> GetBookingFeeList(Guid siteID)
        {
            try
            {
                var list = (from book in db.tblBookingFeeRules
                            join bookinglkp in db.tblBookingFeeSiteLookUps
                                on book.ID equals bookinglkp.BookingFeeID
                            where bookinglkp.SiteID == siteID
                            select book).OrderBy(x => x.StartAmount).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetMaxBookingFee(Guid siteID, bool HasAmount)
        {
            try
            {
                var result = (from book in db.tblBookingFeeRules
                              join bookinglkp in db.tblBookingFeeSiteLookUps
                                  on book.ID equals bookinglkp.BookingFeeID
                              where bookinglkp.SiteID == siteID && book.HasAmount == HasAmount
                              select book).ToList();
                if (result.Any())
                {
                    if (HasAmount)
                        return result.Max(x => x.EndAmount);
                    else
                        return result.Max(x => x.EndDate);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BookingFeeRule GetBookingFeeEdit(Guid ID)
        {
            try
            {
                var result = (from book in db.tblBookingFeeRules
                              join bookinglkp in db.tblBookingFeeSiteLookUps
                                  on book.ID equals bookinglkp.BookingFeeID
                              where book.ID == ID
                              select new { book, bookinglkp }).ToList();

                var list = result.Select(x => new BookingFeeRule
                {
                    ID = x.book.ID,
                    StartAmount = x.book.StartAmount,
                    EndAmount = x.book.EndAmount,
                    BookingFee = x.book.BookingFee,
                    StartDate = x.book.StartDate,
                    EndDate = x.book.EndDate,
                    HasAmount = x.book.HasAmount,
                    SiteId = Guid.Parse(x.bookinglkp.SiteID.ToString())
                }).ToList().ToList();

                return list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddBookingFee(tblBookingFeeRule book)
        {
            try
            {
                db.AddTotblBookingFeeRules(book);
                db.SaveChanges();
                return book.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddBookingFeeLookup(tblBookingFeeSiteLookUp bookingFeeSiteLookUp)
        {

            try
            {
                db.AddTotblBookingFeeSiteLookUps(bookingFeeSiteLookUp);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateBookingFee(tblBookingFeeRule book)
        {
            try
            {
                var _book = db.tblBookingFeeRules.FirstOrDefault(x => x.ID == book.ID);
                if (_book != null)
                {
                    _book.HasAmount = book.HasAmount;
                    _book.StartDate = book.StartDate;
                    _book.EndDate = book.EndDate;
                    _book.StartAmount = book.StartAmount;
                    _book.EndAmount = book.EndAmount;
                    _book.BookingFee = book.BookingFee;
                    _book.ModifyBy = book.ModifyBy;
                    _book.ModifyOn = book.ModifyOn;
                    _book.CurrencyID = book.CurrencyID;
                    db.SaveChanges();
                };
                return _book.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBookingFee(Guid id)
        {
            try
            {
                var result = db.tblBookingFeeSiteLookUps.FirstOrDefault(x => x.BookingFeeID == id);
                db.tblBookingFeeSiteLookUps.DeleteObject(result);

                var rec = db.tblBookingFeeRules.FirstOrDefault(x => x.ID == id);
                db.tblBookingFeeRules.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class BookingFeeRule : tblBookingFeeRule
        {
            public Guid SiteId { get; set; }
        }
        #endregion

        #region Qubit Script Management
        public Guid AddEditQubitScript(tblQubitScript qubit)
        {
            try
            {

                if (qubit.ID == new Guid())
                {
                    qubit.ID = Guid.NewGuid();
                    if (!db.tblQubitScripts.Any(x => x.SiteID == qubit.SiteID))
                        db.tblQubitScripts.AddObject(qubit);
                    else
                        throw new Exception("Qubit sacript is already exist for selected site.");
                }
                else
                {
                    var rec = db.tblQubitScripts.FirstOrDefault(x => x.ID == qubit.ID);
                    rec.IsActive = qubit.IsActive;
                    rec.ModifiedBy = qubit.CreatedBy;
                    rec.ModifiedOn = qubit.CreatedOn;
                    rec.Script = qubit.Script;
                    rec.SiteID = qubit.SiteID;
                }
                db.SaveChanges();
                return qubit.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ActiveInactiveQubitScript(Guid id)
        {
            try
            {
                var rec = db.tblQubitScripts.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteQubitScript(Guid id)
        {
            try
            {
                db.tblQubitScripts.DeleteObject(db.tblQubitScripts.FirstOrDefault(x => x.ID == id));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblQubitScript GetQubitScriptById(Guid id)
        {
            try
            {
                return db.tblQubitScripts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblQubitScript GetQubitScriptBySiteId(Guid siteId)
        {
            try
            {
                return db.tblQubitScripts.FirstOrDefault(x => x.SiteID == siteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<QubitFields> GetQubitScriptAllList()
        {

            return db.tblQubitScripts.Select(x => new QubitFields
            {
                ID = x.ID,
                IsActive = x.IsActive,
                Script = x.Script,
                SiteName = x.tblSite.DisplayName,
                SiteURL = x.tblSite.SiteURL
            }).ToList();
        }

        public List<QubitFields> GetQubitScriptList(Guid siteId)
        {
            try
            {

                return db.tblQubitScripts.Where(y => y.SiteID == siteId).Select(x => new QubitFields
                    {
                        ID = x.ID,
                        IsActive = x.IsActive,
                        Script = x.Script,
                        SiteName = x.tblSite.DisplayName,
                        SiteURL = x.tblSite.SiteURL
                    }).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetQubitScriptBySId(Guid siteId)
        {
            try
            {
                var rec = db.tblQubitScripts.FirstOrDefault(y => y.SiteID == siteId);

                return rec == null ? "<script></script>" : rec.Script;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region OgoneSetting
        public List<tblOgoneMst> GetOgoneList(Guid siteID)
        {
            try
            {
                var list = (from ogone in db.tblOgoneMsts
                            where ogone.SiteID == siteID
                            select ogone).OrderBy(x => x.CreatedOn).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddOgoneSetting(tblOgoneMst ogone)
        {
            try
            {
                var _ogone = new tblOgoneMst
                {
                    ID = ogone.ID,
                    SiteID = ogone.SiteID,
                    PSPID = ogone.PSPID,
                    OgoneID = ogone.OgoneID,
                    UserName = ogone.UserName,
                    Password = ogone.Password,
                    ReturnURLAccept = ogone.ReturnURLAccept,
                    OgoneURL = ogone.OgoneURL,
                    ReturnURLDecline = ogone.ReturnURLDecline,
                    ReturnURLException = ogone.ReturnURLException,
                    TokenizationURL = ogone.TokenizationURL,
                    OgoneShaInPassPhrase = ogone.OgoneShaInPassPhrase,
                    OgoneShaInPassPhraseTokenization = ogone.OgoneShaInPassPhraseTokenization,
                    Language = ogone.Language,
                    Currency = ogone.Currency,
                    CreatedBy = ogone.CreatedBy,
                    CreatedOn = System.DateTime.Now,
                    IsActive = ogone.IsActive,
                    IsEnableThreeD = ogone.IsEnableThreeD
                };
                db.AddTotblOgoneMsts(_ogone);
                db.SaveChanges();
                return _ogone.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblOgoneMst GetOgoneEdit(Guid ID)
        {
            try
            {
                return db.tblOgoneMsts.FirstOrDefault(x => x.ID == ID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateOgone(tblOgoneMst ogone)
        {
            try
            {
                var _ogone = db.tblOgoneMsts.FirstOrDefault(x => x.ID == ogone.ID);
                if (_ogone != null)
                {
                    _ogone.ID = ogone.ID;
                    _ogone.PSPID = ogone.PSPID;
                    _ogone.OgoneID = ogone.OgoneID;
                    _ogone.UserName = ogone.UserName;
                    _ogone.Password = ogone.Password;
                    _ogone.ReturnURLAccept = ogone.ReturnURLAccept;
                    _ogone.OgoneURL = ogone.OgoneURL;
                    _ogone.ReturnURLDecline = ogone.ReturnURLDecline;
                    _ogone.ReturnURLException = ogone.ReturnURLException;
                    _ogone.OgoneShaInPassPhrase = ogone.OgoneShaInPassPhrase;
                    _ogone.Language = ogone.Language;
                    _ogone.Currency = ogone.Currency;
                    _ogone.ModifiedBy = ogone.ModifiedBy;
                    _ogone.ModifiedOn = ogone.ModifiedOn;
                    _ogone.IsActive = ogone.IsActive;
                    _ogone.IsEnableThreeD = ogone.IsEnableThreeD;
                    _ogone.TokenizationURL = ogone.TokenizationURL;
                    _ogone.OgoneShaInPassPhraseTokenization = ogone.OgoneShaInPassPhraseTokenization;
                };

                db.SaveChanges();
                return _ogone.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveOgone(Guid id)
        {
            try
            {
                var objogone = db.tblOgoneMsts.FirstOrDefault(x => x.ID == id);
                if (objogone != null)
                    objogone.IsActive = !objogone.IsActive;
                db.SaveChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteOgone(Guid id)
        {
            try
            {
                var rec = db.tblOgoneMsts.FirstOrDefault(x => x.ID == id);
                db.tblOgoneMsts.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region >  P2PDeliveryCharges <
        public List<ClsP2PDeliveryCharges> GetP2PDeliveryChargesList(Guid siteID)
        {
            try
            {
                var Result = (from objdelivery in db.tblP2PDeliveryChargesMst
                              join Curr in db.tblCurrencyMsts
                             on objdelivery.CurrencyId equals Curr.ID
                              where objdelivery.SiteID == siteID
                              select new ClsP2PDeliveryCharges
                              {
                                  ID = objdelivery.ID,
                                  Name = objdelivery.Name,
                                  Amount = objdelivery.Amount,
                                  Currecny = Curr.ShortCode,
                                  IsActive = objdelivery.IsActive
                              }).ToList();

                return Result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddP2PDeliveryCharges(tblP2PDeliveryChargesMst objChargesMst)
        {
            try
            {
                var info = db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.ID == objChargesMst.ID);
                if (info != null)
                {
                    info.Name = objChargesMst.Name;
                    info.Amount = objChargesMst.Amount;
                    info.SiteID = objChargesMst.SiteID;
                    info.CurrencyId = objChargesMst.CurrencyId;
                    info.ModifiedOn = objChargesMst.CreatedOn;
                    info.ModifiedBy = objChargesMst.CreatedBy;
                    info.IsActive = objChargesMst.IsActive;
                }
                else
                {
                    var _info = db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.SiteID == objChargesMst.SiteID);
                    if (_info == null)
                    {
                        db.AddTotblP2PDeliveryChargesMst(objChargesMst);
                    }
                    else
                    {
                        return false;
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblP2PDeliveryChargesMst GetP2PDeliveryChargesEdit(Guid ID)
        {
            try
            {
                return db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.ID == ID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateP2PDeliveryCharges(tblP2PDeliveryChargesMst _tblP2PDeliveryChargesMst)
        {
            try
            {
                var _oP2PDeliveryCharges = db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.ID == _tblP2PDeliveryChargesMst.ID);
                if (_oP2PDeliveryCharges != null)
                {
                    _oP2PDeliveryCharges.ID = _tblP2PDeliveryChargesMst.ID;
                    _oP2PDeliveryCharges.Name = _tblP2PDeliveryChargesMst.Name;
                    _oP2PDeliveryCharges.Amount = _tblP2PDeliveryChargesMst.Amount;
                    _oP2PDeliveryCharges.SiteID = _tblP2PDeliveryChargesMst.SiteID;
                    _oP2PDeliveryCharges.CurrencyId = _tblP2PDeliveryChargesMst.CurrencyId;
                    _oP2PDeliveryCharges.ModifiedBy = _tblP2PDeliveryChargesMst.ModifiedBy;
                    _oP2PDeliveryCharges.ModifiedOn = _tblP2PDeliveryChargesMst.ModifiedOn;
                    _oP2PDeliveryCharges.IsActive = _tblP2PDeliveryChargesMst.IsActive;
                };

                db.SaveChanges();
                return _oP2PDeliveryCharges.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveP2PDeliveryCharges(Guid id)
        {
            try
            {
                var objdelivery = db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.ID == id);
                if (objdelivery != null)
                    objdelivery.IsActive = !objdelivery.IsActive;
                db.SaveChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteP2PDeliveryCharges(Guid id)
        {
            try
            {
                var rec = db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.ID == id);
                db.tblP2PDeliveryChargesMst.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ClsP2PDeliveryCharges : tblP2PDeliveryChargesMst
        {
            public string Currecny { get; set; }
        }

        #endregion

        #region Email Settings

        public tblEmailSetting GetEmailSettingDetail(Guid siteID)
        {
            try
            {
                var result = from es in db.tblEmailSettings
                             join esLook in db.tblEmailSettingLookups
                                 on es.ID equals esLook.SettingID
                             where esLook.SiteID == siteID
                             && es.IsActive == true
                             select es;
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmailSettingList> GetEmailSetting()
        {
            try
            {
                var res = db.tblEmailSettings.Select(x => new EmailSettingList
                {
                    ID = x.ID,
                    SmtpHost = x.SmtpHost,
                    SmtpUser = x.SmtpUser,
                    SmtpPort = x.SmtpPort,
                    IsActive = x.IsActive,
                    EnableSsl = x.EnableSsl
                }).AsEnumerable().Select(x => new EmailSettingList
                {
                    ID = x.ID,
                    SiteName = GetEmailSettingSiteName(x.ID),
                    SmtpHost = x.SmtpHost,
                    SmtpUser = x.SmtpUser,
                    SmtpPort = x.SmtpPort,
                    IsActive = x.IsActive,
                    EnableSsl = x.EnableSsl
                }).ToList();

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblEmailSetting GetEmailSettingListEdit(Guid ID)
        {
            try
            {
                return db.tblEmailSettings.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditEmailSetting(tblEmailSetting setting)
        {
            try
            {
                if (setting.ID == new Guid())
                {
                    setting.ID = Guid.NewGuid();
                    db.AddTotblEmailSettings(setting);
                }
                else
                {
                    var rec = db.tblEmailSettings.FirstOrDefault(x => x.ID == setting.ID);
                    if (rec != null)
                    {
                        rec.SmtpHost = setting.SmtpHost;
                        rec.SmtpUser = setting.SmtpUser;
                        rec.SmtpPass = setting.SmtpPass;
                        rec.SmtpPort = setting.SmtpPort;
                        rec.Email = setting.Email;
                        rec.IsActive = setting.IsActive;
                        rec.EnableSsl = setting.EnableSsl;
                    }
                }
                db.SaveChanges();
                return setting.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEmailSettingLookupSites(List<tblEmailSettingLookup> oLookup)
        {
            try
            {
                var id = oLookup.FirstOrDefault().SettingID;
                db.tblEmailSettingLookups.Where(x => x.SettingID == id).ToList().ForEach(db.tblEmailSettingLookups.DeleteObject);
                foreach (var item in oLookup)
                {
                    item.ID = Guid.NewGuid();
                    db.tblEmailSettingLookups.AddObject(item);
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveEmailSettings(Guid id)
        {
            try
            {
                var rec = db.tblEmailSettings.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteEmailSettings(Guid id)
        {
            try
            {
                db.tblEmailSettingLookups.Where(w => w.SettingID == id).ToList().ForEach(db.tblEmailSettingLookups.DeleteObject);
                db.SaveChanges();

                var rec = db.tblEmailSettings.FirstOrDefault(x => x.ID == id);
                db.tblEmailSettings.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEmailSettingSiteName(Guid id)
        {
            var res = (from tl in db.tblEmailSettingLookups
                       join ts in db.tblSites on tl.SiteID equals ts.ID
                       where tl.SettingID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";
            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }

        public class EmailSettingList : tblEmailSetting
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region Mandatory Fields
        public List<MandatoryField> GetMandatoryFieldList(Guid siteID)
        {
            try
            {
                var olist = (from mf in db.tblMandatoryFields
                             join menu in db.tblWebMenus
                                 on mf.PageID equals menu.ID
                             join mfLook in db.tblMandatorySiteLookups
                                 on mf.ID equals mfLook.MandatoryID
                             where mfLook.SiteID == siteID
                             select new { mf, menu }).ToList();

                var list = olist.Select(x => new MandatoryField
                    {
                        ID = x.mf.ID,
                        PageName = x.menu.Name,
                        SiteName = GetMndSiteName(x.mf.ID),
                        ControlName = GetControlName(x.mf.ID),
                        IsMandatory = x.mf.IsMandatory
                    }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetMndSiteName(Guid id)
        {
            var res = (from tm in db.tblMandatorySiteLookups
                       join ts in db.tblSites on tm.SiteID equals ts.ID
                       where tm.MandatoryID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";

            }
            return siteName;
        }
        public bool ChkPageExists(Guid pageID, Guid siteID)
        {
            try
            {
                var chk = (from mf in db.tblMandatoryFields
                           join mflook in db.tblMandatorySiteLookups
                               on mf.ID equals mflook.MandatoryID
                           where mf.PageID == pageID
                                 && mflook.SiteID == siteID
                           select new { mf }).Any();
                //var chkPage = db.tblMandatoryFields.Any(ty => ty.PageID == pageID);
                if (chk)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<tblPageControl> GetPageControlList(Guid PageID)
        {
            try
            {
                return db.tblPageControls.Where(x => x.PageID == PageID).OrderBy(x => x.ControlName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddMandatoryFields(MandatoryField field)
        {
            try
            {
                var id = field.ID == Guid.Empty ? Guid.NewGuid() : field.ID;
                if (field.ID == Guid.Empty)
                {
                    db.tblMandatoryFields.AddObject(new tblMandatoryField
                    {
                        ID = id,
                        PageID = field.PageID,
                        IsMandatory = field.IsMandatory
                    });
                }
                else
                {
                    var rec = db.tblMandatoryFields.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.PageID = field.PageID;
                        rec.IsMandatory = field.IsMandatory;
                    }
                }

                db.tblMandatoryControls.Where(x => x.MandatoryID == id).ToList().ForEach(db.tblMandatoryControls.DeleteObject);
                foreach (var item in field.ListControlId)
                {
                    db.tblMandatoryControls.AddObject(new tblMandatoryControl
                    {
                        ID = Guid.NewGuid(),
                        MandatoryID = id,
                        ControlID = item
                    });
                }

                db.tblMandatorySiteLookups.Where(x => x.MandatoryID == id).ToList().ForEach(db.tblMandatorySiteLookups.DeleteObject);
                foreach (var item in field.ListSiteId)
                {
                    db.tblMandatorySiteLookups.AddObject(new tblMandatorySiteLookup
                    {
                        ID = Guid.NewGuid(),
                        MandatoryID = id,
                        SiteID = item
                    });
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteMandatoryFields(Guid id)
        {
            try
            {
                db.tblMandatorySiteLookups.Where(x => x.MandatoryID == id).ToList().ForEach(db.tblMandatorySiteLookups.DeleteObject);
                db.tblMandatoryControls.Where(x => x.MandatoryID == id).ToList().ForEach(db.tblMandatoryControls.DeleteObject);
                db.tblMandatoryFields.Where(x => x.ID == id).ToList().ForEach(db.tblMandatoryFields.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblWebMenu> GetPageNameList()
        {
            try
            {
                return db.tblWebMenus.Where(x => x.IsActive == true).OrderBy(x => x.PageName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MandatoryField GetMandatoryFieldsById(Guid id)
        {
            try
            {
                var rec = db.tblMandatoryFields.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new MandatoryField
                    {
                        IsMandatory = rec.IsMandatory,
                        PageID = rec.PageID,
                        ListSiteId = rec.tblMandatorySiteLookups.Where(x => x.MandatoryID == id).Select(x => x.SiteID).ToList(),
                        ListControlId = rec.tblMandatoryControls.Where(x => x.MandatoryID == id).Select(x => x.ControlID).ToList(),
                    };
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MandatoryField GetControlsByPageId(Guid pageid)
        {
            try
            {
                var rec = db.tblMandatoryFields.FirstOrDefault(x => x.PageID == pageid);
                if (rec != null)
                    return new MandatoryField
                    {
                        ListControlId = rec.tblMandatoryControls.Where(x => x.MandatoryID == rec.ID).Select(x => x.ControlID).ToList(),
                    };
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetControlName(Guid id)
        {
            var res = (from mc in db.tblMandatoryControls
                       join pc in db.tblPageControls
                        on mc.ControlID equals pc.ID
                       where mc.MandatoryID == id
                       select new { pc.ControlName }).OrderBy(x => x.ControlName).ToList();
            string cntName = "";
            foreach (var item in res)
            {
                cntName += "- " + item.ControlName + "<br/>";
            }
            return cntName;
        }

        public List<MandatoryClass> GetMandatoryVal(Guid siteID, Guid PageID)
        {
            try
            {
                var olist = (from mf in db.tblMandatoryFields
                             join mfLook in db.tblMandatorySiteLookups
                                 on mf.ID equals mfLook.MandatoryID
                             join mfControl in db.tblMandatoryControls
                                on mf.ID equals mfControl.MandatoryID
                             join pc in db.tblPageControls
                                on mfControl.ControlID equals pc.ID
                             where mfLook.SiteID == siteID
                             && mf.PageID == PageID
                             select new { pc, mf }).ToList();

                var list = olist.Select(x => new MandatoryClass
                {
                    ControlField = x.pc.ControlField,
                    IsMandatory = x.mf.IsMandatory,
                    ValGrp = x.pc.RequiredFieldValGrp
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public class MandatoryClass
        {
            public string ControlField { get; set; }
            public bool IsMandatory { get; set; }
            public string ValGrp { get; set; }
        }
        #endregion

        #region Security
        /*FrontWebsite*/
        public tblSecurity GetSecurityBySiteid(Guid siteID)
        {
            try
            {
                var result = from sc in db.tblSecurities
                             join scLook in db.tblSecuritySiteLookups
                                 on sc.ID equals scLook.SecurityID
                             where scLook.SiteID == siteID
                             && sc.IsActive
                             select sc;
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SecurityField> GetSecurityList(Guid siteID)
        {
            try
            {
                var olist = (from sc in db.tblSecurities
                             join scLook in db.tblSecuritySiteLookups
                                 on sc.ID equals scLook.SecurityID
                             where scLook.SiteID == siteID
                             select new { sc, scLook }).ToList();

                var list = olist.Select(x => new SecurityField
                {
                    ID = x.sc.ID,
                    Title = x.sc.Title,
                    SiteName = GetSecuritySiteName(x.sc.ID),
                    IsActive = x.sc.IsActive
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetSecuritySiteName(Guid id)
        {
            var res = (from tm in db.tblSecuritySiteLookups
                       join ts in db.tblSites on tm.SiteID equals ts.ID
                       where tm.SecurityID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }
        public SecurityField GetSecurityById(Guid id)
        {
            try
            {
                var rec = db.tblSecurities.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new SecurityField
                    {
                        ID = rec.ID,
                        Title = rec.Title,
                        Description = rec.Description,
                        ListSiteId = rec.tblSecuritySiteLookups.Where(x => x.SecurityID == id).Select(x => x.SiteID).ToList(),
                        IsActive = rec.IsActive
                    };
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddEditSecurity(SecurityField field)
        {
            try
            {
                var id = field.ID == Guid.Empty ? Guid.NewGuid() : field.ID;
                if (field.ID == Guid.Empty)
                {
                    db.tblSecurities.AddObject(new tblSecurity
                    {
                        ID = id,
                        Title = field.Title,
                        Description = field.Description,
                        IsActive = field.IsActive
                    });
                }
                else
                {
                    var rec = db.tblSecurities.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.Title = field.Title;
                        rec.Description = field.Description;
                        rec.IsActive = field.IsActive;
                    }
                }

                db.tblSecuritySiteLookups.Where(x => x.SecurityID == id).ToList().ForEach(db.tblSecuritySiteLookups.DeleteObject);
                foreach (var item in field.ListSiteId)
                {
                    db.tblSecuritySiteLookups.AddObject(new tblSecuritySiteLookup
                    {
                        ID = Guid.NewGuid(),
                        SecurityID = id,
                        SiteID = item
                    });
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteSecurity(Guid id)
        {
            try
            {
                db.tblSecuritySiteLookups.Where(x => x.SecurityID == id).ToList().ForEach(db.tblSecuritySiteLookups.DeleteObject);
                db.tblSecurities.Where(x => x.ID == id).ToList().ForEach(db.tblSecurities.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ActiveInactiveSecurity(Guid id)
        {
            try
            {
                var rec = db.tblSecurities.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Terms and conditions
        public tblTermsAndCondition GetTermsAndConditionsBySiteId(Guid siteID)
        {
            try
            {
                var result = from tc in db.tblTermsAndConditions
                             join tcLook in db.tblTermsandConditionSiteLookups
                                 on tc.ID equals tcLook.TermsandConditionID
                             where tcLook.SiteID == siteID
                             && tc.IsActive
                             select tc;
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TermsAndConditionsField> GetTermsAndConditionList(Guid siteID)
        {
            try
            {
                var olist = (from tc in db.tblTermsAndConditions
                             join tcLook in db.tblTermsandConditionSiteLookups
                                 on tc.ID equals tcLook.TermsandConditionID
                             where tcLook.SiteID == siteID
                             select new { tc, tcLook }).ToList();

                var list = olist.Select(x => new TermsAndConditionsField
                {
                    ID = x.tc.ID,
                    Title = x.tc.Title,
                    SiteName = GetTermsSiteName(x.tc.ID),
                    IsActive = x.tc.IsActive
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TermsAndConditionsField GetTermsAndConditionsById(Guid id)
        {
            try
            {
                var rec = db.tblTermsAndConditions.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new TermsAndConditionsField
                    {
                        ID = rec.ID,
                        Title = rec.Title,
                        Description = rec.Description,
                        ListSiteId = rec.tblTermsandConditionSiteLookups.Where(x => x.TermsandConditionID == id).Select(x => x.SiteID).ToList(),
                        IsActive = rec.IsActive
                    };
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditTerms(TermsAndConditionsField field)
        {
            try
            {
                var id = field.ID == Guid.Empty ? Guid.NewGuid() : field.ID;
                if (field.ID == Guid.Empty)
                {
                    db.tblTermsAndConditions.AddObject(new tblTermsAndCondition
                    {
                        ID = id,
                        Title = field.Title,
                        Description = field.Description,
                        IsActive = field.IsActive
                    });
                }
                else
                {
                    var rec = db.tblTermsAndConditions.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.Title = field.Title;
                        rec.Description = field.Description;
                        rec.IsActive = field.IsActive;
                    }
                }

                db.tblTermsandConditionSiteLookups.Where(x => x.TermsandConditionID == id).ToList().ForEach(db.tblTermsandConditionSiteLookups.DeleteObject);
                foreach (var item in field.ListSiteId)
                {
                    db.tblTermsandConditionSiteLookups.AddObject(new tblTermsandConditionSiteLookup
                    {
                        ID = Guid.NewGuid(),
                        TermsandConditionID = id,
                        SiteID = item
                    });
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteTerms(Guid id)
        {
            try
            {
                db.tblTermsandConditionSiteLookups.Where(x => x.TermsandConditionID == id).ToList().ForEach(db.tblTermsandConditionSiteLookups.DeleteObject);
                db.tblTermsAndConditions.Where(x => x.ID == id).ToList().ForEach(db.tblTermsAndConditions.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveTerms(Guid id)
        {
            try
            {
                var rec = db.tblTermsAndConditions.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetTermsSiteName(Guid id)
        {
            var res = (from tm in db.tblTermsandConditionSiteLookups
                       join ts in db.tblSites on tm.SiteID equals ts.ID
                       where tm.TermsandConditionID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";
            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }
        #endregion

        #region FooterMenu
        public List<FooterMenu> GetFooterMenuList(Guid siteID)
        {
            try
            {
                var olist = (from fm in db.tblFooterMenus
                             join fmLook in db.tblFooterMenuSiteLookups
                                 on fm.ID equals fmLook.MenuID
                             where fmLook.SiteID == siteID
                             select new { fm, fmLook }).ToList();

                var list = olist.Select(x => new FooterMenu
                {
                    ID = x.fm.ID,
                    MenuName = x.fm.MenuName,
                    SiteName = GetFooterSiteName(x.fm.ID),
                    IsActive = x.fm.IsActive,
                    SortOrder = x.fm.SortOrder
                }).ToList().OrderBy(x => x.MenuName);
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetFooterSiteName(Guid id)
        {
            var res = (from fmLook in db.tblFooterMenuSiteLookups
                       join ts in db.tblSites on fmLook.SiteID equals ts.ID
                       where fmLook.MenuID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }
        public FooterMenu GetFooterMenuById(Guid id)
        {
            try
            {
                var rec = db.tblFooterMenus.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new FooterMenu
                    {
                        ID = rec.ID,
                        MenuName = rec.MenuName,
                        ListSiteId = rec.tblFooterMenuSiteLookups.Where(x => x.MenuID == id).Select(x => x.SiteID).ToList(),
                        IsActive = rec.IsActive
                    };
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddEditFooterMenu(FooterMenu field)
        {
            try
            {
                var id = field.ID == Guid.Empty ? Guid.NewGuid() : field.ID;
                if (field.ID == Guid.Empty)
                {
                    db.tblFooterMenus.AddObject(new tblFooterMenu
                    {
                        ID = id,
                        MenuName = field.MenuName,
                        IsActive = field.IsActive,
                        SortOrder = field.SortOrder
                    });
                }
                else
                {
                    var rec = db.tblFooterMenus.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.MenuName = field.MenuName;
                        rec.IsActive = field.IsActive;
                        rec.SortOrder = field.SortOrder;
                    }
                }

                db.tblFooterMenuSiteLookups.Where(x => x.MenuID == id).ToList().ForEach(db.tblFooterMenuSiteLookups.DeleteObject);
                foreach (var item in field.ListSiteId)
                {
                    db.tblFooterMenuSiteLookups.AddObject(new tblFooterMenuSiteLookup
                    {
                        ID = Guid.NewGuid(),
                        MenuID = id,
                        SiteID = item
                    });
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteFooterMenu(Guid id)
        {
            try
            {
                db.tblFooterItems.Where(x => x.MenuID == id).ToList().ForEach(db.tblFooterItems.DeleteObject);
                db.tblFooterMenuSiteLookups.Where(x => x.MenuID == id).ToList().ForEach(db.tblFooterMenuSiteLookups.DeleteObject);
                db.tblFooterMenus.Where(x => x.ID == id).ToList().ForEach(db.tblFooterMenus.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ActiveInactiveFooterMenu(Guid id)
        {
            try
            {
                var rec = db.tblFooterMenus.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FooterItems
        public List<tblFooterMenu> GetActiveFooterMenuList()
        {
            try
            {
                return db.tblFooterMenus.Where(x => x.IsActive).OrderBy(x => x.MenuName).OrderBy(x => x.MenuName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FooterItems> GetFooterItemList()
        {
            try
            {
                var olist = (from fm in db.tblFooterItems
                             join fmMenu in db.tblFooterMenus
                                 on fm.MenuID equals fmMenu.ID
                             select new { fm, fmMenu }).ToList();

                var list = olist.Select(x => new FooterItems
                {
                    ID = x.fm.ID,
                    MenuName = x.fmMenu.MenuName,
                    Name = x.fm.Name,
                    IsActive = x.fm.IsActive
                }).ToList().OrderBy(x => x.MenuName);
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblFooterItem GetFooterItemsById(Guid id)
        {
            try
            {
                var rec = db.tblFooterItems.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new tblFooterItem
                    {
                        ID = rec.ID,
                        MenuID = rec.MenuID,
                        Name = rec.Name,
                        NavUrl = rec.NavUrl,
                        IsActive = rec.IsActive
                    };
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddEditFooterItems(tblFooterItem field)
        {
            try
            {
                var id = field.ID == Guid.Empty ? Guid.NewGuid() : field.ID;
                if (field.ID == Guid.Empty)
                {
                    db.tblFooterItems.AddObject(new tblFooterItem
                    {
                        ID = id,
                        MenuID = field.MenuID,
                        Name = field.Name,
                        NavUrl = field.NavUrl,
                        IsActive = field.IsActive
                    });
                }
                else
                {
                    var rec = db.tblFooterItems.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.MenuID = field.MenuID;
                        rec.Name = field.Name;
                        rec.NavUrl = field.NavUrl;
                        rec.IsActive = field.IsActive;
                    }
                }

                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteFooterItems(Guid id)
        {
            try
            {
                db.tblFooterItems.Where(x => x.ID == id).ToList().ForEach(db.tblFooterItems.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ActiveInactiveFooterItems(Guid id)
        {
            try
            {
                var rec = db.tblFooterItems.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Eurail Promotion
        public tblEurailPromotion GetPromotionByID(Guid id)
        {
            try
            {
                return db.tblEurailPromotions.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PromotionList> GetEurailPromotion(Guid siteID)
        {
            try
            {
                var res = db.tblEurailPromotions.Where(x => x.tblEurailPromotionSiteLookups.Any(y => y.SiteID == siteID)).AsEnumerable().Select(x => new PromotionList
                {
                    SiteName = GetPromotionSiteName(x.ID),
                    ID = x.ID,
                    Title = x.Title,
                    IsActive = x.IsActive,
                    Description = x.Description,
                    NavUrl = x.NavUrl,
                    ImageUrl = x.ImageUrl,
                    sortorder = GetPromotionSortingorderByIdandSiteId(siteID, x.ID)
                }).ToList();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPromotionSortingorderByIdandSiteId(Guid siteID, Guid ID)
        {
            try
            {
                var data = db.tblEurailPromotionSortOrderLookups.FirstOrDefault(t => t.SiteId == siteID && t.EurailPromotionId == ID);
                if (data != null)
                    return Convert.ToString(data.SortOrder);
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updatePromotionSortingorderByIdandSiteId(Guid siteID, Guid ID, int SortOrder)
        {
            try
            {
                var exists = db.tblEurailPromotionSortOrderLookups.Any(t => t.SiteId == siteID && t.SortOrder == SortOrder && t.EurailPromotionId != ID);
                if (exists)
                    throw new Exception("Short order already in use.");
                var data = db.tblEurailPromotionSortOrderLookups.FirstOrDefault(t => t.SiteId == siteID && t.EurailPromotionId == ID);
                if (data != null)
                    data.SortOrder = SortOrder;
                else
                {
                    var obj = new tblEurailPromotionSortOrderLookup()
                    {
                        ID = Guid.NewGuid(),
                        SiteId = siteID,
                        EurailPromotionId = ID,
                        SortOrder = SortOrder
                    };
                    db.AddTotblEurailPromotionSortOrderLookups(obj);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPromotionSiteName(Guid id)
        {
            var res = (from tdosl in db.tblEurailPromotionSiteLookups
                       join ts in db.tblSites on tdosl.SiteID equals ts.ID
                       where tdosl.PromotionID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";
            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }

        public class PromotionList : tblEurailPromotion
        {
            public string SiteName { get; set; }
            public string sortorder { get; set; }
        }

        public Guid UpdatePromotion(tblEurailPromotion promotion)
        {
            try
            {
                var _promotion = db.tblEurailPromotions.FirstOrDefault(x => x.ID == promotion.ID);
                if (_promotion != null)
                {
                    _promotion.Title = promotion.Title;
                    _promotion.NavUrl = promotion.NavUrl;
                    _promotion.Description = promotion.Description;
                    _promotion.IsActive = promotion.IsActive;
                    if (!string.IsNullOrEmpty(promotion.ImageUrl))
                        _promotion.ImageUrl = promotion.ImageUrl;
                }
                db.SaveChanges();
                return _promotion.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddPromotion(tblEurailPromotion promotion)
        {
            try
            {
                var _promotion = new tblEurailPromotion
                {
                    ID = promotion.ID,
                    Title = promotion.Title,
                    NavUrl = promotion.NavUrl,
                    Description = promotion.Description,
                    ImageUrl = promotion.ImageUrl,
                    IsActive = promotion.IsActive,
                };
                db.AddTotblEurailPromotions(_promotion);
                db.SaveChanges();
                return _promotion.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddPromotionLookupSites(tblEurailPromotionSiteLookup promotionSiteLookUp)
        {
            try
            {
                db.AddTotblEurailPromotionSiteLookups(promotionSiteLookUp);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactivePromotion(Guid id)
        {
            try
            {
                var rec = db.tblEurailPromotions.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeletePromotion(Guid id)
        {
            try
            {
                db.tblEurailPromotionSiteLookups.Where(w => w.PromotionID == id).ToList().ForEach(db.tblEurailPromotionSiteLookups.DeleteObject);
                db.SaveChanges();
                db.tblEurailPromotionSortOrderLookups.Where(t => t.EurailPromotionId == id).ToList().ForEach(db.tblEurailPromotionSortOrderLookups.DeleteObject);
                db.SaveChanges();
                var rec = db.tblEurailPromotions.FirstOrDefault(x => x.ID == id);
                db.tblEurailPromotions.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblEurailPromotion GetPromotionListEdit(Guid ID)
        {
            try
            {
                return db.tblEurailPromotions.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UrlMaping
        public int AddUpdateUrlMaping(clsUrlMaping UM)
        {
            try
            {
                var oUM = new tblUrlMaping();
                if (UM.ID != 0)
                    oUM = db.tblUrlMapings.SingleOrDefault(t => t.ID == UM.ID);
                oUM.SiteId = UM.SiteID;
                oUM.OldURL = UM.OldURL;
                oUM.NewURL = UM.NewURL;
                oUM.DefaultURL = UM.DefaultURL;
                oUM.CreatedBy = UM.CreatedBy;
                oUM.CreatedOn = UM.CreatedOn;
                oUM.ModifyBy = UM.ModifyBy;
                oUM.ModifyOn = UM.ModifyOn;
                oUM.Note = UM.Note;
                oUM.IsActive = UM.IsActive.Value;

                if (UM.ID == 0)
                    db.AddTotblUrlMapings(oUM);
                return db.SaveChanges();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public List<clsUrlMaping> GetUrlMaping()
        {
            try
            {
                return (from UM in db.tblUrlMapings
                        join S in db.tblSites on UM.SiteId equals S.ID
                        orderby UM.CreatedOn ascending
                        select new clsUrlMaping
                        {
                            ID = UM.ID,
                            SiteID = UM.SiteId,
                            SiteName = S.DisplayName,
                            OldURL = UM.OldURL,
                            NewURL = UM.NewURL,
                            DefaultURL = UM.DefaultURL,
                            IsActive = UM.IsActive,
                            CreatedBy = UM.CreatedBy,
                            CreatedOn = UM.CreatedOn,
                            ModifyBy = UM.ModifyBy,
                            ModifyOn = UM.ModifyOn,
                            Note = UM.Note
                        }).ToList();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public tblUrlMaping GetUrlMapingTable(int Id)
        {
            try
            {
                return db.tblUrlMapings.SingleOrDefault(t => t.ID == Id);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void DeleteURL(int ID)
        {
            try
            {
                db.tblUrlMapings.Where(t => t.ID == ID).ToList().ForEach(db.DeleteObject);
                db.SaveChanges();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool ChangeStatus(int ID)
        {
            try
            {
                tblUrlMaping UM = db.tblUrlMapings.FirstOrDefault(t => t.ID == ID);
                if (UM.IsActive.Value)
                    UM.IsActive = false;
                else
                    UM.IsActive = true;
                db.SaveChanges();
                return UM.IsActive.Value;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        public StationNameList GetLongitudeLatitude(string StationCode)
        {
            try
            {
                return db.StationNameLists.FirstOrDefault(x => x.StationCode.Contains(StationCode));
            }
            catch (Exception ex) { throw ex; }
        }

        public List<EmailInfo> GetAgentOfficeEmailID(Guid agentID)
        {
            var list = (from admin in db.tblAdminUsers
                        join branch in db.tblBranches
                            on admin.BranchID equals branch.ID
                        where admin.ID == agentID
                        select new { admin, branch });

            return list.Select(x => new EmailInfo
                {
                    ID = x.admin.ID,
                    Email = x.branch.Email,
                    SecondaryEmail = x.branch.SecondaryEmail,
                    chkEmail = (bool)x.branch.chkEmail != null ? (bool)x.branch.chkEmail : false,
                    chkSecondaryEmail = (bool)x.branch.chkSecondaryEmail != null ? (bool)x.branch.chkSecondaryEmail : false
                }).ToList();
        }

        public List<ManagePurchageProduct> GetSiteListBySiteIds(List<Guid> SiteIds, Guid ProductID)
        {
            try
            {
                var data = (from a in db.tblProductSiteLookUps
                            join b in db.tblSites on a.SiteID equals b.ID
                            where a.ProductID == ProductID && SiteIds.Contains(a.SiteID) && b.IsActive == true
                            select new { a, b }).ToList().OrderBy(x => x.b.DisplayName).ToList();
                if (data != null && data.Count > 0)
                {
                    return data.ToList().Select(x => new ManagePurchageProduct
                    {
                        Id = x.a.ID,
                        SiteId = x.b.ID,
                        SiteName = x.b.DisplayName,
                        ProductEnableFromDate = x.a.ProductEnableFromDate,
                        ProductEnableToDate = x.a.ProductEnableToDate,
                        IsProductEnableFromDate = x.a.IsProductEnableFromDate,
                        IsProductEnableToDate = x.a.IsProductEnableToDate,
                        GmtTimeZone = x.b.GmtTimeZone
                    }).ToList();
                }
                return new List<ManagePurchageProduct>();
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddUpdateProductValid(tblProductSiteLookUp lookUpList)
        {
            try
            {
                if (lookUpList != null)
                {
                    var data = db.tblProductSiteLookUps.FirstOrDefault(x => x.SiteID == lookUpList.SiteID && x.ProductID == lookUpList.ProductID);
                    if (data != null)
                    {
                        data.ProductEnableFromDate = lookUpList.ProductEnableFromDate;
                        data.ProductEnableToDate = lookUpList.ProductEnableToDate;
                        data.IsProductEnableFromDate = lookUpList.IsProductEnableFromDate;
                        data.IsProductEnableToDate = lookUpList.IsProductEnableToDate;
                        db.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<StockDetailReportByStockNo_Result> GetStockDetailReportByStockNo(int StockNo, int PrintQueueType)
        {
            try
            {
                return db.StockDetailReportByStockNo(StockNo, PrintQueueType).ToList();
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public class clsUrlMaping
    {
        public Guid? SiteID { get; set; }
        public string SiteName { get; set; }
        public int ID { get; set; }
        public string OldURL { get; set; }
        public string NewURL { get; set; }
        public string DefaultURL { get; set; }
        public bool? IsActive { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifyBy { get; set; }
        public DateTime? ModifyOn { get; set; }
        public string Note { get; set; }
    }


    #region Class properties
    public class Currency : tblCurrencyMst
    {
    }

    public class CurrencyConv : tblCurrencyConversion
    {
    }

    public class Country : tblCountriesMst
    {
    }

    public class Languages : tblLanguagesMst
    {
    }

    public class Navigation : tblNavigation
    {
        public Guid SiteId { get; set; }
    }

    public class Branch : tblBranch
    {
        public string PathName { get; set; }
    }
    public class UserSiteDeletion : tblAdminUser
    {
        public string SiteName { get; set; }
    }
    //public class Sites : tblSite
    //{
    //    public string Theme { get; set; }
    //    public string CurrencyName { get; set; }
    //    public string LanguageName { get; set; }
    //    public string CountryName { get; set; }
    //}
    public class UserSites : tblSite
    {
        public string Theme { get; set; }
        public string CurrencyName { get; set; }
        public string LanguageName { get; set; }
        public string CountryName { get; set; }
        public Guid? AdminUserID { get; set; }
    }
    public class SubscribedUsers : tblNewsLetterUser
    {
        public string SiteName { get; set; }
        public string AreaName { get; set; }
    }

    public class Ap_RoleMenuDetail : ap_RoleMenuDetail
    {
    }

    public class CountriesInfo : tblCountriesInfo
    {
    }



    public class AdminUserLookSites : tblAdminUserLookupSite
    {
    }

    public class ValidUpTo
    {
        public Guid ID { get; set; }
        public int EurailCode { get; set; }
        public string Name { get; set; }
        public string PromoPassText { get; set; }
        public bool IsActive { get; set; }
        public bool IsFlexi { get; set; }
        public string Note { get; set; }
    }


    public class ImageList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string CategoryName { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
    #endregion

    public class FooterItems
    {
        public Guid ID { get; set; }
        public string MenuName { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
    public class FooterMenu : tblFooterMenu
    {
        public string SiteName { get; set; }
        public List<Guid> ListSiteId { get; set; }
        public bool IsActive { get; set; }
    }
    public class EmailInfo
    {
        public Guid ID { get; set; }
        public string Email { get; set; }
        public string SecondaryEmail { get; set; }
        public bool chkEmail { get; set; }
        public bool chkSecondaryEmail { get; set; }
    }
    public class SecurityField
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SiteName { get; set; }
        public List<Guid> ListSiteId { get; set; }
        public bool IsActive { get; set; }
    }
    public class TermsAndConditionsField
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SiteName { get; set; }
        public List<Guid> ListSiteId { get; set; }
        public bool IsActive { get; set; }
    }
    public class MandatoryField
    {
        public Guid ID { get; set; }
        public Guid PageID { get; set; }
        public string PageName { get; set; }
        public string ControlName { get; set; }
        public string SiteName { get; set; }
        public List<Guid> ListSiteId { get; set; }
        public List<Guid> ListControlId { get; set; }
        public bool IsMandatory { get; set; }
    }
    public class FieldsDeliveryOption
    {
        public Guid ID { get; set; }
        public List<Guid> ListSiteId { get; set; }
        public string SiteName { get; set; }
        public List<Guid> ListCountryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal BasePrice { get; set; }
        public bool IsActive { get; set; }
        public bool IsActiveForAdmin { get; set; }
        public Guid CreatedBy { get; set; }
    }

    public class ClsPageCount
    {
        public string PageCount { get; set; }
    }
    public class QubitFields
    {
        public Guid ID { get; set; }
        public string SiteName { get; set; }
        public string SiteURL { get; set; }
        public string Script { get; set; }
        public bool IsActive { get; set; }
    }
    public class P2PReservationIDInfo
    {
        public Guid ID { get; set; }
        public string JourneyType { get; set; }
        public long P2PID { get; set; }
    }
    public class EurailCountries : tblEurailCountry
    {
        public string Level { get; set; }
    }
    public class CrossSaleProduct : tblCrossSaleProductLookup
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public Decimal Price { get; set; }
        public Boolean IsActive { get; set; }
    }
    public class PageLayoutList
    {
        public Guid ID { get; set; }
        public string LayoutName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Guid LayoutId { get; set; }
        public string LayoutPath { get; set; }
        public string ImageUrl { get; set; }
        public string SiteName { get; set; }
        public int SiteType { get; set; }
    }
    public class SiteCurrency
    {
        public Guid CurrencyID { get; set; }
        public string Name { get; set; }
    }
    public class ManagePurchageProduct
    {
        public Guid SiteId { get; set; }
        public Guid Id { get; set; }
        public string SiteName { get; set; }
        public Nullable<DateTime> ProductEnableFromDate { get; set; }
        public Nullable<DateTime> ProductEnableToDate { get; set; }
        public bool IsProductEnableFromDate { get; set; }
        public bool IsProductEnableToDate { get; set; }
        public string GmtTimeZone { get; set; }
    }
    public class Sites : tblSite
    {
        public string Theme { get; set; }
        public string CurrencyName { get; set; }
        public string LanguageName { get; set; }
        public string CountryName { get; set; }
    }
}
