﻿using System;
using System.Text;
using System.Net.Http;
using System.Xml;
using System.Net;
namespace Business
{

    public class WorldPayGateway
    {
        #region Test A/c and Card Detail
        //string baseUrl = "https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp";
        //string MerchantCode = "RAILSG";
        //string Username = "RAILSG";
        //string Password = "Live2015";

        //Test Card Detail:
        //MasterCard: 5555555555554444 and 5454545454545454
        //Visa: 4444333322221111, 4911830000000 and 4917610000000000
        //CVV: 111
        #endregion

        public string ErrorMessage = "";
        /// <summary>
        /// Process world pay request and get URL for payment
        /// </summary>
        /// <param name="request">Request object</param>
        /// <param name="siteUrl">e.g. https://www.internationalrail.com/ </param>
        /// <returns></returns>
        public WorldPayGatewayResponse PaymentProcess(WorldPayGatewayRequest request, string siteUrl, string baseUrl, string MerchantCode, string Username, string Password)
        {
            try
            {
                /*for test account*/
                //baseUrl = "https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp";
                //Username = "G5GMAHOPS3KA76TN9L2K";
                //Password = "Live2015";


                string xmldata = @"<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE paymentService PUBLIC '-//WorldPay//DTD WorldPay PaymentService v1//EN' 'http://dtd.worldpay.com/paymentService_v1.dtd'>";
                xmldata = xmldata + "<paymentService version='1.4' merchantCode='" + MerchantCode + "'><submit>";
                xmldata = xmldata + "<order orderCode='" + request.Order.orderCode + "'>";
                xmldata = xmldata + "<description>" + request.Order.description + "</description>";
                xmldata = xmldata + "<amount value='" + request.Order.amount + "' currencyCode='" + request.Order.CurrencyCode + "' exponent='" + request.Order.exponent + "'/><orderContent></orderContent>";
                xmldata = xmldata + "<paymentMethodMask><include code='" + request.Order.paymentMethodMask.Code + "'/>";
                //[Optional exclude payment type]
                //xmldata = xmldata + "<exclude code='AMEX-SSL'/>";
                xmldata = xmldata + "</paymentMethodMask>";
                xmldata = xmldata + "<shopper><shopperEmailAddress>'" + request.Order.shopper.shopperEmailAddress + "'</shopperEmailAddress></shopper>";
                xmldata = xmldata + "<shippingAddress><address><firstName>" + request.Order.shippingAddress.firstName + "</firstName><lastName>" + request.Order.shippingAddress.lastName + "</lastName><street>" + request.Order.shippingAddress.street + "</street><houseNumber>" + request.Order.shippingAddress.houseNumber + "</houseNumber>";
                xmldata = xmldata + "<postalCode>" + request.Order.shippingAddress.postalCode + "</postalCode><city>" + request.Order.shippingAddress.City + "</city><countryCode>" + request.Order.shippingAddress.countryCode + "</countryCode></address></shippingAddress>";
                xmldata = xmldata + "</order>";
                xmldata = xmldata + "</submit></paymentService>";

                var credential = new NetworkCredential(Username, Password);
                HttpClientHandler handler = new HttpClientHandler { Credentials = credential };
                HttpClient client = new HttpClient(handler);
                client.BaseAddress = new System.Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/xml"));
                System.Net.Http.HttpContent content = new StringContent(xmldata, UTF8Encoding.UTF8, "text/xml");
                HttpResponseMessage messge = client.PostAsync(baseUrl, content).Result;

                if (messge.IsSuccessStatusCode)
                {
                    string result = messge.Content.ReadAsStringAsync().Result;

                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(result);
                    XmlNode node = null;
                    XmlNode error = null;
                    node = xDoc.SelectSingleNode("paymentService/reply/orderStatus");
                    error = xDoc.SelectSingleNode("paymentService/reply/error");

                    string url = "", successURL = siteUrl + "order/PaymentAccepted.aspx", failureURL = siteUrl + "order/PaymentException.aspx", cancelURL = siteUrl + "order/PaymentCancelled.aspx";
                    string ReferenceId = "0";
                    string restUrl = "&country=" + request.Order.shippingAddress.countryCode + "&language=en";
                    restUrl = restUrl + "&successURL=" + successURL;
                    restUrl = restUrl + "&failureURL=" + failureURL;
                    restUrl = restUrl + "&cancelURL=" + cancelURL;

                    if (node != null && !string.IsNullOrEmpty(node.InnerText))
                    {
                        url = node.InnerText;
                    }
                    else if (error != null && !string.IsNullOrEmpty(error.InnerText))
                    {
                        return new WorldPayGatewayResponse
                        {
                            Url = siteUrl + "PaymentProcess",
                            ReferenceId = "",
                            ErrorMessage = ((error).FirstChild).Value
                        };
                    }
                    XmlNodeList elemList = xDoc.GetElementsByTagName("reference");
                    for (int i = 0; i < elemList.Count; i++)
                    {
                        ReferenceId = elemList[i].Attributes["id"].Value;
                    }
                    return new WorldPayGatewayResponse
                    {
                        Url = url + restUrl,
                        ReferenceId = ReferenceId,
                    };
                }
                return null;
            }
            catch (Exception ex)
            {
                return new WorldPayGatewayResponse
                {
                    Url = siteUrl + "PaymentProcess",
                    ReferenceId = "",
                };
            }
        }
    }

    public class WorldPayGatewayResponse
    {
        public string Url { get; set; }
        public string ReferenceId { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class WorldPayGatewayRequest
    {
        public Order Order { get; set; }
    }
    public class Order
    {
        public string orderCode { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public string CurrencyCode { get; set; }
        public string exponent { get; set; }
        public paymentMethodMask paymentMethodMask { get; set; }
        public shopper shopper { get; set; }
        public shippingAddress shippingAddress { get; set; }
    }
    public class shopper
    {
        public string shopperEmailAddress { get; set; }

    }
    public class paymentMethodMask
    {
        public string Code { get; set; }
    }
    public class shippingAddress
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string street { get; set; }
        public int houseNumber { get; set; }
        public string postalCode { get; set; }
        public string City { get; set; }
        public string countryCode { get; set; }
    }
    public class shopperAdditionalData
    {
        public string shopperAccountNumber { get; set; }
        public string lastName { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string postalCode { get; set; }
    }
}

