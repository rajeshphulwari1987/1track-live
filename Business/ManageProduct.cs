﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;

namespace Business
{
    public class ManageProduct
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();

        public string Url { get; set; }

        public List<FieldEurailCountry> InterrailOneCountryCode()
        {
            return db.tblEurailCountries.Where(x => x.IsActive && x.CountryCode > 0 && x.CountryCode < 1000).Select(x => new FieldEurailCountry
            {
                CountryCode = (int)x.CountryCode,
                Name = x.Country1
            }).OrderBy(x => x.Name).ToList();
        }

        public bool IsInterrailCategory(Guid Pid)
        {
            try
            {
                return db.tblProductCategoriesLookUps.FirstOrDefault(t => t.ProductID == Pid).tblCategory.IsInterRailPass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblSupplier> GetSupplierList()
        {
            try
            {
                return db.tblSuppliers.OrderBy(x => x.Name).ThenBy(x => x.Category).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveSupplierList(string Name)
        {
            try
            {
                foreach (var x in db.tblSuppliers.Where(x => x.Name == Name))
                {
                    var data = db.tblSuppliers.FirstOrDefault(p => p.Id == x.Id);
                    if (data != null)
                    {
                        x.IsActive = !x.IsActive;
                    }
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSupplier GetSupplier(string Name, string Category)
        {
            try
            {
                return db.tblSuppliers.FirstOrDefault(x => x.Name == Name && x.Category == Category && x.IsActive);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUpdateSupplier(List<tblSupplier> objList)
        {
            try
            {
                foreach (var obj in objList)
                {
                    var data = db.tblSuppliers.FirstOrDefault(x => x.Id == obj.Id);
                    if (data != null)
                    {
                        data.Name = obj.Name;
                        data.Category = obj.Category;
                        data.IsActive = obj.IsActive;
                        data.IsBritrail = obj.IsBritrail;
                        data.IsInterrail = obj.IsInterrail;
                        data.Currency = obj.Currency;
                        data.Commission = obj.Commission;
                    }
                    else
                        db.tblSuppliers.AddObject(obj);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteSupplier(Guid Id)
        {
            try
            {
                //if (!db.tblProducts.Any(x => x.ID == Id))
                //{
                var data = db.tblSuppliers.FirstOrDefault(x => x.Id == Id);
                if (data != null)
                {
                    db.tblSuppliers.DeleteObject(data);
                    db.SaveChanges();
                }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Discount Coupons
        public List<ProuctDiscount> GetCategoryDiscountList(int treeLevel, Guid siteId)
        {
            try
            {
                Guid id = GetLanguageId();
                List<ProuctDiscount> plistOne = new List<ProuctDiscount>();
                List<ProuctDiscount> listTwo = new List<ProuctDiscount>();
                List<ProuctDiscount> listThree = new List<ProuctDiscount>();

                plistOne.AddRange((from tcn in db.tblCategoriesNames
                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                   join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                   join dis in db.tblDiscountCategories on tcn.CategoryID equals dis.CategoryId into mkp
                                   from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                                   where tc.TreeLevel == 1 && tcn.LanguageID == id && tcLookUp.SiteID == siteId
                                   select new ProuctDiscount
                                   {
                                       CategoryName = tcn.Name,
                                       CategoryId = tc.ID,
                                       DiscountCode = ndis.DiscountCode == null ? string.Empty : ndis.DiscountCode,
                                       IsPercent = ndis.IsPercent == null ? false : ndis.IsPercent,
                                       DiscountPrice = ndis.DiscountPrice == null ? (decimal)0.0 : ndis.DiscountPrice,
                                       ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                                       ModifyOn = ndis.ModifyOn,
                                       DiscountFromDate = ndis.discountFromDate.Value,
                                       DiscountToDate = ndis.discountToDate.Value
                                   }).AsEnumerable().Select(x => new ProuctDiscount
                                   {
                                       CategoryName = x.CategoryName,
                                       CategoryId = x.CategoryId,
                                       DiscountCode = x.DiscountCode,
                                       IsPercent = x.IsPercent,
                                       DiscountPrice = x.DiscountPrice,
                                       ModifyName = GetModifyByName(x.ModifyBy),
                                       ModifyOn = x.ModifyOn,
                                       DiscountFromDate = x.DiscountFromDate,
                                       DiscountToDate = x.DiscountToDate
                                   }).Distinct().ToList());

                foreach (var item in plistOne)
                {
                    List<ProuctDiscount> oList = (from tcn in db.tblCategoriesNames
                                                  join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                                  join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                                  join dis in db.tblDiscountCategories on tcn.CategoryID equals dis.CategoryId into mkp
                                                  from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                                                  where tc.TreeLevel == 2 && tcn.LanguageID == id && tc.ParentID == item.CategoryId && tcLookUp.SiteID == siteId
                                                  select new ProuctDiscount
                                                  {
                                                      CategoryName = item.CategoryName + " -> " + tcn.Name,
                                                      CategoryId = tc.ID,
                                                      DiscountCode = ndis.DiscountCode == null ? string.Empty : ndis.DiscountCode,
                                                      IsPercent = ndis.IsPercent == null ? false : ndis.IsPercent,
                                                      DiscountPrice = ndis.DiscountPrice == null ? (decimal)0.0 : ndis.DiscountPrice,
                                                      ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                                                      ModifyOn = ndis.ModifyOn,
                                                      DiscountFromDate = ndis.discountFromDate.Value,
                                                      DiscountToDate = ndis.discountToDate.Value
                                                  }).AsEnumerable().Select(x => new ProuctDiscount
                                                  {
                                                      CategoryName = x.CategoryName,
                                                      CategoryId = x.CategoryId,
                                                      DiscountCode = x.DiscountCode,
                                                      IsPercent = x.IsPercent,
                                                      DiscountPrice = x.DiscountPrice,
                                                      ModifyName = GetModifyByName(x.ModifyBy),
                                                      ModifyOn = x.ModifyOn,
                                                      DiscountFromDate = x.DiscountFromDate,
                                                      DiscountToDate = x.DiscountToDate
                                                  }).Distinct().ToList();
                    listTwo.AddRange(oList);
                }

                foreach (var item in listTwo)
                {
                    List<ProuctDiscount> oList = (from tcn in db.tblCategoriesNames
                                                  join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                                  join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                                  join dis in db.tblDiscountCategories on tcn.CategoryID equals dis.CategoryId into mkp
                                                  from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                                                  where tc.TreeLevel == 3 && tcn.LanguageID == id && tc.ParentID == item.CategoryId && tcLookUp.SiteID == siteId
                                                  select new ProuctDiscount
                                                  {
                                                      CategoryName = item.CategoryName + " -> " + tcn.Name,
                                                      CategoryId = tc.ID,
                                                      DiscountCode = ndis.DiscountCode == null ? string.Empty : ndis.DiscountCode,
                                                      IsPercent = ndis.IsPercent == null ? false : ndis.IsPercent,
                                                      DiscountPrice = ndis.DiscountPrice == null ? (decimal)0.0 : ndis.DiscountPrice,
                                                      ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                                                      ModifyOn = ndis.ModifyOn,
                                                      DiscountFromDate = ndis.discountFromDate.Value,
                                                      DiscountToDate = ndis.discountToDate.Value
                                                  }).AsEnumerable().Select(x => new ProuctDiscount
                                                  {
                                                      CategoryName = x.CategoryName,
                                                      CategoryId = x.CategoryId,
                                                      DiscountCode = x.DiscountCode,
                                                      IsPercent = x.IsPercent,
                                                      DiscountPrice = x.DiscountPrice,
                                                      ModifyName = GetModifyByName(x.ModifyBy),
                                                      ModifyOn = x.ModifyOn,
                                                      DiscountFromDate = x.DiscountFromDate,
                                                      DiscountToDate = x.DiscountToDate
                                                  }).Distinct().ToList();

                    listThree.AddRange(oList);
                }
                List<ProuctDiscount> list = new List<ProuctDiscount>();
                list = plistOne.Concat(listTwo).Concat(listThree).OrderBy(x => x.CategoryName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProuctDiscount> GetProductDiscountList(Guid siteId, List<Guid> listIds)
        {
            try
            {
                Guid id = GetLanguageId();
                string bidcream = string.Empty;
                List<Guid> listCatId = new List<Guid>();
                if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                {
                    listCatId.AddRange(listIds);
                    listIds = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                    listCatId.AddRange(listIds);
                    if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                    {
                        listCatId = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                        listCatId.AddRange(listIds);
                    }
                }
                else
                    listCatId = listIds;

                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tplookUp in db.tblProductSiteLookUps on tpn.ProductID equals tplookUp.ProductID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        join dis in db.tblDiscountProducts on tc.ProductID equals dis.ProductId into mkp
                        from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                        where tpn.LanguageID == id && tc.tblCategory.tblCategorySiteLookUps.Any(x => x.SiteID == siteId) && tplookUp.SiteID == siteId
                        && listCatId.Contains(tc.CategoryID)
                        select new ProuctDiscount
                        {
                            ProductId = tp.ID,
                            ProductName = tpn.Name,
                            CategoryId = tc.CategoryID,
                            CategoryName = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == id).Name,
                            DiscountCode = ndis.DiscountCode == null ? string.Empty : ndis.DiscountCode,
                            IsPercent = ndis.IsPercent == null ? false : ndis.IsPercent,
                            DiscountPrice = ndis.DiscountPrice == null ? (decimal)0.0 : ndis.DiscountPrice,
                            ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                            ModifyOn = ndis.ModifyOn,
                            DiscountFromDate = ndis.discountFromDate.Value,
                            DiscountToDate = ndis.discountToDate.Value
                        }).OrderBy(x => x.ProductName).AsEnumerable().Select(x => new ProuctDiscount
                        {
                            ProductId = x.ProductId,
                            ProductName = x.ProductName,
                            CategoryId = x.CategoryId,
                            CategoryName = x.CategoryName,
                            DiscountCode = x.DiscountCode,
                            IsPercent = x.IsPercent,
                            DiscountPrice = x.DiscountPrice,
                            ModifyOn = x.ModifyOn,
                            ModifyName = GetModifyByName(x.ModifyBy),
                            DiscountFromDate = x.DiscountFromDate,
                            DiscountToDate = x.DiscountToDate
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductPricing> GetProductsPriceDiscountList(List<Guid> pids, Guid siteId)
        {
            Guid langId = GetLanguageId();
            List<ProductPricing> priceList = new List<ProductPricing>();

            var item =
                (from tpp in db.tblProductPrices
                 join tp in db.tblProducts on tpp.ProductID equals tp.ID
                 join tcm in db.tblCurrencyMsts on tp.CurrencyID equals tcm.ID
                 join tps in db.tblProductSiteLookUps on tpp.ProductID equals tps.ProductID
                 join mkps in db.tblDiscountProductPrices on tpp.ID equals mkps.ProductPriceId into mkp
                 from tpdis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                 where pids.Contains(tpp.ProductID) && tps.SiteID == siteId
                 select new { tpp, tpdis, tcm }).ToList();

            if (item.Count() > 0)
            {
                priceList.AddRange(item.AsEnumerable().Select(x => new ProductPricing
                {
                    ClassId = x.tpp.ClassID,
                    ClassName = x.tpp.tblClassMst.Name,
                    ID = x.tpp.ID,
                    Price = x.tpp.Price,
                    UniqueProductID = x.tpp.UniqueProductID,
                    PassTypeCode = x.tpp.PassTypeCode,
                    TravellerId = x.tpp.TravelerID,
                    TravellerName = x.tpp.tblTravelerMst.Name,
                    TravellerValidId = x.tpp.ValidityID,
                    ProductId = x.tpp.ProductID,
                    TravellerValidName = (db.tblProductNames.Where(a => a.ProductID == x.tpp.ProductID).FirstOrDefault().Name + " " + x.tpp.tblProductValidUpTo.tblProductValidUpToNames.FirstOrDefault(z => z.LanguageID == langId).Name),
                    Currency = x.tcm.Symbol,
                    DiscountPrice = (x.tpdis == null ? (decimal)0.0 : x.tpdis.DiscountPrice),
                    IsPercent = (x.tpdis == null ? false : x.tpdis.IsPercent),
                    DiscountCode = (x.tpdis == null ? string.Empty : x.tpdis.DiscountCode),
                    ModifyByName = GetModifyByName(x.tpdis == null ? new Guid() : (Guid)x.tpdis.ModifyBy),
                    ModifyOn = x.tpdis == null ? (DateTime?)null : x.tpdis.ModifyOn
                }));
            }
            return priceList.OrderBy(x => x.TravellerName).ToList();
        }
        public Boolean CheckDiffDiscountForCategory(Guid catId, Guid siteid)
        {
            var IsDiff = false;
            var rval = db.CheckDiffDiscountCodeValue(siteid, catId).FirstOrDefault();
            if (rval != null)
            {
                if (!string.IsNullOrEmpty(rval.DiscountCode) && rval.DiscountCode != "0") { IsDiff = true; }
                else if (rval.DiscountPrice > 0) { IsDiff = true; }
                else if (rval.DiscountIsPercent.Value) { IsDiff = true; }
            } return IsDiff;
        }
        public int UpdateDiscount(Guid id, Decimal discountprice, String discountcode, Boolean isPercent, Guid siteID, Int32 flag, Guid adminid, DateTime? DiscountFromDate, DateTime? DiscountToDate)
        {
            try
            {
                var result = db.DiscountAssign(id, discountprice, discountcode, isPercent, siteID, flag, adminid, DiscountFromDate, DiscountToDate);
                return result.FirstOrDefault().Value;
            }
            catch (Exception ex)

            { throw ex; }
        }
        #endregion
        public int GetProductCount()
        {
            return db.tblProducts.Count();
        }

        #region Manage Categories
        //--Add
        public Guid AddCategory(tblCategory category)
        {
            try
            {
                if (category.ID.ToString().Contains("0000"))
                {
                    category.ID = Guid.NewGuid();
                    db.tblCategories.AddObject(category);
                }

                db.SaveChanges();
                return category.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddCategorySiteLookUp(tblCategorySiteLookUp siteLookUp)
        {
            try
            {
                if (siteLookUp.ID.ToString().Contains("0000"))
                {
                    siteLookUp.ID = Guid.NewGuid();
                    db.tblCategorySiteLookUps.AddObject(siteLookUp);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddCategoryName(tblCategoriesName categoriesName)
        {
            try
            {
                if (categoriesName.ID.ToString().Contains("0000"))
                {
                    categoriesName.ID = Guid.NewGuid();
                    db.tblCategoriesNames.AddObject(categoriesName);
                }

                db.SaveChanges();
                return categoriesName.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddPrdSiteLookUp(tblProductSiteLookUp siteLookUp)
        {
            try
            {
                if (siteLookUp.ID.ToString().Contains("0000"))
                {
                    siteLookUp.ID = Guid.NewGuid();
                    db.tblProductSiteLookUps.AddObject(siteLookUp);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //--Delete
        public int DeleteCategory(Guid id)
        {
            try
            {
                if (!id.ToString().Contains("0000"))
                {
                    return db.DELETECATEGORYBYID(id).FirstOrDefault().Value;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Product Category is alredy in used.");
            }
        }
        public void ActiveInactiveCategory(Guid id)
        {
            try
            {
                var objCat = db.tblCategories.FirstOrDefault(x => x.ID == id);
                if (objCat != null)
                    objCat.IsActive = !objCat.IsActive;
                db.SaveChanges();
                bool isactive = objCat != null && objCat.IsActive;

                var objSubCat = db.tblCategories.Where(x => x.ParentID == id).ToList();
                foreach (var item in objSubCat)
                {
                    var objSubCat2 = db.tblCategories.Where(x => x.ParentID == item.ID).ToList();
                    foreach (var item2 in objSubCat2)
                    {
                        var objSubCat3 = db.tblCategories.Where(x => x.ParentID == item2.ID).ToList();
                        foreach (var item3 in objSubCat3)
                        {
                            item3.IsActive = isactive;
                            db.SaveChanges();
                        }
                        item2.IsActive = isactive;
                        db.SaveChanges();
                    }
                    item.IsActive = isactive;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CategoryList
        {
            public Guid id { get; set; }
        }

        public bool DeleteCategoriesNameOfDiffLang(Guid catId, Guid langId, string catName)
        {
            try
            {
                var oCatName = db.tblCategoriesNames.FirstOrDefault(x => x.Name.Trim() == catName.Trim() && x.LanguageID == langId && x.CategoryID == catId);
                db.DeleteObject(oCatName);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //--Select By Id for edit
        public List<tblCategoriesName> GetCategoriesNameById(Guid id)
        {
            return db.tblCategoriesNames.Where(x => x.CategoryID == id).Select(y => y).ToList();
        }

        public tblCategory GetParentCategoryById(Guid id)
        {
            return db.tblCategories.FirstOrDefault(x => x.ID == id);
        }

        public List<Guid> GetSiteIdById(Guid id)
        {
            return db.tblCategorySiteLookUps.Where(x => x.CategoryID == id).Select(y => y.SiteID).ToList();
        }
        public List<Guid> GetSiteIdByCatIdList(Guid catId)
        {
            try
            {
                return db.tblCategorySiteLookUps.Where(x => x.CategoryID == catId).Select(y => y.SiteID).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblCategorySiteLookUp> GetLookUpSiteById(Guid id)
        {
            return db.tblCategorySiteLookUps.Where(x => x.CategoryID == id).Select(y => y).ToList();
        }

        //--Select for list item
        public int GetTreeLevelOfCategory(Guid id)
        {
            var oCat = db.tblCategories.FirstOrDefault(x => x.ID == id);
            if (oCat != null)
                return oCat.TreeLevel;
            return 0;
        }
        public List<tblCategoriesName> GetCategoryList(int treeLevel)
        {
            try
            {
                Guid id = GetLanguageId();
                var list = (from tcn in db.tblCategoriesNames
                            join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                            select new { tcn, tc }).ToList();
                return list.Where(t => t.tc.TreeLevel <= treeLevel && t.tcn.LanguageID == id).Select(x => x.tcn).ToList().OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GetProductTerm GetProductTermsById(Guid pid)
        {
            try
            {
                return db.GetProductTerm(pid).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public tblCategory GetCategoryTermsById(Guid id)
        {
            try
            {
                return db.tblCategories.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ProductCategory> GetAssociateCategoryList(int treeLevel, Guid siteId)
        {
            try
            {
                Guid id = GetLanguageId();
                List<ProductCategory> plistOne = new List<ProductCategory>();
                List<ProductCategory> listTwo = new List<ProductCategory>();
                List<ProductCategory> listThree = new List<ProductCategory>();

                plistOne.AddRange((from tcn in db.tblCategoriesNames
                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                   join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                   where tc.TreeLevel == 1 && tcn.LanguageID == id && tcLookUp.SiteID == siteId
                                   select new ProductCategory
                                   {
                                       ID = tc.ID,
                                       Name = tcn.Name,
                                       TreeLevel = 1,
                                       IsActive = tc.IsActive,
                                       IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                       MarkUp = tcLookUp.MarkUpAmount > 0 ? tcLookUp.MarkUpAmount : tcLookUp.MarkUpPercent,
                                       IsPercent = tcLookUp.MarkUpPercent > 0,
                                       IsBritrail = tc.IsBritRailPass,
                                       IsInterRail = tc.IsInterRailPass,
                                       ModifyBy = tcLookUp.ModifyBy == null ? new Guid() : (Guid)tcLookUp.ModifyBy,
                                       ModifyOn = (DateTime)tcLookUp.ModifyOn
                                   }).AsEnumerable().Select(x => new ProductCategory
                                   {
                                       ID = x.ID,
                                       Name = x.Name,
                                       TreeLevel = 1,
                                       IsActive = x.IsActive,
                                       IsPrintQueueEnable = x.IsPrintQueueEnable,
                                       MarkUp = x.MarkUp,
                                       IsPercent = x.IsPercent,
                                       IsBritrail = x.IsBritrail,
                                       IsInterRail = x.IsInterRail,
                                       ModifyByName = GetModifyByName(x.ModifyBy),
                                       ModifyOn = (DateTime)x.ModifyOn
                                   }).ToList());

                foreach (var item in plistOne)
                {
                    List<ProductCategory> oList = (from tcn in db.tblCategoriesNames
                                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                                   join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                                   where tc.TreeLevel == 2 && tcn.LanguageID == id && tc.ParentID == item.ID && tcLookUp.SiteID == siteId

                                                   select new ProductCategory
                                                   {
                                                       Name = item.Name + " -> " + tcn.Name,
                                                       ID = tc.ID,
                                                       TreeLevel = 2,
                                                       IsActive = tc.IsActive,
                                                       IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                                       MarkUp = tcLookUp.MarkUpAmount > 0 ? tcLookUp.MarkUpAmount : tcLookUp.MarkUpPercent,
                                                       IsPercent = tcLookUp.MarkUpPercent > 0,
                                                       IsBritrail = tc.IsBritRailPass,
                                                       IsInterRail = tc.IsInterRailPass,
                                                       ModifyBy = tcLookUp.ModifyBy == null ? new Guid() : (Guid)tcLookUp.ModifyBy,
                                                       ModifyOn = (DateTime)tcLookUp.ModifyOn
                                                   }).AsEnumerable().Select(x => new ProductCategory
                                                   {
                                                       ID = x.ID,
                                                       Name = x.Name,
                                                       TreeLevel = 1,
                                                       IsActive = x.IsActive,
                                                       IsPrintQueueEnable = x.IsPrintQueueEnable,
                                                       MarkUp = x.MarkUp,
                                                       IsPercent = x.IsPercent,
                                                       IsBritrail = x.IsBritrail,
                                                       IsInterRail = x.IsInterRail,
                                                       ModifyByName = GetModifyByName(x.ModifyBy),
                                                       ModifyOn = (DateTime)x.ModifyOn
                                                   }).ToList();
                    listTwo.AddRange(oList);
                }

                foreach (var item in listTwo)
                {
                    List<ProductCategory> oList = (from tcn in db.tblCategoriesNames
                                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                                   join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                                   where tc.TreeLevel == 3 && tcn.LanguageID == id && tc.ParentID == item.ID && tcLookUp.SiteID == siteId
                                                   select new ProductCategory
                                                   {
                                                       Name = item.Name + " --> " + tcn.Name,
                                                       ID = tc.ID,
                                                       TreeLevel = 3,
                                                       IsActive = tc.IsActive,
                                                       IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                                       MarkUp = tcLookUp.MarkUpAmount > 0 ? tcLookUp.MarkUpAmount : tcLookUp.MarkUpPercent,
                                                       IsPercent = tcLookUp.MarkUpPercent > 0,
                                                       IsBritrail = tc.IsBritRailPass,
                                                       IsInterRail = tc.IsInterRailPass,
                                                       ModifyBy = tcLookUp.ModifyBy == null ? new Guid() : (Guid)tcLookUp.ModifyBy,
                                                       ModifyOn = (DateTime)tcLookUp.ModifyOn
                                                   }).AsEnumerable().Select(x => new ProductCategory
                                                   {
                                                       ID = x.ID,
                                                       Name = x.Name,
                                                       TreeLevel = 1,
                                                       IsActive = x.IsActive,
                                                       IsPrintQueueEnable = x.IsPrintQueueEnable,
                                                       MarkUp = x.MarkUp,
                                                       IsPercent = x.IsPercent,
                                                       IsBritrail = x.IsBritrail,
                                                       IsInterRail = x.IsInterRail,
                                                       ModifyByName = GetModifyByName(x.ModifyBy),
                                                       ModifyOn = (DateTime)x.ModifyOn
                                                   }).ToList();

                    listThree.AddRange(oList);
                }

                List<ProductCategory> list = new List<ProductCategory>();
                switch (treeLevel)
                {
                    case 1:
                        list = plistOne.OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                    case 2:
                        list = plistOne.Concat(listTwo).OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                    case 3:
                        list = plistOne.Concat(listTwo).Concat(listThree).OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                }
                if (siteId != new Guid())
                    list = (from lst in list join x in db.tblCategorySiteLookUps on lst.ID equals x.CategoryID where x.SiteID == siteId select lst).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string GetModifyByName(Guid adminid)
        {
            var tbladminuser = db.tblAdminUsers.FirstOrDefault(x => x.ID == adminid);
            return tbladminuser == null ? "--" : tbladminuser.UserName.ToLower().Trim();
        }
        /*RailPass page*/
        public List<ProductCategory> GetAssociateCategoryListRailPass(int treeLevel, Guid siteId)
        {
            try
            {
                var id = GetLanguageId();
                var plistOne = new List<ProductCategory>();
                var listTwo = new List<ProductCategory>();
                var listThree = new List<ProductCategory>();

                plistOne.AddRange((from tcn in db.tblCategoriesNames
                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                   join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                   where tc.TreeLevel == 1 && tcn.LanguageID == id && tcLookUp.SiteID == siteId
                                   select new ProductCategory
                                   {
                                       ID = tc.ID,
                                       Name = tcn.Name,
                                       TreeLevel = 1,
                                       IsActive = tc.IsActive,
                                       IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                       MarkUp = tcLookUp.MarkUpAmount > 0 ? tcLookUp.MarkUpAmount : tcLookUp.MarkUpPercent,
                                       IsPercent = tcLookUp.MarkUpPercent > 0,
                                       IsAD75 = (bool)tc.IsFOCAD75
                                   }).ToList());

                foreach (var item in plistOne)
                {
                    var oList = (from tcn in db.tblCategoriesNames
                                 join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                 join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                 where tc.TreeLevel == 2 && tcn.LanguageID == id && tc.ParentID == item.ID && tcLookUp.SiteID == siteId

                                 select new ProductCategory
                                 {
                                     Name = tcn.Name,
                                     ID = tc.ID,
                                     TreeLevel = 2,
                                     IsActive = tc.IsActive,
                                     IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                     MarkUp = tcLookUp.MarkUpAmount > 0 ? tcLookUp.MarkUpAmount : tcLookUp.MarkUpPercent,
                                     IsPercent = tcLookUp.MarkUpPercent > 0,
                                     IsAD75 = (bool)tc.IsFOCAD75
                                 }).ToList();
                    listTwo.AddRange(oList);
                }

                foreach (var item in listTwo)
                {
                    var oList = (from tcn in db.tblCategoriesNames
                                 join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                 join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                 where tc.TreeLevel == 3 && tcn.LanguageID == id && tc.ParentID == item.ID && tcLookUp.SiteID == siteId
                                 select new ProductCategory
                                 {
                                     Name = tcn.Name,
                                     ID = tc.ID,
                                     TreeLevel = 3,
                                     IsActive = tc.IsActive,
                                     IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                     MarkUp = tcLookUp.MarkUpAmount > 0 ? tcLookUp.MarkUpAmount : tcLookUp.MarkUpPercent,
                                     IsPercent = tcLookUp.MarkUpPercent > 0,
                                     IsAD75 = (bool)tc.IsFOCAD75
                                 }).ToList();
                    listThree.AddRange(oList);
                }

                var list = new List<ProductCategory>();
                switch (treeLevel)
                {
                    case 1:
                        list = plistOne.OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                    case 2:
                        list = plistOne.Concat(listTwo).OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                    case 3:
                        list = plistOne.Concat(listTwo).Concat(listThree).OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                }
                if (siteId != new Guid())
                    list = (from lst in list join x in db.tblCategorySiteLookUps on lst.ID equals x.CategoryID where x.SiteID == siteId select lst).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductCategory> GetCategoryListBySiteId(int treeLevel, List<Guid> siteIdList, List<Guid> parentIdList)
        {
            try
            {
                Guid id = GetLanguageId();
                var list = (from tcn in db.tblCategoriesNames
                            join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                            join ts in db.tblCategorySiteLookUps on tc.ID equals ts.CategoryID
                            where siteIdList.Contains(ts.SiteID) && tc.TreeLevel == treeLevel && tcn.LanguageID == id
                            select new ProductCategory
                            {
                                ID = tc.ID,
                                Name = tcn.Name,
                                ParentId = tc.ParentID
                            }).OrderBy(x => x.Name).ToList();

                return parentIdList == null ? list : list.Where(x => parentIdList.Contains(x.ParentId)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid GetFirstSiteIdForCategory()
        {
            var olkp = db.tblCategorySiteLookUps.FirstOrDefault();
            if (olkp != null)
                return olkp.SiteID;
            return new Guid();
        }
        public Guid GetParentIdforChildId(Guid ID)
        {
            try
            {
                return db.tblCategories.FirstOrDefault(t => t.ID == ID).ParentID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CategorySiteList> GetCategorySiteList(Guid parentId, Guid siteId)
        {
            Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
            var tblLanguagesMst = db.tblLanguagesMsts.FirstOrDefault(x => x.ID == langid);
            if (tblLanguagesMst == null)
                return null;

            Guid langId = tblLanguagesMst.ID;
            List<CategorySiteList> list = new List<CategorySiteList>();
            if (parentId == Guid.Empty)
            {
                List<Guid> removelist = (from p in db.tblCategories join q in db.tblCategories on p.ID equals q.ParentID select p.ID).ToList();
                List<Guid> mainList = db.tblCategories.Where(c => !removelist.Contains(c.ParentID)).Select(c => c.ID).ToList();

                var olist = (from tcn in db.tblCategoriesNames
                             join tc in db.tblCategories on tcn.CategoryID equals tc.ID

                             select new { tc, tcn, }).ToList();

                if (langId != Guid.Empty)
                    olist = olist.Where(t => t.tc.tblCategorySiteLookUps.Any(x => x.SiteID == siteId) && t.tcn.LanguageID == langId && mainList.Contains(t.tcn.CategoryID)).ToList();
                list = olist.Select(x => new CategorySiteList
                {
                    Id = x.tcn.CategoryID,
                    IsActive = x.tc.IsActive,
                    Name = x.tcn.Name.Trim(),
                    SiteName = GetSiteName(x.tcn.CategoryID)
                }).ToList().GroupBy(y => new { y.Id, y.IsActive, y.Name, y.SiteName }).Select(z => new CategorySiteList { Id = z.Key.Id, Name = z.Key.Name, IsActive = z.Key.IsActive, SiteName = z.Key.SiteName }).ToList();

                return list;
            }

            //--Call if(parentId) is exist
            var oplist = (from tcn in db.tblCategoriesNames
                          join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                          join tlkp in db.tblCategorySiteLookUps on tcn.CategoryID equals tlkp.CategoryID
                          where tc.ParentID == parentId && tcn.LanguageID == langId && tlkp.SiteID == siteId
                          select new { tc, tcn, tlkp }).ToList();

            //if (!langId==Guid.Empty && !parentId==Guid.Empty)
            //    oplist = oplist.Where(t => t.tcn.CategoryID == t.tc.ID && t.tcn.LanguageID == langId ).ToList();

            list = oplist.Distinct().Select(x => new CategorySiteList
            {
                Id = x.tlkp.CategoryID,
                IsActive = x.tc.IsActive,
                Name = x.tcn.Name,
                SiteName = GetSiteName(x.tlkp.CategoryID)
            }).ToList().GroupBy(y => new { y.Id, y.IsActive, y.Name, y.SiteName }).Select(z => new CategorySiteList { Id = z.Key.Id, Name = z.Key.Name, IsActive = z.Key.IsActive, SiteName = z.Key.SiteName }).ToList();
            return list.ToList();
        }

        public string GetSiteName(Guid id)
        {
            var res = (from catSi in db.tblCategorySiteLookUps
                       join ts in db.tblSites on catSi.SiteID equals ts.ID
                       where catSi.CategoryID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";
            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }
        //--Get Language Id
        public Guid GetLanguageId()
        {
            Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
            var oLng = db.tblLanguagesMsts.FirstOrDefault(x => x.ID == langid);
            return oLng != null ? oLng.ID : new Guid();
        }
        #endregion

        public bool IsActiveProductWithEnableForPurchase(Guid prdID, Guid SiteId)
        {
            try
            {
                int Hours = 0;
                int Minuts = 0;
                DateTime ZoneDate = DateTime.Now;
                string TimeZone = db.tblSites.FirstOrDefault(x => x.ID == SiteId).GmtTimeZone;
                if (!string.IsNullOrEmpty(TimeZone))
                {
                    Hours = Convert.ToInt32(TimeZone.Split(':')[0] + TimeZone.Split(':')[1]);
                    Minuts = Convert.ToInt32(TimeZone.Split(':')[0] + TimeZone.Split(':')[2]);
                }
                ZoneDate = ZoneDate.AddHours(Hours);
                ZoneDate = ZoneDate.AddMinutes(Minuts);

                if (db.tblProductSiteLookUps.Any(t => t.SiteID == SiteId && t.ProductID == prdID) && db.tblProducts.Any(x => x.ID == prdID && x.IsActive && x.IsAllowAdminPass == false))
                {
                    if (db.tblProducts.Any(x => x.ID == prdID && x.IsProductEnableFromDate && x.IsProductEnableToDate && x.ProductEnableFromDate <= DateTime.Now && x.ProductEnableToDate >= DateTime.Now))
                        return true;
                    else
                        return db.tblProductSiteLookUps.Any(x => x.SiteID == SiteId && x.ProductID == prdID && x.IsProductEnableFromDate && x.IsProductEnableToDate && x.ProductEnableFromDate <= ZoneDate && x.ProductEnableToDate >= ZoneDate);
                }
                else
                    return false;
            }
            catch (Exception ex) { throw ex; }
        }
        #region Manage Product
        public bool IsExistProductName(string name)
        {
            try
            {
                Guid langId = GetLanguageId();
                return db.tblProductNames.Any(x => x.Name == name.Trim() && x.LanguageID == langId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddProduct(tblProduct oProduct)
        {
            try
            {
                if (oProduct.ID == new Guid())
                {
                    oProduct.ID = Guid.NewGuid();
                    db.tblProducts.AddObject(oProduct);
                }
                else
                {
                    var oVal = db.tblProducts.FirstOrDefault(x => x.ID == oProduct.ID);
                    if (oVal != null)
                    {
                        oVal.SupplierId = oProduct.SupplierId;
                        oVal.IsElectronicPass = oProduct.IsElectronicPass;
                        oVal.ElectronicPassNote = oProduct.ElectronicPassNote;
                        oVal.StartDayText = oProduct.StartDayText;
                        oVal.CountryEndCode = oProduct.CountryEndCode;
                        oVal.CountryStartCode = oProduct.CountryStartCode;
                        oVal.IsActive = oProduct.IsActive;
                        oVal.Istwinpass = oProduct.Istwinpass;
                        oVal.IsAllowRefund = oProduct.IsAllowRefund;
                        oVal.IsAllowMiddleName = oProduct.IsAllowMiddleName;
                        oVal.PassportValid = oProduct.PassportValid;
                        oVal.NationalityValid = oProduct.NationalityValid;
                        oVal.IsShippingAplicable = oProduct.IsShippingAplicable;
                        oVal.IsSpecialOffer = oProduct.IsSpecialOffer;
                        oVal.BestSeller = oProduct.BestSeller;
                        oVal.Description = oProduct.Description;
                        oVal.Discount = oProduct.Discount;
                        oVal.Eligibility = oProduct.Eligibility;
                        oVal.EligibilityforSaver = oProduct.EligibilityforSaver;
                        oVal.TermCondition = oProduct.TermCondition;
                        oVal.GreatPassDescription = oProduct.GreatPassDescription;
                        oVal.Validity = oProduct.Validity;
                        oVal.CurrencyID = oProduct.CurrencyID;
                        oVal.ModifiedBy = oProduct.CreatedBy;
                        oVal.ModifiedOn = oProduct.CreatedOn;
                        oVal.ProductImage = oProduct.ProductImage;
                        oVal.ProductImageSecond = oProduct.ProductImageSecond;
                        oVal.MonthValidity = oProduct.MonthValidity;
                        oVal.ProductValidFromDate = oProduct.ProductValidFromDate;
                        oVal.ProductValidToDate = oProduct.ProductValidToDate;
                        oVal.ProductEnableFromDate = oProduct.ProductEnableFromDate;
                        oVal.ProductEnableToDate = oProduct.ProductEnableToDate;
                        oVal.SpecialOfferText = oProduct.SpecialOfferText;
                        oVal.AnnouncementTitle = oProduct.AnnouncementTitle;
                        oVal.Announcement = oProduct.Announcement;
                        oVal.ProductAltTag1 = oProduct.ProductAltTag1;
                        oVal.ProductAltTag2 = oProduct.ProductAltTag2;
                        oVal.BritrailPromoFrom = oProduct.BritrailPromoFrom;
                        oVal.BritrailPromoTo = oProduct.BritrailPromoTo;
                        oVal.IrTitle = oProduct.IrTitle;
                        oVal.IrDesciption = oProduct.IrDesciption;
                        oVal.IsAllowAdminPass = oProduct.IsAllowAdminPass;
                        oVal.IsProductEnableFromDate = oProduct.IsProductEnableFromDate;
                        oVal.IsProductEnableToDate = oProduct.IsProductEnableToDate;
                        oVal.IsPromoPass = oProduct.IsPromoPass;

                        if (!string.IsNullOrEmpty(oProduct.HomePageProductImage))
                            oVal.HomePageProductImage = oProduct.HomePageProductImage;
                        if (!string.IsNullOrEmpty(oProduct.CounrtyImage))
                            oVal.CounrtyImage = oProduct.CounrtyImage;
                        if (!string.IsNullOrEmpty(oProduct.BannerImage))
                            oVal.BannerImage = oProduct.BannerImage;
                    }
                    ManageRoutes objroute = new ManageRoutes();
                    objroute.UpdateRouteCount(Route.ProductRoute, 0);
                }
                db.SaveChanges();
                return oProduct.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddProductName(tblProductName oName)
        {
            try
            {
                if (oName.ID == Guid.Empty)
                {
                    if (IsExistProductName(oName.Name))
                        throw new Exception("Product Name is already exist.");

                    oName.ID = Guid.NewGuid();
                    db.tblProductNames.AddObject(oName);
                }
                else
                {
                    var oVal = db.tblProductNames.FirstOrDefault(x => x.ID == oName.ID);
                    if (oVal != null)
                    {
                        oVal.DefaultWhenMissing = oName.DefaultWhenMissing;
                        oVal.LanguageID = oName.LanguageID;
                        oVal.Name = oName.Name;
                        oVal.UserFriendlyName = oName.UserFriendlyName;
                    }
                }
                db.SaveChanges();
                return oName.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactiveProductPrice(Guid id)
        {
            try
            {
                var oVal = db.tblProductPrices.FirstOrDefault(x => x.ID == id);
                if (oVal != null)
                {
                    oVal.IsActive = !oVal.IsActive;

                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddProductPriceStatus(tblProductPrice oProductPrice)
        {
            try
            {
                var oVal = db.tblProductPrices.FirstOrDefault(x => x.ID == oProductPrice.ID);
                if (oVal != null)
                {
                    oVal.TravelerID = oProductPrice.TravelerID;
                    oVal.IsActive = oProductPrice.IsActive;
                    oVal.Price = oProductPrice.Price;
                    oVal.PriceBindLevel = oProductPrice.PriceBindLevel;
                }
                db.SaveChanges();
                return oProductPrice.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddProductPrice(tblProductPrice oProductPrice)
        {
            try
            {
                if (oProductPrice.ID == Guid.Empty)
                {
                    oProductPrice.ID = Guid.NewGuid();
                    db.tblProductPrices.AddObject(oProductPrice);
                }
                else
                {
                    var oVal = db.tblProductPrices.FirstOrDefault(x => x.ID == oProductPrice.ID);
                    if (oVal != null)
                    {
                        oVal.ProductID = oProductPrice.ProductID;
                        oVal.ClassID = oProductPrice.ClassID;
                        oVal.Price = oProductPrice.Price;
                        oVal.PassTypeCode = oProductPrice.PassTypeCode;
                        oVal.UniqueProductID = oProductPrice.UniqueProductID;
                        oVal.TravelerID = oProductPrice.TravelerID;
                        oVal.ValidityID = oProductPrice.ValidityID;
                        oVal.IsActive = oProductPrice.IsActive;
                    }
                }
                db.SaveChanges();
                return oProductPrice.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetProductShortOrder(Guid siteId, Guid productId)
        {
            try
            {
                var prodSite = db.tblProductSiteLookUps.FirstOrDefault(x => x.ProductID == productId && x.SiteID == siteId);
                return prodSite != null ? prodSite.ShortOrder : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddEditShortOrder(Guid siteId, Guid productId, Int32 shortOrder)
        {
            try
            {
                if (shortOrder == 0)
                    return true;

                if (db.tblProductSiteLookUps.Any(x => x.ShortOrder == shortOrder && x.ProductID != productId && x.SiteID == siteId))
                    throw new Exception("Product short order is already assigned for this site.");
                else
                {
                    var prodSite = db.tblProductSiteLookUps.FirstOrDefault(x => x.ProductID == productId && x.SiteID == siteId);
                    if (prodSite != null)
                    {
                        prodSite.ShortOrder = shortOrder;
                        db.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddProductSiteLookUp(List<tblProductSiteLookUp> lookUpList, Guid productId)
        {
            try
            {
                List<Guid> listSites = lookUpList.Select(x => x.SiteID).ToList();
                db.tblProductSiteLookUps.Where(x => x.ProductID == productId && !listSites.Contains(x.SiteID)).ToList().ForEach(db.tblProductSiteLookUps.DeleteObject);
                if (lookUpList != null)
                    foreach (var lookUp in lookUpList)
                    {
                        lookUp.ID = Guid.NewGuid();
                        if (!db.tblProductSiteLookUps.Any(x => x.ProductID == productId && x.SiteID == lookUp.SiteID))
                            db.tblProductSiteLookUps.AddObject(lookUp);
                    }
                db.SaveChanges();
                return productId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddProductCategoriesLookUp(tblProductCategoriesLookUp lookUpCat, Guid productId)
        {
            try
            {
                db.tblProductCategoriesLookUps.Where(x => x.ProductID == productId).ToList().ForEach(db.tblProductCategoriesLookUps.DeleteObject);
                db.tblProductCategoriesLookUps.AddObject(lookUpCat);
                db.SaveChanges();
                return productId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddProductCountriesLookUp(List<tblProductCountryLookUp> lookUpList, Guid productId)
        {
            try
            {
                db.tblProductCountryLookUps.Where(x => x.ProductID == productId).ToList().ForEach(db.tblProductCountryLookUps.DeleteObject);
                if (lookUpList != null)
                    foreach (var lookUp in lookUpList)
                    {
                        lookUp.ID = Guid.NewGuid();
                        db.tblProductCountryLookUps.AddObject(lookUp);
                    }
                db.SaveChanges();
                return productId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddProductCntPrLookUp(List<tblProductPermittedLookup> lookUpList, Guid productId)
        {
            try
            {
                db.tblProductPermittedLookups.Where(x => x.ProductID == productId).ToList().ForEach(db.tblProductPermittedLookups.DeleteObject);
                if (lookUpList != null)
                    foreach (var lookUp in lookUpList)
                    {
                        lookUp.ID = Guid.NewGuid();
                        db.tblProductPermittedLookups.AddObject(lookUp);
                    }
                db.SaveChanges();
                return productId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteProduct(Guid id)
        {
            try
            {
                return db.DELETEPRODUCTBYID(id).FirstOrDefault().RESULT.Value;
            }
            catch (Exception ex)
            {
                throw new Exception("Sorry! This product is already in use. ");
            }
        }
        public bool DeleteProductName(Guid id, Guid idLang, string name)
        {
            try
            {
                var oProd = db.tblProductNames.FirstOrDefault(t => t.ProductID == id && t.LanguageID == idLang && t.Name == name);
                if (oProd != null)
                    db.tblProductNames.DeleteObject(oProd);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Product> GetProductListByCatID(Guid siteId, Guid catId)
        {
            try
            {
                Guid id = GetLanguageId();

                var results = from p in db.tblCategoriesNames
                              group p.Name by p.Name into g
                              where g.Count() > 1
                              select new
                              {
                                  CatName = g.Key
                              };

                var DupliPoducts = from p in db.tblProductNames
                                   group p.Name by p.Name into g
                                   where g.Count() > 1
                                   select new
                                   {
                                       ProductName = g.Key
                                   };


                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        where tpn.LanguageID == id && tp.IsActive == true && !tp.IsAllowAdminPass && tc.CategoryID == catId && tc.tblCategory.tblCategorySiteLookUps.Any(x => x.SiteID == siteId)
                        && (tp.ProductEnableFromDate <= System.DateTime.Now && tp.ProductEnableToDate >= System.DateTime.Now)
                        select new Product
                        {
                            ID = tp.ID,
                            IsActive = tp.IsActive,
                            Name = tpn.Name,
                            Category = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == id).Name,
                            Currency = tp.tblCurrencyMst.Name + "(" + tp.tblCurrencyMst.Symbol + ")",
                            IsShippingAplicable = tp.IsShippingAplicable,
                            IsTopRailPass = tp.IsTopRailPass,
                            SiteID = siteId,
                            ProductDescription = tp.Description.Substring(0, 240) + "...",
                            ProductImage = tp.ProductImage,
                            CategoryID = tc.CategoryID
                        }).OrderBy(x => x.Name).AsEnumerable().Select(t => new Product
                        {

                            ID = t.ID,
                            IsActive = t.IsActive,
                            Name = t.Name,
                            Category = t.Category,
                            Currency = t.Currency,
                            IsShippingAplicable = t.IsShippingAplicable,
                            IsTopRailPass = t.IsTopRailPass,
                            SiteID = t.SiteID,
                            ProductDescription = t.ProductDescription,
                            ProductImage = t.ProductImage,
                            ProductSEOName = results.Any(y => y.CatName == t.Category) ?
                            t.CategoryID.ToString().Substring(0, 4) + "-" + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                            (DupliPoducts.Any(p => p.ProductName == t.Name) ? t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower() : t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower())
                             : t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                            (DupliPoducts.Any(p => p.ProductName == t.Name) ? t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower() : t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower())
                        }).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Product> GetProductList(Guid siteId, List<Guid> listIds)
        {
            try
            {
                DateTime RedSExpdate = DateTime.Now.AddDays(-60);
                DateTime RedEExpdate = DateTime.Now.AddDays(3);
                DateTime YellowSExpdate = DateTime.Now;
                DateTime YellowEExpdate = DateTime.Now.AddDays(45);
                Guid id = GetLanguageId();
                List<Guid> listCatId = new List<Guid>();
                if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                {
                    listCatId.AddRange(listIds);
                    listIds = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                    listCatId.AddRange(listIds);
                    if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                    {
                        listCatId = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                        listCatId.AddRange(listIds);
                    }
                }
                else
                    listCatId = listIds;
                if (listCatId.Count > 0)
                {
                    return (from tpn in db.tblProductNames
                            join tp in db.tblProducts on tpn.ProductID equals tp.ID
                            join tplookUp in db.tblProductSiteLookUps on tpn.ProductID equals tplookUp.ProductID
                            join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                            where tpn.LanguageID == id && tc.tblCategory.tblCategorySiteLookUps.Any(x => x.SiteID == siteId) && tplookUp.SiteID == siteId && listCatId.Contains(tc.CategoryID)
                            select new { tp, tpn, tplookUp, tc }).ToList().Select(x => new Product
                            {
                                ID = x.tp.ID,
                                CircleImg = (x.tp.ProductEnableToDate < RedEExpdate && x.tp.ProductEnableToDate > RedSExpdate) ? "<img src='../images/Predcircle.png' class='imgcircle' title='Purchase to/expiry date Expired or expire imminently'/>" : (x.tp.ProductEnableToDate < YellowEExpdate && x.tp.ProductEnableToDate > YellowSExpdate ? "<img src='../images/Pyellowcircle.png' class='imgcircle' title='Purchase to/expiry date within this or next month'/>" : "<img src='../images/Pgreencircle.png' class='imgcircle' title='Purchase to/expiry date further than next month'/>"),
                                CircleImgType = (x.tp.ProductEnableToDate < RedEExpdate && x.tp.ProductEnableToDate > RedSExpdate) ? 1 : (x.tp.ProductEnableToDate < YellowEExpdate && x.tp.ProductEnableToDate > YellowSExpdate ? 2 : 3),
                                IsActive = x.tp.IsActive,
                                Name = x.tpn.Name,
                                Category = GetCategoryFullName(x.tc.CategoryID),
                                Currency = x.tp.tblCurrencyMst.Name + "(" + x.tp.tblCurrencyMst.Symbol + ")",
                                IsShippingAplicable = x.tp.IsShippingAplicable,
                                IsTopRailPass = x.tp.IsTopRailPass,
                                IsPromoPass = x.tp.IsPromoPass,
                                IsItxPass = x.tp.IsItxPass,
                                IsTwinPass = x.tp.Istwinpass,
                                MarkUp = x.tplookUp.MarkUpAmount > 0 ? x.tplookUp.MarkUpAmount : x.tplookUp.MarkUpPercent,
                                IsPercent = x.tplookUp.MarkUpPercent > 0,
                                IsPassportValid = x.tp.PassportValid,
                                ProductImage = x.tp.ProductImage,
                                Sortorder = db.tblProductSiteLookUps.FirstOrDefault(t => t.SiteID == siteId && t.ProductID == x.tp.ID).ShortOrder,
                                IsSpecialandBest=(x.tp.IsSpecialOffer?x.tp.IsSpecialOffer:x.tp.BestSeller),
                                ImgSpecialandBest = (x.tp.IsSpecialOffer ? "assets/img/tags/corner-special-offer.png" : (x.tp.BestSeller ? "assets/img/tags/corner-best-seller.png" : string.Empty))
                            }).OrderBy(x => x.Sortorder == 0).ThenBy(x => x.Sortorder).ThenBy(t => t.Name).ToList();
                }
                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tplookUp in db.tblProductSiteLookUps on tpn.ProductID equals tplookUp.ProductID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        where tpn.LanguageID == id && tc.tblCategory.tblCategorySiteLookUps.Any(x => x.SiteID == siteId) && tplookUp.SiteID == siteId
                        select new { tp, tpn, tplookUp, tc }).ToList().Select(x => new Product
                        {
                            ID = x.tp.ID,
                            CircleImg = (x.tp.ProductEnableToDate < RedEExpdate && x.tp.ProductEnableToDate > RedSExpdate) ? "<img src='../images/Predcircle.png' class='imgcircle' title='Purchase to/expiry date Expired or expire imminently'/>" : (x.tp.ProductEnableToDate < YellowEExpdate && x.tp.ProductEnableToDate > YellowSExpdate ? "<img src='../images/Pyellowcircle.png' class='imgcircle' title='Purchase to/expiry date within this or next month'/>" : "<img src='../images/Pgreencircle.png' class='imgcircle' title='Purchase to/expiry date further than next month'/>"),
                            CircleImgType = (x.tp.ProductEnableToDate < RedEExpdate && x.tp.ProductEnableToDate > RedSExpdate) ? 1 : (x.tp.ProductEnableToDate < YellowEExpdate && x.tp.ProductEnableToDate > YellowSExpdate ? 2 : 3),
                            IsActive = x.tp.IsActive,
                            IsPromoPass = x.tp.IsPromoPass,
                            IsItxPass = x.tp.IsItxPass,
                            IsTwinPass = x.tp.Istwinpass,
                            Name = x.tpn.Name,
                            ProductImage = x.tp.ProductImage,
                            Category = GetCategoryFullName(x.tc.CategoryID),
                            Currency = x.tp.tblCurrencyMst.Name + "(" + x.tp.tblCurrencyMst.Symbol + ")",
                            IsShippingAplicable = x.tp.IsShippingAplicable,
                            IsTopRailPass = x.tp.IsTopRailPass,
                            MarkUp = x.tplookUp.MarkUpAmount > 0 ? x.tplookUp.MarkUpAmount : x.tplookUp.MarkUpPercent,
                            IsPercent = x.tplookUp.MarkUpPercent > 0,
                            IsPassportValid = x.tp.PassportValid,
                            Sortorder = db.tblProductSiteLookUps.FirstOrDefault(t => t.SiteID == siteId && t.ProductID == x.tp.ID).ShortOrder,
                            IsSpecialandBest = (x.tp.IsSpecialOffer ? x.tp.IsSpecialOffer : x.tp.BestSeller),
                            ImgSpecialandBest = (x.tp.IsSpecialOffer ? "assets/img/tags/corner-special-offer.png" : (x.tp.BestSeller ? "assets/img/tags/corner-best-seller.png" : string.Empty))
                        }).OrderBy(x => x.Sortorder == 0).ThenBy(x => x.Sortorder).ThenBy(t => t.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCategoryFullName(Guid CatId)
        {
            try
            {
                String Result = "";
                Guid SecoundCat = Guid.Empty;
                Guid ThirdCat = Guid.Empty;
                var HaveSecoundCat = db.tblCategories.FirstOrDefault(x => x.ID == CatId);
                if (HaveSecoundCat != null)
                {
                    SecoundCat = HaveSecoundCat.ParentID;
                    var HaveThirdCat = db.tblCategories.FirstOrDefault(x => x.ID == SecoundCat);
                    if (HaveThirdCat != null)
                        ThirdCat = HaveThirdCat.ParentID;
                }
                /*level 1*/
                var ThirdData = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == ThirdCat);
                if (ThirdData != null)
                    Result += ThirdData.Name + " -> ";
                /*level 2*/
                var SecoundData = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == SecoundCat);
                if (SecoundData != null)
                    Result += SecoundData.Name + (Result.Length > 3 ? " --> " : " -> ");
                /*level 3*/
                Result += db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == CatId).Name;
                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Product> GetProductListMarkUp(Guid siteId, List<Guid> listIds)
        {
            try
            {
                Guid id = GetLanguageId();
                string bidcream = string.Empty;
                List<Guid> listCatId = new List<Guid>();
                if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                {
                    listCatId.AddRange(listIds);
                    listIds = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                    listCatId.AddRange(listIds);
                    if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                    {
                        listCatId = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                        listCatId.AddRange(listIds);
                    }
                }
                else
                    listCatId = listIds;

                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tplookUp in db.tblProductSiteLookUps on tpn.ProductID equals tplookUp.ProductID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        where tpn.LanguageID == id && tc.tblCategory.tblCategorySiteLookUps.Any(x => x.SiteID == siteId) && tplookUp.SiteID == siteId
                        && listCatId.Contains(tc.CategoryID)
                        select new Product
                        {
                            ID = tp.ID,
                            IsActive = tp.IsActive,
                            Name = tpn.Name,
                            CategoryId = tc.CategoryID,
                            Category = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == id).Name,
                            Currency = tp.tblCurrencyMst.Name + "(" + tp.tblCurrencyMst.Symbol + ")",
                            IsShippingAplicable = tp.IsShippingAplicable,
                            IsTopRailPass = tp.IsTopRailPass,
                            MarkUp = tplookUp.MarkUpAmount > 0 ? tplookUp.MarkUpAmount : tplookUp.MarkUpPercent,
                            IsPercent = tplookUp.MarkUpPercent > 0,
                            ModifyBy = tplookUp.ModifyBy == null ? new Guid() : (Guid)tplookUp.ModifyBy,
                            ModifyOn = (DateTime)tplookUp.ModifyOn
                        }).OrderBy(x => x.Name).AsEnumerable().Select(x => new Product
                        {
                            ID = x.ID,
                            IsActive = x.IsActive,
                            Name = x.Name,
                            CategoryId = x.CategoryID,
                            Category = x.Category,
                            Currency = x.Currency,
                            IsShippingAplicable = x.IsShippingAplicable,
                            IsTopRailPass = x.IsTopRailPass,
                            MarkUp = x.MarkUp,
                            IsPercent = x.IsPercent,
                            ModifyByName = GetModifyByName(x.ModifyBy),
                            ModifyOn = (DateTime)x.ModifyOn

                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductPrice(List<Guid> idList)
        {
            try
            {
                db.tblProductPrices.Where(x => idList.Contains(x.ID)).ToList().ForEach(db.tblProductPrices.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductPriceById(Guid id)
        {
            try
            {
                if (db.tblProductPrices.Any(x => x.ID == id))
                    db.DeleteProductPrice(id);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInactiveProduct(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null) objProd.IsActive = !objProd.IsActive;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void IsPromoPass(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null) objProd.IsPromoPass = !objProd.IsPromoPass;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void IsTopRailPass(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null) objProd.IsTopRailPass = !objProd.IsTopRailPass;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public String ProductTemplateNote(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null)
                    return objProd.TemplateNote;
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ProductTemplateNoteUpdate(Guid id, string Note)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null)
                    objProd.TemplateNote = Note;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void IsItxPass(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null) objProd.IsItxPass = !objProd.IsItxPass;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void IsPassportValid(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null) objProd.PassportValid = !objProd.PassportValid;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void IsItxPrpduct(Guid id)
        {
            try
            {
                var objProd = db.tblProducts.FirstOrDefault(x => x.ID == id);
                if (objProd != null) objProd.IsItxPass = !objProd.IsItxPass;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblProductName> GetProductsNameById(Guid id)
        {
            return db.tblProductNames.Where(x => x.ProductID == id).ToList();
        }

        public List<ProductPricing> GetProductsPricesById(Guid id)
        {
            Guid langId = GetLanguageId();
            List<ProductPricing> priceList = new List<ProductPricing>();
            priceList.AddRange(db.tblProductPrices.Where(y => y.ProductID == id).AsEnumerable().Select(x => new ProductPricing
            {
                PriceBand = x.PriceBindLevel,
                PriceBandName = (x.PriceBindLevel > 1 ? db.tblPriceBands.FirstOrDefault(t => t.ID == x.PriceBindLevel).Name : "---"),
                ClassId = x.ClassID,
                ClassName = x.tblClassMst.Name,
                ID = x.ID,
                Price = x.Price,
                UniqueProductID = x.UniqueProductID,
                PassTypeCode = x.PassTypeCode,
                TravellerId = x.TravelerID,
                TravellerName = x.tblTravelerMst.Name,
                TravellerValidId = x.ValidityID,
                IsActive = x.IsActive,
                TravellerValidName = x.tblProductValidUpTo.tblProductValidUpToNames.FirstOrDefault(z => z.LanguageID == langId).Name,
                Note = x.tblTravelerMst.Note,
                ValidityNote = GetTravlerNoteName(id)//x.tblProductValidUpTo.Note
            }));
            return priceList.OrderBy(x => x.TravellerName).ThenBy(x => x.ClassName).ThenBy(t => t.TravellerValidName).ToList();
        }

        public string GetTravlerNoteName(Guid ProductID)
        {
            try
            {
                var data = (from tpp in db.tblProductPrices
                            join tpvt in db.tblProductValidUpToes on tpp.ValidityID equals tpvt.ID
                            where tpp.ProductID == ProductID
                            group tpvt.Note by tpvt.Note into g
                            select new { no = g.Count(), name = g.Key }).ToList().OrderByDescending(t => t.no).ToArray();
                return data[0].name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductPricing> GetProductsPricesWithProductNameById(List<Guid> pids, Guid siteId)
        {
            Guid langId = GetLanguageId();
            List<ProductPricing> priceList = new List<ProductPricing>();

            var item =
                from tpp in db.tblProductPrices
                join mkps in db.tblProductMarkUps on tpp.ID equals mkps.ProductPriceID into mkp
                from tpMarkUp in mkp.DefaultIfEmpty()
                join tp in db.tblProducts on tpp.ProductID equals tp.ID
                join tcm in db.tblCurrencyMsts on tp.CurrencyID equals tcm.ID
                join tpcl in db.tblProductCategoriesLookUps on tpp.ProductID equals tpcl.ProductID
                where pids.Contains(tpcl.ProductID) && tpMarkUp.SiteID == siteId

                select new { tpp, tpMarkUp, tcm };
            if (item.Count() > 0)
            {
                priceList.AddRange(item.AsEnumerable().Select(x => new ProductPricing
                {
                    ClassId = x.tpp.ClassID,
                    ClassName = x.tpp.tblClassMst.Name,
                    ID = x.tpp.ID,
                    Price = x.tpp.Price,
                    UniqueProductID = x.tpp.UniqueProductID,
                    PassTypeCode = x.tpp.PassTypeCode,
                    TravellerId = x.tpp.TravelerID,
                    TravellerName = x.tpp.tblTravelerMst.Name,
                    TravellerValidId = x.tpp.ValidityID,
                    ProductId = x.tpp.ProductID,
                    TravellerValidName = (db.tblProductNames.Where(a => a.ProductID == x.tpp.ProductID).FirstOrDefault().Name + " " + x.tpp.tblProductValidUpTo.tblProductValidUpToNames.FirstOrDefault(z => z.LanguageID == langId).Name),
                    MarkUp = x.tpMarkUp == null ? 0 : (x.tpMarkUp.MarkUpAmount > 0 ? x.tpMarkUp.MarkUpAmount : x.tpMarkUp.MarkUpPercent),
                    IsPercent = x.tpMarkUp == null ? false : (x.tpMarkUp.MarkUpPercent > 0),
                    Currency = x.tcm.Symbol,
                    ModifyByName = GetModifyByName(x.tpMarkUp.ModifyBy == null ? new Guid() : (Guid)x.tpMarkUp.ModifyBy),
                    ModifyOn = (DateTime)x.tpMarkUp.ModifyOn
                }));
            }
            return priceList.OrderBy(x => x.TravellerName).ToList();
        }
        public List<ProductPricing> GetProductsPricesUsingCatIDMPID(Guid cid, Guid pid)
        {
            Guid langId = GetLanguageId();
            List<ProductPricing> priceList = new List<ProductPricing>();
            var item = from tpp in db.tblProductPrices
                       join tpcl in db.tblProductCategoriesLookUps on tpp.ProductID equals tpcl.ProductID
                       where tpcl.CategoryID == cid && tpcl.ProductID == pid
                       select tpp;

            if (item.Count() > 0)
            {
                priceList.AddRange(item.Select(x => new ProductPricing
                {
                    ClassName = x.tblClassMst.Name,
                    ID = x.ID,
                    Price = x.Price,
                    UniqueProductID = x.UniqueProductID,
                    PassTypeCode = x.PassTypeCode,
                    TravellerId = x.TravelerID,
                    TravellerName = x.tblTravelerMst.Name,
                    TravellerValidId = x.ValidityID,
                    ProductId = x.ProductID,
                    TravellerValidName = (db.tblProductNames.Where(a => a.ProductID == x.ProductID).FirstOrDefault().Name + " " + x.tblProductValidUpTo.tblProductValidUpToNames.FirstOrDefault(z => z.LanguageID == langId).Name),
                }));
            }
            return priceList;
        }
        public List<spGetProductPriceListBYCidandPid_Result> spGetProductPriceListBYCidandPid(Guid cid, Guid pid)
        {
            try
            {
                return db.GetProductPriceListBYCidandPid(cid, pid).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblProductMarkUp> GetAllProductMarkupWithCategoryIDnSiteID(Guid cid, Guid sid)
        {
            try
            {
                return db.tblProductMarkUps.Where(a => a.CategoryID == cid && a.SiteID == sid).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid GetCategoryById(Guid id)
        {
            var rec = db.tblProductCategoriesLookUps.FirstOrDefault(x => x.ProductID == id);
            return rec != null ? rec.CategoryID : new Guid();
        }
        public List<tblProductCountryLookUp> GetProductsCountresById(Guid id)
        {
            return db.tblProductCountryLookUps.Where(x => x.ProductID == id).ToList();
        }
        public List<tblProductPermittedLookup> GetPermittedCountresById(Guid id)
        {
            return db.tblProductPermittedLookups.Where(x => x.ProductID == id).ToList();
        }
        public List<tblSite> GetProductsSiteById(Guid id)
        {
            try
            {
                var siteListId = db.tblProductSiteLookUps.Where(x => x.ProductID == id).Select(x => x.ID).ToList();
                return db.tblSites.Where(x => siteListId.Contains(x.ID)).OrderBy(x => x.SiteURL).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblProduct GetProductById(Guid id)
        {
            try
            {
                return db.tblProducts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddProductPublish(tblProductPublish oProduct)
        {
            try
            {
                var result = db.tblProductPublishes.FirstOrDefault(x => x.ProductID == oProduct.ProductID && x.CreatedBy == oProduct.CreatedBy);
                if (result != null)
                {
                    db.tblProductPublishes.DeleteObject(result);
                    db.SaveChanges();
                }
                db.tblProductPublishes.AddObject(oProduct);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool UpdateProductPublishStatus(Guid ProductID, Guid AdminUserId)
        {
            try
            {
                var result = db.tblProductPublishes.FirstOrDefault(x => x.ProductID == ProductID && x.CreatedBy == AdminUserId);
                if (result != null)
                {
                    result.IsPublish = true;
                    result.CreatedOn = DateTime.Now;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public ProductPublishInfo LastPublishInfo(Guid ProductID)
        {
            try
            {
                var result = db.tblProducts.FirstOrDefault(x => x.ID == ProductID);
                if (result != null)
                {
                    return new ProductPublishInfo
                    {
                        LastPublishBy = result.ModifiedBy.HasValue ? db.tblAdminUsers.FirstOrDefault(x => x.ID == result.ModifiedBy.Value).Forename : db.tblAdminUsers.FirstOrDefault(x => x.ID == result.CreatedBy).Forename,
                        LastPublishDate = result.ModifiedOn.HasValue ? result.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm") : result.CreatedOn.ToString("dd/MMM/yyyy HH:mm")
                    };
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblProductPublish GetProductPublishInfo(Guid ProductID, Guid AdminUserId)
        {
            try
            {
                return db.tblProductPublishes.FirstOrDefault(x => x.ProductID == ProductID && x.CreatedBy == AdminUserId && x.IsPublish == false);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Product Markup and Commission
        public Boolean DeleteProductMarkups(Guid cid, Guid sid)
        {
            try
            {
                db.tblProductMarkUps.Where(a => a.CategoryID == cid && a.SiteID == sid).ToList().ForEach(db.tblProductMarkUps.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Product> GetProductListByCategory(Guid CatId)
        {
            try
            {
                Guid id = GetLanguageId();
                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        where tpn.LanguageID == id && tc.tblCategory.ID == CatId
                        select new Product
                        {
                            ID = tp.ID,
                            IsActive = tp.IsActive,
                            Name = tpn.Name,
                            Category = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == id).Name,
                            Currency = tp.tblCurrencyMst.Name + "(" + tp.tblCurrencyMst.Symbol + ")",
                        }).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddProductMarkup(tblProductMarkUp oMarkUp)
        {
            try
            {
                db.tblProductMarkUps.AddObject(oMarkUp);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<spShowAllMarkups_Result> GetAllMarkup(Guid SiteId)
        {
            try
            {
                List<spShowAllMarkups_Result> lst = db.spShowAllMarkups(SiteId, Guid.NewGuid()).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductMarkup(Guid CategoryID, Guid siteID)
        {
            try
            {
                // db.tblProductMarkUps.Where(a => a.CategoryID == CategoryID && a.SiteID == siteID && a.ProductID==null && a.ProductPriceID==null).ToList().ForEach(db.tblProductMarkUps.DeleteObject);
                //db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductMarkupByCatID(Guid CategoryID, Guid siteID)
        {
            try
            {
                db.tblProductMarkUps.Where(a => a.CategoryID == CategoryID && a.SiteID == siteID && a.ProductID == null && a.ProductPriceID == null).ToList().ForEach(db.tblProductMarkUps.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductMarkupByProductID(Guid Cid, Guid Pid, Guid siteID)
        {
            try
            {
                db.tblProductMarkUps.Where(a => a.CategoryID == Cid && a.SiteID == siteID && a.ProductID == Pid && a.ProductPriceID == null).ToList().ForEach(db.tblProductMarkUps.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductMarkupByProductIDHavePriceId(Guid Cid, Guid Pid, Guid siteID)
        {
            try
            {
                db.tblProductMarkUps.Where(a => a.CategoryID == Cid && a.SiteID == siteID && a.ProductID == Pid && a.ProductPriceID != null).ToList().ForEach(db.tblProductMarkUps.DeleteObject);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public spShowAllMarkups_Result GetProductMarkUpById(Guid ID)
        {
            spShowAllMarkups_Result obj = new spShowAllMarkups_Result();
            obj = db.spGetAllMarkups().Where(a => a.MarkUpId == ID).FirstOrDefault();
            return obj;
        }
        public List<GetCategoryMarkup_Result> GetAllCategoriseMarkup(Guid ID)
        {
            List<GetCategoryMarkup_Result> lst = new List<GetCategoryMarkup_Result>();
            lst = db.GetCategoryMarkup(ID).ToList();
            return lst;
        }
        public List<spGetAllMarkUp_Result> GetAllMarkupList(Guid ID)
        {
            List<spGetAllMarkUp_Result> lst = new List<spGetAllMarkUp_Result>();
            lst = db.GetAllMarkUpNew().ToList();
            return lst;
        }
        public void UpdateProductMarkUp(Guid ID, decimal MarkUpAmount, decimal MarkUpPercent)
        {
            db.spUpdateProductMarkup(ID.ToString(), MarkUpAmount, MarkUpPercent);
        }
        public int AddProductCommission(tblProductCommission oCommission)
        {
            try
            {
                List<tblProductCommission> lstCommission = new List<tblProductCommission>();
                lstCommission = db.tblProductCommissions.ToList();
                //lstCommission = lstCommission.Where(a => a.AgentId == oCommission.AgentId && a.ProductID == oCommission.ProductID && a.ProductPriceId == oCommission.ProductPriceId).ToList();
                int countExist = lstCommission.Count();
                if (countExist == 0)
                {
                    db.tblProductCommissions.AddObject(oCommission);
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<spGetAllCommission_Result1> GetAllCommission()
        {
            try
            {
                List<spGetAllCommission_Result1> lst = new List<spGetAllCommission_Result1>();
                lst = db.GetAllCommission_Result1().ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public spGetAllCommission_Result1 GetProductCommissionByID(Guid CategoryId)
        {
            try
            {
                spGetAllCommission_Result1 lst = new spGetAllCommission_Result1();
                lst = db.GetAllCommission_Result1().Where(a => a.ID == CategoryId).FirstOrDefault();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteProductCommission(Guid CommissionID)
        {
            try
            {
                db.tblProductCommissions.Where(a => a.ID == CommissionID).ToList().ForEach(db.tblProductCommissions.DeleteObject);
                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateProductCommission(Guid ID, decimal Percentage)
        {
            try
            {
                db.UpdateProductCommission(ID, Percentage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region New Commission Section
        public List<ProductCategory> GetAssociateCategoryListWithOutSiteId(int treeLevel, Guid OfficeID)
        {
            try
            {
                Guid id = GetLanguageId();
                List<ProductCategory> plistOne = new List<ProductCategory>();
                List<ProductCategory> listTwo = new List<ProductCategory>();
                List<ProductCategory> listThree = new List<ProductCategory>();


                plistOne.AddRange((from tcn in db.tblCategoriesNames
                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                   where tc.TreeLevel == 1 && tcn.LanguageID == id
                                   select new ProductCategory
                                   {
                                       ID = tc.ID,
                                       Name = tcn.Name,
                                       TreeLevel = 1,
                                       IsActive = tc.IsActive,
                                       IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                       Commission = (from fd in db.tblCategoryCommissions where fd.CategoryID == tcn.CategoryID && fd.OfficeID == OfficeID select fd.Commission).FirstOrDefault()
                                   }).ToList());


                foreach (var item in plistOne)
                {
                    List<ProductCategory> oList = (from tcn in db.tblCategoriesNames
                                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID

                                                   where tc.TreeLevel == 2 && tcn.LanguageID == id && tc.ParentID == item.ID
                                                   select new ProductCategory
                                                   {
                                                       Name = item.Name + " -> " + tcn.Name,
                                                       ID = tc.ID,
                                                       TreeLevel = 2,
                                                       IsActive = tc.IsActive,
                                                       IsPrintQueueEnable = tc.IsPrintQueueEnable,
                                                       Commission = (from fd in db.tblCategoryCommissions where fd.CategoryID == tcn.CategoryID && fd.OfficeID == OfficeID select fd.Commission).FirstOrDefault()

                                                   }).ToList();

                    listTwo.AddRange(oList);
                }

                foreach (var item in listTwo)
                {
                    List<ProductCategory> oList = (from tcn in db.tblCategoriesNames
                                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID

                                                   where tc.TreeLevel == 3 && tcn.LanguageID == id && tc.ParentID == item.ID
                                                   select new ProductCategory
                                                   {
                                                       Name = item.Name + " --> " + tcn.Name,
                                                       ID = tc.ID,
                                                       TreeLevel = 3,
                                                       IsActive = tc.IsActive,
                                                       IsPrintQueueEnable = tc.IsPrintQueueEnable
                                                       ,
                                                       Commission = (from fd in db.tblCategoryCommissions where fd.CategoryID == tcn.CategoryID && fd.OfficeID == OfficeID select fd.Commission).FirstOrDefault()

                                                   }).ToList();

                    listThree.AddRange(oList);
                }

                List<ProductCategory> list = new List<ProductCategory>();
                switch (treeLevel)
                {
                    case 1:
                        list = plistOne.OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                    case 2:
                        list = plistOne.Concat(listTwo).OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                    case 3:
                        list = plistOne.Concat(listTwo).Concat(listThree).OrderBy(x => x.TreeLevel).ThenBy(x => x.Name).ToList();
                        break;
                }
                //if (siteId != new Guid())
                //    list = (from lst in list join x in db.tblCategorySiteLookUps on lst.ID equals x.CategoryID where x.SiteID == siteId select lst).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean CategoryHaveCommission(Guid catId, Guid officeId)
        {
            Boolean rval;
            rval = db.CheckIsDiffCommission(officeId, catId).FirstOrDefault().IsDifferentCommission.HasValue ? Convert.ToBoolean(db.CheckIsDiffCommission(officeId, catId).FirstOrDefault().IsDifferentCommission.Value) : false;
            return rval;
        }
        public Boolean CategoryHaveCommissionForProduct(Guid catId, Guid ProductId, Guid officeId)
        {
            Boolean rval;
            rval = db.IsDiffCommForProduct(officeId, ProductId, catId).FirstOrDefault().IsDifferentCommission.HasValue ? Convert.ToBoolean(db.IsDiffCommForProduct(officeId, ProductId, catId).FirstOrDefault().IsDifferentCommission.Value) : false;
            return rval;
        }
        public Boolean CheckDiffMarkUpForCategory(Guid catId, Guid siteid)
        {
            Boolean rval;
            rval = db.CheckDiffMarkUp(siteid, catId).FirstOrDefault().IsDifferentMarkUp.HasValue ? Convert.ToBoolean(db.CheckDiffMarkUp(siteid, catId).FirstOrDefault().IsDifferentMarkUp.Value) : false;
            return rval;
        }
        public Boolean CheckDiffMarkUpForProduct(Guid catId, Guid Pid, Guid siteid)
        {
            Boolean rval;
            rval = db.CheckDiffProductMarkUp(siteid, Pid, catId).FirstOrDefault().IsDifferentMarkUp.HasValue ? Convert.ToBoolean(db.CheckDiffProductMarkUp(siteid, Pid, catId).FirstOrDefault().IsDifferentMarkUp.Value) : false;
            return rval;
        }
        public List<Product> GetProductListUsingCategoryId(List<Guid> listIds, Guid OfficeID)
        {
            try
            {
                Guid id = GetLanguageId();
                string bidcream = string.Empty;
                List<Guid> listCatId = new List<Guid>();
                if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                {
                    listCatId.AddRange(listIds);
                    listIds = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                    listCatId.AddRange(listIds);
                    if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                    {
                        listCatId = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                        listCatId.AddRange(listIds);
                    }
                }
                else
                    listCatId = listIds;

                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        where tpn.LanguageID == id
                        && listCatId.Contains(tc.CategoryID)
                        select new Product
                        {
                            ID = tp.ID,
                            IsActive = tp.IsActive,
                            Name = tpn.Name,
                            Category = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == id).Name,
                            Currency = tp.tblCurrencyMst.Name + "(" + tp.tblCurrencyMst.Symbol + ")",
                            IsShippingAplicable = tp.IsShippingAplicable,
                            IsTopRailPass = tp.IsTopRailPass,
                            CategoryID = tc.CategoryID,
                            Commission = (from fd in db.tblProductCommissions where fd.CategoryID == tc.CategoryID && fd.ProductID == tpn.ProductID && fd.OfficeID == OfficeID select fd.Commission).FirstOrDefault()
                        }).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ProductPricing> GetProductsPricesWithProductNameByProductId(List<Guid> pids, Guid CatID, Guid OfficeID)
        {
            Guid langId = GetLanguageId();
            List<ProductPricing> priceList = new List<ProductPricing>();

            var item =
                from tpp in db.tblProductPrices
                join tp in db.tblProducts on tpp.ProductID equals tp.ID
                join tcm in db.tblCurrencyMsts on tp.CurrencyID equals tcm.ID
                join tpcl in db.tblProductCategoriesLookUps on tpp.ProductID equals tpcl.ProductID
                where pids.Contains(tpcl.ProductID)
                select new { tpp, tcm, tpcl };
            if (item.Count() > 0)
            {
                priceList.AddRange(item.Select(x => new ProductPricing
                {
                    ClassId = x.tpp.ClassID,
                    ClassName = x.tpp.tblClassMst.Name,
                    ID = x.tpp.ID,
                    Price = x.tpp.Price,
                    UniqueProductID = x.tpp.UniqueProductID,
                    PassTypeCode = x.tpp.PassTypeCode,
                    TravellerId = x.tpp.TravelerID,
                    TravellerName = x.tpp.tblTravelerMst.Name,
                    TravellerValidId = x.tpp.ValidityID,
                    ProductId = x.tpp.ProductID,
                    TravellerValidName = (db.tblProductNames.Where(a => a.ProductID == x.tpp.ProductID).FirstOrDefault().Name + " " + x.tpp.tblProductValidUpTo.tblProductValidUpToNames.FirstOrDefault(z => z.LanguageID == langId).Name),
                    Currency = x.tcm.Symbol,
                    CategoryId = x.tpcl.CategoryID,
                    Commission = (from fd in db.tblPriceCommissions where fd.CategoryID == x.tpcl.CategoryID && fd.ProductID == x.tpp.ProductID && fd.OfficeID == OfficeID && fd.ProductPriceID == x.tpp.ID select fd.Commission).FirstOrDefault()
                }));
            }
            return priceList.OrderBy(x => x.TravellerName).ToList();
        }
        public void ManageCategoryCommission(Guid CategoryID, Guid OfficeId, Decimal Commission)
        {
            List<tblBranch> lstBranch = db.tblBranches.Where(a => a.ParentBranchID == OfficeId).ToList();
            foreach (tblBranch Branch in lstBranch)
            {
                ManageCategoryCommission(CategoryID, Branch.ID, Commission);
            }

            db.AddCategoryLevelCommission(OfficeId, CategoryID, Commission);

            //List<tblCategory> lstCategory = db.tblCategories.Where(a => a.ParentID == CategoryID).ToList();
            //foreach (tblCategory tb in lstCategory)
            //{
            //    ManageCategoryCommission(tb.ID,OfficeId,Commission);
            //}
            //db.tblCategoryCommissions.Where(x => x.CategoryID == CategoryID && x.OfficeID==OfficeId).ToList().ForEach(db.tblCategoryCommissions.DeleteObject);
            //tblCategoryCommission objCatCommission = new tblCategoryCommission();
            //objCatCommission.ID = Guid.NewGuid();
            //objCatCommission.Commission = Commission;
            //objCatCommission.CategoryID = CategoryID;
            //objCatCommission.OfficeID = OfficeId;
            //db.tblCategoryCommissions.AddObject(objCatCommission);
            //db.SaveChanges();

            //var lstProduct = (from a in db.tblCategories
            //                  join b in db.tblProductCategoriesLookUps on a.ID equals b.CategoryID
            //                  where a.ID == CategoryID
            //                  select b).ToList();
            //if (lstProduct.Count() > 0)
            //{
            //    foreach (var p in lstProduct)
            //    {
            //        ManageProductCommission(CategoryID, p.ProductID, OfficeId, Commission);
            //    }
            //}
        }
        public void ManageProductCommission(Guid CategoryID, Guid ProductId, Guid OfficeId, Decimal Commission)
        {

            List<tblBranch> lstBranch = db.tblBranches.Where(a => a.ParentBranchID == OfficeId).ToList();
            foreach (tblBranch Branch in lstBranch)
            {
                ManageProductCommission(CategoryID, ProductId, Branch.ID, Commission);
            }

            db.AddProductLevelCommission(OfficeId, ProductId, CategoryID, Commission);

            //db.tblProductCommissions.Where(a => a.CategoryID == CategoryID && a.OfficeID == OfficeId && a.ProductID == ProductId).ToList().ForEach(db.tblProductCommissions.DeleteObject);
            //tblProductCommission objProductCommission = new tblProductCommission();
            //objProductCommission.ID = Guid.NewGuid();
            //objProductCommission.Commission = Commission;
            //objProductCommission.CategoryID = CategoryID;
            //objProductCommission.OfficeID = OfficeId;
            //objProductCommission.ProductID = ProductId;
            //db.tblProductCommissions.AddObject(objProductCommission);
            //db.SaveChanges();

            //var lstPrice = (from a in db.tblProductPrices
            //                  where a.ProductID==ProductId
            //                  select a).ToList();
            //if (lstPrice.Count() > 0)
            //{
            //    foreach (var p in lstPrice)
            //    {
            //        ManagePriceCommission(CategoryID, ProductId, p.ID, OfficeId, Commission);
            //    }
            //}
        }
        public void ManagePriceCommission(Guid CategoryID, Guid ProductId, Guid PriceId, Guid OfficeId, Decimal Commission)
        {
            List<tblBranch> lstBranch = db.tblBranches.Where(a => a.ParentBranchID == OfficeId).ToList();
            foreach (tblBranch Branch in lstBranch)
            {
                ManagePriceCommission(CategoryID, ProductId, PriceId, Branch.ID, Commission);
            }

            db.tblPriceCommissions.Where(a => a.CategoryID == CategoryID && a.OfficeID == OfficeId && a.ProductID == ProductId && a.ProductPriceID == PriceId).ToList().ForEach(db.tblPriceCommissions.DeleteObject);
            tblPriceCommission objCatCommission = new tblPriceCommission();
            objCatCommission.ID = Guid.NewGuid();
            objCatCommission.Commission = Commission;
            objCatCommission.CategoryID = CategoryID;
            objCatCommission.OfficeID = OfficeId;
            objCatCommission.ProductID = ProductId;
            objCatCommission.ProductPriceID = PriceId;
            db.tblPriceCommissions.AddObject(objCatCommission);
            db.SaveChanges();

        }
        #endregion

        #region Admin User Upload
        public int UploadUsersFromExcel(string sPath, Guid? CreatedBy, string fileExt)
        {
            try
            {
                string ConStringOLEDB = string.Empty;
                //if (fileExt.ToUpper() == ".XLS")
                //    ConStringOLEDB = "Provider=Microsoft.Jet.OLEDB.4.0;Data source='" + sPath + "';Extended Properties=Excel 8.0;";
                //else
                ConStringOLEDB = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + sPath + "';Extended Properties=Excel 12.0;";

                OleDbConnection MyConnection = null;

                object missing = System.Reflection.Missing.Value;
                MyConnection = new OleDbConnection(ConStringOLEDB);
                MyConnection.Open();

                DataTable objDT = new DataTable();
                objDT = MyConnection.GetSchema("Tables");
                DataRow dr = objDT.Rows[0];

                for (int j = 0; j < objDT.Rows.Count; j++)
                {
                    string strWorksheetName = objDT.Rows[j]["TABLE_NAME"].ToString(); //"Shee1$";
                    OleDbDataAdapter objDataAdpter = null;
                    try
                    {
                        objDataAdpter = new OleDbDataAdapter("select * from [" + strWorksheetName + "]  ", MyConnection);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    DataSet objDS = new DataSet();
                    objDataAdpter.Fill(objDS);
                    MyConnection.Close();

                    for (int i = 0; i < objDS.Tables.Count; i++)
                    {
                        foreach (DataRow item in objDS.Tables[i].Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item[0])))
                            {
                                tblAdminUser obj = new tblAdminUser();
                                obj.ID = Guid.NewGuid();
                                obj.Salutation = 0;
                                obj.IsActive = true;
                                obj.IsAgent = true;
                                obj.IsDeleted = false;
                                obj.CreatedOn = DateTime.Now;
                                obj.CreatedBy = AdminuserInfo.UserID;
                                obj.UserName = Convert.ToString(item[0]).Trim();
                                obj.Password = string.Empty;
                                obj.EmailAddress = Convert.ToString(item[2]).Trim();
                                obj.Note = Convert.ToString(item[3]).Trim();
                                bool exist = db.tblAdminUsers.Any(t => t.UserName == obj.UserName && t.EmailAddress == obj.EmailAddress && t.Note == obj.Note);
                                if (!exist)
                                {
                                    bool existusername = db.tblAdminUsers.Any(t => t.UserName == obj.UserName);
                                    if (!existusername)
                                    {
                                        db.tblAdminUsers.AddObject(obj);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
        public int GetSalutationPosition(string Salutation)
        {
            try
            {
                switch (Salutation)
                {
                    case "Mr":
                        return 1;


                    case "Miss":
                        return 2;


                    case "Mrs":
                        return 3;


                    case "Ms":
                        return 4;

                    default:
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region upload product
        public void UpdateProductUrl()
        {
            try
            {
                var data = db.tblRouteTables.FirstOrDefault();
                if (data != null)
                    data.IsUpdate = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UploadProductFromExcel(string sPath, Guid? CreatedBy, string fileExt)
        {
            try
            {
                string ConStringOLEDB = string.Empty;
                ConStringOLEDB = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + sPath + "';Extended Properties=Excel 12.0;";
                OleDbConnection MyConnection = null;

                object missing = System.Reflection.Missing.Value;
                MyConnection = new OleDbConnection(ConStringOLEDB);
                MyConnection.Open();

                DataTable objDT = new DataTable();
                objDT = MyConnection.GetSchema("Tables");

                /*Remove old tempProduct data*/
                db.tempProducts.ToList().ForEach(db.tempProducts.DeleteObject);
                db.SaveChanges();

                for (int j = 0; j < objDT.Rows.Count; j++)
                {
                    string strWorksheetName = objDT.Rows[j]["TABLE_NAME"].ToString(); //"Shee1$";
                    OleDbDataAdapter objDataAdpter = null;
                    try
                    {
                        objDataAdpter = new OleDbDataAdapter("select * from [" + strWorksheetName + "]  ", MyConnection);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    DataSet objDS = new DataSet();
                    objDataAdpter.Fill(objDS);
                    MyConnection.Close();

                    for (int i = 0; i < objDS.Tables.Count; i++)
                    {
                        foreach (DataRow item in objDS.Tables[i].Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item[1])) && !strWorksheetName.Contains("FilterDatabase"))
                            {
                                tempProduct oProduct = new tempProduct
                                {
                                    SupplierName = Convert.ToString(item[0]),
                                    SupplierCategory = Convert.ToString(item[1]),
                                    ParentCategoryName = (!string.IsNullOrEmpty(Convert.ToString(item[2])) ? Convert.ToString(item[2]) : "INTERNATIONALRAIL"),
                                    CategoryName = Convert.ToString(item[3]),
                                    ProductName = Convert.ToString(item[4]),
                                    ProductCode = Convert.ToString(item[5]),
                                    Day = Convert.ToString(item[6]),
                                    Month = Convert.ToString(item[7]),
                                    Year = Convert.ToString(item[8]),
                                    Validity = Convert.ToString(item[9]),
                                    ValidityNote = Convert.ToString(item[10]),
                                    RegionalCountryCode = Convert.ToString(item[11]),
                                    StartCountryCode = Convert.ToString(item[12]),
                                    EndCountryCode = Convert.ToString(item[13]),
                                    Traveller = Convert.ToString(item[14]),
                                    Note = Convert.ToString(item[15]),
                                    TravellerFromAge = Convert.ToString(item[16]),
                                    TravellerToAge = Convert.ToString(item[17]),
                                    Class = Convert.ToString(item[18]),
                                    ClassCode = Convert.ToString(item[19]),
                                    Currency = Convert.ToString(item[20]),
                                    Price = Convert.ToString(item[21]),
                                    IsShippingAplicable = Convert.ToString(item[22]),
                                    PromoPassText = Convert.ToString(item[23]),
                                    AdvancePurchaseMonth = Convert.ToString(item[24]),
                                    IsPassportRequired = Convert.ToString(item[25]),
                                    ProductAvilTravelFrom = Convert.ToString(item[26]),
                                    ProductAvilTravelTo = Convert.ToString(item[27]),
                                    ProductAvilPurchaseFrom = Convert.ToString(item[28]),
                                    ProductAvilPurchaseTo = Convert.ToString(item[29]),
                                    IsTwinPass = Convert.ToString(item[30]),
                                    IsActive = Convert.ToString(item[31]),
                                    PriceBand = Convert.ToString(item[32]),
                                    IsAllowAdminPass = Convert.ToString(item[33]),
                                    IsAllowMiddleName = Convert.ToString(item[34]),
                                    UserFriendlyName = string.Empty
                                };
                                db.tempProducts.AddObject(oProduct);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                int res = db.ImportProducts(CreatedBy).FirstOrDefault().Value;
                return 1;

            }
            catch (Exception ex)
            {
                db.tempProducts.ToList().ForEach(db.tempProducts.DeleteObject);
                db.SaveChanges();
                throw ex;
            }
        }
        #endregion

        #region Update MarkUp
        public int UpdateMarkup(Guid id, Decimal MarkUp, bool isPercent, Guid siteID, int flag, Guid adminid)
        {
            try
            {
                var result = db.AssignMarkup(id, MarkUp, isPercent, siteID, flag, adminid);
                db.SaveChanges();
                return result.FirstOrDefault().Value;
            }
            catch (Exception ex)

            { throw ex; }
        }
        #endregion

        #region DownloadProduct
        public List<GetProductList_Result> GetProductList(string id)
        {
            try
            {
                return db.GetProductList(id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GetCategories_Result> GetCategories()
        {
            try
            {
                return db.GetCategories().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateProductList(tblProductPrice prd)
        {
            try
            {
                var _prd = db.tblProductPrices.FirstOrDefault(x => x.ID == prd.ID);
                if (_prd != null)
                {
                    _prd.Price = prd.Price;
                };

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateProductFromExcel(string sPath, Guid? CreatedBy, string fileExt)
        {
            try
            {
                string conStringOledb;

                if (fileExt.ToUpper() == ".XLS")
                    //conStringOledb = "Provider=Microsoft.Jet.OLEDB.4.0;Data source='" + sPath + "';Extended Properties=Excel 8.0;";
                    conStringOledb = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + sPath + "';Extended Properties=Excel 8.0;HDR=YES;IMEX=1;";
                else
                    conStringOledb = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + sPath + "';Extended Properties=Excel 12.0;";

                var myConnection = new OleDbConnection(conStringOledb);
                myConnection.Open();

                var objDt = myConnection.GetSchema("Tables");
                for (int j = 0; j < objDt.Rows.Count; j++)
                {
                    var strWorksheetName = objDt.Rows[j]["TABLE_NAME"].ToString();
                    OleDbDataAdapter objDataAdpter;
                    try
                    {
                        objDataAdpter = new OleDbDataAdapter("select * from [" + strWorksheetName + "]  ", myConnection);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    var objDs = new DataSet();
                    objDataAdpter.Fill(objDs);
                    myConnection.Close();

                    for (int i = 0; i < objDs.Tables.Count; i++)
                    {
                        foreach (DataRow item in objDs.Tables[i].Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item[0])) && !string.IsNullOrEmpty(Convert.ToString(item[6])))
                            {
                                var oProduct = new tblProductPrice
                                {
                                    ID = Guid.Parse((string)item[0]),
                                    Price = Convert.ToDecimal(item[6]),
                                };
                                UpdateProductList(oProduct);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                int res = db.ImportProducts(CreatedBy).FirstOrDefault().Value;
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public class ProductUpdatelist
        {
            public Guid ID { get; set; }
            public decimal Price { get; set; }
        }
        #endregion


        public Boolean UpdateOrderCommition(Guid? siteId, Guid? AgentUserID, long? OrderNo)
        {
            try
            {
                db.UpdateProductCommition(siteId, AgentUserID, OrderNo);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean UpdateOrderCommitionPublic(Guid? siteId, long? OrderNo)
        {
            try
            {
                var data = (from a in db.tblBranchLookupSites
                            join b in db.tblBranches on a.BranchID equals b.ID
                            where a.SiteID == siteId && b.IsVirtualOffice == true
                            select new { Branch = b.ID }).ToList();
                if (data != null && data.Count > 0)
                    db.UpdateProductCommitionPublic(siteId, data.First().Branch, OrderNo);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FieldEurailCountry> RegionalCountryCode()
        {
            return db.tblEurailCountries.Where(x => x.IsActive && !string.IsNullOrEmpty(x.Country1) && !string.IsNullOrEmpty(x.Country2) && string.IsNullOrEmpty(x.Country3) && string.IsNullOrEmpty(x.Country4) && string.IsNullOrEmpty(x.Country5)).Select(x => new FieldEurailCountry
            {
                CountryCode = (int)x.CountryCode,
                Name = x.Country1 + " - " + x.Country2
            }).OrderBy(x => x.Name).ToList();
        }
        public List<FieldEurailCountry> OneCountryCode()
        {
            return db.tblEurailCountries.Where(x => x.IsActive && x.CountryCode > 999 && x.CountryCode < 2000).Select(x => new FieldEurailCountry
            {
                CountryCode = (int)x.CountryCode,
                Name = x.Country1
            }).OrderBy(x => x.Name).ToList();
        }
        public string GetProductName(Guid id)
        {
            try
            {
                var data = db.tblProductNames.FirstOrDefault(t => t.ProductID == id);
                return data.Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetMonthValidity(Guid id)
        {
            try
            {
                int monthVal = 3;
                var data = db.tblProducts.FirstOrDefault(t => t.ID == id);
                if (data != null)
                {
                    monthVal = Convert.ToInt32(data.MonthValidity);
                }
                return monthVal.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetTravellerName(Guid id)
        {
            try
            {
                var data = db.tblTravelerMsts.FirstOrDefault(t => t.ID == id);
                return data.Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region ActiveProduct
        public bool IsActiveCategory(Guid catID)
        {
            try
            {
                var cat = db.tblCategories.FirstOrDefault(x => x.ID == catID);
                return cat != null && cat.IsActive;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool IsActiveProduct(Guid prdID)
        {
            try
            {
                var prd = db.tblProducts.FirstOrDefault(x => x.ID == prdID);
                return prd != null && prd.IsActive;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool IsActivePrdCategory(Guid prdID)
        {
            try
            {
                var category = from lkp in db.tblProductCategoriesLookUps
                               join cat in db.tblCategories on lkp.CategoryID equals cat.ID
                               where lkp.ProductID == prdID
                               select cat;

                if (category.FirstOrDefault() != null)
                {
                    var cat = category.FirstOrDefault();
                    return cat != null && cat.IsActive;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ProductSiteCategory
        public bool siteHasCat(Guid catID, Guid siteID)
        {
            try
            {
                var cat = db.tblCategorySiteLookUps.FirstOrDefault(x => x.CategoryID == catID && x.SiteID == siteID);
                if (cat != null)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool siteHasPrd(Guid prdID, Guid siteID)
        {
            try
            {
                var prd = db.tblProductSiteLookUps.FirstOrDefault(x => x.ProductID == prdID && x.SiteID == siteID);
                if (prd != null)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region IsProductFoc-AD75
        public bool PrdIsFoc(Guid prdID)
        {
            try
            {
                var prd = from lkp in db.tblProductCategoriesLookUps
                          join cat in db.tblCategories on lkp.CategoryID equals cat.ID
                          where lkp.ProductID == prdID
                          select cat;

                if (prd.FirstOrDefault() != null)
                {
                    var cat = prd.FirstOrDefault();
                    return cat != null && (bool)cat.IsFOCAD75;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Product Biooking Fee
        public List<ProductBookingFee> GetCategoryBookingFeeList(int treeLevel, Guid siteId)
        {
            try
            {
                Guid id = GetLanguageId();
                List<ProductBookingFee> plistOne = new List<ProductBookingFee>();
                List<ProductBookingFee> listTwo = new List<ProductBookingFee>();
                List<ProductBookingFee> listThree = new List<ProductBookingFee>();

                plistOne.AddRange((from tcn in db.tblCategoriesNames
                                   join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                   join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                   join dis in db.tblBookingFeeCategories on tcn.CategoryID equals dis.CategoryId into mkp
                                   from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                                   where tc.TreeLevel == 1 && tcn.LanguageID == id && tcLookUp.SiteID == siteId
                                   select new ProductBookingFee
                                   {
                                       CategoryName = tcn.Name,
                                       CategoryId = tc.ID,
                                       Price = ndis.Price == null ? (decimal)0.0 : ndis.Price,
                                       ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                                       ModifyOn = ndis.ModifyOn,
                                       BookingFromDate = ndis.BookingFromDate.Value,
                                       BookingToDate = ndis.BookingToDate.Value,
                                       AmountFrom = ndis.AmountFrom == null ? (decimal)0.0 : ndis.AmountFrom,
                                       AmountTo = ndis.AmountTo == null ? (decimal)0.0 : ndis.AmountTo
                                   }).AsEnumerable().Select(x => new ProductBookingFee
                                   {
                                       CategoryName = x.CategoryName,
                                       CategoryId = x.CategoryId,
                                       Price = x.Price,
                                       ModifyName = GetModifyByName(x.ModifyBy),
                                       ModifyOn = x.ModifyOn,
                                       BookingFromDate = x.BookingFromDate,
                                       BookingToDate = x.BookingToDate,
                                       AmountFrom = x.AmountFrom,
                                       AmountTo = x.AmountTo
                                   }).Distinct().ToList());

                foreach (var item in plistOne)
                {
                    List<ProductBookingFee> oList = (from tcn in db.tblCategoriesNames
                                                     join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                                     join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                                     join dis in db.tblBookingFeeCategories on tcn.CategoryID equals dis.CategoryId into mkp
                                                     from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                                                     where tc.TreeLevel == 2 && tcn.LanguageID == id && tc.ParentID == item.CategoryId && tcLookUp.SiteID == siteId
                                                     select new ProductBookingFee
                                                  {
                                                      CategoryName = item.CategoryName + " -> " + tcn.Name,
                                                      CategoryId = tc.ID,
                                                      Price = ndis.Price == null ? (decimal)0.0 : ndis.Price,
                                                      ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                                                      ModifyOn = ndis.ModifyOn,
                                                      BookingFromDate = ndis.BookingFromDate.Value,
                                                      BookingToDate = ndis.BookingToDate.Value,
                                                      AmountFrom = ndis.AmountFrom == null ? (decimal)0.0 : ndis.AmountFrom,
                                                      AmountTo = ndis.AmountTo == null ? (decimal)0.0 : ndis.AmountTo
                                                  }).AsEnumerable().Select(x => new ProductBookingFee
                                                  {
                                                      CategoryName = x.CategoryName,
                                                      CategoryId = x.CategoryId,
                                                      Price = x.Price,
                                                      ModifyName = GetModifyByName(x.ModifyBy),
                                                      ModifyOn = x.ModifyOn,
                                                      BookingFromDate = x.BookingFromDate,
                                                      BookingToDate = x.BookingToDate,
                                                      AmountFrom = x.AmountFrom,
                                                      AmountTo = x.AmountTo
                                                  }).Distinct().ToList();
                    listTwo.AddRange(oList);
                }

                foreach (var item in listTwo)
                {
                    List<ProductBookingFee> oList = (from tcn in db.tblCategoriesNames
                                                     join tc in db.tblCategories on tcn.CategoryID equals tc.ID
                                                     join tcLookUp in db.tblCategorySiteLookUps on tcn.CategoryID equals tcLookUp.CategoryID
                                                     join dis in db.tblBookingFeeCategories on tcn.CategoryID equals dis.CategoryId into mkp
                                                     from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                                                     where tc.TreeLevel == 3 && tcn.LanguageID == id && tc.ParentID == item.CategoryId && tcLookUp.SiteID == siteId
                                                     select new ProductBookingFee
                                                  {
                                                      CategoryName = item.CategoryName + " -> " + tcn.Name,
                                                      CategoryId = tc.ID,
                                                      Price = ndis.Price == null ? (decimal)0.0 : ndis.Price,
                                                      ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                                                      ModifyOn = ndis.ModifyOn,
                                                      BookingFromDate = ndis.BookingFromDate.Value,
                                                      BookingToDate = ndis.BookingToDate.Value,
                                                      AmountFrom = ndis.AmountFrom == null ? (decimal)0.0 : ndis.AmountFrom,
                                                      AmountTo = ndis.AmountTo == null ? (decimal)0.0 : ndis.AmountTo
                                                  }).AsEnumerable().Select(x => new ProductBookingFee
                                                  {
                                                      CategoryName = x.CategoryName,
                                                      CategoryId = x.CategoryId,
                                                      Price = x.Price,
                                                      ModifyName = GetModifyByName(x.ModifyBy),
                                                      ModifyOn = x.ModifyOn,
                                                      BookingFromDate = x.BookingFromDate,
                                                      BookingToDate = x.BookingToDate,
                                                      AmountFrom = x.AmountFrom,
                                                      AmountTo = x.AmountTo
                                                  }).Distinct().ToList();

                    listThree.AddRange(oList);
                }
                List<ProductBookingFee> list = new List<ProductBookingFee>();
                list = plistOne.Concat(listTwo).Concat(listThree).OrderBy(x => x.CategoryName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductBookingFee> GetProductBookingFeeList(Guid siteId, List<Guid> listIds)
        {
            try
            {
                Guid id = GetLanguageId();
                string bidcream = string.Empty;
                List<Guid> listCatId = new List<Guid>();
                if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                {
                    listCatId.AddRange(listIds);
                    listIds = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                    listCatId.AddRange(listIds);
                    if (db.tblCategories.Any(x => listIds.Contains(x.ParentID)))
                    {
                        listCatId = db.tblCategories.Where(x => listIds.Contains(x.ParentID)).Select(y => y.ID).ToList();
                        listCatId.AddRange(listIds);
                    }
                }
                else
                    listCatId = listIds;

                return (from tpn in db.tblProductNames
                        join tp in db.tblProducts on tpn.ProductID equals tp.ID
                        join tplookUp in db.tblProductSiteLookUps on tpn.ProductID equals tplookUp.ProductID
                        join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                        join dis in db.tblBookingFeeProducts on tc.ProductID equals dis.ProductId into mkp
                        from ndis in mkp.Where(t => t.SiteId == siteId).DefaultIfEmpty()
                        where tpn.LanguageID == id && tc.tblCategory.tblCategorySiteLookUps.Any(x => x.SiteID == siteId) && tplookUp.SiteID == siteId
                        && listCatId.Contains(tc.CategoryID)
                        select new ProductBookingFee
                        {
                            ProductId = tp.ID,
                            ProductName = tpn.Name,
                            CategoryId = tc.CategoryID,
                            CategoryName = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == id).Name,
                            Price = ndis.Price == null ? (decimal)0.0 : ndis.Price,
                            ModifyBy = ndis.ModifyBy == null ? new Guid() : (Guid)ndis.ModifyBy,
                            ModifyOn = ndis.ModifyOn,
                            BookingFromDate = ndis.BookingFromDate.Value,
                            BookingToDate = ndis.BookingToDate.Value,
                            AmountFrom = ndis.AmountFrom == null ? (decimal)0.0 : ndis.AmountFrom,
                            AmountTo = ndis.AmountTo == null ? (decimal)0.0 : ndis.AmountTo
                        }).OrderBy(x => x.ProductName).AsEnumerable().Select(x => new ProductBookingFee
                        {
                            ProductId = x.ProductId,
                            ProductName = x.ProductName,
                            CategoryId = x.CategoryId,
                            CategoryName = x.CategoryName,
                            Price = x.Price,
                            ModifyOn = x.ModifyOn,
                            ModifyName = GetModifyByName(x.ModifyBy),
                            BookingFromDate = x.BookingFromDate,
                            BookingToDate = x.BookingToDate,
                            AmountFrom = x.AmountFrom,
                            AmountTo = x.AmountTo
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateProductBookingFee(Guid ID, Guid SiteID, Int32 Flag, Guid AdminID, DateTime? BookingFromDate, DateTime? BookingToDate, Decimal Price, Decimal AmountFrom, Decimal AmountTo)
        {
            try
            {
                var result = db.BookingFeeAssign(ID, SiteID, Flag, AdminID, BookingFromDate, BookingToDate, Price, AmountFrom, AmountTo);
                return result.FirstOrDefault().Value;
            }
            catch (Exception ex)

            { throw ex; }
        }
        #endregion
    }
    public class ProductDownloadList
    {
        public Guid ID { set; get; }
        public string Category { set; get; }
        public string ProductName { set; get; }
        public string Class { set; get; }
        public string Traveller { set; get; }
        public string Price { set; get; }
    }
    public class CategorySiteList
    {
        public Guid Id { get; set; }
        public string Name { set; get; }
        public string SiteName { set; get; }
        public bool IsActive { get; set; }
    }
    public class CatParentId
    {
        public Guid Id { get; set; }
    }
    public class RepeaterListItem
    {
        public Guid Id { get; set; }
        public int NoOfItems { get; set; }
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public bool IsMissing { get; set; }
    }
    public class ProductCategory
    {
        public Guid ID { get; set; }
        public Guid ParentId { get; set; }
        public int TreeLevel { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrintQueueEnable { get; set; }
        public bool IsAD75 { get; set; }
        public bool IsPercent { get; set; }
        public Decimal? MarkUp { get; set; }
        public Decimal? Commission { get; set; }
        public Boolean HaveDifferentComm { get; set; }
        public Boolean IsBritrail { get; set; }
        public Boolean IsInterRail { get; set; }
        public DateTime ModifyOn { get; set; }
        public Guid ModifyBy { get; set; }
        public string ModifyByName { get; set; }
    }
    public class Product
    {
        public int Sortorder { get; set; }
        public Guid ID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid SiteID { get; set; }
        public int CircleImgType { get; set; }
        public string CircleImg { get; set; }
        public string Name { get; set; }
        public bool IsShippingAplicable { get; set; }
        public string Currency { get; set; }
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public bool IsTopRailPass { get; set; }
        public bool? IsItxPass { get; set; }
        public bool? IsTwinPass { get; set; }
        public bool? IsPromoPass { get; set; }
        public bool? IsPassportValid { get; set; }

        public string ProductImage { get; set; }
        public string ProductDescription { get; set; }
        public bool IsPercent { get; set; }

        public Decimal? MarkUp { get; set; }
        public Decimal? Commission { get; set; }
        public Guid CategoryId { get; set; }

        public string ProductSEOName { get; set; }

        public DateTime ModifyOn { get; set; }
        public Guid ModifyBy { get; set; }
        public string ModifyByName { get; set; }
        public bool IsSpecialandBest { get; set; }
        public string ImgSpecialandBest { get; set; }
    }
    public class ProductPricing
    {
        public Guid ID { get; set; }
        public Guid TravellerValidId { get; set; }
        public string TravellerValidName { get; set; }
        public Guid ProductId { get; set; }
        public Guid ClassId { get; set; }
        public string ClassName { get; set; }
        public Guid TravellerId { get; set; }
        public string TravellerName { get; set; }
        public decimal Price { get; set; }
        public string UniqueProductID { get; set; }
        public int PassTypeCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsPercent { get; set; }
        public Decimal? MarkUp { get; set; }
        public string Currency { get; set; }
        public Decimal? Commission { get; set; }
        public string Note { get; set; }
        public Guid CategoryId { get; set; }
        public string ValidityNote { get; set; }
        public int? PriceBand { get; set; }
        public string PriceBandName { get; set; }
        public DateTime? ModifyOn { get; set; }
        public Guid ModifyBy { get; set; }
        public string ModifyByName { get; set; }
        public decimal DiscountPrice { get; set; }
        public string DiscountCode { get; set; }
    }
    public class FieldEurailCountry
    {
        public int CountryCode { get; set; }
        public string Name { get; set; }
    }
    public class ProuctDiscount
    {
        public Guid ProductPriceId { get; set; }
        public string CategoryName { get; set; }
        public Guid CategoryId { get; set; }
        public string ProductName { get; set; }
        public Guid ProductId { get; set; }
        public decimal DiscountPrice { get; set; }
        public string DiscountCode { get; set; }
        public bool IsPercent { get; set; }
        public DateTime? ModifyOn { get; set; }
        public Guid ModifyBy { get; set; }
        public string ModifyName { get; set; }
        public Nullable<DateTime> DiscountFromDate { get; set; }
        public Nullable<DateTime> DiscountToDate { get; set; }
    }
    public class ProductBookingFee
    {
        public string CategoryName { get; set; }
        public string ModifyName { get; set; }
        public Guid ModifyBy { get; set; }
        public DateTime? ModifyOn { get; set; }
        public Guid CategoryId { get; set; }
        public string ProductName { get; set; }
        public Guid ProductId { get; set; }
        public Nullable<DateTime> BookingFromDate { get; set; }
        public Nullable<DateTime> BookingToDate { get; set; }
        public decimal Price { get; set; }
        public decimal AmountFrom { get; set; }
        public decimal AmountTo { get; set; }
    }
    public class ProductPublishInfo
    {
        public string LastPublishDate { get; set; }
        public string LastPublishBy { get; set; }
    }
}
