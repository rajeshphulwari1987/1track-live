//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEQOQuotationPassSelection
    {
        public System.Guid Id { get; set; }
        public System.Guid QuotationId { get; set; }
        public System.Guid TravellerId { get; set; }
        public System.Guid ProductId { get; set; }
        public string Traveller { get; set; }
        public string Class { get; set; }
        public string Validity { get; set; }
        public int Passanger { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime Createdon { get; set; }
    
        public virtual tblEQOQuotation tblEQOQuotation { get; set; }
        public virtual tblProduct tblProduct { get; set; }
        public virtual tblTravelerMst tblTravelerMst { get; set; }
    }
}
