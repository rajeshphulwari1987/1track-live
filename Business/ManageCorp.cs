﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Data;

namespace Business
{
    public class ManageCorp
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        public List<tblSite> GetCorpSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsCorporate == true && x.IsActive == true && x.IsDelete != true).OrderBy(x => x.DisplayName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Corporate Cms Pages
        public List<CorporateCms> GetCorpPageList(Guid siteId)
        {
            try
            {
                var list = (from cm in _db.tblCorpCms
                            join st in _db.tblSites
                                on cm.SiteId equals st.ID
                            where st.ID == siteId
                            select new { cm, st }).ToList();
                return list.Select(x => new CorporateCms
                {
                    ID = x.cm.ID,
                    Sitename = x.st.DisplayName,
                    PageName = x.cm.PageName,
                    IsActive = x.cm.IsActive
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorporateCms : tblCorpCm
        {
            public string Sitename { get; set; }
        }
        public tblCorpCm GetCorpPageById(Guid id)
        {
            try
            {
                return _db.tblCorpCms.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddCorpPage(tblCorpCm odata)
        {
            try
            {
                var data = _db.tblCorpCms.FirstOrDefault(x => x.ID == odata.ID);
                if (data != null)
                {
                    data.SiteId = odata.SiteId;
                    data.PageName = odata.PageName;
                    data.Url = odata.Url;
                    data.MetaTitle = odata.MetaTitle;
                    data.Keyword = odata.Keyword;
                    data.MetaDescription = odata.MetaDescription;
                    data.Description = odata.Description;
                    data.IsActive = odata.IsActive;
                    data.IsPrimary = odata.IsPrimary;
                }
                else
                {
                    _db.AddTotblCorpCms(odata);
                }
                _db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveCorpPage(Guid id)
        {
            try
            {
                var objPage = _db.tblCorpCms.FirstOrDefault(x => x.ID == id);
                if (objPage != null)
                    objPage.IsActive = !objPage.IsActive;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpPage(Guid id)
        {
            try
            {
                var rec = _db.tblCorpCms.FirstOrDefault(x => x.ID == id);
                _db.tblCorpCms.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Corporate Faqs
        public List<tblCorpFaq> GetCorpFaqList(Guid _siteID)
        {
            return _db.tblCorpFaqs.Where(x => x.SiteID == _siteID).OrderByDescending(x => x.ModifyOn).ToList();
        }

        public tblCorpFaq GetCorpFaqByID(int id)
        {
            return _db.tblCorpFaqs.FirstOrDefault(x => x.ID == id);
        }

        public bool AddEditCorpFaq(tblCorpFaq oFaq)
        {
            try
            {
                if (oFaq.ID == 0)
                    _db.tblCorpFaqs.AddObject(oFaq);
                else
                {
                    var obj = _db.tblCorpFaqs.FirstOrDefault(x => x.ID == oFaq.ID);
                    if (obj != null)
                    {
                        obj.SiteID = oFaq.SiteID;
                        obj.Answer = oFaq.Answer;
                        obj.Question = oFaq.Question;
                        obj.ModifyOn = DateTime.Now;
                        obj.ModifyBy = oFaq.CreatedBy;
                    }
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpFaqByID(int id)
        {
            try
            {
                var res = _db.tblCorpFaqs.FirstOrDefault(x => x.ID == id);
                if (res != null)
                {
                    _db.DeleteObject(res);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Corporate Passenger Info
        public List<tblCorpCountry> GetActiveCountryList()
        {
            try
            {
                return _db.tblCorpCountries.Where(x => x.IsActive).OrderBy(x => x.CountryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CorpPassenger> GetCorpPassengerList()
        {
            try
            {
                var olist = (from cp in _db.tblCorpPassergerInfoes
                             join cm in _db.tblCorpCountries
                             on cp.CountryID equals cm.ID
                             select new { cp, cm }).ToList();

                var list = olist.Select(x => new CorpPassenger
                {
                    ID = x.cp.ID,
                    CountryName = x.cm.CountryName,
                    PassengerType = x.cp.PassengerType,
                    PassengerAge = x.cp.PassengerAge,
                    IsActive = x.cp.IsActive
                });
                return list.OrderBy(x => x.CountryName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCorpPassergerInfo GetCorpPassById(int id)
        {
            try
            {
                return _db.tblCorpPassergerInfoes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddCorpPassenger(tblCorpPassergerInfo odata)
        {
            try
            {
                var data = _db.tblCorpPassergerInfoes.FirstOrDefault(x => x.ID == odata.ID);
                if (data != null)
                {
                    data.CountryID = odata.CountryID;
                    data.PassengerType = odata.PassengerType;
                    data.PassengerAge = odata.PassengerAge;
                    data.IsActive = odata.IsActive;
                }
                else
                {
                    _db.AddTotblCorpPassergerInfoes(odata);
                }
                _db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveCorpPass(int id)
        {
            try
            {
                var objPage = _db.tblCorpPassergerInfoes.FirstOrDefault(x => x.ID == id);
                if (objPage != null)
                    objPage.IsActive = !objPage.IsActive;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpPass(int id)
        {
            try
            {
                var rec = _db.tblCorpPassergerInfoes.FirstOrDefault(x => x.ID == id);
                _db.tblCorpPassergerInfoes.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorpPassenger : tblCorpPassergerInfo
        {
            public string CountryName { get; set; }
        }
        #endregion

        #region Corporate Change/cancel order List
        public List<tblCorpChangeCancel> GetChangeCancelList(Guid siteId)
        {
            try
            {
                return _db.tblCorpChangeCancels.Where(x => x.SiteID == siteId).OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteChangeOrder(Guid id)
        {
            try
            {
                var data = _db.tblCorpChangeCancels.FirstOrDefault(x => x.ID == id);
                if (data != null)
                {
                    _db.tblCorpChangeCancels.DeleteObject(data);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCorpChangeCancel GetChangeOrderByID(Guid id)
        {
            try
            {
                return _db.tblCorpChangeCancels.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Journey Form
        public IQueryable<CorpJourneyPassenger> GetJourneyList(Guid siteId)
        {
            try
            {
                return _db.tblCorpJourneyPassengers.Where(x => x.SiteId == siteId).Select(x => new CorpJourneyPassenger
                {
                    Phone = x.Phone,
                    Firstname = x.Firstname,
                    Lastname = x.Lastname,
                    IATAno = x.IATAno,
                    CreatedOn = x.CreatedOn,
                    Id = x.Id,
                    Title = x.tblCorpRequestStatu.Title,
                    IsBooked = x.IsBooked,
                    Status = x.Status
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CorpJourneyPassenger GetJourneyByID(Guid id)
        {
            try
            {
                var result = (from jf in _db.tblCorpJourneyPassengers
                              join tt in _db.tblCorpTicketTypes on jf.Tickettype equals tt.ID
                              join td in _db.tblCorpTicketDelivereds on jf.Ticketdelivered equals td.Id
                              join s in _db.tblSites on jf.SiteId equals s.ID
                              join ul in _db.tblCorpUsers on jf.CreatedBy equals ul.ID
                              where jf.Id == id
                              select new { jf, tt, td, s, ul });
                return result.Select(x => new CorpJourneyPassenger
                {
                    Id = x.jf.Id,
                    Firstname = x.jf.Firstname,
                    Lastname = x.jf.Lastname,
                    Phone = x.jf.Phone,
                    Email = x.jf.Email,
                    IATAno = x.jf.IATAno,
                    Agencyname = x.jf.Agencyname,
                    Requesttype = x.jf.Requesttype,
                    Tickettype = x.jf.Tickettype,
                    TickettypeName = x.tt.Name,
                    Ticketdelivered = x.jf.Ticketdelivered,
                    TicketdelvName = x.td.Name,
                    Children = x.jf.Children,
                    Youths = x.jf.Youths,
                    Adults = x.jf.Adults,
                    Seniors = x.jf.Seniors,
                    Lname = x.jf.LPTitle + " " + x.jf.LPFname + " " + x.jf.LPLname + " " + x.jf.LPSuffix,
                    LPTitle = x.jf.LPTitle,
                    LPFname = x.jf.LPFname,
                    LPLname = x.jf.LPLname,
                    LPSuffix = x.jf.LPSuffix,
                    Dob = x.jf.Dob,
                    Discountcardtype = x.jf.Discountcardtype,
                    Discount = (x.jf.Discountcardtype != null) ? (_db.tblCorpLoyaltyCards.FirstOrDefault(y => y.Id == x.jf.Discountcardtype).Name) : null,
                    Discountcardno = x.jf.Discountcardno,
                    Discountexp = x.jf.Discountexp,
                    Note = x.jf.Note,
                    SiteName = x.s.DisplayName,
                    CreatedOn = x.jf.CreatedOn,
                    CreatedBy = x.ul.UserName,
                    IPAddress = x.jf.IPAddress,
                    UserId = x.jf.CreatedBy,
                    Status = x.jf.Status
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CorpJourney> GetJourneyByPassengerID(Guid id)
        {
            try
            {
                var result = (from cj in _db.tblCorpJourneys
                              join cm in _db.tblCorpClassMsts
                                  on cj.Class equals cm.Id
                              join ca in _db.tblCorpAccomodations
                                  on cj.Accomodation equals ca.Id
                              where cj.PassengerId == id
                              select new { cj, cm, ca });
                return result.Select(x => new CorpJourney
                {
                    Price = x.cj.Price,
                    Id = x.cj.Id,
                    From = x.cj.From,
                    To = x.cj.To,
                    Via = x.cj.Via,
                    Date = x.cj.Date,
                    Deptime = x.cj.Deptime,
                    Arrtime = x.cj.Arrtime,
                    Trainno = x.cj.Trainno,
                    Class = x.cj.Class,
                    ClassName = x.cm.Name,
                    Accomodation = x.cj.Accomodation,
                    AccomodationName = x.ca.Name,
                    JourneyOrder = x.cj.JourneyOrder
                }).OrderBy(x => x.JourneyOrder).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteJourney(Guid id)
        {
            try
            {
                _db.tblCorpJourneys.Where(x => x.PassengerId == id).ToList().ForEach(_db.tblCorpJourneys.DeleteObject);
                var data = _db.tblCorpJourneyPassengers.FirstOrDefault(x => x.Id == id);
                if (data != null)
                {
                    _db.tblCorpJourneyPassengers.DeleteObject(data);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ParentOfficeName(Guid ID)
        {
            try
            {
                if (ID != Guid.Empty)
                {
                    Guid ParentID = ID;
                Top:
                    var result = _db.tblCorpOffices.FirstOrDefault(x => x.ID == ParentID);
                    if (result != null)
                    {
                        if (result.ParentID != Guid.Empty)
                        {
                            ParentID = result.ParentID;
                            goto Top;
                        }
                        else
                            return _db.tblCorpOffices.FirstOrDefault(x => x.ID == ParentID).Name;
                    }
                    return string.Empty;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblCorpRequestStatu> GetAllStatusList()
        {
            try
            {
                return _db.tblCorpRequestStatus.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblCorpUser> GetAllUserList(Guid SiteID)
        {
            try
            {
                return (from TCU in _db.tblCorpUsers
                        join TCUSL in _db.tblCorpSiteUserLookUps on TCU.ID equals TCUSL.CorpUserID
                        where TCUSL.SiteID == SiteID && TCU.IsActive == true
                        select new { TCU }).ToList().Select(x => new tblCorpUser
                        {
                            ID = x.TCU.ID,
                            UserName = x.TCU.UserName
                        }).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblCorpOffice> GetParentOfficeName(Guid SiteID)
        {
            try
            {
                return (from TCO in _db.tblCorpOffices
                        join TCOSL in _db.tblCorpOfficeSiteLookUps on TCO.ID equals TCOSL.OfficeID
                        where TCOSL.SiteID == SiteID && TCO.IsActive == true
                        select new { TCO }).ToList().Select(x => new tblCorpOffice
                        {
                            ID = x.TCO.ID,
                            Name = x.TCO.Name,
                            ParentID = x.TCO.ParentID
                        }).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<CorpJourneyPassenger> GetCorporateTicketOrderList(Guid SiteID, string OrderID, string TMC, string Office, string Agent, string Passenger, string Status, int PageNO, int RecordPerPage)
        {
            try
            {
                return _db.spGetCorporateTicketOrderList(SiteID, OrderID, TMC, Office, Agent, Passenger, Status, PageNO, RecordPerPage).Select(x => new CorpJourneyPassenger
                {
                    Firstname = x.Firstname,
                    Lastname = x.Lastname,
                    CreatedOn = x.BookingDateTime,
                    Id = x.Id,
                    IsBooked = x.IsBooked,
                    Status = x.StatusID,
                    OrderStatus = x.Title,
                    OrderId = x.OrderId.Value,
                    From = x.From,
                    To = x.To,
                    DeptDateTime = x.DeptDateTime.Value,
                    Agent = x.UserName,
                    Office = x.OfficeName,
                    TMC = ParentOfficeName(x.OfficeParentID),
                    Price = x.Price,
                    TotalRecord = (int)x.TotalRecord
                }).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public class CorpJourneyPassenger : tblCorpJourneyPassenger
        {
            public string TickettypeName { get; set; }
            public string TicketdelvName { get; set; }
            public string Lname { get; set; }
            public string Discount { get; set; }
            public string SiteName { get; set; }
            public Guid UserId { get; set; }
            public string Title { get; set; }
            public string CreatedBy { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public string OrderStatus { get; set; }
            public DateTime DeptDateTime { get; set; }
            public string TMC { get; set; }
            public string Office { get; set; }
            public string Agent { get; set; }
            public string Total { get; set; }
            public decimal Price { get; set; }
            public int TotalRecord { get; set; }
        }

        public class CorpJourney : tblCorpJourney
        {
            public string ClassName { get; set; }
            public string AccomodationName { get; set; }
        }

        #endregion

        #region Corporate Logo
        public List<CorpLogo> GetLogoList()
        {
            try
            {
                var list = (from cl in _db.tblCorpLogoes
                            join st in _db.tblSites
                                on cl.SiteID equals st.ID
                            select new { cl, st }).ToList();
                return list.Select(x => new CorpLogo
                {
                    ID = x.cl.ID,
                    Title = x.cl.Title,
                    SiteID = x.cl.SiteID,
                    SiteName = x.st.DisplayName,
                    LogoPath = "../" + x.cl.LogoPath,
                    IsActive = x.cl.IsActive
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCorpLogo GetLogoByID(Guid ID)
        {
            try
            {
                return _db.tblCorpLogoes.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditLogo(tblCorpLogo odata)
        {
            try
            {
                var data = _db.tblCorpLogoes.FirstOrDefault(x => x.ID == odata.ID);
                if (data != null)
                {
                    data.SiteID = odata.SiteID;
                    data.LogoPath = odata.LogoPath;
                    data.IsActive = odata.IsActive;
                }
                else
                {
                    _db.AddTotblCorpLogoes(odata);
                }
                _db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddLogo(tblCorpLogo img)
        {
            try
            {
                var _img = new tblCorpLogo
                {
                    ID = img.ID,
                    SiteID = img.SiteID,
                    Title = img.Title,
                    LogoPath = img.LogoPath,
                    IsActive = img.IsActive
                };
                _db.AddTotblCorpLogoes(_img);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateLogo(tblCorpLogo img)
        {
            try
            {

                var _img = _db.tblCorpLogoes.FirstOrDefault(x => x.ID == img.ID);
                if (_img != null)
                {
                    _img.SiteID = img.SiteID;
                    _img.Title = img.Title;
                    if (img.LogoPath != null)
                    {
                        _img.LogoPath = img.LogoPath;
                    }
                    _img.IsActive = img.IsActive;
                }
                _db.SaveChanges();
                if (img.IsActive)
                {
                    var logo = _db.tblCorpLogoes.Where(x => x.ID != img.ID && x.SiteID == img.SiteID).ToList();
                    foreach (var item in logo)
                    {
                        item.IsActive = false;
                        _db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ActiveInactiveLogo(Guid ID, Guid siteId)
        {
            try
            {
                var logo = _db.tblCorpLogoes.Where(x => x.ID != ID && x.SiteID == siteId).ToList();
                foreach (var item in logo)
                {
                    item.IsActive = false;
                    _db.SaveChanges();
                }
                var objLogo = _db.tblCorpLogoes.FirstOrDefault(x => x.ID == ID);
                if (objLogo != null)
                    objLogo.IsActive = !objLogo.IsActive;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpLogo(Guid ID)
        {
            try
            {
                var rec = _db.tblCorpLogoes.FirstOrDefault(x => x.ID == ID);
                _db.tblCorpLogoes.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorpLogo : tblCorpLogo
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region Corprate User
        public Guid InsertUpdateCorporateUser(tblAdminUser user)
        {
            try
            {
                if (user.ID == new Guid())
                {
                    bool result = _db.tblAdminUsers.Any(x => x.UserName == user.UserName && x.Password == user.Password && x.IsCorporate);
                    if (result)
                        throw new Exception("User Name is already exist. Please try different!");
                    else
                    {
                        user.ID = Guid.NewGuid();
                        _db.tblAdminUsers.AddObject(user);
                    }
                }
                else
                {
                    tblAdminUser corpuser = _db.tblAdminUsers.FirstOrDefault(x => x.ID == user.ID);
                    corpuser.UserName = user.UserName;
                    corpuser.Password = user.Password;
                    corpuser.Salutation = user.Salutation;
                    corpuser.Forename = user.Forename;
                    corpuser.Surname = user.Surname;
                    corpuser.EmailAddress = user.EmailAddress;
                    corpuser.ModifiedBy = user.CreatedBy;
                    corpuser.ModifiedOn = DateTime.Now;
                    corpuser.IsActive = user.IsActive;
                    corpuser.Note = user.Note;
                    corpuser.BranchID = user.BranchID;
                }
                _db.SaveChanges();
                return user.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public int InsertUpdateCorporateUserSiteLookup(tblAdminUserLookupSite corpsiteuserlookup)
        {
            try
            {
                _db.tblAdminUserLookupSites.AddObject(corpsiteuserlookup);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<CorporateUser> GetCorporateUserList(Guid SiteId)
        {
            try
            {
                var list = (from cu in _db.tblAdminUsers
                            join csul in _db.tblAdminUserLookupSites on cu.ID equals csul.AdminUserID
                            join st in _db.tblSites on csul.SiteID equals st.ID
                            where st.ID == SiteId && cu.IsCorporate
                            select new { cu, csul, st }).ToList();

                return list.Select(x => new CorporateUser
                {
                    ID = x.cu.ID,
                    IsActive = x.cu.IsActive,
                    UserName = x.cu.UserName,
                    Forename = x.cu.Forename,
                    Surname = x.cu.Surname,
                    EmailAddress = x.cu.EmailAddress,
                    Salutation = x.cu.Salutation,
                    Note = x.cu.Note,
                    Password = x.cu.Password,
                    FullName = x.cu.Forename + " " + x.cu.Surname,
                }).OrderBy(x => x.UserName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblAdminUser GettblCorpUserListEdit(Guid ID)
        {
            try
            {
                return _db.tblAdminUsers.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ActiveInactiveCorporateUser(Guid id)
        {
            try
            {
                var rec = _db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorporateUser : tblCorpUser
        {
            public string FullName { get; set; }
        }
        #endregion

        #region Corporate Banner
        public List<CorpBanner> GetBannerList()
        {
            try
            {
                var list = (from cl in _db.tblCorpBanners
                            join st in _db.tblSites
                                on cl.SiteID equals st.ID
                            select new { cl, st }).ToList();
                return list.Select(x => new CorpBanner
                {
                    ID = x.cl.ID,
                    Title = x.cl.Title,
                    SiteID = x.cl.SiteID,
                    SiteName = x.st.DisplayName,
                    LogoPath = "../" + x.cl.LogoPath,
                    IsActive = x.cl.IsActive
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCorpBanner GetBannerByID(Guid ID)
        {
            try
            {
                return _db.tblCorpBanners.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditBanner(tblCorpBanner odata)
        {
            try
            {
                var data = _db.tblCorpBanners.FirstOrDefault(x => x.ID == odata.ID);
                if (data != null)
                {
                    data.SiteID = odata.SiteID;
                    data.LogoPath = odata.LogoPath;
                    data.IsActive = odata.IsActive;
                }
                else
                {
                    _db.AddTotblCorpBanners(odata);
                }
                _db.SaveChanges();
                return odata.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddBanner(tblCorpBanner img)
        {
            try
            {
                var _img = new tblCorpBanner
                {
                    ID = img.ID,
                    SiteID = img.SiteID,
                    Title = img.Title,
                    LogoPath = img.LogoPath,
                    IsActive = img.IsActive
                };
                _db.AddTotblCorpBanners(_img);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBanner(tblCorpBanner img)
        {
            try
            {

                var _img = _db.tblCorpBanners.FirstOrDefault(x => x.ID == img.ID);
                if (_img != null)
                {
                    _img.SiteID = img.SiteID;
                    _img.Title = img.Title;
                    if (img.LogoPath != null)
                    {
                        _img.LogoPath = img.LogoPath;
                    }
                    _img.IsActive = img.IsActive;
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool ActiveInactiveBanner(Guid ID)
        {
            try
            {
                var rec = _db.tblCorpBanners.FirstOrDefault(x => x.ID == ID);
                rec.IsActive = !rec.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCorpBanner(Guid ID)
        {
            try
            {
                var rec = _db.tblCorpBanners.FirstOrDefault(x => x.ID == ID);
                _db.tblCorpBanners.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorpBanner : tblCorpBanner
        {
            public string SiteName { get; set; }
        }
        #endregion

        #region Corporate Request ModifyLog
        public Guid AddUpdateCorporateRequestModifyLog(tblCorpRequestModifyLog odata)
        {
            try
            {
                var data = _db.tblCorpRequestModifyLogs.FirstOrDefault(x => x.Id == odata.Id);
                if (data != null)
                {
                    data.RequestId = odata.RequestId;
                    data.IsUsing = data.IsUsing;
                    data.CreatedBy = odata.CreatedBy;
                    data.CreatedOn = DateTime.Now;
                }
                else
                {
                    _db.AddTotblCorpRequestModifyLogs(odata);
                }
                _db.SaveChanges();
                return odata.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CorpRequestModifyLog> GetRequestList(Guid RequestId)
        {
            var list = _db.tblCorpRequestModifyLogs.Where(x => x.RequestId == RequestId).Select(y => new CorpRequestModifyLog
            {
                Date = y.CreatedOn,
                User = _db.tblAdminUsers.FirstOrDefault(z => z.ID == y.CreatedBy).UserName
            }).ToList();
            return list;
        }

        public List<CorpRequestComment> GetCommentList(Guid RequestId)
        {
            var list = _db.tblCorpRequestComments.Where(x => x.RequestId == RequestId).Select(y => new CorpRequestComment
            {
                Comment = y.Comment,
                UserName = _db.tblAdminUsers.FirstOrDefault(z => z.ID == y.UserId).UserName
            }).ToList();
            return list;
        }

        public class CorpRequestModifyLog
        {
            public string User { get; set; }
            public DateTime? Date { get; set; }
        }

        public class CorpRequestComment : tblCorpRequestComment
        {
            public string UserName { get; set; }
        }
        #endregion

        #region Request Comment
        public bool AddRequestComment(tblCorpRequestComment objtblCorpRequestComment)
        {
            try
            {
                _db.AddTotblCorpRequestComments(objtblCorpRequestComment);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Journey Booking Form
        public List<tblCorpJourneyPassengerBooked> GetJourneyBookingList(Guid siteId)
        {
            try
            {
                return _db.tblCorpJourneyPassengerBookeds.Where(x => x.SiteId == siteId).OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CorpJourneyPassengerBooked GetJourneyBookingByID(Guid id)
        {
            try
            {
                var result = (from jf in _db.tblCorpJourneyPassengerBookeds
                              join tt in _db.tblCorpTicketTypes on jf.Tickettype equals tt.ID
                              join td in _db.tblCorpTicketDelivereds on jf.Ticketdelivered equals td.Id
                              join s in _db.tblSites on jf.SiteId equals s.ID
                              join ul in _db.tblCorpUsers on jf.CreatedBy equals ul.ID
                              where jf.Id == id
                              select new { jf, tt, td, s, ul });

                return result.Select(x => new CorpJourneyPassengerBooked
                {
                    Id = x.jf.Id,
                    Firstname = x.jf.Firstname,
                    Lastname = x.jf.Lastname,
                    Phone = x.jf.Phone,
                    Email = x.jf.Email,
                    IATAno = x.jf.IATAno,
                    Agencyname = x.jf.Agencyname,
                    Requesttype = x.jf.Requesttype,
                    Tickettype = x.jf.Tickettype,
                    TickettypeName = x.tt.Name,
                    Ticketdelivered = x.jf.Ticketdelivered,
                    TicketdelvName = x.td.Name,
                    Children = x.jf.Children,
                    Youths = x.jf.Youths,
                    Adults = x.jf.Adults,
                    Seniors = x.jf.Seniors,
                    Lname = x.jf.LPTitle + " " + x.jf.LPFname + " " + x.jf.LPLname + " " + x.jf.LPSuffix,
                    LPTitle = x.jf.LPTitle,
                    LPFname = x.jf.LPFname,
                    LPLname = x.jf.LPLname,
                    LPSuffix = x.jf.LPSuffix,
                    Dob = x.jf.Dob,
                    Discountcardtype = x.jf.Discountcardtype,
                    Discount = (x.jf.Discountcardtype != null) ? (_db.tblCorpLoyaltyCards.FirstOrDefault(y => y.Id == x.jf.Discountcardtype).Name) : null,
                    Discountcardno = x.jf.Discountcardno,
                    Discountexp = x.jf.Discountexp,
                    Note = x.jf.Note,
                    SiteName = x.s.DisplayName,
                    CreatedOn = x.jf.CreatedOn,
                    CreatedBy = x.ul.UserName,
                    IPAddress = x.jf.IPAddress
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CorpJourneyBooked> GetJourneyBookingByPassengerID(Guid id)
        {
            try
            {
                var result = (from cj in _db.tblCorpJourneyBookeds
                              join cm in _db.tblCorpClassMsts
                                  on cj.Class equals cm.Id
                              join ca in _db.tblCorpAccomodations
                                  on cj.Accomodation equals ca.Id
                              where cj.PassengerId == id
                              select new { cj, cm, ca });
                return result.Select(x => new CorpJourneyBooked
                {
                    Id = x.cj.Id,
                    From = x.cj.From,
                    To = x.cj.To,
                    Via = x.cj.Via,
                    Date = x.cj.Date,
                    Deptime = x.cj.Deptime,
                    Arrtime = x.cj.Arrtime,
                    Trainno = x.cj.Trainno,
                    Class = x.cj.Class,
                    ClassName = x.cm.Name,
                    Accomodation = x.cj.Accomodation,
                    AccomodationName = x.ca.Name,
                    JourneyOrder = x.cj.JourneyOrder
                }).OrderBy(x => x.JourneyOrder).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class CorpJourneyPassengerBooked : tblCorpJourneyPassengerBooked
        {
            public string TickettypeName { get; set; }
            public string TicketdelvName { get; set; }
            public string Lname { get; set; }
            public string Discount { get; set; }
            public string SiteName { get; set; }
            public string CreatedBy { get; set; }
        }

        public class CorpJourneyBooked : tblCorpJourneyBooked
        {
            public string ClassName { get; set; }
            public string AccomodationName { get; set; }
        }
        #endregion
    }
}
