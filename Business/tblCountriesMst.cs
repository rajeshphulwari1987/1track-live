//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCountriesMst
    {
        public tblCountriesMst()
        {
            this.tblAffiliateUsers = new HashSet<tblAffiliateUser>();
            this.tblCorpOffices = new HashSet<tblCorpOffice>();
            this.tblCountrySiteLookUps = new HashSet<tblCountrySiteLookUp>();
            this.tblCountyMsts = new HashSet<tblCountyMst>();
            this.tblDeliveryOptionCountries = new HashSet<tblDeliveryOptionCountry>();
            this.tblJourneys = new HashSet<tblJourney>();
            this.tblPassShippingOptionCountries = new HashSet<tblPassShippingOptionCountry>();
            this.tblProductCountryLookUps = new HashSet<tblProductCountryLookUp>();
            this.tblProductPermittedLookups = new HashSet<tblProductPermittedLookup>();
            this.tblShippingOptionCountries = new HashSet<tblShippingOptionCountry>();
            this.tblSpecialTrains = new HashSet<tblSpecialTrain>();
            this.tblTrainDetailCntryLookups = new HashSet<tblTrainDetailCntryLookup>();
            this.tblUserLogins = new HashSet<tblUserLogin>();
        }
    
        public System.Guid CountryID { get; set; }
        public Nullable<System.Guid> RegionID { get; set; }
        public Nullable<System.Guid> ContinentID { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public Nullable<int> EurailCode { get; set; }
        public string IsoCode { get; set; }
        public string RBSCode { get; set; }
        public string DespatchExpressCountryCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public Nullable<bool> IsVisible { get; set; }
        public string CountryFlagImg { get; set; }
        public string CountryImg { get; set; }
        public string BannerImg { get; set; }
        public string OtherCountry { get; set; }
        public string InterrailCode { get; set; }
        public bool IsInterrail { get; set; }
        public bool IsEurail { get; set; }
    
        public virtual ICollection<tblAffiliateUser> tblAffiliateUsers { get; set; }
        public virtual tblContinent tblContinent { get; set; }
        public virtual ICollection<tblCorpOffice> tblCorpOffices { get; set; }
        public virtual tblRegionMst tblRegionMst { get; set; }
        public virtual ICollection<tblCountrySiteLookUp> tblCountrySiteLookUps { get; set; }
        public virtual ICollection<tblCountyMst> tblCountyMsts { get; set; }
        public virtual ICollection<tblDeliveryOptionCountry> tblDeliveryOptionCountries { get; set; }
        public virtual ICollection<tblJourney> tblJourneys { get; set; }
        public virtual ICollection<tblPassShippingOptionCountry> tblPassShippingOptionCountries { get; set; }
        public virtual ICollection<tblProductCountryLookUp> tblProductCountryLookUps { get; set; }
        public virtual ICollection<tblProductPermittedLookup> tblProductPermittedLookups { get; set; }
        public virtual ICollection<tblShippingOptionCountry> tblShippingOptionCountries { get; set; }
        public virtual ICollection<tblSpecialTrain> tblSpecialTrains { get; set; }
        public virtual ICollection<tblTrainDetailCntryLookup> tblTrainDetailCntryLookups { get; set; }
        public virtual ICollection<tblUserLogin> tblUserLogins { get; set; }
    }
}
