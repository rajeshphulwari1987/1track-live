﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageTestimonial
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        public Guid AddUpdateTestimonial(tblTestimonial objtblTestimonial)
        {
            try
            {
                if (objtblTestimonial.ID == new Guid())
                {
                    objtblTestimonial.ID = Guid.NewGuid();
                    _db.AddTotblTestimonials(objtblTestimonial);
                }
                else
                {
                    var data = _db.tblTestimonials.FirstOrDefault(x => x.ID == objtblTestimonial.ID);
                    if (data != null)
                    {
                        data.Title = objtblTestimonial.Title;
                        data.CustomerName = objtblTestimonial.CustomerName;
                        data.Website = objtblTestimonial.Website;
                        data.Message = objtblTestimonial.Message;
                        data.IsActive = objtblTestimonial.IsActive;
                    }
                }
                _db.SaveChanges();
                return objtblTestimonial.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteTestimonial(Guid Id)
        {
            try
            {
                _db.tblTestimonialSiteLookUps.Where(w => w.TestimonialId == Id).ToList().ForEach(_db.tblTestimonialSiteLookUps.DeleteObject);
                _db.SaveChanges();

                var data = _db.tblTestimonials.FirstOrDefault(x => x.ID == Id);
                if (data != null)
                    _db.DeleteObject(data);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ActiveInActiveTestimonial(Guid Id)
        {
            try
            {
                var data = _db.tblTestimonials.FirstOrDefault(x => x.ID == Id);
                if (data != null)
                    data.IsActive = !data.IsActive;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblTestimonial GetTestimonialById(Guid Id)
        {
            try
            {
                return _db.tblTestimonials.FirstOrDefault(x => x.ID == Id);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<tblTestimonial> GetTestimonialList()
        {
            try
            {
                return _db.tblTestimonials.ToList().OrderBy(x => x.Title).ThenBy(x => x.CustomerName).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid AddTestimonialSiteLookUp(List<tblTestimonialSiteLookUp> oLookup)
        {
            try
            {
                var id = oLookup.FirstOrDefault().TestimonialId;
                _db.tblTestimonialSiteLookUps.Where(x => x.TestimonialId == id).ToList().ForEach(_db.tblTestimonialSiteLookUps.DeleteObject);
                foreach (var item in oLookup)
                {
                    item.ID = Guid.NewGuid();
                    _db.tblTestimonialSiteLookUps.AddObject(item);
                }
                _db.SaveChanges();
                return id.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
