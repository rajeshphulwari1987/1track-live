﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
namespace Business
{


    public class ManagePrinting
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();

        public string GetSiteUrl(Guid siteId)
        {
            try
            {
                var rec = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                return rec != null ? rec.SiteURL : string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
