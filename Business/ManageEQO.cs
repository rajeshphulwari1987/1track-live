﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Collections.Generic;
using System.Web;
using System.Net;

namespace Business
{
    public class ManageEQO
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();
        Masters _master = new Masters();

        public string SaveeqoEnquiryLookUp(tblEQOEnquiry affUser)
        {
            try
            {
                db.tblEQOEnquiries.AddObject(affUser);
                db.SaveChanges();
                return affUser.Id + "_" + affUser.EnquiryID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid SaveEQOP2PEnquiry(tblEQOP2PEnquiry p2p)
        {
            try
            {

                db.tblEQOP2PEnquiry.AddObject(p2p);
                db.SaveChanges();
                return p2p.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid SaveEQOcontactusUser(tblUserLogin obj)
        {
            try
            {
                var objaffUser = db.tblUserLogins.Where(o => o.Email == obj.Email && o.SiteId == obj.SiteId).FirstOrDefault();
                if (objaffUser != null)
                {
                    objaffUser.FirstName = obj.FirstName;
                    objaffUser.LastName = obj.LastName;
                    objaffUser.Phone = obj.Phone;
                    objaffUser.Email = obj.Email;
                    objaffUser.Dob = obj.Dob;
                    objaffUser.Country = obj.Country;
                    objaffUser.City = obj.City;
                    objaffUser.State = obj.State;
                    objaffUser.PostCode = obj.PostCode;
                    objaffUser.IsActive = obj.IsActive;
                    objaffUser.SiteId = obj.SiteId;
                    objaffUser.Address = obj.Address;
                    objaffUser.Address2 = obj.Address2;
                    objaffUser.State = obj.State;
                    objaffUser.IsNewsEmail = obj.IsNewsEmail;
                    if (!string.IsNullOrEmpty(obj.Password))
                        objaffUser.Password = obj.Password;
                    objaffUser.IsContactUsUser = obj.IsContactUsUser;
                    objaffUser.IsEQOUser = obj.IsEQOUser;
                    /**hold exists id*/
                    obj.ID = objaffUser.ID;
                }
                else if (!db.tblUserLogins.Any(x => x.Email == obj.Email && x.SiteId == obj.SiteId))
                    db.tblUserLogins.AddObject(obj);
                else
                    return Guid.Empty;
                db.SaveChanges();
                return obj.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Sendcontactus email
        public bool SendContactUsMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail)
        {
            try
            {
                var masterPage = new Masters();
                var message = new MailMessage();
                var smtpClient = new SmtpClient();
                var result = masterPage.GetEmailSettingDetail(siteId);
                if (result != null)
                {

                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                    message.From = new MailAddress(FromEmail, FromEmail);
                    message.To.Add(ToMail);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = Body;
                    smtpClient.Send(message);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ee)
            {
                return false;
            }
        }


        public List<tblUserLogin> GetUserEmail()
        {
            try
            {
                var list = db.tblUserLogins.Where(x => x.IsActive == true).OrderBy(x => x.Email).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblEQOEmailType GetEmailByCode(string Code)
        {
            tblEQOEmailType objReturn = new tblEQOEmailType();
            try
            {
                tblEQOEmailType Templates = db.tblEQOEmailTypes.FirstOrDefault(x => x.EmailTypeCode.Trim().ToLower().Contains(Code.Trim().ToLower()));
                objReturn = Templates != null ? Templates : null;
            }
            catch
            {
                //objReturn = SetResultStatus<tblEQOEmailType>(null, MessageStatus.Error, false);
            }
            return objReturn;
        }

        public tblEQOEmailTemplate GetTemplateById(int id)
        {
            tblEQOEmailTemplate objReturn = new tblEQOEmailTemplate();
            try
            {
                tblEQOEmailTemplate Templates = db.tblEQOEmailTemplates.FirstOrDefault(x => x.EmailTypeId == id);
                objReturn = Templates != null ? Templates : null;
            }
            catch
            {
                //objReturn = SetResultStatus<tblEQOEmailTemplate>(null, MessageStatus.Error, false);
            }
            return objReturn;
        }


        public Int64 Create(tblEQOMail InsertRecord)
        {
            try
            {

                db.tblEQOMails.AddObject(InsertRecord);
                db.SaveChanges();
                return InsertRecord.MailId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 CreateMailTo(tblEQOMailTo InsertRecord)
        {
            try
            {

                db.tblEQOMailToes.AddObject(InsertRecord);
                db.SaveChanges();
                return InsertRecord.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 CreateMailAttatchments(tblEQOMailsAttachment InsertRecord)
        {
            try
            {

                db.tblEQOMailsAttachments.AddObject(InsertRecord);
                db.SaveChanges();
                return InsertRecord.MailattId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid SaveeqoEnquiryDoc(tblEQOEnquiryDoc affUser)
        {
            try
            {
                db.tblEQOEnquiryDocs.AddObject(affUser);
                db.SaveChanges();
                return affUser.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object GetUserEmailbyEmailid(string stremail)
        {
            try
            {
                var list = db.tblUserLogins.Where(x => x.Email == stremail && x.IsActive == true).FirstOrDefault();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSite Getsitebyid(Guid _siteId)
        {
            try
            {
                var list = db.tblSites.Where(x => x.ID == _siteId && x.IsActive == true).FirstOrDefault();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblEQOEnquiry Getenquirybyid(Guid enqid)
        {
            try
            {
                var list = db.tblEQOEnquiries.Where(x => x.Id == enqid).FirstOrDefault();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblUserLogin GetUserbyid(Guid userid)
        {
            try
            {
                var list = db.tblUserLogins.FirstOrDefault(x => x.ID == userid);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long SaveEQOActivityLog(tblEQOActivityLog activitylog)
        {
            try
            {
                db.tblEQOActivityLogs.AddObject(activitylog);
                db.SaveChanges();
                return activitylog.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

public class AgentSharingGroup
{
    public Guid Id { get; set; }
    public string GroupName { get; set; }
    public string AgentName { get; set; }
    public Guid AgentId { get; set; }
    public System.Guid CreatedBy { get; set; }
    public System.DateTime CreatedOn { get; set; }
    public Nullable<System.Guid> ModifiedBy { get; set; }
    public Nullable<System.DateTime> ModifiedOn { get; set; }
    public bool IsActive { get; set; }
    public bool IsDelete { get; set; }

}
