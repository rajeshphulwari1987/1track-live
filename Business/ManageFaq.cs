﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class ManageFaq
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public List<tblSite> GetSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region
        public string GetFaqDescriptionBySiteId(Guid id)
        {
            try
            {
                var result = _db.tblFaqs.FirstOrDefault(x => x.SiteId == id);
                return result == null ? "" : result.Description;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblFaqQA> GetFaqListonFront(Guid siteId)
        {
            try
            {
                var list = _db.tblFaqs.FirstOrDefault(ty => ty.SiteId == siteId);
                if (list != null)
                {
                    var list2 = (_db.tblFaqQAs.Where(ty => ty.FaqID == list.ID)).ToList();
                    return list2.ToList();
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Faq adn FaqQA

        public List<FaqList> GetFaqList(Guid siteId)
        {
            try
            {
                var list = _db.tblFaqs.Where(ty => ty.SiteId == siteId).Select(x => new FaqList
                {
                    ID = x.ID,
                    Name = x.Name.Trim(),
                    SiteName = x.tblSite.DisplayName
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class FaqList : tblFaq
        {
            public string SiteName { get; set; }
        }

        public bool DeleteFaqPolicy(Guid id)
        {
            try
            {
                _db.tblFaqQAs.Where(x => x.FaqID == id).ToList().ForEach(_db.tblFaqQAs.DeleteObject);
                var rec = _db.tblFaqs.FirstOrDefault(x => x.ID == id);
                _db.tblFaqs.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteFaqQA(Guid id)
        {
            try
            {
                var rec = _db.tblFaqQAs.FirstOrDefault(x => x.Id == id);
                _db.tblFaqQAs.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblFaq GetFaqById(Guid id)
        {
            try
            {
                return _db.tblFaqs.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblFaqQA> GetFaqQAById(Guid id)
        {
            try
            {
                return _db.tblFaqQAs.Where(x => x.FaqID == id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddFaq(tblFaq oFaq)
        {
            int validQaInsert = 0;
            try
            {
                var Faq = new tblFaq
                {
                    ID = oFaq.ID,
                    Name = oFaq.Name,
                    Description = oFaq.Description,
                    Keyword = oFaq.Keyword,
                    CreatedOn = oFaq.CreatedOn,
                    CreatedBy = oFaq.CreatedBy,
                    SiteId = oFaq.SiteId,
                };
                var _test = _db.tblFaqs.FirstOrDefault(x => x.SiteId == oFaq.SiteId);
                if (_test == null)
                {
                    _db.AddTotblFaqs(Faq);
                    validQaInsert = 1;
                }
                _db.SaveChanges();
                return validQaInsert;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddFaqQa(tblFaqQA oFaq)
        {
            try
            {
                var Faq = new tblFaqQA
                {
                    Id = oFaq.Id,
                    FaqID = oFaq.FaqID,
                    Question = oFaq.Question,
                    Answer = oFaq.Answer,
                };
                _db.AddTotblFaqQAs(Faq);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateFaq(tblFaq oFaq)
        {
            try
            {
                var _privacy = _db.tblFaqs.FirstOrDefault(x => x.ID == oFaq.ID);
                if (_privacy != null)
                {
                    _privacy.Name = oFaq.Name;
                    _privacy.Description = oFaq.Description;
                    _privacy.Keyword = oFaq.Keyword;
                    _privacy.ModifyBy = oFaq.ModifyBy;
                    _privacy.ModifyOn = oFaq.ModifyOn;
                    _privacy.SiteId = oFaq.SiteId;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateFaqQA(tblFaqQA oFaq)
        {
            try
            {
                var _privacy = _db.tblFaqQAs.FirstOrDefault(x => x.Id == oFaq.Id);
                if (_privacy != null)
                {
                    _privacy.Question = oFaq.Question;
                    _privacy.Answer = oFaq.Answer;
                }
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region Admin FAQs
        public bool AddEditAdminFaq(tblAdminFaq oFaq)
        {
            try
            {
                if (oFaq.ID == 0)
                    _db.tblAdminFaqs.AddObject(oFaq);
                else
                {
                    tblAdminFaq obj = _db.tblAdminFaqs.FirstOrDefault(x => x.ID == oFaq.ID);
                    if (obj != null)
                    {
                        obj.Answer = oFaq.Answer;
                        obj.Question = oFaq.Question;
                        obj.ModifyOn = System.DateTime.Now;
                        obj.ModifyBy = oFaq.CreatedBy;
                    }
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblAdminFaq GetAdminFaqByID(int id)
        {
            return _db.tblAdminFaqs.FirstOrDefault(x => x.ID == id);
        }

        public bool DeleteAdminFaqByID(int id)
        {
            try
            {
                tblAdminFaq res = _db.tblAdminFaqs.FirstOrDefault(x => x.ID == id);
                if (res != null)
                {
                    _db.DeleteObject(res);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<tblAdminFaq> GetAdminFaqList()
        {
            return _db.tblAdminFaqs.OrderByDescending(x => x.ModifyOn).ToList();
        }
        #endregion
    }
}
