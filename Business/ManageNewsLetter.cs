﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace Business
{
    public class ManageNewsLetter : Masters
    {
        db_1TrackEntities db = new db_1TrackEntities();

        #region [ Save News Letter ]
        public void SaveNewsLetter(tblNewsLetter newsletr)
        {
            var objnews = db.tblNewsLetters.Where(x => x.ID == newsletr.ID).FirstOrDefault();
            if (objnews != null)
            {

            }
            else
            {
                db.tblNewsLetters.AddObject(newsletr);
            }
            db.SaveChanges();
        }
        public void UpdateNewsLetter(tblNewsLetter newsletr)
        {
            db.UpdateNewsLetter(newsletr.ID, newsletr.Title, newsletr.NewsLatterContent, newsletr.StartDate, newsletr.EndDate, newsletr.IsActive, newsletr.ModifiedBy, newsletr.ModifiedOn);
        }
        #endregion

        #region [ News Letter List ][
        public List<tblNewsLetter> GetNewsLetters()
        {
            try
            {
                return db.tblNewsLetters.ToList();
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }
        public List<tblNewsLetter> GetNewsLettersByID(Guid ID)
        {
            try
            {
                return db.tblNewsLetters.Where(a => a.ID == ID).ToList();
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }
        public List<Guid> GetSiteIdById(Guid id)
        {
            return db.tblNewsLetterSiteLookups.Where(x => x.NewsID == id).Select(y => y.SiteID).ToList();
        }
        public string GetSiteName(Guid id)
        {
            var res = (from catSi in db.tblNewsLetterSiteLookups
                       join ts in db.tblSites on catSi.SiteID equals ts.ID
                       where catSi.NewsID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";

            }
            if (siteName == "")
            {
                siteName = "-";
            }
            return siteName;

        }

        #endregion
        #region [ News Letter AttachMent ]
        public void SaveNewsLetterAttachments(tblNewsLetterAttachment oNewsAtach)
        {
            try
            {
                db.tblNewsLetterAttachments.AddObject(oNewsAtach);
                db.SaveChanges();
            }
            catch (Exception exp)
            {
                throw exp;

            }
        }
        public List<tblNewsLetterAttachment> GetNewsLetterAttachmentsByNewsId(Guid NewsID)
        {
            try
            {
                List<tblNewsLetterAttachment> lst = db.tblNewsLetterAttachments.Where(a => a.NewsID == NewsID).ToList();
                return lst;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        public void DeleteNewsLetterAttach(Guid ID)
        {
            try
            {
                tblNewsLetterAttachment obj = db.tblNewsLetterAttachments.Where(a => a.ID == ID).FirstOrDefault();
                db.tblNewsLetterAttachments.DeleteObject(obj);
                db.SaveChanges();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        public void DeleteNewsLetterSiteLookup(Guid NewsID)
        {
            try
            {
                db.tblNewsLetterSiteLookups.Where(a => a.NewsID == NewsID).ToList().ForEach(db.tblNewsLetterSiteLookups.DeleteObject);
                db.SaveChanges();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        public void AddNewsLetterSiteLookUp(tblNewsLetterSiteLookup NewsLetterLookUp)
        {
            try
            {
                NewsLetterLookUp.ID = Guid.NewGuid();
                db.tblNewsLetterSiteLookups.AddObject(NewsLetterLookUp);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteNewsLetter(Guid ID)
        {
            try
            {
                db.tblNewsLetterSiteLookups.Where(a => a.NewsID == ID).ToList().ForEach(db.tblNewsLetterSiteLookups.DeleteObject);
                db.tblNewsLetterAttachments.Where(a => a.NewsID == ID).ToList().ForEach(db.tblNewsLetterAttachments.DeleteObject);
                db.tblNewsLetters.Where(a => a.ID == ID).ToList().ForEach(db.tblNewsLetters.DeleteObject);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActiveInActiveNewsLetter(Guid ID)
        {
            try
            {
                db.ActiveInActiveNewsletter(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Send Mail
        public static bool SendMail(string TO, string Subject, string Body, string[] AttachMents)
        {
            try
            {
                MailMessage objemail = new MailMessage();
                objemail.From = new MailAddress("wwwsmtp@dotsquares.com");
                objemail.To.Add(TO.Trim());
                objemail.Subject = Subject;
                string bodyTxtHtml = Body;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bodyTxtHtml, null, "text/html");
                objemail.AlternateViews.Add(htmlView);
                foreach (string str in AttachMents)
                {
                    objemail.Attachments.Add(new Attachment(str));
                }
                //send the message
                System.Net.NetworkCredential mailAuthentication = new System.Net.NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("mail.dotsquares.com", 25);
                smtp.EnableSsl = false;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = mailAuthentication;
                smtp.Send(objemail);
                return true;
            }
            catch (Exception ee)
            {
                return false;

            }
        }
        public Boolean UnSubscribeUserNewsletter(Guid ID)
        {
            try
            {
                db.UnsubscribeUserNewsletter(ID);
                return true;
            }
            catch (Exception ee)
            {
                return false;

            }
        }
        #endregion
    }
}
