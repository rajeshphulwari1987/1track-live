﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.Xml;
using System.Security.Cryptography;
using System.IO;

namespace Business
{
    public class ManageQuotation
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();

        #region Manage Qouted Order Call Cost SiteWise
        public bool AddEditPaymentCallCost(tblPaymentCallCost paycost)
        {
            try
            {
                if (paycost.ID == Guid.Empty)
                {
                    if (db.tblPaymentCallCosts.Any(x => x.Name == paycost.Name && x.SiteID == paycost.SiteID))
                        throw new Exception("Payment name is already exist.");
                    paycost.ID = Guid.NewGuid();
                    db.tblPaymentCallCosts.AddObject(paycost);
                }
                else
                {
                    var result = db.tblPaymentCallCosts.FirstOrDefault(x => x.ID == paycost.ID);
                    result.Name = paycost.Name;
                    result.Cost = paycost.Cost;
                    result.ServiceCharge = paycost.ServiceCharge;
                    result.SiteID = paycost.SiteID;
                    result.IsActive = paycost.IsActive;
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<tblPaymentCallCost> GetPaymentCallCostList(Guid SiteId)
        {

            try
            {
                return db.tblPaymentCallCosts.Where(x => x.SiteID == SiteId).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblPaymentCallCost> GetPaymentCallCostListBySiteId(Guid SiteId)
        {

            try
            {
                return db.tblPaymentCallCosts.Where(x => x.IsActive == true && x.SiteID == SiteId).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblPaymentCallCost GetPaymentCallCostListById(Guid id)
        {

            try
            {
                return db.tblPaymentCallCosts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInActivePaymentCallCost(Guid id)
        {
            try
            {
                var result = db.tblPaymentCallCosts.FirstOrDefault(x => x.ID == id);
                result.IsActive = !result.IsActive;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeletePaymentCallCost(Guid id)
        {
            try
            {
                var result = db.tblPaymentCallCosts.FirstOrDefault(x => x.ID == id);
                db.tblPaymentCallCosts.DeleteObject(result);
                db.SaveChanges();
                return true;
            }
            catch
            {
                throw new Exception("Payment is already in used.");
            }
        }
        #endregion

        public long CreateOrder(long OrderId, string AffiliateCode, Guid AgentID, Guid USERID, Guid SiteId, string TrvType, string AgentReferenceNo)
        {
            try
            {
                var objOrders = db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (objOrders == null)
                {
                    tblOrder objOrder = new tblOrder();
                    objOrder.CreatedOn = DateTime.Now;
                    objOrder.Status = 21;
                    objOrder.SiteID = SiteId;
                    objOrder.AffiliateCode = AffiliateCode; ;
                    objOrder.UserID = USERID;
                    objOrder.AgentID = AgentID;
                    objOrder.TrvType = TrvType;
                    objOrder.IpAddress = GetIpAddress();
                    objOrder.AgentReferenceNo = AgentReferenceNo;
                    objOrder.ShippingAmount = Convert.ToDecimal("0.00");
                    objOrder.BookingFee = Convert.ToDecimal("0.00");
                    db.tblOrders.AddObject(objOrder);
                    db.SaveChanges();
                    return objOrder.OrderID;
                }
                else
                {
                    objOrders.Status = 21;
                    objOrders.SiteID = SiteId;
                    objOrders.AffiliateCode = AffiliateCode; ;
                    objOrders.UserID = USERID;
                    objOrders.AgentID = AgentID;
                    objOrders.TrvType = TrvType;
                    objOrders.IpAddress = GetIpAddress();
                    objOrders.AgentReferenceNo = AgentReferenceNo;
                    objOrders.ShippingAmount = Convert.ToDecimal("0.00");
                    objOrders.BookingFee = Convert.ToDecimal("0.00");
                    objOrders.AdminFee = Convert.ToDecimal("0.00");
                    objOrders.ShippingMethod = "";
                    objOrders.ShippingDescription = "";
                    objOrders.Note = "";
                    objOrders.CardholderName = "";
                    objOrders.CardNumber = "";
                    objOrders.PaymentId = "";
                    objOrders.ExpDate = "";
                    objOrders.Brand = "";
                    db.SaveChanges();
                    return objOrders.OrderID;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid AddP2PSale(tblP2PSale objP2PSale)
        {
            try
            {
                objP2PSale.ID = Guid.NewGuid();
                db.tblP2PSale.AddObject(objP2PSale);
                db.SaveChanges();
                return objP2PSale.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid AddOrderTraveller(tblOrderTraveller objtblOrderTraveller)
        {
            try
            {
                tblOrderTraveller objOTravell = new tblOrderTraveller();
                objOTravell.ID = Guid.NewGuid();
                objOTravell.Title = objtblOrderTraveller.Title;
                objOTravell.FirstName = objtblOrderTraveller.FirstName;
                objOTravell.LastName = objtblOrderTraveller.LastName;
                objOTravell.Country = objtblOrderTraveller.Country;
                db.tblOrderTravellers.AddObject(objOTravell);
                db.SaveChanges();
                return objOTravell.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid AddPassP2PSalelookup(long OrderId, Guid P2PSaleId, Guid TravellerId, string FIPNumber)
        {
            try
            {
                tblPassP2PSalelookup objP2PLookup = new tblPassP2PSalelookup();
                objP2PLookup.ID = Guid.NewGuid();
                objP2PLookup.OrderID = OrderId;
                objP2PLookup.PassSaleID = P2PSaleId;
                objP2PLookup.ProductType = "P2P";
                objP2PLookup.OrderTravellerID = TravellerId;
                objP2PLookup.FIPNumber = FIPNumber;
                db.tblPassP2PSalelookup.AddObject(objP2PLookup);
                db.SaveChanges();
                return objP2PLookup.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid AddP2POtherTravellerLookup(long OrderId, Guid P2PSaleId, Guid TravellerId, string FIPNumber)
        {
            try
            {
                tblP2POtherTravellerLookup objP2POtherLookUp = new tblP2POtherTravellerLookup();
                objP2POtherLookUp.ID = Guid.NewGuid();
                objP2POtherLookUp.OrderID = OrderId;
                objP2POtherLookUp.PassSaleID = P2PSaleId;
                objP2POtherLookUp.OrderTravellerID = TravellerId;
                objP2POtherLookUp.FIPNumber = FIPNumber;
                db.tblP2POtherTravellerLookup.AddObject(objP2POtherLookUp);
                db.SaveChanges();
                return objP2POtherLookUp.ID;
            }
            catch (Exception ex) { throw ex; }
        }

        public Guid GetTravellerId(long OrderId)
        {
            try
            {
                var data = db.tblPassP2PSalelookup.FirstOrDefault(x => x.OrderID == OrderId);
                if (data != null)
                    return data.OrderTravellerID.Value;
                return Guid.Empty;
            }
            catch (Exception ex) { throw ex; }
        }

        public void AddUpdateOrderBillingAddress(tblOrderBillingAddress objBilling)
        {
            try
            {
                tblOrderBillingAddress objBillingAddress = db.tblOrderBillingAddresses.Where(a => a.OrderID == objBilling.OrderID).FirstOrDefault();
                if (objBillingAddress != null)
                {
                    db.tblOrderBillingAddresses.DeleteObject(objBillingAddress);
                    db.SaveChanges();
                }

                db.tblOrderBillingAddresses.AddObject(objBilling);
                db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }

        public void UpdateFIPNumber(Guid P2PSaleId, string FIPNumber)
        {
            try
            {
                tblP2PSale objP2PSale = db.tblP2PSale.FirstOrDefault(x => x.ID == P2PSaleId);
                if (objP2PSale != null)
                    objP2PSale.FIPNumber = FIPNumber;
                db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }

        public void AddModelLookUp(long OrderId)
        {
            try
            {
                var data = db.tblOrderModes.FirstOrDefault();
                if (data != null)
                {
                    var objOrderModeLookUp = db.tblOrderModeLookUps.FirstOrDefault(x => x.OrderId == OrderId);
                    if (objOrderModeLookUp != null)
                        db.tblOrderModeLookUps.DeleteObject(objOrderModeLookUp);

                    tblOrderModeLookUp objOrder = new tblOrderModeLookUp();
                    objOrder.OrderId = OrderId;
                    objOrder.ModeId = data.Id;
                    db.tblOrderModeLookUps.AddObject(objOrder);
                    db.SaveChanges();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public void AddNotes(tblOrderNote objNote)
        {
            try
            {
                var objOrderNote = db.tblOrderNotes.FirstOrDefault(x => x.OrderID == objNote.OrderID);
                if (objOrderNote != null)
                {
                    objOrderNote.Notes = objNote.Notes;
                    objOrderNote.ModifiedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    db.tblOrderNotes.AddObject(objNote);
                    db.SaveChanges();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetIpAddress()
        {
            try
            {
                string result = string.Empty;
                string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(ip))
                {
                    string[] ipRange = ip.Split(',');
                    int le = ipRange.Length - 1;
                    result = ipRange[0];
                }
                else
                {
                    result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }

                return result;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool AddUpdateQuoteLookUp(tblOrderQuoteLookUp objLookUp)
        {
            try
            {
                tblOrderQuoteLookUp objQuote = db.tblOrderQuoteLookUps.FirstOrDefault(x => x.OrderId == objLookUp.OrderId);
                if (objQuote != null)
                {
                    objQuote.CallCost = objLookUp.CallCost;
                    objQuote.ServiceTax = objLookUp.ServiceTax;
                    objQuote.PaymentDeadLine = objLookUp.PaymentDeadLine;
                    objQuote.SiteId = objLookUp.SiteId;
                    objQuote.PaymentCallCostId = objLookUp.PaymentCallCostId;
                }
                else
                {
                    db.tblOrderQuoteLookUps.AddObject(objLookUp);
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblOrderQuoteLookUp GetOrderQuoteLookUp(long OrderId)
        {
            try
            {
                return db.tblOrderQuoteLookUps.FirstOrDefault(x => x.OrderId == OrderId);
            }
            catch (Exception ex) { throw ex; }
        }

        public tblOrderBillingAddress GetBillingAddress(long OrderId)
        {
            try
            {
                return db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderId);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool DeleteAllOrderInfo(long OrderId)
        {
            try
            {
                var objP2POtherTraveller = db.tblP2POtherTravellerLookup.Where(x => x.OrderID == OrderId).ToList();
                if (objP2POtherTraveller != null)
                {
                    Guid[] OrderTravellerIds = objP2POtherTraveller.Select(x => x.OrderTravellerID.Value).ToArray();
                    objP2POtherTraveller.ForEach(db.tblP2POtherTravellerLookup.DeleteObject);
                    db.tblOrderTravellers.Where(x => OrderTravellerIds.Contains(x.ID)).ToList().ForEach(db.tblOrderTravellers.DeleteObject);

                    var objPassP2PSalelookup = db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderId).ToList();
                    if (objPassP2PSalelookup != null)
                    {
                        Guid[] PassSaleIds = objPassP2PSalelookup.Select(x => x.PassSaleID.Value).ToArray();
                        objPassP2PSalelookup.ForEach(db.tblPassP2PSalelookup.DeleteObject);
                        db.tblPassSales.Where(x => PassSaleIds.Contains(x.ID)).ToList().ForEach(db.tblPassSales.DeleteObject);
                    }
                    db.SaveChanges();
                }
                else
                {
                    var objPassP2PSalelookup = db.tblPassP2PSalelookup.Where(x => x.OrderID == OrderId).ToList();
                    if (objPassP2PSalelookup != null)
                    {
                        Guid[] PassSaleIds = objPassP2PSalelookup.Select(x => x.PassSaleID.Value).ToArray();
                        Guid[] OrderTravellerIds = objPassP2PSalelookup.Select(x => x.OrderTravellerID.Value).ToArray();
                        objPassP2PSalelookup.ForEach(db.tblPassP2PSalelookup.DeleteObject);
                        db.tblPassSales.Where(x => PassSaleIds.Contains(x.ID)).ToList().ForEach(db.tblPassSales.DeleteObject);
                        db.tblOrderTravellers.Where(x => OrderTravellerIds.Contains(x.ID)).ToList().ForEach(db.tblOrderTravellers.DeleteObject);
                    }
                    db.SaveChanges();
                }

                var onjOrderDiscount = db.tblOrderDiscounts.FirstOrDefault(x => x.OrderId == OrderId);
                if (onjOrderDiscount != null)
                {
                    db.tblOrderDiscounts.DeleteObject(onjOrderDiscount);
                    db.SaveChanges();
                }

                var onjOrderQuoteLookUp = db.tblOrderQuoteLookUps.FirstOrDefault(x => x.OrderId == OrderId);
                if (onjOrderQuoteLookUp != null)
                {
                    db.tblOrderQuoteLookUps.DeleteObject(onjOrderQuoteLookUp);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        public tblOrder GetOrders(long OrderId)
        {
            try
            {
                return db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public class DataEncryptor
    {
        public static string EncryptString(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string DecryptString(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
    public class P2PQuoteJourneyRequest
    {
        public Guid P2PSaleId { get; set; }
        public string JourneyType { get; set; }
        public string TrainNo { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public string DateTimeDepature { get; set; }
        public string DateTimeArrival { get; set; }

        public string Class { get; set; }
        public string NetPrice { get; set; }
        public string FareName { get; set; }

        public string DeliveryOption { get; set; }
        public string TicketProtection { get; set; }
        public string Terms { get; set; }
    }
    public class OrderTrvellers
    {
        public Guid TravellerId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FipNumber { get; set; }
        public Guid TvCountry { get; set; }
        public Guid P2POtherId { get; set; }
        public Guid PassP2PId { get; set; }
        public long OrderIdentity { get; set; }
    }
}
