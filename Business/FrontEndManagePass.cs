﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class FrontEndManagePass
    {
        db_1TrackEntities db = new db_1TrackEntities();

        public List<tblClassMst> GetClass(Guid prdID)
        {
            try
            {
                var olist = (from p in db.tblProductPrices
                             join c in db.tblClassMsts
                                 on p.ClassID equals c.ID
                             where p.ProductID == prdID
                             select new { c }).Distinct().ToList();
                var list = olist.Select(x => new tblClassMst
                {
                    ID = x.c.ID,
                    Name = x.c.Name
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblProductValidUpToName> GetTravellerNames(Guid prdID, Guid Classid)
        {
            try
            {
                var olist = (from p in db.tblProductPrices
                             join vn in db.tblProductValidUpToNames
                                 on p.ValidityID equals vn.ProductValidUpToID
                             where p.ProductID == prdID && p.ClassID == Classid
                             select new { vn }).Distinct().ToList();

                var list = olist.Select(x => new tblProductValidUpToName
                {
                    ID = x.vn.ID,
                    Name = x.vn.Name
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblTravelerMst> GetTravellerType(Guid prdId, Guid classid, Guid ValidityID)
        {
            try
            {
                var tppValidityID = db.tblProductValidUpToNames.FirstOrDefault(t => t.ID == ValidityID);
                var idv = (tppValidityID == null ? Guid.Empty : tppValidityID.ProductValidUpToID);

                var olist = (from p in db.tblProductPrices
                             join tv in db.tblTravelerMsts
                                 on p.TravelerID equals tv.ID
                             where p.ProductID == prdId && p.ClassID == classid && p.ValidityID == idv
                             select new { tv }).Distinct().ToList();

                var list = olist.Select(x => new tblTravelerMst
                {
                    ID = x.tv.ID,
                    Name = x.tv.Name,
                    FromAge = x.tv.FromAge,
                    ToAge = x.tv.ToAge
                }).ToList();
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PricewithMarkup> GetproductPrice(Guid validityId, Guid travellerId, Guid classid, Guid prdId, Guid siteid)
        {
            try
            {
                var result = (from tpp in db.tblProductPrices
                              join tpvn in db.tblProductValidUpToNames on tpp.ValidityID equals tpvn.ProductValidUpToID
                              where tpp.TravelerID == travellerId && tpvn.ID == validityId && tpp.ClassID == classid
                              && tpp.ProductID == prdId
                              select new { tpp.ID, tpp.Price, tpp.PriceBindLevel }).ToList().Select(t =>
                                  new PricewithMarkup
                                  {
                                      PriceMarkup = (t.Price + Getproductpricemarkup(t.ID, siteid, t.Price)).ToString("F"),
                                      PriceBand = t.PriceBindLevel
                                  }).OrderBy(t => t.PriceMarkup).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal Getproductpricemarkup(Guid ID, Guid Siteid, decimal Price)
        {
            try
            {
                var markup = db.tblProductMarkUps.FirstOrDefault(t => t.ProductPriceID == ID && t.SiteID == Siteid);
                if (markup != null)
                {
                    if (markup.MarkUpPercent > 0)
                        return (Price * markup.MarkUpPercent / 100);
                    else
                        return markup.MarkUpAmount;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PageDetails> BindPageDetails(Guid id)
        {
            try
            {
                return (from prdNm in db.tblProductNames
                        join prd in db.tblProducts
                        on prdNm.ProductID equals prd.ID
                        where prdNm.ProductID == id
                        select new PageDetails
                        {
                            Name = prdNm.Name,
                            Description = prd.Description,
                            Eligibility = prd.Eligibility,
                            EligibilityforSaver = prd.EligibilityforSaver,
                            Discount = prd.Discount,
                            Validity = prd.Validity,
                            TermCondition = prd.TermCondition
                        }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCategory GetCategoryTermsById(Guid id)
        {
            try
            {
                return db.tblCategories.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Traveller> GetProductTraveller(Guid id)
        {
            try
            {
                return (from x in db.tblTravelerMsts
                        join prd in db.tblProductPrices
                        on x.ID equals prd.TravelerID
                        where prd.ProductID == id && prd.IsActive
                        select new Traveller { ID = x.ID, Name = x.Name, AgeFrom = x.FromAge, AgeTo = x.ToAge, SortOrder = (x.SortOrder == null ? 0 : x.SortOrder) }).Distinct().OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid GetCountryIdOfProduct(Guid id)
        {
            try
            {
                var rec = db.tblProductCountryLookUps.FirstOrDefault(x => x.ProductID == id);
                return rec == null ? new Guid() : rec.CountryID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetProductName(Guid id)
        {
            try
            {
                Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                var rec = db.tblProductNames.FirstOrDefault(x => x.ProductID == id && x.LanguageID == langid);
                return rec == null ? "Not Found" : rec.Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCurrency(Guid id)
        {
            try
            {
                var rec = db.tblCurrencyMsts.FirstOrDefault(x => x.ID == id);
                return rec == null ? "Not Found" : rec.Symbol;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblSite GetCurrencyDetail(Guid siteid)
        {
            try
            {
                return db.tblSites.FirstOrDefault(x => x.ID == siteid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCurrencyShortCode(Guid id)
        {
            try
            {
                var rec = db.tblCurrencyMsts.FirstOrDefault(x => x.ID == id);
                return rec == null ? "Not Found" : rec.ShortCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetEurailCountryPriceBandByCountryCode(int CountryCode)
        {
            try
            {
                var data = db.tblEurailCountries.FirstOrDefault(x => x.CountryCode == CountryCode);
                if (data != null)
                    return data.PriceBindLevel;
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblProduct GetProductById(Guid id)
        {
            try
            {
                return db.tblProducts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Decimal GetPriceAfterConversion(Decimal price, Guid siteId, Guid productCurrencyId, Guid siteCurrencyId)
        {
            try
            {
                db_1TrackEntities _db = new db_1TrackEntities();
                return _db.GetCurrencyAfterConversion(price, siteCurrencyId, productCurrencyId, siteId).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblProductValidUpTo ProductValidityByName(string validityName)
        {
            try
            {
                db_1TrackEntities _db = new db_1TrackEntities();
                Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                return _db.tblProductValidUpToes.FirstOrDefault(x => x.tblProductValidUpToNames.FirstOrDefault(y => y.LanguageID == langid).Name.Trim().ToUpper() == validityName.Trim().ToUpper());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// printing section get validity details
        /// </summary>
        /// <param name="validityName"></param>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        public static string ProductValidityByNameAndProductId(string validityName, Guid ProductId)
        {
            try
            {
                db_1TrackEntities _db = new db_1TrackEntities();
                Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                var data = (from a in _db.tblProductValidUpToes
                            join b in _db.tblProductValidUpToNames on a.ID equals b.ProductValidUpToID
                            join c in _db.tblProductPrices on a.ID equals c.ValidityID
                            where b.LanguageID == langid && c.ProductID == ProductId && b.Name.Contains(validityName)
                            select new { a.PromoPassText }).Distinct().Select(t => t.PromoPassText).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Guid GetCurrencyID(string curName)
        {
            try
            {
                db_1TrackEntities _db = new db_1TrackEntities();
                return _db.tblCurrencyMsts.FirstOrDefault(x => x.ShortCode == curName).ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblTicketProtection GetTicketProtectionPrice(Guid SiteID)
        {
            try
            {
                db_1TrackEntities _db = new db_1TrackEntities();
                return _db.tblTicketProtections.FirstOrDefault(ty => ty.SiteID == SiteID && ty.IsActive == true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsTiApi(long OrderId)
        {
            try
            {
                var data = (from a in db.tblOrders
                            join b in db.tblPassP2PSalelookup on a.OrderID equals b.OrderID
                            join c in db.tblP2PSale on b.PassSaleID equals c.ID
                            where a.OrderID == OrderId && a.TrvType == "P2P"
                            select new { c }).ToList();
                if (data != null && data.Count > 0)
                    return (!string.IsNullOrEmpty(data.FirstOrDefault().c.ApiName) && data.FirstOrDefault().c.ApiName.ToLower() == "italia");
                return false;
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public class PageDetails
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Eligibility { get; set; }
        public string EligibilityforSaver { get; set; }
        public string Discount { get; set; }
        public string Validity { get; set; }
        public string TermCondition { get; set; }

    }
    public class Traveller
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public int AgeFrom { get; set; }
        public int AgeTo { get; set; }
        public int? SortOrder { get; set; }
        public string Age { get; set; }
    }
    public class PricewithMarkup
    {
        public string Price { get; set; }
        public string PriceMarkup { get; set; }
        public int PriceBand { get; set; }
    }
}
