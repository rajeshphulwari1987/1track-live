﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace Business
{
    public class ManageOrder
    {
        private readonly ManagePrintQueue _ManagePrintQueue = new ManagePrintQueue();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();

        public void UpdateBookingFeeByOrderId(Int32 OrderId, decimal BookingFee)
        {
            try
            {
                var objOrder = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (objOrder != null)
                {
                    objOrder.BookingFee = BookingFee;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public decimal GetOrderRefundsById(Guid PassSaleId)
        {
            try
            {
                var data = _db.tblOrderRefunds.FirstOrDefault(x => x.ProductID == PassSaleId);
                if (data != null)
                    return data.TotalProductRefund.Value;
                else
                    return 0;
            }
            catch (Exception ex) { throw ex; }
        }

        # region CRM REPORTS

        public List<CRMUserReport> GetCRMUserList(Guid siteId)
        {
            try
            {
                return _db.CRMUSERREPORT(siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public void AddUpdateOrderDiscountByOrderId(tblOrderDiscount objOrderDiscount)
        {
            try
            {
                tblOrderDiscount objDiscount = _db.tblOrderDiscounts.FirstOrDefault(x => x.OrderId == objOrderDiscount.OrderId);
                if (objDiscount != null)
                {
                    objDiscount.DiscountCode = objOrderDiscount.DiscountCode;
                    objDiscount.IsPercentage = objOrderDiscount.IsPercentage;
                    objDiscount.DiscountAmount = objOrderDiscount.DiscountAmount;
                }
                else
                {
                    _db.tblOrderDiscounts.AddObject(objOrderDiscount);
                }
                _db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetOrderDiscountCodeByOrderId(long orderId)
        {
            try
            {
                var data = _db.tblOrderDiscounts.FirstOrDefault(t => t.OrderId == orderId);
                if (data != null)
                    return data.DiscountCode;
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetOrderByOrderId(long OrderId)
        {
            try
            {
                tblOrder objOrder = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (objOrder != null)
                    return objOrder.BookingFee.HasValue ? objOrder.BookingFee.Value : 0;
                else
                    return 0;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<P2PTravellerList> GetP2PTravellerList(int OrderID)
        {
            try
            {
                var data = _db.tblP2POtherTravellerLookup.Where(t => t.OrderID == OrderID).Select(t => t.OrderTravellerID).ToList();
                if (data != null && data.Count > 0)
                {
                    return _db.tblOrderTravellers.Where(t => data.Contains(t.ID)).Select(t => new P2PTravellerList
                    {
                        OrderTravellerID = t.ID,
                        Title = t.Title,
                        FirstName = t.FirstName,
                        LastName = t.LastName,
                        Fipnumber = _db.tblP2PSale.FirstOrDefault(o => o.ID == (_db.tblP2POtherTravellerLookup.FirstOrDefault(u => u.OrderTravellerID == t.ID).PassSaleID)).FIPNumber,
                        CountryName = _db.tblCountriesMsts.FirstOrDefault(p => p.CountryID == t.Country) != null ? _db.tblCountriesMsts.FirstOrDefault(p => p.CountryID == t.Country).CountryName : string.Empty
                    }).ToList();
                }
                else
                {
                    var dataold = _db.tblPassP2PSalelookup.Where(t => t.OrderID == OrderID).Select(t => t.OrderTravellerID);
                    return _db.tblOrderTravellers.Where(t => dataold.Contains(t.ID)).Select(t => new P2PTravellerList
                    {
                        OrderTravellerID = t.ID,
                        Title = t.Title,
                        FirstName = t.FirstName,
                        LastName = t.LastName,
                        Fipnumber = _db.tblP2PSale.FirstOrDefault(o => o.ID == (_db.tblP2POtherTravellerLookup.FirstOrDefault(u => u.OrderTravellerID == t.ID).PassSaleID)).FIPNumber,
                        CountryName = _db.tblCountriesMsts.FirstOrDefault(p => p.CountryID == t.Country) != null ? _db.tblCountriesMsts.FirstOrDefault(p => p.CountryID == t.Country).CountryName : string.Empty
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetP2POrderListByMode_Result> GetP2POrderListByMode(string OrderID, string Reference, string LastName, string Status, string SiteId, DateTime d1, DateTime d2, int FilterType, int page, int pagesize, int ModeId, string PNRNumber)
        {
            try
            {
                return _db.GetP2POrderListByMode(OrderID, Reference, LastName, Status, SiteId, d1, d2, FilterType, page, pagesize, ModeId, PNRNumber).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOrderDiscountByOrderId(long orderId)
        {
            try
            {
                var data = _db.tblOrderDiscounts.FirstOrDefault(t => t.OrderId == orderId);
                if (data != null)
                {
                    if (data.IsPercentage)
                        return data.OrderDiscountAmount.ToString("F2");
                    else
                        return data.DiscountAmount.ToString("F2");
                }
                return "0.00";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblManualP2PCommition GetManualP2PCommitionById(Guid Id)
        {
            try
            {
                return _db.tblManualP2PCommition.FirstOrDefault(t => t.Id == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetTrainTicketsByOrderSiteId(long Orderid)
        {
            try
            {
                var aa = (from a in _db.tblOrders
                          join b in _db.tblSites on a.SiteID equals b.ID
                          where a.OrderID == Orderid
                          select new { b.IsTrainTickets }).FirstOrDefault().IsTrainTickets;

                return aa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetbillingEmail(int OrderID)
        {
            try
            {
                var data = _db.tblOrderBillingAddresses.FirstOrDefault(t => t.OrderID == OrderID);
                if (data != null)
                    return data.EmailAddress;
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetBccEmailBySiteid(Guid ID)
        {
            try
            {
                var data = _db.tblSites.FirstOrDefault(t => t.ID == ID);
                if (data != null)
                    return data.BccEmail;
                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Guid> GetSiteIdByManualP2PCommID(Guid id)
        {

            try
            {
                return _db.tblManualP2PComSiteLookUp.Where(x => x.P2PCommissionID == id).Select(y => y.SiteID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteManualP2PCommition(Guid id)
        {
            try
            {
                var data = _db.tblManualP2PCommition.FirstOrDefault(t => t.Id == id);
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.DeletedBy = AdminuserInfo.UserID;
                    data.DeletedOn = DateTime.Now;
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddManualP2PCommitionSiteLookUp(tblManualP2PComSiteLookUp p2pcomlookup)
        {
            try
            {

                p2pcomlookup.ID = Guid.NewGuid();
                _db.tblManualP2PComSiteLookUp.AddObject(p2pcomlookup);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool AddUpdateManualP2PCommition(Guid id, tblManualP2PCommition objcomm)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    _db.tblManualP2PCommition.AddObject(objcomm);
                }
                else
                {
                    var data = _db.tblManualP2PCommition.FirstOrDefault(t => t.Id == id);
                    if (data != null)
                    {
                        _db.tblManualP2PComSiteLookUp.Where(x => x.P2PCommissionID == id).ToList().ForEach(_db.tblManualP2PComSiteLookUp.DeleteObject);

                        data.Name = objcomm.Name;
                        data.Commition = objcomm.Commition;
                        data.Ispercentage = objcomm.Ispercentage;
                        data.ModifiedBy = objcomm.CreatedBy;
                        data.ModifiedOn = objcomm.CreatedOn;
                        data.CurrencyId = objcomm.CurrencyId;
                    }
                }

                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetOrderDiscountVisibleBySiteId(Guid SiteId)
        {
            try
            {
                return _db.tblProductDiscountSiteLookups.Any(t => t.SiteID == SiteId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetOrderDiscountVisibleByOrderId(long OrderId)
        {
            try
            {
                return _db.tblOrderDiscounts.Any(t => t.OrderId == OrderId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblSite> GetSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblOrderStatu> GetStatusList()
        {
            try
            {
                return _db.tblOrderStatus.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblPassP2PSalelookup> GetProductType()
        {
            try
            {
                return _db.tblPassP2PSalelookup.Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateAgentRefrenceByOrderNo(Int64 orderNo, string refNo, string note)
        {
            try
            {
                var rec = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderNo);
                rec.AgentReferenceNo = refNo;
                rec.Note = note;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsExistAgentRefrenceNo(Guid siteId, string refNo)
        {
            try
            {
                return _db.tblOrders.Any(x => x.SiteID != siteId && x.AgentReferenceNo == refNo.Trim());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblCurrencyMst getEURCurrencyAndSiteid()
        {
            try
            {
                return _db.tblCurrencyMsts.FirstOrDefault(ty => ty.ShortCode == "EUR");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AffiliateUser(long orderid)
        {
            try
            {
                return _db.tblOrders.Any(t => t.OrderID == orderid && !string.IsNullOrEmpty(t.AffiliateCode) && t.AgentID == Guid.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetOrderPriceSum(long OrderId)
        {
            try
            {
                return (decimal)_db.tblPassSales.Where(ty => ty.OrderID == OrderId).Sum(ty => ty.Price);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<p2p> GetP2PTckP(int ordId)
        {
            try
            {
                var list = (from tppsl in _db.tblPassP2PSalelookup
                            join tps in _db.tblP2PSale on tppsl.PassSaleID equals tps.ID
                            where tppsl.OrderID == ordId
                            select new { tps }).ToList();
                return list.Select(x => new p2p
                    {
                        TicketProtection = x.tps.TicketProtection != null ? (decimal)x.tps.TicketProtection : 0
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblP2PSale> GetP2PCommissionFee(int ordId)
        {
            try
            {
                var list = (from tppsl in _db.tblPassP2PSalelookup
                            join tps in _db.tblP2PSale on tppsl.PassSaleID equals tps.ID
                            where tppsl.OrderID == ordId
                            select new { tps }).ToList();
                return list.Select(x => new tblP2PSale
                {
                    CommissionFee = x.tps.CommissionFee != null ? (decimal)x.tps.CommissionFee : 0
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<passsale> GetPassSale(long ordId)
        {
            try
            {
                Guid Void = Guid.Parse("DE05D0A6-785C-4DA0-B650-CB3CAFCB5330");
                var list = _db.spGetPassSaleData(ordId).ToList();

                return list.Select(ty => new passsale
                {
                    ID = ty.ID,
                    OrderID = ty.OrderID.HasValue ? ty.OrderID.Value : 0,
                    ProductID = (Guid)ty.ProductID,
                    RailPassName = ty.ProductName,
                    SpecialOffer = ty.IsSpecialOffer ? "Yes" : "No",
                    PassDesc = ty.ValidityName,
                    ClassType = ty.ClassName,
                    TrvClass = ty.TravellerName,
                    Price = GetEurailAndNonEurailPrice(ty.ID),
                    CountryName = getpasssaleCountrybyID(ty.ID),
                    TicketProtection = (ty.TicketProtection.HasValue ? ty.TicketProtection.Value : 0),
                    SiteID = ty.SiteID.HasValue ? ty.SiteID.Value : Guid.Empty,
                    TrvlOrderCountry = GetTrvlCountryByCountryID(ty.Country.HasValue ? ty.Country.Value : Guid.Empty),
                    StockNumber = Convert.ToInt64(ty.StockNumber),
                    Commission = (decimal)ty.Commition,
                    Markup = (decimal)ty.MarkUp,
                    IsSaver = ty.IsSaver,
                    SaverID = ty.SaverId.HasValue ? ty.SaverId.Value : Guid.Empty,
                    OrderIdentity = ty.OrderIdentity,
                    SalePrice = ty.Price.Value
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetSaverRefunded(long OrderID)
        {
            try
            {
                var list = (from tps in _db.tblPassSales
                            join todrf in _db.tblOrderRefunds on tps.ID equals todrf.ProductID
                            join ttm in _db.tblTravelerMsts on tps.TravellerID equals ttm.ID
                            where todrf.OrderID == OrderID
                            select new { ttm.EurailCode }).ToList();
                if (list != null && list.Count() > 0)
                    return list.Any(t => t.EurailCode == 51 || t.EurailCode == 53 || t.EurailCode == 74 || t.EurailCode == 75);
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get Eurail Or Non-Eurail price

        public decimal GetEurailData(long orderid)
        {
            try
            {
                decimal Price = 0;
                Guid targetCurrId = FrontEndManagePass.GetCurrencyID("SEU");
                Guid SiteId = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == orderid).SiteID.Value;
                var list = (from tc in _db.tblCategories
                            join tps in _db.tblPassSales on tc.ID equals tps.CategoryID
                            join tod in _db.tblOrders on tps.OrderID equals tod.OrderID
                            join ts in _db.tblSites on tod.SiteID equals ts.ID
                            join tp in _db.tblProducts on tps.ProductID equals tp.ID
                            where tps.OrderID == orderid && tc.IsPrintQueueEnable == true
                            select new { tps.ProductXML, tps.ID, tp.CurrencyID, tod.SiteID, ts.DefaultCurrencyID }).ToList();

                if (list != null && list.Count > 0)
                {
                    var Dataprice = list.Select(ty => new
                    {
                        Price = getActualpriceofProductinpasssale(ty.ID, ty.ProductXML, ty.SiteID, ty.CurrencyID, ty.DefaultCurrencyID),
                    }).ToList();
                    Price = Dataprice.Sum(ty => ty.Price);
                    if (list.Count(ty => ty.CurrencyID != targetCurrId) > 0)
                    {
                        var data = list.FirstOrDefault();
                        decimal Amount = getCurrencyOldExchangeRate(data.ID, targetCurrId, targetCurrId);
                        Price = Price * Amount;
                    }
                }
                Price = decimal.Truncate(Price * 100m) / 100m;
                return Price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetNonEurailData(long orderid)
        {
            try
            {
                decimal Price = 0;
                var list = (from tc in _db.tblCategories
                            join tps in _db.tblPassSales on tc.ID equals tps.CategoryID
                            join tod in _db.tblOrders on tps.OrderID equals tod.OrderID
                            join ts in _db.tblSites on tod.SiteID equals ts.ID
                            join tp in _db.tblProducts on tps.ProductID equals tp.ID
                            where tps.OrderID == orderid && tc.IsPrintQueueEnable == false
                            select new { tps.MarkUp, tps.ID, tps.Commition, tps.ProductXML, tp.CurrencyID, tod.SiteID, ts.DefaultCurrencyID, tps.Price }).ToList();
                if (list != null && list.Count > 0)
                {
                    //refrence bug: http://projects.internationalrail.net/bugtracker/view/project/2/issue/1043
                    var Dataprice = list.Select(ty => new
                    {
                        Price = ty.Price.Value,
                        commition = ty.Commition.Value,
                    }).ToList();
                    Price = Dataprice.Sum(x => x.Price) - Dataprice.Sum(x => x.commition);
                }
                Price = decimal.Truncate(Price * 100m) / 100m;
                return Price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetEurailAndNonEurailPrice(Guid passsaleId)
        {
            try
            {
                decimal Price = 0;
                Guid targetCurrId = FrontEndManagePass.GetCurrencyID("SEU");
                var Data = (from tc in _db.tblCategories
                            join tps in _db.tblPassSales on tc.ID equals tps.CategoryID
                            join tod in _db.tblOrders on tps.OrderID equals tod.OrderID
                            join ts in _db.tblSites on tod.SiteID equals ts.ID
                            join tp in _db.tblProducts on tps.ProductID equals tp.ID
                            where tps.ID == passsaleId
                            select new { tc.IsPrintQueueEnable, tps.ID, tps.ProductID, tps.OrderID, tps.Commition, tps.MarkUp, tps.Price, tps.ProductXML, tp.CurrencyID, tod.SiteID, ts.DefaultCurrencyID }).FirstOrDefault();

                if (Data != null)
                {
                    if (Data.IsPrintQueueEnable)
                    {
                        Price = getActualpriceofProductinpasssale(Data.ID, Data.ProductXML, Data.SiteID, Data.CurrencyID, Data.DefaultCurrencyID);
                        if (Data.CurrencyID != targetCurrId)
                        {
                            decimal Amount = getCurrencyOldExchangeRate(Data.ID, targetCurrId, targetCurrId);
                            Price = Price * Amount;
                        }
                    }
                    else
                    {
                        Price = Data.Price.Value - Data.Commition.Value;
                    }
                }
                Price = decimal.Truncate(Price * 100m) / 100m;
                return Price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public decimal getActualpriceofProductinpasssale(Guid? PassSaleID, string ProductXML, Guid? Siteid, Guid CurrencyID, Guid? DefaultCurrencyID)
        {
            try
            {
                decimal price = 0;
                Guid WSiteid = Siteid.HasValue ? Siteid.Value : Guid.Empty;
                Guid curID = DefaultCurrencyID.HasValue ? DefaultCurrencyID.Value : Guid.Empty;
                if (ProductXML.Contains("OriginalPrice"))
                {
                    XDocument doc = XDocument.Parse(ProductXML);
                    price = Convert.ToDecimal(doc.Descendants("Product").Select(s => new { OriginalProductprice = s.Element("OriginalPrice").Value }).FirstOrDefault().OriginalProductprice);

                    decimal Amount = getCurrencyOldExchangeRate(PassSaleID, DefaultCurrencyID, CurrencyID);//Guid.Parse(curID)
                    price = price * Amount;
                }
                price = decimal.Truncate(price * 100m) / 100m;
                return price;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal getCurrencyOldExchangeRate(Guid? PassSaleId, Guid? ProductCurrency, Guid? SiteCurrency)
        {
            try
            {
                decimal CurrAmount = 1;
                string SiteSybl = string.Empty;
                var data = _db.tblCurrencyMsts.FirstOrDefault(t => t.ID == ProductCurrency);
                if (data != null)
                {
                    var OrderSiteSymbol = _db.tblCurrencyMsts.FirstOrDefault(t => t.ID == SiteCurrency);
                    if (OrderSiteSymbol != null && data.ShortCode == OrderSiteSymbol.ShortCode)
                    {
                        SiteSybl = "SITE";
                    }
                    var amount = _db.tblPassSales.FirstOrDefault(t => t.ID == PassSaleId); ;
                    if (amount != null)
                    {

                        if (data.ShortCode.ToUpper().Contains("USD"))
                        {
                            CurrAmount = amount.USD_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("GBB"))
                        {
                            CurrAmount = amount.GBB_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("SEU"))
                        {
                            CurrAmount = amount.SEU_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("SBD"))
                        {
                            CurrAmount = amount.SBD_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("GBP"))
                        {
                            CurrAmount = amount.GBP_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("EUR"))
                        {
                            CurrAmount = amount.EUR_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("INR"))
                        {
                            CurrAmount = amount.INR_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("SEK"))
                        {
                            CurrAmount = amount.SEK_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("NZD"))
                        {
                            CurrAmount = amount.NZD_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("CAD"))
                        {
                            CurrAmount = amount.CAD_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("JPY"))
                        {
                            CurrAmount = amount.JPY_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("AUD"))
                        {
                            CurrAmount = amount.AUD_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("CHF"))
                        {
                            CurrAmount = amount.CHF_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("THB"))
                        {
                            CurrAmount = amount.THB_MP;
                        }
                        else if (data.ShortCode.ToUpper().Contains("SGD"))
                        {
                            CurrAmount = amount.SGD_MP;
                        }
                        else if (SiteSybl.ToUpper().Contains("SITE"))
                        {
                            CurrAmount = amount.Site_MP;
                        }
                    }
                }
                //CurrAmount = decimal.Truncate(CurrAmount * 100m) / 100m;
                return CurrAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetConvertMarkUpPrice(Guid? PassSaleId, decimal? Markup, Guid? ProductCurrId, Guid? SiteCurrid)
        {
            try
            {
                decimal Markupval = Markup.HasValue ? Markup.Value : 0;
                Guid SiteCurridval = SiteCurrid.HasValue ? SiteCurrid.Value : Guid.Empty;
                Guid ProductCurrIdval = ProductCurrId.HasValue ? ProductCurrId.Value : Guid.Empty;

                decimal amount = getCurrencyOldExchangeRate(PassSaleId, ProductCurrId, SiteCurrid);

                return amount * Markupval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OrderList GetOrderById(long id)
        {
            try
            {
                List<OrderList> list = new List<OrderList>();
                var olist = (from to in _db.tblOrders
                             join s in _db.tblSites on to.SiteID equals s.ID
                             join st in _db.tblOrderStatus on to.Status equals st.ID
                             join ps in _db.tblPassP2PSalelookup on to.OrderID equals ps.OrderID
                             where to.OrderID == id && ps.ProductType != "P2P"
                             select new { s, to, st, ps }).ToList();


                if (olist != null && olist.Count() > 0)
                    list = olist.Select(x => x.ps.PassSaleID != null ? new OrderList
                    {
                        AdminFee = x.to.AdminFee,
                        OrderStatusID = x.to.Status,
                        OrderID = x.to.OrderID,
                        ShippingAmount = x.to.ShippingAmount ?? 0,
                        SiteID = x.to.SiteID,
                        OrderStatus = x.st.Name,
                        SiteName = x.s.DisplayName,
                        IpAddress = x.to.IpAddress,
                        CreatedOn = x.to.CreatedOn,
                        CurrencyIDbySiteID = x.s.DefaultCurrencyID,
                        PaymentDate = x.to.PaymentDate,
                        DateOfDepart = x.to.DateOfDepart,
                        PassStartDate = GetTopPassDate(x.to.OrderID),
                        AgentID = x.to.AgentID,
                        BookingFee = x.to.BookingFee != null ? (decimal)x.to.BookingFee : 0,
                        Note = x.to.Note,
                        AgentReferenceNo = x.to.AgentReferenceNo
                    } : null).ToList();
                return list != null ? list.FirstOrDefault() : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetTopPassDate(long ordID)
        {
            try
            {
                var result = (from ps in _db.tblPassP2PSalelookup
                              join ot in _db.tblOrderTravellers on ps.OrderTravellerID equals ot.ID
                              where ps.OrderID == ordID
                              select new { ot }).ToList();
                if (result != null && result.Count > 0)
                    return result.LastOrDefault().ot.PassStartDate.ToString();
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateDateofDepart(long OrderID)
        {
            try
            {
                var list = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderID);
                if (list != null)
                    list.DateOfDepart = DateTime.Now;
                _db.SaveChanges();
                return list.DateOfDepart.Value.ToString("dd, MMM yyyy");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetstockallocationHistory(long OrderID)
        {
            try
            {
                Guid Void = Guid.Parse("DE05D0A6-785C-4DA0-B650-CB3CAFCB5330");
                Guid Printed = Guid.Parse("AF1D088B-03B9-4657-B231-C2787BA53425");
                var list = (from tpqi in _db.tblPrintQueueItems
                            join tsq in _db.tblStockQueues on tpqi.QueueID equals tsq.ID
                            where tpqi.OrderID == OrderID && tpqi.Status != Void
                            select new { tsq.PrinterCode, tpqi.Status, tsq.BranchID }).ToList();
                if (list != null && list.Count > 0)
                {
                    if (list.FirstOrDefault().Status == Printed)
                        return "Order in printed <br/>" + _ManagePrintQueue.GetOfficeName(list.FirstOrDefault().BranchID) + ".";
                    else
                        return "Order in print queue <br/>" + _ManagePrintQueue.GetOfficeName(list.FirstOrDefault().BranchID) + ".";
                }
                return "No Ticket in Printing Process.";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetAllowedPrinting(long OrderID)
        {
            try
            {
                var AllowedPrinting = _db.tblPassSales.Join(_db.tblCategories, tps => tps.CategoryID, tc => tc.ID, (tps, tc) => new { tps.OrderID, tc.IsBritRailPass, tc.IsInterRailPass, tc.IsPrintQueueEnable }).Any(x => x.OrderID == OrderID && (x.IsBritRailPass || x.IsInterRailPass || x.IsPrintQueueEnable));
                return AllowedPrinting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetTrvlCountryByCountryID(Guid countryID)
        {
            try
            {
                return _db.tblCountriesMsts.FirstOrDefault(ty => ty.CountryID == countryID).CountryName;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getpasssaleCountrybyID(Guid pID)
        {
            try
            {
                return _db.GetCountriesNameByPassSaleID(pID).FirstOrDefault().country;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCountryName(Guid id)
        {
            try
            {
                var passCnt = _db.tblPassSales.FirstOrDefault(x => x.ID == id);
                if (passCnt != null)
                {
                    var cid = (Guid)passCnt.Country1;
                    var tbl = _db.tblProductEurailCountires.FirstOrDefault(x => x.EurailCountryID == cid);
                    if (tbl != null)
                    {
                        var country = tbl.Country;
                        return country;
                    }
                    return null;
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrderTraveller> GetTraveller(long ordId)
        {
            try
            {
                var olist = (from to in _db.tblOrderTravellers
                             join pl in _db.tblPassP2PSalelookup on to.ID equals pl.OrderTravellerID
                             join ps in _db.tblPassSales on pl.PassSaleID equals ps.ID
                             join ct in _db.tblCountriesMsts on to.Country equals ct.CountryID
                             where pl.OrderID == ordId
                             select new { to, ct, ps }).ToList();

                var list = olist.Select(x => new OrderTraveller
                {
                    ID = x.to.ID,
                    Title = x.to.Title,
                    FirstName = x.to.FirstName,
                    MiddleName = x.to.MiddleName,
                    LastName = x.to.LastName,
                    CountryName = x.ct.CountryName,
                    PassportNo = x.to.PassportNo,
                    Nationality = x.to.Nationality,
                    PassStartDate = x.to.PassStartDate,
                    LeadPassenger = x.to.LeadPassenger,
                    PassSalesId = x.ps.ID,
                    OrderIdentity = x.to.OrderIdentity,
                    DOB = x.to.DOB,
                }).ToList().OrderBy(x => x.TicketNumber);

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblOrder> GetOrderCreditCardInfo(long ordId)
        {
            try
            {
                var list = _db.tblOrders.Where(x => x.OrderID == ordId).ToList();
                return list.Select(ty => new tblOrder
                {
                    CardholderName = ty.CardholderName,
                    CardNumber = ty.CardNumber,
                    PaymentId = ty.PaymentId,
                    Brand = ty.Brand,
                    ExpDate = ty.ExpDate
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AgentDetails> GetAgentDetails(long ordId)
        {
            try
            {
                var result = (from ord in _db.tblOrders
                              join agent in _db.tblAdminUsers
                                  on ord.AgentID equals agent.ID
                              join branch in _db.tblBranches
                                  on agent.BranchID equals branch.ID
                              where ord.OrderID == ordId
                              select new { agent, branch }).ToList();

                return result.Select(x => new AgentDetails
                {
                    Name = x.agent.Forename + ' ' + x.agent.Surname,
                    UserName = x.agent.UserName,
                    Office = GetSingleOfficeTreeId(x.branch.ID)
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSingleOfficeTreeId(Guid id)
        {
            try
            {
                var list = _db.tblBranches.FirstOrDefault(ty => ty.ID == id);
                if (list != null)
                    return GetSingleOfficeList(list.TreeID);
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSingleOfficeList(int treeid)
        {
            try
            {
                string name = "";
                var list = _db.tblBranches.FirstOrDefault(ty => ty.TreeID == treeid);
                if (list != null)
                {
                    name = GetSingleOfficeList(list.TreeParentBranchID.HasValue ? list.TreeParentBranchID.Value : 0) + " - " + list.OfficeName;
                }
                return name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Convertdateofexpdate(string date, DateTime CreateDate)
        {
            try
            {
                string datestr = "";
                if (!string.IsNullOrEmpty(date) ? date.Length == 4 : false)
                {
                    int month = Convert.ToInt32(date.Substring(0, 2));
                    int year = Convert.ToInt32(CreateDate.Year.ToString().Substring(0, 2) + date.Substring(2, 2));
                    datestr = new DateTime(year, month, 1).ToString("MMM, yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                return datestr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<getordershippingandbillingInfo> GetBillingInfo(long ordId)
        {
            try
            {
                return _db.tblOrderBillingAddresses.Where(x => x.OrderID == ordId).Select(ty => new getordershippingandbillingInfo
                {
                    Title = ty.Title,
                    TitleShpg = ty.TitleShpg,
                    Address1 = ty.Address1,
                    Address1Shpg = ty.Address1Shpg,
                    Address2 = ty.Address2,
                    Address2Shpg = ty.Address2Shpg,
                    City = ty.City,
                    CityShpg = ty.CityShpg,
                    Country = ty.Country,
                    CountryShpg = ty.CountryShpg,
                    DateOfDepart = ty.tblOrder.DateOfDepart,
                    EmailAddress = ty.EmailAddress,
                    EmailAddressShpg = ty.EmailAddressShpg,
                    FirstName = ty.FirstName,
                    FirstNameShpg = ty.FirstNameShpg,
                    ID = ty.ID,
                    LastName = ty.LastName,
                    LastNameShpg = ty.LastNameShpg,
                    OrderID = ty.OrderID,
                    Postcode = ty.Postcode,
                    PostcodeShpg = ty.PostcodeShpg,
                    State = ty.State,
                    StateShpg = ty.StateShpg,
                    Phone = ty.Phone,
                    PhoneShpg = ty.PhoneShpg,
                    IsVisibleEmailAddress = ty.IsVisibleEmailAddress
                }).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<ClsP2PDeliveryInfomation> GetP2PdelivaryInfo(int ordId)
        {
            try
            {
                return (from ord in _db.tblP2PDeliveryDetails
                        join country in _db.tblCountriesMsts
                            on ord.CountryID equals country.CountryID
                        join st in _db.tblCountyMsts on ord.StateID equals st.CountyId into state
                        from county in state.DefaultIfEmpty()
                        where ord.OrderID == ordId
                        select new ClsP2PDeliveryInfomation
                        {
                            FirstName = ord.FirstName,
                            LastName = ord.LastName,
                            Department = ord.Department,
                            Address = ord.Address,
                            City = ord.City,
                            Postcode = ord.Postcode,
                            Country = country.CountryName,
                            State = county.CountyId != 0 ? county.County : ""
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblOrder> BindOrderId(Guid siteId)
        {
            try
            {
                return _db.tblOrders.Where(x => x.SiteID == siteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblOrder> BindAgentOrderId(Guid siteId)
        {
            try
            {
                return _db.tblOrders.Where(x => x.SiteID == siteId && x.AgentID != new Guid()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblOrderStatu> BindOrderStatus()
        {
            try
            {
                return _db.tblOrderStatus.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCurrcyBySiteId(Guid SiteId)
        {

            var data = (from tc in _db.tblSites
                        join Cu in _db.tblCurrencyMsts
                        on tc.DefaultCurrencyID equals Cu.ID
                        where tc.ID == SiteId
                        select Cu.HTMLCode).ToArray();
            return Regex.Replace(data[0], "<.*?>", string.Empty);
        }

        public List<GetOrderList> GetOrderList(string Siteid, string Orderno, string PassTypeCodet, string Travellername, string STAReferenceCode, string Stockno, string Orderstatus, Guid? AdminId, string officeID, DateTime? StartDate, DateTime? LastDate, int? DateType, int pageIndex, int pageSize)
        {
            try
            {
                List<GetOrderList> list = _db.GetOrderList(Siteid, Orderno, PassTypeCodet, Travellername, STAReferenceCode, Stockno, Orderstatus, AdminId, officeID, StartDate, LastDate, DateType, pageIndex, pageSize).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SpP2PReport> P2PReport(string siteid, DateTime? date1, DateTime? date2, int PageNo, int RecordsPerPage, string AgentId, string Supplier, string OrderNo, string AgentReference, string Station, Guid LoginAgentId)
        {
            try
            {
                return _db.SpP2PReport(siteid, date1, date2, PageNo, RecordsPerPage, AgentId, Supplier, OrderNo, AgentReference, Station, LoginAgentId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getAffiliateUserCode(Guid AffiliID)
        {
            try
            {
                return _db.tblAffiliateUsers.FirstOrDefault(ty => ty.ID == AffiliID).Code;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AgentOrder> GetAgentOrderList(Guid siteId, DateTime DFrom, DateTime DTo, Boolean IsSta)
        {
            try
            {
                var list = _db.AgentOrder(siteId, DFrom, DTo, IsSta).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrderList> GetUserOrderList(Guid siteId)
        {
            try
            {
                var olist = (from tOrd in _db.tblOrders
                             join st in _db.tblOrderStatus on tOrd.Status equals st.ID
                             join plook in _db.tblPassP2PSalelookup on tOrd.OrderID equals plook.OrderID
                             where tOrd.SiteID == siteId && tOrd.UserID != new Guid()
                             select new { tOrd, st, plook }).ToList();

                var list = olist.Select(x => new OrderList
                {
                    Price = GetPrice(x.tOrd.OrderID),
                    UserID = x.tOrd.UserID,
                    OrderID = x.tOrd.OrderID,
                    OrderStatus = x.st.Name,
                    OrderStatusID = x.tOrd.Status,
                    TravellerName = GetTravellerName(x.tOrd.OrderID),
                    CreatedOn = x.tOrd.CreatedOn,
                    AgntAfftName = GetUserName(x.tOrd.UserID),
                }).ToList().GroupBy
                    (y => new { y.Price, y.UserID, y.OrderID, y.OrderStatus, y.OrderStatusID, y.TravellerName, y.CreatedOn, y.AgntAfftName }).Select
                    (z => new OrderList { Price = z.Key.Price, UserID = z.Key.UserID, OrderID = z.Key.OrderID, OrderStatus = z.Key.OrderStatus, OrderStatusID = z.Key.OrderStatusID, TravellerName = z.Key.TravellerName, CreatedOn = z.Key.CreatedOn, AgntAfftName = z.Key.AgntAfftName }).OrderByDescending(x => x.CreatedOn).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetCRMOrderDetailList> GetCRMOrderDetailList(Guid Siteid, Guid? UserID, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize)
        {
            try
            {
                List<GetCRMOrderDetailList> list = _db.GetCRMOrderDetailList(Siteid, UserID, FromDate, ToDate, pageIndex, pageSize).ToList();
                return list;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<GetUserOrderList> GetUserOrderList(Guid Siteid, Guid? UserID, string OrderID, int pageIndex, int pageSize, string OrderStatus, bool RequestCancelOrder)
        {
            try
            {
                List<GetUserOrderList> list = _db.GetUserOrderList(Siteid, UserID, OrderID, pageIndex, pageSize, OrderStatus, RequestCancelOrder).ToList();
                return list;
            }
            catch (Exception ex) { throw ex; }
        }

        public List<AffiliateOrder> GetAffilliatedOrderList(Guid siteId, DateTime DFrom, DateTime DTo, Boolean IsSta)
        {
            try
            {
                return _db.AffiliateOrder(siteId, DFrom, DTo, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetPrice(long id)
        {
            decimal price = 0;
            var res = (from t1 in _db.tblOrders
                       join t2 in _db.tblPassSales on t1.OrderID equals t2.OrderID
                       where t1.OrderID == id
                       select new { t2.Price }).ToList();
            foreach (var data in res)
            {
                price = Convert.ToDecimal(data.Price != 0 ? data.Price : 0) + price;
            }
            return price;
        }

        public string GetUserName(Guid? id)
        {
            try
            {
                var res = (from tOrd in _db.tblOrders
                           join Username in _db.tblUserLogins on tOrd.UserID equals Username.ID
                           select (Username)).ToList();
                var data = res.FirstOrDefault(ty => ty.ID == id);
                return data == null ? "" : data.FirstName + " " + data.LastName + "(User)";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetAgentCommission(long Orderid)
        {
            try
            {
                var res = _db.tblPassSales.Where(t => t.OrderID == Orderid && t.Commition != null).ToList();
                if (res != null && res.Count() > 0)
                    return Convert.ToDecimal(res.Sum(a => a.Commition));
                else
                    return 0;
                return (decimal)_db.tblPassSales.Where(t => t.OrderID == Orderid && t.Commition != null).Sum(x => x.Commition);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetAllBranchList_Result> GetAllBranchlist()
        {
            try
            {
                return _db.GetAllBranchList().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetAllBranchListBySite_Result> GetAllBranchlistBySiteId(Guid SiteId)
        {
            try
            {
                return _db.GetAllBranchListBySiteId(SiteId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AgentEurailReport> AgentEurailReport(Guid? siteid, string branchid, DateTime? date1, DateTime? date2, Boolean IsSta)
        {
            try
            {
                return _db.AgentEurailReport(siteid, branchid, date1, date2, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CardRevenueReport> CardRevenueReport(Guid? siteid, DateTime? date1, DateTime? date2, int PageNo, int RecordsPerPage, string OrderNo, string productCategory, string cardholderName, string cardType, Boolean IsSta)
        {
            try
            {
                return _db.CardRevenueReport(siteid, date1, date2, PageNo, RecordsPerPage, OrderNo, productCategory, cardholderName, cardType, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AgentNonEurailReport> AgentNonEurailReport(Guid? siteid, string branchid, DateTime? date1, DateTime? date2, Boolean IsSta)
        {
            try
            {
                return _db.AgentNonEurailReport(siteid, branchid, date1, date2, IsSta).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetTravellerName(long id)
        {
            string result = string.Empty;
            var res = (from pl in _db.tblPassP2PSalelookup
                       join ot in _db.tblOrderTravellers
                       on pl.OrderTravellerID equals ot.ID
                       where pl.OrderID == id
                       orderby ot.FirstName, ot.LastName
                       select new { ot.FirstName, ot.LastName }).ToList();
            if (res != null)
                foreach (var item in res)
                {
                    result += "- " + item.FirstName + " " + item.LastName + "<br/>";
                }
            return result;
        }

        public int TotalNumberOfRecord()
        {
            return _db.tblOrders.Count();
        }

        public List<tblManualP2PCommition> GetManualP2PCommitionbySiteid(Guid siteId)
        {
            try
            {
                //return _db.tblManualP2PCommition.Where(t => t.IsDeleted == false).ToList();
                var data = (from mc in _db.tblManualP2PCommition
                            join mclkp in _db.tblManualP2PComSiteLookUp
                                on mc.Id equals mclkp.P2PCommissionID
                            where mc.IsDeleted == false && mclkp.SiteID == siteId
                            select mc).ToList();
                return data.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Refund
        public RefundDetails GetRefundDetails(Guid prdId)
        {
            try
            {
                var olist = (from ps in _db.tblPassSales
                             join ord in _db.tblOrders on ps.OrderID equals ord.OrderID
                             join pn in _db.tblProductNames on ps.ProductID equals pn.ProductID
                             join pl in _db.tblPassP2PSalelookup on ps.ID equals pl.PassSaleID
                             join tr in _db.tblOrderTravellers on pl.OrderTravellerID equals tr.ID
                             join pv in _db.tblProductValidUpToNames on ps.ValidityID equals pv.ID
                             join tv in _db.tblTravelerMsts on ps.TravellerID equals tv.ID
                             join cl in _db.tblClassMsts on ps.ClassID equals cl.ID
                             join prd in _db.tblProducts on ps.ProductID equals prd.ID
                             where ps.ID == prdId
                             select new { ps, ord, pn, tr, pv, tv, cl, prd }).ToList();

                var list = olist.Select(x => x.ps.TicketProtection != null ? (x.ps.Price != null ? new RefundDetails
                {
                    SiteID = x.ord.SiteID.HasValue ? x.ord.SiteID.Value : Guid.Empty,
                    OrderID = Convert.ToInt64(x.ps.OrderID),
                    ProductName = x.pn.Name + ',' + ' ' + x.pv.Name + ' ' + x.tv.Name + ' ' + x.cl.Name,
                    TravellerName = x.tr.FirstName + ' ' + x.tr.LastName,
                    Price = GetAllSaverandSimplePassPrice(x.ps.OrderID, x.ps.ID, x.ps.Price),
                    TckProtection = (decimal)x.ps.TicketProtection,
                    CurrencyID = x.prd.CurrencyID
                } : null) : null).ToList();
                return list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetAllSaverandSimplePassPrice(decimal? OrderId, Guid PSId, decimal? Price)
        {
            try
            {
                var list = (from a in _db.tblPassSales
                            join b in _db.tblTravelerMsts on a.TravellerID equals b.ID
                            where a.OrderID == OrderId
                            select new { a.ID, a.Price, b.EurailCode }).ToList();
                if (list != null && list.Count() > 0)
                {
                    var slist = list.Where(ty => ty.EurailCode == 51 || ty.EurailCode == 53 || ty.EurailCode == 74 || ty.EurailCode == 75).ToList();
                    if (slist != null)
                    {
                        var sumlist = slist.Where(ty => ty.ID == PSId);
                        if (sumlist != null && sumlist.Count() > 0)
                            Price = slist.Sum(ty => ty.Price);
                    }
                }
                return Price.HasValue ? Price.Value : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblRefundLookup> BindRefundPerc()
        {
            try
            {
                return _db.tblRefundLookups.OrderBy(x => x.Order).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetIsRefunded(Guid prdId)
        {
            try
            {
                return _db.tblOrderRefunds.Any(x => x.ProductID == prdId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblRefundLookup GetRefundValue(Guid id)
        {
            try
            {
                return _db.tblRefundLookups.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPdfFileURL(Guid id)
        {
            try
            {
                var list = (from tpsl in _db.tblPassP2PSalelookup
                            join tp2p in _db.tblP2PSale on tpsl.PassSaleID equals tp2p.ID
                            where tpsl.ID == id
                            select new { URL = tp2p.PdfURL }).ToList().FirstOrDefault();
                if (list != null)
                    return list.URL;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetBENEPdfURLByOrderId(Int32 OrderID)
        {
            try
            {
                var data = (from tppsl in _db.tblPassP2PSalelookup
                            join tps in _db.tblP2PSale on tppsl.PassSaleID equals tps.ID
                            where tppsl.OrderID == OrderID
                            select new { tps.PdfURL }).Where(x => x.PdfURL != null).FirstOrDefault();
                if (data != null)
                    return data.PdfURL;
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetURLByOrderId(Guid PassSaleId)
        {
            try
            {
                var data = _db.tblP2PSale.FirstOrDefault(x => x.ID == PassSaleId);
                if (data != null)
                {
                    string PdfUrls = "";
                    if (!string.IsNullOrEmpty(data.PdfURL))
                    {
                        string[] urls = data.PdfURL.Split('#');
                        foreach (var url in urls)
                        {
                            if (!string.IsNullOrEmpty(url) && url.Length > 32)
                                PdfUrls += "<a href='" + url + "' class='heading-color' target='_blank'>Print Ticket</a><br/>";
                        }
                    }
                    return PdfUrls;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex) { throw ex; }
        }

        public bool IsHundradPercentRefund(Guid ID, long orderid)
        {
            try
            {
                bool result = false;
                var data = _db.tblPassP2PSalelookup.FirstOrDefault(t => t.PassSaleID == ID && t.OrderID == orderid);
                if (data != null)
                {
                    var rec = _db.tblOrderRefunds.FirstOrDefault(t => t.ProductID == data.ID && t.OrderID == orderid);
                    result = rec == null ? false : rec.TotalProductRefundPercentage == Convert.ToDecimal(100);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddRefund(tblOrderRefund oRefund)
        {
            try
            {
                var list = _db.tblOrderRefunds.FirstOrDefault(t => t.ProductID == oRefund.ProductID && t.OrderID == oRefund.OrderID);
                if (list == null)
                    _db.AddTotblOrderRefunds(oRefund);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUpdateRefund(tblOrderRefund oRefund)
        {
            try
            {
                bool Valid = false;
                var list = _db.tblOrderRefunds.FirstOrDefault(t => t.ProductID == oRefund.ProductID);
                if (list == null)
                {
                    _db.AddTotblOrderRefunds(oRefund);
                    Valid = true;
                }
                else if (oRefund.OrginalProductAmount >= oRefund.TotalProductRefund + list.TotalProductRefund)
                {
                    list.TotalProductRefund = list.TotalProductRefund + oRefund.TotalProductRefund;
                    list.TotalProductRefundPercentage = list.TotalProductRefundPercentage + oRefund.TotalProductRefundPercentage;
                    Valid = true;
                }
                if (Valid)
                {
                    tblOrderRefundsLookUp lokkup = new tblOrderRefundsLookUp
                    {
                        ID = Guid.NewGuid(),
                        OrderID = oRefund.OrderID,
                        UserID = oRefund.UserID,
                        AdminUserID = oRefund.AdminUserID,
                        ProductID = oRefund.ProductID,
                        ProductPriceID = oRefund.ProductPriceID,
                        TotalProductRefund = oRefund.TotalProductRefund,
                        TotalProductRefundPercentage = oRefund.TotalProductRefundPercentage,
                        DateTimeStamp = DateTime.Now
                    };
                    _db.AddTotblOrderRefundsLookUps(lokkup);
                }
                _db.SaveChanges();
            }
            catch (Exception ex) { throw ex; }
        }

        public List<RefundData> GetRefundDetails(long ordId)
        {
            try
            {
                var result = (from refund in _db.tblOrderRefunds
                              join ps in _db.tblPassSales on refund.ProductID equals ps.ID
                              join pn in _db.tblProductNames on ps.ProductID equals pn.ProductID
                              join pv in _db.tblProductValidUpToNames on ps.ValidityID equals pv.ID
                              join tv in _db.tblTravelerMsts on ps.TravellerID equals tv.ID
                              join cl in _db.tblClassMsts on ps.ClassID equals cl.ID
                              join pl in _db.tblPassP2PSalelookup on ps.ID equals pl.PassSaleID
                              join tr in _db.tblOrderTravellers on pl.OrderTravellerID equals tr.ID
                              join user in _db.tblAdminUsers on refund.AdminUserID equals user.ID
                              where refund.OrderID == ordId
                              select new { refund, pn, pv, tv, cl, tr, user }).OrderBy(x => x.tr.OrderIdentity).ToList();

                return result.Select(x => new RefundData
                {
                    PassSalesId = (x.refund.ProductID.HasValue ? x.refund.ProductID.Value : Guid.Empty),
                    Product = x.pn.Name + ',' + ' ' + x.pv.Name + ' ' + x.tv.Name + ' ' + x.cl.Name,
                    Traveller = x.tr.Title + ' ' + x.tr.FirstName + ' ' + x.tr.LastName,
                    User = x.user.Forename + ' ' + x.user.Surname,
                    Price = (decimal)x.refund.OrginalProductAmount,
                    Refund = (decimal)x.refund.TotalProductRefund,
                    RefundDate = (DateTime)x.refund.RefundDateTime
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RefundInfoLIst> GetRefundDetailsByOrderId(long ordId)
        {
            try
            {
                var result = (from refund in _db.tblOrderRefunds
                              join ps in _db.tblPassSales on refund.ProductID equals ps.ID
                              join pn in _db.tblProductNames on ps.ProductID equals pn.ProductID
                              join pv in _db.tblProductValidUpToNames on ps.ValidityID equals pv.ID
                              join tv in _db.tblTravelerMsts on ps.TravellerID equals tv.ID
                              join cl in _db.tblClassMsts on ps.ClassID equals cl.ID
                              join pl in _db.tblPassP2PSalelookup on ps.ID equals pl.PassSaleID
                              join tr in _db.tblOrderTravellers on pl.OrderTravellerID equals tr.ID
                              join user in _db.tblAdminUsers on refund.AdminUserID equals user.ID
                              join to in _db.tblOrders on pl.OrderID equals to.OrderID
                              join ts in _db.tblSites on to.SiteID equals ts.ID
                              where refund.OrderID == ordId
                              select new { refund, ps, pn, pv, tv, cl, tr, user, to, ts }).OrderBy(x => x.tr.OrderIdentity).ToList();

                return result.Select(x => new RefundInfoLIst
                {
                    ProductID = (x.refund.ProductID.HasValue ? x.refund.ProductID.Value : Guid.Empty),
                    ProductName = x.pn.Name,
                    ProductValidUpToName = x.pv.Name,
                    TravellerType = x.tv.Name,
                    Class = x.cl.Name,
                    TravellerName = x.tr.Title + ' ' + x.tr.FirstName + ' ' + x.tr.LastName,
                    User = x.user.Forename + ' ' + x.user.Surname,
                    Price = (decimal)x.refund.OrginalProductAmount,
                    Refund = (decimal)x.refund.TotalProductRefund,
                    RefundDate = (DateTime)x.refund.RefundDateTime,
                    PassSaleID = x.ps.ID,
                    PassStartDate = (DateTime)x.tr.PassStartDate,
                    TicketProtection = x.ps.TicketProtection.HasValue ? x.ps.TicketProtection.Value : 0,
                    CancellationFee = x.refund.Cancellation.Value ? x.refund.CancellationFee.Value : 0,
                    SiteId = x.ts.ID,
                    SiteName = x.ts.DisplayName,
                    SiteUrl = x.ts.SiteURL,
                    OrderId = x.to.OrderID,
                    IsAgentSite = x.ts.IsAgent.Value
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblOrder GetOrderbyOrdId(long ordId)
        {
            try
            {
                return _db.tblOrders.FirstOrDefault(x => x.OrderID == ordId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class RefundData
        {
            public string Product { get; set; }
            public Guid PassSalesId { get; set; }
            public string Traveller { get; set; }
            public string User { get; set; }
            public decimal Price { get; set; }
            public decimal Refund { get; set; }
            public DateTime RefundDate { get; set; }
        }

        public class RefundDetails
        {
            public Guid SiteID { get; set; }
            public long OrderID { get; set; }
            public string ProductName { get; set; }
            public string TravellerName { get; set; }
            public decimal Price { get; set; }
            public decimal TckProtection { get; set; }
            public Guid? CurrencyID { get; set; }
        }

        public class RefundInfoLIst
        {
            public string ProductName { get; set; }
            public Guid ProductID { get; set; }
            public Guid PassSaleID { get; set; }
            public Guid SiteId { get; set; }
            public string SiteName { get; set; }
            public string SiteUrl { get; set; }
            public string ProductValidUpToName { get; set; }
            public string TravellerType { get; set; }
            public string Class { get; set; }
            public string TravellerName { get; set; }
            public string User { get; set; }
            public decimal Price { get; set; }
            public decimal Refund { get; set; }
            public decimal TicketProtection { get; set; }
            public decimal CancellationFee { get; set; }
            public DateTime RefundDate { get; set; }
            public DateTime PassStartDate { get; set; }
            public long OrderId { get; set; }
            public bool IsAgentSite { get; set; }
        }
        #endregion

        #region Data Dump Operation
        public List<spPartnersReportOther_Result> PartnersReportOther()
        {
            try
            {
                return _db.PartnersReportOther().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Order Attachments
        public bool AddAttachments(tblOrderAttachment attachment)
        {
            try
            {
                _db.tblOrderAttachments.AddObject(attachment);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool AddMultipleAttachments(List<tblOrderAttachment> attachment)
        {
            try
            {
                foreach (var item in attachment)
                {
                    _db.tblOrderAttachments.AddObject(item);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw ex; }
        }
        public bool RemoveAttachments(Int32 attachemntid)
        {
            try
            {
                var result = _db.tblOrderAttachments.FirstOrDefault(x => x.ID == attachemntid);
                if (result != null)
                {
                    _db.tblOrderAttachments.DeleteObject(result);
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<tblOrderAttachment> GetOrderAttachments(Int32 orderid)
        {
            try
            {
                return _db.tblOrderAttachments.Where(x => x.OrderID == orderid).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool IsOrderAttachmentExist(Int32 orderid)
        {
            try
            {
                return _db.tblOrderAttachments.Any(x => x.OrderID == orderid);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region OrderNotes
        public List<tblOrderNote> GetOrderNotesList(int ordId)
        {
            return _db.tblOrderNotes.Where(x => x.OrderID == ordId).ToList();
        }

        public bool DeleteOrderNotes(Guid id)
        {
            try
            {
                var rec = _db.tblOrderNotes.FirstOrDefault(x => x.ID == id);
                _db.tblOrderNotes.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblOrderNote GetOrderNotesById(Guid id)
        {
            try
            {
                return _db.tblOrderNotes.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Guid AddEditOrderNotes(tblOrderNote orderNote)
        {
            try
            {
                var id = orderNote.ID == Guid.Empty ? Guid.NewGuid() : orderNote.ID;
                if (orderNote.ID == Guid.Empty)
                {
                    _db.tblOrderNotes.AddObject(new tblOrderNote
                    {
                        ID = id,
                        OrderID = orderNote.OrderID,
                        UserName = orderNote.UserName,
                        Notes = orderNote.Notes,
                        CreatedOn = orderNote.CreatedOn
                    });
                }
                else
                {
                    var rec = _db.tblOrderNotes.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.OrderID = orderNote.OrderID;
                        rec.UserName = orderNote.UserName;
                        rec.Notes = orderNote.Notes;
                        rec.ModifiedOn = orderNote.ModifiedOn;
                    }
                }

                _db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public List<AgentDetails> GetAgentDetails(Guid siteId, Guid officeId)
        {
            try
            {
                List<Guid> rst = _db.GetAllBranchListBySiteId(siteId).Select(x => x.ID).ToList();

                List<AgentDetails> result = (from au in _db.tblAdminUsers
                                             where _db.tblAdminUserLookupSites.Any(x => x.AdminUserID == au.ID && x.SiteID == siteId) && ((au.Forename != null && au.Forename != "") || (au.Surname != null && au.Surname != ""))
                                             && rst.Contains(au.BranchID.HasValue ? au.BranchID.Value : Guid.Empty)
                                             select new AgentDetails
                                             {
                                                 Id = au.ID,
                                                 Name = au.Forename + " " + au.Surname,

                                                 BranchId = (au.BranchID.HasValue ? au.BranchID.Value : Guid.Empty)
                                             }).ToList();

                if (officeId != Guid.Empty)
                {
                    result = result.Where(x => x.BranchId == officeId).ToList();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrderTraveller> GetP2PTraveller(int ordId)
        {
            try
            {
                var olist = (from ptp in _db.tblP2PSale
                             join ptpl in _db.tblPassP2PSalelookup on ptp.ID equals ptpl.PassSaleID
                             join to in _db.tblOrderTravellers on ptpl.OrderTravellerID equals to.ID
                             join ct in _db.tblCountriesMsts on to.Country equals ct.CountryID
                             where ptpl.OrderID == ordId
                             select new { ct, to }).ToList();
                var list = olist.Select(x => new OrderTraveller
                {
                    Title = x.to.Title,
                    FirstName = x.to.FirstName,
                    LastName = x.to.LastName,
                    CountryName = x.ct.CountryName
                }).ToList().OrderBy(x => x.TicketNumber);
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<p2ppasssale> Getp2psale(long OrderId)
        {
            try
            {
                var list = (from ptp in _db.tblP2PSale
                            join ptpl in _db.tblPassP2PSalelookup on ptp.ID equals ptpl.PassSaleID
                            join tod in _db.tblOrders on ptpl.OrderID equals tod.OrderID
                            join tods in _db.tblOrderStatus on tod.Status equals tods.ID
                            where ptpl.OrderID == OrderId && ptpl.ProductType == "P2P"
                            select new { ptp, ptpl, tods, tod }).Select(x => new p2ppasssale
                            {
                                ID = x.ptpl.ID,
                                SiteID = x.tod.SiteID.HasValue ? x.tod.SiteID.Value : Guid.Empty,
                                Status = x.tods.Name,
                                CreatedOn = x.tod.CreatedOn,
                                OrderID = x.ptpl.OrderID.HasValue ? x.ptpl.OrderID.Value : 0,
                                TrainNo = x.ptp.TrainNo,
                                PNRNo = "",
                                IpAddress = x.tod.IpAddress,
                                SiteName = x.tod.tblSite.DisplayName,
                                From = x.ptp.From,
                                To = x.ptp.To,
                                NetPrice = x.ptp.NetPrice,
                                DateTimeDepature = x.ptp.DateTimeDepature,
                                DateTimeArrival = x.ptp.DateTimeArrival,
                                Passenger = x.ptp.Passenger,
                                DepartureTime = x.ptp.DepartureTime,
                                ArrivalTime = x.ptp.ArrivalTime,
                                FareName = (!string.IsNullOrEmpty(x.ptp.FareName) ? x.ptp.FareName : x.ptp.SeviceName),
                                Class = x.ptp.Class,
                                ShippingAmount = (x.tod.ShippingAmount.HasValue ? x.tod.ShippingAmount.Value : 0)

                            }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<p2ppasssale> Getp2psaleByPassSaleId(Guid ID)
        {
            try
            {
                var list = (from ptp in _db.tblP2PSale
                            join ptpl in _db.tblPassP2PSalelookup on ptp.ID equals ptpl.PassSaleID
                            join tod in _db.tblOrders on ptpl.OrderID equals tod.OrderID
                            join tods in _db.tblOrderStatus on tod.Status equals tods.ID
                            where ptpl.ID == ID && ptpl.ProductType == "P2P"
                            select new { ptp, ptpl, tods, tod }).Select(x => new p2ppasssale
                            {
                                ID = x.ptpl.ID,
                                SiteID = x.tod.SiteID.HasValue ? x.tod.SiteID.Value : Guid.Empty,
                                Status = x.tods.Name,
                                CreatedOn = x.tod.CreatedOn,
                                OrderID = x.ptpl.OrderID.HasValue ? x.ptpl.OrderID.Value : 0,
                                TrainNo = x.ptp.TrainNo,
                                PNRNo = "",
                                IpAddress = x.tod.IpAddress,
                                SiteName = x.tod.tblSite.DisplayName,
                                From = x.ptp.From,
                                To = x.ptp.To,
                                NetPrice = x.ptp.NetPrice,
                                DateTimeDepature = x.ptp.DateTimeDepature,
                                DateTimeArrival = x.ptp.DateTimeArrival,
                                Passenger = x.ptp.Passenger,
                                DepartureTime = x.ptp.DepartureTime,
                                ArrivalTime = x.ptp.ArrivalTime,
                                FareName = (!string.IsNullOrEmpty(x.ptp.FareName) ? x.ptp.FareName : x.ptp.SeviceName),
                                Class = x.ptp.Class,
                                ShippingAmount = (x.tod.ShippingAmount.HasValue ? x.tod.ShippingAmount.Value : 0)

                            }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetP2POrderList> GetP2POrderList(string OrderID, string Reference, string LastName, string Status, string SiteId, DateTime d1, DateTime d2, int FilterType, int page, int pagesize, Guid AgentId, string PnrNumber)
        {
            try
            {
                return _db.GetP2POrderList(OrderID, Reference, LastName, Status, SiteId, d1, d2, FilterType, page, pagesize, AgentId, PnrNumber).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetAnyOrderRefunds(long OrderId, int CaseNo)
        {
            try
            {
                switch (CaseNo)//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
                {
                    case 1:
                        return _db.tblOrderRefundAdmins.Any(t => t.OrderID == OrderId && t.BookingFee > 0);
                    case 2:
                        return _db.tblOrderRefundAdmins.Any(t => t.OrderID == OrderId && t.ShippingFee > 0);
                    case 3:
                        return _db.tblOrderRefundAdmins.Any(t => t.OrderID == OrderId && t.AdminFee > 0);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddRefundData(tblOrderRefundAdmin obj)
        {
            try
            {
                var data = _db.tblOrderRefundAdmins.FirstOrDefault(t => t.OrderID == obj.OrderID);
                if (data == null)
                    _db.tblOrderRefundAdmins.AddObject(obj);
                else
                {
                    data.AdminFee = obj.AdminFee == 0 ? data.AdminFee : obj.AdminFee;
                    data.BookingFee = obj.BookingFee == 0 ? data.BookingFee : obj.BookingFee;
                    data.ShippingFee = obj.ShippingFee == 0 ? data.ShippingFee : obj.ShippingFee;
                    data.CreatedBy = obj.CreatedBy;
                    data.CreatedOn = obj.CreatedOn;
                }
                var OrderData = _db.tblOrders.FirstOrDefault(t => t.OrderID == obj.OrderID);
                OrderData.Status = 9;
                OrderData.ModifyedOn = DateTime.Now;
                OrderData.ModifyedBy = AdminuserInfo.UserID;
                new ManageBooking().UpdateOrderStatusLog(9, obj.OrderID);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblOrderRefundAdmin GetOrderAdminRefundById(int OrderID)
        {
            try
            {
                var data = _db.tblOrderRefundAdmins.FirstOrDefault(t => t.OrderID == OrderID);
                if (data != null)
                    return data;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PASSSALESREPORTBYSUPPLIER_Result> PassSaleReportBySupplierId(string siteid, DateTime? date1, DateTime? date2, int PageNo, int RecordsPerPage, string productCategory, string ProductName, string OrderNo, Guid AgentId, string Supplier)
        {
            try
            {
                return _db.PASSSALESREPORTBYSUPPLIER(siteid, date1, date2, PageNo, RecordsPerPage, productCategory, ProductName, OrderNo, AgentId, Supplier).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AdminUsersReportsRoleBased_Result> AdminUsersReportsRoleBased(Guid RoleId)
        {
            try
            {
                return _db.AdminUsersReportsRoleBased(RoleId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<PassSalesReport> PassSaleReport(string siteid, DateTime? date1, DateTime? date2, int PageNo, int RecordsPerPage, string productCategory, string ProductName, string AgentUserName, string OrderNo, Guid AgentId, bool IsClientReport = false)
        {
            try
            {
                return _db.PassSalesReport(siteid, date1, date2, PageNo, RecordsPerPage, productCategory, ProductName, AgentUserName, OrderNo, AgentId, IsClientReport).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class p2ppasssale : tblP2PSale
        {
            public Guid PassP2PSaleId { get; set; }
            public string TrainNo { get; set; }
            public string Status { get; set; }
            public DateTime CreatedOn { get; set; }
            public string PNRNo { get; set; }
            public long OrderID { get; set; }
            public Guid SiteID { get; set; }
            public string SiteName { get; set; }
            public string IpAddress { get; set; }
            public decimal ShippingAmount { get; set; }
            public string AgentReferenceNumber { get; set; }
            public string BookingStatus { get; set; }
            public int StatusId { get; set; }
        }

        public class ClsP2PDeliveryInfomation : tblP2PDeliveryDetails
        {
            public string Country { get; set; }
            public string State { get; set; }
        }

        public class OrderList : tblOrder
        {
            public DateTime? PaymentDate { get; set; }
            public string PassStartDate { get; set; }
            public string SiteName { get; set; }
            public string OrderStatus { get; set; }
            public int OrderStatusID { get; set; }
            public string TravellerName { get; set; }
            public string AgntAfftName { get; set; }
            public Guid AgntId { get; set; }
            public string IpAddress { get; set; }
            public decimal Price { get; set; }
            public decimal Commission { get; set; }
            public Guid? UserID { get; set; }
            public long StockNo { get; set; }
            public long PassTypeCode { get; set; }
            public Guid? CurrencyIDbySiteID { get; set; }
            public decimal BookingFee { get; set; }
        }

        public class OrderPageCount
        {
            public string PageCount { get; set; }
        }

        public class passsale
        {
            public Guid ID { get; set; }
            public decimal SalePrice { get; set; }
            public decimal TicketProtection { get; set; }
            public long OrderID { get; set; }
            public string RailPassName { get; set; }
            public string SpecialOffer { get; set; }
            public string PassDesc { get; set; }
            public string ClassType { get; set; }
            public string TrvClass { get; set; }
            public decimal Price { get; set; }
            public string CountryName { get; set; }
            public Guid SiteID { get; set; }
            public Guid ProductID { get; set; }
            public Guid SaverID { get; set; }
            public string TrvlOrderCountry { get; set; }
            public int TicketNumber { get; set; }
            public long StockNumber { get; set; }
            public string IsSaver { get; set; }
            public decimal Commission { get; set; }
            public decimal Markup { get; set; }
            public long OrderIdentity { get; set; }
        }

        public class p2p
        {
            public decimal TicketProtection { get; set; }
        }

        public class getordershippingandbillingInfo : tblOrderBillingAddress
        {
            public DateTime? DateOfDepart { get; set; }
        }

        public class AgentDetails
        {

            public Guid Id { get; set; }
            public string Name { get; set; }
            public string UserName { get; set; }
            public string Office { get; set; }
            public Guid BranchId { get; set; }
        }

        public class OrderTraveller : tblOrderTraveller
        {
            public Guid PassSalesId { get; set; }
            public string CountryName { get; set; }
            public int TicketNumber { get; set; }
        }

        public class P2PTravellerList
        {
            public Guid OrderTravellerID { get; set; }
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Fipnumber { get; set; }
            public string CountryName { get; set; }
        }

    }
}
