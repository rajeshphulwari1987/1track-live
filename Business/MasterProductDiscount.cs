﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class MasterProductDiscount
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        public List<tblSite> GetActiveSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsActive == true && x.IsDelete != true).OrderBy(x => x.DisplayName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClsProductDiscount> GetProductDiscount(Guid siteId)
        {
            try
            {
                var result = (from pd in _db.tblProductDiscounts
                              join pdlkp in _db.tblProductDiscountSiteLookups
                                  on pd.ID equals pdlkp.ProductDiscountID
                              where pdlkp.SiteID == siteId
                              select new { pd });
                var res = result.Select(x => new ClsProductDiscount
                //var res = _db.tblProductDiscounts.Select(x => new ClsProductDiscount
                {
                    ID = x.pd.ID,
                    Name = x.pd.Name,
                    Amount = (decimal)x.pd.Amount,
                    Code = x.pd.Code,
                    IsPercentage = x.pd.IsPercentage,
                    IsActive = x.pd.IsActive,
                    DiscountFromDate = x.pd.discountFromDate,
                    DiscountToDate = x.pd.discountToDate
                }).AsEnumerable().Select(x => new ClsProductDiscount
                {
                    ID = x.ID,
                    Name = x.Name,
                    SiteName = GetSiteName(x.ID),
                    Amount = x.Amount,
                    Code = x.Code,
                    IsPercentage = x.IsPercentage,
                    IsActive = x.IsActive,
                    DiscountFromDate = x.DiscountFromDate,
                    DiscountToDate = x.DiscountToDate
                }).OrderBy(x => x.Name).ToList();

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSiteName(Guid id)
        {
            var res = (from lkp in _db.tblProductDiscountSiteLookups
                       join ts in _db.tblSites on lkp.SiteID equals ts.ID
                       where lkp.ProductDiscountID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";
            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";
            }
            return siteName;
        }

        public ClsProductDiscount GetDiscountById(Guid id)
        {
            try
            {
                var rec = _db.tblProductDiscounts.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                    return new ClsProductDiscount
                    {
                        Name = rec.Name,
                        Amount = (decimal)rec.Amount,
                        Code = rec.Code,
                        Description = rec.Description,
                        IsPercentage = rec.IsPercentage,
                        IsActive = rec.IsActive,
                        DiscountFromDate = rec.discountFromDate,
                        DiscountToDate = rec.discountToDate,
                        ListSiteId = rec.tblProductDiscountSiteLookups.Where(x => x.ProductDiscountID == id).Select(x => x.SiteID).ToList(),
                    };
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddEditDiscount(ClsProductDiscount dis)
        {
            try
            {
                var id = dis.ID == Guid.Empty ? Guid.NewGuid() : dis.ID;
                if (dis.ID == Guid.Empty)
                {
                    _db.tblProductDiscounts.AddObject(new tblProductDiscount
                    {
                        ID = id,
                        Name = dis.Name,
                        Amount = dis.Amount,
                        Code = dis.Code,
                        Description = dis.Description,
                        IsPercentage = dis.IsPercentage,
                        CreatedBy = dis.CreatedBy,
                        CreatedOn = DateTime.Now,
                        IsActive = dis.IsActive,
                        discountFromDate = dis.DiscountFromDate,
                        discountToDate = dis.DiscountToDate
                    });
                }
                else
                {
                    var rec = _db.tblProductDiscounts.FirstOrDefault(x => x.ID == id);
                    if (rec != null)
                    {
                        rec.Name = dis.Name;
                        rec.Amount = dis.Amount;
                        rec.Code = dis.Code;
                        rec.Description = dis.Description;
                        rec.IsPercentage = dis.IsPercentage;
                        rec.ModifiedBy = dis.CreatedBy;
                        rec.ModifiedOn = DateTime.Now;
                        rec.IsActive = dis.IsActive;
                        rec.discountFromDate = dis.DiscountFromDate;
                        rec.discountToDate = dis.DiscountToDate;
                    }
                }

                _db.tblProductDiscountSiteLookups.Where(x => x.ProductDiscountID == id).ToList().ForEach(_db.tblProductDiscountSiteLookups.DeleteObject);
                foreach (var item in dis.ListSiteId)
                {
                    _db.tblProductDiscountSiteLookups.AddObject(new tblProductDiscountSiteLookup
                    {
                        SiteID = item,
                        ID = Guid.NewGuid(),
                        ProductDiscountID = id,
                    });
                }
                _db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInActiveDiscount(Guid id)
        {
            try
            {
                var rec = _db.tblProductDiscounts.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                {
                    rec.IsActive = !rec.IsActive;
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInActivePercentage(Guid id)
        {
            try
            {
                var rec = _db.tblProductDiscounts.FirstOrDefault(x => x.ID == id);
                if (rec != null)
                {
                    rec.IsPercentage = !rec.IsPercentage;
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteDiscount(Guid id)
        {
            try
            {
                _db.tblProductDiscountSiteLookups.Where(x => x.ProductDiscountID == id).ToList().ForEach(_db.tblProductDiscountSiteLookups.DeleteObject);
                var rec = _db.tblProductDiscounts.FirstOrDefault(x => x.ID == id);
                _db.tblProductDiscounts.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ClsProductDiscount
        {
            public Guid ID { get; set; }
            public List<Guid> ListSiteId { get; set; }
            public string Name { get; set; }
            public string SiteName { get; set; }
            public decimal Amount { get; set; }
            public string Code { get; set; }
            public string Description { get; set; }
            public bool IsPercentage { get; set; }
            public bool IsActive { get; set; }
            public Guid CreatedBy { get; set; }
            public Nullable<DateTime> DiscountFromDate { get; set; }
            public Nullable<DateTime> DiscountToDate { get; set; }
        }
    }
}
