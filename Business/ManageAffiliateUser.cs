﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{

    public class ManageAffiliateUser : Masters
    {
        db_1TrackEntities db = new db_1TrackEntities();



        #region [ Save Affiliate User ]
        /// <summary>
        /// Save and Update Affiliate User
        /// </summary>
        /// <param name="affUser"></param>
        /// <returns></returns>
        public Guid SaveAffiliateUser(tblAffiliateUser affUser)
        {
            try
            {


                var objaffUser = db.tblAffiliateUsers.Where(o => o.ID == affUser.ID).FirstOrDefault();
                if (objaffUser != null)
                {
                    if (db.tblAffiliateUsers.Any(x => x.UserName == affUser.UserName && x.ID != affUser.ID) || db.tblAdminUsers.Any(x => x.UserName == affUser.UserName && x.Password == affUser.Password))
                        throw new Exception("User Name is already exist. Please try different!");

                    objaffUser.Code = affUser.Code;
                    objaffUser.CountryID = affUser.CountryID;
                    objaffUser.Name = affUser.Name;
                    objaffUser.ContactEmail = affUser.ContactEmail;
                    objaffUser.DaysInProgram = affUser.DaysInProgram;
                    objaffUser.ModifiedBy = affUser.ModifiedBy;
                    objaffUser.ModifiedOn = affUser.ModifiedOn;
                    objaffUser.IsActive = affUser.IsActive;

                    objaffUser.UserName = affUser.UserName;
                    objaffUser.Password = affUser.Password;

                }
                else
                {
                    bool result = db.tblAdminUsers.Any(x => x.UserName == affUser.UserName && x.Password == affUser.Password) || db.tblAffiliateUsers.Any(x => x.UserName == affUser.UserName && x.Password == affUser.Password);
                    if (result)
                        throw new Exception("User Name is already exist. Please try different!");

                    if (!db.tblAffiliateUsers.Any(x => x.Code == affUser.Code))
                        db.tblAffiliateUsers.AddObject(affUser);
                    else
                        throw new Exception("Affiliate Code is already exist.");
                }
                db.SaveChanges();
                return affUser.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid? SaveAffilateSiteLookUp(tblAffiliateUserLookupSite siteLookup, bool delete)
        {
            try
            {
                if (delete)
                {
                    db.tblAffiliateUserLookupSites.Where(x => x.AffiliateUserID == siteLookup.AffiliateUserID).ToList().ForEach(db.tblAffiliateUserLookupSites.DeleteObject);
                }
                else
                {
                    db.tblAffiliateUserLookupSites.AddObject(siteLookup);
                }
                db.SaveChanges();
                return siteLookup.AffiliateUserID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Guid?> GetAffiliateUserByID(Guid id)
        {
            return db.tblAffiliateUserLookupSites.Where(x => x.AffiliateUserID == id).Select(y => y.SiteID).ToList();
        }
        #endregion

        #region [ Get Affiliate User ]
        public List<tblAffiliateUser> GetAffiliateBySiteId(Guid SiteId)
        {
            try
            {
                return (from tau in db.tblAffiliateUsers
                        join tausl in db.tblAffiliateUserLookupSites on tau.ID equals tausl.AffiliateUserID
                        where tausl.SiteID == SiteId
                        select tau).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AffiliateUserFields> GetAffiliateUserList()
        {
            var list = (from Au in db.tblAffiliateUsers select Au).ToList();
            return (list.Select(x => new AffiliateUserFields
            {
                Code = x.Code,
                ContactEmail = x.ContactEmail,
                Country = x.tblCountriesMst.CountryName,
                DaysInProgram = x.DaysInProgram,
                ID = x.ID,
                IsActive = x.IsActive,
                Name = x.Name,
                SiteName = GetSiteName(x.ID)
            })).AsEnumerable().ToList();

        }

        public string GetSiteName(Guid? id)
        {
            var res = (from catSi in db.tblAffiliateUserLookupSites
                       join ts in db.tblSites on catSi.SiteID equals ts.ID
                       where catSi.AffiliateUserID == id
                       select new { ts.DisplayName }).ToList();
            string siteName = "";

            foreach (var item in res)
            {
                siteName += "- " + item.DisplayName + "<br/>";

            }
            return siteName;

        }

        public tblAffiliateUser GetAffiliateUserbyID(Guid ID)
        {

            return db.tblAffiliateUsers.Where(x => x.ID == ID).FirstOrDefault();

        }
        public bool DeleteAffiliateUser(Guid ID)
        {
            try
            {
                var rec = db.tblAffiliateUsers.FirstOrDefault(x => x.ID == ID);
                db.tblAffiliateMakeURLs.Where(x => x.AffiliateID == ID).ToList().ForEach(db.tblAffiliateMakeURLs.DeleteObject);
                db.tblAffiliateUserLookupSites.Where(x => x.AffiliateUserID == ID).ToList().ForEach(db.tblAffiliateUserLookupSites.DeleteObject);
                db.tblAffiliateUsers.DeleteObject(rec);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActivateInActivateAffiliateUser(Guid ID)
        {
            bool result = false;
            var affuser = db.tblAffiliateUsers.Where(x => x.ID == ID).FirstOrDefault();
            if (affuser != null)
            {
                result = affuser.IsActive = !(affuser.IsActive);

                db.SaveChanges();
            }
            return result;
        }
        public List<AffiliateMakeURL> GetMakeURLData(Guid AID, Guid CID, Guid siteId)
        {

            var results = from p in db.tblCategoriesNames
                          group p.Name by p.Name into g
                          where g.Count() > 1
                          select new
                          {
                              CatName = g.Key
                          };

            var DupliPoducts = from p in db.tblProductNames
                               group p.Name by p.Name into g
                               where g.Count() > 1
                               select new
                               {
                                   ProductName = g.Key
                               };

            List<AffiliateMakeURL> listbind = new List<AffiliateMakeURL>();
            var list = (
                from affId in db.tblAffiliateUsers
                join affsiteid in db.tblAffiliateUserLookupSites on affId.ID equals affsiteid.AffiliateUserID
                join sitesurl in db.tblSites on affsiteid.SiteID equals sitesurl.ID
                join productID in db.tblProductSiteLookUps on sitesurl.ID equals productID.SiteID
                join productName in db.tblProductNames on productID.ProductID equals productName.ProductID
                join productcat in db.tblProductCategoriesLookUps on productID.ProductID equals productcat.ProductID
                join cat in db.tblCategoriesNames on productcat.CategoryID equals cat.CategoryID
                where affId.ID == AID && productcat.CategoryID == CID && sitesurl.ID == siteId
                select new AffiliateMakeURL
                {
                    ID = new Guid(),
                    AffiliateId = affId.ID,
                    AffCode = affId.Code,
                    ProductID = productID.ProductID,
                    CategoryID = cat.CategoryID,
                    Name = productName.Name,
                    Category = cat.Name,
                    SiteURL = sitesurl.SiteURL
                }).Distinct().ToList();

            foreach (var t in list)
            {
                string url = results.Any(y => y.CatName == t.Category) ? t.SiteURL + t.CategoryID.ToString().Substring(0, 4) + "-" + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                              (DupliPoducts.Any(p => p.ProductName == t.Name) ? t.ProductID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower() : t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower()) + "?af=" + t.AffCode
                              : t.SiteURL + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                              (DupliPoducts.Any(p => p.ProductName == t.Name) ? t.ProductID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower() : t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower()) + "?af=" + t.AffCode;
                bool isExist = db.tblAffiliateMakeURLs.Any(ty => ty.URL == url);
                listbind.Add(new AffiliateMakeURL
                {
                    ID = isExist ? db.tblAffiliateMakeURLs.FirstOrDefault(ty => ty.URL == url).Id : new Guid(),
                    AffiliateId = t.AffiliateId,
                    ProductID = t.ProductID,
                    Name = t.Name,
                    IsActive = isExist ? (bool)db.tblAffiliateMakeURLs.FirstOrDefault(ty => ty.URL == url).IsActive : false,
                    URL = url
                });
            }
            return listbind;
        }

        public List<AffiliateMakeURL> GetProductListByCatID(Guid siteId, Guid catId, Guid AID)
        {
            try
            {
                var results = from p in db.tblCategoriesNames
                              group p.Name by p.Name into g
                              where g.Count() > 1
                              select new
                              {
                                  CatName = g.Key
                              };
                List<AffiliateMakeURL> listbind = new List<AffiliateMakeURL>();
                var list = (from affId in db.tblAffiliateUsers
                            join affsiteid in db.tblAffiliateUserLookupSites on affId.ID equals affsiteid.AffiliateUserID
                            join sitesurl in db.tblSites on affsiteid.SiteID equals sitesurl.ID
                            join productcat in db.tblCategorySiteLookUps on affsiteid.SiteID equals productcat.SiteID
                            join cat in db.tblCategoriesNames on productcat.CategoryID equals cat.CategoryID
                            where affId.ID == AID && productcat.CategoryID == catId && sitesurl.ID == siteId
                            select new AffiliateMakeURL
                            {
                                Category = cat.Name,
                                AffiliateId = affId.ID,
                                CategoryID = cat.CategoryID,
                                AffCode = affId.Code,
                                SiteURL = sitesurl.SiteURL
                            }).Distinct().ToList();

                foreach (var t in list)
                {
                    string url = results.Any(y => y.CatName == t.Category) ?
                                t.SiteURL + t.CategoryID.ToString().Substring(0, 4) + "-" + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").ToLower() + "?af=" + t.AffCode
                                : t.SiteURL + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "?af=" + t.AffCode;

                    bool isExist = db.tblAffiliateMakeURLs.Any(ty => ty.URL == url);

                    listbind.Add(new AffiliateMakeURL
                    {
                        ID = isExist ? db.tblAffiliateMakeURLs.FirstOrDefault(ty => ty.URL == url).Id : new Guid(),
                        AffiliateId = t.AffiliateId,
                        CategoryID = t.CategoryID,
                        Category = t.Category,
                        IsActive = isExist ? (bool)db.tblAffiliateMakeURLs.FirstOrDefault(ty => ty.URL == url).IsActive : false,
                        URL = url
                    });
                }
                return listbind;




            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateMakeURL(tblAffiliateMakeURL AffObj)
        {
            var affliate = db.tblAffiliateMakeURLs.FirstOrDefault(ty => ty.URL == AffObj.URL);
            if (AffObj.Id != Guid.Empty && affliate != null)
                affliate.IsActive = !affliate.IsActive;
            else
            {
                AffObj.Id = Guid.NewGuid();
                db.AddTotblAffiliateMakeURLs(AffObj);
            }
            db.SaveChanges();
            return true;
        }
        public bool GetAffURLIsActiveByAffCode(String AffCode, string AffUrl)
        {
            var Data = db.tblAffiliateUsers.Join(db.tblAffiliateMakeURLs, o => o.ID, i => i.AffiliateID, (t1, t2) => new { t1.Code, t2.URL, t2.IsActive }).FirstOrDefault(ty => ty.Code == AffCode && ty.URL == AffUrl);
            if (Data != null)
                return Data.IsActive.Value;
            else
                return false;
        }
        #endregion
    }
    public class AffiliateUserFields
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactEmail { get; set; }
        public int? DaysInProgram { get; set; }
        public string Country { get; set; }
        public bool IsActive { get; set; }
        public string SiteName { get; set; }
    }
    public class AffiliateMakeURL
    {
        public Guid? ID { get; set; }
        public Guid? AffiliateId { get; set; }
        public Guid? ProductID { get; set; }
        public Guid? CategoryID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public string URL { get; set; }
        public string AffCode { get; set; }
        public string SiteURL { get; set; }
    }
}
