//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPassP2PSalelookup
    {
        public System.Guid ID { get; set; }
        public Nullable<long> OrderID { get; set; }
        public Nullable<System.Guid> PassSaleID { get; set; }
        public string ProductType { get; set; }
        public Nullable<System.Guid> OrderTravellerID { get; set; }
        public int ShortOrder { get; set; }
        public string FIPNumber { get; set; }
    }
}
