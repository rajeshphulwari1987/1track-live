//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    
    public partial class sp_GetPassSaleDetail_Result
    {
        public int PassTypeCode { get; set; }
        public decimal OriginalPrice { get; set; }
        public System.Guid id { get; set; }
        public Nullable<System.Guid> AgentId { get; set; }
        public System.Guid ProductID { get; set; }
        public System.Guid Travelerid { get; set; }
        public System.Guid classid { get; set; }
        public System.Guid Validityid { get; set; }
        public System.Guid CategoryID { get; set; }
        public string Travellername { get; set; }
        public string validityname { get; set; }
        public string classname { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> markup { get; set; }
        public Nullable<decimal> commission { get; set; }
    }
}
