﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace Business
{
    public static class EmailSchedular
    {
        public static void SendScheduleMail()
        {
            try
            {
                bool IsFirst = true;
                var db = new db_1TrackEntities();
                var BodyMessage = new StringBuilder();
                Guid SiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
                var result = new Masters().GetEmailSettingDetail(SiteId);
                var datalist = GetExpiredProducts(DateTime.Now.AddMonths(1), DateTime.Now.AddDays(-60));
                if (datalist.Any() && result != null)
                {
                    BodyMessage.Append("<div style='font-family: Arial;'>Hi Admin,<br/><br/>");
                    BodyMessage.Append("Please be aware that the following product(s) Have expired during last two months or are set to expire in the month of <b>" + String.Format("{0:MMMM}", DateTime.Now) + " " + DateTime.Now.Year + "</b>.<br/>");
                    foreach (var item in datalist)
                    {
                        if (!string.IsNullOrEmpty(item.SiteName))
                        {
                            if (IsFirst)
                            {
                                BodyMessage.Append("<br/><br/><p style='color:red;'><b>Sites:</b> " + item.SiteName + "</p><ul>");
                                IsFirst = false;
                            }
                            else
                                BodyMessage.Append("</ul><br/><br/><br/><p style='color:red;'><b>Sites:</b> " + item.SiteName + "</p><ul>");

                        }
                        BodyMessage.Append("<li style='color:#ccc;'><a style='color: #6d6f71;' href='https://1track.internationalrail.net/Product/Product.aspx?id=" + item.Id + "'>" + item.CategoryName + " => " + item.Name + ": (" + item.ExpireDate + ")</a></li>");
                    }

                    BodyMessage.Append("</ul><br/><br/>Thanks<br/>");
                    BodyMessage.Append("<b>1Track Admin System</b><br/><br/><img src='https://1track.internationalrail.net/images/Logo.png'/></div>");

                    var Message = new MailMessage();
                    var SmtpClient = new SmtpClient();
                    Guid AdminRole = Guid.Parse("A66F5641-D8A2-4CC3-BCB0-1B5F1E2269B9");
                    var AdminRoleEmailList = db.tblAdminUsers.Where(x => x.RoleID == AdminRole && x.IsActive == true && x.IsDeleted == false).Select(x => x.EmailAddress).Distinct().ToList();
                    foreach (var item in AdminRoleEmailList)
                        Message.To.Add(item);
                    Message.From = new MailAddress("noreply@internationalrail.com");
                    Message.Subject = "1Track Product Expiry Notification";
                    Message.IsBodyHtml = true;
                    Message.Body = BodyMessage.ToString();
                    SmtpClient.Host = result.SmtpHost;
                    SmtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    SmtpClient.UseDefaultCredentials = true;
                    SmtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    SmtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    SmtpClient.Send(Message);

                    db.tblExpiredEmailLogs.AddObject(new tblExpiredEmailLog
                    {
                        SentDate = DateTime.Now,
                        Email = Message.Body
                    });
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ProductExpired> GetExpiredProducts(DateTime ExpireDate, DateTime CurrentDate)
        {
            try
            {
                var db = new db_1TrackEntities();
                return db.EXPIREDPRODUCTLIST(CurrentDate, ExpireDate, 0, AdminuserInfo.SiteID).ToList().Select(x => new ProductExpired
               {
                   Id = (Guid)x.ID,
                   CategoryName = x.CATEGORYNAME,
                   Name = x.PRODUCTNAMES,
                   ExpireDate = x.EXPIRED.Value.ToString("dd/MMM/yyyy hh:mm"),
                   SiteName = x.SITENAME
               }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ProductExpired> GetExpiredProductsBySiteID(DateTime ExpireDate, DateTime CurrentDate, Guid SiteId)
        {
            try
            {
                var db = new db_1TrackEntities();
                return db.EXPIREDPRODUCTLIST(CurrentDate, ExpireDate, 1, SiteId).ToList().Select(x => new ProductExpired
                {
                    Id = (Guid)x.ID,
                    CategoryName = x.CATEGORYNAME,
                    Name = x.PRODUCTNAMES,
                    ExpireDate = x.EXPIRED.Value.ToString("dd/MMM/yyyy hh:mm"),
                    SiteName = x.SITENAME
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ExpiredRoleActivated()
        {
            try
            {
                var db = new db_1TrackEntities();
                return db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsProductExpiryEmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class ProductExpired
    {
        public Guid Id { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string ExpireDate { get; set; }
        public string SiteName { get; set; }

    }
}
