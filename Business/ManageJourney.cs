﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class ManageJourney
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public List<tblSite> GetSiteList()
        {
            try
            {
                return _db.tblSites.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region TopJourneys
        public List<JourneyList> GetTopJourneyBySiteID(Guid siteId)
        {
            try
            {
                var olist = (from sj in _db.tblJourneys
                             join sl in _db.tblJourneySiteLookups
                             on sj.ID equals sl.JourneyID
                             where sl.SiteID == siteId
                             && sj.IsActive == true
                             && sj.IsTopJourney == true
                             select new { sj }).ToList();

                var list = olist.Distinct().Select(x => new JourneyList
                {
                    ID = x.sj.ID,
                    From = x.sj.From,
                    To = x.sj.To,
                    FromFlag = x.sj.FromFlag,
                    ToFlag = x.sj.ToFlag,
                    Price = x.sj.Price,
                    DurationHr = x.sj.DurationHr,
                    DurationMin = x.sj.DurationMin,
                    BannerImg = x.sj.BannerImg,
                    Description = x.sj.Description,
                    SortOrder = x.sj.SortOrder
                }).ToList();

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<JourneyList> GetJourneyList(Guid siteId, string station, string topJourney, string isPopular, string isActive, int startIndex, int lastIndex)
        {
            try
            {
                var olist = (from sj in _db.tblJourneys
                             join sl in _db.tblJourneySiteLookups
                                 on sj.ID equals sl.JourneyID
                             join s in _db.tblSites on sl.SiteID equals s.ID
                             join tcm in _db.tblCurrencyMsts on sj.CurrencyId equals tcm.ID
                             where sl.SiteID == siteId
                             select new JourneyList
                                 {
                                     ID = sj.ID,
                                     From = sj.From,
                                     FromFlag = sj.FromFlag,
                                     To = sj.To,
                                     ToFlag = sj.ToFlag,
                                     SiteName = s.DisplayName,
                                     IsActive = sj.IsActive,
                                     IsTopJourney = sj.IsTopJourney,
                                     IsPopular = sj.IsPopular,
                                     SortOrder = sj.SortOrder,
                                     BannerImg = sj.BannerImg,
                                     Country = sj.tblCountriesMst.CountryName,
                                     Price = sj.Price,
                                     CurrencySymbol = tcm.Symbol
                                 });

                if (!string.IsNullOrEmpty(station))
                    olist = olist.Where(x => x.From == station || x.To == station);
                if (!string.IsNullOrEmpty(topJourney))
                    olist = olist.Where(x => x.IsTopJourney == (topJourney == "1"));
                if (!string.IsNullOrEmpty(isPopular))
                    olist = olist.Where(x => x.IsPopular == (isPopular == "1"));
                if (!string.IsNullOrEmpty(isActive))
                    olist = olist.Where(x => x.IsActive == (isActive == "1"));
                return olist.OrderBy(x => x.SortOrder).ToList().Skip(startIndex).Take(lastIndex).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<JourneyList> TotalNumberOfRecord(Guid siteId, string station, string topJourney, string isPopular, string isActive, int startIndex, int lastIndex)
        {
            try
            {
                var olist = (from sj in _db.tblJourneys
                             join sl in _db.tblJourneySiteLookups
                                 on sj.ID equals sl.JourneyID
                             join s in _db.tblSites
                                 on sl.SiteID equals s.ID
                             where sl.SiteID == siteId
                             select new JourneyList
                             {
                                 ID = sj.ID,
                                 From = sj.From,
                                 FromFlag = sj.FromFlag,
                                 To = sj.To,
                                 ToFlag = sj.ToFlag,
                                 SiteName = s.DisplayName,
                                 IsActive = sj.IsActive,
                                 IsTopJourney = sj.IsTopJourney,
                                 IsPopular = sj.IsPopular,
                                 SortOrder = sj.SortOrder,
                                 BannerImg = sj.BannerImg,
                                 Country = sj.tblCountriesMst.CountryName,
                             });
                if (!string.IsNullOrEmpty(station))
                    olist = olist.Where(x => x.From == station || x.To == station);
                if (!string.IsNullOrEmpty(topJourney))
                    olist = olist.Where(x => x.IsTopJourney == (topJourney == "1"));
                if (!string.IsNullOrEmpty(isPopular))
                    olist = olist.Where(x => x.IsPopular == (isPopular == "1"));
                if (!string.IsNullOrEmpty(isActive))
                    olist = olist.Where(x => x.IsActive == (isActive == "1"));
                return olist.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveJourneys(Guid id)
        {
            try
            {
                var objBook = _db.tblJourneys.FirstOrDefault(x => x.ID == id);
                if (objBook != null)
                    objBook.IsActive = !objBook.IsActive;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactiveTopJourney(Guid id)
        {
            try
            {
                var objBook = _db.tblJourneys.FirstOrDefault(x => x.ID == id);
                if (objBook != null)
                    objBook.IsTopJourney = !objBook.IsTopJourney;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActiveInactivePopular(Guid id)
        {
            try
            {
                var objBook = _db.tblJourneys.FirstOrDefault(x => x.ID == id);
                if (objBook != null)
                    objBook.IsPopular = !objBook.IsPopular;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteJourneys(Guid id)
        {
            try
            {
                _db.tblJourneySiteLookups.Where(t => t.JourneyID == id).ToList().ForEach(_db.tblJourneySiteLookups.DeleteObject);

                var rec = _db.tblJourneys.FirstOrDefault(x => x.ID == id);
                _db.tblJourneys.DeleteObject(rec);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid AddJourney(tblJourney oJourney)
        {
            try
            {
                var _journey = new tblJourney
                {
                    ID = oJourney.ID,
                    CountryID = oJourney.CountryID,
                    From = oJourney.From,
                    To = oJourney.To,
                    FromFlag = oJourney.FromFlag,
                    ToFlag = oJourney.ToFlag,
                    Price = oJourney.Price,
                    DurationHr = oJourney.DurationHr,
                    DurationMin = oJourney.DurationMin,
                    BannerImg = oJourney.BannerImg,
                    Description = oJourney.Description,
                    IsActive = oJourney.IsActive,
                    IsTopJourney = oJourney.IsTopJourney,
                    IsPopular = oJourney.IsPopular,
                    IpAddress = oJourney.IpAddress,
                    CreatedBy = oJourney.CreatedBy,
                    CreatedOn = oJourney.CreatedOn,
                    SortOrder = oJourney.SortOrder,
                    CurrencyId = oJourney.CurrencyId
                };

                _db.AddTotblJourneys(_journey);
                _db.SaveChanges();
                return _journey.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddJourneySiteLookup(tblJourneySiteLookup oJourney)
        {
            try
            {
                var _journey = new tblJourneySiteLookup
                {
                    ID = oJourney.ID,
                    JourneyID = oJourney.JourneyID,
                    SiteID = oJourney.SiteID
                };

                _db.AddTotblJourneySiteLookups(_journey);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid UpdateJourney(tblJourney oJourney)
        {
            try
            {
                var _journey = _db.tblJourneys.FirstOrDefault(x => x.ID == oJourney.ID);
                if (_journey != null)
                {
                    _journey.CountryID = oJourney.CountryID;
                    _journey.From = oJourney.From;
                    _journey.To = oJourney.To;
                    _journey.FromFlag = oJourney.FromFlag;
                    _journey.ToFlag = oJourney.ToFlag;
                    _journey.Price = oJourney.Price;
                    _journey.DurationHr = oJourney.DurationHr;
                    _journey.DurationMin = oJourney.DurationMin;
                    _journey.BannerImg = oJourney.BannerImg;
                    _journey.Description = oJourney.Description;
                    _journey.IsActive = oJourney.IsActive;
                    _journey.IsTopJourney = oJourney.IsTopJourney;
                    _journey.IsPopular = oJourney.IsPopular;
                    _journey.IpAddress = oJourney.IpAddress;
                    _journey.ModifyBy = oJourney.ModifyBy;
                    _journey.ModifyOn = oJourney.ModifyOn;
                    _journey.SortOrder = oJourney.SortOrder;
                    _journey.CurrencyId = oJourney.CurrencyId;
                }
                _db.SaveChanges();
                return _journey.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JourneyList GetJourneyById(Guid id)
        {
            try
            {
                var olist = (from sj in _db.tblJourneys
                             join sl in _db.tblJourneySiteLookups
                             on sj.ID equals sl.JourneyID
                             where sj.ID == id
                             select new { sj, sl }).ToList();

                var list = olist.Distinct().Select(x => new JourneyList
                {
                    ID = x.sj.ID,
                    From = x.sj.From,
                    To = x.sj.To,
                    FromFlag = x.sj.FromFlag,
                    ToFlag = x.sj.ToFlag,
                    Price = x.sj.Price,
                    DurationHr = x.sj.DurationHr,
                    DurationMin = x.sj.DurationMin,
                    BannerImg = x.sj.BannerImg,
                    Description = x.sj.Description,
                    IsActive = x.sj.IsActive,
                    IsTopJourney = x.sj.IsTopJourney,
                    IsPopular = x.sj.IsPopular,
                    CountryID = x.sj.CountryID,
                    SiteId = Guid.Parse(x.sl.SiteID.ToString()),
                    SortOrder = x.sj.SortOrder,
                    CurrencyId = x.sj.CurrencyId
                }).ToList().ToList();

                return list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<tblCurrencyMst> GetCurrency()
        {
            try
            {
                return _db.tblCurrencyMsts.Where(x => x.IsActive == true).OrderBy(x => x.ShortCode).ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public bool IsExistSortOrder(Guid siteID, int sortOrder)
        {
            return _db.tblJourneys.Any(x => x.tblJourneySiteLookups.Any(y => y.SiteID == siteID) && x.SortOrder == sortOrder);
        }
        public bool IsExistSortOrderUpdate(Guid id, Guid siteID, int sortOrder)
        {
            return _db.tblJourneys.Any(x => x.tblJourneySiteLookups.Any(y => y.SiteID == siteID) && x.SortOrder == sortOrder && x.ID != id);
        }

        public class JourneyList : tblJourney
        {
            public string SiteName { get; set; }
            public string Country { get; set; }
            public string CurrencySymbol { get; set; }
            public Guid SiteId { get; set; }
        }

        #endregion

        #region Get Top Passes
        public List<TopPasses> GetTopPassesList(Guid siteId)
        {

            try
            {

                Guid langId = Guid.Parse("72D990DF-63B9-4FDF-8701-095FDEB5BBF6");
                var results = from p in _db.tblCategoriesNames
                              group p.Name by p.Name into g
                              where g.Count() > 1
                              select new
                              {
                                  CatName = g.Key
                              };

                var DupliPoducts = from p in _db.tblProductNames
                                   group p.Name by p.Name into g
                                   where g.Count() > 1
                                   select new
                                   {
                                       ProductName = g.Key
                                   };
                var list = (from tps in _db.tblProductNames
                            join tp in _db.tblProducts
                            on tps.ProductID equals tp.ID
                            join tpsite in _db.tblProductSiteLookUps
                            on tps.ProductID equals tpsite.ProductID
                            join tc in _db.tblProductCategoriesLookUps on tps.ProductID equals tc.ProductID

                            where tpsite.SiteID == siteId && tps.LanguageID == langId && tp.IsTopRailPass == true
                            select new TopPasses
                            {
                                ID = tps.ProductID,
                                Name = tps.Name,
                                Category = _db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == langId).Name,
                                CategoryID = tc.CategoryID,

                            }).AsEnumerable().Select(t => new TopPasses
                            {

                                ID = t.ID,
                                Name = t.Name,
                                Category = t.Category,
                                Url = results.Any(y => y.CatName == t.Category) ? t.CategoryID.ToString().Substring(0, 4) + "-" + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" +
                                (DupliPoducts.Any(p => p.ProductName == t.Name) ? t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower() : t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower()) :
                               t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-") + "/" + (DupliPoducts.Any(p => p.ProductName == t.Name) ? t.ID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower() : t.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").ToLower())

                            }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCountryName(int countyCode)
        {
            return "";
        }

        #endregion

        #region ShowTopJourney
        public bool IsTopJourney(Guid siteId)
        {
            try
            {
                var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (result != null)
                {
                    var istopJourney = result.IsTopJourney;
                    if (istopJourney != null)
                        return (bool)istopJourney;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    public class TopPasses
    {
        public Guid ID { get; set; }
        public Guid CategoryID { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Category { get; set; }
        public string CountryName { get; set; }
    }
}
