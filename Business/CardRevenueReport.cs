//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    
    public partial class CardRevenueReport
    {
        public long ORDERID { get; set; }
        public string WEBSITEBOOKEDON { get; set; }
        public string PAYMENTDATE { get; set; }
        public string PAYMENTTIME { get; set; }
        public string PRODUCTCATEGORY { get; set; }
        public string CARDHOLDERNAME { get; set; }
        public string CARDTYPE { get; set; }
        public string SALESCURRENCY { get; set; }
        public Nullable<decimal> GROSSPRODUCTPRICE { get; set; }
        public Nullable<decimal> TICKETPROTECTION { get; set; }
        public decimal BOOKINGFEE { get; set; }
        public Nullable<int> CARDFEES { get; set; }
        public decimal ADMINFEE { get; set; }
        public Nullable<decimal> ADMINEXTRACHARGE { get; set; }
        public decimal SHIPPINGAMOUNT { get; set; }
        public string DISCOUNTCODE { get; set; }
        public Nullable<decimal> ORDERDISCOUNTAMOUNT { get; set; }
        public Nullable<decimal> GRANDTOTAL { get; set; }
        public Nullable<long> ROWNO { get; set; }
        public Nullable<int> TOTALROWS { get; set; }
    }
}
