//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class APIPriceP2PZero
    {
        public System.Guid ID { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Via { get; set; }
        public Nullable<System.DateTime> DateTimeDepature { get; set; }
        public Nullable<System.DateTime> DateTimeArrival { get; set; }
        public string TrainNo { get; set; }
        public string FareName { get; set; }
        public string Passenger { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<System.Guid> CurrencyId { get; set; }
        public string Class { get; set; }
        public string SeviceName { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public Nullable<decimal> NetPrice { get; set; }
        public Nullable<System.Guid> ClassId { get; set; }
        public Nullable<decimal> TicketProtection { get; set; }
        public string ReservationCode { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Senior { get; set; }
        public string Youth { get; set; }
        public string Adult { get; set; }
        public string Children { get; set; }
        public string P2PType { get; set; }
        public string PdfURL { get; set; }
        public string DeliveryOption { get; set; }
        public string PinNumber { get; set; }
        public int P2PId { get; set; }
        public decimal BookingFee { get; set; }
        public decimal CommissionFee { get; set; }
        public decimal Site_USD { get; set; }
        public decimal Site_SEU { get; set; }
        public decimal Site_SBD { get; set; }
        public decimal Site_GBP { get; set; }
        public decimal Site_EUR { get; set; }
        public decimal Site_INR { get; set; }
        public decimal Site_SEK { get; set; }
        public decimal Site_NZD { get; set; }
        public decimal Site_CAD { get; set; }
        public decimal Site_JPY { get; set; }
        public decimal Site_AUD { get; set; }
        public decimal Site_CHF { get; set; }
        public decimal Site_EUT { get; set; }
        public decimal Site_EUB { get; set; }
        public decimal EUR_EUT { get; set; }
        public decimal EUR_EUB { get; set; }
        public string Terms { get; set; }
        public decimal ApiPrice { get; set; }
        public string ApiName { get; set; }
    }
}
