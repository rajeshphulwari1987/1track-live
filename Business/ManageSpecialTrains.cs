﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;

namespace Business
{

    public class ManageSpecialTrains
    {
        readonly db_1TrackEntities db = new db_1TrackEntities();


        public Guid AddEditSpecialTrain(tblSpecialTrain oTrain)
        {
            try
            {
                if (oTrain.ID == new Guid())
                {
                    oTrain.ID = Guid.NewGuid();
                    if (!string.IsNullOrEmpty(oTrain.CountryImg))
                        oTrain.CountryImg = oTrain.CountryImg.Replace("~/", "");
                    if (!string.IsNullOrEmpty(oTrain.BannerImg))
                        oTrain.BannerImg = oTrain.BannerImg.Replace("~/", "");
                    if (!db.tblSpecialTrains.Any(x => x.Name == oTrain.Name.Trim()))
                        db.tblSpecialTrains.AddObject(oTrain);
                    else
                        throw new Exception("Name is already exist.");
                }
                else
                {
                    tblSpecialTrain rec = db.tblSpecialTrains.FirstOrDefault(x => x.ID == oTrain.ID);
                    if (rec == null)
                        return new Guid();
                    rec.IsActive = oTrain.IsActive;
                    rec.ModifyBy = oTrain.CreatedBy;
                    rec.ModifyOn = oTrain.CreatedOn;
                    rec.Name = oTrain.Name;
                    rec.Title = oTrain.Title;
                    rec.ContinentID = oTrain.ContinentID;
                    rec.CountryID = oTrain.CountryID;
                    if (!string.IsNullOrEmpty(oTrain.CountryImg))
                        rec.CountryImg = oTrain.CountryImg.Replace("~/", "");
                    if (!string.IsNullOrEmpty(oTrain.BannerImg))
                        rec.BannerImg = oTrain.BannerImg.Replace("~/", "");
                    rec.Description = oTrain.Description;

                    ManageRoutes objroute = new ManageRoutes();
                    objroute.UpdateRouteCount(Route.SpecialTrainRoute, 0);
                }
                db.SaveChanges();
                return oTrain.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public Guid AddEditSpecialTrainSiteLookup(List<tblSpecialTrainSiteLookUp> oLookup)
        {
            try
            {
                Guid id = oLookup.FirstOrDefault().SpecialTrainID;
                db.tblSpecialTrainSiteLookUps.Where(x => x.SpecialTrainID == id).ToList().ForEach(db.tblSpecialTrainSiteLookUps.DeleteObject);
                foreach (var item in oLookup)
                {
                    item.ID = Guid.NewGuid();
                    db.tblSpecialTrainSiteLookUps.AddObject(item);
                }
                db.SaveChanges();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int TotalNumberOfRecord(Guid siteId, Guid CountryID, Guid ContinentId)
        {
            return db.tblSpecialTrains.Where(x => x.tblSpecialTrainSiteLookUps.Any(y => y.SiteID == siteId) && x.CountryID == (CountryID == Guid.Empty ? x.CountryID : CountryID) && x.tblContinent.ID == (ContinentId == Guid.Empty ? x.tblContinent.ID : ContinentId)).Count();
        }

        public List<SpecialTrainFields> GetSpecialTrainList(Guid siteId, int startIndex, int lastIndex, Guid CountryID, Guid ContinentId)
        {
            try
            {
                return db.tblSpecialTrains.Where(p => p.tblSpecialTrainSiteLookUps.Any(z => z.SiteID == siteId) && p.CountryID == (CountryID == Guid.Empty ? p.CountryID : CountryID) && p.tblContinent.ID == (ContinentId == Guid.Empty ? p.tblContinent.ID : ContinentId)).Select(x => new SpecialTrainFields
                {
                    ID = x.ID,
                    Continent = x.tblContinent.Name,
                    Country = x.tblCountriesMst.CountryName + "(" + x.tblCountriesMst.CountryCode + ")",
                    CountryImg = x.CountryImg ?? "",
                    BannerImg = x.BannerImg ?? "",
                    Description = x.Description,
                    IsActive = x.IsActive,
                    Name = x.Name,
                    Title = x.Title

                }).OrderBy(x => x.Country).ThenBy(x => x.Name).Skip(startIndex).Take(lastIndex).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public tblSpecialTrain GetSpecialTrainById(Guid id)
        {
            try
            {
                return db.tblSpecialTrains.FirstOrDefault(y => y.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<Guid> GetSpecialTrainSiteIdListById(Guid id)
        {
            try
            {
                return db.tblSpecialTrainSiteLookUps.Where(y => y.SpecialTrainID == id).Select(x => x.SiteID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ActiveInactiveSpecialTrain(Guid id)
        {
            try
            {
                tblSpecialTrain rec = db.tblSpecialTrains.FirstOrDefault(x => x.ID == id);
                rec.IsActive = !rec.IsActive;
                db.SaveChanges();
                return true;
            }
            catch
            {
                throw new Exception("Train is already in used.");
            }
        }

        public bool DeleteSpecialTrain(Guid id)
        {
            try
            {
                db.tblSpecialTrainSiteLookUps.Where(x => x.SpecialTrainID == id).ToList().ForEach(db.tblSpecialTrainSiteLookUps.DeleteObject);

                tblSpecialTrain rec = db.tblSpecialTrains.FirstOrDefault(x => x.ID == id);
                db.tblSpecialTrains.DeleteObject(rec);

                db.SaveChanges();
                return true;
            }
            catch
            {
                throw new Exception("Train is already in used.");
            }
        }

    }

    public class SpecialTrainFields
    {
        public Guid ID { get; set; }
        public bool? IsActive { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string BannerImg { get; set; }
        public string Continent { get; set; }
        public string Country { get; set; }
        public string CountryImg { get; set; }
        public string Description { get; set; }
    }
}
