//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblNewsLetterUser
    {
        public tblNewsLetterUser()
        {
            this.tblNewsLetterUserAreaLookups = new HashSet<tblNewsLetterUserAreaLookup>();
        }
    
        public System.Guid ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public System.Guid SiteID { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsSubscribed { get; set; }
    
        public virtual tblSite tblSite { get; set; }
        public virtual ICollection<tblNewsLetterUserAreaLookup> tblNewsLetterUserAreaLookups { get; set; }
    }
}
