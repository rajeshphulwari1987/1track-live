//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEmailSetting
    {
        public tblEmailSetting()
        {
            this.tblEmailSettingLookups = new HashSet<tblEmailSettingLookup>();
        }
    
        public System.Guid ID { get; set; }
        public string SmtpHost { get; set; }
        public string SmtpUser { get; set; }
        public string SmtpPass { get; set; }
        public string SmtpPort { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> EnableSsl { get; set; }
    
        public virtual ICollection<tblEmailSettingLookup> tblEmailSettingLookups { get; set; }
    }
}
