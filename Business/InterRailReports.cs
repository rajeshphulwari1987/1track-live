﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class InterRailReports
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region InterrailStockReport
        public List<INTERRAILSTOCKREPORT_Result> GetInterrailStockReport(Guid SiteId, DateTime startdate, DateTime enddate, string OfficeId, Boolean IsSta)
        {
            try
            {
                return _db.INTERRAILSTOCKREPORT(SiteId, OfficeId, startdate, enddate, IsSta).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InterRailStockReturnReport
        public List<GetinterrailPrintqueue_Result> GetInterrailPrintqueue(Guid Userid)
        {
            try
            {
                return _db.GetinterrailPrintqueue(Userid).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<INTERRAILSTOCKRETURNREPORT_Result> InterRailStockReturnReport(string Queueids, DateTime startdate, DateTime enddate)
        {
            try
            {
                return _db.INTERRAILSTOCKRETURNREPORT(Queueids, startdate, enddate).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region INTERRAILSummaryReport
        public List<INTERRAILSummaryReport_Result> GetINTERRAILSummaryReport(int Months, int Years)
        {
            try
            {
                return _db.INTERRAILSummaryReport(Months, Years).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region INTERRAILSettlementReport
        public List<InterRailSettlement> GetINTERRAILSettlementReport(int Months, int Years)
        {
            try
            {
                return _db.INTERRAILSettlementReport(Months, Years).Select(t => new InterRailSettlement
                    {
                        Id = t.ID.Value,
                        Title = t.TITLE,
                        Amount = t.AMOUNT.Value
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region INTERRAILPASSSALE
        public List<INTERRAILPASSSALE_Result> GetINTERRAILPASSSALE(int Months, int Years)
        {
            try
            {
                return _db.INTERRAILPASSSALE(Months, Years).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}


public class InterRailSettlement
{
    public int Id { get; set; }
    public string Title { get; set; }
    public decimal Amount { get; set; }
    public string Other { get; set; }
}