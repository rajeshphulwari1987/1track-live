﻿
namespace TicketPrinter
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Xml;
    using WebSupergoo.ABCpdf8;
    using System.ComponentModel;

    public class TicketPrinter
    {
        private string _RootPath = "";
        private bool Debugging = false;
        private double voucherheightdivisions;
        private double voucherwidthdivisions;

        public Doc BuildVouchers(double width, double height, XmlNode coupon)
        {
            try
            {
                this.voucherwidthdivisions = width / 72.0;
                this.voucherheightdivisions = height / 18.0;
                Doc voucher = new Doc();
                voucher.Units = UnitType.Didots;
                voucher.TopDown = true;
                voucher.Width = 0.01;
                voucher.MediaBox.SetRect(0, 0, width, height);
                voucher.Page = voucher.AddPage();
                if (this.Debugging)
                {
                    int counter;
                    for (counter = 1; counter <= 0x48; counter++)
                    {
                        voucher.AddLine(counter * this.voucherwidthdivisions, 0.0, counter * this.voucherwidthdivisions, height);
                    }
                    for (counter = 1; counter <= 0x12; counter++)
                    {
                        voucher.AddLine(0.0, counter * this.voucherheightdivisions, width, counter * this.voucherheightdivisions);
                    }
                }
                XmlNodeList elements = coupon.SelectNodes("Element");
                foreach (XmlNode element in elements)
                {
                    string elementtype = "";
                    double startcol = 0.0;
                    double endcol = 0.0;
                    double startrow = 0.0;
                    double endrow = 0.0;
                    bool border = false;
                    string content = "";
                    double fontsize = 0.0;
                    string fontface = "";
                    bool fontbold = false;
                    bool fontitalic = false;
                    string lineformat = "";
                    string lineformatcolor = "000";
                    string align = "";
                    string valign = "";
                    int numberofdays = 0;
                    XmlNode xn = element.Attributes["type"];
                    if (xn != null)
                    {
                        elementtype = xn.Value.ToString().ToUpper();
                    }
                    xn = element.Attributes["startcol"];
                    if (xn != null)
                    {
                        startcol = Convert.ToInt32(xn.Value) - 1;
                    }
                    xn = element.Attributes["endcol"];
                    if (xn != null)
                    {
                        endcol = Convert.ToInt32(xn.Value);
                    }
                    xn = element.Attributes["startrow"];
                    if (xn != null)
                    {
                        startrow = Convert.ToChar(xn.Value.ToString().Substring(0, 1).ToUpper()) - 'A';
                    }
                    xn = element.Attributes["endrow"];
                    if (xn != null)
                    {
                        endrow = Convert.ToChar(xn.Value.ToString().Substring(0, 1).ToUpper()) - '@';
                    }
                    xn = element.Attributes["border"];
                    if (xn != null)
                    {
                        border = Convert.ToBoolean(Convert.ToInt16(xn.Value));
                    }
                    xn = element.Attributes["fontsize"];
                    if (xn != null)
                    {
                        fontsize = Convert.ToDouble(xn.Value);
                    }
                    xn = element.Attributes["fontface"];
                    if (xn != null)
                    {
                        fontface = xn.Value.ToString();
                    }
                    xn = element.Attributes["fontbold"];
                    if (xn != null)
                    {
                        fontbold = Convert.ToBoolean(xn.Value);
                    }
                    xn = element.Attributes["fontitalic"];
                    if (xn != null)
                    {
                        fontitalic = Convert.ToBoolean(xn.Value);
                    }
                    xn = element.Attributes["lineformat"];
                    if (xn != null)
                    {
                        lineformat = xn.Value.ToString().ToUpper();
                    }
                    xn = element.Attributes["fontcolor"];
                    if (xn != null)
                    {
                        lineformatcolor = xn.Value.ToString().ToUpper();
                    }
                    xn = element.Attributes["align"];
                    if (xn != null)
                    {
                        align = xn.Value.ToString().ToUpper();
                    }
                    xn = element.Attributes["valign"];
                    if (xn != null)
                    {
                        valign = xn.Value.ToString().ToUpper();
                    }
                    xn = element.Attributes["numberofdays"];
                    if (xn != null)
                    {
                        numberofdays = Convert.ToInt32(xn.Value);
                    }
                    if ((elementtype == "TEXT") && (element.InnerText != ""))
                    {
                        content = element.InnerText;
                        this.DrawText(ref voucher, startcol * this.voucherwidthdivisions, endcol * this.voucherwidthdivisions, startrow * this.voucherheightdivisions, endrow * this.voucherheightdivisions, content, fontsize, fontface, fontbold, align, valign, fontitalic);
                    }
                    if ((elementtype == "IMAGE") && (element.InnerText != ""))
                    {
                        content = element.InnerText;
                        this.DrawImage(ref voucher, startcol * this.voucherwidthdivisions, endcol * this.voucherwidthdivisions, startrow * this.voucherheightdivisions, endrow * this.voucherheightdivisions, content);
                    }
                    if ((elementtype == "BORDER") && border)
                    {
                        this.DrawBorder(ref voucher, startcol * this.voucherwidthdivisions, endcol * this.voucherwidthdivisions, startrow * this.voucherheightdivisions, endrow * this.voucherheightdivisions);
                    }
                    if ((elementtype == "LINE") && (lineformat != ""))
                    {
                        this.DrawLine(ref voucher, startcol * this.voucherwidthdivisions, endcol * this.voucherwidthdivisions, startrow * this.voucherheightdivisions, endrow * this.voucherheightdivisions, lineformat, lineformatcolor);
                    }
                    if (elementtype == "DATEBLOCK")
                    {
                        this.DrawDateBlock(ref voucher, startcol, startrow);
                    }
                    if (elementtype == "CALENDER")
                    {
                        this.DrawCalender(ref voucher, startcol * this.voucherwidthdivisions, startrow * this.voucherheightdivisions, numberofdays, fontsize, fontface, fontbold, align, valign, fontitalic);
                    }
                }
                return voucher;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Doc DrawBorder(ref Doc voucher, double startcol, double endcol, double startrow, double endrow)
        {
            voucher.Width = 0.1;
            voucher.Color.String = "000";
            voucher.AddLine(startcol, startrow, endcol, startrow);
            voucher.AddLine(endcol, startrow, endcol, endrow);
            voucher.AddLine(endcol, endrow, startcol, endrow);
            voucher.AddLine(startcol, endrow, startcol, startrow);
            return voucher;
        }

        private Doc DrawCalender(ref Doc voucher, double startcol, double startrow, int numberofdays, double fontsize, string fontface, bool fontbold, string align, string valign, bool fontitalic)
        {
            voucher.Width = 0.1;
            voucher.Color.String = "000";
            for (int i = 0; i < numberofdays; i++)
            {
                double currentstartpoint = startcol + ((i * 2) * (this.voucherwidthdivisions * 1.5));
                double currentendpoint = currentstartpoint + ((this.voucherwidthdivisions * 1.5) * 2.0);
                this.DrawBorder(ref voucher, currentstartpoint, currentendpoint, startrow + this.voucherheightdivisions, startrow + (this.voucherheightdivisions * 2.0));
                voucher.AddLine(currentstartpoint + (this.voucherwidthdivisions * 1.5), startrow + (this.voucherheightdivisions * 1.5), currentstartpoint + (this.voucherwidthdivisions * 1.5), startrow + (this.voucherheightdivisions * 2.0));
                this.DrawBorder(ref voucher, currentstartpoint, currentendpoint, startrow + (this.voucherheightdivisions * 2.0), startrow + (this.voucherheightdivisions * 3.0));
                voucher.AddLine(currentstartpoint + (this.voucherwidthdivisions * 1.5), startrow + (this.voucherheightdivisions * 2.5), currentstartpoint + (this.voucherwidthdivisions * 1.5), startrow + (this.voucherheightdivisions * 3.0));
                this.DrawText(ref voucher, currentstartpoint, currentendpoint, startrow, startrow + this.voucherheightdivisions, (i + 1).ToString(), 2.0, fontface, fontbold, align, valign, fontitalic);
            }
            return voucher;
        }

        private Doc DrawDateBlock(ref Doc voucher, double startcol, double startrow)
        {
            voucher.Width = 0.1;
            voucher.Color.String = "000";
            voucher.AddLine(startcol * this.voucherwidthdivisions, (startrow + 1.0) * this.voucherheightdivisions, (startcol + 2.0) * this.voucherwidthdivisions, (startrow + 1.0) * this.voucherheightdivisions);
            voucher.AddLine(startcol * this.voucherwidthdivisions, (startrow + 1.0) * this.voucherheightdivisions, startcol * this.voucherwidthdivisions, (startrow * this.voucherheightdivisions) + (this.voucherheightdivisions / 2.0));
            voucher.AddLine((startcol + 1.0) * this.voucherwidthdivisions, (startrow + 1.0) * this.voucherheightdivisions, (startcol + 1.0) * this.voucherwidthdivisions, (startrow * this.voucherheightdivisions) + (this.voucherheightdivisions / 1.25));
            voucher.AddLine((startcol + 2.0) * this.voucherwidthdivisions, (startrow + 1.0) * this.voucherheightdivisions, (startcol + 2.0) * this.voucherwidthdivisions, (startrow * this.voucherheightdivisions) + (this.voucherheightdivisions / 2.0));
            return voucher;
        }

        private Doc DrawImage(ref Doc voucher, double startcol, double endcol, double startrow, double endrow, string content)
        {
            voucher.Pos.X = 0.0;
            voucher.Pos.Y = 0.0;
            voucher.Rect.Top = startrow;
            voucher.Rect.Bottom = endrow;
            voucher.Rect.Left = startcol;
            voucher.Rect.Right = endcol;
            Bitmap bm = new Bitmap(this._RootPath + @"\" + content);
            voucher.AddImageBitmap(bm, true);
            bm.Dispose();
            return voucher;
        }

        private Doc DrawLine(ref Doc voucher, double startcol, double endcol, double startrow, double endrow, string lineformat, string lineformatcolor)
        {
            voucher.Width = 0.1;
            voucher.Color.String = lineformatcolor;
            if (lineformat.IndexOf("T") > -1)
            {
                voucher.AddLine(startcol, startrow, endcol, startrow);
            }
            if (lineformat.IndexOf("R") > -1)
            {
                voucher.AddLine(endcol, startrow, endcol, endrow);
            }
            if (lineformat.IndexOf("B") > -1)
            {
                voucher.AddLine(endcol, endrow, startcol, endrow);
            }
            if (lineformat.IndexOf("L") > -1)
            {
                voucher.AddLine(startcol, startrow, startcol, endrow);
            }
            return voucher;
        }

        private Doc DrawText(ref Doc voucher, double startcol, double endcol, double startrow, double endrow, string content, double fontsize, string fontface, bool fontbold, string align, string valign, bool fontitalic)
        {
            voucher.Color.String = "000";
            voucher.Pos.X = 0.0;
            voucher.Pos.Y = 0.0;
            voucher.Rect.Top = startrow;
            voucher.Rect.Bottom = endrow;
            voucher.Rect.Left = startcol;
            voucher.Rect.Right = endcol;
            voucher.TextStyle.Size = fontsize;
            voucher.TextStyle.Bold = fontbold;
            voucher.TextStyle.Italic = fontitalic;
            string objDrawText = valign;
            if (objDrawText != null)
            {
                if (!(objDrawText == "TOP"))
                {
                    if (objDrawText == "MIDDLE")
                    {
                        voucher.VPos = 0.5;
                        goto Label_0100;
                    }
                    if (objDrawText == "BOTTOM")
                    {
                        voucher.VPos = 1.0;
                        goto Label_0100;
                    }
                }
                else
                {
                    voucher.VPos = 0.0;
                    goto Label_0100;
                }
            }
            voucher.VPos = 0.5;
        Label_0100:
            objDrawText = align;
            if (objDrawText != null)
            {
                if (!(objDrawText == "LEFT"))
                {
                    if (objDrawText == "MIDDLE")
                    {
                        voucher.HPos = 0.5;
                        goto Label_017B;
                    }
                    if (objDrawText == "RIGHT")
                    {
                        voucher.HPos = 1.0;
                        goto Label_017B;
                    }
                }
                else
                {
                    voucher.HPos = 0.0;
                    goto Label_017B;
                }
            }
            voucher.HPos = 0.5;
        Label_017B:
            voucher.Font = voucher.AddFont(fontface);
            voucher.AddText(content);
            return voucher;
        }

        public MemoryStream LoadTemplate(XmlDocument xDoc, XmlDocument coupons)
        {
            int CouponCounter = 1;
            MemoryStream pdf = new MemoryStream();
            XmlNode node = coupons.SelectSingleNode("//Coupons");
            if (node != null)
            {
                XmlNode mode = node.Attributes["mode"];
                if ((mode != null) && (mode.Value.ToString().ToUpper() == "DEBUG"))
                {
                    this.Debugging = true;
                }
                XmlNodeList coupon = node.ChildNodes;
                int NumberOfCoupons = coupon.Count;
                XmlNodeList elemList = xDoc.GetElementsByTagName("stock");
                XmlNodeList elevoid = xDoc.SelectNodes("controlsheet/void/Coupon");
                if (elemList.Count <= 0)
                {
                    return pdf;
                }
                double width = Convert.ToDouble(elemList[0].Attributes["width"].Value);
                double height = Convert.ToDouble(elemList[0].Attributes["height"].Value);
                double leftmargin = Convert.ToDouble(elemList[0].Attributes["leftmargin"].Value);
                double topmargin = Convert.ToDouble(elemList[0].Attributes["topmargin"].Value);
                double rightmargin = width - Convert.ToDouble(elemList[0].Attributes["rightmargin"].Value);
                double bottommargin = height - Convert.ToDouble(elemList[0].Attributes["bottommargin"].Value);
                Doc thePageDoc = new Doc();

                thePageDoc.Units = UnitType.Mm;
                thePageDoc.TopDown = true;
                thePageDoc.Width = 0.01;
                thePageDoc.MediaBox.SetRect(0, 0, width, height);
                thePageDoc.Rect.String = thePageDoc.MediaBox.String;


                while (CouponCounter <= NumberOfCoupons)
                {
                    thePageDoc.Page = thePageDoc.AddPage();
                    if (this.Debugging)
                    {
                        thePageDoc.Width = 0.1;
                        thePageDoc.Color.String = "000";
                        thePageDoc.AddLine(0.0, 0.0, width, 0.0);
                        thePageDoc.AddLine(0.0, 0.0, 0.0, height);
                        thePageDoc.AddLine(width, 0.0, width, height);
                        thePageDoc.AddLine(width, height, 0.0, height);
                        thePageDoc.AddLine(0.0, topmargin, width, topmargin);
                        thePageDoc.AddLine(0.0, bottommargin, width, bottommargin);
                        thePageDoc.AddLine(leftmargin, 0.0, leftmargin, height);
                        thePageDoc.AddLine(rightmargin, 0.0, rightmargin, height);
                    }
                    double top = topmargin;
                    double left = leftmargin;
                    XmlNodeList vouchers = xDoc.GetElementsByTagName("voucher");
                    foreach (XmlNode voucher in vouchers)
                    {
                        Doc avoucher;
                        double voucherwidth = Convert.ToDouble(voucher.Attributes["width"].Value);
                        double voucherheight = Convert.ToDouble(voucher.Attributes["height"].Value);
                        double vouchertop = Convert.ToDouble(voucher.Attributes["top"].Value);
                        double voucherleft = Convert.ToDouble(voucher.Attributes["left"].Value);
                        if (this.Debugging)
                        {
                            thePageDoc.AddLine(left + voucherleft, top + vouchertop, (left + voucherleft) + voucherwidth, top + vouchertop);
                            thePageDoc.AddLine((left + voucherleft) + voucherwidth, top + vouchertop, (left + voucherleft) + voucherwidth, (top + vouchertop) + voucherheight);
                            thePageDoc.AddLine(left + voucherleft, (top + vouchertop) + voucherheight, (left + voucherleft) + voucherwidth, (top + vouchertop) + voucherheight);
                            thePageDoc.AddLine(left + voucherleft, top + vouchertop, left + voucherleft, (top + vouchertop) + voucherheight);
                        }
                        string[] objDrawTemp = new string[7];
                        double objDrawday = left + voucherleft;
                        objDrawTemp[0] = objDrawday.ToString();
                        objDrawTemp[1] = " ";
                        objDrawday = top + vouchertop;
                        objDrawTemp[2] = objDrawday.ToString();
                        objDrawTemp[3] = " ";
                        objDrawday = (left + voucherleft) + voucherwidth;
                        objDrawTemp[4] = objDrawday.ToString();
                        objDrawTemp[5] = " ";
                        objDrawTemp[6] = ((top + vouchertop) + voucherheight).ToString();

                        thePageDoc.Rect.String = string.Concat(objDrawTemp);

                        if (CouponCounter <= NumberOfCoupons)
                        {
                            avoucher = this.BuildVouchers(voucherwidth, voucherheight, coupon[CouponCounter - 1]);
                            thePageDoc.AddImageDoc(avoucher, 1, null);
                        }
                        else
                        {
                            avoucher = this.BuildVouchers(voucherwidth, voucherheight, elevoid[0]);
                            thePageDoc.AddImageDoc(avoucher, 1, null);
                        }
                        CouponCounter++;
                    }
                }
                thePageDoc.Save(pdf);
            }
            return pdf;
        }

        public MemoryStream Print(XmlDocument stock, XmlDocument coupons)
        {
            XmlNodeList vouchers = stock.GetElementsByTagName("voucher");
            foreach (XmlNode voucher in vouchers)
            {
                double voucherwidth = Convert.ToDouble(voucher.Attributes["width"].Value);
                double voucherheight = Convert.ToDouble(voucher.Attributes["height"].Value);
                double vouchertop = Convert.ToDouble(voucher.Attributes["top"].Value);
                double voucherleft = Convert.ToDouble(voucher.Attributes["left"].Value);
            }
            MemoryStream pdf = new MemoryStream();
            return this.LoadTemplate(stock, coupons);
        }

        public string RootPath
        {
            get
            {
                return this._RootPath;
            }
            set
            {
                this._RootPath = value;
            }
        }
    }

}


