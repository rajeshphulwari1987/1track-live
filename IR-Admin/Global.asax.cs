﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Timers;
using Business;


namespace IR_Admin
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            EamilSchedular();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 30;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string sUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(sUrl);
            }
        }

        public void EamilSchedular()
        {
            System.Timers.Timer myTimer = new System.Timers.Timer();
            /*
             60 * 60 * 1000 => min * sec * msec 
             1380=> 23 hrs
             60 => 1hrs
             */
            myTimer.Interval = 45 * 60 * 1000;
            myTimer.AutoReset = true;
            myTimer.Elapsed += new ElapsedEventHandler(myTimer_Elapsed);
            myTimer.Enabled = true;
        }

        public void myTimer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            if (DateTime.Now.Day == 1 && DateTime.Now.Hour == 10)
            {
                var db = new db_1TrackEntities();
                var data = db.tblExpiredEmailLogs.OrderByDescending(x => x.SentDate).FirstOrDefault();
                if (data == null)
                    EmailSchedular.SendScheduleMail();
                else if (data.SentDate.Month != DateTime.Now.Month)
                    EmailSchedular.SendScheduleMail();
            }
        }
    }
}