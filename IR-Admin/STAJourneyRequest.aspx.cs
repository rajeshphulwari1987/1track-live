﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class STAGurneyRequest : System.Web.UI.Page
    {
        readonly private ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
        readonly private Masters _oMasters = new Masters();
        List<RepeaterListFaqItem> list = new List<RepeaterListFaqItem>();
        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid SiteID, _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            //Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";
            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                BindSite();
                BindGrid(_SiteID);
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;
            grdSTAJourneyRequest.DataSource = _oManageInterRailNew.GettblHelpList(_SiteID);
            grdSTAJourneyRequest.DataBind();
            Tab = "1";
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var SiteId = Master.SiteID;
                bool result = _oManageInterRailNew.AddUpdateHelp(new tblHelp
                {
                    Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value),
                    SiteId = Guid.Parse(ddlSite.SelectedValue),
                    Name = txtFName.Text.Trim() + " " + txtLName.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    Phone = txtPhone.Text.Trim(),
                    Contact = rdnEmail.Checked ? rdnEmail.Text : rdnPhone.Text,
                    CreatedOn = DateTime.Now
                });
                Cleardata();
                BindGrid(SiteId);
                ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Record added successfully." : "Record updated successfully.");
                hdnId.Value = string.Empty;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("STAJourneyRequest.aspx");
        }

        protected void grdSTAJourneyRequest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                _oManageInterRailNew.DeleteHelp(id);
                BindGrid(Master.SiteID);
                ShowMessage(1, "Record delete successfully.");
            }

            else if (e.CommandName == "UpdateRecord")
            {
                var result = _oManageInterRailNew.GettblHelpById(id);
                if (result != null)
                {
                    ddlSite.SelectedValue = result.SiteId.ToString();
                    ddlSite.Enabled = false;
                    string name = result.Name.ToString();
                    string[] sa = name.Split(' ');
                    txtFName.Text = sa[0];
                    txtLName.Text = sa[1];
                    txtEmail.Text = result.Email;
                    txtPhone.Text = result.Phone;
                    if (rdnEmail.Text == result.Contact)
                    {
                        rdnEmail.Checked = true;
                    }
                    else
                    {
                        rdnPhone.Checked = true;
                    }
                    hdnId.Value = result.Id.ToString();
                    Tab = "2";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void Cleardata()
        {
            txtEmail.Text = string.Empty;
            txtFName.Text = string.Empty;
            txtLName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            ddlSite.SelectedIndex = 0;
            ddlSite.Enabled = true;
            rdnEmail.Checked = true;
            rdnPhone.Checked = false;
        }
    }
}