﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="P2POrderDetail.aspx.cs" Inherits="IR_Admin.P2POrderDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Styles/PassBooking.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function hide() { $('.discription-block').hide(); }
        function show() { $('.discription-block').show(); }

        $(document).ready(function () {
            show();
            $('.imgOpen').click(function () {
                if ("images/arr-down.png" == $(this).attr('src').toString()) {
                    $(this).attr('src', 'images/arr-right.png');
                }
                else {
                    $(this).attr('src', 'images/arr-down.png');
                }
                $(this).parent().parent().parent().find('.discription-block').slideToggle();
            });
            $("#rptMainMenu_HyperLink1_3").addClass("active");
            $(".calDate").attr("readonly", "true");
        });

        function chkVal(id, e) {
            if (e == 0)
                e = 3;

            var txtStartDateId = '#' + ($(id).find('input:first-child').attr('id'));
            $(txtStartDateId).datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+' + e + 'm'
            });
            $(txtStartDateId).datepicker('show');
        }
        function chkerrorvali(e) {
            if ($.trim($(e).val()) != '' || $.trim($(e).val()) != 'DD/MM/YYYY')
                $(e).css({ "background-Color": "#FFF" });
            else
                $(e).css({ "background-Color": "#FCCFCF" });
        }
        function showmessagecountry(e) {
            alert("Country not valid for this pass.");
        }
    </script>
    <style>
     .input
    {
        border: 1px solid #ADB9C2;
        color: #424242 !important;
        font-size: 13px !important;
        height: 25px !important;
        line-height: 25px !important;
        float: left !important;
        margin-left: 5px !important;
        margin-left: -2px !important;
    }
    .pass-coloum-forth input
    {
        width: 200px;
        padding:0 4px 0 4px !important;
    }
    .pass-coloum-forth select
    {
        width: 204px;
        height: 28px;
    }
    .pass-coloum-forth
    {
        width: 217px;
    }
    .pass-coloum-three select
    {
        width: 53px;
        height: 28px;
    }
    .pass-coloum-three
    {
        width: 80px;
    }
    .calDate
    {
        padding:0 4px 0 4px!important;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="left-content">
        <div class="bread-crum-in">
            P2P Booking >> <span>P2P Detail</span>
        </div>
        <div class="clear">
            &nbsp;</div>
        <asp:Repeater ID="rptPassDetail" runat="server" OnItemCommand="rptPassDetail_ItemCommand"
            OnItemDataBound="rptPassDetail_ItemDataBound">
            <ItemTemplate>
                <div class="privacy-block-outer pass">
                    <div class="red-title">
                        Journey&nbsp;<%# Container.ItemIndex + 1 %></div>
                    <div class="hd-title">
                        <asp:HiddenField ID="hdnPassP2PSaleLookupId" runat="server" Value='<%#Eval("PassP2PSaleID") %>' />
                        Train No:<%#Eval("TrainNo")%>,
                        <%#Eval("From") + " - " + Eval("To")%>,
                        <%# Eval("Passenger")%>
                        -
                        <%=currency %><%# Eval("NetPrice","{0:0.00}")%>
                        <asp:LinkButton ID="lnkDelete" CommandName="Delete" CommandArgument='<%#Eval("PassP2PSaleID")%>'
                            runat="server" OnClientClick="return window.confirm('Are you sure? Do you want to delete this item?')"><img src='images/btn-cross.png' class="icon-close scale-with-grid" alt="" border="0" /></asp:LinkButton>
                        <a href="javascript:void(0);">
                            <img src='images/arr-down.png' class="icon scale-with-grid imgOpen" alt="" border="0" /></a>
                    </div>
                    <div class="discription-block">
                        <div class="pass-coloum-three">
                            <asp:HiddenField ID="hdnTitle" runat="server" Value='<%#Eval("Title") %>' />
                            Title
                            <br/>
                            <asp:DropDownList ID="ddlTitle" class="input" runat="server">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="pass-coloum-forth">
                            First Name<%--<span class="valid3">*</span>--%>
                            <asp:TextBox ID="txtFirstName" runat="server" class="input chkvalid" Text='<%#Eval("FirstName") %>' MaxLength="40"
                                onkeyup="chkerrorvali(this)"/><br />
                           <%-- <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage='Please enter first name'
                                CssClass="valid" Text="Please enter First Name" Display="Dynamic" ForeColor="Red"
                                ValidationGroup="vg1" ControlToValidate="txtFirstName" />--%>
                            <br />
                            <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                        <div class="pass-coloum-forth">
                            Last Name<%--<span class="valid3">*</span>--%>
                            <asp:TextBox ID="txtLastName" runat="server" class="input chkvalid" Text='<%#Eval("LastName") %>' MaxLength="40"
                                onkeyup="chkerrorvali(this)"/><br />
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="valid"
                                Text="Please enter Last Name" ValidationGroup="vg1" Display="Dynamic" ForeColor="Red"
                                ControlToValidate="txtLastName" />--%>
                            <br />
                            <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtLastName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                        <div class="pass-coloum-forth">
                            <asp:HiddenField ID="hdnCountry" runat="server" Value='<%#Eval("Country") %>' />
                            Country
                            <asp:DropDownList ID="ddlCountry" runat="server" class="chkcountry" />
                            <br />
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="vg1"
                                CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlCountry" InitialValue="0" />--%>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="clear">
        </div>
        <div style="float: right">
            <asp:Button ID="btnChkOut" runat="server" Text="Continue" ValidationGroup="vg1" CssClass="button"
                OnClick="btnChkOut_Click" />
        </div>
    </div>
</asp:Content>
