﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPassDetail.ascx.cs"
    Inherits="IR_Admin.usercontrol.ucPassDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../Styles/PassBooking.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function hide() { $('.discription-block').hide(); }
    function show() { $('.discription-block').show(); }

    $(document).ready(function () {
        show();
        $('.imgOpen').click(function () {
            if ("images/arr-down.png" == $(this).attr('src').toString()) {
                $(this).attr('src', 'images/arr-right.png');
            }
            else {
                $(this).attr('src', 'images/arr-down.png');
            }
            $(this).parent().parent().parent().find('.discription-block').slideToggle();
        });
        $("#rptMainMenu_HyperLink1_3").addClass("active");
    });

    function chkVal(id, e) {
        if (e == 0)
            e = 3;

        var txtStartDateId = '#' + ($(id).find('input:first-child').attr('id'));
        $(txtStartDateId).datepicker({
            numberOfMonths: 2,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            firstDay: 1,
            minDate: 0,
            maxDate: '+' + e + 'm'
        });
        $(txtStartDateId).datepicker('show');
    }

    function chkDob(id, e) {
        var currentYear = (new Date).getFullYear();
        var txtDob = '#' + $(id).find('input[type=text]').attr('id');
        $(txtDob).datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1901:' + currentYear,
            onChangeMonthYear: function (year, month, inst) {
                var selectedDate = $(this).datepicker("getDate"); //Date object
                var day = selectedDate.getDate();
                selectedDate.setDate(day); //set first day of the month
                selectedDate.setMonth(month - 1); //month is 1-12, setMonth is 0-11
                selectedDate.setFullYear(year);
                $(this).datepicker("setDate", selectedDate);
            }
        });
        $(txtDob).datepicker('show');
    }

    //    function btnclick() {
    //        $(".chkvalid").each(function () {
    //            if (($(this).val() == '') || ($(this).val() == 'DD/MM/YYYY'))
    //                $(this).css("background-color", "#FCCFCF");
    //            else {
    //                $(this).css("background-color", "#FFF");
    //            }
    //        });
    //    };

    function chkerrorvali(e) {
        if ($.trim($(e).val()) != '' || $.trim($(e).val()) != 'DD/MM/YYYY')
            $(e).css({ "background-Color": "#FFF" });
        else
            $(e).css({ "background-Color": "#FCCFCF" });
    }
    function showmessagecountry(e) {
        alert("Country Pass Is Not Valid.");
    }

    function chkdatevalid() {
        $("input[id*=rptPassDetail_txtDob],input[id*=rptPassDetail_txtStartDate]").each(function () {
            var value = $(this).val();
            if (value.length == 11) {
                try {
                    var dateTxt = $.datepicker.parseDate('dd/M/yy', value);
                    var Todate = new Date(((new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + new Date().getFullYear()));
                    var MonthValidity = $(this).parent().find('.MonthValidity').text();
                    return false;
                    if (MonthValidity != '') {
                        var dateValidity = new Date(((Todate.getMonth() + 1 + parseInt(MonthValidity)) + "/" + Todate.getDate() + "/" + Todate.getFullYear()));
                        if (!(Todate <= dateTxt && dateTxt <= dateValidity)) {
                            $(this).val("DD/MMM/YYYY");
                            $(this).focus().select();
                            return false;
                        }
                    }
                }
                catch (e) {
                    $(this).val("DD/MMM/YYYY");
                    $(this).focus().select();
                    return false;
                }
            }
            else {
                $(this).val("DD/MMM/YYYY");
                $(this).focus().select();
                return false;
            }
        });
    }
</script>
<style type="text/css">
    .ui-datepicker
    {
        background: #fff !important;
    }
    .row
    {
            padding: 15px;
    }
    .forth-new
    {
        display:inline-block;
        width:24.5%;
    }
    .forth-new-first
    {
        display:inline-block;
        width:15%;
    }    
    .forth-new input,.forth-new-secound input
    {
        width:50%;
    }
    
    .forth-new-first select
    {
                width:50%;
    }
    .forth-new-secound
    {
        width:40%;
        display:inline-block;
    }
</style>
<%=script%>
<div class="left-content">
    <div class="bread-crum-in">
        Select Product >> Pass Booking >> <span>Pass Detail</span>
    </div>
    <p style="font-size: 13px">
        Please enter your details below carefully and ensure they are exactly as per your
        passport.
    </p>
    <div class="clear">
        &nbsp;</div>
    <asp:Repeater ID="rptPassDetail" runat="server" OnItemCommand="rptPassDetail_ItemCommand"
        OnItemDataBound="rptPassDetail_ItemDataBound">
        <ItemTemplate>
            <div class="privacy-block-outer pass">
                <div class="red-title">
                    Pass&nbsp;<%# Container.ItemIndex + 1 %></div>
                <div class="hd-title">
                    <asp:HiddenField ID="hdnPassSale" runat="server" Value='<%#Eval("PrdtId")+","+Eval("Price")+","+Eval("TravellerID")+","+Eval("ClassID")+","+Eval("ValidityID")+","+Eval("CategoryID")+","+Eval("commission")+","+Eval("MarkUp")+","+Eval("CountryStartCode")+","+Eval("CountryEndCode")+","+Eval("CountryLevelIDs")%>' />
                    <asp:HiddenField ID="HdnOriginalPrice" runat="server" Value='<%#Eval("OriginalPrice")%>' />
                    <asp:HiddenField ID="hdnPrdtId" runat="server" Value='<%#Eval("PrdtId")%>' />
                    <asp:HiddenField ID="hdnSalePrice" runat="server" Value='<%#Eval("SalePrice")%>' />
                    <asp:HiddenField ID="hdnCountryLevelIDs" runat="server" Value='<%#Eval("CountryLevelIDs")%>' />
                    <asp:Label ID="lblLID" runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                    <%#Eval("PrdtName")%>
                    <asp:Label ID="lblCountryName" runat="server" />,
                    <%#Eval("ValidityName")%>,
                    <%#Eval("TravellerName")%>,
                    <%#Eval("ClassName")%>
                    -
                    <%=currency.ToString()%>
                    <%#Eval("Price")%>
                    <asp:LinkButton ID="lnkDelete" CommandName="Delete" CommandArgument='<%#Eval("Id")%>'
                        runat="server" OnClientClick="return window.confirm('Are you sure? Do you want to delete this item?')"><img src='../images/btn-cross.png' class="icon-close scale-with-grid" alt="" border="0" /></asp:LinkButton>
                    <a href="javascript:void(0);">
                        <img src='../images/arr-down.png' class="icon scale-with-grid imgOpen" alt="" border="0" /></a>
                </div>
                <div class="discription-block">
                    <div class="row">
                        <div class="forth-new-first">
                            <label>
                                Title</label>
                            <asp:DropDownList ID="ddlTitle" class="input" runat="server">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="forth-new">
                            <label>
                                First Name<span class="valid3">*</span></label>
                            <asp:TextBox ID="txtFirstName" runat="server" class="input chkvalid" onkeyup="chkerrorvali(this)"
                                Text='<%#Eval("FirstName") %>' />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage='Please enter first name'
                                CssClass="valid" Text="Please enter First Name" Display="Dynamic" ForeColor="Red"
                                ValidationGroup="vg1" ControlToValidate="txtFirstName" />
                            
                            <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                        <div class="forth-new">
                            <label>
                                Middle Name</label>
                            <asp:TextBox ID="txtMiddleName" runat="server" class="input chkvalid" onkeyup="chkerrorvali(this)"
                                Text='<%#Eval("MiddleName") %>' />
                            
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFirstName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                        <div class="forth-new">
                            <label>
                                Last Name<span class="valid3">*</span></label>
                            <asp:TextBox ID="txtLastName" runat="server" class="input chkvalid" onkeyup="chkerrorvali(this)"
                                Text='<%#Eval("LastName") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="valid"
                                Text="Please enter Last Name" ValidationGroup="vg1" Display="Dynamic" ForeColor="Red"
                                ControlToValidate="txtLastName" />
                            
                            <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtLastName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                    </div>
                    <div class="row">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server"  class="forth-new-secound">
                            <ContentTemplate>
                                    <label>Country of residence</label>
                                    <asp:HiddenField ID="hdnproductid" runat="server" Value='<%#Eval("PrdtId") %>' />
                                    <asp:DropDownList ID="ddlCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_chkcounty"
                                        runat="server" class="chkcountry" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="vg1"
                                        CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="ddlCountry" InitialValue="0" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCountry" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div id="Div1" class="forth-new-secound" visible='<%#Eval("PassportIsVisible")%>' runat="server">
<label>Passport Number<span class="valid3">*</span></label>
                            <asp:TextBox ID="txtPassportNumber" Text='<%#Eval("PassportNo") %>' class="input chkvalid" runat="server" onkeyup="chkerrorvali(this)" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="vg1" runat="server"
                                CssClass="valid" Text="Please enter Passport No." Display="Dynamic" ForeColor="Red"
                                ControlToValidate="txtPassportNumber" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="forth-new-secound">
                             <label>D.O.B<span class="valid3">*</span></label>
                            <div style="display: inline;" id="Div2" onclick="chkDob(this,0)" onload="chkDob(this,0)">
                                <asp:TextBox ID="txtDob" MaxLength="11" runat="server" Text='<%# (Eval("DOB") != null ? Eval("DOB") : "DD/MMM/YYYY") %>'
                                    ToolTip="ex: 01/Jan/2014" class="input chkDobValid calDate" onfocusout="chkDate(event)" />
                                <span class="imgCal calIcon" title="Select Date of birth."></span>
                               <asp:RequiredFieldValidator ID="rfDob" ValidationGroup="vg1" runat="server" CssClass="valid"
                                    Text="Please enter Date of birth" Display="Dynamic" InitialValue="DD/MMM/YYYY"
                                    ForeColor="Red" ControlToValidate="txtDob" />
                            </div>
                        </div>
                        <div class="forth-new-secound">
                            <label>
                                Pass Start Date<span class="valid3">*</span></label>
                            <div style="display: inline;" onclick="chkVal(this,'<%#Eval("MonthValidity")%>')">
                                <asp:TextBox ID="txtStartDate" MaxLength="11" runat="server" Text='<%# (Eval("PassStartDate") != null ? Eval("PassStartDate","{0:dd/MMM/yyyy}") : "DD/MMM/YYYY") %>' class="input chkvalid calDate" onfocusout="chkDate(event)" />
                                <span class="imgCal calIcon" title="Select Pass Start Date."></span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="vg1" runat="server"
                                    CssClass="valid" Text="Please enter Date" Display="Dynamic" InitialValue="DD/MM/YYYY"
                                    ForeColor="Red" ControlToValidate="txtStartDate" />
                            </div>
                        </div>
                        <div>
                            <asp:Label ID="lblInvalidDate" Visible="False" runat="server" ForeColor="red"/>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="clear">
    </div>
    <div>
        <asp:Button ID="btnContinue" runat="server" Text="Continue Shopping" Style="margin-right: 10px;"
            CausesValidation="false" CssClass="button" OnClick="btnContinue_Click" />
        <asp:Button ID="btnChkOut" runat="server" Text="CheckOut" ValidationGroup="vg1" CssClass="button"
            OnClientClick="chkdatevalid()" OnClick="btnChkOut_Click" />
    </div>
</div>
