﻿using System;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using Image = System.Drawing.Image;
using System.Text.RegularExpressions;

namespace IR_Admin.usercontrol
{
    public partial class BannerImageManager : System.Web.UI.UserControl
    {
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        private int width, imgWd;
        private int height, imgHt;
        private int x, y = 0;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindCategory();
                imgFile.ImageUrl = "../CMSImages/sml-noimg.jpg";
            }

            if (Session["showBannerPopUp"] != null)
            {
                if (Convert.ToInt32(Session["showBannerPopUp"].ToString()) == 1)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "ShowPopup();", true);
            }
            Session["showBannerPopUp"] = 0;
        }

        private void BindCategory()
        {
            ddlCat.DataSource = _db.tblImgCategories.Where(x => x.IsActive == true).OrderBy(x => x.ID).ToList();
            ddlCat.DataTextField = "CategoryName";
            ddlCat.DataValueField = "ID";
            ddlCat.DataBind();
            ddlCat.Items.Insert(0, new ListItem("--Category--", "0"));

            ddlCat.SelectedValue = "1";
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ShowImageSize();
                if (btnSubmit.Text == "Submit")
                {
                    var imgUpload = (FileUpload)Session["imgUpload"];
                    if (imgUpload != null)
                    {
                        var _img = new tblImage();
                        _img.CategoryID = Convert.ToInt32(ddlCat.SelectedValue);
                        _img.ImageName = txtName.Text;
                        _img.ImagePath = "CMSImages/" + Regex.Replace(imgUpload.FileName, "[^a-zA-Z0-9_.]+", "");
                        _img.ImageSize = width + "*" + height;
                        var extension = Path.GetExtension(imgUpload.PostedFile.FileName);
                        if (extension != null)
                            _img.ImageType = extension.Replace(".", "");
                        _img.IsActive = chkactive.Checked;

                        if (imgUpload.FileContent != null)
                        {
                            int res = _Master.AddImage(_img);
                            ShowMessage(1, "You have successfully Added.");
                            Session["imgUpload"] = string.Empty;
                            ddlCat.SelectedIndex = 0;
                            txtName.Text = string.Empty;
                            chkactive.Checked = false;
                            imgFile.ImageUrl = SiteUrl + "CMSImages/sml-noimg.jpg";
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadCurrentPage", "ReloadCurrentPage();", true);
                        Session["showBannerPopUp"] = 1;
                    }
                    else
                        ShowMessage(2, "File not Uploaded.");
                }

            }
            catch (Exception ex)
            {
                hdnCrop.Value = "0";
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnUp_Click(object sender, EventArgs e)
        {
            if (imgUpload.HasFile)
            {
                try
                {
                    if (ddlCat.SelectedValue != "0")
                    {
                        ShowMessage(0, "");

                        ShowImageSize();
                        string imgType = Path.GetExtension(imgUpload.FileName);
                        if (imgType == ".jpg" || imgType == ".jpeg" || imgType == ".png" || imgType == ".bmp")
                        {
                            using (var myImage = Image.FromStream(imgUpload.PostedFile.InputStream))
                            {
                                imgWd = myImage.Width;
                                imgHt = myImage.Height;
                            }

                            if (imgWd < width || imgHt < height)
                                ShowMessage(1, "Image Size should be equal or more than " + width + "*" + height);
                            else
                            {
                                string filename = Path.GetFileName(imgUpload.FileName);
                                var cropArea = new Rectangle(x, y, width, height);
                                var bmpImage = new Bitmap(imgUpload.PostedFile.InputStream);

                                var format = bmpImage.PixelFormat;
                                var bmpCrop = bmpImage.Clone(cropArea, format);

                                if (filename != null) filename = Regex.Replace(filename, "[^a-zA-Z0-9_.]+", "");
                                bmpCrop.Save(Server.MapPath("~/CMSImages/") + filename);
                                Session["imgUpload"] = imgUpload;
                                imgFile.ImageUrl = SiteUrl + "CMSImages/" + Regex.Replace(imgUpload.FileName, "[^a-zA-Z0-9_.]+", "");
                            }
                        }
                        else
                            ShowMessage(2, "Upload only image file.");
                    }
                    else
                        ShowMessage(1, "Select Category.");
                }
                catch (Exception ex) { ShowMessage(2, ex.Message); }
            }
            else
                ShowMessage(2, "Upload File");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowBannerPopUp", "ShowBannerPopUp();", true);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        public void ShowImageSize()
        {
            width = Convert.ToInt32(txtWidth.Text.Trim());
            height = Convert.ToInt32(txtHeight.Text.Trim());
        }
    }
}