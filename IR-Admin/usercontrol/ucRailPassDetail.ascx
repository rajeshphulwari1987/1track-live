﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucRailPassDetail.ascx.cs"
    Inherits="IR_Admin.usercontrol.ucRailPassDetail" %>
<link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
<link href="../Styles/PassBooking.css" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
<%@ Register TagPrefix="uc" TagName="CULVL" Src="UCCountryLevel.ascx" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("ul.innertab li a:first").addClass("active").show();
        var iTS = $(".active").attr('inumber');
        $("#GV" + iTS.toString()).css("display", "block");
        callabc();
        onload();
    });

    function getcurrentLinkPosition() {
        $(".innertab li").find('a').each(function () {
            var classmatch = $(this).attr('class').replace(/ /g, '');
            if (classmatch == 'linkButtonactive' || classmatch == 'linkButtonpie_first-childactive' || classmatch == 'linkButtonactivepie_first-child') {
                $(this).trigger('click');
            }
        });
    }

    function showmsg() {
        alert('Please select at least one Travel Validity.');
        window.location = window.location;
    }
        
</script>
<%=script%>
<style>
.country-block-inner table select {width:45px;}
</style>
<section class="content">
    <div class="bread-crum-in">
         Select Product >> <span>Pass Booking</span>
    </div>
    <h1><div class="headingclass" runat="server" id="getproductname"> </div></h1>
<div class="left-content">
<asp:Repeater ID="rptPassDetail" runat="server">
    <ItemTemplate>
        <h1><%#Eval("Name")%></h1>
        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Description"))%>
</ItemTemplate>
</asp:Repeater>    

<div class="country-block-outer marg-b">
    <ul class="innertab">
          <asp:HiddenField ID="hdnGlobelTrName" runat="server" Value=''/>
        <asp:Repeater ID="rptTraveller" runat="server" 
              OnItemDataBound="rptTraveller_ItemDataBound" >
            <ItemTemplate>
                <li>
                    <asp:LinkButton ID="lnkTraveller" INumber='<%# Container.ItemIndex + 1 %>' ClientIDMode="Static" CssClass="linkButton" runat="server"  CommandName="PriceList" CommandArgument='<%#Eval("ID")%>' Text='<%#Eval("Name")%>' OnClientClick="JavaScript:ChangeLinkPosition(this); return false;" ></asp:LinkButton>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <asp:Repeater ID="rptTravellerGrid" runat="server" OnItemDataBound="rptTravellerGrid_ItemDataBound">
            <ItemTemplate>
             <div class="country-block-inner radius-block" style="display:none;" id="GV<%# Container.ItemIndex + 1 %>">
             <asp:HiddenField ID="hdnT" ClientIDMode="Static" Value='<%#Eval("Name")%>' runat="server"></asp:HiddenField>
             <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID")%>' Visible="false" ></asp:Label>
                <asp:GridView ID="grdPrice" runat="server" AutoGenerateColumns="true" OnRowDataBound="grdPrice_RowDataBound"  >
                </asp:GridView>
                </div>
            </ItemTemplate>
    </asp:Repeater>
    <div class="calculation-detail">
      <div id="divsubtotal">
        </div>
        <div class="colum-name"> <strong>Total</strong></div>
        <div class="colum-price"> <strong>  <div id="divTotal">         
        </div> </strong> </div>
    </div>
    <div class="detail-hints-main">
        <div class="detail-hints" id="divTraveller" runat="server">
        </div>
        <div class="btnBookkNow pos-rel">
             <asp:Button ID="btnBookNow" runat="server" class="button" Text="Book Now" style="margin-top: 4px;" onclick="btnBookNow_Click" OnClientClick="return checkSaverSelect();"></asp:Button>
        </div>
    </div>
    <div class="clear"></div>
<div class="country-clear">&nbsp;</div>
</div>
<div class="clear">&nbsp;</div>
</div>
  <asp:HiddenField id="hdnCurrencySign" runat="server" />
</section>
<div style="clear: both;">
</div>
<div id="pnlCULVL" class="popup" style="display: none;">
    <div class="modalBackground progessposition">
    </div>
    <div class="privacy-block-outer pass progess-inner" style="width: 550px!important">
        <div class="popuHeading" style="width: 549px; background: #951F35; line-height: 25px;">
            <div style="width: 325px; float: left; padding-left: 100px;">
                Country Selection</div>
            <a href="javascript:return(0)" id="lnkcncl" onclick="hideparentblock(this)" runat="server"
                style="color: #fff;margin-left:105px;">X </a>
        </div>
        <uc:CULVL ID="CULVL" runat="server" />
    </div>
</div>
<script type="text/javascript">
    var culevel = 0;
    var hdncountrycode = 0;
    function showcllevelbox() {
        $("#pnlCULVL").show();
    }
    function hideparentblock() {
        $("#pnlCULVL").hide();
        //disable
    }
    function hideparent() {
        $("#pnlCULVL").hide();
        //Get Country StartCode EndCpode 
        var myControl = document.getElementById(hdncountrycode);
        myControl.click();
        //enable
    }
    function bindCULVL(e) {
        //Get Text and Select ID and Value
        var indexval = $(e).prop("selectedIndex") == undefined ? 0 : $(e).prop("selectedIndex");
        culevel = parseInt($(e).prev('span').text() == '' ? '0' : $(e).prev('span').text());
        //hdncountrycode = $(e).next('div').find('a').attr('id');
        hdncountrycode = $(e).next('a').attr('id');
        if (indexval > 0 && culevel > 2) {
            $("#pnlCULVL").show();
        }
    }
    function checkSaverSelect() {
        var CountSaverMax = 0;
        var resultflag = false;
        var SaverName = "";
        var SaverPassesArray = [];
        $(".linkButton").each(function () {
            var strPass = $(this).text();
            var res = strPass.match(/saver/i);
            if (res) {
                SaverName = $(this).text() + " and " + SaverName;
                $("#GV" + $(this).attr("inumber").toString()).find("select").each(function () {
                    var i = parseInt($(this).val());
                    if (i > 0) {
                        SaverPassesArray.push($.trim($(this).parent().parent().find("td:first").text()) + "-" + i.toString());
                    }
                });
            }
        });
        var i = 0;
        var j = SaverPassesArray.length;
        if (j == 0) {
            resultflag = true;
        }
        else if (j == 1) {
            var SaverName1 = SaverPassesArray[0].split('-');
            var Fnum1 = parseInt($.trim(SaverName1[1]));
            if ((Fnum1 >= 2 && Fnum1 <= 5) || Fnum1 == 0) {
                resultflag = true;

            }
            else {
                resultflag = false;
            }
        }
        else {
            for (i = 0; i < j; i++) {
                var k = 0;
                var sameOther = false;
                for (k = 0; k < j; k++) {
                    if (i != k) {
                        var strFN = SaverPassesArray[i].split('-');
                        var strSN = SaverPassesArray[k].split('-');
                        if (strFN[0] == strSN[0]) {
                            sameOther = true;
                            var num1 = parseInt($.trim(strFN[1]));
                            var num2 = parseInt($.trim(strSN[1]));
                            var total = num1 + num2;
                            if ((total >= 2 && total <= 5) || total == 0) {
                                resultflag = true;
                            }
                            else {
                                resultflag = false;
                                break;
                            }
                        }
                    }
                }
                if (sameOther == false) {
                    var SaverName2 = SaverPassesArray[i].split('-');
                    var Fnum2 = parseInt($.trim(SaverName2[1]));
                    if (!((Fnum2 >= 2 && Fnum2 <= 5) || Fnum2 == 0)) {
                        resultflag = false;
                        break;
                    }
                    else {
                        resultflag = true;
                    }
                }
            }
        }
        if (resultflag == true) {
            return true;
        }
        else {
            alert('You can select minimum 2 or maximum 5 number of people for ' + SaverName.substring(0, SaverName.length - 4) + ' tickets.');
            return false;
        }
    }
    function callabc(e) {
        var array = new Array();
        var idx = 0;
        var strShow = "";
        var str = '';
        var Gtotal = 0;
        var currSign = $("#MainContent_ucRailPassDetail_hdnCurrencySign").val();
        var CountSaverMax = 0;
        var SaverName = "";
        var selID = e;
        var flag = false;
        if (selID != null) {
            var selIDSiblingFirstColumn = $("#" + $.trim(selID.id.toString())).parent().parent().find("td:first").text();
            $(".linkButton").each(function () {
                var strPass = $(this).text();
                var res = strPass.match(/saver/i);
                if (res) {
                    SaverName = $(this).text() + " and " + SaverName;
                    $("#GV" + $(this).attr("inumber").toString()).find("select").each(function () {
                        var casd = $(this).parent().parent().find("td:first").text();
                        if ($.trim(casd) == $.trim(selIDSiblingFirstColumn)) {
                            var i = parseInt($(this).val());
                            if (i > 0) {
                                CountSaverMax = CountSaverMax + i;
                            }
                        }
                    });
                }
            });
            var string = $("#" + $.trim(selID.id.toString())).parent().parent().parent().parent().parent().parent().find("#hdnT").val();
            var result = string.match(/saver/i);
            if (result) {
                if (CountSaverMax > 5) {
                    $("#" + $.trim(selID.id.toString())).val('0');
                    alert('Maximum number of people for ' + SaverName.substring(0, SaverName.length - 4) + ' tickets is 5.');
                    flag = true;
                    //return;
                } else {
                    flag = true;
                }
            } else {
                flag = true;
            }
            if (flag) {
                $(".country-block-inner").each(function (index, value) {
                    var iCount = 0;
                    $(this).find("select").each(function () {
                        var i = parseInt($(this).val());
                        if (i > 0) {
                            iCount = iCount + i;
                        }
                    });
                    if (iCount > 0) {
                        var Amt = 0;
                        $(this).find("select").each(function () {
                            var i = parseInt($(this).val());
                            var PAmt = parseFloat($(this).parent().find(".ltrName").text());
                            var subT = 0;
                            if (i > 0) {
                                subT = parseFloat(i) * PAmt;
                                Amt = Amt + subT;
                            }
                        });
                        str = '<div class="CLASS-010"><div class="colum-name">' + $(this).find("#hdnT").val() + ' x ' + iCount.toString() + '</div>' + '<div class="colum-price" style="line-height:28px;">' + currSign + ' ' + Amt.toFixed(0).toString() + '</div></div>';
                        Gtotal = parseFloat(Amt) + Gtotal;
                        array[idx] = str;
                        idx++;
                    }
                });
                $('#divsubtotal').html(array.sort());
                $('#divTotal').html(currSign + ' ' + Gtotal.toFixed(0).toString());
                bindCULVL(e);
            }
        }
    }

    /*Added for saver alert*/
    function callabcSaver(e) {
        var strShow = "";
        var str = '';
        var Gtotal = 0;
        var currSign = $("#MainContent_ucRailPassDetail_hdnCurrencySign").val();
        //var travellerNm = $('#lnkTraveller').text();
        //var travellerClass = $("#lnkTraveller").attr('class');

        $(".country-block-inner").each(function (index, value) {
            var iCount = 0;
            $(this).find("select").each(function () {
                var i = parseInt($(this).val());
                if (i > 0) {
                    iCount = iCount + i;
                }
            });
            if (iCount > 0) {
                if (iCount > 1 && iCount < 6) {
                    str = str + '<div class="CLASS-010"><div class="colum-name">' + iCount.toString() + ' x ' + $(this).find("#hdnT").val() + '</div>';
                    var Amt = 0;
                    $(this).find("select").each(function () {
                        var i = parseInt($(this).val());
                        var PAmt = parseFloat($(this).parent().find(".ltrName").text());
                        var subT = 0;
                        if (i > 0) {
                            subT = parseFloat(i) * PAmt;
                            Amt = Amt + subT;
                        }
                    });
                    str = str + '<div class="colum-price">' + currSign + ' ' + Amt.toFixed(2).toString() + '</div></div>';
                    Gtotal = parseFloat(Amt) + Gtotal;
                } else {
                    alert("Select passenger between 2 to 5");
                }
            }
        });

        $('#divsubtotal').html(str);
        $('#divTotal').html(currSign + ' ' + Gtotal.toFixed(2).toString());
        bindCULVL(e);
    }

    $("#GV1").css("display", "block");

    function ChangeLinkPosition(obj) {
        $(obj).addClass("linkButton active");
        $(obj).parents().siblings().children().removeClass('active');
        var item = $(obj).attr('inumber');
        $("#GV" + item.toString()).css("display", "block");
        $("#GV" + item.toString()).siblings('.country-block-inner').css("display", "none");
    }
</script>

