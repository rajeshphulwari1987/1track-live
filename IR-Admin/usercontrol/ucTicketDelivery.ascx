﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTicketDelivery.ascx.cs"
    Inherits="IR_Admin.usercontrol.ucTicketDelivery" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="round-titles">
            Ticket delivery
        </div>
        <div class="delivery-detail">
            <h1>
                Select your delivery method</h1>
            <p>
                Depending on the carrier, you can choose how to receive your tickets:</p>
            <p>
                &nbsp;</p>
            <div class="deliveryOptions">
                <div class="float-lt">
                    <asp:HiddenField ID="hiddenDileveryMethod" runat="server" />
                    <asp:RadioButtonList ID="rdoBkkoingList" runat="server" AutoPostBack="true" CssClass="clsdeliveryMethod options"
                        OnSelectedIndexChanged="rdoBkkoingList_SelectedIndexChanged" />
                </div>
                <div class="dvRtBlock">
                    <div id="deliverymethodDH" runat="server" style="display: block;">
                        <div class="clsTitle">
                            <asp:Literal ID="ltrDilverName" runat="server" /></div>
                        <div id="HeaderDhMsg" runat="server" style="display: none;">
                            <p>
                                <asp:Image ID="Image11" runat="server" ImageUrl="../images/icoHomePrint.jpg" CssClass="float-rt" />
                                Click the link in the confirmation email that you receive to download your ticket
                                as a PDF. Print it on a plain sheet of A4 paper and keep it to hand during your
                                journey.
                            </p>
                        </div>
                        <div id="HeaderStMsg" runat="server" style="display: none;">
                            <p>
                                You can collect your tickets at the international ticket desks. You will need the
                                booking reference (DNR) mentioned in the confirmation e-mail we'll send you. We
                                advise you to take this e-mail with you to the station.
                                <br />
                                <br />
                                <b>Please choose your collection station:</b>
                                <br />
                                <asp:DropDownList ID="ddlCollectStation" runat="server" Width="200px" CssClass="chkcountry">
                                    <asp:ListItem Value="0">--Choose your station--</asp:ListItem>
                                    <asp:ListItem Value="BEAAL">Aalst (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEALT">Aalter (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEAAR">Aarschot (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLASC">Amsterdam (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEANS">Ans (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBHM">Antwerpen-Berchem (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEABC">Antwerpen-Centraal (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEARL">Arlon (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLAHM">Arnhem (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEATH">Ath (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBLA">Blankenberge (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBRL">Braine-l Alleud (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBLC">Braine-le-Comte (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLBDA">Breda (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEBRG">Brugge (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEABT">Brussel Nat Luchthaven (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBCE">Bruxelles-Central (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBQL">Bruxelles-Luxembourg (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBMI">Bruxelles-Midi (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBNO">Bruxelles-Nord (Be)</asp:ListItem>
                                    <asp:ListItem Value="BESHU">Bruxelles-Schuman (Be)</asp:ListItem>
                                    <asp:ListItem Value="BECHS">Charleroi-Sud (Be)</asp:ListItem>
                                    <asp:ListItem Value="BECIN">Ciney (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDPA">De Panne (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDPI">De Pinte (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDEI">Deinze (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLDHC">Den Haag (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEDEW">Denderleeuw (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDMD">Dendermonde (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDIS">Diest (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEENG">Enghien (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEEUP">Eupen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEE">Geel (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEM">Gembloux (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEN">Genk (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGDA">Gent Dampoort (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGNT">Gent-Sint-Pieters (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEL">Genval (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHAL">Halle (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHAS">Hasselt (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHES">Herentals (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHUY">Huy (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEIEP">Ieper (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEIZE">Izegem (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEKAP">Kapellen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEKNO">Knokke (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEKOR">Kortrijk (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELHP">La Hulpe (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELAS">La Louviere Sud (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELEU">Leuven (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELIB">Libramont (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELGG">Liege-Guillemins (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELPA">Liege-Palais (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELIR">Lier (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELOK">Lokeren (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELOU">Louvain La Neuve Univ (Be)</asp:ListItem>
                                    <asp:ListItem Value="LULUX">Luxembourg (Lu)</asp:ListItem>
                                    <asp:ListItem Value="BEMAL">Marloie (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEMEC">Mechelen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEMOL">Mol (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEQMO">Mons (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEMOU">Mouscron (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEQNM">Namur (Be)</asp:ListItem>
                                    <asp:ListItem Value="BENIV">Nivelles (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEOST">Oostende (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEOTT">Ottignies (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEOUD">Oudenaarde (Be)</asp:ListItem>
                                    <asp:ListItem Value="BERIX">Rixensart (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEROE">Roeselare (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLRTC">Rotterdam (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BESIN">Sint-Niklaas (Be)</asp:ListItem>
                                    <asp:ListItem Value="BESIT">Sint-Truiden (Be)</asp:ListItem>
                                    <asp:ListItem Value="BESOI">Soignies (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETIT">Tielt (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETIE">Tienen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETOU">Tournai (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETUB">Tubize (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETUR">Turnhout (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLUTC">Utrecht Cs (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEVEC">Verviers-Central (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEVIL">Vilvoorde (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEVIS">Vise (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWAR">Waregem (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWAM">Waremme (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWTL">Waterloo (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWAV">Wavre (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWET">Wetteren (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEZOT">Zottegem (Be)</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqCollectStation" runat="server" Text="*" ErrorMessage="Please choose station name."
                                    InitialValue="0" ControlToValidate="ddlCollectStation" ForeColor="#eaeaea" ValidationGroup="vgs1"
                                    Enabled="false" />
                            </p>
                        </div>
                        <div class="clear">
                        </div>
                        <div runat="server" id="divLoyalty" visible="false">
                            <p>
                                <strong>Loyalty cards:</strong> Eurostar:
                                <asp:Literal ID="lblEuCardNumber" runat="server" />, Thalys:
                                <asp:Literal ID="lblThCardNumber" runat="server" />
                            </p>
                        </div>
                        <div>
                            <p>
                                Your personal details are processed in accordance with the <a href="PrivacyPolicy"
                                    target="_blank">privacy policy of InternationalRail.</a></p>
                            <div id="FooterStMsg" runat="server" style="display: none;">
                                <p>
                                    Your personal details are processed in accordance with the privacy policy of International
                                    Rail. Important:
                                    <br />
                                    if you pay by credit card, the card holder will be asked to produce the card and
                                    identity document (such as passport) at the time of ticket collection. Virtual credit
                                    cards are not accepted. Please enquire about the opening times of the international
                                    ticket desks at the selected station.
                                </p>
                            </div>
                            <asp:DataList ID="dtlPassngerDetails" runat="server" RepeatColumns="1" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <div class="divLoyHeader">
                                        <b>
                                            <%#Eval("PassangerType")%></b>
                                    </div>
                                    <div class="divThy" style="float: left;">
                                        <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" Width="180px" />
                                        <asp:RequiredFieldValidator ID="reqFirstName" runat="server" Text="*" ErrorMessage="Please enter First Name."
                                            ControlToValidate="txtFirstName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                        <asp:TextBoxWatermarkExtender runat="server" ID="wtrFirstName" WatermarkText="First Name"
                                            TargetControlID="txtFirstName" WatermarkCssClass="WmaxCss" />
                                        <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                            ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                    </div>
                                    <div class="divThy" style="float: left;">
                                        <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" Width="180px" />
                                        <asp:RequiredFieldValidator ID="reqLastName" runat="server" Text="*" ErrorMessage="Please enter Last Name."
                                            ControlToValidate="txtLastName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                        <asp:TextBoxWatermarkExtender runat="server" ID="wtrLastName" WatermarkText="Last Name"
                                            TargetControlID="txtLastName" WatermarkCssClass="WmaxCss" />
                                        <asp:FilteredTextBoxExtender ID="ftex" runat="server" TargetControlID="txtLastName"
                                            ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="spacer20">
                        </div>
                        <div class="clsbckColor" id="FooterDhMsg2" runat="server" style="display: none;">
                            <p>
                                Self print tickets are strictly personal and non-transferable. Enter your names
                                above as they appear in your passport, if possible.
                                <br>
                                Note: the first name field is limited to 15 characters.</p>
                            <div class="reset">
                            </div>
                        </div>
                    </div>
                    <div id="deliverymethodTL" runat="server" style="display: none;">
                        <div class="clsTitle">
                            Thalys Ticketless</div>
                        <p>
                            Are you a member of the Thalys The Card&nbsp;loyalty programme? Enter the&nbsp;card
                            number for each passenger wanting to travel Ticketless and earn points for this
                            trip.</p>
                        <br>
                        <asp:Image ID="Image1" runat="server" ImageUrl="../images/ticketless.jpg" Width="370"
                            Height="145" />
                        <div>
                            <div class="clear">
                            </div>
                            <asp:DataList ID="dtlLoayalty" runat="server" RepeatColumns="1" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <div class="divLoyHeader">
                                        <%#Eval("PassangerType")%>
                                    </div>
                                    <div class="divLoyleft">
                                        <asp:TextBox ID="txtloyCardThy" runat="server" CssClass="txtThyleft" Style="color: #FFF;"
                                            Text="Thalys" ReadOnly="True" />
                                    </div>
                                    <div class="divThy">
                                        <asp:TextBox ID="txtThalysCardNumber" runat="server" MaxLength="17" Width="180px"
                                            Text='<%#Eval("cardnumber") %>' />
                                        <asp:RequiredFieldValidator ID="reqCardNumber" runat="server" Text="*" ErrorMessage="Please enter Thalys card number."
                                            ControlToValidate="txtThalysCardNumber" ForeColor="#eaeaea" ValidationGroup="vgs1"
                                            Enabled="false" />
                                        <asp:TextBoxWatermarkExtender runat="server" ID="txtThyP" WatermarkText="(card number)"
                                            TargetControlID="txtThalysCardNumber" WatermarkCssClass="WmaxCss" />
                                        <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtThalysCardNumber"
                                            ValidChars="0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="spacer20">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
