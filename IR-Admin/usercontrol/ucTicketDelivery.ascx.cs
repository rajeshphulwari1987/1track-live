﻿#region Using
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using IR_Admin.OneHubServiceRef;

#endregion

namespace IR_Admin.usercontrol
{
    public partial class ucTicketDelivery : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["TrainSearchRequest"] != null && !IsPostBack)
                    BindList();
                BindDilveryOption();
            }
        }

        void BindDilveryOption()
        {
            try
            {
                if (Session["BOOKING-REQUEST"] != null)
                {
                    List<BookingRequest> list = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                    var dlist = list.SelectMany(x => x.PurchasingForServiceRequest.BookingRequestList.SelectMany(y => y.TrainPrice.SelectMany(z => z.ProductionModeList))).Distinct().ToList();
                    List<ProductionModes> listnew = new List<ProductionModes>();
                    foreach (var item in dlist)
                    {
                        if (listnew == null || !listnew.Any(x => x.ProductionMode == item.ProductionMode))
                            listnew.Add(item);
                    }

                    rdoBkkoingList.DataSource = listnew.OrderBy(x => x.ProductionDisplayMode).ToList();

                    rdoBkkoingList.DataTextField = "ProductionDisplayMode";
                    rdoBkkoingList.DataValueField = "DeliveryMethod";
                    rdoBkkoingList.DataBind();

                    rdoBkkoingList.SelectedIndex = 0;
                    BindOptionArea();
                }
            }
            catch { }
        }

        public void BindList()
        {

            TrainInformationRequest objrequest = Session["TrainSearchRequest"] as TrainInformationRequest;
            if (objrequest.Loyaltycards != null)
            {
                foreach (var item in objrequest.Loyaltycards.Where(x => x.carrier.code == "EUR").ToList())
                    lblEuCardNumber.Text = string.IsNullOrEmpty(lblEuCardNumber.Text) ? item.cardnumber : lblEuCardNumber.Text + ", " + item.cardnumber;

                foreach (var item in objrequest.Loyaltycards.Where(x => x.carrier.code == "THA").ToList())
                    lblThCardNumber.Text = string.IsNullOrEmpty(lblThCardNumber.Text) ? item.cardnumber : lblThCardNumber.Text + ", " + item.cardnumber;

                BindLoyaltyList(objrequest.NumAdults + objrequest.NumYouths + objrequest.NumSeniors, objrequest.NumBoys, objrequest.Loyaltycards.Where(x => x.carrier.code == "THA").ToArray());
                divLoyalty.Visible = true;
            }
            else
                BindLoyaltyList(objrequest.NumAdults + objrequest.NumYouths + objrequest.NumSeniors, objrequest.NumBoys, null);

            BindPassengerList(objrequest.NumAdults + objrequest.NumYouths + objrequest.NumSeniors, objrequest.NumBoys);
        }

        public void BindLoyaltyList(int Adult, int child, Loyaltycard[] loyArray)
        {
            var list = new List<Passanger>();
            int cnt = Adult;
            int countLoay = 0;
            if (loyArray != null)
                countLoay = loyArray.Count();
            for (int i = 0; i < cnt; i++)
            {
                list.Add(new Passanger
                {
                    PassangerType = "Adult",
                    cardnumber = loyArray != null ? loyArray[i].cardnumber : ""
                });
                countLoay--;
            }

            int cntc = child;
            for (int i = 0; i < cntc; i++)
            {
                list.Add(new Passanger
                {
                    PassangerType = "Child",
                    cardnumber = loyArray != null ? loyArray[i].cardnumber : ""
                });
                countLoay--;
            }

            dtlLoayalty.DataSource = list;
            dtlLoayalty.DataBind();
        }

        public void BindPassengerList(int Adult, int child)
        {
            var list = new List<Passanger>();
            int cnt = Adult;

            for (int i = 0; i < cnt; i++)
            {
                list.Add(new Passanger
                {
                    PassangerType = "Adult" + (i + 1).ToString()
                });
            }

            int cntc = child;
            for (int i = 0; i < cntc; i++)
            {
                list.Add(new Passanger
                {
                    PassangerType = "Child" + (i + 1).ToString()
                });

            }
            dtlPassngerDetails.DataSource = list;
            dtlPassngerDetails.DataBind();
        }

        protected void rdoBkkoingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindOptionArea();
            }
            catch (Exception ex) { throw ex; }
        }

        void BindOptionArea()
        {
            hiddenDileveryMethod.Value = rdoBkkoingList.SelectedValue;
            ltrDilverName.Text = rdoBkkoingList.SelectedItem.Text;

            if (hiddenDileveryMethod.Value == "TL")
            {
                deliverymethodTL.Style.Add("display", "block");
                deliverymethodDH.Style.Add("display", "none");
            }
            else
            {
                deliverymethodTL.Style.Add("display", "none");
                deliverymethodDH.Style.Add("display", "block");
            }

            if (hiddenDileveryMethod.Value == "ST")
            {
                HeaderDhMsg.Style.Add("display", "none");
                FooterDhMsg2.Style.Add("display", "none");

                HeaderStMsg.Style.Add("display", "block");
                FooterStMsg.Style.Add("display", "block");
                reqCollectStation.Enabled = true;
            }
            else
            {
                reqCollectStation.Enabled = false;
                ddlCollectStation.SelectedIndex = 0;
                HeaderDhMsg.Style.Add("display", "block");
                FooterDhMsg2.Style.Add("display", "block");

                HeaderStMsg.Style.Add("display", "none");
                FooterStMsg.Style.Add("display", "none");
            }

            foreach (DataListItem li in dtlLoayalty.Items)
            {
                RequiredFieldValidator req = (RequiredFieldValidator)li.FindControl("reqCardNumber");
                req.Enabled = (hiddenDileveryMethod.Value == "TL");
            }
            List<BookingRequest> bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;

            foreach (DataListItem li in dtlPassngerDetails.Items)
            {
                RequiredFieldValidator reqFirstName = (RequiredFieldValidator)li.FindControl("reqFirstName");
                reqFirstName.Enabled = (hiddenDileveryMethod.Value != "TL");

                RequiredFieldValidator reqLastName = (RequiredFieldValidator)li.FindControl("reqLastName");
                reqLastName.Enabled = (hiddenDileveryMethod.Value != "TL");

            }

            foreach (DataListItem li in dtlPassngerDetails.Items)
            {
                TextBox txtFirstName = li.FindControl("txtFirstName") as TextBox;
                TextBox txtLastName = li.FindControl("txtLastName") as TextBox;

                foreach (var item in bookingList)
                {
                    if (item.PurchasingForServiceRequest != null)
                    {
                        var rec = item.PurchasingForServiceRequest.PassengerListReply.FirstOrDefault();
                        txtFirstName.Text = rec.FirstName;
                        txtLastName.Text = rec.LastName;
                    }
                    break;
                }
                break;
            }



            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "showHideShipping", "showHideShipping();", true);

        }
    }
}