﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeBehind="ucBookingCart.ascx.cs"
    Inherits="IR_Admin.usercontrol.ucBookingCart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../Styles/BookingCart.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $("#radioChkShp").attr("checked", "checked");
        showHideShipping();
        ShowCall();
        getdata();
        restrictCopy();
    });
    function chkerrorvali(e) {
        if ($.trim($(e).val()) != '')
            $(e).css({ "background-Color": "#FFF" });
        else
            $(e).css({ "background-Color": "#FCCFCF" });
    }
    function callshipping() {
        getdata();
        alert("Please choose shipping amount.");
    }
    function restrictCopy() {
        $(function () {
            $('#MainContent_ucBookingCart_txtConfirmEmail').bind("cut copy paste", function (e) {
                e.preventDefault();
            });
            $('#MainContent_ucBookingCart_txtshpConfirmEmail').bind("cut copy paste", function (e) {
                e.preventDefault();
            });
        });
    }

    function ShowCall() {
        $(".calDate, #calImg").datepicker({
            numberOfMonths: 2,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            firstDay: 1,
            minDate: 0
        });
        $(".calDate").keypress(function (event) { event.preventDefault(); });
        var date = $("#MainContent_ucBookingCart_ldlpassDate").text();
        date = date.split('/');
        
        var d = new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0]));
        $(".calDate, #calImg").datepicker("option", "maxDate", d);
        $(".calDate, #calImg").val("DD/MM/YYYY");
    }

    function getdata() {
        var countPrice = 0.00;
        var bookingFee = 0.00;
        $(".calculatePrice").each(function () {
            var price = $(this).text() == '' ? 0 : $(this).text();
            countPrice = countPrice + parseFloat(price);
        });
        bookingFee = parseFloat($.trim($("#MainContent_ucBookingCart_rptTrain_txtBookingFee").val()));
        countPrice = bookingFee + countPrice;
        $("#lblTotal").text(countPrice.toFixed(2));
        $(".shipping").each(function () {
            if ($(this).is(":checked")) {
                var str = $(this).val().split('^');

                var price = str[0];
                var smethod = str[1];
                var description = str[2];
                $("#MainContent_ucBookingCart_hdnShippingCost").attr('value', price.toString());
                $("#MainContent_ucBookingCart_hdnShipMethod").attr('value', smethod.toString());
                $("#MainContent_ucBookingCart_hdndescription").attr('value', description.toString());
                countPrice = countPrice + parseFloat(price);
            }
        });
        $("#MainContent_ucBookingCart_lblTotalCost").text(countPrice.toFixed(2));
    }

    function BookingFee() {
        var amount = $("#MainContent_ucBookingCart_rptTrain_txtBookingFee").val();
        if (amount != "") {
            getdata();
            $("#MainContent_ucBookingCart_hdnBookingFee").val(amount);
        }
        else
            $("#MainContent_ucBookingCart_rptTrain_txtBookingFee").val("0");
    }
    
    function changeColor() {
        $(".validcheck").each(function () {
            if ($(this).val() == '')
                $(this).css({ "background-Color": "#FCCFCF" });
        });
    }
</script>
<script type="text/javascript">

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function showHideShipping() {
        if ($("#MainContent_ucTicketDelivery_hiddenDileveryMethod").val() == "DH") {
            $("#MainContent_ucBookingCart_pnlShipping").hide();
            $("#MainContent_ucBookingCart_hdnShippingCost").val("0");
            $("#MainContent_ucBookingCart_lblTotalCost").text($("#lblTotal").text());
        }
        else {
            getdata();
            $("#MainContent_ucBookingCart_pnlShipping").show();
            var shipping = $("#MainContent_ucBookingCart_hdnShippingCost").val();
            var subTotal = $("#lblTotal").text();
            var total = parseFloat(subTotal) + parseFloat(shipping);
            $("#MainContent_ucBookingCart_lblTotalCost").text(total.toFixed(2));
        }
    }
</script>
<%=script%>
<asp:HiddenField ID="hdnBookingFee" runat="server" Value="0.00" />
<asp:HiddenField ID="hdnShipMethod" runat="server" Value="" />
<asp:HiddenField ID="hdndescription" runat="server" Value="" />

<div class="content">
    <div class="left-content">
        <div class="bread-crum-in">
            Select Product >> Pass Booking >> Pass Detail >> <span>Booking Details </span>
        </div>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <br />
        <div class="round-titles">
            Review
        </div>
        <div class="booking-detail-in" style="padding: 0; width: 98.5%;">
            <asp:Repeater ID="rptTrain" runat="server" OnItemDataBound="rptTrain_ItemDataBound">
                <HeaderTemplate>
                    <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td width="100%" colspan="3" style="background-color: #5e5e5e; color: White;">
                            <%#Eval("PassDesc")%>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%">
                            <%#Eval("Traveller")%>
                        </td>
                        <td width="20%" style="vertical-align: bottom">
                            <asp:Label ID="lblTckProc" runat="server" Text="Ticket Protection" />
                        </td>
                        <td width="40%">
                            <asp:UpdatePanel ID="updTrain" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    &nbsp;&nbsp;&nbsp;
                                    <div id="divTckProt" runat="server" style="float: left">
                                        <asp:Label runat="server" ID="currsyb" Text='<%#currency%>'></asp:Label>
                                        <asp:Label ID="lbltpPrice" runat="server" Text='<%#Eval("TicketProtectionPrice") %>' />
                                        <asp:CheckBox ID="chkTicketProtection" AutoPostBack="true" CssClass="calculateTotal"
                                            OnCheckedChanged="chk_CheckedChanged" runat="server" Checked='<%#Eval("TicketProtection")%>' />
                                        <a onclick="showthis()" href="#">
                                            <img src="../images/info.png" width="20" /></a>
                                    </div>
                                    <div style="float: right">
                                        <%=currency%>
                                        <asp:Label ID="lblPrice" CssClass="calculatePrice" runat="server" Text='<%#Eval("Price")%>'
                                            Style="margin-right: 8px;" />
                                        <asp:Label ID="lblPassSaleID" runat="server" Visible="false" Text='<%#Eval("ProductID")%>' />
                                        <asp:Label ID="lblHidPriceValue" runat="server" Visible="false" Text='<%#Eval("Price")%>' />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="chkTicketProtection" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="100%" colspan="3" style="background-color: #5e5e5e; color: White;">
                                Booking Fee
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">
                                &nbsp;
                            </td>
                            <td width="20%">
                                &nbsp;
                            </td>
                            <td width="40%">
                                <div style="float: right">
                                    <%=currency%>
                                    <asp:TextBox ID="txtBookingFee" runat="server" Text="" Style="margin-right: 8px;;width: 50px;" onblur="BookingFee();"/>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="total">
                        Sub Total:
                        <%=currency%>
                        <asp:Label ID="lblTotal" ClientIDMode="Static" runat="server"></asp:Label>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Repeater ID="rptTrainTcv" runat="server" OnItemCommand="rptTrainTcv_ItemCommand"
                OnItemDataBound="rptTrainTcv_ItemDataBound">
                <HeaderTemplate>
                    <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr style="background-color: #5e5e5e;">
                            <th width="50%">
                                &nbsp;
                            </th>
                            <th width="15%">
                                &nbsp;
                            </th>
                            <th width="10%">
                                &nbsp;
                            </th>
                            <th width="25%" valign="middle" colspan="2">
                                Ticket Protection <a onclick="showthis()" href="#">
                                    <img src="../images/info.png" width="17" /></a>
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            Ticket (TCV):
                            <asp:Label ID="lblFrm1" runat="server" Text='<%#Eval("DepartureStation")%>' />
                            -
                            <asp:Label ID="lblTo1" runat="server" Text='<%#Eval("ArrivalStation")%>' />
                            <br />
                            From:
                            <asp:Label ID="lblFrm" runat="server" Text='<%#Eval("DepartureStation")%>' />
                            <br />
                            To:
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ArrivalStation")%>' />
                            <br />
                            <asp:Label ID="lblService" runat="server" Text='<%#Eval("ServiceName")%>' />
                            <asp:Label ID="lblPngrType" runat="server" Text='<%#Eval("Passenger")%>' />
                            <br />
                            <asp:Label ID="lblFare" runat="server" Text='<%#Eval("Fare")%>' />
                        </td>
                        <td>
                            <%#Eval("Title")%>
                            <%#Eval("FirstName")%>
                            <%#Eval("LastName")%>
                        </td>
                        <td>
                            <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("Price")%>' />
                        </td>
                        <td width="10%" valign="middle">
                            <input type="checkbox" />
                        </td>
                        <td width="15%" valign="middle">
                            <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%#Eval("Id")%>' CommandName="Remove"><img src='images/icon-delete-new.png' /></asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <div class="total">
                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <%--<uc:ucTicketDelivery ID="ucTicketDelivery" runat="server" />--%>
        <div class="round-titles">
            Billing Address
            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="vgs1" DisplayMode="List"
                ShowSummary="false" ShowMessageBox="true" runat="server" />
        </div>
        <div class="booking-detail-in delvery" style="height: 383px; height: 425px">
            <div class="colum-label-first">
                Title
            </div>
            <div class="colum-label-second">
                <asp:DropDownList ID="ddlMr" runat="server" class="inputsl" Width="50px">
                    <asp:ListItem>Mr.</asp:ListItem>
                    <asp:ListItem>Mrs.</asp:ListItem>
                    <asp:ListItem>Ms.</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colum-label-third">
                First Name<span class="readerror">*</span></div>
            <div class="colum-label-forth">
                <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                    ClientIDMode="Static" />
                <asp:RequiredFieldValidator ID="rfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                    ControlToValidate="txtFirst" ForeColor="#eaeaea" ValidationGroup="vgs1" />
            </div>
            <div class="colum-label-fifth">
                Last name<span class="readerror">*</span></div>
            <div class="colum-label-sixth">
                <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)" />
                <asp:RequiredFieldValidator ID="rfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                    ControlToValidate="txtLast" ForeColor="#eaeaea" ValidationGroup="vgs1" />
            </div>
            <div class="colum-label-first">
                Email<span class="readerror">*</span></div>
            <div class="colum-label-second">
                &nbsp;
            </div>
            <div class="colum-label-third">
                &nbsp;
            </div>
            <div class="colum-label-forth">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                    autocomplete="off">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                    ControlToValidate="txtEmail" ForeColor="#eaeaea" ValidationGroup="vgs1" Display="Dynamic" />
                <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                    ControlToValidate="txtEmail" ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    Display="Dynamic" ValidationGroup="vgs1" />
            </div>
            <div class="colum-label-fifth">
                Confirm<span class="readerror">*</span></div>
            <div class="colum-label-sixth">
                <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                    autocomplete="off" />
                <asp:RequiredFieldValidator ID="rfvEmail2" runat="server" Text="*" ErrorMessage="Please enter confirm email."
                    ControlToValidate="txtEmail" ForeColor="#eaeaea" ValidationGroup="vgs1" Display="Dynamic" />
                <asp:RegularExpressionValidator ID="regEmail2" runat="server" Text="*" ErrorMessage="Please enter valid confirm email."
                    ControlToValidate="txtConfirmEmail" ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    Display="Dynamic" ValidationGroup="vgs1" />
                <asp:CompareValidator ID="cmpEmailCompare" ForeColor="#eaeaea" ControlToCompare="txtEmail"
                    Display="Dynamic" ControlToValidate="txtConfirmEmail" runat="server" Text="*"
                    ValidationGroup="vgs1" ErrorMessage="Email and confirm email not matched."></asp:CompareValidator>
            </div>
            <div class="colum-label-seven">
                Phone Number<span class="readerror">*</span>
            </div>
            <div class="colum-label-eight" style="height: auto;">
                <asp:TextBox ID="txtBillPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                    MaxLength="15" onkeypress="return isNumberKey(event)" />
                <asp:RequiredFieldValidator ID="BookingpassrfPhone" runat="server" Text="*" ErrorMessage="Please enter phone number."
                    ControlToValidate="txtBillPhone" ForeColor="#eaeaea" />
            </div>
            <div class="colum-label-seven">
                Address Line 1 Or Company Name<span class="readerror">*</span>
            </div>
            <div class="colum-label-eight" style="height: auto;">
                <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)" />
                <asp:RequiredFieldValidator ID="rfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                    ControlToValidate="txtAdd" ForeColor="#eaeaea" ValidationGroup="vgs1" />
            </div>
            <div class="colum-label-seven">
                Address Line 2
            </div>
            <div class="colum-label-eight" style="height: auto;">
                <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" />
            </div>
            <div class="colum-label-seven">
                Town / City</div>
            <div class="colum-label-eight">
                <asp:TextBox ID="txtCity" runat="server" CssClass="input" />
            </div>
            <div class="colum-label-seven">
                County / State</div>
            <div class="colum-label-eight">
                <asp:TextBox ID="txtState" runat="server" CssClass="input" />
            </div>
            <div class="colum-label-seven">
                Postal/Zip Code<span class="readerror">*</span></div>
            <div class="colum-label-eight">
                <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                    MaxLength="10" />
                <asp:RequiredFieldValidator ID="rfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                    ControlToValidate="txtZip" ForeColor="#eaeaea" ValidationGroup="vgs1" />
            </div>
            <div class="colum-label-seven">
                Country<span class="readerror">*</span></div>
            <div class="colum-label-eight">
                <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" />
                <asp:RequiredFieldValidator ID="rfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                    ControlToValidate="ddlCountry" InitialValue="0" ValidationGroup="vgs1" ForeColor="#eaeaea" />
            </div>
            <div class="colum-label-seven">
                Amend Shipping Details if different to billing address
            </div>
            <div class="colum-label-eight">
                <asp:CheckBox ID="chkShippingfill" AutoPostBack="true" runat="server" OnCheckedChanged="chkShippingfill_CheckedChanged" />
            </div>
            <asp:Label runat="server" ID="lblmsg" />
        </div>
        <%--Shipping Address Start--%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlbindshippping" runat="server" Visible="false">
                    <div class="round-titles">
                        Shipping Address
                    </div>
                    <div class="booking-detail-in delvery">
                        <div class="colum-label-first">
                            Title
                        </div>
                        <div class="colum-label-second">
                            <asp:DropDownList ID="ddlshpMr" runat="server" class="inputsl" Width="50px">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label-third">
                            First Name<span class="readerror">*</span>
                        </div>
                        <div class="colum-label-forth">
                            <asp:TextBox ID="txtshpfname" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)" />
                            <asp:RequiredFieldValidator ID="rq1" runat="server" Text="*" ErrorMessage="Please enter Shipping first name."
                                ControlToValidate="txtshpfname" ForeColor="#eaeaea" ValidationGroup="vgs1" />
                        </div>
                        <div class="colum-label-fifth">
                            Last name<span class="readerror">*</span></div>
                        <div class="colum-label-sixth">
                            <asp:TextBox ID="txtshpLast" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)" />
                            <asp:RequiredFieldValidator ID="rq2" runat="server" Text="*" ErrorMessage="Please enter Shipping last name."
                                ControlToValidate="txtshpLast" ForeColor="#eaeaea" ValidationGroup="vgs1" />
                        </div>
                        <div class="colum-label-first">
                            Email<span class="readerror">*</span>
                        </div>
                        <div class="colum-label-second">
                            &nbsp;
                        </div>
                        <div class="colum-label-third">
                            &nbsp;
                        </div>
                        <div class="colum-label-forth">
                            <asp:TextBox ID="txtshpEmail" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                                autocomplete="off" />
                            <asp:RequiredFieldValidator ID="rq3" runat="server" Text="*" ErrorMessage="Please enter Shipping email address."
                                ControlToValidate="txtshpEmail" ForeColor="#eaeaea" ValidationGroup="vgs1" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="regx1" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                                ControlToValidate="txtshpEmail" ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic" ValidationGroup="vgs1" />
                        </div>
                        <div class="colum-label-fifth">
                            Confirm<span class="readerror">*</span></div>
                        <div class="colum-label-sixth">
                            <asp:TextBox ID="txtshpConfirmEmail" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                                autocomplete="off" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="*"
                                ErrorMessage="Please enter Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                                ForeColor="#eaeaea" ValidationGroup="vgs1" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="*"
                                ErrorMessage="Please enter valid Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                                ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic" ValidationGroup="vgs1" />
                            <asp:CompareValidator ID="CompareValidator1" ForeColor="#eaeaea" ControlToCompare="txtshpEmail"
                                Display="Dynamic" ControlToValidate="txtshpConfirmEmail" runat="server" Text="*"
                                ErrorMessage="Shipping Email and confirm email not matched." ValidationGroup="vgs1"></asp:CompareValidator>
                        </div>
                        <div class="colum-label-seven">
                            Phone Number<span class="readerror">*</span>
                        </div>
                        <div class="colum-label-eight" style="height: auto;">
                            <asp:TextBox ID="txtShpPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                                MaxLength="15" onkeypress="return isNumberKey(event)" />
                            <asp:RequiredFieldValidator ID="BookingpassrfshpPhone" runat="server" Text="*" ErrorMessage="Please enter Shipping phone number."
                                ControlToValidate="txtShpPhone" ForeColor="#eaeaea" />
                        </div>
                        <div class="colum-label-seven">
                            Address Line 1 Or Company Name<span class="readerror">*</span>
                        </div>
                        <div class="colum-label-eight" style="height: auto;">
                            <asp:TextBox ID="txtshpAdd" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="*"
                                ErrorMessage="Please enter Shipping address." ControlToValidate="txtshpAdd" ForeColor="#eaeaea"
                                ValidationGroup="vgs1" />
                        </div>
                        <div class="colum-label-seven">
                            Address Line 2</div>
                        <div class="colum-label-eight" style="height: auto;">
                            <asp:TextBox ID="txtshpAdd2" runat="server" CssClass="input" />
                        </div>
                        <div class="colum-label-seven">
                            Town / City</div>
                        <div class="colum-label-eight">
                            <asp:TextBox ID="txtshpCity" runat="server" CssClass="input" />
                        </div>
                        <div class="colum-label-seven">
                            County / State</div>
                        <div class="colum-label-eight">
                            <asp:TextBox ID="txtshpState" runat="server" CssClass="input" />
                        </div>
                        <div class="colum-label-seven">
                            Postal/Zip Code<span class="readerror">*</span></div>
                        <div class="colum-label-eight">
                            <asp:TextBox ID="txtshpZip" runat="server" CssClass="input validcheck" onkeyup="chkerrorvali(this)"
                                MaxLength="10" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Text="*"
                                ErrorMessage="Please enter Shipping postal/zip code." ControlToValidate="txtshpZip"
                                ForeColor="#eaeaea" ValidationGroup="vgs1" />
                        </div>
                        <div class="colum-label-seven">
                            Country<span class="readerror">*</span></div>
                        <div class="colum-label-eight">
                            <asp:DropDownList ID="ddlshpCountry" runat="server" class="inputsl" OnSelectedIndexChanged="ddlshpCountry_SelectedIndexChanged"
                                AutoPostBack="True" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Text="*"
                                ErrorMessage="Please select Shipping country." ControlToValidate="ddlshpCountry"
                                InitialValue="0" ValidationGroup="vgs1" ForeColor="#eaeaea" />
                        </div>
                        <asp:Label runat="server" ID="lblshpmsg" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="chkShippingfill" />
            </Triggers>
        </asp:UpdatePanel>
        <%--Shipping Address End--%>
        <div class="round-titles">
            Date for Departure
        </div>
        <div class="booking-detail-in delvery">
            <div class="colum-label-seven">
                Date of Departure
                <br />
                (for fulfilment purposes)</div>
            <div class="colum-label-eight" style="height: auto;">
                <label runat="server" id="ldlpassDate" style="display: none">
                </label>
                <asp:TextBox ID="txtDateOfDepature" runat="server" class="input calDate" Style="margin-right: 4px;"
                    Text="DD/MM/YYYY" />
                <span class="imgCal calIcon" style="float: right;" title="Select Date of Departure.">
                </span>
                <br />
                <br />
                &nbsp;If left empty the ticket start date will be used as delivery date.
            </div>
        </div>
        <asp:Panel ID="pnlShipping" runat="server">
            <div class="round-titles">
                Shipping Option
            </div>
            <div class="booking-detail-in">
                <asp:Repeater ID="rptShippings" runat="server">
                    <ItemTemplate>
                        <div class="colum-input">
                            <input type="radio" id="radioChkShp" class="shipping" value='<%#Eval("Price")+"^"+Eval("ShippingName")+"^"+Eval("description")%>'
                                name="chkshiping" onclick="getdata()" />
                            <%#Eval("ShippingName") %>
                        </div>
                        <div class="colum-label t-right">
                            <strong>
                                <%=currency%>
                                <%#Eval("Price")%></strong></div>
                        <div style="background-color: #eaeaea; padding: 7px; width: 98%; float: left; line-height: 30px;
                            color: Black; font-size: 13px;">
                            <%#Eval("description")%></div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <div class="round-titles">
            Total
        </div>
        <div class="booking-detail-in">
            <div class="colum-input">
                &nbsp;</div>
            <div class="colum-label t-right">
                <strong>
                    <%=currency%>
                    <asp:Label ID="lblTotalCost" runat="server" /></strong></div>
        </div>
        <div class="clear">
        </div>
        <div style="float: right">
            <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Proceed To Payment"
                CssClass="button float-rt" OnClick="btnCheckout_Click" ValidationGroup="vgs1"
                OnClientClick="changeColor()" />
        </div>
    </div>
</div>
<div id="popupDiv">
    <div class="modalBackground progessposition">
    </div>
    <div class="privacy-block-outer pass progess-inner">
        <div class="red-title" style="text-align: left;">
            <span>Ticket Protection</span> <a id="btnOK" href="#" onclick="searchEvent()">X</a>
        </div>
        <div id="divpopupdata" runat="server" class="discription-block" style="text-align: left;
            overflow-x: scroll; height: 300px; font-size: 13px;">
        </div>
    </div>
</div>
<asp:HiddenField ID="hdnShippingCost" runat="server" Value="0" />
<script type="text/javascript">
    function showthis() {
        document.getElementById('popupDiv').style.display = 'block';
    }

    function searchEvent() {
        document.getElementById('popupDiv').style.display = 'none';
    } 
</script>
