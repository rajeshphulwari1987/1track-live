﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Threading;

namespace IR_Admin.usercontrol
{
    public partial class ucPassDetail : UserControl
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        readonly ManageTrainDetails _master = new ManageTrainDetails();
        private readonly FrontEndManagePass _oManageClass = new FrontEndManagePass();
        readonly ManageBooking _masterBooking = new ManageBooking();
        public static List<getRailPassData> list = new List<getRailPassData>();
        public string currency = "$";
        readonly CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        Guid siteId;
        Guid countryID;
        public string script = "<script></script>";

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;

            if (Session["siteId"] != null)
                siteId = Guid.Parse(Session["siteId"].ToString());
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
            SiteSelected();
            FetchRailPassData();
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = siteId;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != siteId.ToString()))
            {
                Session["RailPassData"] = null;
                ViewState["PreSiteID"] = siteId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SiteSelected();
                GetCurrencyCode();
                FetchRailPassData();
            }
        }

        public bool Getvalidcountryid(Guid pid, Guid cid)
        {
            try
            {
                return _db.tblProductPermittedLookups.Any(ty => ty.ProductID == pid && ty.CountryID == cid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ddlCountry_chkcounty(object sender, EventArgs e)
        {
            var ddlcntry = (DropDownList)sender;
            var hdnproductid = (HiddenField)ddlcntry.Parent.FindControl("hdnproductid");
            if (ddlcntry.SelectedIndex != 0)
            {
                var countryid = Guid.Parse(ddlcntry.SelectedValue);
                var productid = string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
                bool result = Getvalidcountryid(productid, countryid);
                if (!result)
                {
                    ddlcntry.SelectedIndex = 0;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "temp", "showmessagecountry()", true);
                }
            }
        }

        public void FetchRailPassData()
        {
            try
            {
                Session["ProductType"] = null;
                if (Session["AgentUsername"] == null)
                {
                    Session["USERUsername"] = "Guest";
                    Session["USERUserID"] = Guid.NewGuid();
                }
                if (Session["RailPassData"] != null)
                {
                    list = Session["RailPassData"] as List<getRailPassData>;
                    rptPassDetail.DataSource = list;
                    rptPassDetail.DataBind();
                }
                else
                {
                    Response.Redirect("ManualPassBooking.aspx");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptPassDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString().Trim());
                    var objRPD = list.Where(a => a.Id == id).FirstOrDefault();
                    if (objRPD.TravellerName.ToLower().Contains("saver"))
                    {
                        var newList = (from a in list where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName && a.TravellerName.ToLower().Contains("saver") select a).ToList();
                        int count = newList.Count();
                        if (count > 2)
                        {
                            list.RemoveAll(ty => ty.Id == id);
                            Session.Add("RailPassData", list);
                        }
                        else
                        {
                            foreach (getRailPassData a in newList)
                            {
                                list.RemoveAll(ty => ty.Id == a.Id);
                            }
                            Session.Add("RailPassData", list);
                        }
                    }
                    else
                    {
                        list.RemoveAll(ty => ty.Id == id);
                        Session.Add("RailPassData", list);
                    }
                    GetCurrencyCode();
                    FetchRailPassData();
                }
            }
            catch (Exception ex) { }
        }

        protected void rptPassDetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var txtDob = e.Item.FindControl("txtDob") as TextBox;
                var txtStartDate = e.Item.FindControl("txtStartDate") as TextBox;
                var ddlTitle = e.Item.FindControl("ddlTitle") as DropDownList;
                var txtFirstName = e.Item.FindControl("txtFirstName") as TextBox;
                var txtMiddleName = e.Item.FindControl("txtMiddleName") as TextBox;
                var txtLastName = e.Item.FindControl("txtLastName") as TextBox;
                var txtPassportNumber = e.Item.FindControl("txtPassportNumber") as TextBox;
                var hdnproductid = e.Item.FindControl("hdnproductid") as HiddenField;
                var ddlCountry = e.Item.FindControl("ddlCountry") as DropDownList;

                if (ddlCountry != null)
                {
                    ddlCountry.DataSource = _master.GetCountryDetail();
                    ddlCountry.DataValueField = "CountryID";
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataBind();
                    ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
                }

                var productid = hdnproductid != null && string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
                bool result = Getvalidcountryid(productid, countryID);

                if (result)
                {
                    if (ddlCountry != null) ddlCountry.SelectedValue = countryID.ToString();
                }
                else if (ddlCountry != null) ddlCountry.SelectedIndex = 0;

                if (Session["DetailRailPass"] != null)
                {
                    var list2 = Session["DetailRailPass"] as List<getpreviousShoppingData>;
                    var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
                    var ID = (!string.IsNullOrEmpty(lnkDelete.CommandArgument) ? Guid.Parse(lnkDelete.CommandArgument) : Guid.Empty);
                    if (list2 != null)
                    {
                        var data2 = list2.FirstOrDefault(ty => ty.Id == ID);
                        if (data2 != null)
                        {
                            if (txtDob != null) txtDob.Text = data2.DOB;
                            if (txtStartDate != null) txtStartDate.Text = data2.Date;
                            if (ddlTitle != null) ddlTitle.SelectedValue = data2.Title;
                            if (txtFirstName != null) txtFirstName.Text = data2.FirstName;
                            if (txtMiddleName != null) txtMiddleName.Text = data2.MiddleName;
                            if (txtLastName != null) txtLastName.Text = data2.LastName;
                            if (ddlCountry != null) ddlCountry.SelectedValue = data2.Country;
                            if (txtPassportNumber != null) txtPassportNumber.Text = data2.Passoprt;
                        }
                    }
                }
                var hdnCountryLevelIDs = e.Item.FindControl("hdnCountryLevelIDs") as HiddenField;
                var lblCountryName = e.Item.FindControl("lblCountryName") as Label;
                string counrty = _master.GetEurailCountryNames(hdnCountryLevelIDs.Value);
                if (!string.IsNullOrEmpty(counrty))
                    lblCountryName.Text = "(" + counrty + ")";

                var lblLID = e.Item.FindControl("lblLID") as Label;
                var objRPD = list.Where(a => a.Id == Guid.Parse(lblLID.Text.Trim())).FirstOrDefault();
                int count = 0;
                if (objRPD.TravellerName.ToLower().Contains("saver"))
                {
                    var newList = (from a in list
                                   where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName && a.TravellerName.ToLower().Contains("saver")
                                   select a).ToList();

                    count = newList.Count();
                    var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
                    if (count == 2)
                    {
                        lnkDelete.OnClientClick = "return window.confirm('You are now deleting the whole saverpass booking, as min 2 pax is required.')";
                    }
                    else
                        lnkDelete.OnClientClick = "return window.confirm('Are you sure? Do you want to delete this item?')";
                }
            }
        }

        public void GetCurrencyCode()
        {
            var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (currencyID != null)
            {
                string curID = currencyID.DefaultCurrencyID.ToString();
                currency = _oManageClass.GetCurrency(Guid.Parse(curID));
            }
        }

        protected void btnChkOut_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsSaver = false;
                Guid SaverCountry = Guid.Empty;
                var textInfo = cultureInfo.TextInfo;
                bool flagInvalidDt = false;
                if (rptPassDetail.Items.Count > 0)
                {
                    long POrderID = 0;
                    
                    var AgentID = (_db.tblSites.Any(x=>x.ID==siteId &&x.IsAgent==true)?AdminuserInfo.UserID:Guid.Empty);
                    var UserID = new Guid();
                   
                    if (Session["USERUserID"] != null)
                        UserID = Guid.Parse(Session["USERUserID"].ToString());
                    if (Session["OrderID"] == null)
                    {
                        POrderID = _masterBooking.CreateOrder(string.Empty, AgentID, UserID, siteId, "AdminPass");
                        Session["OrderID"] = POrderID;
                    }
                    else
                    {
                        POrderID = Convert.ToInt64(Session["OrderID"]);
                        var result = _masterBooking.DeleteOldOrderPassSales(POrderID);
                    }
                    var lstRPData = new List<getRailPassData>();
                    list = Session["RailPassData"] as List<getRailPassData>;
                    foreach (RepeaterItem it in rptPassDetail.Items)
                    {
                        var IdInsert = Guid.NewGuid();
                        var HdnOriginalPrice = it.FindControl("HdnOriginalPrice") as HiddenField;
                        var hdnPassSale = it.FindControl("hdnPassSale") as HiddenField;
                        var hdnSalePrice = it.FindControl("hdnSalePrice") as HiddenField;
                        string[] str = hdnPassSale.Value.Split(',');
                        var LID = it.FindControl("lblLID") as Label;
                        var txtStartDate = it.FindControl("txtStartDate") as TextBox;
                        var txtDob = it.FindControl("txtDob") as TextBox;
                        var lblInvalidDate = it.FindControl("lblInvalidDate") as Label;

                        var ddlTitle = it.FindControl("ddlTitle") as DropDownList;
                        var txtFirstName = it.FindControl("txtFirstName") as TextBox;
                        var txtMiddleName = it.FindControl("txtMiddleName") as TextBox;
                        var txtLastName = it.FindControl("txtLastName") as TextBox;
                        var ddlCountry = it.FindControl("ddlCountry") as DropDownList;
                        var txtPassportNumber = it.FindControl("txtPassportNumber") as TextBox;

                        /*Validate Age*/
                        var travellerID = Guid.Parse(str[2]);
                        var validateAge = _db.tblTravelerMsts.FirstOrDefault(x => x.ID == travellerID && x.IsActive);
                        if (validateAge != null)
                        {
                            if (!string.IsNullOrEmpty(txtDob.Text) && txtDob.Text != "DD/MM/YYYY")
                            {
                                var dob = Convert.ToDateTime(txtDob.Text);
                                var tm = (DateTime.Now - dob);
                                var age = (tm.Days / 365);
                                if (age >= validateAge.FromAge && age <= validateAge.ToAge || validateAge.ToAge == 0)
                                    lblInvalidDate.Visible = false;
                                else
                                {
                                    GetCurrencyCode();
                                    flagInvalidDt = lblInvalidDate.Visible = true;
                                    lblInvalidDate.Text = "Please make sure the passenger date of birth is correct for the pass you have selected.";
                                    return;
                                }
                            }
                            /**end**/
                            if (validateAge.EurailCode == 51 || validateAge.EurailCode == 53 || validateAge.EurailCode == 74 || validateAge.EurailCode == 75)
                            {
                                if (!IsSaver)
                                {
                                    SaverCountry = Guid.Parse(ddlCountry.SelectedValue);
                                    IsSaver = true;
                                }
                                countryID = SaverCountry;
                            }
                            else
                                countryID = Guid.Parse(ddlCountry.SelectedValue);
                        }
                        else
                            countryID = Guid.Parse(ddlCountry.SelectedValue);

                        var objTraveller = new tblOrderTraveller();
                        if (LID.Text.Trim() == string.Empty)
                            objTraveller.ID = IdInsert;
                        else
                            objTraveller.ID = Guid.Parse(LID.Text);

                        objTraveller.Title = ddlTitle.SelectedItem.Text;
                        objTraveller.LastName = textInfo.ToTitleCase(txtLastName.Text);
                        objTraveller.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                        objTraveller.MiddleName = textInfo.ToTitleCase(txtMiddleName.Text);
                        objTraveller.Country = countryID;
                        objTraveller.PassportNo = txtPassportNumber.Text;
                        objTraveller.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                        objTraveller.DOB = Convert.ToDateTime(txtDob.Text);

                        var Gtps = new tblPassSale();
                        Gtps.ID = IdInsert;
                        Gtps.OrderID = POrderID;
                        Gtps.ProductID = Guid.Parse(str[0]);
                        Gtps.Price = Convert.ToDecimal(str[1]);
                        Gtps.TravellerID = Guid.Parse(str[2]);
                        Gtps.ClassID = Guid.Parse(str[3]);
                        Gtps.ValidityID = Guid.Parse(str[4]);
                        Gtps.CategoryID = Guid.Parse(str[5]);
                        Gtps.Commition = Convert.ToDecimal(str[6]);
                        Gtps.MarkUp = Convert.ToDecimal(str[7]);
                        Gtps.TicketProtection = 0;
                        Gtps.CountryStartCode = Convert.ToInt32((!string.IsNullOrEmpty(str[8]) ? str[8] : null));
                        Gtps.CountryEndCode = Convert.ToInt32((!string.IsNullOrEmpty(str[9]) ? str[9] : null));
                        Gtps.Country1 = (!string.IsNullOrEmpty(str[10]) ? Guid.Parse(str[10]) : Guid.Empty);
                        Gtps.Country2 = (!string.IsNullOrEmpty(str[11]) ? Guid.Parse(str[11]) : Guid.Empty);
                        Gtps.Country3 = (!string.IsNullOrEmpty(str[12]) ? Guid.Parse(str[12]) : Guid.Empty);
                        Gtps.Country4 = (!string.IsNullOrEmpty(str[13]) ? Guid.Parse(str[13]) : Guid.Empty);
                        Gtps.Country5 = (!string.IsNullOrEmpty(str[14]) ? Guid.Parse(str[14]) : Guid.Empty);
                        //product currency//
                        Gtps.Site_MP = _masterBooking.GetCurrencyMultiplier("SITE", Guid.Parse(str[0]), POrderID);
                        Gtps.USD_MP = _masterBooking.GetCurrencyMultiplier("USD", Guid.Parse(str[0]), POrderID);
                        Gtps.SEU_MP = _masterBooking.GetCurrencyMultiplier("SEU", Guid.Parse(str[0]), POrderID);
                        Gtps.SBD_MP = _masterBooking.GetCurrencyMultiplier("SBD", Guid.Parse(str[0]), POrderID);
                        Gtps.GBP_MP = _masterBooking.GetCurrencyMultiplier("GBP", Guid.Parse(str[0]), POrderID);
                        Gtps.EUR_MP = _masterBooking.GetCurrencyMultiplier("EUR", Guid.Parse(str[0]), POrderID);
                        Gtps.INR_MP = _masterBooking.GetCurrencyMultiplier("INR", Guid.Parse(str[0]), POrderID);
                        Gtps.SEK_MP = _masterBooking.GetCurrencyMultiplier("SEK", Guid.Parse(str[0]), POrderID);
                        Gtps.NZD_MP = _masterBooking.GetCurrencyMultiplier("NZD", Guid.Parse(str[0]), POrderID);
                        Gtps.CAD_MP = _masterBooking.GetCurrencyMultiplier("CAD", Guid.Parse(str[0]), POrderID);
                        Gtps.JPY_MP = _masterBooking.GetCurrencyMultiplier("JPY", Guid.Parse(str[0]), POrderID);
                        Gtps.AUD_MP = _masterBooking.GetCurrencyMultiplier("AUD", Guid.Parse(str[0]), POrderID);
                        Gtps.CHF_MP = _masterBooking.GetCurrencyMultiplier("CHF", Guid.Parse(str[0]), POrderID);
                        Gtps.EUB_MP = _masterBooking.GetCurrencyMultiplier("EUB", Guid.Parse(str[0]), POrderID);
                        Gtps.EUT_MP = _masterBooking.GetCurrencyMultiplier("EUT", Guid.Parse(str[0]), POrderID);
                        Gtps.GBB_MP = _masterBooking.GetCurrencyMultiplier("GBB", Guid.Parse(str[0]), POrderID);
                        Gtps.THB_MP = _masterBooking.GetCurrencyMultiplier("THB", Guid.Parse(str[0]), POrderID);
                        Gtps.SGD_MP = _masterBooking.GetCurrencyMultiplier("SGD", Guid.Parse(str[0]), POrderID);

                        //site currency//
                        Gtps.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Parse(str[0]), POrderID);
                        Gtps.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Parse(str[0]), POrderID);

                        _masterBooking.AddOrderTraveller(objTraveller, _masterBooking.AddPassSale(Gtps, IdInsert, hdnSalePrice.Value, HdnOriginalPrice.Value));

                        var objUpdates = new getRailPassData();
                        objUpdates = list.Where(a => a.Id == objTraveller.ID).FirstOrDefault();
                        if (objUpdates != null)
                        {
                            objUpdates.Title = ddlTitle.SelectedItem.Text;
                            objUpdates.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                            objUpdates.MiddleName = textInfo.ToTitleCase(txtMiddleName.Text);
                            objUpdates.LastName = textInfo.ToTitleCase(txtLastName.Text);
                            objUpdates.Country = Guid.Parse(ddlCountry.SelectedValue);
                            objUpdates.PassportNo = txtPassportNumber.Text;
                            objUpdates.DOB = txtDob.Text;
                            objUpdates.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                            lstRPData.Add(objUpdates);
                        }
                    }//--end for each
                    if (!flagInvalidDt)
                    {
                        Session["RailPassData"] = lstRPData;
                        //Response.Redirect("~/ManualBooking/BookingCart.aspx");
                        Response.Redirect("~/ManualBooking/PassAgent.aspx");
                    }
                }
            }
            catch (Exception ex) { }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (Session["RailPassData"] != null)
            {
                Session.Remove("DetailRailPass");
                var detailRP = new List<getpreviousShoppingData>();
                foreach (RepeaterItem item in rptPassDetail.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var lnkDelete = item.FindControl("lnkDelete") as LinkButton;
                        var txtDob = item.FindControl("txtDob") as TextBox;
                        var txtStartDate = item.FindControl("txtStartDate") as TextBox;
                        var ddlTitle = item.FindControl("ddlTitle") as DropDownList;
                        var txtFirstName = item.FindControl("txtFirstName") as TextBox;
                        var txtMiddleName = item.FindControl("txtMiddleName") as TextBox;
                        var txtLastName = item.FindControl("txtLastName") as TextBox;
                        var ddlCountry = item.FindControl("ddlCountry") as DropDownList;
                        var txtPassportNumber = item.FindControl("txtPassportNumber") as TextBox;

                        if(!string.IsNullOrEmpty(lnkDelete.CommandArgument))
                        {
                            var datadetail = new getpreviousShoppingData();
                            datadetail.Id = Guid.Parse(lnkDelete.CommandArgument);
                            datadetail.DOB = txtDob.Text;
                            datadetail.Date = txtStartDate.Text;
                            datadetail.Title = ddlTitle.SelectedValue;
                            datadetail.FirstName = txtFirstName.Text; 
                            datadetail.MiddleName = txtMiddleName.Text;
                            datadetail.LastName = txtLastName.Text;
                            datadetail.Country = ddlCountry.SelectedValue;
                            datadetail.Passoprt = txtPassportNumber.Text;
                            detailRP.Add(datadetail);
                        }
                    }
                }
                if (detailRP != null)
                    Session["DetailRailPass"] = detailRP;
            }
            //Response.Redirect("~/railpasses");
            Response.Redirect("ManualPassBooking.aspx");
        }

        public class getpreviousShoppingData
        {
            public Guid Id { get; set; }
            public String DOB { get; set; }
            public String Date { get; set; }
            public String Title { get; set; }
            public String FirstName { get; set; }
            public String MiddleName { get; set; }
            public String LastName { get; set; }
            public String Country { get; set; }
            public String Passoprt { get; set; }
        }
    }
}