﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using IR_Admin.OneHubServiceRef;
using System.Web.UI.HtmlControls;

namespace IR_Admin.usercontrol
{
    public partial class ucBookingCart : UserControl
    {
        #region Global Variables
        readonly Masters _masterPage = new Masters();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        private double _total = 0;
        readonly ManageUser _ManageUser = new ManageUser();
        private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
        ManageTrainDetails _master = new ManageTrainDetails();
        ManageBooking _masterBooking = new ManageBooking();
        public static string currency = "$";
        string curID;
        public static Guid currencyID = new Guid();
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        Guid siteId;
        public string script = "<script></script>";
        #endregion

        public string siteURL;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;

            if (Session["siteId"] != null)
            {
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                siteId = Guid.Parse(Session["siteId"].ToString());
            }
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
            BindSiteinsession();
            GetCurrencyCode();
            SiteSelected();
            PageLoadEvent();
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = siteId;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != siteId.ToString()))
            {
                Session["RailPassData"] = null;
                ViewState["PreSiteID"] = siteId;
            }
        }

        public void BindSiteinsession()
        {
            //siteId = Master.SiteID;
            Session["siteId"] = siteId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ProductType"] != null)
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "restirctCopy", "restrictCopy();", true);
            txtZip.Attributes.Add("onkeypress", "return keycheck()");
            ShowMessage(0, string.Empty);
            Session["USERUserID"] = AdminuserInfo.UserID;

            if (!IsPostBack)
            {
                if (Session["AgentUserID"] != null)
                {
                    var IDuser = Guid.Parse(Session["AgentUserID"].ToString());
                    var Agentlist = _ManageUser.AgentDetailsById(IDuser);
                    var AgentNameAndEmail = _ManageUser.AgentNameEmailById(IDuser);
                    if (Agentlist != null)
                    {
                        txtFirst.Text = AgentNameAndEmail.Forename;
                        txtLast.Text = AgentNameAndEmail.Surname;
                        txtEmail.Text = AgentNameAndEmail.EmailAddress;
                        txtConfirmEmail.Text = AgentNameAndEmail.EmailAddress;
                        ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                        txtCity.Text = Agentlist.Town;
                        txtAdd.Text = Agentlist.Address1;
                        txtAdd2.Text = Agentlist.Address2;
                        txtZip.Text = Agentlist.Postcode;

                        txtshpfname.Text = Agentlist.FirstName;
                        txtshpLast.Text = Agentlist.LastName;
                        txtshpEmail.Text = Agentlist.Email;
                        txtshpConfirmEmail.Text = Agentlist.Email;
                        ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                        txtCity.Text = Agentlist.Town;
                        txtAdd.Text = Agentlist.Address1;
                        txtAdd2.Text = Agentlist.Address2;
                        txtZip.Text = Agentlist.Postcode;
                    }
                }
                else if (Session["USERUserID"] != null)
                {
                    var IDuser = Guid.Parse(Session["USERUserID"].ToString());
                    var userlist = _ManageUser.GetUserbyID(IDuser);
                    if (userlist != null)
                    {
                        txtFirst.Text = userlist.FirstName;
                        txtLast.Text = userlist.LastName;
                        txtEmail.Text = userlist.Email;
                        txtConfirmEmail.Text = userlist.Email;
                        ddlCountry.SelectedValue = Convert.ToString(userlist.Country);
                        txtCity.Text = userlist.City;
                        txtAdd.Text = userlist.Address;
                        txtZip.Text = userlist.PostCode;

                        txtshpfname.Text = userlist.FirstName;
                        txtshpLast.Text = userlist.LastName;
                        txtshpEmail.Text = userlist.Email;
                        txtshpConfirmEmail.Text = userlist.Email;
                        ddlshpCountry.SelectedValue = Convert.ToString(userlist.Country);
                        txtshpCity.Text = userlist.City;
                        txtshpAdd.Text = userlist.Address;
                        txtshpZip.Text = userlist.PostCode;
                    }
                }
                Session["BOOKING-REPLY"] = null;
                PageLoadEvent();
            }
            GetCurrencyCode();
        }

        void PageLoadEvent()
        {
            ddlCountry.DataSource = _master.GetCountryDetail();
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            ddlshpCountry.DataSource = _master.GetCountryDetail();
            ddlshpCountry.DataValueField = "CountryID";
            ddlshpCountry.DataTextField = "CountryName";
            ddlshpCountry.DataBind();
            ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            if (Session["siteId"] != null && Session["OrderID"] != null)
                new ManageBooking().UpdateSiteToOrder(Convert.ToInt64(Session["OrderID"]), Guid.Parse(Session["siteId"].ToString()));

            AddItemInShoppingCart();
            GetCurrencyCode();
            if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
            {
                var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
                string countryID = cookie.Values["_cuntryId"];
                ddlCountry.SelectedValue = countryID;
                ddlshpCountry.SelectedValue = countryID;
            }
            FillShippingData();
            PrepouplateSeniorAndAdult();
        }

        public void FillShippingData()
        {
            if (ddlCountry.SelectedValue != "0")
            {
                var countryid = Guid.Parse(ddlCountry.SelectedValue);
                var lstShip = new ManageBooking().getAllPassShippingDetailAdminSection(siteId, countryid);
                if (lstShip.Any())
                    rptShippings.DataSource = lstShip;
                else
                {
                    var lstDefaultShip = new ManageBooking().getDefaultShippingDetailAdminSection(siteId);
                    if (lstDefaultShip.Any())
                        rptShippings.DataSource = lstDefaultShip;
                    else
                        rptShippings.DataSource = null;
                }
                rptShippings.DataBind();
            }
        }

        void PrepouplateSeniorAndAdult()
        {
            if (Session["RailPassData"] != null)
            {
                var list = Session["RailPassData"] as List<getRailPassData>;
                ldlpassDate.InnerText = list.Min(t => t.PassStartDate).ToString().Substring(0, 10);
                if (Session["AgentUserID"] == null)
                {
                    var listr = list.OrderBy(x => x.TravellerName);
                    foreach (var rec in listr)
                    {
                        if (rec.TravellerName.Contains("Adult") || rec.TravellerName.Contains("Senior "))
                        {
                            ddlMr.SelectedValue = rec.Title;
                            txtFirst.Text = rec.FirstName;
                            txtLast.Text = rec.LastName;
                            break;
                        }
                    }
                }
            }
        }

        public void AddItemInShoppingCart()
        {
            try
            {
                var lstPassDetail = _masterBooking.GetAllPassSale(Convert.ToInt64(Session["OrderID"]), "", getsitetickprotection());
                lstPassDetail = lstPassDetail.ToList();

                if (lstPassDetail.Count() > 0)
                {
                    rptTrain.DataSource = lstPassDetail.Count > 0 ? lstPassDetail : null;
                    rptTrain.DataBind();
                }
                else
                {
                    rptTrainTcv.DataSource = null;
                    rptTrainTcv.DataBind();
                    rptTrain.DataSource = null;
                    rptTrain.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected BillingAddress GetBillingInfo()
        {
            return new BillingAddress
            {
                Address = txtAdd.Text,
                City = txtCity.Text,
                Country = ddlCountry.SelectedItem.Text,
                Email = txtEmail.Text.Trim(),
                FirstName = txtFirst.Text.Trim(),
                Lastname = txtLast.Text.Trim(),
                ZipCode = txtZip.Text.Trim(),
                State = txtState.Text.Trim()
            };
        }

        protected void DeleteTicketInfo(string id)
        {
            try
            {
                var cartList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;

                if (cartList.Count > 0 && bookingList.Count > 0)
                {
                    cartList.RemoveAll(x => x.Id.ToString() == id);
                    bookingList.RemoveAll(x => x.Id.ToString() == id);
                    Session["SHOPPINGCART"] = cartList;
                    Session["BOOKING-REQUEST"] = bookingList;
                    ShowMessage(1, "You have successfully deleted ticket from list view");
                }
                else
                {
                    Session["SHOPPINGCART"] = null;
                    Session["BOOKING-REQUEST"] = null;
                }
                AddItemInShoppingCart();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var chk = e.Item.FindControl("chkTicketProtection") as CheckBox;
                var lblPrice = e.Item.FindControl("lblPrice") as Label;
                var lbltpPrice = e.Item.FindControl("lbltpPrice") as Label;
                var currsyb = e.Item.FindControl("currsyb") as Label;
                var lblTckProc = e.Item.FindControl("lblTckProc") as Label;
                var divTckProt = e.Item.FindControl("divTckProt") as HtmlGenericControl;
                var istckProt = _masterPage.IsTicketProtection(siteId);
                if (divTckProt != null)
                    divTckProt.Visible = istckProt;

                if (lblTckProc != null)
                    lblTckProc.Visible = istckProt;

                if (chk.Checked)
                {
                    lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                    _total = _total + Convert.ToDouble(lblPrice.Text);
                    lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                    currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                }
                else
                {
                    _total = _total + Convert.ToDouble(lblPrice.Text);
                    lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                    currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                var lblTotal = e.Item.FindControl("lblTotal") as Label;
                var txtBookingFee = e.Item.FindControl("txtBookingFee") as TextBox;
                double totalAmount;
                var lst = new ManageBooking().GetBooingFees(Convert.ToDecimal(_total), Guid.Parse(Session["siteId"].ToString()));
                if (lst.Count() > 0)
                {
                    if (Session["AgentUserID"] != null)
                    {
                        txtBookingFee.Text = "0.00";
                        hdnBookingFee.Value = "0.00";
                    }
                    else
                    {
                        decimal amountAfterConv = Convert.ToDecimal(lst.FirstOrDefault().BookingFee) * Convert.ToDecimal(lst.FirstOrDefault().convRate);
                        txtBookingFee.Text = amountAfterConv.ToString("F2");
                        hdnBookingFee.Value = amountAfterConv.ToString("F2");
                    }
                }
                else
                {
                    txtBookingFee.Text = "0.00";
                    hdnBookingFee.Value = "0.00";
                }
                totalAmount = _total + Convert.ToDouble(hdnBookingFee.Value.Trim());
                lblTotal.Text = totalAmount.ToString("F2");
                lblTotalCost.Text = totalAmount.ToString("F2");
            }
        }

        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
            var chk = (CheckBox)sender;
            var lbltpPrice = chk.Parent.FindControl("lbltpPrice") as Label;
            var lblPrice = chk.Parent.FindControl("lblPrice") as Label;
            var lblPassSaleID = chk.Parent.FindControl("lblPassSaleID") as Label;
            var lblHidPriceValue = chk.Parent.FindControl("lblHidPriceValue") as Label;
            var currsyb = chk.Parent.FindControl("currsyb") as Label;

            var passsaleid = new Guid();
            if (!string.IsNullOrEmpty(lblPassSaleID.Text))
                passsaleid = Guid.Parse(lblPassSaleID.Text);

            if (chk.Checked)
            {
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                _masterBooking.SetTicketProtectionAmt(passsaleid, Convert.ToDecimal(lbltpPrice.Text));
                lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            }
            else
            {
                _masterBooking.SetTicketProtectionAmt(passsaleid, 0);
                lblPrice.Text = lblHidPriceValue.Text;
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
            }
            ScriptManager.RegisterStartupScript(Page, GetType(), "temp", "getdata()", true);
        }

        protected void rptTrainTcv_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    string id = e.CommandArgument.ToString();
                    if (e.CommandName == "Remove")
                        DeleteTicketInfo(id);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void rptTrainTcv_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var lblp = e.Item.FindControl("lblPrice") as Label;
                string prc = lblp.Text.Replace("$", "");
                _total = _total + Convert.ToDouble(prc);
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                var lblTotal = e.Item.FindControl("lblTotal") as Label;
                lblTotal.Text = "Total:" + currency + _total.ToString("F2");
            }
        }

        public decimal getsitetickprotection()
        {
            try
            {
                var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
                if (list != null)
                {
                    divpopupdata.InnerHtml = list.Description;
                    return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
                }
                else return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetCurrencyCode()
        {
            var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (currencyID != null)
            {
                curID = currencyID.DefaultCurrencyID.ToString();
                currency = oManageClass.GetCurrency(Guid.Parse(curID));
            }
        }

        protected void chkShippingfill_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShippingfill.Checked)
            {
                pnlbindshippping.Visible = true;
                ddlshpMr.SelectedValue = ddlMr.SelectedValue;
                txtshpfname.Text = txtFirst.Text;
                txtshpLast.Text = txtLast.Text;
                txtshpEmail.Text = txtEmail.Text;
                txtshpConfirmEmail.Text = txtConfirmEmail.Text;
                txtShpPhone.Text = txtBillPhone.Text;
                txtshpAdd.Text = txtAdd.Text;
                txtshpAdd2.Text = txtAdd2.Text;
                txtshpCity.Text = txtCity.Text;
                txtshpState.Text = txtState.Text;
                txtshpZip.Text = txtZip.Text;
                ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
            }
            else
            {
                pnlbindshippping.Visible = false;
            }
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                 
                #region Save Billing and Shipping Address
                Session["ShipMethod"] = hdnShipMethod.Value.Trim();

                bool BookingResult = false;
                if (Session["ProductType"] != null)
                {
                    #region Booking request check from Services
                    if (Session["BOOKING-REQUEST"] != null)
                    {
                        List<BookingRequest> bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                        BillingAddress address = GetBillingInfo();
                        if (bookingList != null)
                            foreach (var item in bookingList)
                            {
                                if (item.PurchasingForServiceOwnerRequest != null)
                                {
                                    var client = new OneHubRailOneHubClient();
                                    PurchasingForServiceOwnerRequest req = item.PurchasingForServiceOwnerRequest;
                                    req.BillingAddress = address;
                                    PurchasingForServiceOwnerResponse response = item.IsInternational ? client.PurchasingForServicesInternational(req) : client.PurchasingForServiceOwner(req);
                                    if (response.TicketBookingDetailList != null && response.TicketIssueResponse != null && response.ErrorMessage == null)
                                    {
                                        GetBookingResponse(response, null, address, item.DepartureStationName);
                                        AddP2PBookingInLocalDB();
                                        BookingResult = true;
                                    }
                                    else
                                    {
                                        ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                        return;
                                    }
                                }
                                if (item.PurchasingForServiceRequest != null)
                                {
                                    var client = new OneHubRailOneHubClient();
                                    PurchasingForServiceRequest req = item.PurchasingForServiceRequest;
                                    req.BillingAddress = address;

                                    var response = client.PurchasingForService(item.PurchasingForServiceRequest);
                                    if (response != null && !String.IsNullOrEmpty(response.ReservationCode) && response.ErrorMessage == null)
                                    {
                                        GetBookingResponse(null, response, null, item.DepartureStationName);
                                        AddP2PBookingInLocalDB();
                                        BookingResult = true;
                                    }
                                    else if (response != null)
                                    {
                                        ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                        return;
                                    }
                                }
                            }
                    }
                    #endregion
                    if (BookingResult)
                    {
                        Response.Redirect("PaymentProcess.aspx", false);
                    }
                    else
                        ShowMessage(2, "P2P booking is failed.");

                }
                else
                    AddPassBookingInLocalDB();
                #endregion
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void AddP2PBookingInLocalDB()
        {
            foreach (RepeaterItem it in rptTrain.Items)
            {
                var chkTicketProtection = it.FindControl("chkTicketProtection") as CheckBox;
                var lbltpPrice = it.FindControl("lbltpPrice") as Label;
                var lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
                if (chkTicketProtection.Checked)
                {
                    new ManageBooking().UpdateTicketProtection(Guid.Parse(lblPassSaleID.Text.Trim()), Convert.ToDecimal(lbltpPrice.Text.Trim()));
                }
            }

            bool isReg = false;
            if (Session["IsRegional"] != null)
                isReg = Convert.ToBoolean(Session["IsRegional"]);

            Int64 orderID = Convert.ToInt64(Session["OrderID"]);
            foreach (RepeaterItem it in rptTrain.Items)
            {
                var lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
                var objBooking = new ManageBooking();
                if (Session["BOOKING-REPLY"] != null)
                {
                    var oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                    foreach (var item in oldList)
                    {
                        bool result = objBooking.UpdateReservationCodebyP2PID(Guid.Parse(lblPassSaleID.Text.Trim()), orderID, item.ReservationCode, item.PinCode, "Manual Booking", false, item.DepStationName, isReg);
                    }
                }
            }

            var objBillingAddress = new tblOrderBillingAddress();
            objBillingAddress.ID = Guid.NewGuid();
            objBillingAddress.OrderID = orderID;
            objBillingAddress.Title = ddlMr.SelectedItem.Text;
            objBillingAddress.FirstName = txtFirst.Text;
            objBillingAddress.LastName = txtLast.Text;
            objBillingAddress.Phone = txtBillPhone.Text;
            objBillingAddress.Address1 = txtAdd.Text;
            objBillingAddress.Address2 = txtAdd2.Text;
            objBillingAddress.EmailAddress = txtEmail.Text;
            objBillingAddress.City = txtCity.Text;
            objBillingAddress.State = txtState.Text;
            objBillingAddress.Country = ddlCountry.SelectedItem.Text;
            objBillingAddress.Postcode = txtZip.Text;

            if (!chkShippingfill.Checked)
            {
                ddlshpMr.SelectedValue = ddlMr.SelectedValue;
                txtshpfname.Text = txtFirst.Text;
                txtshpLast.Text = txtLast.Text;
                txtshpEmail.Text = txtEmail.Text;
                txtshpConfirmEmail.Text = txtConfirmEmail.Text;
                txtShpPhone.Text = txtBillPhone.Text;
                txtshpAdd.Text = txtAdd.Text;
                txtshpAdd2.Text = txtAdd2.Text;
                txtshpCity.Text = txtCity.Text;
                txtshpState.Text = txtState.Text;
                txtshpZip.Text = txtZip.Text;
                ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
            }

            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;

            if (txtDateOfDepature.Text.Trim() != "DD/MM/YYYY")
                new ManageBooking().UpdateDepatureDate(Convert.ToInt64(Session["P2POrderID"]), Convert.ToDateTime(txtDateOfDepature.Text));

            new ManageBooking().AddOrderBillingAddress(objBillingAddress);

            if (Request.QueryString["req"] != null)
            {
                if (Request.QueryString["req"].Trim() == "IT")
                    hdnShippingCost.Value = "0";
            }

            _masterBooking.UpdateOrderData(Convert.ToDecimal(hdnShippingCost.Value),"", "", "", Convert.ToInt64(Session["P2POrderID"]));
            new ManageBooking().UpdateOrderBookingFee(Convert.ToDecimal(hdnBookingFee.Value.Trim()), Convert.ToInt64(Session["P2POrderID"]));
        }

        void AddPassBookingInLocalDB()
        {
            var objBillingAddress = new tblOrderBillingAddress();
            objBillingAddress.ID = Guid.NewGuid();
            objBillingAddress.OrderID = Convert.ToInt64(Session["OrderID"]);
            objBillingAddress.Title = ddlMr.SelectedItem.Text;
            objBillingAddress.FirstName = txtFirst.Text;
            objBillingAddress.LastName = txtLast.Text;
            objBillingAddress.Phone = txtBillPhone.Text;
            objBillingAddress.Address1 = txtAdd.Text;
            objBillingAddress.Address2 = txtAdd2.Text;
            objBillingAddress.EmailAddress = txtEmail.Text;
            objBillingAddress.City = txtCity.Text;
            objBillingAddress.State = txtState.Text;
            objBillingAddress.Country = ddlCountry.SelectedItem.Text;
            objBillingAddress.Postcode = txtZip.Text;

            if (!chkShippingfill.Checked)
            {
                ddlshpMr.SelectedValue = ddlMr.SelectedValue;
                txtshpfname.Text = txtFirst.Text;
                txtshpLast.Text = txtLast.Text;
                txtshpEmail.Text = txtEmail.Text;
                txtshpConfirmEmail.Text = txtConfirmEmail.Text;
                txtShpPhone.Text = txtBillPhone.Text;
                txtshpAdd.Text = txtAdd.Text;
                txtshpAdd2.Text = txtAdd2.Text;
                txtshpCity.Text = txtCity.Text;
                txtshpState.Text = txtState.Text;
                txtshpZip.Text = txtZip.Text;
                ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
            }

            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;

            if (txtDateOfDepature.Text.Trim() != "DD/MM/YYYY")
                new ManageBooking().UpdateDepatureDate(Convert.ToInt64(Session["OrderID"]), Convert.ToDateTime(txtDateOfDepature.Text));

            new ManageBooking().AddOrderBillingAddress(objBillingAddress);
            _masterBooking.UpdateOrderData(Convert.ToDecimal(hdnShippingCost.Value), hdnShipMethod.Value, hdndescription.Value, "", Convert.ToInt64(Session["OrderID"]));
            new ManageBooking().UpdateOrderBookingFee(Convert.ToDecimal(hdnBookingFee.Value.Trim()), Convert.ToInt64(Session["OrderID"]));
            Response.Redirect("PaymentProcess.aspx", false);
        }

        protected void GetBookingResponse(PurchasingForServiceOwnerResponse responseown, PurchasingForServiceResponse response, BillingAddress address, string DepStName)
        {
            try
            {
                List<BookingResponse> listbookingReply = new List<BookingResponse>();
                if (responseown != null)
                {
                    var ticketBookingDetail = responseown.TicketBookingDetailList.FirstOrDefault(x => x.ReservationCode == responseown.TicketIssueResponse.ReservationCode);
                    if (
                        ticketBookingDetail != null)
                        listbookingReply = responseown.TicketIssueResponse != null
                                               ? new List<BookingResponse>
                                                   {
                                                       new BookingResponse
                                                           {
                                                               Issued = responseown.TicketIssueResponse.Issued,
                                                               ReservationCode = responseown.TicketIssueResponse.ReservationCode,
                                                               ChangeReservationCode = ticketBookingDetail.ChangeReservationCode,
                                                               UnitOfWork = responseown.TicketIssueResponse.UnitOfWork,
                                                               BillingAddress = address,
                                                           }
                                                   }
                                               : null;
                }

                if (response != null)
                    listbookingReply = new List<BookingResponse>
                    {
                        new BookingResponse
                            {
                                Issued = true,
                                ReservationCode = response.ReservationCode,
                                PinCode = response.Pincode,                              
                                DepStationName = DepStName
                            }
                    };

                if (Session["BOOKING-REPLY"] != null)
                {
                    List<BookingResponse> oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                    if (oldList != null) if (listbookingReply != null) listbookingReply.AddRange(oldList);
                }
                Session["BOOKING-REPLY"] = listbookingReply;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void ddlshpCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillShippingData();
        }
    }
}