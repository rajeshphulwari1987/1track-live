﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPaymentProcess.ascx.cs"
    Inherits="IR_Admin.usercontrol.ucPaymentProcess" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../Styles/PassBooking.css" rel="stylesheet" type="text/css" />
<%=script%>
<div class="content">
    <div class="left-content">
        <div class="bread-crum-in">
            Select Product >> Pass Booking >> Pass Detail >> Booking Details >> <span>Payment</span>
        </div>
        <%--<p>
            Venenatis per interdum riduculus ligula placerat elit donec cammando dui. Prasent
            tortor aliquam urna Curabitur erat penatibus vel pede suscipit penatibus sadales
            per lociania risus elementum cras orci sapien
        </p>--%>
        <div class="round-titles">
            Order Information
        </div>
        <div class="booking-detail-in">
            <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" width="50%">
                        Pay To :
                    </td>
                    <td width="50%">
                        One Track Booking System
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Email Address :
                    </td>
                    <td>
                        <asp:Label ID="lblEmailAddress" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                    <td>
                        <b>Delivery Address</b>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblDeliveryAddress" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Order Summary:</b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Order Date:
                    </td>
                    <td>
                        <asp:Label ID="lblOrderDate" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Order No.:
                    </td>
                    <td>
                        <asp:Label ID="lblOrderNumber" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <asp:Repeater ID="rptOrderInfo" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td align="left">
                                Journey
                                <%# Container.ItemIndex + 1 %>:
                            </td>
                            <td>
                                <%#Eval("ProductDesc")%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Name:
                            </td>
                            <td>
                                <%#Eval("Name")%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Price
                            </td>
                            <td>
                                <%=currency %>
                                <%#Eval("Price").ToString()%>
                            </td>
                        </tr>
                        <%#(Eval("TktPrtCharge")) != "" ? "<tr><td>Ticket Protection: </td><td>" + currency +" "+ Eval("TktPrtCharge")+"</td></tr>" : ""%>
                    </ItemTemplate>
                </asp:Repeater>
                <tr>
                    <td align="left">
                        Booking Fee:
                    </td>
                    <td>
                        <%=currency %>
                        <asp:Label ID="lblBookingFee" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Shipping:
                    </td>
                    <td>
                        <%=currency %>
                        <asp:Label ID="lblShippingAmount" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Total Price:</b>
                    </td>
                    <td>
                        <b>
                            <%=currency %>
                            <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label></b>
                    </td>
                </tr>
            </table>
        </div>
        <%--<div class="round-titles">
                Payment Gateway Login:
            </div>--%>
        <div class="booking-detail-in" style="display: none;">
            <div class="colum-label">
                Email address*
            </div>
            <div class="colum-input">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="input" />
            </div>
            <div style="float: left; padding-left: 200px; width: 100%; height: 15px; font-size: 12px !important;">
                <asp:RequiredFieldValidator ID="rfEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail"
                    ForeColor="red" ValidationGroup="vgs" />
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid Email."
                    ValidationGroup="vgs" ControlToValidate="txtEmail" ForeColor="red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    Display="Dynamic" />
            </div>
            <div class="colum-label">
                Password*
            </div>
            <div class="colum-input">
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="input"
                    MaxLength="10" />
                <asp:RequiredFieldValidator ID="rfPhn" runat="server" ErrorMessage="*" ControlToValidate="txtPassword"
                    ValidationGroup="vgs" ForeColor="red" />
            </div>
            <div style="clear: both;">
            </div>
            <asp:Label runat="server" ID="lblmsg" />
        </div>
        <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" CssClass="button"
            Style="width: 15%; float: right;" Text="Pay now" />
    </div>
</div>
<asp:HiddenField ID="hdnUserName" runat="server" />
<script type="text/javascript">
    function newPopup(url) {
        popupWindow = window.open(
        url, 'popUpWindow', 'height=400,width=800,left=25%,top=25%,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
    }
</script>
