﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Xml;
using Business;
using System.Collections.Generic;
using System.Net;
using System.Configuration;

namespace IR_Admin.usercontrol
{
    public partial class ucPaymentProcess : UserControl
    {
        readonly Masters _masterPage = new Masters();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string currency = "$";
        public string currencyCode = "USD";
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
        private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
        readonly private ManageOrder _master = new ManageOrder();
        public static Guid currencyID = new Guid();
        Guid siteId;
        private string htmfile = string.Empty;
        public string script = "<script></script>";
        public string products = "";
        private string HtmlFileSitelog = string.Empty;
        public string SiteHeaderColor = string.Empty;
        public string Name = "";
        public string logo = "";
        public string billingEmailAddress;
        public string shippingEmailAddress;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["siteId"] != null)
                siteId = Guid.Parse(Session["siteId"].ToString());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GetCurrencyCode();
            if (!IsPostBack)
            {
                ShowPaymentDetails();
            }
        }

        public void ShowPaymentDetails()
        {
            try
            {
                if (Session["OrderID"] != null)
                {
                    var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Session["OrderID"])).ToList();
                    var lstNew = from a in lst
                                 select new { Name = a.FirstName + " " +a.MiddleName + " " + a.LastName, Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)), ProductDesc = (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType)), TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0) };
                    if (lst.Count > 0)
                    {
                        rptOrderInfo.DataSource = lstNew;
                        lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                        lblDeliveryAddress.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName + ", " + lst.FirstOrDefault().Address1 + ", " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + ", " : string.Empty) + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry + ", " + lst.FirstOrDefault().Postcode;
                        lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();
                        lblShippingAmount.Text = lst.FirstOrDefault().ShippingAmount.ToString();
                        lblGrandTotal.Text = ((lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount + lst.FirstOrDefault().BookingFee).ToString();
                        lblBookingFee.Text = Convert.ToDecimal(lst.FirstOrDefault().BookingFee).ToString("F2");
                        hdnUserName.Value = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                    }
                    else
                        rptOrderInfo.DataSource = null;
                    rptOrderInfo.DataBind();
                }
                else
                    Response.Redirect("~/Home", false);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            SendMail();
            Session["RailPassData"] = null;
            Response.Redirect("SetOrderStatus.aspx?oid=" + Session["OrderID"]);
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                GetCurrencyCode();
                bool isDiscountSite = _master.GetOrderDiscountVisibleByOrderId(Convert.ToInt64(Request["id"]));
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    var orderID = Convert.ToInt32(Session["OrderID"]);
                    string Subject = "Order Confirmation #" + orderID;

                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml;

                    string UserName, EmailAddress, DeliveryAddress, BillingAddress, OrderDate, OrderNumber, Total, ShippingAmount, GrandTotal, BookingFee, NetTotal, Discount, AdminFee;
                    UserName = Discount = NetTotal = EmailAddress = DeliveryAddress = BillingAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = BookingFee = AdminFee = "";
                    string PassProtection = "0.00";
                    string PassProtectionHtml = "";
                    string DiscountHtml = "";
                    bool isAgentSite = false;

                    var lst = new ManageBooking().GetAllCartData(orderID).OrderBy(a => a.OrderIdentity).ToList();
                    var lst1 = from a in lst
                               select
                                   new
                                   {
                                       Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                       ProductDesc =
                               (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                                " " + a.LastName),
                                       TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                   };
                    if (lst.Count > 0)
                    {
                        EmailAddress = lst.FirstOrDefault().EmailAddress;
                        UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                        DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                        lst.FirstOrDefault().DCountry + "<br>" +
                        lst.FirstOrDefault().Postcode + "<br>" +
                        GetBillingAddressPhoneNo(orderID);

                        OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                        Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                        ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                        BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                        Discount = _master.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));
                        AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(orderID).ToString();
                        NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                            (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                            (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                            Convert.ToDecimal(AdminFee)).ToString();
                        GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                        var billAddress = new ManageBooking().GetBillingShippingAddress(Convert.ToInt64(orderID));
                        if (billAddress != null)
                        {
                            BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                        }
                    }

                    // to address
                    string ToEmail = EmailAddress;
                    GetReceiptLoga();
                    body = body.Replace("##headerstyle##", SiteHeaderColor);
                    body = body.Replace("##sitelogo##", logo);
                    body = body.Replace("##OrderNumber##", OrderNumber);
                    body = body.Replace("##UserName##", UserName.Trim());
                    body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                    body = body.Replace("##OrderDate##", OrderDate.Trim());
                    body = body.Replace("##Items##", currency + " " + Total.Trim());
                    body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");

                    var tblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderID);
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                    isAgentSite = objsite.IsAgent.Value;

                    string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + AdminFee.Trim() + "</td></tr>";
                    if (AdminFee == "0.00")
                        AdminFeeRow = string.Empty;
                    if (isDiscountSite)
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        if (Discount == "0.00")
                            body = body.Replace("##Discount##", "");
                        else
                        {
                            DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + Discount.Trim() + "</td></tr>";
                            body = body.Replace("##Discount##", DiscountHtml);
                        }
                    }
                    else
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        body = body.Replace("##Discount##", "");
                    }


                    body = body.Replace("##BookingRef##", "");
                    body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##DeliveryFee##", currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##BookingFee##", currency + " " + BookingFee);
                    body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());
                    if (tblord != null)
                    {
                        Session["ShipMethod"] = tblord.ShippingMethod;
                        Session["ShipDesc"] = tblord.ShippingDescription;
                    }

                    string britrailpromoTerms = isBritRailPromoPass(orderID) ? "BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a>" : "";
                    if (Session["ShipMethod"] != null)
                    {
                        string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                        body = body.Replace("##ShippingMethod##", shippingdesc + britrailpromoTerms);
                    }
                    else
                        body = body.Replace("##ShippingMethod##", britrailpromoTerms);

                    body = body.Replace("#Blanck#", "&nbsp;");
                    var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                    var lstC = new ManageBooking().GetAllCartData(orderID).OrderBy(a => a.OrderIdentity).ToList();
                    var lstNew = (from a in lstC
                                  select new
                                  {
                                      a,
                                      Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                      ProductDesc = a.ProductType == "P2P"
                                      ? (new ManageBooking().getP2PDetailsForEmail(a.PassSaleID, null, a.ProductType, false, isAgentSite, ""))
                                      : (new ManageBooking().getPassDetailsForEmail(a.PassSaleID, null, a.ProductType, "")),
                                      TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                      CommissionFee = a.CommissionFee,
                                      Terms = a.terms
                                  }).ToList();

                    string strProductDesc = "";
                    int i = 1;
                    if (lstNew.Count() > 0)
                    {
                        lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                        body = body.Replace("##HeaderText##", "Delivery address");
                        body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                        body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());

                        foreach (var x in lstNew)
                        {
                            if (x.a.ProductType != "P2P")
                            {
                                strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                                if (isAgentSite)
                                    strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + x.a.LastName).Replace("##NetAmount##", "Net Amount :" + currency + " " + (x.Price + x.TktPrtCharge).ToString());
                                else
                                    strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + x.a.LastName).Replace("##NetAmount##", "");

                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + (x.a.PassStartDate.HasValue ? x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") : string.Empty) + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";
                                strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");

                                string ElectronicNote = new ManageBooking().GetElectronicNote(x.a.PassSaleID.Value);
                                if (ElectronicNote.Length > 0)
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;'>" + ElectronicNote + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";

                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" +
                                    (x.TktPrtCharge > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + currency + " " + Convert.ToString(x.TktPrtCharge) + "</strong></td></tr>";
                                strProductDesc += "</table></td></tr></table>";
                                PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            }
                            i++;
                        }
                    }
                    PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + PassProtection.Trim() + "</td></tr>";
                    body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                    body = body.Replace("##OrderDetails##", strProductDesc);
                    body = body.Replace("##BillingAddress##", BillingAddress);

                    body = body.Replace("##PrintAtHome##", "");
                    body = body.Replace("../images/", SiteName + "images/");

                    var billing = _master.GetBillingInfo(orderID);
                    if (billing.Any())
                    {
                        var firstOrDefault = billing.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            billingEmailAddress = firstOrDefault.EmailAddress;
                            shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                        }
                    }
                    /*Get smtp details*/
                    var result = _masterPage.GetEmailSettingDetail(siteId);
                    if (result != null)
                    {
                        var fromAddres = new MailAddress(result.Email, result.Email);
                        smtpClient.Host = result.SmtpHost;
                        smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                        smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                        message.From = fromAddres;
                        message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        smtpClient.Send(message);
                        retVal = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retVal;
        }

        public string GetBillingAddressPhoneNo(long OrderID)
        {
            var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            if (result != null)
            {
                return !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
            }
            return "";
        }

        public void GetReceiptLoga()
        {
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    logo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    logo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    logo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        bool isBritRailPromoPass(long orderid)
        {
            try
            {
                foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
                {
                    bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                    if (isPromo)
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public string GetCCEmailID()
        {
            try
            {
                string ccEmail = string.Empty;
                if (Session["AgentUserID"] != null)
                {
                    var agentId = Guid.Parse(Session["AgentUserID"].ToString());
                    var result = _masterPage.GetAgentOfficeEmailID(agentId).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.chkEmail && result.Email != string.Empty)
                            ccEmail = result.Email + ",";
                        if (result.chkSecondaryEmail && result.SecondaryEmail != string.Empty)
                            ccEmail = ccEmail + result.SecondaryEmail;
                    }
                }
                return ccEmail.TrimEnd(',');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void nextStepGoto()
        {
            if (Session["OrderID"] != null)
            {
                var objPT = new PaymentGateWayTransffer();
                objPT.Amount = Convert.ToDouble(lblGrandTotal.Text.Trim());
                objPT.customerEmail = lblEmailAddress.Text.Trim();
                objPT.orderReference = Session["OrderID"].ToString();
                Session["PayObj"] = objPT;
                string siteId = "";
                if (Session["siteId"] != null)
                    siteId = Session["siteId"].ToString();

                //if (Session["AgentUsername"] != null)
                //{
                //    Response.Redirect("~/Agent/Agentpayment.aspx" + "?amt=" + lblGrandTotal.Text.Trim() + "&oid=" + Session["OrderID"].ToString());
                //}
                //else
                    //Response.Redirect("PaymentConfirmation.aspx");
            }
            else
            {
                Response.Redirect("~/Home");
            }
        }

        public void GetCurrencyCode()
        {
            //if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
            //{
            //    var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            //    siteId = Guid.Parse(cookie.Values["_siteId"]);
            //    currencyID = Guid.Parse(cookie.Values["_curId"]);
            //    currency = oManageClass.GetCurrency(currencyID);
            //    currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
            //}

            var cID = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (cID != null)
            {
                currencyID = Guid.Parse(cID.DefaultCurrencyID.ToString());
                currency = oManageClass.GetCurrency(currencyID);
                currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
            }
        }
    }
}