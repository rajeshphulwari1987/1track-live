﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerImageManager.ascx.cs"
    Inherits="IR_Admin.usercontrol.BannerImageManager" %>
<script type="text/javascript">
    function cropClk() {
        $("#hdnCrop").val(1);
    }
    function isCropedImg() {
        var valu = $("#hdnCrop").val();

        if (valu == 0) {
            alert("Please Upload and Crop image before to submit.");
            return false;
        } else {
            $("#hdnCrop").val(0);
            return true;
        }
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
<style type="text/css">
    .panes div
    {
        display: block;
        font-size: 14px;
        padding: 0 1px;
    }
    .divMainImgManger
    {
        background: #eeeeee none repeat scroll 0 0;
        border: 1px solid #ccc;
        border-radius: 5px;
        float: left;
        width: 500px;
    }
    .divleftImgManger
    {
        border-bottom: 1px dashed #b1b1b1;
        float: left;
        line-height: 35px;
        padding-bottom: 3px !important;
        padding-top: 4px !important;
        width: 200px;
        color: #000000;
    }
    
    .divrightImgManger
    {
        border-bottom: 1px dashed #b1b1b1;
        float: left;
        line-height: 35px;
        padding-bottom: 3px !important;
        padding-top: 4px !important;
        text-align: left;
        width: 295px;
    }
    .divleftbtnImgManger
    {
        color: transparent;
        float: left;
        line-height: 35px;
        padding-top: 5px !important;
        width: 200px;
    }
    .divrightbtnImgManger
    {
        float: left;
        line-height: 35px;
        padding-top: 5px !important;
        width: 295px;
    }
    select
    {
        border: 1px solid #909090;
        color: #666666;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12px;
        height: 23px;
        line-height: 23px;
        margin-bottom: 0;
        margin-left: 0;
        margin-right: 0;
        padding: 0;
        width: 202px;
    }
    .textname
    {
        border: 1px solid #909090;
        color: #666666;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12px;
        height: 23px;
        line-height: 23px;
        margin: 0;
        padding: 0;
        width: 202px;
    }
    .btnupload
    {
        -moz-user-select: none;
        background-color: #f5f5f5;
        background-image: -moz-linear-gradient(center top , #f5f5f5, #f1f1f1);
        border: 1px solid rgba(0, 0, 0, 0.1);
        border-radius: 2px;
        color: #444444 !important;
        cursor: default !important;
        display: inline-block;
        font-size: 11px;
        font-weight: bold;
        height: 35px;
        line-height: 35px;
        min-width: 54px;
        text-align: center;
        text-decoration: none !important;
    }
    .btncrop
    {
        width: 34% !important;
        -moz-user-select: none !important;
        background-color: #d2d2d2 !important;
        background-image: -moz-linear-gradient(center top , #d2d2d2, #f0f0f0) !important;
        border: 1px solid rgba(0, 0, 0, 0.1) !important;
        border-radius: 2px !important;
        color: #444444 !important;
        cursor: default !important;
        display: inline-block !important;
        font-size: 11px !important;
        font-weight: bold !important;
        height: 35px !important;
        line-height: 35px !important;
        min-width: 54px !important;
        text-align: center !important;
        text-decoration: none !important;
    }
    .divrightImgManger input[type="checkbox"], input[type="radio"]
    {
        line-height: normal;
        margin: 5px 0 17px;
    }
    .warning
    {
        color: #9F6000;
        background-size: 40px 30px;
        font-size: 14px;
        background-color: #FEEFB3;
        background-image: url('images/warning.png');
        border: 1px solid;
        margin: 10px 0px;
        padding: 15px 10px 15px 50px !important;
        background-repeat: no-repeat;
        background-position: 10px center;
        -moz-border-radius: .5em;
        -webkit-border-radius: .5em;
        border-radius: .5em;
    }
</style>
<div class="full mr-tp1">
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <div class="panes">
            <asp:HiddenField ID="hdnId" runat="server" />
            <div id="divNew" runat="server" style="display: Block;">
                <div class="divMainImgManger">
                    <div class="divleftImgManger">
                        Category:
                    </div>
                    <div class="divrightImgManger">
                        <asp:DropDownList ID="ddlCat" runat="server" AutoPostBack="True" Enabled="false"
                            Width="197px" CssClass="selectddl" />
                        <asp:RequiredFieldValidator ID="rvCat" runat="server" ControlToValidate="ddlCat"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" ValidationGroup="rvSave" />
                    </div>
                    <div class="divleftImgManger">
                        Name:
                    </div>
                    <div class="divrightImgManger">
                        <asp:TextBox ID="txtName" runat="server" Width="195px" CssClass="textname" />
                        <asp:RequiredFieldValidator ID="rvName" runat="server" ControlToValidate="txtName"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="rvSave" />
                    </div>
                    <div class="divleftImgManger">
                        Image/Photo:
                    </div>
                    <div class="divrightImgManger">
                        <asp:FileUpload ID="imgUpload" runat="server" Width="195px" CssClass="btnupload" />
                    </div>
                    <div class="divleftImgManger" style="color: transparent;">
                        .
                    </div>
                    <div class="divrightImgManger">
                        <asp:Button ID="btnUp" runat="server" Text="Upload & Crop" CssClass="btncrop" OnClick="btnUp_Click"
                            ValidationGroup="rvSave" OnClientClick="cropClk()" />
                            &nbsp;
                        <asp:TextBox ID="txtWidth" runat="server" Width="70" placeholder="Width (in px)" MaxLength="5" Height="33"
                            CssClass="textname" onkeypress="return isNumberKey(event)" />
                        <asp:RequiredFieldValidator ID="rvWidth" runat="server" ControlToValidate="txtWidth"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="rvSave" />&nbsp;
                        <asp:TextBox ID="txtHeight" runat="server" Width="70" placeholder="Height (in px)" MaxLength="5" Height="33"
                            CssClass="textname" onkeypress="return isNumberKey(event)" />
                        <asp:RequiredFieldValidator ID="rvHeight" runat="server" ControlToValidate="txtHeight"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="rvSave" />
                    </div>
                    <div class="divleftImgManger">
                        Is Active:
                    </div>
                    <div class="divrightImgManger">
                        <asp:CheckBox ID="chkactive" runat="server" />
                    </div>
                    <div class="divleftbtnImgManger" style="color: transparent;">
                        .
                    </div>
                    <div class="divrightbtnImgManger">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                            OnClientClick="return isCropedImg()" Text="Submit" Width="89px" ValidationGroup="rvSave" />
                        <input type="button" id="Button1" value="Cancel" class="button btnClose"  data-dismiss="modal" rel="divcloseBanner" />
                        <asp:HiddenField ID="hdnCrop" runat="server" Value="0" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="divMainImgManger" style="width: 200px; margin-left: 50px; text-align: center;
                    min-height: 150px;">
                    <asp:Image ID="imgFile" runat="server" Width="140" Height="150" />
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <div id="DivWarning" runat="server" class="warning" style="display: none;">
                <asp:Label ID="lblMsg" runat="server" />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function ShowBannerPopUp() {
        $("#ContentBanner").slideToggle("slow");
        $("#divUploadBanner").slideToggle("slow");
        try {
            $("#mymodalbannerupload").modal('show');
        }
        catch (err) {
        }
    }
    function ReloadCurrentPage() {
        window.location.href = window.location.href;
    }
    function ShowPopup() {
        $("#ContentBanner").slideToggle("slow");
    }
</script>
