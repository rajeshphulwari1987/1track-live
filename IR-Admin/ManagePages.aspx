﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Title="Manage Cms"
    Inherits="IR_Admin.ManagePages" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 70%;
            margin-left: 50px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 529px;
            left: 277px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .PopUpSample2
        {
            position: fixed;
            width: 529px;
            left: 277px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px;
            left: 277px;
            top: 150px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .btnClose
        {
            background: #606061;
            color: #FFFFFF;
            line-height: 25px;
            position: absolute;
            right: -12px;
            text-align: center;
            top: -10px;
            width: 24px;
            text-decoration: none;
            font-weight: bold;
            -webkit-border-radius: 12px;
            -moz-border-radius: 12px;
            border-radius: 12px;
            -moz-box-shadow: 1px 1px 3px #000;
            -webkit-box-shadow: 1px 1px 3px #000;
            box-shadow: 1px 1px 3px #000;
        }
        .btnClose:hover
        {
            background: #E58A42;
        }
        .redactor_redactor_editor
        {
            width: 200px !important;
        }
    </style>
    <script src="editor/jquery.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(
	        function () {
	            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
	        }
        );

	        $(document).ready(function () {

	            $(".cms").mouseover(function () {
	                var clkElement = $(this).attr('id').substring(2, $(this).attr('id').length);
	                var divid = "div" + clkElement;
	                $('.' + divid).show();

	            }).mouseout(function () {
	                var clkElement = $(this).attr('id').substring(2, $(this).attr('id').length);
	                var divid = "div" + clkElement;
	                $('.' + divid).hide();
	            });

	            $(".edit").click(function () {

	                if ($(this).attr("rel") == "Image1" || $(this).attr("rel") == "Image2") {
	                    $("#Content3").slideToggle("slow");
	                    //$('.hiddenc').val($(this).attr("rel"));
	                }
	                else {
	                    $("#diveditorContent").slideToggle("slow");
	                    var value = $('div#' + $(this).attr("rel") + '').html();
	                    $('#txtContent').val(value.trim());
	                    //$('.hiddenc').val($(this).attr("rel"));
	                }
	            });

	            $(".editHead").click(function () {
	                //debugger
	                $("#diveditorHead").slideToggle("slow");
	                //alert($('div#' + $(this).attr("rel") + '').html());
	                var value = $('div#' + $(this).attr("rel") + '').html();
	                $('#txtHeading').val(value.trim());
	                $('.hiddenh').val($(this).attr("rel"));
	            });

	            $(".btnClose").click(function () {
	                if ($(this).attr("rel") == "Content3") {
	                    $("#Content3").hide();
	                }
	                else {
	                    $("#diveditorContent").hide();
	                }
	            });

	            //            $(".btnSave").click(function () {
	            //                var Elementrel = $(this).attr("rel");
	            //                if (Elementrel != "Image") {
	            //                    var txtvalue = $('.hiddenc').val() + ':' + $('textarea[name=txtContent]').val();
	            //                    alert(txtvalue);
	            //                    $.ajax({
	            //                        type: "POST",
	            //                        url: "ManagePages.aspx/ManageCms",
	            //                        data: '{content: "' + txtvalue + '" }',
	            //                        contentType: "application/json; charset=utf-8",
	            //                        dataType: "json",
	            //                        success: OnSuccess,
	            //                        failure: function (response) {
	            //                            alert(response.d);
	            //                        }
	            //                    });
	            //                }
	            //                else {
	            //                    var imagename;
	            //                    var txtvalue;
	            //                    $('div#Content3 input[type=radio]').each(function () {

	            //                        if ($(this).is(":checked")) {
	            //                            imagename = $(this).attr('value');
	            //                        }
	            //                    });
	            //                    txtvalue = $('.hiddenc').val() + ":" + imagename;
	            //                    $.ajax({
	            //                        type: "POST",
	            //                        url: "ManagePages.aspx/ManageCms",
	            //                        data: '{content: "' + txtvalue + '" }',
	            //                        contentType: "application/json; charset=utf-8",
	            //                        dataType: "json",
	            //                        success: OnSuccess,
	            //                        failure: function (response) {
	            //                            alert(response.d);
	            //                        }
	            //                    });
	            //                }
	            //            });
	        });
        function OnSuccess(response) {
            if (response.d == "true") {
                // alert("Record Updated!!");
                $("#diveditorContent").hide();
                document.location.reload();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 70%;">
        <asp:DataList ID="dtList" runat="server">
            <ItemTemplate>
                <table class="style1" align="center">
                    <tr>
                        <td colspan="2">
                            <img src="CMSImages/Header.png" width="900px" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="cms" id="td5cms">
                            <div class="div5cms" style="display: none; position: absolute;">
                                <img src="CMSImages/edit.png" class="editHead" rel="pageheading" />
                            </div>
                            <h1>
                                <div id="pageheading">
                                    <%# DataBinder.Eval(Container.DataItem, "pageheading")%>
                                </div>
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td class="cms" id="td3cms">
                            <div class="div3cms" style="display: none; position: absolute;">
                                <img src="CMSImages/edit.png" class="edit" rel="Image1" />
                            </div>
                            <img src="<%# Eval("image2", "CMSImages/{0}") %>" style="width: 142px;" />
                        </td>
                        <td class="cms" id="td1cms">
                            <div id="divcms" class="div1cms" style="display: none; position: absolute;">
                                <img src="CMSImages/edit.png" class="edit" rel="Content1" />
                            </div>
                            <div id="Content1">
                                <%# DataBinder.Eval(Container.DataItem, "content1")%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="cms" id="td4cms">
                            &nbsp;
                            <br />
                            <div class="div4cms" style="display: none; position: absolute;">
                                <img src="CMSImages/edit.png" class="edit" rel="Image2" />
                            </div>
                            <img src="<%# Eval("image2", "CMSImages/{0}") %>" style="width: 142px;" />
                        </td>
                        <td class="cms" id="td2cms">
                            <div id="divdtList_type2_2" class="div2cms" style="display: none; position: absolute;">
                                <img src="CMSImages/edit.png" class="edit" rel="Content2" />
                            </div>
                            <div id="Content2">
                                <%# DataBinder.Eval(Container.DataItem, "content2")%>
                            </div>
                        </td>
                    </tr>
                    <td colspan="2">
                        <img src="CMSImages/footer.png" width="900px" />
                        &nbsp;
                    </td>
                    <tr>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="Content3" class="PopUpSampleIMG" style="display: none">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <img src="CMSImages/images1.jpg" style="width: 100px;" />
                            </td>
                            <td>
                                <img src="CMSImages/images_2.jpg" style="width: 100px;" />
                            </td>
                            <input type="hidden" class="hiddenc" value="" id="Hidden1" />
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="img" checked="checked" value="images1.jpg" />
                            </td>
                            <td>
                                <input type="radio" name="img" checked="checked" value="images_2.jpg" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding-top: 5px;">
                                <input type="button" id="btnSave1" value="Save" class="btnSave" rel="Image" />
                                <a href="#" class="btnClose" rel="Content3" title="Close">X</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="PopUpSample" id="diveditorContent" style="display: none;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
                        <tbody>
                            <tr>
                                <td colspan="2" style="background-color: gainsboro;">
                                    <b>Edit Content</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <textarea id="txtContent" name="txtContent" />
                                    <input type="hidden" class="hiddenc" value="" id="Hidden2" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: 5px;">
                                    <input type="button" id="btnSave" value="Save" class="btnSave" rel="Content1" />
                                    <a href="#" class="btnClose" rel="Content2" title="Close">X</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <%--<div class="PopUpSample2" id="diveditorHead" style="display: none;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
                        <tbody>
                            <tr>
                                <td colspan="2" style="background-color: gainsboro;">
                                    <b>Edit Heading</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <textarea id="txtHeading" name="txtHeading" />
                                    <input type="hidden" class="hiddenh" value="" id="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: 5px;">
                                    <input type="button" id="Button1" value="Save" class="btnSave" rel="pageheading" />
                                    <a href="#" class="btnClose" rel="Content2" title="Close">X</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>--%>
            </ItemTemplate>
        </asp:DataList>
        <div class="clear">
        </div>
    </div>
    </form>
</body>
</html>
