﻿<%@ Page Title="Country " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Languages.aspx.cs" Inherits="IR_Admin.LanguagePage" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script>
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });
        $(document).ready(function () {
            if ('<%=tab.ToString()%>' == '1') {
                document.getElementById('<%= hdnID.ClientID %>').value = '';
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }

            $(".list a").click(function (e) {
                $('input:text').val('');
                if ($(this).attr("id") == 'aList') {
                    document.getElementById('MainContent_divlist').style.display = 'Block';
                    document.getElementById('MainContent_divNew').style.display = 'none';
                }
                else {
                    document.getElementById('<%= hdnID.ClientID %>').value = '';
                    $("#<%=btnSubmit.ClientID%>").val('Submit');
                    document.getElementById('MainContent_divlist').style.display = 'none';
                    document.getElementById('MainContent_divNew').style.display = 'Block';
                }
            });
            function ResetDiv() {
                document.getElementById('MainContent_aNew').className = 'current';
                document.getElementById('aList').className = ' ';
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }
        });
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnID" runat="server" />
    <h2>
        Languages</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="#" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" CssClass=" " NavigateUrl="#" runat="server">New/Edit</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdLanguage" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdLanguage_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Language Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Languages Icon">
                                    <ItemTemplate>
                                        <%#Eval("Logo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: Block;">
                    <div class="divMain">
                        <div class="divleft">
                            Language Name:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtLName" runat="server" />
                            <asp:RequiredFieldValidator ID="txtLNameRequiredFieldValidator" runat="server" ControlToValidate="txtLName"
                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="LanForm" />
                        </div>
                        <div class="divleft">
                            Language Icon:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtICode" runat="server" />
                        </div>
                        <div class="divleft">
                            Is Active?
                        </div>
                        <div class="divright">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="divleftbtn">
                            .
                        </div>
                        <div class="divrightbtn">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="LanForm" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
