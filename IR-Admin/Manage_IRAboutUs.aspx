﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_IRAboutUs.aspx.cs"
    Inherits="IR_Admin.Manage_IRAboutUs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html class="no-js" lang="en">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>International Rail</title>
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:400" rel="stylesheet"
        type="text/css" async="" />
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet"
        type="text/css" async="" />
    <link href="Styles/ircss/css/bootstrap.min.css" rel="stylesheet">
    <link href="Styles/ircss/css/font-awesome.min.css" rel="stylesheet">
    <link href="Styles/ircss/css/internationalrail.css" rel="stylesheet">
    <link href="Styles/ircss/css/layout.css" rel="stylesheet">
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent').redactor({ iframe: true, minHeight: 200 });

            //Edit banner images
            $(".editbtn1").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });
            $(".editbtn5").click(function () {
                $("#divContent").slideToggle("slow");
            });
            $(".editbtn13").click(function () {
                $("#divHeading").slideToggle("slow");
            });


            //----------Edit heading---------//
            $(".editbtn13").click(function () {
                var value;
                $("#divHeading").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdHead').contents().find('body').html(value);
            });

            $(".editbtn5").click(function () {
                var value;
                $("#divContent").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent').contents().find('body').html(value);
            });


            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }

            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <div class="page-wrap">
        <div class="navbarbg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img src="Styles/ircss/images/top.jpg" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="Styles/ircss/images/logo-panel.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="navouter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                        <div class="navouter bdrbtm">
                            <div class="pull-right pos-rel agent_login">
                                <i class="fa fa-user fa-2x search "></i><a href="#">Agent Login</a>
                            </div>
                            <div class="clearfix visible-xs">
                            </div>
                            <div class=" collapse navbar-collapse pull-left">
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="active"><span></span>HOME </a></li>
                                    <li><a href="#"><span></span>RAIL PASSES </a></li>
                                    <li><a href="#"><span></span>RAIL TICKETS </a></li>
                                    <li><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                        id="dropdownMenu2"><span></span>COUNTRY </a></li>
                                    <li class="dropdown dropdown-large "><a href="#"><span></span>SPECIAL TRAINS </a>
                                        <ul class="dropdown-menu dropdown-menu-large row">
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Glyphicons</li>
                                                    <li><a href="#">Available glyphs</a></li>
                                                    <li class="disabled"><a href="#">How to use</a></li>
                                                    <li><a href="#">Examples</a></li>
                                                    <li><a href="#">Example</a></li>
                                                    <li><a href="#">Aligninment options</a></li>
                                                    <li><a href="#">Headers</a></li>
                                                    <li><a href="#">Disabled menu items</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Button groups</li>
                                                    <li><a href="#">Basic example</a></li>
                                                    <li><a href="#">Button toolbar</a></li>
                                                    <li><a href="#">Sizing</a></li>
                                                    <li><a href="#">Nesting</a></li>
                                                    <li><a href="#">Vertical variation</a></li>
                                                    <li><a href="#">Single button dropdowns</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Input groups</li>
                                                    <li><a href="#">Basic example</a></li>
                                                    <li><a href="#">Sizing</a></li>
                                                    <li><a href="#">Checkboxes and radio addons</a></li>
                                                    <li><a href="#">Tabs</a></li>
                                                    <li><a href="#">Pills</a></li>
                                                    <li><a href="#">Justified</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Navbar</li>
                                                    <li><a href="#">Default navbar</a></li>
                                                    <li><a href="#">Buttons</a></li>
                                                    <li><a href="#">Text</a></li>
                                                    <li><a href="#">Non-nav links</a></li>
                                                    <li><a href="#">Component alignment</a></li>
                                                    <li><a href="#">Fixed to top</a></li>
                                                    <li><a href="#">Fixed to bottom</a></li>
                                                    <li><a href="#">Static top</a></li>
                                                    <li><a href="#">Inverted navbar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><span></span>CONTACT US </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="container">
                <div class="row ">
                    <div class="editbaner">
                        <div class="editbtn1">
                            <a href="#" class="edit-btn">Edit</a></div>
                        <img src="Styles/ircss/images/about-banner.png" alt="" class="img-responsive" id="imgMainBanner"
                            runat="server">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row ">
                    <div class="editbaner">
                        <div class="editbtn5">
                            <a href="#" class="edit-btn">Edit</a></div>
                    </div>
                    <div class="editbaner">
                        <div class="editbtn13">
                            <a href="#" class="edit-btn">Edit</a></div>
                    </div>
                    <div style="padding-top: 5px" class="starail-BookingDetails-titleAndButton starail-Wrapper">
                        <h2>
                            <div id="ContentHead" runat="server">
                                <h1>
                                    About us - We have over 20 years experience in the rail industry</h1>
                            </div>
                        </h2>
                        <div class="starail-Section starail-Section--nopadding">
                            <p>
                            </p>
                            <div id="ContentText" runat="server">
                                <p>
                                    <span id="lblContent">International Rail is an independent limited liability company,
                                        specializing in the sale and distribution of Rail Tickets and Passes for rail travel,
                                        world-wide. With over 20 years in the rail industry, we have been able to put our
                                        experience to good use to create the most comprehensive international rail booking
                                        website in existence. Our UK headquarters are based in Hampshire and here we have
                                        our call center where our highly trained staff are there to help you plan and cost
                                        your journey, to help you make the most of your time away. Our product portfolio
                                        is extensive and is constantly being updated. In Europe it includes over 66,000
                                        Point to Point routes including Eurostar, French TGV, German ICE, InterCity, EuroCity
                                        and Spanish high speed trains. When booking online, rest assured that your personal
                                        and account information is secure on our sites as we employ the latest 128-bit encryption
                                        technology in all parts of our site that require any personal information exchange
                                        including the payment pages.Rail fares have become as complex as air fares and require
                                        specialist consultants to ensure the best ticket price is obtained. The route, journey
                                        time and ticket type will affect the price of each ticket. Our technology platforms
                                        are geared to sourcing the lowest available fare and in addition, we routinely offer
                                        excellent discounts on standard ticket prices for our customers based on different
                                        time options and through the combination of one way fares. If we are using a photograph
                                        under copyright and by mistake, omit to show the correct copyright details, we apologize
                                        to those concerned and once notified, will remove the picture straight away, or
                                        if allowed, make reference to the owner in the correct way. It is not our intention
                                        to use any picture without authorization</span></p>
                            </div>
                            <p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerwraper">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Destinations</h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/australia">Australia</a></li>
                                <li><a href="https://www.internationalrail.com/italy">Italy</a></li>
                                <li><a href="https://www.internationalrail.com/japan">Japan</a></li>
                                <li><a href="https://www.internationalrail.com/new-zealand">New Zealand</a></li>
                                <li><a href="https://www.internationalrail.com/united-states">USA</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Rail Companies
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/eurostar">Eurostar</a></li>
                                <li><a href="https://www.internationalrail.com/thalys">Thalys</a></li>
                                <li><a href="https://www.internationalrail.com/thello">Thello</a></li>
                                <li><a href="https://www.internationalrail.com/trenitalia">Trenitalia</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Travel the world
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/Balkan-FlexiPass-(EUR)/balkan-flexipass">
                                    Balkan Flexi-Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Japanese-Rail-Passes/japan-rail-pass">
                                    Japan Rail Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Korean-Rail-Passes/korea-rail-pass">Korea
                                    Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Spanish-Rail-Passes/renfe-spain-pass">
                                    Spain Pass</a></li>
                                <li><a href="https://www.internationalrail.com/USA-Rail-Passes/usa-rail-pass">USA Rail
                                    Pass</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Legal information
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/contact-us">Contact us</a> </li>
                                <li><a href="https://www.internationalrail.com/conditions-of-use">Conditions of Use</a></li>
                                <li><a href="https://www.internationalrail.com/Cookies">Cookies</a></li>
                                <li><a href="https://www.internationalrail.com/Booking-Conditions">General terms and
                                    conditions</a></li>
                                <li><a href="https://www.internationalrail.com/privacy-policy">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                            <ul>
                                <li><a href="https://www.internationalrail.com/">Home </a></li>
                                <li><a href="https://www.internationalrail.com/Contact-Us">Contact Us </a></li>
                                <li><a href="https://www.internationalrail.com/About-Us">About Us </a></li>
                                <li><a href="https://www.internationalrail.com/Booking-Conditions">Booking Conditions
                                </a></li>
                                <li><a href="https://www.internationalrail.com/Privacy-Policy">Privacy Policy </a>
                                </li>
                                <li><a href="https://www.internationalrail.com/Conditions-of-Use">Conditions of Use
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                            <p class="copyright">
                                © Copyright International Rail Ltd. 2015 - All rights reserved. A company registered
                                in England and Wales, company number: 3060803 with registered offices at International
                                Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire.
                                SO53 4AR.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center social_link">
                            <a href="#"><i class=" fa fa-facebook-square fa-2x"></i></a><a href="#"><i class="fa fa-twitter-square fa-2x">
                            </i></a><a href="#"><i class=" fa fa-rss-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 203px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <script src="Styles/ircss/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            },
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            });
        });
    </script>
    </form>
</body>
</html>
