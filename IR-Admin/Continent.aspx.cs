﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Continent : Page
    {
        readonly private Masters _oMasters = new Masters();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            ShowMessage(0, null);
            if (ViewState["tab"] != null)
                tab = ViewState["tab"].ToString();
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(hdnId.Value))
                    tab = "2";
                FillGrid();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int result = _oMasters.AddContinent(new tblContinent
                    {
                        ID = string.IsNullOrEmpty(hdnId.Value) ? new Guid() : Guid.Parse(hdnId.Value),
                        Name = txtName.Text.Trim(),
                        IsActive = chkactive.Checked,
                        CreatedBy = AdminuserInfo.UserID,
                        CreatedOn = DateTime.Now
                    });

                ShowMessage(1, !String.IsNullOrEmpty(hdnId.Value) ? "Record updated successfully" : "Record added successfully");
                chkactive.Checked = false;
                txtName.Text = hdnId.Value = string.Empty;
                FillGrid();
                tab = "1";
                ViewState["tab"] = 1;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void FillGrid()
        {
            var list = _oMasters.GetContinentList();
            if (list != null && list.Count > 0)
            { grvContinent.DataSource = list; } grvContinent.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Continent.aspx");
        }

        protected void grvContinent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvContinent.PageIndex = e.NewPageIndex;
            FillGrid();
            tab = "1";
        }

        protected void grvContinent_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                btnSubmit.Text = "Update";
                var id = Guid.Parse(e.CommandArgument.ToString());
                var ocls = _oMasters.GetContinentById(id);
                txtName.Text = ocls.Name;
                chkactive.Checked = ocls.IsActive;
                hdnId.Value = id.ToString();
                ViewState["tab"] = "2";
                tab = "2";
            }
            if (e.CommandName == "Remove")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                bool result = _oMasters.DeleteContinent(id);
                if (result)
                    ShowMessage(1, "Record deleted successfully");
                FillGrid(); tab = "1";
                ViewState["tab"] = "1";
            }

            if (e.CommandName == "ActiveInActive")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                _oMasters.ActiveInactiveContinent(id);
                FillGrid(); tab = "1";
                ViewState["tab"] = "1";
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}