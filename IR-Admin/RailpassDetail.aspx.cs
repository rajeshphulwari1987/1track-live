﻿#region Using
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
#endregion

public partial class RailpassDetail : Page
{

    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static string ClassName = string.Empty;
    public static string SortcolumnName = string.Empty;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public ManageProduct _master = new ManageProduct();
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly ManageBooking _BMaster = new ManageBooking();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    public static List<getCuntryCode> Levellist = new List<getCuntryCode>();
    public bool passportValid = false;
    string currency;
    string curID;
    Guid siteId;
    Guid productCurID;
    static int startDrp = 0;
    static int endDrp = 11;
    public string script = "<script></script>";
    public string siteURL;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
            Newsletter1.Visible = _masterPage.IsVisibleNewsLetter(siteId);
        }
    }
    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _masterPage.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", SiteUrl + "CMSImages");
            }
            else
                imgRightPanel.Visible = false;
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Response.Cache.SetNoStore();
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);

            string QStr = (Request.QueryString["id"] == null ? "" : Request.QueryString["id"]);
            Guid productCatID;
            if (Guid.TryParse(QStr, out productCatID))
            {
                Session["getproductCatID"] = productCatID;
            }
            else
            {
                Session["getproductCatID"] = Guid.Empty;
            }

            SortcolumnName = string.Empty;
            ClassName = "Asc";
            Session.Remove("CountryCodeList");
            Session.Remove("GETcountryCODE");
            QubitOperationLoad();
        }
        PageLoadEvent();
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    void PageLoadEvent()
    {
        if (Request.QueryString["id"] == null)
            return;
        if (Request.QueryString["affid"] != null)
            Session["AffiliateID"] = Request.QueryString["affid"];

        try
        {
            GetCurrencyCode();
            var pId = new Guid(Request.QueryString["id"]);

            tblProduct oProd = oManageClass.GetProductById(pId);
            passportValid = oProd.PassportValid.HasValue ? oProd.PassportValid.Value : false;
            int CULVL_Code = oProd.CountryStartCode;
            GetCountryLevel(CULVL_Code);
            if (Session["GetcunLvl"] != null && Session["GetcunLvl"].ToString() == "0")
            {
                ViewState["setget"] = oProd.CountryStartCode + "," + oProd.CountryEndCode;
            }

            getproductname.InnerText = oManageClass.GetProductName(oProd.ID);
            productCurID = oProd.CurrencyID;
            imgBanner.ImageUrl = !string.IsNullOrEmpty(oProd.BannerImage) ? SiteUrl + oProd.BannerImage : "images/img_inner-banner.jpg";
            imgMap.ImageUrl = !string.IsNullOrEmpty(oProd.CounrtyImage) ? SiteUrl + oProd.CounrtyImage : "images/innerMap.gif";

            rptDetails.DataSource = oManageClass.BindPageDetails(pId);
            rptDetails.DataBind();

            var travller = oManageClass.GetProductTraveller(pId);
            rptTraveller.DataSource = travller;
            rptTraveller.DataBind();
            string str = string.Empty;
            int i = 1;
            foreach (var item in travller)
            {
                String after = item.Name.Substring(0, 1).ToUpper() + item.Name.Substring(1).ToLower();
                str = str + "<p> <span> " + (i++) + " </span> " + after + " " + item.AgeFrom;

                if (item.AgeTo <= 60)
                    str = str + " -" + item.AgeTo + " </p> ";
                else
                    str = str + "+ </p> ";
            }
            divTraveller.InnerHtml = str;
            rptTravellerGrid.DataSource = oManageClass.GetProductTraveller(pId);
            rptTravellerGrid.DataBind();
            lblPrductName.Text = oManageClass.GetProductName(pId);

            var countryId = oManageClass.GetCountryIdOfProduct(pId);
            BindCountryName(countryId);
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetCountryLevel(int CulevelID)
    {
        switch (CulevelID)
        {
            case 3001:
                Session["GetcunLvl"] = 3;
                break;
            case 4001:
                Session["GetcunLvl"] = 4;
                break;
            case 5001:
                Session["GetcunLvl"] = 5;
                break;
            default:
                Session["GetcunLvl"] = 0;
                break;
        }
    }

    public void GetProductListByTraveler(Guid id, Guid pid, Guid sid, GridView gv)
    {
        try
        {
            var AgentId = new Guid();
            if (Session["AgentUserID"] == null)
            {
                AgentId = Guid.NewGuid();
            }
            else
                AgentId = Guid.Parse(Session["AgentUserID"].ToString());

            gv.DataSource = null;
            gv.DataBind();
            using (var conn = new SqlConnection(con))
            {
                var sqlCommand = new SqlCommand("spGetProductListByTravellerId", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TravelerID", id);
                sqlCommand.Parameters.AddWithValue("@ProductID", pid);
                sqlCommand.Parameters.AddWithValue("@SiteID", sid);
                sqlCommand.Parameters.AddWithValue("@AgentID", AgentId.ToString());
                conn.Open();
                var da = new SqlDataAdapter(sqlCommand);
                var ds = new DataSet();
                da.Fill(ds);
                startDrp = 0;
                endDrp = 11;
                var dv = ds.Tables[0].DefaultView;
                dv.Sort = SortcolumnName;

                gv.DataSource = dv;
                gv.DataBind();
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void rptTraveller_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var lnkTraveller = (LinkButton)e.Item.FindControl("lnkTraveller");
        string[] match = lnkTraveller.Text.ToLower().Split(' ');
        for (int i = 0; i < match.Length; i++)
        {
            if (match[i].Equals("saver"))
            {
                //lnkTraveller.ToolTip = "Travelling together? See below point 3, Eligibility for Saving";
                //break;
            }
        }
        ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(lnkTraveller);
    }

    protected void grdPrice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Visible = false;
        e.Row.Cells[1].Visible = false;
        int countCell = e.Row.Cells.Count;

        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            if (countCell > 3)
            {
                for (int i = 3; i < countCell; i++)
                {
                    var lblname = new Label();
                    lblname.ID = "lblname" + i.ToString();
                    lblname.Text = e.Row.Cells[i].Text;

                    var LnkSortID = new LinkButton();//Get C0untry StartCode EndCpode
                    LnkSortID.ID = "LnkSortID" + i.ToString();
                    LnkSortID.CausesValidation = false;
                    LnkSortID.Attributes.Add("class", "SortBtnStyle " + ClassName);
                    LnkSortID.Width = 20;
                    LnkSortID.Height = 20;
                    LnkSortID.Click += new EventHandler(LnkSortID_Click);
                    if (ClassName == "Asc")
                    {
                        string name = SortcolumnName.Replace(" Desc", string.Empty);
                        if (e.Row.Cells[i].Text == name)
                        {
                            LnkSortID.Text = "▲";
                            LnkSortID.ToolTip = "Desc";
                        }
                        else
                        {
                            LnkSortID.Text = "▼";
                            LnkSortID.ToolTip = "Asce";
                        }
                    }
                    else
                    {
                        LnkSortID.Text = "▼";
                        LnkSortID.ToolTip = "Asce";
                    }
                    e.Row.Cells[i].Controls.Add(lblname);
                    e.Row.Cells[i].Controls.Add(LnkSortID);
                    ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(LnkSortID);
                }
            }
        }
        else
        {
            if (countCell > 3)
            {
                for (int i = 3; i < countCell; i++)
                {
                    var ltrName = new Label();
                    ltrName.ID = "ltrName" + i.ToString();
                    
                    //var hdnClassName = new HiddenField();
                    //hdnClassName.ID = "hdnClassName-" + i.ToString();
                    //hdnClassName.Value = e.Row.Cells[i].Text;
                    //e.Row.Cells[i].Controls.Add(hdnClassName);
                    ////e.Row.Cells[i].Text
                    
                    var hdnActualPrice = new HiddenField();
                    hdnActualPrice.ID = "hdnActualPrice" + i.ToString();

                    var ltrCurrency = new Label { ID = "ltrCurr" + i.ToString(), Text = currency };
                    ltrCurrency.Attributes.Add("Style", "padding-right:4px; font-weight:bold;");
                    ltrCurrency.Attributes.Add("class", "currency-value");

                    var hdnClass = new Label();
                    hdnClass.Text = "CLASS-01" + e.Row.RowIndex.ToString();
                    hdnClass.Style.Add("display", "none");
                    ltrName.Attributes.Add("class", "ltrName");
                    ltrName.Attributes.Add("Style", "padding-right:4px;");

                    ltrName.Text = string.IsNullOrEmpty(e.Row.Cells[i].Text) || e.Row.Cells[i].Text == "&nbsp;" ? "-" : e.Row.Cells[i].Text;
                    e.Row.Cells[i].Controls.Add(hdnClass);
                    if (ltrName.Text != "-")
                    {
                        e.Row.Cells[i].Controls.Add(ltrCurrency);
                        hdnActualPrice.Value = ltrName.Text;
                        ltrName.Text = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(ltrName.Text), siteId, productCurID, Guid.Parse(curID))));

                    }

                    e.Row.Cells[i].Controls.Add(hdnActualPrice);
                    e.Row.Cells[i].Controls.Add(ltrName);
                    if (!string.IsNullOrEmpty(e.Row.Cells[i].Text) && e.Row.Cells[i].Text != "&nbsp;")
                    {
                        var hdnCLvl = new Label();//get Country Level
                        hdnCLvl.ID = "hdnCLvl" + i.ToString();
                        hdnCLvl.Text = Session["GetcunLvl"] != null ? Session["GetcunLvl"].ToString() : "0";
                        hdnCLvl.Attributes.Add("Style", "display:none;");

                        var hdnCLvlID = new LinkButton();//Get C0untry StartCode EndCpode
                        hdnCLvlID.ID = "hdnCLvlID" + i.ToString();
                        hdnCLvlID.CausesValidation = false;
                        hdnCLvlID.Click += new EventHandler(LnkGetCountryCode_Click);
                        hdnCLvlID.Attributes.Add("Style", "display:none;");

                        var ddl = new DropDownList();
                        ddl.ID = "ddlID" + i.ToString();
                        ddl.Attributes.Add("onchange", "callabc(this);");

                        for (int j = startDrp; j < endDrp; j++)
                        {
                            ddl.Items.Add(new ListItem(j.ToString(), j.ToString()));
                        }

                        e.Row.Cells[i].Controls.Add(hdnCLvl);
                        e.Row.Cells[i].Controls.Add(ddl);
                        e.Row.Cells[i].Controls.Add(hdnCLvlID);

                        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(hdnCLvlID);
                        string namdsd = e.Row.Cells[0].Text + ", " + e.Row.Cells[1].Text + ", " + e.Row.Cells[i].Text;

                        if (Session["CountryCodeList"] != null)
                        {
                            Levellist = Session["CountryCodeList"] as List<getCuntryCode>;//get previous countrycode
                            if (Levellist != null)
                            {
                                var dd = Levellist.SingleOrDefault(ty => ty.Row == e.Row.Cells[0].Text && ty.ID == e.Row.Cells[1].Text);
                                if (dd != null)
                                {
                                    hdnCLvlID.Text = dd.CuntryCode;
                                    ddl.SelectedValue = dd.selectValue;
                                }
                            }
                        }
                        else
                        {
                            if (ViewState["setget"] != null)
                            {
                                string[] ViewStateData = ViewState["setget"].ToString().Split(',');
                                hdnCLvlID.Text = ViewStateData[0] + "," + ViewStateData[1] + "ñ,,,,";
                            }
                            //else
                            //hdnCLvlID.Text = ",ñ,,,,";
                        }
                    }
                }
            }
        }
    }

    protected void LnkSortID_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        string id = btn.ID.Replace("LnkSortID", "lblname");
        var lbl = (Label)btn.Parent.FindControl(id);
        if (ClassName == "Asc")
        {
            SortcolumnName = lbl.Text + " " + ClassName;
            ClassName = "Desc";
        }
        else
        {
            SortcolumnName = lbl.Text + " " + ClassName;
            ClassName = "Asc";
        }
        PageLoadEvent();
        UpdatePanel1.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "getcurrentLinkPosition();callabc();", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void LnkGetCountryCode_Click(object sender, EventArgs e)
    {
        var Levellist1 = new List<getCuntryCode>();
        var btn = (LinkButton)sender;
        string selid = btn.ID.Replace("hdnCLvlID", "ddlID");
        var selvalue = (DropDownList)btn.Parent.FindControl(selid);
        var row = (GridViewRow)(btn.Parent.Parent);

        if (Session["CountryCodeList"] != null)
        {
            Levellist1 = Session["CountryCodeList"] as List<getCuntryCode>;//get previous countrycode
            Session.Remove("CountryCodeList");
            if (Levellist1 != null)
                Levellist1.RemoveAll(ty => ty.Row == row.Cells[0].Text && ty.ID == row.Cells[1].Text);
        }
        btn.Text = (Session["GETcountryCODE"] != null ? Session["GETcountryCODE"].ToString() : "");
        if (!string.IsNullOrEmpty(btn.Text))
        {
            var datacode = new getCuntryCode();
            datacode.CuntryCode = btn.Text.Trim();
            datacode.Row = row.Cells[0].Text;
            datacode.ID = row.Cells[1].Text;
            datacode.selectValue = selvalue.SelectedValue;

            Levellist1.Add(datacode);
            Session["CountryCodeList"] = Levellist1;
            Session["GETcountryCODE"] = null;
        }
    }

    protected void rptTravellerGrid_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var grdPrice = e.Item.FindControl("grdPrice") as GridView;
            var lblID = e.Item.FindControl("lblID") as Label;
            if (lblID != null)
            {
                ViewState["tID"] = lblID.Text; /*added for saver alert*/
                if (Request.QueryString["id"] != null)
                    GetProductListByTraveler(Guid.Parse(lblID.Text), Guid.Parse(Request.QueryString["id"]), Guid.Parse(Session["siteId"].ToString()), grdPrice);
            }
        }
    }

    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        try
        {
            var list = new List<getRailPassData>();
            string PrdtId = string.Empty;
            string PrdtName = string.Empty;
            string TravellerID = string.Empty;
            string TravellerName = string.Empty;
            string ValidityName = string.Empty;
            string Price = string.Empty;
            string Actualprice = string.Empty;
            string Qty = string.Empty;
            string CountryLevelCode = string.Empty;
            string MonthValidity = string.Empty;

            if (Request.QueryString["id"] != null)
            {
                PrdtId = Convert.ToString(Request.QueryString["id"]);
                PrdtName = _master.GetProductName(Guid.Parse(PrdtId));
                MonthValidity = _master.GetMonthValidity(Guid.Parse(PrdtId));
            }
            foreach (RepeaterItem item in rptTravellerGrid.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var grdPrice = item.FindControl("grdPrice") as GridView;
                    var lblTabId = item.FindControl("lblID") as Label;
                    if (grdPrice != null)
                    {
                        foreach (GridViewRow row in grdPrice.Rows)
                        {
                            TravellerID = lblTabId.Text;
                            foreach (RepeaterItem _TabName in rptTraveller.Items)
                            {
                                var lnkTabName = _TabName.FindControl("lnkTraveller") as LinkButton;
                                if (lnkTabName != null && lnkTabName.CommandArgument == TravellerID)
                                {
                                    TravellerName = lnkTabName.Text;
                                    break;
                                }
                            }

                            ValidityName = row.Cells[2].Text;
                            int countCell = row.Cells.Count;
                            var AgentId = new Guid();
                            if (Session["AgentUserID"] == null)
                            {
                                AgentId = Guid.NewGuid();
                            }
                            else
                                AgentId = Guid.Parse(Session["AgentUserID"].ToString());
                            for (int i = countCell; i > 0; i--)
                            {
                                string ltrName = "ltrName" + i.ToString();
                                string ddlIDs = "ddlID" + i.ToString();
                                string hdnCLvlIDs = "hdnCLvlID" + i.ToString();
                                string uppnls = "upd" + i.ToString();
                                string hdnPrice = "hdnActualPrice" + i.ToString();

                                var hdnActualPrice = row.FindControl(hdnPrice) as HiddenField;
                                var lblPrce = row.FindControl(ltrName) as Label;
                                var ddlID = row.FindControl(ddlIDs) as DropDownList;
                                var hdnCLvlID = row.FindControl(hdnCLvlIDs) as LinkButton;

                                if (ddlID != null && !ddlID.SelectedValue.Equals("0"))
                                {
                                    string ClassName = grdPrice.HeaderRow.Cells[i].Text;
                                    Qty = ddlID.SelectedValue;
                                    CountryLevelCode = hdnCLvlID.Text;
                                    Actualprice = hdnActualPrice.Value;
                                    Price = lblPrce.Text;

                                    string[] strSplit_dIDandCode = CountryLevelCode.Split('ñ');
                                    string[] strCode = strSplit_dIDandCode[0].Split(',');

                                    var listdata = _BMaster.GetClassAndValidityID((Actualprice.Trim()), ValidityName, ClassName, TravellerName, Guid.Parse(PrdtId), Guid.Parse(TravellerID), siteId, AgentId);
                                    for (int j = 0; j < Convert.ToInt32(Qty); j++)
                                    {
                                        var Id1 = Guid.NewGuid();
                                        if (listdata != null)
                                            list.Add(new getRailPassData { OriginalPrice = Convert.ToString(listdata.OriginalPrice), Id = Id1, PrdtId = PrdtId, TravellerID = TravellerID, ClassID = listdata.classid.ToString(), ValidityID = listdata.Validityid.ToString(), CategoryID = listdata.CategoryID.ToString(), PrdtName = PrdtName, TravellerName = TravellerName, ClassName = ClassName, ValidityName = ValidityName, Price = Price, SalePrice = Actualprice.Trim(), Qty = Qty, CountryStartCode = strCode[0], CountryEndCode = strCode[1], Commission = listdata.commission.ToString(), MarkUp = listdata.markup.ToString(), CountryLevelIDs = strSplit_dIDandCode[1], PassportIsVisible = passportValid, MonthValidity = MonthValidity });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
           
            if (list.Count > 0)
            {
                if (Session["RailPassData"] != null)
                {
                    var lstRP = Session["RailPassData"] as List<getRailPassData>;
                    lstRP.AddRange(list);
                    
                }
                else
                    Session.Add("RailPassData", list);
                Levellist.Clear();
                Session.Remove("CountryCodeList");
                Response.Redirect("PassDetail", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "showmsg()", true);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void BindCountryName(Guid cuId)
    {
        try
        {
            if (cuId != new Guid())
            {
                var cuName = _cMaster.GetCountryNameById(cuId);
                lblCn.Text = cuName.CountryName + " Passes";
                //lblCntyNm.Text = cuName.CountryName + " Rail Passes";
            }
            else
            {
                lblCn.Text = "Country";
                //lblCntyNm.Text = "Rail Passes";
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetCurrencyCode()
    {
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            curID = cookie.Values["_curId"];
            siteId = Guid.Parse(cookie.Values["_siteId"]);
            currency = oManageClass.GetCurrency(Guid.Parse(curID));
            hdnCurrencySign.Value = currency;
        }
    }

    public class getCuntryCode
    {
        public string CuntryCode { get; set; }
        public string Row { get; set; }
        public string ID { get; set; }
        public string selectValue { get; set; }
    }
}
