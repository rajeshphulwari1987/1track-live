﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using OneHubServiceRef;
using Business;
using System.Configuration;

public partial class LoyaltyCardInfo : Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    BookingRequestUserControl objBRUC;
    public static string unavailableDates1 = "";
    private Guid siteId;
    public string siteURL;
    public string script;
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            ShowHaveRailPass(siteId);
            Newsletter1.Visible = _masterPage.IsVisibleNewsLetter(siteId);
        }
    }
    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _masterPage.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal", "LoadCal();", true);
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);
            unavailableDates1 = "[";
            if (siteDDates.Count() > 0)
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
        }

        if (!IsPostBack)
        {
            bool isUseSite = (bool)_oWebsitePage.IsUsSite(siteId);
            rdBookingType.Items[1].Text = isUseSite ? "Round Trip" : "Return";
            script = new Masters().GetQubitScriptBySId(siteId);
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);

            for (int j = 10; j >= 0; j--)
            {
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }
            FillPageInfo();
        }
    }

    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        divRailPass.Visible = railPass;
    }

    public void FillPageInfo()
    {
        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            rdBookingType.SelectedValue = objBRUC.Journeytype;
            if (objBRUC.ReturnDate != string.Empty)
            {
                txtReturnDate.Enabled = true;
                reqLoyReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                txtReturnDate.Text = objBRUC.ReturnDate;
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                reqLoyReturnDate.Enabled = true;
            }
            else
            {
                txtReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                txtReturnDate.Text = "";
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                reqLoyReturnDate.Enabled = false;
            }
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            chkLoyalty.Checked = objBRUC.Loyalty;
            chkIhaveRailPass.Checked = objBRUC.isIhaveRailPass;
            chkLoyalty_CheckedChanged(null, null);
        }
    }

    #region Get Loyality List
    protected void ddlAdult_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            BindLoyaltyList();
            FtpCardsDescriptionContent.Visible = true;
        }
        else
        {
            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void ddlChild_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            BindLoyaltyList();
            FtpCardsDescriptionContent.Visible = true;
        }
        else
        {
            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void ddlYouth_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            BindLoyaltyList();
            FtpCardsDescriptionContent.Visible = true;
        }
        else
        {
            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void ddlSenior_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            BindLoyaltyList();
            FtpCardsDescriptionContent.Visible = true;
        }
        else
        {
            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }
    public void BindLoyaltyList()
    {
        List<Passanger> list = new List<Passanger>();
        int cnt = Convert.ToInt32(ddlAdult.SelectedItem.Text.Trim());
        for (int i = 0; i < cnt; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Adult"
            });
        }

        int cntc = Convert.ToInt32(ddlChild.SelectedItem.Text.Trim());
        for (int i = 0; i < cntc; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Child"
            });
        }

        int cnty = Convert.ToInt32(ddlYouth.SelectedItem.Text.Trim());
        for (int i = 0; i < cnty; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Youth"
            });
        }

        int cnts = Convert.ToInt32(ddlSenior.SelectedItem.Text.Trim());
        for (int i = 0; i < cnts; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Senior"
            });
        }
        dtlLoayalty.DataSource = list;
        dtlLoayalty.DataBind();
    }

    public List<Loyaltycard> GetLoyaltycardDetails()
    {
        try
        {
            List<Loyaltycard> list = new List<Loyaltycard>();
            foreach (DataListItem item in dtlLoayalty.Items)
            {
                TextBox txtloyCardNoEuro = (TextBox)item.FindControl("txtloyCardNoEur");
                TextBox txtloyCardNoThy = (TextBox)item.FindControl("txtloyCardNoThy");

                //--Add Eurostar Train Loyalty Card Detalis
                string lEuroCode = txtloyCardNoEuro.Text.Trim();
                if (!String.IsNullOrEmpty(lEuroCode))
                    list.Add(new Loyaltycard { cardnumber = lEuroCode, carrier = new Carrier { code = "EUR" } });

                //--Add Thalys Train Loyalty Card Detalis
                string lThyCode = txtloyCardNoThy.Text.Trim();
                if (!String.IsNullOrEmpty(lThyCode))
                    list.Add(new Loyaltycard { cardnumber = lThyCode, carrier = new Carrier { code = "THA" } });
            }

            #region Validate Card Numbers

            var chkduplicate = list.GroupBy(x => x.cardnumber).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
            if (chkduplicate.Any(x => x.Count > 1))
            {
                list = null;
                ShowMessage(2, "Similar (Thalys or Eurostar) card number is not allowed for another traveller.");
            }
            else if (list.Any(x => x.cardnumber.Length < 16))
            {
                list = null;
                ShowMessage(2, "Invalid card number.");
            }
            else if (list.Where(z => z.carrier.code == "EUR").Any(x => x.cardnumber.Substring(0, 6) != "308381"))
            {
                list = null;
                ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
            }

            else if (list.Where(z => z.carrier.code == "THA").Any(x => x.cardnumber.Substring(0, 6) != "308406"))
            {
                list = null;
                ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
            }

            #endregion

            return list;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            return null;
        }
    }
    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
    protected void chkLoyalty_CheckedChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            BindLoyaltyList();
            FtpCardsDescriptionContent.Visible = true;
            chkIhaveRailPass.Checked = false;
        }
        else
        {

            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()",true);
    }

    protected void chkIhaveRailPass_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIhaveRailPass.Checked)
        {

            chkLoyalty.Checked = false;
            dtlLoayalty.DataSource = null;
            dtlLoayalty.DataBind();
            FtpCardsDescriptionContent.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }
    
    #endregion
    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdBookingType.SelectedValue == "0")
        {
            txtReturnDate.Enabled = false;
            txtReturnDate.Text = "";
            ddlReturnTime.Enabled = false;
            reqLoyReturnDate.Enabled = false;
        }
        else
        {
            txtReturnDate.Enabled = true;
            ddlReturnTime.Enabled = true;
            reqLoyReturnDate.Enabled = true;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "calenable()", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        string stri = "BE";
        try
        {

            Search();

            if (ViewState["depRCode"] != null)
                stri = ViewState["depRCode"].ToString() == "0" ? "BE" : "TI";

            if (chkLoyalty.Checked == true)
                Response.Redirect("TrainResults.aspx?req=" + stri);
            else
                Response.Redirect("TrainResults.aspx?req=" + stri);

        }
        catch
        {

        }
    }
    protected void Search()
    {
        try
        {
            if (!String.IsNullOrEmpty(txtReturnDate.Text))
                if (DateTime.ParseExact(txtReturnDate.Text, "dd/MMM/yyyy", null) < DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCa1l", "alert('Return Date should be greater than the departure Date')", true);
                    return;
                }

            #region Alert message
            var stList = _db.StationNameLists.Where(x => (x.StationName == txtFrom.Text.Trim() || x.StationName == txtTo.Text.Trim()) && x.IsUK).FirstOrDefault();
            var isUK = stList != null && stList.IsUK;

            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue == "0")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCa1l", "alert('Please enter at least 1 adult, senior or junior(youth) passenger.')", true);
                return;
            }
            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue != "0" && isUK)
            {
                mdPassengerUK.Show();
                return;
            }
            else if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue != "0" && !isUK)
            {
                mdPassenger.Show();
                return;
            }

            int totalAdult = Convert.ToInt32(ddlAdult.SelectedValue) * 4;
            int totalYouth = Convert.ToInt32(ddlYouth.SelectedValue) * 4;
            int totalSenior = Convert.ToInt32(ddlSenior.SelectedValue) * 4;
            int totalChilden = totalAdult + totalYouth + totalSenior;
            if (Convert.ToInt32(ddlChild.SelectedValue) > totalChilden && isUK)
            {
                mdPassengerUK.Show();
                return;
            }
            else if (Convert.ToInt32(ddlChild.SelectedValue) > totalChilden && !isUK)
            {
                mdPassenger.Show();
                return;
            }
            #endregion

            BookingRequestUserControl objBruc = new BookingRequestUserControl();
            ManageBooking objBooking = new ManageBooking();

            objBruc.FromDetail = txtFrom.Text.Trim();
            objBruc.ToDetail = txtTo.Text.Trim();
            objBruc.depdt = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null);
            objBruc.depTime = Convert.ToDateTime(ddldepTime.SelectedValue);

            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(txtFrom.Text.Trim());
            objBruc.depstCode = objStationDeptDetail != null ? objStationDeptDetail.StationCode : string.Empty;
            objBruc.depRCode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
            ViewState["depRCode"] = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";

            GetStationDetailsByStationName objStationArrDetail = objBooking.GetStationDetailsByStationName(txtTo.Text.Trim());
            objBruc.arrstCode = objStationArrDetail != null ? objStationArrDetail.StationCode : string.Empty;
            objBruc.arrRCode = objStationArrDetail != null ? objStationArrDetail.RailwayCode : "0";
            objBruc.OneHubServiceName = objStationDeptDetail.RailName.Trim() == "BENE" ? "BeNe" : "Trenitalia";

            objBruc.ClassValue = Convert.ToInt32(ddlClass.SelectedValue);
            objBruc.Adults = Convert.ToInt32(ddlAdult.SelectedValue);
            objBruc.Boys = Convert.ToInt32(ddlChild.SelectedValue);
            objBruc.Seniors = 0;
            objBruc.Youths = 0;
            objBruc.Transfare = Convert.ToInt32(ddlTransfer.SelectedValue);
            objBruc.ReturnDate = String.IsNullOrEmpty(txtReturnDate.Text) || txtReturnDate.Text.Trim() == "DD/MM/YYYY" ? string.Empty : txtReturnDate.Text;
            objBruc.ReturnTime = ddlReturnTime.SelectedValue;
            objBruc.Loyalty = chkLoyalty.Checked;
            objBruc.isIhaveRailPass = chkIhaveRailPass.Checked;
            objBruc.Journeytype = rdBookingType.SelectedValue;

            if (rdBookingType.SelectedValue == "1")
                objBruc.IsReturnJurney = true;

            if (chkLoyalty.Checked == true)
                objBruc.lstLoyalty = GetLoyaltycardDetails();

            Session["BookingUCRerq"] = objBruc;
            var currDate = DateTime.Now;
            int daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);
            var maxDate = currDate.AddDays(daysLimit - 1);

            if (objBruc.depdt > maxDate)
                return;

            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            TrainInformationRequest request = TrainInformation(objBruc, 1);
            if (Convert.ToInt32(rdBookingType.SelectedValue) > 0 && Convert.ToInt32(objBruc.depRCode) > 0)
                request.IsReturnJourney = false;
            else if (Convert.ToInt32(rdBookingType.SelectedValue) > 0)
                request.IsReturnJourney = true;

            TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);
            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
            {
                //--TreniItalia Search return request                
                if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                {
                    List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    request = TrainInformation(objBruc, 2);
                    request.IsReturnJourney = true;
                    TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                    List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    list.AddRange(listReturn);
                    if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                    {
                        List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                        List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                        List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                        pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();

                    }
                }

            }
            Session["TrainSearch"] = pInfoSolutionsResponse;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GetRequest(BookingRequestUserControl request)
    {

        TrainInformationRequest objrequest = new TrainInformationRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            DepartureRailwayCode = request.depRCode,
            DepartureStationCode = request.depstCode,
            ArrivalRailwayCode = request.arrRCode,
            ArrivalStationCode = request.arrstCode,
            Class = request.ClassValue,
            NumAdults = request.Adults,
            NumBoys = request.Boys,
            NumSeniors = request.Seniors,
            NumYouths = request.Youths,
            NumberOfTransfare = request.Transfare,
            IsHaveRailPass = request.isIhaveRailPass,
            IsReturnJourney = request.IsReturnJurney,
            DepartureDate = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null),
            DepartureTime = Convert.ToDateTime(ddldepTime.SelectedValue)
        };
        if (chkLoyalty.Checked)
        {
            objrequest.Loyaltycards = GetLoyaltycardDetails().ToArray();
            objrequest.IsRequiredLoyaltyCard = true;
        }
        if (!string.IsNullOrEmpty(txtReturnDate.Text) && !txtReturnDate.Text.Contains("DD"))
        {
            objrequest.ArrivalDate = DateTime.ParseExact(txtReturnDate.Text, "dd/MMM/yyyy", null);
            objrequest.ArrivalTime = Convert.ToDateTime(ddlReturnTime.SelectedValue);
        }
        Session["TrainSearchRequest"] = objrequest;

    }
    public TrainInformationRequest TrainInformation(BookingRequestUserControl request, int flag)
    {
        GetRequest(request);

        TrainInformationRequest objrequest = new TrainInformationRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            DepartureRailwayCode = flag == 1 ? request.depRCode : request.arrRCode,
            DepartureStationCode = flag == 1 ? request.depstCode : request.arrstCode,
            ArrivalRailwayCode = flag == 1 ? request.arrRCode : request.depRCode,
            ArrivalStationCode = flag == 1 ? request.arrstCode : request.depstCode,

            Class = request.ClassValue,
            NumAdults = request.Adults,
            NumBoys = request.Boys,
            NumSeniors = request.Seniors,
            NumYouths = request.Youths,
            NumberOfTransfare = request.Transfare,
            IsHaveRailPass = request.isIhaveRailPass,
            IsReturnJourney = request.IsReturnJurney
        };

        //--Departure IF BENE
        objrequest.DepartureDate = request.depdt;
        objrequest.DepartureTime = request.depTime;
        if (request.ReturnDate != string.Empty && flag == 1)
        {
            objrequest.IsReturnJourney = true;
            objrequest.ArrivalDate = DateTime.ParseExact(request.ReturnDate, "dd/MMM/yyyy", null);
            objrequest.ArrivalTime = Convert.ToDateTime(request.ReturnTime);
        }

        //-Return For TI
        if (request.ReturnDate != string.Empty && flag == 2)
        {
            objrequest.IsReturnJourney = true;
            objrequest.DepartureDate = DateTime.ParseExact(request.ReturnDate, "dd/MMM/yyyy", null);
            objrequest.DepartureTime = Convert.ToDateTime(request.ReturnTime);
        }
        if (chkLoyalty.Checked)
        {
            objrequest.Loyaltycards = GetLoyaltycardDetails().ToArray();
            objrequest.IsRequiredLoyaltyCard = true;
        }
        return objrequest;
    }



}