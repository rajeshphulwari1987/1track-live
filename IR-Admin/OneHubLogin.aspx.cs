﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class OneHubLogin : Page
    {
        readonly ManageUser _mu = new ManageUser();
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            ddlSite.SelectedValue = _siteID.ToString();
            FillOneHubLoginCredential(_siteID);
        }
        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                ddlSite.SelectedValue = _siteID.ToString();
                FillOneHubLoginCredential(_siteID);
                ViewState["PreSiteID"] = _siteID;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _siteID = Master.SiteID;
                FillCommonddl(_siteID);
                FillOneHubLoginCredential(_siteID);
            }
        }

        public void FillCommonddl(Guid _siteID)
        {
            try
            {
                ddlSite.DataSource = _master.GetActiveSiteList();
                ddlSite.DataTextField = "DisplayName";
                ddlSite.DataValueField = "ID";
                ddlSite.DataBind(); 
                ddlSite.SelectedValue = _siteID.ToString();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        private void FillOneHubLoginCredential(Guid _siteID)
        {
            grvOneHubPrd.DataSource = _mu.GetApiLogingDetail(_siteID).Where(x => x.IsProductionAccnt == true).ToList();
            grvOneHubPrd.DataBind();

            grdOneHubTest.DataSource = _mu.GetApiLogingDetail(_siteID).Where(x => x.IsProductionAccnt != true).ToList();
            grdOneHubTest.DataBind();
        }

        protected void grdOneHubTest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    if (ddlSite.SelectedValue != "-1")
                        _siteID = Guid.Parse(ddlSite.SelectedValue);

                    string[] str = e.CommandArgument.ToString().Split(',');
                    int apiAccountID = int.Parse(str[0]);
                    string trainName = str[1];

                    _mu.ActiveInActiveApiLogingDetail(new tblApiLoginDetailSiteLookup { ApiAccountID = apiAccountID, SiteID = _siteID, TrainName = trainName });
                    FillOneHubLoginCredential(_siteID);
                }

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grvOneHubPrd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    if (ddlSite.SelectedValue != "-1")
                        _siteID = Guid.Parse(ddlSite.SelectedValue);

                    string[] str = e.CommandArgument.ToString().Split(',');
                    int apiAccountID = int.Parse(str[0]);
                    string trainName = str[1];

                    _mu.ActiveInActiveApiLogingDetail(new tblApiLoginDetailSiteLookup { ApiAccountID = apiAccountID, SiteID = _siteID, TrainName = trainName });
                    FillOneHubLoginCredential(_siteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }



        protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSite.SelectedValue != "-1")
                FillOneHubLoginCredential(Guid.Parse(ddlSite.SelectedValue));

        }
    }
}