﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI;

namespace IR_Admin
{
    public partial class Traveler : Page
    {
        readonly private Masters _oMasters = new Masters();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            tab = "1";
            if (ViewState["tab"] != null)
                tab = ViewState["tab"].ToString();

            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(hdnId.Value))
                {
                    ViewState["tab"] = "2";
                    tab = "2";
                }
                FillDropDown();
                FillGrid();
            }
        }

        void FillDropDown()
        {
            ddlAgeFrom.Items.Insert(0, new ListItem("--Age From--", "-1"));
            ddlAgeTo.Items.Insert(0, new ListItem("--Age To--", "-1"));
            for (int i = 0; i <= 100; i++)
            {
                ddlAgeFrom.Items.Insert(i + 1, new ListItem(i.ToString(), i.ToString()));
                ddlAgeTo.Items.Insert(i + 1, new ListItem(i.ToString(), i.ToString()));
            }
        }
        void FillGrid()
        {
            var list = _oMasters.GetTravelerList();
            if (list != null && list.Count > 0)
            { grvTraveler.DataSource = list; } grvTraveler.DataBind();
        }

        protected void grvTraveler_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Modify")
                {
                    btnSubmit.Text = "Update";
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var ocls = _oMasters.GetTravelerById(id);
                    txtName.Text = ocls.Name;
                    ddlAgeFrom.SelectedValue = ocls.FromAge.ToString();
                    txtEurailCode.Text = ocls.EurailCode.ToString();
                    ddlAgeTo.SelectedValue = ocls.ToAge.ToString();
                    chkactive.Checked = ocls.IsActive;
                    txtRank.Text = ocls.SortOrder.ToString();
                    hdnId.Value = id.ToString();
                    txtNote.Text = ocls.Note != null ? ocls.Note.Trim() : "";
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    bool result = _oMasters.DeleteTraveler(id);
                    if (result)
                        ShowMessage(1, "Record deleted successfully");
                    FillGrid();
                    tab = "1";
                    ViewState["tab"] = "1";
                }
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _oMasters.ActiveInactiveTraveler(id);
                    FillGrid(); tab = "1"; ViewState["tab"] = "1";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Guid id = _oMasters.AddTraveler(new tblTravelerMst
                  {
                      ID = !String.IsNullOrEmpty(hdnId.Value) ? Guid.Parse(hdnId.Value) : new Guid(),
                      CreatedBy = AdminuserInfo.UserID,
                      CreatedOn = DateTime.Now,
                      FromAge = byte.Parse(ddlAgeFrom.SelectedValue),
                      ToAge = byte.Parse(ddlAgeTo.SelectedValue),
                      EurailCode = Convert.ToInt32(txtEurailCode.Text),
                      Name = txtName.Text.Trim(),
                      IsActive = chkactive.Checked,
                      SortOrder = Convert.ToInt32(txtRank.Text),
                      Note = txtNote.Text.Trim()
                  });
                FillGrid();
                if (id!=new Guid())
                    ShowMessage(1, !String.IsNullOrEmpty(hdnId.Value) ? "Traveler details updated successfully" : "Traveler details added successfully");
                else
                    ShowMessage(1, "Operation Failed");
                ClearControls();
                tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Traveler.aspx");
        }

        private void ClearControls()
        {
            hdnId.Value = string.Empty;
            txtName.Text = string.Empty;
            txtNote.Text = string.Empty;
            txtEurailCode.Text = string.Empty;
            txtRank.Text = string.Empty;
            ddlAgeFrom.SelectedIndex = 0;
            ddlAgeTo.SelectedIndex = 0;
            chkactive.Checked = false;
            btnSubmit.Text = "Submit";
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grvTraveler_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvTraveler.PageIndex = e.NewPageIndex;
            FillGrid(); tab = "1";
            ViewState["tab"] = "1";
        }
    }
}