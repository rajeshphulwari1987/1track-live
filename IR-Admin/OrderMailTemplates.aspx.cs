﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;

namespace IR_Admin
{
    public partial class OrderMailTemplates : System.Web.UI.Page
    {
        readonly ManageEmailTemplate _mailtemplate = new ManageEmailTemplate();
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                tab = "1";
                if (!Page.IsPostBack)
                {
                    GetTemplateList();
                    BindSite();
                    BindMailTemplates();
                }
                ShowMessage(0, null);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void BindMailTemplates()
        {
            try
            {
                tab = "1";
                grdMailTemplete.DataSource = _mailtemplate.GetMailTemplatesWithSiteName().ToList();
                grdMailTemplete.DataBind();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void GetTemplateList()
        {
            try
            {
                ddltemplete.DataSource = _mailtemplate.GetMailTemplates().ToList();
                ddltemplete.DataTextField = "MailTemplateName";
                ddltemplete.DataValueField = "Id";
                ddltemplete.DataBind();
                ddltemplete.Items.Insert(0, new ListItem("Select Template", "0"));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void GetMailTemplateSiteLookUpBySiteId(Guid siteid)
        {
            try
            {
                var result = _mailtemplate.GetMailTemplateSiteLookUpBySiteId(siteid);
                if (result != null)
                {
                    ddltemplete.SelectedValue = result.MailTemplateId.ToString();
                    chksite.SelectedValue = result.SiteId.ToString();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                List<MailTemplate> tempList = new List<MailTemplate>();
                for (int i = 0; i < chksite.Items.Count; i++)
                {
                    if (chksite.Items[i].Selected)
                    {
                        _mailtemplate.deleteTemplatesFromLookUp(Guid.Parse(chksite.Items[i].Value));
                        tempList.Add(new MailTemplate()
                        {
                            MailTemplateId = Guid.Parse(ddltemplete.SelectedValue),
                            SiteId = Guid.Parse(chksite.Items[i].Value)
                        });
                    }
                }
                _mailtemplate.AddEditTemplate(tempList);
                BindMailTemplates();
                ClearControls();
                tab = "1";
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        void ClearControls()
        {
            ddltemplete.SelectedIndex = 0;
            tab = "1";
            for (int i = 0; i < chksite.Items.Count; i++)
            {
                if (chksite.Items[i].Selected)
                {
                    chksite.Items[i].Selected = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderMailTemplates.aspx");
        }

        protected void grdMailTemplete_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _mailtemplate.deleteTemplatesFromLookUp(id);
                    ShowMessage(1, "record deleted successfully.");
                }
                BindMailTemplates();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void grdMailTemplete_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdMailTemplete.PageIndex = e.NewPageIndex;
                BindMailTemplates();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void BindSite()
        {
            try
            {
                IEnumerable<tblSite> objSite = _Master.GetActiveSiteList();
                chksite.DataSource = objSite.ToList();
                chksite.DataTextField = "DisplayName";
                chksite.DataValueField = "ID";
                chksite.DataBind();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void FillSiteData()
        {
            try
            {
                var data = _mailtemplate.GetMailTemplateSiteLookUpByTemplateId(Guid.Parse(ddltemplete.SelectedValue)).ToList();
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        for (int i = 0; i < chksite.Items.Count; i++)
                        {
                            if (item.SiteId.ToString() == chksite.Items[i].Value)
                                chksite.Items[i].Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void ddltemplete_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < chksite.Items.Count; i++)
                {
                    if (chksite.Items[i].Selected == true)
                        chksite.Items[i].Selected = false;
                }
                if (ddltemplete.SelectedIndex > 0)
                    FillSiteData();
                tab = "2";
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }
}