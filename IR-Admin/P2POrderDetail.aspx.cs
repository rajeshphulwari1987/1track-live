﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class P2POrderDetail : Page
    {
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        readonly ManageTrainDetails _master = new ManageTrainDetails();
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
        public static List<getRailPassData> list = new List<getRailPassData>();
        public string currency;
        readonly CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        Guid siteId;
        public string curID;
        public string script = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
            FillOrderGrid();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                siteId = Master.SiteID;
                if (Session["P2POrderID"] == null)
                    Response.Redirect("OrderDetails.aspx");
                FillOrderGrid();
            }
        }

        public void FillOrderGrid()
        {
            if (Session["P2POrderID"] != null)
            {
                var lst = new ManageBooking().GetP2PSaleDetail(Convert.ToInt64(Session["P2POrderID"]));
                rptPassDetail.DataSource = lst.OrderBy(x => x.ShortOrder);
                rptPassDetail.DataBind();
            }
        }

        protected void rptPassDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString().Trim());
                    var obj = _db.tblPassP2PSalelookup.FirstOrDefault(a => a.ID == id);
                    if (obj != null)
                    {
                        _db.tblPassP2PSalelookup.DeleteObject(obj);
                        _db.SaveChanges();
                    }
                    FillOrderGrid();
                }
            }
            catch (Exception ex) { }
        }

        protected void rptPassDetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var ddlTitle = e.Item.FindControl("ddlTitle") as DropDownList;
                var hdnTitle = e.Item.FindControl("hdnTitle") as HiddenField;
                var hdnCountry = e.Item.FindControl("hdnCountry") as HiddenField;

                var ddlCountry = e.Item.FindControl("ddlCountry") as DropDownList;
                if (ddlCountry != null)
                {
                    ddlCountry.DataSource = _master.GetCountryDetail();
                    ddlCountry.DataValueField = "CountryID";
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataBind();
                    ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
                }

                if (hdnTitle != null && hdnTitle.Value.Trim() != "")
                    if (ddlTitle != null) ddlTitle.SelectedValue = hdnTitle.Value.Trim();
                if (hdnCountry != null && hdnCountry.Value.Trim() != "")
                    if (ddlCountry != null) ddlCountry.SelectedValue = hdnCountry.Value.Trim();
            }
        }

        public void GetCurrencyCode()
        {
            var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (currencyID != null)
            {
                curID = currencyID.DefaultCurrencyID.ToString();
                currency = oManageClass.GetCurrency(Guid.Parse(curID));
            }
        }

        protected void btnChkOut_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (RepeaterItem eItem in rptPassDetail.Items)
                {
                    var ddlTitle = eItem.FindControl("ddlTitle") as DropDownList;
                    var ddlCountry = eItem.FindControl("ddlCountry") as DropDownList;
                    var txtFirst = eItem.FindControl("txtFirstName") as TextBox;
                    var txtLast = eItem.FindControl("txtLastName") as TextBox;
                    var hdnPassP2PSaleLookupId = eItem.FindControl("hdnPassP2PSaleLookupId") as HiddenField;

                    var obj = new tblOrderTraveller();
                    if (ddlTitle != null) obj.Title = ddlTitle.SelectedValue;
                    if (ddlCountry != null) obj.Country =ddlCountry.SelectedValue =="0"?Guid.Empty: Guid.Parse(ddlCountry.SelectedValue);
                    if (txtFirst != null) obj.FirstName = txtFirst.Text;
                    if (txtLast != null) obj.LastName = txtLast.Text;
                    obj.LeadPassenger = true;
                    if (hdnPassP2PSaleLookupId != null)
                        new ManageBooking().AddEditLeadPassengerByPassP2PSaleId(Guid.Parse(hdnPassP2PSaleLookupId.Value.Trim()), obj);

                    /*Update siteID*/
                    if (Session["P2POrderID"] == null)
                    {
                        var ordID = Convert.ToInt64(Session["P2POrderID"]);
                        new ManageBooking().UpdateSiteID(siteId, ordID);
                    }
                }
                Response.Redirect("P2PBookingCart.aspx");
            }
            catch (Exception ex) { }
        }
    }
}