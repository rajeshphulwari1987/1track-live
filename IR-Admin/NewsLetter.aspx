﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewsLetter.aspx.cs" Inherits="IR_Admin.NewsLetter" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
  <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript">
    function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }
    $(window).load(function () {
        setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
        setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
    });
    $(document).ready(function () {
        if ('<%=tab.ToString()%>' == '1') {
            document.getElementById('<%= hdnID.ClientID %>').value = '';
            document.getElementById('MainContent_divlist').style.display = 'Block';
            document.getElementById('MainContent_divNew').style.display = 'none';
        }
        else {
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }

        $(".list a").click(function (e) {
            document.getElementById('<%= hdnID.ClientID %>').value = '';
            $('input:text').val('');
            if ($(this).attr("id") == 'aList') {
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }
        });

        function clearControls() {
            document.getElementById('MainContent_txtCode').value = '';
            document.getElementById('MainContent_txtName').value = '';
            document.getElementById('MainContent_divlist').value = '';
            document.getElementById('MainContent_divlist').value = '';

        }
    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function ResetDiv() {
        document.getElementById('MainContent_aNew').className = 'current';
        document.getElementById('aList').className = ' ';
        document.getElementById('MainContent_divlist').style.display = 'none';
        document.getElementById('MainContent_divNew').style.display = 'Block';
    }

    function dateChangedStart(sender, args) {
        var selectedDate = sender.get_selectedDate();
        var hiddenStart = $get("MainContent_txtStartDateHidden");
        var validator = $get("MainContent_startDateCompareValidator");
        hiddenStart.value = dateToString(selectedDate);
        ValidatorValidate(validator);
    }
    function dateChangedEnd(sender, args) {
        var selectedDate = sender.get_selectedDate();
        var hiddenEnd = $get("MainContent_txtEndDateHidden");
        var validator = $get("MainContent_startDateCompareValidator");
        hiddenEnd.value = dateToString(selectedDate);
        ValidatorValidate(validator);
    }
    function dateToString(d) {
        var year = d.getFullYear();
        var month = d.getMonth() + 1; //months are zero based
        var day = d.getDate();
        return year + "/" + month + "/" + day;
    }

</script>
<style type="text/css">
    .hidden
    {
        display:none;   
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:HiddenField ID="hdnID" runat="server" />
<h2>
      News Letter</h2>
      <div class="full mr-tp1">
      <ul class="list">
            <li id="lilist"><a id="aList" href="#" class="current">List</a></li>
            <li  id="linew" >
                <asp:HyperLink ID="aNew" CssClass=" "  NavigateUrl="#" runat="server">New/Edit</asp:HyperLink>
            </li>
        </ul>
         <asp:Panel ID="pnlErrSuccess" runat="server" >
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top:24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;margin-top:24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
         <div class="full mr-tp1">
            <div class="panes">
             <div id="divlist" runat="server" >
                    <div class="crushGvDiv">
                      <asp:GridView ID="grdNewsLetter" runat="server" CellPadding="4" DataKeyNames="ID"
                        CssClass="grid-head2"  ForeColor="#333333"
                        GridLines="None" AutoGenerateColumns="False" AllowPaging="True" >
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Title" >
                             <ItemTemplate>
                                <%#Eval("Title")%>
                             </ItemTemplate>
                             </asp:TemplateField>
                               <%--<asp:TemplateField HeaderText="Content" >
                             <ItemTemplate>
                             <div >
                             <%#Eval("NewsLatterContent")%>
                          <%-- <asp:Label ID="lblContent" runat="server" Text='<%# TruncateContent(Eval("NewsLatterContent")) %>'></asp:Label>--%>
                        <%--   </div>--%>
                            <%-- </ItemTemplate>
                             </asp:TemplateField>--%>
                              <asp:TemplateField HeaderText="Start Date" >
                             <ItemTemplate>
                             <%# Eval("StartDate", "{0:MM/dd/yyyy}")%>
                             </ItemTemplate>
                             </asp:TemplateField>
                               <asp:TemplateField HeaderText="End Date" >
                             <ItemTemplate>
                             <%# Eval("EndDate", "{0:MM/dd/yyyy}")%>
                             </ItemTemplate>
                             </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%# Eval("ID") %>'
                                        CommandName="Remove" ImageUrl="images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%# Eval("ID") %>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                     </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: none;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                               Title
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtTitleRequiredFieldValidator" runat="server" ControlToValidate="txtTitle"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="NewsForm"></asp:RequiredFieldValidator>
                            </td>
                            <td class="col">
                              
                            </td>
                            <td class="col">
                              
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                              Start Date
                            </td>
                            <td class="col">
                      <asp:TextBox ID="txtStartDate" CausesValidation="false" ReadOnly="true" runat="server">
    </asp:TextBox>
     <asp:RequiredFieldValidator ID="txtStartDateFieldValidator" runat="server" ControlToValidate="txtStartDate"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="NewsForm"></asp:RequiredFieldValidator>

    <asp:TextBox ID="txtStartDateHidden" CssClass="hidden" ValidationGroup="NewsForm" CausesValidation="true" ReadOnly="false" runat="server">
    </asp:TextBox>
    <ajax:CalendarExtender ID="StartDateCalendar" TargetControlID="txtStartDate" runat="server"
        OnClientDateSelectionChanged="dateChangedStart"
        Format="MM/dd/yyyy">
    </ajax:CalendarExtender>
    <asp:CompareValidator ID="startDateCompareValidator" runat="server" EnableClientScript="true" CssClass="valdreq"
        ControlToValidate="txtStartDateHidden" Display="Static" Operator="LessThanEqual" ValidationGroup="DateCheck"
        ControlToCompare="txtEndDateHidden" Enabled="true" Type="Date" Text="Startdate should be <= enddate">
    </asp:CompareValidator>
    
                            </td>
                            <td class="col">
                               End Date
                            </td>
                            <td class="col">
                          <asp:TextBox ID="TxtEndDate" CausesValidation="false" ReadOnly="true" runat="server">  </asp:TextBox>
                           <asp:RequiredFieldValidator ID="TxtEndDateFieldValidator" runat="server" ControlToValidate="txtStartDate"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="NewsForm"></asp:RequiredFieldValidator>
  
    <asp:TextBox ID="txtEndDateHidden" CssClass="hidden" ValidationGroup="NewsForm" CausesValidation="true" ReadOnly="false" runat="server">
    </asp:TextBox>
    <ajax:CalendarExtender ID="EndDateCalendar" TargetControlID="txtEndDate" runat="server"
        OnClientDateSelectionChanged="dateChangedEnd"
        Format="MM/dd/yyyy">
    </ajax:CalendarExtender>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkactive" runat="server" />
                            </td>
                            <td class="col">
                            </td>
                            <td class="col">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Description
                            </td>
                            <td colspan="4">
                                <textarea id="txtContent" name="content" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click" 
                                    Text="Submit" Width="89px" ValidationGroup="NewsForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" 
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
