﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using IR_Admin.OneHubServiceRef;

namespace IR_Admin
{
    public partial class OfferCodes : Page
    {
        readonly OneHubRailOneHubClient _clientCode = new OneHubRailOneHubClient();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindOffers();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                BindOffers();
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _siteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                BindOffers();
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindOffers()
        {
            var result = _clientCode.GetOfferDescription();
            grdOffers.DataSource = result;
            grdOffers.DataBind();
        }

        protected void grdOffers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "FareRules")
            {
                ShowMessage(0, null);
                var gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                var index = gvRow.RowIndex;
                var divFareRules = (HtmlGenericControl)grdOffers.Rows[index].FindControl("divFareRules");
                divFareRules.Visible = true;
            }
            if (e.CommandName == "Add")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                var gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                var index = gvRow.RowIndex;

                var txtFRules = (HtmlTextArea)grdOffers.Rows[index].FindControl("txtFRules");
                if (txtFRules != null)
                {
                    var offer = new OfferDescription { Id = id, FareRules = txtFRules.InnerHtml };
                    _clientCode.UpdateFareRules(offer);
                    BindOffers();
                    DivSuccess.Style.Add("display", "block");
                    ShowMessage(1, "Fare Rules have been saved successfully!");
                }
            }
            if (e.CommandName == "Close")
            {
                ShowMessage(0, null);
                var gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                var index = gvRow.RowIndex;
                var divFareRules = (HtmlGenericControl)grdOffers.Rows[index].FindControl("divFareRules");
                divFareRules.Visible = false;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}