﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="newsletter.ascx.cs" Inherits="newsletter" %>

    <style type="text/css">
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".clsEmail").keyup(function () {
                document.getElementById("divNews").style.display = 'block';
            });
        });
    </script>
    
<div class="newsletter-outer">
    <div class="title"> Newsletter Signup </div>
     <div class="newsletter-inner">
              <asp:TextBox ID="txtEmail" runat="server" CssClass="clsEmail" AutoComplete="off"/>
              <asp:RegularExpressionValidator ID="valRegExEmail" runat="server" ControlToValidate="txtEmail"
                   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Email."
                   Display="dynamic" ValidationGroup="vcNews" CssClass="clsfont" ForeColor="Red"/>
                <asp:RequiredFieldValidator ID="rfEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ForeColor="Red" ValidationGroup="vcNews"/>
              <div id="divNews" style="display:none" class="clsfont">
                First Name: 
                <asp:TextBox ID="txtName" runat="server" style="width:167px" AutoComplete="off"/>
                <asp:RequiredFieldValidator ID="rfNm" runat="server" ControlToValidate="txtName" ErrorMessage="*" ForeColor="Red" ValidationGroup="vcNews"/>
                <div>Please select the areas you are interested in:
                <asp:TreeView ID="trNews" runat="server" ShowCheckBoxes="All" CssClass="trNews">
                     <NodeStyle ChildNodesPadding="5px" />
                </asp:TreeView>
                </div>
              </div>
                <asp:Button ID="btnNews" Text="SUBMIT" class="w92 f-right" runat="server" 
                    OnClick="btnNews_Click" ValidationGroup="vcNews"/>
            </div>
    <img src='<%=siteURL%>images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
</div>
