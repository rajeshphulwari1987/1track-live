﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class BranchesAddEdit : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!IsPostBack)
            {
                FillCountryDDl();
                FillMenu();
                /*FillAgentPhone*/
                txtTelephone.Text = _Master.GetAgentOfficePhone(AdminuserInfo.UserID);
                if (Request.QueryString["treeid"] != null)
                {
                    btnUpdate.Text = "Update";
                    var result = _Master.BranchListforEdit(Convert.ToInt32(Request.QueryString["treeid"]));
                    lblBranchFullPath.Text = result.PathName;
                }
                if (Request.QueryString["id"] != null && Request.QueryString["treeid"] == null)
                {
                    btnUpdate.Text = "Update";
                    BindBranchListForEdit(Convert.ToInt32(Request.QueryString["id"]));
                    var result = _Master.BranchListforEdit(Convert.ToInt32(Request.QueryString["id"]));
                    lblBranchFullPath.Text = result.PathName;
                }
                else if (Request.QueryString["treeid"] == null && Request.QueryString["treeid"] == null)
                {
                    btnUpdate.Text = "Save";
                    lblBranchFullPath.Text = "Add New Office";
                }
                if (Request.QueryString["id"] != null)
                    GetUserListByTreeId(Convert.ToInt32(Request.QueryString["id"]));
            }
        }

        public void FillCountryDDl()
        {
            try
            {
                ddlCountry.DataSource = _Master.GetCountryList();
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("--Country--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void FillMenu()
        {
            trSites.Nodes.Clear();
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    tab = "2";
                    return;
                }

                var _Branch = new Branch
                {
                    OfficeName = txtOname.Text,
                    Title = txtTitle.Text,
                    FirstName = txtfName.Text,
                    LastName = txtlName.Text,
                    Address1 = txtAddress1.Text,
                    Address2 = txtAddress2.Text,
                    Town = txtTown.Text,
                    County = txtCounty.Text,
                    Country = Guid.Parse(ddlCountry.SelectedValue),
                    Postcode = txtPostCode.Text,
                    Telephone = txtTelephone.Text,
                    Email = txtEmail.Text,
                    SecondaryEmail = txtSecondaryEmail.Text,
                    chkEmail = chkEmail.Checked,
                    chkSecondaryEmail = chkSecondaryEmail.Checked,
                    IsActive = chkactive.Checked,
                    IsVirtualOffice = chkVirtual.Checked
                };

                if (btnUpdate.Text == "Update" && Request.QueryString["id"] != null && Request.QueryString["id"] != string.Empty)
                    _Branch.TreeID = Convert.ToInt32(Request.QueryString["id"]);
                else if (btnUpdate.Text == "Update" && Request.QueryString["treeid"] != null && Request.QueryString["treeid"] != string.Empty)
                {
                    _Branch.ID = Guid.NewGuid();
                    _Branch.TreeParentBranchID = Convert.ToInt32(Request.QueryString["treeid"]);
                    _Branch.ParentBranchID = _Master.GetBranchParentID(Convert.ToInt32(Request.QueryString["treeid"]));
                }
                else
                {
                    _Branch.ID = Guid.NewGuid();
                    if (Request.QueryString["id"] != null && Request.QueryString["id"] != string.Empty)
                    {
                        _Branch.TreeParentBranchID = Convert.ToInt32(Request.QueryString["id"]);
                        _Branch.ParentBranchID = _Master.GetBranchParentID(Convert.ToInt32(Request.QueryString["id"]));
                    }
                    else
                    {
                        _Branch.ParentBranchID = Guid.NewGuid();
                    }
                }

                if (tn == 0)
                    ShowMessage(2, "Please Select Site");
                else
                {
                    var res = _Master.AddBranch(_Branch);
                    _db.tblBranchLookupSites.Where(w => w.BranchID == res).ToList().ForEach(_db.tblBranchLookupSites.DeleteObject);
                    _db.SaveChanges();

                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            var branchLookSites = new tblBranchLookupSite { ID = Guid.NewGuid(), BranchID = res, SiteID = Guid.Parse(node.Value) };
                            _Master.AddBranchLookupSites(branchLookSites);
                        }
                    }

                    string message = btnUpdate.Text == "Update" ? "You have successfully Updated" : "You have successfully added";
                    ShowMessage(1, message);
                    ClearControls();
                    Response.Redirect("Branches.aspx");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void ClearControls()
        {
            txtOname.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtCounty.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtSecondaryEmail.Text = string.Empty;
            txtfName.Text = string.Empty;
            txtPostCode.Text = string.Empty;
            txtTitle.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            txtTown.Text = string.Empty;
            chkactive.Checked = false;
            chkEmail.Checked = false;
            chkSecondaryEmail.Checked = false;
            ddlCountry.ClearSelection();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Branches.aspx");
        }

        public void BindBranchListForEdit(int ID)
        {
            try
            {
                btnUpdate.Text = "Update";
                var result = _Master.BranchListforEdit(ID);
                lblBranchFullPath.Text = result.PathName;
                txtOname.Text = result.OfficeName;
                txtTitle.Text = result.Title;
                txtfName.Text = result.FirstName;
                txtlName.Text = result.LastName;
                txtAddress1.Text = result.Address1;
                txtAddress2.Text = result.Address2;
                txtTown.Text = result.Town;
                txtCounty.Text = result.County;
                ddlCountry.ClearSelection();
                if (ddlCountry.Items.FindByValue(result.Country.ToString()) != null)
                {
                    ddlCountry.Items.FindByValue(result.Country.ToString()).Selected = true;
                }
                txtPostCode.Text = result.Postcode;
                txtTelephone.Text = result.Telephone;
                txtEmail.Text = result.Email;
                txtSecondaryEmail.Text = result.SecondaryEmail;
                chkEmail.Checked = result.chkEmail == true;
                chkSecondaryEmail.Checked = result.chkSecondaryEmail == true;
                chkactive.Checked = result.IsActive == true;
                chkVirtual.Checked = result.IsVirtualOffice;
                // Look up  Sites
                var branchId = _db.tblBranches.FirstOrDefault(x => x.TreeID == ID).ID;
                var lookUpSites = _db.tblBranchLookupSites.Where(x => x.BranchID == branchId).ToList();
                foreach (var lsites in lookUpSites)
                {
                    foreach (TreeNode pitem in trSites.Nodes)
                    {
                        if (Guid.Parse(pitem.Value) == lsites.SiteID)
                            pitem.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        private void GetUserListByTreeId(int TreeId)
        {
            try
            {
                Guid branchId = _db.tblBranches.FirstOrDefault(x => x.TreeID == TreeId).ID;
                if (branchId != null)
                {
                    var result = (from tb in _db.tblBranches
                                  join tau in _db.tblAdminUsers on tb.ID equals tau.BranchID
                                  where tb.ID == branchId
                                  select new { tau }).AsEnumerable().Select(x => new
                                  {
                                      Name = x.tau.UserName
                                  }).OrderBy(x => x.Name).ToList();
                    rptUserList.DataSource = result;
                    rptUserList.DataBind();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }
}