﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CountryDetail.aspx.cs" Inherits="CountryDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControls/map.ascx" TagName="map" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="SliderGallery/thumb-scroller.css" rel="stylesheet" type="text/css" />
    <script src="SliderGallery/preview.js" type="text/javascript"></script>
    <script src="SliderGallery/jquery.thumb-scroller.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#aCountry").addClass("active");
        });
    </script>
    <style type="text/css">
        .float-lt
        {
            float: left;
        }
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
        .left-content .country-block-outer
        {
            position: relative;
        }
        .left-content .country-block-inner
        {
            background: #fff;
            width: 100%;
            float: left;
            padding: 0;
        }
        .contory-block
        {
            width: 150px;
            height: 170px;
            float: left;
            border: 1px solid #b3b3b3;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -ms-border-radius: 5px;
            behavior: url(PIE.htc);
            margin: 6px;
            padding: 8px;
        }
        
        #mcts1 .navPrev, #mcts1 .navNext, #mcts2 .navPrev, #mcts2 .navNext
        {
            top: 95px;
        }
        .clsList
        {
            clear: left;
            color: #4d4d4d;
            font-size: 13px;
            padding: 5px 0 0 10px;
            background: url(images/arr-red.jpg) no-repeat left 10px;
        }
        div.trainname
        {
            white-space: nowrap;
            width: 170px;
            overflow: hidden;
        }
    </style>
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <section class="content">
    <div class="breadcrumb"> <a href="#">Home </a> >> <asp:Label ID="lblCn" runat="server"/>  </div>
    <div class="clear"></div>
   <%--Banner section--%> 
    <div class="innner-banner" style="padding-bottom:3px;">
    <div class="bannerLft"> <div class="banner90"><asp:Label ID="lblCntyNm" runat="server"/></div></div>
    <div class="btn-conect"> <a href="#"><img src='images/btn-wewill-conected.png' alt="" border="0" /></a> </div>
    <div class="full" style=" height:190px;">
        <div style="float:left;width: 732px"><asp:Image id="imgBanner" alt="" border="0" runat="server" Width="732px" height="190px" /></div>
        <div style="float:right;width:272px;"><asp:Image id="imgCountry" Width="272px" height="190px" runat="server"/></div>
    </div>
    </div>
    <%--Banner section end--%>
    <div class="left-content">
        <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
        </asp:Panel>
        <div>
            <asp:Label ID="lblCtryName" runat="server"></asp:Label>
            <p><asp:Label ID="lblCtryInfo" runat="server"></asp:Label></p>
        </div>
        <div id="divPopular" runat="server" class="rail-detail-block">
            <div class="list-tab">
                <ul>
                    <li>
                         <input value="Popular Journeys" BorderStyle="None" ID="Tab1" type="button"
                            style="width: 150px;height: 32px" runat="server" class="btnactive"/>
                    </li>
                </ul>
            </div>
            <div class="tab-detail-in">
                <asp:Repeater ID="rptJourney" runat="server" OnItemCommand="rptJourney_ItemCommand">
                    <ItemTemplate>
                        <div class="booking-block brbotm">
                            <div class="booking-detail">
                                <div class="colum01">
                                 <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("FromFlag")%>' alt="" border="0" height="26px"/> 
                                 <h3><%#(Eval("From").ToString().Length > 15) ? (Eval("From").ToString().Substring(0, 15) + "...") : (Eval("From").ToString())%> 
                                  &nbsp;to <br />
                                 <span> Duration  </span> </h3>
                                 <p> 
                                    <%#(Eval("Description").ToString().Length > 20) ? (Eval("Description").ToString().Substring(0, 20) + "...") : (Eval("Description").ToString())%>
                                 </p>
                                </div>
                                <div class="colum01" style="float:right;width:110px">
                                <%--<div class="price"><span>from</span> <br /> <%=currency%> <%#Eval("Price")%></div>--%>
                                 <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("ToFlag")%>' alt="" border="0" height="26px"/> 
                                 <h3><%#(Eval("To").ToString().Length > 15) ? (Eval("To").ToString().Substring(0, 15) + "...") : (Eval("To").ToString())%>   <br />
                                 <span> <%#Eval("DurationHr")%>hr <%#Eval("DurationMin")%> min</span> </h3>
                                </div>            
                              </div>
                            <%--<div class="imgblock">
                                <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImg")=="" ? "images/NoproductImage.png":Eval("BannerImg")%>'
                                    alt="" border="0" width="112px" height="70px" />
                            </div>--%>
                            <div class="clear"></div>
                            <div style="width:85px; margin:auto;">
                                <asp:Button ID="btnBook" Text="Book Now" runat="server" CommandName="Book" CommandArgument='<%#Eval("ID") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="clear">
            &nbsp;</div>
        <div class="map-container-title">
            <h1 style="display:none;">
                <a href="#" id="lnkViewMap" runat="server">
                    <img src='images/icon-locate.png' alt="" border="0"  />
                    View map  </a>
            </h1>
            <input type="submit" class="f-right mar-t" style="width: 166px;" value="Country Information" />
        </div>
        
   <asp:Panel ID="pnlMap" runat="server" CssClass="popup" > 
      <div  class="popuHeading">
      <div style="width:400px;float:left;padding-left:15px;"> International Rail Map</div>
         <a href="#" id="btnClose" runat="server" style="color:#fff;"> X </a>
    </div>
  <uc2:map ID="map1" runat="server" />  
  </asp:Panel>
   <asp:ModalPopupExtender ID="mdpupStock" runat="server" TargetControlID="lnkViewMap" CancelControlID="btnClose" PopupControlID="pnlMap" BackgroundCssClass="modalBackground" />
        <div class="clear">
            &nbsp;</div>
        <br />
        <div class="clear">
            &nbsp;</div>
            <%--Train detail--%>
            <strong class="txt-red">Trains</strong>
        <div style="width: 695px">
    <div style="width: 700px; float:left; position:relative;">
    <div id="banner-slide">
<!-- start Basic Jquery Slider -->
    <div id="ca-container" class="ca-container" style="background-color:White;">
        <div class="ca-wrapper">
            <asp:Repeater ID="tptTrainList" runat="server">
            <HeaderTemplate>
                <div id="ts" class="thumb-scroller" style="height:195px !important;">
                    <ul class="ts-list">
            </HeaderTemplate>
                <ItemTemplate>
                        <li><a href="<%#Eval("NavUrl") %>" data-lightbox-group="gallery1" style="text-decoration:none;">
                            <img src="<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImage")=="" ? "images/NoproductImage.png":Eval("BannerImage") %>" alt="" style="height:135px !important; width:99% !important;" />
                         
                          <h3 style="padding-top: 5px; text-align:center;"> <div class="trainname" style="text-overflow:ellipsis;">
                           
                            <%#Eval("Name") %> 
                         
                            </div>
                               </h3>
                            </a>
                        </li> 
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    </div>
    </div>
    </div>
    <div class="clear">&nbsp;</div>
    </div>
    <div class="right-content">
        <div class="ticketbooking" style="padding-top:0px">
            <div class="list-tab divEnableP2P">
                <ul>
                    <li><a href="#" class="active">Rail Tickets </a><li>
                    <li><a href="rail-passes">Rail Passes </a></li>
                </ul>
            </div>
            <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
            <img src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
        </div>
<uc:Newsletter ID="Newsletter1" runat="server" />
  <div class="right" style="display:none;">
        <div id="dvRight" class="slider-wrapper theme-default">
            <div id="sliderRt">
                <asp:Repeater ID="rptRtImg" runat="server">
                    <ItemTemplate>
                        <img src='<%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
    </section>
</asp:Content>
