﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="FooterItems.aspx.cs"
    Inherits="IR_Admin.FooterItems" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/Tab/jquery.js"></script>
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
            if(<%=tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Footer Menu Items
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="FooterItems.aspx" class="current">List</a></li>
            <li><a id="aNew" href="FooterItems.aspx" class="">New</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdFooter" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdFooter_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Menu Name">
                                    <ItemTemplate>
                                        <%#Eval("MenuName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="35%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Footer Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="35%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="FooterItems.aspx?id=<%#Eval("Id")%>" title="Edit" style="text-decoration: none;">
                                            <img alt="edit" src="images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Menu Name
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlMenu" runat="server" Width="250px" />
                                            <asp:RequiredFieldValidator ID="rfMenu" runat="server" ErrorMessage="*" ForeColor="Red"
                                                ControlToValidate="ddlMenu" ValidationGroup="submit" InitialValue="-1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtName" runat="server" Width="250px" />
                                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Navigation Url
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtUrl" runat="server" Width="500px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsActv" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="submit" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
