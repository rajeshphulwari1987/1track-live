﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class FooterItems : Page
    {
        private readonly Masters _oMasters = new Masters();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            FillGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            if (!IsPostBack)
            {
                btnSubmit.Enabled = true;
                if (Request["id"] != null)
                {
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                BindMenu();
                GetDetailsByEdit();

                _SiteID = Master.SiteID;
                SiteSelected();
                FillGrid(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        public void BindMenu()
        {
            ddlMenu.DataSource = _oMasters.GetActiveFooterMenuList();
            ddlMenu.DataTextField = "MenuName";
            ddlMenu.DataValueField = "ID";
            ddlMenu.DataBind();
            ddlMenu.Items.Insert(0, new ListItem("-- Footer Menu --", "-1"));
        }

        public void FillGrid(Guid siteID)
        {
            grdFooter.DataSource = _oMasters.GetFooterItemList();
            grdFooter.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditFooterItems();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("FooterMenu.aspx");
        }

        public void AddEditFooterItems()
        {
            try
            {
                var id = string.IsNullOrEmpty(Request.QueryString["id"]) ? new Guid() : Guid.Parse(Request.QueryString["id"]);
                _oMasters.AddEditFooterItems(new tblFooterItem
                    {
                        ID = id,
                        MenuID = Guid.Parse(ddlMenu.SelectedValue),
                        Name = txtName.Text.Trim(),
                        NavUrl = txtUrl.Text.Trim(),
                        IsActive = chkIsActv.Checked,
                    });
                    ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "Footer items updated successfully." : "Footer items added successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ClearControls();
                    tab = "1";
                    ViewState["tab"] = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlMenu.SelectedIndex = 0;
            txtName.Text = string.Empty;
            txtUrl.Text = string.Empty;
            chkIsActv.Checked = false;
        }

        public void GetDetailsByEdit()
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    return;

                btnSubmit.Text = "Update";
                var id = Guid.Parse(Request.QueryString["id"]);
                var rec = _oMasters.GetFooterItemsById(id);
                ddlMenu.SelectedValue = rec.MenuID.ToString();
                txtName.Text = rec.Name;
                txtUrl.Text = rec.NavUrl;
                chkIsActv.Checked = rec.IsActive;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void grdFooter_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                var res = _oMasters.DeleteFooterItems(id);
                if (res)
                    ShowMessage(1, "Record deleted Successfully.");
            }
            if (e.CommandName == "ActiveInActive")
            {
                _oMasters.ActiveInactiveFooterItems(id);
            }
            _SiteID = Master.SiteID;
            SiteSelected();
            FillGrid(_SiteID);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}