﻿using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;
using System.Linq;

namespace IR_Admin
{
    public partial class Manage_StaVEPage : Page
    {
        readonly Masters _master = new Masters();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        db_1TrackEntities _db = new db_1TrackEntities();
        private readonly ManageInterRailNew _ManageIRNew = new ManageInterRailNew();
        Guid _siteID;
        public string Banner, PTP, Map, Map1, Map2;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["SITEIDVEPAGE"] != null)
                    _siteID = Guid.Parse(Session["SITEIDVEPAGE"].ToString());
                //else
                //_siteID = Guid.Parse("F06549A9-139A-44F9-8BEE-A60DC5CC2E05");
                //var UserExist = new ManageUser().AdminLogin("Admin", "admin123");
                //if (UserExist != null)
                //{
                //    Login User Information
                //    AdminuserInfo.RoleId = UserExist.RoleID;
                //    AdminuserInfo.UserID = UserExist.ID;
                //    AdminuserInfo.Username = UserExist.UserName;
                //    AdminuserInfo.BranchID = UserExist.BranchID;
                //    AdminuserInfo.SiteID = new ManageUser().GetSiteIdByUser(UserExist.ID);
                //}
                BindImages();
                BindPage();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindImages()
        {
            try
            {
                var resultbanner = _master.GetImageListByID(1); //banner images
                dtBanner.DataSource = resultbanner;
                dtBanner.DataBind();
                dtBanner2.DataSource = resultbanner;
                dtBanner2.DataBind();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindPage()
        {
            try
            {
                var list = _db.GetAllProductORSpecialTrain(_siteID, 0).ToList();
                rptproduct.DataSource = list;
                rptproduct.DataBind();

                var list2 = _db.GetAllProductORSpecialTrain(_siteID, 1).ToList();
                rpttrain.DataSource = list2;
                rpttrain.DataBind();

                if (Session["pageid"] != null)
                {
                    Guid id = Guid.Parse(Session["pageid"].ToString());
                    var result = (from pages in _db.tblPages
                                  join st in _db.tblSites
                                      on pages.SiteID equals st.ID
                                  where pages.ID == id
                                  select new { pages }).FirstOrDefault();

                    hdnVEbanner.Value = result.pages.VEbanner;
                    hdnVEptpimg.Value = result.pages.VEptpimg;
                    hdnVEmap.Value = result.pages.VEmap;
                    hdnVEmaps1.Value = result.pages.VEmaps1;
                    hdnVEmaps2.Value = result.pages.VEmaps2;

                    hdnVEmaplink.Value = result.pages.VEmaplink;
                    hdnVEmaps1link.Value = result.pages.VEmaps1link;
                    hdnVEmaps2link.Value = result.pages.VEmaps2link;

                    hdnVEpass.Value = result.pages.VEpass;
                    hdnVEtrain.Value = result.pages.VEtrain;
                    if (result.pages.VEnewadventure != null && result.pages.VEnewadventure.Length > 500)
                    {
                        pnlnewadventure.InnerHtml = result.pages.VEnewadventure;
                        hdnVEnewadventure.Value = result.pages.VEnewadventure;
                    }

                    hdnVEbtncontactcall.Value = result.pages.VEbtncontactcall;
                    hdnVEbtncontactbook.Value = result.pages.VEbtncontactbook;
                    hdnVEbtncontactlive.Value = result.pages.VEbtncontactlive;
                    hdnVEbtncontactemail.Value = result.pages.VEbtncontactemail;

                    hdnVEbtntext1.Value = result.pages.VEbtntext1;
                    hdnVEbtntext2.Value = result.pages.VEbtntext2;
                    hdnVEbtntext3.Value = result.pages.VEbtntext3;
                    hdnVEbtntext4.Value = result.pages.VEbtntext4;
                    hdnVEbtntext5.Value = result.pages.VEbtntext5;
                    hdnVEbtntext6.Value = result.pages.VEbtntext6;
                    hdnVEbtntext7.Value = result.pages.VEbtntext7;
                    hdnVEbtntext8.Value = result.pages.VEbtntext8;


                    hdnVEmaplink.Value = (string.IsNullOrEmpty(result.pages.VEmaplink)) ? "#" : result.pages.VEmaplink;
                    hdnVEmaps1link.Value = (string.IsNullOrEmpty(result.pages.VEmaps1link)) ? "#" : result.pages.VEmaps1link;
                    hdnVEmaps2link.Value = (string.IsNullOrEmpty(result.pages.VEmaps2link)) ? "#" : result.pages.VEmaps2link;


                    var resultimg = _master.GetImageListByID(1); //banner images
                    if (!string.IsNullOrEmpty(result.pages.VEbanner))
                        Banner = resultimg.AsEnumerable().FirstOrDefault(x => x.ID == Convert.ToInt32(result.pages.VEbanner)).ImagePath;
                    else
                        Banner = "images/banner-1.jpg";

                    if (!string.IsNullOrEmpty(result.pages.VEptpimg))
                        PTP = resultimg.AsEnumerable().FirstOrDefault(x => x.ID == Convert.ToInt32(result.pages.VEptpimg)).ImagePath;
                    else
                        PTP = "images/plan-img.jpg";

                    if (!string.IsNullOrEmpty(result.pages.VEmap))
                        Map = resultimg.AsEnumerable().FirstOrDefault(x => x.ID == Convert.ToInt32(result.pages.VEmap)).ImagePath;
                    else
                        Map = "images/map-img.png";

                    if (!string.IsNullOrEmpty(result.pages.VEmaps1))
                        Map1 = resultimg.AsEnumerable().FirstOrDefault(x => x.ID == Convert.ToInt32(result.pages.VEmaps1)).ImagePath;
                    else
                        Map1 = "images/demo-img-1.jpg";

                    if (!string.IsNullOrEmpty(result.pages.VEmaps2))
                        Map2 = resultimg.AsEnumerable().FirstOrDefault(x => x.ID == Convert.ToInt32(result.pages.VEmaps2)).ImagePath;
                    else
                        Map2 = "images/demo-img-2.jpg";
                }
                else
                {
                    Banner = "images/banner-1.jpg";
                    PTP = "images/plan-img.jpg";
                    Map = "images/map-img.png";
                    Map1 = "images/demo-img-1.jpg";
                    Map2 = "images/demo-img-2.jpg";
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}

/*
hdnVEbanner
hdnVEptpimg
hdnVEmap
hdnVEmaps1
hdnVEmaps2
hdnVEpass
hdnVEtrain
    
hdnVEbtncontactcall
hdnVEbtncontactbook
hdnVEbtncontactlive
hdnVEbtncontactemail

hdnVEbtntext1
hdnVEbtntext2
hdnVEbtntext3
hdnVEbtntext4
hdnVEbtntext5
hdnVEbtntext6
hdnVEbtntext7
hdnVEbtntext8
*/