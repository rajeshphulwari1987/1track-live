﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;

public partial class Order_PaymentConfirmation : System.Web.UI.Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string script = "<script></script>";
    private Guid _siteId;
    public string OrderNo = string.Empty;
    public string linkURL = string.Empty;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            _siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _masterPage.GetPageDetailsByUrl(url);
                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null)
        {
            OrderNo = Request.Params["req"];
            linkURL="OrderPayment.aspx?req="+OrderNo;
        }
        else if (Session["OrderID"] != null)
        {
            OrderNo = Session["OrderID"].ToString();
            linkURL="OrderPayment.aspx";
        }

        if (string.IsNullOrEmpty(OrderNo))
        {
            Response.Redirect("~/Home");
        }
        if (Session["ProductType"] != null)
            lblBannerTxt.Text = "Rail Tickets";

        /*Hide Journey Request Form*/
        //if (!IsPostBack)
        //{
        //    var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
        //    if (pageId != null)
        //        PageContent(pageId, _siteId);
        //}

        QubitOperationLoad();
        setOrderData();
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    public void setOrderData()
    {
        ManageBooking objB = new ManageBooking();
        List<GetAllCartData_Result> lst=objB.GetAllCartData(Convert.ToInt64(OrderNo));
        if (lst.Count() > 0)
        {
            PaymentGateWayTransffer objPT = Session["PayObj"] as PaymentGateWayTransffer;
            
            Session["CustomerData"] = lst.FirstOrDefault().Address1 + " " + lst.FirstOrDefault().Address2 + ";" + lst.FirstOrDefault().City + ";" + lst.FirstOrDefault().Postcode + ";" + lst.FirstOrDefault().DCountry + ";" + lst.FirstOrDefault().EmailAddress;
            tblSite objSite = new ManageJourney().GetSiteList().Where(a => a.ID == lst.FirstOrDefault().SiteID).FirstOrDefault();
            tblCurrencyMst objCurrency = new Masters().GetCurrencyList().Where(a => a.ID == objSite.DefaultCurrencyID).FirstOrDefault();
            string StTkProtnAmt = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(objPT.Amount), objSite.ID, objCurrency.ID, objSite.DefaultCurrencyID.HasValue ? objSite.DefaultCurrencyID.Value : new Guid()).ToString("F");
            Session["Amount"] = Convert.ToDouble(StTkProtnAmt);
            Session["currencyCode"] = objCurrency.ShortCode;
        }
    }
}