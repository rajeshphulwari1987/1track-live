﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Traveler.aspx.cs" Inherits="IR_Admin.Traveler" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script>
        $(function () {                  
             if(<%=tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }
           
        });   
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Traveller</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Traveler.aspx" class="current">List</a></li>
            <li><asp:HyperLink ID="aNew" CssClass="" NavigateUrl="#" runat="server">New</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grvTraveler" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            PageSize="10" CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None"
                            Width="100%" OnRowCommand="grvTraveler_RowCommand" OnPageIndexChanging="grvTraveler_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Traveller Type">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Notes">
                                    <ItemTemplate>
                                        <%#Eval("Note")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eurail Code">
                                    <ItemTemplate>
                                        <%#Eval("EurailCode")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age From">
                                    <ItemTemplate>
                                        <%#Eval("FromAge")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age To">
                                    <ItemTemplate>
                                        <%#Eval("ToAge")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sort Order">
                                    <ItemTemplate>
                                        <%#Eval("SortOrder")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField runat="server" ID="hdnId" />
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <div style="margin: 20px;">
                        <table class="tblMainSection">
                            <tr>
                                <td class="col">
                                    Name
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="200" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqtxtName" runat="server" ControlToValidate="txtName"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Note
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtNote" runat="server" MaxLength="200" />
                                    &nbsp;<asp:RequiredFieldValidator ID="ReftxtNote" runat="server" ControlToValidate="txtNote"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Eurail Code
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtEurailCode" runat="server" MaxLength="5" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqEurailCode" runat="server" ControlToValidate="txtEurailCode"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                    <asp:FilteredTextBoxExtender ID="ftbEurailCode" TargetControlID="txtEurailCode" ValidChars=".0123456789"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Age From
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddlAgeFrom" runat="server" Width="205" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqAgeFrom" runat="server" ControlToValidate="ddlAgeFrom"
                                        InitialValue="-1" CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Age To
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddlAgeTo" runat="server" Width="205" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqAgeTo" runat="server" ControlToValidate="ddlAgeTo"
                                        InitialValue="-1" CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Sort Order
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtRank" runat="server" MaxLength="200" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqRank" runat="server" ControlToValidate="txtRank"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Is Active
                                </td>
                                <td class="col">
                                    <asp:CheckBox ID="chkactive" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                        Text="Submit" Width="89px" ValidationGroup="vg" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                        Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
