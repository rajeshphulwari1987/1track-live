﻿<%@ Page Title="DashBoard" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="DashBoard.aspx.cs" Inherits="IR_Admin.DashBoard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Others/feedburner.ascx" TagName="feedburner" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type="submit"]').click(function () {
                $('#divprogress').hide();
                $find('#MainContent_mdpupSite').hide();
                $("#MainContent_pnlSite").hide();
            });
            $("#MainContent_feedburner_lblFeedTitle,#MainContent_feedburner2_lblFeedTitle").addClass("heading-color");
        });
    </script>
     <style type="text/css">
        .content
        {
            float: none;
            margin: 0 auto;
            width: 982px;
        }
        .popup
        {
            width: 550px;
            height: 155px;
            position: fixed;
            z-index: 999999 !important;
            left: 375px;
            top: 200px;
            border-left: 1px solid #d5d5d5;
            border-right: 1px solid #d5d5d5;
            background: #f2efef;
            padding: 0px 12px 0px 12px;
        }
        .clsBtnOk
        {
            width: 490px;
            text-align: center;
            margin: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content">
        
        <div class="heading">
            Dash Board</div>

        <uc1:feedburner ID="feedburner" runat="server" FeedUrl="http://feeds.feedburner.com/railtravelnews"
            FeedTitleHeader="Global Rail News" />
        <uc1:feedburner ID="feedburner2" runat="server" FeedUrl="http://feeds.feedburner.com/1trackUpdateLog"
            FeedTitleHeader="1Track Update Log" />
         
    </div>
    <asp:HiddenField ID="hdnlnk" runat="server" />
    <asp:Panel ID="pnlSite" runat="server" CssClass="popup">
        <div style="float: left; width: 560px">
            <div style="float: right">
                <asp:ImageButton ID="btnClose" runat="server" ImageUrl="images/cross.png" OnClick="btnClose_Click" />
            </div>
        </div>
        <div style="margin: 40px 25px 25px 25px;">
            <asp:DropDownList ID="ddlSites" runat="server" Style="width: 490px; height: 30px;
                padding: 5px" />
        </div>
        <div class="clsBtnOk">
            <asp:Button ID="btnOk" runat="server" CssClass="button1" OnClick="btnOk_Click" Text="OK" />
        </div>
    </asp:Panel>
    <div id="divload" class="loading-admin" runat="server">
        &nbsp;</div>
    <asp:ModalPopupExtender ID="mdpupSite" runat="server" TargetControlID="hdnlnk" CancelControlID="btnClose"
        PopupControlID="pnlSite" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
</asp:Content>
