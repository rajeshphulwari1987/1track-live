﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="PrintTool.aspx.cs" Inherits="IR_Admin.PrintTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <style>
   .colum-two li {padding-bottom:5px;}
    #install li {padding-bottom:5px;}
   </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="full mr-tp1" style="font-size: 13px">
        <div class="panes">
            <div id="divNew" runat="server" style="display: block;">
                <div class="heading">
                    Phoenix Print Tool
                </div>
                <div class="content-in">
                    <div class="clrup">
                    </div>
                    <div class="colum-one" style="height:150px;">
                          <a style="color:#b53859" href="software/download/phoenix_print_tool_V2.3_licensed.exe" onclick=var that=this;_gaq.push(['_trackEvent','Download','EXE',this.href]);setTimeout(function(){location.href=that.href;},200);return false;>
                        <div class="file-container" style="width: 125px; height: 100px;">
                        <img src="software/save-icon.png" alt="download" border="0" /> 
                        </div>
                         [Download Phoenix Print Tool]</a>
                    </div>
                    <div class="colum-two">
                        <b>Key points:</b>
                        <ul>
                            <li>The Phoenix Print Tool will need to be loaded on all PCs that print Eurail passes.
                            </li>
                            <li>PC Administrator access will be required to complete the install. </li>
                            <li>.NET minimum version 2.0 is required </li>
                        </ul>
                    </div>
                    <div class="clrup">
                    </div>
                    <div id="install" style="margin-top: 40px;">
                        <b>Steps for the installation:</b>
                        <ul>
                            <li>Download the latest version of the Phoenix Print Tool to start the download & install
                                process. </li>
                            <li>Follow the on-screen notes & accept all defaults when installing. </li>
                            <li>When the install is complete, and depending on your operating system, a new folder/application
                                called “International Rail > Phoenix Print Tool” will show on the “All Programs”
                                list on your PC. </li>
                            <li>Click to open and select the Printer you will use to print your Eurail passes from
                                the drop down (see screenshot below)[select-printer.png] .</li>
                            <li>Finally, ensure that you have “Legal (8.5 x 14)” size selected as both the Output
                                Paper Size and the Original Size in your Printer Preferences (as below) [printer-prefs.png].
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="file-container" style="width: 150px; height: 120px;">
                            <a href="software/select-printer.png"  target="_blank">
                                <img src="software/select-printer.png" alt="download" border="0" height="98px" width="140px" />
                            </a>[select-printer.png]
                        </div>
                        <div class="file-container" style="width: 150px; height: 120px;">
                            <a href="software/printer-prefs.png" target="_blank">
                                <img src="software/printer-prefs.png" alt="download" border="0" height="98px" width="140px" /></a>
                            [printer-prefs.png]
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
