﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Business;
using System.Net;
using System.Net.Sockets;

namespace IR_Admin
{
    public partial class TrainDetails : Page
    {
        ManageTrainDetails _master = new ManageTrainDetails();
        readonly Masters _oMaster = new Masters();
        public string tab = string.Empty;
        static List<FileContent> fileList = new List<FileContent>();
        private const int pageSize = 10;
        Guid _siteID;
        #region [ Page InIt must write on every page of CMS ]

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            tab = "1";
            _siteID = Guid.Parse(selectedValue);
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!IsPostBack)
            {
                PageLoadEvent();
            }
            ShowMessage(0, null);
        }

        void PageLoadEvent()
        {
            dtlCountry.DataSource = _oMaster.GetRegionList().ToList().OrderBy(x => x.RegionName).ToList();
            dtlCountry.DataBind();

            IEnumerable<tblSite> objSite = _oMaster.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }

            _siteID = Master.SiteID;
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
            BindPager();
        }

        void BindList(Guid siteID, int start, int end)
        {
            if (siteID == new Guid())
                siteID = Master.SiteID;

            var list = _master.GetTrainDetailList(siteID, start, end);
            dtlTrains.DataSource = list != null && list.Count > 0 ? list : null;
            dtlTrains.DataBind();
        }

        protected void dtlCountry_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var trCntry = e.Item.FindControl("trCntry") as TreeView;
                var hdnRegionId = e.Item.FindControl("hdnRegion") as HiddenField;
                var lisCountry = _oMaster.GetCountryList().Where(x => hdnRegionId != null && (x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value)));
                foreach (var itemCountry in lisCountry)
                {
                    var trCat = new TreeNode
                        {
                            Text = itemCountry.CountryName,
                            Value = itemCountry.CountryID.ToString(),
                            SelectAction = TreeNodeSelectAction.None
                        };
                    if (trCntry != null) trCntry.Nodes.Add(trCat);
                }
            }
        }

        public string GetIpAddress()
        {
            string IPAddressThisPc = string.Empty;
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    IPAddressThisPc = ip.ToString();
                    break;
                }
            }
            return IPAddressThisPc;
            //name = host.AddressList.GetValue(1).ToString();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool check = false;
                bool checkCnt = false;
                foreach (DataListItem item in dtlCountry.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var treeCnt = item.FindControl("trCntry") as TreeView;
                        foreach (TreeNode node in treeCnt.Nodes)
                        {
                            if (node.Checked)
                            {
                                checkCnt = true;
                                break;
                            }
                        }
                    }
                }
                foreach (TreeNode node in trSites.Nodes)
                {
                    if (node.Checked)
                    {
                        check = true;
                        break;
                    }
                }
                if (!check)
                {
                    tab = "2";
                    ShowMessage(2, "Please select at least one site.");
                    return;
                }
                else if (!checkCnt)
                {
                    tab = "2";
                    ShowMessage(2, "Please select at least one country.");
                    return;
                }
                else
                {
                    string TrnImage = UploadFile();
                    RemoveImageFromDirec(TrnImage);
                    var id = _master.AddEditTrainDetail(new tblTrainDetail
                        {
                            ID = string.IsNullOrEmpty(hdnId.Value) ? new Guid() : Guid.Parse(hdnId.Value),
                            Name = txtTName.Text,
                            NavUrl = txtUrl.Text.Trim(),
                            BannerImage = TrnImage.Replace("~/", ""),
                            ShortDescription = txtTrainInfo.InnerHtml,
                            LongDescription = txtContent.InnerHtml,
                            IPAddress = GetIpAddress(),
                            IsActive = chkactive.Checked,
                            CreatedOn = DateTime.Now,
                            CreatedBy = AdminuserInfo.UserID
                        });

                    if (fileList != null)
                        foreach (var item in fileList)
                        {
                            _master.AddGalry(new tblTrainDetailGallery
                                {
                                    ID = new Guid(),
                                    TrainDetailID = id,
                                    GalleryImage = item.filePath.Replace("~/", "")
                                });
                        }
                    fileList = null;

                    var lstTrainSiteLookup = new List<tblTrainDetailSiteLookup>();
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            lstTrainSiteLookup.Add(new tblTrainDetailSiteLookup
                            {
                                SiteID = Guid.Parse(node.Value),
                                TrainID = id
                            });
                        }
                    }
                    if (lstTrainSiteLookup.Count > 0)
                        _master.AddEditTrainDetailSiteLookup(lstTrainSiteLookup);

                    #region Product Promotion Country LookUp
                    var listCntLkp = new List<tblTrainDetailCntryLookup>();
                    foreach (DataListItem item in dtlCountry.Items)
                    {
                        if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                        {
                            var treeCnty = item.FindControl("trCntry") as TreeView;
                            if (treeCnty != null)
                            {
                                var res = treeCnty.Nodes.Cast<TreeNode>().Where(node => node.Checked).Select(x => x.Value);
                                listCntLkp.AddRange(res.Select(itm => new tblTrainDetailCntryLookup
                                {
                                    CountryID = Guid.Parse(itm),
                                    TrainID = id,
                                    ID = new Guid()
                                }));
                            }
                        }
                    }
                    _master.AddCntLookUp(listCntLkp, id);
                    #endregion

                    BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
                    ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Train Detail added successfully." : "Train Detail updated successfully.");
                    txtTName.Text = string.Empty;
                    txtTrainInfo.InnerText = string.Empty;
                    txtContent.InnerText = string.Empty;
                    chkactive.Checked = false;
                    rptGallery.DataSource = null;
                    rptGallery.DataBind();
                    hdnId.Value = string.Empty;
                    UncheckTree();
                    tab = "1";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                tab = "2";
            }
        }

        void UncheckTree()
        {
            foreach (DataListItem item in dtlCountry.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var chkRegion = item.FindControl("chkRegion") as CheckBox;
                    chkRegion.Checked = false;
                    var treeCnt = item.FindControl("trCntry") as TreeView;
                    foreach (TreeNode node in treeCnt.Nodes)
                    {
                        node.Checked = false;
                    }
                }
            }

            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("TrainDetails.aspx");
        }

        //--Removed Old Train Image At Edit case
        void RemoveImageFromDirec(string TrnImage)
        {
            if (!string.IsNullOrEmpty(hdnId.Value) && !string.IsNullOrEmpty(TrnImage))
            {
                string BannerImage = _master.GetTrainDetialById(Guid.Parse(hdnId.Value)).BannerImage;
                string bPath = "~/" + BannerImage.Trim();
                if (File.Exists(Server.MapPath(bPath)))
                    File.Delete(Server.MapPath(bPath));
            }
        }

        #region Manage Images
        public void AjaxFileUpload2_OnUploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            string[] ext = new string[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
            string FileName = e.FileName.Substring(e.FileName.LastIndexOf("."));

            if (!ext.Contains(FileName.ToUpper()))
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
            else
            {
                FileName = Guid.NewGuid().ToString() + FileName;
                CreateTrainImageFolder();
                string path = "~/Uploaded/TrainGalleryImg/" + FileName;

                if (File.Exists(Server.MapPath(path)))
                    File.Delete(Server.MapPath(path));
                else
                    AjaxFileUpload2.SaveAs(Server.MapPath(path));
                if (fileList == null)
                    fileList = new List<FileContent>();

                fileList.Add(new FileContent
                {
                    filePath = path
                });
            }
        }

        private void CreateTrainImageFolder()
        {
            if (!Directory.Exists(Server.MapPath("~/Uploaded/TrainGalleryImg")))
                Directory.CreateDirectory(Server.MapPath("~Uploaded/TrainGalleryImg"));
        }

        protected string UploadFile()
        {
            try
            {
                string cImgpath = string.Empty;
                string[] ext = new string[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupCountryImg.HasFile)
                {
                    if (fupCountryImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('Uploaded Country Image is larger up to 1Mb.')</script>", false);
                    }

                    string FileName = fupCountryImg.FileName.Substring(fupCountryImg.FileName.LastIndexOf("."));
                    if (!ext.Contains(FileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                    }

                    FileName = fupCountryImg.FileName;
                    cImgpath = "~/Uploaded/TrainGalleryImg/";
                    Common oCom = new Common();
                    cImgpath = cImgpath + oCom.CropImage(fupCountryImg, cImgpath, 400, 250);
                }
                return cImgpath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        protected void dtlTrains_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    var hdnTrainId = (HiddenField)e.Item.FindControl("hdnTrainId");
                    hdnId.Value = hdnTrainId.Value;
                    switch (e.CommandName)
                    {
                        case "Modify":
                            btnSubmit.Text = "Update";
                            var rec = _master.GetTrainDetialById(Guid.Parse(hdnTrainId.Value));
                            var resultSite = _master.GetTrainDetailSiteIdListById(Guid.Parse(hdnTrainId.Value));

                            txtContent.InnerHtml = rec.LongDescription;
                            txtTrainInfo.InnerHtml = rec.ShortDescription;
                            txtTName.Text = rec.Name;
                            txtUrl.Text = rec.NavUrl;
                            chkactive.Checked = rec.IsActive;

                            foreach (var sItem in resultSite)
                            {
                                foreach (TreeNode node in trSites.Nodes)
                                {
                                    if (node.Value == sItem.ToString())
                                        node.Checked = true;
                                }
                            }

                            var listCnt = _master.GetTrainDetailCntryIdListById(Guid.Parse(hdnTrainId.Value)).Select(x => x.CountryID);
                            foreach (DataListItem item in dtlCountry.Items)
                            {
                                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                                {
                                    var treeCtry = item.FindControl("trCntry") as TreeView;
                                    if (treeCtry == null)
                                        return;
                                    foreach (TreeNode node in treeCtry.Nodes)
                                    {
                                        node.Checked = listCnt.Contains(Guid.Parse(node.Value));
                                    }
                                }
                            }
                            BindGalleryOnEdit();
                            tab = "2";
                            break;

                        case "Remove":

                            //--Deleted Images from Directory
                            var list = _master.GetGalleryListByTrainID(Guid.Parse(hdnTrainId.Value));
                            RemoveReadOnlyFile();
                            foreach (var item in list)
                            {
                                string cImgpath = "~/" + item.GalleryImage.Trim();
                                if (File.Exists(Server.MapPath(cImgpath)))
                                    File.Delete(Server.MapPath(cImgpath));
                            }

                            //--Deleted Images from Directory of Train Banner
                            string BannerImage = _master.GetTrainDetialById(Guid.Parse(hdnTrainId.Value)).BannerImage;
                            string bPath = "~/" + BannerImage.Trim();
                            if (File.Exists(Server.MapPath(bPath)))
                                File.Delete(Server.MapPath(bPath));

                            //--Delete Record From DB
                            bool res = _master.DeleteTrain(Guid.Parse(hdnTrainId.Value));
                            if (res)
                                ShowMessage(1, "Record deleted successfully");
                            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
                            tab = "1";
                            break;

                        case "ActiveInActive":
                            _master.ActiveInactive(Guid.Parse(hdnTrainId.Value));
                            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
                            tab = "1";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                tab = "1";
            }
        }

        protected void rptGallery_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                string imgpath = _master.DeleteGalleryImagesById(Guid.Parse(e.CommandArgument.ToString()));
                string cImgpath = "~/" + imgpath.Trim();
                if (File.Exists(Server.MapPath(cImgpath)))
                    File.Delete(Server.MapPath(cImgpath));
            }
            BindGalleryOnEdit();
            tab = "2";
        }

        void BindGalleryOnEdit()
        {
            try
            {
                rptGallery.DataSource = _master.GetGalleryListByTrainID(Guid.Parse(hdnId.Value));
                rptGallery.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Paging
        public void BindPager()
        {
            /*Bind Pager*/
            var oPageList = new List<ClsPageCount>();
            _siteID = Master.SiteID;
            int cnt = _master.TotalNumberOfRecord(_siteID);
            int newpagecount = cnt / pageSize + ((cnt % pageSize) > 0 ? 1 : 0);
            for (int i = 1; i <= newpagecount; i++)
            {
                var oPage = new ClsPageCount { PageCount = i.ToString() };
                oPageList.Add(oPage);
            }
            DLPageCountItem.DataSource = oPageList;
            DLPageCountItem.DataBind();
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);

            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage != null)
                    lblPage.Attributes.Add("class", "activepaging");
                break;
            }
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument) - 1;
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex + 1).ToString(CultureInfo.InvariantCulture))
                {
                    lblPage.Attributes.Add("class", "activepaging");
                }
                else
                {
                    lblPage.Attributes.Remove("class");
                }
            }
            ViewState["PageIndex"] = pageIndex;
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
        }
        #endregion

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        public void RemoveReadOnlyFile()
        {
            const string cntPath = "~/Uploaded/TrainGalleryImg/";
            foreach (string cntImg in Directory.GetFiles(Server.MapPath(cntPath), "*.*", SearchOption.AllDirectories))
            {
                new FileInfo(cntImg) { Attributes = FileAttributes.Normal };
            }
        }
    }

    public class FileContent
    {
        public string filePath { get; set; }
    }
}

