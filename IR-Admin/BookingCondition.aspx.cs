﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class BookingCondition : Page
    {
        readonly private ManageBooking _masterBook = new ManageBooking();
        readonly private Masters _oMasters = new Masters();
        List<RepeaterListItem> list = new List<RepeaterListItem>();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            GetProductDetailForEdit();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                BindSite();
                GetProductDetailForEdit();
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddBookingCondition()
        {
            try
            {
                _masterBook.AddBooking(new tblBookingCondition
                    {
                        ID = Guid.NewGuid(),
                        Title = txtTitle.Text.Trim(),
                        Description = txtDesc.Text.Trim(),
                        SiteID = Guid.Parse(ddlSite.SelectedValue),
                        Contract = txtContract.InnerHtml,
                        BookingsReservations = txtBookRes.InnerHtml,
                        Validity = txtValidity.InnerHtml,
                        FaresPayment = txtFarePay.InnerHtml,
                        TicketRefunds = txtTckRef.InnerHtml,
                        LostDamagedTickets = txtLostDam.InnerHtml,
                        Baggage = txtBaggage.InnerHtml,
                        ForceMajeure = txtForce.InnerHtml,
                        GoverningLaw = txtGLaw.InnerHtml,
                        BoardingTimes = txtBoarding.InnerHtml,
                        TimetableInformation = txtTimeTable.InnerHtml,
                        Variations = txtVariations.InnerHtml,
                        IsActive = chkIsActv.Checked,
                    });

                //ShowMessage(1, "You have successfully added booking conditions.");
                Response.Redirect("BookingConditionList.aspx");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdateBookingCondition()
        {
            try
            {
                Guid id = Guid.Parse(Request["id"]);
                var bookingCondition = new tblBookingCondition
                    {
                        ID = id,
                        Title = txtTitle.Text.Trim(),
                        Description = txtDesc.Text.Trim(),
                        SiteID = Guid.Parse(ddlSite.SelectedValue),
                        Contract = txtContract.InnerHtml,
                        BookingsReservations = txtBookRes.InnerHtml,
                        Validity = txtValidity.InnerHtml,
                        FaresPayment = txtFarePay.InnerHtml,
                        TicketRefunds = txtTckRef.InnerHtml,
                        LostDamagedTickets = txtLostDam.InnerHtml,
                        Baggage = txtBaggage.InnerHtml,
                        ForceMajeure = txtForce.InnerHtml,
                        GoverningLaw = txtGLaw.InnerHtml,
                        BoardingTimes = txtBoarding.InnerHtml,
                        TimetableInformation = txtTimeTable.InnerHtml,
                        Variations = txtVariations.InnerHtml,
                        IsActive = chkIsActv.Checked,
                    };

                _masterBook.UpdateBookingCondition(bookingCondition);
                //ShowMessage(1, "You have successfully updated booking conditions.");
                Response.Redirect("BookingConditionList.aspx");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddBookingCondition();
                else
                    UpdateBookingCondition();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("BookingConditionList.aspx");
        }

        void GetProductDetailForEdit()
        {
            if (Request.QueryString["id"] != null)
            {
                var id = Guid.Parse(Request.QueryString["id"]);
                btnSubmit.Text = "Update";
                tblBookingCondition oBook = _masterBook.GetBookingCondById(id);
                if (oBook != null)
                {
                    ddlSite.SelectedValue = oBook.SiteID.ToString();
                    txtTitle.Text = oBook.Title;
                    txtDesc.Text = oBook.Description;
                    txtContract.InnerHtml = oBook.Contract;
                    txtBookRes.InnerHtml = oBook.BookingsReservations;
                    txtValidity.InnerHtml = oBook.Validity;
                    txtFarePay.InnerHtml = oBook.FaresPayment;
                    txtTckRef.InnerHtml = oBook.TicketRefunds;
                    txtLostDam.InnerHtml = oBook.LostDamagedTickets;
                    txtBaggage.InnerHtml = oBook.Baggage;
                    txtForce.InnerHtml = oBook.ForceMajeure;
                    txtGLaw.InnerHtml = oBook.GoverningLaw;
                    txtBoarding.InnerHtml = oBook.BoardingTimes;
                    txtTimeTable.InnerHtml = oBook.TimetableInformation;
                    txtVariations.InnerHtml = oBook.Variations;
                    if (oBook.IsActive != null) chkIsActv.Checked = (bool)oBook.IsActive;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}