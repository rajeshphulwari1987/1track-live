﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SEObreadcrumbs.aspx.cs" Inherits="IR_Admin.MetaTag.SEObreadcrumbs" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
       
    </script>
    <style type="text/css">
        .inline, .inline a
        {
            padding-left: 5px;
            display: inline;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2>
                <asp:Label runat="server" ID="lblHeader"></asp:Label></h2>
                
            <div class="full mr-tp1">
             <ul class="list">
            <li><a id="aList" runat="server" href="" class="current">List</a></li>
        </ul>
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="full mr-tp1">
                    <div id="divNew" runat="server" class="grid-sec2">
                        <table class="cat-outer">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" class="cat-inner-alt">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left: 5px; width: 17%;" class="col">
                                                        <asp:Label runat="server" ID="lblddlname" Text=""></asp:Label>
                                                    </td>
                                                    <td class="col">
                                                        <asp:DropDownList runat="server" ID="ddlList" AutoPostBack="True" OnSelectedIndexChanged="ddlList_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 5px; width: 17%;" class="col">
                                                        URL Link:
                                                    </td>
                                                    <td class="col">
                                                        <span style="background: #dcdce2; padding: 5px; border: 1px solid #b1aeae;">
                                                            <asp:Literal runat="server" ID="literalbodydata"></asp:Literal></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 5px; width: 17%; vertical-align: top;" class="col">
                                                        URL Source Code:
                                                    </td>
                                                    <td class="col" style="padding-top: 4px;">
                                                        <asp:TextBox runat="server" ID="txtSaveData" TextMode="MultiLine" Width="50%" Height="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 5px; width: 17%; vertical-align: top;" class="col">
                                                        IsActive:
                                                    </td>
                                                    <td class="col" style="padding-top: 4px;">
                                                        <asp:CheckBox runat="server" ID="chkActive" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 5px;">
                                                    </td>
                                                    <td style="padding-top: 5px;">
                                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="clear: both;">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
