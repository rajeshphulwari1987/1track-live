﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CMSPageMetaInfo.aspx.cs" Inherits="IR_Admin.MetaTag.CMSPageMetaInfo" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript"> 
        $(function () {                  
          if(<%=tab.ToString()%>=="1")   {
                $("ul.list").tabs("div.panes > div");
            }
        });
         
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });

    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 500);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        CMS Pages SEO
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="CMSPageMetaInfo.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CMSPageMetaInfo.aspx" class="">New/Edit</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdCMS" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="10" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"
                            AllowPaging="True" OnPageIndexChanging="grdCMSPage_PageIndexChanging" OnRowCommand="grdCMSPage_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Page Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
                                        <%#Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="CMSPageMetaInfo.aspx?id=<%#Eval("Id")%>" style="text-decoration: none">
                                            <img title="Edit" alt="Edit" src="../images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <table width="100%">
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Page <span class="valdreq">*</span>
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlCMSPage" runat="server" Width="510px" />
                                            <asp:RequiredFieldValidator ID="reqCMSPage" runat="server" ControlToValidate="ddlCMSPage"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit"
                                                InitialValue="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Title<span class="valdreq">*</span>
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="100" Width="510px" />
                                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtTitle"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px; vertical-align: top;" class="col">
                                            Keywords<span class="valdreq">*</span>
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtKewords" runat="server" MaxLength="500" TextMode="MultiLine"
                                                Width="510px" Height="100" />
                                            <asp:RequiredFieldValidator ID="reqKewords" runat="server" ControlToValidate="txtKewords"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px; vertical-align: top;" class="col">
                                            Description
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtDescription" runat="server" MaxLength="500" TextMode="MultiLine"
                                                Width="510px" Height="100" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="submit" OnClick="btnSubmit_Click" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
