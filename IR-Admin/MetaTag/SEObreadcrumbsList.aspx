﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  
    CodeBehind="SEObreadcrumbsList.aspx.cs" Inherits="IR_Admin.MetaTag.SEObreadcrumbsList" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
       
    </script>
    <style type="text/css">
        .inline, .inline a
        {
            padding-left: 5px;
            display: inline;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                <asp:Label runat="server" ID="lblHeader"></asp:Label></h2>
            <div class="full mr-tp1">
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divlist" runat="server" style="display: block;">
                            <div class="crushGvDiv">
                                <asp:GridView ID="grdseo" runat="server" AutoGenerateColumns="False" PageSize="10"
                                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                    AllowPaging="True" OnPageIndexChanging="grdseo_PageIndexChanging" OnRowCommand="grdseo_RowCommand">
                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                    <PagerStyle CssClass="paging"></PagerStyle>
                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        Record not found.</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText=' Name'>
                                            <ItemTemplate>
                                                <%#Eval("Name")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="URL">
                                            <ItemTemplate>
                                                <%#Eval("Url")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <a href='SEObreadcrumbs.aspx?Name=<%=TitleName%>&Id=<%#Eval("Id")%>'>
                                                    <img src="../images/edit.png" /></a>
                                                <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                                    Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>' ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>'/>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
