﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.MetaTag
{
    public partial class CategoryMetaInfo : Page
    {
        readonly private SEOTools _SEOTools = new SEOTools();
        readonly private ManageProduct _oProduct = new ManageProduct();
        private readonly Masters _oMasters = new Masters();
        public string tab = string.Empty;
        public Guid _SiteID;

        #region [ Page InIt must write on every page of CMS ]

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            PageLoadEvent();
            BingGrid(_SiteID);
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            _SiteID = Master.SiteID;
            if (!IsPostBack)
            {
                PageLoadEvent();
                BingGrid(_SiteID);
                if (Request.Params["Edit"] != null)
                    EditData(Guid.Parse(Request.Params["Edit"]));
            }
        }

        private void PageLoadEvent()
        {
            try
            {
                //--Bind Site Tree
                IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
                foreach (var oSite in objSite)
                {
                    var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                    trSites.Nodes.Add(trCat);
                }
                //--Bind Category 
                const int treeLevel = 3;
                var list = _oProduct.GetAssociateCategoryList(treeLevel, _SiteID);
                ddlCategory.DataSource = list;
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BingGrid(Guid Site)
        {
            try
            {
                var list = _SEOTools.GetCategory(Site);
                grdCategory.DataSource = list;
                grdCategory.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditData(Guid ID)
        {
            try
            {
                var data = _SEOTools.EditDataCategory(ID);
                if (data != null)
                {
                    ddlCategory.SelectedValue = data.CategoryID.ToString();
                    txtTitle.Text = data.Title;
                    txtKewords.Text = data.Keywords;
                    txtDescription.Text = data.Description;
                    // Look up Sites
                    foreach (TreeNode item in trSites.Nodes)
                        if (Guid.Parse(item.Value) == data.SiteID)
                            item.Checked = true;
                    btnSubmit.Text = "Update";
                    tab = "2";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string str = e.CommandArgument.ToString();
            switch (e.CommandName)
            {
                case "Edit":
                    {
                        Response.Redirect("CategoryMetaInfo.aspx?Edit=" + str);
                        break;
                    }

                case "Remove":
                    {
                        _SEOTools.DeleteDataCategory(Guid.Parse(str));
                        Response.Redirect("CategoryMetaInfo.aspx");
                        break;
                    }
            }
        }

        protected void grdCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCategory.PageIndex = e.NewPageIndex;
            BingGrid(_SiteID);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = true;
                if (trSites.CheckedNodes.Count == 0)
                {
                    ShowMessage(2, "Please select site.");
                    tab = "2";
                    return;
                }
                else
                {
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            tblCategoryMetaInfo obj = new tblCategoryMetaInfo();
                            obj.ID = Guid.NewGuid();
                            obj.CategoryID = Guid.Parse(ddlCategory.SelectedValue);
                            obj.SiteID = Guid.Parse(node.Value);
                            obj.Title = txtTitle.Text;
                            obj.Keywords = txtKewords.Text;
                            obj.Description = txtDescription.Text;
                            obj.CreatedBy = AdminuserInfo.UserID;
                            obj.CreatedOn = DateTime.Now;
                            result = _SEOTools.AddEditCategory(obj);
                        }
                    }
                    if (btnSubmit.Text == "Submit")
                        ShowMessage(1, "Product category added successfully.");
                    else
                        ShowMessage(1, "Product category uploaded successfully.");
                }
                txtTitle.Text = txtKewords.Text = txtDescription.Text = string.Empty;
                trSites.Nodes.Clear();
                PageLoadEvent();
                BingGrid(_SiteID);
                tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CategoryMetaInfo.aspx");
        }

        void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

    }
}