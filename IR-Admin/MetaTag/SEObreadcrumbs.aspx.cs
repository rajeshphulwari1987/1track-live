﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

namespace IR_Admin.MetaTag
{
    public partial class SEObreadcrumbs : Page
    {
        public Guid _SiteID;

        #region [ Page InIt must write on every page of CMS ]

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }

        #endregion

        public ManageSeo master = new ManageSeo();
        public string Url = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            _SiteID = Master.SiteID;
            if (!IsPostBack && Request.QueryString["Name"] != null && Request.QueryString["Id"] != null)
            {
                ddlList.Enabled = false;
                string SiteUrl = master.GetlinkbodySiteURL(_SiteID).ToLower();
                string SEORequest = Request.QueryString["Name"];
                aList.Attributes.Add("href", "SEObreadcrumbsList.aspx?ID=" + SEORequest);
                string TName=string.Empty;
                switch (SEORequest)
                {
                    case "railpass":
                        lblHeader.Text = "Rail Passes Breadcrumbs";
                        ddlList.DataSource = master.Getproductlist(_SiteID);
                        ddlList.DataValueField = "Id";
                        ddlList.DataTextField = "Name";
                        ddlList.DataBind();
                        ddlList.SelectedValue = Request.QueryString["Id"];
                        TName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ddlList.SelectedItem.Text);
                        lblddlname.Text = "Product:";
                        Url = "<a href='" + SiteUrl + "'>Home</a> >> <a href='" + SiteUrl + "railpasses'>Rail Passes</a> >> " + TName;
                        BindData();
                        break;
                    case "railpassdetails":
                        lblHeader.Text = "Rail Pass Details Breadcrumbs";
                        ddlList.DataSource = master.Getproductlist(_SiteID);
                        ddlList.DataValueField = "Id";
                        ddlList.DataTextField = "Name";
                        ddlList.DataBind();
                        ddlList.SelectedValue = Request.QueryString["Id"];
                        TName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ddlList.SelectedItem.Text);
                        lblddlname.Text = "Product:";
                        Url = ddlList.SelectedValue.Substring(0, 4) + "-" + ddlList.SelectedItem.Text.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower();
                        Url = "<a href='" + SiteUrl + "'>Home</a> >> <a href='" + SiteUrl + "railpasses'>Rail Passes</a> >> <a href='" + (SiteUrl + Url) + "'>" + TName + "</a> >> Pass Details";
                        BindData(true);
                        break;
                    case "speciltrains":
                        lblHeader.Text = "Speciltrains Breadcrumbs";
                        ddlList.DataSource = master.Getspecialtrainlist(_SiteID);
                        ddlList.DataValueField = "Id";
                        ddlList.DataTextField = "Name";
                        ddlList.DataBind();
                        ddlList.SelectedValue = Request.QueryString["Id"];
                        TName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ddlList.SelectedItem.Text);
                        lblddlname.Text = "Specil train:";
                        Url = "<a href='" + SiteUrl + "'>Home</a> >> <a href='" + SiteUrl + "Specialtrains'>Special Trains</a> >> " + TName;
                        BindData();
                        break;
                    case "countries":
                        lblHeader.Text = "Countries Breadcrumbs";
                        ddlList.DataSource = master.Getcountrylist(_SiteID);
                        ddlList.DataValueField = "Id";
                        ddlList.DataTextField = "Name";
                        ddlList.DataBind();
                        ddlList.SelectedValue = Request.QueryString["Id"];
                        TName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ddlList.SelectedItem.Text);
                        lblddlname.Text = "Country:";
                        Url = "<a href='" + SiteUrl + "'>Home</a> >> <a href='" + SiteUrl + "Countries.aspx'>Countries</a> >> " + TName + "";
                        BindData();
                        break;
                    case "otherpages":
                        lblHeader.Text = "Other Pages Breadcrumbs";
                        ddlList.DataSource = master.Getotherpagelist(_SiteID);
                        ddlList.DataValueField = "Id";
                        ddlList.DataTextField = "Name";
                        ddlList.DataBind();
                        ddlList.SelectedValue = Request.QueryString["Id"];
                        TName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ddlList.SelectedItem.Text);
                        lblddlname.Text = "Page:";
                        Url = ddlList.SelectedItem.Text.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower();
                        if (ddlList.SelectedItem.Text.ToLower() != "home")
                            Url = "<a href='" + SiteUrl + "'>Home</a> >> " + TName;
                        BindData();
                        break;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool IsCategory = false;
            if (Request.QueryString["Name"] != null && Request.QueryString["Name"].ToString() == "railpassdetails")
                IsCategory = true;
            Guid Id = Guid.Parse(ddlList.SelectedValue);
            master.Savelinkbody(txtSaveData.Text, Id, _SiteID, chkActive.Checked, IsCategory);
            ShowMessage(1, "Successfully updated.");
            BindData(IsCategory);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string SEORequest = Request.QueryString["Name"];
            Response.Redirect("SEObreadcrumbsList.aspx?ID=" + SEORequest);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        public void BindData(bool IsCategory = false)
        {
            if (ddlList != null && !string.IsNullOrEmpty(ddlList.SelectedValue))
            {
                if (IsCategory)
                {
                    var Data = master.GetlinkbodyCategory(Guid.Parse(ddlList.SelectedValue), _SiteID);
                    if (Data != null)
                    {
                        if (!string.IsNullOrEmpty(Data.SourceCode))
                            txtSaveData.Text = literalbodydata.Text = Data.SourceCode;
                        chkActive.Checked = Data.IsActive;
                    }
                    else
                    {
                        txtSaveData.Text = Url;
                        literalbodydata.Text = string.Empty;
                    }
                }
                else
                {
                    var Data = master.Getlinkbody(Guid.Parse(ddlList.SelectedValue), _SiteID);
                    if (Data != null)
                    {
                        if (!string.IsNullOrEmpty(Data.SourceCode))
                            txtSaveData.Text = literalbodydata.Text = Data.SourceCode;
                        chkActive.Checked = Data.IsActive;
                    }
                    else
                    {
                        txtSaveData.Text = Url;
                        literalbodydata.Text = string.Empty;
                    }
                }
            }
        }

        protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}