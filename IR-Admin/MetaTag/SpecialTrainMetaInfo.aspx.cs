﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.MetaTag
{
    public partial class SpecialTrainMetaInfo : Page
    {
        private readonly Masters _oMasters = new Masters();
        private readonly ManageSeo _masterSeo = new ManageSeo();
        public string tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindSpecialTrainSeo(_siteID);
            ShowMessage(0, "");
        }
        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                ViewState["PreSiteID"] = _siteID;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                SiteSelected();
                PageLoadEvent();
                BindSpecialTrainSeo(_siteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    tab = "2";
                    btnSubmit.Text = "Update";
                    BindSpTrainSeoByID(Guid.Parse(Request["id"]));
                }
            }
        }

        private void PageLoadEvent()
        {
            try
            {
                //--Bind Site Tree
                IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
                foreach (var oSite in objSite)
                {
                    var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                    trSites.Nodes.Add(trCat);
                }

                //--Bind Special Train 
                _siteID = Master.SiteID;
                SiteSelected();
                var list = _masterSeo.GetSpecialTrainList();
                ddlSpecialTrain.DataSource = list;
                ddlSpecialTrain.DataTextField = "Name";
                ddlSpecialTrain.DataValueField = "ID";
                ddlSpecialTrain.DataBind();
                ddlSpecialTrain.Items.Insert(0, new ListItem("--Select Special Train--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BindSpecialTrainSeo(Guid siteID)
        {
            try
            {
                grdSpTrain.DataSource = _masterSeo.GetSpecialTrainSeoList(siteID);
                grdSpTrain.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindSpTrainSeoByID(Guid Id)
        {
            try
            {
                var result = _masterSeo.GetspTrainSeoById(Id);
                if (result != null)
                {
                    ddlSpecialTrain.SelectedValue = Guid.Parse(result.SpecialTrainID.ToString()).ToString();
                    txtTitle.Text = result.Title;
                    txtKewords.Text = result.Keywords;
                    txtDescription.Text = result.Description;
                    foreach (TreeNode item in trSites.Nodes)
                    {
                        if (Guid.Parse(item.Value) == result.SiteID)
                            item.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdSpTrain_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                var result = _masterSeo.DeleteSpTrainSeo(Guid.Parse(id.ToString()));
                if (result)
                    ShowMessage(1, "Record deleted successfully");
                _siteID = Master.SiteID;
                SiteSelected();
                BindSpecialTrainSeo(_siteID);
                tab = "1";
            }
        }

        protected void grdSpTrain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSpTrain.PageIndex = e.NewPageIndex;
            _siteID = Master.SiteID;
            SiteSelected();
            BindSpecialTrainSeo(_siteID);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var tn = trSites.CheckedNodes.Count;
            if (tn == 0)
            {
                ShowMessage(2, "Please Select Site");
                tab = "2";
                return;
            }
            foreach (TreeNode node in trSites.Nodes)
            {
                if (node.Checked)
                {
                    AddEditSpecialTrainInfo(Guid.Parse(node.Value));
                }
            }
            if (Request["id"] != null)
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            ShowMessage(1, Request["id"] == null ? "Record added successfully." : "Record updated successfully.");

            _siteID = Master.SiteID;
            SiteSelected();
            BindSpecialTrainSeo(_siteID);
            ClearControls();
            tab = "1";
        }

        void AddEditSpecialTrainInfo(Guid siteNodeValue)
        {
            _masterSeo.AddEditSpecialTrainSeo(new tblSpecialTrainMetaInfo
            {
                ID = Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid(),
                SpecialTrainID = Guid.Parse(ddlSpecialTrain.SelectedValue),
                Title = txtTitle.Text.Trim(),
                Keywords = txtKewords.Text.Trim(),
                Description = txtDescription.Text.Trim(),
                SiteID = siteNodeValue,
                CreatedOn = DateTime.Now,
                CreatedBy = AdminuserInfo.UserID
            });
        }

        void ClearControls()
        {
            ddlSpecialTrain.SelectedIndex = 0;
            txtTitle.Text = string.Empty;
            txtKewords.Text = string.Empty;
            txtDescription.Text = string.Empty;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SpecialTrainMetaInfo.aspx");
        }
    }
}