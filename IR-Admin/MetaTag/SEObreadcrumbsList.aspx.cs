﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.MetaTag
{
    public partial class SEObreadcrumbsList : Page
    {
        public Guid _SiteID;
        public string TitleName = string.Empty;
        #region [ Page InIt must write on every page of CMS ]

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            GridBind();
        }

        #endregion

        public ManageSeo master = new ManageSeo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Request.QueryString["ID"] != null)
                GridBind();
        }

        public void GridBind()
        {
            _SiteID = Master.SiteID;
            var data = new List<SeoDropdownList>();
            string SEORequest = Request.QueryString["ID"];
            TitleName = SEORequest;
            switch (SEORequest)
            {
                case "railpass":
                    lblHeader.Text = "Rail Passes Breadcrumbs";
                    data = master.GetListDataSEO(master.Getproductlist(_SiteID), _SiteID);
                    grdseo.DataSource = data;
                    grdseo.DataBind();
                    break;
                case "railpassdetails":
                    lblHeader.Text = "Rail Pass Details Breadcrumbs";
                    data = master.GetListDataSEO(master.Getproductlist(_SiteID), _SiteID,true);
                    grdseo.DataSource = data;
                    grdseo.DataBind();
                    break;
                case "speciltrains":
                    lblHeader.Text = "Speciltrains Breadcrumbs";
                    data = master.GetListDataSEO(master.Getspecialtrainlist(_SiteID), _SiteID);
                    grdseo.DataSource = data;
                    grdseo.DataBind();
                    break;
                case "countries":
                    lblHeader.Text = "Countries Breadcrumbs";
                    data = master.GetListDataSEO(master.Getcountrylist(_SiteID), _SiteID);
                    grdseo.DataSource = data;
                    grdseo.DataBind();
                    break;
                case "otherpages":
                    lblHeader.Text = "Other Pages Breadcrumbs";
                    data = master.GetListDataSEO(master.Getotherpagelist(_SiteID), _SiteID);
                    grdseo.DataSource = data;
                    grdseo.DataBind();
                    break;
            }
        }

        protected void grdseo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdseo.PageIndex = e.NewPageIndex;
            GridBind();
        }

        protected void grdseo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                _SiteID = Master.SiteID;
                if (e.CommandName == "ActiveInActive")
                {
                    ShowMessage(0, "");
                    var Id = Guid.Parse(e.CommandArgument.ToString());
                    string result = master.Activelinkbody(Id, _SiteID);
                    GridBind();
                    if (!string.IsNullOrEmpty(result))
                        ShowMessage(2, result);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}