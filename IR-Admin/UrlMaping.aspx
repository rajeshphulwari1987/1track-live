﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UrlMaping.aspx.cs" Inherits="IR_Admin.UrlMaping" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        URL Redirection</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <ul class="list">
        <li><a id="aList" href="UrlMaping.aspx" class="current">List</a></li>
        <li>
            <asp:HyperLink ID="aNew" NavigateUrl="AddEditUrlMapping.aspx" CssClass=" " runat="server">New/Edit</asp:HyperLink></li>
    </ul>
    <!-- tab "panes" -->
    <div class="full mr-tp1">
        <div class="panes">
            <div id="divlist" runat="server">
                <div class="crushGvDiv" style="font-size: 13px;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <legend><b>URL Redirection</b></legend>
                                    <asp:GridView ID="grvURL" runat="server" AutoGenerateColumns="False" PageSize="10"
                                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                        AllowPaging="True" OnPageIndexChanging="grvURL_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                        <PagerStyle CssClass="paging"></PagerStyle>
                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                            BorderColor="#FFFFFF" BorderWidth="1px" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            Record not found.</EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Srno.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SiteName">
                                                <ItemTemplate>
                                                    <%#Eval("SiteName")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="20%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Notes">
                                                <ItemTemplate>
                                                    <%#Convert.ToString(Eval("Note")).Length > 50 ? Convert.ToString(Eval("Note")).Substring(0, 50) + "...." : Convert.ToString(Eval("Note"))%>
                                                </ItemTemplate>
                                                <ItemStyle Width="30%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                        OnCommand="imgActive_OnCommand" Height="16" CommandName="Active" AlternateText="status"
                                                        ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this Url?');" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <a href='AddEditUrlMapping.aspx?Id=<%#Eval("Id") %>'>
                                                        <img src="images/edit.png" alt='' style="borderwidth: 0px;" /></a> &nbsp;
                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete.png" CommandArgument='<%#Eval("Id")%>'
                                                        OnCommand="imgDelete_OnCommand" Text="Delete" Height="16" CommandName="Delete"
                                                        AlternateText="Delete" OnClientClick="return confirm('Are you sure you want to Delete this Url?');" />
                                                </ItemTemplate>
                                                <ItemStyle Width="10%"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
