﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Pages.aspx.cs" Inherits="IR_Admin.Pages" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #header
        {
            float: none;
            height: 160px;
            margin: 0 auto;
            width: 1006px;
        }
        
        .content
        {
            float: none;
            margin: 0 auto;
            width: 1010px;
        }
        
        .grid-sec2
        {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #D5D5D5;
            float: left;
            width: 1010px;
        }
        .err
        {
            width: 120px;
            height: 33px;
            line-height: 33px;
            background: url(images/err-grey.png) no-repeat 0 -3px;
            position: absolute;
            padding: 0 0px 0px 18px !important;
            color: #fff !important;
            font-size: 12px !important;
        }
    </style>
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {
            if ('<%=Tab.ToString()%>' == "1") {
                $("ul.list").tabs("div.panes > div");
            }
        });
        $(window).load(function () {
            $('#MainContent_myIframe').contents().find('.edit-btn').on("click", function () {
                $('html, body').animate({ scrollTop: "1150px" }, 500);
            });
        });

        function ResetDiv() {
            document.getElementById('aNew').className = 'current';
            document.getElementById('aList').className = '';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }

        window.scrollTo = function () {
        };
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }

        function bindUrl() {
            var value = $('[id$=MainContent_hdnUrl]').val() + $('input[name="ctl00$MainContent$txtPageName"]').val();
            $('[id$=MainContent_lblUrl]').text(value.replace(" ", "-"));
        }
    </script>
    <script type="text/javascript">
        function saveFinal() {
            var lblHeadText = $("#MainContent_myIframe").contents().find("#ContentHead").html();
            $("#MainContent_hdnHeading").val(lblHeadText);

            var lblContent = $("#MainContent_myIframe").contents().find("#ContentText").html();
            $("#MainContent_hdnContent").val(lblContent);

            /*prduct head*/
            var lblPrdHead = $("#MainContent_myIframe").contents().find("#ContentPrdHead").html();
            $("#MainContent_hdnPrdHead").val(lblPrdHead);

            /*prduct desc*/
            var lblPrdDesc = $("#MainContent_myIframe").contents().find("#ContentPrdDesc").html();
            $("#MainContent_hdnPrdDesc").val(lblPrdDesc);

            /**/
            var footerBlockheadertxt = $("#MainContent_myIframe").contents().find("#footerBlockheadertxt").html();
            $("#MainContent_hdnfooterBlockheadertxt").val(footerBlockheadertxt);

            var lblFooter = $("#MainContent_myIframe").contents().find("#footerBlock").html();
            $("#MainContent_hdnFooter").val(lblFooter);

            var lblRight = $("#MainContent_myIframe").contents().find("#hdnRtImgIDs").val();
            $("#MainContent_hdnRight").val(lblRight);

            var lblRightPanelUrl = $("#MainContent_myIframe").contents().find("#rightNav").html();
            if (lblRightPanelUrl != null) {
                if (lblRightPanelUrl.indexOf('<span id="lblrtNav"></span>') > "-1") {
                    $("#MainContent_hdnRightPanelUrl").val("0");
                } else {
                    $("#MainContent_hdnRightPanelUrl").val(lblRightPanelUrl);
                }
            }

            var lblBanner = $("#MainContent_myIframe").contents().find("#hdnBannerIDs").val();
            $("#MainContent_hdnBanner").val(lblBanner);

            var lblhdnRtPanel1 = $("#MainContent_myIframe").contents().find("#rtPannel1").html();
            $("#MainContent_hdnRtPanel1").val(lblhdnRtPanel1);

            var lblhdnRtPanel2 = $("#MainContent_myIframe").contents().find("#rtPannel2").html();
            $("#MainContent_hdnRtPanel2").val(lblhdnRtPanel2);

            var lblhdnCntPanel = $("#MainContent_myIframe").contents().find("#addBlock").html();
            $("#MainContent_hdnContactPanel").val(lblhdnCntPanel);

            var lblhdnCntCall = $("#MainContent_myIframe").contents().find("#callBlock").html();
            $("#MainContent_hdnContactCall").val(lblhdnCntCall);

            /*show/hide hdnProductRtImg page controls*/
            var hdnProductRtImg = $("#MainContent_myIframe").contents().find("#hdnProductRtImg").val();
            //alert(hdnProductRtImg);
            $("#MainContent_hdnProductRtImg").val(hdnProductRtImg);

            var hdnPrdContent = $("#MainContent_myIframe").contents().find("#hdnContent").val();
            //alert(hdnPrdContent);
            $("#MainContent_hdnPrdContent").val(hdnPrdContent);

            var hdnPrdImagePanel = $("#MainContent_myIframe").contents().find("#hdnImagePanel").val();
            //alert(hdnPrdImagePanel);
            $("#MainContent_hdnPrdImagePanel").val(hdnPrdImagePanel);

            var hdnPrdRtImageSlider = $("#MainContent_myIframe").contents().find("#hdnRtImageSlider").val();
            //alert(hdnPrdRtImageSlider);
            $("#MainContent_hdnPrdRtImageSlider").val(hdnPrdRtImageSlider);

            //Home page heading text and description
            var lblHeading1Text = $("#MainContent_myIframe").contents().find("#ContentHeading1").html();
            $("#MainContent_hdnHeading1").val(lblHeading1Text);

            var lblHeading2Text = $("#MainContent_myIframe").contents().find("#ContentHeading2").html();
            $("#MainContent_hdnHeading2").val(lblHeading2Text);

            var lblDescription1Text = $("#MainContent_myIframe").contents().find("#ContentDescription1").html();
            $("#MainContent_hdnDescription1").val(lblDescription1Text);

            var lblDescription2Text = $("#MainContent_myIframe").contents().find("#ContentDescription2").html();
            $("#MainContent_hdnDescription2").val(lblDescription2Text);


            /**VE Loading**/
            var PhdnVEbanner = $("#MainContent_myIframe").contents().find("#hdnVEbanner").val();
            $("#PhdnVEbanner").val(PhdnVEbanner);
            var PhdnVEptpimg = $("#MainContent_myIframe").contents().find("#hdnVEptpimg").val();
            $("#PhdnVEptpimg").val(PhdnVEptpimg);

            var PhdnVEmap = $("#MainContent_myIframe").contents().find("#hdnVEmap").val();
            $("#PhdnVEmap").val(PhdnVEmap);
            var PhdnVEmaps1 = $("#MainContent_myIframe").contents().find("#hdnVEmaps1").val();
            $("#PhdnVEmaps1").val(PhdnVEmaps1);
            var PhdnVEmaps2 = $("#MainContent_myIframe").contents().find("#hdnVEmaps2").val();
            $("#PhdnVEmaps2").val(PhdnVEmaps2);

            var PhdnVEmaplink = $("#MainContent_myIframe").contents().find("#hdnVEmaplink").val();
            $("#PhdnVEmaplink").val(PhdnVEmaplink);
            var PhdnVEmaps1link = $("#MainContent_myIframe").contents().find("#hdnVEmaps1link").val();
            $("#PhdnVEmaps1link").val(PhdnVEmaps1link);
            var PhdnVEmaps2link = $("#MainContent_myIframe").contents().find("#hdnVEmaps2link").val();
            $("#PhdnVEmaps2link").val(PhdnVEmaps2link);

            var PhdnVEpass = $("#MainContent_myIframe").contents().find("#hdnVEpass").val();
            $("#PhdnVEpass").val(PhdnVEpass);

            var PhdnVEtrain = $("#MainContent_myIframe").contents().find("#hdnVEtrain").val();
            $("#PhdnVEtrain").val(PhdnVEtrain);

            var PhdnVEnewadventure = $("#MainContent_myIframe").contents().find("#hdnVEnewadventure").val();
            $("#PhdnVEnewadventure").val(PhdnVEnewadventure);

            var PhdnVEbtncontactcall = $("#MainContent_myIframe").contents().find("#hdnVEbtncontactcall").val();
            $("#PhdnVEbtncontactcall").val(PhdnVEbtncontactcall);
            var PhdnVEbtncontactbook = $("#MainContent_myIframe").contents().find("#hdnVEbtncontactbook").val();
            $("#PhdnVEbtncontactbook").val(PhdnVEbtncontactbook);
            var PhdnVEbtncontactlive = $("#MainContent_myIframe").contents().find("#hdnVEbtncontactlive").val();
            $("#PhdnVEbtncontactlive").val(PhdnVEbtncontactlive);
            var PhdnVEbtncontactemail = $("#MainContent_myIframe").contents().find("#hdnVEbtncontactemail").val();
            $("#PhdnVEbtncontactemail").val(PhdnVEbtncontactemail);

            var PhdnVEbtntext1 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext1").val();
            $("#PhdnVEbtntext1").val(PhdnVEbtntext1);
            var PhdnVEbtntext2 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext2").val();
            $("#PhdnVEbtntext2").val(PhdnVEbtntext2);
            var PhdnVEbtntext3 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext3").val();
            $("#PhdnVEbtntext3").val(PhdnVEbtntext3);
            var PhdnVEbtntext4 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext4").val();
            $("#PhdnVEbtntext4").val(PhdnVEbtntext4);
            var PhdnVEbtntext5 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext5").val();
            $("#PhdnVEbtntext5").val(PhdnVEbtntext5);
            var PhdnVEbtntext6 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext6").val();
            $("#PhdnVEbtntext6").val(PhdnVEbtntext6);
            var PhdnVEbtntext7 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext7").val();
            $("#PhdnVEbtntext7").val(PhdnVEbtntext7);
            var PhdnVEbtntext8 = $("#MainContent_myIframe").contents().find("#hdnVEbtntext8").val();
            $("#PhdnVEbtntext8").val(PhdnVEbtntext8);

            var lblContentValue2 = $("#MainContent_myIframe").contents().find("#ContentValue2").html();
            $("#MainContent_hdnRailTicketInnerHeading").val(lblContentValue2);
            var lblContentValue3 = $("#MainContent_myIframe").contents().find("#ContentValue3").html();
            $("#MainContent_hdnRailTicketSubInnerLower").val(lblContentValue3);
            var lblhdnBannerIDs1 = $("#MainContent_myIframe").contents().find("#hdnBannerIDs1").val();
            $("#MainContent_hdnLeftBannerImage").val(lblhdnBannerIDs1);
            var lblhdnBannerIDs2 = $("#MainContent_myIframe").contents().find("#hdnBannerIDs2").val();
            $("#MainContent_hdnRightBanner").val(lblhdnBannerIDs2);
        }

    </script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Manage Page
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Pages.aspx" class="current">List</a></li>
            <li><a id="aNew" href="Pages.aspx" class="">New/Edit </a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <%--VE Loading--%>
        <asp:HiddenField runat="server" ID="PhdnVEbanner" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEptpimg" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEmap" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEmaps1" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEmaps2" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEmaplink" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEmaps1link" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEmaps2link" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEpass" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEtrain" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEnewadventure" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtncontactcall" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtncontactbook" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtncontactlive" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtncontactemail" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext1" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext2" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext3" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext4" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext5" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext6" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext7" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="PhdnVEbtntext8" ClientIDMode="Static" />
        <%--Hidden Fields--%>
        <asp:HiddenField runat="server" ID="hdnHeading" />
        <asp:HiddenField runat="server" ID="hdnContent" />
        <asp:HiddenField runat="server" ID="hdnfooterBlockheadertxt" />
        <asp:HiddenField runat="server" ID="hdnFooter" />
        <asp:HiddenField runat="server" ID="hdnRight" />
        <asp:HiddenField runat="server" ID="hdnRightPanelUrl" />
        <asp:HiddenField runat="server" ID="hdnBanner" />
        <asp:HiddenField runat="server" ID="hdnRtPanel1" />
        <asp:HiddenField runat="server" ID="hdnRtPanel2" />
        <asp:HiddenField runat="server" ID="hdnContactPanel" />
        <asp:HiddenField runat="server" ID="hdnContactCall" />
        <asp:HiddenField runat="server" ID="hdnPrdHead" />
        <asp:HiddenField runat="server" ID="hdnPrdDesc" />
        <asp:HiddenField runat="server" ID="hdnProductRtImg" />
        <asp:HiddenField runat="server" ID="hdnPrdContent" />
        <asp:HiddenField runat="server" ID="hdnPrdImagePanel" />
        <asp:HiddenField runat="server" ID="hdnPrdRtImageSlider" />
        <asp:HiddenField runat="server" ID="hdnHeading1" />
        <asp:HiddenField runat="server" ID="hdnHeading2" />
        <asp:HiddenField runat="server" ID="hdnDescription1" />
        <asp:HiddenField runat="server" ID="hdnDescription2" />
        <asp:HiddenField runat="server" ID="hdnRailTicketInnerHeading" />
        <asp:HiddenField runat="server" ID="hdnRailTicketSubInnerLower" />
        <asp:HiddenField runat="server" ID="hdnLeftBannerImage" />
        <asp:HiddenField runat="server" ID="hdnRightBanner" />
        <%--Hidden Fields End--%>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdPages" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdPages_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Page Name">
                                    <ItemTemplate>
                                        <%#Eval("PageName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgRemove" CommandArgument='<%#Eval("ID")%>'
                                            OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                            Height="16" CommandName="Remove" AlternateText="status" ImageUrl="~/images/delete.png" />
                                        <a href="Pages.aspx?edit=<%#Eval("Id")%>" style="text-decoration: none">
                                            <img title="Edit" alt="Edit" src="images/edit.png" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No records found !</EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <div class="alternate-block02">
                        <table id="Maintable" class="tblMainSection" style="font-size: 14px">
                            <tr>
                                <td width="80%" valign="top" style="line-height: 35px">
                                    <table width="100%" cellpadding="5" cellspacing="5">
                                        <tr>
                                            <td>
                                                Site Name :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlSiteNm" runat="server" Width="250px" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlSiteNm_SelectedIndexChanged" />
                                                &nbsp;<asp:RequiredFieldValidator ID="rvSite" runat="server" ErrorMessage="Select Site Name"
                                                    CssClass="err" ControlToValidate="ddlSiteNm" ValidationGroup="rForm" InitialValue="-1" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Page Name:
                                            </td>
                                            <td width="80%">
                                                <asp:TextBox ID="txtPageName" runat="server" Width="250px" onkeyup="bindUrl()" AutoComplete="off" />
                                                &nbsp;<asp:RequiredFieldValidator ID="rvPageName" runat="server" ErrorMessage="Select Page Name"
                                                    CssClass="err" ControlToValidate="txtPageName" ValidationGroup="rForm" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                URL:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUrl" runat="server" />
                                                <asp:HiddenField ID="hdnUrl" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Navigation:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlNavigation" runat="server" Width="250px" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlNavigation_SelectedIndexChanged" />
                                                &nbsp;<asp:RequiredFieldValidator ID="rvNav" runat="server" ErrorMessage="Select Navigation"
                                                    CssClass="err" ControlToValidate="ddlNavigation" ValidationGroup="rForm" InitialValue="-1" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Layout:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlLayout" runat="server" Width="250px" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlLayout_SelectedIndexChanged" Enabled="False" />
                                                &nbsp;<asp:RequiredFieldValidator ID="rvLayout" runat="server" ErrorMessage="Select Layout"
                                                    CssClass="err" ControlToValidate="ddlLayout" ValidationGroup="rForm" InitialValue="-1" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                IsActive:
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkActive" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:Image ID="imgTemplate" Width="200px" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="alternate-block" id="dvFrame" runat="server" visible="True">
                        <table style="width: 100%;" cellpadding="5" cellspacing="5">
                            <tr>
                                <td>
                                    <h2>
                                        <asp:Label ID="lblFrameHead" runat="server" />
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <iframe id="myIframe" scrolling="yes" style="background-color: #fff; border-left: 0;
                                        border-right: 0; border-top: 4px solid #ECECEC; border-bottom: 4px solid #ECECEC;
                                        border-right: 4px solid #ECECEC;" runat="server" visible="False"></iframe>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="alternate-block02">
                        <table width="100%" cellpadding="5" cellspacing="5">
                            <tr>
                                <td colspan="2">
                                    <h2>
                                        Meta Data</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Meta Title:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMetaTitle" runat="server" Width="600px" />
                                    &nbsp;<asp:RequiredFieldValidator ID="rvMeta" runat="server" ErrorMessage="Enter Meta Title"
                                        CssClass="err" ControlToValidate="txtMetaTitle" ValidationGroup="rForm" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    Description:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Columns="60"
                                        Rows="5" Width="600px" />
                                    &nbsp;<asp:RequiredFieldValidator ID="rvDesp" runat="server" ErrorMessage="Enter Description"
                                        ControlToValidate="txtDescription" ValidationGroup="rForm" Display="Dynamic"
                                        CssClass="err" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    Keyword:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtKeyWord" runat="server" TextMode="MultiLine" Columns="60" Rows="5"
                                        Width="600px" />
                                    &nbsp;<asp:RequiredFieldValidator ID="rvKeyword" runat="server" ErrorMessage="Enter Keyword"
                                        ControlToValidate="txtKeyWord" ValidationGroup="rForm" Display="Dynamic" CssClass="err" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                        OnClientClick="saveFinal();" ValidationGroup="rForm" Text="SAVE ALL CHANGES"
                                        CommandName="Submit" Width="160px" />
                                    <asp:ValidationSummary ID="vs" runat="server" ValidationGroup="rForm" ShowSummary="False"
                                        ShowMessageBox="True" EnableClientScript="True" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
