﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class MandatoryFields : Page
    {
        private readonly Masters _oMasters = new Masters();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            FillGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            if (!IsPostBack)
            {
                btnSubmit.Enabled = true;
                if (Request["id"] != null)
                {
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                PageLoadEvent();
                GetDetailsByEdit();

                _SiteID = Master.SiteID;
                SiteSelected();
                FillGrid(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        private void PageLoadEvent()
        {
            ddlPageName.DataSource = _oMasters.GetPageNameList();
            ddlPageName.DataTextField = "Name";
            ddlPageName.DataValueField = "ID";
            ddlPageName.DataBind();
            ddlPageName.Items.Insert(0, new ListItem("--Page--", "-1"));

            IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        protected void ddlPageName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var id = ddlPageName.SelectedValue != "0" ? Guid.Parse(ddlPageName.SelectedValue) : new Guid();
                {
                    ShowMessage(0, "");
                    BindControls(id);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void BindControls(Guid id)
        {
            tab = "2";
            IEnumerable<tblPageControl> objControls = _oMasters.GetPageControlList(id);
            if (objControls.Any())
            {
                chkControls.Visible = true;
                foreach (var oControl in objControls)
                {
                    var trCat = new TreeNode
                        {
                            Text = oControl.ControlName,
                            Value = oControl.ID.ToString(),
                            SelectAction = TreeNodeSelectAction.None
                        };
                    trControls.Nodes.Add(trCat);
                }

                var rec = _oMasters.GetControlsByPageId(Guid.Parse(ddlPageName.SelectedValue));
                if (rec != null)
                {
                    foreach (TreeNode itm in trControls.Nodes)
                    {
                        itm.Checked = rec.ListControlId.Contains(Guid.Parse(itm.Value));
                    }
                }
            }
            else
            {
                chkControls.Visible = false;
                trControls.Nodes.Clear();
            }
        }

        public void FillGrid(Guid siteID)
        {
            grdControl.DataSource = _oMasters.GetMandatoryFieldList(siteID);
            grdControl.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditMandatoryFields();
            tab = "1";
            ViewState["tab"] = "1";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("MandatoryFields.aspx");
        }

        public void AddEditMandatoryFields()
        {
            try
            {
                var id = string.IsNullOrEmpty(Request.QueryString["id"]) ? new Guid() : Guid.Parse(Request.QueryString["id"]);
                if (Request.QueryString["id"] == null)
                {
                    var siteId = (from TreeNode node in trSites.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();
                    foreach (var guid in siteId)
                    {
                        var chk = _oMasters.ChkPageExists(Guid.Parse(ddlPageName.SelectedValue), guid);
                        if (chk)
                        {
                            ShowMessage(2, "Page Already Exists.");
                            tab = "2";
                            return;
                        }
                    }
                }
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    tab = "2";
                    return;
                }
                int tc = trControls.CheckedNodes.Count;
                if (tc == 0)
                {
                    ShowMessage(2, "Please Select Controls");
                    tab = "2";
                    return;
                }
                else
                {
                    var listSitId = (from TreeNode node in trSites.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();
                    var listControlId = (from TreeNode node in trControls.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();
                    var res = _oMasters.AddMandatoryFields(new MandatoryField
                        {
                            ID = id,
                            PageID = Guid.Parse(ddlPageName.SelectedValue),
                            ListSiteId = listSitId,
                            ListControlId = listControlId,
                            IsMandatory = true
                        });
                    ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "Mandatory field details updated successfully." : "Mandatory field details added successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ClearControls();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlPageName.SelectedIndex = 0;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        public void GetDetailsByEdit()
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    return;

                btnSubmit.Text = "Update";
                var id = Guid.Parse(Request.QueryString["id"]);
                var rec = _oMasters.GetMandatoryFieldsById(id);
                ddlPageName.SelectedValue = rec.PageID.ToString();

                if (ddlPageName.SelectedValue != "-1")
                    BindControls(Guid.Parse(ddlPageName.SelectedValue));

                foreach (TreeNode itm in trSites.Nodes)
                {
                    itm.Checked = rec.ListSiteId.Contains(Guid.Parse(itm.Value));
                }
                foreach (TreeNode itm in trControls.Nodes)
                {
                    itm.Checked = rec.ListControlId.Contains(Guid.Parse(itm.Value));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void grdControl_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                bool res = _oMasters.DeleteMandatoryFields(id);
                if (res)
                    ShowMessage(1, "Record deleted Successfully.");
            }
            _SiteID = Master.SiteID;
            SiteSelected();
            FillGrid(_SiteID);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdControl_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdControl.PageIndex = e.NewPageIndex;
            FillGrid(_SiteID);
        }
    }
}