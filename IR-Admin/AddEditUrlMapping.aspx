﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AddEditUrlMapping.aspx.cs" Inherits="IR_Admin.AddEditUrlMapping" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style>
        .linktxt
        {
            color: #061D3C;
            cursor: pointer;
            text-decoration: underline;
            margin-left: 10px;
            font-size: 14px;
            font-weight: bold;
        }
        .marg5
        {
            margin: 5px;
        }
    </style>
    <script language="javascript" type="text/javascript">

     function ShowHidePass() {
        alert('hi');
        var Pass = $('#txtPassword');
        if (Pass.type == 'password') {
            Pass.type =  'text';
            lnk.innerHTML = "Hide Password";
        }
        else {
            Pass.type =  = 'password';
            lnk.innerHTML = "Show Password";
         }
         return false;
        }

    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        URL Redirection
    </h2>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
 <div class="full mr-tp1" style="font-size:13px">
            <div class="panes">
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" style="width: 250px;">
                                Site Name:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" runat="server" Width="600px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Old URL:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtOldURL" runat="server" MaxLength="400" Width="600px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfOLDURL" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtOldURL" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                New URL:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtNewURL" runat="server" MaxLength="400" Width="600px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfNewURL" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtNewURL" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Default URL:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtDefaultURL" runat="server" MaxLength="400" Width="600px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfDefaultURL" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="txtDefaultURL" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Note:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtNote" runat="server" MaxLength="400" Width="600px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="rv" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" PostBackUrl="~/UrlMaping.aspx"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
