﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class currencyPage : Page
    {
        readonly Masters _Master = new Masters();
        public string tab = "1";
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["tab"] != null)
                tab = ViewState["tab"].ToString();
            if (!Page.IsPostBack)
            {
                btnSubmit.Text = "Submit";
                BindCurrencyList();
            }
        }

        public void BindCurrencyList()
        {
            try
            {
                grdCurrency.DataSource = _Master.GetCurrencyList();
                grdCurrency.DataBind();
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }
        public void BindCurrencyListForEdit(Guid ID)
        {
            try
            {
                var result = _Master.GetCurrencyListEdit(ID);
                txtCName.Text = result.Name;
                txtCCode.Text = result.Symbol;
                txtHtmlcode.Text = result.HTMLCode;
                txtShortcode.Text = result.ShortCode;
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive) ? true : false;
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(hdnCurrencyID.Value.ToString()))
                {
                    var _curency = new Currency();
                    _curency.Name = txtCName.Text;
                    _curency.ID = Guid.NewGuid();
                    _curency.IsActive = chkactive.Checked == true ? true : false;
                    _curency.HTMLCode = txtHtmlcode.Text;
                    _curency.ShortCode = txtShortcode.Text;
                    _curency.Symbol = txtCCode.Text;

                    int res = _Master.AddCurrency(_curency);
                    if (res > 0)
                    {
                        ShowMessage(1, "Currency added successfully .");
                        txtCCode.Text = string.Empty;
                        txtHtmlcode.Text = string.Empty;
                        txtShortcode.Text = string.Empty;
                        txtCName.Text = string.Empty;
                        BindCurrencyList();

                    }
                }
                else
                {
                    var _curency = new Currency();
                    _curency.Name = txtCName.Text;
                    _curency.ID = Guid.Parse(hdnCurrencyID.Value);
                    _curency.IsActive = chkactive.Checked;
                    _curency.HTMLCode = txtHtmlcode.Text;
                    _curency.ShortCode = txtShortcode.Text;
                    _curency.Symbol = txtCCode.Text;
                    int res = _Master.UpdateCurrency(_curency);
                    if (res > 0)
                    {
                        ShowMessage(1, "Currency updated successfully.");
                        btnSubmit.Text = "Submit";
                        txtCCode.Text = string.Empty;
                        txtHtmlcode.Text = string.Empty;
                        txtShortcode.Text = string.Empty;
                        txtCName.Text = string.Empty;
                        BindCurrencyList();
                    }
                }
                tab = "1";
                ViewState["tab"] = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("currency.aspx");
        }

        protected void grdCurrency_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            var currencyID = Guid.Parse(e.CommandArgument.ToString());
            DivError.Style.Add("display", "none");
            DivSuccess.Style.Add("display", "none");
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        hdnCurrencyID.Value = currencyID.ToString();
                        tab = "2";
                        ViewState["tab"] = "2";
                        BindCurrencyListForEdit(currencyID);
                        break;
                    }
                case "ActiveInActive":
                    {
                        _Master.ActiveInActiveCurrency(currencyID);
                        Response.Redirect("currency.aspx");
                        break;
                    }
            }
        }
    }
}
