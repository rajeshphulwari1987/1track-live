﻿<%@ Page Title="Navigation " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Navigation.aspx.cs" Inherits="IR_Admin.NavigationPage" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .tblMainSection
        {
            width: 100%;
        }
        .styCombo
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Add/ Edit Navigation Master</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="failureNotification" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="failureNotification" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <table class="tblMainSection">
        <tr>
            <td>
                Navigation Name
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtNameRequiredFieldValidator" runat="server" ControlToValidate="txtName"
                    ErrorMessage="Name Required" ValidationGroup="NavForm"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="styCombo">
                Parent Navigation
            </td>
            <td class="style2">
                <asp:DropDownList ID="ddlParentCategory" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Navigation Url
            </td>
            <td>
                <asp:TextBox ID="txtPageurl" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Show on Top Navigation
            </td>
            <td>
                <asp:CheckBox ID="chkIsTop" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Show on Footer Navigation
            </td>
            <td>
                <asp:CheckBox ID="chkIsBottom" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Is Active
            </td>
            <td>
                <asp:CheckBox ID="chkActive" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" CssClass="button"  OnClick="btnSubmit_Click" Text="Submit"
                    Width="89px" ValidationGroup="NavForm" />
                &nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button"  OnClick="btnCancel_Click" Text="Cancel" />
            </td>
            <asp:HiddenField ID="hdID" runat="server" Value="" />
        </tr>
    </table>
</asp:Content>
