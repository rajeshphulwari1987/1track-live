﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ManualP2PSuppliers.aspx.cs"
    Inherits="IR_Admin.ManualP2PSuppliers" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">    
        function keycheck() {
        var KeyID = event.keyCode;
        if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
            return true;
        return false;
    }
        
     window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(function () {
            
            if (<%=Tab%> == '1') {
                $("ul.list").tabs("div.panes > div");
                    
               if($(location).attr('href').indexOf('?') > "-1") {
                   $(location).attr('href', $(location).attr('pathname'));
                }
            }
        });

        function ResetDiv() {
            document.getElementById('aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Manual P2P suppliers
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ManualP2PSuppliers.aspx" class="current">List</a></li>
            <li><a id="aNew" href="ManualP2PSuppliers.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:GridView ID="grdCommition" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdCommition_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Commission Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Commission Fee">
                                    <ItemTemplate>
                                        <%#Eval("Commition")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Commission(%)">
                                    <ItemTemplate>
                                        <input type="checkbox" <%#Convert.ToBoolean(Eval("Ispercentage"))?"checked":string.Empty%>
                                            disabled="disabled" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href='ManualP2PSuppliers.aspx?id=<%#Eval("ID")%>'>
                                            <img src="images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                            CommandName="Remove" ImageUrl="~/images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td valign="top">
                                <table class="tblMainSection">
                                    <tr>
                                        <td class="col">
                                            Commission Name:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtcomname" runat="server" />
                                            &nbsp;<asp:RequiredFieldValidator ID="req1" runat="server" ErrorMessage="*" ForeColor="Red"
                                                ControlToValidate="txtcomname" ValidationGroup="rv" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Supplier Currency:
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlCurrency" runat="server" />
                                            &nbsp;<asp:RequiredFieldValidator ID="reqCurrency" runat="server" ErrorMessage="*" ValidationGroup="rv"
                                                InitialValue="-1" ControlToValidate="ddlCurrency" ForeColor="Red" SetFocusOnError="True" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Commission Fee:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtcommFee" runat="server" />
                                            &nbsp;<asp:RequiredFieldValidator ID="req2" runat="server" ErrorMessage="*" ForeColor="Red"
                                                ControlToValidate="txtcommFee" ValidationGroup="rv" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Commission(%):
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkisper" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                                Text="Submit" Width="89px" ValidationGroup="rv" />
                                            &nbsp;
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                                Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
