﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;

public partial class PaymentProcess : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly private ManageOrder _master = new ManageOrder();

    public string currency = "$";
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static Guid currencyID = new Guid();
    Guid siteId;
    public string script = "<script></script>";
    public string products = "";
    public string siteURL = "";
    public string OrderNo = string.Empty;
    public string linkURL = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null)
        {
            OrderNo = Request.Params["req"];
            linkURL = "?req=" + OrderNo;
        }
        else if (Session["OrderID"] != null)
            OrderNo = Session["OrderID"].ToString();

        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
        GetCurrencyCode();
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                long OrderId = Convert.ToInt64(OrderNo);
                var UpdateAgent = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId);
                if (UpdateAgent != null)
                    UpdateAgent.AgentID = AgentuserInfo.UserID;
                _db.SaveChanges();
            }
            var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (objsite != null && objsite.IsAgent.HasValue ? objsite.IsAgent.Value : false)
            {
                if (Session["AgentUsername"] == null)
                {
                    Session.Add("redirectpage", linkURL);
                    btnContinue.Visible = false;
                    btnAgentLogon.Visible = true;
                }
                else
                {
                    if (Session["redirectpage"] != null)
                        Session.Remove("redirectpage");

                    btnAgentLogon.Visible = false;
                    btnContinue.Visible = true;
                }
            }
            if (objsite != null && objsite.IsAgent == false)
            {
                //trNetPrice.Visible = false;
            }
            ShowPaymentDetails();
            QubitOperationLoad();
        }
    }
    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    public void ShowPaymentDetails()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = from a in lst
                             select new { Name = a.FirstName + " " + a.LastName, Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)), ProductDesc = (new ManageBooking().getPassDescType(a.PassSaleID, a.ProductType, (string) (Session["TrainType"]))), TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0) };
                if (lst.Count > 0)
                {
                    rptOrderInfo.DataSource = lstNew;
                    lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                    lblDeliveryAddress.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName + ", " + lst.FirstOrDefault().Address1 + ", " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + ", " : string.Empty) + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry + ", " + lst.FirstOrDefault().Postcode;
                    lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();

                    lblShippingAmount.Text = lst.FirstOrDefault().ShippingAmount.ToString();
                    lblGrandTotal.Text = ((lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount + lst.FirstOrDefault().BookingFee).ToString();
                    lblBookingFee.Text = Convert.ToDecimal(lst.FirstOrDefault().BookingFee).ToString("F2");
                    hdnUserName.Value = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                }
                else
                    rptOrderInfo.DataSource = null;
                rptOrderInfo.DataBind();
            }
            else
            {
                Response.Redirect("~/Home", false);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void btnAgentLogon_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Agent/");
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        nextStepGoto();
    }
    public void nextStepGoto()
    {
        if (!string.IsNullOrEmpty(OrderNo))
        {
            var objPT = new PaymentGateWayTransffer();
            objPT.Amount = Convert.ToDouble(lblGrandTotal.Text.Trim());
            objPT.customerEmail = lblEmailAddress.Text.Trim();
            objPT.orderReference = OrderNo;
            Session["PayObj"] = objPT;
            string siteId = "";
            if (Session["siteId"] != null)
                siteId = Session["siteId"].ToString();

            if (Session["AgentUsername"] != null)
            {
                Response.Redirect("~/Agent/Agentpayment.aspx" + "?amt=" + lblGrandTotal.Text.Trim() + "&oid=" + OrderNo);
            }
            else
            {
                if (Request.Params["req"] != null)
                    Response.Redirect("~/Order/PaymentConfirmation.aspx?req=" + OrderNo);
                else
                    Response.Redirect("~/Order/PaymentConfirmation.aspx");
            }
        }
        else
        {
            Response.Redirect("~/Home");
        }
    }

    public void GetCurrencyCode()
    {
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            siteId = Guid.Parse(cookie.Values["_siteId"]);
            currencyID = Guid.Parse(cookie.Values["_curId"]);
            currency = oManageClass.GetCurrency(currencyID);
            currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null && Session["P2BookURL"] != null)
        {
            Response.Redirect(Session["P2BookURL"].ToString(), true);
            Session.Remove("P2BookURL");
        }
        else
            Response.Redirect("BookingCart", true);
    }
}

