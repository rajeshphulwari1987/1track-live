﻿#region Using
using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;
#endregion

namespace IR_Admin
{
    public partial class BookingFee : Page
    {
        readonly Masters _master = new Masters();
        public string Tab = string.Empty;
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID, currID, siteid;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindBookingFeeList(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                FillCommonddl();
                BindBookingFeeList(_siteID);
                txtStartAmount.Attributes.Add("onkeypress", "return keycheck()");
                txtEndAmount.Attributes.Add("onkeypress", "return keycheck()");
                txtBookingFee.Attributes.Add("onkeypress", "return keycheck()");

                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    ddlSite.Enabled = txtStartAmount.Enabled = txtEndAmount.Enabled = ddlStartDate.Enabled = ddlEndDate.Enabled = ddltype.Enabled = false;
                    BindBookingFeeForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _siteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                BindBookingFeeList(_siteID);
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindBookingFeeList(Guid siteId)
        {
            try
            {
                grBookingFee.DataSource = _master.GetBookingFeeList(siteId);
                grBookingFee.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void FillCommonddl()
        {
            try
            {
                var SelectDateList = new List<ListItem>();
                ddlSite.DataSource = _master.GetActiveSiteList();
                ddlSite.DataTextField = "DisplayName";
                ddlSite.DataValueField = "ID";
                ddlSite.DataBind();
                ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
                for (int i = 0; i <= 20; i++)
                    SelectDateList.Add(new ListItem { Text = i.ToString(), Value = i.ToString() });
                ddlStartDate.DataSource = ddlEndDate.DataSource = SelectDateList;
                ddlStartDate.DataBind();
                ddlEndDate.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindBookingFeeForEdit(Guid id)
        {
            try
            {
                var result = _master.GetBookingFeeEdit(id);
                ddlSite.SelectedValue = result.SiteId.ToString();
                txtStartAmount.Text = result.StartAmount.ToString(CultureInfo.InvariantCulture);
                txtEndAmount.Text = result.EndAmount.ToString(CultureInfo.InvariantCulture);
                txtBookingFee.Text = result.BookingFee.ToString(CultureInfo.InvariantCulture);
                ddlStartDate.SelectedValue = result.StartDate.ToString();
                ddlEndDate.Text = result.EndDate.ToString();
                ddltype.SelectedValue= result.HasAmount?"Amount":"Date";
                btnSubmit.Text = "Update";
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var HasAmount = ddltype.SelectedValue == "Amount";
                siteid = Guid.Parse(ddlSite.SelectedValue);
                currID = (Guid)_db.tblSites.FirstOrDefault(x => x.ID == siteid).DefaultCurrencyID;
                if (btnSubmit.Text == "Submit")
                {
                    var startAmt = HasAmount ? Convert.ToDecimal(txtStartAmount.Text) : Convert.ToDecimal(ddlStartDate.SelectedValue);
                    var maxEndAmount = _master.GetMaxBookingFee(siteid, HasAmount);
                    if (maxEndAmount != 0 && startAmt < maxEndAmount)
                    {
                        ShowMessage(2, "Invalid start/end Amount");
                        Tab = "2";
                    }
                    else
                        AddBookingFee(HasAmount);
                }
                else if (btnSubmit.Text == "Update")
                    UpdateBookingFee(HasAmount);
                ClearControls();
                Tab = "1";
                BindBookingFeeList(siteid);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void AddBookingFee(bool HasAmount)
        {
            var book = new tblBookingFeeRule
            {
                ID = Guid.NewGuid(),
                HasAmount = HasAmount,
                StartDate = Convert.ToInt32(ddlStartDate.SelectedValue),
                EndDate = Convert.ToInt32(ddlEndDate.SelectedValue),
                StartAmount = HasAmount ? Convert.ToDecimal(txtStartAmount.Text) : 0,
                EndAmount = HasAmount ? Convert.ToDecimal(txtEndAmount.Text) : 0,
                BookingFee = Convert.ToDecimal(txtBookingFee.Text),
                CreatedBy = AdminuserInfo.UserID,
                CreatedOn = DateTime.Now,
                CurrencyID = currID
            };

            var bookingFeeID = _master.AddBookingFee(book);
            if (bookingFeeID != null)
            {
                var booklkp = new tblBookingFeeSiteLookUp
                {
                    ID = Guid.NewGuid(),
                    BookingFeeID = bookingFeeID,
                    SiteID = Guid.Parse(ddlSite.SelectedValue)
                };
                _master.AddBookingFeeLookup(booklkp);
                ShowMessage(1, "Booking Fee added successfully.");
            }
        }

        void UpdateBookingFee(bool HasAmount)
        {
            var book = new tblBookingFeeRule
            {
                ID = Guid.Parse(Request["id"]),
                HasAmount = HasAmount,
                StartDate = Convert.ToInt32(ddlStartDate.SelectedValue),
                EndDate = Convert.ToInt32(ddlEndDate.SelectedValue),
                StartAmount = HasAmount ? Convert.ToDecimal(txtStartAmount.Text) : 0,
                EndAmount = HasAmount ? Convert.ToDecimal(txtEndAmount.Text) : 0,
                BookingFee = Convert.ToDecimal(txtBookingFee.Text),
                ModifyBy = AdminuserInfo.UserID,
                ModifyOn = DateTime.Now,
                CurrencyID = currID
            };
            var bookingFeeID = _master.UpdateBookingFee(book);
            if (bookingFeeID != null)
            {
                //Delete Existing Record
                var id = Guid.Parse(Request["id"]);
                var result = _db.tblBookingFeeSiteLookUps.FirstOrDefault(x => x.BookingFeeID == id);
                _db.tblBookingFeeSiteLookUps.DeleteObject(result);
                _db.SaveChanges();

                var booklkp = new tblBookingFeeSiteLookUp
                {
                    ID = Guid.NewGuid(),
                    BookingFeeID = bookingFeeID,
                    SiteID = Guid.Parse(ddlSite.SelectedValue)
                };
                _master.AddBookingFeeLookup(booklkp);
                ShowMessage(1, "Booking Fee updated successfully.");
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            }
        }

        void ClearControls()
        {
            txtStartAmount.Text = string.Empty;
            txtEndAmount.Text = string.Empty;
            txtBookingFee.Text = string.Empty;
            ddlSite.SelectedValue = "-1";
        }

        protected void grBookingFee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteBookingFee(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    SiteSelected();
                    BindBookingFeeList(_siteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("BookingFee.aspx");
        }
    }
}