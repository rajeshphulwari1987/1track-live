﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="BookingFee.aspx.cs"
    Inherits="IR_Admin.BookingFee" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
                return true;
            return false;
        }

        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(function () {
            if ('<%=Tab%>' == '1') {
                $("ul.list").tabs("div.panes > div");
            }
            getbookingtype(true);
        });

        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }

        function getbookingtype(first) {
            ValidatorEnable($('[id*=tferrormsg]')[0], false);
            $(".amountvalid,.datevalid,#tderrormsg").hide();
            $("#errormsg").text('');
            var result = true;
            if ($("[id*=ddltype]").val() == "Amount") {
                $("[id*=ddlStartDate]").val('0');
                $("[id*=ddlEndDate]").val('0');
                $(".show-Amount").show();
                $(".show-Date").hide();
                if (!first) {
                    if ($("[id*=txtStartAmount]").val() == '' || $("[id*=txtEndAmount]").val() == '' || $("[id*=txtEndAmount]").val() == 0) {
                        result = false;
                        $(".amountvalid").show();
                        ValidatorEnable($('[id*=tferrormsg]')[0], true);
                    }
                    var startamt = parseFloat($("[id*=txtStartAmount]").val());
                    var endamt = parseFloat($("[id*=txtEndAmount]").val());
                    if (startamt >= endamt) {
                        $("#tferrormsg").text("end amount should not be less than or equal to start amount.");
                        result = false;
                        $(".amountvalid,#tderrormsg").show();
                        ValidatorEnable($('[id*=tferrormsg]')[0], true);
                    }
                }
            }
            else {
                $("[id*=txtStartAmount]").val('');
                $("[id*=txtEndAmount]").val('');
                $(".show-Date").show();
                $(".show-Amount").hide();
                if (!first) {
                    if ($("[id*=ddlEndDate]").val() == 0) {
                        result = false;
                        $(".datevalid").show();
                        ValidatorEnable($('[id*=tferrormsg]')[0], true);
                    }
                    var startamt = parseFloat($("[id*=ddlStartDate]").val());
                    var endamt = parseFloat($("[id*=ddlEndDate]").val());
                    if (startamt >= endamt) {
                        $("#tferrormsg").text("end date should not be less than or equal to start date.");
                        result = false;
                        $(".datevalid,#tderrormsg").show();
                        ValidatorEnable($('[id*=tferrormsg]')[0], true);
                    }
                }
            }
            return result;
        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Booking Fee Rules
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="BookingFee.aspx" class="current">List</a></li>
            <li><a id="aNew" href="BookingFee.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:GridView ID="grBookingFee" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grBookingFee_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Start Amount">
                                    <ItemTemplate>
                                        <%#Eval("StartAmount", "{0:n2}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Amount">
                                    <ItemTemplate>
                                        <%#Eval("EndAmount", "{0:n2}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Start Date">
                                    <ItemTemplate>
                                        <%#Eval("StartDate", "{0:n2}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Date">
                                    <ItemTemplate>
                                        <%#Eval("EndDate", "{0:n2}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booking Fee">
                                    <ItemTemplate>
                                        <%#Eval("BookingFee", "{0:n2}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href='BookingFee.aspx?id=<%#Eval("ID")%>'>
                                            <img src="images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                            CommandName="Remove" ImageUrl="~/images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" style="width:142px;">
                                Site Name:
                            </td>
                            <td class="col" style="width:218px;">
                                <asp:DropDownList ID="ddlSite" runat="server" Width="205px" />
                                <asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                            <td class="col">
                                Applied On:
                                <asp:DropDownList ID="ddltype" runat="server" Width="205px" onchange="getbookingtype(true)">
                                    <asp:ListItem Text="Amount" Value="Amount"></asp:ListItem>
                                    <asp:ListItem Text="Date" Value="Date"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="show-Amount">
                            <td class="col">
                                Amount Range: <span style="font-size:12px;">(From)</span>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtStartAmount" runat="server" />
                                <span class="amountvalid" style="color: red; display: none;">*</span>
                            </td>
                            <td class="col">
                            <span style="font-size:12px;">(To)</span>
                                <asp:TextBox ID="txtEndAmount" runat="server" />
                                <span class="amountvalid" style="color: red; display: none;">*</span>
                            </td>
                        </tr>
                        <tr class="show-Date">
                            <td class="col">
                                Date Range: <span style="font-size:12px;">(From)</span>
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlStartDate" runat="server" Width="205px" />
                                <span class="datevalid" style="color: red; display: none;">*</span>
                            </td>
                            <td class="col">
                           <span style="font-size:12px;">(To)</span>
                                <asp:DropDownList ID="ddlEndDate" runat="server" Width="205px" />
                                <span class="datevalid" style="color: red; display: none;">*</span>
                            </td>
                        </tr>
                        <tr id="tderrormsg">
                        <td class="col"></td>
                            <td colspan="2" class="col">
                            <asp:TextBox runat="server" ID="errormsg" style="display:none;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="tferrormsg" ClientIDMode="Static" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="errormsg" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Booking Fee:
                            </td>
                            <td class="col" colspan="2">
                                <asp:TextBox ID="txtBookingFee" runat="server" />
                                <asp:RequiredFieldValidator ID="rfBookingFee" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtBookingFee" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    OnClientClick="return getbookingtype()" Text="Submit" Width="89px" ValidationGroup="rv" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
