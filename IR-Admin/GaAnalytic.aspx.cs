﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class GaAnalytic : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        readonly ManageAnalyticTag analytic = new ManageAnalyticTag();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            FillGrid(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";

            if (!Page.IsPostBack)
            {
                FillCommonddl();
                ddlSite.SelectedValue = _siteID == new Guid() ? Master.SiteID.ToString() : _siteID.ToString();
                _siteID = Master.SiteID;
                FillGrid(_siteID);
            }
        }

        public void FillGrid(Guid siteId)
        {
            try
            {
                if (siteId == new Guid())
                    _siteID = Master.SiteID;
                dtlGaAnalytic.DataSource = analytic.GetListTagBySiteId(_siteID);
                dtlGaAnalytic.DataBind();
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("GaAnalytic.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool res = analytic.AddAnalyticTag(new tblAnalyticTag
                    {
                        ID = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value),
                        Actions = txtAction.Text.Trim(),
                        addImpression = txtImpression.Text.Trim(),
                        ChkOutActions = txtCheckoutAction.Text.Trim(),
                        ChkOutActionsPageView = txtCheckoutPageView.Text.Trim(),
                        ChkOutSetActions = txtCheckoutSetAction.Text.Trim(),
                        Ecommerce = txtEcommerce.Text.Trim(),
                        EventTracking = txtEventTracking.Text.Trim(),
                        ImpressionPageView = txtImpressionPageView.Text.Trim(),
                        PurchaseActions = txtPurchaseAction.Text.Trim(),
                        PurchaseActionsPageView = txtPurchasePageView.Text.Trim(),
                        PurchaseSetActions = txtPurchaseSetAction.Text.Trim(),
                        RmvActions = txtRemoveAction.Text.Trim(),
                        RmvSetActions = txtRemoveSetAction.Text.Trim(),
                        SetActions = txtSetAction.Text.Trim(),
                        SiteID = Guid.Parse(ddlSite.SelectedValue),
                        VisitTracking = txtVisitTracking.Text.Trim(),
                        VisitTrackingPageView = txtVisitTrackingPageView.Text.Trim(),
                    });
                if (res == true)
                    ClearControls();
                FillGrid(_siteID);
                ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Record added successfully." : "Record updated successfully.");
                hdnId.Value = string.Empty;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void FillCommonddl()
        {
            try
            {
                ddlSite.DataSource = _master.GetActiveSiteList();
                ddlSite.DataTextField = "DisplayName";
                ddlSite.DataValueField = "ID";
                ddlSite.DataBind();
                ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void dtlGaAnalytic_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "Remove")
                {
                    analytic.RomoveTag(id);
                    FillGrid(_siteID);
                    ShowMessage(1, "Record delete successfully.");
                }
                else if (e.CommandName == "UpdateRecord")
                {
                    var result = analytic.GetTagById(id);
                    if (result != null)
                    {
                        ddlSite.SelectedValue = result.SiteID.ToString();
                        txtVisitTrackingPageView.Text = result.VisitTrackingPageView;
                        txtVisitTracking.Text = result.VisitTracking;
                        txtEventTracking.Text = result.EventTracking;
                        txtEcommerce.Text = result.Ecommerce;
                        txtImpression.Text = result.addImpression;
                        txtImpressionPageView.Text = result.ImpressionPageView;
                        txtAction.Text = result.Actions;
                        txtSetAction.Text = result.SetActions;
                        txtRemoveAction.Text = result.RmvActions;
                        txtRemoveSetAction.Text = result.RmvSetActions;
                        txtCheckoutAction.Text = result.ChkOutActions;
                        txtCheckoutSetAction.Text = result.ChkOutSetActions;
                        txtCheckoutPageView.Text = result.ChkOutActionsPageView;
                        txtPurchaseAction.Text = result.PurchaseActions;
                        txtPurchaseSetAction.Text = result.PurchaseSetActions;
                        txtPurchasePageView.Text = result.PurchaseActionsPageView;
                        hdnId.Value = id.ToString();
                        Tab = "2";
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ClearControls()
        {
            txtAction.Text = string.Empty;
            txtCheckoutAction.Text = string.Empty;
            txtCheckoutPageView.Text = string.Empty;
            txtCheckoutSetAction.Text = string.Empty;
            txtEcommerce.Text = string.Empty;
            txtEventTracking.Text = string.Empty;
            txtImpression.Text = string.Empty;
            txtImpressionPageView.Text = string.Empty;
            txtPurchaseAction.Text = string.Empty;
            txtPurchasePageView.Text = string.Empty;
            txtPurchaseSetAction.Text = string.Empty;
            txtRemoveAction.Text = string.Empty;
            txtRemoveSetAction.Text = string.Empty;
            txtSetAction.Text = string.Empty;
            txtVisitTracking.Text = string.Empty;
            txtVisitTrackingPageView.Text = string.Empty;
        }
    }
}