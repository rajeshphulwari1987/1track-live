﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="P2PStation.aspx.cs"
    Inherits="IR_Admin.P2PStation" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        P2P Station</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="P2PStation.aspx" class="current">List</a></li>
            <li><a id="aNew" href="P2PStation.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdP2PCountry" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="grdP2PCountry_PageIndexChanging" OnRowCommand="grdP2PCountry_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Station Name">
                                <ItemTemplate>
                                    <%#Eval("CountryName")%>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site Name">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="30%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Banner">
                                <ItemTemplate>
                                    <div style="float: left; width: 70px; padding: 5px; text-align: center;">
                                        <img height="50px" src='<%#Eval("Logo")%>' /></div>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='P2PStation.aspx?id=<%#Eval("Id")%>'>
                                        <img src="images/edit.png" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Station Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCountryName" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtCountryName" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Content:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="10" Columns="5" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfDesc" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtDesc" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is Active ?
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:CheckBox ID="chkIsActve" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Banner
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:FileUpload ID="fupImg" runat="server" Width="200px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
