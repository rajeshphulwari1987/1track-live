﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class P2PdeliveryCharges : Page
    {
        readonly Masters _Master = new Masters();
        readonly private ManageBooking _masterBook = new ManageBooking();
        db_1TrackEntities db = new db_1TrackEntities();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            BindGrid(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";

            if (Request["id"] != null)
            {
                tab = "2";
                if (ViewState["tab"] != null)
                {
                    tab = ViewState["tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                BindSite();
                BindGrid(_siteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    tab = "2";
                    GetP2PDeliveryChargesEdit();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void BindSite()
        {
            //Site
            ddlSite.DataSource = _Master.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));

            //Currency
            ddlcurrency.DataSource = _Master.GetCurrencyList().Where(x => x.IsActive == true);
            ddlcurrency.DataTextField = "Name";
            ddlcurrency.DataValueField = "ID";
            ddlcurrency.DataBind();
            ddlcurrency.Items.Insert(0, new ListItem("--Currency--", "-1"));
        }

        void BindGrid(Guid _SiteID)
        {
            tab = "1";
            grdDeliveryCharges.DataSource = _Master.GetP2PDeliveryChargesList(_SiteID);
            grdDeliveryCharges.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                AddP2PDeliveryCharges();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void AddP2PDeliveryCharges()
        {
            try
            {
                bool charge = _Master.AddP2PDeliveryCharges(new tblP2PDeliveryChargesMst
                {
                    ID = Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid(),
                    Name = txtName.Text.Trim(),
                    Amount = Convert.ToDecimal(txtAmount.Text.Trim()),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    CurrencyId = Guid.Parse(ddlcurrency.SelectedValue),
                    CreatedBy = AdminuserInfo.UserID,
                    IsActive = chkactive.Checked,
                });

                if(charge)
                    ShowMessage(1, Request["id"] == null ? "You have successfully added the record." : "You have successfully updated the record.");
                else
                    ShowMessage(2, "P2P Delivery charge for this site already exists.");

                BindGrid(new Guid());
                txtName.Text = string.Empty;
                txtAmount.Text = string.Empty;
                ddlSite.SelectedIndex = 0;
                ddlcurrency.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message + ex.InnerException);
            }
        }

        void GetP2PDeliveryChargesEdit()
        {
            if (Request.QueryString["id"] != null)
            {
                var id = Guid.Parse(Request.QueryString["id"]);
                tblP2PDeliveryChargesMst OdeliveryCharges = _Master.GetP2PDeliveryChargesEdit(id);
                if (OdeliveryCharges != null)
                {
                    txtName.Text = OdeliveryCharges.Name;
                    txtAmount.Text = OdeliveryCharges.Amount.ToString();
                    ddlcurrency.SelectedValue = OdeliveryCharges.CurrencyId.ToString();
                    ddlSite.SelectedValue = OdeliveryCharges.SiteID.ToString();
                    if (OdeliveryCharges.IsActive != null) chkactive.Checked = (bool)OdeliveryCharges.IsActive;
                }

                btnSubmit.Text = "Update";
                ddlSite.Enabled = false;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("P2PDeliveryCharges.aspx");
        }

        protected void grdDeliveryCharges_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ActiveInactiveP2PDeliveryCharges(id);
                    _siteID = Master.SiteID;
                    BindGrid(_siteID);
                }
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _Master.DeleteP2PDeliveryCharges(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    BindGrid(_siteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
    }
}
