﻿using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Manage_Contact : Page
    {
        readonly Masters _master = new Masters();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        protected void Page_Load(object sender, EventArgs e)
        {
            BindImages();
            BindPage();
        }

        public void BindImages()
        {
            var resultBanner = _master.GetImageListByID(1); //inner Banner images
            if (resultBanner != null)
            {
                dtBanner.DataSource = resultBanner;
                dtBanner.DataBind();
            }

            var resultContact = _master.GetImageListByID(7); //contact images
            if (resultContact != null)
            {
                dtRtPanel.DataSource = resultContact;
                dtRtPanel.DataBind();
            }

            var resultCallCenter = _master.GetImageListByID(10); //call center images
            if (resultCallCenter != null)
            {
                dtRtCall.DataSource = resultCallCenter;
                dtRtCall.DataBind();
            }
        }

        public void BindPage()
        {
            string url = null;
            if (Session["url"] != null)
                url = Session["url"].ToString();

            if (url != string.Empty)
            {
                var result = _master.GetPageDetailsByUrl(url);
                if (result != null)
                {
                    ContentHead.InnerHtml = result.PageHeading;
                    ContentText.InnerHtml = result.PageContent;
                    addBlock.InnerHtml = result.ContactPanel;
                    callBlock.InnerHtml = result.ContactCall;
                }
                else
                {
                    BindStaticImgs();
                }
            }
            else
            {
                BindStaticImgs();
            }
        }

        protected void dtBanner_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        var bannerid = result.BannerIDs.Trim();
                        string[] arrID = bannerid.Split(',');
                        foreach (string id in arrID)
                        {
                            var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                            if (cbID != null)
                            {
                                if (id == cbID.Value)
                                {
                                    cbID.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void dtRtPanel_ItemDataBound(object sender, DataListItemEventArgs e)
        {
        }
        protected void dtRtCall_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        var contactCallHtml = result.ContactCall;
                        var rdID = (HtmlInputRadioButton)e.Item.FindControl("rdCallID");
                        const string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                        var matchesImgSrc = Regex.Matches(contactCallHtml, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        foreach (Match m in matchesImgSrc)
                        {
                            string imgSrc = m.Groups[1].Value;
                            if (rdID != null)
                            {
                                if (rdID.Value == imgSrc)
                                    rdID.Checked = true;
                            }
                        }
                    }
                }
            }
        }

        private void BindStaticImgs()
        {
            imgContactHome.Src = SiteUrl + "images/page/home-red.png";
            imgContactCall.Src = SiteUrl + "images/page/icon-call-red.png";
            imgContactEmail.Src = SiteUrl + "images/page/icon-email-red.png";
            imgRtCall.Src = SiteUrl + "images/page/CallCenter.jpg";
        }
    }
}