﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class AdminuserPage : Page
    {
        readonly Masters _Master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string tab = string.Empty;
        readonly ManagePrintQueue _oQueue = new ManagePrintQueue();
        public static List<GetOfficeFullName> listFullStationList = new List<GetOfficeFullName>();
        Guid _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindUsers(_SiteID);
            CheckPermission();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request["edit"] != null) && (Request["edit"] != ""))
                tab = "2";
            else tab = "1";
            if (!Page.IsPostBack)
            {
                listFullStationList = null;
                CommonddlControls();
                FillSiteMenu();
                FillUserRoleList();

                _SiteID = Master.SiteID;
                SiteSelected();
                BindUsers(_SiteID);
                //Edit Mode
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                    BindUserListForEdit(Guid.Parse(Request["edit"]));
                else
                    FillStockMenu(AdminuserInfo.UserID);
                //Check permission for Is Visible is True or Not
                CheckPermission();
                aNew.Visible = !(bool)_db.tblAdminUsers.FirstOrDefault(x => x.ID == AdminuserInfo.UserID).IsAgent;
                ShowExportToExcelDiv();

            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindUsers(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        protected void CheckPermission()
        {
            var menuId = Guid.Parse("B3BBCFA2-5F83-40AF-9BC2-9A230BF53A3C");
            var roleID = Guid.Parse(AdminuserInfo.RoleId.ToString());
            var checkRule = _db.ap_RoleMenuDetail.Where(x => x.RoleId == roleID && x.MenuId == menuId).SingleOrDefault();
            if (checkRule != null)
            {
                btnSubmit.Enabled = checkRule.Visible == true;
            }
        }

        public void CommonddlControls()
        {
            try
            {
                ddlRoles.DataSource = _Master.GetRolesList();
                ddlRoles.DataTextField = "RoleName";
                ddlRoles.DataValueField = "RoleID";
                ddlRoles.DataBind();
                ddlRoles.Items.Insert(0, new ListItem("--Select Role--", "0"));

                if (listFullStationList == null)
                    listFullStationList = _oQueue.GetOfficeList();
                ddlOffices.DataSource = listFullStationList;
                ddlOffices.DataValueField = "ID";
                ddlOffices.DataTextField = "Name";
                ddlOffices.DataBind();
                ddlOffices.Items.Insert(0, new ListItem("--Select Office--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        private void FillSiteMenu()
        {
            trSites.Nodes.Clear();
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        private void FillStockMenu(Guid AdminID)
        {
            var objStock = _oQueue.GetActiveStockListByAdminId(AdminID);
            rptStocks.DataSource = objStock;
            rptStocks.DataBind();
        }

        public void BindUserListForEdit(Guid ID)
        {
            try
            {
                FillStockMenu(ID);
                var result = _Master.GetUserListEdit(ID);
                txtUserName.Text = result.UserName;
                txtPassword.Attributes.Add("value", result.Password);
                txtSurname.Text = result.Surname;
                txtForename.Text = result.Forename;
                txtEmail.Text = result.EmailAddress;
                chkAgent.Checked = Convert.ToBoolean(result.IsAgent);
                txtUserNote.Text = result.Note;
                chkMenualP2P.Checked = result.MenualP2PIsActive.HasValue ? result.MenualP2PIsActive.Value : false;
                chkManualPass.Checked = result.ManualPassIsActive.HasValue ? result.ManualPassIsActive.Value : false;
                chkActive.Checked = result.IsActive.HasValue ? result.IsActive.Value : false;
                ddlAllowTo.SelectedValue = result.AssignTo.ToString();// string.IsNullOrEmpty(result.AssignTo.ToString()) ? "0" : result.AssignTo.ToString();
                if (ddlTitle.Items.FindByValue(result.Salutation.ToString()) != null)
                {
                    ddlTitle.Items.FindByValue(result.Salutation.ToString()).Selected = true;
                }

                if (ddlRoles.Items.FindByValue(result.RoleID.ToString()) != null)
                {
                    ddlRoles.Items.FindByValue(result.RoleID.ToString()).Selected = true;
                }

                btnSubmit.Text = "Update";

                // Look up Sites
                var lookUpSites = _db.tblAdminUserLookupSites.Where(x => x.AdminUserID == ID).ToList();
                foreach (var lsites in lookUpSites)
                {
                    foreach (TreeNode pitem in trSites.Nodes)
                    {
                        if (Guid.Parse(pitem.Value) == lsites.SiteID)
                            pitem.Checked = true;
                    }
                }

                //Select Branch
                bool IsAgent = !(bool)_db.tblAdminUsers.FirstOrDefault(x => x.ID == AdminuserInfo.UserID).IsAgent;
                ddlOffices.SelectedValue = result.BranchID.ToString();
                ddlOffices.Enabled = IsAgent;
                hideSiteSelection.Visible = !IsAgent;
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUsers(_SiteID);
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ddlOffices.SelectedValue))
                {
                    ShowMessage(2, "Please select office.");
                    tab = "2";
                    return;
                }
                else
                {
                    var tn = trSites.CheckedNodes.Count;
                    if (tn == 0)
                    {
                        ShowMessage(2, "Please Select Site");
                        tab = "2";
                        return;
                    }
                }

                var branchId = ddlOffices.SelectedValue == "0" ? new Guid() : Guid.Parse(ddlOffices.SelectedValue);
                if (btnSubmit.Text == "Submit")
                {
                    bool isexists = _Master.IsExistUserName(txtUserName.Text);
                    if (isexists)
                    {
                        ShowMessage(2, "UserName Already Exists.");
                        tab = "2";
                        return;
                    }

                    var adminUser = new AdminUser
                        {
                            UserName = txtUserName.Text,
                            Password = txtPassword.Text,
                            Salutation = Convert.ToInt32(ddlTitle.SelectedItem.Value),
                            Surname = txtSurname.Text,
                            Forename = txtForename.Text,
                            EmailAddress = txtEmail.Text,
                            CreatedOn = Convert.ToDateTime(DateTime.Now),
                            BranchID = branchId,
                            RoleID = Guid.Parse(ddlRoles.SelectedItem.Value),
                            IsActive = chkActive.Checked,
                            AssignTo = Convert.ToInt32(ddlAllowTo.SelectedValue),
                            IsAgent = chkAgent.Checked,
                            ID = Guid.NewGuid(),
                            Note = txtUserNote.Text,
                            IsDeleted = false,
                            MenualP2PIsActive = chkMenualP2P.Checked,
                            ManualPassIsActive = chkManualPass.Checked,

                        };

                    int tn = trSites.CheckedNodes.Count;
                    if (tn == 0)
                        ShowMessage(2, "Please Select Site");
                    else
                    {
                        var res = _Master.AddAdminUser(adminUser);
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                var adminUserLookSites = new AdminUserLookSites { ID = Guid.NewGuid(), AdminUserID = res, SiteID = Guid.Parse(node.Value) };
                                _Master.AddUserLookupSites(adminUserLookSites);
                            }
                        }

                        foreach (RepeaterItem it in rptStocks.Items)
                        {
                            var chkStock = it.FindControl("chkStock") as CheckBox;
                            if (chkStock.Checked)
                            {
                                var rdnstockInterrail = it.FindControl("rdnstockInterrail") as HtmlInputRadioButton;
                                var rdnstockBritRail = it.FindControl("rdnstockBritRail") as HtmlInputRadioButton;
                                var rdnstockEurail = it.FindControl("rdnstockEurail") as HtmlInputRadioButton;
                                var adminLookStock = new tblAdminUserStockQueueLookup
                                {
                                    ID = Guid.NewGuid(),
                                    AdminUserID = res,
                                    StockQueueID = Guid.Parse(rdnstockInterrail.Value),
                                    InterrailDefaultPrinter = rdnstockInterrail.Checked,
                                    BritrailDefaultPrinter = rdnstockBritRail.Checked,
                                    EurailDefaultPrinter = rdnstockEurail.Checked,
                                };
                                _oQueue.AddAdminUserStockQueueLookups(adminLookStock);
                            }
                        }

                        ShowMessage(1, "Admin User details added successfully.");
                    }
                }
                else if (btnSubmit.Text == "Update")
                {
                    var ID = Guid.Parse(Request["edit"]);
                    bool isexists = _Master.IsExistUserName(txtUserName.Text, ID);
                    if (isexists)
                    {
                        ShowMessage(2, "UserName Already Exists.");
                        tab = "2";
                        return;
                    }

                    var adminUser = new AdminUser
                    {
                        UserName = txtUserName.Text,
                        Password = txtPassword.Text,
                        Salutation = Convert.ToInt32(ddlTitle.SelectedItem.Value),
                        Surname = txtSurname.Text,
                        Forename = txtForename.Text,
                        EmailAddress = txtEmail.Text,
                        ModifiedOn = Convert.ToDateTime(DateTime.Now),
                        BranchID = branchId,
                        RoleID = Guid.Parse(ddlRoles.SelectedItem.Value),
                        IsActive = chkActive.Checked,
                        AssignTo = Convert.ToInt32(ddlAllowTo.SelectedValue),
                        IsAgent = chkAgent.Checked,
                        ID = ID,
                        Note = txtUserNote.Text,
                        MenualP2PIsActive = chkMenualP2P.Checked,
                        ManualPassIsActive = chkManualPass.Checked,

                    };

                    var res = _Master.UpdateAdminUser(adminUser);
                    //Delete Existing Record
                    _db.tblAdminUserLookupSites.Where(w => w.AdminUserID == res).ToList().ForEach(_db.tblAdminUserLookupSites.DeleteObject);
                    _db.tblAdminUserStockQueueLookups.Where(w => w.AdminUserID == res).ToList().ForEach(_db.tblAdminUserStockQueueLookups.DeleteObject);
                    _db.SaveChanges();

                    //Get All Selected Site
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            var adminUserLookSites = new AdminUserLookSites { ID = Guid.NewGuid(), AdminUserID = res, SiteID = Guid.Parse(node.Value) };
                            _Master.AddUserLookupSites(adminUserLookSites);
                        }
                    }

                    //Get All Selected Stock
                    foreach (RepeaterItem it in rptStocks.Items)
                    {
                        var chkStock = it.FindControl("chkStock") as CheckBox;
                        if (chkStock.Checked)
                        {
                            var rdnstockInterrail = it.FindControl("rdnstockInterrail") as HtmlInputRadioButton;
                            var rdnstockBritRail = it.FindControl("rdnstockBritRail") as HtmlInputRadioButton;
                            var rdnstockEurail = it.FindControl("rdnstockEurail") as HtmlInputRadioButton;
                            var adminLookStock = new tblAdminUserStockQueueLookup
                            {
                                ID = Guid.NewGuid(),
                                AdminUserID = res,
                                StockQueueID = Guid.Parse(rdnstockInterrail.Value),
                                InterrailDefaultPrinter = rdnstockInterrail.Checked,
                                BritrailDefaultPrinter = rdnstockBritRail.Checked,
                                EurailDefaultPrinter = rdnstockEurail.Checked,
                            };
                            _oQueue.AddAdminUserStockQueueLookups(adminLookStock);
                        }
                    }

                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ShowMessage(1, "Admin User details updated successfully.");
                }
                ClearControls();
                tab = "1";
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUsers(_SiteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void ClearControls()
        {
            txtUserName.Text = txtPassword.Text = txtForename.Text = txtSurname.Text = txtEmail.Text = txtUserNote.Text = string.Empty;
            ddlTitle.ClearSelection();
            ddlRoles.ClearSelection();
            ddlOffices.ClearSelection();
            ddlAllowTo.ClearSelection();
            chkAgent.Checked = chkMenualP2P.Checked = chkManualPass.Checked = chkActive.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageAdminUsers.aspx");
        }

        private void BindUsers(Guid siteID)
        {
            if (chkAll.Checked)
            {
                var user = (from ta in _db.tblAdminUsers
                            join off in _db.tblBranches
                                on ta.BranchID equals off.ID into branch
                            from i in branch.DefaultIfEmpty()
                            join rl in _db.aspnet_Roles
                                on ta.RoleID equals rl.RoleId into role
                            from j in role.DefaultIfEmpty()
                            where ta.IsDeleted == false
                            select new
                            {
                                ta.ID,
                                Name = ta.Forename + " " + ta.Surname,
                                ta.UserName,
                                ta.EmailAddress,
                                j.RoleName,
                                ta.Note,
                                i.OfficeName,
                                ta.IsActive,
                                ta.IsAgent,
                                ta.IsDeleted,
                                ta.RoleID
                            }).OrderBy(x => x.UserName).ToList();
                user = user.Where(x => x.UserName.ToLower().Contains(txtSearchUserName.Text.ToLower()) && x.EmailAddress.ToLower().Contains(txtSearchEmail.Text.ToLower())).ToList();
                if (ddlUserType.SelectedValue != "-1")
                {
                    bool chk = (ddlUserType.SelectedValue == "1");
                    user = user.Where(x => x.IsAgent == chk).ToList();
                }
                grdUsers.DataSource = user;
                grdUsers.DataBind();
            }
            else
            {
                var user = (from ta in _db.tblAdminUsers
                            join lk in _db.tblAdminUserLookupSites
                                on ta.ID equals lk.AdminUserID into lookup
                            from l in lookup.DefaultIfEmpty()
                            join off in _db.tblBranches
                                on ta.BranchID equals off.ID into branch
                            from i in branch.DefaultIfEmpty()
                            join rl in _db.aspnet_Roles
                                on ta.RoleID equals rl.RoleId into role
                            from j in role.DefaultIfEmpty()
                            where ta.IsDeleted == false && l.SiteID == siteID
                            select new
                                {
                                    ta.ID,
                                    Name = ta.Forename + " " + ta.Surname,
                                    ta.UserName,
                                    ta.EmailAddress,
                                    j.RoleName,
                                    ta.Note,
                                    i.OfficeName,
                                    ta.IsActive,
                                    ta.IsAgent,
                                    ta.IsDeleted,
                                    ta.RoleID
                                }).OrderBy(x => x.UserName).ToList();
                user = user.Where(x => x.UserName.ToLower().Contains(txtSearchUserName.Text.ToLower()) && x.EmailAddress.ToLower().Contains(txtSearchEmail.Text.ToLower())).ToList();
                if (ddlUserType.SelectedValue != "-1")
                {
                    bool chk = ddlUserType.SelectedValue == "1";
                    user = user.Where(x => x.IsAgent == chk).ToList();
                }
                grdUsers.DataSource = user;
                grdUsers.DataBind();
            }
        }

        protected void grdusers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string str = e.CommandArgument.ToString();
            var mID = new Guid();

            if (str.Length > 30)
                mID = Guid.Parse(str);
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        Response.Redirect("ManageAdminUsers.aspx?edit=" + mID);
                        break;
                    }

                case "IsAgent":
                    {
                        _Master.AgentNotAgentAdminUser(mID);
                        Response.Redirect("ManageAdminUsers.aspx");
                        break;
                    }

                case "ActiveInActive":
                    {
                        _Master.ActivateInActivateAdminUser(mID);
                        Response.Redirect("ManageAdminUsers.aspx");
                        break;
                    }
                case "Remove":
                    {
                        if (mID != AdminuserInfo.UserID)
                        {
                            _Master.DeleteAdminUser(mID);
                            ShowMessage(1, "User deleted Successfully.");
                        }
                        else
                        {
                            ShowMessage(2, "Logged in User can not be deleted.");
                        }

                        tab = "1";
                        _SiteID = Master.SiteID;
                        SiteSelected();
                        BindUsers(_SiteID);
                        break;
                    }
            }
        }

        protected void grdUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdUsers.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindUsers(_SiteID);
        }

        protected void grdUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var imgDelete = e.Row.FindControl("imgDelete") as ImageButton;
                var adminID = AdminuserInfo.UserID;
                var isAdmin = _Master.RoleIsAdmin(adminID);
                if (isAdmin)
                    if (imgDelete != null) imgDelete.Visible = true;
            }
        }

        protected void btnSearchSubmit_Click(object sender, EventArgs e)
        {
            _SiteID = Master.SiteID;
            SiteSelected();
            BindUsers(_SiteID);
            grdExportExcel.Visible = false;
            grdUsers.Visible = true;
            ddlUserRole.SelectedValue = "-1";
        }

        public void FillUserRoleList()
        {
            var list = _db.aspnet_Roles.OrderBy(x => x.RoleName).ToList();
            ddlUserRole.DataSource = list;
            ddlUserRole.DataValueField = "RoleId";
            ddlUserRole.DataTextField = "RoleName";
            ddlUserRole.DataBind();
            ddlUserRole.Items.Insert(0, new ListItem("-Select User Role-", "-1"));
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                _SiteID = Master.SiteID;
                BindUsers(_SiteID);
                grdExportExcel.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#d06b95");
                Context.Response.ClearContent();
                Context.Response.ContentType = "application/ms-excel";
                Context.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xls", "grdUsers"));
                Context.Response.Charset = "";
                var stringwriter = new System.IO.StringWriter();
                var htmlwriter = new HtmlTextWriter(stringwriter);
                grdExportExcel.RenderControl(htmlwriter);
                Context.Response.Write(stringwriter.ToString());
                Context.Response.End();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void ddlUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _SiteID = Master.SiteID;
                BindExportToExcel(_SiteID);
                grdExportExcel.Visible = true;
                grdUsers.Visible = false;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "showExcelDiv", "showExcelDiv();", true);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void BindExportToExcel(Guid siteID)
        {
            try
            {
                if (ddlUserRole.SelectedValue != "-1")
                {
                    var user = (from ta in _db.tblAdminUsers
                                join lk in _db.tblAdminUserLookupSites
                                    on ta.ID equals lk.AdminUserID into lookup
                                from l in lookup.DefaultIfEmpty()
                                join off in _db.tblBranches
                                    on ta.BranchID equals off.ID into branch
                                from i in branch.DefaultIfEmpty()
                                join rl in _db.aspnet_Roles
                                    on ta.RoleID equals rl.RoleId into role
                                from j in role.DefaultIfEmpty()
                                where ta.IsDeleted == false && l.SiteID == siteID
                                select new
                                {
                                    ta.ID,
                                    Salutaion = ta.Salutation,
                                    Name = ta.Forename + " " + ta.Surname,
                                    ta.UserName,
                                    ta.EmailAddress,
                                    j.RoleName,
                                    ta.Note,
                                    i.OfficeName,
                                    ta.IsActive,
                                    ta.IsAgent,
                                    ta.IsDeleted,
                                    ta.RoleID,
                                    ta.Password,
                                    ta.ManualPassIsActive,
                                    ta.MenualP2PIsActive
                                }).OrderBy(x => x.UserName).ToList();
                    user = user.Where(x => x.RoleID.HasValue && x.RoleID.Value == Guid.Parse(ddlUserRole.SelectedValue)).ToList();
                    var data = user.Select(x => new
                    {
                        ID = x.ID,
                        Name = GetSalutation(x.Salutaion.HasValue ? x.Salutaion.Value : 0) + " " + x.Name,
                        UserName = x.UserName,
                        EmailAddress = x.EmailAddress,
                        RoleName = x.RoleName,
                        OfficeName = x.OfficeName,
                        Note = x.Note,
                        IsActive = x.IsActive.Value ? "Yes" : "No",
                        IsAgent = x.IsAgent.Value ? "Yes" : "No",
                        IsDeleted = x.IsDeleted.Value ? "Yes" : "No",
                        RoleID = x.RoleID,
                        Password = x.Password,
                        ManualPassIsActive = x.ManualPassIsActive.Value ? "Yes" : "No",
                        MenualP2PIsActive = x.MenualP2PIsActive.Value ? "Yes" : "No",
                    }).ToList();
                    grdExportExcel.DataSource = data;
                    grdExportExcel.DataBind();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private string GetSalutation(int value)
        {
            switch (value)
            {
                case 1:
                    return "Mr.";
                case 2:
                    return "Miss.";
                case 3:
                    return "Mrs.";
                case 4:
                    return "Ms.";
                default:
                    return "Mr.";
            }
        }

        private void ShowExportToExcelDiv()
        {
            /* Show div only for admin*/
            Guid RoleId = Guid.Parse("a66f5641-d8a2-4cc3-bcb0-1b5f1e2269b9");
            if (AdminuserInfo.RoleId == RoleId)
                showHideExportExcelDiv.Visible = true;
        }
    }
}
