﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;
using System.Text.RegularExpressions;

namespace IR_Admin
{
    public partial class Manage_AboutUs : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        protected void Page_Load(object sender, EventArgs e)
        {
            BindImages();
            BindPage();
        }

        public void BindImages()
        {
            var resultBanner = _master.GetImageListByID(1); //Banner images
            dtBanner.DataSource = resultBanner;
            dtBanner.DataBind();

            var result2 = _master.GetImageListByID(4); //footer images
            dtFooter.DataSource = result2;
            dtFooter.DataBind();

            var result3 = _master.GetImageListByID(5); // rt panel images
            dtRtPanel.DataSource = result3;
            dtRtPanel.DataBind();
        }

        public void BindPage()
        {
            string url = null;
            if (Session["url"] != null)
                url = Session["url"].ToString();

            if (url != string.Empty)
            {
                var result = _master.GetPageDetailsByUrl(url);
                if (result != null)
                {
                    ContentHead.InnerHtml = result.PageHeading;
                    ContentText.InnerHtml = result.PageContent;
                    footerBlock.InnerHtml = result.FooterImg;
                    rtPannel1.InnerHtml = result.RightPanel1;
                }
                else
                    BindStaticImgs();
            }
            else
                BindStaticImgs();
        }

        protected void dtBanner_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    tblPage result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        var bannerid = result.BannerIDs.Trim();
                        string[] arrID = bannerid.Split(',');
                        foreach (string id in arrID)
                        {
                            var cbID = (HtmlInputCheckBox) e.Item.FindControl("chkID");
                            if (cbID != null)
                            {
                                if (id == cbID.Value)
                                {
                                    cbID.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void dtRtPanel_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        var rtPanelHtml = result.RightPanel1;
                        var rdID = (HtmlInputRadioButton)e.Item.FindControl("rdrtID");
                        const string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                        var matchesImgSrc = Regex.Matches(rtPanelHtml, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        foreach (Match m in matchesImgSrc)
                        {
                            string imgSrc = m.Groups[1].Value;
                            if (rdID != null)
                            {
                                if (rdID.Value == imgSrc)
                                    rdID.Checked = true;
                            }
                        }
                    }
                }
            }
        }

        private void BindStaticImgs()
        {
            imgFooter1.Src = SiteUrl + "images/NoImage.jpg";
            imgFooter2.Src = SiteUrl + "images/NoImage.jpg";
            imgFooter3.Src = SiteUrl + "images/NoImage.jpg";
            imgFooter4.Src = SiteUrl + "images/NoImage.jpg";
            imgRtPanelTop.Src = SiteUrl + "images/page/request-form.jpg";
        }
    }
}