﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class PrivacyPolicy : Page
    {
        readonly private ManageBooking _master = new ManageBooking();
        readonly Masters _oMaster = new Masters();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                ShowMessage(0, null);
                BindSite();
                BindGrid(_SiteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetPrivacyForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp",
                                                        "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            Tab = "1";
            grdPolicy.DataSource = _master.GetPrivacyList(_SiteID);
            grdPolicy.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdPolicy_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPolicy.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }

        protected void grdPolicy_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactivePrivacy(id);
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }

                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeletePrivacyPolicy(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #region add/edit
        public void BindSite()
        {
            ddlSite.DataSource = _oMaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddPrivacyPolicy()
        {
            try
            {
                _master.AddPrivacy(new tblPrivacyPolicy
                {
                    ID = Guid.NewGuid(),
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.Text.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    YourConsent = txtConsent.InnerHtml,
                    InfoCollected = txtInfo.InnerHtml,
                    UseInfo = txtUseinfo.InnerHtml,
                    DisclosureInfo = txtDisc.InnerHtml,
                    Person18 = txtPerson.InnerHtml,
                    Cookies = txtCookies.InnerHtml,
                    SecurityInfo = txtSecurity.InnerHtml,
                    PersonalData = txtPersonal.InnerHtml,
                    AlterationsPrivacy = txtAlt.InnerHtml,
                    IsActive = chkIsActv.Checked,
                });
                ShowMessage(1, "You have successfully added privacy policies.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdatePrivacyPolicy()
        {
            try
            {
                var id = Guid.Parse(Request["id"]);
                var privacyPolicy = new tblPrivacyPolicy
                {
                    ID = id,
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.Text.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    YourConsent = txtConsent.InnerHtml,
                    InfoCollected = txtInfo.InnerHtml,
                    UseInfo = txtUseinfo.InnerHtml,
                    DisclosureInfo = txtDisc.InnerHtml,
                    Person18 = txtPerson.InnerHtml,
                    Cookies = txtCookies.InnerHtml,
                    SecurityInfo = txtSecurity.InnerHtml,
                    PersonalData = txtPersonal.InnerHtml,
                    AlterationsPrivacy = txtAlt.InnerHtml,
                    IsActive = chkIsActv.Checked,
                };
                _master.UpdatePrivacy(privacyPolicy);
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, "You have successfully updated privacy policies.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddPrivacyPolicy();
                else
                    UpdatePrivacyPolicy();
                ClearControls();
                _SiteID = Master.SiteID;
                SiteSelected();
                BindGrid(_SiteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlSite.SelectedIndex = 0;
            txtTitle.Text = string.Empty;
            txtDesc.Text = string.Empty;
            chkIsActv.Checked = false;
            txtConsent.InnerHtml = string.Empty;
            txtInfo.InnerHtml = string.Empty;
            txtUseinfo.InnerHtml = string.Empty;
            txtDisc.InnerHtml = string.Empty;
            txtPerson.InnerHtml = string.Empty;
            txtCookies.InnerHtml = string.Empty;
            txtSecurity.InnerHtml = string.Empty;
            txtPersonal.InnerHtml = string.Empty;
            txtAlt.InnerHtml = string.Empty;
        }
        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("PrivacyPolicy.aspx");
        }

        public void GetPrivacyForEdit(Guid id)
        {
            var oP = _master.GetPrivacyById(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = oP.SiteID.ToString();
                txtTitle.Text = oP.Title;
                txtDesc.Text = oP.Description;
                txtConsent.InnerHtml = oP.YourConsent;
                txtInfo.InnerHtml = oP.InfoCollected;
                txtUseinfo.InnerHtml = oP.UseInfo;
                txtDisc.InnerHtml = oP.DisclosureInfo;
                txtPerson.InnerHtml = oP.Person18;
                txtCookies.InnerHtml = oP.Cookies;
                txtSecurity.InnerHtml = oP.SecurityInfo;
                txtPersonal.InnerHtml = oP.PersonalData;
                txtAlt.InnerHtml = oP.AlterationsPrivacy;
                if (oP.IsActive != null) chkIsActv.Checked = (bool)oP.IsActive;
            }
        }

        #endregion
    }
}