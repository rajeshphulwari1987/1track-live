﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin
{
    public partial class DeliveryOption : System.Web.UI.Page
    {
        private readonly Masters _oMasters = new Masters();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            FillGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "checkUncheckCountyTree", "checkUncheckCountyTree();", true);
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            if (!IsPostBack)
            {
                btnSubmit.Enabled = true;
                if (Request["id"] != null)
                {
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                PageLoadEvent();
                GetValueForEdit();

                _SiteID = Master.SiteID;
                SiteSelected();
                FillGrid(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        private void PageLoadEvent()
        {
            dtlCountry.DataSource = _oMasters.GetRegionList().ToList().OrderBy(x => x.RegionName).ToList();
            dtlCountry.DataBind();

            IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void FillGrid(Guid siteID)
        {
            grdDelivery.DataSource = _oMasters.GetDeliveryOptions(siteID);
            grdDelivery.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditDeliveryOption();
            ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "You have successfully updated product details." : "You have successfully added product details.");
            tab = "2";
            ViewState["tab"] = "2";
            btnSubmit.Enabled = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("DeliveryOption.aspx");
        }

        public void AddEditDeliveryOption()
        {
            try
            {
                Guid id = string.IsNullOrEmpty(Request.QueryString["id"]) ? new Guid() : Guid.Parse(Request.QueryString["id"]);
                List<Guid> listCountryId = new List<Guid>();

                foreach (DataListItem item in dtlCountry.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        TreeView treeSite = item.FindControl("trSites") as TreeView;
                        if (treeSite != null)
                        {
                            listCountryId.AddRange(treeSite.Nodes.Cast<TreeNode>().Where(node => node.Checked).Select(x => Guid.Parse(x.Value)));
                        }
                    }
                }

                List<Guid> listSitId = (from TreeNode node in trSites.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();
                Guid res = _oMasters.AddDeliveryOption(new FieldsDeliveryOption
                    {
                        BasePrice = Convert.ToDecimal(txtBasePrice.Text),
                        Name = txtName.Text.Trim(),
                        IsActive = chkIsActv.Checked,
                        IsActiveForAdmin = chkIsActiveForAdmin.Checked,
                        ID = id,
                        CreatedBy = AdminuserInfo.UserID,
                        Description = txtContent.InnerHtml,
                        ListSiteId = listSitId,
                        ListCountryId = listCountryId
                    });
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        public void GetValueForEdit()
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    return;

                Guid id = Guid.Parse(Request.QueryString["id"]);
                var rec = _oMasters.GetDeliveryOptionsById(id);
                txtName.Text = rec.Name;
                txtContent.InnerHtml = rec.Description;
                txtBasePrice.Text = rec.BasePrice.ToString();
                chkIsActv.Checked = rec.IsActive;
                chkIsActiveForAdmin.Checked = rec.IsActiveForAdmin;

                var listProdCuntr = rec.ListCountryId;
                foreach (DataListItem item in dtlCountry.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        TreeView treeSite = item.FindControl("trSites") as TreeView;
                        if (treeSite == null)
                            return;
                        foreach (TreeNode node in treeSite.Nodes)
                        {
                            node.Checked = listProdCuntr.Contains(Guid.Parse(node.Value));
                        }
                    }
                }
                foreach (TreeNode itm in trSites.Nodes)
                {
                    itm.Checked = rec.ListSiteId.Contains(Guid.Parse(itm.Value));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void grdDelivery_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                bool res = _oMasters.DeleteDeliveryOption(id);
                if (res)
                    ShowMessage(1, "Successfully deleted record.");
            }

            if (e.CommandName == "ActiveInActive")
            {
                _oMasters.ActiveInActiveDeliveryOption(id);
            }

            if (e.CommandName == "ActiveInActiveForAdmin")
            {
                _oMasters.ActiveInActiveDeliveryOptionForAdmin(id);
            }

            FillGrid(_SiteID);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdDelivery_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDelivery.PageIndex = e.NewPageIndex;
            FillGrid(_SiteID);
        }

        protected void dtlCountry_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                TreeView trSites = e.Item.FindControl("trSites") as TreeView;
                HiddenField hdnRegionId = e.Item.FindControl("hdnRegion") as HiddenField;
                var lisCountry = _oMasters.GetCountryList().Where(x => x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value));
                foreach (var itemCountry in lisCountry)
                {
                    TreeNode trCat = new TreeNode();
                    trCat.Text = itemCountry.CountryName;
                    trCat.Value = itemCountry.CountryID.ToString();
                    trCat.SelectAction = TreeNodeSelectAction.None;
                    trSites.Nodes.Add(trCat);
                }
            }
        }
    }
}