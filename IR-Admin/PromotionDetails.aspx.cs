﻿using System;
using System.Linq;
using Business;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using OneTrack.OneHubServiceRef;

public partial class PromotionDetails : System.Web.UI.Page
{
    readonly Masters _master = new Masters();
    private Guid _siteId;
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
    }

    #region PageLoad Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Request.QueryString["id"] != null)
            {
                try
                {
                    BindPromotion(Guid.Parse(Request.QueryString["id"]));
                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
            }
        }
    }
    #endregion

    #region UserDefined function
    public void BindPromotion(Guid id)
    {
        var result = _master.GetPromotionByID(id);
        lblTitle.Text = result.Title;
        var imgUrl = result.ImageUrl != "" ? result.ImageUrl : "images/NoproductImage.png";
        imgPromo.ImageUrl = SiteUrl + imgUrl;
        lblDesp.Text = Server.HtmlDecode(result.Description);
    }
    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
    #endregion

    #region  One Hub Implementation
    public string[] CreateStationXml()
    {
        var clientCode = new OneHubRailOneHubClient();
        var stationList = clientCode.GetStationInfo().ToList();
        return BindXml(ReadObjectAsString(stationList));
    }
    public string ReadObjectAsString(object o)
    {
        var xmlS = new XmlSerializer(o.GetType());
        var sw = new StringWriter();
        var tw = new XmlTextWriter(sw);
        xmlS.Serialize(tw, o);
        return sw.ToString();
    }
    public string[] BindXml(string strXml)
    {
        try
        {
            if (strXml != "")
            {
                var doc = new XmlDocument();
                doc.LoadXml(strXml);
                string filePath = Server.MapPath(@"StationList.xml");
                doc.Save(filePath);
                return null;
            }

            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}