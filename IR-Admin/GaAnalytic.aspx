﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="GaAnalytic.aspx.cs" Inherits="IR_Admin.GaAnalytic" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">    
        function keycheck() {
        var KeyID = event.keyCode;
        if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
            return true;
        return false;
    }
        
     window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }  
        $(function () {                  
             if(<%=Tab%>=='1')
            {
               $("ul.list").tabs("div.panes > div");
            }
        });
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
    <style type="text/css">
        .col
        {
            border-bottom: 1px dashed #B1B1B1;
            line-height: 30px;
            padding-top: 6px;
        }
        .train-detail-block
        {
            width: 100% !important;
        }
        
        #MainContent_dtlGaAnalytic table
        {
            width: 100%;
        }
        #MainContent_dtlGaAnalytic table tr td
        {
            text-align: left;
            vertical-align: top;
            padding: 10px;
        }
        #MainContent_dtlGaAnalytic table tr th
        {
            text-align: left;
            vertical-align: top;
            padding: 10px;
            font-size: 13px;
            color: White;
            background: url(./schemes/images/grid-head-bg.jpg) no-repeat 0px 0px;
        }
        #MainContent_dtlGaAnalytic table tr:nth-child(even)
        {
            background-color: #ECECEC;
            border:1px solid #ECECEC;
        }
        #MainContent_dtlGaAnalytic table tr:nth-child(odd)
        {
            background-color: #FBDEE6;
            border:1px solid #FBDEE6;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        STA Google Analytic Tags
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="GaAnalytic.aspx" class="current">List</a></li>
            <li><a id="aNew" href="GaAnalytic.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:DataList ID="dtlGaAnalytic" runat="server" Width="100%" RepeatDirection="Horizontal"
                            OnItemCommand="dtlGaAnalytic_ItemCommand">
                            <ItemTemplate>
                                <div class="train-detail-block">
                                   <table width="100%" cellpadding="0" cellspacing="0" class="table-layout-new">
                                        <tr>
                                            <th style="text-align: right; padding-right: 15px !important;" colspan="2">
                                                <asp:ImageButton runat="server" ID="ImgUpdateRecord" AlternateText="UpdateRecord"
                                                    ToolTip="Update Record" CommandArgument='<%#Eval("ID")%>' CommandName="UpdateRecord"
                                                    ImageUrl="~/images/edit.png" />
                                                <asp:ImageButton runat="server" ID="imgDelete" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                                    CommandName="Remove" ImageUrl="~/images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                            </th>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Visit Tracking
                                            </td>
                                            <td>
                                                <%#Eval("VisitTracking")%>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Visit Tracking Page View
                                            </td>
                                            <td>
                                                <%#Eval("VisitTrackingPageView")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Event Tracking
                                            </td>
                                            <td>
                                                <%#Eval("EventTracking")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Ecommerce
                                            </td>
                                            <td>
                                                <%#Eval("Ecommerce")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Impression
                                            </td>
                                            <td>
                                                <%#Eval("addImpression")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Impression Page View
                                            </td>
                                            <td>
                                                <%#Eval("ImpressionPageView")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Action
                                            </td>
                                            <td>
                                                <%#Eval("Actions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Set Action
                                            </td>
                                            <td>
                                                <%#Eval("SetActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Remove Action
                                            </td>
                                            <td>
                                                <%#Eval("RmvActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Remove Set Action
                                            </td>
                                            <td>
                                                <%#Eval("RmvSetActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Checkout Action
                                            </td>
                                            <td>
                                                <%#Eval("ChkOutActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Checkout Set Action
                                            </td>
                                            <td>
                                                <%#Eval("ChkOutSetActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Checkout Page View
                                            </td>
                                            <td>
                                                <%#Eval("ChkOutActionsPageView")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Purchase Action
                                            </td>
                                            <td>
                                                <%#Eval("PurchaseActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Purchase Set Action
                                            </td>
                                            <td>
                                                <%#Eval("PurchaseSetActions")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="font-weight: bold; width: 18%;">
                                                Purchase Page View
                                            </td>
                                            <td>
                                                <%#Eval("PurchaseActionsPageView")%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                Site Name:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" runat="server" Width="100%" />
                                <asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Visit Tracking:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtVisitTracking" runat="server" TextMode="MultiLine" MaxLength="2000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>

                           <tr>
                            <td class="col">
                                Visit Tracking Page View:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtVisitTrackingPageView" runat="server" TextMode="MultiLine" MaxLength="2000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>

                        <tr>
                            <td class="col">
                                Event Tracking:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtEventTracking" runat="server" TextMode="MultiLine" MaxLength="2000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Ecommerce:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtEcommerce" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Impression
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtImpression" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Impression Page View
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtImpressionPageView" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Set Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSetAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Remove Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtRemoveAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Remove Set Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtRemoveSetAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Checkout Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtCheckoutAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Checkout Set Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtCheckoutSetAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Checkout Page View
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtCheckoutPageView" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Purchase Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPurchaseAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="10" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Purchase Set Action
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPurchaseSetAction" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Purchase Page View
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPurchasePageView" runat="server" TextMode="MultiLine" MaxLength="1000"
                                    Width="100%" Rows="5" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="rv" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
