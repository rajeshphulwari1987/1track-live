﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BranchesAddEdit.aspx.cs"
    MasterPageFile="~/Site.master" Inherits="IR_Admin.BranchesAddEdit" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">

        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Add/Edit Offices</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Branches.aspx" class="current">List</a></li>
            <li><a id="aNew" href="BranchesAddEdit.aspx" class=" ">New/Edit</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divNew" class="grid-sec2" runat="server">
                    <div class="divMain" style="width: 950px; border-bottom: 1px solid #00aeef; border-radius: 0px;
                        border-top: 0px; border-left: 0px; border-right: 0px">
                        <span style="color: #00aeef; font-weight: bold;">Path:</span>
                        <asp:Label ID="lblBranchFullPath" runat="server" /></div>
                    <div style="clear: both;">
                    </div>
                    <table class="tblMainSection">
                        <tr>
                            <td colspan="2" valign="top" style="height: 25px;">
                                <div style="display: block; width: 970px;">
                                    <table class="tblMainSection" id="tblForm" runat="server">
                                        <tr>
                                            <td class="col" style="width: 30%">
                                                Office Name
                                            </td>
                                            <td class="col" style="width: 40%">
                                                <asp:TextBox ID="txtOname" runat="server" MaxLength="100" />
                                                <asp:RequiredFieldValidator ID="txtONameRequiredFieldValidator" runat="server" ControlToValidate="txtOname"
                                                    ErrorMessage="*" CssClass="valdreq" ValidationGroup="BranchEForm" />
                                            </td>
                                            <td rowspan="15" valign="top" class="col" style="width: 30%; border-left: 1px dashed #B1B1B1;">
                                                &nbsp; <b>Select Site</b>
                                                <div style="width: 95%; height: 483px; overflow-y: auto; margin-left: 5px;">
                                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                                    All
                                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                                        <NodeStyle ChildNodesPadding="5px" />
                                                    </asp:TreeView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Title
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtTitle" runat="server" MaxLength="100" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                First Name
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtfName" runat="server" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Last Name
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtlName" runat="server" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Address 1
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtAddress1" runat="server" TextMode="MultiLine" Width="202px" MaxLength="250" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Address 2
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtAddress2" runat="server" TextMode="MultiLine" Width="202px" MaxLength="250" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Town
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtTown" runat="server" MaxLength="100" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                County/State
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtCounty" runat="server" MaxLength="100" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Country
                                            </td>
                                            <td class="col">
                                                <asp:DropDownList ID="ddlCountry" runat="server" />
                                                <asp:RequiredFieldValidator ID="reqCountry" runat="server" ControlToValidate="ddlCountry"
                                                    InitialValue="0" ErrorMessage="*" CssClass="valdreq" ValidationGroup="BranchEForm" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Postcode/Zip code
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtPostCode" runat="server" MaxLength="200" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Telephone
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtTelephone" runat="server" MaxLength="20" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Email
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" />
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                                    CssClass="valdreq" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="BranchEForm" />
                                                <asp:CheckBox ID="chkEmail" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col" valign="top">
                                                Secondary Email
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtSecondaryEmail" runat="server" MaxLength="50" />
                                                <asp:RegularExpressionValidator ID="revEmail2" runat="server" ControlToValidate="txtSecondaryEmail"
                                                    CssClass="valdreq" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="BranchEForm" />
                                                <asp:CheckBox ID="chkSecondaryEmail" runat="server" />
                                                <br />
                                                <span>if ticked, all confirmation emails from bookings under this office will be sent
                                                    to this email address as well as to agents own email address.</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Is Active
                                            </td>
                                            <td class="col">
                                                <asp:CheckBox ID="chkactive" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Is Virtual (Public site office)
                                            </td>
                                            <td class="col">
                                                <asp:CheckBox ID="chkVirtual" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="2">
                                                <asp:Button ID="btnUpdate" CssClass="button" runat="server" OnClick="btnSubmit_Click"
                                                    Text="Save" ValidationGroup="BranchEForm" />
                                                &nbsp;
                                                <asp:Button ID="btnCancel" CssClass="button" runat="server" OnClick="btnCancel_Click"
                                                    Text="Cancel" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <div style="border-bottom: 3px double #00aeef;padding-top:10px;">
                                                </div>
                                                <b style="font-size: 16px;">Users associated to this office :</b>
                                                <div style="overflow-y: scroll; height: 160px; padding-left: 5px; background: #a1e5ff;">
                                                    <asp:Repeater ID="rptUserList" runat="server">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("Name") %>'></asp:Label><br />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <asp:HiddenField ID="hdnBranchid" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
