﻿using System;
using System.Data;

namespace IR_Admin
{
    public partial class ManagePages : System.Web.UI.Page
    {
        readonly DataTable _table = new DataTable();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!Page.IsPostBack)
            {
                FillPage();
            }
        }

        public void FillPage()
        {
            _table.Columns.Add("ID", typeof(int));
            _table.Columns.Add("pageheading", typeof(string));
            _table.Columns.Add("content1", typeof(string));
            _table.Columns.Add("content2", typeof(string));
            _table.Columns.Add("image1", typeof(string));
            _table.Columns.Add("image2", typeof(string));

            DataRow dr = _table.NewRow();

            //Fill all columns with value
            dr["ID"] = 1;
            dr["pageheading"] = "Page Heading";
            dr["content1"] = " This is a free CSS website template by iWebsiteTemplate.com. This work is distributed" +
                    "under the Commons Attribution" +
                        "3.0 License which means that you are free to use it for any personal or" +
                    "commercial purpose provided you credit me in the form of a link back to iWebsiteTemplate.com.";
            dr["content2"] = " This is a free CSS website Content2 template by iWebsiteTemplate.com. This work is distributed" +
                  "under the Commons Attribution" +
                      "3.0 License, which means that you are free to use it for any personal or" +
                  "commercial purpose provided you credit me in the form of a link back to iWebsiteTemplate.com.";

            dr["image1"] = "images1.jpg";
            dr["image2"] = "images_2.jpg";
            _table.Rows.Add(dr);

            if (Session["dt"] != null)
            {
                dtList.DataSource = Session["dt"];
                dtList.DataBind();
            }
            else
            {
                Session["dt"] = _table;
                dtList.DataSource = _table;
                dtList.DataBind();
            }
        }

        [System.Web.Services.WebMethod]
        public static string ManageCms(string content)
        {
            string[] contentarr = content.Split(':');
            var dt = (DataTable)System.Web.HttpContext.Current.Session["dt"];
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["ID"].ToString() == "1")
                {
                    dr[contentarr[0]] = contentarr[1];
                }
            }
            dt.AcceptChanges();
            return "true";
        }
    }
}