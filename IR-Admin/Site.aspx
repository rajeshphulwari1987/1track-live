﻿<%@ Page Title="Site " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Site.aspx.cs" Inherits="IR_Admin.SitePage" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });
        $(function () {
            $("#MainContent_txtBccEmail").blur(function () {
                $(this).val($(this).val().replace(/\;/g, ','));
            });
            if ('<%=tab.ToString()%>' == '1') {
                $("ul.list").tabs("div.panes > div");
            }
        });
        function pageLoad(sender, args) {
            $('#MainContent_txtAttentionTitle,#MainContent_txtAttentionMsg').redactor({ iframe: false, minHeight: 50 });
        }
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }

        $(document).ready(function () {
            //$.each($(".defaultprintqueue"), function () {
            //    var id = this.id;
            //    var PrintQueueId = $("#" + id).val() == 0 ? '00000000-0000-0000-0000-000000000000' : $("#" + id).val();
            //    var SiteId = $("#hdnsiteid").val();
            //    var hostUrl = window.location.origin;
            //    if (hostUrl == "http://localhost")
            //        hostUrl = "http://localhost/1Track-Admin";
            //    hostUrl += '/Site.aspx/AjaxPrintQueueAgentList';
            //    $.ajax({
            //        url: hostUrl,
            //        type: 'POST',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: 'json',
            //        data: "{'SiteId':'" + SiteId + "','PrintQueueId':'" + PrintQueueId + "'}",
            //        success: function (Pdata) {
            //            if (Pdata.d.length > 0) {
            //                $("#" + id + "Agent option").remove();
            //                $.each(Pdata.d, function () {
            //                    $("#" + id + "Agent").append("<option value=" + this.Id + ">" + this.Name + "</option>");
            //                });
            //                $("#" + id + "Agent").val($("#hdn" + id + 'Agent').val());
            //            }
            //        },
            //        error: function (Error) {
            //            console.log(Error.statusText);
            //        }
            //    });
            //});

            //$(".defaultprintqueue").on("change", function () {
            //    var id = this.id;
            //    var PrintQueueId = $("#" + id).val() == 0 ? '00000000-0000-0000-0000-000000000000' : $("#" + id).val();
            //    var SiteId = $("#hdnsiteid").val();
            //    var hostUrl = window.location.origin;
            //    if (hostUrl == "http://localhost")
            //        hostUrl = "http://localhost/1Track-Admin";
            //    hostUrl += '/Site.aspx/AjaxPrintQueueAgentList';

            //    $.ajax({
            //        url: hostUrl,
            //        type: 'POST',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: 'json',
            //        data: "{'SiteId':'" + SiteId + "','PrintQueueId':'" + PrintQueueId + "'}",
            //        success: function (Pdata) {
            //            $("#" + id + "Agent option").remove();
            //            $.each(Pdata.d, function () {
            //                $("#" + id + "Agent").append("<option value=" + this.Id + ">" + this.Name + "</option>");
            //            });
            //        },
            //        error: function (Error) {
            //            console.log(Error.statusText);
            //        }
            //    });
            //});
            //$(".defaultprintqueueagent").on("change", function () {
            //    $("#hdn" + this.id).val($("#" + this.id).val());
            //});
        });
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>Sites
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Site.aspx" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" CssClass=" " NavigateUrl="#" runat="server">New/Edit</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:GridView ID="grdSites" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdSites_RowCommand"
                            OnRowDataBound="grdSites_RowDataBound">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("DisplayName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site URL">
                                    <ItemTemplate>
                                        <a href='<%#Eval("SiteURL")%>' target="_blank">
                                            <%#Eval("SiteURL")%></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Theme">
                                    <ItemTemplate>
                                        <%#Eval("Theme")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Currency">
                                    <ItemTemplate>
                                        <%#Eval("CurrencyName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Language">
                                    <ItemTemplate>
                                        <%#Eval("LanguageName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country">
                                    <ItemTemplate>
                                        <%#Eval("CountryName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsSTA">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgSTA" CommandArgument='<%#Eval("ID")%>' Height="16"
                                            CommandName="IsSTA" AlternateText="IsSTA" ImageUrl='<%#Eval("IsSTA").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsSTA").ToString()=="True" ?"STA":"In-STA" %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsAgent">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgAgent" CommandArgument='<%#Eval("ID")%>' Height="16"
                                            CommandName="IsAgent" AlternateText="IsAgent" ImageUrl='<%#Eval("IsAgent").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSiteID" runat="server" Text='<%#Eval("ID")%>' Visible="false"></asp:Label>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Remove"
                                            OnClientClick="return confirm('Are you sure you want to delete this site? This process is irreversable.');"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">Site Name
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtSNameRequiredFieldValidator" runat="server" ControlToValidate="txtSName"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm"></asp:RequiredFieldValidator>
                            </td>
                            <td class="col">Site Url
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSiteUrl" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvSiteUrl" runat="server" ControlToValidate="txtSiteUrl"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Theme
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlTheme" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="col">Default Currency
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlCurrency" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rvCurrency" runat="server" ControlToValidate="ddlCurrency"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm" InitialValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Default Language
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlLanguage" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rvLang" runat="server" ControlToValidate="ddlLanguage"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm" InitialValue="0" />
                            </td>
                            <td class="col">Default Country
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlCountry" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Phone Number
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPhoneNumber" runat="server" MaxLength="40" />
                                <asp:RequiredFieldValidator ID="reqPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm" />
                            </td>
                            <td class="col">Site Heading
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSiteHeading" runat="server" MaxLength="40" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Is STA
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsSTA" runat="server" />
                            </td>
                            <td class="col">Site Heading(Title)
                            </td>
                            <td class="col">
                                <asp:TextBox MaxLength="115" ID="txtSiteHeadingTitle" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 165px">Is Wholesale Site
                            </td>
                            <td class="col" style="width: 310px;">
                                <asp:CheckBox ID="chkWholesale" runat="server" />
                            </td>
                            <td class="col">Is US site
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsUsSIte" runat="server" />
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr>
                            <td class="col">Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkactive" runat="server" />
                            </td>
                            <td class="col">Is Agent
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="ChkAgent" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Is Corporate
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIdCorporate" runat="server" />
                            </td>
                            <td class="col">Date Format
                            </td>
                            <td class="col">
                                <asp:RadioButtonList ID="rdnDateFormat" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="MM/DD/YYYY"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="2" Text="DD/MM/YYYY"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" valign="top">BCC order confirmation email address
                            </td>
                            <td class="col" colspan="4">
                                <asp:CheckBox ID="chkIsBccEmail" runat="server" />
                                <asp:TextBox ID="txtBccEmail" runat="server" Width="633px" MaxLength="2000"></asp:TextBox>
                                <span style="color: Red; display: inline-table;">Add "," or ";" after the email address</span>
                                <%--<asp:RegularExpressionValidator ID="revBccEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="txtBccEmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Display="Dynamic" ValidationGroup="SiteForm" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Payment Method
                            </td>
                            <td class="col">
                                <asp:RadioButtonList ID="rdnPaymentType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="Ogone" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Agent Payment"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="WorldPay"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="col">Enable Cookie Consent
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkEnableCookie" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" valign="top">Layout Type
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlLayoutType" runat="server">
                                    <asp:ListItem Value="0" Text="-- Select Layout Type --"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="STA"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Internation Rail"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Old Internation Rail"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="TravelCuts"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Merit Travel"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="Thailand"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="Singapore"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="Omega Travel"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="Freebird Club"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqLayoutType" runat="server" ControlToValidate="ddlLayoutType"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm" InitialValue="0" />
                            </td>
                            <td class="col">GMT Time(Sign/HH/MM)
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlGmtTimeZoneSIgn" runat="server" Width="50px">
                                    <asp:ListItem Value="+" Text="+"></asp:ListItem>
                                    <asp:ListItem Value="-" Text="-"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlGmtTimeZoneHour" runat="server" Width="50px">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlGmtTimeZoneMinute" runat="server" Width="50px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Upload Site Logo
                            </td>
                            <td class="col">
                                <asp:FileUpload ID="upUploadLogo" runat="server" Width="200px" />
                                <asp:LinkButton ID="lnkSiteLogo" runat="server" Text="View Logo" CssClass="clsLink"></asp:LinkButton>
                            </td>
                            <td class="col" valign="top">Upload Site Favicon
                            </td>
                            <td class="col" colspan="3">
                                <asp:FileUpload ID="upUploadFavicon" runat="server" Width="200px" />
                                <asp:LinkButton ID="lnkFavicon" runat="server" Text="View Favicon" CssClass="clsLink"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" valign="top">Site Alias
                            </td>
                            <td class="col" colspan="3">
                                <asp:TextBox ID="txtSiteAlias" runat="server" Width="200px" MaxLength="20" />
                                <span style="font-size: 11px"><b>Note:</b> Sita Alias used for the New STA design for
                                    populating header/footer from STA. </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" valign="top">Order prefix name
                            </td>
                            <td class="col" colspan="3">
                                <asp:TextBox ID="txtOrderPrefixName" runat="server" Width="200px" MaxLength="10" />
                                <span style="font-size: 11px"><b>Note:</b> Order prefix name is used to show order number
                                    (e.g: 111) after adding prefix name.(e.g : IR111) </span>
                            </td>
                        </tr>
                    </table>

                    <table id="DefaultPrinterSection" runat="server" class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color" colspan="4">
                                <b>Select Default Printer:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Eurail Printer
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlEurailPrinter" ClientIDMode="Static" runat="server" CssClass="defaultprintqueue" Width="520px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Britrail Printer
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlBritrailPrinter" ClientIDMode="Static" runat="server" CssClass="defaultprintqueue" Width="520px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Interrail Printer
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlInterrailPrinter" ClientIDMode="Static" runat="server" CssClass="defaultprintqueue" Width="520px" />
                            </td>
                        </tr>
                    </table>

                    <table class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color">
                                <b>Multi Currency Settings:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <asp:Repeater ID="rptCurrency" runat="server">
                                    <ItemTemplate>
                                        <div style="width: 24%; float: left;">
                                            <asp:CheckBox ID="chkCurrency" runat="server" Text='<%#Eval("Name")%>' />
                                            <asp:HiddenField ID="hdnCurrencyID" runat="server" Value='<%#Eval("ID")%>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <table class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color" colspan="4">
                                <b>Attention Settings:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 18px">Attention setion visible
                            </td>
                            <td class="col" style="width: 165px;" colspan="3">
                                <asp:CheckBox ID="ChkAttentionVisible" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" colspan="4">
                                <table style="width: 100%;">
                                    <td style="width: 12%;">Background Colour
                                    </td>
                                    <td style="width: 15%;">
                                        <asp:TextBox runat="server" ID="BGColor" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td style="width: 10%;">Border Colour
                                    </td>
                                    <td style="width: 15%;">
                                        <asp:TextBox runat="server" ID="BDRColor" MaxLength="50"></asp:TextBox>
                                    </td>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Title
                            </td>
                            <td class="col">
                                <asp:TextBox runat="server" ID="txtAttentionTitle" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 165px">Image
                            </td>
                            <td class="col" style="width: 310px; float: left;">
                                <asp:FileUpload ID="flpAttentionimg" runat="server" />
                                <img height="30" width="30" id="imgAttengionimg" runat="server" style="position: absolute;" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Messages
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtAttentionMsg" runat="server" TextMode="MultiLine" Rows="10" Columns="5"
                                    Width="100%" />
                            </td>
                        </tr>
                    </table>
                    <table class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color" colspan="4">
                                <b>Settings:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px">Journey Request form email delivery address:
                            </td>
                            <td class="col" style="width: 165px;" colspan="3">
                                <asp:TextBox ID="txtjourneyEmail" runat="server" Width="400px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="txtjourneyEmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Display="Dynamic" ValidationGroup="SiteForm" />
                                <%-- <span style="color: Red; display: inline-table;">Add "," or ";" after the email address</span>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Enable P2P widget tab's:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkEnableP2P" runat="server" />
                            </td>
                            <td class="col" style="width: 165px">Enable P2P widget:
                            </td>
                            <td class="col" style="width: 310px; float: left;">
                                <asp:CheckBox ID="chkEnableP2PWidget" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">P2P Booking day limit:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlBookingDayLimit" runat="server" Width="80" />
                            </td>
                            <td class="col" style="width: 165px">Enable Book Tickets:
                            </td>
                            <td class="col" style="width: 310px;">
                                <asp:CheckBox ID="chktrainticket" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color" colspan="4">
                                <b>Page Control Setting:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 165px">Default home page tab:
                            </td>
                            <td class="col" style="width: 310px; float: left;">
                                <asp:RadioButtonList ID="rdbtnDefaultTab" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="Top Journeys" Selected="True" />
                                    <asp:ListItem Value="0" Text="Top Rail Passes" />
                                </asp:RadioButtonList>
                            </td>
                            <td class="col" style="width: 165px"></td>
                            <td class="col" style="width: 325px; float: left;"></td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 165px">I have a rail pass:
                            </td>
                            <td class="col" style="width: 310px; float: left;">
                                <asp:CheckBox ID="chkHavRailPass" runat="server" />
                            </td>
                            <td class="col" style="width: 165px">Enable Ticket Protection:
                            </td>
                            <td class="col" style="width: 325px; float: left;">
                                <asp:CheckBox ID="chkTicketProtection" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 165px">Enable Contact Us Email:
                            </td>
                            <td class="col" style="width: 310px; float: left;">
                                <asp:CheckBox ID="chkContactEmail" runat="server" />
                            </td>
                            <td class="col">Enable Affiliate Tracking:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkAffiliateTracking" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color" colspan="4">
                                <b>Right Panel Controls:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px">Visible News letter area:
                            </td>
                            <td class="col" style="width: 300px;" colspan="3">
                                <asp:CheckBox ID="chkNewsLetter" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px">Visible Journey request form widget:
                            </td>
                            <td valign="top" class="col" style="width: 300px;" colspan="3">
                                <asp:CheckBox ID="chkJRF" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px">Visible General information widget:
                            </td>
                            <td valign="top" class="col" style="width: 300px;" colspan="3">
                                <asp:CheckBox ID="chkGI" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px">Visible Images:
                            </td>
                            <td valign="top" class="col" style="width: 300px;" colspan="3">
                                <asp:CheckBox ID="chkRtImg" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px"></td>
                            <td valign="top" class="col" style="width: 300px;" colspan="3"></td>
                        </tr>
                    </table>
                    <table class="tblMainSection">
                        <tr style="background: #fff">
                            <td class="col border-bottom-color" colspan="4">
                                <b>Google Code:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px" valign="top">Google Analytics Tracking Script:
                            </td>
                            <td class="col" style="width: 300px;" colspan="3">
                                <textarea id="txtAnalytic" runat="server" style="width: 90%" rows="10"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 300px" valign="top">Google Tag Manager code:
                            </td>
                            <td class="col" style="width: 300px;" colspan="3">
                                <textarea id="txtManager" runat="server" style="width: 90%" rows="10"></textarea>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <center>
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                            Text="Submit" Width="89px" ValidationGroup="SiteForm" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                            Text="Cancel" />
                    </center>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlSiteLogo" runat="server" CssClass="popup pBannerImg">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Site Logo Image
            </div>
            <a href="#" id="btnClose3" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgSiteLogo" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg" />
            <br />
            <br />
            <asp:Button ID="btnRemoveLogo" Text="Remove" runat="server" CssClass="button" OnClick="btnRemoveLogo_Click" />
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdpupBanner" runat="server" TargetControlID="lnkSiteLogo"
        CancelControlID="btnClose3" PopupControlID="pnlSiteLogo" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="pnlSiteFavicon" runat="server" CssClass="popup pBannerImg">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Site Favicon Image
            </div>
            <a href="#" id="btnClose2" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgSiteFavicon" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg" />
            <br />
            <br />
            <asp:Button ID="btnRemoveFavicon" Text="Remove" runat="server" CssClass="button"
                OnClick="btnRemoveFavicon_Click" />
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkFavicon"
        CancelControlID="btnClose2" PopupControlID="pnlSiteFavicon" BackgroundCssClass="modalBackground" />
</asp:Content>
