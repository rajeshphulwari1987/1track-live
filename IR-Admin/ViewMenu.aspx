﻿<%@ Page Title="Navigation " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ViewMenu.aspx.cs" Inherits="IR_Admin.ViewMenu" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">   
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });    
        $(function () {                  
            if(<%=tab.ToString()%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
            }
           
        });  
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>Navigations</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ViewMenu.aspx" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" NavigateUrl="#" CssClass=" " runat="server">New/Edit</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdMenu" runat="server" OnRowCommand="grdMenu_RowCommand" CellPadding="4"
                        CssClass="grid-head2" ForeColor="#333333" DataKeyNames="ID" GridLines="None"
                        AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Navigation Name" />
                            <asp:TemplateField HeaderText="Parent Navigation">
                                <ItemTemplate>
                                    <%#GetMenuName(Guid.Parse(Eval("ParentID").ToString()))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgUp" runat="server" CommandName="Up" ImageUrl="schemes/internationalrail/images/icon-move-up.png"
                                        CommandArgument='<%# String.Format("{0},{1},{2},{3}", Eval("CSortOrder"), Eval("ID"),Eval("ParentID"),Eval("PSortOrder")) %>' />&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDown" runat="server" CommandName="Down" ImageUrl="schemes/internationalrail/images/icon-move-down.png"
                                        CommandArgument='<%# String.Format("{0},{1},{2},{3}", Eval("CSortOrder"), Eval("ID"),Eval("ParentID"),Eval("PSortOrder")) %>' />&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%# String.Format("{0},{1},{2},{3}", Eval("ID"),Eval("ParentID"),Eval("PSortOrder"),Eval("CSortOrder")) %>'
                                        CommandName="Remove" ImageUrl="images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">Navigation Name
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtNameRequiredFieldValidator" runat="server" ControlToValidate="txtName"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="NavForm"></asp:RequiredFieldValidator>
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr>
                            <td class="col">Parent Navigation
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlParentCategory" runat="server" Width="205" AutoPostBack="True" OnSelectedIndexChanged="ddlParentCategory_SelectedIndexChanged" />
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr>
                            <td class="col">Parent Navigation (Second)
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSecondParentCategory" runat="server" Width="205" />
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr>
                            <td class="col">Navigation Url
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPageurl" runat="server" />
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr style="display: none;">
                            <td class="col">Show on Top Navigation
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsTop" runat="server" />
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr style="display: none;">
                            <td class="col">Show on Footer Navigation
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsBottom" runat="server" />
                            </td>
                            <td class="col"></td>
                        </tr>
                        <tr>
                            <td class="col">Is Corporate
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkCorp" runat="server" />
                            </td>
                            <td class="col" style="width: 200px"></td>
                        </tr>
                        <tr>
                            <td class="col">Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                            <td class="col" style="width: 200px"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="NavForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td></td>
                            <asp:HiddenField ID="hdID" runat="server" Value="" />
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
