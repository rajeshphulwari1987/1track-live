﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class Banner : Page
    {
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string Tab = string.Empty;
        string _imagePath = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BannerList(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["edit"] != null)
            {
                Tab = "2";
                if (ViewState["Tab"] != null)
                {
                    Tab = ViewState["Tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                BindSite();
                _siteID = Master.SiteID;
                SiteSelected();
                BannerList(_siteID);
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindListForEdit(Guid.Parse(Request["edit"]));
                    BindLookupforEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _siteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                BannerList(_siteID);
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindSite()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void BindListForEdit(Guid id)
        {
            try
            {
                var result = _master.GetBannerListEdit(id);
                txtTitle.Text = result.Title;
                txtNavUrl.Text = result.NavUrl;
                txtDesp.InnerText = Server.HtmlDecode(result.Description);
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
                //hSortOrder.Value=_master.GetBannerSortingorderByIdandSiteId(id,sit)
                if (!string.IsNullOrEmpty(result.ImageUrl))
                    imgBanner.ImageUrl = result.ImageUrl;
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindLookupforEdit(Guid id)
        {
            var lookUpSites = _db.tblBannerLookups.Where(x => x.BannerId == id).ToList();
            foreach (var lsites in lookUpSites)
            {
                foreach (TreeNode pitem in trSites.Nodes)
                {
                    if (Guid.Parse(pitem.Value) == lsites.SiteID)
                        pitem.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UploadFile();
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    Tab = "2";
                    return;
                }

                if (btnSubmit.Text == "Submit")
                {
                    var newId = Guid.NewGuid();
                    var Banner = new tblBanner
                        {
                            ID = newId,
                            Title = txtTitle.Text.Trim(),
                            NavUrl = txtNavUrl.Text.Trim(),
                            Description = txtDesp.InnerHtml,
                            IsActive = chkactive.Checked,
                            ImageUrl = _imagePath.Replace("~/", "")
                        };

                    var pid = _master.AddBanner(Banner);
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            var BannerLookSites = new tblBannerLookup { ID = Guid.NewGuid(), BannerId = pid, SiteID = Guid.Parse(node.Value) };
                            _master.AddBannerLookupSites(BannerLookSites);
                        }
                    }

                    ShowMessage(1, "Record Added successfully.");
                }
                else if (btnSubmit.Text == "Update")
                {
                    var Banner = new tblBanner
                        {
                            Title = txtTitle.Text.Trim(),
                            NavUrl = txtNavUrl.Text.Trim(),
                            Description = txtDesp.InnerHtml,
                            ID = Guid.Parse(Request["edit"]),
                            IsActive = chkactive.Checked,
                        };

                    if (!string.IsNullOrEmpty(_imagePath))
                        Banner.ImageUrl = _imagePath.Replace("~/", "");

                    var res = _master.UpdateBanner(Banner);
                    if (res != null)
                    {
                        //Delete Existing Record
                        var id = Guid.Parse(Request["edit"]);
                        _db.tblBannerLookups.Where(w => w.BannerId == id).ToList().ForEach(_db.tblBannerLookups.DeleteObject);
                        _db.SaveChanges();

                        //Get All Seletected Site
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                var BannerSiteLookUp = new tblBannerLookup { ID = Guid.NewGuid(), BannerId = res, SiteID = Guid.Parse(node.Value) };
                                _master.AddBannerLookupSites(BannerSiteLookUp);
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ShowMessage(1, "Record Updated successfully.");
                }
                ClearControls();
                Tab = "1";
                ViewState["Tab"] = "1";
                BannerList(_siteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void ClearControls()
        {
            txtTitle.Text = string.Empty;
            txtDesp.InnerHtml = string.Empty;
            chkactive.Checked = false;
            hSortOrder.Value = string.Empty;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Banner.aspx");
        }
        private void BannerList(Guid siteId)
        {
            List<Business.Masters.BannerList> lstdata = new List<Business.Masters.BannerList>();
            Guid swapid=Guid.Empty;
            siteId = Master.SiteID;
            var result = _master.GetBanner(siteId);
            grdBanner.DataSource = result.OrderBy(t => t.sortorder).ThenBy(t => t.Title).ToList();
            grdBanner.DataBind();
            shortorderpnl.Attributes.Add("style", "display:none");
        }

        protected void grdBanner_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "sortorder")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var result = _master.GetBanner(Master.SiteID).FirstOrDefault(t => t.ID == id);
                    if (result != null)
                    {
                        hdnsortorderid.Value = result.ID.ToString();
                        lblTitle.Text = result.Title;
                        lblSortorder.Text = result.sortorder;
                        txtSortorder.Text = result.sortorder;
                        if (string.IsNullOrEmpty(result.sortorder))
                            txtSortorder.Attributes.Add("placeholder", "noorder");
                    }
                    lblerrorshortorder.Text = string.Empty;
                    diverrorshortorder.Visible = false;
                    shortorderpnl.Attributes.Add("style", "display:block");
                    Tab = "1";
                    ViewState["Tab"] = "1";
                }
                else
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactiveBanner(id);
                    Tab = "1";
                    ViewState["Tab"] = "1";
                    BannerList(_siteID);
                }
                else
                if (e.CommandName == "Modify")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    Tab = "2";
                    ViewState["Tab"] = "2";
                    Response.Redirect("Banner.aspx?edit=" + id);
                }
                else
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteBanner(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    SiteSelected();
                    Tab = "1";
                    ViewState["Tab"] = "1";
                    BannerList(_siteID);
                }
            }
            catch (Exception ee)
            {
                ShowMessage(2, ee.ToString());
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupBannerImg.HasFile)
                {
                    if (fupBannerImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupBannerImg.FileName.Substring(fupBannerImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    _imagePath = "~/Uploaded/BannerImg/";
                    _imagePath = _imagePath + oCom.CropImage(fupBannerImg, _imagePath, 200, 300);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                Guid ID = Guid.Parse(hdnsortorderid.Value);
                if (!string.IsNullOrEmpty(txtSortorder.Text))
                {
                    int SortOrder = Convert.ToInt32(txtSortorder.Text);
                    _master.updateBannerSortingorderByIdandSiteId(Master.SiteID, ID, SortOrder);
                }
                shortorderpnl.Attributes.Add("style", "display:none");
                diverrorshortorder.Visible = false;
                lblerrorshortorder.Text = txtSortorder.Text = lblSortorder.Text = lblTitle.Text = hdnsortorderid.Value = string.Empty;
                Tab = "1";
                ViewState["Tab"] = "1";
                BannerList(_siteID);
            }
            catch (Exception ex)
            {
                shortorderpnl.Attributes.Add("style", "display:block");
                Tab = "1";
                ViewState["Tab"] = "1";
                diverrorshortorder.Visible = true;
                lblerrorshortorder.Text = ex.Message;
            }
        }
    }
}