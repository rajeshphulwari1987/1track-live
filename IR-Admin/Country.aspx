﻿<%@ Page Title="Country " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Country.aspx.cs" Inherits="IR_Admin.countryPage" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">   
        function pageLoad(sender, args) {$('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 });$('#MainContent_txtIRContent').redactor({ iframe: true, minHeight: 200 });}

        $(function () {                  
            if(<%=tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {              
                $("ul.tabs").tabs("div.inner-tabs-container > div");
            }         
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            $(".tabs a").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).parent("li").addClass("active");
            });
           
            if(<%=tab%>=="2")
            {
                $(".list a").click(function () {               
                    $("ul.list").tabs("div.panes > div");
                });
            }
            
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        img
        {
            vertical-align: middle;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Country</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Country.aspx" class="current">List</a></li>
            <li><a id="aNew" href="Country.aspx" class="">New</a></li>
            <li id="liCD" runat="server" class="LiCDetail">
                <asp:Label ID="lblCountryDetail" runat="server" Text=""></asp:Label>
            </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="searchDiv">
                        Select Region :
                        <asp:DropDownList ID="ddlRegionWiseCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRegionWiseCountry_SelectedIndexChanged">
                        </asp:DropDownList>
                        Select Continent :
                        <asp:DropDownList ID="ddlContinentSearch" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlContinentSearch_SelectedIndexChanged" />
                    </div>
                    <asp:GridView ID="grdCountry" runat="server" CellPadding="4" DataKeyNames="CountryID"
                        CssClass="grid-head2" OnRowCommand="grdCountry_RowCommand" ForeColor="#333333"
                        GridLines="None" AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("CountryID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Flag">
                                <ItemTemplate>
                                    <div style="float: left; width: 50px; padding: 5px; text-align: center;">
                                        <img alt="map" height="30px" src='<%#(Eval("CountryFlagImg") == null || Eval("CountryFlagImg") == string.Empty)? "images/map.png":Eval("CountryFlagImg")%>' /></div>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CountryName" HeaderText="Name" />
                            <asp:BoundField DataField="CountryCode" HeaderText="Country Code" />
                            <asp:BoundField DataField="EurailCode" HeaderText="Eurail Code" />
                            <asp:BoundField DataField="IsoCode" HeaderText="ISO-code" />
                            <asp:BoundField DataField="DespatchExpressCountryCode" HeaderText="DES-code" />
                            <asp:BoundField DataField="RBSCode" HeaderText="RBS-code" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("CountryID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%# String.Format("{0},{1}", Eval("CountryID"),Eval("RegionID")) %>'
                                        CommandName="Remove" ImageUrl="images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("CountryID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <div class="tabs">
                                    <ul class="tabs">
                                        <li class="active"><a class="current" href="#">Add/Edit</a></li>
                                        <li><a href="#">Information</a></li>
                                    </ul>
                                </div>
                                <!-- tab "panes" -->
                                <div class="inner-tabs-container">
                                    <div id="tabs-inner-1" class="grid-sec2" style="display: block; width: 700px;">
                                        <table class="tblMainSection">
                                            <tr>
                                                <td class="col">
                                                    Select Region
                                                </td>
                                                <td class="col">
                                                    <asp:DropDownList ID="ddlRegion" runat="server">
                                                    </asp:DropDownList>
                                                    &nbsp;<asp:RequiredFieldValidator ID="ddlRegionRequiredFieldValidator" runat="server"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" ControlToValidate="ddlRegion" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Continent
                                                </td>
                                                <td class="col">
                                                    <asp:DropDownList ID="ddlContinent" runat="server" />
                                                    &nbsp;<asp:RequiredFieldValidator ID="reqContinent" runat="server" CssClass="valdreq"
                                                        ErrorMessage="*" ValidationGroup="CForm" ControlToValidate="ddlContinent" InitialValue="0" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Country Name
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtCName" runat="server" />
                                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtCName"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Other Countries Name
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtOtherCName" runat="server" />
                                                    <span style="color: Red">Note: Please enter ',' comma seprated value</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Country Code
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtCCode" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Eurail Code
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtEurailCode" runat="server" MaxLength="3" />
                                                    <asp:RequiredFieldValidator ID="reqEurailCode" runat="server" ControlToValidate="txtEurailCode"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                    <asp:FilteredTextBoxExtender ID="ftbEurailCode" TargetControlID="txtEurailCode" ValidChars=".0123456789"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    ISO Code
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtIsoCode" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    DES Code
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtDEScode" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    RBS Code
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtRBScode" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 5px;" class="col">
                                                    Country Flag Image
                                                </td>
                                                <td class="col">
                                                    <asp:FileUpload ID="fupFlagCountryImg" runat="server" Width="200px" />
                                                    <asp:LinkButton ID="lnkFlag" runat="server" Text="View Flag" CssClass="clsLink"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 5px;" class="col">
                                                    Country Image
                                                </td>
                                                <td class="col">
                                                    <asp:FileUpload ID="fupCountryImg" runat="server" Width="200px" />
                                                    <asp:LinkButton ID="lnkImg" runat="server" Text="View Country Image" CssClass="clsLink"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 5px;" class="col">
                                                    Banner Image
                                                </td>
                                                <td class="col">
                                                    <asp:FileUpload ID="fupBannerImg" runat="server" Width="200px" />
                                                    <asp:LinkButton ID="lnkBanner" runat="server" Text="View Banner Image" CssClass="clsLink"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Is Active
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkactive" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Is Visible
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkVisible" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="tabs-inner-2" class="grid-sec2" style="display: block; width: 700px;">
                                        <table class="tblMainSection">
                                            <tr>
                                                <td class="col">
                                                    Capital
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtCapital" runat="server" />
                                                </td>
                                                <td class="col">
                                                    Population
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtPopulation" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Language
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtLanguage" runat="server" />
                                                </td>
                                                <td class="col">
                                                    Currency
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtCurrency" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Train Operator
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtTrainOperator" runat="server" />
                                                </td>
                                                <td class="col">
                                                    Tourist Board
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtTouristBoard" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Time Zone
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtTimeZone" runat="server" />
                                                </td>
                                                <td class="col">
                                                    Calling Code
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtCallingCode" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Internet TLD
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtInternetTLD" runat="server" />
                                                </td>
                                                <td class="col">
                                                    Visas
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtVisas" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Drives On
                                                </td>
                                                <td class="col">
                                                    <asp:RadioButtonList ID="rdbtnDrivesOn" RepeatDirection="Horizontal" runat="server">
                                                        <asp:ListItem>Left</asp:ListItem>
                                                        <asp:ListItem>Right</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td class="col">
                                                </td>
                                                <td class="col">
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                    Country Title
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtTitle" runat="server" TextMode="MultiLine" Columns="90" MaxLength="2000"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    Country information
                                                </td>
                                                <td colspan="4">
                                                    <textarea id="txtContent" runat="server"></textarea>
                                                </td>
                                            </tr>
                                              <tr>
                                                <td style="vertical-align: top;">
                                                    Country information (Only for IR Rail)
                                                </td>
                                                <td colspan="4">
                                                    <textarea id="txtIRContent" runat="server"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr align="center">
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="CForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="BtnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td class="col">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlFlag" runat="server" CssClass="popup pflag">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Country Flag</div>
            <a href="#" id="btnClose" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgFlag" runat="server" ImageUrl="~/images/map.png" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlImg" runat="server" CssClass="popup pImg">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Country Image</div>
            <a href="#" id="btnClose2" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgCountry" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg" Width="272px"
                Height="190px" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlBanner" runat="server" CssClass="popup pBannerImg">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Country Banner Image</div>
            <a href="#" id="btnClose3" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgBanner" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg" />
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdpupFlag" runat="server" TargetControlID="lnkFlag" CancelControlID="btnClose"
        PopupControlID="pnlFlag" BackgroundCssClass="modalBackground" />
    <asp:ModalPopupExtender ID="mdpupImage" runat="server" TargetControlID="lnkImg" CancelControlID="btnClose2"
        PopupControlID="pnlImg" BackgroundCssClass="modalBackground" />
    <asp:ModalPopupExtender ID="mdpupBanner" runat="server" TargetControlID="lnkBanner"
        CancelControlID="btnClose3" PopupControlID="pnlBanner" BackgroundCssClass="modalBackground" />
</asp:Content>
