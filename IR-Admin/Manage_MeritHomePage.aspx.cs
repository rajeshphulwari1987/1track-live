﻿using System;
using System.Web.UI;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Manage_MeritHomePage : Page
    {
        readonly Masters _master = new Masters();
        db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindImages();
                BindPage();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindImages()
        {
            try
            {
                var resultbanner = _master.GetImageListByID(1); //banner images
                dtBanner.DataSource = resultbanner;
                dtBanner.DataBind();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindPage()
        {
            try
            {
                imgMainBanner.Src = SiteUrl + "images/page/banner.jpg";
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        ContentHead.InnerHtml = result.HomeSearchText;
                        ContentText.InnerHtml = result.HomeCountryText;

                        string[] bannerimgid = result.BannerIDs.TrimEnd(',').Split(',');
                        int bimgid = 0;
                        foreach (string obj in bannerimgid)
                        {
                            if (obj != "")
                                bimgid = Convert.ToInt32(obj);
                        }
                        var bimgpath = _db.tblImages.FirstOrDefault(x => x.ID == bimgid);
                        if (bimgpath != null)
                        {
                            imgMainBanner.Src = bimgpath.ImagePath;
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        protected void dtBanner_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string url = null;
                    if (Session["url"] != null)
                        url = Session["url"].ToString();

                    if (url != string.Empty)
                    {
                        tblPage result = _master.GetPageDetailsByUrl(url);
                        if (result != null)
                        {
                            var bannerid = result.BannerIDs.Trim();
                            string[] arrId = bannerid.Split(',');
                            foreach (string id in arrId)
                            {
                                var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                                if (cbID != null)
                                {
                                    if (id == cbID.Value)
                                    {
                                        cbID.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}