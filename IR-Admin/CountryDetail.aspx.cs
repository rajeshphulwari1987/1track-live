﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI.HtmlControls;

public partial class CountryDetail : System.Web.UI.Page
{
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly Masters _masterPage = new Masters();
    static string _countryId = string.Empty;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private Guid _siteId;
    public string script;
    public static string currency;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _masterPage.IsVisibleNewsLetter(_siteId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowP2PWidget(_siteId);
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Request.QueryString["Id"] != null)
            {
                _countryId = Request.QueryString["Id"];
                BindCountryName(_countryId);
                GetCurrency();

                if (Page.RouteData.Values["PageId"] != null)
                {
                    try
                    {
                        BindTrainDtlImage(Guid.Parse(_countryId), _siteId);
                        BindTopJourneys(Guid.Parse(_countryId), _siteId);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(2, ex.Message);
                    }
                }
            }
        }
    }

    void ShowP2PWidget(Guid siteID)
    {
        //var enableP2P = _oWebsitePage.EnableP2P(siteID);
        //divPopular.Visible = enableP2P;
    }

    public void BindCountryName(string cuId)
    {
        try
        {
            var cId = Guid.Parse(cuId);
            var cuName = _cMaster.GetCountryNameById(cId);
            lblCtryName.Text = lblCntyNm.Text = lblCn.Text = cuName.CountryName;
            imgBanner.ImageUrl = !string.IsNullOrEmpty(cuName.BannerImg)
                                     ? SiteUrl + cuName.BannerImg
                                     : "images/img_inner-banner.jpg";
            imgCountry.ImageUrl = !string.IsNullOrEmpty(cuName.CountryImg)
                                      ? SiteUrl + cuName.CountryImg
                                      : "images/innerMap.gif";

            var cuData = _db.tblCountriesInfoes.FirstOrDefault(x => x.CountryID == cId);
            if (cuData != null) lblCtryInfo.Text = cuData.CountryInfo;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
   
    public void BindTopJourneys(Guid cuId, Guid siteId)
    {
        rptJourney.DataSource = _cMaster.GetTopJourneyByCntryID(cuId, siteId);
        rptJourney.DataBind();
    }

    public void BindTrainDtlImage(Guid ctryId, Guid siteId)
    {
        tptTrainList.DataSource = _cMaster.GetTrainDtlByCId(ctryId, siteId);
        tptTrainList.DataBind();
    }

    public void GetCurrency()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (result != null)
        {
            var currId = result.DefaultCurrencyID;
            if (currId != null)
                currency = oManageClass.GetCurrency(Guid.Parse(currId.ToString()));
            else
                currency = "£";
        }
        else
            currency = "£";
    }
    
    protected void rptJourney_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Book")
        {
            var divSearch = (HtmlGenericControl)ucTrainSearch.FindControl("divSearch");
            if (divSearch != null) divSearch.Attributes.Add("class", "tab-detail-in on-focusdiv");

            var enableP2P = _oWebsitePage.EnableP2P(_siteId);
            if (!enableP2P)
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);

            var id = Guid.Parse(e.CommandArgument.ToString());
            var result = _cMaster.GetJourneyById(id);

            var txtFrm = (TextBox)ucTrainSearch.FindControl("txtFrom");
            var txtTo = (TextBox)ucTrainSearch.FindControl("txtTo");

            var ddldepTime = (DropDownList)ucTrainSearch.FindControl("ddldepTime");
            var ddlReturnTime = (DropDownList)ucTrainSearch.FindControl("ddlReturnTime");
            var ddlClass = (RadioButtonList)ucTrainSearch.FindControl("ddlClass");
            var ddlTransfer = (DropDownList)ucTrainSearch.FindControl("ddlTransfer");

            if (txtFrm != null && txtTo != null)
            {
                txtFrm.Text = result.From;
                txtFrm.Attributes.CssStyle.Add("background-color", "#ECFFFF");
                txtTo.Text = result.To;
                txtTo.Attributes.CssStyle.Add("background-color", "#ECFFFF");
            }

            if (ddldepTime != null)
                ddldepTime.SelectedValue = "09:00";
            if (ddlReturnTime != null)
                ddlReturnTime.SelectedValue = "09:00";
            if (ddlClass != null)
                ddlClass.SelectedValue = "0";
            if (ddlTransfer != null)
                ddlTransfer.SelectedValue = "3";
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}