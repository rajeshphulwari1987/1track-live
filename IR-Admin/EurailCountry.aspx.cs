﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class EurailCountry : System.Web.UI.Page
    {
        readonly private Masters _oMasters = new Masters();
        readonly private ManagePriceBand _PriceBand = new ManagePriceBand();
        public string tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            ShowMessage(0, null);
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }

            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(hdnId.Value))
                    tab = "2";
                FillGrid();
            }
        }

        void FillGrid()
        {
            var list = _oMasters.GetEurailCountryList();
            grvEurailCountry.DataSource = list;
            grvEurailCountry.DataBind();

            ddllevel.DataSource = _PriceBand.GetPriceBandList().Where(t=>t.IsActive).ToList();
            ddllevel.DataTextField = "Name";
            ddllevel.DataValueField = "ID";
            ddllevel.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int result = _oMasters.AddEurailCountry(new tblEurailCountry
                    {
                        ID = string.IsNullOrEmpty(hdnId.Value) ? new Guid() : Guid.Parse(hdnId.Value),
                        CountryCode = int.Parse(txtCountryCode.Text.Trim()),
                        Country1 = txtCountry1.Text.Trim().Length > 0 ? txtCountry1.Text.Trim() : null,
                        Country2 = txtCountry2.Text.Trim().Length > 0 ? txtCountry2.Text.Trim() : null,
                        Country3 = txtCountry3.Text.Trim().Length > 0 ? txtCountry3.Text.Trim() : null,
                        Country4 = txtCountry4.Text.Trim().Length > 0 ? txtCountry4.Text.Trim() : null,
                        Country5 = txtCountry5.Text.Trim().Length > 0 ? txtCountry5.Text.Trim() : null,
                        IsActive = chkactive.Checked,
                        OtherCountry=txtOtherCountry.Text,
                        PriceBindLevel = Convert.ToInt32(ddllevel.SelectedValue)
                    });
                ShowMessage(1, !String.IsNullOrEmpty(hdnId.Value) ? "Record updated successfully" : "Record added successfully");
                hdnId.Value = string.Empty;
                txtCountryCode.Text = txtCountry1.Text = txtCountry2.Text = txtCountry3.Text = txtCountry4.Text = txtCountry5.Text = string.Empty;
                dvcl2.Visible = dvcl1.Visible = false;
                FillGrid();
                tab = "1";
                ViewState["tab"] = 1;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("EurailCountry.aspx");
        }

        protected void grvEurailCountry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvEurailCountry.PageIndex = e.NewPageIndex;
            FillGrid();
            tab = "1";
        }

        protected void grvEurailCountry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                var ocls = _oMasters.GetEurailCountryById(id);
                txtCountryCode.Text = ocls.CountryCode.ToString();
                txtCountry1.Text = ocls.Country1;
                txtCountry2.Text = ocls.Country2;
                txtCountry3.Text = ocls.Country3;
                txtCountry4.Text = ocls.Country4;
                txtCountry5.Text = ocls.Country5;
                chkactive.Checked = ocls.IsActive;
                txtOtherCountry.Text = ocls.OtherCountry;
                hdnId.Value = id.ToString();
                ddllevel.SelectedValue = ocls.PriceBindLevel.ToString();
                if (ocls.CountryCode > 2000)
                    dvcl1.Visible = dvcl2.Visible = true;
                ViewState["tab"] = "2";
                tab = "2";
            }
            if (e.CommandName == "Remove")
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                bool result = _oMasters.DeleteEurailCountry(id);
                if (result)
                    ShowMessage(1, "Record deleted successfully");
                FillGrid(); tab = "1";
                ViewState["tab"] = "1";
            }
            if (e.CommandName == "ActiveInActive")
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                _oMasters.ActiveInactiveEurailCountry(id);
                FillGrid(); tab = "1";
                ViewState["tab"] = "1";
            }
        }

        protected void txtCountryCode_TextChanged(object sender, EventArgs e)
        {
            string txtcode = txtCountryCode.Text;
            if (txtcode.Length > 0)
            {
                string match = txtcode.Substring(0, 1);
                if (match.Equals("3") || match.Equals("4") || match.Equals("5"))
                    dvcl2.Visible = dvcl1.Visible = true;
                else
                    dvcl2.Visible = dvcl1.Visible = false;
            }
            ViewState["tab"] = "2";
            tab = "2";
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}