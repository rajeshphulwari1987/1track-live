﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.Configuration;
using System.IO;

namespace IR_Admin.Orders
{
    public partial class OrderDetails : Page
    {
        public int nxtpaging = 0;
        private const int pageSize = 50;
        private Int32 TotalRecord = 1;
        private readonly Masters _masterPage = new Masters();
        public string Currency = "$";
        private readonly ManageOrder _master = new ManageOrder();
        private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();
        private readonly ManageBooking Objbooking = new ManageBooking();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string Tab = string.Empty;
        public bool isSaver = true;
        public bool isTrvEdit = true;
        private string htmfile = string.Empty;
        public string billingEmailAddress;
        public string shippingEmailAddress;
        public bool IsVisibleAttachmentDeleteBtn = false;
        private string HtmlFileSitelog = string.Empty;
        public string SiteHeaderColor = string.Empty;
        public string Name = "";
        public string logo = "";

        #region [ Page InIt must write on every page of CMS ]
        private Guid _siteId;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            txtOrderId.Text = string.Empty;
            txtOrderPassId.Text = string.Empty;
            txtTrvName.Text = string.Empty;
            txtRef.Text = string.Empty;
            txtStockNo.Text = string.Empty;
            ddlOrderSt.SelectedValue = "-1";
            ViewState["PageIndex"] = null; TotalRecord = 1;
            _siteId = Guid.Parse(selectedValue);
            BindGrid();
            BindPager();
            PageloadEventBranch();
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtDob.Attributes.Add("onkeyup", "return preventDt()");
                txtOrderId.Attributes.Add("onkeypress", "return keycheck()");
                txtPhone.Attributes.Add("onkeypress", "return keycheck()");
                txtStockNo.Attributes.Add("onkeypress", "return keycheck()");
                txtTrvName.Attributes.Add("onkeypress", "return keynumcheck()");
                _siteId = Master.SiteID;
                divlist.Visible = true;
                divDetail.Visible = false;
                var data = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
                if (data != null)
                {
                    OrderAttachments.Visible = (data.IsOrderAttachmentAllow || _master.IsOrderAttachmentExist(Convert.ToInt32(Request["id"])));
                    IsVisibleAttachmentDeleteBtn = data.RoleName.ToLower().Trim() == "admin";
                    Guid Bookmyrst = Guid.Parse("a5f8e257-d4e1-48e9-bfa4-59a582ebe56f");
                    if (data.IsEditableBookingFee && _siteId == Bookmyrst)
                        lnkeditbooking.Visible = true;
                    else
                        lnkeditbooking.Visible = false;
                }
                if (!IsPostBack)
                {
                    Session["syswide"] = divsyswide.Visible = _masterPage.GetSystemReportByUserId(AdminuserInfo.UserID);//= chkSysWide.Checked 
                    GetCurrencyCode();
                    if (Request["id"] == null)
                    {
                        PageLoadEvent();
                        PageloadEventBranch();
                    }
                    if ((Request["id"] != null) && (Request["id"] != ""))
                    {
                        #region order not exists and not valid
                        int OrderId = 0;
                        var haveint = Int32.TryParse(Request["id"], out OrderId);
                        if (haveint)
                        {
                            if (!_db.tblOrders.Any(x => x.OrderID == OrderId))
                            {
                                ShowMessage(2, "Record not found order no. " + OrderId);
                                return;
                            }
                        }
                        else
                        {
                            ShowMessage(2, "Record not found and order " + Request["id"] + " not valid.");
                            return;
                        }
                        #endregion

                        ISAffiliateUser();
                        BindOrderStatus();
                        BindCountry();
                        BindAttachments();
                        isTrvEdit = true;
                        var tckPrintStatus = _master.GetstockallocationHistory(Convert.ToInt64(Request.Params["id"]));

                        TicketPrintedDIV.Attributes.Add("title", tckPrintStatus.Replace("<br/>", string.Empty));
                        if (tckPrintStatus.Contains("Order in printed"))
                        {
                            divprintqueuein.InnerHtml = tckPrintStatus;
                            lblTicketPrint.Text = "Yes";
                            isTrvEdit = false;
                            divPrint.Style.Add("display", "none");
                        }
                        else if (!tckPrintStatus.Contains("No Ticket in Printing Process."))
                        {
                            divprintqueuein.InnerHtml = tckPrintStatus;
                            lblTicketPrint.Text = "No";
                            divPrint.Style.Add("display", "none");
                        }
                        else
                        {
                            divprintqueuein.Style.Add("display", "none");
                            lblTicketPrint.Text = "No";
                        }
                        if (!_master.GetAllowedPrinting(Convert.ToInt64(Request.Params["id"])))
                            divPrint.Style.Add("display", "none");


                        lnkPrintReceipt.HRef = "OrderSaleRecipt.aspx?id=" + Request["id"];
                        divlist.Visible = false;
                        divDetail.Visible = true;
                        GetOrdersForEdit(Convert.ToInt32(Request["id"]));
                        if ((Request["notesid"] != null) && (Request["notesid"] != ""))
                        {
                            EditOrderNotes(Guid.Parse(Request["notesid"]));
                            btnAddNotes.Text = "Update";
                        }
                    }
                }
                //RefundEMail();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ISAffiliateUser()
        {
            try
            {
                bool isaff = _master.AffiliateUser(Convert.ToInt64(Request["id"]));
                netpricesection.Visible = !isaff;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindOrderStatus()
        {
            try
            {
                ddlOrderStatus.DataSource = new ManageOrder().GetStatusList().OrderBy(a => a.Name).ToList();
                ddlOrderStatus.DataValueField = "ID";
                ddlOrderStatus.DataTextField = "Name";
                ddlOrderStatus.DataBind();
                ddlOrderStatus.Items.Insert(0, new ListItem("--Select Order--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void PageloadEventBranch()
        {
            try
            {
                _siteId = Master.SiteID;
                var list = new ManageOrder().GetAllBranchlistBySiteId(_siteId).OrderBy(t => t.PathName);
                ddlOffice.DataSource = list;
                ddlOffice.DataTextField = "PathName";
                ddlOffice.DataValueField = "Id";
                ddlOffice.DataBind();
                ddlOffice.Items.Insert(0, new ListItem("-All Office-", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void GetCurrencyCode()
        {
            try
            {
                if (Request["id"] == null)
                    return;

                FrontEndManagePass oManageClass = new FrontEndManagePass();
                Int32 orderno = Convert.ToInt32(Request["id"]);
                tblOrder objtblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderno);
                if (objtblord != null)
                {
                    var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == objtblord.SiteID);
                    if (currencyID != null)
                        Currency = oManageClass.GetCurrency((Guid)currencyID.DefaultCurrencyID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void PageLoadEvent()
        {
            try
            {
                var status = _master.BindOrderStatus();
                ddlOrderSt.DataSource = status;
                ddlOrderSt.DataTextField = "Name";
                ddlOrderSt.DataValueField = "ID";
                ddlOrderSt.DataBind();
                ddlOrderSt.Items.Insert(0, new ListItem("-Status-", "-1"));
                BindGrid();
                BindPager();
                BindCountry();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void BindGrid()
        {
            try
            {
                string SiteId = Master.SiteID.ToString();
                if (chkSysWide.Checked)
                    SiteId = "2";
                DateTime d1 = DateTime.Now.AddMonths(-6).Date;
                DateTime d2 = DateTime.Now.Date;
                if (string.IsNullOrEmpty(txtStartDate.Text) && string.IsNullOrEmpty(txtLastDate.Text))
                {
                    txtStartDate.Text = d1.ToString("dd/MM/yyyy");
                    txtLastDate.Text = d2.ToString("dd/MM/yyyy");
                }
                if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
                {
                    d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", null);
                    d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", null);
                }
                int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
                String Status = ddlOrderSt.SelectedValue == "-1" ? "" : ddlOrderSt.SelectedValue;
                var list = _master.GetOrderList(SiteId, txtOrderId.Text, txtOrderPassId.Text, txtTrvName.Text, txtRef.Text, txtStockNo.Text, Status, AdminuserInfo.UserID, ddlOffice.SelectedValue, d1, d2, Convert.ToInt32(ddltypeDate.SelectedValue), pageIndex, pageSize);
                if (ddlOrderSt.SelectedValue != "-1")
                    list = list.Where(x => x.OrderStatusID == ddlOrderSt.SelectedValue).ToList();

                var DataRecord = list.FirstOrDefault();
                if (DataRecord != null)
                    TotalRecord = DataRecord.totalRecord.Value;
                if (ViewState["sortExpression"] != null)
                {
                    if (ViewState["sortExpression"].ToString() == "DateOfDepart")
                    {
                        if (GridViewSortDirection == SortDirection.Descending)
                            list = list.OrderBy(x => x.DateOfDepart).ToList();
                        else
                            list = list.OrderByDescending(x => x.DateOfDepart).ToList();
                    }
                    else
                    {
                        if (GridViewSortDirection == SortDirection.Descending)
                            list = list.OrderBy(x => x.CreatedOn).ToList();
                        else
                            list = list.OrderByDescending(x => x.CreatedOn).ToList();
                    }
                }
                grdOrders.DataSource = list;
                grdOrders.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void LoadPrintQueue(Guid agentid)
        {
            try
            {
                var stockList = _oPrint.GetStockList(AdminuserInfo.UserID);
                ddlPrntQ.DataSource = stockList;
                ddlPrntQ.DataTextField = "Name";
                ddlPrntQ.DataValueField = "ID";
                ddlPrntQ.DataBind();
                ddlPrntQ.Items.Insert(0, new ListItem("--Stock--", "-1"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void GetOrdersForEdit(long ordId)
        {
            try
            {
                bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                hdnRefundType.Value = IsFullRefund ? "true" : "false";
                BindOrderNotes(Convert.ToInt32(Request["id"]));
                var oP = _master.GetOrderById(ordId);
                if (oP != null)
                {
                    if (Session["syswide"] != null && Convert.ToBoolean(Session["syswide"].ToString()))
                        Session["syswidesite"] = oP.SiteID.Value;
                    bool Refbooking = _master.GetAnyOrderRefunds(ordId, 1);//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
                    lnkRefBookingFee.Visible = !Refbooking;
                    bool RefShippingFee = _master.GetAnyOrderRefunds(ordId, 2);//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
                    lnkRefShippingFee.Visible = !RefShippingFee;
                    bool RefAdminFee = _master.GetAnyOrderRefunds(ordId, 3);//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
                    lnkRefAdminFee.Visible = !RefAdminFee;
                    LoadPrintQueue(oP.AgentID);
                    Currency = _master.GetCurrcyBySiteId(Guid.Parse(oP.SiteID.ToString()));
                    divNote.InnerHtml = (oP.Note);
                    lblPaymentDate.Text = (oP.PaymentDate.HasValue ? oP.PaymentDate.Value.ToString("dd MMM yyyy HH':'mm 'GMT'") : "-----");
                    lblSite.Text = oP.SiteName;
                    lblOrderID.Text = oP.OrderID.ToString(CultureInfo.InvariantCulture);
                    lblStatus.Text = oP.OrderStatus;
                    txtIp.Text = oP.IpAddress;
                    txtDepDt.Text = oP.DateOfDepart != null ? Convert.ToDateTime(oP.DateOfDepart).ToString("dd MMM yyyy") : (oP.PassStartDate != null ? Convert.ToDateTime(oP.PassStartDate).ToString("dd MMM yyyy") : null);
                    txtOrdCDate.Text = oP.CreatedOn.ToString("dd MMM yyyy HH':'mm 'GMT'");
                    lblShippingCost.Text = oP.ShippingAmount != null ? oP.ShippingAmount.ToString() : "0.00";
                    decimal bookingFee = oP.BookingFee;
                    if (bookingFee < 1)
                        lnkRefBookingFee.Visible = false;
                    if (oP.AdminFee < 1)
                        lnkRefAdminFee.Visible = false;
                    if (Convert.ToDecimal(lblShippingCost.Text) < 1)
                        lnkRefShippingFee.Visible = false;
                    lblBookindFee.Text = bookingFee.ToString();
                    lblAdminFee.Text = oP.AdminFee.ToString("F");
                    LblEurail.Text = _master.GetEurailData(oP.OrderID).ToString("F");
                    LblNonEurail.Text = _master.GetNonEurailData(oP.OrderID).ToString("F");
                    lblNetPrice.Text = (Convert.ToDecimal(LblEurail.Text) + Convert.ToDecimal(LblNonEurail.Text)).ToString("F");
                    lblAgentRef.Text = oP.AgentReferenceNo;
                    Session["OrderStatusId"] = oP.OrderStatusID;

                    if (oP.OrderStatusID == 1)
                        divPrint.Style.Add("display", "none");

                    var prdPass = _master.GetPassSale(ordId); //calculate price
                    decimal TicketProtectionCharge = prdPass.Sum(ty => ty.TicketProtection);
                    decimal Markup = 0;
                    decimal Commission = 0;

                    Guid siteId = Master.SiteID;
                    Guid siteCurrencyId = (Guid)Objbooking.GetCurrencyIdBySiteId(siteId);
                    foreach (var item in prdPass)
                    {
                        Guid ProductCurrencyId = (Guid)Objbooking.GetCurrencyIdByProductId(item.ProductID);
                        Markup += FrontEndManagePass.GetPriceAfterConversion(item.Markup, siteId, ProductCurrencyId, siteCurrencyId);
                        Commission += FrontEndManagePass.GetPriceAfterConversion(item.Commission, siteId, ProductCurrencyId, siteCurrencyId);
                    }
                    decimal netTotal = _master.GetOrderPriceSum(ordId);
                    lblTckProtection.Text = TicketProtectionCharge.ToString(CultureInfo.InvariantCulture);
                    lblGrossTotal.Text = (netTotal).ToString("F");
                    lblDiscount.Text = _master.GetOrderDiscountByOrderId(ordId);
                    decimal Extracharge = Objbooking.GetExtraChargeByOrderId(ordId);
                    lblGrandTotal.Text = ((netTotal + Convert.ToDecimal(lblShippingCost.Text) + bookingFee + TicketProtectionCharge + oP.AdminFee) - (Convert.ToDecimal(lblDiscount.Text))).ToString("F");
                    grdPass.DataSource = prdPass.OrderBy(s => s.OrderIdentity).ThenBy(x => x.RailPassName).ThenBy(ty => ty.CountryName).ThenBy(ty => ty.PassDesc).ThenBy(ty => ty.TrvClass).ThenBy(ty => ty.StockNumber);
                    grdPass.DataBind();
                    var dataprdTrv = _master.GetTraveller(ordId);
                    var prdTrv = dataprdTrv.Select(ty => new
                        {
                            ID = ty.ID,
                            TicketNumber = ty.TicketNumber,
                            Country = ty.Country,
                            Title = ty.Title,
                            FirstName = ty.FirstName,
                            MiddleName = ty.MiddleName,
                            LastName = ty.LastName,
                            DOB = ty.DOB,
                            CountryName = ty.CountryName,
                            PassportNo = ty.PassportNo,
                            Nationality = ty.Nationality,
                            PassStartDate = ty.PassStartDate,
                            LeadPassenger = ty.LeadPassenger,
                            StockNo = prdPass.FirstOrDefault(v => v.ID == ty.PassSalesId).StockNumber,
                            OrderIdentity = ty.OrderIdentity
                        }).OrderBy(ty => ty.OrderIdentity).ToList();
                    grdTraveller.DataSource = prdTrv;
                    grdTraveller.DataBind();

                    var billing = _master.GetBillingInfo(ordId);
                    if (billing.Any())
                    {
                        rptBilling.DataSource = rptShipping.DataSource = billing;
                        rptBilling.DataBind();
                        rptShipping.DataBind();
                        btnSendEmail.Visible = true;
                    }

                    var ccInfo = _master.GetOrderCreditCardInfo(Convert.ToInt64(ordId)).FirstOrDefault();
                    if (ccInfo != null)
                    {
                        lblCardholderName.Text = ccInfo.CardholderName;
                        lblCardNumber.Text = string.IsNullOrEmpty(ccInfo.CardNumber) ? "" : "XXXX-XXXX-XXXX-XXXX";
                        lblPaymentId.Text = ccInfo.PaymentId;
                        lblExpDate.Text = string.IsNullOrEmpty(ccInfo.ExpDate) ? "" : "XX/XX";
                        lblBrand.Text = ccInfo.Brand;
                    }

                    var agentDetails = _master.GetAgentDetails(Convert.ToInt64(ordId)).FirstOrDefault();
                    if (agentDetails != null)
                    {
                        tblAgent.Visible = true;
                        tblNonAgent.Visible = false;
                        lblAgentNm.Text = agentDetails.Name;
                        lblAgentUnm.Text = agentDetails.UserName;
                        lblAgentOff.Text = agentDetails.Office;
                    }
                    else
                    {
                        tblNonAgent.Visible = true;
                        tblAgent.Visible = false;
                    }
                    var data = _master.GetOrderAdminRefundById(Convert.ToInt32(ordId));
                    if (data != null)
                    {
                        lblshippingref.Text = data.ShippingFee.ToString("F");
                        lblbookingref.Text = data.BookingFee.ToString("F");
                        lbladminref.Text = data.AdminFee.ToString("F");
                    }
                    else
                    {
                        lblshippingref.Text = "0.00";
                        lblbookingref.Text = "0.00";
                        lbladminref.Text = "0.00";
                    }
                    lblship.Text = lblShippingCost.Text;
                    lblbook.Text = lblBookindFee.Text;
                    lbladmin.Text = lblAdminFee.Text;
                    var refundDetails = _master.GetRefundDetails(Convert.ToInt64(ordId));
                    if (refundDetails != null && refundDetails.Count > 0)
                    {
                        var refund = refundDetails.Select(ty => new
                            {
                                Product = ty.Product,
                                Traveller = ty.Traveller,
                                User = ty.User,
                                Price = ty.Price,
                                Refund = ty.Refund,
                                RefundDate = ty.RefundDate,
                                StockNo = prdPass.FirstOrDefault(v => v.ID == ty.PassSalesId).StockNumber,
                                IsFullRefund = IsFullRefund,
                                PassSalesId = ty.PassSalesId
                            }).OrderBy(ty => ty.StockNo).ToList();
                        lablCurrentGrandTotal.Text = (((netTotal + Convert.ToDecimal(lblShippingCost.Text) + bookingFee + TicketProtectionCharge + Extracharge + oP.AdminFee)) - refund.Sum(x => x.Refund)).ToString("F");
                        // - (Convert.ToDecimal(lblDiscount.Text) +  )
                        grdRefund.DataSource = refund;
                        grdRefund.DataBind();
                        BtnRefundPrint.HRef = ConfigurationManager.AppSettings["HttpHost"] + "Orders/RefundReceipt.aspx?id=" + Request.Params["id"];
                    }
                    else
                    {
                        BtnRefundPrint.Visible = false;
                        BtnRefundMailSend.Visible = false;
                    }
                    gvagentupdatestatus.DataSource = Objbooking.GetListUpdateOrderStatusLog(Convert.ToInt64(ordId));
                    gvagentupdatestatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            BindGrid();
            BindPager();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                _siteId = Master.SiteID;
                AddUpdatePrintQueue(_siteId);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void AddUpdatePrintQueue(Guid siteId)
        {
            try
            {
                int OrderId = Convert.ToInt32(Request["id"]);
                var list = _db.tblPassSales.Where(x => x.OrderID == OrderId).ToList();
                Guid AgentID = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId).AgentID;
                if (AgentID == Guid.Empty)
                    _oPrint.updateOrderAgentIdForPublicSite(OrderId, AgentID = AdminuserInfo.UserID);

                foreach (var item in list)
                {
                    _oPrint.AddUpdatePrintQ(OrderId, item.ID, siteId);
                    _oPrint.AddPrintQItems(new tblPrintQueueItem
                        {
                            ID = Guid.NewGuid(),
                            SiteID = siteId,
                            OrderID = Convert.ToInt32(Request["id"]),
                            CategoryID = item.CategoryID,
                            ProductID = item.ProductID,
                            PassSaleID = item.ID,
                            Status = Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b"),
                            AdminUserID = AgentID,
                            DateTimeStamp = DateTime.Now,
                            QueueID = Guid.Parse(ddlPrntQ.SelectedValue)
                        });
                }
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        protected void grdPass_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    bool SaverRefund = false;
                    if (Request["id"] != null)
                    {
                        SaverRefund = _master.GetSaverRefunded(Convert.ToInt64(Request["id"]));
                    }
                    foreach (GridViewRow row in grdPass.Rows)
                    {
                        var lnkEditExtraCharge = row.FindControl("lnkEditExtraCharge") as LinkButton;
                        var lblExtraChargeAmount = row.FindControl("lblExtraChargeAmount") as Label;
                        var hdnpasssaleid = row.FindControl("hdnpasssaleid") as HiddenField;
                        var lnkRefund = row.FindControl("lnkRefund") as LinkButton;
                        var hdnIsSaver = row.FindControl("hdnIsSaver") as HiddenField;
                        var hdnProductID = row.FindControl("hdnProductID") as HiddenField;
                        var hdnID = row.FindControl("hdnID") as HiddenField;
                        if (!string.IsNullOrEmpty(hdnpasssaleid.Value))
                        {
                            Guid Id = Guid.Parse(hdnpasssaleid.Value);
                            decimal ExtraCharge = Objbooking.GetExtraCharge(Id, "Pass");
                            if (ExtraCharge > 0)
                            {
                                lblExtraChargeAmount.Text = Currency + " " + ExtraCharge.ToString("F");
                                lnkEditExtraCharge.Visible = false;
                            }
                            else
                                lblExtraChargeAmount.Visible = false;
                        }

                        if (lnkRefund != null && hdnID != null)
                        {
                            var isRefunded = _master.GetIsRefunded(Guid.Parse(hdnID.Value));
                            Guid ProductId = !string.IsNullOrEmpty(hdnProductID.Value) ? Guid.Parse(hdnProductID.Value) : Guid.Empty;
                            if (!(_db.tblProducts.FirstOrDefault(x => x.ID == ProductId).IsAllowRefund))
                                lnkRefund.Visible = false;
                            if (isRefunded)
                                lnkRefund.Visible = false;
                            else
                            {
                                if (hdnIsSaver.Value == "yes" && isSaver)
                                {
                                    isSaver = false;
                                    if (SaverRefund)
                                        lnkRefund.Visible = false;
                                    else
                                        lnkRefund.Visible = true;
                                    lnkRefund.PostBackUrl = "OrderRefund.aspx?prdId=" + hdnID.Value;
                                }
                                else if (hdnIsSaver.Value == "no")
                                {
                                    lnkRefund.Visible = true;
                                    lnkRefund.PostBackUrl = "OrderRefund.aspx?prdId=" + hdnID.Value;
                                }
                                else
                                {
                                    lnkRefund.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void rptShipping_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            tblOrderBillingAddress objBillAddress =
                Objbooking.GetBillingShippingAddress(Convert.ToInt64(Request["id"]));
            if (objBillAddress != null)
            {
                txtAdd.Text = objBillAddress.Address1Shpg;
                txtAdd2.Text = objBillAddress.Address2Shpg;
                txtCity.Text = objBillAddress.CityShpg;
                txtEmail.Text = objBillAddress.EmailAddressShpg;
                txtFirst.Text = objBillAddress.FirstNameShpg;
                txtLast.Text = objBillAddress.LastNameShpg;
                txtState.Text = objBillAddress.StateShpg;
                txtZip.Text = objBillAddress.PostcodeShpg;
                ddlMr.SelectedValue = objBillAddress.TitleShpg.Trim();
                ddlCountry.SelectedValue = objBillAddress.CountryShpg.Trim();
                txtPhone.Text = objBillAddress.PhoneShpg;
            }
            mdpupShipping.Show();

            divlist.Visible = false;
            divDetail.Visible = true;
        }

        private void BindCountry()
        {
            var data = new ManageTrainDetails().GetCountryDetail();
            ddlCountry.DataSource = data;
            ddlCountry.DataValueField = "CountryName";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            ddlTrCountry.DataSource = data;
            ddlTrCountry.DataValueField = "CountryID";
            ddlTrCountry.DataTextField = "CountryName";
            ddlTrCountry.DataBind();
            ddlTrCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (SendMail())
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.');", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Failed to send E-mail confimation! Please check Email Settings.');", true);
            GetOrdersForEdit(Convert.ToInt64(Request["id"]));
            mupTraveller.Hide();
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                _siteId = Master.SiteID;
                GetCurrencyCode();
                bool isDiscountSite = _master.GetOrderDiscountVisibleByOrderId(Convert.ToInt64(Request["id"]));
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    var orderID = Convert.ToInt32(Request["id"]);
                    string Subject = "Order Confirmation #" + orderID;

                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(_siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(_siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml;

                    string UserName, EmailAddress, DeliveryAddress, BillingAddress, OrderDate, OrderNumber, Total, ShippingAmount, GrandTotal, BookingFee, NetTotal, Discount, AdminFee;
                    UserName = Discount = NetTotal = EmailAddress = DeliveryAddress = BillingAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = BookingFee = AdminFee = "";
                    string PassProtection = "0.00";
                    string PassProtectionHtml = "";
                    string DiscountHtml = "";
                    bool isAgentSite = false;

                    var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Request.Params["id"])).OrderBy(a => a.OrderIdentity).ToList();
                    var lst1 = from a in lst
                               select
                                   new
                                   {
                                       Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                       ProductDesc =
                               (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                                " " + a.LastName),
                                       TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                   };
                    if (lst.Count > 0)
                    {
                        EmailAddress = lst.FirstOrDefault().EmailAddress;
                        UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                        DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                        lst.FirstOrDefault().DCountry + "<br>" +
                        lst.FirstOrDefault().Postcode + "<br>" +
                        GetBillingAddressPhoneNo(Convert.ToInt64(Request.Params["id"]));

                        OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                        Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                        ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                        BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                        Discount = _master.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));
                        AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(orderID).ToString();
                        NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                            (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                            (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                            Convert.ToDecimal(AdminFee)).ToString();
                        GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                        var billAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(orderID));
                        if (billAddress != null)
                        {
                            BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                        }
                    }

                    // to address
                    string ToEmail = EmailAddress;
                    GetReceiptLoga();
                    body = body.Replace("##headerstyle##", SiteHeaderColor);
                    body = body.Replace("##sitelogo##", logo);
                    body = body.Replace("##OrderNumber##", OrderNumber);
                    body = body.Replace("##UserName##", UserName.Trim());
                    body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                    body = body.Replace("##OrderDate##", OrderDate.Trim());
                    body = body.Replace("##Items##", Currency + " " + Total.Trim());
                    body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");

                    var tblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderID);
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    isAgentSite = objsite.IsAgent.Value;

                    string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + AdminFee.Trim() + "</td></tr>";
                    if (AdminFee == "0.00")
                        AdminFeeRow = string.Empty;
                    if (isDiscountSite)
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        if (Discount == "0.00")
                            body = body.Replace("##Discount##", "");
                        else
                        {
                            DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + Discount.Trim() + "</td></tr>";
                            body = body.Replace("##Discount##", DiscountHtml);
                        }
                    }
                    else
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        body = body.Replace("##Discount##", "");
                    }


                    body = body.Replace("##BookingRef##", "");
                    body = body.Replace("##Shipping##", Currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##DeliveryFee##", Currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##BookingFee##", Currency + " " + BookingFee);
                    body = body.Replace("##Total##", Currency + " " + GrandTotal.Trim());
                    if (tblord != null)
                    {
                        Session["ShipMethod"] = tblord.ShippingMethod;
                        Session["ShipDesc"] = tblord.ShippingDescription;
                    }

                    string britrailpromoTerms = isBritRailPromoPass(orderID) ? "BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a>" : "";
                    if (Session["ShipMethod"] != null)
                    {
                        string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                        body = body.Replace("##ShippingMethod##", shippingdesc + britrailpromoTerms);
                    }
                    else
                        body = body.Replace("##ShippingMethod##", britrailpromoTerms);

                    body = body.Replace("#Blanck#", "&nbsp;");
                    var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                    var lstC = new ManageBooking().GetAllCartData(orderID).OrderBy(a => a.OrderIdentity).ToList();
                    var lstNew = (from a in lstC
                                  select new
                                  {
                                      a,
                                      Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                      ProductDesc = a.ProductType == "P2P"
                                      ? (Objbooking.getP2PDetailsForEmail(a.PassSaleID, null, a.ProductType, false, isAgentSite, ""))
                                      : (Objbooking.getPassDetailsForEmail(a.PassSaleID, null, a.ProductType, "")),
                                      TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                      CommissionFee = a.CommissionFee,
                                      Terms = a.terms
                                  }).ToList();

                    string strProductDesc = "";
                    int i = 1;
                    if (lstNew.Count() > 0)
                    {
                        lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                        body = body.Replace("##HeaderText##", "Delivery address");
                        body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                        body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());

                        foreach (var x in lstNew)
                        {
                            if (x.a.ProductType != "P2P")
                            {
                                string nettPrice = Currency + " " + (_master.GetEurailAndNonEurailPrice(x.a.PassSaleID.Value)).ToString("F");
                                strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                                if (isAgentSite)
                                    strProductDesc += x.ProductDesc.Replace("##Price##", Currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + x.a.LastName).Replace("##NetAmount##", "Net Amount :" + nettPrice);
                                else
                                    strProductDesc += x.ProductDesc.Replace("##Price##", Currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + x.a.LastName).Replace("##NetAmount##", "");

                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + (x.a.PassStartDate.HasValue ? x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") : string.Empty) + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";
                                strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");

                                string ElectronicNote = Objbooking.GetElectronicNote(x.a.PassSaleID.Value);
                                if (ElectronicNote.Length > 0)
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;'>" + ElectronicNote + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";

                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" +
                                    (x.TktPrtCharge > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + Currency + " " + Convert.ToString(x.TktPrtCharge) + "</strong></td></tr>";
                                strProductDesc += "</table></td></tr></table>";
                                PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            }
                            i++;
                        }
                    }
                    PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + PassProtection.Trim() + "</td></tr>";
                    body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                    body = body.Replace("##OrderDetails##", strProductDesc);
                    body = body.Replace("##BillingAddress##", BillingAddress);
                    body = body.Replace("##PrintAtHome##", "");
                    body = body.Replace("../images/", SiteName + "images/");

                    var billing = _master.GetBillingInfo(orderID);
                    if (billing.Any())
                    {
                        var firstOrDefault = billing.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            billingEmailAddress = firstOrDefault.EmailAddress;
                            shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                        }
                    }
                    /*Get smtp details*/
                    var result = _masterPage.GetEmailSettingDetail(_siteId);
                    if (result != null)
                    {
                        var fromAddres = new MailAddress(result.Email, result.Email);
                        smtpClient.Host = result.SmtpHost;
                        smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                        smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                        message.From = fromAddres;
                        message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        smtpClient.Send(message);
                        retVal = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Failed to send E-mail confimation!");
            }
            return retVal;
        }

        public string GetBillingAddressPhoneNo(long OrderID)
        {
            var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            if (result != null)
            {
                return !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
            }
            return "";
        }

        public void GetReceiptLoga()
        {
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    logo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    logo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    logo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        bool isBritRailPromoPass(long orderid)
        {
            try
            {
                foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
                {
                    bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                    if (isPromo)
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            try
            {
                var objBillAddress = new tblOrderBillingAddress();
                if (objBillAddress != null)
                {
                    objBillAddress.Address1Shpg = txtAdd.Text;
                    objBillAddress.Address2Shpg = txtAdd2.Text;
                    objBillAddress.CityShpg = txtCity.Text;
                    objBillAddress.EmailAddressShpg = txtEmail.Text;
                    objBillAddress.FirstNameShpg = txtFirst.Text;
                    objBillAddress.LastNameShpg = txtLast.Text;
                    objBillAddress.StateShpg = txtState.Text;
                    objBillAddress.PostcodeShpg = txtZip.Text;
                    objBillAddress.TitleShpg = ddlMr.SelectedValue;
                    objBillAddress.CountryShpg = ddlCountry.SelectedValue;
                    objBillAddress.PhoneShpg = txtPhone.Text;
                    Objbooking.UpdateShippingAddress(Convert.ToInt64(Request["id"]), objBillAddress);
                }
                var billing = _master.GetBillingInfo(Convert.ToInt32(Request["id"]));
                rptShipping.DataSource = billing;
                rptShipping.DataBind();
                mdpupShipping.Hide();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnTravellerUpdate_Click1(object sender, EventArgs e)
        {
            try
            {
                var Trvid = Guid.Parse(hdntrvID.Value);
                var obj = new tblOrderTraveller();
                obj.Title = ddlTrTitle.SelectedValue;
                obj.FirstName = ddlTrFirstName.Text;
                obj.MiddleName = ddlTrMiddleName.Text;
                obj.LastName = txtTrLName.Text;
                obj.DOB = Convert.ToDateTime(txtDob.Text);
                obj.Country = Guid.Parse(ddlTrCountry.SelectedValue);
                obj.PassportNo = txtPassportNumber.Text;
                obj.Nationality = txtNationality.Text;
                obj.PassStartDate = Convert.ToDateTime(txtPassStartDate.Text);
                obj.LeadPassenger = chkLead.Checked;
                Objbooking.UpdateTraveller(Trvid, obj);

                GetOrdersForEdit(Convert.ToInt64(Request["id"]));
                mupTraveller.Hide();
                divlist.Visible = false;
                divDetail.Visible = true;
                ShowMessage(1, "Successfully update Traveller Information.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdTraveller_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var Trvid = Guid.Parse(e.CommandArgument.ToString());
                hdntrvID.Value = Trvid.ToString();
                var obj = Objbooking.GetTraveller(Trvid);
                if (obj != null)
                {
                    ddlTrCountry.SelectedValue = obj.Country.ToString();
                    ddlTrTitle.SelectedValue = obj.Title;
                    ddlTrFirstName.Text = obj.FirstName;
                    ddlTrMiddleName.Text = obj.MiddleName;
                    txtTrLName.Text = obj.LastName;
                    txtDob.Text = Convert.ToDateTime(obj.DOB).ToString("MMM/dd/yyyy");
                    txtPassportNumber.Text = obj.PassportNo;
                    txtNationality.Text = obj.Nationality;
                    txtPassStartDate.Text = Convert.ToDateTime(obj.PassStartDate).ToString("MMM/dd/yyyy");
                    chkLead.Checked = Convert.ToBoolean(obj.LeadPassenger);
                }
                mupTraveller.Show();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnOrderStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Objbooking.UpdateOrderStatus(Convert.ToInt32(ddlOrderStatus.SelectedValue), Convert.ToInt64(Request["id"]));
                mpeOrderStatus.Hide();
                if (ddlOrderStatus.SelectedValue == "3")
                    SendMail();
                GetOrdersForEdit(Convert.ToInt32(Request["id"]));
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void lnkEditStatus_Click(object sender, EventArgs e)
        {
            ddlOrderStatus.SelectedValue = Session["OrderStatusId"].ToString();
            mpeOrderStatus.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        protected void btnExtraChargeSave_Click(object sender, EventArgs e)
        {
            try
            {
                mpExtraCharge.Hide();
                if (!String.IsNullOrEmpty(txtExtraCharge.Text))
                {
                    Guid PassSaleId = string.IsNullOrEmpty(hdnpassaleidextracharge.Value) ? Guid.Empty : Guid.Parse(hdnpassaleidextracharge.Value);
                    decimal ExtraCharge = Convert.ToDecimal(txtExtraCharge.Text);
                    Objbooking.UpdateExtraCharge(ExtraCharge, PassSaleId);
                }
                txtExtraCharge.Text = string.Empty;
                GetOrdersForEdit(Convert.ToInt32(Request["id"]));
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void lnkEditExtraCharge_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
                var hdnpasssaleid = grdrow.FindControl("hdnpasssaleid") as HiddenField;
                hdnpassaleidextracharge.Value = hdnpasssaleid.Value;
                mpExtraCharge.Show();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdOrders_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            ViewState["sortExpression"] = sortExpression;

            GridViewSortDirection = GridViewSortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
            BindGrid();
        }

        private const string ASCENDING = "ASC";

        private const string DESCENDING = "DESC";

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        #region Paging
        public void BindPager()
        {
            try
            {
                int no = 1;
                var oPageList = new List<ClsPageCount>();
                int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
                int newpagecount = (TotalRecord - 1) / pageSize + (((TotalRecord - 1) % pageSize) > 0 ? 1 : 0);
                if (nxtpaging > 0)
                    pageIndex = no = nxtpaging;
                else
                    no = pageIndex > 10 ? (((pageIndex - 1) / 10) * 10) + 1 : no;
                if (no > 10)
                    lnkPrepaging.Visible = true;
                else
                    lnkPrepaging.Visible = false;
                lnkPrepaging.Attributes.Add("newpage", (no > 1 ? (no - 1) : 1).ToString());
                for (int i = no; i <= newpagecount; i++)
                {
                    if (i == newpagecount || newpagecount < 11)
                        lnknextpaging.Visible = false;
                    else
                        lnknextpaging.Visible = true;
                    var oPage = new ClsPageCount { PageCount = i.ToString() };
                    if ((i % 10) == 0)
                    {
                        oPageList.Add(oPage);
                        lnknextpaging.Attributes.Add("newpage", (i + 1).ToString());
                        i = newpagecount;
                    }
                    else
                        oPageList.Add(oPage);
                }
                if (newpagecount <= 1)
                {
                    lnknextpaging.Visible = lnkPrepaging.Visible = lnkPrevious.Visible = lnkNext.Visible = false;
                    litTotalPages.Text = "";
                }
                else
                {
                    if (pageIndex == 1)
                        lnkPrevious.Visible = false;
                    else
                        lnkPrevious.Visible = true;
                    if (pageIndex == newpagecount)
                        lnkNext.Visible = false;
                    else
                        lnkNext.Visible = true;
                    litTotalPages.Text = "Showing page " + CurrentPage + " of " + newpagecount;
                }
                DLPageCountItem.DataSource = oPageList;
                DLPageCountItem.DataBind();
                foreach (RepeaterItem item1 in DLPageCountItem.Items)
                {
                    var lblPage = (LinkButton)item1.FindControl("lnkPage");
                    if (lblPage.Text.Trim() == (pageIndex).ToString(CultureInfo.InvariantCulture))
                        lblPage.Attributes.Add("class", "activepaging");
                    else
                        lblPage.Attributes.Remove("class");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void lnkPrepaging_Click(object sender, EventArgs e)
        {
            CurrentPage = Convert.ToInt32(lnkPrepaging.Attributes["newpage"]);
            BindGrid();
            BindPager();
        }

        protected void lnknextpaging_Click(object sender, EventArgs e)
        {
            CurrentPage = nxtpaging = Convert.ToInt32(lnknextpaging.Attributes["newpage"]);
            BindGrid();
            BindPager();
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument);
            ViewState["PageIndex"] = pageIndex;
            BindGrid();
            BindPager();
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage - 1;
            BindGrid();
            BindPager();
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage + 1;
            BindGrid();
            BindPager();
        }

        private int CurrentPage
        {
            get
            {
                return ((ViewState["PageIndex"] == null || ViewState["PageIndex"].ToString() == "") ? 1 : int.Parse(ViewState["PageIndex"].ToString()));
            }
            set
            {
                ViewState["PageIndex"] = value;
            }
        }
        #endregion

        #region OrderNotes
        void BindOrderNotes(int ordId)
        {
            var oNotes = _master.GetOrderNotesList(ordId);
            if (oNotes != null)
            {
                grdOrderNotes.DataSource = oNotes.OrderBy(x => x.CreatedOn);
                grdOrderNotes.DataBind();
            }
        }

        protected void btnAddNotes_Click(object sender, EventArgs e)
        {
            AddEditOrderNotes();
            Response.Redirect("OrderDetails.aspx?id=" + Convert.ToInt32(Request["id"]));
        }

        protected void grdOrderNotes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    Response.Redirect("OrderDetails.aspx?id=" + Convert.ToInt32(Request["id"]) + "&notesid=" + id);
                }

                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteOrderNotes(id);
                    if (res)
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Record deleted successfully.')", true);
                    Response.Redirect("OrderDetails.aspx?id=" + Convert.ToInt32(Request["id"]));
                }
            }
            catch (Exception ex)
            {
                //ShowMessage(2, ex.Message);
            }
        }

        void EditOrderNotes(Guid id)
        {
            var oP = _master.GetOrderNotesById(id);
            if (oP != null)
                txtNotes.InnerHtml = oP.Notes;
        }

        public void AddEditOrderNotes()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNotes.InnerHtml))
                {
                    var id = (Request["notesid"] == "" || Request["notesid"] == null) ? new Guid() : Guid.Parse(Request["notesid"]);
                    _master.AddEditOrderNotes(new tblOrderNote
                        {
                            ID = id,
                            OrderID = Convert.ToInt32(Request["id"]),
                            UserName = AdminuserInfo.Username,
                            Notes = txtNotes.InnerHtml,
                            CreatedOn = DateTime.Now,
                            ModifiedOn = DateTime.Now
                        });
                    //ScriptManager.RegisterStartupScript(this, GetType(), "", !string.IsNullOrEmpty(Request["notesid"]) ? "alert('Notes updated successfully.')" : "alert('Notes added successfully.')", true);
                    txtNotes.InnerHtml = string.Empty;
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Please Enter notes.')", true);
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        #endregion

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddltypeDate_SelectIndexChange(object sender, EventArgs e)
        {
            BindGrid();
            BindPager();
        }

        #region Refund Admin,booking,shipping Fee
        public void BindPercentage()
        {
            var rPerc = _master.BindRefundPerc();
            ddlrefundsel.DataSource = rPerc;
            ddlrefundsel.DataTextField = "Description";
            ddlrefundsel.DataValueField = "PercentageRefund";
            ddlrefundsel.DataBind();
            ddlrefundsel.Items.Insert(0, new ListItem("-Select Refund-", "-1"));
            GetCurrencyCode();
        }

        protected void lnkRefShippingFee_Click(object sender, EventArgs e)
        {
            hdnrfndtype.Value = "shipping";
            lblhdr.Text = "Shipping Cost:";
            lblhdrRefundTxt.Text = lblShippingCost.Text;
            lblrefhdr.Text = "Shipping Cost Refund:";
            mprefund.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindPercentage();
        }

        protected void lnkRefBookingFee_Click(object sender, EventArgs e)
        {
            hdnrfndtype.Value = "booking";
            lblhdr.Text = "Booking Fee:";
            lblhdrRefundTxt.Text = lblBookindFee.Text;
            lblrefhdr.Text = "Booking Fee Refund:";
            mprefund.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindPercentage();
        }

        protected void lnkRefAdminFee_Click(object sender, EventArgs e)
        {
            hdnrfndtype.Value = "admin";
            lblhdr.Text = "Admin Fee:";
            lblhdrRefundTxt.Text = lblAdminFee.Text;
            lblrefhdr.Text = "Admin Fee Refund:";
            mprefund.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindPercentage();
        }

        protected void btnrefund_Click(object sender, EventArgs e)
        {
            try
            {
                var order = Convert.ToInt32(Request["id"]);
                tblOrderRefundAdmin obj = new tblOrderRefundAdmin();
                obj.ID = Guid.NewGuid();
                obj.OrderID = order;
                obj.CreatedBy = AdminuserInfo.UserID;
                obj.CreatedOn = DateTime.Now;
                obj.AdminFee = 0;
                obj.BookingFee = 0;
                obj.ShippingFee = 0;
                if (hdnrfndtype.Value == "admin")
                    obj.AdminFee = Convert.ToDecimal(hdnrfndamount.Value);
                else if (hdnrfndtype.Value == "booking")
                    obj.BookingFee = Convert.ToDecimal(hdnrfndamount.Value);
                else if (hdnrfndtype.Value == "shipping")
                    obj.ShippingFee = Convert.ToDecimal(hdnrfndamount.Value);

                _master.AddRefundData(obj);
                _siteId = Master.SiteID;
                GetOrdersForEdit((long)order);
                divlist.Visible = false;
                divDetail.Visible = true;
                RefundEMail();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void RefundEMail()
        {
            GetCurrencyCode();
            String RCurrency = Currency + " ";
            try
            {
                _siteId = Master.SiteID;
                DateTime RefundDate = DateTime.Now;
                string PassList = string.Empty;
                var smtpClient = new SmtpClient();
                var message = new MailMessage();

                var orderID = Convert.ToInt32(Request["id"]);
                string EmailAddress = _master.GetbillingEmail(orderID);
                string BccEmailAddress = _master.GetBccEmailBySiteid(_siteId);
                string Subject = "Order Refund Confirmation #" + orderID;
                var theme = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (theme != null && (theme.IsSTA ?? false))
                    htmfile = Server.MapPath("~/Orders/BlueRefundTemplate.htm");
                else
                    htmfile = Server.MapPath("~/Orders/RefundTemplate.htm");

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml;

                //***********************************//
                body = body.Replace("##OrderNo##", orderID.ToString());
                body = body.Replace("##SA##", RCurrency + lblShippingCost.Text);
                body = body.Replace("##BA##", RCurrency + lblBookindFee.Text);
                body = body.Replace("##AA##", RCurrency + lblAdminFee.Text);
                var data = _master.GetOrderAdminRefundById(orderID);
                if (data != null)
                {
                    body = body.Replace("##ShippingCost##", RCurrency + data.ShippingFee.ToString("F"));
                    body = body.Replace("##BookingFee##", RCurrency + data.BookingFee.ToString("F"));
                    body = body.Replace("##AdminFee##", RCurrency + data.AdminFee.ToString("F"));
                    RefundDate = data.CreatedOn;
                }
                else
                {
                    body = body.Replace("##ShippingCost##", RCurrency + "0.00");
                    body = body.Replace("##BookingFee##", RCurrency + "0.00");
                    body = body.Replace("##AdminFee##", RCurrency + "0.00");
                }
                var refundDetails = _master.GetRefundDetails(Convert.ToInt64(orderID));
                if (refundDetails != null)
                {
                    foreach (var ty in refundDetails.Select((x, i) => new { Value = x, Index = i }))
                    {
                        string TravelerTblRow = "<tr><td class='left-header' colspan='2'><hr />Passenger: ##PNO##<hr /></td></tr><tr><td class='first'>Product Name:</td><td class='secound'>##ProductName##</td></tr><tr><td class='first'>Name:</td><td class='secound'>##Name##</td></tr><tr><td class='first'>Gross Price:</td><td class='secound'>##GrossPrice##</td></tr><tr><td class='first'>Refund Price:</td><td class='secound'>##RefundPrice##</td></tr>";
                        if (RefundDate < ty.Value.RefundDate)
                            RefundDate = ty.Value.RefundDate;
                        TravelerTblRow = TravelerTblRow.Replace("##PNO##", (ty.Index + 1).ToString());
                        TravelerTblRow = TravelerTblRow.Replace("##ProductName##", ty.Value.Product);
                        TravelerTblRow = TravelerTblRow.Replace("##Name##", ty.Value.Traveller);
                        TravelerTblRow = TravelerTblRow.Replace("##GrossPrice##", RCurrency + ty.Value.Price.ToString("F"));
                        TravelerTblRow = TravelerTblRow.Replace("##RefundPrice##", RCurrency + ty.Value.Refund.ToString("F"));
                        PassList += TravelerTblRow;
                    }
                }
                body = body.Replace("##BindPassTravelerRefund##", PassList);
                body = body.Replace("##RefundDate##", RefundDate.ToString("dd/MMM/yyyy"));
                body = body.Replace("#Blanck#", "&nbsp;");
                //**********************************//

                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(_siteId);
                if (result != null && !string.IsNullOrEmpty(EmailAddress))
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(EmailAddress);
                    if (!string.IsNullOrEmpty(BccEmailAddress))
                        message.Bcc.Add(BccEmailAddress);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to send E-mail Refund confimation!");
            }
        }
        #endregion

        #region OrderAttachment
        protected void btnAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                var filesList = UploadFile();
                if (fupAttachment.HasFile)
                {
                    if (filesList != null && filesList.Count > 0)
                    {
                        _master.AddMultipleAttachments(filesList);
                    }
                }
                BindAttachments();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindAttachments()
        {
            try
            {

                grdOrderAttachments.DataSource = _master.GetOrderAttachments(Convert.ToInt32(Request["id"]));
                grdOrderAttachments.DataBind();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void grdOrderAttachments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] val = e.CommandArgument.ToString().Split(',');
                if (e.CommandName == "Remove")
                {
                    string path = Server.MapPath(val[1]);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    _master.RemoveAttachments(Convert.ToInt32(val[0]));
                    BindAttachments();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        List<tblOrderAttachment> UploadFile()
        {
            try
            {
                List<tblOrderAttachment> _ManageMultipleFiles = new List<tblOrderAttachment>();
                if (fupAttachment.HasFile)
                {
                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 10)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('You can not upload more than 10 files.')</script>", false);
                        return null;
                    }
                    else
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            string path = "";
                            Guid id = Guid.NewGuid();
                            HttpPostedFile uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            if (uploadfile.ContentLength > 2097152)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('Uploaded product file is larger up to 5Mb.')</script>", false);
                                return null;
                            }
                            else
                            {
                                string fExt = uploadfile.FileName.Substring(uploadfile.FileName.LastIndexOf("."));
                                path = "../Uploaded/OrderAttachments/" + id + fExt;
                                if (System.IO.File.Exists(Server.MapPath(path)))
                                    System.IO.File.Delete(Server.MapPath(path));
                                else
                                {
                                    uploadfile.SaveAs(Server.MapPath(path));
                                    string dircPath = Server.MapPath(path);
                                }
                                _ManageMultipleFiles.Add(new tblOrderAttachment
                                {
                                    OrderID = Convert.ToInt32(Request["id"]),
                                    AttachmentName = uploadfile.FileName.Substring(0, uploadfile.FileName.LastIndexOf(".")),
                                    AttachmentPath = path
                                });
                            }
                        }
                    }
                }

                if (_ManageMultipleFiles != null && _ManageMultipleFiles.Count > 0)
                    return _ManageMultipleFiles;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetFileExtension(string filepath)
        {
            try
            {
                if (string.IsNullOrEmpty(filepath))
                    return "unknown.png";

                string p = filepath.Substring(filepath.LastIndexOf('.'));
                return p.Replace(".", "") + ".png";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        protected void grdRefund_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "additionalRefund")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    Response.Redirect("OrderRefund.aspx?prdId=" + id);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #region Edit Booking Fee

        protected void lnkeditbooking_Click(object sender, EventArgs e)
        {
            txtbookingEdit.Text = lblBookindFee.Text;
            mpBookingEdit.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        protected void btnEditBookingUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var OrderID = Convert.ToInt32(Request["id"]);
                if (!string.IsNullOrEmpty(txtbookingEdit.Text))
                    _master.UpdateBookingFeeByOrderId(OrderID, Convert.ToDecimal(txtbookingEdit.Text));
                GetOrdersForEdit(OrderID);
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #endregion

        protected void BtnRefundMailSend_Click(object sender, EventArgs e)
        {
            try
            {
                _siteId = Master.SiteID;
                GetCurrencyCode();
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    var orderID = Convert.ToInt32(Request["id"]);
                    string Subject = "Refund Confirmation #" + orderID;
                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/OrderRefundMailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(_siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(_siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml;

                    var prdPass = _master.GetPassSale(orderID); //calculate price
                    var Datalist = _master.GetRefundDetailsByOrderId(orderID);
                    var refundDetails = Datalist.Select(x => new
                    {
                        ProductID = x.ProductID,
                        ProductName = x.ProductName,
                        ProductValidUpToName = x.ProductValidUpToName,
                        TravellerType = x.TravellerType,
                        Class = x.Class,
                        TravellerName = x.TravellerName,
                        User = x.User,
                        Price = x.Price,
                        Refund = x.Refund,
                        RefundDate = (DateTime)x.RefundDate,
                        PassSaleID = x.PassSaleID,
                        PassStartDate = (DateTime)x.PassStartDate,
                        TicketProtection = x.TicketProtection,
                        CancellationFee = x.CancellationFee,
                        SiteId = x.SiteId,
                        SiteName = x.SiteName,
                        SiteUrl = x.SiteUrl,
                        OrderId = x.OrderId,
                        IsAgentSite = x.IsAgentSite,
                        StockNo = prdPass.FirstOrDefault(v => v.ID == x.PassSaleID).StockNumber,
                    }).OrderBy(ty => ty.StockNo).ToList();
                    if (refundDetails != null && refundDetails.Count > 0)
                    {
                        string UserName = "", strProductDesc = "";
                        GetReceiptLoga();
                        var billingDetails = Objbooking.GetBillingShippingAddress(orderID);
                        if (billingDetails != null)
                            UserName = (!string.IsNullOrEmpty(billingDetails.TitleShpg) ? billingDetails.TitleShpg : billingDetails.Title) + " " + (!string.IsNullOrEmpty(billingDetails.FirstNameShpg) ? billingDetails.FirstNameShpg : billingDetails.FirstName) + " " + (!string.IsNullOrEmpty(billingDetails.LastNameShpg) ? billingDetails.LastNameShpg : billingDetails.LastName);

                        bool isAgentSite = st.IsAgent.Value;
                        foreach (var item in refundDetails)
                        {
                            var ProductDesc = Objbooking.getPassDetailsForEmail(item.PassSaleID, null, "", "");
                            string nettPrice = Currency + " " + (_master.GetEurailAndNonEurailPrice(item.PassSaleID)).ToString("F");
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                            strProductDesc += ProductDesc.Replace("##Price##", Currency + " -" + item.Refund.ToString("F2")).Replace("##Name##", item.TravellerName).Replace("##NetAmount##", "");
                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + (item.PassStartDate != null ? item.PassStartDate.ToString("dd/MMM/yyyy") : string.Empty) + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";
                            strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");

                            string ElectronicNote = Objbooking.GetElectronicNote(item.PassSaleID);
                            if (ElectronicNote.Length > 0)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;'>" + ElectronicNote + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>";

                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" +
                                (item.TicketProtection > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + Currency + " 0.00</strong></td></tr>";
                            //(item.TicketProtection > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>" + Currency + " " + Convert.ToString(item.TicketProtection) + "</strong></td></tr>";
                            strProductDesc += "</table></td></tr></table>";
                        }
                        body = body.Replace("##headerstyle##", SiteHeaderColor);
                        body = body.Replace("##sitelogo##", logo);
                        body = body.Replace("##SiteUrl##", st.SiteURL);
                        body = body.Replace("##OrderNumber##", orderID.ToString());
                        body = body.Replace("##UserName##", UserName);
                        body = body.Replace("##RefundDate##", refundDetails.FirstOrDefault().RefundDate.ToString("dd/MM/yyyy"));
                        body = body.Replace("##OrderDetails##", strProductDesc);
                        body = body.Replace("##DeliveryAddress##", "");

                        decimal Refund = refundDetails.Sum(x => x.Refund);
                        decimal CancellationFee = refundDetails.Sum(x => x.CancellationFee);
                        decimal totalRefund = Refund - CancellationFee;
                        body = body.Replace("##RefundPrice##", Currency + " -" + Refund.ToString("F2"));
                        body = body.Replace("##CancellationFee##", Currency + " " + CancellationFee.ToString("F2"));
                        body = body.Replace("##TotalRefund##",Currency + " -" + totalRefund.ToString("F2"));

                        /*Get smtp details*/
                        var billing = _master.GetBillingInfo(orderID);
                        if (billing.Any())
                        {
                            var firstOrDefault = billing.FirstOrDefault();
                            if (firstOrDefault != null)
                            {
                                billingEmailAddress = firstOrDefault.EmailAddress;
                                shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                            }
                        }
                        var result = _masterPage.GetEmailSettingDetail(_siteId);
                        if (result != null)
                        {
                            var fromAddres = new MailAddress(result.Email, result.Email);
                            smtpClient.Host = result.SmtpHost;
                            smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                            smtpClient.UseDefaultCredentials = true;
                            smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                            smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                            message.From = fromAddres;
                            message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                            message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                            message.Subject = Subject;
                            message.IsBodyHtml = true;
                            message.Body = body;
                            smtpClient.Send(message);
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                        }
                    }
                }
                GetOrdersForEdit(Convert.ToInt64(Request["id"]));
                mupTraveller.Hide();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

    }
}

public class cls_PageCount
{
    public string PageCount { get; set; }
}

