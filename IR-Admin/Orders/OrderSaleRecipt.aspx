﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderSaleRecipt.aspx.cs"
    Inherits="IR_Admin.Orders.OrderSaleRecipt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function printItn() {
            var printContent = document.getElementById('divPrintGrid');
            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var WinPrint = window.open(windowUrl, windowName, 'left=300,top=300,right=500,bottom=500,width=1000,height=500');
            WinPrint.document.write(printContent.innerHTML);
            WinPrint.document.write('<' + '/body' + '><' + '/html' + '>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
    <style type="text/css">
        table tr td {
            font-size: 12px;
            font-family: sans-serif;
        }

        .clsLft {
            width: 60%;
        }

        .clsRt {
            width: 40%;
        }

        .brd {
            border: 1px solid #999;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divPrintGrid">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                <tr bgcolor="#ffffff">
                    <td align="center" valign="top" bgcolor="#ffffff">
                        <table width="650" cellspacing="0" cellpadding="0" class="deviceWidth" bgcolor="#ffffff"
                            align="center">
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        bgcolor="#ffffff" align="center">
                                        <tr>
                                            <td bgcolor="#ffffff" height="5"></td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff">
                                                <table width="50%" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                                    align="left">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <a href="#" style="border: none;">
                                                                <img src='<%=SiteLogo%>' alt="" style="border: none;" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="50%" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                                    align="right">
                                                    <tr>
                                                        <td style="padding: 15px 14px 0px 0px;" align="right" valign="top"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff" height="5"></td>
                                        </tr>
                                    </table>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        align="center">
                                        <tr>
                                            <td style="background: <%=SiteHeaderColor%>;-webkit-print-color-adjust: exact; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif; padding-left: 20px;"
                                                align="left" valign="middle" height="42" width="320">Thanks for your order!
                                            </td>
                                            <td bgcolor="#ffffff" height="5" width="6"></td>
                                            <td style="background: <%=SiteHeaderColor%>;-webkit-print-color-adjust: exact; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif; padding-left: 20px;"
                                                align="left" valign="middle" height="42" width="320"><%=HeaderText %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff" height="5" width="6"></td>
                                            <td bgcolor="#ffffff" height="5" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#eeeeee" valign="top" style="padding: 7px;" width="320">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="3" align="left" class="deviceWidth ">
                                                    <tr>
                                                        <td width="40%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong>Order ID: </strong>
                                                        </td>
                                                        <td width="58%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"><%= OrderNumber %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong>Order placed on: </strong>
                                                        </td>
                                                        <td align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"><%= OrderDate %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong>Order placed by: </strong>
                                                        </td>
                                                        <td align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"><%=UserName %></td>
                                                    </tr>
                                                    <%= BookingRefNO %>
                                                    <tr>
                                                        <td align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong>Billing address: </strong>
                                                        </td>
                                                        <td align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"><%= BillingAddress %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center" valign="top" style="font-size: 13px; color: #fc0000; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong>This is not your travel document </strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td bgcolor="#ffffff" height="5" width="6"></td>
                                            <td bgcolor="#eeeeee" valign="top" style="padding: 7px;" width="320">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="3" align="left" class="deviceWidth ">
                                                    <tr>
                                                        <td colspan="2" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong><%=HeaderDeliveryText %> </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"
                                                            height="10"><%=DeliveryAddress %>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        align="center">
                                        <tr>
                                            <td class="fnt" style="background: <%=SiteHeaderColor%>;-webkit-print-color-adjust: exact; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif; padding-left: 20px;"
                                                align="left"
                                                valign="middle" height="42">Tickets ordered
                                            </td>
                                        </tr>
                                    </table>
                                    <%=OrderDetailsText %>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        align="center" style="##passshipping##">
                                        <tr>
                                            <td class="fnt" style="background: <%=SiteHeaderColor%>;-webkit-print-color-adjust: exact; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif; padding-left: 20px;"
                                                align="left"
                                                valign="middle" height="42">Delivery
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        align="center" bgcolor="#eeeeee">
                                        <tr>
                                            <td bgcolor="#ffffff" height="5"></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px;" align="center" valign="top" class="pad">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="deviceWidth ">
                                                    <tr>
                                                        <td width="25%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong>Your delivery option: </strong>
                                                        </td>
                                                        <td width="45%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"><%=ShippingMethod %>
                                                        </td>
                                                        <td width="30%" align="right" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">
                                                            <strong><%=Shipping %></strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff" height="5"></td>
                                        </tr>
                                    </table>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        align="center">
                                        <tr>
                                            <td class="fnt" style="background: <%=SiteHeaderColor%>;-webkit-print-color-adjust: exact; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif; padding-left: 20px;"
                                                align="left"
                                                valign="middle" height="42">Order total
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth"
                                        align="center" bgcolor="#eeeeee">
                                        <tr>
                                            <td bgcolor="#ffffff" height="5"></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px;" align="center" valign="top" class="pad">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="deviceWidth ">
                                                    <tr>
                                                        <td width="20%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;">Ticket total:
                                                        </td>
                                                        <td width="45%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"></td>
                                                        <td width="35%" align="right" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;"><%=ItemsText %>
                                                        </td>
                                                    </tr>
                                                    <%=PassProtectionText %> <%=AdminFeeText %>
                                                    <tr>
                                                        <td width="20%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;">Booking Fee:
                                                        </td>
                                                        <td width="45%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;"></td>
                                                        <td width="35%" align="right" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;"><%=BookingFeeText %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;">Delivery:
                                                        </td>
                                                        <td width="45%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;"></td>
                                                        <td width="35%" align="right" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;"><%=DeliveryFeeText %>
                                                        </td>
                                                    </tr>
                                                    <%=DiscountText %>
                                                    <tr>
                                                        <td width="20%" align="left" valign="top" style="font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;">
                                                            <strong>Order total: </strong>
                                                        </td>
                                                        <td width="45%" align="left" valign="top" style="font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;"></td>
                                                        <td width="35%" align="right" valign="top" style="font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;">
                                                            <strong><%=TotalText %> </strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff" height="5"></td>
                                        </tr>
                                    </table>
                                    <%=printAthomeText %>
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="deviceWidth">
                                        <tr>
                                            <td style="padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;"
                                                colspan="2">For general booking conditions of the site please <a target="_blank" href='<%=BookingConditionText %>'>click here</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;"
                                                colspan="2">Please note: This e-mail was sent from a notification-only address that can't accept
                                        incoming e-mail. Please do not reply to this message.<br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
         <div style="width: 50%; text-align: center; margin: auto; margin-bottom: 50px;">
            <a href="#" onclick="printItn();" class="btnlink">Print Receipt</a>
        </div>
    </form>
</body>
</html>
