﻿#region Using
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.Configuration;
using System.IO;
#endregion

namespace IR_Admin.Orders
{
    public partial class P2POrderDetails : Page
    {
        public int nxtpaging = 0;
        public int TotalRecord = 1;
        public bool IsTrainTickts = false;
        private const int pageSize = 50;
        public bool IsBENE = false;
        public string Currency = "$";
        public Guid CurrencyID = Guid.Empty;
        public decimal BookingFee = 0;
        public decimal Commission = 0;
        public decimal NetPrice = 0;
        public decimal GrossPrice = 0;
        public decimal TicketProtection = 0;
        public decimal GrandTotal = 0;
        public decimal Discount = 0;
        public decimal AmexPrice = 0;
        private string htmfile = string.Empty;
        public string billingEmailAddress;
        public string shippingEmailAddress;
        public string FileURL = "";
        public string PaymentTakenDate = "";
        public Guid OrderSitelId = Guid.Empty;
        private readonly Masters _masterPage = new Masters();
        readonly private ManageOrder _masterOrder = new ManageOrder();
        private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();
        private readonly ManageBooking Objbooking = new ManageBooking();

        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public static string unavailableDates1 = "";
        public int OrderId = 0;
        public bool IsVisibleAttachmentDeleteBtn = false;
        private string HtmlFileSitelog = string.Empty;
        public string SiteHeaderColor = string.Empty;
        public bool isEvolviBooking = false;
        public bool isNTVBooking = false;
        public bool isBene = false;
        public bool isTI = false;
        public string EvOtherCharges = "0";
        public string Name = "";
        public string logo = "";
        public string EvolviTandC = "";

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
            BindGrid();
            BindPager();
            txtOrderId.Text = string.Empty;
            txtRef.Text = string.Empty;
            txtName.Text = string.Empty;
            ddlOrderSt.SelectedValue = "0";
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            DivMsg.Visible = false;
            if ((Request["id"] != null) && (Request["id"] != ""))
            {
                #region order not exists and not valid
                var haveint = Int32.TryParse(Request["id"], out OrderId);
                if (haveint)
                {
                    if (!_db.tblOrders.Any(x => x.OrderID == OrderId))
                    {
                        ShowMessage(2, "Record not found order no. " + OrderId);
                        return;
                    }
                }
                else
                {
                    ShowMessage(2, "Record not found and order " + Request["id"] + " not valid.");
                    return;
                }
                #endregion
            }
            var result = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
            if (result != null)
                _siteId = (Guid)result.SiteID;
            else
                _siteId = Master.SiteID;

            txtPhone.Attributes.Add("onkeypress", "return keycheck()");
            var data = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                OrderAttachments.Visible = data.IsOrderAttachmentAllow;
                IsVisibleAttachmentDeleteBtn = data.RoleName.ToLower().Trim() == "admin";
                Guid Bookmyrst = Guid.Parse("a5f8e257-d4e1-48e9-bfa4-59a582ebe56f");
                if (data.IsEditableBookingFee && _siteId == Bookmyrst)
                    lnkeditbooking.Visible = true;
                else
                    lnkeditbooking.Visible = false;
            }

            if (!IsPostBack)
            {
                Session["syswide"] = divsyswide.Visible = _masterPage.GetSystemReportByUserId(AdminuserInfo.UserID);//= chkSysWide.Checked 
                divlist.Visible = true;
                divDetail.Visible = false;
                GetCurrencyCode();
                BindCountry();
                BindGrid();
                BindPager();
                BindEditData();
                if ((Request["notesid"] != null) && (Request["notesid"] != ""))
                {
                    EditOrderNotes(Guid.Parse(Request["notesid"]));
                    btnAddNotes.Text = "Update";
                }
            }
            //RefundEMail();
        }

        public void BindEditData()
        {
            if ((Request["id"] != null) && (Request["id"] != ""))
            {
                Guid userid = AdminuserInfo.UserID;
                if (_db.tblAdminUserLookupSites.Any(x => x.AdminUserID == userid && x.SiteID == _siteId))
                {

                    DivMsg.Visible = false;
                    DivOrderDetail.Visible = true;
                    ISAffiliateUser();
                    BindOrderNotes(Convert.ToInt32(Request["id"]));
                    divlist.Visible = false;
                    divDetail.Visible = true;
                    GetOrdersForEdit(Convert.ToInt32(Request["id"]));
                    BindAttachments();
                }
                else
                {
                    DivMsg.Visible = true;
                    DivOrderDetail.Visible = false;
                    var site = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    lblmsg.Text = "Agent is not able to check order (" + OrderId + ") details, because " + site.DisplayName + " site is not allowed for this agent.";
                }
            }
        }

        public void GetCurrencyCode()
        {
            if (Request["id"] == null)
                return;

            FrontEndManagePass oManageClass = new FrontEndManagePass();
            Int32 orderno = Convert.ToInt32(Request["id"]);
            var odr = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderno);
            if (odr != null)
            {
                OrderSitelId = odr.SiteID.Value;
                var Data = _db.tblSites.FirstOrDefault(x => x.ID == OrderSitelId);
                if (Data != null)
                {
                    CurrencyID = Data.DefaultCurrencyID.Value;
                    Currency = oManageClass.GetCurrency(CurrencyID);
                }
            }
        }

        public void ISAffiliateUser()
        {
            try
            {
                shownetparice.Attributes.Add("style", (_masterOrder.AffiliateUser(Convert.ToInt64(Request["id"]))) ? "display:none" : "font-size:12px");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void BindCountry()
        {
            var data = new ManageTrainDetails().GetCountryDetail();
            ddlbcountry.DataSource = ddlCountry.DataSource = data;
            ddlbcountry.DataValueField = ddlCountry.DataValueField = "CountryName";
            ddlbcountry.DataTextField = ddlCountry.DataTextField = "CountryName";
            ddlbcountry.DataBind();
            ddlbcountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            ddlTrCountry.DataSource = data;
            ddlTrCountry.DataValueField = "CountryID";
            ddlTrCountry.DataTextField = "CountryName";
            ddlTrCountry.DataBind();
            ddlTrCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            ddlOrderStatus.DataSource = new ManageOrder().GetStatusList().OrderBy(a => a.Name).ToList();
            ddlOrderStatus.DataValueField = "ID";
            ddlOrderStatus.DataTextField = "Name";
            ddlOrderStatus.DataBind();
            ddlOrderStatus.Items.Insert(0, new ListItem("--Select Order--", "0"));

            ddlOrderSt.DataSource = new ManageOrder().GetStatusList().OrderBy(a => a.Name).ToList();
            ddlOrderSt.DataValueField = "ID";
            ddlOrderSt.DataTextField = "Name";
            ddlOrderSt.DataBind();
            ddlOrderSt.Items.Insert(0, new ListItem("--Select Order--", "0"));

            long orderid = Convert.ToInt64(Request["id"]);
            string CountryName = "";
            bool nodefaultcountry = false;
            var lstShip = new List<tblShipping>();
            var billing = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID.Value == orderid);
            CountryName = (billing == null) ? null : billing.Country;
            if (string.IsNullOrEmpty(CountryName))
            {
                Guid sitecountry = _db.tblSites.FirstOrDefault(x => x.ID == _siteId).DefaultCountryID.Value;
                var countydata = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == sitecountry);
                if (countydata != null)
                    CountryName = countydata.CountryName;
                else
                    nodefaultcountry = true;
            }
            if (nodefaultcountry)
                lstShip = _db.tblShippingOptionCountries.Where(x => x.tblShipping.IsActive && x.tblShipping.SiteID == _siteId).Select(x => x.tblShipping).Distinct().ToList();
            else
                lstShip = _db.tblShippingOptionCountries.Where(x => x.tblCountriesMst.CountryName == CountryName && x.tblShipping.IsActive && x.tblShipping.SiteID == _siteId).Select(x => x.tblShipping).Distinct().ToList();
            if (lstShip.Any())
            {
                ddlShippindMethod.DataSource = ddlEditShippindMethod.DataSource = (lstShip != null && lstShip.Count > 0) ? lstShip.OrderBy(x => x.ShippingName).ToList() : lstShip;
                ddlShippindMethod.DataValueField = "ID";
                ddlShippindMethod.DataTextField = "ShippingName";
                ddlShippindMethod.DataBind();
                ddlShippindMethod.Items.Insert(0, new ListItem("--Select Shipping--", "0"));

                ddlEditShippindMethod.DataTextField = ddlEditShippindMethod.DataValueField = "ShippingName";
                ddlEditShippindMethod.DataBind();
                ddlEditShippindMethod.Items.Insert(0, new ListItem("--Select Shipping--", "0"));
            }
        }

        void BindGrid()
        {
            string SiteId = Master.SiteID.ToString();
            if (chkSysWide.Checked)
                SiteId = "2";
            DateTime d1 = DateTime.Now.AddMonths(-6).Date;
            DateTime d2 = DateTime.Now.Date;
            if (string.IsNullOrEmpty(txtStartDate.Text) && string.IsNullOrEmpty(txtLastDate.Text))
            {
                txtStartDate.Text = d1.ToString("dd/MM/yyyy");
                txtLastDate.Text = d2.ToString("dd/MM/yyyy");
            }
            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
            {
                d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", null);
                d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", null);
            }
            int Skip = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());

            String Status = ddlOrderSt.SelectedValue == "0" ? "" : ddlOrderSt.SelectedValue;
            var list = _masterOrder.GetP2POrderList(txtOrderId.Text, txtRef.Text, txtName.Text, Status, SiteId, d1.Date, d2.Date, Convert.ToInt32(ddltypeDate.SelectedValue), Skip, pageSize, AdminuserInfo.UserID, txtPNRNumber.Text).ToList();
            if (ddlOrderSt.SelectedValue != "0")
                list = list.Where(x => x.STATUS == Convert.ToInt16(ddlOrderSt.SelectedValue)).ToList();
            var date = list.FirstOrDefault();
            if (date != null)
            {
                TotalRecord = date.TOTALRECORD.Value;
            }
            if (ViewState["sortExpression"] != null)
            {
                if (ViewState["sortExpression"].ToString() == "DeptDate")
                {
                    if (GridViewSortDirection == SortDirection.Descending)
                        list = list.OrderBy(x => x.DEPTDATE).ToList();
                    else
                        list = list.OrderByDescending(x => x.DEPTDATE).ToList();
                }
                else
                {
                    if (GridViewSortDirection == SortDirection.Descending)
                        list = list.OrderBy(x => x.BOOKINGDATE).ToList();
                    else
                        list = list.OrderByDescending(x => x.BOOKINGDATE).ToList();
                }
            }
            grdOrders.DataSource = list;
            grdOrders.DataBind();
        }

        public void GetOrdersForEdit(int ordId)
        {
            lnkPrintReceipt.HRef = "OrderSaleRecipt.aspx?ID=" + ordId;
            bool Refbooking = _masterOrder.GetAnyOrderRefunds(ordId, 1);//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
            lnkRefBookingFee.Visible = !Refbooking;
            bool RefShippingFee = _masterOrder.GetAnyOrderRefunds(ordId, 2);//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
            lnkRefShippingFee.Visible = !RefShippingFee;
            bool RefAdminFee = _masterOrder.GetAnyOrderRefunds(ordId, 3);//case 1: BookingFee, 2:ShippingFee, 3:AdminFee
            lnkRefAdminFee.Visible = !RefAdminFee;
            IsTrainTickts = _masterOrder.GetTrainTicketsByOrderSiteId(ordId);
            var lst = Objbooking.GetP2PSaleDetail(ordId).OrderBy(t => t.ShortOrder).ToList(); ;
            if (lst != null && lst.Count() > 0 && lst.FirstOrDefault().TrvType == "P2P")
            {
                var data = lst.FirstOrDefault();
                if (Session["syswide"] != null && Convert.ToBoolean(Session["syswide"].ToString()))
                    Session["syswidesite"] = data.SiteID.Value;
                if (data.BookingFee < 1)
                    lnkRefBookingFee.Visible = false;
                if (data.AdminFee < 1)
                    lnkRefAdminFee.Visible = false;
                if (data.ShippingAmount < 1)
                    lnkRefShippingFee.Visible = false;

                decimal NetPriceValue = 0;
                Currency = _masterOrder.GetCurrcyBySiteId(data.SiteID.Value);
                BookingFee = data.BookingFee;
                Commission = lst.Sum(t => t.CommissionFee);
                NetPriceValue = (lst.Sum(t => t.NetPrice ?? 0) - Commission);
                AmexPrice = (decimal)data.AmexPrice;
                NetPrice = NetPriceValue;
                GrossPrice = lst.Sum(t => t.NetPrice ?? 0);
                TicketProtection = lst.Sum(t => t.TicketProtection ?? 0);
                Discount = Convert.ToDecimal(_masterOrder.GetOrderDiscountByOrderId(ordId));
                decimal Extracharge = Objbooking.GetExtraChargeByOrderId(ordId);
                GrandTotal = (GrossPrice + TicketProtection + BookingFee + data.ShippingAmount + AmexPrice) - (Discount + Extracharge + data.AdminFee);
                lblNote.Text = (string.IsNullOrEmpty(data.Note) ? data.notes : data.Note);
                Session["OrderStatusId"] = data.Status;
                hdnStatus.Value = data.Status.ToString();
                if ((data.Status == 3 || data.Status == 7 || data.Status == 9 || data.Status == 19))
                {
                    spnPayment.Attributes.Add("class", "clsGreen");
                    PaymentTakenDate = Convert.ToDateTime(data.PaymentDate).ToString("MMM dd,yyyy HH:mm");
                    btnTakePayment.Visible = false;
                    trpaymentdt.Visible = true;
                    tdPayment.Attributes.Add("class", "clsbckGreen");
                    lblPaymentTaken2.Text = lblPaymentTaken.Text = "Yes";
                }
                var ordshipMethod = _db.tblOrders.FirstOrDefault(x => x.OrderID == ordId);
                if (ordshipMethod != null)
                {
                    var shipID = _db.tblShippings.FirstOrDefault(x => x.ShippingName == ordshipMethod.ShippingMethod && x.SiteID == _siteId);
                    if (shipID != null)
                        Session["ShippingMethodId"] = shipID.ID;
                }
                lblorder.Text = data.OrderID.ToString();
                lblStatusName.Text = data.StatusName;
                lblCreatedOn.Text = data.CreatedOn.ToString("dd MMM yyyy HH':'mm 'GMT'");
                lblSiteName.Text = data.SiteName;
                lblIpAddress.Text = data.IpAddress;
                lblOfficeName.Text = data.OfficeName;
                lblShippingAmount.Text = data.ShippingAmount.ToString("F");
                lblAmexPrice.Text = data.AmexPrice.ToString("F");
                lblBookingFee.Text = data.BookingFee.ToString("F");
                lblCommission.Text = data.CommissionFee.ToString("F");
                lblTicketProtection.Text = data.TicketProtection.HasValue ? data.TicketProtection.Value.ToString("F") : "0.00";
                lblNetPrice.Text = lblGrossPrice.Text = GrossPrice.ToString("F");
                lblGrandTotal.Text = GrandTotal.ToString("F");
                lblDiscount.Text = Discount.ToString("F");
                lblAdminFee.Text = data.AdminFee.ToString("F");
                var data1 = _masterOrder.GetOrderAdminRefundById(Convert.ToInt32(ordId));
                if (data1 != null)
                {
                    lblshippingref.Text = data1.ShippingFee.ToString("F");
                    lblbookingref.Text = data1.BookingFee.ToString("F");
                    lbladminref.Text = data1.AdminFee.ToString("F");
                }
                else
                {
                    lblshippingref.Text = "0.00";
                    lblbookingref.Text = "0.00";
                    lbladminref.Text = "0.00";
                }
                lblship.Text = lblShippingAmount.Text;
                lblbook.Text = lblBookingFee.Text;
                lbladmin.Text = lblAdminFee.Text;

                lblAgentRef2.Text = data.AgentReferenceNo;
                var agentDetails = _masterOrder.GetAgentDetails(Convert.ToInt64(ordId)).FirstOrDefault();
                if (agentDetails != null)
                    tblNonAgent.Visible = false;
                else
                    tblNonAgent.Visible = true;
                FillRefund(ordId);
                var datalist = lst.Where(t => t.ID != Guid.Empty).Select(t => new
                {
                    Id = t.ID,
                    TrainNo = t.TrainNo,
                    From = t.From,
                    To = t.To,
                    DateTimeDepature = t.DateTimeDepature,
                    DateTimeArrival = t.DateTimeArrival,
                    Passenger = t.Passenger,
                    Symbol = t.Symbol,
                    NetPrice = t.NetPrice,
                    DepartureTime = t.DepartureTime,
                    ArrivalTime = t.ArrivalTime,
                    Class = t.Class,
                    FareName = t.FareName,
                    ReservationCode = t.ReservationCode,
                    PinNumber = t.PinNumber,
                    PassP2PSaleID = t.PassP2PSaleID,
                    DeliveryOption = t.DeliveryOption,
                    IsNotRefundedFull = !_masterOrder.IsHundradPercentRefund(t.ID, Convert.ToInt32(Request["id"])),
                    PdfURL = !string.IsNullOrEmpty(t.ReservationCode) ? _masterOrder.GetURLByOrderId(t.ID) : string.Empty,
                    //PdfURL = !string.IsNullOrEmpty(t.ReservationCode) ? (t.ReservationCode.Length == 7 ? _masterOrder.GetBENEPdfURLByOrderId(Convert.ToInt32(Request["id"])) : t.PdfURL) : string.Empty,
                    IsBENE = !string.IsNullOrEmpty(t.ReservationCode) ? (t.ReservationCode.Length == 7 ? true : false) : false,
                    Terms = Server.HtmlDecode(t.terms),
                    Fipnumber = t.Fipnumber,
                    FIPClass = t.FIPClass,
                    Via = t.Via,
                    OldReservationCode = Objbooking.GetOldReservationCode(t.ID),
                    isOldReservationCode = !string.IsNullOrEmpty(Objbooking.GetOldReservationCode(t.ID)) ? true : false
                }).ToList();
                if (datalist.Count == 1 && datalist.Any(t => t.Id == Guid.Empty))
                    datalist = null;
                rptJourney.DataSource = datalist;
                rptJourney.DataBind();

                grdTraveller.DataSource = lst;
                grdTraveller.DataBind();

                var billing = _masterOrder.GetBillingInfo(ordId);
                if (billing.Any())
                {
                    rptBilling.DataSource = rptShipping.DataSource = billing;
                    rptBilling.DataBind();
                    rptShipping.DataBind();
                    btnSendEmail.Visible = true;
                    lnkAddShipping.Visible = false;
                    lnkAddBilling.Visible = false;
                }

                var DeliveryInfo = _masterOrder.GetP2PdelivaryInfo(ordId).Take(1);
                rptDelivery.DataSource = DeliveryInfo;
                rptDelivery.DataBind();

                var ccInfo = _masterOrder.GetOrderCreditCardInfo(Convert.ToInt64(ordId)).FirstOrDefault();
                if (ccInfo != null)
                {
                    lblCardholderName.Text = ccInfo.CardholderName;
                    lblCardNumber.Text = string.IsNullOrEmpty(ccInfo.CardNumber) ? "" : "XXXX-XXXX-XXXX-XXXX";
                    lblPaymentId.Text = ccInfo.PaymentId;
                    lblExpDate.Text = string.IsNullOrEmpty(ccInfo.ExpDate) ? "" : "XX/XX";
                    lblBrand.Text = ccInfo.Brand;
                }
                gvagentupdatestatus.DataSource = Objbooking.GetListUpdateOrderStatusLog(Convert.ToInt64(ordId));
                gvagentupdatestatus.DataBind();

                if (data.Status != 3 && data.Status != 7 && data.Status != 9 && data.Status != 19)
                    lnkPrintReceipt.Visible = false;
            }
        }

        public void FillRefund(int ordId)
        {
            var lstRefund = Objbooking.GetP2PRefund(ordId).ToList();
            rptRefunds.DataSource = lstRefund;
            rptRefunds.DataBind();
            if (lstRefund != null && lstRefund.Count > 0)
                BtnRefundPrint.HRef = ConfigurationManager.AppSettings["HttpHost"] + "Orders/RefundReceipt.aspx?id=" + ordId;
            else
            {
                BtnRefundPrint.Visible = false;
                BtnRefundMailSend.Visible = false;
            }
        }

        protected void lnkEditStatus_Click(object sender, EventArgs e)
        {
            ddlOrderStatus.SelectedValue = Session["OrderStatusId"].ToString();
            mpeOrderStatus.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindEditData();
        }

        protected void rptBilling_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var objBillAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(Request["id"]));
            if (objBillAddress != null)
            {
                ddlbtitle.SelectedValue = objBillAddress.Title;
                txtbfname.Text = objBillAddress.FirstName;
                txtblname.Text = objBillAddress.LastName;
                txtbemail.Text = objBillAddress.EmailAddress;
                txtbphone.Text = objBillAddress.Phone;
                txtbadd.Text = objBillAddress.Address1;
                txtbadd2.Text = objBillAddress.Address2;
                txtbcity.Text = objBillAddress.City;
                txtbState.Text = objBillAddress.State;
                txtbzip.Text = objBillAddress.Postcode;
                ddlbcountry.SelectedValue = objBillAddress.Country;
                chkVisibleEmailAddress.Checked = objBillAddress.IsVisibleEmailAddress;
            }
            mdpopBilling.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindEditData();
        }

        protected void rptShipping_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            tblOrderBillingAddress objBillAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(Request["id"]));
            if (objBillAddress != null)
            {
                txtAdd.Text = objBillAddress.Address1Shpg;
                txtAdd2.Text = objBillAddress.Address2Shpg;
                txtCity.Text = objBillAddress.CityShpg;
                txtEmail.Text = objBillAddress.EmailAddressShpg;
                txtFirst.Text = objBillAddress.FirstNameShpg;
                txtLast.Text = objBillAddress.LastNameShpg;
                txtState.Text = objBillAddress.StateShpg;
                txtZip.Text = objBillAddress.PostcodeShpg;
                ddlMr.SelectedValue = objBillAddress.TitleShpg != null ? objBillAddress.TitleShpg.Trim() : null;
                ddlCountry.SelectedValue = objBillAddress.CountryShpg != null ? objBillAddress.CountryShpg.Trim() : null;
                txtPhone.Text = objBillAddress.PhoneShpg;
            }
            mdpopShipping.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindEditData();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            BindGrid();
            BindPager();
        }

        protected void btnbillingt_Click(object sender, EventArgs e)
        {
            var objBillAddress = new tblOrderBillingAddress();
            objBillAddress.Address1 = txtbadd.Text;
            objBillAddress.Address2 = txtbadd2.Text;
            objBillAddress.City = txtbcity.Text;
            objBillAddress.EmailAddress = txtbemail.Text;
            objBillAddress.FirstName = txtbfname.Text;
            objBillAddress.LastName = txtblname.Text;
            objBillAddress.State = txtbState.Text;
            objBillAddress.Postcode = txtbzip.Text;
            objBillAddress.Title = ddlbtitle.SelectedValue;
            objBillAddress.Country = ddlbcountry.SelectedValue;
            objBillAddress.Phone = txtbphone.Text;
            objBillAddress.IsVisibleEmailAddress = chkVisibleEmailAddress.Checked;
            Objbooking.UpdateBillingAddress(Convert.ToInt64(Request["id"]), objBillAddress);
            Objbooking.UpdateBillingEmailAddressVisible(Convert.ToInt64(Request["id"]), chkVisibleEmailAddress.Checked);
            var billing = _masterOrder.GetBillingInfo(Convert.ToInt32(Request["id"]));
            if (billing.Any())
            {
                rptBilling.DataSource = billing;
                rptBilling.DataBind();
                mdpopBilling.Hide();
                divlist.Visible = false;
                divDetail.Visible = true;
                lnkAddShipping.Visible = false;
                lnkAddBilling.Visible = false;
            }
            BindCountry();
            BindEditData();
        }

        protected void btnShipping_Click(object sender, EventArgs e)
        {
            var objBillAddress = new tblOrderBillingAddress();
            if (objBillAddress != null)
            {
                objBillAddress.Address1Shpg = txtAdd.Text;
                objBillAddress.Address2Shpg = txtAdd2.Text;
                objBillAddress.CityShpg = txtCity.Text;
                objBillAddress.EmailAddressShpg = txtEmail.Text;
                objBillAddress.FirstNameShpg = txtFirst.Text;
                objBillAddress.LastNameShpg = txtLast.Text;
                objBillAddress.StateShpg = txtState.Text;
                objBillAddress.PostcodeShpg = txtZip.Text;
                objBillAddress.TitleShpg = ddlMr.SelectedValue;
                objBillAddress.CountryShpg = ddlCountry.SelectedValue;
                objBillAddress.PhoneShpg = txtPhone.Text;
                Objbooking.UpdateShippingAddress(Convert.ToInt64(Request["id"]), objBillAddress);
            }
            var billing = _masterOrder.GetBillingInfo(Convert.ToInt32(Request["id"]));
            if (billing.Any())
            {
                rptShipping.DataSource = billing;
                rptShipping.DataBind();
                mdpopShipping.Hide();
                divlist.Visible = false;
                divDetail.Visible = true;
                lnkAddShipping.Visible = false;
                lnkAddBilling.Visible = false;
            }
            BindEditData();
        }

        protected void btnTravellerUpdate_Click1(object sender, EventArgs e)
        {
            var obj = new tblOrderTraveller();
            obj.Country = Guid.Parse(ddlTrCountry.SelectedValue);
            obj.FirstName = ddlTrFirstName.Text;
            obj.LastName = txtTrLName.Text;
            obj.LeadPassenger = true;
            obj.Title = ddlTrTitle.SelectedValue;

            long ordid = Convert.ToInt64(Request["id"]);
            if (Session["TrvID"] != null)
                Objbooking.UpdateTraveller(Guid.Parse(Session["TrvID"].ToString()), obj);
            else
            {
                var lookUp = _db.tblPassP2PSalelookup.FirstOrDefault(x => x.OrderID == ordid);
                if (lookUp != null)
                {
                    obj.ID = Guid.NewGuid();
                    Objbooking.AddTraveller(obj, lookUp.ID);
                }
            }

            List<GetP2PSaleData> lst = Objbooking.GetP2PSaleDetail(Convert.ToInt32(Request["id"]));
            grdTraveller.DataSource = lst;
            grdTraveller.DataBind();
            mupTraveller.Hide();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindEditData();
        }

        protected void grdTraveller_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                Session["TrvID"] = id;

                tblOrderTraveller obj = Objbooking.GetTraveller(id);
                if (obj != null)
                {
                    if (obj.Country != Guid.Empty)
                        ddlTrCountry.SelectedValue = obj.Country.ToString();
                    ddlTrFirstName.Text = obj.FirstName;
                    txtTrLName.Text = obj.LastName;
                }
            }
            mupTraveller.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindEditData();
        }

        protected void btnOrderStatus_Click(object sender, EventArgs e)
        {
            Objbooking.UpdateOrderStatus(Convert.ToInt32(ddlOrderStatus.SelectedValue), Convert.ToInt64(Request["id"]));
            mpeOrderStatus.Hide();
            if (ddlOrderStatus.SelectedValue == "3")
                SendMail();
            GetOrdersForEdit(Convert.ToInt32(Request["id"]));
            divlist.Visible = false;
            divDetail.Visible = true;

            var lst = Objbooking.GetP2PSaleDetail(Convert.ToInt32(Request["id"])).OrderBy(t => t.ShortOrder).ToList(); ;
            if (lst != null && lst.Count() > 0 && lst.FirstOrDefault().TrvType == "P2P")
            {
                var data = lst.FirstOrDefault();
                if (data.Status != 3 && data.Status != 7 && data.Status != 9 && data.Status != 19)
                    lnkPrintReceipt.Visible = false;
                else
                    lnkPrintReceipt.Visible = true;
            }
        }

        protected void btnUpdatePnrNo_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnPnrNoUpdate.Value) && !string.IsNullOrEmpty(txtPnrNo.Text))
            {
                bool result = Objbooking.UpdatePnrNoByPassSaleId(Guid.Parse(hdnPnrNoUpdate.Value), txtPnrNo.Text);
                if (result)
                {
                    hdnPnrNoUpdate.Value = txtPnrNo.Text = "";
                    GetOrdersForEdit(Convert.ToInt32(Request["id"]));
                }
            }
        }

        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        protected void grdOrders_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            ViewState["sortExpression"] = sortExpression;

            GridViewSortDirection = GridViewSortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
            BindGrid();
            BindPager();
        }

        protected void rptJourney_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var divPrintURL = e.Item.FindControl("divPrintURL") as HtmlControl;
            var hdnPdfURL = e.Item.FindControl("hdnPdfURL") as HiddenField;
            var lnkRefund = e.Item.FindControl("lnkRefund") as LinkButton;
            var lblTrvType = e.Item.FindControl("lblTrvType") as Label;
            var hdnPassP2PSaleID = e.Item.FindControl("hdnPassP2PSaleID") as HiddenField;
            var hdnID = e.Item.FindControl("hdnID") as HiddenField;
            var hdnIsBENE = e.Item.FindControl("hdnIsBENE") as HiddenField;
            var lnkTiRefund = e.Item.FindControl("lnkTiRefund") as LinkButton;
            var lblExtraChargeAmount = e.Item.FindControl("lblExtraChargeAmount") as Label;
            var lnkEditExtraCharge = e.Item.FindControl("lnkEditExtraCharge") as LinkButton;
            var hdnPnr = e.Item.FindControl("hdnPnr") as HiddenField;
            var lnkUpdatePnrNo = e.Item.FindControl("lnkUpdatePnrNo") as LinkButton;

            if (!string.IsNullOrEmpty(hdnID.Value))
            {
                Guid Id = Guid.Parse(hdnID.Value);
                decimal ExtraCharge = Objbooking.GetExtraCharge(Id, "P2P");
                if (ExtraCharge > 0)
                {
                    lblExtraChargeAmount.Text = Currency + " " + ExtraCharge.ToString("F");
                    lnkEditExtraCharge.Visible = false;
                }
                else
                    lblExtraChargeAmount.Visible = false;
            }

            if (lnkTiRefund != null && !string.IsNullOrEmpty(lnkTiRefund.ToolTip))
                lnkTiRefund.Visible = !IsRefundedReservation(lnkTiRefund.ToolTip);

            if (lnkRefund != null && hdnPassP2PSaleID != null && hdnIsBENE != null)
            {
                List<GetP2PSaleRefundsData> lstRefund = Objbooking.GetP2PSaleRefundsDetail(Convert.ToInt64(Request["id"].ToString())).Where(a => a.PassP2PSaleID == Guid.Parse(hdnPassP2PSaleID.Value)).ToList();
                if (lstRefund.Count() > 0)
                    lnkRefund.Visible = false;
                else
                    lnkRefund.Visible = true;
                if (IsBENE)
                {
                    lnkRefund.Visible = false;
                    divPrintURL.Visible = false;
                }
                lnkRefund.PostBackUrl = "P2POrderRefund.aspx?OrdId=" + Request["id"].ToString() + "&tpId=" + hdnPassP2PSaleID.Value;

                if (hdnIsBENE.Value.ToLower() == "true")
                    IsBENE = true;
                if (string.IsNullOrEmpty(hdnPdfURL.Value))
                    divPrintURL.Visible = false;
            }

            if (hdnStatus.Value == "3" || hdnStatus.Value == "7" || hdnStatus.Value == "9" || hdnStatus.Value == "16" || hdnStatus.Value == "19" || hdnStatus.Value == "22")
                lnkUpdatePnrNo.Visible = true;
            else
                lnkUpdatePnrNo.Visible = false;
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        void BindOrderNotes(int ordId)
        {
            var oNotes = _masterOrder.GetOrderNotesList(ordId);
            if (oNotes != null)
            {
                grdOrderNotes.DataSource = oNotes.OrderBy(x => x.CreatedOn);
                grdOrderNotes.DataBind();
            }
        }

        protected void btnAddNotes_Click(object sender, EventArgs e)
        {
            AddEditOrderNotes();
            Response.Redirect("P2POrderDetails.aspx?id=" + Convert.ToInt32(Request["id"]));
        }

        protected void grdOrderNotes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    Response.Redirect("P2POrderDetails.aspx?id=" + Convert.ToInt32(Request["id"]) + "&notesid=" + id);
                }

                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _masterOrder.DeleteOrderNotes(id);
                    if (res)
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Record deleted successfully.')", true);
                    Response.Redirect("P2POrderDetails.aspx?id=" + Convert.ToInt32(Request["id"]));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void EditOrderNotes(Guid id)
        {
            var oP = _masterOrder.GetOrderNotesById(id);
            if (oP != null)
                txtNotes.InnerHtml = oP.Notes;
        }

        public void AddEditOrderNotes()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNotes.InnerHtml))
                {
                    var id = (Request["notesid"] == "" || Request["notesid"] == null) ? new Guid() : Guid.Parse(Request["notesid"]);
                    _masterOrder.AddEditOrderNotes(new tblOrderNote
                        {
                            ID = id,
                            OrderID = Convert.ToInt32(Request["id"]),
                            UserName = AdminuserInfo.Username,
                            Notes = txtNotes.InnerHtml,
                            CreatedOn = DateTime.Now,
                            ModifiedOn = DateTime.Now
                        });

                    //ScriptManager.RegisterStartupScript(this, GetType(), "", !string.IsNullOrEmpty(Request["notesid"]) ? "alert('Notes updated successfully.')" : "alert('Notes added successfully.')", true);
                    txtNotes.InnerHtml = string.Empty;
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Please Enter notes.')", true);
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (SendMail())
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.');", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Failed to send E-mail confimation! Please check Email Settings.');", true);
            GetOrdersForEdit(Convert.ToInt32(Request["id"]));
            mupTraveller.Hide();
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                _siteId = Master.SiteID;
                GetCurrencyCode();
                bool isDiscountSite = _masterOrder.GetOrderDiscountVisibleBySiteId(_siteId);
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    var orderID = Convert.ToInt32(Request["id"]);
                    string Subject = "Order Confirmation #" + orderID;

                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(_siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(_siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml.ToString();

                    string UserName, EmailAddress, DeliveryAddress, BillingAddress, OrderDate, OrderNumber, Total, ShippingAmount, GrandTotal, NetTotal, Discount, AdminFee;
                    decimal BookingFee = 0; UserName = Discount = NetTotal = EmailAddress = DeliveryAddress = BillingAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = AdminFee = "";
                    string OtherCharges = string.Empty;
                    string BookingRefNO = string.Empty;
                    string PassProtection = "0.00";
                    string PassProtectionHtml = "";
                    string DiscountHtml = "";
                    bool isBeneDeliveryByMail = false;
                    bool isAgentSite = false;

                    var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Request.Params["id"])).OrderBy(a => a.OrderIdentity).ToList();
                    var lst1 = from a in lst
                               select
                                   new
                                   {
                                       Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                       ProductDesc =
                               (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                                " " + a.LastName),
                                       TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                   };
                    if (lst.Count > 0)
                    {
                        EmailAddress = lst.FirstOrDefault().EmailAddress;
                        UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                        DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                        lst.FirstOrDefault().DCountry + "<br>" +
                        lst.FirstOrDefault().Postcode + "<br>" +
                        GetBillingAddressPhoneNo(Convert.ToInt64(Request.Params["id"]));

                        OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                        Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                        ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                        BookingFee = lst.FirstOrDefault().BookingFee != null ? (decimal)lst.FirstOrDefault().BookingFee : 0;
                        Discount = _masterOrder.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));

                        AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(orderID).ToString();
                        NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                            (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                            (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                            Convert.ToDecimal(AdminFee) + Convert.ToDecimal(EvOtherCharges)).ToString();
                        GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                        var billAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(orderID));
                        if (billAddress != null)
                        {
                            BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                        }
                    }
                    // to address
                    string ToEmail = EmailAddress;
                    GetReceiptLoga();
                    body = body.Replace("##headerstyle##", SiteHeaderColor);
                    body = body.Replace("##sitelogo##", logo);
                    body = body.Replace("##OrderNumber##", OrderNumber);
                    body = body.Replace("##UserName##", UserName.Trim());
                    body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                    body = body.Replace("##OrderDate##", OrderDate.Trim());
                    body = body.Replace("##Items##", Currency + " " + Total.Trim());
                    body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");

                    var tblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderID);
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    isAgentSite = objsite.IsAgent.Value;

                    GetSiteType(orderID);
                    if (isEvolviBooking)
                        EvolviTandC = objsite.SiteURL + "uk-ticket-collection";

                    string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + AdminFee.Trim() + "</td></tr>";
                    if (AdminFee == "0.00")
                        AdminFeeRow = string.Empty;
                    if (isDiscountSite)
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        if (Discount == "0.00")
                            body = body.Replace("##Discount##", "");
                        else
                        {
                            DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + Discount.Trim() + "</td></tr>";
                            body = body.Replace("##Discount##", DiscountHtml);
                        }
                    }
                    else
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        body = body.Replace("##Discount##", "");
                    }

                    if (isEvolviBooking)
                    {
                        OtherCharges = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Other Charges:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + EvOtherCharges + "</td></tr>";
                        BookingRefNO = "<tr><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>Booking Ref: </strong></td><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>" + EvolviBookingRefNO(orderID) + "</td></tr>";
                    }

                    body = body.Replace("##BookingRef##", BookingRefNO);
                    body = body.Replace("##Shipping##", Currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##DeliveryFee##", Currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##BookingFee##", Currency + " " + BookingFee);
                    body = body.Replace("##Total##", Currency + " " + GrandTotal.Trim());

                    if (tblord != null)
                    {
                        Session["ShipMethod"] = tblord.ShippingMethod;
                        Session["ShipDesc"] = tblord.ShippingDescription;
                        Session["CollectStation"] = tblord.CollectionStation;
                    }

                    var P2PSaleList = new ManageBooking().GetP2PSaleListByOrderID(orderID);
                    if (isEvolviBooking)
                        body = body.Replace("##ShippingMethod##", "Ticket on collection");
                    else if (isBene)
                    {
                        if (P2PSaleList != null && !string.IsNullOrEmpty(P2PSaleList.FirstOrDefault().DeliveryOption))
                        {
                            string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                            if (P2PSaleList.Any(x => x.DeliveryOption.ToLower() == "delivery by mail"))
                            {
                                body = body.Replace("##ShippingMethod##", !string.IsNullOrEmpty(shippingdesc) ? shippingdesc : "Delivery by mail");
                                isBeneDeliveryByMail = true;
                            }
                            else
                                body = body.Replace("##ShippingMethod##", P2PSaleList != null ? P2PSaleList.FirstOrDefault().DeliveryOption : "");
                        }
                    }
                    else
                    {
                        if (Session["ShipMethod"] != null)
                        {
                            string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                            body = body.Replace("##ShippingMethod##", shippingdesc);
                        }
                        else
                            body = body.Replace("##ShippingMethod##", "");
                    }
                    body = body.Replace("#Blanck#", "&nbsp;");


                    var lstC = new ManageBooking().GetAllCartData(orderID).OrderBy(a => a.OrderIdentity).ToList();
                    var lstNew = (from a in lstC
                                  select new
                                  {
                                      a,
                                      Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                      ProductDesc = a.ProductType == "P2P"
                                      ? (Objbooking.getP2PDetailsForEmail(a.PassSaleID, Objbooking.GetPrintResponse((Guid)a.PassSaleID), a.ProductType, isEvolviBooking, isAgentSite, EvolviTandC))
                                      : (Objbooking.getPassDetailsForEmail(a.PassSaleID, Objbooking.GetPrintResponse((Guid)a.PassSaleID), a.ProductType, EvolviTandC)),
                                      TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                      CommissionFee = a.CommissionFee,
                                      Terms = a.terms
                                  }).ToList();

                    string strProductDesc = "";
                    int i = 1;
                    if (lstNew.Count() > 0)
                    {
                        lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                        if (lstNew.Any(x => x.a.ProductType == "P2P"))
                        {
                            if (isBeneDeliveryByMail)
                            {
                                body = body.Replace("##HeaderText##", "Delivery address");
                                body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                                body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                            }
                            else
                            {
                                body = body.Replace("##HeaderText##", "Tickets");
                                if (!isEvolviBooking && !isBene && !isNTVBooking && !isTI)
                                {
                                    body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                                    body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                                }
                                else
                                {
                                    body = body.Replace("##HeaderDeliveryText##", "Your ticket collection reference:");
                                    body = body.Replace("##DeliveryAddress##", Objbooking.getP2PDetailsForEmailTicket(orderID, EvolviTandC, objsite.SiteURL + "images/pdf-istock.jpg"));
                                }
                            }
                        }
                        else
                        {
                            body = body.Replace("##HeaderText##", "Delivery address");
                            body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                            body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                        }

                        foreach (var x in lstNew)
                        {
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                            if (isAgentSite)
                                strProductDesc += x.ProductDesc.Replace("##Price##", Currency + " " + x.Price.ToString()).Replace("##NetAmount##", "Net Amount :" + Currency + " " + (x.Price + x.TktPrtCharge).ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));
                            else
                                strProductDesc += x.ProductDesc.Replace("##Price##", Currency + " " + x.Price.ToString()).Replace("##NetAmount##", "").Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));

                            strProductDesc += (HttpContext.Current.Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Session["CollectStation"] == null ? "" : HttpContext.Current.Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");
                            if (P2PSaleList != null && P2PSaleList.Count > 0 && P2PSaleList.Any(z => z.ApiName.ToUpper() == "ITALIA"))
                            {
                                if (string.IsNullOrEmpty(P2PSaleList.FirstOrDefault(z => z.ID == x.a.PassSaleID).PdfURL))
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Important:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>This PNR is issued as a ticket on depart and must be printed from one of the self-service ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine.</td></tr>";
                            }
                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            if (x.Terms != null)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Server.HtmlDecode(x.Terms) + "</td></tr>";
                            if (isEvolviBooking)
                            {
                                var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == x.a.PassSaleID.Value);
                                if (P2PData != null)
                                {
                                    if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                    {
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>";
                                        foreach (var item in P2PData.EvSleeper.Split('@'))
                                        {
                                            string ss = item.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                            strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                        }
                                        strProductDesc += "</td></tr>";
                                    }
                                }
                            }
                            strProductDesc += "</table></td></tr></table>";
                            i++;
                        }
                    }
                    PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + Currency + " " + PassProtection.Trim() + "</td></tr>";
                    body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                    body = body.Replace("##OrderDetails##", strProductDesc);
                    body = body.Replace("##BillingAddress##", BillingAddress);

                    var getDeliveryType = Objbooking.getDeliveryOption(orderID);
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth '><tr><td style='padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;'colspan='2'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</td></tr></table>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                    body = body.Replace("../images/", SiteName + "images/");

                    var billing = _masterOrder.GetBillingInfo(orderID);
                    if (billing.Any())
                    {
                        var firstOrDefault = billing.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            billingEmailAddress = firstOrDefault.EmailAddress;
                            shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                        }
                    }

                    /*Get smtp details*/
                    var result = _masterPage.GetEmailSettingDetail(_siteId);
                    if (result != null)
                    {
                        var fromAddres = new MailAddress(result.Email, result.Email);
                        smtpClient.Host = result.SmtpHost;
                        smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                        smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                        message.From = fromAddres;
                        message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        smtpClient.Send(message);
                        retVal = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Failed to send E-mail confimation!");
            }
            return retVal;
        }

        public string GetBillingAddressPhoneNo(long OrderID)
        {
            string BillingPhoneNo = string.Empty;
            var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            if (result != null)
            {
                BillingPhoneNo += !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
                BillingPhoneNo += result.IsVisibleEmailAddress ? "<br/>" + result.EmailAddress : "";
            }
            return BillingPhoneNo;
        }

        public void GetReceiptLoga()
        {
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    logo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    logo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    logo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        public void GetSiteType(long OrderID)
        {
            var apiTypeList = new ManageBooking().GetAllApiType(OrderID);
            if (apiTypeList != null)
            {
                isEvolviBooking = apiTypeList.isEvolvi;
                isNTVBooking = apiTypeList.isNTV;
                isBene = apiTypeList.isBene;
                isTI = apiTypeList.isTI;
            }
        }

        public string EvolviBookingRefNO(long OrderID)
        {
            var result = _db.tblEvolviOtherChargesLookUps.FirstOrDefault(x => x.OrderId == OrderID);
            if (result != null)
            {
                return !string.IsNullOrEmpty(result.BookingRef) ? result.BookingRef : "";
            }
            return "";
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddltypeDate_SelectIndexChange(object sender, EventArgs e)
        {
            BindGrid();
            BindPager();
        }

        protected void btnExtraChargeSave_Click(object sender, EventArgs e)
        {
            mpExtraCharge.Hide();
            if (!String.IsNullOrEmpty(txtExtraCharge.Text))
            {
                Guid PassSaleId = string.IsNullOrEmpty(hdnpassaleidextracharge.Value) ? Guid.Empty : Guid.Parse(hdnpassaleidextracharge.Value);
                decimal ExtraCharge = Convert.ToDecimal(txtExtraCharge.Text);
                Objbooking.UpdateExtraChargeP2P(ExtraCharge, PassSaleId);
            }
            txtExtraCharge.Text = string.Empty;
            BindGrid();
            GetOrdersForEdit(Convert.ToInt32(Request["id"]));
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        protected void rptJourney_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "AdminExtraCharge")
            {
                hdnpassaleidextracharge.Value = (e.CommandArgument.ToString());
                mpExtraCharge.Show();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            else if (e.CommandName == "DeleteJourney")
            {
                Guid Id = Guid.Parse(e.CommandArgument.ToString());
                Objbooking.DeleteP2PSaleDataById(Id);
                BindEditData();
            }
            else if (e.CommandName == "EditShipping")
            {
                Guid Id = Guid.Parse(e.CommandArgument.ToString());
                hdnEditJourneyId.Value = Id.ToString();
                var result = _db.tblP2PSale.FirstOrDefault(x => x.ID == Id);
                if (result != null)
                {
                    if (!string.IsNullOrEmpty(result.DeliveryOption) && ddlEditShippindMethod.Items.FindByText(result.DeliveryOption) != null)
                        ddlEditShippindMethod.SelectedValue = result.DeliveryOption;
                    else
                        ddlEditShippindMethod.SelectedIndex = 0;
                }
                mupShippingMethod.Show();
                divlist.Visible = false;
                divDetail.Visible = true;
                BindEditData();
            }
            else if (e.CommandName == "EditJourney")
            {
                JourneyLoadEvent();
                //ddlSupplier.Visible = lblSupplier.Visible = false;
                Guid Id = Guid.Parse(e.CommandArgument.ToString());
                hdnJourneyId.Value = e.CommandArgument.ToString();
                tblP2PSale obj = Objbooking.GetP2PSaleDataById(Id);
                if (obj != null)
                {
                    txtTrainNumber.Text = obj.TrainNo;
                    txtFrom.Text = obj.From;
                    txtTo.Text = obj.To;
                    txtDepartureDate.Text = obj.DateTimeDepature.Value.ToString("dd/MMM/yyyy");
                    ddldepTime.SelectedIndex = Convert.ToInt32(!string.IsNullOrEmpty(obj.DepartureTime) ? obj.DepartureTime.Split(':')[0] : "0");
                    ddlDepSec.SelectedIndex = Convert.ToInt32(!string.IsNullOrEmpty(obj.DepartureTime) ? obj.DepartureTime.Split(':')[1] : "0");
                    txtReturnDate.Text = obj.DateTimeArrival.Value.ToString("dd/MMM/yyyy");
                    ddlReturnTime.SelectedIndex = Convert.ToInt32(!string.IsNullOrEmpty(obj.ArrivalTime) ? obj.ArrivalTime.Split(':')[0] : "0");
                    ddlRetSec.SelectedIndex = Convert.ToInt32(!string.IsNullOrEmpty(obj.ArrivalTime) ? obj.ArrivalTime.Split(':')[1] : "0");

                    ddlAdult.SelectedValue = obj.Adult;
                    ddlChildren.SelectedValue = obj.Children;
                    ddlSeniors.SelectedValue = obj.Senior;
                    ddlYouth.SelectedValue = obj.Youth;
                    txtClass.Text = obj.Class;
                    txtprice.Text = obj.NetPrice.ToString();
                    txtFareName.Text = obj.FareName;
                    txtDeliveryOption.Text = obj.DeliveryOption;
                    txtTerms.InnerHtml = obj.Terms;
                    txtTicketprotection.Text = (obj.TicketProtection.HasValue ? obj.TicketProtection.Value.ToString() : "0");
                    txtCommission.Text = obj.CommissionFee.ToString();
                    //txtbookingfee.Text = obj.BookingFee.ToString();
                    ddlFipClass.SelectedValue = !string.IsNullOrEmpty(obj.FIPClass) ? obj.FIPClass.Trim() : string.Empty;
                    txtFipCardNumber.Text = obj.FIPNumber;
                    txtVia.Text = obj.Via;
                    if (ddlSupplier.Items.FindByValue(obj.ApiName) != null)
                        ddlSupplier.SelectedValue = obj.ApiName;
                }
                divlist.Visible = false;
                divDetail.Visible = true;
                mpejourney.Show();
            }
            else if (e.CommandName == "TiRefund")
            {
                var hdnPnr = e.Item.FindControl("hdnPnr") as HiddenField;
                if (hdnPnr != null)
                    Response.Redirect("p2prefund.aspx?pn=" + hdnPnr.Value + "&id=" + Request["id"]);
            }
            else if (e.CommandName == "UpdatePnrNo")
            {
                hdnPnrNoUpdate.Value = (e.CommandArgument.ToString());
                var hdnPnr = e.Item.FindControl("hdnPnr") as HiddenField;
                txtPnrNo.Text = hdnPnr.Value;
                mpPnrNoEdit.Show();
            }
        }

        public bool IsRefundedReservation(string resCode)
        {
            var bk = new ManageP2PBooking();
            return bk.IsRefundedReservation(resCode);
        }

        protected void btnTakePayment_Click(object sender, EventArgs e)
        {
            trTakepayment.Visible = true;
            BindEditData();
        }

        protected void btnRef_Click(object sender, EventArgs e)
        {
            if (txtRefNum.Text != null)
            {
                if (!string.IsNullOrEmpty(Request["id"]))
                {
                    var OrderId = Convert.ToInt64(Request["id"]);
                    Objbooking.UpdateAgentReferenceNumber(txtRefNum.Text.Trim(), Convert.ToInt64(OrderId));
                    Objbooking.UpdateOrderStatus(7, Convert.ToInt64(OrderId));
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
            }
        }

        /*Shipping method*/
        protected void lnkEditShipMethod_Click(object sender, EventArgs e)
        {
            if (Session["ShippingMethodId"] != null)
                ddlShippindMethod.SelectedValue = Session["ShippingMethodId"].ToString();
            mupShipMethod.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindEditData();
        }

        protected void btnShipMethod_Click(object sender, EventArgs e)
        {
            if (ddlShippindMethod.SelectedValue != "0")
            {
                var shipID = Guid.Parse(ddlShippindMethod.SelectedValue);
                var shippingDetails = _db.tblShippings.FirstOrDefault(x => x.ID == shipID);
                if (shippingDetails != null)
                {
                    var shipMehod = shippingDetails.ShippingName;
                    var shipDesc = shippingDetails.Description;
                    var shipAmount = shippingDetails.Price;
                    Objbooking.UpdateShippingMethodForManualBooking(shipMehod, shipDesc, shipAmount, Convert.ToInt64(Request["id"]));
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
            }
        }

        protected void btnShippingMethod_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnEditJourneyId.Value) && ddlEditShippindMethod.SelectedValue != "0")
            {
                Guid JourneyId = Guid.Parse(hdnEditJourneyId.Value);
                var result = _db.tblP2PSale.FirstOrDefault(x => x.ID == JourneyId);
                if (result != null)
                {
                    result.DeliveryOption = ddlEditShippindMethod.SelectedValue;
                    _db.SaveChanges();
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
            }
        }

        #region Journey Editing

        public void JourneyLoadEvent()
        {
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
            unavailableDates1 = "[";
            if (siteDDates.Count() > 0)
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
            tblAdminUser objA = Objbooking.GetAdminUserbyId(AdminuserInfo.UserID);

            for (int j = 99; j >= 0; j--)
            {
                ddlChildren.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSeniors.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
            }
            for (int k = 0; k <= 59; k++)
            {
                ddlRetSec.Items.Add((k < 10 ? "0" : "") + k.ToString());
                ddlDepSec.Items.Add((k < 10 ? "0" : "") + k.ToString());
            }
            ddlAdult.SelectedValue = "1";

            Int32 orderno = Convert.ToInt32(Request["id"]);
            var odr = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderno);
            if (odr != null)
            {
                OrderSitelId = odr.SiteID.Value;
            }
            var list = _masterOrder.GetManualP2PCommitionbySiteid(OrderSitelId).OrderBy(x => x.Name).ToList();
            ddlSupplier.DataSource = list;
            ddlSupplier.DataTextField = "Name";
            ddlSupplier.DataValueField = "Name";
            ddlSupplier.DataBind();
            ddlSupplier.Items.Insert(0, new ListItem("-Please select-", "0"));
            ClearP2PData();
        }

        protected void btnJourneyUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlAdult.SelectedValue == "0" && ddlChildren.SelectedValue == "0" && ddlSeniors.SelectedValue == "0" && ddlYouth.SelectedValue == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('Select aleast one passenger.')", true);
                    BindEditData();
                    mpejourney.Show();
                    return;
                }
                GetCurrencyCode();
                Guid Id = Guid.Parse(hdnJourneyId.Value);
                tblP2PSale obj = new tblP2PSale
                {
                    ID = Id,
                    From = txtFrom.Text.Trim(),
                    To = txtTo.Text.Trim(),
                    DateTimeDepature = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null),
                    DepartureTime = ddldepTime.SelectedValue + ":" + ddlDepSec.SelectedValue,
                    DateTimeArrival = DateTime.ParseExact(txtReturnDate.Text, "dd/MMM/yyyy", null),
                    ArrivalTime = ddlReturnTime.SelectedValue + ":" + ddlRetSec.SelectedValue,
                    TrainNo = txtTrainNumber.Text,
                    Senior = ddlSeniors.SelectedValue,
                    Adult = ddlAdult.SelectedValue,
                    Children = ddlChildren.SelectedValue,
                    Youth = ddlYouth.SelectedValue,
                    Passenger =
                        ddlAdult.SelectedValue + " Adult " + ddlChildren.SelectedValue + " Children " +
                        ddlSeniors.SelectedValue + " Seniors " + ddlYouth.SelectedValue + " Youth",
                    NetPrice = Convert.ToDecimal(txtprice.Text),
                    Price = Convert.ToDecimal(txtprice.Text),
                    Class = txtClass.Text,
                    FareName = txtFareName.Text,
                    DeliveryOption = txtDeliveryOption.Text,
                    Terms = txtTerms.InnerHtml,
                    ApiPrice = Convert.ToDecimal(txtprice.Text),
                    CurrencyId = CurrencyID,
                    TicketProtection = Convert.ToDecimal(string.IsNullOrEmpty(txtTicketprotection.Text) ? "0" : txtTicketprotection.Text),
                    //BookingFee = Convert.ToDecimal(string.IsNullOrEmpty(txtbookingfee.Text) ? "0" : txtbookingfee.Text),
                    CommissionFee = Convert.ToDecimal(string.IsNullOrEmpty(txtCommission.Text) ? "0" : txtCommission.Text),
                    ApiName = ddlSupplier.SelectedValue == "0" ? "" : ddlSupplier.SelectedValue,
                    FIPClass = ddlFipClass.SelectedValue == "0" ? "" : ddlFipClass.SelectedValue,
                    FIPNumber = txtFipCardNumber.Text.Trim(),
                    Via = txtVia.Text
                };
                Objbooking.UpdateP2PSaleDataById(obj, OrderId);
                ClearP2PData();
                BindEditData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClearP2PData()
        {
            ddldepTime.SelectedValue = ddlReturnTime.SelectedValue = "09";
            ddlDepSec.SelectedValue = ddlRetSec.SelectedValue = "00";
            ddlSupplier.SelectedIndex = ddlSeniors.SelectedIndex = ddlAdult.SelectedIndex = ddlChildren.SelectedIndex = ddlYouth.SelectedIndex = ddlFipClass.SelectedIndex = 0;
            hdnJourneyId.Value = txtClass.Text = txtFrom.Text = txtTo.Text = txtDepartureDate.Text = txtReturnDate.Text = txtTrainNumber.Text = txtprice.Text = txtFareName.Text = txtDeliveryOption.Text = txtTerms.InnerHtml = txtCommission.Text = txtTicketprotection.Text = txtFipCardNumber.Text = string.Empty;
        }

        protected void BtnAddJourney_Click(object sender, EventArgs e)
        {
            JourneyLoadEvent();
            hdnJourneyId.Value = Guid.NewGuid().ToString();
            divlist.Visible = false;
            divDetail.Visible = true;
            mpejourney.Show();
        }
        #endregion

        #region Paging
        public void BindPager()
        {
            try
            {
                int no = 1;
                var oPageList = new List<ClsPageCount>();
                int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
                int newpagecount = (TotalRecord - 1) / pageSize + (((TotalRecord - 1) % pageSize) > 0 ? 1 : 0);
                if (nxtpaging > 0)
                    pageIndex = no = nxtpaging;
                else
                    no = pageIndex > 10 ? (((pageIndex - 1) / 10) * 10) + 1 : no;
                if (no > 10)
                    lnkPrepaging.Visible = true;
                else
                    lnkPrepaging.Visible = false;
                lnkPrepaging.Attributes.Add("newpage", (no > 1 ? (no - 1) : 1).ToString());
                for (int i = no; i <= newpagecount; i++)
                {
                    if (i == newpagecount || newpagecount < 11)
                        lnknextpaging.Visible = false;
                    else
                        lnknextpaging.Visible = true;
                    var oPage = new ClsPageCount { PageCount = i.ToString() };
                    if ((i % 10) == 0)
                    {
                        oPageList.Add(oPage);
                        lnknextpaging.Attributes.Add("newpage", (i + 1).ToString());
                        i = newpagecount;
                    }
                    else
                        oPageList.Add(oPage);
                }
                if (newpagecount <= 1)
                {
                    lnknextpaging.Visible = lnkPrepaging.Visible = lnkPrevious.Visible = lnkNext.Visible = false;
                    litTotalPages.Text = "";
                }
                else
                {
                    if (pageIndex == 1)
                        lnkPrevious.Visible = false;
                    else
                        lnkPrevious.Visible = true;
                    if (pageIndex == newpagecount)
                        lnkNext.Visible = false;
                    else
                        lnkNext.Visible = true;
                    litTotalPages.Text = "Showing page " + CurrentPage + " of " + newpagecount;
                }
                DLPageCountItem.DataSource = oPageList;
                DLPageCountItem.DataBind();
                foreach (RepeaterItem item1 in DLPageCountItem.Items)
                {
                    var lblPage = (LinkButton)item1.FindControl("lnkPage");
                    if (lblPage.Text.Trim() == (pageIndex).ToString(CultureInfo.InvariantCulture))
                        lblPage.Attributes.Add("class", "activepaging");
                    else
                        lblPage.Attributes.Remove("class");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void lnkPrepaging_Click(object sender, EventArgs e)
        {
            CurrentPage = Convert.ToInt32(lnkPrepaging.Attributes["newpage"]);
            BindGrid();
            BindPager();
        }

        protected void lnknextpaging_Click(object sender, EventArgs e)
        {
            CurrentPage = nxtpaging = Convert.ToInt32(lnknextpaging.Attributes["newpage"]);
            BindGrid();
            BindPager();
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument);
            ViewState["PageIndex"] = pageIndex;
            BindGrid();
            BindPager();
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage - 1;
            BindGrid();
            BindPager();
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage + 1;
            BindGrid();
            BindPager();
        }

        private int CurrentPage
        {
            get
            {
                return ((ViewState["PageIndex"] == null || ViewState["PageIndex"].ToString() == "") ? 1 : int.Parse(ViewState["PageIndex"].ToString()));
            }
            set
            {
                ViewState["PageIndex"] = value;
            }
        }
        #endregion

        #region Refund Admin,booking,shipping Fee
        public void BindPercentage()
        {
            var rPerc = _masterOrder.BindRefundPerc();
            ddlrefundsel.DataSource = rPerc;
            ddlrefundsel.DataTextField = "Description";
            ddlrefundsel.DataValueField = "PercentageRefund";
            ddlrefundsel.DataBind();
            ddlrefundsel.Items.Insert(0, new ListItem("-Select Refund-", "-1"));
            GetCurrencyCode();
        }

        protected void lnkRefShippingFee_Click(object sender, EventArgs e)
        {
            hdnrfndtype.Value = "shipping";
            lblhdr.Text = "Shipping Cost:";
            lblhdrRefundTxt.Text = lblShippingAmount.Text;
            lblrefhdr.Text = "Shipping Cost Refund:";
            mprefund.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindPercentage();
        }

        protected void lnkRefBookingFee_Click(object sender, EventArgs e)
        {
            hdnrfndtype.Value = "booking";
            lblhdr.Text = "Booking Fee:";
            lblhdrRefundTxt.Text = lblBookingFee.Text;
            lblrefhdr.Text = "Booking Fee Refund:";
            mprefund.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindPercentage();
        }

        protected void lnkRefAdminFee_Click(object sender, EventArgs e)
        {
            hdnrfndtype.Value = "admin";
            lblhdr.Text = "Admin Fee:";
            lblhdrRefundTxt.Text = lblAdminFee.Text;
            lblrefhdr.Text = "Admin Fee Refund:";
            mprefund.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
            BindPercentage();
        }

        protected void btnrefund_Click(object sender, EventArgs e)
        {
            try
            {
                var order = Convert.ToInt32(Request["id"]);
                tblOrderRefundAdmin obj = new tblOrderRefundAdmin();
                obj.ID = Guid.NewGuid();
                obj.OrderID = order;
                obj.CreatedBy = AdminuserInfo.UserID;
                obj.CreatedOn = DateTime.Now;
                obj.AdminFee = 0;
                obj.BookingFee = 0;
                obj.ShippingFee = 0;
                if (hdnrfndtype.Value == "admin")
                    obj.AdminFee = Convert.ToDecimal(hdnrfndamount.Value);
                else if (hdnrfndtype.Value == "booking")
                    obj.BookingFee = Convert.ToDecimal(hdnrfndamount.Value);
                else if (hdnrfndtype.Value == "shipping")
                    obj.ShippingFee = Convert.ToDecimal(hdnrfndamount.Value);

                _masterOrder.AddRefundData(obj);
                _siteId = Master.SiteID;
                GetOrdersForEdit(order);
                divlist.Visible = false;
                divDetail.Visible = true;
                RefundEMail();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void RefundEMail()
        {
            GetCurrencyCode();
            String RCurrency = Currency + " ";
            try
            {
                _siteId = Master.SiteID;
                DateTime RefundDate = DateTime.Now;
                string PassList = string.Empty;
                var smtpClient = new SmtpClient();
                var message = new MailMessage();

                var orderID = Convert.ToInt32(Request["id"]);
                string EmailAddress = _masterOrder.GetbillingEmail(orderID);
                string BccEmailAddress = _masterOrder.GetBccEmailBySiteid(_siteId);
                string Subject = "Order Refund Confirmation #" + orderID;
                var theme = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (theme != null && (theme.IsSTA ?? false))
                    htmfile = Server.MapPath("~/Orders/BlueRefundTemplate.htm");
                else
                    htmfile = Server.MapPath("~/Orders/RefundTemplate.htm");

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml;

                //***********************************//
                body = body.Replace("##OrderNo##", orderID.ToString());
                body = body.Replace("##SA##", RCurrency + lblShippingAmount.Text);
                body = body.Replace("##BA##", RCurrency + lblBookingFee.Text);
                body = body.Replace("##AA##", RCurrency + lblAdminFee.Text);
                var data = _masterOrder.GetOrderAdminRefundById(orderID);
                if (data != null)
                {
                    body = body.Replace("##ShippingCost##", RCurrency + data.ShippingFee.ToString("F"));
                    body = body.Replace("##BookingFee##", RCurrency + data.BookingFee.ToString("F"));
                    body = body.Replace("##AdminFee##", RCurrency + data.AdminFee.ToString("F"));
                    RefundDate = data.CreatedOn;
                }
                else
                {
                    body = body.Replace("##ShippingCost##", RCurrency + "0.00");
                    body = body.Replace("##BookingFee##", RCurrency + "0.00");
                    body = body.Replace("##AdminFee##", RCurrency + "0.00");
                }
                var refundDetails = Objbooking.GetP2PRefund(Convert.ToInt64(orderID));
                if (refundDetails != null)
                {
                    foreach (var ty in refundDetails.Select((x, i) => new { Value = x, Index = i }))
                    {
                        string TravelerTblRow = "<tr><td class='left-header' colspan='2'><hr />Passenger: ##PNO## <hr /></td></tr><tr><td class='first'> PNR No: </td><td class='secound'> ##PNRNo## </td></tr><tr><td class='first'> Train No: </td><td class='secound'> ##TrainNo## </td></tr><tr><td class='first'> From: </td><td class='secound'> ##From## </td></tr><tr><td class='first'> To: </td><td class='secound'> ##To## </td></tr><tr><td class='first'> Depature Date-Time: </td><td class='secound'> ##DepatureDateTime##<span class='time'> ##DepatureTime##</span></td></tr><tr><td class='first'> Arrival Date-Time: </td><td class='secound'> ##ArrivalDateTime##<span class='time'> ##ArrivalTime##</span></td></tr><tr><td class='first'> Passenger: </td><td class='secound'> ##Passenger## </td></tr><tr><td class='first'> Class: </td><td class='secound'> ##Class## </td></tr><tr><td class='first'> Fare Name: </td><td class='secound'> ##FareName## </td></tr><tr><td class='first'> Net Price: </td><td class='secound'> ##NetPrice## </td></tr><tr><td class='first'> Refund Price: </td><td class='secound'> ##RefundPrice## </td></tr>";
                        if (RefundDate < ty.Value.RefundDateTime.Value)
                            RefundDate = ty.Value.RefundDateTime.Value;
                        TravelerTblRow = TravelerTblRow.Replace("##PNO##", (ty.Index + 1).ToString());
                        TravelerTblRow = TravelerTblRow.Replace("##TrainNo##", ty.Value.TrainNo);
                        TravelerTblRow = TravelerTblRow.Replace("##From##", ty.Value.From);
                        TravelerTblRow = TravelerTblRow.Replace("##To##", ty.Value.To);
                        TravelerTblRow = TravelerTblRow.Replace("##DepatureDateTime##", ty.Value.DateTimeDepature.Value.ToString("dd/MMM/yyyy"));
                        TravelerTblRow = TravelerTblRow.Replace("##DepatureTime##", ty.Value.DepartureTime);
                        TravelerTblRow = TravelerTblRow.Replace("##ArrivalTime##", ty.Value.ArrivalTime);
                        TravelerTblRow = TravelerTblRow.Replace("##ArrivalDateTime##", ty.Value.DateTimeArrival.Value.ToString("dd/MMM/yyyy"));
                        TravelerTblRow = TravelerTblRow.Replace("##Passenger##", ty.Value.Passenger);
                        TravelerTblRow = TravelerTblRow.Replace("##Class##", ty.Value.Class);
                        TravelerTblRow = TravelerTblRow.Replace("##FareName##", ty.Value.FareName);
                        TravelerTblRow = TravelerTblRow.Replace("##PNRNo##", ty.Value.ReservationCode);
                        TravelerTblRow = TravelerTblRow.Replace("##NetPrice##", RCurrency + ty.Value.NetPrice);
                        TravelerTblRow = TravelerTblRow.Replace("##RefundPrice##", RCurrency + ty.Value.TotalProductRefund.Value);
                        PassList += TravelerTblRow;
                    }
                }
                body = body.Replace("##BindPassTravelerRefund##", PassList);
                body = body.Replace("##RefundDate##", RefundDate.ToString("dd/MMM/yyyy"));
                body = body.Replace("#Blanck#", "&nbsp;");
                //**********************************//

                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(_siteId);
                if (result != null && !string.IsNullOrEmpty(EmailAddress))
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(EmailAddress);
                    if (!string.IsNullOrEmpty(BccEmailAddress))
                        message.Bcc.Add(BccEmailAddress);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to send E-mail Refund confimation!");
            }
        }
        #endregion

        #region OrderAttachment
        protected void btnAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                var filesList = UploadFile();
                if (fupAttachment.HasFile)
                {
                    if (filesList != null && filesList.Count > 0)
                    {
                        _masterOrder.AddMultipleAttachments(filesList);
                    }
                }
                BindAttachments();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindAttachments()
        {
            try
            {

                grdOrderAttachments.DataSource = _masterOrder.GetOrderAttachments(Convert.ToInt32(Request["id"]));
                grdOrderAttachments.DataBind();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void grdOrderAttachments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] val = e.CommandArgument.ToString().Split(',');
                if (e.CommandName == "Remove")
                {
                    string path = Server.MapPath(val[1]);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    _masterOrder.RemoveAttachments(Convert.ToInt32(val[0]));
                    BindAttachments();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        List<tblOrderAttachment> UploadFile()
        {
            try
            {
                List<tblOrderAttachment> _ManageMultipleFiles = new List<tblOrderAttachment>();
                if (fupAttachment.HasFile)
                {
                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 10)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('You can not upload more than 10 files.')</script>", false);
                        return null;
                    }
                    else
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            string path = "";
                            Guid id = Guid.NewGuid();
                            HttpPostedFile uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            if (uploadfile.ContentLength > 2097152)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('Uploaded product file is larger up to 5Mb.')</script>", false);
                                return null;
                            }
                            else
                            {
                                string fExt = uploadfile.FileName.Substring(uploadfile.FileName.LastIndexOf("."));
                                path = "../Uploaded/OrderAttachments/" + id + fExt;
                                if (System.IO.File.Exists(Server.MapPath(path)))
                                    System.IO.File.Delete(Server.MapPath(path));
                                else
                                {
                                    uploadfile.SaveAs(Server.MapPath(path));
                                    string dircPath = Server.MapPath(path);
                                }
                                _ManageMultipleFiles.Add(new tblOrderAttachment
                                {
                                    OrderID = Convert.ToInt32(Request["id"]),
                                    AttachmentName = uploadfile.FileName.Substring(0, uploadfile.FileName.LastIndexOf(".")),
                                    AttachmentPath = path
                                });
                            }
                        }
                    }
                }

                if (_ManageMultipleFiles != null && _ManageMultipleFiles.Count > 0)
                    return _ManageMultipleFiles;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetFileExtension(string filepath)
        {
            try
            {
                if (string.IsNullOrEmpty(filepath))
                    return "unknown.png";

                string p = filepath.Substring(filepath.LastIndexOf('.'));
                return p.Replace(".", "") + ".png";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Edit Booking Fee

        protected void lnkeditbooking_Click(object sender, EventArgs e)
        {
            txtbookingEdit.Text = lblBookingFee.Text;
            mpBookingEdit.Show();
            divlist.Visible = false;
            divDetail.Visible = true;
        }

        protected void btnEditBookingUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var OrderID = Convert.ToInt32(Request["id"]);
                if (!string.IsNullOrEmpty(txtbookingEdit.Text))
                    _masterOrder.UpdateBookingFeeByOrderId(OrderID, Convert.ToDecimal(txtbookingEdit.Text));
                GetOrdersForEdit(OrderID);
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #endregion

        protected void BtnRefundMailSend_Click(object sender, EventArgs e)
        {
            try
            {
                _siteId = Master.SiteID;
                GetCurrencyCode();
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    var orderID = Convert.ToInt32(Request["id"]);
                    string Subject = "Refund Confirmation #" + orderID;
                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/OrderRefundMailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(_siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(_siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml;

                    var refundDetails = Objbooking.GetP2PRefund(orderID).ToList();
                    if (refundDetails != null && refundDetails.Count > 0)
                    {
                        string UserName = "", strProductDesc = "";
                        GetReceiptLoga();
                        var billingDetails = Objbooking.GetBillingShippingAddress(orderID);
                        if (billingDetails != null)
                            UserName = (!string.IsNullOrEmpty(billingDetails.TitleShpg) ? billingDetails.TitleShpg : billingDetails.Title) + " " + (!string.IsNullOrEmpty(billingDetails.FirstNameShpg) ? billingDetails.FirstNameShpg : billingDetails.FirstName) + " " + (!string.IsNullOrEmpty(billingDetails.LastNameShpg) ? billingDetails.LastNameShpg : billingDetails.LastName);

                        bool isAgentSite = st.IsAgent.Value;
                        GetSiteType(orderID);
                        foreach (var item in refundDetails)
                        {
                            var ProductDesc = Objbooking.getP2PDetailsForEmail(item.PassSaleId, null, "", isEvolviBooking, isAgentSite, "");
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                            strProductDesc += ProductDesc.Replace("##Price##", Currency + " -" + item.TotalProductRefund.ToString()).Replace("##Name##", item.TravellerName).Replace("##NetAmount##", "");

                            if (item.Terms != null)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + HttpContext.Current.Server.HtmlDecode(item.Terms) + "</td></tr>";
                            if (isEvolviBooking)
                            {
                                var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == item.PassSaleId);
                                if (P2PData != null)
                                {
                                    if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                    {
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>";
                                        foreach (var pitem in P2PData.EvSleeper.Split('@'))
                                        {
                                            string ss = pitem.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                            strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                        }
                                        strProductDesc += "</td></tr>";
                                    }
                                }
                            }
                            strProductDesc += "</table></td></tr></table>";
                        }
                        body = body.Replace("##headerstyle##", SiteHeaderColor);
                        body = body.Replace("##sitelogo##", logo);
                        body = body.Replace("##SiteUrl##", st.SiteURL);
                        body = body.Replace("##OrderNumber##", orderID.ToString());
                        body = body.Replace("##UserName##", UserName);
                        body = body.Replace("##RefundDate##", refundDetails.FirstOrDefault().RefundDateTime.Value.ToString("dd/MM/yyyy"));
                        body = body.Replace("##OrderDetails##", strProductDesc);
                        body = body.Replace("##DeliveryAddress##", "");

                        decimal Refund = refundDetails.Sum(x => x.TotalProductRefund.Value);
                        decimal CancellationFee = refundDetails.Sum(x => x.CancellationFee.Value);
                        decimal totalRefund = Refund - CancellationFee;
                        body = body.Replace("##RefundPrice##", Currency + " -" + Refund.ToString("F2"));
                        body = body.Replace("##CancellationFee##", Currency + " " + CancellationFee.ToString("F2"));
                        body = body.Replace("##TotalRefund##", Currency + " -" + totalRefund.ToString("F2"));

                        /*Get smtp details*/
                        var billing = _masterOrder.GetBillingInfo(orderID);
                        if (billing.Any())
                        {
                            var firstOrDefault = billing.FirstOrDefault();
                            if (firstOrDefault != null)
                            {
                                billingEmailAddress = firstOrDefault.EmailAddress;
                                shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                            }
                        }
                        var result = _masterPage.GetEmailSettingDetail(_siteId);
                        if (result != null)
                        {
                            var fromAddres = new MailAddress(result.Email, result.Email);
                            smtpClient.Host = result.SmtpHost;
                            smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                            smtpClient.UseDefaultCredentials = true;
                            smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                            smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                            message.From = fromAddres;
                            message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                            message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                            message.Subject = Subject;
                            message.IsBodyHtml = true;
                            message.Body = body;
                            smtpClient.Send(message);
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                        }
                    }
                }
                GetOrdersForEdit(Convert.ToInt32(Request["id"]));
                mupTraveller.Hide();
                divlist.Visible = false;
                divDetail.Visible = true;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

    }
}