﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Business;
using System.Web;

namespace IR_Admin.Orders
{
    public partial class RefundReceipt : System.Web.UI.Page
    {
        private readonly ManageOrder _master = new ManageOrder();
        private readonly ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
        private readonly ManageBooking Objbooking = new ManageBooking();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteLogo = "", HtmlFileSitelog = "", Name = "", SiteHeaderColor = "", SiteUrl = "", UserName = "", strProductDesc = "";
        public string Currency = "$";
        long OrderId;
        Guid SiteId;
        public bool isEvolviBooking = false; public bool isNTVBooking = false; public bool isBene = false; public bool isTI = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (AdminuserInfo.RoleId != new Guid())
            {
                if (Request.QueryString["id"] != null)
                {
                    OrderId = Convert.ToInt64(Request.QueryString["id"]);
                    if (!Page.IsPostBack)
                        FillOrderInfo(OrderId);
                }
                else
                    Response.Redirect("~/DashBoard.aspx");
            }
            else
                Response.Redirect("~/Default.aspx");
        }

        public void FillOrderInfo(long OrderId)
        {
            try
            {
                var result = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (result != null)
                {
                    if (!string.IsNullOrEmpty(result.TrvType) && result.TrvType.ToLower() == "pass")
                    {
                        var prdPass = _master.GetPassSale(OrderId); //calculate price
                        var Datalist = _master.GetRefundDetailsByOrderId(OrderId);
                        var refundDetails = Datalist.Select(x => new
                        {
                            ProductID = x.ProductID,
                            ProductName = x.ProductName,
                            ProductValidUpToName = x.ProductValidUpToName,
                            TravellerType = x.TravellerType,
                            Class = x.Class,
                            TravellerName = x.TravellerName,
                            User = x.User,
                            Price = x.Price,
                            Refund = x.Refund,
                            RefundDate = (DateTime)x.RefundDate,
                            PassSaleID = x.PassSaleID,
                            PassStartDate = (DateTime)x.PassStartDate,
                            TicketProtection = x.TicketProtection,
                            CancellationFee = x.CancellationFee,
                            SiteId = x.SiteId,
                            SiteName = x.SiteName,
                            SiteUrl = x.SiteUrl,
                            OrderId = x.OrderId,
                            IsAgentSite = x.IsAgentSite,
                            StockNo = prdPass.FirstOrDefault(v => v.ID == x.PassSaleID).StockNumber,
                        }).OrderBy(ty => ty.StockNo).ToList();

                        if (refundDetails != null && refundDetails.Count > 0)
                        {
                            GetCurrencyCode();
                            SiteId = refundDetails.FirstOrDefault().SiteId;
                            HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(SiteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(SiteId));
                            GetReceiptLoga();
                            var billingDetails = Objbooking.GetBillingShippingAddress(OrderId);
                            if (billingDetails != null)
                                UserName = (!string.IsNullOrEmpty(billingDetails.TitleShpg) ? billingDetails.TitleShpg : billingDetails.Title) + " " + (!string.IsNullOrEmpty(billingDetails.FirstNameShpg) ? billingDetails.FirstNameShpg : billingDetails.FirstName) + " " + (!string.IsNullOrEmpty(billingDetails.LastNameShpg) ? billingDetails.LastNameShpg : billingDetails.LastName);

                            SiteUrl = refundDetails.FirstOrDefault().SiteUrl;
                            td_OrderNumber.InnerText = refundDetails.FirstOrDefault().OrderId.ToString();
                            td_RefundDate.InnerText = refundDetails.FirstOrDefault().RefundDate.ToString("dd/MM/yyyy");
                            td_UserName.InnerText = UserName;

                            bool isAgentSite = refundDetails.FirstOrDefault().IsAgentSite;
                            foreach (var item in refundDetails)
                            {
                                var ProductDesc = Objbooking.getPassDetailsForEmail(item.PassSaleID, null, "", "");
                                string nettPrice = Currency + " " + (_master.GetEurailAndNonEurailPrice(item.PassSaleID)).ToString("F");
                                strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                                strProductDesc += ProductDesc.Replace("##Price##", Currency + " -" + item.Refund.ToString("F2")).Replace("##Name##", item.TravellerName).Replace("##NetAmount##", "");
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + (item.PassStartDate != null ? item.PassStartDate.ToString("dd/MMM/yyyy") : "") + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                                strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>" : "");

                                string ElectronicNote = Objbooking.GetElectronicNote(item.PassSaleID);
                                if (ElectronicNote.Length > 0)
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + ElectronicNote + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";

                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" +
                                    (item.TicketProtection > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>" + Currency + " 0.00</strong></td></tr>";
                                //(item.TicketProtection > 0 ? "Yes" : "No") + "</td><td width='20%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>" + Currency + " " + Convert.ToString(item.TicketProtection) + "</strong></td></tr>";
                                strProductDesc += "</table></td></tr></table>";
                            }
                            decimal Refund = refundDetails.Sum(x => x.Refund);
                            decimal CancellationFee = refundDetails.Sum(x => x.CancellationFee);
                            decimal totalRefund = Refund - CancellationFee;

                            td_RefundPrice.InnerHtml = "-" + Refund.ToString("F2");
                            td_CancellationFee.InnerHtml = CancellationFee.ToString("F2");
                            td_TotalRefund.InnerHtml = "-" + totalRefund.ToString("F2");
                        }
                        lnkgotoback.Attributes.Add("href", "OrderDetails.aspx?id=" + OrderId);
                    }
                    else if (!string.IsNullOrEmpty(result.TrvType) && result.TrvType.ToLower() == "p2p")
                    {
                        SiteId = result.SiteID.Value;
                        GetCurrencyCode();
                        var st = _db.tblSites.FirstOrDefault(x => x.ID == SiteId);
                        if (st != null)
                        {
                            var refundDetails = Objbooking.GetP2PRefund(OrderId).ToList();
                            if (refundDetails != null && refundDetails.Count > 0)
                            {
                                string SiteName = st.SiteURL + "Home";
                                var orderID = Convert.ToInt32(Request["id"]);
                                HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(SiteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(SiteId));
                                GetReceiptLoga();
                                var billingDetails = Objbooking.GetBillingShippingAddress(OrderId);
                                if (billingDetails != null)
                                    UserName = (!string.IsNullOrEmpty(billingDetails.TitleShpg) ? billingDetails.TitleShpg : billingDetails.Title) + " " + (!string.IsNullOrEmpty(billingDetails.FirstNameShpg) ? billingDetails.FirstNameShpg : billingDetails.FirstName) + " " + (!string.IsNullOrEmpty(billingDetails.LastNameShpg) ? billingDetails.LastNameShpg : billingDetails.LastName);

                                SiteUrl = st.SiteURL;
                                td_OrderNumber.InnerText = OrderId.ToString();
                                td_RefundDate.InnerText = refundDetails.FirstOrDefault().RefundDateTime.Value.ToString("dd/MM/yyyy");
                                td_UserName.InnerText = UserName;

                                bool isAgentSite = st.IsAgent.Value;
                                GetSiteType(orderID);
                                foreach (var item in refundDetails)
                                {
                                    var ProductDesc = Objbooking.getP2PDetailsForEmail(item.PassSaleId, null, "", isEvolviBooking, isAgentSite, "");
                                    strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                                    strProductDesc += ProductDesc.Replace("##Price##", Currency + " -" + item.TotalProductRefund.ToString()).Replace("##Name##", item.TravellerName).Replace("##NetAmount##", "");

                                    if (item.Terms != null)
                                        strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + HttpContext.Current.Server.HtmlDecode(item.Terms) + "</td></tr>";
                                    if (isEvolviBooking)
                                    {
                                        var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == item.PassSaleId);
                                        if (P2PData != null)
                                        {
                                            if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                            {
                                                strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>";
                                                foreach (var pitem in P2PData.EvSleeper.Split('@'))
                                                {
                                                    string ss = pitem.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                                    strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                                }
                                                strProductDesc += "</td></tr>";
                                            }
                                        }
                                    }
                                    strProductDesc += "</table></td></tr></table>";
                                }
                                decimal Refund = refundDetails.Sum(x => x.TotalProductRefund.Value);
                                decimal CancellationFee = refundDetails.Sum(x => x.CancellationFee.Value);
                                decimal totalRefund = Refund - CancellationFee;

                                td_RefundPrice.InnerHtml = "-" + Refund.ToString("F2");
                                td_CancellationFee.InnerHtml = CancellationFee.ToString("F2");
                                td_TotalRefund.InnerHtml = "-" + totalRefund.ToString("F2");
                            }
                        }
                        lnkgotoback.Attributes.Add("href", "P2POrderDetails.aspx?id=" + OrderId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetSiteType(long OrderID)
        {
            var apiTypeList = new ManageBooking().GetAllApiType(OrderID);
            if (apiTypeList != null)
            {
                isEvolviBooking = apiTypeList.isEvolvi;
                isNTVBooking = apiTypeList.isNTV;
                isBene = apiTypeList.isBene;
                isTI = apiTypeList.isTI;
            }
        }

        public void GetReceiptLoga()
        {
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    SiteLogo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    SiteLogo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    SiteLogo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    SiteLogo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        public void GetCurrencyCode()
        {
            try
            {
                if (Request["id"] == null)
                    return;
                FrontEndManagePass oManageClass = new FrontEndManagePass();
                Int32 orderno = Convert.ToInt32(Request["id"]);
                tblOrder objtblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderno);
                if (objtblord != null)
                {
                    var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == objtblord.SiteID);
                    if (currencyID != null)
                        Currency = oManageClass.GetCurrency((Guid)currencyID.DefaultCurrencyID);
                }
            }
            catch (Exception ex) { throw ex; }
        }

    }
}