﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="OrderRefund.aspx.cs"
    Inherits="IR_Admin.Orders.OrderRefund" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            if (parseFloat($("#MainContent_lblalreadyrefund").text()) > 0) {
                $("#MainContent_ddlRefundPerc").val("f46909c3-dece-421f-85ad-f7d80b69d278");
                GetMaxRefund();
            }
        });

        function GetMaxRefund(extra) {
            $("#MainContent_DivError,#msgerr").hide();
            if (extra == undefined) {
                $('#MainContent_txtAdditionalRefund').val('0.00');
            }
            else {
                $("#MainContent_lblRefundPrice").text("0.00");
                $("#MainContent_ddlRefundPerc").val("f46909c3-dece-421f-85ad-f7d80b69d278");
            }

            var price = parseFloat($("#MainContent_lblPrice").text());
            var refund = parseInt($("#MainContent_ddlRefundPerc  option:selected").text().replace('%', '').replace("-Select Refund-", "0"));
            if ($("#hdnRefundType").val() == "false" && refund > 85) {
                $("#msgerr").show();
                $("#MainContent_lblRefundPrice").text("0.00");
                $("#hdnrefund").val("0.00");
                $("#MainContent_ddlRefundPerc").val("-1");
                $("#MainContent_DivError").show().text("you need to select 85% as the ‘percentage to be refunded’.");
                return false;
            }
            else if ($("#MainContent_txtAdditionalRefund").val() != undefined) {
                var oldrefund = parseFloat($("#MainContent_lblalreadyrefund").text());
                var refundamunt = parseFloat($("#MainContent_txtAdditionalRefund").val());
                if (extra == undefined) {
                    var amount = (parseFloat(price) * parseFloat(refund)) / 100;
                    $("#MainContent_lblRefundPrice").text(amount.toFixed(2));
                    $("#hdnrefund").val(amount.toFixed(2));
                    if ((oldrefund + amount) > price) {
                        $("#MainContent_ddlRefundPerc").val("f46909c3-dece-421f-85ad-f7d80b69d278");
                        $("#MainContent_DivError").show().text("Refunded amount greater than total price.");
                        $("#MainContent_lblRefundPrice").text("0.00");
                        $("#hdnrefund").val("0.00");
                        return false;
                    }
                }
                else if ((oldrefund + refundamunt) > price) {
                    $('#MainContent_txtAdditionalRefund').val('0.00');
                    $("#MainContent_DivError").show().text("Refunded amount greater than total price.");
                    return false;
                }
            }
            else {
                var amount = (parseFloat(price) * parseFloat(refund)) / 100;
                $("#MainContent_lblRefundPrice").text(amount.toFixed(2));
                $("#hdnrefund").val(amount.toFixed(2));
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnrefund" ClientIDMode="Static" runat="server" Value="0.00" />
    <asp:HiddenField ID="hdnRefundType" ClientIDMode="Static" runat="server" Value="false" />
    <h2>
        Order Refund</h2>
    <div class="full mr-tp1">
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="panes">
                <div id="divDetail" runat="server">
                    <table class="tblMainSection">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <div class="cat-inner-ord">
                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="3" class="heading-color">
                                                    <strong>Order id: </strong>
                                                    <asp:Label ID="lblOrderID" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%">
                                                    <strong>Product: </strong>
                                                    <asp:Label ID="lblProduct" runat="server" />
                                                </td>
                                                <td>
                                                    <strong>Traveller: </strong>
                                                    <asp:Label ID="lblTraveller" runat="server" />
                                                </td>
                                                <td>
                                                    <strong>Price: </strong>
                                                    <%=Currency%><asp:Label ID="lblPrice" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><strong>Refund</strong></legend>
                                    <div class="cat-inner-ord">
                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <strong>Select the percentage to be refunded:</strong>
                                                    <asp:DropDownList ID="ddlRefundPerc" runat="server" Width="120px" onchange="GetMaxRefund()" />
                                                    <span style="padding-left: 5px; font-weight: bold">
                                                        <%=Currency%>
                                                        <asp:Label ID="lblRefundPrice" runat="server" Text="0.00" /></span>
                                                    <div class="clear">
                                                    </div>
                                                    <span style="color: Red; display: none;" id="msgerr">Please select a refund percentage.</span>
                                                </td>
                                                <td colspan="2">
                                                    <asp:Button ID="btnCancel" Text="Cancel Refund" CssClass="button" runat="server"
                                                        OnClick="btnCancel_Click" />
                                                    <asp:Button ID="btnRefund" Text="Refund" CssClass="button" runat="server" OnClientClick="return GetMaxRefund()"
                                                        OnClick="btnRefund_Click" ValidationGroup="vs" />
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vs"
                                                        ShowSummary="False" ShowMessageBox="True" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:Label runat="server" ID="lblrefundnote"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="rowcancellation" runat="server">
                                                <td colspan="3">
                                                    Refund is a cancellation
                                                    <asp:CheckBox ID="chkCancel" runat="server" />
                                                </td>
                                            </tr>
                                            <tr id="tr_alreadyrefund" runat="server" visible="false">
                                                <td colspan="3">
                                                    Already refunded amount:
                                                    <%=Currency%>
                                                    <asp:Label ID="lblalreadyrefund" runat="server" Text="0.0"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="tr_AdditionalRefund" runat="server" visible="false">
                                                <td colspan="3">
                                                    Write value of the refund:
                                                    <asp:TextBox ID="txtAdditionalRefund" runat="server" Style="margin-left: 20px;" onblur="return GetMaxRefund('extra');" />
                                                    <asp:Button ID="btnCancel2" Text="Cancel Refund" CssClass="button" runat="server"
                                                        OnClick="btnCancel_Click" />
                                                    <asp:Button ID="btnRefund2" Text="Refund" CssClass="button" runat="server" OnClick="btnRefund2_Click"
                                                        OnClientClick="return GetMaxRefund('extra')" />
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td colspan="2">
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAdditionalRefund" Text="Additional Refund" Enabled="false" CssClass="button"
                                                        runat="server" Style="margin-left: 20px;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
