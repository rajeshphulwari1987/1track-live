﻿#region Using
using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.Web.UI;
#endregion

namespace IR_Admin.Orders
{
    public partial class P2POrderRefund : System.Web.UI.Page
    {
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId, _prdCurrencyId, _siteCurrencyId;
        public string Currency = "$";
        private int _tckProtection;
        private decimal _netPrice;
        private bool _isCancel;
        private string htmfile = string.Empty;
        private readonly Masters _masterPage = new Masters();
        readonly private ManageOrder _master = new ManageOrder();
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["OrdId"] != null && Request.QueryString["tpId"] != null)
                {
                    var OrdId = Request.QueryString["OrdId"];
                    var tpId = Request.QueryString["tpId"];

                    bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                    if (IsFullRefund)
                        rowcancellation.Visible = false;
                    GetOrdersForRefund(Convert.ToInt64(OrdId), Guid.Parse(tpId));
                    BindPercentage(Guid.Parse(tpId));
                }
            }
        }

        public void GetOrdersForRefund(long ordId, Guid PassP2PSaleId)
        {
            try
            {
                Boolean ISBENE = false;
                List<GetP2PSaleData> lst = new ManageBooking().GetP2PSaleDetail(ordId).OrderByDescending(t => t.ReservationCode).ToList();
                var lstdata = lst.Where(a => a.PassP2PSaleID == PassP2PSaleId).ToList();
                if (lst.Count() > 0 && lstdata.Count() > 0)
                {
                    ISBENE = lst.AsEnumerable().Any(x => (string.IsNullOrEmpty(x.ReservationCode) ? 0 : x.ReservationCode.Length) == 7);
                    if (ISBENE)
                    {
                        lblPrice.Text = lst.Sum(t => t.NetPrice).ToString();
                        ViewState["tckProtection"] = lst.Sum(t => t.TicketProtection);
                    }
                    else
                    {
                        lblPrice.Text = lstdata.FirstOrDefault().NetPrice.ToString();
                        ViewState["tckProtection"] = lstdata.FirstOrDefault().TicketProtection.HasValue ? lstdata.FirstOrDefault().TicketProtection : 0;
                    }
                    lblOrderID.Text = lstdata.FirstOrDefault().OrderID.ToString();
                    Guid SiteId = Guid.Parse(lstdata.FirstOrDefault().SiteID.ToString());
                    Currency = _master.GetCurrcyBySiteId(SiteId);
                    ViewState["SiteID"] = SiteId;
                    ViewState["Currency"] = Currency;
                    ViewState["prdCurrencyId"] = Guid.Parse(lstdata.FirstOrDefault().CurrencyId.ToString());

                    var datalist = lstdata.Select(t => new GetP2PSaleData
                    {
                        OrderID = t.OrderID,
                        TrainNo = t.TrainNo,
                        From = t.From,
                        To = t.To,
                        DateTimeDepature = t.DateTimeDepature,
                        DateTimeArrival = t.DateTimeArrival,
                        Passenger = t.Passenger,
                        Symbol = t.Symbol,
                        NetPrice = Convert.ToDecimal(lblPrice.Text),
                        Class = t.Class,
                        FareName = t.FareName,
                        ReservationCode = t.ReservationCode
                    }).ToList();
                    rptJourney.DataSource = datalist;
                    rptJourney.DataBind();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message + "::" + ex.StackTrace); }
        }

        public void BindPercentage(Guid prdId)
        {
            try
            {
                var rPerc = _master.BindRefundPerc();
                ddlRefundPerc.DataSource = rPerc;
                ddlRefundPerc.DataTextField = "Description";
                ddlRefundPerc.DataValueField = "ID";
                ddlRefundPerc.DataBind();
                ddlRefundPerc.Items.Insert(0, new ListItem("-Select Refund-", "-1"));
                bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                if (IsFullRefund)
                {
                    ddlRefundPerc.SelectedValue = "808fdb35-98ef-4428-966c-53bc27fc3093";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "checkRefundfee", "checkRefundfee();", true);
                    tr_AdditionalRefund.Visible = true;
                }
                else
                {
                    ddlRefundPerc.SelectedIndex = 86;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "checkRefundfee", "checkRefundfee();", true);
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message + "::" + ex.StackTrace); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("P2POrderDetails.aspx?id=" + lblOrderID.Text);
        }

        protected void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblOrderID.Text != null)
                {
                    _siteId = Master.SiteID;
                    long ordId = Convert.ToInt64(lblOrderID.Text);
                    new ManageBooking().UpdateOrderStatus(9, Convert.ToInt64(ordId));
                    var ord = _master.GetOrderbyOrdId(ordId);
                    var prdId = Guid.Parse(Request.QueryString["tpId"]);
                    var price = Convert.ToDecimal(lblPrice.Text);
                    var refundedPrice = Convert.ToDecimal(hdnRefundPrice.Value);
                    var cancelFee = Convert.ToDecimal(15.00);
                    string prot = ViewState["tckProtection"].ToString().Trim();
                    _tckProtection = Convert.ToInt32(Convert.ToDecimal(prot));
                    _prdCurrencyId = (Guid)ViewState["prdCurrencyId"];

                    var sID = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    if (sID != null)
                        _siteCurrencyId = Guid.Parse(sID.DefaultCurrencyID.ToString());
                    _netPrice = Convert.ToDecimal(price);

                    var perc = _master.GetRefundValue(Guid.Parse(ddlRefundPerc.SelectedValue));
                    decimal refundedPerc = Convert.ToInt32(perc.PercentageRefund);
                    if (chkCancel.Checked)
                    {
                        _isCancel = true;
                        refundedPrice = ((price + 15) * refundedPerc) / 100;
                    }
                    bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                    if (IsFullRefund)
                    {
                        cancelFee = 0;
                        refundedPerc = refundedPrice * 100 / Convert.ToDecimal(lblPrice.Text);
                    }
                    if (chkCancel.Checked && perc.PercentageRefund >= 85 && IsFullRefund == false)
                        ShowMessage(2, "With 15% cancellation charge, you need to select 85% as the percentage to be refunded");
                    else
                    {
                        _master.AddRefund(new tblOrderRefund
                        {
                            ID = Guid.NewGuid(),
                            OrderID = ordId,
                            UserID = ord.UserID,
                            AdminUserID = AdminuserInfo.UserID,
                            ProductID = prdId,
                            ProductPriceID = prdId,
                            OrginalProductAmount = price,
                            OrginalProductAmountCurrencyID = _prdCurrencyId,
                            LocalProductAmount = _netPrice,
                            LocalProductAmountCurrencyId = _siteCurrencyId,
                            DateTimeStamp = ord.CreatedOn,
                            TicketProtection = _tckProtection,
                            RefundDateTime = DateTime.Now,
                            Status = 2,
                            Cancellation = _isCancel,
                            TotalProductRefund = refundedPrice,
                            TotalProductRefundPercentage = refundedPerc,
                            CancellationFee = cancelFee,
                            Partial = false,
                            PassProtectionRefunded = true
                        });

                        if (perc.PercentageRefund == Convert.ToDecimal(100))
                        {
                            string url = _master.GetPdfFileURL(prdId);
                            if (url != null)
                            {
                                int length = url.LastIndexOf("/");
                                string FullURL = url.Substring(0, (length + 1));
                                string filename = url.Replace(FullURL, string.Empty);
                                string LocalDirectoryPath = ConfigurationManager.AppSettings["P2PTickerURl"];
                                string FilePath = LocalDirectoryPath + filename;
                                string RenameFile = Guid.NewGuid().ToString() + filename;
                                if (File.Exists(FilePath))
                                    File.Move(FilePath, FilePath.ToString().Replace(filename, RenameFile));
                            }
                        }
                        Response.Redirect("P2POrderDetails.aspx?id=" + lblOrderID.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void RefundEMail()
        {
            try
            {
                Currency = _master.GetCurrcyBySiteId(Guid.Parse(ViewState["SiteID"].ToString()));
                String RCurrency = Currency + " ";
                _siteId = Master.SiteID;
                DateTime RefundDate = DateTime.Now;
                string PassList = string.Empty;
                var smtpClient = new SmtpClient();
                var message = new MailMessage();

                var orderID = Convert.ToInt32(lblOrderID.Text);
                string EmailAddress = _master.GetbillingEmail(orderID);
                string BccEmailAddress = _master.GetBccEmailBySiteid(_siteId);
                string Subject = "Order Refund Confirmation #" + orderID;

                var theme = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (theme != null && (theme.IsSTA ?? false))
                    htmfile = Server.MapPath("~/Orders/BlueRefundTemplate.htm");
                else
                    htmfile = Server.MapPath("~/Orders/RefundTemplate.htm");

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml;

                //***********************************//
                var Orderdata = _master.GetOrderbyOrdId(orderID);
                if (Orderdata != null)
                {
                    body = body.Replace("##OrderNo##", orderID.ToString());
                    body = body.Replace("##SA##", RCurrency + Orderdata.ShippingAmount ?? "0.00");
                    body = body.Replace("##BA##", RCurrency + Orderdata.BookingFee ?? "0.00");
                    body = body.Replace("##AA##", RCurrency + Orderdata.AdminFee);
                    var data = _master.GetOrderAdminRefundById(orderID);
                    if (data != null)
                    {
                        body = body.Replace("##ShippingCost##", RCurrency + data.ShippingFee.ToString("F"));
                        body = body.Replace("##BookingFee##", RCurrency + data.BookingFee.ToString("F"));
                        body = body.Replace("##AdminFee##", RCurrency + data.AdminFee.ToString("F"));
                        RefundDate = data.CreatedOn;
                    }
                    else
                    {
                        body = body.Replace("##ShippingCost##", RCurrency + "0.00");
                        body = body.Replace("##BookingFee##", RCurrency + "0.00");
                        body = body.Replace("##AdminFee##", RCurrency + "0.00");
                    }
                    var refundDetailsP2P = new ManageBooking().GetP2PRefund(Convert.ToInt64(orderID));
                    if (Orderdata.TrvType == "P2P" && refundDetailsP2P != null)
                    {
                        foreach (var ty in refundDetailsP2P.Select((x, i) => new { Value = x, Index = i }))
                        {
                            string TravelerTblRow = "<tr><td class='left-header' colspan='2'><hr />Passenger: ##PNO## <hr /></td></tr><tr><td class='first'> PNR No: </td><td class='secound'> ##PNRNo## </td></tr><tr><td class='first'> Train No: </td><td class='secound'> ##TrainNo## </td></tr><tr><td class='first'> From: </td><td class='secound'> ##From## </td></tr><tr><td class='first'> To: </td><td class='secound'> ##To## </td></tr><tr><td class='first'> Depature Date-Time: </td><td class='secound'> ##DepatureDateTime##<span class='time'> ##DepatureTime##</span></td></tr><tr><td class='first'> Arrival Date-Time: </td><td class='secound'> ##ArrivalDateTime##<span class='time'> ##ArrivalTime##</span></td></tr><tr><td class='first'> Passenger: </td><td class='secound'> ##Passenger## </td></tr><tr><td class='first'> Class: </td><td class='secound'> ##Class## </td></tr><tr><td class='first'> Fare Name: </td><td class='secound'> ##FareName## </td></tr><tr><td class='first'> Net Price: </td><td class='secound'> ##NetPrice## </td></tr><tr><td class='first'> Refund Price: </td><td class='secound'> ##RefundPrice## </td></tr>";
                            if (RefundDate < ty.Value.RefundDateTime.Value)
                                RefundDate = ty.Value.RefundDateTime.Value;
                            TravelerTblRow = TravelerTblRow.Replace("##PNO##", (ty.Index + 1).ToString());
                            TravelerTblRow = TravelerTblRow.Replace("##TrainNo##", ty.Value.TrainNo);
                            TravelerTblRow = TravelerTblRow.Replace("##From##", ty.Value.From);
                            TravelerTblRow = TravelerTblRow.Replace("##To##", ty.Value.To);
                            TravelerTblRow = TravelerTblRow.Replace("##DepatureDateTime##", ty.Value.DateTimeDepature.Value.ToString("dd/MMM/yyyy"));
                            TravelerTblRow = TravelerTblRow.Replace("##DepatureTime##", ty.Value.DepartureTime);
                            TravelerTblRow = TravelerTblRow.Replace("##ArrivalTime##", ty.Value.ArrivalTime);
                            TravelerTblRow = TravelerTblRow.Replace("##ArrivalDateTime##", ty.Value.DateTimeArrival.Value.ToString("dd/MMM/yyyy"));
                            TravelerTblRow = TravelerTblRow.Replace("##Passenger##", ty.Value.Passenger);
                            TravelerTblRow = TravelerTblRow.Replace("##Class##", ty.Value.Class);
                            TravelerTblRow = TravelerTblRow.Replace("##FareName##", ty.Value.FareName);
                            TravelerTblRow = TravelerTblRow.Replace("##PNRNo##", ty.Value.ReservationCode);
                            TravelerTblRow = TravelerTblRow.Replace("##NetPrice##", RCurrency + ty.Value.NetPrice);
                            TravelerTblRow = TravelerTblRow.Replace("##RefundPrice##", RCurrency + (ty.Value.TotalProductRefund.Value.ToString("F")));
                            PassList += TravelerTblRow;
                        }
                    }
                }
                body = body.Replace("##BindPassTravelerRefund##", PassList);
                body = body.Replace("##RefundDate##", RefundDate.ToString("dd/MMM/yyyy"));
                body = body.Replace("#Blanck#", "&nbsp;");
                //**********************************//

                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(_siteId);
                if (result != null && !string.IsNullOrEmpty(EmailAddress))
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(EmailAddress);
                    if (!string.IsNullOrEmpty(BccEmailAddress))
                        message.Bcc.Add(BccEmailAddress);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to send E-mail Refund confimation!");
            }
        }

        protected void btnRefund2_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblOrderID.Text != null && !string.IsNullOrEmpty(txtAdditionalRefund.Text))
                {
                    bool isvalid = true;
                    byte StatusId = 1;
                    _siteId = Master.SiteID;
                    long ordId = Convert.ToInt64(lblOrderID.Text);
                    new ManageBooking().UpdateOrderStatus(9, Convert.ToInt64(ordId));
                    var ord = _master.GetOrderbyOrdId(ordId);
                    var prdId = Guid.Parse(Request.QueryString["tpId"]);
                    var price = Convert.ToDecimal(lblPrice.Text);
                    var refundedPrice = Convert.ToDecimal(txtAdditionalRefund.Text);
                    var cancelFee = Convert.ToDecimal(15.00);
                    _tckProtection = Convert.ToInt32(Convert.ToDecimal(ViewState["tckProtection"].ToString()));
                    _prdCurrencyId = (Guid)ViewState["prdCurrencyId"];
                    var sID = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    if (sID != null)
                        _siteCurrencyId = Guid.Parse(sID.DefaultCurrencyID.ToString());
                    _netPrice = Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(price, _siteId, _prdCurrencyId, _siteCurrencyId).ToString("F"));
                    var perc = _master.GetRefundValue(Guid.Parse(ddlRefundPerc.SelectedValue));
                    decimal refundedPerc = 0;
                    bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                    if (IsFullRefund)
                    {
                        StatusId = 2;
                        cancelFee = 0;
                        refundedPerc = refundedPrice * 100 / Convert.ToDecimal(lblPrice.Text);
                    }
                    if (isvalid)
                    {
                        tblOrderRefund _OrderRefund = new tblOrderRefund
                        {
                            ID = Guid.NewGuid(),
                            OrderID = ordId,
                            UserID = ord.UserID,
                            AdminUserID = AdminuserInfo.UserID,
                            ProductID = prdId,
                            ProductPriceID = prdId,
                            OrginalProductAmount = price,
                            OrginalProductAmountCurrencyID = _prdCurrencyId,
                            LocalProductAmount = _netPrice,
                            LocalProductAmountCurrencyId = _siteCurrencyId,
                            DateTimeStamp = ord.CreatedOn,
                            TicketProtection = _tckProtection,
                            RefundDateTime = DateTime.Now,
                            Status = StatusId,
                            Cancellation = _isCancel,
                            TotalProductRefund = refundedPrice,
                            TotalProductRefundPercentage = refundedPerc,
                            CancellationFee = cancelFee,
                            Partial = false,
                            PassProtectionRefunded = true
                        };
                        _master.AddRefund(_OrderRefund);
                        Response.Redirect("P2POrderDetails.aspx?id=" + lblOrderID.Text);
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}