﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IR_Admin.OneHubServiceRef;
using Business;
namespace IR_Admin.Orders
{
    public partial class p2prefund : System.Web.UI.Page
    {
        ManageP2PBooking objBooking = new ManageP2PBooking();
        public String DepartureStationName;
        public String ArrivalStationName;
        public Decimal ExchangeRate = 0;
        public Int32 orderNo = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                mv.ActiveViewIndex = 0;

            if (Request["pn"] != null && Request["id"] != null)
            {
                txtDNR.Text = Request["pn"];
                orderNo = Convert.ToInt32(Request["id"]);
                ExtraRefundField result = objBooking.GetByReservationCode(txtDNR.Text, orderNo);
                if (result != null)
                {
                    DepartureStationName = result.DepartureStationName;
                    ArrivalStationName = result.ArrivalStationName;
                    ExchangeRate = result.ExchangeRate;
                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                ReleaseReservationRequest rrRequest = new ReleaseReservationRequest
                {
                    Header = new Header
                    {
                        onehubusername = "#@dots!squares",
                        onehubpassword = "#@dots!squares",
                        unitofwork = 0,
                        language = Language.en,
                    },
                    ReservationCode = txtDNR.Text.ToUpper().Trim()
                };
                OneHubRailOneHubClient client = new OneHubRailOneHubClient();
                ApiLogin(rrRequest);
                ReleaseReservationResponse rrResponse = client.ReleaseReservation(rrRequest);

                if (rrResponse != null && string.IsNullOrEmpty(rrResponse.ErrorMessage.MessageCode))
                {


                    txtUnitofWrk.Text = rrResponse.UnitOfWork.ToString();
                    ltrPnr.Text = rrResponse.ReservationCode;
                    ltrPnrDate.Text = rrResponse.ReservationDate.ToString();
                    ltrTitles.Text = rrResponse.NumTitles.ToString();
                    ltrTotalAmt.Text = (rrResponse.TotalPrice * ExchangeRate).ToString("0.00");
                    dtlTitles.DataSource = rrResponse.PTitle.ToList();
                    dtlTitles.DataBind();
                    mv.ActiveViewIndex = 1;
                }
                else
                    ShowMessage(2, rrResponse.ErrorMessage.MessageCode + ": " + rrResponse.ErrorMessage.MessageDescription);

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
   

        protected void btnRefundTotal_Click(object sender, EventArgs e)
        {
            try
            {
                TotalRefundRequest trRequest = new TotalRefundRequest
                {
                    Header = new Header
                    {
                        onehubusername = "#@dots!squares",
                        onehubpassword = "#@dots!squares",
                        unitofwork = Convert.ToInt64(txtUnitofWrk.Text),
                        language = Language.en,
                    },
                    Address = txtAddress.Text,
                    Document = txtDocument.Text,
                    Nominative = txtNominative.Text,
                    Notes = txtNote.Text
                };

                OneHubRailOneHubClient client = new OneHubRailOneHubClient();
                ApiLogin(trRequest);
                TotalRefundResponse rrResponse = client.TotalRefund(trRequest);

                if (rrResponse != null && string.IsNullOrEmpty(rrResponse.ErrorMessage.MessageCode))
                {
                    new ManageBooking().UpdateOrderStatus(9, Convert.ToInt64(orderNo));
                    ltrRefundStatus.Text = rrResponse.IsRefunded ? "YES" : "No";
                    dtlTotalRefund.DataSource = rrResponse.Refundables.ToList();
                    dtlTotalRefund.DataBind();
                    SaveRefund(rrResponse);
                    mv.ActiveViewIndex = 2;
                }
                else
                    ShowMessage(2, rrResponse.ErrorMessage.MessageCode + ": " + rrResponse.ErrorMessage.MessageDescription);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void SaveRefund(TotalRefundResponse rrResponse)
        {
            try
            {
                tblP2PRefund refund = new tblP2PRefund
                    {
                        Address = txtAddress.Text,
                        Documents = txtDocument.Text,
                        IsRefunded = rrResponse.IsRefunded,
                        Nominative = txtNominative.Text,
                        Note = txtNominative.Text,
                        OrderNo = orderNo,
                        RefundCauseCode = "MP",
                        ReservationCode = txtDNR.Text,
                        UnitOfWork = txtUnitofWrk.Text,
                    };

                Guid id = objBooking.InsertRefund(refund);

                if (rrResponse.Refundables != null && rrResponse.Refundables.Count() > 0)
                {
                    foreach (var item in rrResponse.Refundables)
                    {
                        tblP2PRefundTitle refundt = new tblP2PRefundTitle
                        {
                            P2PRefundId = id,
                            Amount = item.TotalAmount * ExchangeRate,
                            DeductedAmount = item.TitleDeductedAmount * ExchangeRate, //-- the amount will be variy in report area.
                            DeductedAmtInPercent = item.DeductionPerc,
                            RefundCause = item.RefundCause,
                            RefundedAmount = item.TitleRefundedAmount * ExchangeRate,//-- the amount will be variy in report area.
                            TitleNumber = item.TitleNumber,
                        };
                        Guid tid = objBooking.InsertRefundTitle(refundt);
                    }
                }

                if (id != Guid.Empty)
                    ShowMessage(1, "Booking amount refunded successfully.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }


        protected void dtlTotalRefund_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Literal ltrAmount = e.Item.FindControl("ltrAmount") as Literal;
                    Literal ltrPercent = e.Item.FindControl("ltrPercent") as Literal;
                    Literal ltrDeductedAmount = e.Item.FindControl("ltrDeductedAmount") as Literal;
                    Literal ltrRefundedAmount = e.Item.FindControl("ltrRefundedAmount") as Literal;
                    ltrDeductedAmount.Text = ((Convert.ToDecimal(ltrAmount.Text) * Convert.ToDecimal(ltrPercent.Text)) / 100).ToString("0.00");
                    ltrRefundedAmount.Text = ((Convert.ToDecimal(ltrAmount.Text) - Convert.ToDecimal(ltrDeductedAmount.Text))).ToString("0.00");
                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
            }
        }

        private void ApiLogin(ReleaseReservationRequest request)
        {
            #region API Account

            db_1TrackEntities db = new db_1TrackEntities();
            orderNo = Convert.ToInt32(Request["id"]);
            Guid id = (Guid)db.tblOrders.FirstOrDefault(x => x.OrderID == orderNo).SiteID;
            var api = new ManageUser().GetApiLogingDetail(id).Where(x => x.IsActive == 1).ToList();
            if (api != null)
            {
                var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
                var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

                request.Header.ApiAccount = new ApiAccountDetail
                {
                    BeNeApiId = result != null ? result.ID : 0,
                    TiApiId = resultTI != null ? resultTI.ID : 0,
                };
            }
            #endregion
        }

        private void ApiLogin(TotalRefundRequest request)
        {
            #region API Account

            db_1TrackEntities db = new db_1TrackEntities();
            orderNo = Convert.ToInt32(Request["id"]);
            Guid id = (Guid)db.tblOrders.FirstOrDefault(x => x.OrderID == orderNo).SiteID;
            var api = new ManageUser().GetApiLogingDetail(id).Where(x => x.IsActive == 1).ToList();
            if (api != null)
            {
                var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
                var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

                request.Header.ApiAccount = new ApiAccountDetail
                {
                    BeNeApiId = result != null ? result.ID : 0,
                    TiApiId = resultTI != null ? resultTI.ID : 0,
                };
            }
            #endregion
        }
    }
}

