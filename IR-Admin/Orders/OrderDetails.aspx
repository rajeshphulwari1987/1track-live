﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="OrderDetails.aspx.cs"
    Inherits="IR_Admin.Orders.OrderDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .ajax__calendar_day, .ajax__calendar_dayname, .ajax__calendar_title, .ajax__calendar_footer, .ajax__calendar_month, .ajax__calendar_year {
            font-size: 11px !important;
        }

        .ajax__calendar_today, .ajax__calendar_footer {
            margin-bottom: 5px;
        }

        .gridP {
            background: #666;
        }

        .tr-color-span span {
            color: #000 !important;
        }

        table tr {
            vertical-align: top;
        }

        .grid-sec2 {
            padding: 5px;
        }
    </style>
    <script type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }
        function preventDt() {
            $("#MainContent_txtDob, #MainContent_txtPassStartDate").keypress(function (event) { event.preventDefault(); });
        };

        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46 && KeyID == 250)
                return true;
            return false;
        }

        function keynumcheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57)
                return false;
            return true;
        }
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');

            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            //            var ssdate = (new Date(sdate.getFullYear(), sdate.getMonth()+6, sdate.getDay())); 
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
                else
                    $("[id*=txtOrderId]").trigger('change');
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });

            if ($("#hdnRefundType").val() == "false") {
                $(".IsFullRefund").remove();
            }
        });
        function validamount() {
            if (!$.isNumeric($("[id*=txtbookingEdit]").val()))
                $("[id*=txtbookingEdit]").val("00.00").select();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnRefundType" ClientIDMode="Static" runat="server" Value="false" />
    <h2>All Orders</h2>
    <asp:UpdatePanel ID="Upnl1" runat="server">
        <ContentTemplate>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="OrderDetails.aspx" class="current">List</a></li>
                </ul>
                <!-- tab "panes" -->
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="full mr-tp1">
                    <div class="panes">
                        <div id="divlist" runat="server">
                            <div id="dvSearch" class="searchDiv" runat="server">
                                <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                                <table width="100%" style="line-height: 20px">
                                    <tr style="display: none;">
                                        <td colspan="6">Office:
                                            <asp:DropDownList ID="ddlOffice" runat="server" Style="margin-left: 5px;" Width="90%"
                                                AutoPostBack="true" OnTextChanged="btnSubmit_Click" ToolTip="Select Office.">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div runat="server" id="divsyswide" visible="false">
                                                System wide search<asp:CheckBox runat="server" ID="chkSysWide" OnCheckedChanged="btnSubmit_Click"
                                                    AutoPostBack="true" />
                                            </div>
                                        </td>
                                        <td colspan="2">From Date:
                                        </td>
                                        <td colspan="2">To Date:
                                        </td>
                                        <td>Departure/Booking Date:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Date Range:
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static" Width="243" />
                                            <asp:CalendarExtender ID="cal1" runat="server" TargetControlID="txtStartDate" Format="dd/MM/yyyy"
                                                PopupButtonID="imgCal1" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                            <asp:Image ID="imgCal1" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                                                AlternateText="Calendar" Style="position: absolute; margin: -3px 0 0 2px;" />
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static" Width="243" />
                                            <asp:CalendarExtender ID="cal2" runat="server" TargetControlID="txtLastDate" Format="dd/MM/yyyy"
                                                PopupButtonID="imgCal2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                            <asp:Image ID="imgCal2" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                                                AlternateText="Calendar" Style="position: absolute; margin: -3px 0 0 2px;" />
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddltypeDate" Width="160" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddltypeDate_SelectIndexChange">
                                                <asp:ListItem Value="1">Departure Date</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">Booking Date</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Order ID :
                                        </td>
                                        <td style="display: none;">Ticket/Pass No :
                                        </td>
                                        <td>Traveller Name:
                                        </td>
                                        <td>Your reference :
                                        </td>
                                        <td>Stock Number :
                                        </td>
                                        <td>Order Status :
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtOrderId" runat="server" Width="120px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                ToolTip="Enter Order ID." MaxLength="50" />
                                        </td>
                                        <td style="display: none;">
                                            <asp:TextBox ID="txtOrderPassId" runat="server" Width="120px" AutoPostBack="true"
                                                OnTextChanged="btnSubmit_Click" ToolTip="Enter Ticket/Pass Number." MaxLength="50" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTrvName" runat="server" Width="120px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                ToolTip="Enter Traveller Name." MaxLength="50" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRef" runat="server" Width="120px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                ToolTip="Enter STA Reference." MaxLength="50" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtStockNo" runat="server" Width="120px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                ToolTip="Enter Stock Number." MaxLength="50" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlOrderSt" runat="server" ValidationGroup="rv" Width="160px"
                                                AutoPostBack="true" OnTextChanged="btnSubmit_Click" ToolTip="Select Order Status." />
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <asp:GridView ID="grdOrders" runat="server" AutoGenerateColumns="False" PageSize="10"
                                PagerStyle-CssClass="paging" CssClass="grid-head2" CellPadding="4" ForeColor="#333333"
                                GridLines="None" Width="100%" AllowSorting="True" OnSorting="grdOrders_Sorting">
                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                <HeaderStyle Font-Size="12px" ForeColor="White" HorizontalAlign="Left" />
                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    Record not found.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Order ID">
                                        <ItemTemplate>
                                            <%#Eval("OrderID")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Travellers">
                                        <ItemTemplate>
                                            <%#Eval("TravellerName")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="16%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Your reference">
                                        <ItemTemplate>
                                            <%#Eval("STAReferenceCode")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock">
                                        <ItemTemplate>
                                            <%#Eval("StockNo").ToString() == "0" ? "-" : Eval("StockNo")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <%#Eval("OrderStatus")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departure Date" SortExpression="DateOfDepart">
                                        <ItemTemplate>
                                            <%# Eval("DateOfDepart", "{0: dd/MMM/yy}")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Booking Date" SortExpression="CreatedOn">
                                        <ItemTemplate>
                                            <%# Eval("CreatedOn", "{0: dd/MMM/yy}")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View Details">
                                        <ItemTemplate>
                                            <a href='OrderDetails.aspx?id=<%#Eval("OrderID")%>' title="">
                                                <img src="../images/<%#Eval("OrderViewImgURL")%>" title="View" /></a>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                            <table width="100%">
                                <tr class="paging">
                                    <td style="float: left;">
                                        <asp:LinkButton ID="lnkPrevious" Text='<< Previous' runat="server" OnClick="lnkPrevious_Click" />
                                    </td>
                                    <td style="float: left;">
                                        <asp:LinkButton ID="lnkPrepaging" Text='...' runat="server" OnClick="lnkPrepaging_Click" />
                                    </td>
                                    <asp:Repeater ID="DLPageCountItem" runat="server">
                                        <ItemTemplate>
                                            <td style="float: left">
                                                <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                    Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <td style="float: left;">
                                        <asp:LinkButton ID="lnknextpaging" Text='...' runat="server" OnClick="lnknextpaging_Click" />
                                    </td>
                                    <td style="float: left;">
                                        <asp:LinkButton ID="lnkNext" Text='Next >>' runat="server" OnClick="lnkNext_Click" />
                                    </td>
                                    <td style="float: right;">
                                        <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divDetail" runat="server">
                            <table class="tblMainSection">
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Orders</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table class="table-stock" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3" class="heading-color">
                                                            <strong>Order Number: </strong>
                                                            <%#Eval("SiteID")%>
                                                            <asp:Label ID="lblOrderID" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">
                                                            <strong>Status: </strong>
                                                            <asp:Label ID="lblStatus" runat="server" />
                                                            <asp:LinkButton ID="lnkEditStatus" runat="server" class="heading-color" OnClick="lnkEditStatus_Click">Edit</asp:LinkButton>
                                                        </td>
                                                        <td width="35%">
                                                            <strong>Creation Date:</strong>
                                                            <asp:Label ID="txtOrdCDate" runat="server" />
                                                        </td>
                                                        <td width="35%">
                                                            <strong>Payment Date: </strong>
                                                            <asp:Label ID="lblPaymentDate" runat="server" Text="-------" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Site:</strong>
                                                            <asp:Label ID="lblSite" runat="server" />
                                                        </td>
                                                        <td>
                                                            <strong>Ip Adderess:</strong>
                                                            <asp:Label ID="txtIp" runat="server" />
                                                        </td>
                                                        <td>
                                                            <strong>Departure date:</strong>
                                                            <asp:Label ID="txtDepDt" runat="server" />
                                                        </td>
                                                        <tr>
                                                            <td valign="top">
                                                                <strong>Agent Name:</strong>
                                                                <asp:Label ID="lblAgentNm" runat="server" />
                                                            </td>
                                                            <td valign="top">
                                                                <strong>Agent Username:</strong>
                                                                <asp:Label ID="lblAgentUnm" runat="server" />
                                                            </td>
                                                            <td valign="top">
                                                                <strong>Agent Office:</strong>
                                                                <asp:Label ID="lblAgentOff" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Grand Total Sold:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblGrandTotal" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Product(s) Gross Total:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblGrossTotal" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Discount Amount:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblDiscount" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Shipping Cost:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblShippingCost" runat="server" />
                                                                <asp:LinkButton ID="lnkRefShippingFee" class="heading-color" Style="float: right; margin-right: 20px;"
                                                                    Text="Refund" runat="server" OnClick="lnkRefShippingFee_Click" />
                                                            </td>
                                                            <td>
                                                                <strong>Booking Fee:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblBookindFee" runat="server" />
                                                                <asp:LinkButton ID="lnkeditbooking" CssClass="heading-color" Text="Edit" runat="server" OnClick="lnkeditbooking_Click"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkRefBookingFee" class="heading-color" Style="float: right; margin-right: 20px;"
                                                                    Text="Refund" runat="server" OnClick="lnkRefBookingFee_Click"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <strong>Ticket Protection:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblTckProtection" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Admin Fee:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblAdminFee" runat="server" />
                                                                <asp:LinkButton ID="lnkRefAdminFee" class="heading-color" Style="float: right; margin-right: 20px;"
                                                                    Text="Refund" runat="server" OnClick="lnkRefAdminFee_Click" />
                                                            </td>
                                                            <td> <strong>Current Grand Total:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lablCurrentGrandTotal" runat="server" /></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Eurail Product(s) NET:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="LblEurail" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Product(s) NET (non Eurail):</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="LblNonEurail" runat="server" />
                                                            </td>
                                                            <td>
                                                                <div runat="server" id="netpricesection" style="font-size: 12px;">
                                                                    <strong>Product(s) Net Total:</strong>
                                                                    <%=Currency.ToString()%>
                                                                    <asp:Label ID="lblNetPrice" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <a id="lnkPrintReceipt" runat="server" href="#" class="heading-color" target="_Receipt">Print Receipt</a>
                                                            </td>
                                                            <td>
                                                                <div id="TicketPrintedDIV" runat="server" style="font-size: 12px">
                                                                    <strong>TicketPrinted: </strong>
                                                                    <asp:Label ID="lblTicketPrint" runat="server" Text="No" />
                                                                </div>
                                                            </td>
                                                            <td align="center">
                                                                <div id="divPrint" runat="server">
                                                                    <a id="lnkSentToQueue" runat="server" href="#" class="heading-color">
                                                                        <div>
                                                                            Send Unprinted Passes
                                                                            <br />
                                                                            To Print Queue
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div id="divprintqueuein" runat="server">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong>Note:</strong>
                                                                <div id="divNote" runat="server">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <div class="cat-outer" style="width: 100%; margin: 0;">
                                                <div class="cat-inner-ord">
                                                    <span class="clsRed">Pass Details</span>
                                                    <div id="dvPass" runat="server">
                                                        <asp:GridView ID="grdPass" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                            CssClass="gridP tr-color" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                            Width="100%" OnRowDataBound="grdPass_RowDataBound">
                                                            <AlternatingRowStyle BackColor="#FBDEE6" />
                                                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                                BorderColor="#FFFFFF" BorderWidth="1px" VerticalAlign="Top" />
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <EmptyDataRowStyle HorizontalAlign="Center" />
                                                            <EmptyDataTemplate>
                                                                Record not found.
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Ticket No.">
                                                                    <ItemTemplate>
                                                                        <%# ((GridViewRow)Container).RowIndex + 1%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle Width="8%" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="RailPassName">
                                                                    <ItemTemplate>
                                                                        <%#Eval("RailPassName")%>
                                                                        <%#Eval("CountryName")%>,
                                                                        <%#Eval("PassDesc")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Traveller Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTrvType" Text=' <%#Eval("TrvClass")%>' runat="server" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Special Offer">
                                                                    <ItemTemplate>
                                                                        <%#(Convert.ToInt64(Eval("OrderID").ToString())>134100) ? Eval("SpecialOffer") : "-"%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Class Name">
                                                                    <ItemTemplate>
                                                                        <%#Eval("ClassType")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Price">
                                                                    <ItemTemplate>
                                                                        <%=Currency%> <%#Eval("SalePrice")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="65px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Price (NET)">
                                                                    <ItemTemplate>
                                                                        <%=Currency%> <%#Eval("Price")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket Protection">
                                                                    <ItemTemplate>
                                                                        <%=Currency.ToString()%>
                                                                        <%#Eval("TicketProtection")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Stock No.">
                                                                    <ItemTemplate>
                                                                        <%#Eval("StockNumber")%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cxl/Extra Charge">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkEditExtraCharge" runat="server" class="heading-color" OnClick="lnkEditExtraCharge_Click">Apply</asp:LinkButton>
                                                                        <asp:Label runat="server" ID="lblExtraChargeAmount"></asp:Label>
                                                                        <asp:HiddenField runat="server" ID="hdnpasssaleid" Value='<%#Eval("ID")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkRefund" class="heading-color" Text="Refund" runat="server" />
                                                                        <asp:HiddenField ID="hdnIsSaver" runat="server" Value='<%#Eval("IsSaver")%>' />
                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Eval("SaverID")%>' />
                                                                        <asp:HiddenField ID="hdnProductID" runat="server" Value='<%#Eval("ProductID")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataRowStyle ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Traveller Information</strong></legend>
                                            <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0;">
                                                <asp:GridView ID="grdTraveller" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                    CssClass="gridP tr-color" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                    Width="100%" OnRowCommand="grdTraveller_RowCommand">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Ticket No.">
                                                            <ItemTemplate>
                                                                <%# ((GridViewRow)Container).RowIndex + 1%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <%#Eval("Title")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="First Name">
                                                            <ItemTemplate>
                                                                <%#Eval("FirstName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Middle Name">
                                                            <ItemTemplate>
                                                                <%#Eval("MiddleName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last Name">
                                                            <ItemTemplate>
                                                                <%#Eval("LastName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date of Birth">
                                                            <ItemTemplate>
                                                                <%# Eval("DOB", "{0:MMM dd, yyyy}")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Country of residence">
                                                            <ItemTemplate>
                                                                <%#Eval("CountryName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Nationality">
                                                            <ItemTemplate>
                                                                <%#Eval("Nationality")!=null?(Eval("Nationality").ToString() == "0" ? "-" : Eval("Nationality")):Eval("Nationality")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Passport No">
                                                            <ItemTemplate>
                                                                <%#Eval("PassportNo")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Pass Start Date">
                                                            <ItemTemplate>
                                                                <%# Eval("PassStartDate", "{0:MMM dd, yyyy}")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lead Passenger">
                                                            <ItemTemplate>
                                                                <%-- <%#Eval("LeadPassenger")%>--%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stock No.">
                                                            <ItemTemplate>
                                                                <%# Eval("StockNo")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:Panel runat="server" ID="upnltrvEdit" Visible='<%#isTrvEdit %>'>
                                                                    <asp:LinkButton ID="lnkEditTraveller" CommandName="cmdEdit" CommandArgument='<%#Eval("ID") %>'
                                                                        class="heading-color" runat="server" OnClientClick="preventdt()">Edit</asp:LinkButton>
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Billing Information</strong></legend>
                                            <div class="cat-inner-ord">
                                                <asp:Repeater ID="rptBilling" runat="server">
                                                    <ItemTemplate>
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <strong>Title: </strong>
                                                                    <%#Eval("Title")%>
                                                                </td>
                                                                <td>
                                                                    <strong>First Name:</strong>
                                                                    <%#Eval("FirstName")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Last Name:</strong>
                                                                    <%#Eval("LastName")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Address1:</strong>
                                                                    <%#Eval("Address1")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Address2:</strong>
                                                                    <%#Eval("Address2")%>
                                                                </td>
                                                                <td>
                                                                    <strong>City:</strong>
                                                                    <%#Eval("City")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>State:</strong>
                                                                    <%# Eval("State")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Postal/Zip Code:</strong>
                                                                    <%#Eval("Postcode")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Country:</strong>
                                                                    <%#Eval("Country")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Email Address:</strong>
                                                                    <%#Eval("EmailAddress")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Phone Number:</strong>
                                                                    <%#Eval("Phone")%>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Shipping Information</strong></legend>
                                            <div class="cat-inner-ord">
                                                <asp:Repeater ID="rptShipping" runat="server" OnItemCommand="rptShipping_ItemCommand">
                                                    <ItemTemplate>
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <strong>Title: </strong>
                                                                    <%#Eval("TitleShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>First Name:</strong>
                                                                    <%#Eval("FirstNameShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Last Name:</strong>
                                                                    <%#Eval("LastNameShpg")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Address1:</strong>
                                                                    <%#Eval("Address1Shpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Address2:</strong>
                                                                    <%#Eval("Address2Shpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>City:</strong>
                                                                    <%#Eval("CityShpg")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>State:</strong>
                                                                    <%# Eval("StateShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Postal/Zip Code:</strong>
                                                                    <%#Eval("PostcodeShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Country:</strong>
                                                                    <%#Eval("CountryShpg")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Email Address:</strong>
                                                                    <%#Eval("EmailAddressShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Phone Number:</strong>
                                                                    <%#Eval("PhoneShpg")%>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkEditShipping" class="heading-color" runat="server">Edit shipping details</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <div style="float: right; margin-right: 18px;">
                                                    <asp:Button ID="btnSendEmail" runat="server" CssClass="button1" Text="Send confirmation email"
                                                        OnClick="btnSendEmail_Click" Width="215px" Visible="False" />
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Payment Details</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table id="tblNonAgent" runat="server" class="table-stock" cellpadding="0" cellspacing="0"
                                                    visible="False">
                                                    <tr>
                                                        <td>
                                                            <strong>Name:</strong>
                                                            <asp:Label ID="lblCardholderName" runat="server" />
                                                        </td>
                                                        <td>
                                                            <strong>Card Number:</strong>
                                                            <asp:Label ID="lblCardNumber" runat="server" />
                                                        </td>
                                                        <td>
                                                            <strong>Payment Id:</strong>
                                                            <asp:Label ID="lblPaymentId" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Exp. Date:</strong>
                                                            <asp:Label ID="lblExpDate" runat="server" />
                                                        </td>
                                                        <td>
                                                            <strong>Brand:</strong>
                                                            <asp:Label ID="lblBrand" runat="server" />
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table id="tblAgent" runat="server" class="table-stock" cellpadding="0" cellspacing="0"
                                                    visible="False">
                                                    <tr>
                                                        <td>
                                                            <strong>Agent ref / Folder No:</strong>
                                                            <asp:Label ID="lblAgentRef" runat="server" />
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset id="idRefund" runat="server" class="grid-sec2">
                                            <legend><strong>Refund Details</strong></legend>
                                            <div class="cat-outer" style="width: 100%; margin: 0;">
                                                <table width="100%" class="tr-color tr-color-span" style="color: white; background: #666; border: 1px solid #FFF;"
                                                    cellspacing="0" cellpadding="5">
                                                    <tr>
                                                        <td>
                                                            <b>Shipping Cost</b>
                                                        </td>
                                                        <td>
                                                            <b>Booking Fee</b>
                                                        </td>
                                                        <td>
                                                            <b>Admin Fee</b>
                                                        </td>
                                                    </tr>
                                                    <tr style="color: Black; background-color: #ECECEC;">
                                                        <td>
                                                            <%=Currency%><asp:Label runat="server" ID="lblship" />
                                                        </td>
                                                        <td>
                                                            <%=Currency%><asp:Label runat="server" ID="lblbook" />
                                                        </td>
                                                        <td>
                                                            <%=Currency%><asp:Label runat="server" ID="lbladmin" />
                                                        </td>
                                                    </tr>
                                                    <tr style="color: Black; background-color: #FBDEE6;">
                                                        <td>
                                                            <%=Currency%>(<asp:Label runat="server" ID="lblshippingref" />) Refund
                                                        </td>
                                                        <td>
                                                            <%=Currency%>(<asp:Label runat="server" ID="lblbookingref" />) Refund
                                                        </td>
                                                        <td>
                                                            <%=Currency%>(<asp:Label runat="server" ID="lbladminref" />) Refund
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:GridView ID="grdRefund" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                    CssClass="gridP tr-color" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                    Width="100%" OnRowCommand="grdRefund_RowCommand">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Product">
                                                            <ItemTemplate>
                                                                <%#Eval("Product")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="20%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stock No.">
                                                            <ItemTemplate>
                                                                <%# Eval("StockNo")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Traveller">
                                                            <ItemTemplate>
                                                                <%#Eval("Traveller")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="User">
                                                            <ItemTemplate>
                                                                <%#Eval("User")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Gross Price">
                                                            <ItemTemplate>
                                                                <%=Currency%>
                                                                <%#Eval("Price", "{0:F2}")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Refund">
                                                            <ItemTemplate>
                                                                <%=Currency%>
                                                                (<%#Eval("Refund", "{0:F2}")%>)
                                                                <br />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Refund Date">
                                                            <ItemTemplate>
                                                                <%# Eval("RefundDate", "{0:MMM dd, yyyy}")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Additional Refund">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnAdditionalRefund" CommandName="additionalRefund" CommandArgument='<%#Eval("PassSalesId")%>' Visible='<%#Eval("IsFullRefund")%>'
                                                                    Text="Additional Refund" CssClass="heading-color" runat="server" />
                                                                <asp:HiddenField ID="hdnPassSalesId" runat="server" Value='<%#Eval("PassSalesId")%>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="IsFullRefund" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" CssClass="IsFullRefund" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="float: right;">
                                                            <a href="#" target="_blank" style="display: inline-block; text-align: center; text-decoration: none;" title="print" id="BtnRefundPrint" runat="server" class="button1">Print</a>
                                                            <asp:Button ID="BtnRefundMailSend" runat="server" Text="Send" CssClass="button1" OnClick="BtnRefundMailSend_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset id="OrderAttachments" runat="server" class="grid-sec2">
                                            <legend><strong>Order Attachments</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table width="100%" style="background: #fff; padding: 3px 0;">
                                                    <tr>
                                                        <td>Upload File:
                                                        </td>
                                                        <td>
                                                            <asp:FileUpload ID="fupAttachment" runat="server" AllowMultiple="true" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddAttachment" Text="Upload" runat="server" CssClass="button1"
                                                                OnClick="btnAddAttachment_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="cat-inner-ord">
                                                <asp:GridView ID="grdOrderAttachments" runat="server" AutoGenerateColumns="False"
                                                    BorderStyle="None" CssClass="gridP tr-color" CellPadding="4" ForeColor="#333333"
                                                    GridLines="None" Width="100%" OnRowCommand="grdOrderAttachments_RowCommand">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. no.">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="File Name">
                                                            <ItemTemplate>
                                                                <%#Eval("AttachmentName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="File Type">
                                                            <ItemTemplate>
                                                                <img src="../images/FileIcon/<%# GetFileExtension(Eval("AttachmentPath").ToString())%>"
                                                                    alt="file" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:ImageButton runat="server" ID="imgRemove" Visible="<%#IsVisibleAttachmentDeleteBtn%>"
                                                                    CommandArgument='<%#Eval("ID")+","+ Eval("AttachmentPath")%>' CommandName="Remove"
                                                                    ImageUrl='../images/delete.png' ToolTip="Delete file" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                                                <a href='<%#Eval("AttachmentPath")%>' title="Download file" target="_blank">
                                                                    <img src="../images/download.png" alt="download" width="20px" />
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Agent Notes</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <textarea id="txtNotes" runat="server" rows="7" style="width: 100%" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnAddNotes" Text="Add" runat="server" CssClass="button1" OnClick="btnAddNotes_Click"
                                                                OnClientClick="return CheckIsRepeat();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="cat-inner-ord">
                                                <asp:GridView ID="grdOrderNotes" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                    CssClass="gridP tr-color" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                    Width="100%" OnRowCommand="grdOrderNotes_RowCommand">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <%#Eval("UserName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Top" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Notes">
                                                            <ItemTemplate>
                                                                <%#Eval("Notes")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="40%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                <%#Eval("CreatedOn", "{0:dd MMM yyyy HH':'mm 'GMT'}")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="15%" HorizontalAlign="Left" VerticalAlign="Top" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Agent Updated Status</strong></legend>
                                            <div class="cat-inner-ord">
                                                <asp:Repeater ID="gvagentupdatestatus" runat="server">
                                                    <HeaderTemplate>
                                                        <table class="gridP tr-color" width="100%" style="border-collapse: collapse; font-size: 12px; border: 1px solid white;">
                                                            <tr style="color: white;">
                                                                <th>Agent Name
                                                                </th>
                                                                <th>Date
                                                                </th>
                                                                <th>Time
                                                                </th>
                                                                <th>Status
                                                                </th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <%#Eval("AgentName")%>
                                                            </td>
                                                            <td>
                                                                <%#Eval("Date")%>
                                                            </td>
                                                            <td>
                                                                <%#Eval("Time")%>
                                                            </td>
                                                            <td>
                                                                <%#Eval("Status")%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlPrntQ" runat="server" CssClass="popup" Width="450px" Style="overflow: hidden; padding: 10px;">
                                <div class="dvPopupHead" style="padding: 4px; width: 98%;">
                                    <div class="dvPopupTxt">
                                        Send to Queue
                                    </div>
                                    <a href="#" id="btnClose" runat="server" style="color: #fff;">X </a>
                                </div>
                                <div class="clsMrgTop10">
                                    <asp:DropDownList ID="ddlPrntQ" runat="server" Width="400px" Style="margin: 10px 0px 30px 0px; padding: 5px; height: auto;"
                                        ValidationGroup="vp" />
                                </div>
                                <div class="clsMrgTop10">
                                    <asp:Button ID="btnSend" runat="server" CssClass="button clsBtn" Text="Send" ValidationGroup="vp"
                                        OnClick="btnSend_Click" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button clsBtn" Text="Cancel"
                                        OnClick="btnCancel_Click1" />
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="mdpupPrntQ" runat="server" TargetControlID="lnkSentToQueue"
                                CancelControlID="btnClose" PopupControlID="pnlPrntQ" BackgroundCssClass="modalBackground" />
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <asp:LinkButton ID="lnkPop" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop1" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop2" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop4" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop3" runat="server" Text="lnk" />
            </div>
            <div id="pnlStockAdd" class="popup-container" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Add/Edit Shipping Detail
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Title:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlMr" runat="server" class="inputsl">
                            <asp:ListItem>Mr</asp:ListItem>
                            <asp:ListItem>Mrs</asp:ListItem>
                            <asp:ListItem>Ms</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="divleft">
                        First Name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                        <asp:RequiredFieldValidator ID="rfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                            ControlToValidate="txtFirst" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Last name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="rfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                            ControlToValidate="txtLast" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Email<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" autocomplete="off">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                            ControlToValidate="txtEmail" ForeColor="Red" ValidationGroup="vgs1" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                            ControlToValidate="txtEmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Phone Number<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtPhone" runat="server" CssClass="input validcheck" MaxLength="15" />
                        <asp:RequiredFieldValidator ID="rfPhone" runat="server" Text="*" ErrorMessage="Please enter phone."
                            ControlToValidate="txtPhone" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Address Line 1 Or Company Name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="rfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                            ControlToValidate="txtAdd" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Address Line 2
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" />
                    </div>
                    <div class="divleft">
                        Town / City
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="input" />
                    </div>
                    <div class="divleft">
                        County / State
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtState" runat="server" CssClass="input" />
                    </div>
                    <div class="divleft">
                        Postal/Zip Code<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="rfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                            ControlToValidate="txtZip" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Country<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" />
                        <asp:RequiredFieldValidator ID="rfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                            ControlToValidate="ddlCountry" InitialValue="0" ValidationGroup="vgs1" ForeColor="Red" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button1" Text="Update shipping detail"
                            ValidationGroup="vgs1" OnClick="btnSubmit_Click1" />
                        &nbsp;
                        <button id="btnCancelShip" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="lblMessage" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mdpupShipping" runat="server" TargetControlID="lnkPop"
                CancelControlID="btnCancelShip" PopupControlID="pnlStockAdd" BackgroundCssClass="modalBackground" />
            <div id="pnlTraveller" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Add/Edit Traveller
                    <asp:HiddenField runat="server" ID="hdntrvID" />
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Title:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlTrTitle" runat="server" class="inputsl">
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="divleft">
                        First Name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="ddlTrFirstName" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*"
                            ErrorMessage="Please enter first name." ControlToValidate="ddlTrFirstName" ForeColor="Red"
                            ValidationGroup="hvgs1" />
                    </div>
                    <div class="divleft">
                        Middle Name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="ddlTrMiddleName" runat="server" CssClass="input" ClientIDMode="Static" />
                    </div>
                    <div class="divleft">
                        Last name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtTrLName" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*"
                            ErrorMessage="Please enter last name." ControlToValidate="txtTrLName" ForeColor="Red"
                            ValidationGroup="hvgs1" />
                    </div>
                    <div class="divleft">
                        Date of Birth
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtDob" runat="server" CssClass="input" MaxLength="11" />
                        <asp:RequiredFieldValidator ID="rfDob" runat="server" Text="*" ErrorMessage="Please enter dob."
                            ControlToValidate="txtDob" ForeColor="Red" ValidationGroup="hvgs1" />
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDob"
                            Format="MMM/dd/yyyy" PopupButtonID="imgCalDob" PopupPosition="BottomLeft" />
                        <asp:Image ID="imgCalDob" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                            AlternateText="Calendar" Style="position: absolute; margin: -2px 5px; 0 0;" />
                    </div>
                    <div class="divleft">
                        Country of residence
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlTrCountry" runat="server" class="chkcountry" Style="width: 206px; height: 28px; margin-left: -2px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="hvgs1"
                            CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                            ControlToValidate="ddlTrCountry" InitialValue="0" />
                    </div>
                    <div class="divleft">
                        Passport Number
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtPassportNumber" class="input chkvalid" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="hvgs1"
                            runat="server" CssClass="valid" Text="*" ErrorMessage="Please enter Passport No."
                            ForeColor="Red" ControlToValidate="txtPassportNumber" />
                    </div>
                    <div class="divleft">
                        Nationality
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtNationality" class="input chkvalid" runat="server" />
                        <asp:RequiredFieldValidator ID="reqNationality" ValidationGroup="hvgs1" runat="server"
                            CssClass="valid" Text="*" ErrorMessage="Please enter Nationality." ForeColor="Red"
                            ControlToValidate="txtNationality" />
                    </div>
                    <div class="divleft">
                        Pass Start Date
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtPassStartDate" runat="server" CssClass="input" MaxLength="11" />
                        <asp:RequiredFieldValidator ID="rfPassDate" runat="server" Text="*" ErrorMessage="Please enter Pass start date."
                            ControlToValidate="txtPassStartDate" ForeColor="Red" ValidationGroup="hvgs1" />
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtPassStartDate"
                            Format="MMM/dd/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft"
                            OnClientDateSelectionChanged="checkDate" />
                        <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                            BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px; 0 0;" />
                    </div>
                    <div class="divleft">
                        Lead Passenger
                    </div>
                    <div class="divright">
                        <asp:CheckBox ID="chkLead" runat="server" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnTravellerUpdate" runat="server" CssClass="button1" Text="Update Traveller detail"
                            ValidationGroup="hvgs1" OnClick="btnTravellerUpdate_Click1" />
                        &nbsp;
                        <button id="btnCcacelTrv" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="Label1" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mupTraveller" runat="server" TargetControlID="lnkPop1"
                CancelControlID="btnCcacelTrv" PopupControlID="pnlTraveller" BackgroundCssClass="modalBackground" />
            <div id="pnlOrderStatus" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Edit Order Status
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Order Status:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlOrderStatus" runat="server" class="inputsl">
                        </asp:DropDownList>
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnOrderStatus" runat="server" CssClass="button1" CausesValidation="false"
                            Text="Update order status" OnClick="btnOrderStatus_Click" />
                        &nbsp;
                        <button id="btnOrderStausCancel" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="Label2" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeOrderStatus" runat="server" TargetControlID="lnkPop2"
                CancelControlID="btnOrderStausCancel" PopupControlID="pnlOrderStatus" BackgroundCssClass="modalBackground" />
            <div id="pnlOrderRefund" style="display: block; width: 700px;">
                <div class="heading-hd">
                    Order Refund
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        <asp:Label runat="server" ID="lblhdr" Text=""></asp:Label>
                    </div>
                    <div class="divright">
                        <%=Currency%><asp:Label runat="server" ID="lblhdrRefundTxt" ClientIDMode="Static"></asp:Label>
                    </div>
                    <div class="divleft">
                        <asp:Label runat="server" ID="lblrefhdr" Text=""></asp:Label>
                    </div>
                    <div class="divright" style="height: 35px;">
                        <asp:DropDownList runat="server" ID="ddlrefundsel" onchange="onchange()" ClientIDMode="Static" />
                        <label id="lblamountbind">
                        </label>
                        <asp:RequiredFieldValidator Font-Size="13px" ID="req" runat="server" ControlToValidate="ddlrefundsel"
                            InitialValue="-1" ErrorMessage="Please select refund." ValidationGroup="refdata"
                            ForeColor="Red" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnrefund" runat="server" CssClass="button1" Text="Refund" OnClick="btnrefund_Click"
                            ValidationGroup="refdata" />
                        &nbsp;
                        <button id="btnRefundCancel" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="lblrefmsg" ForeColor="green" runat="server" /></span>
                    </div>
                    <asp:HiddenField runat="server" ID="hdnrfndamount" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdnrfndtype" />
                </div>
            </div>
            <asp:ModalPopupExtender ID="mprefund" runat="server" TargetControlID="lnkPop3" CancelControlID="btnRefundCancel"
                PopupControlID="pnlOrderRefund" BackgroundCssClass="modalBackground" />
            <div id="pnlExtraCharge" style="display: block; width: 700px;">
                <div class="heading-hd">
                    Cxl/Extra Charge
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        <asp:HiddenField runat="server" ID="hdnpassaleidextracharge" ClientIDMode="Static" />
                        <asp:Label runat="server" ID="lblhdrExtraCharge" Text="Charge Amount"></asp:Label>
                    </div>
                    <div class="divright">
                        <%=Currency%>
                        &nbsp;<asp:TextBox runat="server" ID="txtExtraCharge" ClientIDMode="Static" MaxLength="5"
                            placeholder="Enter Charge Amount" onKeyUp="hasamount(this)"></asp:TextBox>
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnExtraChargeSave" runat="server" CssClass="button1" Text="Charge"
                            OnClick="btnExtraChargeSave_Click" />
                        &nbsp;
                        <button id="btnExtraChargeCancel" runat="server" class="button1" onclick="hideExtraCharge()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpExtraCharge" runat="server" TargetControlID="lnkPop3"
                CancelControlID="btnExtraChargeCancel" PopupControlID="pnlExtraCharge" BackgroundCssClass="modalBackground" />

            <%--Booking fee edit--%>
            <asp:ModalPopupExtender ID="mpBookingEdit" runat="server" TargetControlID="lnkPop3" CancelControlID="btnBookingEditCancel"
                PopupControlID="pnlBookingEdit" BackgroundCssClass="modalBackground" />
            <div id="pnlBookingEdit" style="display: block; width: 700px;">
                <div class="heading-hd">
                    Edit Booking Fee
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        <asp:Label runat="server" ID="Label4" Text="Update New Booking Fee"></asp:Label>
                    </div>
                    <div class="divright">
                        <%=Currency%>
                        <asp:TextBox runat="server" ID="txtbookingEdit" Text="00.00" onblur="validamount()"></asp:TextBox>
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnUpdate" runat="server" CssClass="button1" Text="Update" OnClick="btnEditBookingUpdate_Click" />
                        &nbsp;
                            <button id="btnBookingEditCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAddAttachment" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnTravellerId" runat="server" />
    <script type="text/javascript">
        function hidepopup() {
            $("#divprogress").hide();
        }
        function hideExtraCharge() {
            $("#MainContent_mpExtraCharge_backgroundElement,#pnlExtraCharge").hide();
            $("#txtExtraCharge").val('');
        }
        function hasamount(obj) {
            if ($.isNumeric($(obj).val())) {
            }
            else {
                $("#txtExtraCharge").val('');
            }
        }
        function onchange() {
            var Per = parseFloat($("#ddlrefundsel").val());
            var amount = parseFloat($("#lblhdrRefundTxt").text());
            var refund = amount * Per / 100;
            $("#hdnrfndamount").val(refund)
            $("#lblamountbind").text('<%=Currency%>' + refund.toFixed(2));
            if (Per == -1)
                $("#lblamountbind").hide();
            else
                $("#lblamountbind").show();
        }
    </script>
</asp:Content>
