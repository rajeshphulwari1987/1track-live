﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Business;
using System.Xml;
using System.Web;

namespace IR_Admin.Orders
{
    public partial class OrderSaleRecipt : Page
    {
        static long id;
        static string currency = "$";
        readonly db_1TrackEntities db = new db_1TrackEntities();
        private readonly ManageOrder _master = new ManageOrder();
        private readonly ManageBooking oBooking = new ManageBooking();
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
        public string SiteLogo = "", HtmlFileSitelog = "", Name = "", SiteHeaderColor = "", SiteUrl = "", strProductDesc = "", UserName = "", htmfile = "", OrderDate = "", OrderNumber = "";
        public string BillingAddress = "", BookingRefNO = "", HeaderDeliveryText = "", DeliveryAddress = "", HeaderText = "", ShippingMethod = "", Shipping = "";
        public string BookingFeeText = "", DeliveryFeeText = "", DiscountText = "", AdminFeeText = "", PassProtectionText = "", TotalText = "", printAthomeText = "", OrderDetailsText = "";
        public string ItemsText = "", BookingConditionText = "", BookingRef = "", EvolviTandC = "";
        public bool isEvolviBooking = false; public bool isNTVBooking = false; public bool isBene = false; public bool isTI = false;
        public string EvOtherCharges = "0";
        ManageEmailTemplate objMailTemp = new ManageEmailTemplate();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (AdminuserInfo.RoleId != new Guid())
            {
                if (Request.QueryString["id"] != null)
                {
                    if (!Int64.TryParse(Request.QueryString["id"].Trim(), out id))
                        Response.Redirect("~/Orders/OrderDetails.aspx");
                }
                if (!IsPostBack)
                {
                    if (Int64.TryParse(Request.QueryString["id"].Trim(), out id))
                    {
                        var apiTypeList = new ManageBooking().GetAllApiType(Convert.ToInt32(Request.QueryString["id"]));
                        if (apiTypeList != null)
                        {
                            isEvolviBooking = apiTypeList.isEvolvi;
                            isNTVBooking = apiTypeList.isNTV;
                            isBene = apiTypeList.isBene;
                            isTI = apiTypeList.isTI;
                        }
                        FillOrderInfo(Convert.ToInt32(Request.QueryString["id"]));
                    }
                }
            }
            else
                Response.Redirect("~/Default.aspx");
        }

        public void FillOrderInfo(Int32 OrderId)
        {
            try
            {
                var result = db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                {
                    Guid siteId = result.SiteID.Value;
                    bool isDiscountSite = _master.GetOrderDiscountVisibleBySiteId(siteId);
                    var st = db.tblSites.FirstOrDefault(x => x.ID == siteId);
                    if (st != null)
                    {
                        bool isAgentSite = false;
                        isAgentSite = st.IsAgent.HasValue ? st.IsAgent.Value : false;
                        string SiteName = st.SiteURL + "Home";
                        currency = oManageClass.GetCurrency(st.DefaultCurrencyID.HasValue ? (st.DefaultCurrencyID.Value) : new Guid());
                        string Total = string.Empty; string ShippingAmount = string.Empty; string GrandTotal = string.Empty; string BookingFee = string.Empty;
                        string NetTotal = string.Empty; string Discount = string.Empty; string AdminFee = string.Empty; string OtherCharges = string.Empty;
                        string EmailAddress = string.Empty; string PassProtection = "0.00"; string PassProtectionHtml = "", nettPrice = "";
                        string DiscountHtml = ""; bool isBeneDeliveryByMail = false;

                        HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(siteId));
                        GetEvolviTicketInfo(OrderId);
                        var lst = oBooking.GetAllCartData(OrderId).OrderBy(a => a.OrderIdentity).ToList();
                        var lst1 = (from a in lst
                                    select new
                                    {
                                        Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                        ProductDesc = (oBooking.getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName + " " + a.MiddleName + " " + a.LastName),
                                        TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                    }).ToList();
                        if (lst.Count > 0)
                        {
                            EmailAddress = lst.FirstOrDefault().EmailAddress;
                            UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                            DeliveryAddress = UserName + "<br>" +
                                lst.FirstOrDefault().Address1 + "<br>" +
                              (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                              (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                              (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                              lst.FirstOrDefault().DCountry + "<br>" +
                            lst.FirstOrDefault().Postcode + "<br>" +
                            GetBillingAddressPhoneNo(OrderId);

                            OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                            OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                            Total = (lst1.Sum(a => a.Price)).ToString();
                            ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                            BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                            Discount = _master.GetOrderDiscountByOrderId(OrderId);
                            AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(OrderId).ToString();
                            NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                                (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                                (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                                Convert.ToDecimal(AdminFee) + Convert.ToDecimal(EvOtherCharges)).ToString();
                            GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                            var billAddress = oBooking.GetBillingShippingAddress(OrderId);
                            if (billAddress != null)
                            {
                                BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                                    (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                                    (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                                    (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                                    (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                                    (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                            }
                        }
                        GetReceiptLoga();
                        ItemsText = currency + " " + Total;
                        BookingConditionText = st.SiteURL + "Booking-Conditions";
                        if (isEvolviBooking)
                        {
                            OtherCharges = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Other Charges:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + EvOtherCharges + "</td></tr>";
                            BookingRefNO = "<tr><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>Booking Ref: </strong></td><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>" + BookingRef + "</td></tr>";
                        }
                        Shipping = !string.IsNullOrEmpty(ShippingAmount) ? currency + " " + ShippingAmount : currency + " 0.00";
                        DeliveryFeeText = currency + " " + ShippingAmount;
                        BookingFeeText = currency + " " + BookingFee;
                        TotalText = currency + " " + GrandTotal;

                        string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + AdminFee.Trim() + "</td></tr>";
                        if (AdminFee == "0.00")
                            AdminFeeRow = string.Empty;
                        if (isDiscountSite)
                        {
                            AdminFeeText = AdminFeeRow;
                            if (Discount == "0.00")
                                DiscountText = "";
                            else
                            {
                                DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + Discount.Trim() + "</td></tr>";
                                DiscountText = DiscountHtml;
                            }
                        }
                        else
                        {
                            AdminFeeText = AdminFeeRow;
                            DiscountText = "";
                        }

                        Session["ShipMethod"] = result.ShippingMethod;
                        Session["ShipDesc"] = result.ShippingDescription;
                        var P2PSaleList = new ManageBooking().GetP2PSaleListByOrderID(OrderId);
                        if (isEvolviBooking)
                        {
                            EvolviTandC = st.SiteURL + "uk-ticket-collection";
                            ShippingMethod = "Ticket on collection";
                        }
                        else if (isBene)
                        {
                            if (P2PSaleList != null && !string.IsNullOrEmpty(P2PSaleList.FirstOrDefault().DeliveryOption))
                            {
                                string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                                if (P2PSaleList.Any(x => x.DeliveryOption.ToLower() == "delivery by mail"))
                                {
                                    ShippingMethod = !string.IsNullOrEmpty(shippingdesc) ? shippingdesc : "Delivery by mail";
                                    isBeneDeliveryByMail = true;
                                }
                                else
                                    ShippingMethod = P2PSaleList != null ? P2PSaleList.FirstOrDefault().DeliveryOption : "";
                            }
                        }
                        else
                        {
                            string britrailpromoTerms = isBritRailPromoPass(OrderId) ? "BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a>" : "";
                            if (Session["ShipMethod"] != null)
                            {
                                string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                                ShippingMethod = shippingdesc + britrailpromoTerms;
                            }
                            else
                                ShippingMethod = britrailpromoTerms;
                        }

                        var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                        List<GetAllCartData_Result> lstC = oBooking.GetAllCartData(OrderId).ToList();
                        var lstNew = (from a in lstC
                                      select new
                                      {
                                          a,
                                          Price = (oBooking.getPrice(a.PassSaleID, a.ProductType)),
                                          ProductDesc = a.ProductType == "P2P"
                                          ? (oBooking.getP2PDetailsForEmail(a.PassSaleID, PrintResponselist, (string)(Session["TrainType"]), isEvolviBooking, false, EvolviTandC))
                                          : (oBooking.getPassDetailsForEmailReceipt(a.PassSaleID, PrintResponselist, (string)(Session["TrainType"]), EvolviTandC)),
                                          ReceiptProductDetail = (oBooking.GetReceiptPassDescMail(a.PassSaleID, a.ProductType, PrintResponselist, (string)(Session["TrainType"]))),
                                          TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                          Terms = a.terms
                                      }).ToList();
                        string strProductDesc = "";
                        int i = 1;
                        if (lstNew.Count > 0)
                        {
                            lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                            if (lstNew.Any(x => x.a.ProductType == "P2P"))
                            {
                                if (isBeneDeliveryByMail)
                                {
                                    HeaderText = "Delivery address";
                                    HeaderDeliveryText = "Your order will be sent to:";
                                    DeliveryAddress = DeliveryAddress.Trim();
                                }
                                else
                                {
                                    HeaderText = "Tickets";
                                    if (!isEvolviBooking && !isBene && !isNTVBooking && !isTI)
                                    {
                                        HeaderDeliveryText = "Your order will be sent to:";
                                        DeliveryAddress = DeliveryAddress.Trim();
                                    }
                                    else
                                    {
                                        HeaderDeliveryText = "Your ticket collection reference:";
                                        DeliveryAddress = oBooking.getP2PDetailsForEmailTicket(OrderId, EvolviTandC, st.SiteURL + "images/pdf-istock.jpg");
                                    }
                                }
                            }
                            else
                            {
                                HeaderText = "Delivery address";
                                HeaderDeliveryText = "Your order will be sent to:";
                            }

                            foreach (var x in lstNew)
                            {
                                if (x.a.ProductType != "P2P")
                                {
                                    //if (isAgentSite)
                                    //    nettPrice = "Net Amount : " + currency + " " + (_master.GetEurailAndNonEurailPrice(x.a.PassSaleID.Value)).ToString("F");
                                    strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth '>";
                                    string dobtext = "";
                                    if (x.a.DOB.HasValue)
                                        dobtext += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Date of Birth :</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + x.a.DOB.Value.ToString("dd/MMM/yyyy") + "</td><td width='33%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                                    if (!string.IsNullOrEmpty(x.a.NATIONALITY) && x.a.NATIONALITY != "0")
                                        dobtext += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Country of residence:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + x.a.NATIONALITY + "</td><td width='33%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                                    if (!string.IsNullOrEmpty(x.a.PassportNo))
                                        dobtext += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Passport number:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + x.a.PassportNo + "</td><td width='33%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";

                                    strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " <strong>" + x.a.LastName + "</strong>").Replace("##NetAmount##", nettPrice).Replace("##DOB##", dobtext);
                                    strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Pass start date:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + (x.a.PassStartDate.HasValue ? x.a.PassStartDate.Value.ToString("dd/MMM/yyyy") : string.Empty) + "</td><td width='33%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";
                                    strProductDesc += (Session["CollectStation"] != null ? "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + Session["CollectStation"].ToString() + "</td><td width='33%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>" : "");

                                    string ElectronicNote = oBooking.GetElectronicNote(x.a.PassSaleID.Value);
                                    if (ElectronicNote.Length > 0)
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #ff0000;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + ElectronicNote + "</td><td width='33%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>";

                                    strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Ticket Protection:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" +
                                        (x.TktPrtCharge > 0 ? "Yes" : "No") + "</td><td width='22%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>" + currency + " " + Convert.ToString(x.TktPrtCharge) + "</strong></td></tr>";
                                    strProductDesc += "</table></td></tr></table>";

                                    PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                                }
                                else
                                {
                                    //if (isAgentSite)
                                    //{
                                    //    var p2p = _master.GetP2PTckP(OrderId);
                                    //    decimal p2PTckPCharge = p2p.Sum(tp => tp.TicketProtection);
                                    //    nettPrice = "Net Amount : " + currency + " " + (Convert.ToDecimal(Total) - Convert.ToDecimal(p2PTckPCharge)).ToString();
                                    //}
                                    strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                                    strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", nettPrice).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? "<strong>" + x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] + "</strong>" : ""));
                                    strProductDesc += (HttpContext.Current.Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + HttpContext.Current.Session["CollectStation"] == null ? "" : HttpContext.Current.Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong></strong></td></tr>" : "");
                                    if (P2PSaleList != null && P2PSaleList.Count > 0 && P2PSaleList.Any(z => z.ApiName.ToUpper() == "ITALIA"))
                                    {
                                        if (string.IsNullOrEmpty(P2PSaleList.FirstOrDefault(z => z.ID == x.a.PassSaleID).PdfURL))
                                            strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Important:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>This PNR is issued as a ticket on depart and must be printed from one of the self-service ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine.</td></tr>";
                                    }
                                    PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                                    if (x.Terms != null)
                                        strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + HttpContext.Current.Server.HtmlDecode(x.Terms) + "</td></tr>";
                                    if (isEvolviBooking)
                                    {
                                        var P2PData = db.tblP2PSale.FirstOrDefault(Q => Q.ID == x.a.PassSaleID.Value);
                                        if (P2PData != null)
                                        {
                                            if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                            {
                                                strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>";
                                                foreach (var item in P2PData.EvSleeper.Split('@'))
                                                {
                                                    string ss = item.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                                    strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                                }
                                                strProductDesc += "</td></tr>";
                                            }
                                        }
                                    }
                                    strProductDesc += "</table></td></tr></table>";
                                }
                                i++;
                            }
                        }

                        PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'>" + currency + " " + PassProtection.Trim() + "</td></tr>";
                        PassProtectionText = PassProtection == "0.00" ? "" : PassProtectionHtml;
                        OrderDetailsText = strProductDesc;
                        if (result.TrvType == "P2P")
                        {
                            var getDeliveryType = oBooking.getDeliveryOption(OrderId);
                            if (getDeliveryType != null && getDeliveryType == "Print at Home")
                            {
                                string printAthome = "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth '><tr><td style='padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;padding-top: 7px;'colspan='2'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</td></tr></table>";
                                printAthomeText = printAthome;

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        string GetCountry(Guid cid)
        {
            tblCountriesMst cont = db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == cid);
            return cont != null ? cont.CountryName : "";
        }

        public void GetReceiptLoga()
        {
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    SiteLogo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    SiteLogo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    SiteLogo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    SiteLogo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        public string GetBillingAddressPhoneNo(long OrderID)
        {
            string BillingPhoneNo = string.Empty;
            var result = db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            if (result != null)
            {
                BillingPhoneNo += !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
                BillingPhoneNo += result.IsVisibleEmailAddress ? "<br/>" + result.EmailAddress : "";
            }
            return BillingPhoneNo;
        }

        public string GetApiName()
        {
            if (isTI)
                return "TrainItalia";
            else if (isBene)
                return "BeNe";
            else if (isNTVBooking)
                return "Ntv";
            else if (isEvolviBooking)
                return "Evolvi";
            else
                return "";
        }

        bool isBritRailPromoPass(long orderid)
        {
            try
            {
                foreach (var result in db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
                {
                    bool isPromo = (db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                    if (isPromo)
                        return true;
                }
                return false;
            }
            catch { return false; }
        }

        public void GetEvolviTicketInfo(long OrderNo)
        {
            try
            {
                var evChargesList = oBooking.GetEvolviChargesByOrderId(OrderNo);
                if (evChargesList != null)
                {
                    EvOtherCharges = ((evChargesList.TicketChange + evChargesList.CreditCardCharge) - (evChargesList.Discount)).ToString("F2");
                    if (!string.IsNullOrEmpty(evChargesList.BookingRef) && !string.IsNullOrEmpty(evChargesList.TODRef))
                    {
                        BookingRef = evChargesList.BookingRef;
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

    }
}