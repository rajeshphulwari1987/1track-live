﻿#region Using
using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Xml;
using System.Web.UI;

#endregion

namespace IR_Admin.Orders
{
    public partial class OrderRefund : System.Web.UI.Page
    {
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId, _prdCurrencyId, _siteCurrencyId;
        public string Currency = "$";
        private int _tckProtection;
        private decimal _netPrice;
        private bool _isCancel;
        private string htmfile = string.Empty;
        private readonly Masters _masterPage = new Masters();
        readonly private ManageOrder _master = new ManageOrder();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public bool IsEurail = false;
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["prdId"] != null)
                {
                    HasEurailPass();
                    bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                    if (IsEurail || IsFullRefund)
                    {
                        lblrefundnote.Text = "For Eurail, percentage to be refunded should ALWAYS be set at 100%.";
                        rowcancellation.Visible = false;
                        hdnRefundType.Value = "true";
                    }
                    else
                        lblrefundnote.Text = "Should there be a 15% cancellation charge attached to a ticket or pass, you need to select 85% as the ‘percentage to be refunded’.";
                    var prdId = Request.QueryString["prdId"];
                    BindRefund(Guid.Parse(prdId));
                    BindPercentage();
                }
            }
        }

        public void BindRefund(Guid prdId)
        {
            var result = _master.GetRefundDetails(prdId);
            if (result != null)
            {
                lblalreadyrefund.Text = _master.GetOrderRefundsById(Guid.Parse(Request.QueryString["prdId"])).ToString("F");
                lblOrderID.Text = result.OrderID.ToString(CultureInfo.InvariantCulture);
                lblProduct.Text = result.ProductName;
                lblTraveller.Text = result.TravellerName;
                lblPrice.Text = result.Price.ToString(CultureInfo.InvariantCulture);
                Currency = _master.GetCurrcyBySiteId(Guid.Parse(result.SiteID.ToString()));
                ViewState["SiteID"] = result.SiteID;
                ViewState["Currency"] = Currency;
                ViewState["tckProtection"] = (int)result.TckProtection;
                ViewState["prdCurrencyId"] = Guid.Parse(result.CurrencyID.ToString());
            }
        }

        public void BindPercentage()
        {
            var rPerc = _master.BindRefundPerc();
            ddlRefundPerc.DataSource = rPerc;
            ddlRefundPerc.DataTextField = "Description";
            ddlRefundPerc.DataValueField = "ID";
            ddlRefundPerc.DataBind();
            ddlRefundPerc.Items.Insert(0, new ListItem("-Select Refund-", "-1"));
            bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
            if (IsEurail || IsFullRefund)
            {
                ddlRefundPerc.SelectedValue = "808fdb35-98ef-4428-966c-53bc27fc3093";
                tr_alreadyrefund.Visible = tr_AdditionalRefund.Visible = true;
            }
            else
            {
                ddlRefundPerc.SelectedIndex = 86;
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "GetMaxRefund", "GetMaxRefund();", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderDetails.aspx?id=" + lblOrderID.Text);
        }

        protected void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblOrderID.Text != null)
                {
                    bool isvalid = true;
                    lblRefundPrice.Text = hdnrefund.Value;
                    byte StatusId = 1;
                    _siteId = Master.SiteID;
                    long ordId = Convert.ToInt64(lblOrderID.Text);
                    new ManageBooking().UpdateOrderStatus(9, Convert.ToInt64(ordId));
                    var ord = _master.GetOrderbyOrdId(ordId);
                    var prdId = Guid.Parse(Request.QueryString["prdId"]);
                    var price = Convert.ToDecimal(lblPrice.Text);
                    var refundedPrice = Convert.ToDecimal(lblRefundPrice.Text);
                    var cancelFee = Convert.ToDecimal(15.00);
                    _tckProtection = Convert.ToInt32(ViewState["tckProtection"].ToString());
                    _prdCurrencyId = (Guid)ViewState["prdCurrencyId"];
                    var sID = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    if (sID != null)
                        _siteCurrencyId = Guid.Parse(sID.DefaultCurrencyID.ToString());
                    _netPrice = Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(price, _siteId, _prdCurrencyId, _siteCurrencyId).ToString("F"));
                    var perc = _master.GetRefundValue(Guid.Parse(ddlRefundPerc.SelectedValue));
                    decimal refundedPerc = Convert.ToInt32(perc.PercentageRefund);
                    bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                    HasEurailPass();
                    if (IsEurail || IsFullRefund)
                    {
                        refundedPrice = (price * refundedPerc) / 100;
                        StatusId = 2;
                        cancelFee = 0;
                    }
                    else if (perc.PercentageRefund > 85)
                    {
                        if (chkCancel.Checked)
                            ShowMessage(2, "With 15% cancellation charge, you need to select 85% as the percentage to be refunded");
                        ShowMessage(2, "You need to select 85% as the percentage to be refunded");
                        isvalid = false;
                    }
                    else
                    {
                        StatusId = 2;
                        if (chkCancel.Checked)
                        {
                            StatusId = 3;
                            _isCancel = true;
                            refundedPrice = (price * refundedPerc) / 100;
                        }
                    }

                    if (isvalid)
                    {
                        tblOrderRefund _OrderRefund = new tblOrderRefund
                        {
                            ID = Guid.NewGuid(),
                            OrderID = ordId,
                            UserID = ord.UserID,
                            AdminUserID = AdminuserInfo.UserID,
                            ProductID = prdId,
                            ProductPriceID = prdId,
                            OrginalProductAmount = price,
                            OrginalProductAmountCurrencyID = _prdCurrencyId,
                            LocalProductAmount = _netPrice,
                            LocalProductAmountCurrencyId = _siteCurrencyId,
                            DateTimeStamp = ord.CreatedOn,
                            TicketProtection = _tckProtection,
                            RefundDateTime = DateTime.Now,
                            Status = StatusId,
                            Cancellation = _isCancel,
                            TotalProductRefund = refundedPrice,
                            TotalProductRefundPercentage = refundedPerc,
                            CancellationFee = cancelFee,
                            Partial = false,
                            PassProtectionRefunded = true
                        };
                        _master.AddUpdateRefund(_OrderRefund);
                        //RefundEMail();
                        Response.Redirect("OrderDetails.aspx?id=" + lblOrderID.Text);
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void RefundEMail()
        {
            try
            {
                Currency = _master.GetCurrcyBySiteId(Guid.Parse(ViewState["SiteID"].ToString()));
                String RCurrency = Currency + " ";
                _siteId = Master.SiteID;
                DateTime RefundDate = DateTime.Now;
                string PassList = string.Empty;
                var smtpClient = new SmtpClient();
                var message = new MailMessage();

                var orderID = Convert.ToInt32(lblOrderID.Text);
                string EmailAddress = _master.GetbillingEmail(orderID);
                string BccEmailAddress = _master.GetBccEmailBySiteid(_siteId);
                string Subject = "Order Refund Confirmation #" + orderID;
                var theme = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (theme != null && (theme.IsSTA ?? false))
                    htmfile = Server.MapPath("~/Orders/BlueRefundTemplate.htm");
                else
                    htmfile = Server.MapPath("~/Orders/RefundTemplate.htm");

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml;

                //***********************************//
                var Orderdata = _master.GetOrderbyOrdId(orderID);
                if (Orderdata != null)
                {
                    body = body.Replace("##OrderNo##", orderID.ToString());
                    body = body.Replace("##SA##", RCurrency + Orderdata.ShippingAmount ?? "0.00");
                    body = body.Replace("##BA##", RCurrency + Orderdata.BookingFee ?? "0.00");
                    body = body.Replace("##AA##", RCurrency + Orderdata.AdminFee);
                    var data = _master.GetOrderAdminRefundById(orderID);
                    if (data != null)
                    {
                        body = body.Replace("##ShippingCost##", RCurrency + data.ShippingFee.ToString("F"));
                        body = body.Replace("##BookingFee##", RCurrency + data.BookingFee.ToString("F"));
                        body = body.Replace("##AdminFee##", RCurrency + data.AdminFee.ToString("F"));
                        RefundDate = data.CreatedOn;
                    }
                    else
                    {
                        body = body.Replace("##ShippingCost##", RCurrency + "0.00");
                        body = body.Replace("##BookingFee##", RCurrency + "0.00");
                        body = body.Replace("##AdminFee##", RCurrency + "0.00");
                    }
                    var refundDetailsPass = _master.GetRefundDetails(Convert.ToInt64(orderID));
                    if (Orderdata.TrvType == "Pass" && refundDetailsPass != null)
                    {
                        foreach (var ty in refundDetailsPass.Select((x, i) => new { Value = x, Index = i }))
                        {
                            string TravelerTblRow = "<tr><td class='left-header' colspan='2'><hr />Passenger: ##PNO##<hr /></td></tr><tr><td class='first'>Product Name:</td><td class='secound'>##ProductName##</td></tr><tr><td class='first'>Name:</td><td class='secound'>##Name##</td></tr><tr><td class='first'>Gross Price:</td><td class='secound'>##GrossPrice##</td></tr><tr><td class='first'>Refund Price:</td><td class='secound'>##RefundPrice##</td></tr>";
                            if (RefundDate < ty.Value.RefundDate)
                                RefundDate = ty.Value.RefundDate;
                            TravelerTblRow = TravelerTblRow.Replace("##PNO##", (ty.Index + 1).ToString());
                            TravelerTblRow = TravelerTblRow.Replace("##ProductName##", ty.Value.Product);
                            TravelerTblRow = TravelerTblRow.Replace("##Name##", ty.Value.Traveller);
                            TravelerTblRow = TravelerTblRow.Replace("##GrossPrice##", RCurrency + ty.Value.Price.ToString("F"));
                            TravelerTblRow = TravelerTblRow.Replace("##RefundPrice##", RCurrency + ty.Value.Refund.ToString("F"));
                            PassList += TravelerTblRow;
                        }
                    }
                }
                body = body.Replace("##BindPassTravelerRefund##", PassList);
                body = body.Replace("##RefundDate##", RefundDate.ToString("dd/MMM/yyyy"));
                body = body.Replace("#Blanck#", "&nbsp;");
                //**********************************//

                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(_siteId);
                if (result != null && !string.IsNullOrEmpty(EmailAddress))
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(EmailAddress);
                    if (!string.IsNullOrEmpty(BccEmailAddress))
                        message.Bcc.Add(BccEmailAddress);
                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to send E-mail Refund confimation!");
            }
        }

        public void HasEurailPass()
        {
            var prdId = Guid.Parse(Request.QueryString["prdId"]);
            IsEurail = (from TPS in _db.tblPassSales
                        join TC in _db.tblCategories on TPS.CategoryID equals TC.ID
                        where TPS.ID == prdId
                        select new { TC }).FirstOrDefault().TC.IsPrintQueueEnable;
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnRefund2_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblOrderID.Text != null && !string.IsNullOrEmpty(txtAdditionalRefund.Text))
                {
                    bool isvalid = true;
                    byte StatusId = 1;
                    _siteId = Master.SiteID;
                    long ordId = Convert.ToInt64(lblOrderID.Text);
                    new ManageBooking().UpdateOrderStatus(9, Convert.ToInt64(ordId));
                    var ord = _master.GetOrderbyOrdId(ordId);
                    var prdId = Guid.Parse(Request.QueryString["prdId"]);
                    var price = Convert.ToDecimal(lblPrice.Text);
                    var refundedPrice = Convert.ToDecimal(txtAdditionalRefund.Text);
                    var cancelFee = Convert.ToDecimal(15.00);
                    _tckProtection = Convert.ToInt32(ViewState["tckProtection"].ToString());
                    _prdCurrencyId = (Guid)ViewState["prdCurrencyId"];
                    var sID = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    if (sID != null)
                        _siteCurrencyId = Guid.Parse(sID.DefaultCurrencyID.ToString());
                    _netPrice = Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(price, _siteId, _prdCurrencyId, _siteCurrencyId).ToString("F"));
                    var perc = _master.GetRefundValue(Guid.Parse(ddlRefundPerc.SelectedValue));
                    decimal refundedPerc = 0;
                    bool IsFullRefund = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId).IsAllowHundredPercentRefund;
                    HasEurailPass();
                    if (IsEurail || IsFullRefund)
                    {
                        StatusId = 2;
                        cancelFee = 0;
                        refundedPerc = refundedPrice * 100 / Convert.ToDecimal(lblPrice.Text);
                    }
                    if (isvalid)
                    {
                        tblOrderRefund _OrderRefund = new tblOrderRefund
                        {
                            ID = Guid.NewGuid(),
                            OrderID = ordId,
                            UserID = ord.UserID,
                            AdminUserID = AdminuserInfo.UserID,
                            ProductID = prdId,
                            ProductPriceID = prdId,
                            OrginalProductAmount = price,
                            OrginalProductAmountCurrencyID = _prdCurrencyId,
                            LocalProductAmount = _netPrice,
                            LocalProductAmountCurrencyId = _siteCurrencyId,
                            DateTimeStamp = ord.CreatedOn,
                            TicketProtection = _tckProtection,
                            RefundDateTime = DateTime.Now,
                            Status = StatusId,
                            Cancellation = _isCancel,
                            TotalProductRefund = refundedPrice,
                            TotalProductRefundPercentage = refundedPerc,
                            CancellationFee = cancelFee,
                            Partial = false,
                            PassProtectionRefunded = true
                        };
                        _master.AddUpdateRefund(_OrderRefund);
                        //RefundEMail();
                        Response.Redirect("OrderDetails.aspx?id=" + lblOrderID.Text);
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }
}