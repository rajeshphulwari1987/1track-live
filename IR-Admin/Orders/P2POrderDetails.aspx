﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="P2POrderDetails.aspx.cs"
    Inherits="IR_Admin.Orders.P2POrderDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .heading-hd {
            text-transform: uppercase;
        }

        .False {
            display: none;
        }

        .lblname1 {
            margin-right: 63px;
            margin-left: 5px;
        }

        .lblname2 {
            margin-left: 5px;
            margin-right: 9px;
        }

        .lblname3 {
            margin-left: 15px;
            margin-right: 38px;
        }

        .lblname4 {
            margin-left: 15px;
            margin-right: 15px;
        }

        .popupselect {
            font-size: 13px;
            line-height: 2;
        }

        .lbltxt1 {
            margin-left: 30px;
        }

        .lbltxt2 {
            margin-left: 50px;
        }

        .lbltxt3 {
            margin-left: 50px;
        }

        .lbltxt4 {
            margin-left: 58px;
        }

        #pnlJourney {
            font-size: 13px !important;
            background: none repeat scroll 0 0 #EEEEEE !important;
            width: 750px !important;
            border-radius: 7px 7px 0 0 !important;
            top: 30px !important;
            height: 580px;
            overflow-y: auto;
        }

        .divleftjourney {
            padding-top: 5px;
            border-bottom: 1px dashed #B1B1B1;
            float: left;
            width: 150px;
            margin-left: 10px;
            height: 30px;
        }

        .divrightjourney {
            padding-top: 5px;
            border-bottom: 1px dashed #B1B1B1;
            float: left;
            width: 572px;
            height: 30px;
        }

        .grid-head2 {
            height: auto;
            line-height: 16px;
        }

        .ui-datepicker {
            background-color: White;
        }

        .number {
        }

        .bordernone {
            border: none;
        }

        .agentsec {
            display: none;
        }

        .ajax__calendar_day, .ajax__calendar_dayname, .ajax__calendar_title, .ajax__calendar_footer, .ajax__calendar_month, .ajax__calendar_year {
            font-size: 11px !important;
        }

        .ajax__calendar_today, .ajax__calendar_footer {
            margin-bottom: 5px;
        }

        .toogle {
            display: block !important;
        }

        .gridP {
            background: #666;
        }

        .grid-sec2 {
            width: 948px;
        }

        .clsBtn {
            min-width: 50px;
        }

        .clsMrgTop10 {
            margin-top: 10px;
            float: left;
        }

        .clsRed {
            color: red;
        }

        .clsGreen {
            color: green;
        }

        .clsbckRed {
            background-color: red !important;
            color: #FFF;
        }

        .clsbckGreen {
            background-color: green !important;
            color: #FFF;
        }
    </style>
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <%--<link href="../Styles/FaqtblQAstyle.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.core.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/DatePicker/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        var count = 0;
        var countKey = 1;
        var conditionone = 0;
        var conditiontwo = 0;
        var tabkey = 0;
        $(document).ready(function () {
            if ('<%=IsTrainTickts%>' == 'False')
                $(".TrueFalse").removeAttr('class').addClass('False');
            $(".txtnumber").keyup(function (event) {
                var data = $(this).val();
                if (data != '') {
                    var isAmount = $.isNumeric(data);
                    if (!isAmount) {
                        alert("Amount is not Valid!");
                        $(this).val('');
                    }
                }
            });
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');

            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
                else
                    $("[id*=txtOrderId]").trigger('change');
            }
        }
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('An attempt was made to submit this form more than once; this extra attempt will be ignored.');
                return false;
            }
        }
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46 && KeyID == 250)
                return true;
            return false;
        }
        function keynumcheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57)
                return false;
            return true;
        }
        function hidepopup() {
            $("#divprogress").hide();
        }
        $(document).ready(function () {
            if ($("#txtFrom").val() != '') {
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            $(window).click(function (event) {
                $('#_bindDivData').remove();
            });
            $(window).keydown(function (event) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            });
        });
        function Load() {
            $(document).ready(function () {
                //40,38
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;
                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
        }
        function selectpopup(e) {
            $(document).ready(function () {
                if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                    var $this = $(e);
                    var data = $this.val();
                    var station = '';
                    var hostName = window.location.host;
                    var url = "http://" + hostName;
                    if (window.location.toString().indexOf("https:") >= 0)
                        url = "https://" + hostName;
                    if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                        $('#spantxtFrom').text('');
                        $('#spantxtTo').text('');
                    }
                    var filter = $("#span" + $this.attr('id') + "").text();
                    if (filter == "" && $this.val() != "")
                        filter = $("#hdnFilter").val();
                    $("#hdnFilter").val(filter);
                    if ($this.attr('id') == 'txtTo') {
                        station = $("#txtFrom").val();
                    }
                    else {
                        station = $("#txtTo").val();
                    }
                    if (hostName == "localhost")
                        url = "http://" + hostName + "/IR-Admin";
                    else if (hostName == "admin.1tracktest.com")
                        url = "http://" + hostName;
                    var hostUrl = url + "/StationList.asmx/getStationsXList";
                    data = data.replace(/[']/g, "♥");
                    var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
                    $.ajax({
                        type: "POST",
                        url: hostUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                        success: function (msg) {
                            $('#_bindDivData').remove();
                            var lentxt = data.length;
                            $.each(msg.d, function (key, value) {
                                var splitdata = value.split('ñ');
                                var lenfull = splitdata[0].length;;
                                var txtupper = splitdata[0].substring(0, lentxt);
                                var txtlover = splitdata[0].substring(lentxt, lenfull);
                                $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                            });
                            $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                        },
                        error: function () {
                            //     alert("Wait...");
                        }
                    });
                }
            });
        }
        function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
            $('#_bindDivData').remove();
        }
        function CheckCardVal(sender, args) {
            if (document.getElementById('MainContent_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
        function validamount() {
            if (!$.isNumeric($("[id*=txtbookingEdit]").val()))
                $("[id*=txtbookingEdit]").val("00.00").select();
        }
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <div style="width: 100%" runat="server" id="DivOrderDetail">
        <h2>All Orders</h2>
        <asp:UpdatePanel ID="Upnl1" runat="server">
            <ContentTemplate>
                <div class="full mr-tp1">
                    <ul class="list">
                        <li><a id="aList" href="P2POrderDetails.aspx" class="current">List</a></li>
                    </ul>
                    <!-- tab "panes" -->
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                            <asp:Label ID="lblSuccessMsg" runat="server" />
                        </div>
                        <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="full mr-tp1">
                        <div class="panes">
                            <div id="divlist" runat="server">
                                <div id="dvSearch" class="searchDiv" runat="server">
                                    <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                                    <table width="100%" style="line-height: 20px; font-size: 13px;">
                                        <tr>
                                            <td>
                                                <div runat="server" id="divsyswide" visible="false">
                                                    System wide search<asp:CheckBox runat="server" ID="chkSysWide" OnCheckedChanged="btnSubmit_Click"
                                                        AutoPostBack="true" />
                                                </div>
                                            </td>
                                            <td>From Date:
                                            </td>
                                            <td>To Date:
                                            </td>
                                            <td>Departure/Booking Date:
                                            </td>
                                            <td>PNR Number</td>
                                        </tr>
                                        <tr>
                                            <td>Date Range:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static"
                                                    Width="151" />
                                                <asp:CalendarExtender ID="cal1" runat="server" TargetControlID="txtStartDate" Format="dd/MM/yyyy"
                                                    PopupButtonID="imgCal1" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                                <asp:Image ID="imgCal1" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                                                    AlternateText="Calendar" Style="position: absolute; margin: -3px 0 0 2px;" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static"
                                                    Width="151" />
                                                <asp:CalendarExtender ID="cal2" runat="server" TargetControlID="txtLastDate" Format="dd/MM/yyyy"
                                                    PopupButtonID="imgCal2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                                <asp:Image ID="imgCal2" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                                                    AlternateText="Calendar" Style="position: absolute; margin: -3px 0 0 2px;" />
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddltypeDate" Width="200" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddltypeDate_SelectIndexChange">
                                                    <asp:ListItem Value="1">Departure Date</asp:ListItem>
                                                    <asp:ListItem Value="2" Selected="True">Booking Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPNRNumber" runat="server" Width="180px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                    ToolTip="Enter Pnr Number." />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Order No :
                                            </td>
                                            <td>Reference Number:
                                            </td>
                                            <td>Passenger Last Name:
                                            </td>
                                            <td>Order Status :
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtOrderId" runat="server" Width="180px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                    ToolTip="Enter Order Number." />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRef" runat="server" Width="180px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                    ToolTip="Enter Reference Number." />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" Width="180px" AutoPostBack="true" OnTextChanged="btnSubmit_Click"
                                                    ToolTip="Enter Passenger Name." />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlOrderSt" runat="server" ValidationGroup="rv" Width="200px"
                                                    AutoPostBack="true" OnTextChanged="btnSubmit_Click" ToolTip="Select Order Status." />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:GridView ID="grdOrders" runat="server" AutoGenerateColumns="False" PagerStyle-CssClass="paging"
                                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" AllowSorting="true"
                                    OnSorting="grdOrders_Sorting">
                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        Record not found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="OrderNo.">
                                            <ItemTemplate>
                                                <%#Eval("OrderID")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reference">
                                            <ItemTemplate>
                                                <%#Eval("Reference")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Order status">
                                            <ItemTemplate>
                                                <%#Eval("BookingStatus")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DeliveryMethod">
                                            <ItemTemplate>
                                                <%#Eval("DeliveryOption")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Station From-To">
                                            <ItemTemplate>
                                                <%#Eval("From") + " - " + Eval("To")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Booking Date" SortExpression="BookingDate">
                                            <ItemTemplate>
                                                <%# Eval("BookingDate", "{0: MMM dd, yyyy}")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dept.Date" SortExpression="DeptDate">
                                            <ItemTemplate>
                                                <%# Eval("DeptDate", "{0: MMM dd, yyyy}")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dept. Time">
                                            <ItemTemplate>
                                                <%# Eval("DepTime")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Passenger">
                                            <ItemTemplate>
                                                &nbsp;&nbsp;&nbsp;&nbsp;<%# Eval("LastName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View Details">
                                            <ItemTemplate>
                                                <a href='P2POrderDetails.aspx?id=<%#Eval("OrderID")%>' title="">
                                                    <img alt="View" src="../images/<%#Eval("OrderViewImgURL")%>" title="View" /></a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                                </asp:GridView>
                                <table width="100%">
                                    <tr class="paging">
                                        <td style="float: left">
                                            <asp:LinkButton ID="lnkPrevious" Text='<< Previous' runat="server" OnClick="lnkPrevious_Click" />
                                        </td>
                                        <td style="float: left;">
                                            <asp:LinkButton ID="lnkPrepaging" Text='...' runat="server" OnClick="lnkPrepaging_Click" />
                                        </td>
                                        <asp:Repeater ID="DLPageCountItem" runat="server">
                                            <ItemTemplate>
                                                <td style="float: left">
                                                    <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                        Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td style="float: left;">
                                            <asp:LinkButton ID="lnknextpaging" Text='...' runat="server" OnClick="lnknextpaging_Click" />
                                        </td>
                                        <td style="float: left;">
                                            <asp:LinkButton ID="lnkNext" Text='Next >>' runat="server" OnClick="lnkNext_Click" />
                                        </td>
                                        <td style="float: right;">
                                            <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divDetail" runat="server">
                                <table class="tblMainSection">
                                    <tr>
                                        <td>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Orders</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <table cellpadding="0" cellspacing="0" class="table-stock">
                                                        <tr>
                                                            <td colspan="3" class="heading-color">
                                                                <strong>Order Number: </strong>
                                                                <asp:Label runat="server" ID="lblorder"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">
                                                                <strong>Status: </strong>
                                                                <asp:Label runat="server" ID="lblStatusName"></asp:Label>
                                                                <asp:LinkButton ID="lnkEditStatus" runat="server" OnClick="lnkEditStatus_Click" CssClass="heading-color">Edit</asp:LinkButton>
                                                            </td>
                                                            <td width="35%">
                                                                <strong>Creation Date:</strong>
                                                                <asp:Label runat="server" ID="lblCreatedOn"></asp:Label>
                                                            </td>
                                                            <td width="35%">
                                                                <strong>Site:</strong>
                                                                <asp:Label runat="server" ID="lblSiteName"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Ip Address:</strong>
                                                                <asp:Label runat="server" ID="lblIpAddress"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <strong>Office Name:</strong>
                                                                <asp:Label runat="server" ID="lblOfficeName"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <strong>Shipping Amount:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblShippingAmount"></asp:Label>
                                                                <asp:LinkButton ID="lnkRefShippingFee" Style="color: #b53859; float: right; margin-right: 20px;"
                                                                    Text="Refund" runat="server" OnClick="lnkRefShippingFee_Click"></asp:LinkButton>
                                                                <div style="clear: both;">
                                                                </div>
                                                                <asp:LinkButton ID="lnkEditShipMethod" runat="server" OnClick="lnkEditShipMethod_Click"
                                                                    Style="float: right;" CssClass="heading-color">Edit Shipping method</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Amex Fee:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblAmexPrice"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <strong>Booking Fee:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblBookingFee"></asp:Label>
                                                                <asp:LinkButton ID="lnkeditbooking" CssClass="heading-color" Text="Edit" runat="server" OnClick="lnkeditbooking_Click"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkRefBookingFee" CssClass="heading-color" Style="float: right; margin-right: 20px;"
                                                                    Text="Refund" runat="server" OnClick="lnkRefBookingFee_Click"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <strong>Commission:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblCommission"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="shownetparice" runat="server">
                                                                    <strong>Net Price: </strong>
                                                                    <%=Currency%>
                                                                    <asp:Label runat="server" ID="lblNetPrice"></asp:Label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <strong>Gross Price:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblGrossPrice"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <strong>Ticket Protection:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblTicketProtection"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Grand Total:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblGrandTotal"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <strong>Discount Amount:</strong>
                                                                <%=Currency%>
                                                                <asp:Label runat="server" ID="lblDiscount"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <strong>Admin Fee:</strong>
                                                                <%=Currency.ToString()%>
                                                                <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
                                                                <asp:LinkButton ID="lnkRefAdminFee" CssClass="heading-color" Style="float: right; margin-right: 20px;"
                                                                    Text="Refund" runat="server" OnClick="lnkRefAdminFee_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="tdPayment" runat="server" class="clsbckRed">
                                                                <strong>Payment Taken:</strong>
                                                                <asp:Label ID="lblPaymentTaken2" runat="server" />

                                                            </td>
                                                            <td></td>
                                                            <td align="center">
                                                                <a id="lnkPrintReceipt" runat="server" cssclass="heading-color" target="_Receipt">Print
                                                                    Receipt</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong>Notes:</strong>
                                                                <asp:Label runat="server" ID="lblNote"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Journey Information</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <asp:Repeater ID="rptJourney" runat="server" OnItemCommand="rptJourney_ItemCommand"
                                                        OnItemDataBound="rptJourney_ItemDataBound">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="table-stock">
                                                                <tr>
                                                                    <td>
                                                                        <strong>Train No: </strong>
                                                                        <%#Eval("TrainNo")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>From:</strong>
                                                                        <%#Eval("From")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>To:</strong>
                                                                        <%#Eval("To")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Departure Date-Time:</strong>
                                                                        <%#Eval("DateTimeDepature", "{0:dd/MMM/yyyy}")%>
                                                                        <span style="margin-left: 5px;">
                                                                            <%#Eval("DepartureTime")%>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Arrival Date-Time:</strong>
                                                                        <%#Eval("DateTimeArrival", "{0:dd/MMM/yyyy}")%>
                                                                        <span style="margin-left: 5px;">
                                                                            <%#Eval("ArrivalTime")%></span>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Passenger:</strong>
                                                                        <%#Eval("Passenger")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Net Price:</strong>
                                                                        <%# Eval("Symbol")%>&nbsp;<%# Eval("NetPrice")%><td>
                                                                            <strong>Class:</strong>
                                                                            <%#Eval("Class")%>
                                                                        </td>
                                                                        <td>
                                                                            <strong>Fare Name:</strong>
                                                                            <%#Eval("FareName")%>
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>PNR No:</strong>
                                                                        <%#Eval("ReservationCode")%>

                                                                        <asp:HiddenField ID="hdnPnr" runat="server" Value='<%#Eval("ReservationCode")%>' />
                                                                        <asp:LinkButton ID="lnkUpdatePnrNo" runat="server" CommandName="UpdatePnrNo"
                                                                            CommandArgument='<%#Eval("Id")%>' CssClass="heading-color">Edit</asp:LinkButton>
                                                                        <br />
                                                                        <div style='<%#Eval("isOldReservationCode").ToString()=="True" ? "": "display: none" %>'><strong>Previous PNR No: </strong><del><%#Eval("OldReservationCode")%></del></div>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Pin No:</strong>
                                                                        <%#Eval("PinNumber")%>
                                                                    </td>
                                                                    <td>
                                                                        <b>Cxl/Extra Charge:</b>
                                                                        <asp:LinkButton ID="lnkEditExtraCharge" runat="server" CommandName="AdminExtraCharge"
                                                                            CommandArgument='<%#Eval("Id")%>' CssClass="heading-color">Apply</asp:LinkButton>
                                                                        <asp:Label runat="server" ID="lblExtraChargeAmount"></asp:Label>
                                                                        <asp:LinkButton ID="lnkRefund" runat="server" CssClass="heading-color" Style="float: right; padding-right: 10px;">Refund</asp:LinkButton>
                                                                        <asp:HiddenField ID="hdnPassP2PSaleID" runat="server" Value='<%#Eval("PassP2PSaleID")%>' />
                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Eval("ID")%>' />
                                                                        <asp:HiddenField ID="hdnIsBENE" runat="server" Value='<%#Eval("IsBENE")%>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Delivery Option:</strong>
                                                                        <asp:LinkButton ID="lnkEditShippingMethod" runat="server" CommandName="EditShipping" CssClass="heading-color"
                                                                            CommandArgument='<%#Eval("Id")%>' Style="float: right; margin-left: 10px;" Text="Edit"
                                                                            ToolTip="Edit" />
                                                                        <%#Eval("DeliveryOption")%>
                                                                    </td>
                                                                    <td>
                                                                        <div id="divPrintURL" runat="server" visible='<%#Eval("IsNotRefundedFull")%>'>
                                                                            <asp:HiddenField runat="server" ID="hdnPdfURL" Value='<%#Eval("PdfURL")%>' />
                                                                            <strong>URL:</strong>
                                                                            <%#Eval("PdfURL")%>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div id="divAPi" runat="server" visible='<%#Eval("ReservationCode") != null && Eval("ReservationCode").ToString().Length==6 %>'>
                                                                            <asp:LinkButton ID="lnkTiRefund" runat="server" CommandName="TiRefund" CssClass="heading-color"
                                                                                Text="Refund through API" ToolTip='<%#Eval("ReservationCode")%>' />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>My FIP Card Number:</strong>
                                                                        <%#Eval("Fipnumber")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>My FIP Card Class:</strong>
                                                                        <%#Eval("FIPClass")%>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Via/change:</strong>
                                                                        <%#Eval("Via")%>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <strong>Terms:</strong>
                                                                        <%#Eval("Terms")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <asp:LinkButton ID="EditJourney" runat="server" CommandName="EditJourney" CssClass="heading-color"
                                                                            CommandArgument='<%#Eval("Id")%>' Style="float: right; margin-left: 10px;" Text="Edit"
                                                                            ToolTip="Edit Journey" />
                                                                        <asp:LinkButton ID="DeleteJourney" runat="server" CssClass="heading-color" CommandName="DeleteJourney"
                                                                            CommandArgument='<%#Eval("Id")%>' Style="float: right;" Text="Delete" ToolTip="Delete Journey"
                                                                            OnClientClick="return confirm('Are you sure you want to delete this journey?');" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <asp:LinkButton ID="BtnAddJourney" runat="server" Text="Add Journey" CssClass="button1"
                                                        Style="float: right; width: 145px; margin-right: 20px; text-align: center; text-decoration: none;"
                                                        OnClick="BtnAddJourney_Click"></asp:LinkButton>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Traveller Information</strong></legend>
                                                <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0;">
                                                    <asp:GridView ID="grdTraveller" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                        CellPadding="4" CssClass="gridP" ForeColor="#333333" GridLines="None" OnRowCommand="grdTraveller_RowCommand"
                                                        Width="100%">
                                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                        <RowStyle BackColor="#ECECEC" BorderColor="#FFFFFF" BorderStyle="Solid" BorderWidth="1px"
                                                            ForeColor="Black" HorizontalAlign="Left" />
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            Record not found.
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Ticket No.">
                                                                <ItemTemplate>
                                                                    <%# ((GridViewRow)Container).RowIndex + 1%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Title">
                                                                <ItemTemplate>
                                                                    <%#Eval("Title")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="First Name">
                                                                <ItemTemplate>
                                                                    <%#Eval("FirstName")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Last Name">
                                                                <ItemTemplate>
                                                                    <%#Eval("LastName")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="FIP Card Number">
                                                                <ItemTemplate>
                                                                    <%#Eval("FIPNumber")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="TrueFalse" />
                                                                <ItemStyle HorizontalAlign="Left" Width="10%" CssClass="TrueFalse" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Country">
                                                                <ItemTemplate>
                                                                    <%#Eval("CountryName")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEditTraveller" runat="server" CommandArgument='<%#Eval("OrderTravellerID") %>'
                                                                        CommandName="cmdEdit" CssClass="heading-color">Edit</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Billing Information</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <asp:LinkButton ID="lnkAddBilling" runat="server" CssClass="heading-color">Add billing details</asp:LinkButton>
                                                    <asp:Repeater ID="rptBilling" runat="server" OnItemCommand="rptBilling_ItemCommand">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="table-stock">
                                                                <tr>
                                                                    <td>
                                                                        <strong>Title: </strong>
                                                                        <%#Eval("Title")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>First Name:</strong>
                                                                        <%#Eval("FirstName")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Last Name:</strong>
                                                                        <%#Eval("LastName")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Address1:</strong>
                                                                        <%#Eval("Address1")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Address2:</strong>
                                                                        <%#Eval("Address2")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>City:</strong>
                                                                        <%#Eval("City")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>State:</strong>
                                                                        <%# Eval("State")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Postal/Zip Code:</strong>
                                                                        <%#Eval("Postcode")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Country:</strong>
                                                                        <%#Eval("Country")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Email Address:</strong>
                                                                        <%#Eval("EmailAddress")%>
                                                                        <div style="float: right;">
                                                                            Visible in receipt
                                                                            <asp:CheckBox ID="chkVisibleEmailAddress" runat="server" Checked='<%# Eval("IsVisibleEmailAddress")%>' Enabled="false"></asp:CheckBox>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Phone Number:</strong>
                                                                        <%#Eval("Phone")%>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkEditBilling" runat="server" CssClass="heading-color">Edit billing details</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Shipping Information</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <asp:LinkButton ID="lnkAddShipping" runat="server" CssClass="heading-color">Add shipping details</asp:LinkButton>
                                                    <asp:Repeater ID="rptShipping" runat="server" OnItemCommand="rptShipping_ItemCommand">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="table-stock">
                                                                <tr>
                                                                    <td>
                                                                        <strong>Title: </strong>
                                                                        <%#Eval("TitleShpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>First Name:</strong>
                                                                        <%#Eval("FirstNameShpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Last Name:</strong>
                                                                        <%#Eval("LastNameShpg")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Address1:</strong>
                                                                        <%#Eval("Address1Shpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Address2:</strong>
                                                                        <%#Eval("Address2Shpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>City:</strong>
                                                                        <%#Eval("CityShpg")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>State:</strong>
                                                                        <%# Eval("StateShpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Postal/Zip Code:</strong>
                                                                        <%#Eval("PostcodeShpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Country:</strong>
                                                                        <%#Eval("CountryShpg")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Email Address:</strong>
                                                                        <%#Eval("EmailAddressShpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Phone Number:</strong>
                                                                        <%#Eval("PhoneShpg")%>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkEditShipping" runat="server" CssClass="heading-color">Edit shipping details</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <div style="float: right; margin-right: 18px;">
                                                        <asp:Button ID="btnSendEmail" runat="server" CssClass="button1" OnClick="btnSendEmail_Click"
                                                            Text="Send confirmation email" Visible="False" Width="215px" />
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Delivery Information</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <asp:Repeater ID="rptDelivery" runat="server">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="table-stock">
                                                                <tr>
                                                                    <td>
                                                                        <strong>First Name:</strong>
                                                                        <%#Eval("FirstName")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Last Name:</strong>
                                                                        <%#Eval("LastName")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Department:</strong>
                                                                        <%#Eval("Department")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Address:</strong>
                                                                        <%#Eval("Address")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>City:</strong>
                                                                        <%#Eval("City")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Postal/Zip Code:</strong>
                                                                        <%# Eval("Postcode")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Country:</strong>
                                                                        <%#Eval("Country")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>State:</strong>
                                                                        <%#Eval("State")%>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Payment Details</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <table id="tblNonAgent" runat="server" cellpadding="0" cellspacing="0" class="table-stock">
                                                        <tr>
                                                            <td>
                                                                <strong>Name: </strong>
                                                                <asp:Label ID="lblCardholderName" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Card Number:</strong>
                                                                <asp:Label ID="lblCardNumber" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Payment Id:</strong>
                                                                <asp:Label ID="lblPaymentId" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Exp. Date:</strong>
                                                                <asp:Label ID="lblExpDate" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Brand:</strong>
                                                                <asp:Label ID="lblBrand" runat="server" />
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                    <table id="tblPayment" runat="server" cellpadding="0" cellspacing="0" class="table-stock">
                                                        <tr>
                                                            <td>
                                                                <strong>Agent ref / Folder No:</strong>
                                                                <asp:Label ID="lblAgentRef2" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong>Payment Taken: <span id="spnPayment" runat="server" class="clsRed">
                                                                    <asp:Label ID="lblPaymentTaken" runat="server" />
                                                                </span></strong><span style="padding-left: 30px">
                                                                    <asp:Button ID="btnTakePayment" runat="server" CssClass="button1" OnClick="btnTakePayment_Click"
                                                                        Text="Take Payment" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr id="trpaymentdt" runat="server" visible="False">
                                                            <td>
                                                                <strong>Date Taken:
                                                                    <%=PaymentTakenDate%></strong>
                                                            </td>
                                                        </tr>
                                                        <tr id="trTakepayment" runat="server" visible="False">
                                                            <td>
                                                                <strong>*Enter Reference number:
                                                                    <asp:TextBox ID="txtRefNum" runat="server" Width="100px" />
                                                                    <asp:RequiredFieldValidator ID="rfRef" runat="server" ControlToValidate="txtRefNum"
                                                                        ErrorMessage="Enter Reference Number" ForeColor="Red" ValidationGroup="ref" />
                                                                    <asp:Button ID="btnRef" runat="server" CssClass="button1" OnClick="btnRef_Click"
                                                                        Text="Enter Reference" ValidationGroup="ref" />
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Refunds</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <table width="100%" cellspacing="0" cellpadding="5" class="table-stock">
                                                        <tr>
                                                            <td>
                                                                <b>Shipping Cost</b>
                                                            </td>
                                                            <td>
                                                                <b>Booking Fee</b>
                                                            </td>
                                                            <td>
                                                                <b>Admin Fee</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%=Currency%><asp:Label runat="server" ID="lblship" />
                                                            </td>
                                                            <td>
                                                                <%=Currency%><asp:Label runat="server" ID="lblbook" />
                                                            </td>
                                                            <td>
                                                                <%=Currency%><asp:Label runat="server" ID="lbladmin" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%=Currency%>(<asp:Label runat="server" ID="lblshippingref" />) Refund
                                                            </td>
                                                            <td>
                                                                <%=Currency%>(<asp:Label runat="server" ID="lblbookingref" />) Refund
                                                            </td>
                                                            <td>
                                                                <%=Currency%>(<asp:Label runat="server" ID="lbladminref" />) Refund
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Repeater ID="rptRefunds" runat="server">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="table-stock">
                                                                <tr>
                                                                    <td>
                                                                        <strong>Train No: </strong>
                                                                        <%#Eval("TrainNo")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>From:</strong>
                                                                        <%#Eval("From")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>To:</strong>
                                                                        <%#Eval("To")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Depature Date-Time:</strong>
                                                                        <%#Eval("DateTimeDepature", "{0:dd/MMM/yyyy}")%>
                                                                        <span style="color: #a82e4b; margin-left: 5px;">
                                                                            <%#Eval("DepartureTime")%>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Arrival Date-Time:</strong>
                                                                        <%#Eval("DateTimeArrival", "{0:dd/MMM/yyyy}")%>
                                                                        <span style="color: #a82e4b; margin-left: 5px;">
                                                                            <%#Eval("ArrivalTime")%></span>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Passenger:</strong>
                                                                        <%#Eval("Passenger")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Net Price:</strong>
                                                                        <%# Eval("Symbol")%>&nbsp;<%# Eval("NetPrice")%></td>
                                                                    <td>
                                                                        <strong>Class:</strong>
                                                                        <%#Eval("Class")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Fare Name:</strong>
                                                                        <%#Eval("FareName")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>PNR No:</strong>
                                                                        <%#Eval("ReservationCode")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Refund Price:</strong>
                                                                        <%# Eval("Symbol")%> <%#Eval("TotalProductRefund", "{0:0.00}")%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Refund Date:</strong>
                                                                        (<%#Eval("RefundDateTime", "{0:MMM dd, yyyy}")%>)
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td style="float: right;">
                                                                <a href="#" target="_blank" style="font-size: 13px; display: inline-block; text-align: center; text-decoration: none;" title="print" id="BtnRefundPrint" runat="server" class="button1">Print</a>
                                                                <asp:Button ID="BtnRefundMailSend" runat="server" Text="Send" CssClass="button1" OnClick="BtnRefundMailSend_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                            <fieldset id="OrderAttachments" runat="server" class="grid-sec2">
                                                <legend><strong>Order Attachments</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <table width="100%" style="background: #fff; padding: 3px 0;">
                                                        <tr>
                                                            <td>Upload File:
                                                            </td>
                                                            <td>
                                                                <asp:FileUpload ID="fupAttachment" runat="server" AllowMultiple="true" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAddAttachment" Text="Upload" runat="server" CssClass="button1"
                                                                    OnClick="btnAddAttachment_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="cat-inner-ord">
                                                    <asp:GridView ID="grdOrderAttachments" runat="server" AutoGenerateColumns="False"
                                                        BorderStyle="None" CssClass="gridP" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                        Width="100%" OnRowCommand="grdOrderAttachments_RowCommand">
                                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                            BorderColor="#FFFFFF" BorderWidth="1px" />
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            Record not found.
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sr. no.">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="File Name">
                                                                <ItemTemplate>
                                                                    <%#Eval("AttachmentName")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="File Type">
                                                                <ItemTemplate>
                                                                    <img src="../images/FileIcon/<%# GetFileExtension(Eval("AttachmentPath").ToString())%>"
                                                                        alt="file" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="imgRemove" Visible="<%#IsVisibleAttachmentDeleteBtn%>"
                                                                        CommandArgument='<%#Eval("ID")+","+ Eval("AttachmentPath")%>' CommandName="Remove"
                                                                        ImageUrl='../images/delete.png' ToolTip="Delete file" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                                                    <a href='<%#Eval("AttachmentPath")%>' title="Download file" target="_blank">
                                                                        <img src="../images/download.png" alt="download" width="20px" />
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Agent Notes</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <textarea id="txtNotes" runat="server" rows="7" style="width: 100%" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="btnAddNotes" runat="server" CssClass="button1" OnClick="btnAddNotes_Click"
                                                                    OnClientClick="return CheckIsRepeat();" Text="Add" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="cat-inner-ord">
                                                    <asp:GridView ID="grdOrderNotes" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                        CellPadding="4" CssClass="gridP" ForeColor="#333333" GridLines="None" OnRowCommand="grdOrderNotes_RowCommand"
                                                        Width="100%">
                                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                        <RowStyle BackColor="#ECECEC" BorderColor="#FFFFFF" BorderStyle="Solid" BorderWidth="1px"
                                                            ForeColor="Black" HorizontalAlign="Left" />
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            Record not found.
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <%#Eval("UserName")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Notes">
                                                                <ItemTemplate>
                                                                    <%#Eval("Notes")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date">
                                                                <ItemTemplate>
                                                                    <%#Eval("CreatedOn", "{0:dd MMM yyyy HH':'mm 'GMT'}")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataRowStyle ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </fieldset>
                                            <fieldset class="grid-sec2">
                                                <legend><strong>Agent Updated Status</strong></legend>
                                                <div class="cat-inner-ord">
                                                    <asp:Repeater ID="gvagentupdatestatus" runat="server">
                                                        <HeaderTemplate>
                                                            <table class="gridP tr-color" width="100%" style="border-collapse: collapse; font-size: 12px; border: 1px solid white;">
                                                                <tr style="color: white;">
                                                                    <th>Agent Name
                                                                    </th>
                                                                    <th>Date
                                                                    </th>
                                                                    <th>Time
                                                                    </th>
                                                                    <th>Status
                                                                    </th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <%#Eval("AgentName")%>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("Date")%>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("Time")%>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("Status")%>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                </table>
                            </div>
                            </fieldset> </td> </tr> </table>
                        </div>
                    </div>
                </div>

                <div style="display: none;">
                    <asp:LinkButton ID="lnkPop" runat="server" Text="lnk" />
                    <asp:LinkButton ID="lnkPop1" runat="server" Text="lnk" />
                    <asp:LinkButton ID="lnkPop2" runat="server" Text="lnk" />
                    <asp:LinkButton ID="lnkPop3" runat="server" Text="lnk" />
                    <asp:LinkButton ID="lnkPop4" runat="server" Text="lnk" />
                    <asp:LinkButton ID="lnkPop5" runat="server" Text="lnk" />
                    <asp:LinkButton ID="lnkPop6" runat="server" Text="lnk" />
                </div>
                <div id="pnlAddEditBill" class="popup-container" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Add/Edit Billing Information
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Title:
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlbtitle" runat="server" class="inputsl">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="divleft">
                            First Name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbfname" runat="server" CssClass="input validcheck" ClientIDMode="Static"
                                MaxLength="20" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="*"
                                ErrorMessage="Please enter first name." ControlToValidate="txtbfname" ForeColor="Red"
                                ValidationGroup="vgs21" />
                        </div>
                        <div class="divleft">
                            Last name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtblname" runat="server" CssClass="input validcheck" MaxLength="20" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="*"
                                ErrorMessage="Please enter last name." ControlToValidate="txtblname" ForeColor="Red"
                                ValidationGroup="vgs21" />
                        </div>
                        <div class="divleft">
                            Email<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbemail" runat="server" CssClass="input validcheck" autocomplete="off"
                                MaxLength="50"> </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Text="*"
                                ErrorMessage="Please enter email address." ControlToValidate="txtbemail" ForeColor="Red"
                                ValidationGroup="vgs21" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="*"
                                ErrorMessage="Please enter a valid email." ControlToValidate="txtbemail" ForeColor="Red"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                ValidationGroup="vgs21" />
                            <div style="float: right;">
                                Visible in receipt
                                <asp:CheckBox ID="chkVisibleEmailAddress" runat="server"></asp:CheckBox>
                            </div>
                        </div>
                        <div class="divleft">
                            Phone Number<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbphone" runat="server" CssClass="input validcheck" MaxLength="15" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Text="*"
                                ErrorMessage="Please enter phone." ControlToValidate="txtbphone" ForeColor="Red"
                                ValidationGroup="vgs21" />
                        </div>
                        <div class="divleft">
                            Address Line 1 Or Company Name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbadd" runat="server" CssClass="input validcheck" MaxLength="100" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Text="*"
                                ErrorMessage="Please enter address." ControlToValidate="txtbadd" ForeColor="Red"
                                ValidationGroup="vgs21" />
                        </div>
                        <div class="divleft">
                            Address Line 2
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbadd2" runat="server" CssClass="input" MaxLength="100" />
                        </div>
                        <div class="divleft">
                            Town / City
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbcity" runat="server" CssClass="input" MaxLength="20" />
                        </div>
                        <div class="divleft">
                            County / State
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbState" runat="server" CssClass="input" MaxLength="20" />
                        </div>
                        <div class="divleft">
                            Postal/Zip Code<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtbzip" runat="server" CssClass="input validcheck" MaxLength="10" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Text="*"
                                ErrorMessage="Please enter postal/zip code." ControlToValidate="txtbzip" ForeColor="Red"
                                ValidationGroup="vgs21" />
                        </div>
                        <div class="divleft">
                            Country<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlbcountry" runat="server" class="inputsl" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Text="*"
                                ErrorMessage="Please select country." ControlToValidate="ddlbcountry" InitialValue="0"
                                ValidationGroup="vgs21" ForeColor="Red" />
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnbilling" runat="server" CssClass="button1" Text="Update billing detail"
                                ValidationGroup="vgs21" OnClick="btnbillingt_Click" />
                            &nbsp;
                            <button id="btnCancelBill" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="txtbilling" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mdpopBilling" runat="server" TargetControlID="lnkPop3"
                    CancelControlID="btnCancelBill" PopupControlID="pnlAddEditBill" BackgroundCssClass="modalBackground" />
                <asp:ModalPopupExtender ID="mdpopBillingAdd" runat="server" TargetControlID="lnkAddBilling"
                    CancelControlID="btnCancelBill" PopupControlID="pnlAddEditBill" BackgroundCssClass="modalBackground" />
                <div id="pnlShippingAddEdit" class="popup-container" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Add/Edit Shipping Detail
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Title:
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlMr" runat="server" class="inputsl">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="divleft">
                            First Name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                            <asp:RequiredFieldValidator ID="rfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                                ControlToValidate="txtFirst" ForeColor="Red" ValidationGroup="vgs1" />
                        </div>
                        <div class="divleft">
                            Last name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" />
                            <asp:RequiredFieldValidator ID="rfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                                ControlToValidate="txtLast" ForeColor="Red" ValidationGroup="vgs1" />
                        </div>
                        <div class="divleft">
                            Email<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" autocomplete="off"> </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                                ControlToValidate="txtEmail" ForeColor="Red" ValidationGroup="vgs1" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                                ControlToValidate="txtEmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic" ValidationGroup="vgs1" />
                        </div>
                        <div class="divleft">
                            Phone Number<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtPhone" runat="server" CssClass="input validcheck" MaxLength="15" />
                            <asp:RequiredFieldValidator ID="rfPhone" runat="server" Text="*" ErrorMessage="Please enter phone."
                                ControlToValidate="txtPhone" ForeColor="Red" ValidationGroup="vgs1" />
                        </div>
                        <div class="divleft">
                            Address Line 1 Or Company Name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" />
                            <asp:RequiredFieldValidator ID="rfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                                ControlToValidate="txtAdd" ForeColor="Red" ValidationGroup="vgs1" />
                        </div>
                        <div class="divleft">
                            Address Line 2
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" />
                        </div>
                        <div class="divleft">
                            Town / City
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="input" />
                        </div>
                        <div class="divleft">
                            County / State
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtState" runat="server" CssClass="input" />
                        </div>
                        <div class="divleft">
                            Postal/Zip Code<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" MaxLength="10" />
                            <asp:RequiredFieldValidator ID="rfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                                ControlToValidate="txtZip" ForeColor="Red" ValidationGroup="vgs1" />
                        </div>
                        <div class="divleft">
                            Country<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" />
                            <asp:RequiredFieldValidator ID="rfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                                ControlToValidate="ddlCountry" InitialValue="0" ValidationGroup="vgs1" ForeColor="Red" />
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button1" Text="Update shipping detail"
                                ValidationGroup="vgs1" OnClick="btnShipping_Click" />
                            &nbsp;
                            <button id="btnCancelShip" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="lblMessage" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mdpopShipping" runat="server" TargetControlID="lnkPop"
                    CancelControlID="btnCancelShip" PopupControlID="pnlShippingAddEdit" BackgroundCssClass="modalBackground" />
                <asp:ModalPopupExtender ID="mdpupShippingAdd" runat="server" TargetControlID="lnkAddShipping"
                    CancelControlID="btnCancelShip" PopupControlID="pnlShippingAddEdit" BackgroundCssClass="modalBackground" />
                <div id="pnlTraveller" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Add/Edit Traveller
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Title:
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlTrTitle" runat="server" class="inputsl">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="divleft">
                            First Name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="ddlTrFirstName" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*"
                                ErrorMessage="Please enter first name." ControlToValidate="ddlTrFirstName" ForeColor="Red"
                                ValidationGroup="hvgs1" />
                        </div>
                        <div class="divleft">
                            Last name<span class="readerror">*</span>
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtTrLName" runat="server" CssClass="input validcheck" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*"
                                ErrorMessage="Please enter last name." ControlToValidate="txtTrLName" ForeColor="Red"
                                ValidationGroup="hvgs1" />
                        </div>
                        <div class="divleft">
                            Country of residence
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlTrCountry" runat="server" class="chkcountry" Style="width: 206px; height: 28px; margin-left: -2px;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="hvgs1"
                                CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlTrCountry" InitialValue="0" />
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnTravellerUpdate" runat="server" CssClass="button1" Text="Update Traveller detail"
                                ValidationGroup="hvgs1" OnClick="btnTravellerUpdate_Click1" />
                            &nbsp;
                            <button id="btnCcacelTrv" runat="server" class="button1">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="Label1" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mupTraveller" runat="server" TargetControlID="lnkPop1"
                    CancelControlID="btnCcacelTrv" PopupControlID="pnlTraveller" BackgroundCssClass="modalBackground" />
                <div id="pnlOrderStatus" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Edit Order Status
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Order Status:
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlOrderStatus" runat="server" class="inputsl">
                            </asp:DropDownList>
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnOrderStatus" runat="server" CssClass="button1" CausesValidation="false"
                                Text="Update order status" OnClick="btnOrderStatus_Click" />
                            &nbsp;
                            <button id="btnOrderStausCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="Label2" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeOrderStatus" runat="server" TargetControlID="lnkPop2"
                    CancelControlID="btnOrderStausCancel" PopupControlID="pnlOrderStatus" BackgroundCssClass="modalBackground" />
                <div id="pnlShipMethod" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Edit Shipping Method
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Shipping Method:
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlShippindMethod" runat="server" class="inputsl">
                            </asp:DropDownList>
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnShipMethod" runat="server" CssClass="button1" CausesValidation="false"
                                Text="Update shipping method" OnClick="btnShipMethod_Click" />
                            &nbsp;
                            <button id="btnShipMethodCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="Label3" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mupShipMethod" runat="server" TargetControlID="lnkPop2"
                    CancelControlID="btnShipMethodCancel" PopupControlID="pnlShipMethod" BackgroundCssClass="modalBackground" />

                <%--Edit shipping method for each journey--%>
                <div id="pnlShippingMethod" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Edit Shipping Method
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Shipping Method:
                        </div>
                        <div class="divright">
                            <asp:DropDownList ID="ddlEditShippindMethod" runat="server" class="inputsl">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnEditJourneyId" runat="server" ClientIDMode="Static" />
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnShippingMethod" runat="server" CssClass="button1" CausesValidation="false"
                                Text="Update shipping method" OnClick="btnShippingMethod_Click" />
                            &nbsp;
                            <button id="btnShippingMethodCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="lblShippingMethod" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mupShippingMethod" runat="server" TargetControlID="lnkPop6"
                    CancelControlID="btnShippingMethodCancel" PopupControlID="pnlShippingMethod" BackgroundCssClass="modalBackground" />
                <%--End Edit shipping method for each journey--%>

                <%--p2p Journey Editing--%>
                <div id="pnlJourney" style="display: none">
                    <asp:HiddenField ID="hdnJourneyId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
                    <div class="heading-hd">
                        Edit Journey Informations
                    </div>
                    <div class="divleftjourney">
                        Train number(s)
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtTrainNumber" runat="server" class="input" MaxLength="20" />
                        <asp:RequiredFieldValidator ID="reqTrain" runat="server" ForeColor="red" ValidationGroup="vgjourney"
                            Display="Dynamic" ControlToValidate="txtTrainNumber" ErrorMessage="Please enter train number."
                            Text="*" />
                    </div>
                    <div class="divleftjourney">
                        From
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtFrom" ClientIDMode="Static" onkeyup="selectpopup(this)" onkeydown="Load()"
                            runat="server" MaxLength="40" class="input" autocomplete="off" />
                        <span id="spantxtFrom" style="display: none"></span>
                        <asp:HiddenField ID="hdnFrm" runat="server" />
                        <asp:RequiredFieldValidator ID="reqFrom" runat="server" ForeColor="red" ValidationGroup="vgjourney"
                            Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                            Text="*" />
                        <asp:Label ID="lblVia" CssClass="lblname1" runat="server" Text="Via(info only)"></asp:Label>
                        <asp:TextBox ID="txtVia" runat="server" class="input">
                        </asp:TextBox>
                    </div>
                    <div class="divleftjourney">
                        To
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ClientIDMode="Static" ID="txtTo" runat="server" onkeyup="selectpopup(this)"
                            onkeydown="Load()" class="input" autocomplete="off" MaxLength="40" />
                        <span id="spantxtTo" style="display: none"></span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ForeColor="red"
                            ValidationGroup="vgjourney" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                            Text="*" CssClass="font14" />
                    </div>
                    <div class="divleftjourney">
                        Date of departure
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtDepartureDate" runat="server" class="input" Style="margin-right: 4px; float: left;"
                            autocomplete="off" MaxLength="10" />
                        <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select DepartureDate."
                            border="0" alt="">
                        <asp:RequiredFieldValidator ID="reqDepartureDate" runat="server" ForeColor="red"
                            ValidationGroup="vgjourney" Display="Dynamic" ControlToValidate="txtDepartureDate"
                            ErrorMessage="Please enter departure date." Text="*" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDepartureDate"
                            ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                            Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                            ValidationGroup="vgjourney">*</asp:RegularExpressionValidator>
                        <asp:DropDownList ID="ddldepTime" runat="server" class="inputsl" Style="width: 60px !important; height: 25px; margin-left: 0px; float: left;">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem Selected="True">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlDepSec" runat="server" class="inputsl" Style="width: 60px !important; height: 25px; margin-left: 5px; float: left;">
                        </asp:DropDownList>
                    </div>
                    <div class="divleftjourney">
                        Date of arrival
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtReturnDate" runat="server" class="input" Style="margin-right: 4px; float: left;"
                            autocomplete="off" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="reqReturnDate" runat="server" ForeColor="red" ValidationGroup="vgjourney"
                            Display="Dynamic" ControlToValidate="txtReturnDate" ErrorMessage="Please enter return date."
                            Text="*" />
                        <asp:RegularExpressionValidator ID="regReturnDate" runat="server" ControlToValidate="txtReturnDate"
                            ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                            Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid return date"
                            ValidationGroup="vgjourney" Enabled="False">*</asp:RegularExpressionValidator>
                        <asp:DropDownList ID="ddlReturnTime" runat="server" class="inputsl" Style="width: 60px !important; height: 25px; margin-left: 0px; float: left;">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem Selected="True">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlRetSec" runat="server" class="inputsl" Style="width: 60px !important; margin-left: 5px; float: left; height: 25px;">
                        </asp:DropDownList>
                    </div>
                    <div class="divleftjourney" style="height: 41px;">
                        Select Passanger
                    </div>
                    <div class="divrightjourney" style="height: 41px;">
                        <asp:DropDownList ID="ddlAdult" runat="server" class="inputsl m-none" Width="90px"
                            Style="height: 25px;" ToolTip="Adult" />
                        <asp:DropDownList ID="ddlChildren" runat="server" class="inputsl m-none" Width="90px"
                            Style="height: 25px;" ToolTip="Children" />
                        <asp:DropDownList ID="ddlSeniors" runat="server" class="inputsl m-none" Width="90px"
                            ToolTip="Seniors" />
                        <asp:DropDownList ID="ddlYouth" runat="server" class="inputsl m-none" Width="90px"
                            ToolTip="Youth" Style="height: 25px;" />
                        <div style="width: 100%; font-size: 12px;">
                            <label class="lbltxt1">
                                Adult</label>
                            <label class="lbltxt2">
                                Children</label>
                            <label class="lbltxt3">
                                Seniors</label>
                            <label class="lbltxt4">
                                Youth</label>
                        </div>
                    </div>
                    <div class="divleftjourney">
                        Class Preference
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox runat="server" ID="txtClass" CssClass="input" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ForeColor="red"
                            ValidationGroup="vgjourney" Display="Static" ControlToValidate="txtClass" ErrorMessage="Please enter Class."
                            Text="*" />
                        <asp:Label ID="lblSupplier" CssClass="lblname1" runat="server" Text="Supplier"></asp:Label>
                        <asp:DropDownList ID="ddlSupplier" runat="server" class="inputsl">
                        </asp:DropDownList>
                    </div>
                    <div class="divleftjourney">
                        Price
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtprice" ClientIDMode="Static" MaxLength="10" runat="server" CssClass="input txtnumber" />
                        <asp:RequiredFieldValidator ID="reqPrice" runat="server" ForeColor="red" ValidationGroup="vgjourney"
                            Display="Static" ControlToValidate="txtprice" ErrorMessage="Please enter price."
                            Text="*" />
                        <lable class="lblname2">
                        Ticket Protection</lable>
                        <asp:TextBox ID="txtTicketprotection" class="input txtnumber" runat="server" MaxLength="10"
                            Style="margin-left: 6px;"></asp:TextBox>
                    </div>
                    <div class="divleftjourney">
                        Fare Name
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtFareName" runat="server" class="input" MaxLength="100" />
                        <%--<lable class="lblname3">
                        Booking Fee</lable>
                        <asp:TextBox ID="txtbookingfee" class="input txtnumber" runat="server" MaxLength="10"
                            Style="margin-left: 6px;"></asp:TextBox>--%>
                        <lable class="lblname4">
                        Commission Fee</lable>
                        <asp:TextBox ID="txtCommission" class="input txtnumber" runat="server" MaxLength="10"
                            Style="margin-left: 6px;"></asp:TextBox>
                    </div>
                    <div class="divleftjourney">
                        My FIP Card Number
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtFipCardNumber" class="input" runat="server"></asp:TextBox>
                        <lable class="lblname4">My FIP Card Class</lable>
                        <asp:DropDownList ID="ddlFipClass" runat="server">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="1st class FIP card">1st class FIP card</asp:ListItem>
                            <asp:ListItem Value="2nd class FIP card">2nd class FIP card</asp:ListItem>
                            <asp:ListItem Value="1st class coupon">1st class coupon</asp:ListItem>
                            <asp:ListItem Value="2nd class coupon">2nd class coupon</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="divleftjourney">
                        Delivery Option
                    </div>
                    <div class="divrightjourney">
                        <asp:TextBox ID="txtDeliveryOption" runat="server" class="input" MaxLength="100" />
                    </div>
                    <div class="divleftjourney" style="height: 120px;">
                        Terms
                    </div>
                    <div class="divrightjourney" style="height: 120px; overflow-y: scroll;">
                        <textarea id="txtTerms" runat="server" class="input"></textarea>
                    </div>
                    <div class="divleftjourney" style="border-bottom: 0px solid; margin-top: 5px;">
                    </div>
                    <div class="divrightjourney" style="border-bottom: 0px solid; margin-top: 5px;">
                        <asp:Button ID="btnJourneyUpdate" runat="server" Text="Update" CssClass="button1"
                            ValidationGroup="vgjourney" Style="width: 15%!important;" OnClick="btnJourneyUpdate_Click" />
                        <button id="btnJourney" class="button1">
                            Cancel</button>
                        <asp:ValidationSummary ID="VSum" ValidationGroup="vgjourney" DisplayMode="List" ShowSummary="false"
                            ShowMessageBox="true" runat="server" />
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpejourney" runat="server" TargetControlID="lnkPop4"
                    CancelControlID="btnJourney" PopupControlID="pnlJourney" BackgroundCssClass="modalBackground" />
                <div id="pnlOrderRefund" style="display: block; width: 700px;">
                    <div class="heading-hd">
                        Order Refund
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            <asp:Label runat="server" ID="lblhdr" Text="00.00"></asp:Label>
                        </div>
                        <div class="divright">
                            <%=Currency%><asp:Label runat="server" ID="lblhdrRefundTxt" ClientIDMode="Static"></asp:Label>
                        </div>
                        <div class="divleft">
                            <asp:Label runat="server" ID="lblrefhdr" Text="00.00"></asp:Label>
                        </div>
                        <div class="divright" style="height: 35px;">
                            <asp:DropDownList runat="server" ID="ddlrefundsel" onchange="onchange()" ClientIDMode="Static" />
                            <label id="lblamountbind">
                            </label>
                            <asp:RequiredFieldValidator Font-Size="13px" ID="req" runat="server" ControlToValidate="ddlrefundsel"
                                InitialValue="-1" ErrorMessage="Please select refund." ValidationGroup="refdata"
                                ForeColor="Red" />
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnrefund" runat="server" CssClass="button1" Text="Refund" OnClick="btnrefund_Click"
                                ValidationGroup="refdata" />
                            &nbsp;
                            <button id="btnRefundCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="lblrefmsg" ForeColor="green" runat="server" /></span>
                        </div>
                        <asp:HiddenField runat="server" ID="hdnrfndamount" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hdnrfndtype" />
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mprefund" runat="server" TargetControlID="lnkPop3" CancelControlID="btnRefundCancel"
                    PopupControlID="pnlOrderRefund" BackgroundCssClass="modalBackground" />
                <div id="pnlExtraCharge" style="display: block; width: 700px;">
                    <div class="heading-hd">
                        Cxl/Extra Charge
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            <asp:HiddenField runat="server" ID="hdnpassaleidextracharge" ClientIDMode="Static" />
                            <asp:Label runat="server" ID="lblhdrExtraCharge" Text="Charge Amount"></asp:Label>
                        </div>
                        <div class="divright">
                            <%=Currency%>
                            &nbsp;<asp:TextBox runat="server" ID="txtExtraCharge" ClientIDMode="Static" MaxLength="5"
                                placeholder="Enter Charge Amount" onKeyUp="hasamount(this)"></asp:TextBox>
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnExtraChargeSave" runat="server" CssClass="button1" Text="Charge"
                                OnClick="btnExtraChargeSave_Click" />
                            &nbsp;
                            <button id="btnExtraChargeCancel" runat="server" class="button1" onclick="hideExtraCharge()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                        </div>
                    </div>
                </div>
                </div>
                <asp:ModalPopupExtender ID="mpExtraCharge" runat="server" TargetControlID="lnkPop3"
                    CancelControlID="btnExtraChargeCancel" PopupControlID="pnlExtraCharge" BackgroundCssClass="modalBackground" />
                <%--Booking fee edit--%>
                <asp:ModalPopupExtender ID="mpBookingEdit" runat="server" TargetControlID="lnkPop3" CancelControlID="btnBookingEditCancel"
                    PopupControlID="pnlBookingEdit" BackgroundCssClass="modalBackground" />
                <div id="pnlBookingEdit" style="display: block; width: 700px;">
                    <div class="heading-hd">
                        Edit Booking Fee
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            <asp:Label runat="server" ID="Label4" Text="Update New Booking Fee"></asp:Label>
                        </div>
                        <div class="divright">
                            <%=Currency%>
                            <asp:TextBox runat="server" ID="txtbookingEdit" Text="00.00" onblur="validamount()"></asp:TextBox>
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button1" Text="Update" OnClick="btnEditBookingUpdate_Click" />
                            &nbsp;
                            <button id="btnBookingEditCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </div>

                <%--Update Pnr No--%>
                <asp:ModalPopupExtender ID="mpPnrNoEdit" runat="server" TargetControlID="lnkPop5"
                    CancelControlID="btnCcacelTrv" PopupControlID="pnlUpdatePnrNo" BackgroundCssClass="modalBackground" />
                <div id="pnlUpdatePnrNo" style="display: none; width: 700px;">
                    <div class="heading-hd">
                        Edit Pnr No
                    </div>
                    <div class="divMain-container" style="margin-top: -2px;">
                        <div class="divleft">
                            Pnr No:
                        </div>
                        <div class="divright">
                            <asp:HiddenField ID="hdnPnrNoUpdate" runat="server" />
                            <asp:TextBox ID="txtPnrNo" runat="server"></asp:TextBox>
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn2" style="padding-top: 10px;">
                            <asp:Button ID="btnUpdatePnrNo" runat="server" CssClass="button1" CausesValidation="false"
                                Text="Update PNR" OnClick="btnUpdatePnrNo_Click" />
                            &nbsp;
                            <button id="btnPnrNoCancel" runat="server" class="button1" onclick="hidepopup()">
                                Cancel</button>
                            <div class="clear">
                            </div>
                            <span>
                                <asp:Label ID="Label5" ForeColor="green" runat="server" /></span>
                        </div>
                    </div>
                </div>
                <%-- End Update Pnr No--%>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddAttachment" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div style="width: 100%; margin: 50px 0px 150px 0px;" runat="server" id="DivMsg">
        <div class="warning">
            <asp:Label ID="lblmsg" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        LoadCal();
        function hideExtraCharge() {
            $("#MainContent_mpExtraCharge_backgroundElement,#pnlExtraCharge").hide();
            $("#txtExtraCharge").val('');
        }
        function hasamount(obj) {
            if ($.isNumeric($(obj).val())) {
            }
            else {
                $("#txtExtraCharge").val('');
            }
        }
        var CalenderMinDate = null;
        function minDateCal() {
            $("#MainContent_txtDepartureDate").datepicker("destroy");
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                onClose: function (selectedDate) {
                    $("#MainContent_txtReturnDate").datepicker("option", "minDate", selectedDate);
                }
            });
        }
        function LoadCal() {
            $("#MainContent_txtDepartureDate").datepicker("destroy");
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                onClose: function (selectedDate) {
                    $("#MainContent_txtReturnDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#MainContent_txtReturnDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
            });
            $("#MainContent_txtReturnDate").datepicker("option", "minDate", $("#MainContent_txtDepartureDate").datepicker('getDate'));
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });
            $(".imgCal").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });
            $(".imgCal1, #MainContent_txtReturnDate").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked'))
                    $("#MainContent_txtReturnDate").datepicker('show');
            });
            $('#MainContent_txtTerms').redactor({ iframe: true, minHeight: 77 });
        }
        function onchange() {
            var Per = parseFloat($("#ddlrefundsel").val());
            var amount = parseFloat($("#lblhdrRefundTxt").text());
            var refund = amount * Per / 100;
            $("#hdnrfndamount").val(refund)
            $("#lblamountbind").text('<%=Currency%>' + refund.toFixed(2));
            if (Per == -1)
                $("#lblamountbind").hide();
            else
                $("#lblamountbind").show();
        }
    </script>
</asp:Content>
