﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="P2POrderRefund.aspx.cs" Inherits="IR_Admin.Orders.P2POrderRefund" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function checkRefundfee() {
            var refund = $("#MainContent_ddlRefundPerc  option:selected").text().replace('%', '');
            var price = $("#MainContent_lblPrice").text();
            if (refund.length < 4) {
                $("#msgerr").hide();
                var amount = (parseFloat(price) * parseFloat(refund)) / 100;
                $("#MainContent_lblRefundPrice").text(amount.toFixed(2));
                $("#MainContent_hdnRefundPrice").val(amount.toFixed(2));
            }
            else {
                $("#msgerr").show();
                $("#MainContent_lblRefundPrice").text("0.00");
                $("#MainContent_hdnRefundPrice").val(amount.toFixed(2));
            }
        }
        function additionalRefund() {
            if (parseFloat($('#MainContent_txtAdditionalRefund').val()) > parseFloat($('#MainContent_lblPrice').text())) {
                $('#MainContent_txtAdditionalRefund').val('');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Order Refund</h2>
    <asp:UpdatePanel ID="Upnl1" runat="server">
        <ContentTemplate>
            <div class="full mr-tp1">
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divDetail" runat="server">
                            <table class="tblMainSection">
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <div class="cat-inner-ord">
                                                <asp:Label ID="lblPrice" runat="server" Text="0" ></asp:Label>
                                                <asp:Label ID="lblOrderID" runat="server" Text="0" ></asp:Label>
                                                <asp:Repeater ID="rptJourney" runat="server">
                                                    <ItemTemplate>
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3" style="color: #a82e4b">
                                                                    <strong>Order id: </strong>
                                                                    <%#Eval("OrderId") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Train No: </strong>
                                                                    <%#Eval("TrainNo")%>
                                                                </td>
                                                                <td>
                                                                    <strong>From:</strong>
                                                                    <%#Eval("From")%>
                                                                </td>
                                                                <td>
                                                                    <strong>To:</strong>
                                                                    <%#Eval("To")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Depature Date-Time:</strong>
                                                                    <%#Eval("DateTimeDepature","{0:dd/MMM/yyyy}")%><span style="color: #a82e4b; margin-left: 5px;"><%#Eval("DepartureTime")%></span>
                                                                </td>
                                                                <td>
                                                                    <strong>Arrival Date-Time:</strong>
                                                                    <%#Eval("DateTimeArrival", "{0:dd/MMM/yyyy}")%><span style="color: #a82e4b; margin-left: 5px;"><%#Eval("ArrivalTime")%></span>
                                                                </td>
                                                                <td>
                                                                    <strong>Passenger:</strong>
                                                                    <%#Eval("Passenger")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Net Price:</strong>
                                                                    <%# Eval("Symbol")%>&nbsp;<%# Eval("NetPrice")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Class:</strong>
                                                                    <%#Eval("Class")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Fare Name:</strong>
                                                                    <%#Eval("FareName")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>PNR No:</strong>
                                                                    <%#Eval("ReservationCode")%>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Refund</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table class="table-stock" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <strong>Select the percentage to be refunded:</strong>
                                                            <asp:DropDownList ID="ddlRefundPerc" runat="server" Width="120px"  onchange="checkRefundfee()"/>
                                                            <asp:RequiredFieldValidator ID="rfddl" runat="server" Text="*" ErrorMessage="Please select a refund percentage."
                                                                ForeColor="Red" ControlToValidate="ddlRefundPerc" InitialValue="-1" ValidationGroup="vs" />
                                                            <span style="padding-left: 5px; font-weight: bold">
                                                                <%=Currency%>
                                                                <asp:Label ID="lblRefundPrice" runat="server" Text="0.00" />
                                                                <asp:HiddenField ID="hdnRefundPrice" runat="server" Value="0.00" />
                                                                </span>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Button ID="btnCancel" Text="Cancel Refund" CssClass="button" runat="server"
                                                                OnClick="btnCancel_Click" />
                                                            <asp:Button ID="btnRefund" Text="Refund" CssClass="button" runat="server" OnClick="btnRefund_Click"
                                                                ValidationGroup="vs" />
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vs"
                                                                ShowSummary="False" ShowMessageBox="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            Should there be a 15% cancellation charge attached to a ticket or pass, you need
                                                            to select 85% as the "percentage to be refunded".
                                                        </td>
                                                    </tr>
                                                    <tr id="rowcancellation" runat="server">
                                                        <td colspan="3">
                                                            Refund is a cancellation
                                                            <asp:CheckBox ID="chkCancel" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_AdditionalRefund" runat="server" visible="false">
                                                        <td colspan="3">
                                                            Write value of the refund:
                                                            <asp:TextBox ID="txtAdditionalRefund" runat="server" Style="margin-left: 20px;" onblur="additionalRefund();" />
                                                            <asp:Button ID="btnCancel2" Text="Cancel Refund" CssClass="button" runat="server"
                                                                OnClick="btnCancel_Click" />
                                                            <asp:Button ID="btnRefund2" Text="Refund" CssClass="button" runat="server" OnClick="btnRefund2_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAdditionalRefund" Enabled="false" Text="Additional Refund" CssClass="button" runat="server"
                                                                Style="margin-left: 20px;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
