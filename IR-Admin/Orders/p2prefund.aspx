﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="p2prefund.aspx.cs" Inherits="IR_Admin.Orders.p2prefund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <fieldset class="grid-sec2">
            <legend><strong>Refund</strong></legend>
            <div class="cat-inner-ord">
                <asp:MultiView ID="mv" runat="server" ActiveViewIndex="0">
                    <asp:View ID="step1" runat="server">
                        <table class="table-stock" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2" style="color: #a82e4b">
                                    <strong>Order Number:
                                        <%=orderNo%></strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 30%;">
                                    <span class="valdreq">*</span> PNR No.:
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="txtDNR" runat="server" MaxLength="8" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqDNR" runat="server" ErrorMessage="Please enter PNR."
                                        ControlToValidate="txtDNR" ValidationGroup="vg" Display="Dynamic" CssClass="valdreq" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                </td>
                                <td>
                                    <asp:Button ID="btnNext" runat="server" CssClass="button" OnClick="btnNext_Click"
                                        ValidationGroup="vg" Text="Next" Width="80px" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="step2" runat="server">
                        <table width="100%" class="table-stock" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    PNR:
                                    <asp:Literal ID="ltrPnr" runat="server" />
                                </td>
                                <td>
                                    PNR Date:
                                    <asp:Literal ID="ltrPnrDate" runat="server" />
                                </td>
                                <td>
                                    Total Amount:
                                    <asp:Literal ID="ltrTotalAmt" runat="server" />
                                </td>
                                <td>
                                    Num of Titles:
                                    <asp:Literal ID="ltrTitles" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="dtlTitles" runat="server" Width="100%">
                            <AlternatingItemStyle BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="2px" />
                            <ItemTemplate>
                                <table width="100%" class="table-stock" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <b>Title:</b>
                                            <%#Eval("Title")%>
                                        </td>
                                        <td>
                                            Date:
                                            <%#Eval("TitleDate")%>
                                        </td>
                                        <td>
                                            Status:
                                            <%#Eval("TitleStatus")%>
                                        </td>
                                        <td>
                                            Time:
                                            <%#Eval("TitleTime")%>
                                        </td>
                                        <td>
                                            CP:
                                            <%#Eval("Cp")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From:
                                            <%#Eval("ArrivalStationName")== null ? ArrivalStationName : Eval("ArrivalStationName")%>
                                        </td>
                                        <td>
                                            To:
                                            <%#Eval("DepartureStationName")== null ? DepartureStationName : Eval("DepartureStationName")%>
                                        </td>
                                        <td>
                                            Adults:
                                            <%#Eval("NumAdults")%>
                                        </td>
                                        <td>
                                            Childs:
                                            <%#Eval("NumBoys")%>
                                        </td>
                                        <td>
                                            Amount:
                                            <%# (Convert.ToDecimal(Eval("TotalPrice").ToString()) * ExchangeRate).ToString("0.00")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Validity Date:
                                            <%#Eval("StartValidityDate")%>
                                        </td>
                                        <td>
                                            Validity:
                                            <%#Eval("Validity")%>
                                        </td>
                                        <td>
                                            Ticket less:
                                            <%#Eval("Ticketless")%>
                                        </td>
                                        <td>
                                            Num of Non Fill:
                                            <%#Eval("NumNonFill")%>
                                        </td>
                                        <td>
                                            Num of Non Paying:
                                            <%#Eval("NumNonPaying")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <table class="table-stock" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: right;">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 30%;">
                                    <span class="valdreq">* </span>Unit of work:
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="txtUnitofWrk" runat="server" MaxLength="10" ReadOnly="True" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter Unit of work."
                                        ControlToValidate="txtUnitofWrk" ValidationGroup="vg2" Display="Dynamic" CssClass="valdreq" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; height: 34px;">
                                    <span class="valdreq">* </span>Address:
                                </td>
                                <td style="height: 34px">
                                    <asp:TextBox ID="txtAddress" runat="server" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="reqAddress" runat="server" ErrorMessage="Please enter Address."
                                        ControlToValidate="txtAddress" ValidationGroup="vg2" Display="Dynamic" CssClass="valdreq" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="valdreq">* </span>Nominative:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNominative" runat="server" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="reqNominative" runat="server" ErrorMessage="Please enter Nominative."
                                        ControlToValidate="txtNominative" ValidationGroup="vg2" Display="Dynamic" CssClass="valdreq" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="valdreq">* </span>Document:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDocument" runat="server" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="reqDocument" runat="server" ErrorMessage="Please enter Document."
                                        ControlToValidate="txtDocument" ValidationGroup="vg2" Display="Dynamic" CssClass="valdreq" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    Note:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNote" runat="server" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                </td>
                                <td>
                                    <asp:Button ID="btnRefundTotal" runat="server" CssClass="button" Text="Submit" Width="80px"
                                        ValidationGroup="vg2" OnClick="btnRefundTotal_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="step3" runat="server">
                        <table width="100%" class="table-stock" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Refund status:
                                    <asp:Literal ID="ltrRefundStatus" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="dtlTotalRefund" runat="server" Width="100%" OnItemDataBound="dtlTotalRefund_ItemDataBound">
                            <AlternatingItemStyle BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="2px" />
                            <ItemTemplate>
                                <table width="100%" class="table-stock" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <b>Title No.:</b>
                                            <%#Eval("TitleNumber")%>
                                        </td>
                                        <td colspan="3">
                                            Refund Cause:
                                            <%#Eval("RefundCause")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Amount:
                                            <asp:Literal ID="ltrAmount" runat="server" Text='<%# (Convert.ToDecimal(Eval("TotalAmount").ToString()) * ExchangeRate).ToString("0.00")%>' />
                                        </td>
                                        <td>
                                            Deduction (%):
                                            <asp:Literal ID="ltrPercent" runat="server" Text='<%#Eval("DeductionPerc")%>' />
                                        </td>
                                        <td>
                                            Deducted Amount:
                                            <asp:Literal ID="ltrDeductedAmount" runat="server" />
                                        </td>
                                        <td>
                                            Refunded Amount:
                                            <asp:Literal ID="ltrRefundedAmount" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </asp:View>
                </asp:MultiView>
            </div>
        </fieldset>
    </div>
</asp:Content>
