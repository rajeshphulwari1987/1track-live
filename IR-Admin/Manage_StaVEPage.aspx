﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_StaVEPage.aspx.cs"
    Inherits="IR_Admin.Manage_StaVEPage" %>

<%@ Register Src="usercontrol/BannerImageManager.ascx" TagName="BannerImageManager"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title>STA Rail | STA Travel Rail</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700,900"
        rel="stylesheet">
    <link href='Styles/bootstrapNew.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="Styles/styleNew.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="Scripts/js/jquery.min.js"></script>
    <script src="Scripts/js/bootstrap.js"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <style>
        #redactor_modal
        {
            top: 8% !important;
        }
    </style>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <script type="text/javascript">
        var passids = "";
        var passdivs = "";
        var imgpath = "";
        var imgpathid = "";
        var imgpath2 = "";
        var imgpathid2 = "";
        var listdata = "";
        $(document).ready(function () {
            $('#mymodaltextedit').redactor({ iframe: true, minHeight: 200 });
            $('#txtcontactus').redactor({ iframe: true, minHeight: 200 });
            $('#txtaddlink').redactor({ iframe: true, minHeight: 200 });
            $('#txtlink').redactor({ iframe: true, minHeight: 100 });
            $("#dtBanner").find(":radio").attr("name", "banner");
            $("#dtBanner2").find(":radio").attr("name", "banner2");
            $("#dtBanner").find(":radio").click(function () {
                imgpathid = $(this).attr("value");
                imgpath = $(this).parent().find("img").attr("src");
            });

            $("#dtBanner2").find(":radio").click(function () {
                imgpathid2 = $(this).attr("value");
                imgpath2 = $(this).parent().find("img").attr("src");
            });

            $("[id*=btntext]").click(function () {
                var data = $(this).next().html(); //.replace(/ +(?= )/g, '')
                $("#textsection").find('.redactor_rdContent').contents().find('body').html(data);
                $("#mymodaltextedit").val(data);
                $("#btntextmodelsave").attr("linkid", $(this).attr("id"));
            });

            $("[id*=imgbtn]").click(function () {
                $("#imgbtnmodelsave").attr("linkid", $(this).attr("id"));
                resetdata();
            });

            $("[id*=btncontactus],#btnlinkhtml6").click(function () {
                var id = $(this).attr("linkid");
                $("#textcontactus").find('.redactor_rdContent').contents().find('body').html($("#" + id).html());
                $("#txtcontactus").val($("#" + id).html());
                $("#btncontactussave").attr("linkid", id);
            });

            $("[id*=btngroup]").click(function () {
                if ($(this).attr("id") == 'btngrouppass') {
                    $("#productblock").show();
                    $("#trainblock").hide();
                    $("#produtlbl").text("Choose Products");
                }
                else {
                    $("#produtlbl").text("Choose Special Trains");
                    $("#productblock").hide();
                    $("#trainblock").show();
                }
                $("#btngroupmodelsave").attr("linkid", $(this).attr("id"));
            });

            $("[id*=imglinkmap]").click(function () {
                var id = $(this).attr("linkid");
                var url = $("#" + id).find("a").attr("href");
                $("#textaddlink").find('.redactor_rdContent').contents().find('body').html(url);
                $("#txtaddlink").val(url);
                $("#btnaddlinksave").attr("linkid", id);
            });

            $(".newadventure").click(function () {
                var id = $(this).parent().attr("id");
                var headerhtml = $("#" + id).find("h4").text();
                var linkhtml = $("#" + id).find("h4").next('div').html();
                var imghtml = $("#" + id).find("img").attr("src");
                var imgid = $("#" + id).find(".imgid").text();
                $.each($("#dtBanner2").find(":radio"), function (key, value) {
                    if ($(this).attr("value") == imgid) {
                        $(this).attr("checked", "checked");
                        imgpath2 = $(this).parent().find("img").attr("src");
                        imgpathid2 = imgid;
                    }
                });
                $("#txth4").val(headerhtml.replace(/\r?\n|\r/g, '').replace(/  /g, ''));
                $("#linkdivnewaadventure").find('.redactor_rdContent').contents().find('body').html(linkhtml.replace(/\r?\n|\r/g, '').replace(/  /g, ''));
                $("#txtlink").val(linkhtml.replace(/\r?\n|\r/g, '').replace(/  /g, ''));
                $("#txtimg").val(imghtml);
                $("#btnnewaadventure").attr("linkid", id);
            });
            bindpageload();
        });

        function getnewaadventureval() {
            var id = $("#btnnewaadventure").attr("linkid");
            var datah4 = $("#txth4").val();
            var datalink = $("#txtlink").val();
            var actualimgpath = window.location.origin + "/" + imgpath2;
            $("#" + id).find("h4").text(datah4);
            $("#" + id).find("h4").next('div').html(datalink);
            if (imgpath2 != "") {
                $("#" + id).find("img").attr("src", actualimgpath);
                $("#" + id).find(".imgid").text(imgpathid2);
            }
            $("#hdnVEnewadventure").val($("#pnlnewadventure").html())
        }

        function getaddlinkval() {
            var id = $("#btnaddlinksave").attr("linkid");
            var data = $("#txtaddlink").val();
            $("#" + id).find("a").attr("href", data);
            if (id == 'linkmap')
                $("#hdnVEmaplink").val(data);
            else if (id == 'linkmaps1')
                $("#hdnVEmaps1link").val(data);
            if (id == 'linkmaps2')
                $("#hdnVEmaps2link").val(data);
        }

        function resetdata() {
            $("#dtBanner").find(":radio").removeAttr("checked");
            imgpathid = "";
            imgpath = "";
        }

        function getcontactusval() {
            var id = $("#btncontactussave").attr("linkid");
            var data = $("#txtcontactus").val();
            $("#" + id).html(data);
            if (id == "sta-call")
                $("#hdnVEbtncontactcall").val(data);
            else if (id == "sta-book")
                $("#hdnVEbtncontactbook").val(data);
            else if (id == "sta-live")
                $("#hdnVEbtncontactlive").val(data);
            else if (id == "sta-email")
                $("#hdnVEbtncontactemail").val(data);
            else if (id == 'linkjourney')
                $("#hdnVEbtntext6").val(data);
        }

        function getpassval() {
            var id = $("#btngroupmodelsave").attr("linkid");
            if (id == 'btngrouppass') {
                $.each($("#divproduct").find(":checkbox"), function (key, value) {
                    if ($(this).is(":checked")) {
                        passids = passids == "" ? $(this).val() : passids + ',' + $(this).val();
                        var newtag = $("." + $(this).attr("id")).clone();
                        newtag.find(".rmv").remove();
                        passdivs = passdivs == "" ? "<div class='col-md-4 col-sm-4'>" + newtag.html() + "</div>" : passdivs + "<div class='col-md-4 col-sm-4'>" + newtag.html() + "</div>";
                    }
                });
                if (passids != "") {
                    $("#hdnVEpass").val(passids);
                    $("#pnlpass").html(passdivs);
                }
            }
            else if (id == 'btngrouptrain') {
                $.each($("#trainblock").find(":checkbox"), function (key, value) {
                    if ($(this).is(":checked")) {
                        passids = passids == "" ? $(this).val() : passids + ',' + $(this).val();
                        var newtag = $("." + $(this).attr("id")).clone();
                        newtag.find(".rmv").remove();
                        passdivs = passdivs == "" ? newtag.html() : passdivs + newtag.html();
                    }
                });
                if (passids != "") {
                    $("#hdnVEtrain").val(passids);
                    $("#pnltrain").html(passdivs);
                }
            }
            passids = "";
            passdivs = "";
        }

        function setimg() {
            var id = $("#imgbtnmodelsave").attr("linkid");
            if (imgpath.length > 5) {
                if (id == 'imgbtnbanner') {
                    $("#imgbanner").attr("src", imgpath);
                    $("#hdnVEbanner").val(imgpathid);
                }
                else if (id == 'imgbtnptp') {
                    $("#imgptp").attr("src", imgpath);
                    $("#hdnVEptpimg").val(imgpathid);
                }
                else if (id == 'imgbtnmap') {
                    $("#imgmap").attr("src", imgpath);
                    $("#hdnVEmap").val(imgpathid);
                }
                else if (id == 'imgbtnmaps1') {
                    $("#imgmaps1").attr("src", imgpath);
                    $("#hdnVEmaps1").val(imgpathid);
                }
                else if (id == 'imgbtnmaps2') {
                    $("#imgmaps2").attr("src", imgpath);
                    $("#hdnVEmaps2").val(imgpathid);
                }
            }
        }

        function getval() {
            //            var body = $("#textsection").find('.redactor_rdContent').contents().find('body').html();
            var body = $("#mymodaltextedit").val();
            var id = $("#btntextmodelsave").attr("linkid");
            $("#" + id).next().html(body);
            if (id == 'btntext1')
                $("#hdnVEbtntext1").val(body);
            else if (id == 'btntext2')
                $("#hdnVEbtntext2").val(body);
            else if (id == 'btntext3')
                $("#hdnVEbtntext3").val(body);
            else if (id == 'btntext4')
                $("#hdnVEbtntext4").val(body);
            else if (id == 'btntext5')
                $("#hdnVEbtntext5").val(body);
            else if (id == 'btntext7')
                $("#hdnVEbtntext7").val(body);
            else if (id == 'btntext8')
                $("#hdnVEbtntext8").val(body);
        }

        function bindpageload() {
            if ($("#hdnVEbtncontactcall").val() != '')
                $("#sta-call").html($("#hdnVEbtncontactcall").val());
            if ($("#hdnVEbtncontactbook").val() != '')
                $("#sta-book").html($("#hdnVEbtncontactbook").val());
            if ($("#hdnVEbtncontactlive").val() != '')
                $("#sta-live").html($("#hdnVEbtncontactlive").val());
            if ($("#hdnVEbtncontactemail").val() != '')
                $("#sta-email").html($("#hdnVEbtncontactemail").val());
            if ($("#hdnVEbtntext6").val() != '')
                $("#linkjourney").html($("#hdnVEbtntext6").val());

            $("#imgbanner").attr("src", '<%=Banner %>');
            $("#imgptp").attr("src", '<%=PTP %>');
            $("#imgmap").attr("src", '<%=Map %>');
            $("#imgmaps1").attr("src", '<%=Map1 %>');
            $("#imgmaps2").attr("src", '<%=Map2 %>');

            $("#linkmap").find('a').attr("href", $("#hdnVEmaplink").val());
            $("#linkmaps1").find('a').attr("href", $("#hdnVEmaps1link").val());
            $("#linkmaps2").find('a').attr("href", $("#hdnVEmaps2link").val());

            if ($("hdnVEpass").val() != '') {
                var item = $("#hdnVEpass").val().split(',');
                for (var i = 0; i < item.length; i++) {
                    $("#" + item[i]).attr("checked", "checked");
                }
                $.each($("#divproduct").find(":checkbox"), function (key, value) {
                    if ($(this).is(":checked")) {
                        passids = passids == "" ? $(this).val() : passids + ',' + $(this).val();
                        var newtag = $("." + $(this).attr("id")).clone();
                        newtag.find(".rmv").remove();
                        passdivs = passdivs == "" ? "<div class='col-md-4 col-sm-4'>" + newtag.html() + "</div>" : passdivs + "<div class='col-md-4 col-sm-4'>" + newtag.html() + "</div>";
                    }
                });
                if (passids != "") {
                    $("#pnlpass").html(passdivs);
                }
                passids = "";
                passdivs = "";
            }
            if ($("hdnVEtrain").val() != '') {
                var item = $("#hdnVEtrain").val().split(',');
                for (var i = 0; i < item.length; i++) {
                    $("#" + item[i]).attr("checked", "checked");
                }
                $.each($("#trainblock").find(":checkbox"), function (key, value) {
                    if ($(this).is(":checked")) {
                        passids = passids == "" ? $(this).val() : passids + ',' + $(this).val();
                        var newtag = $("." + $(this).attr("id")).clone();
                        newtag.find(".rmv").remove();
                        passdivs = passdivs == "" ? newtag.html() : passdivs + newtag.html();
                    }
                });
                if (passids != "") {
                    $("#pnltrain").html(passdivs);
                }
                passids = "";
                passdivs = "";
            }

            if ($("#hdnVEbtntext1").val() != '')
                $("#btntext1").next().html($("#hdnVEbtntext1").val());
            if ($("#hdnVEbtntext2").val() != '')
                $("#btntext2").next().html($("#hdnVEbtntext2").val());
            if ($("#hdnVEbtntext3").val() != '')
                $("#btntext3").next().html($("#hdnVEbtntext3").val());
            if ($("#hdnVEbtntext4").val() != '')
                $("#btntext4").next().html($("#hdnVEbtntext4").val());
            if ($("#hdnVEbtntext5").val() != '')
                $("#btntext5").next().html($("#hdnVEbtntext5").val());
            if ($("#hdnVEbtntext7").val() != '')
                $("#btntext7").next().html($("#hdnVEbtntext7").val());
            if ($("#hdnVEbtntext8").val() != '')
                $("#btntext8").next().html($("#hdnVEbtntext8").val());
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="hdnVEbanner" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEptpimg" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEmap" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEmaps1" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEmaps2" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEmaplink" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEmaps1link" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEmaps2link" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEpass" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEtrain" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEnewadventure" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtncontactcall" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtncontactbook" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtncontactlive" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtncontactemail" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext1" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext2" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext3" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext4" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext5" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext6" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext7" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnVEbtntext8" ClientIDMode="Static" />
    <!-- Main Wrapper Start -->
    <div class="main-wrapper pos-rel">
        <div class="container">
            <div class="row">
                <!-- Header Start -->
                <div class="col-md-12">
                    <img src="images/staheader.jpg" />
                </div>
                <!-- Header End -->
                <!-- Banner Start -->
                <div class="col-md-12">
                    <div class="banner">
                        <div class="banner-text">
                            <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="left: 21px!important;
                                margin-top: 22px;" id="btntext1">Edit</a>
                            <h1 id="sbtn1">
                                Page Title</h1>
                            <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="left: 21px!important;
                                margin-top: 10px;" id="btntext2">Edit</a>
                            <h2 id="sbtn2">
                                SUb title would go along here</h2>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#mymodalbanner" class="edit-btn" id="imgbtnbanner">
                            Edit</a>
                        <img src="images/banner-1.jpg" id="imgbanner" alt="" class="img-responsive">
                    </div>
                </div>
                <!-- Banner End -->
                <!-- Page Text Start -->
                <div class="col-md-12">
                    <div class="contant-text">
                        <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="left: 15px;"
                            id="btntext3">Edit</a>
                        <p id="sbtn3">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Nemo enim ipsam
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.
                        </p>
                    </div>
                </div>
                <!-- Page Text End -->
                <!-- Plan Start -->
                <div class="col-md-12">
                    <div class="point-to">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="top: -6px;"
                                    id="btntext4">Edit</a>
                                <h2 id="sbtn4">
                                    Point to Point</h2>
                                <hr class="blank">
                                <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="top: 55px;"
                                    id="btntext5">Edit</a>
                                <p id="sbtn5">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fermentum tellus
                                    in sodales tincidunt. Morbi odio lorem, consequat vel lacus quis, commodo imperdiet
                                    libero. Ut imperdiet pellentesque urna vel elementum. Sed fringilla non enim blandit
                                    egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam commodo
                                    varius diam eget euismod. Mauris eu libero leo.</p>
                                <hr class="blank">
                                <a href="#" data-toggle="modal" data-target="#mymodelcontactus" class="edit-btn"
                                    style="margin: -29px -29px -29px 0px;" id="btnlinkhtml6" linkid="linkjourney">Edit</a>
                                <div id="linkjourney">
                                    <a href="#" class="light-green-btn">plan your journey</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <a href="#" data-toggle="modal" data-target="#mymodalbanner" class="edit-btn" id="imgbtnptp">
                                    Edit</a>
                                <img id="imgptp" src="images/plan-img.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Plan End -->
                <!-- Tour Packages Start -->
                <div class="col-md-12">
                    <div class="tour-packages">
                        <hr class="blank">
                        <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="margin-top: -20px;"
                            id="btntext7">Edit</a>
                        <h2 id="sbtn7">
                            Choose Your Pass</h2>
                        <a href="#" data-toggle="modal" data-target="#mymodalproduct" class="edit-btn" style="margin-top: -23px;
                            margin-left: 868px;" id="btngrouppass">Edit</a>
                        <div class="row" id="pnlpass">
                            <div class="col-md-4 col-sm-4">
                                <div class="snip1174 yellow">
                                    <img src="images/tour-package-img.jpg" alt="sq-sample33" />
                                    <div>
                                        <div class="figcaption">
                                            <div class="yellow-box">
                                                <h2>
                                                    Britrail England Flexipass</h2>
                                                <p>
                                                    Low Season Discount
                                                </p>
                                            </div>
                                            <a href="#">
                                                <img src="images/book-now-btn.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="snip1174 yellow">
                                    <img src="images/tour-package-img.jpg" alt="sq-sample33" />
                                    <div>
                                        <div class="figcaption">
                                            <div class="yellow-box">
                                                <h2>
                                                    Britrail England Flexipass</h2>
                                                <p>
                                                    Low Season Discount
                                                </p>
                                            </div>
                                            <a href="#">
                                                <img src="images/book-now-btn.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="snip1174 yellow">
                                    <img src="images/tour-package-img.jpg" alt="sq-sample33" />
                                    <div>
                                        <div class="figcaption">
                                            <div class="yellow-box">
                                                <h2>
                                                    Britrail England Flexipass</h2>
                                                <p>
                                                    Low Season Discount
                                                </p>
                                            </div>
                                            <a href="#">
                                                <img src="images/book-now-btn.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Tour Packages End -->
                <!-- Gray Box Start -->
                <div class="col-md-12">
                    <div class="gray-text-box">
                        <a href="#" data-toggle="modal" data-target="#mymodaltext" class="edit-btn" style="left: 17px;
                            top: 0px;" id="btntext8">Edit</a>
                        <p id="sbtn8">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fermentum tellus
                            in sodales tincidunt. Morbi odio lorem, consequat vel lacus quis, commodo imperdiet
                            libero. Ut imperdiet pellentesque urna vel elementum. Sed fringilla non enim blandit
                            egestas.
                        </p>
                    </div>
                </div>
                <!-- Gray Box End -->
                <!-- Map Section Start -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="map-sec">
                                <a href="#" data-toggle="modal" data-target="#mymodalbanner" class="edit-btn" id="imgbtnmap">
                                    Edit</a>
                                <div id="linkmap">
                                    <a href="#">
                                        <img id="imgmap" src="images/map-img.png" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <a href="#" data-toggle="modal" data-target="#mymodeladdlink" class="edit-btn" id="imglinkmap"
                                    style="margin: -29px -29px -29px 0px; width: 100px;" linkid="linkmap">Add link</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="visit-box">
                                <a href="#" data-toggle="modal" data-target="#mymodalbanner" class="edit-btn" id="imgbtnmaps1">
                                    Edit</a>
                                <div id="linkmaps1">
                                    <a href="#">
                                        <img id="imgmaps1" src="images/demo-img-1.jpg" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <a href="#" data-toggle="modal" data-target="#mymodeladdlink" class="edit-btn" id="imglinkmaps1"
                                    style="margin: -29px -29px -29px 0px; width: 100px;" linkid="linkmaps1">Add link</a>
                            </div>
                            <div class="visit-box">
                                <a href="#" data-toggle="modal" data-target="#mymodalbanner" class="edit-btn" id="imgbtnmaps2">
                                    Edit</a>
                                <div id="linkmaps2">
                                    <a href="#">
                                        <img id="imgmaps2" src="images/demo-img-2.jpg" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <a href="#" data-toggle="modal" data-target="#mymodeladdlink" class="edit-btn" id="imglinkmaps2"
                                    style="margin: -29px -29px -29px 0px; width: 100px;" linkid="linkmaps2">Add link</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Map Section End -->
                <!-- Contact Us Start -->
                <div class="col-md-12">
                    <div class="contact-box">
                        <h2>
                            Contact us</h2>
                        <div style="height: 23px;">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
                                    <a href="#" data-toggle="modal" data-target="#mymodelcontactus" class="edit-btn"
                                        id="btncontactuscall" linkid="sta-call">Edit</a>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <a href="#" data-toggle="modal" data-target="#mymodelcontactus" class="edit-btn"
                                        id="btncontactusbook" linkid="sta-book">Edit</a>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <a href="#" data-toggle="modal" data-target="#mymodelcontactus" class="edit-btn"
                                        id="btncontactuslive" linkid="sta-live">Edit</a>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <a href="#" data-toggle="modal" data-target="#mymodelcontactus" class="edit-btn"
                                        id="btncontactusemail" linkid="sta-email">Edit</a>
                                </div>
                            </div>
                        </div>
                        <div class="sta-contact-box">
                            <ul class="row">
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-phone" id="sta-call">
                                        <a href="tel:134782">Call 134 782</a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-appt" id="sta-book">
                                        <a href="http://www.statravel.com.au/appointment.htm">Book an Appointment</a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-chat" id="sta-live">
                                        <a onclick="javascript:window.open('http://statravel.ehosts.net/netagent/cimlogin.aspx?questid=13FC0C61-B374-49CD-B7A3-6D6468600D2A&amp;portid=363559B1-4892-4B6B-9C45-3877462C7BA9&amp;nareferer='+escape(document.location),'_blank','resizable=no,width=600,height=400,scrollbars=no'); return false;"
                                            href="//www.statravel.com.aujavascript:void(0);">Live Chat</a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-email" id="sta-email">
                                        <a href="http://www.statravel.com.au/email_us.htm">Email Us</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Contact Us End -->
                <!-- Adventure Tours Start -->
                <%--<div class="col-md-12">
                    <a href="#" data-toggle="modal" data-target="#mymodalproduct" class="edit-btn" style="margin-top: -32px;"
                        id="btngrouptrain">Edit</a>                       
                    <div class="adventure-tours" id="pnltrain">
                        <div class="col-xs-6 col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h4>
                                        Asia
                                        <br>
                                        Adventure Tours</h4>
                                    <a href="#" class="book-now-btn">Book Now</a>
                                </div>
                                <img src="images/adventure-toure-img-1.jpg" alt="...">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h4>
                                        Asia
                                        <br>
                                        Adventure Tours</h4>
                                    <a href="#" class="book-now-btn">Book Now</a>
                                </div>
                                <img src="images/adventure-toure-img-2.jpg" alt="...">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h4>
                                        Asia
                                        <br>
                                        Adventure Tours</h4>
                                    <a href="#" class="book-now-btn">Book Now</a>
                                </div>
                                <img src="images/adventure-toure-img-3.jpg" alt="...">
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>--%>
                <div class="col-md-12" id="pnlnewadventure" runat="server" clientidmode="Static">
                    <div class="newadventure-tours" style="padding: 15px; margin: 0px; float: left;">
                        <div class="col-xs-6 col-sm-4 col-md-4" style="padding: 0px;" id="newadone">
                            <a href="#" data-toggle="modal" data-target="#mymodelnewadventure" class="edit-btn newadventure"
                                style="margin-top: -32px;">Edit</a>
                            <div class="thumbnail" style="padding: 0px;">
                                <div class="caption" style="width: 100%; height: 200px; background: rgba(246, 190, 0, 1);
                                    text-align: center; color: #fff !important;">
                                    <h4>
                                        UK’s top Harry Potter filming locations</h4>
                                        <div>
                                    <a href="http://localhost/1tracknew/15a4-eurostar" class="book-now-btn">Book Now</a></div>
                                </div>
                                <img src="https://1track.internationalrail.net/Uploaded/SPCountryImg/e971df46-ec54-4eff-b60b-e065790a7cc4.jpg"
                                    alt="..." style="width: 100%; height: 200px;">
                            </div>
                            <label class="hide imgid">4</label>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4" style="padding: 0px;" id="newadtwo">
                            <a href="#" data-toggle="modal" data-target="#mymodelnewadventure" class="edit-btn newadventure"
                                style="margin-top: -32px;">Edit</a>
                            <div class="thumbnail" style="padding: 0px;">
                                <img src="https://1track.internationalrail.net/Uploaded/SPCountryImg/855caa7a-abb7-44c8-b1a8-8b028c0837c3.jpg"
                                    alt="..." style="width: 100%; height: 200px;">
                                <div class="caption" style="width: 100%; height: 200px; background: rgba(246, 190, 0, 1);
                                    text-align: center; color: #fff !important;">
                                    <h4>
                                        Top 10 British dishes around the UK</h4>
                                        <div>
                                    <a href="http://localhost/1tracknew/15a4-eurostar" class="book-now-btn">Book Now</a>
                                </div>
                                </div>
                            </div>
                            <label class="hide imgid">4</label>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4" style="padding: 0px;" id="newadthree">
                            <a href="#" data-toggle="modal" data-target="#mymodelnewadventure" class="edit-btn newadventure"
                                style="margin-top: -32px;">Edit</a>
                            <div class="thumbnail" style="padding: 0px;">
                                <div class="caption" style="width: 100%; height: 200px; background: rgba(246, 190, 0, 1);
                                    text-align: center; color: #fff !important;">
                                    <h4>
                                        Alternative nights out around the UK</h4>
                                        <div>
                                    <a href="http://localhost/1tracknew/15a4-eurostar" class="book-now-btn">Book Now</a>
                                </div>
                                </div>
                                <img src="https://1track.internationalrail.net/Uploaded/SPCountryImg/bc1814cb-5778-4265-82be-921376aad002.jpg"
                                    alt="..." style="width: 100%; height: 200px;">
                            </div>
                            <label class="hide imgid">66</label>
                        </div> 
                    </div>
                </div>
                <!-- Adventure Tours End -->
                <!-- Footer Start -->
                <div class="col-md-12">
                    <img src="images/stafooter.jpg" />
                </div>
                <!-- Footer End -->
            </div>
        </div>
    </div>
    <!-- Main Wrapper End -->
    <!-- line modal -->
    <div class="modal fade" id="mymodalbanner" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        aria-hidden="true">
        <div class="modal-dialog pos-rel">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">
                        Choose Image</h3>
                </div>
                <div class="modal-body">
                    <p class="text-center">
                        <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                            CellPadding="2" CellSpacing="2">
                            <ItemTemplate>
                                <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                                    Height="120" CssClass="bGray" />
                                <br />
                                <input id="chkID" type="radio" name="img" value='<%#Eval("ID")%>' runat="server" />
                            </ItemTemplate>
                        </asp:DataList>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" data-toggle="modal" data-dismiss="modal" data-target="#mymodalbannerupload"
                        class="button">
                        Upload</button>
                    <button class="button" data-dismiss="modal" onclick="setimg()" id="imgbtnmodelsave">
                        Save</button>
                    <button class="button" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodalbannerupload" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">
                        Upload</h3>
                </div>
                <div class="modal-body">
                    <p class="text-center">
                        <uc1:BannerImageManager ID="ucBannerImageManager" runat="server" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodaltext" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="H1">
                        Enter Text</h3>
                </div>
                <div class="modal-body" id="textsection">
                    <textarea id="mymodaltextedit" name="txtContent" cols="10" rows="5" class="rdContent"
                        style="text-align: left; width: 100%;"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" id="btntextmodelsave" onclick="getval()">
                        Save</button>
                    <button type="button" class="btn" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodalproduct" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        style="left: -30%" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 924px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="produtlbl">
                        Product</h3>
                </div>
                <div class="modal-body">
                    <asp:Repeater ID="rptproduct" runat="server">
                        <HeaderTemplate>
                            <div class="col-md-12" id="productblock">
                                <div class="tour-packages" id="divproduct">
                                    <div class="row">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="col-md-4 col-sm-4 <%#Eval("ID")%>">
                                <div class="snip1174 yellow">
                                    <img src="<%#SiteUrl+""+Eval("PRODUCTIMAGE")%>" />
                                    <div>
                                        <div class="figcaption">
                                            <div class="yellow-box">
                                                <h2>
                                                    <%#Eval("NAME")%></h2>
                                                <p style='display: <%#Convert.ToBoolean(Eval("ISSPECIALOFFER"))?"":"none"%>'>
                                                    Special Offer
                                                </p>
                                                <p style='display: <%#Convert.ToBoolean(Eval("BESTSELLER"))?"":"none"%>'>
                                                    Best Seller
                                                </p>
                                            </div>
                                            <a href="<%#Eval("URL")%>">
                                                <img src="images/book-now-btn.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <br class="rmv" />
                                <input id='<%#Eval("ID")%>' type="checkbox" class="rmv" name="img" value='<%#Eval("ID")%>' />
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div> </div> </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rpttrain" runat="server">
                        <HeaderTemplate>
                            <div class="adventure-tours" id="trainblock">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="<%#Eval("ID")%>">
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <div class="caption">
                                            <h4>
                                                <%#Eval("NAME") %>
                                                <br>
                                                Adventure Tours</h4>
                                            <a href="<%#Eval("URL")%>" class="book-now-btn">Book Now</a>
                                        </div>
                                        <img src="<%#SiteUrl+""+Eval("PRODUCTIMAGE")%>" alt="...">
                                    </div>
                                    <input id='<%#Eval("ID")%>' type="checkbox" class="rmv" name="img" value='<%#Eval("ID")%>' />
                                    <br class="rmv" />
                                </div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" id="btngroupmodelsave" onclick="getpassval()">
                        Save</button>
                    <button type="button" class="btn" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodelcontactus" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="H2">
                        Change Link</h3>
                </div>
                <div class="modal-body">
                    <div class="text-center" id="textcontactus">
                        <textarea id="txtcontactus" name="txtContent" cols="10" rows="5" class="rdContent"
                            style="text-align: left; width: 100%;"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" id="btncontactussave" onclick="getcontactusval()">
                        Save</button>
                    <button type="button" class="btn" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodeladdlink" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="H3">
                        Add Link Path</h3>
                </div>
                <div class="modal-body">
                    <div class="text-center" id="textaddlink">
                        <textarea id="txtaddlink" name="txtaddlink" cols="10" rows="5" class="rdContent"
                            style="text-align: left; width: 100%;"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" id="btnaddlinksave" onclick="getaddlinkval()">
                        Save</button>
                    <button type="button" class="btn" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodelnewadventure" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="H4">
                        Manage Title, Link and Path</h3>
                </div>
                <div class="modal-body">
                    <label>Title text</label>
                    <div class="text-center" id="Div2">
                        <input id="txth4" cols="10" rows="5" class="rdContent" style="text-align: left;
                            width: 100%;" />
                    </div>
                    
                     <label style="padding-top: 10px;">Book now button link</label>
                    <div class="text-center" id="linkdivnewaadventure">
                        <textarea id="txtlink" cols="10" rows="5" class="rdContent" style="text-align: left;
                            width: 100%;"></textarea>
                    </div>
                    
                     <label style="padding-top: 10px;">image path</label>
                    <div class="text-center" id="Div4" style="overflow-y: scroll;height: 300px;padding-top:10px;border: 1px solid #ccc;">
                             <asp:DataList ID="dtBanner2" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                            CellPadding="2" CellSpacing="2">
                            <ItemTemplate>
                                <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                                    Height="120" CssClass="bGray" />
                                <br />
                                <input id="chkID" type="radio" name="img" value='<%#Eval("ID")%>' runat="server" />
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" id="btnnewaadventure" onclick="getnewaadventureval()">
                        Save</button>
                    <button type="button" class="btn" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
