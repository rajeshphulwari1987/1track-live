﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs"
    MasterPageFile="Site.master" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsAbs
        {
            display: none;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".clsEmail").keyup(function () {
                document.getElementById("divNews").style.display = 'block';
            });
        });
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%--Banner section--%>
    <div id="dvBanner" runat="server" class="banner">
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%=adminSiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content">
<div class="left-content">
   <div class="cms">
        <div id="ContentHead" runat="server"></div>
   </div>
   <div class="cms">
        <div id="ContentText" runat="server"></div>
   </div>

<div class="clear"> &nbsp;</div>
<div class="country-block-outer" id="divcountryblock" runat="server">
<div class="country-block-inner row">
    <div id="footerBlock" runat="server"></div>
</div>
</div>
</div>
<div class="right-content">
<a runat="server" href='<%=siteURL%>TrainResults.aspx' id="trainresults">
    <div id="rtPannel" runat="server">
    </div>
    <img src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />
</a>
<a href="<%=siteURL%>GeneralInformation.aspx">
<uc:GeneralInformation ID="GInfo" runat="server" />
</a>
<img src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
