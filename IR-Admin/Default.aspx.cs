﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Net.Mail;
namespace IR_Admin
{
    public partial class Login : System.Web.UI.Page
    {
        readonly ManageUser _ManageUser = new ManageUser();
        protected void Page_Load(object sender, EventArgs e)
        {

            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                SetPassword();
            }

        }
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtUsername.Text) && !String.IsNullOrEmpty(txtPassword.Text))
                {
                    var UserExist = _ManageUser.AdminLogin(txtUsername.Text, txtPassword.Text);
                    if (UserExist != null)
                    {
                        //Login User Information
                        AdminuserInfo.RoleId = UserExist.RoleID;
                        AdminuserInfo.UserID = UserExist.ID;
                        AdminuserInfo.Username = UserExist.UserName;
                        AdminuserInfo.BranchID = UserExist.BranchID;
                        AdminuserInfo.SiteID = _ManageUser.GetSiteIdByUser(UserExist.ID);
                        //Remember  Me 
                        if (chkRememberMe.Checked)
                        {
                            HttpCookie cookie = new HttpCookie("login");
                            Response.Cookies.Add(cookie);
                            string pass = Common.Encrypt(txtPassword.Text.Trim(), true);
                            cookie.Values.Add("UserName", txtUsername.Text);
                            cookie.Values.Add("Password", pass); 
                            cookie.Values.Add("Remember", chkRememberMe.Checked.ToString());
                            cookie.Expires = DateTime.Now.AddHours(2);
                        }
                        else
                        {
                            HttpContext.Current.Response.Cookies.Remove("login");
                            Response.Cookies["login"].Expires = DateTime.Now;
                        }
                        Response.Redirect("DashBoard.aspx");
                    }
                    else
                    {
                        lblSuccessMsg.Text = "Invalid User Name And Password";
                    }
                }
            }
            catch { }
        }
        void SetPassword()
        {
            if (Request.Cookies["login"] != null)
            {
                HttpCookie getCookie = Request.Cookies.Get("login");
                if (getCookie != null && getCookie["Remember"] != null && getCookie["Remember"].ToLower() == "true")
                {
                    string pass = Common.Decrypt(getCookie.Values["Password"].Trim(), true);
                    txtUsername.Text = getCookie.Values["UserName"].Trim();
                    txtPassword.Attributes.Add("value", pass);
                    chkRememberMe.Checked = true;
                }
            }
        }

        //protected void btnCancel_Click(object sender, EventArgs e)
        //{
        //    txtUsername.Text = " ";
        //    txtPassword.Text = " ";
        //    lblSuccessMsg.Text = string.Empty;
        //}
        protected void btnFSubmit_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtEmail.Text) && !String.IsNullOrEmpty(txtUser.Text))
            {
                var UserExist = _ManageUser.CheckEmail(txtUser.Text, txtEmail.Text);
                if (UserExist != null)
                {
                    string Subject = "Reset Password";

                    string Body = "<html><head><title></title></head><body><p> Dear " + UserExist.Forename + "" + UserExist.Surname + " , <br /> <br /> Your password is reset on International Rail <br />" +
                              "<br />	Your login details are as below:<br />	<br />	User Name : " + UserExist.UserName + "<br />	Password : " + UserExist.Password + "<br /><br />Thanks " +
                              "<br />International Rail</p></body></html>";

                    var SendEmail = ManageUser.SendAdminMail(Subject, Body, txtEmail.Text);
                    if (SendEmail)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>PasswordSent();</script>", false);
                        txtEmail.Text = string.Empty;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>PasswordErrorSent();</script>", false);
                }
            }
        }

    }
}