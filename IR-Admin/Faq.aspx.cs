﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Faq : Page
    {
        readonly private ManageFaq _master = new ManageFaq();
        readonly private Masters _oMasters = new Masters();
        List<RepeaterListFaqItem> list = new List<RepeaterListFaqItem>();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            //Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";
            if (!Page.IsPostBack)
            {
                ViewState["NoOfItems"] = 5;
                BindRepeter(null);
                _SiteID = Master.SiteID;
                BindSite();
                BindGrid(_SiteID);

                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetPrivacyForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;
            grdFAQ.DataSource = _master.GetFaqList(_SiteID);
            grdFAQ.DataBind();
            Tab = "1";
        }

        #region add/edit
        public void BindSite()
        {
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddFaq()
        {
            var Faqid = Guid.NewGuid();
            try
            {
                int ival = _master.AddFaq(new tblFaq
                 {
                     ID = Faqid,
                     Name = txtTitle.Text.Trim(),
                     Description = txtDesc.InnerHtml,
                     Keyword = txtKeyword.Text.Trim(),
                     CreatedBy = AdminuserInfo.UserID,
                     CreatedOn = DateTime.Now,
                     SiteId = Guid.Parse(ddlSite.SelectedValue)
                 });
                if (ival != 0)
                {
                    foreach (RepeaterItem item in rptProduct.Items)
                    {
                        if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                        {
                            var txtQuestion = item.FindControl("txtQuestion") as TextBox;
                            var txtAnswer = item.FindControl("txtAnswer") as System.Web.UI.HtmlControls.HtmlTextArea;
                            if (txtQuestion.Text.Trim() != "")
                                _master.AddFaqQa(new tblFaqQA
                                {
                                    Id = Guid.NewGuid(),
                                    FaqID = Faqid,
                                    Question = txtQuestion.Text.Trim(),
                                    Answer = txtAnswer.InnerHtml
                                });
                        }
                    }
                    ShowMessage(1, "FAQs added successfully.");
                }
                else{
                    ShowMessage(2, "Already added record.");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdateFaq()
        {
            var FaqId = Guid.Parse(Request["id"].ToString());
            try
            {
                _master.UpdateFaq(new tblFaq
                {
                    ID = FaqId,
                    Name = txtTitle.Text.Trim(),
                    Description = txtDesc.InnerHtml,
                    Keyword = txtKeyword.Text.Trim(),
                    ModifyBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now,
                    SiteId = Guid.Parse(ddlSite.SelectedValue)
                });

                foreach (RepeaterItem item in rptProduct.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var hdnId = item.FindControl("hdnId") as HiddenField;
                        var txtQuestion = item.FindControl("txtQuestion") as TextBox;
                        var txtAnswer = item.FindControl("txtAnswer") as System.Web.UI.HtmlControls.HtmlTextArea;
                        string k = new Guid().ToString();
                        string s = hdnId.Value;
                        string q = txtQuestion.Text;
                        string a = txtAnswer.InnerText;
                        if (txtQuestion.Text.Trim() != "" && hdnId.Value != new Guid().ToString())
                        {
                            _master.UpdateFaqQA(new tblFaqQA
                            {
                                Id = Guid.Parse(hdnId.Value),
                                Question = txtQuestion.Text.Trim(),
                                Answer = txtAnswer.InnerText
                            });
                        }
                        else
                        {
                            _master.AddFaqQa(new tblFaqQA
                            {
                                Id = Guid.NewGuid(),
                                FaqID = FaqId,
                                Question = txtQuestion.Text.Trim(),
                                Answer = txtAnswer.InnerText
                            });
                        }
                    }
                }
                if (Session["rmvFaqQaList"] != null)
                {
                    RemoveFaqQAbyId();
                }
                ShowMessage(1, "FAQs updated successfully.");
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void RemoveFaqQAbyId()
        {
            var removeList = Session["rmvFaqQaList"] as List<RepeaterListFaqItem>;
            if (removeList != null)
                foreach (var item in removeList.Where(item => !String.IsNullOrEmpty(item.Answer)))
                {
                    _master.DeleteFaqQA(item.Id);
                }
            Session["rmvFaqQaList"] = null;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddFaq();
                else
                    UpdateFaq();
                SiteSelected();
                BindGrid(_SiteID);
                txtTitle.Text = string.Empty;
                txtDesc.InnerHtml = string.Empty;
                txtKeyword.Text = string.Empty;
                ddlSite.SelectedIndex = 0;
                ViewState["NoOfItems"] = 5;
                BindRepeter(null);
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Faq.aspx");
        }

        public void GetPrivacyForEdit(Guid id)
        {
            var oFaq = _master.GetFaqById(id);
            if (oFaq != null)
            {
                ddlSite.SelectedValue = oFaq.SiteId.ToString();
                txtTitle.Text = oFaq.Name;
                txtDesc.InnerHtml = oFaq.Description;
                txtKeyword.Text = oFaq.Keyword;
            }
            var oFaqQA = _master.GetFaqQAById(id);
            if (oFaqQA != null)
            {
                rptProduct.DataSource = oFaqQA;
                rptProduct.DataBind();
            }
        }

        #endregion

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdFAQ_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                bool res = _master.DeleteFaqPolicy(Guid.Parse(e.CommandArgument.ToString()));
                if (res)
                    ShowMessage(1, "Record deleted successfully.");
                Tab = "1";
                BindGrid(new Guid());
            }
        }

        protected void btnAddMore_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["NoOfItems"] != null)
                {
                    ViewState["NoOfItems"] = Convert.ToInt32(ViewState["NoOfItems"]) + 1;
                    var newpage = new RepeaterListFaqItem { NoOfItems = Convert.ToInt32(ViewState["NoOfItems"]) };
                    list.Add(newpage);
                    list = AddRepeaterItemInList();
                    Session["rptProdList"] = list;
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptProduct.Items.Count > 0)
                {
                    list = AddRepeaterItemInList();

                    var record = list.FirstOrDefault();
                    if (Session["rmvFaqQaList"] == null)
                    {
                        var removeList = new List<RepeaterListFaqItem> { record };
                        Session["rmvFaqQaList"] = removeList;
                    }
                    else
                    {
                        var removeList = Session["rmvFaqQaList"] as List<RepeaterListFaqItem>;
                        if (removeList != null) removeList.Add(record);
                        Session["rmvFaqQaList"] = removeList;
                    }

                    list.RemoveAt(0);
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected List<RepeaterListFaqItem> AddRepeaterItemInList()
        {
            foreach (RepeaterItem item in rptProduct.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var hdnid = item.FindControl("hdnId") as HiddenField;
                    var txtQuestion = item.FindControl("txtQuestion") as TextBox;
                    var txtAnswer = item.FindControl("txtAnswer") as System.Web.UI.HtmlControls.HtmlTextArea;

                    var page = new RepeaterListFaqItem
                    {
                        Id = hdnid.Value==string.Empty?new Guid():Guid.Parse(hdnid.Value),
                        NoOfItems = item.ItemIndex,
                        Question = txtQuestion.Text,
                        Answer = txtAnswer.InnerHtml,
                    };
                    list.Add(page);
                }
            }
            return list;
        }

        public void BindRepeter(List<RepeaterListFaqItem> listItm)
        {
            try
            {
                if (listItm == null)
                {
                    int numOfItem = Convert.ToInt32(ViewState["NoOfItems"]);
                    for (int i = 0; i < numOfItem; i++)
                    {
                        var page = new RepeaterListFaqItem { NoOfItems = i };
                        list.Add(page);
                    }
                    Session["rptProdList"] = list;
                    rptProduct.DataSource = list;
                }
                else
                    rptProduct.DataSource = listItm;
                rptProduct.DataBind();
                Tab = "2";
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
    }

    public class RepeaterListFaqItem
    {
        public Guid Id { get; set; }
        public int NoOfItems { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}