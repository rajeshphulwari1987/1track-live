﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Web.UI;

public partial class AboutUs : Page
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    private Guid siteId;
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));

            Newsletter1.Visible = _master.IsVisibleNewsLetter(siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            script = new Masters().GetQubitScriptBySId(siteId);
            if (Page.RouteData.Values["PageId"] != null)
            {
                var pageId = (Guid)Page.RouteData.Values["PageId"];
                PageContent(pageId, siteId);
            }
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _master.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList =
                    (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);

                if (list.Count > 0)
                {
                    rptBanner.DataSource = list;
                    rptBanner.DataBind();
                }
                else
                    dvBanner.Visible = false;

                ContentHead.InnerHtml = oPage.PageHeading;
                ContentText.InnerHtml = oPage.PageContent;
                footerBlock.InnerHtml = oPage.FooterImg.Replace("CMSImages", adminSiteUrl + "CMSImages");
                rtPannel.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
            else
            {
                dvBanner.Visible = false;
                divcountryblock.Visible = false;
                trainresults.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}