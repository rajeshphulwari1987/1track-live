﻿using System;
using System.Linq;
using Business;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;
using System.Collections.Generic;
using System.Web.Services;

namespace IR_Admin
{
    public partial class SiteMaster : MasterPage
    {
        readonly Masters _Master = new Masters();
        readonly ManageUser _manageUser = new ManageUser();
        public string Username = string.Empty;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        private readonly db_1TrackEntities db = new db_1TrackEntities();
        public Guid _SiteID;
        public Guid SiteID;
        public string RedExpired = "";
        public string YellowExpired = "";
        public delegate void SiteSelected(object sender, String SelectedValue);
        public event SiteSelected OnSiteSelected;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (AdminuserInfo.RoleId == new Guid())
                Response.Redirect(SiteUrl + "Default.aspx");
            if (AdminuserInfo.SiteID != new Guid())
            {
                _SiteID = string.IsNullOrEmpty(hdnSitemasterID.Value) ? AdminuserInfo.SiteID : Guid.Parse(hdnSitemasterID.Value);
                SiteID = _SiteID;
            }
            else
                SiteID = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID).FirstOrDefault().ID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BindSites();
                }
                LoadImages();
                if (AdminuserInfo.RoleId != new Guid())
                {
                    BindMenu();
                    lnklogin.Visible = false;
                    lnkLogout.Visible = true;
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
                if (EmailSchedular.ExpiredRoleActivated() && Request.Url.AbsolutePath.ToLower().Contains("dashboard"))
                    BindCircles();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindMenu()
        {
            //Bind Admin Menu Role Wise
            var RoleId = Guid.Parse(AdminuserInfo.RoleId.ToString());
            if (!string.IsNullOrEmpty(lstsites.SelectedValue))
                SiteID = Guid.Parse(lstsites.SelectedValue);
            var site = db.tblSites.FirstOrDefault(x => x.ID == SiteID);
            hdnSiteGMTTime.Value = site.GmtTimeZone;
            var Result = _manageUser.GetMenuRoleWise(RoleId);
            if (site.IsCorporate)
            {
                Result = Result.Where(x => x.IsCorporate == true).ToList();
            }
            else
            {
                Result = Result.Where(x => x.IsCorporate == false).ToList();
            }

            var userID = AdminuserInfo.UserID;
            var forename = db.tblAdminUsers.FirstOrDefault(x => x.ID == userID).Forename;
            if (forename != string.Empty)
                lblUser.Text = forename;
            else
                lblUser.Text = AdminuserInfo.Username;

            bool isAgent = (bool)db.tblAdminUsers.FirstOrDefault(x => x.ID == userID).IsAgent;
            if (isAgent)
                lblType.Text = "Hello";
            else
                lblType.Text = "Welcome";

            ltrMenu.Text = "<div class='menu'>";
            ltrMenu.Text += "<ul>";

            foreach (var menu in Result)
            {
                string newUrl = String.IsNullOrEmpty(menu.PageName.Trim()) ? "#" : SiteUrl + menu.PageName;
                if (menu.Name == "Home")
                {
                    ltrMenu.Text += "<li id='" + menu.ID + "'><a href='" + newUrl + "' >" + menu.Name + "</a>";
                }
                else
                {
                    ltrMenu.Text += "<li id='" + menu.ID + "'><a href='" + newUrl + "'>" + menu.Name + "</a>";
                }
                //Bind Submenu
                var SubmenuResult = db.tblNavigations.Where(x => x.ParentID == menu.ID).OrderBy(x => x.CSortOrder).ToList();
                if (SubmenuResult.Count > 0)
                {
                    ltrMenu.Text += "<ul>";
                    foreach (var submenu in SubmenuResult)
                    {
                        var MenuExist = db.ap_RoleMenuDetail.Where(x => x.RoleId == RoleId && x.MenuId == submenu.ID).SingleOrDefault();
                        string newsUrl = SiteUrl + submenu.PageName;
                        if (MenuExist != null)
                        {
                            if (menu.Name == "CMS" || menu.Name == "Settings")
                            {
                                if (lstsites.SelectedValue != "064c6e1d-7580-4a75-b970-20683436c54e" && submenu.Name.ToLower().Contains("inspiration"))
                                {
                                    //inspiration tab will be show when italiajunction site select
                                }
                                else
                                {
                                    ltrMenu.Text += "<li class='clsli'><a class='clsa' href='" + newsUrl + "'> " + submenu.Name + "</a>";
                                    ltrMenu.Text += GetSubMenuList(submenu.ID, RoleId);
                                    ltrMenu.Text += "</li>";
                                }
                            }
                            else
                            {
                                if (submenu.Name == "FOC & AD75 Eurail tickets")
                                {
                                    ltrMenu.Text += "<li><a href='" + newsUrl + "' style='display:none'> " + submenu.Name + "</a>";
                                    ltrMenu.Text += GetSubMenuList(submenu.ID, RoleId);
                                    ltrMenu.Text += "</li>";
                                }
                                else
                                {
                                    ltrMenu.Text += "<li><a href='" + newsUrl + "'> " + submenu.Name + "</a>";
                                    ltrMenu.Text += GetSubMenuList(submenu.ID, RoleId);
                                    ltrMenu.Text += "</li>";
                                }
                            }
                        }
                    }
                    ltrMenu.Text += "</ul>";
                    ltrMenu.Text += "</li>";
                }
            }
            ltrMenu.Text += "</ul>";
            ltrMenu.Text += "</div>";

            string url = Request.ServerVariables["URL"].ToString();

            var result = db.tblNavigations.FirstOrDefault(x => url.Trim().ToUpper().Contains(x.PageName.ToUpper()));
            if (result == null)
                return;

            if (result.ParentID == new Guid())
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "act", " active('12239cf2-8300-4718-9599-bdd4cd80da1b');", true);
            else
            {
                var id = (Guid)result.ParentID;
                ScriptManager.RegisterStartupScript(Page, GetType(), "act", " active('" + id + "');", true);
            }
        }

        public void BindSites()
        {
            bool wideuser = false;
            try
            {
                var result = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID).OrderBy(x => x.DISPLAYNAME);
                if (result == null)
                    return;

                if (result.Count() > 0)
                {
                    lstsites.DataSource = result;
                    lstsites.DataTextField = "DisplayName";
                    lstsites.DataValueField = "ID";
                    if (Session["syswide"] != null && Session["syswidesite"] != null && Convert.ToBoolean(Session["syswide"].ToString()))
                    {
                        wideuser = true;
                        lstsites.SelectedValue = Session["syswidesite"].ToString();
                        SiteID = Guid.Parse(Session["syswidesite"].ToString());
                        AdminuserInfo.SiteID = SiteID;
                        Session.Remove("syswide");
                        Session.Remove("syswidesite");
                    }
                    else if (AdminuserInfo.SiteID == new Guid())
                        lstsites.SelectedValue = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID).FirstOrDefault().ID.ToString();
                    else
                        lstsites.SelectedValue = _Master.UserSitelist().Where(x => x.ID == AdminuserInfo.SiteID && x.ADMINUSERID == AdminuserInfo.UserID).FirstOrDefault().ID.ToString();
                    lstsites.DataBind();
                }
            }
            catch (Exception ex)
            {
                if (wideuser)
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alertwidesite();", true);
                return;
            }
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            AdminuserInfo.RoleId = new Guid();
            Session.Remove("selectedSite");
            Response.Redirect(SiteUrl + "Default.aspx");
        }

        public void BindCircles()
        {
            var RedCircle = EmailSchedular.GetExpiredProductsBySiteID(DateTime.Now.AddDays(3), DateTime.Now.AddDays(-60), AdminuserInfo.SiteID);
            var YellowCircle = EmailSchedular.GetExpiredProductsBySiteID(DateTime.Now.AddDays(45), DateTime.Now, AdminuserInfo.SiteID);
            if (RedCircle.Count > 0)
                IdRedExpired.InnerHtml = "<img src='images/redcircle.png' class='imgcircle'/><span style='color: #6d6f71;'>" + RedCircle.Count + " Expired product</span>";
            if (YellowCircle.Count > 0)
                IdYellowExpired.InnerHtml = "<img src='images/yellowcircle.png' class='imgcircle'/><span style='color: #6d6f71;'>" + YellowCircle.Count + "</span>";
        }

        protected void lstsites_SelectedIndexChanged(object sender, EventArgs e)
        {
            SiteID = Guid.Parse(lstsites.SelectedValue.ToString());
            AdminuserInfo.SiteID = SiteID;
            OnSiteSelected(sender, ((DropDownList)sender).SelectedValue);
            if (EmailSchedular.ExpiredRoleActivated() && Request.Url.AbsolutePath.ToLower().Contains("dashboard"))
                BindCircles();
            Response.Redirect(Request.Url.AbsolutePath);
        }

        public void LoadImages()
        {
            try
            {
                var result = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID).OrderBy(x => x.DISPLAYNAME);
                if (result.Count() > 0)
                {
                    for (int i = 0; i < lstsites.Items.Count; i++)
                    {
                        ListItem item = lstsites.Items[i];
                        var lists = result.ToList();
                        item.Attributes["data-image"] = SiteUrl + "images/" + lists[i].OrderViewImgURL;
                    }
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "bindDropdown", "bindDropdown();", true);
            }
            catch (Exception ex) { }
        }

        public string GetSubMenuList(Guid MenuId, Guid RoleId)
        {
            try
            {
                string menuTest = "";
                var SubmenuResult = db.tblNavigations.Where(x => x.ParentID == MenuId).OrderBy(x => x.CSortOrder).ToList();
                if (SubmenuResult.Count > 0)
                {
                    menuTest += "<ul>";
                    foreach (var submenu in SubmenuResult)
                    {
                        var MenuExist = db.ap_RoleMenuDetail.Where(x => x.RoleId == RoleId && x.MenuId == submenu.ID).SingleOrDefault();
                        string newsUrl = SiteUrl + submenu.PageName;
                        if (MenuExist != null)
                            menuTest += "<li><a href='" + newsUrl + "'> " + submenu.Name + "</a></li>";
                    }
                    menuTest += "</ul>";
                }
                return menuTest;
            }
            catch (Exception ex) { return string.Empty; }
        }
    }
}

