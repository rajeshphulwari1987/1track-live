﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Business;

namespace IR_Admin
{
    public partial class UploadDocs : System.Web.UI.Page
    {
        readonly Masters _oMaster = new Masters();
        static List<FileDocument> fileList = new List<FileDocument>();
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!IsPostBack)
                FillGrid();
        }
        public string hostPath = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        void FillGrid()
        {
            grdUploadFile.DataSource = _oMaster.GetFileList();
            grdUploadFile.DataBind();
        }

        #region Manage Images
        public void AjaxFileUpload2_OnUploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            string fileName = e.FileName;
            string fileType = e.FileName.Substring(e.FileName.LastIndexOf("."));
            string path = "~/Uploaded/Documents/" + Guid.NewGuid().ToString() + fileType;
            if (File.Exists(Server.MapPath(path)))
                File.Delete(Server.MapPath(path));
            else
                AjaxFileUpload2.SaveAs(Server.MapPath(path));
            if (fileList == null)
                fileList = new List<FileDocument>();
            fileList.Add(new FileDocument
            {
                FileType = fileType,
                FileName = fileName,
                NewFileName = path
            });

        }
        #endregion

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileList != null)
                {
                    foreach (var item in fileList)
                    {
                        _oMaster.AddFile(new tblDocument
                        {
                            NewFileName = item.NewFileName.Replace("~/", ""),
                            FileName = item.FileName,
                            FileType = item.FileType,
                            CreatedBy = AdminuserInfo.UserID,
                        });
                    }

                    ShowMessage(1, "Document uploaded successfully.");
                    FillGrid();
                }
                fileList = null;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdUploadFile.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void grdBooking_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                try
                {
                    tblDocument doc = _oMaster.GetFileById(Convert.ToInt32(e.CommandArgument.ToString()));
                    string path = Server.MapPath("~/" + doc.NewFileName); 
                    if (File.Exists(path)) 
                        File.Delete(path);
                   
                    _oMaster.DeleteFile(Convert.ToInt32(e.CommandArgument.ToString()));

                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
                FillGrid();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }
    }
    public class FileDocument
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string NewFileName { get; set; }
    }
}