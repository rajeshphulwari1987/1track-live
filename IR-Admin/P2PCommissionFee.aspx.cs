﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class P2PCommissionFee : System.Web.UI.Page
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public ManageBooking booking = new ManageBooking();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                PageLoadEvent();
        }
        protected void PageLoadEvent()
        {
            try
            {
                grvCommissionFee.DataSource = booking.GetP2PCommissionFee();
                grvCommissionFee.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void grvCommissionFee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCommissionFee.PageIndex = e.NewPageIndex;
            PageLoadEvent();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender;
                HiddenField hdnSiteId = btnUpdate.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = btnUpdate.Parent.FindControl("hdnId") as HiddenField;
                //TextBox txtFee = btnUpdate.Parent.FindControl("txtFee") as TextBox;
                TextBox txtIsPercent = btnUpdate.Parent.FindControl("txtIsPercent") as TextBox;
                Guid Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value);
                Guid SiteId = string.IsNullOrEmpty(hdnSiteId.Value) ? Guid.Empty : Guid.Parse(hdnSiteId.Value);
                decimal IsPercent = string.IsNullOrEmpty(txtIsPercent.Text) ? (decimal)0.00 : Convert.ToDecimal(txtIsPercent.Text);
                if (IsPercent > (decimal)99.99)
                {
                    ShowMessage(2, "Commission is not grater then 100%.");
                    return;
                }
                Business.P2PCommissionFee CommissionFee = new Business.P2PCommissionFee();
               CommissionFee.ID =  (Id == Guid.Empty ? Guid.NewGuid() : Id);
               CommissionFee.SiteID = SiteId;
               CommissionFee.IsPercentage = IsPercent;
               CommissionFee.CreatedOn = DateTime.Now;
               CommissionFee.CreatedBy = AdminuserInfo.UserID;
               CommissionFee.ModifiedOn = DateTime.Now;
               CommissionFee.ModifiedBy = AdminuserInfo.UserID;
               int reault = booking.AddUpdateP2PCommissionFee(CommissionFee, Id);
               if (reault > 0)
               {
                   ShowMessage(1, "Commission Fee update successfully.");
               }
               PageLoadEvent();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        protected void imgApplicable_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgApplicable = ((ImageButton)sender);
                HiddenField hdnSiteId = imgApplicable.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = imgApplicable.Parent.FindControl("hdnId") as HiddenField;
                
                TextBox txtIsPercent = imgApplicable.Parent.FindControl("txtIsPercent") as TextBox;
                Guid Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value);
                Guid SiteId = string.IsNullOrEmpty(hdnSiteId.Value) ? Guid.Empty : Guid.Parse(hdnSiteId.Value);
               
                decimal IsPercent = string.IsNullOrEmpty(txtIsPercent.Text) ? (decimal)0.00 : Convert.ToDecimal(txtIsPercent.Text);
                if (IsPercent > (decimal)99.99)
                {
                    ShowMessage(2, "Fee in (%) is not 100%.");
                    return;
                }

                var Booking = new tblP2PCommissionFee
                {
                    ID = (Id == Guid.Empty ? Guid.NewGuid() : Id),
                    SiteId = SiteId,
                    IsPercentage = IsPercent,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    IsApplicable = true
                };
                int reault = booking.updateP2PBookingCommissionFeeStatus(Booking);
                PageLoadEvent();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        
    }
}