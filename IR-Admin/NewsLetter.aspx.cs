﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class NewsLetter : System.Web.UI.Page
    {
        ManageNewsLetter _NewsLetter = new ManageNewsLetter();
        public string tab = "1";

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();

            } 
            if (!Page.IsPostBack)
            {
               
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                
            }
            BindNewsLetters();

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                tblNewsLetter newsltr = new tblNewsLetter();
                newsltr.ID = string.IsNullOrEmpty(hdnID.Value.ToString()) ? Guid.NewGuid() : Guid.Parse(hdnID.Value.ToString());
                newsltr.Title = txtTitle.Text.Trim();
                newsltr.StartDate = Convert.ToDateTime(txtStartDateHidden.Text);
                newsltr.EndDate = Convert.ToDateTime(txtStartDateHidden.Text);
                newsltr.IsActive = chkactive.Checked;
                newsltr.CreatedBy = AdminuserInfo.UserID;
                newsltr.CreatedOn = System.DateTime.UtcNow;
                newsltr.ModifiedBy = AdminuserInfo.UserID;
                newsltr.ModifiedOn = System.DateTime.UtcNow;
                newsltr.NewsLatterContent = txtContent.InnerText;
                _NewsLetter.SaveNewsLetter(newsltr);
                tab = "1";
                ViewState["tab"] = "1";
                ShowMessage(1, "News Letter Saved Successfully");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            
            
            }
        }

        public void BindNewsLetters()
        {
            try
            {
                var newsltrs = _NewsLetter.GetNewsLetters();
                grdNewsLetter.DataSource = newsltrs;
                grdNewsLetter.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public string TruncateContent(object content)
        {
            if (object.ReferenceEquals(content, DBNull.Value) && content.ToString().Length > 50)
            {

                return content.ToString().Substring(0, 50);
            }
            else
            {
                return content.ToString();
            }
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}