﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class UserJourneyRequest : Page
    {
        public string currency = "$";
        readonly private ManageBooking _master = new ManageBooking();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
            // BindGrid(_siteId, 0, 0);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _siteId = Master.SiteID;

                BindGrid(_siteId, 0, 0);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void BindGrid(Guid siteId, int orderId, int orderstatus)
        {
            var list = new ManageJourneyRequest().GetAllJourneyRequests();
            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
            {
                var d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                list = list.Where(x => x.DepartDate >= d1 && x.DepartDate <= d2).ToList();
            }
            grdJourneyReq.DataSource = list;
            grdJourneyReq.DataBind();
        }

        protected void grdJourneyReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdJourneyReq.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGrid(_siteId, 0, 0);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _siteId = Master.SiteID;
            BindGrid(_siteId, 0, 0);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //Response.Redirect("OrderDetails.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            grdJourneyReq.AllowPaging = false;
            _siteId = Master.SiteID;
            BindGrid(_siteId, 0, 0);
            grdJourneyReq.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#d06b95");
            Context.Response.ClearContent();
            Context.Response.ContentType = "application/ms-excel";
            Context.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xls", "UserJourneyRequest"));
            Context.Response.Charset = "";
            var stringwriter = new System.IO.StringWriter();
            var htmlwriter = new HtmlTextWriter(stringwriter);
            grdJourneyReq.RenderControl(htmlwriter);
            Context.Response.Write(stringwriter.ToString());
            Context.Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
    }
}