﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin
{
    public partial class ManageClass : System.Web.UI.Page
    {
        readonly private Masters _oMasters = new Masters();
        public string tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();

            }
            if (!Page.IsPostBack)
            {
              
                if (!String.IsNullOrEmpty(hdnId.Value))
                    tab = "2";
                FillGrid();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                tblClassMst oMst = new tblClassMst();
                if (!String.IsNullOrEmpty(hdnId.Value))
                {
                    oMst.ID = Guid.Parse(hdnId.Value);
                    oMst.ModifiedBy = AdminuserInfo.UserID;
                    oMst.ModifiedOn = DateTime.Now;
                }
                else
                {
                    oMst.CreatedBy = AdminuserInfo.UserID;
                    oMst.CreatedOn = DateTime.Now;
                    oMst.ID = new Guid();
                }
                oMst.IsActive = chkactive.Checked;
                oMst.Name = txtName.Text.Trim();
                oMst.EurailCode = Convert.ToInt32(txtEurailCode.Text);

                int result = _oMasters.AddClass(oMst);

                if (result > 0 && !String.IsNullOrEmpty(hdnId.Value))
                    ShowMessage(1, "Record updated sucessfully");
                else
                    ShowMessage(1, "Record added sucessfully");

                hdnId.Value = string.Empty;
                txtName.Text = string.Empty;
                chkactive.Checked = false;
                FillGrid();
                tab = "1";
                ViewState["tab"] = "1";

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void FillGrid()
        {
            var list = _oMasters.GetClassList();
            if (list != null && list.Count > 0)
            { grvClass.DataSource = list; } grvClass.DataBind();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageClass.aspx");

        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grvClass_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvClass.PageIndex = e.NewPageIndex;
            FillGrid();
            tab = "1";
            ViewState["tab"] = "1";
        }

        protected void grvClass_RowCommand(object sender, GridViewCommandEventArgs e)
        {

          
            if (e.CommandName == "Modify")
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                var ocls = _oMasters.GetClassById(id);
                txtName.Text = ocls.Name;
                chkactive.Checked = ocls.IsActive;
                ViewState["tab"] = "2";
                tab = "2";
                hdnId.Value = id.ToString();
            }
            if (e.CommandName == "Remove")
            {
                try
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    bool result = _oMasters.DeleteClass(id);
                    if (result)
                        ShowMessage(1, "Record deleted successfully");
                    FillGrid();
                    tab = "1";
                    ViewState["tab"] = "1";
                }
                catch  
                {
                    ShowMessage(2, "The Class is already in used.");
                }
            }

            if (e.CommandName == "ActiveInActive")
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                _oMasters.ActiveInactiveClass(id);
                FillGrid(); tab = "1";
                ViewState["tab"] = "1";
            }
        }
    }
}