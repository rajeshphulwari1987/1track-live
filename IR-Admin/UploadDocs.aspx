﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UploadDocs.aspx.cs" Inherits="IR_Admin.UploadDocs" %>

 
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %> 
 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script>
        $(document).ready(function () {
            $create(Sys.Extended.UI.AjaxFileUpload, { "clientStateField": $get("MainContent_AjaxFileUpload2_ClientState"), "contextKey": "{DA8BEDC8-B952-4d5d-8CC2-59FE922E2923}", "contextKeys": "2", "maximumNumberOfFiles": 0, "postBackUrl": "/UploadDocs.aspx", "throbber": $get("MainContent_myThrobber_tq") }, null, null, $get("MainContent_AjaxFileUpload2"));
            $("#divAdd").hide();
            $("#divlist").show();

            $("#aNew").click(function () {
                $("#divAdd").show();
                $("#divlist").hide();
            });
        });

        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        } 
    </script>
    <style>
        .ajax__fileupload_fileItemInfo div.removeButton
        {
            font-size: 12px;
        }
        
        .ajax__fileupload_uploadbutton
        {
            font-size: 13px;
        }
        element.style
        {
        }
        .ajax__fileupload_fileItemInfo .filename
        {
            font-size: 13px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Upload Documents</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div id="divlist">
        <ul class="list">
            <li><a id="aNew" style="cursor: pointer;">Add New File</a> 
            </li>
        </ul>
        <div>
            <asp:GridView ID="grdUploadFile" runat="server" AutoGenerateColumns="False" PageSize="20"
                CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                AllowPaging="True" OnPageIndexChanging="grdBooking_PageIndexChanging" OnRowCommand="grdBooking_RowCommand">
                <AlternatingRowStyle BackColor="#FBDEE6" />
                <PagerStyle CssClass="paging"></PagerStyle>
                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                    BorderColor="#FFFFFF" BorderWidth="1px" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <EmptyDataRowStyle HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    Record not found.</EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="File Name">
                        <ItemTemplate>
                            <%#Eval("FileName")%>
                        </ItemTemplate>
                        <ItemStyle Width="30%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="File Type">
                        <ItemTemplate>
                            <img src='<%# String.Format("images/FileIcon/{0}.png", ((string)Eval("FileType")).Replace(".",""))%>' />
                        </ItemTemplate>
                        <ItemStyle Width="10%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="File Path">
                        <ItemTemplate>
                         <a target="_blank" href='<%=hostPath%><%#Eval("NewFileName") %>'>   <%=hostPath%><%#Eval("NewFileName") %></a>
                        </ItemTemplate>
                        <ItemStyle Width="45%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Uploaded Date">
                        <ItemTemplate>
                            <%#Eval("CreatedOn", "{0:dd/MM/yyyy}") %>
                        </ItemTemplate>
                        <ItemStyle Width="10%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remove">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                            </div>
                        </ItemTemplate>
                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div id="divAdd" style="border: 1px solid #ccc; background: #eee; padding: 10px;">
         <asp:Label runat="server" ID="myThrobber_tq" Style="display: none;">
                                
                                 <img align="absmiddle" alt="" src='images/uploadexc.png'/>
                                            </asp:Label>
                                            <asp:AjaxFileUpload ID="AjaxFileUpload2" runat="server" padding-bottom="4" ContextKeys="2"
                                                padding-left="2" padding-right="1" padding-top="4" ThrobberID="myThrobber_tq"
                                                OnUploadComplete="AjaxFileUpload2_OnUploadComplete" />
                                            <br />
                                            <div id="testuploaded" style="display: none; padding: 4px; border: gray 1px solid;">
                                                <h4>
                                                    Uploaded files:</h4>
                                                <hr />
                                                <div id="fileList">
                                                </div>
                                            </div>
        <asp:Button ID="btnUpload" runat="server" Text="Submit" OnClick="btnUpload_Click"
            CssClass="button" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
    </div>
</asp:Content>
