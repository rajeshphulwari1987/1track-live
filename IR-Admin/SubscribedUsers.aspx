﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscribedUsers.aspx.cs"
    MasterPageFile="~/Site.Master" Inherits="IR_Admin.SubscribedUsers" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Subscribed Users
    </h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdSubUsers" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="5" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"
                            AllowPaging="True" OnRowCommand="grdSubUsers_RowCommand" OnPageIndexChanging="grdSubUsers_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="UserName">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <%#Eval("Email")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Areas selected">
                                    <ItemTemplate>
                                        <%#Eval("AreaName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Is Subscribed">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgIsSubscribed" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" AlternateText="status" ImageUrl='<%#Eval("IsSubscribed").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsSubscribed").ToString()=="True" ?"Subscribed":"Unsubscribed" %>' />
                                        <%--<asp:CheckBox ID="chkUser" runat="server" />--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this item?');" />
                                       <%-- <asp:CheckBox ID="chkUser" runat="server" />--%>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
