﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewsLetters.aspx.cs" Inherits="IR_Admin.NewsLetters.NewsLetters" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });

        function checkUncheckCountyTree() {

        }  
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        function ClientValidate_JobDate(source, arguments) {
            var txtFromJobDate = document.getElementById("<%=txtStartDate.ClientID%>");
            var txtToJobDate = document.getElementById("<%=txtEndDate.ClientID%>");
            if (txtFromJobDate.value != '' && txtToJobDate.value != '' && txtFromJobDate.value != '__/__/__' && txtFromJobDate.value != '__/__/____' && txtToJobDate.value != '__/__/__' && txtToJobDate.value != '__/__/____') {
                if (parseDate(txtFromJobDate.value) <= parseDate(txtToJobDate.value)) {
                    arguments.IsValid = true;
                } else {
                    arguments.IsValid = false;
                }
            }
        }
        function parseDate(date) {
            var parts = date.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
   
    </script>
    <style type="text/css">
        .tblMainSection tbody tr
        {
            line-height: normal;
        }
        .col
        {
            line-height: normal;
        }
        .ajax__calendar_container
        {
            min-height: 190px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Newsletter
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="NewsLetterList.aspx" class=" ">List</a></li>
            <li><a id="aNew" href="NewsLetters.aspx" class="current">New</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <table width="100%">
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Title
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="180" />
                                            <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtTitle"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Start Date
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtStartDate" runat="server" MaxLength="10" Style="float: left;" />
                                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                                PopupButtonID="imgFromDateCal" Format="dd/MM/yyyy" PopupPosition="BottomLeft">
                                            </asp:CalendarExtender>
                                            <span style="display: block; float: left; margin-left: 10px;">
                                                <asp:Image ID="imgFromDateCal" runat="server" ImageUrl="~/images/Calendar.gif" CssClass="Calendar"
                                                    ImageAlign="Top" AlternateText="Calendar" />
                                            </span>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                                MaskType="Date" TargetControlID="txtStartDate">
                                            </asp:MaskedEditExtender>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtStartDate"
                                                ValidationGroup="submit" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$">
                                            </asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="req2" runat="server" ControlToValidate="txtStartDate"
                                                CssClass="valdreq" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            End Date
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtEndDate" runat="server" MaxLength="10" Style="float: left;" />
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtEndDate"
                                                PopupButtonID="imgFromDateCal2" Format="dd/MM/yyyy" PopupPosition="BottomLeft">
                                            </asp:CalendarExtender>
                                            <span style="display: block; float: left; margin-left: 10px;">
                                                <asp:Image ID="imgFromDateCal2" runat="server" ImageUrl="~/images/Calendar.gif" CssClass="Calendar"
                                                    ImageAlign="Top" AlternateText="Calendar" />
                                            </span>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                                MaskType="Date" TargetControlID="txtEndDate">
                                            </asp:MaskedEditExtender>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEndDate"
                                                ValidationGroup="submit" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$">
                                            </asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="req3" runat="server" ControlToValidate="txtEndDate"
                                                CssClass="valdreq" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                                ValidationGroup="submit" />
                                            <asp:CustomValidator ID="CustomValidator1" ClientValidationFunction="ClientValidate_JobDate" ErrorMessage="End date must be greater then before start date"
                                            runat="server" Display="None"  ValidationGroup="submit"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col" valign="top">
                                            Attachment
                                        </td>
                                        <td class="col">
                                            <asp:FileUpload ID="fupAttachment" runat="server" />
                                            <asp:Button ID="btnAdd" CausesValidation="false" runat="server" CssClass="button"
                                                Text="Add" Style="min-width: 50px!important; height: 20px!important;" ValidationGroup="submit"
                                                OnClick="btnAdd_Click" />
                                            <br />
                                            <asp:Repeater ID="rptAtachment" runat="server" OnItemCommand="rptAtachment_ItemCommand">
                                                <ItemTemplate>
                                                    <div style="width: 100%;">
                                                        <a href='<%#Eval("AttchmentPath")%>'>
                                                            <img src="../images/File.png" /></a>
                                                        <asp:LinkButton ID="lnkAttachPath" runat="server" CommandArgument='<%#Eval("AttchmentPath")+","+ Eval("ID") %>'
                                                            CommandName="cmdDelete" Style="text-decoration: none;"><img src="../images/DeleteRed.png" alt='' /></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsActv" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="cat-outer" style="width: 99%; margin: 2% .5% .5% .5%; position: relative;">
                                    <span style="color: #941E34; line-height: 30px; font-weight: bold">Newsletter Content
                                    </span>
                                    <textarea id="txtContent" runat="server"></textarea>
                                </div>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="submit" OnClick="btnSubmit_Click" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
