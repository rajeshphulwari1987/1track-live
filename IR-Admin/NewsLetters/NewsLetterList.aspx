﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewsLetterList.aspx.cs" Inherits="IR_Admin.NewsLetters.NewsLetterList" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .style1
        {
            width: 682px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlProductCommission" runat="server">
        <ContentTemplate>
            <h2>
                Newsletters List</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="NewsLetterList.aspx" class="current">List</a></li>
                    <li style="font-size: 14px;">
                        <asp:HyperLink ID="aNew" NavigateUrl="NewsLetters.aspx" CssClass="" runat="server">New</asp:HyperLink>
                    </li>
                </ul>
                <br />
            </div>
            <!-- tab "panes" -->
            <div class="full mr-tp1" style="margin-top: 0px">
                <div class="panes">
                    <div id="divNew" runat="server" style="display: block;">
                        <asp:HiddenField runat="server" ID="hdnPath" />
                        <asp:Repeater ID="rptNewsLetter" runat="server" OnItemCommand="rptNewsLetter_ItemCommand">
                            <HeaderTemplate>
                                <table style="width: 100%; font-weight: bold;" class="grid-head2">
                                    <tr>
                                        <td style="width: 20%; padding-left: 1%;">
                                            Title
                                        </td>
                                        <td style="width: 20%;">
                                            Site's Name
                                        </td>
                                        <td style="width: 16%;">
                                            Start Date
                                        </td>
                                        <td style="width: 14%;">
                                            End Date
                                        </td>
                                        <td style="width: 10%; text-align: center;">
                                            Action
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <AlternatingItemTemplate>
                                <div class="cat-inner-alt" style="margin: 0px; width: 100%">
                                    <div style="width: 24%; padding-left: 1%; float: left;">
                                        <%#Eval("Title") %>
                                    </div>
                                    <div style="width: 25%; float: left;">
                                        <%#Eval("siteName")%>
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        <%#Eval("StartDate","{0:dd/MM/yyyy}")%>
                                    </div>
                                    <div style="width: 12%; float: left;">
                                        <%#Eval("EndDate","{0:dd/MM/yyyy}")%>
                                    </div>
                                    <div style="width: 16%; float: left; text-align: right">
                                        <a href='NewsLetters.aspx?id=<%#Eval("ID") %>'>
                                            <img src="../images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID") %>' CommandName="Delete" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgSend" AlternateText="Send" ToolTip="Send newsletter"
                                            CommandArgument='<%#Eval("ID") %>' CommandName="Send" ImageUrl="../images/mail_forward.png" />
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                            <ItemTemplate>
                                <div class="cat-inner" style="margin: 0px; width: 100%">
                                    <div style="width: 24%; padding-left: 1%; float: left;">
                                        <%#Eval("Title") %>
                                    </div>
                                    <div style="width: 25%; float: left;">
                                        <%#Eval("siteName")%>
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        <%#Eval("StartDate","{0:dd/MM/yyyy}")%>
                                    </div>
                                    <div style="width: 12%; float: left;">
                                        <%#Eval("EndDate","{0:dd/MM/yyyy}")%>
                                    </div>
                                    <div style="width: 16%; float: left; text-align: right">
                                        <a href='NewsLetters.aspx?id=<%#Eval("ID") %>'>
                                            <img src="../images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID") %>' CommandName="Delete" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgSend" AlternateText="Send" ToolTip="Send newsletter"
                                            CommandArgument='<%#Eval("ID") %>' CommandName="Send" ImageUrl="../images/mail_forward.png" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div id="divRecordNotfnd" runat="server" class="warning" style="display: none;">
                            Record not found.
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
