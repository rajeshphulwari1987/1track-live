﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.NewsLetters
{
    public partial class NewsLetterList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillAllNewsLetters();
            }
        }
        public void FillAllNewsLetters()
        {
            try
            {
                List<tblNewsLetter> lst = new ManageNewsLetter().GetNewsLetters();
                var newLst = from a in lst
                             select new { a.ID, a.Title,a.StartDate,a.EndDate,a.IsActive ,siteName = new ManageNewsLetter().GetSiteName(a.ID) };
                if (newLst.Count() > 0)
                    rptNewsLetter.DataSource = newLst;
                else
                    rptNewsLetter.DataSource = null;
                rptNewsLetter.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void rptNewsLetter_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    new ManageNewsLetter().DeleteNewsLetter(Guid.Parse(e.CommandArgument.ToString()));
                    FillAllNewsLetters();
                }
                else if (e.CommandName == "ActiveInActive")
                {
                    new ManageNewsLetter().ActiveInActiveNewsLetter(Guid.Parse(e.CommandArgument.ToString()));
                    FillAllNewsLetters();
                }
                else if (e.CommandName == "Send")
                {
                    Session["NewsLID"] = e.CommandArgument.ToString();
                    Response.Redirect("SendNewsletter.aspx");
                }

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
    }
}