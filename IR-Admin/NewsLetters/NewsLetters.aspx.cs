﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.IO;

namespace IR_Admin.NewsLetters
{
    public partial class NewsLetters : System.Web.UI.Page
    {
        IFormatProvider cultureAU = new CultureInfo("en-AU", true);
        readonly Masters _master = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillMenu();
                if (Request.QueryString["id"] != null)
                {
                    Session["NID"] = Guid.Parse(Request.QueryString["id"]);
                    FillPageInfo();
                }
                else
                    Session["NID"] = Guid.NewGuid();

            }
        }
        public void FillPageInfo()
        {
            List<tblNewsLetter> lst = new ManageNewsLetter().GetNewsLettersByID(Guid.Parse(Session["NID"].ToString()));
            if (lst.Count() > 0)
            {
                txtTitle.Text = lst.FirstOrDefault().Title;
                txtStartDate.Text = lst.FirstOrDefault().StartDate.ToString("dd/MM/yyyy", cultureAU);
                txtEndDate.Text = lst.FirstOrDefault().EndDate.ToString("dd/MM/yyyy", cultureAU);
                txtContent.InnerText = (lst.FirstOrDefault().NewsLatterContent);
                if (lst.FirstOrDefault().IsActive != null)
                    chkIsActv.Checked = Convert.ToBoolean(lst.FirstOrDefault().IsActive);
                FillNewsAttachment(Guid.Parse(Session["NID"].ToString()));
                List<Guid> oSiteId = new ManageNewsLetter().GetSiteIdById(Guid.Parse(Session["NID"].ToString()));
                //--Bind Site Tree
                foreach (var sItem in oSiteId)
                {
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Value == sItem.ToString())
                            node.Checked = true;
                    }
                }

            }

        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strPath = Server.MapPath("~/Uploaded/NewsLetters/");
                if (fupAttachment.HasFile)
                {
                    string fileName = fupAttachment.FileName;
                    string ext = fupAttachment.FileName.Substring(fupAttachment.FileName.LastIndexOf("."));
                    //FileName = id + FileName;
                    //string[] ext = fileName.Split('.');
                    string NewFileName = Guid.NewGuid().ToString();
                    NewFileName = NewFileName + ext;
                    fupAttachment.SaveAs(strPath + NewFileName);
                    tblNewsLetterAttachment oNewsAtaach = new tblNewsLetterAttachment();
                    oNewsAtaach.ID = Guid.NewGuid();
                    oNewsAtaach.NewsID = Guid.Parse(Session["NID"].ToString());
                    oNewsAtaach.AttchmentPath = System.Configuration.ConfigurationSettings.AppSettings["HttpHost"].ToString() + "Uploaded/NewsLetters/" + NewFileName;
                    new ManageNewsLetter().SaveNewsLetterAttachments(oNewsAtaach);
                    FillNewsAttachment(oNewsAtaach.NewsID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        private void FillNewsAttachment(Guid ID)
        {
            try
            {
                List<tblNewsLetterAttachment> lst = new ManageNewsLetter().GetNewsLetterAttachmentsByNewsId(ID);
                if (lst.Count() > 0)
                    rptAtachment.DataSource = lst;
                else
                    rptAtachment.DataSource = null;
                rptAtachment.DataBind();

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void rptAtachment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "cmdDelete")
                {
                    string[] str = e.CommandArgument.ToString().Split(',');
                    string hreflink = str[0];
                    new ManageNewsLetter().DeleteNewsLetterAttach(Guid.Parse(str[1]));
                    Uri uri = new Uri(hreflink);
                    string filename = System.IO.Path.GetFileName(uri.LocalPath);
                    string strPath = Server.MapPath("~/Uploaded/NewsLetters/");
                    string path = strPath + filename;
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                }
                FillNewsAttachment(Guid.Parse(Session["NID"].ToString()));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    tblNewsLetter oNewsLetter = new tblNewsLetter();
                    oNewsLetter.ID = Guid.Parse(Request.QueryString["id"]);
                    oNewsLetter.Title = txtTitle.Text;
                    oNewsLetter.NewsLatterContent = (txtContent.Value);
                    oNewsLetter.StartDate = Convert.ToDateTime(DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null).ToString("MM/dd/yyyy"));
                    oNewsLetter.EndDate = Convert.ToDateTime(DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null).ToString("MM/dd/yyyy"));
                    oNewsLetter.ModifiedBy = AdminuserInfo.UserID;
                    oNewsLetter.ModifiedOn = DateTime.Now;
                    oNewsLetter.IsActive = chkIsActv.Checked;
                    new ManageNewsLetter().UpdateNewsLetter(oNewsLetter);
                    new ManageNewsLetter().DeleteNewsLetterSiteLookup(oNewsLetter.ID);
                    //--tblNewsLetterSiteLookup
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            new ManageNewsLetter().AddNewsLetterSiteLookUp(new tblNewsLetterSiteLookup
                            {
                                SiteID = Guid.Parse(node.Value),
                                NewsID = Guid.Parse(Session["NID"].ToString())
                            });

                        }
                    }
                    Response.Redirect("NewsLetterList.aspx");

                }
                else
                {
                    if (Session["NID"] != null)
                    {
                        tblNewsLetter oNewsLetter = new tblNewsLetter();
                        oNewsLetter.ID = Guid.Parse(Session["NID"].ToString());
                        oNewsLetter.Title = txtTitle.Text;
                        oNewsLetter.NewsLatterContent = (txtContent.Value);
                        oNewsLetter.StartDate = Convert.ToDateTime(DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null).ToString("MM/dd/yyyy"));
                        oNewsLetter.EndDate = Convert.ToDateTime(DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null).ToString("MM/dd/yyyy"));
                        oNewsLetter.CreatedBy = AdminuserInfo.UserID;
                        oNewsLetter.CreatedOn = DateTime.Now;
                        oNewsLetter.IsActive = chkIsActv.Checked;
                        new ManageNewsLetter().SaveNewsLetter(oNewsLetter);

                        //--tblNewsLetterSiteLookup
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                new ManageNewsLetter().AddNewsLetterSiteLookUp(new tblNewsLetterSiteLookup
                                {
                                    SiteID = Guid.Parse(node.Value),
                                    NewsID = Guid.Parse(Session["NID"].ToString())
                                });

                            }
                        }
                        Response.Redirect("NewsLetterList.aspx");

                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        private void FillMenu()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewsLetterList.aspx");
        }
    }
}