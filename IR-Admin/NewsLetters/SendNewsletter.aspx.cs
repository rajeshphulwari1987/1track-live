﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;


namespace IR_Admin.NewsLetters
{
    public partial class SendNewsletter : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUserList(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindUserList(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        public void BindUserList(Guid _SiteID)
        {
            try
            {
                grdSubUsers.DataSource = _master.GetSubscribedUserList(_SiteID).Where(a=>a.IsSubscribed==true && a.IsActive==true).ToList();
                grdSubUsers.DataBind();
            }

            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SubscribedUers.aspx");
        }

        protected void grdSubUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ActiveInActive")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                _master.ActiveInactiveSubsUsers(id);
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUserList(_SiteID);
            }

            if (e.CommandName == "Remove")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                var res = _master.DeleteSubscribedUser(id);
                if (res)
                    ShowMessage(1, "Record deleted successfully.");
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUserList(_SiteID);
            }
        }

        protected void grdSubUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSubUsers.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindUserList(_SiteID);
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["NewsLID"] != null)
                {
                    List<tblNewsLetter> lst = new ManageNewsLetter().GetNewsLettersByID(Guid.Parse(Session["NewsLID"].ToString()));
                    if (lst.Count() > 0)
                    {
                        bool IsSend=false;
                        string UnSubscribePath = System.Configuration.ConfigurationSettings.AppSettings["HttpHost"].ToString() + "NewsLetters/UnSubscribedNewsLetter.aspx";
                        foreach (GridViewRow rw in grdSubUsers.Rows)
                        {
                            Label lblUserInfo = rw.FindControl("lblUserInfo") as Label;
                            CheckBox chkSelect = rw.FindControl("chkSelect") as CheckBox;
                            if (chkSelect.Checked)
                            {
                                string[] str = lblUserInfo.Text.Split(',');
                                bool IsActive = Convert.ToBoolean(str[1]);
                                if (IsActive)
                                {
                                    string TO = str[2];
                                    string Subject = lst.FirstOrDefault().Title;
                                    string Message = lst.FirstOrDefault().NewsLatterContent;
                                    string Body = "<html><head><title></title></head><body><p> Dear " + str[0] +
                                        " , <br /> <br /> " + Message +
                                      "<br /><br />	Thanks &amp; Regards<br />" +
                                      "<br />International Rail</p><br><a href='" + UnSubscribePath + "?id="+str[3]+"'>Click here to unsubscribed.</a></body></html>";
                                    List<tblNewsLetterAttachment> lst1 = new ManageNewsLetter().GetNewsLetterAttachmentsByNewsId(Guid.Parse(Session["NewsLID"].ToString()));
                                    string[] AttachedFiles = { };
                                    string Path = Server.MapPath("~/Uploaded/NewsLetters/");
                                    int i = 0;
                                    if (lst1.Count() > 0)
                                    {
                                        AttachedFiles = new string[lst1.Count()];
                                        foreach (var a in lst1)
                                        {
                                            string hreflink = a.AttchmentPath;
                                            Uri uri = new Uri(hreflink);
                                            string filename = System.IO.Path.GetFileName(uri.LocalPath);
                                            AttachedFiles[i] = Path + filename;
                                            i++;
                                        }

                                    }
                                    IsSend = ManageNewsLetter.SendMail(TO, Subject, Body, AttachedFiles);
                                }
                            }
                            
                        }
                        if (IsSend == true)
                        {
                            ShowMessage(1, "Newsletter send successfully.");
                        }
                    }
                    
                }
                else
                {
                    Response.Redirect("NewsLetterList.aspx");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            Response.Redirect("NewsLetterList.aspx");
        }
    }
}