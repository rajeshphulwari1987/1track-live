﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.NewsLetters
{
    public partial class UnSubscribedNewsLetter : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    Guid ID = Guid.Parse(Request.QueryString["id"]);
                    Boolean res = new ManageNewsLetter().UnSubscribeUserNewsletter(ID);
                }
            }
        }
    }
}