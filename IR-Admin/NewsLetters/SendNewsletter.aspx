﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SendNewsletter.aspx.cs" Inherits="IR_Admin.NewsLetters.SendNewsletter" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            $('.SelectChkAll').click(function () {
                $('#<%=grdSubUsers.ClientID %>').find("input:checkbox").each(function () {
                    if ($('#MainContent_grdSubUsers_chkSelectAll').is(':checked')) {
                        this.checked = true;
                    } else {
                        this.checked = false;
                    } 
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Send Newsletter
    </h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdSubUsers" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="20" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"
                            AllowPaging="True" OnRowCommand="grdSubUsers_RowCommand" OnPageIndexChanging="grdSubUsers_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" class="SelectChkAll" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" class="SelectChk" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="S.No.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UserName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserInfo" runat="server" Visible="false" Text='<%#Eval("Name")+","+ Eval("IsActive").ToString()+","+Eval("Email")+","+Eval("ID") %>'></asp:Label>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <%#Eval("Email")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div style="clear:both;"></div>
                    <div style="margin-top:10px; text-align:center;">
                        <asp:Button ID="btnSend" runat="server" CssClass="button" Text="Send" Width="89px"
                            ValidationGroup="submit" onclick="btnSend_Click" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" 
                            onclick="btnCancel_Click1" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
