﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PrintTicket : System.Web.UI.Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid _siteId;
    public string script;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            Newsletter1.Visible = _masterPage.IsVisibleNewsLetter(_siteId);
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);
                rtPannel.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ProductType"] != null)
            lblBannerTxt.Text = "Rail Tickets";

        if (!IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, _siteId);

            if (Session["TicketBooking-REPLY"] != null && hdnExec.Value != "1")
            {
                var list = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                if (list.Count != 0)
                {
                    ltrTicketMsg.Visible = !string.IsNullOrEmpty(list.FirstOrDefault().URL);
                    rptBeNePrint.DataSource = list;
                    rptBeNePrint.DataBind();
                    hdnExec.Value = "1";
                }
            }
        }
    }

    protected void rptBeNePrint_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var divPrintAtHome = e.Item.FindControl("divPrintAtHome") as HtmlControl;
            if (Session["showPrintAtHome"] != null)
            {
                if (Session["showPrintAtHome"].ToString() == "1")
                    if (divPrintAtHome != null)
                        divPrintAtHome.Visible = true;
            }
            Session.Remove("divPrintAtHome");
        }
    }
}