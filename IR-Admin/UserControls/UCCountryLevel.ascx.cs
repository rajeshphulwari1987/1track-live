﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data.SqlClient;
using System.Data;

public partial class UserControls_UCCountryLevel : System.Web.UI.UserControl
{
    public ManageProduct _master = new ManageProduct();
    public List<SetCountry> list = new List<SetCountry>();
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    public static int LevelID = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            selcunid2.Visible = false;
            selcunid3.Visible = false;
            selcunid4.Visible = false;
            selcunid5.Visible = false;
            selcunid6.Visible = false;
            if (Session["GetcunLvl"] != null && Convert.ToInt32(Session["GetcunLvl"]) > 2)
            {
            LevelID = Convert.ToInt32(Session["GetcunLvl"]);
            GetCountryLevelData(null, null, null, null, 1);
            Bindcountry();
            }
        }
    }
    public void Bindcountry()
    {
        if (list != null)
        {
            
            ddlCunt1.DataSource = list.OrderBy(ty => ty.Country).ToList();
            ddlCunt1.DataTextField = "Country";
            ddlCunt1.DataValueField = "ID";
            ddlCunt1.DataBind();
            ddlCunt1.Items.Insert(0, new ListItem("-- Select the first country --", "0"));
            selcunid1.Visible = true;
        }
    }
    protected void ddlCunt1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), null, null, null, 2);
        Session["SelectLevel"] = 1;
        if (list != null)
        {
            ddlCunt2.DataSource = list.OrderBy(ty => ty.Country).ToList();
            ddlCunt2.DataTextField = "Country";
            ddlCunt2.DataValueField = "ID";
            ddlCunt2.DataBind();
            ddlCunt2.Items.Insert(0, new ListItem("-- Select the second country --", "0"));
            selcunid2.Visible = true;
        }
    }
    protected void ddlCunt2_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), Guid.Parse(ddlCunt2.SelectedValue), null, null, 3);
        if (list != null)
        {
            Session["SelectLevel"] = 2;
            ddlCunt3.DataSource = list.OrderBy(ty => ty.Country).ToList();
            ddlCunt3.DataTextField = "Country";
            ddlCunt3.DataValueField = "ID";
            ddlCunt3.DataBind();
            ddlCunt3.Items.Insert(0, new ListItem("-- Select the third country --", "0"));
            selcunid3.Visible = true;
        }
    }
    protected void ddlCunt3_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["SelectLevel"] = 3;
        if ((LevelID - 3 != 0))
            GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), Guid.Parse(ddlCunt2.SelectedValue), Guid.Parse(ddlCunt3.SelectedValue), null, 4);
        if (list != null && list.Count > 0)
        {
            ddlCunt4.DataSource = list.OrderBy(ty => ty.Country).ToList();
            ddlCunt4.DataTextField = "Country";
            ddlCunt4.DataValueField = "ID";
            ddlCunt4.DataBind();
            ddlCunt4.Items.Insert(0, new ListItem("-- Select the fourth country --", "0"));
            selcunid4.Visible = true;
        }
        ChkLevelid(LevelID - 3);
    }
    protected void ddlCunt4_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["SelectLevel"] = 4;
        if ((LevelID - 4 != 0))
            GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), Guid.Parse(ddlCunt2.SelectedValue), Guid.Parse(ddlCunt3.SelectedValue), Guid.Parse(ddlCunt4.SelectedValue), 5);
        if (list != null && list.Count > 0)
        {
            ddlCunt5.DataSource = list.OrderBy(ty => ty.Country).ToList();
            ddlCunt5.DataTextField = "Country";
            ddlCunt5.DataValueField = "ID";
            ddlCunt5.DataBind();
            ddlCunt5.Items.Insert(0, new ListItem("-- Select the fifth country --", "0"));
            selcunid5.Visible = true;
        }
        ChkLevelid(LevelID - 4);
    }
    protected void ddlCunt5_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["SelectLevel"] = 5;
        ChkLevelid(0);
    }
    public void ChkLevelid(int ChkLevelid)
    {
        if (ChkLevelid == 0)
        {
            selcunid6.Visible = true;
        }
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {

        Guid c1 = Guid.Parse(ddlCunt1.SelectedValue);
        Guid c2 = Guid.Parse(ddlCunt2.SelectedValue);
        Guid? c3 = null;
        Guid? c4 = null;
        Guid? c5 = null;

        if (!string.IsNullOrEmpty(ddlCunt3.SelectedValue))
            c3 = Guid.Parse(ddlCunt3.SelectedValue);
        if (!string.IsNullOrEmpty(ddlCunt4.SelectedValue))
            c4 = Guid.Parse(ddlCunt4.SelectedValue);
        if (!string.IsNullOrEmpty(ddlCunt5.SelectedValue))
            c5 = Guid.Parse(ddlCunt5.SelectedValue);
 
        String StartCode = GetCountryCodeValue(c1, c2, c3, c4, c5, LevelID);
        Session["GETcountryCODE"] = StartCode + "," + null + "ñ" + c1 + "," + c2 + "," + c3 + "," + c4 + "," + c5;
        if (ddlCunt1.Items.Count > 0)
            ddlCunt1.SelectedIndex = 0;
        if (ddlCunt2.Items.Count > 0)
            ddlCunt2.SelectedIndex = 0;
        if (ddlCunt3.Items.Count > 0)
            ddlCunt3.SelectedIndex = 0;
        if (ddlCunt4.Items.Count > 0)
            ddlCunt4.SelectedIndex = 0;
        if (ddlCunt5.Items.Count > 0)
            ddlCunt5.SelectedIndex = 0;
        selcunid2.Visible = false;
        selcunid3.Visible = false;
        selcunid4.Visible = false;
        selcunid5.Visible = false;
        selcunid6.Visible = false;
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "hideparent()", true);
    }

    #region Execute Procedure For country data
    public List<SetCountry> GetCountryLevelData(Guid? Country1, Guid? Country2, Guid? Country3, Guid? Country4, int productcountrycount)
    {
        try
        {
            SqlDataReader sqlDataReader;
            using (SqlConnection conn = new SqlConnection(con))
            {
                SqlCommand sqlCommand = new SqlCommand("SP_GetCountryAllLevel", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Country1", Country1);
                sqlCommand.Parameters.AddWithValue("@Country2", Country2);
                sqlCommand.Parameters.AddWithValue("@Country3", Country3);
                sqlCommand.Parameters.AddWithValue("@Country4", Country4);
                sqlCommand.Parameters.AddWithValue("@productcountrycount", productcountrycount);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataSet ds = new DataSet();
                da.Fill(ds); 
                int count = ds.Tables[0].Rows.Count;
                for (int i = 0; i < count; i++)
                {
                    SetCountry getdata = new SetCountry();
                    getdata.ID = Convert.ToString(ds.Tables[0].Rows[i]["EurailCountryID"]);
                    getdata.Country = Convert.ToString(ds.Tables[0].Rows[i]["country"]);
                    list.Add(getdata);
                }
                conn.Close();
                return list;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string GetCountryCodeValue(Guid? Country1, Guid? Country2, Guid? Country3, Guid? Country4, Guid? Country5, int productcountrycount)
    {
        try
        {
            string code = string.Empty;
            SqlDataReader sqlDataReader;
            using (SqlConnection conn = new SqlConnection(con))
            {
                SqlCommand sqlCommand = new SqlCommand("SP_GetEurailPassCountryCode", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Country1", Country1);
                sqlCommand.Parameters.AddWithValue("@Country2", Country2);
                sqlCommand.Parameters.AddWithValue("@Country3", Country3);
                sqlCommand.Parameters.AddWithValue("@Country4", Country4);
                sqlCommand.Parameters.AddWithValue("@Country5", Country5);
                sqlCommand.Parameters.AddWithValue("@productcountrycount", productcountrycount);
                conn.Open();
                SqlDataReader dr = sqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    code = dr.GetValue(0).ToString();
                }
                conn.Close();
                return code;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion
    public class SetCountry
    {
        public string Country { get; set; }
        public string ID { get; set; }
    }
}