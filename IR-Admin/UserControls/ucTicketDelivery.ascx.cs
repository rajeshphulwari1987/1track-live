﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using OneHubServiceRef;
using Business;

public partial class UserControls_ucTicketDelivery : UserControl
{
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["TrainSearchRequest"] != null && !IsPostBack)
                    BindList();
                BindCollectionList();
                BindDeliveryOption();
                GetCharge();

                var obMaster = new Masters();
                ddlCountryMail.DataSource = obMaster.GetCountryList().OrderBy(x => x.CountryName).ToList();
                ddlCountryMail.DataTextField = "CountryName";
                ddlCountryMail.DataValueField = "CountryID";
                ddlCountryMail.DataBind();
                ddlCountryMail.Items.Insert(0, new ListItem("--Select Country--", "-1"));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void BindCollectionList()
    {
        var stcode = new List<string> { "BE", "NL" };
        var list = _db.StationNameLists.Where(x => x.IsActive == true && x.RailName == "BENE" && stcode.Contains(x.StationFilterCode)).OrderBy(x => x.StationName).ToList();
        if (list == null && list.Count() == 0)
            return;
    }

    void BindDeliveryOption()
    {
        try
        {
            if (Session["BOOKING-REQUEST"] != null)
            {
                var list = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                var dlist = list.SelectMany(x => x.PurchasingForServiceRequest.BookingRequestList.SelectMany(y => y.TrainPrice.SelectMany(z => z.ProductionModeList))).Distinct().ToList();
                var listnew = new List<ProductionModes>();
                foreach (var item in dlist)
                {
                    if (listnew == null || !listnew.Any(x => x.DeliveryMethod == item.DeliveryMethod))
                        listnew.Add(item);
                }

                rdoBkkoingList.DataSource = listnew.OrderBy(x => x.ProductionDisplayMode).OrderBy(x=>x.DeliveryMethod).ToList();
                rdoBkkoingList.DataTextField = "ProductionDisplayMode";
                rdoBkkoingList.DataValueField = "DeliveryMethod";
                rdoBkkoingList.DataBind();

                if (rdoBkkoingList.Items.FindByValue("DH") != null)
                    rdoBkkoingList.Items.FindByValue("DH").Selected = true;
                else
                    rdoBkkoingList.SelectedIndex = 0;

                BindOptionArea();
            }
        }
        catch { }
    }

    void BindDeliveryOptionST()
    {
        try
        {
            if (Session["BOOKING-REQUEST"] != null)
            {
                var list = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                var dlist = list.SelectMany(x => x.PurchasingForServiceRequest.BookingRequestList.SelectMany(y => y.TrainPrice.SelectMany(z => z.ProductionModeList))).Distinct().ToList();
                var listnew = new List<ProductionModes>();
                foreach (var item in dlist)
                {
                    if (listnew == null || !listnew.Any(x => x.DeliveryMethod == item.DeliveryMethod))
                        listnew.Add(item);
                }

                rdoBkkoingList.DataSource = listnew.OrderBy(x => x.ProductionDisplayMode).OrderBy(x => x.DeliveryMethod).Where(x => x.DeliveryMethod != "ST").OrderBy(x => x.DeliveryMethod).ToList();
                rdoBkkoingList.DataTextField = "ProductionDisplayMode";
                rdoBkkoingList.DataValueField = "DeliveryMethod";
                rdoBkkoingList.DataBind();
                rdoBkkoingList.SelectedIndex = 0; 
                BindOptionArea();
            }
        }
        catch { }
    }
    void GetCharge()
    {
        try
        {
            var p2pCurrency = _db.tblP2PDeliveryChargesMst.FirstOrDefault(x => x.SiteID == siteId && x.IsActive == true);
            if (p2pCurrency != null)
            {
                lblAmount.Text = p2pCurrency.Amount.ToString();
                var curID = p2pCurrency.CurrencyId;
                if (curID != null)
                {
                    var currency = _db.tblCurrencyMsts.FirstOrDefault(x => x.ID == curID);
                    if (currency != null)
                        lblCurrncy.Text = currency.Symbol;
                }
            }
        }
        catch { }
    }

    public void BindList()
    {
        var objrequest = Session["TrainSearchRequest"] as TrainInformationRequest;
        if (objrequest.Loyaltycards != null)
        {
            foreach (var item in objrequest.Loyaltycards.Where(x => x.carrier.code == "EUR").ToList())
                lblEuCardNumber.Text = string.IsNullOrEmpty(lblEuCardNumber.Text) ? item.cardnumber : lblEuCardNumber.Text + ", " + item.cardnumber;

            foreach (var item in objrequest.Loyaltycards.Where(x => x.carrier.code == "THA").ToList())
                lblThCardNumber.Text = string.IsNullOrEmpty(lblThCardNumber.Text) ? item.cardnumber : lblThCardNumber.Text + ", " + item.cardnumber;

            BindLoyaltyList(objrequest.NumAdults, objrequest.NumYouths, objrequest.NumSeniors, objrequest.NumBoys, objrequest.Loyaltycards.Where(x => x.carrier.code == "THA").ToArray());
            divLoyalty.Visible = true;
        }
        else
            BindLoyaltyList(objrequest.NumAdults, objrequest.NumYouths, objrequest.NumSeniors, objrequest.NumBoys, null);
        BindPassengerList(objrequest.NumAdults, objrequest.NumYouths, objrequest.NumSeniors, objrequest.NumBoys);
    }

    public void BindLoyaltyList(int Adult, int youth, int senior, int child, Loyaltycard[] loyArray)
    {

        var list = new List<Passanger>();
        int countLoay = 0;
        if (loyArray != null)
            countLoay = loyArray.Count();
        if (countLoay == 0)
            loyArray = null;

        int cnt = Adult;
        for (int i = 0; i < cnt; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Adult",
                cardnumber = loyArray != null && countLoay >= i ? loyArray[i].cardnumber : ""
            });

            if (countLoay > i)
                countLoay--;
        }

        int cntc = child;
        for (int i = 0; i < cntc; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Child",
                cardnumber = loyArray != null && countLoay >= i ? loyArray[i].cardnumber : ""
            });
            if (countLoay > i)
                countLoay--;
        }

        int cntyouth = youth;
        for (int i = 0; i < cntyouth; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Youth",
                cardnumber = loyArray != null && countLoay >= i ? loyArray[i].cardnumber : ""
            });
            if (countLoay > i)
                countLoay--;
        }

        int cntS = senior;
        for (int i = 0; i < cntS; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Senior",
                cardnumber = loyArray != null && countLoay >= i ? loyArray[i].cardnumber : ""
            });
            if (countLoay > i)
                countLoay--;
        }

        dtlLoayalty.DataSource = list;
        dtlLoayalty.DataBind();
    }

    public void BindPassengerList(int Adult, int youth, int senior, int child)
    {
        var list = new List<Passanger>();
        for (int i = 0; i < Adult; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Adult" + (i + 1).ToString()
            });
        }

        for (int i = 0; i < child; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Child" + (i + 1).ToString()
            });
        }

        for (int i = 0; i < youth; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Youth" + (i + 1).ToString()
            });
        }

        for (int i = 0; i < senior; i++)
        {
            list.Add(new Passanger
            {
                PassangerType = "Senior" + (i + 1).ToString()
            });
        }

        dtlPassngerDetails.DataSource = list;
        dtlPassngerDetails.DataBind();

        dtlPassngerDetails2.DataSource = list;
        dtlPassngerDetails2.DataBind();
    }

    protected void rdoBkkoingList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindOptionArea();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showmessagecountry", "showmessagecountry()", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void BindOptionArea()
    {
        hiddenDileveryMethod.Value = rdoBkkoingList.SelectedValue;
        ltrDilverName.Text = rdoBkkoingList.SelectedItem.Text;
        reqMailFName.Enabled = reqMailLName.Enabled = reqStreet.Enabled = reqPostalCode.Enabled = reqCountryMail.Enabled = false;
        if (hiddenDileveryMethod.Value == "TL")
        {
            dtlPassngerDetails2.Visible = dtlPassngerDetails.Visible = false;
            deliverymethodTL.Style.Add("display", "block");
            deliverymethodDH.Style.Add("display", "none");
            deliverymethodMail.Style.Add("display", "none");
        }
        else if (hiddenDileveryMethod.Value == "TA")
        {
            dtlPassngerDetails2.Visible = dtlPassngerDetails.Visible = false;
            deliverymethodMail.Style.Add("display", "block");
            deliverymethodTL.Style.Add("display", "none");
            deliverymethodDH.Style.Add("display", "none");
            reqMailFName.Enabled = reqMailLName.Enabled = reqStreet.Enabled = reqPostalCode.Enabled = reqCountryMail.Enabled = true;
        }
        else
        {
            dtlPassngerDetails2.Visible = false;
            dtlPassngerDetails.Visible = true;
            deliverymethodTL.Style.Add("display", "none");
            deliverymethodMail.Style.Add("display", "none");
            deliverymethodDH.Style.Add("display", "block");
        }

        if (hiddenDileveryMethod.Value == "ST")
        {
            dtlPassngerDetails2.Visible = dtlPassngerDetails.Visible = false;
            HeaderDhMsg.Style.Add("display", "none");
            FooterDhMsg2.Style.Add("display", "none");

            HeaderStMsg.Style.Add("display", "block");
            FooterStMsg.Style.Add("display", "block");
            reqCollectStation.Enabled = true;
            var objreq = Session["TrainSearchRequest"] as TrainInformationRequest;
            if (objreq != null) ddlCollectStation.SelectedValue = objreq.DepartureStationCode;
            if (ddlCollectStation.SelectedValue == "0")
            {
                deliverymethodDH.Style.Add("display", "none");
                deliverymethodMail.Style.Add("display", "block");
                BindDeliveryOptionST();
            }
        }
        else
        {
            reqCollectStation.Enabled = false;
            ddlCollectStation.SelectedIndex = 0;
            HeaderDhMsg.Style.Add("display", "block");
            FooterDhMsg2.Style.Add("display", "block");

            HeaderStMsg.Style.Add("display", "none");
            FooterStMsg.Style.Add("display", "none");
        }

        foreach (DataListItem li in dtlLoayalty.Items)
        {
            var req = (RequiredFieldValidator)li.FindControl("reqCardNumber");
            req.Enabled = (hiddenDileveryMethod.Value == "TL");
        }

        var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
        foreach (DataListItem li in dtlPassngerDetails.Items)
        {
            var reqFirstName = (RequiredFieldValidator)li.FindControl("reqFirstName");
            reqFirstName.Enabled = (hiddenDileveryMethod.Value != "TL");

            var reqLastName = (RequiredFieldValidator)li.FindControl("reqLastName");
            reqLastName.Enabled = (hiddenDileveryMethod.Value != "TL");
        }

        if (hiddenDileveryMethod.Value == "TA")
        {
            foreach (DataListItem li in dtlPassngerDetails2.Items)
            {
                var txtFirstName = li.FindControl("txtFirstName") as TextBox;
                var txtLastName = li.FindControl("txtLastName") as TextBox;

                foreach (var item in bookingList)
                {
                    if (item.PurchasingForServiceRequest != null)
                    {
                        var rec = item.PurchasingForServiceRequest.PassengerListReply.FirstOrDefault();
                        if (txtFirstName != null) txtFirstName.Text = rec.FirstName;
                        if (txtLastName != null) txtLastName.Text = rec.LastName;
                    }
                    break;
                }
                break;
            }
        }
        else
        {
            foreach (DataListItem li in dtlPassngerDetails.Items)
            {
                var txtFirstName = li.FindControl("txtFirstName") as TextBox;
                var txtLastName = li.FindControl("txtLastName") as TextBox;

                foreach (var item in bookingList)
                {
                    if (item.PurchasingForServiceRequest != null)
                    {
                        var rec = item.PurchasingForServiceRequest.PassengerListReply.FirstOrDefault();
                        if (txtFirstName != null) txtFirstName.Text = rec.FirstName;
                        if (txtLastName != null) txtLastName.Text = rec.LastName;
                    }
                    break;
                }
                break;
            }
        }

        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "showHideShipping", "showHideShipping();", true);
    }
    protected void ddlCountryMail_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCountryMail.SelectedValue != "-1")
        {
            var states = _masterBooking.GetCountyList(Guid.Parse(ddlCountryMail.SelectedValue));
            if (states != null)
            {
                ddlStateMail.DataSource = states;
                ddlStateMail.DataTextField = "County";
                ddlStateMail.DataValueField = "CountyID";
                ddlStateMail.DataBind();
            }
        }
    }
}