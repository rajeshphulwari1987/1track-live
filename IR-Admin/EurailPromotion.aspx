﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="EurailPromotion.aspx.cs"
    Inherits="IR_Admin.EurailPromotion" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">   
        function pageLoad() {$('#MainContent_txtDesp').redactor({ iframe: true, minHeight: 200 });}

        $(function () {                  
             if(<%=Tab%>=="1")
            {
                  $("ul.list").tabs("div.panes > div");
                  $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {              
              $("ul.tabs").tabs("div.inner-tabs-container > div");               
                          
            }         
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            $(".tabs a").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).parent("li").addClass("active");
            });

            if ('<%=Tab%>' == "2") {
                $(".list a").click(function () {

                    $("ul.list").tabs("div.panes > div");
                });
            }
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
            $("#MainContent_txtshortorder").focus(function () { $(this).select(); });
            $("#MainContent_txtshortorder").blur(function () {
                var value = $(this).val();
                if (!$.isNumeric(value)) {
                    $(this).val('0');
                }
            });
            $("#btncloseshortorderpnl").click(function () {
                $("#shortorderpnl").hide();
            });
            $(".txtsortordernumeric").blur(function () {
                if (!$.isNumeric($(this).val()))
                    $(this).val('');
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        #shortorderpnl
        {
            float:left;
            position:absolute;
        }        
        img
        {
            vertical-align: middle;
            border: none;
        }
        .lnksortorder
        {
            cursor: pointer;
            background: #E2E2E2;
            border: 1px solid #ccc;
            width: 150px;
            border-radius: 5px;
            position: relative;
            padding: 5px;
            text-decoration: blink;
            color: #414141;
        }
        .lnksortorder:hover
        {
            color: black;
            text-decoration: blink !important;
        }        
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
       Promotions </h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="EurailPromotion.aspx" class="current">List</a></li>
            <li><a id="aNew" href="EurailPromotion.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdPromotion" runat="server" CellPadding="4" DataKeyNames="ID"
                        CssClass="grid-head2" OnRowCommand="grdPromotion_RowCommand" ForeColor="#333333"
                        GridLines="None" AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:BoundField DataField="Title" HeaderText="Title" />
                            <asp:TemplateField HeaderText="Short Order">
                                <ItemTemplate>
                                        <%#Eval("sortorder")%>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnksortorder" runat="server" CssClass="lnksortorder" CommandName="sortorder"
                                        CommandArgument='<%#Eval("ID")%>'>Change Order</asp:LinkButton>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                </ItemTemplate>
                                <ItemStyle Width="30%"></ItemStyle>
                                <HeaderStyle />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <!-- tab "panes" -->
                                <div class="inner-tabs-container">
                                    <div id="tabs-inner-1" class="grid-sec2" style="display: block; width: 700px;">
                                        <table class="tblMainSection">
                                            <tr>
                                                <td class="col">
                                                    Title
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtTitle" runat="server" />
                                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtTitle"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Navigation URL
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtNavUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    Description
                                                </td>
                                                <td colspan="4">
                                                    <textarea id="txtDesp" runat="server"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 5px;" class="col">
                                                    Image
                                                </td>
                                                <td class="col">
                                                    <asp:FileUpload ID="fupPromotionImg" runat="server" Width="200px" />
                                                    <asp:LinkButton ID="lnkImg" runat="server" Text="View Image" CssClass="clsLink" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Is Active
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkactive" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr align="center">
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="CForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="BtnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td class="col">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlImg" runat="server" CssClass="popup pImg">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Image</div>
            <a href="#" id="btnClose" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgPromotion" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg"
                Width="272px" Height="190px" />
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdpupFlag" runat="server" TargetControlID="lnkImg" CancelControlID="btnClose"
        PopupControlID="pnlImg" BackgroundCssClass="modalBackground" />
    <div id="shortorderpnl" style="display: none;" runat="server" clientidmode="Static">
        <div class="modalBackground">
        </div>
        <div class="popup" style="position: relative; width: 700px; left: 136px;">
            <div class="dvPopupHead" style="line-height: 34px; margin-left: -4px;">
                <div class="dvPopupTxt">
                    Change Sorting Order
                </div>
                <a id="btncloseshortorderpnl" style="cursor: pointer; color: #fff;">X</a>
            </div>
            <div class="error" style="margin-top: 45px;" id="diverrorshortorder" runat="server" visible="false">
            <asp:Label ID="lblerrorshortorder" runat="server"></asp:Label></div>
            <table class="tblsortorder grid-sec2" style="width: 700px; border-spacing: 0px; font-size: 13px;margin-bottom: 0px;padding: 5px;">
                <tr>
                    <td class="col">
                        Title
                    </td>
                    <td class="col">
                        <asp:Label runat="server" ID="lblTitle"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdnsortorderid" />
                    </td>
                </tr>
                <tr>
                    <td class="col">
                        Current Sort Order
                    </td>
                    <td class="col">
                        <asp:Label runat="server" ID="lblSortorder"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="col">
                        Change Sort Order
                    </td>
                    <td class="col">
                        <asp:TextBox runat="server" ID="txtSortorder" CssClass="txtsortordernumeric"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                <td class="col"></td>
                <td class="col">
                <asp:Button runat="server" Text="Submit" onclick="Submit_Click" />
                </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
