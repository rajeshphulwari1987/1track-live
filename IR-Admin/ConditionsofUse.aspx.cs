﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ConditionsofUse : Page
    {
        readonly private ManageBooking _master = new ManageBooking();
        readonly private Masters _oMasters = new Masters();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                ShowMessage(0, null);
                BindSite();
                BindGrid(_SiteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetConditionsForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp",
                                                        "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            Tab = "1";
            grdConditions.DataSource = _master.GetConditionsList(_SiteID);
            grdConditions.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdConditions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdConditions.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }

        protected void grdConditions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactiveConditions(id);
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }

                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteConditions(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #region Add/Edit
        public void BindSite()
        {
            //Site
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddConditions()
        {
            try
            {
                _master.AddConditions(new tblConditionsofUse
                {
                    ID = Guid.NewGuid(),
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.Text.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    WarrantyLiability = txtWl.InnerHtml,
                    Products = txtPrd.InnerHtml,
                    UseWebsite = txtWeb.InnerHtml,
                    AntiViralSoftware = txtAnti.InnerHtml,
                    Copyright = txtCopy.InnerHtml,
                    IsActive = chkIsActv.Checked,
                });
                ShowMessage(1, "Conditions of use added successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdateConditions()
        {
            try
            {
                var id = Guid.Parse(Request["id"]);
                var conditions = new tblConditionsofUse
                {
                    ID = id,
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.Text.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    WarrantyLiability = txtWl.InnerHtml,
                    Products = txtPrd.InnerHtml,
                    UseWebsite = txtWeb.InnerHtml,
                    AntiViralSoftware = txtAnti.InnerHtml,
                    Copyright = txtCopy.InnerHtml,
                    IsActive = chkIsActv.Checked,
                };
                _master.UpdateConditions(conditions);
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, "Conditions of use updated successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddConditions();
                else
                    UpdateConditions();
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ConditionsofUse.aspx");
        }

        public void GetConditionsForEdit(Guid id)
        {
            var oP = _master.GetConditionsById(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = oP.SiteID.ToString();
                txtTitle.Text = oP.Title;
                txtDesc.Text = oP.Description;
                txtWl.InnerHtml = oP.WarrantyLiability;
                txtPrd.InnerHtml = oP.Products;
                txtWeb.InnerHtml = oP.UseWebsite;
                txtAnti.InnerHtml = oP.AntiViralSoftware;
                txtCopy.InnerHtml = oP.Copyright;
                if (oP.IsActive != null) chkIsActv.Checked = (bool)oP.IsActive;
            }
        }

        #endregion
    }
}