﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_AboutUs.aspx.cs"
    Inherits="IR_Admin.Manage_AboutUs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>:: International Rail :: </title>
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="width" />
    <script type="text/javascript" src="Scripts/html5.js"></script>
    <script src="editor/jquery.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important; /*background-color: #FBDEE6;*/
        }
        .edit-btn
        {
            background: #c5456a url("images/icon-edit.png")10px 5px no-repeat;
            border: medium none;
            border-radius: 0 0 0 0 !important;
            box-shadow: 3px 3px 3px 0 #000000;
            color: #fff;
            cursor: pointer;
            font-size: 14px;
            font-weight: bold;
            height: 28px;
            padding: 0 15px 0 35px;
            position: absolute;
            width: 70px;
            left: 10px;
            top: 10px;
        }
        
        .wrapper
        {
            width: 1004px;
        }
        .clsDisable
        {
            background-color: #D4D4D4;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 700px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        .clsRead
        {
            float: right;
            font-size: 11px;
            color: #951f35;
        }
        .read
        {
            background: url(images/page/arr-red.jpg) no-repeat left;
            padding-left: 10px;
        }
        .clsAbs
        {
            display: block;
            position: absolute;
        }
        .pTop40
        {
            padding-top: 40px;
        }
        .pTop5
        {
            padding-top: 5px;
        }
        .button
        {
            min-width: 60px;
            width: auto;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px;
            border-right: 1px thin #a1a1a1;
            border: thin none;
            font-weight: bold;
            color: white;
            cursor: pointer;
            height: 30px;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(
	        function () {
	            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
	            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
	            $('#txtFooter').redactor({ iframe: true, minHeight: 200 });
	            $('#txtRtPanel').redactor({ iframe: true, minHeight: 200 });
	        }
        );

        $(document).ready(function () {

            $("input[class=rdrtID]").attr('name', "img");
            
            //----------Edit heading---------//
            $(".edit").click(function () {
                var value;
                if ($(this).attr("rel") == "ContentHead") {
                    $("#divHeading").slideToggle("slow");
                    value = $('#ContentHead').html();
                    $('.redactor_rdHead').contents().find('body').html(value);
                } else if ($(this).attr("rel") == "ContentText") {

                    $("#divContent").slideToggle("slow");
                    value = $('#ContentText').html();
                    $('.redactor_rdContent').contents().find('body').html(value);
                } else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRt").slideToggle("slow");
                }
            });

            //-------Edit footer images-------//
            $(".editFooter").click(function () {
                $("#ContentFooter").slideToggle("slow");
                $('.hdnfooter').val($(this).attr("rel"));
            });

            //-------Edit footer images text-------//
            $(".editFooterText").click(function () {
                $("#ContentFooterTxt").slideToggle("slow");
                $('.hdnfooterTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "footerHead1") {
                    value = $('#footerHead1').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerTxt1") {
                    value = $('#footerTxt1').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerHead2") {
                    value = $('#footerHead2').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerTxt2") {
                    value = $('#footerTxt2').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerHead3") {
                    value = $('#footerHead3').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerTxt3") {
                    value = $('#footerTxt3').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerHead4") {
                    value = $('#footerHead4').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "footerTxt4") {
                    value = $('#footerTxt4').html();
                    $('.redactor_rdFooter').contents().find('body').html(value);
                }
            });


            //-------Edit right panel images-------//
            $(".editRtPanel").click(function () {
                $("#ContentRight").slideToggle("slow");
                $('.hdnRight').val($(this).attr("rel"));
            });


            //-------Edit right panel images text-------//
            $(".editRtPanelText").click(function () {
                $("#ContentRightTxt").slideToggle("slow");
                $('.hdnrtPanelTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "rightHead1") {
                    value = $('#rightHead1').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                if ($(this).attr("rel") == "rightTxt1") {
                    value = $('#rightTxt1').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
            });

            //------Edit banner images------//
            $(".editBanner").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });

            //-----Close-----//
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "ContentFooter") {
                    $("#ContentFooter").hide();
                }
                else if ($(this).attr("rel") == "ContentFooterTxt") {
                    $("#ContentFooterTxt").hide();
                }
                else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRight").hide();
                }
                else if ($(this).attr("rel") == "ContentRightTxt") {
                    $("#ContentRightTxt").hide();
                }
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //------Save homepage footer images------//
            $("#btnSaveFooter").click(function () {
                var imagename;
                $('div#ContentFooter input[type=radio]').each(function () {
                    if ($('.hdnfooter').val() == "footer1") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgFooter1").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnfooter').val() == "footer2") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgFooter2").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnfooter').val() == "footer3") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgFooter3").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnfooter').val() == "footer4") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgFooter4").attr("src", imagename);
                        }
                    }
                    $("#ContentFooter").hide();
                });
            });

            //-----Save footer list text------//
            $(".btnsaveFootertxt").click(function () {
                var value = $('textarea[name=txtFooter]').val();
                if ($('.hdnfooterTxt').val() == "footerHead1") {
                    if (value != "")
                        $('#footerHead1').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerTxt1") {
                    if (value != "")
                        $('#footerTxt1').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerHead2") {
                    if (value != "")
                        $('#footerHead2').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerTxt2") {
                    if (value != "")
                        $('#footerTxt2').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerHead3") {
                    if (value != "")
                        $('#footerHead3').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerTxt3") {
                    if (value != "")
                        $('#footerTxt3').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerHead4") {
                    if (value != "")
                        $('#footerHead4').html(value);
                }
                else if ($('.hdnfooterTxt').val() == "footerTxt4") {
                    if (value != "")
                        $('#footerTxt4').html(value);
                }
                $("#ContentFooterTxt").hide();
            });

            //------Save right panel images------//
            $("#btnSaveRtPanel").click(function () {
                var imagename;
                $('div#ContentRight input[type=radio]').each(function () {
                    if ($('.hdnRight').val() == "rtPanelImgTop") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgRtPanelTop").attr("src", imagename);
                        }
                    }
                    $("#ContentRight").hide();
                });
            });

            //-----Save right panel text------//
            $(".btnSaveRttxt").click(function () {
                var value = $('textarea[name=txtRtPanel]').val();
                if ($('.hdnrtPanelTxt').val() == "rightHead1") {
                    if (value != "")
                        $('#rightHead1').html(value);
                }
                else if ($('.hdnrtPanelTxt').val() == "rightTxt1") {
                    if (value != "")
                        $('#rightTxt1').html(value);
                }
                $("#ContentRightTxt").hide();
            });

        });
	        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="wrapper">
<header>
    <div class="logo"> 
    <img src="images/page/logo.png" class="scale-with-grid" alt="" border="0" />
    </div>
    <div class="top-rightblock">
    <div class="toplink">
    <a href="#"> Feedback </a> | <a href="#"> Sitemap   </a>| <a href="#"> Security  </a> | <a href="#"> Agent </a>
    </div>
    <div class="call"> 
    <img src="images/page/icon-call.png" alt="" border="0" class="scale-with-grid" />
    <span> +44 (0) 871 231 0790 </span>
    </div>
    </div>
</header>
<nav>
<ul> 
    <li><a href="#"> Home  </a> </li>
    <li><a href="#"> Countries </a> </li>
    <li><a href="#" class="active"> Rail Passes   </a> </li>
    <li><a href="#"> Speciality Trains  </a> </li>
    <li><a href="#"> Accommodation  </a> </li>
    </ul>
    <div class="agentlogin"> <a href="#"> <img src="images/page/icon-basket.png" alt="" border="0" width='27' height='25' /> Basket </a></div>
</nav>
<div class="banner">
    <%--Banner section--%>
    <div id="dvBanner">
        <div id="div3" class="" style="display: block; position: absolute;">
            <input type="button" class="editBanner edit-btn" value="Edit"/>
        </div>
        <img id="imgMainBanner" src="images/page/banner.jpg" alt="" border="0" />
        <asp:HiddenField ID="hdnBannerIDs" runat="server"/>
    </div>
    <%--Banner section end--%>
</div>

<section  class="content">
<div class="left-content">
    
    <div class="cms">
       <div id="divList1" class="clsAbs">
            <input type="button" class="edit edit-btn" value="Edit" rel="ContentHead"/>
       </div>
       <div class="pTop40">
                <div id="ContentHead" runat="server">
                     <h1><asp:Label ID="lblHead" runat="server" Text="About us - We have over 20 years experience in the rail industry"></asp:Label></h1>
                </div>
            </div>
    </div>
        
    <div class="cms">
      <div id="div1" class="clsAbs">
         <input type="button" class="edit edit-btn" value="Edit" rel="ContentText"/>
      </div>
      <div class="pTop40">
         <div id="ContentText" runat="server">
        <p><asp:Label ID="lblContent" runat="server" Text="International Rail is an independent limited liability company, specializing in the sale and distribution of Rail Tickets and Passes for rail travel, world-wide. With over 20 years in the rail industry, we have been able to put our experience to good use to create the most comprehensive international rail booking website in existence. Our UK headquarters are based in Hampshire and here we have our call center where our highly trained staff are there to help you plan and cost your journey, to help you make the most of your time away. Our product portfolio is extensive and is constantly being updated. In Europe it includes over 66,000 Point to Point routes including Eurostar, French TGV, German ICE, InterCity, EuroCity and Spanish high speed trains.
            When booking online, rest assured that your personal and account information is secure on our sites as we employ the latest 128-bit encryption technology in all parts of our site that require any personal information exchange including the payment pages.Rail fares have become as complex as air fares and require specialist consultants to ensure the best ticket price is obtained.  The route, journey time and ticket type will affect the price of each ticket.  Our technology platforms are geared to sourcing the lowest available fare and in addition, we routinely offer excellent discounts on standard ticket prices for our customers based on different time options and through the combination of one way fares.
            If we are using a photograph under copyright and by mistake, omit to show the correct copyright details, we apologize to those concerned and once notified, will remove the picture straight away, or if allowed, make reference to the owner in the correct way. It is not our intention to use any picture without authorization."/></p>
        </div>
      </div>
    </div>

<div class="clear"> &nbsp;</div>

<div class="country-block-outer" style="margin-top:10px">
<div class="country-block-inner">
<div id="footerBlock" runat="server">
    <div class="contory-block">
    <div class="cms" id="footer1">
        <div class="clsAbs">
        <input type="button" class="editFooter edit-btn" value="Edit" rel="footer1"/>
    </div>
        <div class="pTop5">
        <img id="imgFooter1" alt="" border="0" runat="server" />
        <input type="hidden" class="hdnfooter" id="Hidden1" />
    </div>
    </div>

    <div class="clsAbs">
        <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerHead1"/>
    </div>
    <div id="footerHead1" class="pTop40">
        <h4> <asp:Label ID="lblFHead1" runat="server" Text="Switzerland"/>  </h4>
    </div>

    <div class="clsAbs">
        <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerTxt1"/>
    </div>
    <div id="footerTxt1" class="pTop40">
        <p> <asp:Label ID="lblFtxt1" runat="server" Text="This summer explore Switze-rland from £40 >>"/>  </p>
    </div>

    </div>
    <div class="contory-block">
        <div class="cms" id="footer2">
            <div class="clsAbs">
                <input type="button" class="editFooter edit-btn" value="Edit" rel="footer2"/>
            </div>
            <div class="pTop5">
                <img id="imgFooter2" alt="" border="0" runat="server" />
                <input type="hidden" class="hdnfooter" id="Hidden2" />
            </div>
        </div>
        
    <div class="clsAbs">
        <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerHead2"/>
    </div>
    <div id="footerHead2" class="pTop40">
        <h1> <asp:Label ID="lblFHead2" runat="server" Text="Hotels & Hostels"/>  </h1>
    </div>
    
    <div class="clsAbs">
        <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerTxt2"/>
    </div>
    <div id="footerTxt2" class="pTop40">
        <p> <asp:Label ID="lblFtxt2" runat="server" Text="Choose from thousands of hostels & Hotels worldwide >>"/>  </p>
    </div>

    </div>
    <div class="contory-block">
        <div class="cms" id="footer3">
            <div class="clsAbs">
                <input type="button" class="editFooter edit-btn" value="Edit" rel="footer3"/>
            </div>
            <div class="pTop5">
                <img id="imgFooter3" alt="" border="0" runat="server" />
                <input type="hidden" class="hdnfooter" id="Hidden3" />
            </div>
        </div>
        
        <div class="clsAbs">
            <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerHead3"/>
        </div>
        <div id="footerHead3" class="pTop40">
            <h1> <asp:Label ID="lblFHead3" runat="server" Text="Amtrak"/>  </h1>
        </div>
    
        <div class="clsAbs">
            <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerTxt3"/>
        </div>
        <div id="footerTxt3" class="pTop40">
            <p> <asp:Label ID="lblFtxt3" runat="server" Text="Explore the USA by rail with Amtrak >>"/>  </p>
        </div>
    </div>
    <div class="contory-block">
        <div class="cms" id="footer4">
        <div class="clsAbs">
            <input type="button" class="editFooter edit-btn" value="Edit" rel="footer4"/>
        </div>
        <div class="pTop5">
            <img id="imgFooter4" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnfooter" id="Hidden4" />
        </div>
        </div>
    
        <div class="clsAbs">
            <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerHead4"/>
        </div>
        <div id="footerHead4" class="pTop40">
            <h1> <asp:Label ID="lblFHead4" runat="server" Text="Rail News"/>  </h1>
        </div>

        <div class="clsAbs">
            <input type="button" class="editFooterText edit-btn" value="Edit" rel="footerTxt4"/>
        </div>
        <div id="footerTxt4" class="pTop40">
            <p> <asp:Label ID="lblFtxt4" runat="server" Text="Keep up to date with all the latest rail related news >>"/>  </p>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="right-content">
    
<div id="rtPannel1" runat="server">
    <div class="sml-detail-block-outer">
    
    <div class="clsAbs">
        <input type="button" class="editRtPanelText edit-btn" value="Edit" rel="rightHead1"/>
    </div>
    <div id="rightHead1" class="pTop40">
        <div class="hd-title"> <asp:Label ID="lblRtPanelHead" runat="server" Text="Journey request form"/></div>
    </div>

    <div class="sml-detail-block-inner">
        <div id="rtPanelImgTop">
            <div class="clsAbs">
            <input type="button" class="editRtPanel edit-btn" value="Edit" rel="rtPanelImgTop"/>
        </div>
            <div class="pTop5">
            <img id="imgRtPanelTop" class="imground scale-with-grid" src="" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnRight" id="Hidden6" />
        </div>
        </div>

        <div class="clsAbs">
            <input type="button" class="editRtPanelText edit-btn" value="Edit" rel="rightTxt1"/>
        </div>
        <div id="rightTxt1" class="pTop40">
            <p> <asp:Label ID="lblRtPanelTxt" runat="server" Text="Fill out our online form and one of our rail experts will reply with a custom itinerary & quote »"/>  </p>
        </div>
    </div> 
</div>
</div>

<img src="images/page/block-shadow.jpg" class="scale-with-grid" alt="" border="0" />

<%--<div id="rtPannel2" runat="server">
    <div class="sml-detail-block-outer">
    <div id="rightHead2">
        <div class="hd-title"> <asp:Label ID="lblRtPanelHead2" runat="server" Text="General Information"/></div>
    </div>
    <div class="sml-detail-block-inner">
       <div id="rtPanelImgBottom">
            <div class="pTop5">
                <img id="imgRtPanelBottom" class="imground scale-with-grid" alt="" border="0" runat="server" />
                <input type="hidden" class="hdnRight" id="Hidden8" />
            </div>
        </div>
        <div id="rightTxt2" class="pTop40">
            <p> <asp:Label ID="lblRtPanelTxt2" runat="server" Text="Information on passes, tickets, this site, our organisation, booking, schedules »"/>  </p>
        </div>
    </div> 
    </div>
</div>--%>

<div style="padding-bottom: 10px">
<img src="images/page/generalinformation_watermark.png" />
<img src="images/page/block-shadow.jpg" class="scale-with-grid" alt="" border="0" />
</div>

<div style="padding-bottom: 10px">
    <img src="images/page/newsletter-watermark.png" />
    <img src="images/page/block-shadow.jpg" class="scale-with-grid" alt="" border="0" />
</div>

</div>
</section>
</section>
    <footer>
    <div class="wrapper">
<div class="foot-col1"> 
<h3> Destinations </h3>
<ul> 
<li> <a href="#"> Paris  </a></li> 
<li> <a href="#"> Disneyland Paris  </a></li> 
<li> <a href="#"> Brussels  </a></li> 
<li> <a href="#"> Lille  </a></li> 
<li> <a href="#"> Bruges </a></li> </ul>
</div>
<div class="foot-col1"> 
<h3> Offers </h3>
<ul> 
<li> <a href="#"> Thalys Unmissables </a></li> 
<li> <a href="#"> Favourites </a></li> 
<li> <a href="#"> Flanders Battlefields   </a></li> 
</ul>
</div>
<div class="foot-col1"> 
<h3> Rail Companies </h3>
<ul> 
<li> <a href="#"> Eurostar  </a></li> 
 <li> <a href="#">  Thalys  </a></li> 
 <li> <a href="#">  ICE  </a></li> 
 <li> <a href="#">  InterCity-EuroCity </a></li>
</ul>
</div>
<div class="foot-col1"> 
<h3> Travel the world  </h3>
<ul> 
<li> <a href="#"> Canada Rail Pass  </a></li>
<li> <a href="#">  USA Rail Pass  </a></li>
<li> <a href="#">  Australia Rail Pass  </a></li>
 <li> <a href="#">  New Zealand Pass  </a></li>
 <li> <a href="#">  InterRail Global   </a></li>
</ul>
</div>
<div class="foot-col1"> 
<h3> Practical   </h3>
<ul> 
<li> <a href="#"> Your tickets  </a></li>
 <li> <a href="#"> Contact us  </a></li>
 <li> <a href="#"> Station information  </a></li>
 <li> <a href="#"> Refunds for delays  </a></li>
 <li> <a href="#"> FAQ   </a></li>
</ul>
</div>
<div class="foot-col2"> 
<h3> Legal information    </h3>
<ul> 
<li> <a href="#"> Passenger rights </a></li>
 <li> <a href="#"> Privacy and cookies </a></li>
 <li> <a href="#"> Conditions of use </a></li>
 <li> <a href="#"> General terms and conditions </a></li> 
</ul>
</div>
</div>
    <section class="bottom-section">
    <div class="wrapper">
<div class="f-links">
<p> 
  <a href="aboutus.html">  About us </a>  |   <a href="contactus.html"> Contact us </a>  |   <a href="privacy-policy.html"> Privacy policy  </a> |   <a href="condition-of-use.html"> Conditions of use  </a> |   <a href="booking-condition.html"> Booking conditions </a>  |   <a href="Delivery and Service.html"> Delivery & service  </a>|   <a href="#">  Unsubscribe </a>
 </p>
 <span> * Calls from UK landlines cost £0.10p per minute. Call charges from mobiles and international numbers may vary. <br />
 © Copyright 2013 International Rail Ltd.
 </span>
</div>
<div class="social-links">
<p>Follow us online </p>
<a href="#"><img src="images/page/fb.png" alt="" class="scale-with-grid" border="0" /></a> 
<a href="#"><img src="images/page/tw.png" alt="" class="scale-with-grid" border="0" /></a> 
<a href="#"><img src="images/page/blog.png" alt="" class="scale-with-grid" border="0" /></a> 
<a href="#"><img src="images/page/linkdin.png" alt="" class="scale-with-grid" border="0" /></a> 
</div>
</div>
    </section>
</footer>
    <div class="clear">
    </div>
    <div id="ContentFooter" class="PopUpSampleIMG" style="display: none; width: 460px;
        height: auto">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll;height: 280px">
            <asp:DataList ID="dtFooter" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="208px"
                        Height="119px" CssClass="bGray" />
                    <br />
                    <input type="radio" name="img" checked="checked" value='<%#Eval("ImagePath")%>' />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button8" value="Cancel" class="button btnClose" rel="ContentFooter" />
            <input type="button" id="btnSaveFooter" value="Save" class="button btnSave" rel="ContentFooter" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentFooterTxt" style="display: none; left: 260px;
        top: 700px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtFooter" name="txtFooter" cols="10" rows="5" class="rdFooter"></textarea>
                        <input type="hidden" class="hdnfooterTxt" id="Hidden5" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button10" value="Cancel" class="button btnClose" rel="ContentFooterTxt" />
                    <input type="button" id="Button7" value="Save" class="button btnsaveFootertxt" rel="ContentFooterTxt" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 530px;
        height: auto; left: 277px; top: 150px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 277px; top: ">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 277px; top: 180px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentRight" class="PopUpSampleIMG" style="display: none; width: 460px;
        height: auto; top: 500px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll">
            <asp:DataList ID="dtRtPanel" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtRtPanel_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="208px"
                        Height="119px" CssClass="bGray" />
                    <br />
                    <input id="rdrtID" class="rdrtID" runat="server" type="radio" name="img" value='<%#Eval("ImagePath")%>' />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button2" value="Cancel" class="button btnClose" rel="ContentRight" />
            <input type="button" id="btnSaveRtPanel" value="Save" class="button btnSaveRtPanel"
                rel="ContentRight" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentRightTxt" style="display: none; left: 260px;
        top: 500px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtRtPanel" name="txtRtPanel" cols="10" rows="5" class="rdRight"></textarea>
                        <input type="hidden" class="hdnrtPanelTxt" id="Hidden7" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button3" value="Cancel" class="button btnClose" rel="ContentRightTxt" />
                    <input type="button" id="btnSaveRttxt" value="Save" class="button btnSaveRttxt" rel="ContentRightTxt" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
