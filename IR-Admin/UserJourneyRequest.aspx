﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UserJourneyRequest.aspx.cs" Inherits="IR_Admin.UserJourneyRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .BindGridScrolle
        {
            float: left;
            width: 100%;
            height: 100%;
            position: relative;
            overflow-x: scroll;
            border-radius: 7px;
        }
    </style>
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            if (sdate != "" && ldate != "")
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        P2P Search Feedback</h2>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div class="pass-coloum-two" style="width: 315px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px;
                                0 0;" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                        </div>
                        <div class="pass-coloum-two" style="width: 300px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px;
                                0 0;" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                        </div>
                        <div style="float: right">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                ValidationGroup="rv" OnClick="btnSubmit_Click" />
                            &nbsp;<asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel" data-export="export"
                                Width="159px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click" />
                        </div>
                    </div>
                    <div class="BindGridScrolle">
                        <asp:GridView ID="grdJourneyReq" runat="server" AutoGenerateColumns="False" PageSize="10"
                            CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="170%"
                            AllowPaging="True" OnPageIndexChanging="grdJourneyReq_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%# Eval("UserName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <%# Eval("Email")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone">
                                    <ItemTemplate>
                                        <%# !String.IsNullOrEmpty(Convert.ToString(Eval("Phone"))) ? Eval("Phone").ToString() : String.Empty%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="From">
                                    <ItemTemplate>
                                        <%#Eval("From")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="To">
                                    <ItemTemplate>
                                        <%#Eval("To")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Depart Date">
                                    <ItemTemplate>
                                        <%#Eval("DepartDate","{0:dd/MMM/yyyy}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time">
                                    <ItemTemplate>
                                        <%#Eval("DepartTime")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Return Date">
                                    <ItemTemplate>
                                        <%#Eval("ReturnDate", "{0:dd/MMM/yyyy}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Return Time">
                                    <ItemTemplate>
                                        <%#Eval("ReturnTime")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Adult">
                                    <ItemTemplate>
                                        <%#Eval("Adult")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Children">
                                    <ItemTemplate>
                                        <%#Eval("Children")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ClassPreference">
                                    <ItemTemplate>
                                        <%#Eval("ClassPreference")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
