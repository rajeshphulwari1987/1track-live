﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI;

namespace IR_Admin.PrintQueueInterrail
{
    public partial class StockDetails : Page
    {
        readonly public ManagePrintQueue oQueue = new ManagePrintQueue();
        readonly public Masters oMaster = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
        }
        #endregion
          
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!IsPostBack)
            {
                var id = Request.QueryString["id"] != null ? Guid.Parse(Request.QueryString["id"]) : new Guid();
                BindStocks(id);
            }
        }

        void BindStocks(Guid id)
        {
            try
            {
                var list = oQueue.GetStatusOfStockNumberInterrailByParentBranchID(id);
                var data = oQueue.GetInterrailStockBranchInfo(id);
                if (data != null)
                {
                    ltrCreatedBy.Text = data.CreatedBy;
                    ltrCreatedOn.Text = data.CreatedOn;
                    ltrIP.Text = data.IPAddress;
                    ltrOfficeTree.Text = "Master Admin Account " + data.OfficeTreeName;
                    ltrStockNoFrom.Text = data.StockNumberFrom.ToString();
                    ltrStockNoTo.Text = data.StockNumberTo.ToString();
                    ltrAllocatedFrom.Text = data.OfficeName;
                }
                if (list != null && list.Count > 0)
                {
                    ltrAllocatedFrom.Text = list.FirstOrDefault().Allocated_From;
                    ltrStockNoFrom.Text = list.Min(ty => ty.StockFrom).ToString();
                    ltrStockNoTo.Text = list.Max(ty => ty.StockTo).ToString();
                    grdAllStocks.Visible = false;
                    grdStock.DataSource = list;
                    grdStock.DataBind();
                }
                else
                {
                    grdAllStocks.DataSource = oQueue.GetAllStockNumberInterrailByBranch(id);
                    grdAllStocks.DataBind();
                }
            }
            catch (Exception ex)
            { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void lnkNew_OnClick(object sender, EventArgs e)
        {
            LinkButton btnSubmit = (LinkButton)sender;
            GridViewRow Row = (GridViewRow)btnSubmit.NamingContainer;
            Literal ltrStockFrom = (Literal)Row.FindControl("ltrStockFrom");
            Literal ltrStockTo = (Literal)Row.FindControl("ltrStockTo");
            txtStockFrom.Text = ltrStockFrom.Text;
            txtStockTo.Text = ltrStockTo.Text;
            AjaxControlToolkit.ModalPopupExtender mm = (AjaxControlToolkit.ModalPopupExtender)Row.FindControl("mdpupStock");
            mm.TargetControlID = btnSubmit.ClientID;
            mm.Show();
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSubmit = (Button)sender;
                TextBox txtFrom = txtStockFrom;
                TextBox txtTo = txtStockTo;
                if (!string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtFrom.Text))
                {
                    int From = Convert.ToInt32(txtFrom.Text);
                    int To = Convert.ToInt32(txtTo.Text);
                    Guid Void = new Guid("de05d0a6-785c-4da0-b650-cb3cafcb5330");
                    _db.tblStockAllocationInterrails.Where(t => t.StockNo >= From && t.StockNo <= To).ToList().ForEach(t => t.StatusID = Void);
                    _db.SaveChanges();
                    string Id = Request.QueryString["id"];
                    BindStocks(Guid.Parse(Id));
                    ClientScript.RegisterStartupScript(this.GetType(), "script234", "CallParent('" + Id + "');", true);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmitUnused_OnClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSubmit = (Button)sender;
                TextBox txtFrom = txtStockFromUnused;
                TextBox txtTo = txtStockToUnused;
                if (!string.IsNullOrEmpty(txtFrom.Text) && !string.IsNullOrEmpty(txtTo.Text))
                {
                    int From = Convert.ToInt32(txtFrom.Text);
                    int To = Convert.ToInt32(txtTo.Text);
                    for (long i = From; i <= To; i++)
                    {
                        Guid PenDel = Guid.Parse("82DE1DBB-05D2-4AC2-BE59-569DDC9BE292");
                        if (_db.tblStockAllocationInterrails.Where(t => t.StockNo == i && t.StatusID == PenDel).Any())
                            oQueue.UpdateInterrailStockNoReuse(i);
                    }
                    string Id = Request.QueryString["id"];
                    BindStocks(Guid.Parse(Id));
                    ClientScript.RegisterStartupScript(this.GetType(), "script234", "CallParent('" + Id + "');", true);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void lnkDelivered_Click(object sender, EventArgs e)
        {
            hdnstockrange.Value = ((LinkButton)sender).CommandArgument.ToString();
            try
            {
                Guid id = Request.QueryString["id"] != null ? Guid.Parse(Request.QueryString["id"]) : new Guid();
                string[] str = hdnstockrange.Value.Split(',');
                Int32 stkFrom = Convert.ToInt32(str[0]);
                Int32 stkTo = Convert.ToInt32(str[1]);
                bool res = oQueue.SetStockInterrailToDelivered(Guid.Empty, id, stkFrom, stkTo);
                if (res)
                    ShowMessage(1, "Stock delivered successfully.");
                else
                    ShowMessage(2, "Stock not delivered.");
                BindStocks(id);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdStock_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnk = (LinkButton)e.Row.FindControl("lnkNew");
                LinkButton lnkUnused = (LinkButton)e.Row.FindControl("lnkUnused");
                Literal ltrFrom = (Literal)e.Row.FindControl("ltrFrom");
                Literal ltrTo = (Literal)e.Row.FindControl("ltrTo");
                lnk.Attributes.Add("onclick", "FillValue('" + ltrFrom.Text + "','" + ltrTo.Text + "')");
                lnkUnused.Attributes.Add("onclick", "FillValueUnused('" + ltrFrom.Text + "','" + ltrTo.Text + "')");
            }
        }
    }
}