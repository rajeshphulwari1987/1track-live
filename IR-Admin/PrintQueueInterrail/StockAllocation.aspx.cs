﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data;
using System.Text;
namespace IR_Admin.PrintQueueInterrail
{
    public partial class StockAllocation : System.Web.UI.Page
    {
        readonly ManagePrintQueue _oQueue = new ManagePrintQueue();
        public string tab = string.Empty;
        public static int level = 0;
        Guid _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!Page.IsPostBack)
            {
                tab = "1";
                if (!String.IsNullOrEmpty(hdnId.Value))
                    tab = "2";
                FillDropDown();
                BindList();
            }            
            ShowMessage(0, null);
            lblMessage.Text = string.Empty;
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "addClasd", "addClas();", true);
        }

        void FillDropDown()
        {
            try
            {
                FillOffice(new Guid());
                ddlStatus.DataSource = _oQueue.GetStockAllocationStatus();
                ddlStatus.DataValueField = "ID";
                ddlStatus.DataTextField = "Name";
                ddlStatus.DataBind();
                ddlStatus.SelectedValue = "0e758c24-d919-43d6-a80a-e60d9da8c53a";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void FillOffice(Guid officeId)
        {
            ddlParentStockQueue.Items.Clear();
            var list = _oQueue.GetParentNameOfficeList(officeId);
            if (list != null && list.Count > 0)
            {
                ddlParentStockQueue.DataSource = list;
                ddlParentStockQueue.DataValueField = "ID";
                ddlParentStockQueue.DataTextField = "Name";
                ddlParentStockQueue.DataBind();
            }
            ddlParentStockQueue.Items.Insert(0, new ListItem("--Select Stock Queue--", "0"));
        }

        void BindList()
        {
            db_1TrackEntities db = new db_1TrackEntities();
            var query = (from m in db.tblStockInterrails
                         select new
                         {
                             TreeID = m.TreeID,
                             ParentID = m.TreeParentBranchID,
                             Text = m.tblBranch.OfficeName,
                             StockFrom = m.StockNoFrom,
                             StockTo = m.StockNoTo,
                             Status = m.tblStockAllocationStatu.Name,
                             BranchID = m.BranchID,
                             ParentBranchID = m.ParentBranchID,
                             ID = m.ID
                         }).OrderBy(x => x.Text).ThenBy(x => x.StockFrom).ToList();

            DataSet dataSet = new DataSet();
            dataSet.Tables.Add("Table");
            dataSet.Tables[0].Columns.Add("TreeID", typeof(int));
            dataSet.Tables[0].Columns.Add("ParentID", typeof(int));
            dataSet.Tables[0].Columns.Add("Text", typeof(string)); 
            dataSet.Tables[0].Columns.Add("ID", typeof(int));
            int count = 1;
            foreach (var item in query)
            {
                DataRow row = dataSet.Tables[0].NewRow();
                row["TreeID"] = item.TreeID;
                if (item.ParentID != null)
                {
                    row["ParentID"] = item.ParentID;
                    row["ID"] = count;
                    count++;
                }
                else
                {
                    row["ID"] = 0;
                    count = 1;
                }
                StringBuilder nodeText = new StringBuilder();
                nodeText = nodeText.Append("<a class='aStock' href='StockDetails.aspx?id=" + item.ParentBranchID.ToString() + "'><div class='stockrange'>" + item.Text + " Stock Range :" + item.StockFrom + " - " + item.StockTo + "</div></a>");
                nodeText.Append(@"<div class='btn-container'><span><INPUT id=""btndelete").Append(item.ID.ToString()).Append(@""" type=""image"" src=""../images/icon-delete.png""  title=""Delete Stock"" onclick=""return deleteStockNo('").Append(item.ID.ToString() + "," + item.StockFrom + "," + item.StockTo).Append(@"')""></span>");
                nodeText.Append(@"&nbsp<span><INPUT id=""btnedit").Append(item.ID.ToString()).Append(@""" type=""image""  src=""../images/icon-treeedit.png""  title=""Edit Stock"" onclick=""editStockNo('").Append(item.ID.ToString()).Append(@"')""></span>");
                nodeText.Append(@"<span><INPUT id=""btnadd").Append(item.BranchID.ToString()).Append(@""" type=""image"" src=""../images/icon-plus.png""  title=""Add stock to child office"" onclick=""addStockNo('").Append(item.ID.ToString() + "ñ" + item.BranchID.ToString()).Append(@"')""></span></div>");
                row["Text"] = nodeText.ToString();
                dataSet.Tables[0].Rows.Add(row);
            }

            var list = new HierarchicalDataSet(dataSet, "TreeID", "ParentID");
            if(dataSet.Tables[0].Rows.Count>0)
            level = dataSet.Tables[0].AsEnumerable().Max(al => al.Field<int>("ID")) + 2;

            trStock.DataSource = list;
            trStock.DataBind();
            trStock.CollapseAll();
        }

        void Check_Changed(Object sender, EventArgs e)
        {
            // Alternate the state of the root node. The root node
            // is the first element (index 0) of the Nodes collection.
            trStock.Nodes[0].ToggleExpandState();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditStockAllocation();
            hdnId.Value = string.Empty;
            BindList();
            tab = "1";
        }

        void FillDropDownForNextChild()
        {
            FillOffice(Guid.Parse(ddlParentStockQueue.SelectedValue));
            if (ddlParentStockQueue.Items.Count > 1)
            {
                mdpupStock.Show();
            }
            else
            {
                mdpupStock.Hide();
                txtStockFrom.Text = string.Empty;
                txtStockTo.Text = string.Empty;
            }
        }

        protected void AddEditStockAllocation()
        {
            try
            {
                Guid id = _oQueue.AddEditStockInterrail(new tblStockInterrail
                {
                    ID = !string.IsNullOrEmpty(hdnId.Value) ? Guid.Parse(hdnId.Value) : new Guid(),
                    BranchID = Guid.Parse(ddlParentStockQueue.SelectedValue),
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    IPAddress = Request.ServerVariables["REMOTE_ADDR"].ToString(),
                    StockNoFrom = Convert.ToInt32(txtStockFrom.Text.Trim()),
                    StockNoTo = Convert.ToInt32(txtStockTo.Text.Trim()),
                    StatusID = Guid.Parse(ddlStatus.SelectedValue)
                });
                string msg = string.IsNullOrEmpty(hdnId.Value) != true ? "Stock updated successfully." : "Stock allocated to this office.";
                lblMessage.Text = msg;
                lblMessage.ForeColor = System.Drawing.Color.Green;
                ShowMessage(1, msg);
                FillDropDownForNextChild();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("already"))
                {
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    lblMessage.Text = ex.Message;
                    mdpupStock.Show();
                }
                else
                {
                    ShowMessage(2, ex.Message);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("StockAllocation.aspx");
        }
        
        protected void btneditStock_Click(object sender, EventArgs e)
        {
            try
            {
                Guid id = Guid.Parse(hdnId.Value.ToString());
                var rec = _oQueue.GetStockInterrailForEdit(id);
                if (rec != null)
                {
                    //hdnIdaddstockParent.Value = id.ToString();
                    hdnId.Value = rec.ID.ToString();
                    txtStockFrom.Text = rec.StockNoFrom.ToString();
                    txtStockTo.Text = rec.StockNoTo.ToString();
                    FillOffice(rec.ParentBranchID);
                    ddlParentStockQueue.SelectedValue = rec.BranchID.ToString();
                    ddlStatus.SelectedValue = rec.StatusID.ToString();
                }
                tab = "1";
                mdpupStock.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnDeleteStock_Click(object sender, EventArgs e)
        {
            string[] str = hdnId.Value.Split(',');
            Guid id = Guid.Parse(str[0]);
            string rangeFrom = str[1];
            string rangeTo = str[2];
            bool res = _oQueue.DeleteStockNumberInterrail(id, level, rangeFrom, rangeTo);
            BindList();
            if (res)
                ShowMessage(1, "Record delete successfully.");
            else
                ShowMessage(2, "Stock number in used so can't delete.");
            tab = "1";
            ViewState["tab"] = "1";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
        }

        protected void btnaddStock_Click(object sender, EventArgs e)
        {
            try
            {
                string[] data = hdnBranchId.Value.Split('ñ');

                Guid id = Guid.Parse(data[0]);
                Guid Branchid = Guid.Parse(data[1]);

                var rec = _oQueue.GetStockInterrailForEdit(id);
                if (rec != null)
                {
                    //hdnIdaddstockParent.Value = data[0];
                    txtStockFrom.Text = rec.StockNoFrom.ToString();
                    txtStockTo.Text = rec.StockNoTo.ToString();                    
                    ddlStatus.SelectedValue = rec.StatusID.ToString();
                }
                FillOffice(Branchid);
                hdnId.Value = string.Empty; 
                tab = "1";
                mdpupStock.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}