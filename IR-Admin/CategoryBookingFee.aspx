﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CategoryBookingFee.aspx.cs" Inherits="IR_Admin.CategoryBookingFee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="date-time-Picker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="date-time-Picker/jquery.js" type="text/javascript"></script>
    <script src="date-time-Picker/jquery.datetimepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            bindDatePicker();
        });
        function bindDatePicker() {
            $('[id*=txtBookingFromDate]').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                step: 5,
                defaultDate: new Date()
            });
            $('[id*=txtBookingToDate]').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                step: 5,
                defaultDate: new Date()
            });
        }
    </script>
    <style type="text/css">
        .align {
            text-align: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>Category Booking Fee</h2>
            <p>(category booking fee is in addition to product booking fee and can be set by site)</p>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" />
                </div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <!-- tab "panes" -->
            <div class="full mr-tp1">
                <div class="panes">
                    <div id="divlist" runat="server">
                        <div class="crushGvDiv" style="font-size: 13px;">
                            <table class="tblMainSection" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: right;" class="valdreq">Fields marked with * are mandatory
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><b>Site </b></legend>
                                            <div class="cat-inner-alt" style="text-align: center; width: 100%; border-bottom: 2px dashed #FBDEE6;">
                                                <asp:DropDownList runat="server" ID="ddlSites" AutoPostBack="True" Width="500px"
                                                    OnSelectedIndexChanged="ddlSites_SelectedIndexChanged" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                    ValidationGroup="submit" InitialValue="0" ControlToValidate="ddlSites" CssClass="valdreq"
                                                    SetFocusOnError="True" />
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><b>Category </b></legend>
                                            <asp:GridView ID="grvCategories" runat="server" AutoGenerateColumns="False" PageSize="10"
                                                CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                AllowPaging="True" OnPageIndexChanging="grvCategories_PageIndexChanging">
                                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                                <PagerStyle CssClass="paging"></PagerStyle>
                                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                                <EmptyDataTemplate>
                                                    Record not found.
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <%#Eval("CategoryName")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="52%"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="From">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtBookingFromDate" runat="server" Text='<%# Eval("BookingFromDate", "{0:dd/MM/yyyy HH:mm}")  %>' Width="100px"
                                                                Style="float: left;" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="To">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtBookingToDate" runat="server" Text='<%#Eval("BookingToDate","{0:dd/MM/yyyy HH:mm}")%>' Width="100px"
                                                                Style="float: left;" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Amount Range(From - To)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtAmountFrom" runat="server" Text='<%#Eval("AmountFrom")%>' Width="50px" />
                                                            <asp:FilteredTextBoxExtender ID="ftbAmountFrom" runat="server" TargetControlID="txtAmountFrom" ValidChars="0123456789." />
                                                            <asp:RequiredFieldValidator ID="reqAmountFrom" runat="server" ControlToValidate="txtAmountFrom" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                            <span>-</span>
                                                            <asp:TextBox ID="txtAmountTo" runat="server" Text='<%#Eval("AmountTo")%>' Width="50px" />
                                                            <asp:FilteredTextBoxExtender ID="ftbAmountTo" runat="server" TargetControlID="txtAmountTo" ValidChars="0123456789." />
                                                            <asp:RequiredFieldValidator ID="reqAmountTo" runat="server" ControlToValidate="txtAmountTo" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle Width="20%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Booking fee">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPrice" runat="server" Text='<%#Eval("Price")%>' Width="50px"
                                                                Style="float: left;" />
                                                            <asp:FilteredTextBoxExtender ID="ftbPrice" runat="server" TargetControlID="txtPrice" ValidChars="-0123456789." />
                                                            <asp:RequiredFieldValidator ID="reqPrice" runat="server" ControlToValidate="txtPrice" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnFlag" runat="server" Value="1" />
                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("CategoryId")%>' />
                                                            <asp:Button ID="btnUpdate" ValidationGroup="vg" runat="server" Text="Update" CommandName="UpdateDiscount" OnClick="btnUpdate_Click" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <div>
                                            <fieldset class="grid-sec2">
                                                <legend><b>Product </b></legend>
                                                <div class="searchDiv" style="text-align: center">
                                                    <asp:DropDownList ID="ddlCategory" runat="server" Width="510px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                                                </div>
                                                <asp:GridView ID="grvProduct" runat="server" AutoGenerateColumns="False" PageSize="10"
                                                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                    OnPageIndexChanging="grvProduct_PageIndexChanging" AllowPaging="True">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <PagerStyle CssClass="paging"></PagerStyle>
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <%#Eval("ProductName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="30%"></ItemStyle>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Category Name">
                                                            <ItemTemplate>
                                                                <%#Eval("CategoryName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20%"></ItemStyle>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="From">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtBookingFromDate" runat="server" Text='<%# Eval("BookingFromDate", "{0:dd/MM/yyyy HH:mm}")  %>' Width="100px"
                                                                    Style="float: left;" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="To">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtBookingToDate" runat="server" Text='<%#Eval("BookingToDate","{0:dd/MM/yyyy HH:mm}")%>' Width="100px"
                                                                    Style="float: left;" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Amount Range(From - To)">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtAmountFrom" runat="server" Text='<%#Eval("AmountFrom")%>' Width="50px" />
                                                                <asp:FilteredTextBoxExtender ID="ftbAmountFrom" runat="server" TargetControlID="txtAmountFrom" ValidChars="0123456789." />
                                                                <asp:RequiredFieldValidator ID="reqAmountFrom" runat="server" ControlToValidate="txtAmountFrom" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                                <span>-</span>
                                                                <asp:TextBox ID="txtAmountTo" runat="server" Text='<%#Eval("AmountTo")%>' Width="50px" />
                                                                <asp:FilteredTextBoxExtender ID="ftbAmountTo" runat="server" TargetControlID="txtAmountTo" ValidChars="0123456789." />
                                                                <asp:RequiredFieldValidator ID="reqAmountTo" runat="server" ControlToValidate="txtAmountTo" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="20%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Booking Fee">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtPrice" runat="server" Text='<%#Eval("Price")%>' Width="50px" Style="float: left;" />
                                                                <asp:FilteredTextBoxExtender ID="ftbPrice" runat="server" TargetControlID="txtPrice" ValidChars="-0123456789." />
                                                                <asp:RequiredFieldValidator ID="reqPrice" runat="server" ControlToValidate="txtPrice" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnFlag" runat="server" Value="2" />
                                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ProductId")%>' />
                                                                <asp:Button ID="btnUpdate" ValidationGroup="vg" runat="server" Text="Update" CommandName="UpdateDiscount" OnClick="btnUpdate_Click" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </fieldset>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

