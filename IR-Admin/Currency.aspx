﻿<%@ Page Title="Currency" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Currency.aspx.cs" Inherits="IR_Admin.currencyPage" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script>
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });
        $(document).ready(function () {
            if ('<%=tab.ToString()%>' == '1') {
                document.getElementById('<%= hdnCurrencyID.ClientID %>').value = '';
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }

            $(".list a").click(function (e) {
                $('input:text').val('');
                document.getElementById('<%= hdnCurrencyID.ClientID %>').value = '';
                if ($(this).attr("id") == 'aList') {
                    document.getElementById('MainContent_divlist').style.display = 'Block';
                    document.getElementById('MainContent_divNew').style.display = 'none';
                }
                else {
                    $("#<%=btnSubmit.ClientID%>").val('Submit');
                    document.getElementById('MainContent_divlist').style.display = 'none';
                    document.getElementById('MainContent_divNew').style.display = 'Block';
                }
            });

        });
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnCurrencyID" runat="server" />
    <h2>
        Currency
    </h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="#" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" NavigateUrl="#" CssClass=" " runat="server">New/Edit</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdCurrency" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdCurrency_OnRowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Currency Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Symbol">
                                    <ItemTemplate>
                                        <%#Eval("Symbol")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Code">
                                    <ItemTemplate>
                                        <%#Eval("ShortCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HTML Code">
                                    <ItemTemplate>
                                        <%#Eval("HTMLCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: Block;">
                    <div class="divMain">
                        <div class="divleft">
                            Currency Name:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="txtCNameRequiredFieldValidator" runat="server" ControlToValidate="txtCName"
                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="CurForm"></asp:RequiredFieldValidator>
                        </div>
                        <div class="divleft">
                            Currency Symbol:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCCode" runat="server" MaxLength="50" />
                        </div>
                        <div class="divleft">
                            Currency Short Code:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtShortcode" runat="server" MaxLength="6" />
                        </div>
                        <div class="divleft">
                            Currency HTML Code:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtHtmlcode" runat="server" MaxLength="50" />
                        </div>
                        <div class="divleft">
                            Is Active?
                        </div>
                        <div class="divright">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="divleftbtn"> .
                        </div>
                        <div class="divrightbtn">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="CurForm" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
