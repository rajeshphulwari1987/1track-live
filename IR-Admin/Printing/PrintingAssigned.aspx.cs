using System;
using System.Globalization;
using System.Web.UI;
using Business;
using System.Xml;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

public partial class Printing_PrintingAssigned : Page
{
    private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static List<GetInJobedPrintQueueItems> datalist = new List<GetInJobedPrintQueueItems>();
    private Guid _siteId;
    public string SiteUrl;
    public string script = "<script></script>";
    public string HideSaver = string.Empty;
    public bool IsSaver = true;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            SiteUrl = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["AgentUsername"] == null)
                Response.Redirect(SiteUrl);
            if (!Page.IsPostBack)
            {
                BindQueue();
                QubitOperationLoad();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindQueue()
    {
        List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
        List<Guid> listIds = listCatIds != null ? listCatIds.Select(x => x.Id).Distinct().ToList() : null;
        if (Request.QueryString["qId"] != null && listIds != null)
        {
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            dvPrd.Visible = true;
            List<GetInJobedPrintQueueItems> list = new List<GetInJobedPrintQueueItems>();
            string strIds = "";
            foreach (var item in listIds)
                strIds = string.IsNullOrEmpty(strIds) ? item.ToString() : strIds + "," + item.ToString();

            
            list = _oPrint.GetPrintQueueInJobList(queueId, strIds, _siteId, AgentuserInfo.UserID);
                if (list.Count > 0 && list != null)
                {
                    lblRangeFrm.Text = list.Min(ty => ty.StockNo).ToString();
                    lblRangeTo.Text = list.Max(ty => ty.StockNo).ToString();
                    datalist = list;
                    grdPrint.DataSource = list;
                    grdPrint.DataBind();
                }
             else
                {
                    ShowMessage(2, "Stock not available for this agent.");
                    btnPrint.Visible = false;
                }
             
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("PrintingAssigned");
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
            List<Guid> listIds = listCatIds.Select(x => x.Id).Distinct().ToList();
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            foreach (var item in listIds)
                _oPrint.UpdatePrintQStausFromAssigenToInJob(queueId, item, _siteId);
            PrintClick();
            btnConfirm_Click();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void PrintClick()//Guid catId
    {
        Guid PrintQueueIdNew = Guid.Empty;
        bool caseCount = true;
        Guid PQID = Guid.Empty;
        Guid queueId = new Guid();
        if (Request.QueryString["qId"] != null)
            queueId = Guid.Parse(Request.QueryString["qId"]);
        string oldorderId = string.Empty;
        string PassSaleIdIn = string.Empty;
        Guid Passid = Guid.Empty;
        Guid PassPrintid = Guid.Empty;
        Guid PassPrintidSecondSaver = Guid.Empty;
        bool SecondSaver = true;
        Int32 StockNumber = 0;
        int rowcount = 0;
        int NO = grdPrint.Rows.Count;
        foreach (GridViewRow row in grdPrint.Rows)
        {
            rowcount++;
            CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            HiddenField hdnCategoryID = row.FindControl("hdnCategoryID") as HiddenField;

            HiddenField hdnSaver = row.FindControl("hdnSaver") as HiddenField;
            HiddenField hdnStatus = row.FindControl("hdnStatus") as HiddenField;
            Guid catId = Guid.Parse(hdnCategoryID.Value);
            Guid id = Guid.Parse(hdnID.Value);
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            
			

			if (Convert.ToUInt64(hdnOrderNo.Value) > 0)
                PQID = id;
            if (chkPrint.Checked && !string.IsNullOrWhiteSpace(hdnOrderNo.Value))
            {
                if (oldorderId != hdnOrderNo.Value && oldorderId != "" || oldorderId != "" && rowcount == grdPrint.Rows.Count)
                {
                    if (hdnSaver.Value.ToLower().Contains("yes"))
                    {
                        Guid passSaleID = Guid.Empty;
                        if (oldorderId != hdnOrderNo.Value)
                        {
                            oldorderId = hdnOrderNo.Value;
                            PassPrintid = id;
                            PassSaleIdIn = _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                            Passid = Guid.Parse(PassSaleIdIn);
                            StockNumber = stNo;
                        }
                        else
                        {
                            
                            if (SecondSaver)
                            {
                                PassPrintidSecondSaver = id;
                                SecondSaver = false;
                            }

                            PassSaleIdIn += "," + _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                            StockNumber = StockNumber > stNo ? stNo : StockNumber;
                        }
                    }
                    string tktXML = "";
                    var list1 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                    foreach (var item in list1)
                    {
                        tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                    }
                    if (!string.IsNullOrEmpty(tktXML))
                    {
                        XmlDataDocument xDoc = new XmlDataDocument();
                        xDoc.LoadXml(tktXML);
                        XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                        XmlNode xnr = result.SelectSingleNode("/Coupons");
                        if (xnr != null)
                        {
                            foreach (XmlNode xn in xnr)
                            {
                                Guid userid = AdminuserInfo.UserID;
                                if (AgentuserInfo.UserID != new Guid())
                                    userid = AgentuserInfo.UserID;
                                _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid);
                                StockNumber++;
                                PassPrintid = PassPrintidSecondSaver;
                            }
                        }
                    }
                    oldorderId = string.Empty;
                    Passid = Guid.Empty;
                    PassSaleIdIn = string.Empty;
                    StockNumber = 0;
                }
                if (hdnSaver.Value.ToLower().Contains("yes"))
                {
                    Guid passSaleID = Guid.Empty;
                    if (oldorderId != hdnOrderNo.Value)
                    {
                        oldorderId = hdnOrderNo.Value;
                        PassPrintid = id;
                        PassSaleIdIn = _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                        Passid = Guid.Parse(PassSaleIdIn);
                        StockNumber = stNo;
                    }
                    else
                    {
                        if(SecondSaver){
                            PassPrintidSecondSaver=id;
                            SecondSaver = false;
                        }
                        PassSaleIdIn += "," + _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                        StockNumber = StockNumber > stNo ? stNo : StockNumber;
                    }
                }
                else
                {
                    string tktXML = "";
                    Guid passSaleID = (Guid)_oPrint.GetPassSaleIDByPrintQueueID(id);
                    var list = _oPrint.GetRetrieveEurailProductXML(passSaleID, passSaleID.ToString());
                    foreach (var item in list)
                    {
                        tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                    }
                    if (!string.IsNullOrEmpty(tktXML))
                    {
                        XmlDataDocument xDoc = new XmlDataDocument();
                        xDoc.LoadXml(tktXML);
                        XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                        XmlNode xnr = result.SelectSingleNode("/Coupons");
                        if (xnr != null)
                        {
                            foreach (XmlNode xn in xnr)
                            {
                                Guid userid = AdminuserInfo.UserID;
                                if (AgentuserInfo.UserID != new Guid())
                                    userid = AgentuserInfo.UserID;
                                _oPrint.InsertStockInUsage(catId, queueId, id, stNo, xn.OuterXml, userid);
                            }
                        }
                    }
                    else
                    {
                        if (caseCount)
                        {
                            caseCount = false;
                            PrintQueueIdNew = Guid.NewGuid();
                            _oPrint.Insertqueueitems(PrintQueueIdNew, _oPrint.getPQdata(PQID));
                        }
                        string VOID = "<Coupon name=\"Pass\"><Element type=\"text\" name=\"void\" startcol=\"1\" endcol=\"72\" startrow=\"A\" endrow=\"R\" fontsize=\"37\" valign=\"middle\" align=\"middle\" fontface=\"Verdana\" fontbold=\"true\">VOID</Element></Coupon>";
                        _oPrint.InsertStockInUsage(catId, queueId, PrintQueueIdNew, stNo, VOID, AdminuserInfo.UserID);
                    }
                }
            }
        }
    }

    protected void btnConfirm_Click()
    {
        try
        {
            Guid queueId = new Guid();
            if (Request.QueryString["qId"] != null)
                queueId = Guid.Parse(Request.QueryString["qId"]);

            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;

            string tktXML = "";
            string pqitmIds = "";
            int start = 0;
            List<Guid> ids = new List<Guid>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
                if (chkPrint.Checked)
                {
                    HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
                    Guid id = Guid.Parse(hdnID.Value);
                    ids.Add(id);
                    if (start == 0)
                        pqitmIds = id.ToString();
                    else
                        pqitmIds += "," + id.ToString();
                    start = 1;
                }
            }
            var list = _oPrint.GetPrintQueueItemsInJobList(userid, pqitmIds, queueId);
            foreach (var item in list)
                tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;

            if (!string.IsNullOrEmpty(tktXML))
                PrintTicket(queueId, tktXML);
            foreach (var id in ids)
                _oPrint.UpdatePrintQConfirmedStatus(id, 2);
            UpdateEurailStockNumberStatus(2);
        }
        catch (Exception ex)
        {
            UpdateEurailStockNumberStatus(3);
            CleareprintingQueue(3);
            ShowMessage(2, ex.Message);
        }
    }

    void UpdateEurailStockNumberStatus(int status)
    {
        foreach (GridViewRow row in grdPrint.Rows)
        {
            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            Int32 OrderNo = Convert.ToInt32(hdnOrderNo.Value);
            if (OrderNo == 0 && status!=3)//void
                _oPrint.UpdateEurailStockNo(stNo, 5, userid);
            else
                _oPrint.UpdateEurailStockNo(stNo, status, userid);
        }
    }

    protected void btnProblem_Click(object sender, EventArgs e)
    {
        try
        {
            bool thisisVoid = false;
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateEurailStockNo(stno, 5, AgentuserInfo.UserID);
            CleareprintingQueue(4);

            btnCancel.Visible = false;
            btnPrint.Visible = false;
            lblHeader.Text = "Void Stock";
            Msgtxt.InnerText = "Please select (tick) the stock numbers that are still usable/undamaged and press 'OK' to attempt printing again on the same stock. If the stock is unusable and cannot be used again please untick all of the stock numbers on that sheet and click 'OK' to void it.";
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Showprinting", "Showprinting()", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnNotVoidOk_Click(object sender, EventArgs e)
    {
        bool thisisVoid = false;
        List<long> stockno = new List<long>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
            if (!chkunused.Checked)
                thisisVoid = true;
        }
        if (thisisVoid)
        {
            foreach (var stno in stockno)
                _oPrint.UpdateEurailStockNo(stno, 5, AgentuserInfo.UserID);
        }
        else
        {
            foreach (var stno in stockno)
            {
                _oPrint.DeleteEurailStockNo(stno);
                _oPrint.UpdateEurailStockNoReuse(stno);
            }
        }
        CleareprintingQueue(4);
        Response.Redirect("PrintingOrders");
    }

    protected void btnPrintedOK_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grdPrint.Rows)
        {
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            new ManageBooking().UpdateOrderStatus(7, Convert.ToInt64(hdnOrderNo.Value));
        }
        Response.Redirect("PrintingOrders");
    }

    public void CleareprintingQueue(int STATUS)
    {
        List<Guid> ids = new List<Guid>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            Guid id = Guid.Parse(hdnID.Value);
            ids.Add(id);
        }
        foreach (var id in ids)
            _oPrint.UpdatePrintQConfirmedStatus(id, STATUS);
    }

    public void PrintTicket(Guid branchId, string tktXML)
    {
        try
        {
            XmlDocument coupan = new XmlDocument();
            coupan.LoadXml(tktXML);

            //--Load RCT2 Template     
            var list = _oPrint.GetRCT2Template(branchId);
            if (list == null || list.Count == 0) return;
            XmlDocument template = new XmlDocument();
            template.LoadXml(list[0].Template);

            TicketPrinter.TicketPrinter TP = new TicketPrinter.TicketPrinter();
            TP.RootPath = Server.MapPath("");

            string password = "";
            password = Guid.NewGuid().ToString().Replace("-", "");
            UTF8Encoding enc = new UTF8Encoding();
            byte[] passby = enc.GetBytes(password);

            MemoryStream ms = new MemoryStream();
            ms = TP.Print(template, coupan, password);

            byte[] streampdf = ms.ToArray();
            int newSize = passby.Length + streampdf.Length;
            MemoryStream pdf = new MemoryStream(new byte[newSize], 0, newSize, true, true);
            pdf.Write(passby, 0, passby.Length);
            pdf.Write(streampdf, 0, streampdf.Length);

            byte[] initVectorBytes = Encoding.ASCII.GetBytes("162134562smelqpd");
            byte[] saltValueBytes = Encoding.ASCII.GetBytes("barbar");
            PasswordDeriveBytes enpassword = new PasswordDeriveBytes("larrylarry", saltValueBytes, "SHA1", 2);
            byte[] keyBytes = enpassword.GetBytes(32);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            byte[] outputstream = pdf.ToArray();
            cryptoStream.Write(outputstream, 0, outputstream.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();

            string length = pdf.Length.ToString();
            length = length.PadLeft(100, '0');
            byte[] lengths = enc.GetBytes(length);
            int newSizea = lengths.Length + cipherTextBytes.Length;
            MemoryStream finalPDF = new MemoryStream(new byte[newSizea], 0, newSizea, true, true);
            finalPDF.Write(lengths, 0, lengths.Length);
            finalPDF.Write(cipherTextBytes, 0, cipherTextBytes.Length);

            byte[] end = finalPDF.ToArray();

            Response.ContentType = "application/tim";
            Response.AddHeader("Content-disposition", "attachment; filename=printjob.pptool");
            Response.AddHeader("content-length", end.Length.ToString());

            Response.BinaryWrite(end);
            Response.Flush();
        }
        catch
        {
            Response.End();
        }
    }

    public XmlDocument EurailBuildTemplate(XmlDocument xDoc, string Path)
    {
        string CIVNo = "0";
        int TravellerCat = 0;
        string PassType = "";
        int DaysIn = 0;
        int Days = 0;
        int Months = 0;
        int CountryCodeStart = 0;
        string Traveller = "";
        string validity = "";
        DateTime startdate = DateTime.MinValue;
        DateTime enddate = DateTime.MinValue;
        string PassengerName = "";
        string Countryofresidence = "";
        string passportno = "";
        Boolean travelcalender = false;
        int ClassCode = 2;
        DateTime dateofissue = DateTime.Now;
        decimal cost = 0;
        //string Travellers = "";
        DateTime musstbeactivated = dateofissue.AddMonths(6);
        string ValidIN = "";
        Passenger[] pas = new Passenger[0];
        string PromoText = "";
        string currency = "EUR";
        //Rules

        Boolean MustBeActivatedVisible = false;
        Boolean MustBeActivatedVisibleUse = false;
        DateTime ActivatedByDate = DateTime.MinValue;
        int AdditionalDays = 0;
        int AdditionalMonths = 0;
        Boolean UseHideFirstAndLastDay = false;
        Boolean HideFirstAndLastDay = false;
        Boolean UseHidePassportNumber = false;
        Boolean HidePassportNumber = false;
        Boolean UseHideActivationStamp = false;
        Boolean HideActivationStamp = false;

        //  ***

        XmlNode xnr;

        XmlDocument xTemplate = new XmlDocument();
        xTemplate.Load(Path);

        //Rules

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/ActivatedByDate");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            try
            {
                ActivatedByDate = DateTime.Parse(xnr.InnerText);
            }
            catch (Exception e) { }
        }

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/AdditionalDays");
        if ((xnr != null) && (xnr.InnerText != "")) AdditionalDays = Convert.ToInt32(xnr.InnerText);

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/AdditionalMonths");
        if ((xnr != null) && (xnr.InnerText != "")) AdditionalMonths = Convert.ToInt32(xnr.InnerText);

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/MustBeActivatedVisible");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            MustBeActivatedVisibleUse = true;
            if (Convert.ToInt32(xnr.InnerText) == 1)
            {
                MustBeActivatedVisible = true;
            }
            if (Convert.ToInt32(xnr.InnerText) == 0)
            {
                MustBeActivatedVisible = false;
            }
        }

        if (ActivatedByDate != DateTime.MinValue)
        {
            ActivatedByDate = ActivatedByDate.AddDays(AdditionalDays);
            ActivatedByDate = ActivatedByDate.AddMonths(AdditionalMonths);
            musstbeactivated = ActivatedByDate;
        }

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/HideFirstAndLastDay");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            UseHideFirstAndLastDay = true;
            if (Convert.ToInt32(xnr.InnerText) == 1)
            {
                HideFirstAndLastDay = true;
            }
            if (Convert.ToInt32(xnr.InnerText) == 0)
            {
                HideFirstAndLastDay = false;
            }
        }
        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/HidePassportNumber");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            UseHidePassportNumber = true;
            if (Convert.ToInt32(xnr.InnerText) == 1)
            {
                HidePassportNumber = true;
            }
            if (Convert.ToInt32(xnr.InnerText) == 0)
            {
                HidePassportNumber = false;
            }
        }
        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Rule/HideActivationStamp");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            UseHideActivationStamp = true;
            if (Convert.ToInt32(xnr.InnerText) == 1)
            {
                HideActivationStamp = true;
            }
            if (Convert.ToInt32(xnr.InnerText) == 0)
            {
                HideActivationStamp = false;
            }
        }
        //  ***
        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/TravellerCat");
        if ((xnr != null) && (xnr.InnerText != "")) TravellerCat = Convert.ToInt32(xnr.InnerText);

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/PromoText");
        if ((xnr != null) && (xnr.InnerText != "")) PromoText = xnr.InnerText;

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/EuroCost");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            try
            {
                cost = Convert.ToDecimal(xnr.InnerText.ToString());
            }
            catch (Exception e) { }
        }

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Cost");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            try
            {
                cost = Convert.ToDecimal(xnr.InnerText.ToString());
            }
            catch (Exception e) { }
        }

        xnr = xDoc.SelectSingleNode("/Voucher/Passengers");
        if (xnr != null)
        {
            XmlNodeList nl = xnr.ChildNodes;
            cost = cost * nl.Count;
        }


        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/SupplierCurrencyID");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            Guid sourceCurId = Guid.Parse(xnr.InnerText);
            Guid targetCurrId = FrontEndManagePass.GetCurrencyID("SEU");
            cost = FrontEndManagePass.GetPriceAfterConversion(cost, _siteId, targetCurrId, sourceCurId);
        }

        xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/PassStartDate");
        if ((xnr != null) && (xnr.InnerText != ""))
        {
            try
            {
                startdate = DateTime.Parse(xnr.InnerText);
            }
            catch (Exception e) { }
        }


        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ClassCode");
        if ((xnr != null) && (xnr.InnerText != "")) ClassCode = Convert.ToInt32(xnr.InnerText);

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Name");
        if ((xnr != null) && (xnr.InnerText != "")) PassType = xnr.InnerText;


        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Traveller");
        if ((xnr != null) && (xnr.InnerText != "")) Traveller = xnr.InnerText;



        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CountryCodeStart");
        if (xnr != null && xnr.InnerText != "") CountryCodeStart = Convert.ToInt32(xnr.InnerText);

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Countries");
        if (xnr != null) ValidIN = xnr.InnerText;

        xnr = xDoc.SelectSingleNode("/Voucher/Passengers");
        if (xnr != null)
        {
            XmlNodeList nl = xnr.ChildNodes;
            pas = new Passenger[nl.Count];
            int counter = 0;
            foreach (XmlNode xn in nl)
            {
                pas[counter].Name = xn["Name"].InnerText;
                pas[counter].Country = xn["Country"].InnerText;
                pas[counter].PassportNumber = xn["PassportNo"].InnerText;
                counter++;
            }
        }

        validity = "";

        xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Vailidity");
        if ((xnr != null) && (xnr.InnerText != "")) validity = xnr.InnerText;
        tblProductValidUpTo objValid = FrontEndManagePass.ProductValidityByName(validity);

        if (objValid != null)
        {
            DaysIn = objValid.Day;
            Months = objValid.Month;
            Days = objValid.Day;
        }
        if(DaysIn>0 && Months==0)
        	enddate = startdate.AddDays(DaysIn);
        else if (Months > 0 && DaysIn == 0)
            enddate = startdate.AddMonths(Months);
        else
        {
            enddate = startdate.AddDays(DaysIn);
            enddate = enddate.AddMonths(Months);
        }

        if ((TravellerCat == 51) || (TravellerCat == 53) || (TravellerCat == 74) || (TravellerCat == 75))
        {


            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='tandc']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);

            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passdescriptiontext']");
            if (xnr != null) xnr.InnerText = "Eurail Global Pass No. " + CIVNo + " is only valid with the control voucher";

            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='CIVNo']");
            if (xnr != null) xnr.InnerText = CIVNo + 1;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissue']");
            if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Product']");
            if (xnr != null) xnr.InnerText = PassType.ToUpper();
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='DurationType']");
            if (DaysIn > 0)
            {
                travelcalender = true;
                if (xnr != null) xnr.InnerText = "FLEXI";
            }
            else
            {
                xnr.InnerText = "CONTINUOUS";
            }

            if (Traveller.ToLower().Contains("saver"))
            {

                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Traveller']");
                if (xnr != null) xnr.InnerText = Traveller.Replace("Saver", "        ");

            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='TravellerSaver']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);

                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Traveller']");
                if (xnr != null) xnr.InnerText = Traveller;
            }


            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='validity']");
            if (xnr != null) xnr.InnerText = validity;

            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Class']");
            if (xnr != null) xnr.InnerText = ClassCode.ToString();

            PassengerName = "SEE CONTROL VOUCHER";
            Countryofresidence = "SEE CONTROL VOUCHER";
            passportno = "SEE CONTROL VOUCHER";

            if (pas != null)
            {
                for (var counter = 0; counter < pas.Length; counter++)
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='PassengerName" + (counter + 1).ToString() + "']");
                    if (xnr != null) xnr.InnerText = pas[counter].Name;
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Countryofresidence" + (counter + 1).ToString() + "']");
                    if (xnr != null) xnr.InnerText = pas[counter].Country;
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno" + (counter + 1).ToString() + "']");
                    if (xnr != null) xnr.InnerText = pas[counter].PassportNumber;
                }
            }

            if ((MustBeActivatedVisibleUse == true) && (MustBeActivatedVisible == true))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='musstbeactivated']");
                if (xnr != null) xnr.InnerText = "BEFORE TRAVEL";
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='musstbeactivated']");
                if (xnr != null) xnr.InnerText = "BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
            }

            if ((TravellerCat == 61) || (TravellerCat == 31) || (TravellerCat == 32) || (TravellerCat == 71) || (TravellerCat == 72) || (TravellerCat == 73) || (TravellerCat == 74) || (TravellerCat == 75))
            {
                if ((MustBeActivatedVisibleUse == false) || ((MustBeActivatedVisibleUse == true) && (MustBeActivatedVisible == false)))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='MUSTBEACTIVATEDTEXT']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='MUSTBEACTIVATED']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
                if ((UseHidePassportNumber == true) && (HidePassportNumber == true))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno1']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno2']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno3']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno4']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno5']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
                if ((UseHideFirstAndLastDay == true) && (HideFirstAndLastDay == true))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp1']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp2']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp3']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='printername2']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp5']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp6']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp7']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp8']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp9']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
            }
            else
            {
                if ((UseHidePassportNumber == false) || ((UseHidePassportNumber == true) && (HidePassportNumber == true)))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno1']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno2']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno3']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno4']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno5']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
                if ((UseHideFirstAndLastDay == false) || ((UseHideFirstAndLastDay == true) && (HideFirstAndLastDay == true)))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp1']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp2']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp3']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='printername2']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp5']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp6']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp7']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp8']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp9']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
            }
        }
        else
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='tandccoupon']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);

            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);

            xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/Name");
            if (xnr != null) PassengerName = xnr.InnerText;
            xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/Country");
            if ((xnr != null) && (xnr.InnerText != "")) Countryofresidence = xnr.InnerText;
            xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/PassportNo");
            if ((xnr != null) && (xnr.InnerText != "")) passportno = xnr.InnerText;
        }

        if ((MustBeActivatedVisibleUse == true) && (MustBeActivatedVisible == true))
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATED']");
            if (xnr != null) xnr.InnerText = "BEFORE TRAVEL";
        }
        else
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATED']");
            if (xnr != null) xnr.InnerText = "BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='CIVNo']");
        if (xnr != null) xnr.InnerText = CIVNo;
        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissue']");
        if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp6']");
        if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Product']");
        if (xnr != null) xnr.InnerText = PassType.ToUpper();

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='DurationType']");


        if (DaysIn > 0)
        {
            travelcalender = true;
            if (xnr != null) xnr.InnerText = "FLEXI";
        }
        else
        {
            if (xnr != null) xnr.InnerText = "CONTINUOUS";
        }

        if ((CountryCodeStart > 0) && (CountryCodeStart < 9999))
        {
            if (ValidIN != "")
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='countries']");
                if (xnr != null) xnr.InnerText = ValidIN;
            }
            else
            {
                xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='countries']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validin']");
                if (xnr != null && xnr.ParentNode != null) xnr.ParentNode.RemoveChild(xnr);
            }
        }
        else
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validin']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
        }

        if (Traveller.ToLower().Contains("saver"))
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Traveller']");
            if (xnr != null) xnr.InnerText = Traveller.Replace("Saver", "        ");

        }
        else
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TravellerSaver']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Traveller']");
            if (xnr != null) xnr.InnerText = Traveller;
        }


        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validity']");
        if (xnr != null) xnr.InnerText = validity;

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday1']");
        if (startdate.Day > 9)
        {
            if (xnr != null) xnr.InnerText = startdate.Day.ToString().Substring(0, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = "0";
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday2']");
        if (startdate.Day > 9)
        {
            if (xnr != null) xnr.InnerText = startdate.Day.ToString().Substring(1, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = startdate.Day.ToString();
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth1']");
        if (startdate.Month > 9)
        {
            if (xnr != null) xnr.InnerText = startdate.Month.ToString().Substring(0, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = "0";
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth2']");
        if (startdate.Month > 9)
        {
            if (xnr != null) xnr.InnerText = startdate.Month.ToString().Substring(1, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = startdate.Month.ToString();
        }

        try
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear1']");
            if (xnr != null) xnr.InnerText = startdate.Year.ToString().Substring(2, 1);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear2']");
            if (xnr != null) xnr.InnerText = startdate.Year.ToString().Substring(3, 1);
        }
        catch (Exception e) { }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday1']");
        if (enddate.Day > 9)
        {
            if (xnr != null) xnr.InnerText = enddate.Day.ToString().Substring(0, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = "0";
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday2']");
        if (enddate.Day > 9)
        {
            if (xnr != null) xnr.InnerText = enddate.Day.ToString().Substring(1, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = enddate.Day.ToString();
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth1']");
        if (enddate.Month > 9)
        {
            if (xnr != null) xnr.InnerText = enddate.Month.ToString().Substring(0, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = "0";
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth2']");
        if (enddate.Month > 9)
        {
            if (xnr != null) xnr.InnerText = enddate.Month.ToString().Substring(1, 1);
        }
        else
        {
            if (xnr != null) xnr.InnerText = enddate.Month.ToString();
        }

        try
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear1']");
            if (xnr != null) xnr.InnerText = enddate.Year.ToString().Substring(2, 1);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear2']");
            if (xnr != null) xnr.InnerText = enddate.Year.ToString().Substring(3, 1);
        }
        catch (Exception e) { }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PassengerName']");
        if (xnr != null) xnr.InnerText = PassengerName;
        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Countryofresidence']");
        if (xnr != null) xnr.InnerText = Countryofresidence;
        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
        if (xnr != null) xnr.InnerText = passportno;

        if (travelcalender)
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calender']");
            if (xnr != null) xnr.Attributes["numberofdays"].InnerXml = DaysIn.ToString();
        }
        else
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='travelcalender']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calender']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calenderDay']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calenderMonth']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
        }

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Class']");
        if (xnr != null) xnr.InnerText = ClassCode.ToString();

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='cost']");
        if (xnr != null) xnr.InnerText = currency + ": " + cost.ToString("F2");

        if ((TravellerCat != 31) && (TravellerCat != 32))
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='AD75']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
        }

        if ((TravellerCat != 61))
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='COMPLIMENTARY']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='COMPLIMENTARYcost']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
        }

        if ((TravellerCat != 71) && (TravellerCat != 72) && (TravellerCat != 73) && (TravellerCat != 74) && (TravellerCat != 75))
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONAL']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOBOX']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALTEXT']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
        }
        else
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='travelcalender']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALTEXT']");
            if (xnr != null) xnr.InnerText = PromoText;
        }

        if ((TravellerCat == 61) || (TravellerCat == 31) || (TravellerCat == 32) || (TravellerCat == 71) || (TravellerCat == 72) || (TravellerCat == 73) || (TravellerCat == 74) || (TravellerCat == 75))
        {
            if ((MustBeActivatedVisibleUse == false) || ((MustBeActivatedVisibleUse == true) && (MustBeActivatedVisible == false)))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATEDTEXT']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATED']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }

            if ((UseHideFirstAndLastDay == true) && (HideFirstAndLastDay == true))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            if ((UseHidePassportNumber == true) && (HidePassportNumber == true))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
                if (xnr.InnerText != "SEE CONTROL VOUCHER")
                {
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
            }
            if ((UseHideActivationStamp == true) && (HideActivationStamp == true))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp3']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='printername2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp5']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp6']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp7']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp8']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp9']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
        }
        else
        {
            if ((UseHideActivationStamp == false) || ((UseHideActivationStamp == true) && (HideActivationStamp == true)))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp3']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='printername2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp5']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp6']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp7']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp8']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp9']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            if ((UseHideFirstAndLastDay == false) || ((UseHideFirstAndLastDay == true) && (HideFirstAndLastDay == true)))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            if ((UseHidePassportNumber == false) || ((UseHidePassportNumber == true) && (HidePassportNumber == true)))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
                if ((xnr != null) && (xnr.InnerText != ""))
                {
                    if (xnr.InnerText != "SEE CONTROL VOUCHER")
                    {
                        if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    }
                }
            }


        }

        if ((TravellerCat == 61))
        {
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='cost']");
            if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
        }


        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='blank1']");
        if (xnr != null) xnr.InnerText = "1111111111111111111111111";

        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='blank2']");
        if (xnr != null) xnr.InnerText = "2222222222222222222222222222";

        return xTemplate;
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void chkPrint_CheckedChanged(object sender, EventArgs e)
    {
    }
 
    protected void grdPrint_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblHideClass = e.Row.FindControl("lblHideClass") as Label;
            if (!string.IsNullOrEmpty(lblHideClass.Text))
            {
                e.Row.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
            }
        }
    }

    struct Passenger
    {
        public string Name;
        public string Country;
        public string PassportNumber;
    }

}