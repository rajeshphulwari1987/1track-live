﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PrintingOrders.aspx.cs" Inherits="Printing_PrintingOrders" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register TagPrefix="uc" TagName="Newsletter" Src="../newsletter.ascx" %>--%>
<%--<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>--%>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src='../Scripts/jquery-1.9.0.min.js'></script>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <section class="content">

    <h1>Print Queue - Assignment</h1>
    <p>Welcome to Print Queue.</p>
    <p style="width:845px;">Click on the word "Queued" to assign an order(s) to your printing. Then click "print" when you're ready to check the ticket stock.</p>
    <div class="clear"> &nbsp;</div>
    <div class="privacy-block-outer pass" style="margin-top: 10px; width:84%;">
    <div class="red-title">
        Print Queue</div>
    <div class="discription-block">
        <div class="clsdivPrduct">
             <div class="clsPrd">Category:</div>
             <div class="clsPrd">
                 <asp:DropDownList ID="ddlCategory" Width="210" class="input clsDrp" runat="server" ValidationGroup="rv" AutoPostBack="True"      OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"/>
             </div>
             <div class="clsPrd"> Stock Queue:</div>  
             <div class="clsPrd" >
             <asp:DropDownList ID="ddlStock" class="input clsDrpQ" Width="275px"  runat="server" ValidationGroup="rv" AutoPostBack="True"   OnSelectedIndexChanged="ddlStock_SelectedIndexChanged"/>
             <asp:RequiredFieldValidator ID="reqStock" runat="server" ValidationGroup="st" ControlToValidate="ddlStock" InitialValue ="-1"  Display="Dynamic" CssClass="validreq"/>
             <%--ErrorMessage="Please select stock queue."--%>
             </div>       
                    
             <asp:Button ID="btnPrint" runat="server" Text="Print" 
                 CssClass="btn-red-cart f-right" Width="100px" OnClick="btnPrint_Click" ValidationGroup="st"/>
        </div>
    </div>
    </div>
    <table id="tblPrintQueueList"><tr class="red-title">
			<td style="width:100px" class="clsPrintColor">Order</td>
            <td style="width:200px;text-align:left;" class="clsPrintColor">Lead Passenger</td>
            <td style="width:400px;text-align:left;" class="clsPrintColor">Product</td>
            <td style="width:145px;text-align:center;" class="clsPrintColor">Status</td>
		</tr></table>
    <div id="dvPrd" class="grdBrd" runat="server" Visible="False" style="width:843px">
        <div class="clsdivPrduct">
        
        <asp:GridView ID="grdPrintQueue" runat="server" AutoGenerateColumns="False" PageSize="10" ShowHeader="false"
            CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" 
                Width="100%" OnRowCommand="grdPrintQueue_RowCommand">
            <AlternatingRowStyle CssClass="clsPrintingGrid"/>
            <PagerStyle CssClass="paging"></PagerStyle> 
            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC"/>
            <HeaderStyle CssClass="clsPrintingHeader" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <EmptyDataTemplate>
                Record not found.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Order">
                    <ItemTemplate>
                        <%#Eval("OrderNo")%>
                        <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryID")%>'/>
                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Eval("ID")%>'/>
                    </ItemTemplate>
                    <ItemStyle Width="80px"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lead Passenger">
                    <ItemTemplate>
                      <%#Eval("LeadPassenger")%>
                    </ItemTemplate>
                    <ItemStyle Width="150px"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <%#Eval("ProductName")%>
                    </ItemTemplate>
                    <ItemStyle Width="300px"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                       <asp:LinkButton ID="lnkStatus" runat="server" Text='<%#Eval("Status")%>' CommandArgument='<%#Eval("ID")%>' CommandName='<%#"QueueStatusñ"+Eval("OrderNo")%>'/>                    
                       <asp:LinkButton ID="lnkMove" runat="server" Text="Move" CommandArgument='<%#Eval("ID")%>' CommandName="Move"/>
                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandArgument='<%#Eval("ID")%>' CommandName='<%#"Removeñ"+Eval("OrderNo")%>' OnClientClick="return confirm('Are you sure? Do you want to delete this item?')"/>
                    </ItemTemplate>
                    <ItemStyle Width="110px" HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div> 
    </div>
</section>
    <div style="display: none">
        <a id="lnkSentToQueue" runat="server" href="#"></a>
    </div>
    <%--Stock Queue Popup--%>
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:Panel ID="pnlPrntQ" runat="server" CssClass="popup" Width="450px">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Send to Queue</div>
            <a href="#" id="btnClose" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="clsMrgTop10">
            <asp:DropDownList ID="ddlPrntQ" runat="server" Width="400px" ValidationGroup="vp" />
            <asp:RequiredFieldValidator ID="reqddlPrntQ" ControlToValidate="ddlPrntQ" InitialValue="-1"
                ValidationGroup="vp" runat="server" ErrorMessage="Please select Stock Queue"
                CssClass="validreq" />
        </div>
        <div class="clsMrgTop10">
            <asp:Button ID="btnSend" runat="server" CssClass="button clsBtn" Text="Send" ValidationGroup="vp"
                Width="90" OnClick="btnSend_Click" />
            <asp:Button ID="btnCancel" runat="server" CssClass="button clsBtn" Text="Cancel"
                Width="90" />
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdpupPrntQ" runat="server" TargetControlID="lnkSentToQueue"
        CancelControlID="btnClose" PopupControlID="pnlPrntQ" BackgroundCssClass="modalBackground" />
</asp:Content>
