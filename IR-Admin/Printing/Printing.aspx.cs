﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using System.IO;
using System.Security.Cryptography;

public partial class Printing_Printing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void PrintTicket()
    {
        XmlDocument xDoc = new XmlDocument();
        XmlDocument Stock = new XmlDocument();
        //Stock.LoadXml(result);

        TicketPrinter.TicketPrinter TP = new TicketPrinter.TicketPrinter();
        TP.RootPath = Server.MapPath("");

        string password = "";
        password = Guid.NewGuid().ToString().Replace("-", "");
        UTF8Encoding enc = new UTF8Encoding();
        byte[] passby = enc.GetBytes(password);

        MemoryStream ms = new MemoryStream();
        ms = TP.Print(Stock, xDoc, password);
        byte[] streampdf = ms.ToArray();
        int newSize = passby.Length + streampdf.Length;
        MemoryStream pdf = new MemoryStream(new byte[newSize], 0, newSize, true, true);
        pdf.Write(passby, 0, passby.Length);
        pdf.Write(streampdf, 0, streampdf.Length);

        byte[] initVectorBytes = Encoding.ASCII.GetBytes("162134562smelqpd");
        byte[] saltValueBytes = Encoding.ASCII.GetBytes("barbar");
        PasswordDeriveBytes enpassword = new PasswordDeriveBytes("larrylarry", saltValueBytes, "SHA1", 2);
        byte[] keyBytes = enpassword.GetBytes(32);
        RijndaelManaged symmetricKey = new RijndaelManaged();
        symmetricKey.Mode = CipherMode.CBC;
        ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
        MemoryStream memoryStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        byte[] outputstream = pdf.ToArray();
        cryptoStream.Write(outputstream, 0, outputstream.Length);
        cryptoStream.FlushFinalBlock();
        byte[] cipherTextBytes = memoryStream.ToArray();
        memoryStream.Close();
        cryptoStream.Close();

        string length = pdf.Length.ToString();
        length = length.PadLeft(100, '0');
        byte[] lengths = enc.GetBytes(length);
        int newSizea = lengths.Length + cipherTextBytes.Length;
        MemoryStream finalPDF = new MemoryStream(new byte[newSizea], 0, newSizea, true, true);
        finalPDF.Write(lengths, 0, lengths.Length);
        finalPDF.Write(cipherTextBytes, 0, cipherTextBytes.Length);

        byte[] end = finalPDF.ToArray();

        Response.ContentType = "application/tim";
        Response.AddHeader("Content-disposition", "attachment; filename=printjob.pptool");
        Response.AddHeader("content-length", end.Length.ToString());

        Response.BinaryWrite(end);

        Response.End();


    }
}