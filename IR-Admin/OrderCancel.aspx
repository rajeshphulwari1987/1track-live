﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OrderCancel.aspx.cs" Inherits="OrderCancel" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        /*booking detail*/
        
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
    </style>
    <%=script %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="content">
        <div class="innner-banner">
            <div class="bannerLft">
                <div class="banner90">
                    <asp:Label ID="lblBannerTxt" runat="server" Text="Rail Passes"/></div>
            </div>
            <div>
                <div class="float-lt" style="width: 73%">
                    <asp:Image ID="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="901px" Height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
                <div class="float-rt" style="width: 27%; text-align: right;">
                    <asp:Image ID="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="100%" Height="190px" ImageUrl="images/innerMap.gif" />
                </div>
            </div>
        </div>
        <div class="left-content">
            <h1>
                Order request has been canceled.
            </h1>
            <p>
                Venenatis per interdum riduculus ligula placerat elit donec cammando dui. Prasent
                tortor aliquam urna Curabitur erat penatibus vel pede suscipit penatibus sadales
                per lociania risus elementum cras orci sapien
            </p>
        </div>
        <div class="right-content">
            <a href='<%=siteURL%>TrainResults.aspx' id="trainresults">
                <div id="rtPannel" runat="server">
                </div>
            </a>
            <img src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />
            <a href='<%=siteURL%>GeneralInformation.aspx'>
                <uc:GeneralInformation ID="GInfo" runat="server" />
            </a>
            <img src="images/block-shadow.jpg" class="scale-with-grid" alt="" border="0" />
            <uc:Newsletter ID="Newsletter1" runat="server" />
        </div>
    </div>
    <asp:HiddenField ID="hdnUserName" runat="server" />
</asp:Content>
