﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI.HtmlControls;

namespace IR_Admin
{
    public partial class DeleteSiteRole : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                BindUsers();
                BindSiteDeletionRightList();
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    tab = "2";
                    BindSiteListForEdit(Guid.Parse(Request["edit"]));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }

        }
        private void BindUsers()
        {
            ddlUser.Items.Clear();
            var user = (from ta in _db.tblAdminUsers
                        join ar in _db.aspnet_Roles on ta.RoleID equals ar.RoleId
                        where ta.IsActive == true && (ar.RoleName.ToLower().Contains("admin") || ar.RoleName.ToLower().Contains("administator"))
                        orderby ta.UserName
                        select new { ta.UserName, ta.Note, ta.ID, ta.IsActive, ta.IsAgent }).ToList();

            //List<tblAdminUser> user = db.tblAdminUsers.ToList();
            ddlUser.DataSource = user;
            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "UserName";
            ddlUser.DataBind();
            ddlUser.Items.Insert(0, new ListItem("-Select-", "0"));
        }
        public void BindSiteList()
        {
            try
            {
                if (ddlUser.SelectedIndex > 0)
                {
                    List<UserSiteList_Result> lstSite = _Master.UserSitelist().Where(a => a.ADMINUSERID == Guid.Parse(ddlUser.SelectedValue.Trim())).ToList();
                    if (lstSite.Count() > 0)
                    {
                        chkSite.DataSource = lstSite;
                        chkSite.DataValueField = "ID";
                        chkSite.DataTextField = "DisplayName";
                        chkSite.DataBind();
                    }
                }
                else
                {
                    chkSite.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }
        public void BindSiteDeletionRightList()
        {
            try
            {
                List<UserSiteDeletion> lst = new Masters().GetUsersWithSiteDeletionRight();
                grdSites.DataSource = lst;
                grdSites.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindSiteListForEdit(Guid ID)
        {
            try
            {
                ddlUser.SelectedValue = ID.ToString();
                BindSiteList();
                List<tblSiteDeletionRight> lst = new Masters().GetUserSiteDeletionRight(ID);
                foreach (tblSiteDeletionRight o in lst)
                {
                    foreach (ListItem lit in chkSite.Items)
                    {
                        if (Guid.Parse(lit.Value.Trim()) == o.SiteID)
                        {
                            lit.Selected = true;
                        }
                    }
                }
                btnSubmit.Text = "Update";
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        public void AddSitePges(Guid id)
        {
            if (_db.tblWebMenuSiteLookups.Any(x => x.SiteID == id))
                return;

            var tblWebMenu = _db.tblWebMenus.ToList();
            foreach (var item in tblWebMenu)
            {
                var masterlkUp = new tblWebMenuSiteLookup();
                masterlkUp.ID = Guid.NewGuid();
                masterlkUp.SiteID = id;
                masterlkUp.PageID = item.ID;
                _db.AddTotblWebMenuSiteLookups(masterlkUp);
                _db.SaveChanges();
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("DeleteSiteRole.aspx");
        }

        protected void grdSites__RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid ID = Guid.Parse(e.CommandArgument.ToString());
            DivError.Style.Add("display", "none");
            DivSuccess.Style.Add("display", "none");
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        Response.Redirect("DeleteSiteRole.aspx?edit=" + ID);
                        break;
                    }
                case "Remove":
                    {
                        new Masters().DeleteSiteDeletionRightForUser(ID);
                        BindSiteDeletionRightList();
                        break;
                    }
            }
            Response.Redirect("DeleteSiteRole.aspx");
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            tab = "2";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
            BindSiteList();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            tab = "2";
            if (ddlUser.SelectedIndex > 0)
            {
                int count = 0;
                new Masters().DeleteSiteDeletionRightForUser(Guid.Parse(ddlUser.SelectedValue.Trim()));
                foreach (ListItem lit in chkSite.Items)
                {
                    if (lit.Selected == true)
                    {
                        tblSiteDeletionRight objRight = new tblSiteDeletionRight();
                        objRight.ID = Guid.NewGuid();
                        objRight.SiteID =Guid.Parse(lit.Value.Trim());
                        objRight.AssignTo = Guid.Parse(ddlUser.SelectedValue);
                        count++;
                        new Masters().AddSiteDeletionRight(objRight);
                    }
                }
                if (count == 0)
                {
                    ShowMessage(2, "Please select site name.");
                    tab = "2";
                }
                else
                {
                    ShowMessage(1, "Save changes successfully.");
                }
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
        }


    }
}
