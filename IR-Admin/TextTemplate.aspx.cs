﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class TextTemplate : Page
    {
        readonly private ManageBooking _master = new ManageBooking();
        readonly Masters _oMaster = new Masters();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                ShowMessage(0, null);
                BindSite();
                BindGrid(_SiteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetTextTemplateForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp",
                                                        "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            Tab = "1";
            grdText.DataSource = _master.GetTextTemplate(_SiteID);
            grdText.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdText_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdText.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }
        
        protected void grdText_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteTextTemplate(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #region Add/Edit
        public void BindSite()
        {
            ddlSite.DataSource = _oMaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddTemplate()
        {
            try
            {
                _master.AddTextTemplate(new tblTextContent
                {
                    ID = Guid.NewGuid(),
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.Text.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now
                });
                ShowMessage(1, "Template added successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdateTemplate()
        {
            try
            {
                var id = Guid.Parse(Request["id"]);
                var txt = new tblTextContent
                {
                    ID = id,
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.Text.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    ModifyBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now
                };
                _master.UpdateTextTemplate(txt);
                ShowMessage(1, "Template updated successfully.");
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddTemplate();
                else
                    UpdateTemplate();
                Tab = "1";
                ClearControls();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlSite.SelectedIndex = 0;
            txtTitle.Text = string.Empty;
            txtDesc.Text = string.Empty;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("TextTemplate.aspx");
        }

        public void GetTextTemplateForEdit(Guid id)
        {
            tblTextContent oP = _master.GetTextTemplateById(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = oP.SiteID.ToString();
                txtTitle.Text = oP.Title;
                txtDesc.Text = oP.Description;
            }
        }

        #endregion  
    }
}