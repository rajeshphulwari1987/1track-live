﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using System.Collections.Generic;

namespace IR_Admin
{
    public partial class Shippingdetails : Page
    {
        readonly private ManageBooking _master = new ManageBooking();
        readonly Masters _oMaster = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
            BindSite(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                BindSite(_SiteID);
                SiteSelected();
                ShowMessage(0, null);
                BindGrid(_SiteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetShippingForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp",
                                                        "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            txtDesc.Text = string.Empty;
            txtPrice.Text = string.Empty;
            txtTitle.Text = string.Empty;
            chkIsactive.Checked = false;
            Tab = "1";
            grdShipping.DataSource = _master.GetShippingData(_SiteID);
            grdShipping.DataBind();
        }

        protected void grdShipping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdShipping.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }

        public void BindSite(Guid SiteID)
        {
            dtlCountry.DataSource = _oMaster.GetRegionList().ToList().OrderBy(x => x.RegionName).ToList();
            dtlCountry.DataBind();

            dtlPassCountry.DataSource = _oMaster.GetRegionList().ToList().OrderBy(x => x.RegionName).ToList();
            dtlPassCountry.DataBind();

            ddlSite.DataSource = _oMaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
            ddlSite.SelectedValue = SiteID.ToString();
        }

        protected void grdShipping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteShippingByID(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
                if (e.CommandName == "ActiveInActive")
                {
                    string[] str = e.CommandArgument.ToString().Split(',');
                    var res = _master.ActiveShippingByID(Guid.Parse(str[0]), Convert.ToBoolean(str[1]), AdminuserInfo.UserID, DateTime.Now);
                    if (res)
                        ShowMessage(1, "Record updated successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
                if (e.CommandName == "VisibleFront")
                {
                    string[] str = e.CommandArgument.ToString().Split(',');
                    var res = _master.VisibleShippingByID(Guid.Parse(str[0]), Convert.ToBoolean(str[1]), AdminuserInfo.UserID, DateTime.Now);
                    if (res)
                        ShowMessage(1, "Record updated successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
                if (e.CommandName == "DefaultIfMissing")
                {
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    string[] str = e.CommandArgument.ToString().Split(',');
                    var res = _master.DefaultShippingByID(Guid.Parse(str[0]), Convert.ToBoolean(str[1]), AdminuserInfo.UserID, DateTime.Now);
                    var shipid = Guid.Parse(str[0]);
                    var setNotdefault = _db.tblShippings.Where(x => x.ID != shipid && x.SiteID == _SiteID).ToList();
                    foreach (var item in setNotdefault)
                    {
                        item.DefaultWhenMissing = false;
                        _db.SaveChanges();
                    }
                    if (res)
                        ShowMessage(1, "Record updated successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var shippingid = _master.AddUpdateShippingData(new tblShipping
                 {
                     ID = (Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid()),
                     ShippingName = txtTitle.Text,
                     Price = Convert.ToDecimal(txtPrice.Text),
                     SiteID = Guid.Parse(ddlSite.SelectedValue),
                     Description = txtDesc.Text,
                     CreatedBy = AdminuserInfo.UserID,
                     CreatedOn = DateTime.Now,
                     IsActive = chkIsactive.Checked
                 });

                //P2P Shipping
                var lstShippingCountryLookup = new List<tblShippingOptionCountry>();
                foreach (DataListItem item in dtlCountry.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var trCountry = item.FindControl("trCountry") as TreeView;
                        if (trCountry != null)
                        {
                            foreach (TreeNode node in trCountry.Nodes)
                            {
                                if (node.Checked)
                                {
                                    lstShippingCountryLookup.Add(new tblShippingOptionCountry
                                        {
                                            CountryID = Guid.Parse(node.Value),
                                            ShippingID = shippingid
                                        });
                                }
                            }
                        }
                    }
                }
                _master.AddUpdateShippingCountryLookup(shippingid, lstShippingCountryLookup);

                //Pass Shipping
                var lstPassShippingCountryLookup = new List<tblPassShippingOptionCountry>();
                foreach (DataListItem item in dtlPassCountry.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var trCountry = item.FindControl("trCountry") as TreeView;
                        if (trCountry != null)
                        {
                            foreach (TreeNode node in trCountry.Nodes)
                            {
                                if (node.Checked)
                                {
                                    lstPassShippingCountryLookup.Add(new tblPassShippingOptionCountry
                                    {
                                        CountryID = Guid.Parse(node.Value),
                                        ShippingID = shippingid
                                    });
                                }
                            }
                        }
                    }
                }
                _master.AddUpdatePassShippingCountryLookup(shippingid, lstPassShippingCountryLookup);

                if (Request["id"] != null)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ClearControls();
                Tab = "1";
                ShowMessage(1, Request["id"] == null ? "Shipping Details added successfully." : "Shipping Details updated successfully.");
                _SiteID = Master.SiteID;
                SiteSelected();
                BindGrid(_SiteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            txtTitle.Text = string.Empty;
            txtPrice.Text = string.Empty;
            txtDesc.Text = string.Empty;
            ddlSite.SelectedIndex = 0;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ShippingDetails.aspx");
        }

        public void GetShippingForEdit(Guid id)
        {
            var oP = _master.GetShippingByID(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = oP.SiteID.ToString();
                txtTitle.Text = oP.ShippingName;
                txtDesc.Text = oP.Description;
                chkIsactive.Checked = oP.IsActive;
                txtPrice.Text = Convert.ToString(oP.Price);
            }

            // P2P Look Up Sites
            var listCnt = _master.GetShippingCountriesById(id).Select(x => x.CountryID);
            foreach (DataListItem item in dtlCountry.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var trCountry = item.FindControl("trCountry") as TreeView;
                    if (trCountry == null)
                        return;
                    foreach (TreeNode node in trCountry.Nodes)
                    {
                        node.Checked = listCnt.Contains(Guid.Parse(node.Value));
                    }
                }
            }

            //Pass Look Up Sites
            var listPassCnt = _master.GetPassShippingCountriesById(id).Select(x => x.CountryID);
            foreach (DataListItem item in dtlPassCountry.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var trCountry = item.FindControl("trCountry") as TreeView;
                    if (trCountry == null)
                        return;
                    foreach (TreeNode node in trCountry.Nodes)
                    {
                        node.Checked = listPassCnt.Contains(Guid.Parse(node.Value));
                    }
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void dtlCountry_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var trCountry = e.Item.FindControl("trCountry") as TreeView;
                var hdnRegionId = e.Item.FindControl("hdnRegion") as HiddenField;
                var lisCountry = _oMaster.GetCountryList().Where(x => x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value));
                foreach (var itemCountry in lisCountry)
                {
                    var trCat = new TreeNode();
                    trCat.Text = itemCountry.CountryName;
                    trCat.Value = itemCountry.CountryID.ToString();
                    trCat.SelectAction = TreeNodeSelectAction.None;
                    trCountry.Nodes.Add(trCat);
                }
            }
        }

        protected void dtlPassCountry_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var trCountry = e.Item.FindControl("trCountry") as TreeView;
                var hdnRegionId = e.Item.FindControl("hdnRegion") as HiddenField;
                var lisCountry = _oMaster.GetCountryList().Where(x => x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value));
                foreach (var itemCountry in lisCountry)
                {
                    var trCat = new TreeNode();
                    trCat.Text = itemCountry.CountryName;
                    trCat.Value = itemCountry.CountryID.ToString();
                    trCat.SelectAction = TreeNodeSelectAction.None;
                    trCountry.Nodes.Add(trCat);
                }
            }
        }
    }
}