﻿<%@ Page Title="Add Menu " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ViewWebMenu.aspx.cs" Inherits="IR_Admin.AddMenu" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">  
     $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });       
        $(function () {                  
             if(<%=tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }           
        });
  
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Web Navigation</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ViewWebMenu.aspx" class="current">List</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <asp:GridView ID="grdWebMenu" runat="server" CellPadding="4" CssClass="grid-head2"
                        GridLines="None" DataKeyNames="ID" AutoGenerateColumns="False" OnRowCommand="grdWebMenu_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Navigation Name" />
                            <asp:TemplateField HeaderText="Show On Top">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgTop" CommandArgument='<%#Eval("ID")%>' Height="16"
                                        CommandName="IsTop" AlternateText="IsTop" ImageUrl='<%#Eval("IsTop").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsTop").ToString()=="True" ?"Active":"InActive" %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Show On Bottom">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgBottom" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="IsBottom" AlternateText="IsBottom" ImageUrl='<%#Eval("IsBottom").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsBottom").ToString()=="True" ?"Active":"InActive" %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgUp" runat="server" CommandName="Up" ImageUrl="schemes/internationalrail/images/icon-move-up.png"
                                        CommandArgument='<%# String.Format("{0},{1},{2}", Eval("ID"),Eval("PSortOrder"),Eval("CSortOrder")) %>' />&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDown" runat="server" CommandName="Down" ImageUrl="schemes/internationalrail/images/icon-move-down.png"
                                        CommandArgument='<%# String.Format("{0},{1},{2}", Eval("ID"),Eval("PSortOrder"),Eval("CSortOrder")) %>' />&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
