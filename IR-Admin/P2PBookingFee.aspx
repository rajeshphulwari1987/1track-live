﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="P2PBookingFee.aspx.cs" Inherits="IR_Admin.P2PBookingFee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        P2P Booking Fee</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <!-- tab "panes" -->
    <div class="full mr-tp1">
        <div class="panes">
            <div id="divlist" runat="server">
                <div class="crushGvDiv" style="font-size: 13px;">
                    <%-- <div id="divNew" class="grid-sec2" runat="server" style="display: block;">--%>
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <legend><b>P2P Booking Fee </b></legend>
                                    <asp:GridView ID="grvBookingFee" runat="server" AutoGenerateColumns="False" PageSize="10"
                                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                        AllowPaging="True" OnPageIndexChanging="grvBookingFee_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                        <PagerStyle CssClass="paging"></PagerStyle>
                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                            BorderColor="#FFFFFF" BorderWidth="1px" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            Record not found.</EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <%#Eval("Name")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="50%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fee">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtFee" runat="server" Text='<%#string.IsNullOrEmpty(Convert.ToString(Eval("Fee")))? "0.00" : Eval("Fee")%>'
                                                        Width="50px" Style="float: left; text-align: right;" />
                                                    <asp:FilteredTextBoxExtender ID="ftbFee" runat="server" TargetControlID="txtFee"
                                                        ValidChars="0123456789." />
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fee in (%)">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtIsPercent" runat="server" Text='<%#string.IsNullOrEmpty(Convert.ToString(Eval("IsPercentage")))? "0.00" : Eval("IsPercentage")%>'
                                                        Width="50px" Style="float: left; text-align: right;" />
                                                    <asp:FilteredTextBoxExtender ID="ftbIsPercent" runat="server" TargetControlID="txtIsPercent"
                                                        ValidChars="0123456789." />
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Is Applicable">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ID="imgApplicable" CommandArgument='<%#Eval("ID")%>'
                                                        Height="16" CommandName="IsAgent" AlternateText="IsAgent" ImageUrl='<%#Eval("IsApplicable").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                        ToolTip='<%#Eval("IsApplicable").ToString()=="True" ?"Applicable":"Not Applicable" %>'
                                                        OnClick="imgApplicable_Click" />
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%#Eval("SiteID")%>' />
                                                    <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ID")%>' />
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><b>P2P Api Booking Fee </b></legend>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="cat-inner-alt" style="text-align: center;margin-bottom: 13px;">
                                                <asp:DropDownList runat="server" style="margin: 10px;padding:3px;height:30px;" Width="500" ID="ddlSitelist" OnSelectedIndexChanged="ddlSitelist_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:GridView ID="GrdAPIBooking" runat="server" AutoGenerateColumns="False" PageSize="10"
                                                CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                AllowPaging="false">
                                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                                <PagerStyle CssClass="paging"></PagerStyle>
                                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                                <EmptyDataTemplate>
                                                    Record not found.</EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <%#Eval("Name")%>
                                                            <asp:HiddenField runat="server" ID="hdnApiName" Value='<%#Eval("Name")%>'/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="50%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Fee">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtFee" runat="server" Text='<%#string.IsNullOrEmpty(Convert.ToString(Eval("Fee")))? "0.00" : Eval("Fee")%>'
                                                                Width="50px" Style="float: left; text-align: right;" />
                                                            <asp:FilteredTextBoxExtender ID="ftbFee" runat="server" TargetControlID="txtFee"
                                                                ValidChars="0123456789." />
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Fee in (%)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtIsPercent" runat="server" Text='<%#string.IsNullOrEmpty(Convert.ToString(Eval("IsPercentage")))? "0.00" : Eval("IsPercentage")%>'
                                                                Width="50px" Style="float: left; text-align: right;" />
                                                            <asp:FilteredTextBoxExtender ID="ftbIsPercent" runat="server" TargetControlID="txtIsPercent"
                                                                ValidChars="0123456789." />
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Is Applicable">
                                                        <ItemTemplate>
                                                            <asp:ImageButton runat="server" ID="imgApplicable" CommandArgument='<%#Eval("ID")%>'
                                                                Height="16" CommandName="IsAgent" AlternateText="IsAgent" ImageUrl='<%#Eval("IsApplicable").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                                ToolTip='<%#Eval("IsApplicable").ToString()=="True" ?"Applicable":"Not Applicable" %>'
                                                                OnClick="imgAPIBookingApplicable_Click" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%#Eval("SiteID")%>' />
                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ID")%>' />
                                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdateAPIBooking_Click" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
