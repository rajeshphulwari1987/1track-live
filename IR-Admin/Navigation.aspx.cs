﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data;
namespace IR_Admin
{
    public partial class NavigationPage : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        db_1TrackEntities db = new db_1TrackEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillParentCategoryDDl();

                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindNavigationListForEdit(Guid.Parse(Request["edit"]));
                }
            }

        }
        public void BindNavigationListForEdit(Guid ID)
        {
            try
            {

                var result = _Master.GetNavigationListEdit(ID);
                txtName.Text = result.Name;
                ddlParentCategory.ClearSelection();
                ddlParentCategory.Items.FindByValue(result.ParentID.ToString()).Selected = true;
                txtPageurl.Text = result.PageName;
                btnSubmit.Text = "Update";
                chkActive.Checked = Convert.ToBoolean(result.IsActive) ? true : false;
                chkIsTop.Checked = Convert.ToBoolean(result.IsTop) ? true : false;
                chkIsBottom.Checked = Convert.ToBoolean(result.IsBottom) ? true : false;

            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void FillParentCategoryDDl()
        {
            try
            {

                ddlParentCategory.DataSource = _Master.GetParentNavigationList(Guid.Parse("00000000-0000-0000-0000-000000000000"));
                ddlParentCategory.DataTextField = "Name";
                ddlParentCategory.DataValueField = "ID";
                ddlParentCategory.DataBind();
                ddlParentCategory.Items.Insert(0, new ListItem("--Category--", "00000000-0000-0000-0000-000000000000"));

            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                if (btnSubmit.Text == "Submit")
                {

                    Navigation _Navigation = new Navigation();
                    _Navigation.Name = txtName.Text;
                    _Navigation.ID = Guid.NewGuid();
                    _Navigation.IsActive = chkActive.Checked == true ? true : false;
                    _Navigation.PageName = txtPageurl.Text;
                    _Navigation.ParentID = ddlParentCategory.SelectedIndex == 0 ? Guid.Parse("00000000-0000-0000-0000-000000000000") : Guid.Parse(ddlParentCategory.SelectedItem.Value.ToString());

                    if (ddlParentCategory.SelectedIndex != 0)
                    {
                        Guid _ParentID = Guid.Parse(ddlParentCategory.SelectedItem.Value.ToString());
                        var MaxSortOrder = db.tblNavigations.Where(x => x.ParentID == _ParentID).Max(t => t.CSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.CSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.CSortOrder = 1;
                        }

                        _Navigation.PSortOrder = 0;
                    }
                    else
                    {
                        var MaxSortOrder = db.tblNavigations.Max(t => t.PSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.PSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.PSortOrder = 1;
                        }

                        _Navigation.CSortOrder = 0;
                    }

                    _Navigation.IsTop = chkIsTop.Checked == true ? true : false;
                    _Navigation.IsBottom = chkIsBottom.Checked == true ? true : false;
                    int res = _Master.AddNavigation(_Navigation);
                    if (res > 0)
                    {
                        ShowMessage(1, "You have successfully Added.");
                        txtPageurl.Text = string.Empty;
                        txtName.Text = string.Empty;

                    }
                }
                else if (btnSubmit.Text == "Update")
                {

                    Navigation _Navigation = new Navigation();
                    _Navigation.Name = txtName.Text;
                    _Navigation.ID = Guid.Parse(Request["edit"]);
                    _Navigation.IsActive = chkActive.Checked == true ? true : false;
                    _Navigation.PageName = txtPageurl.Text;
                    _Navigation.ParentID = ddlParentCategory.SelectedIndex == 0 ? Guid.Parse("00000000-0000-0000-0000-000000000000") : Guid.Parse(ddlParentCategory.SelectedItem.Value.ToString());
                    _Navigation.IsTop = chkIsTop.Checked == true ? true : false;
                    _Navigation.IsBottom = chkIsBottom.Checked == true ? true : false;

                    Guid ID = Guid.Parse(Request["edit"]);
                    var ParentId = db.tblNavigations.Where(x => x.ID == ID).SingleOrDefault();
                    if (ParentId.ParentID != null)
                    {
                        hdID.Value = ParentId.ParentID.ToString();
                    }

                    if (ddlParentCategory.SelectedIndex != 0)
                    {
                        Guid _ParentID = Guid.Parse(ddlParentCategory.SelectedItem.Value.ToString());
                        var MaxSortOrder = db.tblNavigations.Where(x => x.ParentID == _ParentID).Max(t => t.CSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.CSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.CSortOrder = 1;
                        }
                        _Navigation.PSortOrder = 0;
                    }
                    else
                    {
                        var MaxSortOrder = db.tblNavigations.Max(t => t.PSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.PSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.PSortOrder = 1;
                        }

                        _Navigation.CSortOrder = 0;
                    }

                    int res = _Master.UpdateNavigation(_Navigation);
                    if (res > 0)
                    {
                        ShowMessage(1, "You have successfully Updated.");
                        txtPageurl.Text = string.Empty;
                        txtName.Text = string.Empty;
                        SetIndex();

                        // Response.Redirect("ViewMenu.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewMenu.aspx");

        }

        protected void SetIndex()
        {
            using (db_1TrackEntities db = new db_1TrackEntities())
            {
                if (hdID.Value != "00000000-0000-0000-0000-000000000000")
                {
                    Guid Id = Guid.Parse(hdID.Value.ToString());
                    var obj = db.tblNavigations.ToList().Where(x => x.ParentID == Id).OrderBy(t => t.CSortOrder);
                    int i = 1;
                    foreach (var o in obj)
                    {
                        tblNavigation oMenu = db.tblNavigations.SingleOrDefault(t => t.ID == o.ID);
                        oMenu.CSortOrder = i;
                        db.SaveChanges();
                        i += 1;
                    }
                }
                else
                {
                    Guid Id = Guid.Parse(hdID.Value.ToString());
                    var obj = db.tblNavigations.ToList().Where(x => x.ParentID == Id).OrderBy(t => t.PSortOrder);
                    int i = 1;
                    foreach (var o in obj)
                    {
                        tblNavigation oMenu = db.tblNavigations.SingleOrDefault(t => t.ID == o.ID);
                        oMenu.PSortOrder = i;
                        db.SaveChanges();
                        i += 1;
                    }
                }
            }
        }



    }
}
