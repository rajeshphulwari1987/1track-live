﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="P2PPayment.aspx.cs" Inherits="IR_Admin.P2PPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="Styles/jquery.ui.all.css">
    <link href="Styles/BookingCart.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content">
        <div class="left-content">
            <div class="round-titles">
                Payment Type
            </div>
            <div class="booking-detail-in">
                <div class="colum-input">
                    <asp:RadioButton runat="server" ID="payment1" GroupName="pay" Checked="true" />Payment On Account &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton runat="server" ID="payment2" GroupName="pay"/>External Card Payment
                </div>
                <div class="colum-label t-right">
                    <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Payment"
                        CssClass="button float-rt" OnClick="btnCheckout_Click"/>
                </div>
            </div>
            <div class="clear">
            </div>
            <div style="float: right">
            </div>
        </div>
    </div>
</asp:Content>
