﻿using System;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using Image = System.Drawing.Image;
using System.Text.RegularExpressions;

namespace IR_Admin
{
    public partial class ImageManager : Page
    {
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        private int width, imgWd;
        private int height, imgHt;
        private int x, y = 0;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                BindImages();
                BindCategory();
            }
            ShowMessage(0, null);
        }

        private void BindCategory()
        {
            ddlCat.DataSource = _db.tblImgCategories.Where(x => x.IsActive == true).OrderBy(x => x.ID).ToList();
            ddlCat.DataTextField = "CategoryName";
            ddlCat.DataValueField = "ID";
            ddlCat.DataBind();
            ddlCat.Items.Insert(0, new ListItem("--Category--", "0"));
        }

        public void BindImages()
        {
            try
            {
                tab = "1";
                grdImg.DataSource = _Master.GetImageList().OrderBy(x => x.CategoryName).ToList();
                grdImg.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindImagesListForEdit(int id)
        {
            try
            {
                var result = _Master.GetImageListEdit(id);
                ddlCat.SelectedValue = result.CategoryID.ToString();
                txtName.Text = result.ImageName;
                imgFile.ImageUrl = result.ImagePath;
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                GetSize(Convert.ToInt32(ddlCat.SelectedValue));
                if (btnSubmit.Text == "Submit")
                {
                    var imgUpload = (FileUpload)Session["imgUpload"];
                    if (imgUpload != null)
                    {
                        var _img = new tblImage();
                        _img.CategoryID = Convert.ToInt32(ddlCat.SelectedValue);
                        _img.ImageName = txtName.Text;
                        _img.ImagePath = "CMSImages/" + Regex.Replace(imgUpload.FileName, "[^a-zA-Z0-9_.]+", "");
                        _img.ImageSize = width + "*" + height;
                        var extension = Path.GetExtension(imgUpload.PostedFile.FileName);
                        if (extension != null)
                            _img.ImageType = extension.Replace(".", "");
                        _img.IsActive = chkactive.Checked;

                        if (imgUpload.FileContent != null)
                        {
                            int res = _Master.AddImage(_img);
                            ShowMessage(1, "You have successfully Added.");
                            Session["imgUpload"] = string.Empty;
                            BindImages();
                            ddlCat.SelectedIndex = 0;
                            txtName.Text = string.Empty;
                            chkactive.Checked = false;
                            imgFile.ImageUrl = SiteUrl + "CMSImages/sml-noimg.jpg";
                            tab = "1";
                        }
                    }
                    else
                    {
                        ShowMessage(2, "File not Uploaded.");
                    }
                }
                else if (btnSubmit.Text == "Update")
                {
                    var _img = new tblImage();
                    _img.CategoryID = Convert.ToInt32(ddlCat.SelectedValue);
                    _img.ImageName = txtName.Text;
                    _img.ImageSize = width + "*" + height;

                    if (Session["imgUpload"] != null && Session["imgUpload"] != "")
                    {
                        var imgUpload = (FileUpload)Session["imgUpload"];
                        if (imgUpload != null)
                        {
                            _img.ImagePath = "CMSImages/" + Regex.Replace(imgUpload.FileName, "[^a-zA-Z0-9_.]+", "");
                            var extension = Path.GetExtension(imgUpload.PostedFile.FileName);
                            if (extension != null)
                                _img.ImageType = extension.Replace(".", "");
                        }
                    }

                    _img.IsActive = chkactive.Checked;
                    _img.ID = string.IsNullOrEmpty(hdnId.Value) ? 0 : Convert.ToInt32(hdnId.Value);

                    int res = _Master.UpdateImage(_img);
                    Session["imgUpload"] = string.Empty;
                    BindImages();
                    ShowMessage(1, "You have successfully Updated.");
                    ddlCat.SelectedIndex = 0;
                    txtName.Text = string.Empty;
                    chkactive.Checked = false;
                    imgFile.ImageUrl = SiteUrl + "CMSImages/sml-noimg.jpg";
                    hdnId.Value = string.Empty;
                    btnSubmit.Text = "Submit";
                    tab = "1";
                }
            }
            catch (Exception ex)
            {
                hdnCrop.Value = "0";
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ImageManager.aspx");
        }

        public void GetSize(int categoryID)
        {
            var db = new db_1TrackEntities();
            var size = from cat in db.tblImgCategories
                       where cat.ID == categoryID
                       select cat;

            var firstOrDefault = size.FirstOrDefault();
            if (firstOrDefault != null)
            {
                width = Convert.ToInt32(firstOrDefault.Width.ToString());
                height = Convert.ToInt32(firstOrDefault.Height.ToString());
            }
        }

        protected void btnUp_Click(object sender, EventArgs e)
        {
            if (imgUpload.HasFile)
            {
                try
                {
                    tab = "2";
                    if (ddlCat.SelectedValue != "0")
                    {
                        ShowMessage(0, "");
                        GetSize(Convert.ToInt32(ddlCat.SelectedValue));

                        string imgType = Path.GetExtension(imgUpload.FileName);
                        if (imgType == ".jpg" || imgType == ".jpeg" || imgType == ".png" || imgType == ".bmp")
                        {
                            using (var myImage = Image.FromStream(imgUpload.PostedFile.InputStream))
                            {
                                imgWd = myImage.Width;
                                imgHt = myImage.Height;
                            }

                            if (imgWd < width || imgHt < height)
                                ShowMessage(1, "Image Size should be equal or more than " + width + "*" + height);
                            else
                            {
                                string filename = Path.GetFileName(imgUpload.FileName);
                                var cropArea = new Rectangle(x, y, width, height);
                                var bmpImage = new Bitmap(imgUpload.PostedFile.InputStream);

                                var format = bmpImage.PixelFormat;
                                var bmpCrop = bmpImage.Clone(cropArea, format);

                                if (filename != null) filename = Regex.Replace(filename, "[^a-zA-Z0-9_.]+", "");
                                bmpCrop.Save(Server.MapPath("~/CMSImages/") + filename);
                                Session["imgUpload"] = imgUpload;
                                imgFile.ImageUrl = SiteUrl + "CMSImages/" + Regex.Replace(imgUpload.FileName, "[^a-zA-Z0-9_.]+", "");
                            }
                        }
                        else
                        {
                            ShowMessage(2, "Upload only image file.");
                        }
                    }
                    else
                    {
                        ShowMessage(1, "Select Category.");
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
            }
            else
            {
                tab = "2";
                ShowMessage(2, "Upload File");
            }
        }

        protected void ddlCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            tab = "2";
            ShowMessage(0, "");
            GetSize(Convert.ToInt32(ddlCat.SelectedValue));
            lblMsg.Text = "Image Size is " + width + "*" + height + ". Images larger than this will automatically resize, images smaller than this will not be uploaded.";
            DivWarning.Style.Add("display", "block");
        }

        protected void grdImg_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument.ToString());
            if (e.CommandName == "Modify")
            {
                btnSubmit.Text = "Update";
                hdnId.Value = id.ToString();
                hdnCrop.Value = "1";
                BindImagesListForEdit(Convert.ToInt32(id));
                tab = "2";
            }
            else if (e.CommandName == "Remove")
            {
                tab = "1";
                _Master.DeleteImage(id);
                BindImages();
            }
            else if (e.CommandName == "ActiveInActive")
            {
                tab = "1";
                _Master.ActiveInactiveImage(id);
                BindImages();
            }
        }
    }
}