﻿using System;
using System.Web.UI;
using System.Linq;
using Business;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web;
using System.Configuration;

public partial class RailPasses : Page
{
    readonly private ManageProduct _master = new ManageProduct();
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static bool ErrorIn;
    public Guid siteId;
    public string script = "<script></script>";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["cid"] == null)
            {
                BindCategory(siteId);
                rptCat.Visible = true;
            }
            else
            {
                ulPasses.Visible = true;
                rptCat.Visible = false;
                BindRailPasses(siteId, Guid.Parse(Request.QueryString["cid"]));
            }
            QubitOperationLoad();
            GetRailPassData();
        }
    }

    public void GetRailPassData()
    {
        var railpass1 = _db.tblRailPassSec1.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass1 != null)
        {
            imgRail1.Src = adminSiteUrl + railpass1.Imagepath;
            lblRailTitle1.Text = railpass1.Name;
            lblRailDesp1.Text = (railpass1.Description.Length > 50) ? Server.HtmlDecode(railpass1.Description).Substring(0, 50) + "..." : Server.HtmlDecode(railpass1.Description);
        }
        var railpass2 = _db.tblRailPassSec2.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass2 != null)
        {
            imgRail2.Src = adminSiteUrl + railpass2.Imagepath;
            lblRailTitle2.Text = railpass2.Name;
            lblRailDesp2.Text = (railpass2.Description.Length > 50) ? Server.HtmlDecode(railpass2.Description).Substring(0, 50) + "..." : Server.HtmlDecode(railpass2.Description);
        }
        var railpass3 = _db.tblRailPassSec3.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass3 != null)
        {
            imgRail3.Src = adminSiteUrl + railpass3.Imagepath;
            lblRailTitle3.Text = railpass3.Name;
            lblRailDesp3.Text = (railpass3.Description.Length > 50) ? Server.HtmlDecode(railpass3.Description).Substring(0, 50) + "..." : Server.HtmlDecode(railpass3.Description);
        }
    }

    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindCategory(Guid siteId)
    {
        var _product = new ManageProduct();
        List<ProductCategory> result = new List<ProductCategory>();
        if (Request.QueryString["category"] != null)
        {
            if (Request.QueryString["category"] == "FOC-AD75")
                result = _product.GetAssociateCategoryListRailPass(3, siteId).Where(x => x.IsActive && x.IsAD75).ToList();
        }
        else
            result = _product.GetAssociateCategoryListRailPass(3, siteId).Where(x => x.IsActive && !x.IsAD75).ToList();

        var list = new List<Categories>();
        foreach (var item in result)
        {
            if (_db.tblProductCategoriesLookUps.Any(x => x.CategoryID == item.ID))
                list.Add(new Categories
                {
                    ID = item.ID,
                    Name = item.Name
                });
        }

        rptCat.DataSource = list.OrderBy(x => x.Name);
        rptCat.DataBind();
    }

    public void BindRailPasses(Guid siteId, Guid catId)
    {
        ErrorIn = false;
        var result = _master.GetProductListByCatID(siteId, catId);
        rptPasses.DataSource = result;
        rptPasses.DataBind();
        rptCat.Visible = false;
        ScriptManager.RegisterStartupScript(Page, GetType(), "ie", "loadIE()", true);
    }

    protected void rptCat_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "RailPass")
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), " Validationxx", " Validationxx()", true);
            ulPasses.Visible = true;
            var catId = Guid.Parse(e.CommandArgument.ToString());
            BindRailPasses(siteId, catId);
        }
    }

    protected void rptCat_OnItemCreated(object sender, RepeaterItemEventArgs args)
    {
        var lnkEdit = (LinkButton)args.Item.FindControl("lnkCat");
        if (lnkEdit != null)
        {
            ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(lnkEdit);
            ShowP2PWidget(siteId);
        }
    }

    void ShowP2PWidget(Guid siteID)
    {
        var enableP2P = _oWebsitePage.EnableP2P(siteID);
        if (!enableP2P)
            //ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.ticketbooking').hide();</script>", false);
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);
    }
    protected void rptPasses_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (ErrorIn == false)
        {
            ErrorIn = true;
            if (e.Item.DataItem == null)
            {
                var lblerrmsg = e.Item.FindControl("lblerrmsg") as Label;
                var lnkGotoback = e.Item.FindControl("lnkGotoback") as LinkButton;
                lblerrmsg.Visible = true;
                lnkGotoback.Visible = true;
            }
        }
    }
    protected void rptPasses_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Redirect")
            Response.Redirect("Rail-Passes");
    }

    public class Categories
    {
        public Guid ID { get; set; }
        public String Name { get; set; }
    }
}