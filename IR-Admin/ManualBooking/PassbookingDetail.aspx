﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="PassbookingDetail.aspx.cs" Inherits="IR_Admin.ManualBooking.PassbookingDetail" %>

<%@ Register TagPrefix="uc" TagName="RailPassDetail" Src="../usercontrol/ucRailPassDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc:RailPassDetail ID="ucRailPassDetail" runat="server" />
</asp:Content>
