﻿#region Using
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Business;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Collections.Generic;

#endregion

namespace IR_Admin.Orders
{
    public partial class P2POrderView : Page
    {
        public string EvOtherCharges = "0";
        public string PaymentTaken = "No";
        public string PaymentTakenDate = "";
        public decimal BookingFee = 0;
        public decimal ShippingAmount = 0;
        public decimal Commission = 0;
        public decimal NetPrice = 0;
        public decimal GrossPrice = 0;
        public decimal TicketProtection = 0;
        public decimal GrandTotal = 0;
        public decimal Discount = 0;
        public string CurrCode = "";
        public string AgentUsername = "";
        public int OrderId = 0;
        private string htmfile = string.Empty;
        public string billingEmailAddress;
        public string shippingEmailAddress;
        public static string unavailableDates1 = "";
        public bool IsVisibleAttachmentDeleteBtn = false;
        readonly private ManageOrder _masterOrder = new ManageOrder();

        readonly db_1TrackEntities _db = new db_1TrackEntities();
        private readonly Masters _masterPage = new Masters();
        readonly private ManageOrder _master = new ManageOrder();
        private readonly ManageBooking Objbooking = new ManageBooking();

        public string currency = "$";
        public string currencyCode = "USD";
        public Guid currencyID = new Guid();
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
        private string HtmlFileSitelog = string.Empty;
        public string Name = "";
        public string logo = "";
        public string SiteHeaderColor = string.Empty;
        public bool isEvolviBooking = false;
        public bool isNTVBooking = false;
        public bool isBene = false;
        public string EvolviTandC = "";
        public bool isTI = false;

        #region [ Page InIt must write on every page of CMS ]
        public Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            //_siteId = Guid.Parse(selectedValue);
            BindCurrency();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //_siteId = Master.SiteID;
            if (!string.IsNullOrEmpty(Request["id"]))
                OrderId = Convert.ToInt32(Request["id"]);
            var data = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                OrderAttachments.Visible = data.IsOrderAttachmentAllow;
                IsVisibleAttachmentDeleteBtn = data.RoleName.ToLower().Trim() == "admin";
            }
            GetOrdersForEdit(OrderId);
            BindOrderNotes(OrderId);
            if ((Request["notesid"] != null) && (Request["notesid"] != ""))
            {
                EditOrderNotes(Guid.Parse(Request["notesid"]));
                btnAddNotes.Text = "Update";
            }
            if (!IsPostBack && OrderId > 0)
            {
                BindAttachments();
                BindCurrency();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal", "LoadCal();", true);
                var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
                BindCountry();
                unavailableDates1 = "[";
                if (siteDDates.Count() > 0)
                {
                    foreach (var it in siteDDates)
                    {
                        unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
                }
                unavailableDates1 += "]";
                //for Children
                for (int i = 10; i >= 0; i--)
                {
                    ddlChildren.Items.Insert(0, new ListItem(i.ToString(CultureInfo.InvariantCulture), i.ToString(CultureInfo.InvariantCulture)));
                }
                //for Adult
                for (int j = 10; j >= 1; j--)
                {
                    ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                }
                //for Youth
                for (int j = 10; j >= 0; j--)
                {
                    ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                }
                //for Senior
                for (int j = 10; j >= 0; j--)
                {
                    ddlSeniors.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                }

                for (int k = 0; k <= 59; k++)
                {
                    ddlRetSec.Items.Add((k < 10 ? "0" : "") + k.ToString());
                    ddlDepSec.Items.Add((k < 10 ? "0" : "") + k.ToString());
                }
            }
        }

        public void BindCurrency()
        {
            //_siteId = Master.SiteID;
            currencyID = Objbooking.GetCurrencyIdBySiteId(_siteId).Value;
            CurrCode = new FrontEndManagePass().GetCurrencyShortCode(currencyID);
        }

        void BindCountry()
        {
            var data = new ManageTrainDetails().GetCountryDetail();
            ddlbcountry.DataSource = ddlCountry.DataSource = data;
            ddlbcountry.DataValueField = ddlCountry.DataValueField = "CountryName";
            ddlbcountry.DataTextField = ddlCountry.DataTextField = "CountryName";
            ddlbcountry.DataBind();
            ddlbcountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            ddlTrCountry.DataSource = data;
            ddlTrCountry.DataValueField = "CountryID";
            ddlTrCountry.DataTextField = "CountryName";
            ddlTrCountry.DataBind();
            ddlTrCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            ddlOrderStatus.DataSource = _master.GetStatusList().OrderBy(a => a.Name).ToList();
            ddlOrderStatus.DataValueField = "ID";
            ddlOrderStatus.DataTextField = "Name";
            ddlOrderStatus.DataBind();
            ddlOrderStatus.Items.Insert(0, new ListItem("--Select Order--", "0"));

            var lstShip = _db.tblShippings.Where(x => x.SiteID == _siteId && x.IsActive).ToList();
            if (lstShip.Any())
            {
                ddlShippindMethod.DataSource = lstShip;
                ddlShippindMethod.DataValueField = "ID";
                ddlShippindMethod.DataTextField = "ShippingName";
                ddlShippindMethod.DataBind();
                ddlShippindMethod.Items.Insert(0, new ListItem("--Select Shipping--", "0"));
            }
        }

        public void GetOrdersForEdit(int ordId)
        {
            var oP = _master.Getp2psale(ordId);
            if (oP != null)
            {
                var lst = Objbooking.GetP2PSaleDetail(ordId).OrderBy(t => t.ShortOrder).ToList();
                if (lst.Count() > 0)
                {
                    var agentid = Guid.Parse(lst.FirstOrDefault().AgentID.ToString());
                    var user = _db.tblAdminUsers.FirstOrDefault(x => x.ID == agentid);
                    if (user != null)
                        AgentUsername = user.UserName;

                    currency = _master.GetCurrcyBySiteId(Guid.Parse(lst.FirstOrDefault().SiteID.ToString()));
                    _siteId = Guid.Parse(lst.FirstOrDefault().SiteID.ToString());
                    //BookingFee = lst.Sum(t => t.BookingFee);
                    var tblOrder = _db.tblOrders.FirstOrDefault(x => x.OrderID == ordId);
                    if (tblOrder != null)
                    {
                        BookingFee = Convert.ToDecimal(tblOrder.BookingFee);
                        ShippingAmount = Convert.ToDecimal(tblOrder.ShippingAmount);
                    }

                    Commission = lst.Sum(t => t.CommissionFee);
                    var NetPriceValue = (lst.Sum(t => t.Price ?? 0) - Commission);
                    NetPrice = NetPriceValue;
                    GrossPrice = lst.Sum(t => t.Price ?? 0);
                    TicketProtection = lst.Sum(t => t.TicketProtection ?? 0);

                    //decimal ShippingAmount = lst.Sum(t => t.ShippingAmount ?? 0);
                    Discount = Convert.ToDecimal(_master.GetOrderDiscountByOrderId(ordId));
                    GrandTotal = (GrossPrice + TicketProtection + BookingFee + ShippingAmount) - Discount;

                    if (lst.FirstOrDefault() != null)
                    {
                        var getP2PSaleData = lst.FirstOrDefault();
                        if (getP2PSaleData != null) Session["OrderStatusId"] = getP2PSaleData.Status;
                        if (getP2PSaleData != null && (getP2PSaleData.Status == 3 || getP2PSaleData.Status == 7
                            || getP2PSaleData.Status == 9 || getP2PSaleData.Status == 19))
                        {
                            PaymentTaken = "Yes";
                            spnPayment.Attributes.Add("class", "clsGreen");
                            PaymentTakenDate = Convert.ToDateTime(getP2PSaleData.PaymentDate).ToString("MMM dd,yyyy HH:mm");
                            btnTakePayment.Visible = false;
                            trpaymentdt.Visible = true;
                        }
                    }
                    var ordshipMethod = _db.tblOrders.FirstOrDefault(x => x.OrderID == ordId);
                    if (ordshipMethod != null)
                    {
                        var shipID = _db.tblShippings.FirstOrDefault(x => x.ShippingName == ordshipMethod.ShippingMethod && x.SiteID == _siteId);
                        if (shipID != null)
                            Session["ShippingMethodId"] = shipID.ID;
                    }

                    RptP2P.DataSource = lst.Take(1).ToList();
                    RptP2P.DataBind();
                    lblAgentRef.Text = lst.FirstOrDefault().AgentReferenceNo;

                    var datalist = lst.Select(t => new
                    {
                        ID = t.ID,
                        TrainNo = t.TrainNo,
                        From = t.From,
                        To = t.To,
                        DateTimeDepature = t.DateTimeDepature,
                        DateTimeArrival = t.DateTimeArrival,
                        Passenger = t.Passenger,
                        Symbol = t.Symbol,
                        NetPrice = t.NetPrice,
                        DepartureTime = t.DepartureTime,
                        ArrivalTime = t.ArrivalTime,
                        Class = t.Class,
                        FareName = t.FareName,
                        ReservationCode = t.ReservationCode,
                        PinNumber = t.PinNumber,
                        PassP2PSaleID = t.PassP2PSaleID,
                        DeliveryOption = t.DeliveryOption,
                        IsNotRefundedFull = !_master.IsHundradPercentRefund(t.ID, Convert.ToInt32(Request["id"])),
                        PdfURL = t.PdfURL,
                        IsBENE = !string.IsNullOrEmpty(t.ReservationCode) ? (t.ReservationCode.Length == 7 ? true : false) : false,
                        Terms = Server.HtmlDecode(t.terms)
                    }).ToList();

                    rptJourney.DataSource = datalist;
                    rptJourney.DataBind();

                    grdTraveller.DataSource = lst;
                    grdTraveller.DataBind();

                    var billing = _master.GetBillingInfo(ordId);
                    if (billing.Any())
                    {
                        rptBilling.DataSource = rptShipping.DataSource = billing;
                        rptBilling.DataBind();
                        rptShipping.DataBind();
                        btnSendEmail.Visible = true;
                    }
                }
            }
        }

        #region ControlEvents
        /*Order status*/
        protected void lnkEditStatus_Click(object sender, EventArgs e)
        {
            ddlOrderStatus.SelectedValue = Session["OrderStatusId"].ToString();
            mpeOrderStatus.Show();
        }
        protected void btnOrderStatus_Click(object sender, EventArgs e)
        {
            Objbooking.UpdateOrderStatus(Convert.ToInt32(ddlOrderStatus.SelectedValue), Convert.ToInt64(OrderId));
            Response.Redirect(Request.UrlReferrer.ToString());
        }
        /*Shipping method*/
        protected void lnkEditShipMethod_Click(object sender, EventArgs e)
        {
            ddlShippindMethod.SelectedValue = Session["ShippingMethodId"].ToString();
            mupShipMethod.Show();
        }
        protected void btnShipMethod_Click(object sender, EventArgs e)
        {
            if (ddlShippindMethod.SelectedValue != "0")
            {
                var shipID = Guid.Parse(ddlShippindMethod.SelectedValue);
                var shippingDetails = _db.tblShippings.FirstOrDefault(x => x.ID == shipID);
                if (shippingDetails != null)
                {
                    var shipMehod = shippingDetails.ShippingName;
                    var shipDesc = shippingDetails.Description;
                    var shipAmount = shippingDetails.Price;
                    Objbooking.UpdateShippingMethodForManualBooking(shipMehod, shipDesc, shipAmount, Convert.ToInt64(OrderId));
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
            }
        }
        protected void rptShipping_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            tblOrderBillingAddress objBillAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(OrderId));
            if (objBillAddress != null)
            {
                txtAdd.Text = objBillAddress.Address1Shpg;
                txtAdd2.Text = objBillAddress.Address2Shpg;
                txtCity.Text = objBillAddress.CityShpg;
                txtEmail.Text = objBillAddress.EmailAddressShpg;
                txtFirst.Text = objBillAddress.FirstNameShpg;
                txtLast.Text = objBillAddress.LastNameShpg;
                txtState.Text = objBillAddress.StateShpg;
                txtZip.Text = objBillAddress.PostcodeShpg;
                ddlMr.SelectedValue = objBillAddress.TitleShpg != null ? objBillAddress.TitleShpg.Trim() : null;
                ddlCountry.SelectedValue = objBillAddress.CountryShpg != null ? objBillAddress.CountryShpg.Trim() : null;
                txtPhone.Text = objBillAddress.PhoneShpg;
            }
            mdpupShipping.Show();
        }
        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            tblOrderBillingAddress objBillAddress = new tblOrderBillingAddress();
            objBillAddress.Address1Shpg = txtAdd.Text;
            objBillAddress.Address2Shpg = txtAdd2.Text;
            objBillAddress.CityShpg = txtCity.Text;
            objBillAddress.EmailAddressShpg = txtEmail.Text;
            objBillAddress.FirstNameShpg = txtFirst.Text;
            objBillAddress.LastNameShpg = txtLast.Text;
            objBillAddress.StateShpg = txtState.Text;
            objBillAddress.PostcodeShpg = txtZip.Text;
            objBillAddress.TitleShpg = ddlMr.SelectedValue;
            objBillAddress.CountryShpg = ddlCountry.SelectedValue;
            objBillAddress.PhoneShpg = txtPhone.Text;
            Objbooking.UpdateShippingAddress(Convert.ToInt64(OrderId), objBillAddress);
            Response.Redirect(Request.UrlReferrer.ToString());
        }
        protected void btnTravellerUpdate_Click1(object sender, EventArgs e)
        {
            tblOrderTraveller obj = new tblOrderTraveller();
            obj.Country = Guid.Parse(ddlTrCountry.SelectedValue);
            obj.FirstName = ddlTrFirstName.Text;
            obj.LastName = txtTrLName.Text;
            obj.LeadPassenger = true;
            obj.Title = ddlTrTitle.SelectedValue;
            Objbooking.UpdateTraveller(Guid.Parse(Session["TrvID"].ToString()), obj);
            Response.Redirect(Request.UrlReferrer.ToString());
        }
        protected void grdTraveller_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid id = Guid.Parse(e.CommandArgument.ToString());
            Session["TrvID"] = id;
            tblOrderTraveller obj = Objbooking.GetTraveller(id);
            if (obj != null)
            {
                ddlTrCountry.SelectedValue = obj.Country.ToString();
                ddlTrFirstName.Text = obj.FirstName;
                txtTrLName.Text = obj.LastName;
            }
            GetOrdersForEdit(OrderId);
            mupTraveller.Show();
        }

        protected void rptBilling_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            tblOrderBillingAddress objBillAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(OrderId));
            if (objBillAddress != null)
            {
                ddlbtitle.SelectedValue = objBillAddress.Title;
                txtbfname.Text = objBillAddress.FirstName;
                txtblname.Text = objBillAddress.LastName;
                txtbemail.Text = objBillAddress.EmailAddress;
                txtbphone.Text = objBillAddress.Phone;
                txtbadd.Text = objBillAddress.Address1;
                txtbadd2.Text = objBillAddress.Address2;
                txtbcity.Text = objBillAddress.City;
                txtbState.Text = objBillAddress.State;
                txtbzip.Text = objBillAddress.Postcode;
                ddlbcountry.SelectedValue = objBillAddress.Country;
            }
            mdpupBilling.Show();
        }
        protected void btnbillingt_Click1(object sender, EventArgs e)
        {
            tblOrderBillingAddress objBillAddress = new tblOrderBillingAddress();
            objBillAddress.Address1 = txtbadd.Text;
            objBillAddress.Address2 = txtbadd2.Text;
            objBillAddress.City = txtbcity.Text;
            objBillAddress.EmailAddress = txtbemail.Text;
            objBillAddress.FirstName = txtbfname.Text;
            objBillAddress.LastName = txtblname.Text;
            objBillAddress.State = txtbState.Text;
            objBillAddress.Postcode = txtbzip.Text;
            objBillAddress.Title = ddlbtitle.SelectedValue;
            objBillAddress.Country = ddlbcountry.SelectedValue;
            objBillAddress.Phone = txtbphone.Text;
            Objbooking.UpdateBillingAddress(Convert.ToInt64(OrderId), objBillAddress);
            Response.Redirect(Request.UrlReferrer.ToString());
        }
        protected void rptJourney_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Guid ID = Guid.Parse(e.CommandArgument.ToString());
            var data = Objbooking.GetP2PSaleDataById(ID);
            if (data != null)
            {
                hdnp2pjourneyID.Value = ID.ToString();
                txtFrom.Text = data.From;
                txtTo.Text = data.To;
                txtDepartureDate.Text = data.DateTimeDepature.Value.ToString("dd/MMM/yyyy");
                txtReturnDate.Text = data.DateTimeArrival.Value.ToString("dd/MMM/yyyy");
                string[] t1 = data.DepartureTime.Split(':');
                string[] t2 = data.ArrivalTime.Split(':');
                ddldepTime.SelectedValue = t1[0];
                ddlDepSec.SelectedValue = t1[1];
                ddlReturnTime.SelectedValue = t2[0];
                ddlRetSec.SelectedValue = t2[1];
                txtTrainNumber.Text = data.TrainNo;
                ddlClass.SelectedValue = data.Class;
                ddlSeniors.SelectedValue = data.Senior;
                ddlAdult.SelectedValue = data.Adult;
                ddlChildren.SelectedValue = data.Children;
                ddlYouth.SelectedValue = data.Youth;
                txtTicketType.Text = data.SeviceName;
                //txtprice.Text = Convert.ToString(Convert.ToInt64(data.Price));
            }
            mpeP2Pjourney.Show();
        }
        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            var objBruc = new tblP2PSale
            {
                ID = Guid.Parse(hdnp2pjourneyID.Value),
                From = txtFrom.Text.Trim(),
                To = txtTo.Text.Trim(),
                DateTimeDepature = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null),
                DepartureTime = ddldepTime.SelectedValue + ":" + ddlDepSec.SelectedValue,
                DateTimeArrival = DateTime.ParseExact(txtReturnDate.Text, "dd/MMM/yyyy", null),
                ArrivalTime = ddlReturnTime.SelectedValue + ":" + ddlRetSec.SelectedValue,
                TrainNo = txtTrainNumber.Text,
                Senior = ddlSeniors.SelectedValue,
                Adult = ddlAdult.SelectedValue,
                Children = ddlChildren.SelectedValue,
                Youth = ddlYouth.SelectedValue,
                Passenger =
                    ddlAdult.SelectedValue + " Adult " + ddlChildren.SelectedValue + " Children " +
                    ddlSeniors.SelectedValue + " Seniors " + ddlYouth.SelectedValue + " Youth",
                SeviceName = txtTicketType.Text,
                //NetPrice = Convert.ToDecimal(txtprice.Text),
                //Price = Convert.ToDecimal(txtprice.Text),
                Class = ddlClass.SelectedValue,
            };
            Objbooking.GetP2PSaleDataById(objBruc);
            Response.Redirect(Request.UrlReferrer.ToString());
        }
        #endregion

        #region AgentNotes
        void BindOrderNotes(int ordId)
        {
            var oNotes = _master.GetOrderNotesList(ordId);
            if (oNotes != null)
            {
                grdOrderNotes.DataSource = oNotes.OrderBy(x => x.CreatedOn);
                grdOrderNotes.DataBind();
            }
        }

        void EditOrderNotes(Guid id)
        {
            var oP = _master.GetOrderNotesById(id);
            if (oP != null)
                txtNotes.InnerHtml = oP.Notes;
        }

        protected void btnAddNotes_Click(object sender, EventArgs e)
        {
            AddEditOrderNotes();
            Response.Redirect("P2POrderView.aspx?id=" + Convert.ToInt32(Request["id"]));
        }

        public void AddEditOrderNotes()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNotes.InnerHtml))
                {
                    var id = (Request["notesid"] == "" || Request["notesid"] == null) ? new Guid() : Guid.Parse(Request["notesid"]);
                    _master.AddEditOrderNotes(new tblOrderNote
                    {
                        ID = id,
                        OrderID = Convert.ToInt32(Request["id"]),
                        UserName = AdminuserInfo.Username,
                        Notes = txtNotes.InnerHtml,
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now
                    });
                    txtNotes.InnerHtml = string.Empty;
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Please Enter notes.')", true);
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        #endregion

        #region Sendmail
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            SendMail();
            GetOrdersForEdit(Convert.ToInt32(Request["id"]));
            mupTraveller.Hide();
            ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
        }

        public void GetCurrencyCode()
        {
            var cID = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
            if (cID != null)
            {
                currencyID = Guid.Parse(cID.DefaultCurrencyID.ToString());
                currency = oManageClass.GetCurrency(currencyID);
                currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
            }
        }

        public string GetBillingAddressPhoneNo(long OrderID)
        {
            string BillingPhoneNo = string.Empty;
            var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            if (result != null)
            {
                BillingPhoneNo += !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
                BillingPhoneNo += result.IsVisibleEmailAddress ? "<br/>" + result.EmailAddress : "";
            }
            return BillingPhoneNo;
        }

        public void GetReceiptLoga()
        {
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    logo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    logo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    logo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        bool isBritRailPromoPass(long orderid)
        {
            try
            {
                foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
                {
                    bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                    if (isPromo)
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public void GetSiteType(long OrderID)
        {
            var apiTypeList = new ManageBooking().GetAllApiType(OrderID);
            if (apiTypeList != null)
            {
                isEvolviBooking = apiTypeList.isEvolvi;
                isNTVBooking = apiTypeList.isNTV;
                isBene = apiTypeList.isBene;
                isTI = apiTypeList.isTI;
            }
        }

        public string EvolviBookingRefNO(long OrderID)
        {
            var result = _db.tblEvolviOtherChargesLookUps.FirstOrDefault(x => x.OrderId == OrderID);
            if (result != null)
            {
                return !string.IsNullOrEmpty(result.BookingRef) ? result.BookingRef : "";
            }
            return "";
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                _siteId = Master.SiteID;
                GetCurrencyCode();
                bool isDiscountSite = _masterOrder.GetOrderDiscountVisibleBySiteId(_siteId);
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    var orderID = Convert.ToInt32(Request["id"]);
                    string Subject = "Order Confirmation #" + orderID;

                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(_siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(_siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml.ToString();

                    string UserName, EmailAddress, DeliveryAddress, BillingAddress, OrderDate, OrderNumber, Total, ShippingAmount, GrandTotal, NetTotal, Discount, AdminFee;
                    decimal BookingFee = 0; UserName = Discount = NetTotal = EmailAddress = DeliveryAddress = BillingAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = AdminFee = "";
                    string OtherCharges = string.Empty;
                    string BookingRefNO = string.Empty;
                    string PassProtection = "0.00";
                    string PassProtectionHtml = "";
                    string DiscountHtml = "";
                    bool isBeneDeliveryByMail = false;
                    bool isAgentSite = false;

                    var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Request.Params["id"])).OrderBy(a => a.OrderIdentity).ToList();
                    var lst1 = from a in lst
                               select
                                   new
                                   {
                                       Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                       ProductDesc =
                               (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                                " " + a.LastName),
                                       TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                   };
                    if (lst.Count > 0)
                    {
                        EmailAddress = lst.FirstOrDefault().EmailAddress;
                        UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                        DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                        lst.FirstOrDefault().DCountry + "<br>" +
                        lst.FirstOrDefault().Postcode + "<br>" +
                        GetBillingAddressPhoneNo(Convert.ToInt64(Request.Params["id"]));

                        OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                        Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                        ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                        BookingFee = lst.FirstOrDefault().BookingFee != null ? (decimal)lst.FirstOrDefault().BookingFee : 0;
                        Discount = _masterOrder.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));

                        AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(orderID).ToString();
                        NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                            (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                            (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                            Convert.ToDecimal(AdminFee) + Convert.ToDecimal(EvOtherCharges)).ToString();
                        GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                        var billAddress = Objbooking.GetBillingShippingAddress(Convert.ToInt64(orderID));
                        if (billAddress != null)
                        {
                            BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                        }
                    }
                    // to address
                    string ToEmail = EmailAddress;
                    GetReceiptLoga();
                    body = body.Replace("##headerstyle##", SiteHeaderColor);
                    body = body.Replace("##sitelogo##", logo);
                    body = body.Replace("##OrderNumber##", OrderNumber);
                    body = body.Replace("##UserName##", UserName.Trim());
                    body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                    body = body.Replace("##OrderDate##", OrderDate.Trim());
                    body = body.Replace("##Items##", currency + " " + Total.Trim());
                    body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");

                    var tblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderID);
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                    isAgentSite = objsite.IsAgent.Value;

                    GetSiteType(orderID);
                    if (isEvolviBooking)
                        EvolviTandC = objsite.SiteURL + "uk-ticket-collection";

                    string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + AdminFee.Trim() + "</td></tr>";
                    if (AdminFee == "0.00")
                        AdminFeeRow = string.Empty;
                    if (isDiscountSite)
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        if (Discount == "0.00")
                            body = body.Replace("##Discount##", "");
                        else
                        {
                            DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + Discount.Trim() + "</td></tr>";
                            body = body.Replace("##Discount##", DiscountHtml);
                        }
                    }
                    else
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        body = body.Replace("##Discount##", "");
                    }

                    if (isEvolviBooking)
                    {
                        OtherCharges = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Other Charges:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + EvOtherCharges + "</td></tr>";
                        BookingRefNO = "<tr><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>Booking Ref: </strong></td><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>" + EvolviBookingRefNO(orderID) + "</td></tr>";
                    }

                    body = body.Replace("##BookingRef##", BookingRefNO);
                    body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##DeliveryFee##", currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##BookingFee##", currency + " " + BookingFee);
                    body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());

                    if (tblord != null)
                    {
                        Session["ShipMethod"] = tblord.ShippingMethod;
                        Session["ShipDesc"] = tblord.ShippingDescription;
                        Session["CollectStation"] = tblord.CollectionStation;
                    }

                    var P2PSaleList = new ManageBooking().GetP2PSaleListByOrderID(orderID);
                    if (isEvolviBooking)
                        body = body.Replace("##ShippingMethod##", "Ticket on collection");
                    else if (isBene)
                    {
                        if (P2PSaleList != null && !string.IsNullOrEmpty(P2PSaleList.FirstOrDefault().DeliveryOption))
                        {
                            string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                            if (P2PSaleList.Any(x => x.DeliveryOption.ToLower() == "delivery by mail"))
                            {
                                body = body.Replace("##ShippingMethod##", !string.IsNullOrEmpty(shippingdesc) ? shippingdesc : "Delivery by mail");
                                isBeneDeliveryByMail = true;
                            }
                            else
                                body = body.Replace("##ShippingMethod##", P2PSaleList != null ? P2PSaleList.FirstOrDefault().DeliveryOption : "");
                        }
                    }
                    else
                    {
                        if (Session["ShipMethod"] != null)
                        {
                            string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                            body = body.Replace("##ShippingMethod##", shippingdesc);
                        }
                        else
                            body = body.Replace("##ShippingMethod##", "");
                    }
                    body = body.Replace("#Blanck#", "&nbsp;");


                    var lstC = new ManageBooking().GetAllCartData(orderID).OrderBy(a => a.OrderIdentity).ToList();
                    var lstNew = (from a in lstC
                                  select new
                                  {
                                      a,
                                      Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                      ProductDesc = a.ProductType == "P2P"
                                      ? (Objbooking.getP2PDetailsForEmail(a.PassSaleID, Objbooking.GetPrintResponse((Guid)a.PassSaleID), a.ProductType, isEvolviBooking, isAgentSite, EvolviTandC))
                                      : (Objbooking.getPassDetailsForEmail(a.PassSaleID, Objbooking.GetPrintResponse((Guid)a.PassSaleID), a.ProductType, EvolviTandC)),
                                      TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                      CommissionFee = a.CommissionFee,
                                      Terms = a.terms
                                  }).ToList();

                    string strProductDesc = "";
                    int i = 1;
                    if (lstNew.Count() > 0)
                    {
                        lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                        body = body.Replace("##HeaderText##", "Delivery address");
                        body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                        body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                        foreach (var x in lstNew)
                        {
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                            if (isAgentSite)
                                strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", "Net Amount :" + currency + " " + (x.Price + x.TktPrtCharge).ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));
                            else
                                strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", "").Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));

                            strProductDesc += (HttpContext.Current.Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Session["CollectStation"] == null ? "" : HttpContext.Current.Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");
                            if (P2PSaleList != null && P2PSaleList.Count > 0 && P2PSaleList.Any(z => z.ApiName.ToUpper() == "ITALIA"))
                            {
                                if (string.IsNullOrEmpty(P2PSaleList.FirstOrDefault(z => z.ID == x.a.PassSaleID).PdfURL))
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Important:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>This PNR is issued as a ticket on depart and must be printed from one of the self-service ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine.</td></tr>";
                            }
                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            if (x.Terms != null)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Server.HtmlDecode(x.Terms) + "</td></tr>";
                            if (isEvolviBooking)
                            {
                                var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == x.a.PassSaleID.Value);
                                if (P2PData != null)
                                {
                                    if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                    {
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>";
                                        foreach (var item in P2PData.EvSleeper.Split('@'))
                                        {
                                            string ss = item.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                            strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                        }
                                        strProductDesc += "</td></tr>";
                                    }
                                }
                            }
                            strProductDesc += "</table></td></tr></table>";
                            i++;
                        }
                    }
                    PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + PassProtection.Trim() + "</td></tr>";
                    body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                    body = body.Replace("##OrderDetails##", strProductDesc);
                    body = body.Replace("##BillingAddress##", BillingAddress);

                    var getDeliveryType = Objbooking.getDeliveryOption(orderID);
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth '><tr><td style='padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;'colspan='2'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</td></tr></table>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                    body = body.Replace("../images/", SiteName + "images/");

                    var billing = _masterOrder.GetBillingInfo(orderID);
                    if (billing.Any())
                    {
                        var firstOrDefault = billing.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            billingEmailAddress = firstOrDefault.EmailAddress;
                            shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                        }
                    }

                    /*Get smtp details*/
                    var result = _masterPage.GetEmailSettingDetail(_siteId);
                    if (result != null)
                    {
                        var fromAddres = new MailAddress(result.Email, result.Email);
                        smtpClient.Host = result.SmtpHost;
                        smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                        smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                        message.From = fromAddres;
                        message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        smtpClient.Send(message);
                        retVal = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Failed to send E-mail confimation!");
            }
            return retVal;
        }

        #endregion

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void RptP2P_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    var td = (HtmlTableCell)e.Item.FindControl("tdPayment");
                    if (PaymentTaken != null)
                    {
                        if (PaymentTaken == "Yes")
                            td.Attributes.Add("class", "clsbckGreen");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnTakePayment_Click(object sender, EventArgs e)
        {
            trTakepayment.Visible = true;
        }

        protected void btnRef_Click(object sender, EventArgs e)
        {
            if (txtRef.Text != null)
            {
                Objbooking.UpdateAgentReferenceNumber(txtRef.Text.Trim(), Convert.ToInt64(OrderId));
                Objbooking.UpdateOrderStatus(7, Convert.ToInt64(OrderId));
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        #region OrderAttachment
        protected void btnAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                string path = UploadFile();
                if (!string.IsNullOrEmpty(path))
                {
                    _masterOrder.AddAttachments(new tblOrderAttachment
                    {
                        AttachmentName = fupAttachment.FileName.Substring(0, fupAttachment.FileName.LastIndexOf(".")),
                        AttachmentPath = path,
                        OrderID = Convert.ToInt32(Request["id"])
                    });
                    BindAttachments();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindAttachments()
        {
            try
            {

                grdOrderAttachments.DataSource = _masterOrder.GetOrderAttachments(Convert.ToInt32(Request["id"]));
                grdOrderAttachments.DataBind();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void grdOrderAttachments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] val = e.CommandArgument.ToString().Split(',');
                if (e.CommandName == "Remove")
                {
                    string path = Server.MapPath(val[1]);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    _masterOrder.RemoveAttachments(Convert.ToInt32(val[0]));
                    BindAttachments();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        string UploadFile()
        {
            try
            {
                string path = "";
                Guid id = Guid.NewGuid();
                if (fupAttachment.HasFile)
                {
                    if (fupAttachment.PostedFile.ContentLength > 2097152)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('Uploaded product file is larger up to 2Mb.')</script>", false);
                        return path;
                    }
                    else
                    {
                        string fExt = fupAttachment.FileName.Substring(fupAttachment.FileName.LastIndexOf("."));
                        path = "../Uploaded/OrderAttachments/" + id + fExt;
                        if (System.IO.File.Exists(Server.MapPath(path)))
                            System.IO.File.Delete(Server.MapPath(path));
                        else
                        {
                            fupAttachment.SaveAs(Server.MapPath(path));
                            string dircPath = Server.MapPath(path);

                        }
                    }
                }
                return path;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetFileExtension(string filepath)
        {
            try
            {
                if (string.IsNullOrEmpty(filepath))
                    return "unknown.png";

                string p = filepath.Substring(filepath.LastIndexOf('.'));
                return p.Replace(".", "") + ".png";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}