﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ManualPassBooking.aspx.cs" Inherits="IR_Admin.ManualPassBooking" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style>
        #btnContinue
        {
            padding: 7px;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <div id="divInvalidMsg" runat="server">
                <h2>
                    You are not permitted to book pass.
                </h2>
            </div>
            <div id="divPass" runat="server">
                <div class="bread-crum-in">
                    <span>Select Product </span>
                </div>
                <div class="full mr-tp1" runat="server">
                    <%-- <ul class="list">
                    <li><a id="aList" href="#" class="current">List</a></li>
                    <li><a id="aNew" href="#" class=" ">New/Edit</a> </li>
                </ul>--%>
                    <!-- tab "panes" -->
                    <div class="full mr-tp1">
                        <asp:Panel ID="pnlErrSuccess" runat="server">
                            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                            <div id="DivError" runat="server" class="error" style="display: none;">
                                <asp:Label ID="lblErrorMsg" runat="server" />
                            </div>
                        </asp:Panel>
                        <div class="searchDiv" style="text-align: center">
                            <asp:DropDownList ID="ddlCategory" runat="server" Width="510px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                        </div>
                        <div class="panes">
                            <div id="divlist" runat="server" style="display: block;">
                                <div class="crushGvDiv">
                                    <asp:GridView ID="grvProduct" runat="server" AutoGenerateColumns="False" PageSize="10"
                                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                        AllowPaging="True" OnPageIndexChanging="grvProduct_PageIndexChanging" OnRowCommand="grvProduct_RowCommand">
                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                        <PagerStyle CssClass="paging"></PagerStyle>
                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                            BorderColor="#FFFFFF" BorderWidth="1px" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            Record not found.</EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <%#Eval("Name")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="30%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Category">
                                                <ItemTemplate>
                                                    <%#Eval("Category")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="25%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Currency">
                                                <ItemTemplate>
                                                    <%#Eval("Currency")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="15%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Shipping">
                                                <ItemTemplate>
                                                    <%#Eval("IsShippingAplicable").ToString() == "True" ? "Yes" : "No"%>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <a href='PassbookingDetail.aspx?Id=<%#Eval("ID")%>' id="btnContinue" class="button">
                                                        Continue</a>
                                                </ItemTemplate>
                                                <ItemStyle Width="8%" HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
