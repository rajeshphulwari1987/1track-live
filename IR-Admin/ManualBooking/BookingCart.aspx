﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="BookingCart.aspx.cs"
    Inherits="IR_Admin.ManualBooking.BookingCart" %>

<%@ Register TagPrefix="uc" TagName="BookingCart" Src="../usercontrol/ucBookingCart.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/DatePicker/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.widget.js" type="text/javascript"></script>
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../Styles/jquery.ui.all.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc:BookingCart ID="ucBookingCart" runat="server" />
</asp:Content>
