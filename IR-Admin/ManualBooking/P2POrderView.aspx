﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="P2POrderView.aspx.cs"
    Inherits="IR_Admin.Orders.P2POrderView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../Styles/jquery.ui.datepicker.css">
    <link rel="stylesheet" href="../Styles/jquery.ui.all.css">
    <script src="../Scripts/DatePicker/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            LoadCal();
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            if ($("#txtFrom").val() != '') {
                //                alert(localStorage.getItem("spantxtTo"));
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                //                alert(localStorage.getItem("spantxtFrom"));
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            $(window).click(function (event) {
                $('#_bindDivData').remove();
            });
        });
        function selectpopup(e) {
            $(function () {
                var $this = $(e);
                var data = $this.val();
                var station = '';
                var hostName = window.location.host;
                var url = "http://" + hostName;
                if (window.location.toString().indexOf("https:") >= 0)
                    url = "https://" + hostName;

                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }
                var filter = $("#span" + $this.attr('id') + "").text();
                if (filter == "" && $this.val() != "")
                    filter = $("#hdnFilter").val();
                $("#hdnFilter").val(filter);
                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }
                if (hostName == "localhost")
                    url = "http://" + hostName + "/IR-Admin";
                else if (hostName == "admin.1tracktest.com")
                    url = "http://" + hostName;
                var hostUrl = url + "/StationList.asmx/getStationsXList";
                data = data.replace(/[']/g, "♥");
                var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {
                        $('#_bindDivData').remove();
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length; ;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);
                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                        });
                        $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                    },
                    error: function () {
                        //     alert("Wait...");
                    }
                });
            });
        }
        function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
            $('#_bindDivData').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }

        var unavailableDates = '<%=unavailableDates1 %>';
        //var unavailableDates = '[25/01/2013]';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
        function LoadCal() {
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                onClose: function (selectedDate) {
                    $("#MainContent_txtReturnDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#MainContent_txtReturnDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0
            });
            $("#MainContent_txtReturnDate").datepicker("option", "minDate", $("#MainContent_txtDepartureDate").datepicker('getDate'));
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });
            $(".imgCal").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });
            $(".imgCal1, #MainContent_txtReturnDate").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked'))
                    $("#MainContent_txtReturnDate").datepicker('show');
            });
        }
        function calenable() {
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('enable');
        }
        function caldisable() {
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('disable');
        }
    </script>
    <style type="text/css">
        .hidepanal
        {
            display: none;
        }
        .divright
        {
            width: 486px;
        }
        .divleft
        {
            width: 159px;
        }
        .ui-datepicker
        {
            background-color: White;
        }
        .gridP
        {
            background: #666;
        }
        .clsRed
        {
            color: red;
        }
        .clsGreen
        {
            color: green;
        }
        .clsbckRed
        {
            background-color: red !important;
            color: #FFF;
        }
        .clsbckGreen
        {
            background-color: green !important;
            color: #FFF;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
    <h2>
        P2P Orders Review</h2>
    <asp:UpdatePanel ID="Upnl1" runat="server">
        <ContentTemplate>
            <div class="full mr-tp1">
                <!-- tab "panes" -->
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="full mr-tp1">
                    <div class="panes">
                        <div id="divDetail">
                            <table class="tblMainSection">
                                <tr>
                                    <td style="vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <asp:Repeater ID="RptP2P" runat="server" OnItemDataBound="RptP2P_ItemDataBound">
                                                <HeaderTemplate>
                                                    <legend><strong>Orders</strong></legend>
                                                    <div class="cat-inner-ord">
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="3" style="color: #00aeef">
                                                            <strong>Order Number: </strong>
                                                            <%#Eval("OrderId")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">
                                                            <strong>Status: </strong>
                                                            <%#Eval("StatusName")%>
                                                            <asp:LinkButton ID="lnkEditStatus" runat="server" Style="color: #00aeef;" OnClick="lnkEditStatus_Click">Edit</asp:LinkButton>
                                                        </td>
                                                        <td width="35%">
                                                            <strong>Creation Date:</strong>
                                                            <%# Eval("CreatedOn", "{0: MMM dd, yyyy}")%>
                                                        </td>
                                                        <td width="35%">
                                                            <strong>Payment Date:</strong>
                                                            <%# Eval("PaymentDate", "{0: MMM dd, yyyy}")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">
                                                            <strong>Site:</strong>
                                                            <%#Eval("SiteName")%>
                                                        </td>
                                                        <td>
                                                            <strong>Ip Address:</strong>
                                                            <%#Eval("IpAddress")%>
                                                        </td>
                                                        <td>
                                                            <strong>Discount Amount:</strong>
                                                            <%=currency%>
                                                            <%=Discount%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Order Entered by:</strong>
                                                            <%=AgentUsername%>
                                                        </td>
                                                        <td>
                                                            <strong>Office Name:</strong>
                                                            <%#Eval("OfficeName")%>
                                                        </td>
                                                        <td>
                                                            <strong>Commission:</strong>
                                                            <%=currency%>
                                                            <%=Commission%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Shipping Amount:</strong>
                                                            <%=currency%>
                                                            <%=ShippingAmount%>
                                                            <span style="float: right">
                                                                <asp:LinkButton ID="lnkEditShipMethod" Style="color: #00aeef;" runat="server" OnClick="lnkEditShipMethod_Click">Edit Shipping method</asp:LinkButton></span>
                                                        </td>
                                                        <td>
                                                            <strong>Booking Fee:</strong>
                                                            <%=currency%>
                                                            <%=BookingFee%>
                                                        </td>
                                                        <td>
                                                            <strong>Ticket Protection:</strong>
                                                            <%=currency%>
                                                            <%=TicketProtection%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Net Price:</strong>
                                                            <%=currency%>
                                                            <%=NetPrice%>
                                                        </td>
                                                        <td>
                                                            <strong>Gross Price:</strong>
                                                            <%=currency%>
                                                            <%=GrossPrice%>
                                                        </td>
                                                        <td>
                                                            <strong>Grand Total:</strong>
                                                            <%=currency%>
                                                            <%=GrandTotal%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <a id="lnkPrintReceipt" href="<%# String.Format("../Orders/OrderSaleRecipt.aspx?id={0}", Eval("OrderId")) %>"
                                                                style="color: #00aeef" target="_Receipt">Print Receipt</a>
                                                        </td>
                                                        <td id="tdPayment" runat="server" class="clsbckRed">
                                                            <strong>Payment Taken:</strong>
                                                            <%=PaymentTaken%>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table> </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Journey Information</strong></legend>
                                            <div class="cat-inner-ord">
                                                <asp:Repeater ID="rptJourney" runat="server" OnItemCommand="rptJourney_ItemCommand">
                                                    <ItemTemplate>
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <strong>Train No: </strong>
                                                                    <%#Eval("TrainNo")%>
                                                                </td>
                                                                <td>
                                                                    <strong>From:</strong>
                                                                    <%#Eval("From")%>
                                                                </td>
                                                                <td>
                                                                    <strong>To:</strong>
                                                                    <%#Eval("To")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Depature Date-Time:</strong>
                                                                    <%#Eval("DateTimeDepature", "{0:dd/MMM/yyyy}")%><span style="color: #00aeef; margin-left: 5px;"><%#Eval("DepartureTime")%></span>
                                                                </td>
                                                                <td>
                                                                    <strong>Arrival Date-Time:</strong>
                                                                    <%#Eval("DateTimeArrival", "{0:dd/MMM/yyyy}")%><span style="color: #00aeef; margin-left: 5px;"><%#Eval("ArrivalTime")%></span>
                                                                </td>
                                                                <td>
                                                                    <strong>Passenger:</strong>
                                                                    <%#Eval("Passenger")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Net Price:</strong>
                                                                    <%# Eval("Symbol")%>&nbsp;<%# Eval("NetPrice")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Class:</strong>
                                                                    <%#Eval("Class")%>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkEditjourney" CommandName="cmdEdit" CommandArgument='<%#Eval("ID") %>'
                                                                        Style="color: #00aeef;" runat="server">Edit</asp:LinkButton>
                                                                    <%--  <strong>Fare Name:</strong>
                                                                    <%#Eval("FareName")%>--%>
                                                                </td>
                                                            </tr>
                                                            <%--  <tr>
                                                                <td>
                                                                    <strong>PNR No:</strong>
                                                                    <%#Eval("ReservationCode")%>
                                                                    <asp:HiddenField ID="hdnPnr" Value='<%#Eval("ReservationCode")%>' runat="server" />
                                                                </td>
                                                                <td>
                                                                    <strong>Pin No:</strong>
                                                                    <%#Eval("PinNumber")%>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>--%>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Traveller Information</strong></legend>
                                            <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0;">
                                                <asp:GridView ID="grdTraveller" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                    CssClass="gridP" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                    OnRowCommand="grdTraveller_RowCommand">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" BackColor="#00aeef" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.</EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Ticket No.">
                                                            <ItemTemplate>
                                                                <%# ((GridViewRow)Container).RowIndex + 1%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="8%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title">
                                                            <ItemTemplate>
                                                                <%#Eval("Title")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="First Name">
                                                            <ItemTemplate>
                                                                <%#Eval("FirstName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last Name">
                                                            <ItemTemplate>
                                                                <%#Eval("LastName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Country">
                                                            <ItemTemplate>
                                                                <%#Eval("CountryName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEditTraveller" CommandName="cmdEdit" CommandArgument='<%#Eval("OrderTravellerID") %>'
                                                                    Style="color: #00aeef;" runat="server">Edit</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Billing Information</strong></legend>
                                            <div class="cat-inner-ord">
                                                <asp:Repeater ID="rptBilling" runat="server" OnItemCommand="rptBilling_ItemCommand">
                                                    <ItemTemplate>
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <strong>Title: </strong>
                                                                    <%#Eval("Title")%>
                                                                </td>
                                                                <td>
                                                                    <strong>First Name:</strong>
                                                                    <%#Eval("FirstName")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Last Name:</strong>
                                                                    <%#Eval("LastName")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Address1:</strong>
                                                                    <%#Eval("Address1")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Address2:</strong>
                                                                    <%#Eval("Address2")%>
                                                                </td>
                                                                <td>
                                                                    <strong>City:</strong>
                                                                    <%#Eval("City")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>State:</strong>
                                                                    <%# Eval("State")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Postal/Zip Code:</strong>
                                                                    <%#Eval("Postcode")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Country:</strong>
                                                                    <%#Eval("Country")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Email Address:</strong>
                                                                    <%#Eval("EmailAddress")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Phone Number:</strong>
                                                                    <%#Eval("Phone")%>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkEditBilling" Style="color: #00aeef;" runat="server">Edit Billing Details</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Shipping Information</strong></legend>
                                            <div class="cat-inner-ord">
                                                <asp:Repeater ID="rptShipping" runat="server" OnItemCommand="rptShipping_ItemCommand">
                                                    <ItemTemplate>
                                                        <table class="table-stock" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <strong>Title: </strong>
                                                                    <%#Eval("TitleShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>First Name:</strong>
                                                                    <%#Eval("FirstNameShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Last Name:</strong>
                                                                    <%#Eval("LastNameShpg")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Address1:</strong>
                                                                    <%#Eval("Address1Shpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Address2:</strong>
                                                                    <%#Eval("Address2Shpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>City:</strong>
                                                                    <%#Eval("CityShpg")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>State:</strong>
                                                                    <%# Eval("StateShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Postal/Zip Code:</strong>
                                                                    <%#Eval("PostcodeShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Country:</strong>
                                                                    <%#Eval("CountryShpg")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <strong>Email Address:</strong>
                                                                    <%#Eval("EmailAddressShpg")%>
                                                                </td>
                                                                <td>
                                                                    <strong>Phone Number:</strong>
                                                                    <%#Eval("PhoneShpg")%>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkEditShipping" Style="color: #00aeef;" runat="server">Edit Shipping details</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <div style="float: right; margin-right: 18px;">
                                                    <asp:Button ID="btnSendEmail" runat="server" CssClass="button1" Text="Send confirmation email"
                                                        OnClick="btnSendEmail_Click" Width="215px" Visible="False" /></div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><strong>Payment Details</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table id="tblAgent" runat="server" class="table-stock" cellpadding="0" cellspacing="0"
                                                    visible="True">
                                                    <tr>
                                                        <td>
                                                            <strong>Agent ref / Folder No:</strong>
                                                            <asp:Label ID="lblAgentRef" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Payment Taken: <span id="spnPayment" runat="server" class="clsRed">
                                                                <%=PaymentTaken%></span></strong> <span style="padding-left: 30px">
                                                                    <asp:Button ID="btnTakePayment" Text="Take Payment" runat="server" CssClass="button1"
                                                                        OnClick="btnTakePayment_Click" /></span>
                                                        </td>
                                                    </tr>
                                                    <tr id="trpaymentdt" runat="server" visible="False">
                                                        <td>
                                                            <strong>Date Taken:
                                                                <%=PaymentTakenDate%></strong>
                                                        </td>
                                                    </tr>
                                                    <tr id="trTakepayment" runat="server" visible="False">
                                                        <td>
                                                            <strong>*Enter Reference number:
                                                                <asp:TextBox ID="txtRef" runat="server" Width="100px" />
                                                                <asp:RequiredFieldValidator ID="rfRef" runat="server" ControlToValidate="txtRef"
                                                                    ErrorMessage="Enter Reference Number" ForeColor="Red" ValidationGroup="ref"/>
                                                                <asp:Button ID="btnRef" Text="Enter Reference" runat="server" CssClass="button1"
                                                                    OnClick="btnRef_Click" ValidationGroup="ref"/></strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset id="OrderAttachments" runat="server" class="grid-sec2">
                                            <legend><strong>Order Attachments</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table width="100%" style="background: #fff; padding: 3px 0;">
                                                    <tr>
                                                        <td>
                                                            Upload File:
                                                        </td>
                                                        <td>
                                                            <asp:FileUpload ID="fupAttachment" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddAttachment" Text="Upload" runat="server" CssClass="button1"
                                                                OnClick="btnAddAttachment_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="cat-inner-ord">
                                                <asp:GridView ID="grdOrderAttachments" runat="server" AutoGenerateColumns="False"
                                                    BorderStyle="None" CssClass="gridP" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                    Width="100%" OnRowCommand="grdOrderAttachments_RowCommand">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.</EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. no.">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="File Name">
                                                            <ItemTemplate>
                                                                <%#Eval("AttachmentName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="File Type">
                                                            <ItemTemplate>
                                                                <img src="../images/FileIcon/<%# GetFileExtension(Eval("AttachmentPath").ToString())%>"
                                                                    alt="file" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                  
                                                                <asp:ImageButton runat="server" ID="imgRemove" Visible="<%#IsVisibleAttachmentDeleteBtn%>" CommandArgument='<%#Eval("ID")+","+ Eval("AttachmentPath")%>' 
                                                                    CommandName="Remove" ImageUrl='../images/delete.png' ToolTip="Delete file"  OnClientClick="return confirm('Are you sure you want to delete this item?')" />

                                                                <a href='<%#Eval("AttachmentPath")%>' title="Download file" target="_blank" >
                                                                    <img src="../images/download.png" alt="download"  width="20px"/>
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>

                                        <fieldset class="grid-sec2">
                                            <legend><strong>Agent Notes</strong></legend>
                                            <div class="cat-inner-ord">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <textarea id="txtNotes" runat="server" rows="7" style="width: 100%" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnAddNotes" Text="Add" runat="server" CssClass="button1" OnClick="btnAddNotes_Click"
                                                                OnClientClick="return CheckIsRepeat();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="cat-inner-ord">
                                                <asp:GridView ID="grdOrderNotes" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                    CssClass="gridP" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" BackColor="#00aeef" HorizontalAlign="Left"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.</EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <%#Eval("UserName")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Top" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Notes">
                                                            <ItemTemplate>
                                                                <%#Eval("Notes")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="60%" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                <%#Eval("CreatedOn", "{0:dd/MMM/yyyy hh:mm}")%>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Width="15%" HorizontalAlign="Left" VerticalAlign="Top" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <asp:LinkButton ID="lnkPop" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop1" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop2" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop3" runat="server" Text="lnk" />
                <asp:LinkButton ID="lnkPop4" runat="server" Text="lnk" />
            </div>
            <div id="pnlStockbill" class="popup-container" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Add/Edit Billing Information
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Title:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlbtitle" runat="server" class="inputsl">
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="divleft">
                        First Name<span class="readerror">*</span></div>
                    <div class="divright">
                        <asp:TextBox ID="txtbfname" runat="server" CssClass="input validcheck" ClientIDMode="Static"
                            MaxLength="20" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="*"
                            ErrorMessage="Please enter first name." ControlToValidate="txtbfname" ForeColor="Red"
                            ValidationGroup="vgs21" />
                    </div>
                    <div class="divleft">
                        Last name<span class="readerror">*</span></div>
                    <div class="divright">
                        <asp:TextBox ID="txtblname" runat="server" CssClass="input validcheck" MaxLength="20" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="*"
                            ErrorMessage="Please enter last name." ControlToValidate="txtblname" ForeColor="Red"
                            ValidationGroup="vgs21" />
                    </div>
                    <div class="divleft">
                        Email<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbemail" runat="server" CssClass="input validcheck" autocomplete="off"
                            MaxLength="50">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Text="*"
                            ErrorMessage="Please enter email address." ControlToValidate="txtbemail" ForeColor="Red"
                            ValidationGroup="vgs21" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="*"
                            ErrorMessage="Please enter a valid email." ControlToValidate="txtbemail" ForeColor="Red"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                            ValidationGroup="vgs21" />
                    </div>
                    <div class="divleft">
                        Phone Number<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbphone" runat="server" CssClass="input validcheck" MaxLength="15" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Text="*"
                            ErrorMessage="Please enter phone." ControlToValidate="txtbphone" ForeColor="Red"
                            ValidationGroup="vgs21" />
                    </div>
                    <div class="divleft">
                        Address Line 1 Or Company Name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbadd" runat="server" CssClass="input validcheck" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Text="*"
                            ErrorMessage="Please enter address." ControlToValidate="txtbadd" ForeColor="Red"
                            ValidationGroup="vgs21" />
                    </div>
                    <div class="divleft">
                        Address Line 2
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbadd2" runat="server" CssClass="input" MaxLength="100" />
                    </div>
                    <div class="divleft">
                        Town / City
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbcity" runat="server" CssClass="input" MaxLength="20" />
                    </div>
                    <div class="divleft">
                        County / State
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbState" runat="server" CssClass="input" MaxLength="20" />
                    </div>
                    <div class="divleft">
                        Postal/Zip Code<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtbzip" runat="server" CssClass="input validcheck" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Text="*"
                            ErrorMessage="Please enter postal/zip code." ControlToValidate="txtbzip" ForeColor="Red"
                            ValidationGroup="vgs21" />
                    </div>
                    <div class="divleft">
                        Country<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlbcountry" runat="server" class="inputsl" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Text="*"
                            ErrorMessage="Please select country." ControlToValidate="ddlbcountry" InitialValue="0"
                            ValidationGroup="vgs21" ForeColor="Red" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnbilling" runat="server" CssClass="button1" Text="Update billing detail"
                            ValidationGroup="vgs21" OnClick="btnbillingt_Click1" />
                        &nbsp;
                        <button id="btnCancelBill" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="txtbilling" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mdpupBilling" runat="server" TargetControlID="lnkPop3"
                CancelControlID="btnCancelBill" PopupControlID="pnlStockbill" BackgroundCssClass="modalBackground" />
            <div id="pnlStockAdd" class="popup-container" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Add/Edit Shipping Information
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Title:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlMr" runat="server" class="inputsl">
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="divleft">
                        First Name<span class="readerror">*</span></div>
                    <div class="divright">
                        <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" ClientIDMode="Static"
                            MaxLength="20" />
                        <asp:RequiredFieldValidator ID="rfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                            ControlToValidate="txtFirst" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Last name<span class="readerror">*</span></div>
                    <div class="divright">
                        <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" MaxLength="20" />
                        <asp:RequiredFieldValidator ID="rfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                            ControlToValidate="txtLast" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Email<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" autocomplete="off"
                            MaxLength="50">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                            ControlToValidate="txtEmail" ForeColor="Red" ValidationGroup="vgs1" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                            ControlToValidate="txtEmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Phone Number<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtPhone" runat="server" CssClass="input validcheck" MaxLength="15" />
                        <asp:RequiredFieldValidator ID="rfPhone" runat="server" Text="*" ErrorMessage="Please enter phone."
                            ControlToValidate="txtPhone" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Address Line 1 Or Company Name<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="rfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                            ControlToValidate="txtAdd" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Address Line 2
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" MaxLength="100" />
                    </div>
                    <div class="divleft">
                        Town / City
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="input" MaxLength="20" />
                    </div>
                    <div class="divleft">
                        County / State
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtState" runat="server" CssClass="input" MaxLength="20" />
                    </div>
                    <div class="divleft">
                        Postal/Zip Code<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="rfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                            ControlToValidate="txtZip" ForeColor="Red" ValidationGroup="vgs1" />
                    </div>
                    <div class="divleft">
                        Country<span class="readerror">*</span>
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" />
                        <asp:RequiredFieldValidator ID="rfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                            ControlToValidate="ddlCountry" InitialValue="0" ValidationGroup="vgs1" ForeColor="Red" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button1" Text="Update shipping detail"
                            ValidationGroup="vgs1" OnClick="btnSubmit_Click1" />
                        &nbsp;
                        <button id="btnCancelShip" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="lblMessage" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mdpupShipping" runat="server" TargetControlID="lnkPop"
                CancelControlID="btnCancelShip" PopupControlID="pnlStockAdd" BackgroundCssClass="modalBackground" />
            <div id="pnlTraveller" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Add/Edit Traveller Information
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Title:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlTrTitle" runat="server" class="inputsl">
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="divleft">
                        First Name<span class="readerror">*</span></div>
                    <div class="divright">
                        <asp:TextBox ID="ddlTrFirstName" runat="server" CssClass="input validcheck" ClientIDMode="Static"
                            MaxLength="20" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*"
                            ErrorMessage="Please enter first name." ControlToValidate="ddlTrFirstName" ForeColor="Red"
                            ValidationGroup="hvgs1" />
                    </div>
                    <div class="divleft">
                        Last name<span class="readerror">*</span></div>
                    <div class="divright">
                        <asp:TextBox ID="txtTrLName" runat="server" CssClass="input validcheck" MaxLength="20" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*"
                            ErrorMessage="Please enter last name." ControlToValidate="txtTrLName" ForeColor="Red"
                            ValidationGroup="hvgs1" />
                    </div>
                    <div class="divleft">
                        Country of residence
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlTrCountry" runat="server" class="chkcountry" Style="width: 145px;
                            height: 28px; margin-left: -2px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="hvgs1"
                            CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                            ControlToValidate="ddlTrCountry" InitialValue="0" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnTravellerUpdate" runat="server" CssClass="button1" Text="Update Traveller detail"
                            ValidationGroup="hvgs1" OnClick="btnTravellerUpdate_Click1" />
                        &nbsp;
                        <button id="btnCcacelTrv" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="Label1" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mupTraveller" runat="server" TargetControlID="lnkPop1"
                CancelControlID="btnCcacelTrv" PopupControlID="pnlTraveller" BackgroundCssClass="modalBackground" />
            <div id="pnlOrderStatus" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Edit Order Status
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Order Status:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlOrderStatus" runat="server" class="inputsl">
                        </asp:DropDownList>
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnOrderStatus" runat="server" CssClass="button1" CausesValidation="false"
                            Text="Update order status" OnClick="btnOrderStatus_Click" />
                        &nbsp;
                        <button id="btnOrderStausCancel" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="Label2" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeOrderStatus" runat="server" TargetControlID="lnkPop2"
                CancelControlID="btnOrderStausCancel" PopupControlID="pnlOrderStatus" BackgroundCssClass="modalBackground" />
            <%--Shipping Method Popup start--%>
            <div id="pnlShipMethod" style="display: none; width: 700px;">
                <div class="heading-hd">
                    Edit Shipping Method
                </div>
                <div class="divMain-container" style="margin-top: -2px;">
                    <div class="divleft">
                        Shipping Method:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlShippindMethod" runat="server" class="inputsl">
                        </asp:DropDownList>
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn2" style="padding-top: 10px;">
                        <asp:Button ID="btnShipMethod" runat="server" CssClass="button1" CausesValidation="false"
                            Text="Update shipping method" OnClick="btnShipMethod_Click" />
                        &nbsp;
                        <button id="btnShipMethodCancel" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <div class="clear">
                        </div>
                        <span>
                            <asp:Label ID="Label3" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mupShipMethod" runat="server" TargetControlID="lnkPop2"
                CancelControlID="btnShipMethodCancel" PopupControlID="pnlShipMethod" BackgroundCssClass="modalBackground" />
            <%--Shipping Method Popup End--%>
            <div id="P2PjournyEdit" class="divMain" style="display: none; width: 650px; height: 430px;
                font-size: 13px;">
                <div class="divleft">
                    From
                </div>
                <div class="divright">
                    <asp:TextBox ID="txtFrom" ClientIDMode="Static" onkeyup="selectpopup(this)" runat="server"
                        MaxLength="40" class="input" autocomplete="off" Style="margin-left: -3px;" />
                    <span id="spantxtFrom" style="display: none"></span>
                    <asp:HiddenField ID="hdnFrm" runat="server" />
                    <asp:RequiredFieldValidator ID="reqFrom" runat="server" ForeColor="red" ValidationGroup="vgs"
                        Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                        Text="*" />
                </div>
                <div class="divleft">
                    To
                </div>
                <div class="divright">
                    <asp:TextBox ClientIDMode="Static" ID="txtTo" runat="server" onkeyup="selectpopup(this)"
                        class="input" autocomplete="off" Style="margin-left: -3px;" MaxLength="40" />
                    <span id="spantxtTo" style="display: none"></span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ForeColor="red"
                        ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                        Text="*" CssClass="font14" />
                </div>
                <div class="divleft" style="width: 99%;">
                    <div style="float: left; width: 156px">
                        Date of departure
                    </div>
                    <div style="float: right; width: 486px;">
                        <asp:TextBox ID="txtDepartureDate" runat="server" class="input" Style="margin-right: 4px;
                            float: left;" autocomplete="off" MaxLength="10" />
                        <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select DepartureDate."
                            border="0" alt="">
                        <asp:RequiredFieldValidator ID="reqDepartureDate" runat="server" ForeColor="red"
                            ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtDepartureDate"
                            ErrorMessage="Please enter departure date." Text="*" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDepartureDate"
                            ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                            Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                            ValidationGroup="vgs">*</asp:RegularExpressionValidator>
                        <asp:DropDownList ID="ddldepTime" runat="server" class="inputsl" Style="width: 90px !important;
                            margin-left: 0px; float: left;">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem Selected="True">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlDepSec" runat="server" class="inputsl" Style="width: 90px !important;
                            margin-left: 5px; float: left;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divleft" style="width: 99%;">
                    <div style="float: left; width: 156px">
                        Date of arrival
                    </div>
                    <div style="float: right; width: 486px">
                        <asp:TextBox ID="txtReturnDate" runat="server" class="input" Style="margin-right: 4px;
                            float: left;" autocomplete="off" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="reqReturnDate" runat="server" ForeColor="red" ValidationGroup="vgs"
                            Display="Dynamic" ControlToValidate="txtReturnDate" ErrorMessage="Please enter return date."
                            Text="*" />
                        <asp:RegularExpressionValidator ID="regReturnDate" runat="server" ControlToValidate="txtReturnDate"
                            ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                            Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid return date"
                            ValidationGroup="vgs" Enabled="False">*</asp:RegularExpressionValidator>
                        <asp:DropDownList ID="ddlReturnTime" runat="server" class="inputsl" Style="width: 90px !important;
                            margin-left: 0px; float: left;">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem Selected="True">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlRetSec" runat="server" class="inputsl" Style="width: 90px !important;
                            margin-left: 5px; float: left;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divleft" style="width: 99%;">
                    <div style="float: left; width: 156px">
                        Train number(s)
                    </div>
                    <div style="float: right; width: 486px">
                        <asp:TextBox ID="txtTrainNumber" runat="server" class="input" autocomplete="off"
                            MaxLength="20" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ForeColor="red"
                            ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTrainNumber" ErrorMessage="Please enter train number."
                            Text="*" />
                    </div>
                </div>
                <div class="divleft" style="width: 99%;">
                    <div style="float: left; width: 156px">
                        Class Preference
                    </div>
                    <div style="float: right; width: 486px">
                        <asp:RadioButtonList ID="ddlClass" runat="server" RepeatDirection="Horizontal" Width="150px">
                            <asp:ListItem Value="1" Selected="True">1st</asp:ListItem>
                            <asp:ListItem Value="2">2nd</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="divleft" style="width: 99%;">
                    <div style="float: left; width: 156px">
                        Number of adults
                    </div>
                    <div style="float: left; width: 17%">
                        <asp:DropDownList ID="ddlAdult" runat="server" class="inputsl m-none" Width="90px">
                        </asp:DropDownList>
                    </div>
                    <div style="float: left; width: 18%">
                        Number of children
                    </div>
                    <div style="float: left; width: 28%">
                        <asp:DropDownList ID="ddlChildren" runat="server" class="inputsl m-none" Width="90px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divleft" style="width: 99%;">
                    <div style="float: left; width: 156px">
                        Number of seniors
                    </div>
                    <div style="float: left; width: 17%">
                        <asp:DropDownList ID="ddlSeniors" runat="server" class="inputsl m-none" Width="90px">
                        </asp:DropDownList>
                    </div>
                    <div style="float: left; width: 18%">
                        Number of youths
                    </div>
                    <div style="float: left; width: 28%">
                        <asp:DropDownList ID="ddlYouth" runat="server" class="inputsl m-none" Width="90px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divleft">
                    Ticket type*
                </div>
                <div class="divright">
                    <asp:TextBox ID="txtTicketType" runat="server" class="input" autocomplete="off" Style="margin-left: -3px;"
                        MaxLength="20" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ForeColor="red"
                        ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTicketType" ErrorMessage="Please enter ticket type."
                        Text="*" />
                </div>
                <%-- <div class="divleft">
                    Price*
                </div>
                <div class="divright">
                    <asp:TextBox ID="txtprice" ClientIDMode="Static" MaxLength="10" runat="server" class="input"
                        Style="margin-left: -3px;" autocomplete="off" CssClass="number" />
                    <%=CurrCode%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ForeColor="red"
                        ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtprice" ErrorMessage="Please enter price."
                        Text="*" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Incorrect Price."
                        Text="*" ForeColor="Red" ControlToValidate="txtprice" ValidationExpression="\b(\d+(?:\.(?:[^0]\d|\d|\d|\d[^0]))?)\b"
                        ValidationGroup="vgs" />
                </div>--%>
                <div style="width: 99%;">
                    <div style="float: left; width: 156px">
                    </div>
                    <div style="float: left; width: 486px; margin-top: 9px; margin-left: 147px;">
                        <asp:Button ID="btnCheckout" runat="server" Text="Update" CssClass="button1" ValidationGroup="vgs"
                            Style="margin-left: 10px;" OnClick="btnCheckout_Click" />
                        <asp:ValidationSummary ID="VSum" ValidationGroup="vgs" DisplayMode="List" ShowSummary="false"
                            ShowMessageBox="true" runat="server" />
                        <button id="btnp2pCancel" runat="server" class="button1" onclick="hidepopup()">
                            Cancel</button>
                        <asp:HiddenField runat="server" ID="hdnp2pjourneyID" />
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeP2Pjourney" runat="server" TargetControlID="lnkPop4"
                CancelControlID="btnp2pCancel" PopupControlID="P2PjournyEdit" BackgroundCssClass="modalBackground" />
            </div> </div>
        </ContentTemplate>
        <Triggers>
         <asp:PostBackTrigger ControlID="btnAddAttachment" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function hidepopup() {
            $("#divprogress").hide();
        }
    </script>
</asp:Content>
