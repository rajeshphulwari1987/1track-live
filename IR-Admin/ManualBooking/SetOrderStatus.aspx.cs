﻿#region Using
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

#endregion

namespace IR_Admin.ManualBooking
{
    public partial class SetOrderStatus : Page
    {
        #region [ Page InIt must write on every page of CMS ]
        Guid siteId;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
            Session["siteId"] = siteId;
            PageLoadEvent();
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            siteId = Master.SiteID;
            Session["siteId"] = siteId;

            if (!IsPostBack)
            {
                PageLoadEvent();
                ddlOrderStatus.DataSource = new ManageOrder().GetStatusList().OrderBy(a => a.Name).ToList();
                ddlOrderStatus.DataValueField = "ID";
                ddlOrderStatus.DataTextField = "Name";
                ddlOrderStatus.DataBind();
                ddlOrderStatus.Items.Insert(0, new ListItem("--Select Order--", "0")); 
                if (Session["P2POrderID"] != null)
                {
                    litOrderStatus.Text = "P2P Booking >> P2P Detail >> Booking Details >> <span>Set Order Status</span>";
                }
                else if (Session["OrderId"] != null)
                {
                    litOrderStatus.Text = "Select Product >> Pass Booking >> Pass Detail >> Booking Details >> Payment >> <span>Set Order Status</span>";
                }
            }
        }

        private void PageLoadEvent()
        {
            try
            {
                siteId = Guid.Parse(Session["siteId"].ToString());
                var list = new ManageOrder().GetAllBranchlistBySiteId(siteId).OrderBy(t => t.PathName).ToList();
                ddlOffice.DataSource = list;
                ddlOffice.DataTextField = "PathName";
                ddlOffice.DataValueField = "Id";
                ddlOffice.DataBind();
                ddlOffice.Items.Insert(0, new ListItem("-All Office-", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnOrderStatus_Click(object sender, EventArgs e)
        {
            if (Session["P2POrderID"] != null)
            {
                new ManageBooking().UpdateOrderStatusForManualBooking(Convert.ToInt32(ddlOrderStatus.SelectedValue), Convert.ToInt64(Session["P2POrderID"]), Guid.Parse(ddlAgent.SelectedValue));
                Response.Redirect("../Orders/P2POrderDetails.aspx?id=" + Convert.ToInt64(Session["P2POrderID"]));
            }
            else if (Session["OrderId"] != null)
            {
                new ManageBooking().UpdateOrderStatusForManualBooking(Convert.ToInt32(ddlOrderStatus.SelectedValue), Convert.ToInt64(Session["OrderId"]), Guid.Parse(ddlAgent.SelectedValue));
                Response.Redirect("../Orders/OrderDetails.aspx?id=" + Convert.ToInt64(Session["OrderId"]));
            }
        }

        protected void ddlOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlOffice.SelectedValue != "0")
                {
                    var branchid = Guid.Parse(ddlOffice.SelectedValue);
                    var list = new Masters().GetAdminUserListByBranchId(branchid).OrderBy(t => t.Forename).ToList();
                    ddlAgent.Enabled = true;
                    ddlAgent.DataSource = list;
                    ddlAgent.DataTextField = "Name";
                    ddlAgent.DataValueField = "ID";
                    ddlAgent.DataBind();
                    ddlAgent.Items.Insert(0, new ListItem("-All Agent-", "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}