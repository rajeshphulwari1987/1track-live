﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.ManualBooking
{
    public partial class PassAgent : Page
    {
        #region [ Page InIt must write on every page of CMS ]
        Guid siteId;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
            Session["siteId"] = siteId;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FillOffice();
        }

        private void FillOffice()
        {
            try
            {
                siteId = Master.SiteID;
                var list = new ManageOrder().GetAllBranchlistBySiteId(siteId).OrderBy(t => t.PathName).ToList();
                ddlOffice.DataSource = list;
                ddlOffice.DataTextField = "PathName";
                ddlOffice.DataValueField = "Id";
                ddlOffice.DataBind();
                ddlOffice.Items.Insert(0, new ListItem("-All Office-", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblAgentMsg.Visible = false;
                if (ddlOffice.SelectedValue != "0")
                {
                    var branchid = Guid.Parse(ddlOffice.SelectedValue);
                    var list = new Masters().GetAdminUserListByBranchId(branchid).OrderBy(t => t.Forename).ToList();
                    ddlAgent.Enabled = true;
                    ddlAgent.DataSource = list;
                    ddlAgent.DataTextField = "Name";
                    ddlAgent.DataValueField = "ID";
                    ddlAgent.DataBind();
                    ddlAgent.Items.Insert(0, new ListItem("-All Agent-", "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnOrderStatus_Click(object sender, EventArgs e)
        {
            if (Session["OrderId"] != null)
            {
                var isAgent = new ManageBooking().CheckIfAgent(Guid.Parse(ddlAgent.SelectedValue));
                if (!isAgent)
                    lblAgentMsg.Visible = true;
                else
                {
                    Session["AgentUserID"] = ddlAgent.SelectedValue;
                    new ManageBooking().UpdateOrderStatusForManualBooking(1, Convert.ToInt64(Session["OrderId"]),
                                                                          Guid.Parse(ddlAgent.SelectedValue));
                    Response.Redirect("../ManualBooking/BookingCart.aspx");
                }
            }
        }
    }
}