﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="SetOrderStatus.aspx.cs"
    Inherits="IR_Admin.ManualBooking.SetOrderStatus" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../Styles/PassBooking.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content">
        <div class="left-content">
            <div class="bread-crum-in">
                <asp:Literal ID="litOrderStatus" runat="server"></asp:Literal>
            </div>
            <div class="round-titles">
                Order Information
            </div>
            <div class="booking-detail-in">
                <div class="divleft">
                    Order Status:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlOrderStatus" runat="server" class="inputsl" />
                    <asp:RequiredFieldValidator ID="reqOrderStatus" runat="server" ControlToValidate="ddlOrderStatus"
                        ValidationGroup="stu" InitialValue="0" ErrorMessage="*" Text="*" CssClass="valdreq"
                        ForeColor="Red" Display="Dynamic" />
                </div>
                <div class="divleft">
                    Office Name:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlOffice" runat="server" class="inputsl" ValidationGroup="stu"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlOffice_SelectedIndexChanged" />
                    <asp:RequiredFieldValidator ID="reqOffice" runat="server" ControlToValidate="ddlOffice"
                        ValidationGroup="stu" InitialValue="0" ErrorMessage="*" CssClass="valdreq" Display="Dynamic" />
                </div>
                <div class="divleft">
                    Agent:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlAgent" runat="server" class="inputsl" Enabled="false" />
                    <asp:RequiredFieldValidator ID="reqAgent" runat="server" ControlToValidate="ddlAgent"
                        ValidationGroup="stu" InitialValue="0" ErrorMessage="*" CssClass="valdreq" Display="Dynamic" />
                </div>
                <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                    .
                </div>
                <div class="divrightbtn2" style="padding-top: 10px;">
                    <asp:Button ID="btnOrderStatus" runat="server" CssClass="button1" ValidationGroup="stu"
                        Text="Submit" OnClick="btnOrderStatus_Click" />
                    &nbsp;
                    <asp:Button ID="btnOrderStausCancel" runat="server" CssClass="button1" Text="Cancel" />
                    <div class="clear">
                    </div>
                    <span>
                        <asp:Label ID="Label2" ForeColor="green" runat="server" /></span>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
