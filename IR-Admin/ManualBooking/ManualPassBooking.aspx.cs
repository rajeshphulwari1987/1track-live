﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ManualPassBooking : System.Web.UI.Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            _SiteID = Master.SiteID;
            if (!Page.IsPostBack)
            {
                var adminUserID = AdminuserInfo.UserID;
                if (adminUserID != null)
                {
                    var isUser = _db.tblAdminUsers.FirstOrDefault(x => x.ID == adminUserID);
                    if (isUser != null)
                    {
                        if (isUser.IsAgent == true)
                        {
                            Session["AgentUserID"] = isUser.ID;
                            Session["AgentUsername"] = isUser.UserName;
                        }
                        if (isUser.ManualPassIsActive == false)
                        {
                            divInvalidMsg.Style.Add("display", "block");
                            divPass.Style.Add("display", "none");
                        }
                        else
                        {
                            divInvalidMsg.Style.Add("display", "none");
                            divPass.Style.Add("display", "block");
                        }
                    }
                }

                BindCategory();
                BindGrid();
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        void BindCategory()
        {
            _SiteID = this.Master.SiteID;
            const int treeLevel = 3;
            ddlCategory.DataSource = _oProduct.GetAssociateCategoryList(treeLevel, _SiteID);
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
        }

        void BindGrid()
        {
            _SiteID = this.Master.SiteID;
            List<Guid> categoriesId = new List<Guid>();
            if (ddlCategory.SelectedIndex > 0)
                categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };
            grvProduct.DataSource = _oProduct.GetProductList(_SiteID, categoriesId);
            grvProduct.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grvProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProduct.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void grvProduct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Continue")
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    Response.Redirect("PassbookingDetail.aspx?Id=" + id);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
    }
}