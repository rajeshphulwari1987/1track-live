﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PaymentProcess.aspx.cs"
    Inherits="IR_Admin.ManualBooking.PaymentProcess" %>

<%@ Register TagPrefix="uc" TagName="PaymentProcess" Src="../usercontrol/ucPaymentProcess.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc:PaymentProcess ID="ucPaymentProcess" runat="server" />
</asp:Content>
