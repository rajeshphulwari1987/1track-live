﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="TopJourney.aspx.cs"
    Inherits="IR_Admin.TopJourney" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <style type="text/css">
        .loading
        {
            background-image: url(images/loading3.gif);
            background-position: right;
            background-repeat: no-repeat;
        }
        .clsFlag
        {
            min-height: 115px;
            max-height: 115px;
            overflow-y: auto;
            border: 1px solid #b3b3b3;
            background: #fff;
        }
        #MainContent_dtlTopJourney
        {
            border: 1px solid #cccccc;
            margin-left: -2px;
        }
    </style>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('MainContent_aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
        
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }

        function OnClientPopulating(sender) {
            sender._element.className = "loading";
        }
        function OnClientCompleted(sender) {
            sender._element.className = "";
        }

        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
                return true;
            return false;
        }

        function SetSingleFromFlag(nameregex, current) {
            var pp = eval("nameregex");
            var re = new RegExp(nameregex);
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var elm = document.forms[0].elements[i];
                if (elm.type == 'radio' && elm.value == 'rdFlagFrom') {
                    if (elm != current) {
                        elm.checked = false;
                    }
                }
            }
            current.checked = true;
        }

        function SetSingleToFlag(nameregex, current) {
            var pp = eval("nameregex");
            re = new RegExp(nameregex);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'radio' && elm.value == 'rdFlagTo') {
                    if (elm != current) {
                        elm.checked = false;
                    }
                }
            }
            current.checked = true;
        }
    </script>
    <script>
        function selectpopup(e) {
            $(document).ready(function () {
                var $this = $(e);
                var data = $this.val();
                var filter = $("#span" + $this.attr('id') + "").text();
                var station = '';
                var hostName = window.location.host;
                var url = "https://" + hostName;

                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }
                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }

                if (hostName == "localhost")
                    url = "http://" + hostName + "/IR-Admin";

                else if (hostName == "admin.1tracktest.com")
                    url = "http://" + hostName;

                var hostUrl = url + "/StationList.asmx/getStationsXList";
                data = data.replace(/[']/g, "♥");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {
                        $('#_bindDivData').remove();
                        var $div = $("<div id='_bindDivData' onmouseleave='_hideThisDiv(this)'/>");
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length; ;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);
                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                        });
                    },
                    error: function () {
                        //                    alert("Wait...");
                    }
                });
            });
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo')
                $('#spantxtFrom').text($(e).find('div').text());
            else
                $('#spantxtTo').text($(e).find('div').text());
            $('#_bindDivData').remove();
        }
        function _hideThisDiv(e) {
            $('#_bindDivData').remove();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Top Journeys</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" runat="server" href="TopJourney.aspx" class="current">List</a></li>
            <li><a id="aNew" runat="server" href="TopJourney.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div id="dvSearch" class="searchDiv" runat="server">
                        <table width="100%" style="line-height: 20px">
                            <tr>
                                <td>
                                    Station :
                                </td>
                                <td>
                                    Top Journey :
                                </td>
                                <td>
                                    Popular Journey:
                                </td>
                                <td>
                                    Status :
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtStation" runat="server" ClientIDMode="Static" onkeyup="selectpopup(this)"
                                        autocomplete="off" OnTextChanged="btnFilter_Click" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTopJourney" runat="server" ValidationGroup="rv" Width="160px"
                                        AutoPostBack="true" OnTextChanged="btnFilter_Click">
                                        <asp:ListItem Value="-1">-Select-</asp:ListItem>
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPopular" runat="server" ValidationGroup="rv" Width="160px"
                                        AutoPostBack="true" OnTextChanged="btnFilter_Click">
                                        <asp:ListItem Value="-1">-Select-</asp:ListItem>
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlActive" runat="server" ValidationGroup="rv" Width="160px"
                                        AutoPostBack="true" OnTextChanged="btnFilter_Click">
                                        <asp:ListItem Value="-1">-Select-</asp:ListItem>
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="crushGvDiv1">
                        <div class="train-detail-in">
                            <asp:DataList ID="dtlTopJourney" runat="server" RepeatColumns="2" Width="101%" RepeatDirection="Horizontal"
                                OnItemCommand="dtlTopJourney_ItemCommand">
                                <ItemTemplate>
                                    <div class="train-detail-block">
                                        <div class="trainblockjrny">
                                            <img alt="map" src='<%#Eval("BannerImg").ToString()=="" ? "images/NoproductImage.png":Eval("BannerImg")%>' />
                                        </div>
                                        <div class="trainblockleft">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        From:
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Image ID="Image2" runat="server" ImageUrl='<%#Eval("FromFlag")%>' />
                                                        <%#Eval("From")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        To:
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("ToFlag")%>' />
                                                        <%#Eval("To")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Country:
                                                    </td>
                                                    <td valign="top">
                                                        <%#Eval("Country")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Site Name:
                                                    </td>
                                                    <td>
                                                        <%#Eval("SiteName")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Is Top Journey ?
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton runat="server" ID="imgJourney" CommandArgument='<%#Eval("Id")%>'
                                                            Height="16" CommandName="TopJourney" AlternateText="topJourney" ImageUrl='<%#Eval("IsTopJourney").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                            ToolTip='<%#Eval("IsTopJourney").ToString()=="True" ?"Top-Journey":"Not Top-Journey" %>'
                                                            OnClientClick="return confirm('Are you sure you want to activate/deactivate this journey as top journey?');" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Is Popular Journey ?
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton runat="server" ID="imgPopular" CommandArgument='<%#Eval("Id")%>'
                                                            Height="16" CommandName="Popular" AlternateText="popular" ImageUrl='<%#Eval("IsPopular").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                            ToolTip='<%#Eval("IsPopular").ToString()=="True" ?"Popular":"Not Popular" %>'
                                                            OnClientClick="return confirm('Are you sure you want to activate/deactivate this journey as popular?');" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Price:
                                                    </td>
                                                    <td>
                                                        <%#Eval("CurrencySymbol")%>
                                                        <%# String.IsNullOrEmpty(Eval("Price").ToString())?"0":Eval("Price")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Action:
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <a href='TopJourney.aspx?id=<%#Eval("ID")%>'>
                                                            <img src="images/edit.png" /></a>
                                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                                            OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this journey?');" />
                                                        Sort Order:
                                                        <%#Eval("SortOrder")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <table width="100%">
                                <tr class="paging">
                                    <asp:Repeater ID="DLPageCountItem" runat="server">
                                        <ItemTemplate>
                                            <td style="float: left">
                                                <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                    Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: none;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" style="width: 120px">
                                Site:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" runat="server" Width="210px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Country:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlCountry" runat="server" Width="210px" />
                                &nbsp;<asp:RequiredFieldValidator ID="reqCountry" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="ddlCountry" ValidationGroup="rv" InitialValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                From:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtFrom" runat="server" ClientIDMode="Static" onkeyup="selectpopup(this)" Width="208px"
                                    autocomplete="off" /><span id="spantxtFrom" style="display: none"></span>
                                <asp:HiddenField ID="hdnFrm" runat="server" />
                                <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtFrom" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                From Flag:
                            </td>
                            <td class="col">
                                <div class="clsFlag">
                                    <asp:DataList ID="dtlFrmFlag" runat="server" RepeatDirection="Horizontal" CellPadding="5"
                                        RepeatColumns="3" OnItemDataBound="dtlFrmFlag_ItemDataBound">
                                        <ItemTemplate>
                                            <table cellpadding="3">
                                                <tr style="line-height: 0px;">
                                                    <td>
                                                        <asp:RadioButton ID="rdFlagFrom" runat="server" OnCheckedChanged="rdFlagFrom_CheckedChanged" />
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="imgFlag" runat="server" ImageUrl='<%# Eval("CountryFlagImg")==null ||Eval("CountryFlagImg").ToString()=="" ? "images/map.png":Eval("CountryFlagImg")%>'
                                                            Width="41px" Height="26px" />
                                                    </td>
                                                </tr>
                                                <tr style="line-height: 5px;">
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td style="line-height: 15px">
                                                        <%#Eval("CountryName")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                To:
                            </td>
                            <td class="col">
                                <div class="colum-two">
                                    <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" ClientIDMode="Static" Width="208px"
                                        autocomplete="off" /><span id="spantxtTo" style="display: none"></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                To Flag:
                            </td>
                            <td class="col">
                                <div class="clsFlag">
                                    <asp:DataList ID="dtlToFlag" runat="server" RepeatDirection="Horizontal" CellPadding="5"
                                        RepeatColumns="3" OnItemDataBound="dtlToFlag_ItemDataBound">
                                        <ItemTemplate>
                                            <table cellpadding="3">
                                                <tr style="line-height: 0px;">
                                                    <td>
                                                        <asp:RadioButton ID="rdFlagTo" runat="server" OnCheckedChanged="rdFlagTo_CheckedChanged" />
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="imgFlag" runat="server" ImageUrl='<%# Eval("CountryFlagImg")==null ||Eval("CountryFlagImg").ToString()=="" ? "images/map.png":Eval("CountryFlagImg")%>'
                                                            Width="41px" Height="26px" />
                                                    </td>
                                                </tr>
                                                <tr style="line-height: 5px;">
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td style="line-height: 15px">
                                                        <%#Eval("CountryName")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Price:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPrice" runat="server" Width="210px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Duration:
                            </td>
                            <td class="col">
                                Hour:
                                <asp:TextBox ID="txtHr" runat="server" Width="50px" />
                                <asp:RequiredFieldValidator ID="rfHr" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtHr" ValidationGroup="rv" />
                                Minute:
                                <asp:TextBox ID="txtMin" runat="server" Width="50px" />
                                <asp:RequiredFieldValidator ID="rfMin" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtMin" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Banner Image:
                            </td>
                            <td class="col">
                                <asp:FileUpload ID="fuBannerImg" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsActv" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Top Journey:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsTopjourney" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Popular:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkPopular" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Sort Order:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSort" runat="server" Width="50px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Currency Code:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlCurrency" runat="server" Width="210px" />
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="ddlCurrency" ValidationGroup="rv" InitialValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Description:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="8" Columns="5" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
