﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OfferCodes.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="IR_Admin.OfferCodes" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="editor/jquery.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(
            function () {
                $('.txtFRules').redactor({ iframe: true, minHeight: 200 });
            });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 2000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("fast", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Commercial Offer Codes</h2>
    <div class="full mr-tp1">
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <asp:GridView ID="grdOffers" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnRowCommand="grdOffers_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Offer Type Code">
                                <ItemTemplate>
                                    <%#Eval("OfferTypeCode")%>
                                </ItemTemplate>
                                <ItemStyle Width="12%" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Offer Code">
                                <ItemTemplate>
                                    <%#Eval("OfferCode")%>
                                </ItemTemplate>
                                <ItemStyle Width="12%" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Agreement">
                                <ItemTemplate>
                                    <%#Eval("Agreement")%>
                                </ItemTemplate>
                                <ItemStyle Width="10%" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Offer Text">
                                <ItemTemplate>
                                    <%#Eval("OfferText")%>
                                </ItemTemplate>
                                <ItemStyle Width="15%" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add/Edit Fare Rules">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="lnkFare" CssClass="lnkFare" AlternateText="Add/Edit"
                                        ToolTip="Add/Edit FareRules" CommandArgument='<%#Eval("ID")%>' CommandName="FareRules"
                                        ImageUrl="~/images/add.png" />
                                    <br />
                                    <div id="divFareRules" runat="server" visible="False">
                                        <textarea id="txtFRules" class="txtFRules" runat="server" innerhtml='<%#Eval("FareRules")%>'></textarea>
                                        <div style="float: right">
                                            <asp:Button ID="btnSave" runat="server" Text="Add/Edit" CssClass="button" CommandArgument='<%#Eval("ID")%>'
                                                CommandName="Add" />
                                            <asp:Button ID="btnClose" runat="server" Text="Cancel" CssClass="button" CommandArgument='<%#Eval("ID")%>'
                                                CommandName="Close" />
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle ForeColor="White" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
