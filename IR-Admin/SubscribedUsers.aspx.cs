﻿using System;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class SubscribedUsers : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUserList(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindUserList(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        public void BindUserList(Guid _SiteID)
        {
            try
            {
                grdSubUsers.DataSource = _master.GetSubscribedUserList(_SiteID);
                grdSubUsers.DataBind();
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SubscribedUers.aspx");
        }

        protected void grdSubUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ActiveInActive")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                _master.ActiveInactiveSubsUsers(id);
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUserList(_SiteID);
            }

            if (e.CommandName == "Remove")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                var res = _master.DeleteSubscribedUser(id);
                if (res)
                    ShowMessage(1, "Record deleted successfully.");
                _SiteID = Master.SiteID;
                SiteSelected();
                BindUserList(_SiteID);
            }
        }

        protected void grdSubUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSubUsers.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindUserList(_SiteID);
        }
    }
}