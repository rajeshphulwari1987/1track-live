﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SEOManagment.aspx.cs" Inherits="IR_Admin.SEOManagment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        /*new design css*/
        fieldset
        {
            border: 1px solid #00aeef !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="full mr-tp1">
        <div class="panes">
            <h2>
                SEO Management
            </h2>
            <div class="seo-top" style="height: auto;">
                <fieldset style="border: 1px solid #bc3e5f">
                    <legend>Other</legend><a href="MetaTag/CategoryMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                Category SEO</h1>
                        </div>
                    </a><a href="MetaTag/ProductMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                Product SEO</h1>
                        </div>
                    </a><a href="MetaTag/ProductDetailsMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                Prooduct Details SEO</h1>
                        </div>
                    </a><a href="MetaTag/SpecialTrainMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                Special train SEO</h1>
                        </div>
                    </a><a href="MetaTag/CountryMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                Country SEO</h1>
                        </div>
                    </a><a href="MetaTag/WebMenuMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                Other Page SEO</h1>
                        </div>
                    </a><a href="MetaTag/CMSPageMetaInfo.aspx">
                        <div class="seo-div">
                            <h1>
                                CMS Site SEO</h1>
                        </div>
                    </a>
                </fieldset>
            </div>
            <div class="seo-top" style="height: auto;">
                <fieldset style="border: 1px solid #bc3e5f">
                    <legend>Breadcrumb</legend><a href="MetaTag/SEObreadcrumbsList.aspx?ID=railpass">
                        <div class="seo-div">
                            <h1>
                                Rail Passes</h1>
                        </div>
                    </a><a href="MetaTag/SEObreadcrumbsList.aspx?ID=railpassdetails">
                        <div class="seo-div">
                            <h1>
                                Rail Pass Deatils</h1>
                        </div>
                    </a><a href="MetaTag/SEObreadcrumbsList.aspx?ID=speciltrains">
                        <div class="seo-div">
                            <h1>
                                Special Trains</h1>
                        </div>
                    </a><a href="MetaTag/SEObreadcrumbsList.aspx?ID=countries">
                        <div class="seo-div">
                            <h1>
                                Countries</h1>
                        </div>
                    </a><a href="MetaTag/SEObreadcrumbsList.aspx?ID=otherpages">
                        <div class="seo-div">
                            <h1>
                                Other Pages</h1>
                        </div>
                    </a>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
