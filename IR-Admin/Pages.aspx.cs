﻿#region Using
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class Pages : Page
    {
        readonly Masters _master = new Masters();
        public string Tab = string.Empty;
        public string Pagecontent;
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        Guid _siteID;
        Guid LayoutsId = Guid.Empty;
        PageLayoutList PageLayout = new PageLayoutList();
        int SiteTypeValue = 0;

        
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                var master = (SiteMaster)Page.Master;
                if (master != null) master.OnSiteSelected += MasterSelected;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            try
            {
                _siteID = Guid.Parse(selectedValue);
                SiteSelected();
                BindPageDetail(_siteID);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Tab = "1";
                if (Request["edit"] != null)
                    Tab = "2";
                Session["siteId"] = _siteID = Master.SiteID;
                if (!Page.IsPostBack)
                {
                    SiteSelected();
                    BindPageDetail(_siteID);
                    BindDdlSite();
                    BindDdlLayout();
                    Session["url"] = string.Empty;
                    if ((Request["edit"] != null) && (Request["edit"] != ""))
                    {
                        Session["pageid"] = Guid.Parse(Request["edit"]);
                        BindPageForEdit(Guid.Parse(Request["edit"]));
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
         
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string pageHeading = hdnHeading.Value;
                string pageContent = hdnContent.Value;

                string pageUrl = hdnUrl.Value.Trim() + txtPageName.Text.Trim().Replace(" ", "-");

                var boolProductRtImg = (hdnProductRtImg.Value == "1" || hdnProductRtImg.Value == "True");
                var boolPrdContent = (hdnPrdContent.Value == "1" || hdnPrdContent.Value == "True");
                var boolPrdImagePanel = (hdnPrdImagePanel.Value == "1" || hdnPrdImagePanel.Value == "True");
                var boolPrdRtImageSlider = (hdnPrdRtImageSlider.Value == "1" || hdnPrdRtImageSlider.Value == "True");

                Guid SiteId = Guid.Parse(ddlSiteNm.SelectedValue);
                var siteDetails = _db.tblSites.FirstOrDefault(x => x.ID == SiteId && x.IsActive == true);
                if (siteDetails != null)
                {
                    if (siteDetails.LayoutType == 1)
                        SiteTypeValue = 1;
                    else if (siteDetails.LayoutType == 2)
                        SiteTypeValue = 2;
                    else if (siteDetails.LayoutType == 3)
                        SiteTypeValue = 3;
                    else if (siteDetails.LayoutType == 4)
                        SiteTypeValue = 4;
                    else if (siteDetails.LayoutType == 5)
                        SiteTypeValue = 5;
                    else if (siteDetails.LayoutType == 6)
                        SiteTypeValue = 6;
                    else if (siteDetails.LayoutType == 7)
                        SiteTypeValue = 7;
                    else if (siteDetails.LayoutType == 8)
                        SiteTypeValue = 8;
                    else if (siteDetails.LayoutType == 9)
                        SiteTypeValue = 9;
                    else
                        SiteTypeValue = 3;
                }

                if (btnSubmit.CommandName == "Submit")
                {
                    var page = new tblPage
                    {
                        ID = Guid.NewGuid(),
                        PageName = txtPageName.Text.Trim(),
                        SiteID = Guid.Parse(ddlSiteNm.SelectedValue),
                        Url = pageUrl,
                        NavigationID = Guid.Parse(ddlNavigation.SelectedValue),
                        LayoutID = Guid.Parse(ddlLayout.SelectedValue),
                        IsActive = chkActive.Checked,

                        BannerIDs = hdnBanner.Value,
                        PageHeading = pageHeading,
                        PageContent = pageContent,
                        FooterImg = hdnFooter.Value,
                        RightPanelImgID = hdnRight.Value,
                        RightPanelNavUrl = hdnRightPanelUrl.Value,
                        RightPanel1 = hdnRtPanel1.Value,
                        RightPanel2 = hdnRtPanel2.Value,
                        ContactPanel = hdnContactPanel.Value,
                        ContactCall = hdnContactCall.Value,

                        HomePageHeading1 = hdnHeading1.Value,
                        HomePageHeading2 = hdnHeading2.Value,
                        HomePageDescription1 = hdnDescription1.Value,
                        HomePageDescription2 = hdnDescription2.Value,

                        //ProductHeading = hdnPrdHead.Value,
                        //ProductDesc = hdnPrdDesc.Value,
                        //ProductRtImgActive = Convert.ToBoolean(Convert.ToInt32(boolProductRtImg)),
                        //ProductDescActive = Convert.ToBoolean(Convert.ToInt32(boolPrdContent)),
                        //ProductPanelActive = Convert.ToBoolean(Convert.ToInt32(boolPrdImagePanel)),
                        //ProductRtSliderActive = Convert.ToBoolean(Convert.ToInt32(boolPrdRtImageSlider)),

                        MetaTitle = txtMetaTitle.Text.Trim(),
                        Description = txtDescription.Text.Trim(),
                        Keyword = txtKeyWord.Text.Trim(),
                        FooterHeader = hdnfooterBlockheadertxt.Value,

                        HomeSearchText = pageHeading,
                        HomeCountryText = pageContent,
                        SiteType = SiteTypeValue,

                        VEbanner=PhdnVEbanner.Value,
                        VEptpimg=PhdnVEptpimg.Value,
                        VEmap = PhdnVEmap.Value,
                        VEmaps1 = PhdnVEmaps1.Value,
                        VEmaps2 = PhdnVEmaps2.Value,
                        VEmaplink = PhdnVEmaplink.Value,
                        VEmaps1link = PhdnVEmaps1link.Value,
                        VEmaps2link = PhdnVEmaps2link.Value,
                        VEpass = PhdnVEpass.Value,
                        VEtrain = PhdnVEtrain.Value,
                        VEnewadventure = PhdnVEnewadventure.Value,
                        VEbtncontactcall=PhdnVEbtncontactcall.Value,
                        VEbtncontactbook=PhdnVEbtncontactbook.Value,
                        VEbtncontactlive=PhdnVEbtncontactlive.Value,
                        VEbtncontactemail=PhdnVEbtncontactemail.Value,
                        VEbtntext1 = PhdnVEbtntext1.Value,
                        VEbtntext2 = PhdnVEbtntext2.Value,
                        VEbtntext3 = PhdnVEbtntext3.Value,
                        VEbtntext4 = PhdnVEbtntext4.Value,
                        VEbtntext5 = PhdnVEbtntext5.Value,
                        VEbtntext6 = PhdnVEbtntext6.Value,
                        VEbtntext7 = PhdnVEbtntext7.Value,
                        VEbtntext8 = PhdnVEbtntext8.Value,

                        RailTicketInnerHeading = hdnRailTicketInnerHeading.Value,
                        RailTicketSubInnerLower = hdnRailTicketSubInnerLower.Value,
                        LeftBannerImage = hdnLeftBannerImage.Value,
                        RightBanner = hdnRightBanner.Value
                    };

                    var chkUrl = _master.GetPageDetailsByUrl(lblUrl.Text.Trim());
                    if (chkUrl == null)
                    {
                        int res = _master.AddPage(page);
                        ResetRoute();
                        if (res > 0)
                        {
                            ShowMessage(1, "Page details added successfully.");
                            ClearControls();
                            Tab = "1";
                        }
                    }
                    else
                    {
                        ShowMessage(2, "Url already Assigned");
                    }
                }
                else if (btnSubmit.CommandName == "Update")
                {
                    var page = new tblPage
                    {
                        ID = Guid.Parse(Request["edit"]),
                        SiteID = Guid.Parse(ddlSiteNm.SelectedValue),
                        PageName = txtPageName.Text.Trim(),
                        Url = pageUrl,
                        NavigationID = Guid.Parse(ddlNavigation.SelectedValue),
                        LayoutID = Guid.Parse(ddlLayout.SelectedValue),
                        IsActive = chkActive.Checked,

                        BannerIDs = hdnBanner.Value,
                        PageHeading = pageHeading,
                        PageContent = pageContent,
                        FooterImg = hdnFooter.Value,
                        RightPanelImgID = hdnRight.Value,
                        RightPanelNavUrl = hdnRightPanelUrl.Value,
                        RightPanel1 = hdnRtPanel1.Value,
                        RightPanel2 = hdnRtPanel2.Value,
                        ContactPanel = hdnContactPanel.Value,
                        ContactCall = hdnContactCall.Value,

                        HomePageHeading1 = hdnHeading1.Value,
                        HomePageHeading2 = hdnHeading2.Value,
                        HomePageDescription1 = hdnDescription1.Value,
                        HomePageDescription2 = hdnDescription2.Value,

                        //ProductHeading = hdnPrdHead.Value,
                        //ProductDesc = hdnPrdDesc.Value,
                        //ProductRtImgActive = Convert.ToBoolean(Convert.ToInt32(boolProductRtImg)),
                        //ProductDescActive = Convert.ToBoolean(Convert.ToInt32(boolPrdContent)),
                        //ProductPanelActive = Convert.ToBoolean(Convert.ToInt32(boolPrdImagePanel)),
                        //ProductRtSliderActive = Convert.ToBoolean(Convert.ToInt32(boolPrdRtImageSlider)),

                        MetaTitle = txtMetaTitle.Text.Trim(),
                        Description = txtDescription.Text.Trim(),
                        Keyword = txtKeyWord.Text.Trim(),
                        FooterHeader = hdnfooterBlockheadertxt.Value,

                        HomeSearchText = pageHeading,
                        HomeCountryText = pageContent,
                        SiteType = SiteTypeValue,

                        VEbanner = PhdnVEbanner.Value,
                        VEptpimg = PhdnVEptpimg.Value,
                        VEmap = PhdnVEmap.Value,
                        VEmaps1 = PhdnVEmaps1.Value,
                        VEmaps2 = PhdnVEmaps2.Value,

                        VEmaplink = PhdnVEmaplink.Value,
                        VEmaps1link = PhdnVEmaps1link.Value,
                        VEmaps2link = PhdnVEmaps2link.Value,

                        VEpass = PhdnVEpass.Value,
                        VEtrain = PhdnVEtrain.Value,
                        VEnewadventure = PhdnVEnewadventure.Value,
                        VEbtncontactcall = PhdnVEbtncontactcall.Value,
                        VEbtncontactbook = PhdnVEbtncontactbook.Value,
                        VEbtncontactlive = PhdnVEbtncontactlive.Value,
                        VEbtncontactemail = PhdnVEbtncontactemail.Value,
                        VEbtntext1 = PhdnVEbtntext1.Value,
                        VEbtntext2 = PhdnVEbtntext2.Value,
                        VEbtntext3 = PhdnVEbtntext3.Value,
                        VEbtntext4 = PhdnVEbtntext4.Value,
                        VEbtntext5 = PhdnVEbtntext5.Value,
                        VEbtntext6 = PhdnVEbtntext6.Value,
                        VEbtntext7 = PhdnVEbtntext7.Value,
                        VEbtntext8 = PhdnVEbtntext8.Value,

                        RailTicketInnerHeading = hdnRailTicketInnerHeading.Value,
                        RailTicketSubInnerLower = hdnRailTicketSubInnerLower.Value,
                        LeftBannerImage = hdnLeftBannerImage.Value,
                        RightBanner = hdnRightBanner.Value
                    };

                    int res = _master.UpdatePage(page);
                    ResetRoute();
                    if (res > 0)
                    {
                        ShowMessage(1, "Page details updated successfully.");
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                        ClearControls();
                        Tab = "1";
                    }
                }
                _siteID = Master.SiteID;
                SiteSelected();
                BindPageDetail(_siteID);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pages.aspx");
        }

        protected void ddlNavigation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Tab = "2";
                ShowMessage(0, "");
                if (ddlSiteNm.SelectedValue != "-1")
                {
                    GetRootUrl(Guid.Parse(ddlSiteNm.SelectedValue));
                    if (ddlNavigation.SelectedIndex > 0)
                    {
                        var result = _master.GetPageByNavID(Guid.Parse(ddlNavigation.SelectedValue), Guid.Parse(ddlSiteNm.SelectedValue));
                        if (result != null)
                        {
                            ShowMessage(2, "Menu already Assigned");
                            ddlNavigation.SelectedValue = "-1";
                        }
                        else
                        {
                            ddlLayout.Items.Clear();
                            BindDdlLayout();
                            if (ddlNavigation.SelectedItem.Text == "Home")
                            {
                                var layoutid = _db.tblPageLayouts.FirstOrDefault(x => x.LayoutName == "HomePage");
                                if (layoutid != null)
                                    ddlLayout.SelectedValue = layoutid.ID.ToString();
                                SetLayout(Guid.Parse(ddlLayout.SelectedValue));
                                ddlLayout.Enabled = false;
                            }
                            else
                            {
                                imgTemplate.ImageUrl = "";
                                myIframe.Visible = false;
                                ddlLayout.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        ddlLayout.Items.Clear();
                        myIframe.Visible = false;
                        imgTemplate.ImageUrl = "";
                    }
                }
                else
                {
                    ddlNavigation.SelectedValue = "-1";
                    ShowMessage(2, "Select Site");
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void ddlLayout_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Tab = "2";
                ShowMessage(0, "");
                if (ddlLayout.SelectedIndex > 0)
                {
                    SetLayout(Guid.Parse(ddlLayout.SelectedValue));
                    GetRootUrl(Guid.Parse(ddlSiteNm.SelectedValue));
                }
                else
                {
                    myIframe.Visible = false;
                    imgTemplate.ImageUrl = "";
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void ddlSiteNm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Tab = "2";
                ShowMessage(0, "");
                if (ddlSiteNm.SelectedIndex > 0)
                {
                    BindNav(Guid.Parse(ddlSiteNm.SelectedValue));
                    GetRootUrl(Guid.Parse(ddlSiteNm.SelectedValue));
                    if (ddlNavigation.SelectedValue != "-1")
                    {
                        var result = _master.GetPageByNavID(Guid.Parse(ddlNavigation.SelectedValue),
                                                            Guid.Parse(ddlSiteNm.SelectedValue));
                        if (result != null)
                        {
                            Tab = "2";
                            ShowMessage(2, "Menu already Assigned");
                            ddlNavigation.SelectedValue = "-1";
                        }
                    }
                }
                else
                {
                    ddlNavigation.Items.Clear();
                    ddlLayout.Items.Clear();
                    myIframe.Visible = false;
                    imgTemplate.ImageUrl = "";
                    lblUrl.Text = "";
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void grdPages_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "ActiveInActive")
                {
                    _master.ActiveInactivePage(Guid.Parse(id.ToString()));
                }
                else if (e.CommandName == "Remove")
                {
                    _master.DeletePage(Guid.Parse(id.ToString()));
                }
                _siteID = Master.SiteID;
                SiteSelected();
                BindPageDetail(_siteID);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void ClearControls()
        {
            try
            {
                txtPageName.Text = string.Empty;
                lblUrl.Text = string.Empty;
                txtMetaTitle.Text = string.Empty;
                txtDescription.Text = string.Empty;
                txtKeyWord.Text = string.Empty;
                ddlSiteNm.SelectedIndex = 0;
                ddlNavigation.SelectedIndex = 0;
                ddlLayout.SelectedIndex = 0;
                myIframe.Visible = false;
                imgTemplate.Visible = false;
                ddlNavigation.Items.Clear();
                ddlLayout.Items.Clear();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
        
        void SiteSelected()
        {
            try
            {
                if (!Page.IsPostBack)
                    ViewState["PreSiteID"] = _siteID;
                if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
                {
                    BindPageDetail(_siteID);
                    ViewState["PreSiteID"] = _siteID;
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void BindPageDetail(Guid siteId)
        {
            try
            {
                grdPages.DataSource = _master.GetPageList(siteId).OrderBy(x => x.PageName).ToList();
                grdPages.DataBind();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void BindPageForEdit(Guid id)
        {
            try
            {
                ddlSiteNm.Enabled = false;
                var result = (from pages in _db.tblPages
                              join st in _db.tblSites
                                  on pages.SiteID equals st.ID
                              where pages.ID == id
                              select new { pages, st }).FirstOrDefault();

                ddlSiteNm.SelectedValue = Guid.Parse(result.pages.SiteID.ToString()).ToString();
                BindNav(Guid.Parse(ddlSiteNm.SelectedValue));
                ddlNavigation.SelectedValue = Guid.Parse(result.pages.NavigationID.ToString()).ToString();
                txtPageName.Text = result.pages.PageName;
                ddlLayout.SelectedValue = result.pages.LayoutID.ToString();
                txtMetaTitle.Text = result.pages.MetaTitle;
                txtDescription.Text = result.pages.Description;
                txtKeyWord.Text = result.pages.Keyword;
                btnSubmit.CommandName = "Update";
                chkActive.Checked = Convert.ToBoolean(result.pages.IsActive);
                SetLayout(Guid.Parse(ddlLayout.SelectedValue));
                Session["url"] = result.pages.Url.Replace(" ", "-");
                ddlNavigation.Enabled = txtPageName.Text.ToLower() != "homepage";
                hdnUrl.Value = result.st.SiteURL;
                lblUrl.Text = result.pages.Url.Replace(" ", "-");
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void BindDdlSite()
        {
            try
            {
                ddlSiteNm.DataSource = _master.GetActiveSiteList();
                ddlSiteNm.DataTextField = "DisplayName";
                ddlSiteNm.DataValueField = "ID";
                ddlSiteNm.DataBind();
                ddlSiteNm.Items.Insert(0, new ListItem("--Site--", "-1"));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void BindDdlLayout()
        {
            try
            {
                //Layout
                ddlLayout.DataSource = _master.GetPageLayoutList().ToList();
                ddlLayout.DataTextField = "LayoutName";
                ddlLayout.DataValueField = "ID";
                ddlLayout.DataBind();
                ddlLayout.Items.Insert(0, new ListItem("--Layout--", "-1"));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void BindNav(Guid siteId)
        {
            try
            {
                Session["SITEIDVEPAGE"] = siteId;
                //Navigation
                ddlNavigation.DataSource = _master.GetActiveParentWebNavigationList(siteId);
                ddlNavigation.DataTextField = "Name";
                ddlNavigation.DataValueField = "ID";
                ddlNavigation.DataBind();
                ddlNavigation.Items.Insert(0, new ListItem("--Category--", "-1"));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        public void SetLayout(Guid id)
        {
            try
            {
                PageLayout = null;
                Guid SiteId = Guid.Parse(ddlSiteNm.SelectedValue);
                var siteDetails = _db.tblSites.FirstOrDefault(x => x.ID == SiteId && x.IsActive == true);
                if (siteDetails != null)
                {
                    if (siteDetails.LayoutType == 1)
                        PageLayout = _master.GetPageLayout(1, "STA", id);
                    else if (siteDetails.LayoutType == 2)
                        PageLayout = _master.GetPageLayout(2, "NewIR", id);
                    else if (siteDetails.LayoutType == 3)
                        PageLayout = _master.GetPageLayout(3, "OldIR", id);
                    else if (siteDetails.LayoutType == 4)
                        PageLayout = _master.GetPageLayout(4, "TravelCuts", id);
                    else if (siteDetails.LayoutType == 5)
                        PageLayout = _master.GetPageLayout(5, "Merit", id);
                    else if (siteDetails.LayoutType == 6)
                        PageLayout = _master.GetPageLayout(6, "Thailand", id);
                    else if (siteDetails.LayoutType == 7)
                        PageLayout = _master.GetPageLayout(7, "Singapore", id);
                    else if (siteDetails.LayoutType == 8)
                        PageLayout = _master.GetPageLayout(8, "Omega", id);
                    else if (siteDetails.LayoutType == 9)
                        PageLayout = _master.GetPageLayout(9, "NewIR", id);
                    else
                        PageLayout = _master.GetPageLayout(3, "OldIR", id);
                }

                if (PageLayout != null)
                {
                    imgTemplate.ImageUrl = PageLayout.ImageUrl;
                    lblFrameHead.Text = PageLayout.LayoutName;
                    myIframe.Visible = true;
                    myIframe.Attributes.Add("src", PageLayout.LayoutPath);
                    myIframe.Attributes.Add("width", PageLayout.Width.ToString());
                    myIframe.Attributes.Add("height", PageLayout.Height.ToString());
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public static string StringReplace(string strtext)
        {
            strtext = strtext.Replace(" ", "-");
            return strtext;
        }

        public void ResetRoute()
        {
            System.Web.Routing.RouteTable.Routes.Clear();
        }

        private void GetRootUrl(Guid siteId)
        {
            try
            {
                var rootUrl = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (rootUrl != null)
                {
                    if (rootUrl.SiteURL != null)
                        lblUrl.Text = rootUrl.SiteURL + txtPageName.Text.Trim();
                    hdnUrl.Value = rootUrl.SiteURL;
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }
}