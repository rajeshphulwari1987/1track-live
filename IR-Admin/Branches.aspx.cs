﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using Business;

namespace IR_Admin
{
    public partial class BranchPage : Page
    {
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            ShowMessage(0, null);
            if (!IsPostBack)
                BindBranchTreeView();
        }

        protected void BindBranchTreeView()
        {
            var db = new db_1TrackEntities();
            var query = (from m in db.tblBranches
                         orderby m.OfficeName
                         select new { ID = m.TreeID, ParentID = m.TreeParentBranchID, Text = m.OfficeName }).ToList();

            var dataSet = new DataSet();
            dataSet.Tables.Add("Table");
            dataSet.Tables[0].Columns.Add("ID", typeof(int));
            dataSet.Tables[0].Columns.Add("ParentID", typeof(int));
            dataSet.Tables[0].Columns.Add("Text", typeof(string));

            foreach (var item in query)
            {
                DataRow row = dataSet.Tables[0].NewRow();
                row["ID"] = item.ID;
                if (item.ParentID != null)
                {
                    row["ParentID"] = item.ParentID;
                }
                var nodeText = new StringBuilder();
                nodeText = nodeText.Append(item.Text);
                nodeText.Append(@"<INPUT id=""btnupload").Append(item.ID.ToString()).Append(@""" type=""image"" src=""images/uploadexc.png"" height=""18"" title=""Upload Sub Offices"" onclick=""UpLoadOffice(").Append(item.ID.ToString()).Append(@")"">&nbsp&nbsp");
                nodeText.Append(@"&nbsp<INPUT id=""btnedit").Append(item.ID.ToString()).Append(@""" type=""image""  src=""images/edit.png""  title=""Edit Office"" onclick=""editOffice(").Append(item.ID.ToString()).Append(@")"">&nbsp&nbsp");
                nodeText.Append(@"<INPUT id=""btnadd").Append(item.ID.ToString()).Append(@""" type=""image"" src=""images/office-add.png""  title=""Add Sub Office"" onclick=""addSubOffice(").Append(item.ID.ToString()).Append(@")"">&nbsp&nbsp");
                nodeText.Append(@"<INPUT id=""btndelete").Append(item.ID.ToString()).Append(@""" type=""image"" src=""images/delete.png""  title=""Delete Office"" onclick=""deleteOffice(").Append(item.ID.ToString()).Append(@")"">");

                row["Text"] = nodeText.ToString();
                dataSet.Tables[0].Rows.Add(row);
            }

            trBranch.DataSource = new HierarchicalDataSet(dataSet, "ID", "ParentID");
            trBranch.DataBind();
            trBranch.CollapseAll();
        }

        protected void btneditOffice_Click(object sender, EventArgs e)
        {
            try
            {
                var id = Convert.ToInt32(hdnBranchid.Value);
                Response.Redirect("BranchesAddEdit.aspx?id=" + id);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnDeleteOffice_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = _Master.DeleteBranch(int.Parse(hdnBranchid.Value));
                if (result)
                    ShowMessage(1, "Office deleted successfully.");
                BindBranchTreeView();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }

            tab = "1";
            ViewState["tab"] = "1";
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
        }

        protected void btnUploadBranches_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadBranches.aspx?id=" + hdnBranchid.Value);
        }

        protected void btnaddOffice_Click(object sender, EventArgs e)
        {
            try
            {
                var id = Convert.ToInt32(hdnBranchid.Value);
                Response.Redirect("BranchesAddEdit.aspx?treeid=" + id);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void lnkanew_Click(object sender, EventArgs e)
        {
            try
            {
                //btnUpdate.Text = "Save";
                //var rootnode = _Master.GetRootNode();
                //if (rootnode != null)
                //{
                //    var result = _Master.BranchListforEdit(Convert.ToInt32(rootnode.TreeID));
                //    //lblBranchFullPath.Text = result.PathName;
                //    hdnBranchid.Value = rootnode.TreeID.ToString();
                //    Response.Redirect("BranchesAddEdit.aspx?treeid=" + hdnBranchid.Value);
                //}
                //else
                //{
                //    //lblBranchFullPath.Text = "Add New Office";
                //    Response.Redirect("BranchesAddEdit.aspx");
                //}
                //tab = "2";
                //ViewState["tab"] = "2";
                //ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void trBranch_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                hdnBranchid.Value = trBranch.SelectedNode.Value;
                Response.Redirect("BranchesAddEdit.aspx?id=" + hdnBranchid.Value);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}
