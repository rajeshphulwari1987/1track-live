﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class STAAdventureRequest : System.Web.UI.Page
    {
        readonly private ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
        readonly private Masters _oMasters = new Masters();
        List<RepeaterListFaqItem> list = new List<RepeaterListFaqItem>();
        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid SiteID, _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            //Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";

            if (!Page.IsPostBack)
            {
                FillDayMonthYear();
                _SiteID = Master.SiteID;
                BindSite();
                BindGrid(_SiteID);
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;
            if (_SiteID == Guid.Parse("302d426f-6c40-4b71-a565-5d89d91c037e"))
                grdSTAAdventureRequest.DataSource = null;
            else
                grdSTAAdventureRequest.DataSource = _oManageInterRailNew.GettblPassQueryList(_SiteID);
            grdSTAAdventureRequest.DataBind();
            Tab = "1";
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var SiteId = Master.SiteID;
                bool result = _oManageInterRailNew.AddUpdatetblPassQuery(new tblPassQuery
                {
                    Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value),
                    SiteId = Guid.Parse(ddlSite.SelectedValue),
                    Name = txtFName.Text.Trim() + " " + txtLName.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    Phone = txtPhone.Text.Trim(),
                    TellUs = txtTellUs.Text.Trim(),
                    Dob = ddlDay.SelectedItem.Text.Trim() + "-" + ddlMonth.SelectedItem.Text.Trim() + "-" + ddlYear.SelectedItem.Text.Trim(),
                    IsEmail = chkIsActive.Checked ? true : false,
                    CreatedOn = DateTime.Now
                });
                Cleardata();
                BindGrid(SiteId);
                ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Record added successfully." : "Record updated successfully.");
                hdnId.Value = string.Empty;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("STAAdventureRequest.aspx");
        }

        public void FillDayMonthYear()
        {
            ddlDay.Items.Insert(0, new ListItem("DD", "0"));
            ddlMonth.Items.Insert(0, new ListItem("MM", "0"));
            ddlYear.Items.Insert(0, new ListItem("YYYY", "0"));
            for (int i = 01; i <= 31; i++)
            {
                ddlDay.Items.Add(i.ToString());
            }
            for (int i = 01; i <= 12; i++)
            {
                ddlMonth.Items.Add(i.ToString());
            }
            for (int i = 1950; i <= 2015; i++)
            {
                ddlYear.Items.Add(i.ToString());
            }
        }

        protected void grdSTAAdventureRequest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                _oManageInterRailNew.DeletetblPassQuery(id);
                BindGrid(Master.SiteID);
                ShowMessage(1, "Record delete successfully.");
            }

            else if (e.CommandName == "UpdateRecord")
            {
                var result = _oManageInterRailNew.GettblPassQueryById(id);
                if (result != null)
                {
                    ddlSite.SelectedValue = result.SiteId.ToString();
                    ddlSite.Enabled = false;
                    string name = result.Name.ToString();
                    string[] san = name.Split(' ');
                    txtFName.Text = san[0];
                    txtLName.Text = san[1];

                    txtEmail.Text = result.Email;
                    txtPhone.Text = result.Phone;
                    txtTellUs.Text = result.TellUs;
                    chkIsActive.Checked = result.IsEmail.Value;
                    hdnId.Value = result.Id.ToString();
                    Tab = "2";

                    string dob = result.Dob.ToString();
                    string[] sa = dob.Split('-');

                    ddlDay.SelectedValue = sa[0];
                    ddlMonth.SelectedValue = sa[1];
                    ddlYear.SelectedValue = sa[2];
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void Cleardata()
        {
            txtEmail.Text = string.Empty;
            txtFName.Text = string.Empty;
            txtLName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtTellUs.Text = string.Empty;
            ddlSite.SelectedIndex = 0;
            ddlSite.Enabled = true;
            ddlDay.SelectedIndex = 0;
            ddlMonth.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            chkIsActive.Checked = false;
        }
    }
}