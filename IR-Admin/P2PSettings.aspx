﻿<%@ Page Title="P2P setting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="P2PSettings.aspx.cs" Inherits="IR_Admin.P2PSettings" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        P2P Integration Setting</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="p2psettings.aspx" class="current">List</a></li>
            <li><a id="aNew" href="p2psettings.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdp2pSetting" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnRowCommand="grdp2pSetting_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Site Name">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="P2PWidget Page">
                                <ItemTemplate>
                                    <%#Eval("P2PWidgetPage")%>
                                </ItemTemplate>
                                <ItemStyle Width="18%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="P2PResult Page">
                                <ItemTemplate>
                                    <%#Eval("P2pTrainResultsPage")%>
                                </ItemTemplate>
                                <ItemStyle Width="18%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="P2PLoyatyCard Page">
                                <ItemTemplate>
                                    <%#Eval("P2PLoyaltyCardInfoPage")%>
                                </ItemTemplate>
                                <ItemStyle Width="18%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Directory Path">
                                <ItemTemplate>
                                    <%#Eval("DirectoryPath")%>
                                </ItemTemplate>
                                <ItemStyle Width="21%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="ImgUpdateRecord" AlternateText="UpdateRecord"
                                        ToolTip="Update Record" CommandArgument='<%#Eval("ID")%>' CommandName="UpdateRecord"
                                        ImageUrl="~/images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                Site:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" ClientIDMode="Static" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                P2PWidget Page:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtP2PWidgetPage" runat="server" Width="500" MaxLength="200" />
                                &nbsp;<asp:RequiredFieldValidator ID="req1" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtP2PWidgetPage" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                P2PResult Page:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtP2pTrainResultsPage" runat="server" Width="500px" MaxLength="200" />
                                &nbsp;<asp:RequiredFieldValidator ID="req2" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtP2pTrainResultsPage" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                               P2PLoyaltyCard Page:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtP2PLoyaltyCardInfoPage" runat="server" Width="500px" MaxLength="200" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                               Directory Url:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtDirectoryUrl" runat="server" Width="500px" MaxLength="200" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                               Is Active:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2">
                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
