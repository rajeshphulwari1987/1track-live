﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_MeritContact.aspx.cs"
    Inherits="IR_Admin.Manage_MeritContact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title>STA Rail | STA Travel Rail</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="Styles/meritcss/assets/css/main-agent.css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:400" rel="stylesheet"
        type="text/css">
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="//www.statravel.com/static/us_division_web_live/css/jquery.mmenu.all.css"
        type="text/css" />
    <link href="//www.statravel.com/static/us_division_web_live/css/partner.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/meritcss/assets/img/icons/touch-icon.png" rel="apple-touch-icon-precomposed"
        sizes="180x180" type="image/png" />
    <link href="Styles/meritcss/assets/img/icons/favicon.png" rel="shortcut icon" type="image/png" />
    <link href="Styles/meritcss/assets/img/icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <style type="text/css">
        .starail-HomeHero img
        {
            width: 100%;
        }
    </style>
    <link href="Styles/meritcss/assets/css/layout.css" rel="stylesheet" />
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
   
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
            $('#txtRtPanel').redactor({ iframe: true, minHeight: 200 });

            $("input[class=rdrtID]").attr('name', "img");

            //Edit banner images
            $(".editbtn1").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });
            $(".editbtn6").click(function () {
                $("#divHeading").slideToggle("slow");
            });
            $(".editbtn7").click(function () {
                $("#divContent").slideToggle("slow");
            });

            //----------Edit heading---------//
            $(".editbtn6").click(function () {
                var value;
                $("#divHeading").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdHead').contents().find('body').html(value);
            });

            $(".editbtn7").click(function () {
                var value;
                $("#divContent").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent').contents().find('body').html(value);
            });


            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRight").hide();
                }
                else if ($(this).attr("rel") == "ContentRightTxt") {
                    $("#ContentRightTxt").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

            //-------Edit right panel images-------//
            $(".editRtPanel").click(function () {
                $("#ContentRight").slideToggle("slow");
                $('.hdnRight').val($(this).attr("rel"));
            });

            //------Save right panel images------//
            $("#btnSaveRtPanel").click(function () {
                var imagename;
                $('div#ContentRight input[type=radio]').each(function () {
                    if ($('.hdnRight').val() == "rtPanelImgHome") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactHome").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgCall") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactCall").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgEmail") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactEmail").attr("src", imagename);
                        }
                    }
                    $("#ContentRight").hide();
                });
            });

            //-------Edit right panel images text-------//
            $(".editRtPanelText").click(function () {
                $("#ContentRightTxt").slideToggle("slow");
                $('.hdnrtPanelTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "contactHomeTxt") {
                    value = $('#contactHomeTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactCallTxt") {
                    value = $('#contactCallTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactEmailTxt") {
                    value = $('#contactEmailTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
            });

            //-----Save right panel text------//
            $(".btnSaveRttxt").click(function () {
                var value = $('textarea[name=txtRtPanel]').val();
                if ($('.hdnrtPanelTxt').val() == "contactHomeTxt") {
                    if (value != "") {
                        $('#contactHomeTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactCallTxt") {
                    if (value != "") {
                        $('#contactCallTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactEmailTxt") {
                    if (value != "") {
                        $('#contactEmailTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                $("#ContentRightTxt").hide();
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <input type='hidden' id='sta-page-responsive' value='true' />
    <div id='sta-full-wrap'>
        <div id='sta-page-wrap'>
            <header class="sta-clearfix sta-header" role="banner" id="sta-header">
                <div class="starail-Nav-mobile starail-u-hideDesktop">
                </div>
                <div class="starail-Outer-wrap">
                    <div class="js-starail-nav-trigger starail-u-hideDesktop starail-Nav-mobileClose">
                    </div>

                    <div class='starail-Full-wrap'>
                        <div class='starail-Page-wrap'>
                            <header class="starail-clearfix starail-Header starail-u-hideMobile" role="banner">
                                <div class="starail-u-hideMobile starail-u-cf starail-Header-topRow" itemscope itemtype="http://schema.org/Organization">
                                    <div class="starail-Header-logo">
                                        <a itemprop="url" href="#">
                                            <img src="Styles/meritcss/assets/img/meritlogo.png" />
                                        </a>
                                    </div>

                                    <div class="starail-Header-contact">
                                        <p class="starail-Header-contact-title">Merit Travel</p>
                                        <h2 class="starail-Header-contact-phone" itemprop="telephone">1.800.667.2887</h2>
                                        <p class="starail-Header-contact-disclaimer"></p>
                                    </div>
                                </div>
                                <nav class="starail-u-cf starail-Nav-wrap" id="sta-nav-wrap">
                                    <ul class="starail-u-cf starail-Nav" role="navigation" id="starail-nav">
                                        <li>
                                            <a href="#"> Rail Home   </a>
                                        </li>
                                        <li><a href="#">Print Queue </a> </li>
                                        <li><a href="#">FOC / AD75</a></li>
                                        <li>
                                            <a href="#"> FAQ   </a>
                                        </li>
                                        <li>
                                            <a href="#"> Feedback </a>
                                        </li>
                                        <li><a href="#"> Security  </a></li>
                                    </ul>
                                </nav>
                            </header>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: STA Global Header -->
            <main class="starail-Wrapper starail-Wrapper--main" role="main">
                <div class="starail-Grid starail-Grid--mobileFull">
                    <div class="starail-Grid-col starail-Grid-col--nopadding ">
                        <div class="starail-Grid--mobileFull imgfullbox editbaner">
                            <div class="editbtn1"><a class="edit-btn" href="#">Edit</a></div>
                            <div class="starail-Grid-col starail-Grid-col--nopadding">
                                <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                                    <img src='http://admin.1tracktest.com/CMSImages/Japan-electric-train.png' class="starail-HomeHero-img dots-header"
                                         id="imgMainBanner" runat="server" alt="." border="0" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Section starail-Section--nopadding">
                            <div style="display:none;">
                                <div class="starail-Switcher">

                                    <ul class="starail-Switcher-tabs">
                                        <li class="starail-Switcher-tab starail-Switcher-tab--active">
                                            <a href="#starail-passes" class="js-starail-Switcher-trigger">Rail Passes</a>
                                        </li>
                                        <li class="starail-Switcher-tab">
                                            <a href="#starail-tickets" class="js-starail-Switcher-trigger">Rail Tickets</a>
                                        </li>
                                    </ul>
                                    <div id="starail-passes" class="starail-Switcher-content">
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>
                                            <img src="http://lorempixel.com/220/220/city" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>
                                            <img src="http://lorempixel.com/220/220/nature" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>
                                            <img src="http://lorempixel.com/220/220/city" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div id="starail-tickets" class="starail-Switcher-content starail-Switcher-content--hidden">
                                        <div class="starail-SearchTickets">
                                            <h2 class="starail-SearchTickets-title starail-u-alpha">Tell us <span class="starail-u-hideMobile">more </span>about your trip</h2>
                                            <div class="starail-Box starail-Box--whiteMobile starail-Box--noPaddingBottomMobile">
                                                <div action="#" method="post" class="starail-Form starail-Form--onBoxNarrow starail-SearchTickets-form">
                                                    <div class="starail-Form-row starail-SearchTickets-destination">
                                                        <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--start">
                                                            <label for="starail-startlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">From</label>
                                                            <div class="starail-SearchTickets-destinationInput">
                                                                <input class="starail-Form-input" type="text" name="starail-startlocation" id="starail-startlocation" placeholder="Enter a start location" value="" />
                                                            </div>
                                                        </div>
                                                        <div class="starail-DestinationIcon">
                                                            <div class="starail-DestinationIcon-line"></div>
                                                            <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled"></div>
                                                            <div class="starail-DestinationIcon-circle"></div>
                                                        </div>
                                                        <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--end">
                                                            <label for="starail-endlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">To</label>
                                                            <div class="starail-SearchTickets-destinationInput">
                                                                <input class="starail-Form-input" type="text" name="starail-endlocation" id="starail-endlocation" placeholder="Enter a destination" value="" />
                                                            </div>
                                                        </div>
                                                        <a href="#" class="starail-SearchTickets-switch-trigger"><i class="starail-Icon starail-Icon-reverse"></i><span class="starail-u-visuallyHidden">Switch direction</span></a>
                                                    </div>
                                                    <div class="starail-Form-row">
                                                        <label for="" class="starail-Form-label starail-u-invisible starail-u-visuallyHiddenMobile">Journey Type</label>
                                                        <div class="starail-u-cf starail-Form-switchRadioGroup">
                                                            <label for="starail-oneway">
                                                                <input type="radio" id="starail-oneway" name="journey" value="one way" checked>
                                                                <span>One way</span>
                                                            </label>
                                                            <label for="starail-return">
                                                                <input type="radio" id="starail-return" name="journey" value="return">
                                                                <span>Return</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-u-cf">
                                                        <label for="leaving" class="starail-Form-label">Leaving</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                                            <div class="starail-Form-datePicker">
                                                                <input class="starail-Form-input" name="leaving" id="leaving" placeholder="DD/MM/YYYY" value="" />
                                                                <i class="starail-Icon-datepicker"></i>
                                                            </div>
                                                            <select class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last" name="outbound-time" id="outbound-time">
                                                                <option value="09:00">09:00</option>
                                                                <option value="09:15">09:15</option>
                                                                <option value="09:30">09:30</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="starail-Form-row starail-u-cf">
                                                        <label for="returning" class="starail-Form-label">Returning</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                                            <div class="starail-Form-datePicker">
                                                                <input class="starail-Form-input" type="text" name="returning" id="returning" placeholder="DD/MM/YYYY" value="" />
                                                                <i class="starail-Icon-datepicker"></i>
                                                            </div>
                                                            <select class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last" name="inbound-time" id="inbound-time">
                                                                <option value="09:00">09:00</option>
                                                                <option value="09:15">09:15</option>
                                                                <option value="09:30">09:30</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                                                        <label for="" class="starail-Form-label">Who's going?</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-adult" id="starail-adult">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Adults (26-65 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-children" id="starail-children">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-children">Children (under 17 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-youths" id="starail-youths">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Youths (17-25 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-seniors" id="starail-seniors">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Seniors (over 66 at time of travel)</label>
                                                            </div>
                                                        </div><!-- starail-Form-selectGroup -->
                                                    </div>
                                                    <hr />
                                                    <div class="starail-Form-row">
                                                        <button type="submit" class="starail-Button starail-Form-button starail-Form-button--primary">Search Tickets</button>
                                                    </div>
                                                </div><!-- .starail-Form -->
                                            </div>
                                            <div class="starail-Box starail-Box--whiteMobile starail-ContactForm">
                                                <h3>We don't have that journey online</h3>
                                                <p>Call us now on <strong>0871 984 7783</strong> to book! Or fill in some details and we'll call you back:</p>
                                                <div action="#" method="post" class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
                                                    <div class="starail-Form-row">
                                                        <label for="starail-firstname" class="starail-Form-label">Name <span class="starail-Form-required">*</span></label>

                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input class="starail-Form-input" type="text" name="starail-firstname" id="starail-firstname" placeholder="First name" value="" />
                                                            </div>
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input class="starail-Form-input" type="text" name="starail-lastname" id="starail-lastname" placeholder="Last name" value="" />
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <label for="starail-email" class="starail-Form-label">Email <span class="starail-Form-required">*</span></label>
                                                        <div class="starail-Form-inputContainer">
                                                            <input class="starail-Form-input starail-Form-error" type="email" name="starail-email" id="starail-email" placeholder="Email" value="" /><!-- .starail-Form-error class used for errors -->
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <label for="starail-phone" class="starail-Form-label">Phone <span class="starail-Form-required">*</span></label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div><input class="starail-Form-input" type="tel" name="starail-phone" id="starail-phone" placeholder="Phone" value="" /></div>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-ContactForm-contactPreference">
                                                        <label for="" class="starail-Form-label starail-ContactForm-contactPreferenceLabel">How would you prefer us to contact you?</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input type="radio" id="starail-preference-email" name="starail-contact-preference" value="email" checked>
                                                                <label for="starail-preference-email">Email</label>
                                                            </div>
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input type="radio" id="starail-preference-phone" name="starail-contact-preference" value="phone">
                                                                <label for="starail-preference-phone">Phone</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="starail-Form-row">
                                                        <button type="submit" class="starail-Button starail-Form-button">Help Me Book!</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Box starail-Box--whiteMobile starail-ContactForm ">
                                <div style="width:100%;" id="MainContent_divSta ">
                                </div>
                                <h3 class="editbaner">
                                    <div id="ContentHead" runat="server">
                                        <h1><span id="lblHead">Contact us at STA Travel<br></span></h1>

                                    </div>
                                    <div class="editbtn6"><a class="edit-btn" href="#">Edit</a></div>
                                </h3>
                               
                                <div class="editbaner" >
                                    <div id="ContentText" runat="server"><p>
                                       If you have any rail related question about any of our products featured on these pages, please do not hesitate to contact us.</span>
                                    </p></div>
                                        <div class="editbtn7"><a class="edit-btn" href="#">Edit</a></div>
                                </div>
                                <br>
                                <p></p>
                                <div class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
                                   <div class="floatt starail-u-size8of12 contact-img">
                                        <img src="Styles/meritcss/assets/img/contactform.jpg">
                                    </div>
                                        <div class="floatt starail-u-size4of12 pl10 contact-txt">
                                       <div class="address-block" id="addBlock" runat="server">


                                            <div class="fullrow box1">
                                                <div id="rtPanelImgHome" class="cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="rtPanelImgHome" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactHome" runat="server" src="http://admin.1tracktest.com/CMSImages/home-blue.png">
                                                            <input type="hidden" id="Hidden3" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
												<div class="con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="contactHomeTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactHomeTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span  id="lblRtPanelHead"><a href="http://www.statravel.com.au/stores.htm">Contact your nearest store</a></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="fullrow box2">
                                                <div id="rtPanelImgCall" class=" cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="rtPanelImgCall" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactCall" runat="server" src="http://admin.1tracktest.com/CMSImages/icon-call-blue.png">
                                                            <input type="hidden" id="Hidden6" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="contactCallTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactCallTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span id="Label1">phone: 134 782</span>
                                                        </span>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="fullrow box3">
                                                <div id="rtPanelImgEmail" class=" cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="rtPanelImgEmail" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactEmail" runat="server" src="http://admin.1tracktest.com/CMSImages/icon-email.png">
                                                            <input type="hidden" id="Hidden2" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="contactEmailTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactEmailTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span id="Label3">email: <a href="mailto:webquotes@statravel.com.au">webquotes@statravel.com.au</a></span>
                                                        </span><br>
                                                    </div>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <hr>
                                    <img src="Styles/meritcss/assets/img/submitbtn.jpg" alt="">
                                    <div class="starail-Form-row" style="display:none;">
                                        <input type="submit" class="starail-Button starail-Form-button" id="MainContent_btnSubmit" onclick="return SaveContactUs();WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$btnSubmit&quot;, &quot;&quot;, true, &quot;vs&quot;, &quot;&quot;, false, false))" value="SUBMIT" name="ctl00$MainContent$btnSubmit" data-ga-category="http://staau.1tracktest.com/Contact-Us" data-ga-action="click" data-ga-label="staau.1tracktest.com">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </main>
        </div>
    </div>
    <!-- END: STA Global Footer -->
    <script type="text/javascript">
        window.jQuery || document.write('<script src="Styles/meritcss/assets/js/vendor/jquery-1.11.1.min.js"><\/script>')
    </script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"
        type="text/javascript"></script>
    <!-- STA GLOBAL BOTTOMJS -->
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/partner.js"
        type="text/javascript"></script>
    <script type="text/javascript" src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.hammer.min.js"></script>
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.mmenu-partner.js"
        type="text/javascript"></script>
    <!-- /STA GLOBAL BOTTOMJS -->
    <script src="Styles/meritcss/assets/js/main.min.js" type="text/javascript"></script>
    <footer class="starail-Footer">
        <div class="starail-Footer-outerWrap">
            <div class="starail-Footer-wrap">
               <section class="starail-Footer-links">
                    <p>
                        <a href='#'> Home </a>  <span>| </span>
                        <a href='#'> Contact Us </a>  <span>| </span>
                        <a href='#'> About Us </a>  <span>| </span>
                        <a href='#'> Booking Conditions </a>  <span>| </span>
                        <a href='#'> Privacy Policy </a>  <span>| </span>
                        <a href='#'> Conditions of Use </a>  <span> </span>
                    </p>
                </section>
                <p>
                    © Copyright International Rail Ltd. 2015 - All rights reserved. A company registered in England and Wales, company number: 3060803 with registered offices at International Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire. SO53 4AR.
                </p>
            </div>
        </div>
    </footer>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 141px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="padding-right: 50px; float: right; padding-top: 20px;">
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 240px; top: 141px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 240px; top: 141px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentRight" class="PopUpSampleIMG" style="display: none; width: 300px;
        height: auto; top: 637px; left: 189px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll">
            <asp:DataList ID="dtRtPanel" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="18px"
                        Height="18px" CssClass="bGray" />
                    <br />
                    <input id="rdrtID" class="rdrtID" runat="server" type="radio" name="img" value='<%#Eval("ImagePath")%>'
                        style="margin: 2px 0 2px 0" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="float: right; padding-top: 10px; padding-right: 20px;">
            <input type="button" id="Button2" value="Cancel" class="button btnClose" rel="ContentRight" />
            <input type="button" id="btnSaveRtPanel" value="Save" class="button btnSaveRtPanel"
                rel="ContentRight" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentRightTxt" style="display: none; left: 88px;
        top: 635px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtRtPanel" name="txtRtPanel" cols="10" rows="5" class="rdRight"></textarea>
                        <input type="hidden" class="hdnrtPanelTxt" id="Hidden7" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="float-rt" style="float: right; padding-top: 10px; padding-right: 20px;">
            <table>
                <tr>
                    <td>
                        <input type="button" id="Button3" value="Cancel" class="button btnClose" rel="ContentRightTxt" />
                        <input type="button" id="btnSaveRttxt" value="Save" class="button btnSaveRttxt" rel="ContentRightTxt" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
