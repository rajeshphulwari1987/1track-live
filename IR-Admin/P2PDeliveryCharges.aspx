﻿<%@ Page Title="Delivery Charges " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="P2PDeliveryCharges.aspx.cs" Inherits="IR_Admin.P2PdeliveryCharges" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">   
//        function pageLoad(sender, args) {$('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 });}

        $(function () {                  
             if(<%=tab.ToString()%>=="1")
            {
                  $("ul.list").tabs("div.panes > div");
                  $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {              
              $("ul.tabs").tabs("div.inner-tabs-container > div");               
                          
            }         
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            $(".tabs a").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).parent("li").addClass("active");

            });
           
            if(<%=tab.ToString()%>=="2")
            {
            $(".list a").click(function () {
               
                $("ul.list").tabs("div.panes > div");
            });
            }
            
           
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 10000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        img
        {
            vertical-align: middle;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        P2P Delivery Charges</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="P2PDeliveryCharges.aspx" class="current">List</a></li>
            <li><a id="aNew" href="P2PDeliveryCharges.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdDeliveryCharges" runat="server" CellPadding="4" DataKeyNames="ID"
                        CssClass="grid-head2" OnRowCommand="grdDeliveryCharges_RowCommand" ForeColor="#333333"
                        GridLines="None" AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="Amount" HeaderText="Amount" />
                            <asp:BoundField DataField="Currecny" HeaderText="Currency" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='P2PDeliveryCharges.aspx?id=<%#Eval("ID")%>'>
                                        <img src="images/edit.png" style="margin-top: -6px;" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this record?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("id")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this Record?');" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="">
                    <div class="divMain">
                        <table class="tblMainSection">
                            <tr>
                                <td class="col" style="width: 300px;">
                                    Site:
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddlSite" runat="server" Width="205px" />
                                    &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                        ControlToValidate="ddlSite" ValidationGroup="CForm" InitialValue="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Name:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ReqName" runat="server" ControlToValidate="txtName"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Amount:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtAmount" runat="server" Text="0" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqAmount" runat="server" ControlToValidate="txtAmount"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                    <%--<asp:CompareValidator ID="CompareValidator1" ErrorMessage="*" CssClass="valdreq" ControlToValidate="txtAmount"
                                        runat="server" Operator="NotEqual" Type="Double" ValueToCompare="0" ValidationGroup="CForm"  />--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Currency:
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddlcurrency" runat="server" Width="205px">
                                    </asp:DropDownList>
                                    &nbsp;<asp:RequiredFieldValidator ID="reqfroCurrency" runat="server" ErrorMessage="*"
                                        ForeColor="Red" ControlToValidate="ddlcurrency" ValidationGroup="CForm" InitialValue="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Is Active?
                                </td>
                                <td class="col">
                                    <asp:CheckBox ID="chkactive" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                        Text="Submit" Width="89px" ValidationGroup="CForm" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="BtnCancel_Click"
                                        Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                        </>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
