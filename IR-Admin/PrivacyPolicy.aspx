﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="PrivacyPolicy.aspx.cs"
    Inherits="IR_Admin.PrivacyPolicy" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtConsent').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtInfo').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtUseinfo').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtDisc').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtPerson').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtCookies').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtSecurity').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtPersonal').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtAlt').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".grid-sec2").find(".cat-outer-cms").hide();

            $("#divCmsCon").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsCon").next().show();
            });

            $("#divCmsInfo").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsInfo").next().show();
            });

            $("#divCmsUse").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsUse").next().show();
            });

            $("#divCmsDisc").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsDisc").next().show();
            });

            $("#divCmsPers").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsPers").next().show();
            });

            $("#divCmsCk").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsCk").next().show();
            });

            $("#divCmsSc").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsSc").next().show();
            });

            $("#divCmsPd").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsPd").next().show();
            });

            $("#divCmsAlt").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsAlt").next().show();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Privacy Policy</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="PrivacyPolicy.aspx" class="current">List</a></li>
            <li><a id="aNew" href="PrivacyPolicy.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdPolicy" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="grdPolicy_PageIndexChanging" OnRowCommand="grdPolicy_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <%#Eval("Title")%>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SiteName">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="30%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='PrivacyPolicy.aspx?id=<%#Eval("ID")%>'>
                                        <img src="images/edit.png" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this user?');" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <legend><b>Description</b></legend>
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Title:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTitle" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Width="450px" Rows="10" Columns="5" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfDesc" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is Active ?
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:CheckBox ID="chkIsActv" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><b>Contents</b></legend>
                                    <div id="divCmsCon">
                                        Your Consent
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtConsent" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsInfo">
                                        Information Collected
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtInfo" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsUse">
                                        Use of Your Information
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtUseinfo" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsDisc">
                                        Disclosure of Your Information
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtDisc" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsPers">
                                        Person under 18 years
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtPerson" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsCk">
                                        Cookies
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtCookies" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsSc">
                                        Security of your information
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtSecurity" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsPd">
                                        Your Personal Data
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtPersonal" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsAlt">
                                        Alterations to privacy statement
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtAlt" runat="server"></textarea>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
