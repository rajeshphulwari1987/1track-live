﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductListWithCommission.aspx.cs" Inherits="IR_Admin.Product.ProductListWithCommission" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link href="../Styles/colortip-1.0-jquery.css"
        rel="stylesheet" type="text/css" />
    <script src="../Scripts/colortip-1.0-jquery.js"
        type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.yellow').colorTip({ color: 'yellow' });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <asp:ValidationSummary ID="vsCommission" ValidationGroup="vg" DisplayMode="List" EnableClientScript="true" ShowSummary="false" ShowMessageBox="true" runat="server" />
            <h2>
                Product Commission</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <!-- tab "panes" -->
            <div class="full mr-tp1">
                <div class="panes">
                    <div id="divlist" runat="server">
                        <div class="crushGvDiv" style="font-size: 13px;">
                            <asp:HiddenField runat="server" ID="hdnCurrency" />
                            <%-- <div id="divNew" class="grid-sec2" runat="server" style="display: block;">--%>
                            <asp:HiddenField runat="server" ID="hdnProdNmId" />
                            <asp:HiddenField runat="server" ID="hdnProdId" />
                            <table class="tblMainSection" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: right;" class="valdreq">
                                        Fields marked with * are mandatory
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><b>Office </b></legend>
                                            <div class="cat-inner-alt" style="text-align: center; width: 100%; border-bottom: 2px dashed #FBDEE6;">
                                                <asp:DropDownList runat="server" ID="ddlOffices" AutoPostBack="True" Width="500px"
                                                    OnSelectedIndexChanged="ddlOffices_SelectedIndexChanged" />
                                                <asp:RequiredFieldValidator ID="resOffices" runat="server" ErrorMessage="*"
                                                    ValidationGroup="vg" InitialValue="0" ControlToValidate="ddlOffices" CssClass="valdreq"
                                                    SetFocusOnError="True" />
                                                
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><b>Category </b></legend>
                                            <asp:GridView ID="grvCategories" runat="server" AutoGenerateColumns="False" PageSize="10"
                                                CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                AllowPaging="True" OnPageIndexChanging="grvCategories_PageIndexChanging" 
                                                onrowdatabound="grvCategories_RowDataBound">
                                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                                <PagerStyle CssClass="paging"></PagerStyle>
                                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                                <EmptyDataTemplate>
                                                    Record not found.</EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <%#Eval("Name")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="65%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Commission(%)">
                                                        <ItemTemplate>
                                                            
                                                      <%--  <%#Eval("HaveDifferentComm")%>--%>
                                                            <asp:TextBox ID="txtMarkUp" runat="server" Text='<%#Eval("Commission")%>' Width="50px" MaxLength="5" style="float:left;" />
                                                            <asp:Label ID="lblHaveDifferentComm" runat="server" Text=''></asp:Label>
                                                            <asp:FilteredTextBoxExtender ID="ftbMarkUp" runat="server" TargetControlID="txtMarkUp"
                                                                ValidChars="0123456789." />
                                                             <asp:RequiredFieldValidator ID="reqMarkUp" runat="server" ControlToValidate="txtMarkUp"
                                                                    ErrorMessage="Please insert commission." Text="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                                <asp:RangeValidator ID="rangeCommission" ForeColor="Red" runat="server" ErrorMessage="Commission should not be grater then 100" ControlToValidate="txtMarkUp" Text="*" Type="Double" MinimumValue="0" MaximumValue="100" ValidationGroup="vg"></asp:RangeValidator>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle Width="20%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnFlag" runat="server" Value="1" />
                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ID")%>' />
                                                            <asp:Button ID="btnUpdate" ValidationGroup="vg" runat="server" Text="Update" CommandName="UpdateMarkUp"
                                                                OnClick="btnUpdate_Click" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <div>
                                            <fieldset class="grid-sec2">
                                                <legend><b>Product </b></legend>
                                                <div class="searchDiv" style="text-align:center">
                                                    <asp:DropDownList ID="ddlCategory" runat="server" Width="510px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                                                </div>
                                                <asp:GridView ID="grvProduct" runat="server" AutoGenerateColumns="False" PageSize="10"
                                                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                    OnPageIndexChanging="grvProduct_PageIndexChanging"
                                                    onrowdatabound="grvProduct_RowDataBound" AllowPaging="True">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <PagerStyle CssClass="paging"></PagerStyle>
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.</EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <%#Eval("Name")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="50%"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Category Name">
                                                            <ItemTemplate>
                                                                <%#Eval("Category")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="25%"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Commission(%)">
                                                            <ItemTemplate>
                                                             
                                                                <asp:TextBox ID="txtMarkUp" runat="server" Text='<%#Eval("Commission")%>' Width="50px" MaxLength="5" style="float:left;"  />
                                                                <asp:Label ID="lblHaveDifferentComm" runat="server" Text=''></asp:Label>
                                                                <asp:FilteredTextBoxExtender ID="ftbMarkUp" runat="server" TargetControlID="txtMarkUp"
                                                                    ValidChars="0123456789." />
                                                                <asp:RequiredFieldValidator ID="reqMarkUp" runat="server" ControlToValidate="txtMarkUp"
                                                                    ErrorMessage="Please insert commission." Text="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                                <asp:RangeValidator ID="rangeCommission" ForeColor="Red" runat="server" ErrorMessage="Commission should not be grater then 100" ControlToValidate="txtMarkUp" Text="*" Type="Double" MinimumValue="0" MaximumValue="100" ValidationGroup="vg"></asp:RangeValidator>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnFlag" runat="server" Value="2" />
                                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ID")%>' />
                                                                <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryId")%>' />
                                                                <asp:Button ID="btnUpdate" ValidationGroup="vg" runat="server" Text="Update" CommandName="UpdateMarkUp"
                                                                    OnClick="btnUpdate_Click" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </fieldset>
                                        </div>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Product Pricing</b></legend>
                                            <div class="searchDiv" style="text-align:center">
                                                <asp:DropDownList ID="ddlProducts" runat="server" AutoPostBack="True" 
                                                    OnSelectedIndexChanged="ddlProducts_SelectedIndexChanged" Width="510px" />
                                            </div>
                                            <asp:GridView ID="grvProductPrice" runat="server" AutoGenerateColumns="False" PageSize="50"
                                                CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                OnPageIndexChanging="grvProductPrice_PageIndexChanging" AllowPaging="True">
                                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                                <PagerStyle CssClass="paging"></PagerStyle>
                                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                                <EmptyDataTemplate>
                                                    Record not found.</EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Pass Code">
                                                        <ItemTemplate>
                                                            <%#Eval("PassTypeCode")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <%#Eval("TravellerValidName")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Class">
                                                        <ItemTemplate>
                                                            <%#Eval("ClassName")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="10%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Traveller">
                                                        <ItemTemplate>
                                                            <%#Eval("TravellerName")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="15%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Price">
                                                        <ItemTemplate>
                                                            <%#Eval("Currency")%> <%#Eval("Price")%>
                                                        </ItemTemplate>
                                                       
                                                          <ItemStyle Width="10%" HorizontalAlign="Right"></ItemStyle>
                                                         <HeaderStyle Width="10%" HorizontalAlign="Right" />
                                                    </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Commission(%)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtMarkUp" runat="server" Text='<%#Eval("Commission")%>' Width="50px" MaxLength="5" style="float:left;" />
                                                            <asp:FilteredTextBoxExtender ID="ftbMarkUp" runat="server" TargetControlID="txtMarkUp"
                                                                ValidChars="0123456789." />
                                                            <asp:RequiredFieldValidator ID="reqMarkUp" runat="server" ControlToValidate="txtMarkUp"
                                                                    ErrorMessage="Please insert commission." Text="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                                <asp:RangeValidator ID="rangeCommission" ForeColor="Red" runat="server" ErrorMessage="Commission should not be grater then 100" ControlToValidate="txtMarkUp" Text="*" Type="Double" MinimumValue="0" MaximumValue="100" ValidationGroup="vg"></asp:RangeValidator>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="8%" HorizontalAlign="Center"></ItemStyle>
                                                        <HeaderStyle Width="8%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnFlag" runat="server" Value="3" />
                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ID")%>' />
                                                             <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryId")%>' />
                                                             <asp:HiddenField ID="hdnProductID" runat="server" Value='<%#Eval("ProductId")%>' />
                                                            <asp:Button ID="btnUpdate" ValidationGroup="vg" runat="server" Text="Update" CommandName="UpdateMarkUp"
                                                                OnClick="btnUpdate_Click" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="9%" HorizontalAlign="Center"></ItemStyle>
                                                         <HeaderStyle Width="9%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- <asp:UpdateProgress ID="updProgress" runat="server" ClientIDMode="Static" DisplayAfter="0"
        DynamicLayout="True" AssociatedUpdatePanelID="upnlCategory">
        <ProgressTemplate>
            <div class=" modalBackground progessposition"> &nbsp;</div>
                <div class="progess-inner">
                    <img src='<%=ConfigurationSettings.AppSettings["HttpHost"]%>Images/ajax-loader.gif'
                        width="190px" height="16px" />
                    Processing..</div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
</asp:Content>

