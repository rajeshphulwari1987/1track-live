﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductCategoryURL.aspx.cs" Inherits="IR_Admin.Product.ProductCategoryURL" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                Rail pass category URL</h2>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="ProductCategoryURL.aspx" class="current">List</a></li>
                </ul>
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divlist" runat="server" style="display: block;">
                            <div class="crushGvDiv">
                                <asp:GridView ID="grvProductCategory" runat="server" AutoGenerateColumns="False"
                                    PageSize="10" CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None"
                                    Width="100%">
                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                    <PagerStyle CssClass="paging"></PagerStyle>
                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        Record not found.</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Category Name">
                                            <ItemTemplate>
                                                <%#Eval("Name")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="34%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="URL">
                                            <ItemTemplate>
                                             <a href='<%= siteURL%>rail-passes?cid=<%#Eval("ID")%>' target="_blank">  <%= siteURL%>rail-passes?cid=<%#Eval("ID")%></a>
                                            </ItemTemplate>
                                            <ItemStyle Width="75%"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
