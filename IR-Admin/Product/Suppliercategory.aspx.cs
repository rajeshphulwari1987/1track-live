﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class Suppliercategory : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        readonly ManageProduct _product = new ManageProduct();
        public string tab = string.Empty;
        List<RepeaterListItem> list = new List<RepeaterListItem>();
        db_1TrackEntities _db = new db_1TrackEntities();
        string CategoryImagePath = string.Empty;
        public List<SuppCName> SuppilerCatName = new List<SuppCName>();
        public string AdminUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        public int Count = 1;
        Guid _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                tab = "1";
                ListBind();
                PageLoadBind();
                PageLoadEvent();
            }
        }

        public void PageLoadEvent()
        {
            try
            {
                txtcommission.Attributes.Add("type", "number"); 
                ddlCurrency.DataSource = _master.GetCurrencyList();
                ddlCurrency.DataTextField = "Name";
                ddlCurrency.DataValueField = "ID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("---Currency---", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void PageLoadBind()
        {
            try
            {
                if (Count != 9999)
                {
                    if (Count == 1)
                        SuppilerCatName.Add(new SuppCName { Id=Guid.NewGuid()});
                    else
                        SuppilerCatName.Add(new SuppCName { Id=Guid.NewGuid(),No = Count, Name = null });
                }
                rptSCName.DataSource = SuppilerCatName.ToList();
                rptSCName.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ListBind()
        {
            try
            {
                grdSupplier.DataSource = _product.GetSupplierList().GroupBy(x=>new{x.Name,x.IsActive,x.Currency,x.Commission}).AsEnumerable().Select(x => 
                    new Supplist { 
                        Name = x.Key.Name, 
                        IsActive = x.Key.IsActive,
                        Commission = x.Key.Commission,
                        Currency = x.Key.Currency.HasValue?_db.tblCurrencyMsts.FirstOrDefault(o => o.ID == x.Key.Currency).Name:"",
                    }).ToList();
                grdSupplier.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdSupplier_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            DivError.Style.Add("display", "none");
            DivSuccess.Style.Add("display", "none");
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        string Name = e.CommandArgument.ToString();
                        var data = _product.GetSupplierList().Where(x => x.Name == Name).ToList();
                        var rowdata = data.FirstOrDefault();
                        txtSName.Text = rowdata.Name;
                        txtcommission.Text = rowdata.Commission;
                        ddlCurrency.SelectedValue = rowdata.Currency.HasValue ? rowdata.Currency.Value.ToString() : "0";
                        chkactive.Checked = rowdata.IsActive;
                        chkBritrail.Checked = rowdata.IsBritrail;
                        chkInterrail.Checked = rowdata.IsInterrail;
                        foreach (var item in data)
                        {
                            var obj = new SuppCName();
                            obj.No = Count;
                            obj.Id = item.Id;
                            obj.Name = item.Category;
                            SuppilerCatName.Add(obj);
                            Count++;
                        }
                        Count = 9999;
                        PageLoadBind();
                        btnSubmit.Text = "Update";
                        tab = "2";
                        break;
                    }
                case "ActiveInActive":
                    {
                        string Name = e.CommandArgument.ToString();
                        _product.ActiveSupplierList(Name);
                        tab = "1";
                        ListBind();
                        break;
                    }
            }
        }

        protected void grdSupplier_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSupplier.PageIndex = e.NewPageIndex;
            ListBind();
            tab = "1";
        }

        protected void grdSupplier_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var rptinnerlist = e.Row.FindControl("rptinnerlist") as Repeater;
                var lblName = e.Row.FindControl("lblName") as Label;
                rptinnerlist.DataSource = _product.GetSupplierList().Where(x => x.Name == lblName.Text).Select(x => new SuppCName { Name = x.Category }).ToList();
                rptinnerlist.DataBind();
            }
        }

        protected void rptSCName_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    Guid Id = Guid.Parse(e.CommandArgument.ToString());
                    var selecteddata = _product.GetSupplierList().FirstOrDefault(x => x.Id == Id);
                    if (selecteddata != null)
                    {
                        _product.DeleteSupplier(Id);
                        var data = _product.GetSupplierList().Where(x => x.Name == selecteddata.Name).ToList();
                        if (data.Count > 0)
                        {
                            var rowdata = data.FirstOrDefault();
                            txtSName.Text = rowdata.Name;
                            chkactive.Checked = rowdata.IsActive;
                            chkBritrail.Checked = rowdata.IsBritrail;
                            chkInterrail.Checked = rowdata.IsInterrail;
                            foreach (var item in data)
                            {
                                var obj = new SuppCName();
                                obj.No = Count;
                                obj.Id = item.Id;
                                obj.Name = item.Category;
                                SuppilerCatName.Add(obj);
                                Count++;
                            }
                        }
                    }
                    else
                    {
                        var SuppilerCatNameRemove = new List<SuppCName>();
                        foreach (RepeaterItem x in rptSCName.Items)
                        {
                            var obj = new SuppCName();
                            var txtSCName = x.FindControl("txtSCName") as TextBox;
                            var hdnID = x.FindControl("hdnID") as HiddenField;
                            obj.No = Count;
                            obj.Id = Guid.Parse(hdnID.Value);
                            obj.Name = txtSCName.Text;
                            SuppilerCatName.Add(obj);
                            Count++;
                        }
                        Count = 1;
                        SuppilerCatName.Remove(SuppilerCatName.FirstOrDefault(x => x.Id == Id));
                        foreach (var x in SuppilerCatName)
                        {
                            var obj = new SuppCName();
                            obj.No = Count;
                            obj.Id = x.Id;
                            obj.Name = x.Name;
                            SuppilerCatNameRemove.Add(obj);
                            Count++;
                        }
                        SuppilerCatName = SuppilerCatNameRemove;
                    }
                    Count = 9999;
                    PageLoadBind();
                    tab = "2";
                }
                else if (e.CommandName == "Add")
                {
                    foreach (RepeaterItem x in rptSCName.Items)
                    {
                        var obj = new SuppCName();
                        var txtSCName = x.FindControl("txtSCName") as TextBox;
                        var hdnID = x.FindControl("hdnID") as HiddenField;
                        obj.No = Count;
                        obj.Id = Guid.Parse(hdnID.Value);
                        obj.Name = txtSCName.Text;
                        SuppilerCatName.Add(obj);
                        Count++;
                    }
                    PageLoadBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Suppliercategory.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var DataList = new List<tblSupplier>();
            foreach (RepeaterItem x in rptSCName.Items)
            {
                var obj = new tblSupplier();
                var txtSCName = x.FindControl("txtSCName") as TextBox;
                var hdnID = x.FindControl("hdnID") as HiddenField;
                obj.Id = Guid.Parse(hdnID.Value);
                obj.Name = txtSName.Text;
                obj.Category = txtSCName.Text;
                obj.CreatedBy = AdminuserInfo.UserID;
                obj.CreatedOn = DateTime.Now;
                obj.IsActive = chkactive.Checked;
                obj.IsBritrail = chkBritrail.Checked;
                obj.IsInterrail = chkInterrail.Checked;
                obj.Currency = ddlCurrency.SelectedValue != "0" ?(dynamic) Guid.Parse(ddlCurrency.SelectedValue) : null;
                obj.Commission = txtcommission.Text;
                if (!DataList.Any(o => o.Category == obj.Category) && !string.IsNullOrEmpty(obj.Category))
                    DataList.Add(obj);
            }
            if (DataList.Count > 0)
            {
                _product.AddUpdateSupplier(DataList);
                txtSName.Text = "";
                ddlCurrency.SelectedValue= "0";
                txtcommission.Text = "";
                chkactive.Checked = false;
                Count = 1;
                ListBind();
                PageLoadBind();
                tab = "1";
                if (btnSubmit.Text == "Update")
                {
                    ShowMessage(1, "Supplier Record update successfully.");
                    btnSubmit.Text = "Submit";
                }
                else
                    ShowMessage(1, "Supplier Record add successfully.");
            }
            else
                ShowMessage(2, "Incorrect data! please try again.");
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }

    public class SuppCName
    {
        public Guid Id { get; set; }
        public int? No { get; set; }
        public string Name { get; set; }
    }

    public class Supplist
    {
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
        public string Commission { get; set; }
    }
}
