﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class ProductCommissionList : System.Web.UI.Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            FillCommissions("0");
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillCommissions("0");
                BindCategory();
            }
        }

        public void BindCategory()
        {
            _siteID = Master.SiteID;
            ddlCategory.DataSource = _oProduct.GetAssociateCategoryList(3, _siteID).Where(x => x.IsActive);
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("--Select Product Category--", "0"));
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            FillCommissions(ddlCategory.SelectedValue);
        }

        public void FillCommissions(string prdCat)
        {
            try
            {
                _siteID = Master.SiteID;
                var lst = _oProduct.GetAllCommission();
                if (lst.Any())
                {
                    var lstnew = (from x in lst
                                  select new
                                      {
                                          CommissionID = x.ID,
                                          x.Name,
                                          x.ProductName,
                                          x.CategoryID,
                                          Agent = x.UserName,
                                          //PassTypeCode1 = (x.PassTypeCode == null ? "-" : "<a href='#' title='Traveller Validity : " + x.Validity + " Class : " + x.ClassName + " Traveller : " + x.TravelerName + " Price : " + x.Price.ToString() + "' class='yellow colorTipContainer'>" + x.PassTypeCode.ToString() + "</a>"),
                                          x.Validity,
                                          x.ClassName,
                                          x.TravelerName,
                                          x.Price,
                                          x.PassTypeCode,
                                          x.Percentage
                                      }).Distinct();

                    if (prdCat == "0")
                        rptProductCommission.DataSource = lstnew.OrderBy(x => x.ProductName);
                    else
                    {
                        var catID = Guid.Parse(prdCat);
                        rptProductCommission.DataSource = lstnew.Where(x => x.CategoryID == catID);
                    }
                }
                else
                    rptProductCommission.DataSource = null;
                rptProductCommission.DataBind();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
                ShowMessage(2, "Unexpected Error");
            }
        }

        protected void rptProductCommission_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    bool res = _oProduct.DeleteProductCommission(Guid.Parse(e.CommandArgument.ToString()));
                    if (res)
                        ShowMessage(1, "Record deleted successfully");
                    FillCommissions("0");
                }
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
                ShowMessage(2, "Unexpected Error");
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}