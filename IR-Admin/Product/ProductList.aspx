﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductList.aspx.cs" Inherits="IR_Admin.Product.ProductList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }

        $(document).ready(function () {
            changeProductColor();
            searchData();
        });

        function changeProductColor() {
            $("#MainContent_grvProduct").first("tr").find("th").eq(1).css("display", "none");
            if ('True' == '<%=RoleAdmin%>') {
                $(".CircleImg").show();
                $("#MainContent_grvProduct").first("tr").find("th").eq(1).css("display", "block");
            }
            if ('True' == '<%=RoleSTAAdministrator%>') {
                $("#MainContent_grvProduct").first("tr").find("th").eq(1).css("display", "block");
                $("[class*=searchDiv],.list").hide();
            }
        }

        function searchData() {
            $('#divExpandCollapseAdvanceSearch').click(function () {
                try {
                    var div = $('#divAdvancedSearch');
                    var img = $('#imgUpDown');
                    if (!div.is(':visible')) {
                        div.slideDown(100, function () { img.attr('src', '../images/ar-up.png'); });
                        $('#hidAdvancedSearchToggle').val('show');
                    }
                    else {
                        div.slideUp(100);
                        img.attr('src', '../images/ar-down.png');
                        $('#hidAdvancedSearchToggle').val('');
                        $('#hidAdvanceSearchType').val('');
                        resetAdvancedSearch();
                    }
                }
                catch (ex) {
                    alert(ex);
                    return false;
                }
            });
        }

        function resetAdvancedSearch() {
            try {
                $('#rdbShortOrder').removeAttr("checked");
                $('#rdbTopPasses').removeAttr("checked");
                $('#rdbProductName').removeAttr("checked");
                $('#rdbCurrency').removeAttr("checked");
                $('#rdbSpecialandBestPasses').removeAttr("checked");
                $('#rdbItxPasses').removeAttr("checked");
                $('#rdbActive').removeAttr("checked");
                $('#rdbExpiry').removeAttr("checked");
                $('#divAgentSearch').hide();
            }
            catch (ex) {
                alert(ex);
            }
        }

        function SearchCriteriaChanged(obj) {
            if (obj.toString() != $('#hidAdvanceSearchType').val().toString()) {
                clearAdvanceSearch();
            }
            switch (obj) {
                case 'shortorder':
                    showHideSearchCriteria('shortorder');
                    $('#spnTextSearchLabel').text('Short Order : ');
                    $('#hidAdvanceSearchType').val('shortorder');
                    break;
                case 'product':
                    showHideSearchCriteria('product');
                    $('#spnTextSearchLabel').text('Product Name : ');
                    $('#hidAdvanceSearchType').val('product');
                    break;
                case 'currency':
                    showHideSearchCriteria('currency');
                    $('#spnTextSearchLabel').text('Currency : ');
                    $('#hidAdvanceSearchType').val('currency');
                    break;
                case 'top':
                    showHideSearchCriteria('top');
                    $('#spnTextSearchLabel').text('Top Pass : ');
                    $('#hidAdvanceSearchType').val('top');
                    break;
                case 'SpecialandBest':
                    showHideSearchCriteria('SpecialandBest');
                    $('#spnTextSearchLabel').text('Special, Best : ');
                    $('#hidAdvanceSearchType').val('SpecialandBest');
                    break;
                case 'itx':
                    showHideSearchCriteria('itx');
                    $('#spnTextSearchLabel').text('Itx Pass : ');
                    $('#hidAdvanceSearchType').val('itx');
                    break;
                case 'active':
                    showHideSearchCriteria('active');
                    $('#spnTextSearchLabel').text('Active : ');
                    $('#hidAdvanceSearchType').val('active');
                    break;
                case 'expiry':
                    showHideSearchCriteria('expiry');
                    $('#spnTextSearchLabel').text('Expiry : ');
                    $('#hidAdvanceSearchType').val('expiry');
                    break;
            }
        }

        function showHideSearchCriteria(obj) {
            switch (obj) {
                case 'shortorder':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').show(); $('#ddlProductName').hide(); $('#ddlCurrency').hide(); $('#ddlActive').hide(); $('#ddlExpiry').hide();
                    break;
                case 'product':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').show(); $('#ddlCurrency').hide(); $('#ddlActive').hide(); $('#ddlExpiry').hide();
                    break;
                case 'currency':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').hide(); $('#ddlCurrency').show(); $('#ddlActive').hide(); $('#ddlExpiry').hide();
                    break;
                case 'top':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').hide(); $('#ddlCurrency').hide(); $('#ddlActive').show(); $('#ddlExpiry').hide();
                    break;
                case 'SpecialandBest':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').hide(); $('#ddlCurrency').hide(); $('#ddlActive').show(); $('#ddlExpiry').hide();
                    break;
                case 'itx':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').hide(); $('#ddlCurrency').hide(); $('#ddlActive').show(); $('#ddlExpiry').hide();
                    break;
                case 'active':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').hide(); $('#ddlCurrency').hide(); $('#ddlActive').show(); $('#ddlExpiry').hide();
                    break;
                case 'expiry':
                    $('#divAgentSearch').show();
                    $('#ddlShortOrder').hide(); $('#ddlProductName').hide(); $('#ddlCurrency').hide(); $('#ddlActive').hide(); $('#ddlExpiry').show();
                    break;
            }
        }

        function clearAdvanceSearch() {
            $("#ddlShortOrder").val('-1');
            $("#ddlProductName").val('-1');
            $("#ddlCurrency").val('-1');
            $("#ddlActive").val('True');
            $("#ddlExpiry").val('1');
        }

        function holdOldData() {
            try {
                var div = $('#divAdvancedSearch');
                var img = $('#imgUpDown');
                if (!div.is(':visible')) {
                    div.slideDown(100, function () { img.attr('src', '../images/ar-up.png'); });
                    $('#hidAdvancedSearchToggle').val('show');
                    SearchCriteriaChanged($('#hidAdvanceSearchType').val());
                }
                else {
                    div.slideUp(100);
                    img.attr('src', '../images/ar-down.png');
                    $('#hidAdvancedSearchToggle').val('');
                    $('#hidAdvanceSearchType').val('');
                    resetAdvancedSearch();
                }
            }
            catch (ex) {
                alert(ex);
                return false;
            }
        }
    </script>
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            shortProductOrder();
        });
        function shortProductOrder() {
            var strorder;
            $("[id*=grvProduct]").sortable({
                items: 'tr.inner-row',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                },
                receive: function (e, ui) {

                    $(this).find("tbody").append(ui.item);
                },
                update: function (event, ui) {
                    var ittt = ui.item.context.innerHTML;
                    var currentProductID = ($(ittt).find('#hdnProductId').attr('value'));

                    var setOrder = '';
                    $('#MainContent_grvProduct tr').each(function (key, value) {
                        if ($(value).find('#hdnProductId').attr('value') != undefined && $(value).find('#hdnProductId').attr('value') != '') {
                            setOrder += $(value).find('#hdnProductId').attr('value') + ",";
                        }
                    });

                    var hostUrl = window.location.href + '/UpdateProductOrder';
                    var pageNo = $('#MainContent_grvProduct tbody tr.paging span').text();
                    if (pageNo == '')
                        pageNo = 1;
                    $.ajax({
                        type: "POST",
                        url: hostUrl,
                        contentType: "application/json; charset=utf-8",
                        data: "{'Value':'" + setOrder + "','SiteId':'" + $('#hdnSiteId').val() + "','pageNo':'" + pageNo + "','pageSize':'" + $('[id*=ddlPaging]').val() + "'}",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == "Yes") {
                                var i = 1;
                                var page = parseInt(pageNo);
                                if (page >= 1)
                                    i = (page - 1) * parseInt($('[id*=ddlPaging]').val()) + 1;

                                $('#MainContent_grvProduct tbody tr span.short-order').each(function (key, value) {
                                    $(this).text(i);
                                    i++;
                                });
                                $('.list-icon').hide();
                                $('#list-icon-' + currentProductID).show();
                                window.location = window.location;
                            }
                        },
                        failure: function (response) {
                            console.log(response.d);
                        }
                    });
                }
            });
        }
    </script>
    <style type="text/css">
        .CircleImg {
            display: none;
        }

        .imgcircle {
            height: 17px;
            width: 17px;
        }

        .templateimg {
            margin-left: -3px;
            height: 17px;
        }

        .collapse-tab {
            padding: 10px !important;
            margin-top: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hidAdvancedSearchToggle" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSiteId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidAdvanceSearchType" runat="server" ClientIDMode="Static" />
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>Product</h2>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="ProductList.aspx" class="current">List</a></li>
                    <li><a id="aNew" href="Product.aspx" class=" ">New/Edit</a> </li>
                </ul>
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" />
                        </div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="searchDiv" style="text-align: center">
                        <asp:DropDownList ID="ddlCategory" runat="server" Width="510px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                        <asp:DropDownList ID="ddlPaging" runat="server" Width="50px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="50">50</asp:ListItem>
                            <asp:ListItem Value="5000">All</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="panes">
                        <div id="divlist" runat="server" style="display: block;">
                            <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px; padding: 0px 1px; font-size: 14px;">
                                <%--Advanced Search panel starts here..--%>
                                <div class="collapse-tab">
                                    <div id="divExpandCollapseAdvanceSearch" style="width: 938px; float: left; cursor: pointer; color: white;">
                                        <img src="../images/ar-down.png" id="imgUpDown" class="icon" />
                                        <strong>Advance Search </strong>
                                    </div>
                                    <div id="divAdvancedSearch" style="display: none;" class="innertb-content">
                                        <div class="pass-coloum-two" style="width: 916px; float: left;">
                                            <asp:RadioButton ID="rdbShortOrder" runat="server" Text="Short Order" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('shortorder')" ClientIDMode="Static" />&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdbExpiry" runat="server" Text="Expiry" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('expiry')" ClientIDMode="Static" />&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdbProductName" runat="server" Text="Product Name" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('product')" ClientIDMode="Static" />&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdbCurrency" runat="server" Text="Currency" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('currency')" ClientIDMode="Static" />&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdbTopPasses" runat="server" Text="Top Passes" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('top')" ClientIDMode="Static" />&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdbSpecialandBestPasses" runat="server" Text="Special, Best" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('SpecialandBest')" ClientIDMode="Static" />
                                            <asp:RadioButton ID="rdbItxPasses" runat="server" Text="Itx Passes" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('itx')" ClientIDMode="Static" />
                                            <asp:RadioButton ID="rdbActive" runat="server" Text="Active" GroupName="AdvanceSearch"
                                                onclick="SearchCriteriaChanged('active')" ClientIDMode="Static" />
                                        </div>
                                        <div id="divAgentSearch" class="pass-coloum-two" style="float: left; padding-bottom: 8px; padding-top: 10px; padding-left: 10px; display: none">
                                            <label style="padding-left: 10px;">
                                                <div id="spnTextSearchLabel" style="width: 110px !important; float: left;">
                                                </div>
                                            </label>
                                            <asp:DropDownList runat="server" ID="ddlShortOrder" class="input" ClientIDMode="Static">
                                            </asp:DropDownList>
                                            <asp:DropDownList runat="server" ID="ddlExpiry" class="input" ClientIDMode="Static">
                                                <asp:ListItem Value="1">Purchase to/expiry date Exprired or expire imminently</asp:ListItem>
                                                <asp:ListItem Value="2">Purchase to/expiry date within this or next month</asp:ListItem>
                                                <asp:ListItem Value="3">Purchase to/expiry date further than next month</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList runat="server" ID="ddlProductName" class="input" ClientIDMode="Static">
                                            </asp:DropDownList>
                                            <asp:DropDownList runat="server" ID="ddlCurrency" class="input" ClientIDMode="Static">
                                            </asp:DropDownList>
                                            <asp:DropDownList runat="server" ID="ddlActive" class="input" ClientIDMode="Static">
                                                <asp:ListItem>True</asp:ListItem>
                                                <asp:ListItem>False</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Button ID="btnAdvanceSearch" runat="server" Text="Search" CssClass="button"
                                                OnClick="btnAdvanceSearch_Click" />
                                        </div>
                                    </div>
                                </div>
                                <%--Advanced Search panel ends here..--%>
                            </div>
                            <div class="crushGvDiv">
                                <asp:GridView ID="grvProduct" runat="server" AutoGenerateColumns="False"
                                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                    AllowPaging="True" OnPageIndexChanging="grvProduct_PageIndexChanging" OnRowCommand="grvProduct_RowCommand" OnRowDataBound="grvProduct_RowDataBound">
                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                    <PagerStyle CssClass="paging"></PagerStyle>
                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                        BorderColor="#FFFFFF" BorderWidth="1px" VerticalAlign="Top" CssClass="inner-row" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        Record not found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="S.Order">
                                            <ItemTemplate>
                                                <span class="short-order"><%#Eval("Sortorder")%></span>
                                                <img src="../images/list-line.png" id="list-icon-<%#Eval("ID")%>" class="list-icon" style="display: none; height: 15px; width: 15px; vertical-align: middle;" />
                                                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdnProductId" Value='<%#Eval("ID")%>'></asp:HiddenField>
                                            </ItemTemplate>
                                            <ItemStyle Width="7%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiry">
                                            <ItemTemplate>
                                                <%#Eval("CircleImg")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="3%" CssClass="CircleImg"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <%#Eval("Name")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="25%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category">
                                            <ItemTemplate>
                                                <%#Eval("Category")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="20%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                <%#Eval("Currency")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="12%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Top Passes">
                                            <ItemTemplate>
                                                <asp:ImageButton runat="server" ID="imgTopRailPass" CommandArgument='<%#Eval("ID")%>'
                                                    Height="16" CommandName="IsTopRailPass" AlternateText="status" ImageUrl='<%#Eval("IsTopRailPass").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    ToolTip='<%#Eval("IsTopRailPass").ToString()=="True" ? "Top Rail Pass":"Normal Pass" %>' />
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Width="8%" HorizontalAlign="Center"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Special, Best">
                                            <ItemTemplate>
                                                <img src='https://www.internationalrail.com/<%#Eval("ImgSpecialandBest")%>' width="50px" style='<%#Convert.ToBoolean(Eval("IsSpecialandBest"))?"display:block;":"display:none;"%>'/>
                                            </ItemTemplate>
                                            <ItemStyle Width="12%"></ItemStyle>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Promo Pass">
                                            <ItemTemplate>
                                                <asp:ImageButton runat="server" ID="imgPromoPass" CommandArgument='<%#Eval("ID")%>'
                                                    Height="16" CommandName="IsPromoPass" AlternateText="status" ImageUrl='<%#Eval("IsPromoPass").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    ToolTip='<%#Eval("IsPromoPass").ToString()=="True" ? "Promo Pass":"Normal Pass" %>' />
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Width="8%" HorizontalAlign="Center"></ItemStyle>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Itx Pass">
                                            <ItemTemplate>
                                                <asp:ImageButton runat="server" ID="imgItxPass" CommandArgument='<%#Eval("ID")%>'
                                                    Height="16" CommandName="IsItxPass" AlternateText="status" ImageUrl='<%#Eval("IsItxPass").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    ToolTip='<%#Eval("IsItxPass").ToString()=="True" ? "Itx Pass":"Normal Pass" %>' />
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Width="6%" HorizontalAlign="Center"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <a href='Product.aspx?id=<%#Eval("ID")%>'>
                                                    <img src="../images/edit.png" /></a>
                                                <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                    OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                                    CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                                <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                    Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                                <asp:ImageButton runat="server" ID="imgTemplateNote" AlternateText="Note" ToolTip="Template Note"
                                                    CommandArgument='<%#Eval("ID")%>' CommandName="TemplateNote" ImageUrl="~/images/note.png"
                                                    CssClass="templateimg" />
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active">
                                            <ItemTemplate>
                                                <asp:Image runat="server" ID="imgActiveInactive"
                                                    Height="16" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="6%" HorizontalAlign="Center"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnlnk" runat="server" />
            <asp:Panel ID="pnlTemplateNote" runat="server" CssClass="popup" Style="display: none; box-shadow: none; border-radius: 1px;">
                <div class="div-popup">
                    <div style="float: right">
                        <asp:ImageButton ID="btnClose" runat="server" ImageUrl="../images/cross.png" />
                    </div>
                    <asp:HiddenField runat="server" ID="hdnProductID" />
                    Template Note
                </div>
                <div style="margin: 40px 25px 25px 25px;">
                    <asp:TextBox ID="txtTemplateNote" runat="server" BorderColor="#CCCCCC" BorderStyle="Solid"
                        Width="100%" BorderWidth="1px" MaxLength="450" TextMode="MultiLine" Rows="5"
                        Columns="20" />
                </div>
                <div style="text-align: center;">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mdpupTemplateNote" runat="server" TargetControlID="hdnlnk"
                CancelControlID="btnClose" PopupControlID="pnlTemplateNote" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
