﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.Services;

namespace IR_Admin.Product
{
    public partial class Product : Page
    {
        readonly private ManagePriceBand _PriceBand = new ManagePriceBand();
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private Masters _oMasters = new Masters();
        List<RepeaterListItem> list = new List<RepeaterListItem>();
        string cBannerpath;
        string cImagepath;
        string PrdtImagepath, PrdtImagepath2, HomePrdtImagepath;
        public string showpricebind = "class='hide'";
        Guid pId = Guid.Empty;
        bool IsInterrailPass = false;
        Guid _SiteID;
        bool isProductVisibleFromToDate = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            PageLoadEvent();
            GetProductDetailForEdit();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                pId = Guid.Parse(Request.QueryString["id"]);
                IsInterrailPass = _oProduct.IsInterrailCategory(pId);
                if (!IsInterrailPass)
                    IsInterrailPass = _oProduct.GetProductName(pId).ToLower().Contains("interrail");
                tblProduct oProd = _oProduct.GetProductById(pId);
                showpricebind = (oProd.CountryStartCode <= 2000 || oProd.CountryStartCode == 9999) ? "class='hide'" : "";
                BindCrossSaleRptr();

                if (_oMasters.isBritRailPromoPass(pId))
                    isBritrailPromo.Visible = true;

                ShowMessage(0, null);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "onload();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "checkUncheckCountyTree", "checkUncheckCountyTree();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "checkUncheckPermittedCntTree", "checkUncheckPermittedCntTree();", true);
                if (!Page.IsPostBack)
                {
                    for (int i = 12; i >= 1; i--)
                    {
                        ddlMonthValid.Items.Insert(0, new ListItem(i.ToString(), i.ToString()));
                    }
                    for (int i = 0; i <= _oProduct.GetProductCount(); i++)
                    {
                        ddlShortOrder.Items.Insert(0, new ListItem(i.ToString(), i.ToString()));
                    }
                    _SiteID = Master.SiteID;
                    PageLoadEvent();
                    GetProductDetailForEdit();
                }
            }
            else
                Response.Redirect("ProductList.aspx");
        }

        private void PageLoadEvent()
        {
            try
            {
                Session["PriceList"] = null;
                ViewState["NoOfItems"] = 0;
                var listLang = _oMasters.GetLanguangesList();
                if (listLang != null)
                {
                    Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                    ddlLanguage.DataSource = listLang.Where(x => x.ID == langid);
                    ddlLanguage.DataTextField = "Name";
                    ddlLanguage.DataValueField = "ID";
                    ddlLanguage.DataBind();
                }

                ddlCrossSaleAdsense.DataSource = _oMasters.GetCrossSaleAdsense().Where(x => x.IsActive == true).OrderBy(x => x.Name);
                ddlCrossSaleAdsense.DataTextField = "Name";
                ddlCrossSaleAdsense.DataValueField = "ID";
                ddlCrossSaleAdsense.DataBind();
                ddlCrossSaleAdsense.Items.Insert(0, new ListItem("--Select Cross Sale Adsense--", "0"));

                ddlCurrency.DataSource = _oMasters.GetCurrencyList().Where(x => x.IsActive == true);
                ddlCurrency.DataTextField = "Name";
                ddlCurrency.DataValueField = "ID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("--Currency--", "-1"));

                ddlClass.DataSource = _oMasters.GetClassList().Where(x => x.IsActive);
                ddlClass.DataTextField = "Name";
                ddlClass.DataValueField = "ID";
                ddlClass.DataBind();
                ddlClass.Items.Insert(0, new ListItem("--Select Class--", "0"));

                ddlTraveler.DataSource = _oMasters.GetTravelerListWithType().Where(x => x.IsActive);
                ddlTraveler.DataTextField = "Name";
                ddlTraveler.DataValueField = "ID";
                ddlTraveler.DataBind();
                ddlTraveler.Items.Insert(0, new ListItem("--Select Traveller--", "0"));


                ddlCategory.DataSource = _oProduct.GetAssociateCategoryList(3, _SiteID);
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("--Select Product Category--", "0"));

                dtlCountry.DataSource = _oMasters.GetRegionList().ToList().OrderBy(x => x.RegionName).ToList();
                dtlCountry.DataBind();

                dtlCtPermitted.DataSource = _oMasters.GetRegionList().ToList().OrderBy(x => x.RegionName).ToList();
                dtlCtPermitted.DataBind();

                ddlSupName.DataSource = _oProduct.GetSupplierList().Where(x => x.IsActive).GroupBy(x => new { x.Name }).Select(x => new { x.Key.Name }).ToList();
                ddlSupName.DataTextField = "Name";
                ddlSupName.DataValueField = "Name";
                ddlSupName.DataBind();
                ddlSupName.Items.Insert(0, new ListItem("--Select Supplier Name--", "0"));

                ddlSupCat.Items.Insert(0, new ListItem("--Select Supplier Category--", "0"));

                //--Bind Product or Pass Valid Up To
                BindTravellerValidPass();

                //--Bind Repeater For Name
                BindRepeter(null);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BindRepeter(List<RepeaterListItem> listItm)
        {
            try
            {
                if (listItm == null)
                {
                    int numOfItem = Convert.ToInt32(ViewState["NoOfItems"]);
                    for (int i = 0; i < numOfItem; i++)
                    {
                        var page = new RepeaterListItem { NoOfItems = i };
                        list.Add(page);
                    }
                    Session["rptProdList"] = list;
                    rptProduct.DataSource = list;
                }
                else
                    rptProduct.DataSource = listItm;
                rptProduct.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected List<RepeaterListItem> AddRepeaterItemInList()
        {
            foreach (RepeaterItem item in rptProduct.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    DropDownList ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    HiddenField idpnam = item.FindControl("hdnProdNmId") as HiddenField;
                    TextBox txtPass = item.FindControl("txtProductName") as TextBox;
                    RadioButton rdoMissing = item.FindControl("rdoMissing") as RadioButton;
                    RepeaterListItem page = new RepeaterListItem
                    {
                        Id = !String.IsNullOrEmpty(idpnam.Value) ? Guid.Parse(idpnam.Value) : new Guid(),
                        NoOfItems = item.ItemIndex,
                        LanguageId = ddlLang.SelectedValue,
                        Name = txtPass.Text,
                        IsMissing = rdoMissing.Checked
                    };
                    list.Add(page);
                }
            }
            return list;
        }

        protected void rptProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DropDownList ddlLang = e.Item.FindControl("ddlLanguage") as DropDownList;
                    HiddenField hdnLangId = e.Item.FindControl("hdnLangId") as HiddenField;

                    var list = _oMasters.GetLanguangesList();
                    if (list != null)
                    {
                        Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                        list.RemoveAll(x => x.ID == langid);
                        ddlLang.DataSource = list;
                    }
                    ddlLang.DataTextField = "Name";
                    ddlLang.DataValueField = "ID";
                    ddlLang.DataBind();

                    if (hdnLangId != null && !String.IsNullOrEmpty(hdnLangId.Value))
                        ddlLang.SelectedValue = hdnLangId.Value;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnAddMore_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["NoOfItems"] != null)
                {
                    ViewState["NoOfItems"] = Convert.ToInt32(ViewState["NoOfItems"]) + 1;
                    var newpage = new RepeaterListItem { NoOfItems = Convert.ToInt32(ViewState["NoOfItems"]) };
                    list.Add(newpage);
                    list = AddRepeaterItemInList();
                    Session["rptProdList"] = list;
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptProduct.Items.Count > 0)
                {
                    list = AddRepeaterItemInList();

                    //--Add Removed Record in list at edit time
                    if (!String.IsNullOrEmpty(hdnProdId.Value))
                    {
                        var record = list.FirstOrDefault();
                        if (Session["reovProdList"] == null)
                        {
                            List<RepeaterListItem> removeList = new List<RepeaterListItem> { record };
                            Session["reovProdList"] = removeList;
                        }
                        else
                        {
                            List<RepeaterListItem> removeList = Session["reovProdList"] as List<RepeaterListItem>;
                            if (removeList != null) removeList.Add(record);
                            Session["reovProdList"] = removeList;
                        }
                    }
                    list.RemoveAt(0);
                    Session["rptProdList"] = list;
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #region Save and Cancel
        protected bool IsExistDuplicateNameForSameLang()
        {
            var lngIdList = new List<Guid>();
            foreach (RepeaterItem item in rptProduct.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    var txtProd = item.FindControl("txtProductName") as TextBox;
                    var id = Guid.Parse(ddlLang.SelectedValue);

                    if (!String.IsNullOrEmpty(txtProd.Text.Trim()) && lngIdList.Any(x => x.ToString() == id.ToString()) == false)
                    {
                        lngIdList.Add(id);
                    }
                    else if (!String.IsNullOrEmpty(txtProd.Text.Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        void AddEditProductValidUpTo()
        {
            try
            {
                pId = pId == Guid.Empty ? new Guid() : pId;
                hdnProdId.Value = pId.ToString();
                RemoveProductNameOfDiffLang(pId);
                if (Session["PriceListId"] != null)
                {
                    var pricIdList = Session["PriceListId"] as List<Guid>;
                    _oProduct.DeleteProductPrice(pricIdList);
                }

                #region Product Name
                if (txtProductName.Text.Trim() != "")
                {
                    string[] array = ddlCountryRange.SelectedValue.Split('-');
                    int strtCountry = int.Parse(array[0].Trim());
                    int endCountry = array.Length > 1 ? int.Parse(array[1].Trim()) : 0;

                    if (ddlCountryRange.SelectedValue == "1-1000")//one country Interrail
                    {
                        strtCountry = int.Parse(ddlRegionalCountryPass.SelectedValue);
                        endCountry = 0;
                    }

                    if (ddlCountryRange.SelectedValue == "1001-1999")//one country
                    {
                        strtCountry = int.Parse(ddlRegionalCountryPass.SelectedValue);
                        endCountry = 0;
                    }
                    Guid SupplierId = Guid.Empty;
                    if (ddlSupCat.SelectedValue != "0")
                        SupplierId = Guid.Parse(ddlSupCat.SelectedValue);

                    pId = _oProduct.AddProduct(new tblProduct
                    {
                        ID = pId,
                        SupplierId = SupplierId,
                        CreatedBy = AdminuserInfo.UserID,
                        CreatedOn = DateTime.Now,
                        IsActive = chkIsActv.Checked,
                        PassportValid = chkpassport.Checked,
                        NationalityValid = chkNationality.Checked,
                        Istwinpass = chkTwinPass.Checked,
                        IsAllowRefund = chkAllowRefund.Checked,
                        IsAllowMiddleName = chkAllowMiddleName.Checked,
                        CurrencyID = Guid.Parse(ddlCurrency.SelectedValue),
                        Description = txtProductDesc.InnerHtml,
                        Discount = txtDiscount.InnerHtml,
                        Eligibility = txtEligibility.InnerHtml,
                        EligibilityforSaver = txtEligibilityforSaver.InnerHtml,
                        TermCondition = txtTerm.InnerHtml,
                        GreatPassDescription = txtGreatPass.InnerHtml,
                        Announcement = txtareaAnnouncement.InnerHtml,
                        AnnouncementTitle = txtAnnouncement.Text,
                        Validity = txtValidity.InnerHtml,
                        IsAllowAdminPass = chkAllowAdminPass.Checked,
                        IsPromoPass = chkIsPromoPass.Checked,
                        CountryStartCode = strtCountry,
                        CountryEndCode = endCountry,
                        IsShippingAplicable = chkShipping.Checked,
                        IsSpecialOffer = rdnSpecialOffer.SelectedValue == "0" ? true : false,
                        BestSeller = rdnSpecialOffer.SelectedValue == "1" ? true : false,
                        MonthValidity = Convert.ToInt32(ddlMonthValid.SelectedValue),
                        BannerImage = string.IsNullOrEmpty(cBannerpath) == true ? hdnBannerImg.Value : cBannerpath.Replace("~/", ""),
                        CounrtyImage = string.IsNullOrEmpty(cImagepath) == true ? hdnCountryImg.Value : cImagepath.Replace("~/", ""),
                        ProductImage = string.IsNullOrEmpty(PrdtImagepath) == true ? hdnprdtImage.Value : PrdtImagepath.Replace("~/", ""),
                        ProductImageSecond = string.IsNullOrEmpty(PrdtImagepath2) == true ? hdnprdtImage2.Value : PrdtImagepath2.Replace("~/", ""),
                        HomePageProductImage = string.IsNullOrEmpty(HomePrdtImagepath) == true ? hdnHomepageprodtimage.Value : HomePrdtImagepath.Replace("~/", ""),
                        ProductValidFromDate = DateTime.ParseExact((txtFromDate.Text), "dd/MM/yyyy HH:mm", null),
                        ProductValidToDate = DateTime.ParseExact((txtToDate.Text), "dd/MM/yyyy HH:mm", null),
                        ProductEnableFromDate = DateTime.ParseExact((txtVisibleFromDate.Text), "dd/MM/yyyy HH:mm", null),
                        ProductEnableToDate = DateTime.ParseExact((txtVisibleToDate.Text), "dd/MM/yyyy HH:mm", null),
                        SpecialOfferText = txtSpecialOfferText.Text,
                        StartDayText = TxtStartDayText.Text,
                        ProductAltTag1 = txtPrdtAltTag1.Text,
                        ProductAltTag2 = txtPrdtAltTag2.Text,
                        BritrailPromoFrom = isBritrailPromo.Visible ? DateTime.ParseExact((txtBritrailFromDate.Text), "dd/MM/yyyy HH:mm", null) : (DateTime?)null,
                        BritrailPromoTo = isBritrailPromo.Visible ? DateTime.ParseExact((txtBritrailToDate.Text), "dd/MM/yyyy HH:mm", null) : (DateTime?)null,
                        IrTitle = txtTitle.Text,
                        IrDesciption = txtDesciption.InnerHtml,
                        IsElectronicPass = chkIsElectronic.Checked,
                        ElectronicPassNote = TxtElectronic.Text,
                        IsProductEnableFromDate = chkVisibleFromDate.Checked,
                        IsProductEnableToDate = chkVisibleToDate.Checked
                    });

                    _oProduct.AddProductName(new tblProductName
                    {
                        DefaultWhenMissing = rdoMissing.Checked,
                        LanguageID = Guid.Parse(ddlLanguage.SelectedValue),
                        Name = txtProductName.Text.Trim(),
                        UserFriendlyName = "",
                        ProductID = pId,
                        ID = !String.IsNullOrEmpty(hdnProdNmId.Value) ? Guid.Parse(hdnProdNmId.Value) : new Guid()
                    });
                }

                foreach (RepeaterItem item in rptProduct.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                        var idpnam = item.FindControl("hdnProdNmId") as HiddenField;
                        var txtProd = item.FindControl("txtProductName") as TextBox;
                        var rdoMis = item.FindControl("rdoMissing") as RadioButton;
                        if (txtProd.Text.Trim() != "")
                            _oProduct.AddProductName(new tblProductName
                            {
                                DefaultWhenMissing = rdoMis.Checked,
                                LanguageID = Guid.Parse(ddlLang.SelectedValue),
                                Name = txtProd.Text.Trim(),
                                ProductID = pId,
                                ID = !String.IsNullOrEmpty(idpnam.Value) ? Guid.Parse(idpnam.Value) : new Guid()
                            });
                    }
                }
                #endregion

                #region Product Category LookUp
                var rec = new tblProductCategoriesLookUp
                    {
                        ID = Guid.NewGuid(),
                        CategoryID = Guid.Parse(ddlCategory.SelectedValue),
                        ProductID = pId
                    };
                _oProduct.AddProductCategoriesLookUp(rec, pId);
                #endregion

                #region Product Country LookUp
                var listPrdCuntLkp = new List<tblProductCountryLookUp>();
                foreach (DataListItem item in dtlCountry.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var treeSite = item.FindControl("trSites") as TreeView;
                        if (treeSite != null)
                        {
                            var res = treeSite.Nodes.Cast<TreeNode>().Where(node => node.Checked).Select(x => x.Value);
                            listPrdCuntLkp.AddRange(res.Select(itm => new tblProductCountryLookUp
                                {
                                    CountryID = Guid.Parse(itm),
                                    ProductID = pId,
                                    ID = new Guid()
                                }));
                        }
                    }
                }
                _oProduct.AddProductCountriesLookUp(listPrdCuntLkp, pId);
                #endregion

                #region Product Site LookUp
                List<tblProductSiteLookUp> listProductSiteLkp = (from DataListItem item in rptSite.Items
                                                                 where item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item
                                                                 select item.FindControl("hdnSiteId") as HiddenField into hdSiteId
                                                                 select new tblProductSiteLookUp
                                                                 {
                                                                     ID = new Guid(),
                                                                     ProductID = pId,
                                                                     SiteID = Guid.Parse(hdSiteId.Value),
                                                                 }).ToList();

                _oProduct.AddProductSiteLookUp(listProductSiteLkp, pId);
                _oProduct.AddEditShortOrder(Master.SiteID, pId, Convert.ToInt32(ddlShortOrder.SelectedValue));
                AddUpdateProductValid();
                #endregion

                #region Product Promotion Country LookUp
                var listPrdPrmCntLkp = new List<tblProductPermittedLookup>();
                foreach (DataListItem item in dtlCtPermitted.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var treeSite = item.FindControl("trPSites") as TreeView;
                        if (treeSite != null)
                        {
                            var res = treeSite.Nodes.Cast<TreeNode>().Where(node => node.Checked).Select(x => x.Value);
                            listPrdPrmCntLkp.AddRange(res.Select(itm => new tblProductPermittedLookup
                            {
                                CountryID = Guid.Parse(itm),
                                ProductID = pId,
                                ID = new Guid()
                            }));
                        }
                    }
                }
                _oProduct.AddProductCntPrLookUp(listPrdPrmCntLkp, pId);
                #endregion
                ShowMessage(1, !String.IsNullOrEmpty(hdnProdId.Value) ? "You have successfully updated product details." : "You have successfully added product details.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void RemoveProductNameOfDiffLang(Guid id)
        {
            if (Session["reovProdList"] != null)
            {
                var removeList = Session["reovProdList"] as List<RepeaterListItem>;
                if (removeList != null)
                    foreach (var item in removeList.Where(item => !String.IsNullOrEmpty(item.Name)))
                    {
                        _oProduct.DeleteProductName(id, Guid.Parse(item.LanguageId), item.Name);
                    }
            }

            Session["reovProdList"] = null;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    DateTime.ParseExact((txtFromDate.Text), "dd/MM/yyyy HH:mm", null);
                    DateTime.ParseExact((txtToDate.Text), "dd/MM/yyyy HH:mm", null);
                    DateTime.ParseExact((txtVisibleFromDate.Text), "dd/MM/yyyy HH:mm", null);
                    DateTime.ParseExact((txtVisibleToDate.Text), "dd/MM/yyyy HH:mm", null);
                    if (isBritrailPromo.Visible)
                    {
                        DateTime.ParseExact((txtBritrailFromDate.Text), "dd/MM/yyyy HH:mm", null);
                        DateTime.ParseExact((txtBritrailToDate.Text), "dd/MM/yyyy HH:mm", null);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(2, "Please enter valid Date Format(DD/MM/YYYY).");
                    return;
                }
                if (FupldPdtImg.HasFile && String.IsNullOrEmpty(txtPrdtAltTag1.Text))
                {
                    ShowMessage(2, "Please enter product image Alt tag.");
                    return;
                }
                if (FupldPdtImg2.HasFile && String.IsNullOrEmpty(txtPrdtAltTag2.Text))
                {
                    ShowMessage(2, "Please enter product image 2 Alt tag.");
                    return;
                }
                UploadFile();
                uploadPrdtImg();
                uploadPrdtImg2();
                uploadHomePagePrdtImg();
                if (!IsExistDuplicateNameForSameLang())
                {
                    ShowMessage(2, "You have selected one Language multiple time");
                    return;
                }
                if (ddlCountryRange.SelectedValue == "0")
                {
                    ShowMessage(2, "Please select Country.");
                    return;
                }
                AddEditProductValidUpTo();
                UpdatePublishStatus();
                SetPublishImageVisibleFalse();
                GetProductDetailForEdit();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductList.aspx");
        }
        #endregion

        #region Product Price
        protected void btnSaveNew_Click(object sender, EventArgs e)
        {
            try
            {
                AddProductPriceInTempList();
                BindProductPrice();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AddProductPriceInTempList();
                BindProductPrice();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void AddProductPriceInTempList()
        {
            try
            {
                string countryRange = ddlCountryRange.SelectedValue;
                string passType = txtPassTypeCode.Text.Trim();

                var classId = Guid.Parse(ddlClass.SelectedValue.Trim());
                string clsCode = _oMasters.GetClassById(classId).EurailCode.ToString();

                var validityId = Guid.Parse(ddlProductValidUpTo.SelectedValue.Trim());
                string validityCode = _oMasters.GetProductValidUpToById(validityId).EurailCode.ToString();

                string pUid = passType + countryRange + clsCode + validityCode;
                ProductPricing oPricing = new ProductPricing
                {
                    ID = new Guid(),
                    ClassId = classId,
                    ClassName = ddlClass.SelectedItem.Text.Trim(),
                    TravellerId = Guid.Parse(ddlTraveler.SelectedValue.Trim()),
                    TravellerName = ddlTraveler.SelectedItem.Text.Trim(),
                    TravellerValidId = validityId,
                    TravellerValidName = ddlProductValidUpTo.SelectedItem.Text.Trim(),
                    Price = Convert.ToDecimal(txtPrice.Text),
                    PassTypeCode = Convert.ToInt32(passType),
                    UniqueProductID = pUid
                };
                if (oPricing != null)
                {
                    if (_oProduct.GetProductsPricesById(pId).Where(x => x.PassTypeCode == oPricing.PassTypeCode
                        && x.ClassId == oPricing.ClassId
                        && x.TravellerId == oPricing.TravellerId
                        && x.TravellerValidId == oPricing.TravellerValidId).Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "checkduplicate", "alert('Please change Traveller Validity type.')", true);
                        return;
                    }
                    else
                    {
                        _oProduct.AddProductPrice(new tblProductPrice
                        {
                            ID = oPricing.ID,
                            ProductID = pId,
                            ClassID = oPricing.ClassId,
                            Price = oPricing.Price,
                            PassTypeCode = oPricing.PassTypeCode,
                            UniqueProductID = oPricing.UniqueProductID,
                            TravelerID = oPricing.TravellerId,
                            ValidityID = oPricing.TravellerValidId,
                            IsActive = true
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void BindProductPrice()
        {
            try
            {
                rptProductPrice.DataSource = _oProduct.GetProductsPricesById(pId).OrderBy(x => x.TravellerName).ToList();
                rptProductPrice.DataBind();

                ddlProductValidUpTo.SelectedIndex = 0;
                ddlTraveler.SelectedIndex = 0;
                ddlClass.SelectedIndex = 0;
                txtPrice.Text = string.Empty;
                txtPassTypeCode.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static string updateprice(Guid Id, Guid TravelerID, decimal Price, int PriceBand)
        {
            try
            {
                ManageProduct Product = new ManageProduct();
                Guid prdtPriceid = Product.AddProductPriceStatus(new tblProductPrice
                {
                    ID = Id,
                    TravelerID = TravelerID,
                    Price = Price,
                    PriceBindLevel = PriceBand
                });
                return "yes";
            }
            catch (Exception ex)
            {
                return "no";
            }
        }

        [WebMethod]
        public static string activeInactiveProductPrice(Guid Id)
        {
            try
            {
                ManageProduct Product = new ManageProduct();
                Product.ActiveInactiveProductPrice(Id);
                return "yes";
            }
            catch (Exception ex)
            {
                return "no";
            }
        }

        protected void rptProductPrice_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    bool rs = _oProduct.DeleteProductPriceById(Guid.Parse(e.CommandArgument.ToString()));
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    if (Session["PriceListId"] == null)
                    {
                        List<Guid> pricIdList = new List<Guid> { id };
                        Session["PriceListId"] = pricIdList;
                    }
                    else
                    {
                        List<Guid> pricIdList = Session["PriceListId"] as List<Guid>;
                        pricIdList.Add(id);
                    }
                    BindProductPrice();
                }
                //else if (e.CommandName == "EditPrice")
                //{
                //    DropDownList ddlTravellerName = (DropDownList)e.Item.FindControl("ddlTravellerName");
                //    DropDownList ddlpriceband = (DropDownList)e.Item.FindControl("ddlpriceband");
                //    ImageButton EditPriceSubmit = (ImageButton)e.Item.FindControl("EditPriceSubmit");
                //    TextBox txtpriceEdit = (TextBox)e.Item.FindControl("txtpriceEdit");
                //    ImageButton chkEdit = (ImageButton)e.Item.FindControl("chkEdit");
                //    Label lblPriceShow = (Label)e.Item.FindControl("lblPriceShow");
                //    Label lblpriceband = (Label)e.Item.FindControl("lblpriceband");
                //    Label lblTravellerName = (Label)e.Item.FindControl("lblTravellerName");

                //    lblpriceband.Visible = lblTravellerName.Visible = chkEdit.Visible = lblPriceShow.Visible = false;
                //    ddlpriceband.Visible = ddlTravellerName.Visible = EditPriceSubmit.Visible = txtpriceEdit.Visible = true;
                //}
                //else if (e.CommandName == "EditPriceSubmit")
                //{
                //    string[] str = e.CommandArgument.ToString().Split(',');
                //    Guid id = Guid.Parse(str[0]);
                //    TextBox txtpriceEdit = (TextBox)e.Item.FindControl("txtpriceEdit");
                //    DropDownList ddlTravellerName = e.Item.FindControl("ddlTravellerName") as DropDownList;
                //    DropDownList ddlpriceband = (DropDownList)e.Item.FindControl("ddlpriceband");
                //    Guid TrvId = Guid.Parse(ddlTravellerName.SelectedValue.Trim());
                //    Guid prdtPriceid = _oProduct.AddProductPriceStatus(new tblProductPrice
                //    {
                //        ID = id,
                //        TravelerID = TrvId,
                //        Price = Convert.ToDecimal(!string.IsNullOrEmpty(txtpriceEdit.Text) ? txtpriceEdit.Text : "0"),
                //        IsActive = Convert.ToBoolean(str[1]),
                //        PriceBindLevel = Convert.ToInt32(ddlpriceband.SelectedValue)
                //    });
                //    BindProductPrice();
                //}
                //else if (e.CommandName == "ActiveInActive")
                //{
                //    string[] str = e.CommandArgument.ToString().Split(',');
                //    Guid id = Guid.Parse(str[0]);
                //    _oProduct.ActiveInactiveProductPrice(id);
                //    BindProductPrice();
                //}
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Product price is already in use.");
            }
        }
        #endregion

        void GetProductDetailForEdit()
        {
            if (Request.QueryString["id"] == null)
            {
                hdnProdId.Value = string.Empty;
                hdnProdNmId.Value = string.Empty;
                return;
            }
            hdnProdId.Value = Request.QueryString["id"];
            //--Name and CMS
            Guid pId = Guid.Parse(Request.QueryString["id"]);
            hdnProdId.Value = pId.ToString();
            List<tblProductName> oProdNameList = _oProduct.GetProductsNameById(pId);
            tblProduct oProd = _oProduct.GetProductById(pId);
            if (oProd.SupplierId != Guid.Empty)
            {
                var data = _oProduct.GetSupplierList();
                var rows = data.FirstOrDefault(x => x.Id == oProd.SupplierId);
                if (rows != null)
                {
                    ddlSupName.SelectedValue = rows.Name;
                    //if (rows.Currency.HasValue)
                    //    txtsupcurrency.Text = _oMasters.GetCurrencyList().FirstOrDefault(x => x.ID == rows.Currency.Value).Name;
                    //else
                    //    txtsupcurrency.Text = "Not Set";
                    ddlSupCat.DataSource = data.Where(x => x.IsActive && x.Name == rows.Name).Select(x => new { x.Category, x.Id }).ToList();
                    ddlSupCat.DataTextField = "Category";
                    ddlSupCat.DataValueField = "Id";
                    ddlSupCat.DataBind();
                    ddlSupCat.Items.Insert(0, new ListItem("--Select Supplier Category--", "0"));
                    ddlSupCat.SelectedValue = oProd.SupplierId.ToString();
                }
            }

            #region Product Publish
            var _PublishInfo = _oProduct.GetProductPublishInfo(pId, AdminuserInfo.UserID);
            if (_PublishInfo != null)
            {
                bool ShortOrder = _oProduct.GetProductShortOrder(Master.SiteID, pId) == _PublishInfo.ShortOrder;
                ddlShortOrder.SelectedValue = ShortOrder ? _oProduct.GetProductShortOrder(Master.SiteID, pId).ToString() : _PublishInfo.ShortOrder.ToString();
                imgShortOrder.Visible = !ShortOrder;

                chkIsElectronic.Checked = (oProd.IsElectronicPass == _PublishInfo.IsElectronicPass) ? oProd.IsElectronicPass : _PublishInfo.IsElectronicPass;
                imgIsElectronic.Visible = !(oProd.IsElectronicPass == _PublishInfo.IsElectronicPass);

                bool ElectronicPassNote = ((string.IsNullOrEmpty(oProd.ElectronicPassNote) ? "" : oProd.ElectronicPassNote) == (string.IsNullOrEmpty(_PublishInfo.ElectronicPassNote) ? "" : _PublishInfo.ElectronicPassNote));
                TxtElectronic.Text = ElectronicPassNote ? oProd.ElectronicPassNote : _PublishInfo.ElectronicPassNote;
                imgElectronic.Visible = !ElectronicPassNote;

                bool SpecialOfferText2 = ((string.IsNullOrEmpty(oProd.SpecialOfferText) ? "" : oProd.SpecialOfferText) == (string.IsNullOrEmpty(_PublishInfo.SpecialOfferText) ? "" : _PublishInfo.SpecialOfferText));
                txtSpecialOfferText.Text = SpecialOfferText2 ? oProd.SpecialOfferText : _PublishInfo.SpecialOfferText;
                imgSpecialOfferText.Visible = !SpecialOfferText2;

                bool StartDayText = ((string.IsNullOrEmpty(oProd.StartDayText) ? "" : oProd.StartDayText) == (string.IsNullOrEmpty(_PublishInfo.StartDayText) ? "" : _PublishInfo.StartDayText));
                TxtStartDayText.Text = StartDayText ? oProd.StartDayText : _PublishInfo.StartDayText;
                imgStartDayText.Visible = !StartDayText;

                ddlCurrency.SelectedValue = (oProd.CurrencyID == _PublishInfo.CurrencyID) ? oProd.CurrencyID.ToString() : _PublishInfo.CurrencyID.ToString();
                imgCurrency.Visible = !(oProd.CurrencyID == _PublishInfo.CurrencyID);

                chkShipping.Checked = (oProd.IsShippingAplicable == _PublishInfo.IsShippingAplicable) ? oProd.IsShippingAplicable : _PublishInfo.IsShippingAplicable;
                imgShipping.Visible = !(oProd.IsShippingAplicable == _PublishInfo.IsShippingAplicable);

                chkTwinPass.Checked = (oProd.Istwinpass == _PublishInfo.Istwinpass) ? oProd.Istwinpass : _PublishInfo.Istwinpass;
                imgTwinPass.Visible = !(oProd.Istwinpass == _PublishInfo.Istwinpass);

                chkAllowRefund.Checked = (oProd.IsAllowRefund == _PublishInfo.IsAllowRefund) ? oProd.IsAllowRefund : _PublishInfo.IsAllowRefund;
                imgAllowRefund.Visible = !(oProd.IsAllowRefund == _PublishInfo.IsAllowRefund);

                chkAllowMiddleName.Checked = (oProd.IsAllowMiddleName == _PublishInfo.IsAllowMiddleName) ? oProd.IsAllowMiddleName : _PublishInfo.IsAllowMiddleName;
                imgAllowMiddleName.Visible = !(oProd.IsAllowMiddleName == _PublishInfo.IsAllowMiddleName);

                chkAllowAdminPass.Checked = (oProd.IsAllowAdminPass == _PublishInfo.IsAllowAdminPass) ? oProd.IsAllowAdminPass : _PublishInfo.IsAllowAdminPass;
                imgAllowAdminPass.Visible = !(oProd.IsAllowAdminPass == _PublishInfo.IsAllowAdminPass);

                chkIsPromoPass.Checked = (oProd.IsPromoPass == _PublishInfo.IsPromoPass) ? oProd.IsPromoPass : _PublishInfo.IsPromoPass;
                imgIsPromoPass.Visible = !(oProd.IsPromoPass == _PublishInfo.IsPromoPass);

                chkIsActv.Checked = (oProd.IsActive == _PublishInfo.IsActive) ? oProd.IsActive : _PublishInfo.IsActive;
                imgIsActv.Visible = !(oProd.IsActive == _PublishInfo.IsActive);

                if (oProd.PassportValid.HasValue)
                {
                    chkpassport.Checked = (oProd.PassportValid.Value == _PublishInfo.PassportValid) ? oProd.PassportValid.Value : _PublishInfo.PassportValid;
                    imgPassport.Visible = !(oProd.PassportValid == _PublishInfo.PassportValid);
                }
                if (oProd.NationalityValid.HasValue)
                {
                    chkNationality.Checked = (oProd.NationalityValid.Value == _PublishInfo.NationalityValid) ? oProd.NationalityValid.Value : _PublishInfo.NationalityValid;
                    imgNationality.Visible = !(oProd.NationalityValid == _PublishInfo.NationalityValid);
                }


                bool IrTitle = ((string.IsNullOrEmpty(oProd.IrTitle) ? "" : oProd.IrTitle) == (string.IsNullOrEmpty(_PublishInfo.IrTitle) ? "" : _PublishInfo.IrTitle));
                txtTitle.Text = IrTitle ? oProd.IrTitle : _PublishInfo.IrTitle;
                imgTitle.Visible = !IrTitle;

                bool Desciption = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.IrDesciption) ? "" : oProd.IrDesciption), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.IrDesciption) ? "" : _PublishInfo.IrDesciption), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtDesciption.InnerHtml = Desciption ? oProd.IrDesciption : _PublishInfo.IrDesciption;
                imgDesciption.Visible = !Desciption;


                bool ProductDesc = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.Description) ? "" : oProd.Description), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.Description) ? "" : _PublishInfo.Description), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtProductDesc.InnerHtml = ProductDesc ? oProd.Description : _PublishInfo.Description;
                imgProductDesc.Visible = !ProductDesc;


                bool Discount = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.Discount) ? "" : oProd.Discount), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.Discount) ? "" : _PublishInfo.Discount), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtDiscount.InnerHtml = Discount ? oProd.Discount : _PublishInfo.Discount;
                imgDiscount.Visible = !Discount;


                bool Eligibility = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.Eligibility) ? "" : oProd.Eligibility), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.Eligibility) ? "" : _PublishInfo.Eligibility), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtEligibility.InnerHtml = Eligibility ? oProd.Eligibility : _PublishInfo.Eligibility;
                imgEligibility.Visible = !Eligibility;


                bool EligibilityforSaver = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.EligibilityforSaver) ? "" : oProd.EligibilityforSaver), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.EligibilityforSaver) ? "" : _PublishInfo.EligibilityforSaver), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtEligibilityforSaver.InnerHtml = EligibilityforSaver ? oProd.EligibilityforSaver : _PublishInfo.EligibilityforSaver;
                imgEligibilityforSaver.Visible = !EligibilityforSaver;


                bool TermCondition = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.TermCondition) ? "" : oProd.TermCondition), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.TermCondition) ? "" : _PublishInfo.TermCondition), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtTerm.InnerHtml = TermCondition ? oProd.TermCondition : _PublishInfo.TermCondition;
                imgTerm.Visible = !TermCondition;


                bool Validity = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.Validity) ? "" : oProd.Validity), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.Validity) ? "" : _PublishInfo.Validity), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtValidity.InnerHtml = Validity ? oProd.Validity : _PublishInfo.Validity;
                imgValidity.Visible = !Validity;

                bool GreatPassDescription = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.GreatPassDescription) ? "" : oProd.GreatPassDescription), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.GreatPassDescription) ? "" : _PublishInfo.GreatPassDescription), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtGreatPass.InnerHtml = GreatPassDescription ? oProd.GreatPassDescription : _PublishInfo.GreatPassDescription;
                imgGreatPass.Visible = !GreatPassDescription;

                bool AnnouncementTitle = ((string.IsNullOrEmpty(oProd.AnnouncementTitle) ? "" : oProd.AnnouncementTitle) == (string.IsNullOrEmpty(_PublishInfo.AnnouncementTitle) ? "" : _PublishInfo.AnnouncementTitle));
                txtAnnouncement.Text = AnnouncementTitle ? oProd.AnnouncementTitle : _PublishInfo.AnnouncementTitle;

                bool Announcement = System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(oProd.Announcement) ? "" : oProd.Announcement), @"<[^>]*>|\n|\t|\r| ", String.Empty) == System.Text.RegularExpressions.Regex.Replace(Server.HtmlDecode(string.IsNullOrEmpty(_PublishInfo.Announcement) ? "" : _PublishInfo.Announcement), @"<[^>]*>|\n|\t|\r| ", String.Empty);
                txtareaAnnouncement.InnerHtml = Announcement ? oProd.Announcement : _PublishInfo.Announcement;

                imgAnnouncement.Visible = !(AnnouncementTitle == Announcement);

                string SpecialOfferText = "";
                bool IsSpecialOfferImgShow = false;
                if (oProd.IsSpecialOffer != _PublishInfo.IsSpecialOffer)
                {
                    if (_PublishInfo.IsSpecialOffer)
                    {
                        SpecialOfferText = "0";
                        IsSpecialOfferImgShow = true;
                    }
                }
                else if (oProd.BestSeller != _PublishInfo.BestSeller)
                {
                    if (_PublishInfo.BestSeller)
                    {
                        SpecialOfferText = "1";
                        IsSpecialOfferImgShow = true;
                    }
                }
                else
                    SpecialOfferText = "3";

                rdnSpecialOffer.SelectedValue = SpecialOfferText;
                imgSpecialOffer.Visible = IsSpecialOfferImgShow;

                if (oProd.MonthValidity.HasValue && _PublishInfo.MonthValidity.HasValue)
                {
                    ddlMonthValid.SelectedValue = (oProd.MonthValidity.Value == _PublishInfo.MonthValidity.Value) ? oProd.MonthValidity.Value.ToString() : _PublishInfo.MonthValidity.Value.ToString();
                    imgMonthValid.Visible = (oProd.MonthValidity.Value == _PublishInfo.MonthValidity.Value) ? false : true;
                }
                else if (oProd.MonthValidity.HasValue)
                {
                    ddlMonthValid.SelectedValue = oProd.MonthValidity.Value.ToString();
                }
                else if (_PublishInfo.MonthValidity.HasValue)
                {
                    ddlMonthValid.SelectedValue = _PublishInfo.MonthValidity.Value.ToString();
                    imgMonthValid.Visible = true;
                }
            }
            else
            {
                ddlShortOrder.SelectedValue = _oProduct.GetProductShortOrder(Master.SiteID, pId).ToString();
                chkIsElectronic.Checked = oProd.IsElectronicPass;
                TxtElectronic.Text = oProd.ElectronicPassNote;
                txtSpecialOfferText.Text = oProd.SpecialOfferText;
                TxtStartDayText.Text = oProd.StartDayText;
                ddlCurrency.SelectedValue = oProd.CurrencyID.ToString();
                chkShipping.Checked = oProd.IsShippingAplicable;
                if (oProd.IsSpecialOffer == true)
                    rdnSpecialOffer.SelectedValue = "0";
                else if (oProd.BestSeller == true)
                    rdnSpecialOffer.SelectedValue = "1";
                else
                    rdnSpecialOffer.SelectedValue = "2";

                chkTwinPass.Checked = oProd.Istwinpass;
                chkAllowRefund.Checked = oProd.IsAllowRefund;
                chkAllowMiddleName.Checked = oProd.IsAllowMiddleName;
                chkAllowAdminPass.Checked = oProd.IsAllowAdminPass;
                chkIsPromoPass.Checked = oProd.IsPromoPass;
                if (oProd.MonthValidity != null)
                    ddlMonthValid.SelectedValue = oProd.MonthValidity.ToString();

                chkIsActv.Checked = oProd.IsActive;
                chkpassport.Checked = (oProd.PassportValid.HasValue) ? oProd.PassportValid.Value : false;
                chkNationality.Checked = (oProd.NationalityValid.HasValue) ? oProd.NationalityValid.Value : false;
                txtProductDesc.InnerHtml = oProd.Description;
                txtDiscount.InnerHtml = oProd.Discount;
                txtEligibility.InnerHtml = oProd.Eligibility;
                txtEligibilityforSaver.InnerHtml = oProd.EligibilityforSaver;
                txtTerm.InnerHtml = oProd.TermCondition;
                txtValidity.InnerHtml = oProd.Validity;
                txtGreatPass.InnerHtml = oProd.GreatPassDescription;
                txtareaAnnouncement.InnerHtml = oProd.Announcement;
                txtAnnouncement.Text = oProd.AnnouncementTitle;
                txtTitle.Text = oProd.IrTitle;
                txtDesciption.InnerHtml = oProd.IrDesciption;
            }
            #endregion

            showpricebind = (oProd.CountryStartCode <= 2000 || oProd.CountryStartCode == 9999) ? "class='hide'" : "";
            txtFromDate.Text = oProd.ProductValidFromDate.ToString("dd/MM/yyyy HH:mm");
            txtToDate.Text = oProd.ProductValidToDate.ToString("dd/MM/yyyy HH:mm");
            txtVisibleFromDate.Text = oProd.ProductEnableFromDate.ToString("dd/MM/yyyy HH:mm");
            txtVisibleToDate.Text = oProd.ProductEnableToDate.ToString("dd/MM/yyyy HH:mm");
            if (isBritrailPromo.Visible)
            {
                txtBritrailFromDate.Text = oProd.BritrailPromoFrom.HasValue ? oProd.BritrailPromoFrom.Value.ToString("dd/MM/yyyy HH:mm") : "";
                txtBritrailToDate.Text = oProd.BritrailPromoTo.HasValue ? oProd.BritrailPromoTo.Value.ToString("dd/MM/yyyy HH:mm") : "";
            }
            imgBanner.Src = !string.IsNullOrEmpty(oProd.BannerImage) ? "../" + oProd.BannerImage : "~/CMSImages/sml-noimg.jpg";
            imgMap.Src = !string.IsNullOrEmpty(oProd.CounrtyImage) ? "../" + oProd.CounrtyImage : "~/CMSImages/sml-noimg.jpg";
            hdnCountryImg.Value = oProd.CounrtyImage;
            hdnBannerImg.Value = oProd.BannerImage;

            txtPrdtAltTag1.Text = oProd.ProductAltTag1;
            txtPrdtAltTag2.Text = oProd.ProductAltTag2;

            prdtImg.Src = !string.IsNullOrEmpty(oProd.ProductImage) ? "../" + oProd.ProductImage : "~/CMSImages/sml-noimg.jpg";
            hdnprdtImage.Value = oProd.ProductImage;

            prdtImg2.Src = !string.IsNullOrEmpty(oProd.ProductImageSecond) ? "../" + oProd.ProductImageSecond : "~/CMSImages/sml-noimg.jpg";
            hdnprdtImage2.Value = oProd.ProductImageSecond;

            hdnHomepageprodtimage.Value = oProd.HomePageProductImage;
            HomepageprdtImg.Src = !string.IsNullOrEmpty(oProd.HomePageProductImage) ? "../" + oProd.HomePageProductImage : "~/CMSImages/sml-noimg.jpg";

            chkVisibleFromDate.Checked = oProd.IsProductEnableFromDate;
            chkVisibleToDate.Checked = oProd.IsProductEnableToDate;
            try
            {
                if (oProd.CountryStartCode == 9999)
                {
                    ddlCountryRange.SelectedValue = "9999";
                    dtlCountry.Visible = true;
                }
                else if (oProd.CountryStartCode >= 1 && oProd.CountryStartCode <= 1000)
                {
                    if (ddlCountryRange.SelectedValue != "1-1000")
                    {
                        ddlCountryRange.SelectedValue = "1001-1999";
                        ddlCountryRange.SelectedItem.Value = "1-1000";
                    }
                    ddlCountryRange_SelectedIndexChanged(new object(), new EventArgs());
                    ddlRegionalCountryPass.SelectedValue = oProd.CountryStartCode.ToString();
                    ddlRegionalCountryPass.Visible = ltrRegContryName.Visible = true;
                }
                else if (oProd.CountryStartCode >= 1001 && oProd.CountryStartCode <= 2000)
                {
                    ddlCountryRange.SelectedValue = "1001-1999";
                    ddlCountryRange_SelectedIndexChanged(new object(), new EventArgs());
                    ddlRegionalCountryPass.SelectedValue = oProd.CountryStartCode.ToString();
                    ddlRegionalCountryPass.Visible = ltrRegContryName.Visible = true;
                }
                else
                {
                    ddlCountryRange.SelectedValue = oProd.CountryStartCode.ToString() + "-" + oProd.CountryEndCode.ToString();
                }
            }
            catch { }

            Guid langId = _oProduct.GetLanguageId();
            ddlLanguage.SelectedValue = langId.ToString();
            var oprodname = oProdNameList.FirstOrDefault(x => x.LanguageID == langId);
            if (oprodname != null)
            {
                hdnProdNmId.Value = oprodname.ID.ToString();
                txtProductName.Text = oprodname.Name;
                rdoMissing.Checked = oprodname.DefaultWhenMissing;
            }
            oProdNameList.Remove(oprodname);
            var oList = oProdNameList;
            var listItm = new List<RepeaterListItem>();
            listItm.AddRange(oList.Select(x => new RepeaterListItem
            {
                Id = x.ID,
                Name = x.Name,
                IsMissing = x.DefaultWhenMissing,
                LanguageId = x.LanguageID.ToString(),
                NoOfItems = oList.Count
            }));
            if (listItm.Count > 0)
                BindRepeter(listItm);
            else
            {
                ViewState["NoOfItems"] = 0;
                BindRepeter(null);
            }
            BindProductPrice();
            //-Countries
            var listProdCuntr = _oProduct.GetProductsCountresById(pId).Select(x => x.CountryID);
            foreach (DataListItem item in dtlCountry.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var treeSite = item.FindControl("trSites") as TreeView;
                    if (treeSite == null)
                        return;
                    foreach (TreeNode node in treeSite.Nodes)
                    {
                        node.Checked = listProdCuntr.Contains(Guid.Parse(node.Value));
                    }
                }
            }

            //Permitted Country List
            var listPermittedCnt = _oProduct.GetPermittedCountresById(pId).Select(x => x.CountryID);
            foreach (DataListItem item in dtlCtPermitted.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var treePSite = item.FindControl("trPSites") as TreeView;
                    if (treePSite == null)
                        return;
                    foreach (TreeNode node in treePSite.Nodes)
                    {
                        node.Checked = listPermittedCnt.Contains(Guid.Parse(node.Value));
                    }
                }
            }
            try
            { /*If Category not allowed for site*/
                string id = _oProduct.GetCategoryById(pId).ToString();
                ddlCategory.SelectedValue = id;
                ddlCategory_SelectedIndexChanged(new object(), new EventArgs());
            }
            catch (Exception ex)
            {
                Response.Redirect("ProductList.aspx");
            }
        }

        void BindTravellerValidPass()
        {
            try
            {
                var trvlist = _oMasters.GetProductValidUpToNameList().Where(x => x.IsActive);
                if (list != null)
                {
                    ddlProductValidUpTo.DataSource = trvlist;
                    ddlProductValidUpTo.DataTextField = "Name";
                    ddlProductValidUpTo.DataValueField = "ID";
                    ddlProductValidUpTo.DataBind();
                    ddlProductValidUpTo.Items.Insert(0, new ListItem("--Select Traveller Validity--", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                Guid id = ddlCategory.SelectedValue != "0" ? Guid.Parse(ddlCategory.SelectedValue) : new Guid();
                List<Guid> siteIdList = _oProduct.GetSiteIdByCatIdList(id);
                BindSiteOnSelectedCategory(siteIdList);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BindSiteOnSelectedCategory(List<Guid> siteList)
        {
            try
            {
                rptSite.DataSource = siteList == null ? null : _oMasters.GetSiteList().Where(x => x.IsActive == true && siteList.Contains(x.ID)).ToList();
                rptSite.DataBind();
                GetAllSitesByProductId(siteList);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void ddlCountryRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            UncheckCountryTree();
            switch (ddlCountryRange.SelectedValue)
            {
                case "1-1000"://one country Interrail
                    ddlRegionalCountryPass.Visible = ltrRegContryName.Visible = true;
                    dtlCountry.Visible = false;
                    ltrRegContryName.Text = "One Country :";
                    FillOneCountryAndRegionalCoutryCode(1);
                    break;
                case "1001-1999"://one country
                    ddlRegionalCountryPass.Visible = ltrRegContryName.Visible = true;
                    dtlCountry.Visible = false;
                    ltrRegContryName.Text = "One Country :";
                    FillOneCountryAndRegionalCoutryCode(1);
                    break;
                case "9999"://global country
                    ltrRegContryName.Visible = ddlRegionalCountryPass.Visible = false;
                    dtlCountry.Visible = true;
                    break;
                default:
                    dtlCountry.Visible = ltrRegContryName.Visible = ddlRegionalCountryPass.Visible = false;
                    break;
            }
        }

        void FillOneCountryAndRegionalCoutryCode(int country)
        {

            ddlRegionalCountryPass.Items.Clear();
            ddlRegionalCountryPass.DataSource = country == 1 ? (IsInterrailPass ? _oProduct.InterrailOneCountryCode() : _oProduct.OneCountryCode()) : _oProduct.RegionalCountryCode();
            ddlRegionalCountryPass.DataTextField = "Name";
            ddlRegionalCountryPass.DataValueField = "CountryCode";
            ddlRegionalCountryPass.DataBind();
        }

        protected void dtlCountry_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var trSites = e.Item.FindControl("trSites") as TreeView;
                var hdnRegionId = e.Item.FindControl("hdnRegion") as HiddenField;
                var lisCountry = _oMasters.GetCountryList().Where(x => hdnRegionId != null && (x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value)));
                foreach (var itemCountry in lisCountry)
                {
                    var trCat = new TreeNode();
                    trCat.Text = itemCountry.CountryName;
                    trCat.Value = itemCountry.CountryID.ToString();
                    trCat.SelectAction = TreeNodeSelectAction.None;
                    if (trSites != null) trSites.Nodes.Add(trCat);
                }
            }
        }

        protected void dtlCtPermitted_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var trSites = e.Item.FindControl("trPSites") as TreeView;
                var hdnRegionId = e.Item.FindControl("hdnRegion") as HiddenField;
                var lisCountry = new List<tblCountriesMst>();
                if (IsInterrailPass && hdnRegionId != null && hdnRegionId.Value == "6229b10e-6fc0-4c68-9b16-2c3116a81217")
                    lisCountry = _oMasters.GetCountryList().Where(x => (x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value)) && x.IsInterrail).ToList();
                else
                    lisCountry = _oMasters.GetCountryList().Where(x => hdnRegionId != null && (x.IsActive == true && x.RegionID == Guid.Parse(hdnRegionId.Value)) && x.IsEurail).ToList();

                foreach (var itemCountry in lisCountry)
                {
                    var trCat = new TreeNode();
                    trCat.Text = itemCountry.CountryName;
                    trCat.Value = itemCountry.CountryID.ToString();
                    trCat.SelectAction = TreeNodeSelectAction.None;
                    if (trSites != null) trSites.Nodes.Add(trCat);
                }
            }
        }

        void UncheckCountryTree()
        {
            foreach (DataListItem item in dtlCountry.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var treeSite = item.FindControl("trSites") as TreeView;
                    foreach (TreeNode node in treeSite.Nodes)
                    {
                        node.Checked = false;
                    }
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupCountryImg.HasFile)
                {
                    if (fupCountryImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Country Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupCountryImg.FileName.Substring(fupCountryImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    cImagepath = "~/Uploaded/ProductCountryImg/";
                    Guid newFileName = Guid.NewGuid();
                    cImagepath = cImagepath + oCom.CropImage(newFileName, fupCountryImg, cImagepath, 190, 272);
                }

                if (fupBannerImg.HasFile)
                {
                    if (fupBannerImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupBannerImg.FileName.Substring(fupBannerImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    cBannerpath = "~/Uploaded/ProductBannerImg/";
                    Guid newFileName = Guid.NewGuid();
                    cBannerpath = cBannerpath + oCom.CropImage(newFileName, fupBannerImg, cBannerpath, 370, 1400);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool uploadPrdtImg()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (FupldPdtImg.HasFile)
                {
                    if (FupldPdtImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Product Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = FupldPdtImg.FileName.Substring(FupldPdtImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    PrdtImagepath = "~/Uploaded/ProductImages/";
                    Guid newFileName = Guid.NewGuid();
                    PrdtImagepath = PrdtImagepath + oCom.CropImage(newFileName, FupldPdtImg, PrdtImagepath, 180, 260);
                    oCom.CropImage(newFileName, FupldPdtImg, "~/Uploaded/STAProductImages/", 180, 260);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool uploadPrdtImg2()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (FupldPdtImg2.HasFile)
                {
                    if (FupldPdtImg2.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Product Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = FupldPdtImg2.FileName.Substring(FupldPdtImg2.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    PrdtImagepath2 = "~/Uploaded/ProductImages/";
                    Guid newFileName = Guid.NewGuid();
                    PrdtImagepath2 = PrdtImagepath2 + oCom.CropImage(newFileName, FupldPdtImg2, PrdtImagepath2, 180, 260);
                    oCom.CropImage(newFileName, FupldPdtImg2, "~/Uploaded/STAProductImages/", 180, 260);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool uploadHomePagePrdtImg()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (FupldHomeprdtimg.HasFile)
                {
                    if (FupldHomeprdtimg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Homepage Product Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = FupldHomeprdtimg.FileName.Substring(FupldHomeprdtimg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    HomePrdtImagepath = "~/Uploaded/ProductImages/";
                    Guid newFileName = Guid.NewGuid();
                    HomePrdtImagepath = HomePrdtImagepath + oCom.CropImage(newFileName, FupldHomeprdtimg, HomePrdtImagepath, 220, 220);
                    oCom.CropImage(newFileName, FupldHomeprdtimg, "~/Uploaded/STAProductImages/", 220, 220);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptProductPrice_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lblTravellerId = e.Item.FindControl("lblTravellerId") as Label;
            DropDownList ddlTravellerName = e.Item.FindControl("ddlTravellerName") as DropDownList;
            if (ddlTravellerName != null)
            {
                ddlTravellerName.DataSource = _oMasters.GetTravelerListWithType().Where(x => x.IsActive);
                ddlTravellerName.DataTextField = "Name";
                ddlTravellerName.DataValueField = "ID";
                ddlTravellerName.DataBind();
                ddlTravellerName.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlTravellerName.SelectedValue = lblTravellerId.Text.Trim();
            }
            HiddenField hdnpriceband = e.Item.FindControl("hdnpriceband") as HiddenField;
            DropDownList ddlpriceband = e.Item.FindControl("ddlpriceband") as DropDownList;
            if (ddlpriceband != null)
            {
                ddlpriceband.DataSource = _PriceBand.GetPriceBandList().Where(t => t.IsActive).ToList();
                ddlpriceband.DataTextField = "Name";
                ddlpriceband.DataValueField = "ID";
                ddlpriceband.DataBind();
                ddlpriceband.SelectedValue = hdnpriceband.Value;
            }
        }

        protected void btnSaveCrossSale_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    Guid pId = Guid.Parse(Request.QueryString["id"]);
                    tblCrossSaleProductLookup obj = new tblCrossSaleProductLookup();
                    obj.Id = Guid.NewGuid();
                    obj.ProductId = pId;
                    obj.CrossSaleAdsenseId = Guid.Parse(ddlCrossSaleAdsense.SelectedItem.Value);
                    obj.CrossSaleAdsenseId = Guid.Parse(ddlCrossSaleAdsense.SelectedItem.Value);
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = AdminuserInfo.UserID;
                    _oMasters.AddCrossSaleProductLookup(obj);
                }
                BindCrossSaleRptr();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BindCrossSaleRptr()
        {
            if (Request.QueryString["id"] != null)
            {
                Guid pId = Guid.Parse(Request.QueryString["id"]);
                var crosslist = _oMasters.GetCrossSaleProductLookup(pId);
                rptCrossSaleAdsense.DataSource = crosslist.OrderBy(x => x.Name);
                rptCrossSaleAdsense.DataBind();
            }
        }

        protected void brnRemoveCrossSaleByid_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnCrossSaleRemoveId.Value))
            {
                Guid id = Guid.Parse(hdnCrossSaleRemoveId.Value);
                _oMasters.DeleteCrossSaleProductLookup(id);
                hdnCrossSaleRemoveId.Value = string.Empty;
            }
            BindCrossSaleRptr();
        }

        protected void ddlSupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            var data = _oProduct.GetSupplierList().Where(x => x.IsActive && x.Name == ddlSupName.SelectedValue).ToList();
            //if (data.FirstOrDefault().Currency.HasValue)
            //    txtsupcurrency.Text = _oMasters.GetCurrencyList().FirstOrDefault(x => x.ID == data.FirstOrDefault().Currency.Value).Name;
            //else
            //    txtsupcurrency.Text = "Not Set";
            ddlSupCat.DataSource = data.Select(x => new { x.Category, x.Id }).ToList();
            ddlSupCat.DataTextField = "Category";
            ddlSupCat.DataValueField = "Id";
            ddlSupCat.DataBind();
            ddlSupCat.Items.Insert(0, new ListItem("--Select Supplier Category--", "0"));
        }

        public void GetAllSitesByProductId(List<Guid> siteList)
        {
            List<ManagePurchageProduct> dataList = _oMasters.GetSiteListBySiteIds(siteList, pId).ToList();
            rptDynamicPurchageDate.DataSource = dataList;
            rptDynamicPurchageDate.DataBind();
            if (isProductVisibleFromToDate)
                chkVisibleFromDate.Checked = chkVisibleToDate.Checked = true;
            else
            {
                if (dataList != null && dataList.Count > 0)
                {
                    if (dataList.Any(x => x.IsProductEnableFromDate) || dataList.Any(x => x.IsProductEnableToDate))
                        chkVisibleFromDate.Checked = chkVisibleToDate.Checked = false;
                    else
                        chkVisibleFromDate.Checked = chkVisibleToDate.Checked = true;
                }
                else
                    chkVisibleFromDate.Checked = chkVisibleToDate.Checked = true;
            }
        }

        bool AddUpdateProductValid()
        {
            if (rptDynamicPurchageDate.Items.Count > 0)
            {
                for (int i = 0; i < rptDynamicPurchageDate.Items.Count; i++)
                {
                    var hdnSiteID = rptDynamicPurchageDate.Items[i].FindControl("hdnSiteID") as HiddenField;
                    var hdnSiteProductId = rptDynamicPurchageDate.Items[i].FindControl("hdnSiteProductId") as HiddenField;
                    var txtVisibleDateFrom = rptDynamicPurchageDate.Items[i].FindControl("txtVisibleDateFrom") as TextBox;
                    var txtVisibleDateTo = rptDynamicPurchageDate.Items[i].FindControl("txtVisibleDateTo") as TextBox;
                    var chkVisibleDateFrom = rptDynamicPurchageDate.Items[i].FindControl("chkVisibleDateFrom") as CheckBox;
                    var chkVisibleDateTo = rptDynamicPurchageDate.Items[i].FindControl("chkVisibleDateTo") as CheckBox;
                    tblProductSiteLookUp _lookup = new tblProductSiteLookUp
                    {
                        ProductID = pId,
                        SiteID = Guid.Parse(hdnSiteID.Value),
                        ProductEnableFromDate = !string.IsNullOrEmpty(txtVisibleDateFrom.Text) ? DateTime.ParseExact((txtVisibleDateFrom.Text), "dd/MM/yyyy HH:mm", null) : (DateTime?)null,
                        ProductEnableToDate = !string.IsNullOrEmpty(txtVisibleDateTo.Text) ? DateTime.ParseExact((txtVisibleDateTo.Text), "dd/MM/yyyy HH:mm", null) : (DateTime?)null,
                        IsProductEnableFromDate = chkVisibleDateFrom.Checked,
                        IsProductEnableToDate = chkVisibleDateTo.Checked,
                    };
                    _oMasters.AddUpdateProductValid(_lookup);
                }
                return true;
            }
            return false;
        }

        protected void btnPageSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = _oProduct.AddProductPublish(new tblProductPublish
                {
                    ID = Guid.NewGuid(),
                    ProductID = pId,
                    LanguageID = Guid.Parse(ddlLanguage.SelectedValue),
                    Name = txtProductName.Text,
                    CurrencyID = Guid.Parse(ddlCurrency.SelectedValue),
                    MonthValidity = Convert.ToInt32(ddlMonthValid.SelectedValue),
                    DefaultWhenMissing = rdoMissing.Checked,
                    IsActive = chkIsActv.Checked,
                    Istwinpass = chkTwinPass.Checked,
                    PassportValid = chkpassport.Checked,
                    IsShippingAplicable = chkShipping.Checked,
                    IsAllowRefund = chkAllowRefund.Checked,
                    IsAllowMiddleName = chkAllowMiddleName.Checked,
                    IsAllowAdminPass = chkAllowAdminPass.Checked,
                    IsPromoPass = chkIsPromoPass.Checked,
                    IsSpecialOffer = rdnSpecialOffer.SelectedValue == "0" ? true : false,
                    BestSeller = rdnSpecialOffer.SelectedValue == "1" ? true : false,
                    NationalityValid = chkNationality.Checked,
                    ShortOrder = Convert.ToInt32(ddlShortOrder.SelectedValue),
                    SpecialOfferText = txtSpecialOfferText.Text,
                    StartDayText = TxtStartDayText.Text,
                    IsElectronicPass = chkIsElectronic.Checked,
                    ElectronicPassNote = TxtElectronic.Text,
                    IrTitle = txtTitle.Text,
                    IrDesciption = txtDesciption.InnerHtml,
                    AnnouncementTitle = txtAnnouncement.Text,
                    Announcement = txtareaAnnouncement.InnerHtml,
                    GreatPassDescription = txtGreatPass.InnerHtml,
                    Description = txtProductDesc.InnerHtml,
                    Eligibility = txtEligibility.InnerHtml,
                    EligibilityforSaver = txtEligibilityforSaver.InnerHtml,
                    Discount = txtDiscount.InnerHtml,
                    Validity = txtValidity.InnerHtml,
                    TermCondition = txtTerm.InnerHtml,
                    IsPublish = false,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                });
                if (result)
                {
                    tr_publish_text1.Visible = tr_publish_text2.Visible = true;
                    var publishInfo = _oProduct.LastPublishInfo(pId);
                    if (publishInfo != null)
                        span_publish.InnerText = " " + publishInfo.LastPublishDate + " (" + publishInfo.LastPublishBy + ")";
                }
                GetProductDetailForEdit();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "padeloadcall", "padeloadcall();", true);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public bool UpdatePublishStatus()
        {
            try
            {
                var _PublishInfo = _oProduct.GetProductPublishInfo(pId, AdminuserInfo.UserID);
                if (_PublishInfo != null)
                    return _oProduct.UpdateProductPublishStatus(pId, AdminuserInfo.UserID);
                return false;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); return false; }
        }

        public void SetPublishImageVisibleFalse()
        {
            imgProductName.Visible = imgLanguage.Visible = imgCurrency.Visible = imgMonthValid.Visible = imgMissing.Visible = imgIsActv.Visible = imgTwinPass.Visible = false;
            imgPassport.Visible = imgShipping.Visible = imgAllowRefund.Visible = imgAllowMiddleName.Visible = imgAllowAdminPass.Visible = imgIsPromoPass.Visible = imgSpecialOffer.Visible = false;
            imgNationality.Visible = imgShortOrder.Visible = imgSpecialOfferText.Visible = imgStartDayText.Visible = imgIsElectronic.Visible = imgElectronic.Visible = imgTitle.Visible = false;
            imgDesciption.Visible = imgAnnouncement.Visible = imgGreatPass.Visible = imgProductDesc.Visible = imgEligibility.Visible = imgEligibilityforSaver.Visible = imgDiscount.Visible = imgValidity.Visible = imgTerm.Visible = false;
        }
    }
}
