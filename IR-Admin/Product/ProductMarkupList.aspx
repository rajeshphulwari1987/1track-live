﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductMarkupList.aspx.cs" Inherits="IR_Admin.Product.ProductMarkupList" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="<%=ConfigurationSettings.AppSettings["HttpAdminHost"] %>Styles/colortip-1.0-jquery.css"
        rel="stylesheet" type="text/css" />
    <script src="<%=ConfigurationSettings.AppSettings["HttpAdminHost"] %>Scripts/colortip-1.0-jquery.js"
        type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.yellow').colorTip({ color: 'yellow' });
        });
    </script>
    <style type="text/css">
        .style1
        {
            width: 682px;
        }
    </style>
     <script type="text/javascript">
         window.setTimeout("closeDiv();", 5000);
         function closeDiv() {
             $("#MainContent_DivSuccess").fadeOut("slow", null);
             $("#MainContent_DivError").fadeOut("slow", null);
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlProductMarkUp" runat="server">
        <ContentTemplate>
            <h2>
                Product Markup List</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="ProductMarkupList.aspx" class="current">List</a></li>
                    <li style="font-size: 14px;">
                        <asp:HyperLink ID="aNew" NavigateUrl="ProductMarkup.aspx" CssClass="" runat="server">New</asp:HyperLink>
                    </li>
                </ul>
                <br />
            </div>
            <!-- tab "panes" -->
            <div class="full mr-tp1" style="margin-top: 0px">
                <div class="panes">
                    <div id="divNew" runat="server" style="display: block;">
                        <asp:HiddenField runat="server" ID="hdnPath" />
                        <asp:Repeater ID="rptProductMarkUp" runat="server" OnItemCommand="rptProductMarkUp_ItemCommand">
                            <HeaderTemplate>
                                <table style="width: 100%; font-weight: bold;" class="grid-head2">
                                    <tr>
                                        <td style="width: 25%; padding-left: 1%;">
                                           Markup Category
                                        </td>
                                         <td style="width: 15%;">
                                            Number of Items
                                        </td>
                                        <td style="width: 10%;">
                                            Markup (Price)
                                        </td>
                                        <td style="width: 10%;">
                                            Markup (%)
                                        </td>
                                        <td style="width: 10%; text-align: center;">
                                            Action
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <AlternatingItemTemplate>
                                <div class="cat-inner-alt" style="margin: 0px; width: 100%">
                                     <div style="width: 35%; float: left; padding-left: 1%">
                                        <%#Eval("CategoryName")%>
                                    </div>
                                     <div style="width: 15%; float: left; padding-left: 1%">
                                        <%#Eval("ItemCount")%>
                                    </div>
                                    <div style="width: 15%; float: left; text-align: center; padding-left: 1%;">
                                       <%#Eval("Symbol")%>  <%#Eval("MarkUpAmount")%>
                                    </div>
                                    <div style="width: 15%; float: left; text-align: center;">
                                        <%#Eval("MarkUpPercent")%>
                                    </div>
                                    <div style="width: 10%; float: left; text-align: right">
                                        <a href='ProductMarkup.aspx?id=<%#Eval("CategoryID") %>'>
                                            <img src="../images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete" 
                                            CommandArgument='<%#Eval("CategoryID") %>' CommandName="Delete" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                            <ItemTemplate>
                                <div class="cat-inner" style="margin: 0px; width: 100%">
                                    <div style="width: 35%; float: left; padding-left: 1%">
                                        <%#Eval("CategoryName")%>
                                    </div>
                                     <div style="width: 15%; float: left; padding-left: 1%">
                                        <%#Eval("ItemCount")%>
                                    </div>
                                    <div style="width: 15%; float: left; text-align: center; padding-left: 1%;">
                                       <%#Eval("Symbol")%>  <%#Eval("MarkUpAmount")%>
                                    </div>
                                    <div style="width: 15%; float: left; text-align: center;">
                                        <%#Eval("MarkUpPercent")%>
                                    </div>
                                    <div style="width: 10%; float: left; text-align: right">
                                        <a href='ProductMarkup.aspx?id=<%#Eval("CategoryID") %>'>
                                            <img src="../images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete" 
                                            CommandArgument='<%#Eval("CategoryID") %>' CommandName="Delete" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div id="divRecordNotfnd" runat="server" class="warning" style="display: none;">
                            Record not found.
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
