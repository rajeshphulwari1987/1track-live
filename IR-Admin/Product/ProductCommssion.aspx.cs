﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace IR_Admin.Product
{
    public partial class ProductCommssion : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        db_1TrackEntities db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            ShowMessage(0, null);
            if (!IsPostBack)
            {
                BindBranchTreeView();
            }
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
        }
        protected void BindBranchTreeView()
        {
           
            var query = (from m in db.tblBranches
                         join c in db.tblOfficeCommissionLookups on m.ID equals c.OfficeID into ps
                         from c in ps.DefaultIfEmpty()
                         orderby m.OfficeName
                         select new { ID = m.TreeID, ParentID = m.TreeParentBranchID, Text = m.OfficeName,Commission=c==null?0:c.Commission }).ToList();

            DataSet dataSet = new DataSet();
            dataSet.Tables.Add("Table");
            dataSet.Tables[0].Columns.Add("ID", typeof(int));
            dataSet.Tables[0].Columns.Add("ParentID", typeof(int));
            dataSet.Tables[0].Columns.Add("Text", typeof(string));
           

            foreach (var item in query)
            {
                DataRow row = dataSet.Tables[0].NewRow();
                row["ID"] = item.ID;
                if (item.ParentID != null)
                {
                    row["ParentID"] = item.ParentID;
                }
                StringBuilder nodeText = new StringBuilder();
                nodeText = nodeText.Append(item.Text +"<span class='cms'> Commission : "+item.Commission.ToString()+"%</span>");
                nodeText.Append(@"&nbsp<INPUT id=""btnedit").Append(item.ID.ToString()).Append(@""" type=""image""  src=""../images/edit.png""  title=""Edit Commission"" onclick=""editOffice(").Append(item.ID.ToString()).Append(@")"">&nbsp&nbsp");
                row["Text"] = nodeText.ToString();
                dataSet.Tables[0].Rows.Add(row);
            }

            trBranch.DataSource = new HierarchicalDataSet(dataSet, "ID", "ParentID");
            trBranch.DataBind();
            trBranch.CollapseAll();

        }

      

        protected void btneditOffice_Click(object sender, EventArgs e)
        {
            try
            {
                decimal officeCommission = new Masters().GetOfficeCommission(Convert.ToInt32(hdnBranchid.Value));
                txtCommission.Text = officeCommission.ToString();
                mdpupStock.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void ClearControls()
        {
            txtCommission.Text = "";
        }
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int OfficeId = Convert.ToInt32(hdnBranchid.Value);
            Decimal commission = Convert.ToDecimal(txtCommission.Text.Trim());
            new Masters().ManageOfficeCommission(OfficeId, commission);
            BindBranchTreeView();
        }
        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductCommssion.aspx");
        }
        protected void trBranch_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                hdnBranchid.Value = trBranch.SelectedNode.Value.ToString();
                tab = "2";
                ViewState["tab"] = "2";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

       
    }
}
