﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class DiscountCoupon : System.Web.UI.Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private Masters _oMasters = new Masters();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            PageLoadEvent();

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _SiteID = this.Master.SiteID;
                PageLoadEvent();
            }
        }

        void PageLoadEvent()
        {
            try
            {
                _SiteID = Master.SiteID;
                ddlSites.Items.Clear();
                ddlSites.DataSource = _oMasters.Branchlist().Where(x => x.IsActive == true);
                ddlSites.DataTextField = "DisplayName";
                ddlSites.DataValueField = "ID";
                ddlSites.DataBind();
                ddlSites.Items.Insert(0, new ListItem("--Select Site--", "0"));
                ddlSites.SelectedValue = _SiteID.ToString();
                BindCategory();

            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void BindCategory()
        {
            if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                _SiteID = this.Master.SiteID;
            else
                _SiteID = Guid.Parse(ddlSites.SelectedValue);

            var list = _oProduct.GetCategoryDiscountList(3, _SiteID);
            grvCategories.DataSource = list;
            grvCategories.DataBind();

            Session["CatIds"] = list.Select(t => t.CategoryId).ToList();
            ddlCategory.DataSource = list;
            ddlCategory.DataTextField = "CategoryName";
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
            if (ViewState["CategoryIndx"] != null)
                ddlCategory.SelectedIndex = Convert.ToInt32(ViewState["CategoryIndx"].ToString());
        }

        void BindProductGrid()
        {
            if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                _SiteID = this.Master.SiteID;
            else
                _SiteID = Guid.Parse(ddlSites.SelectedValue);

            List<Guid> categoriesId = new List<Guid>();
            if (ddlCategory.SelectedIndex > 0)
                categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };
            else
            {
                if (Session["CatIds"] != null)
                    categoriesId = Session["CatIds"] as List<Guid>;
            }

            var list = _oProduct.GetProductDiscountList(_SiteID, categoriesId);
            grvProduct.DataSource = list;
            grvProduct.DataBind();
        }

        protected void grvCategories_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCategories.PageIndex = e.NewPageIndex;
            BindCategory();
        }

        protected void grvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnCategoryID = e.Row.FindControl("hdnId") as HiddenField;
                Label lblHaveDifferentComm = e.Row.FindControl("lblHaveDifferentComm") as Label;
                if (hdnCategoryID != null && lblHaveDifferentComm != null)
                {
                    Boolean rval = false;
                    rval = _oProduct.CheckDiffDiscountForCategory(Guid.Parse(hdnCategoryID.Value.Trim()), Guid.Parse(ddlSites.SelectedValue));
                    if (rval == true)
                        lblHaveDifferentComm.Text = "<a href='#' title='Related Products have different discount code, value or percentage.'><img src='../images/info.png' alt='' width='25px' style='float:left;' /></a>";
                    else
                        lblHaveDifferentComm.Text = "";

                }
            }
        }

        protected void grvProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProduct.PageIndex = e.NewPageIndex;
            BindProductGrid();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender;
                HiddenField hdnFlag = btnUpdate.Parent.FindControl("hdnFlag") as HiddenField;
                HiddenField hdnId = btnUpdate.Parent.FindControl("hdnId") as HiddenField;
                TextBox txtDiscountPrice = btnUpdate.Parent.FindControl("txtDiscountPrice") as TextBox;
                TextBox txtDiscountCode = btnUpdate.Parent.FindControl("txtDiscountCode") as TextBox;
                CheckBox chkPercent = btnUpdate.Parent.FindControl("chkIsPercent") as CheckBox;
                TextBox txtDiscountFromDate = btnUpdate.Parent.FindControl("txtDiscountFromDate") as TextBox;
                TextBox txtDiscountToDate = btnUpdate.Parent.FindControl("txtDiscountToDate") as TextBox;

                Guid Id = Guid.Parse(hdnId.Value);
                Int32 flag = Convert.ToInt32(hdnFlag.Value);
                if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                    _SiteID = this.Master.SiteID;
                else
                    _SiteID = Guid.Parse(ddlSites.SelectedValue);

                DateTime? DiscountFromDate = null;
                DateTime? DiscountToDate = null;
                if (!string.IsNullOrEmpty(txtDiscountFromDate.Text))
                    DiscountFromDate = DateTime.ParseExact((txtDiscountFromDate.Text), "dd/MM/yyyy HH:mm", null);
                if (!string.IsNullOrEmpty(txtDiscountToDate.Text))
                    DiscountToDate = DateTime.ParseExact((txtDiscountToDate.Text), "dd/MM/yyyy HH:mm", null);

                _oProduct.UpdateDiscount(Id, Convert.ToDecimal(txtDiscountPrice.Text), txtDiscountCode.Text, chkPercent.Checked, _SiteID, flag, AdminuserInfo.UserID, DiscountFromDate, DiscountToDate);
                ShowMessage(1, "Discount update successfully.");
                BindCategory();
                if (ddlCategory.SelectedIndex > 0)
                    BindProductGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "bindDatePicker", "bindDatePicker();", true);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["CategoryIndx"] = null;
            ShowMessage(0, null);
            if (ddlSites.SelectedIndex > 0)
                BindCategory();
            grvProduct.DataSource = null;
            grvProduct.DataBind();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["CategoryIndx"] = ddlCategory.SelectedIndex;
            ViewState["ProductIndx"] = null;
            BindProductGrid();
        }
    }
}