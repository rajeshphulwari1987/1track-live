﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ProductDiscount.aspx.cs"
    Inherits="IR_Admin.Product.ProductDiscount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }

        $(function () {
            if ('<%=tab%>' == '1') {
                $("ul.list").tabs("div.panes > div");
            }
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 500);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }

        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
    <style type="text/css">
        .newheader {
    vertical-align: top;
    line-height: 19px;
}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>Product Discount Codes For Site
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="ProductDiscount.aspx" class="current">List</a></li>
            <li><a id="aNew" href="ProductDiscount.aspx" class=" ">New/Edit</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdPrdDiscount" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdPrdDiscount_RowCommand"
                            AllowPaging="True" OnPageIndexChanging="grdPrdDiscount_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" CssClass="newheader"/>
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site's Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Discount From">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiscountFromDate" runat="server" Text='<%# Eval("discountFromDate", "{0:dd/MM/yyyy HH:mm}")  %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Discount To">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiscountToDate" runat="server" Text='<%# Eval("discountToDate", "{0:dd/MM/yyyy HH:mm}")  %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Discount code">
                                    <ItemTemplate>
                                        <%#Eval("Code")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discount amount">
                                    <ItemTemplate>
                                        <%#Eval("Amount")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discount(%)">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgPerc" CommandArgument='<%#Eval("Id")%>' Height="16"
                                            CommandName="IsPercent" AlternateText="IsPercent" ImageUrl='<%#Eval("IsPercentage").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsPercentage").ToString()=="True" ?"IsPercentage Active":"IsPercentage inactive" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Active">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="ActiveInActive" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="ProductDiscount.aspx?id=<%#Eval("Id")%>" title="Edit" style="text-decoration: none;">
                                            <img alt="edit" src="../images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <table width="100%">
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Name:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtName" runat="server" MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rvName" runat="server" ControlToValidate="txtName"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Discount Code:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtCode" runat="server" MaxLength="15" />
                                            <asp:RequiredFieldValidator ID="rvCode" runat="server" ControlToValidate="txtCode"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Discount From:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtDiscountFromDate" runat="server" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Discount To:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtDiscountToDate" runat="server" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Discount Amount:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtAmount" runat="server" />
                                            <asp:RequiredFieldValidator ID="rvAmount" runat="server" ControlToValidate="txtAmount"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col" valign="top">Description:
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="5" Width="350px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Discount(%):
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsPerc" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">Active:
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsActv" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="submit" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc"></td>
                        </tr>
                    </table>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link href="../date-time-Picker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="../date-time-Picker/jquery.datetimepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bindDatePicker();
        });

        function bindDatePicker() {
            $('[id*=txtDiscountFromDate]').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                step: 5,
                defaultDate: new Date()
            });
            $('[id*=txtDiscountToDate]').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'en',
                step: 5,
                defaultDate: new Date()
            });
        }
    </script>
</asp:Content>

