﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.IO;
using System.Web.UI.WebControls;
using System.Data;
using System.ComponentModel;

namespace IR_Admin.Product
{
    public partial class DownloadProduct : Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
                BindCategory();
        }
        void BindCategory()
        {

            ddlCategory.DataSource = _oProduct.GetCategories();
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
            
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (fupProduct.PostedFile.FileName != "")
                {
                    bool rs = UploadFile();
                    if (rs)
                        ShowMessage(1, "Product uploaded successfully.");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("DownloadProduct.aspx");
        }

        protected bool UploadFile()
        {
            try
            {
                var id = Guid.NewGuid();
                var ext = new[] { ".XLS", ".XLSX" };
                if (fupProduct.HasFile)
                {
                    if (fupProduct.PostedFile.ContentLength > 2097152)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded product file is larger up to 2Mb.')</script>", false);
                        return false;
                    }
                    string fExt = fupProduct.FileName.Substring(fupProduct.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    string fileExt = fExt;
                    if (!ext.Contains(fileExt.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    fileExt = id + fileExt;
                    string path = "~/Uploaded/ProductList/" + fileExt;
                    if (File.Exists(Server.MapPath(path)))
                        File.Delete(Server.MapPath(path));
                    else
                    {
                        fupProduct.SaveAs(Server.MapPath(path));
                        var dircPath = Server.MapPath(path);

                        var result = _oProduct.UpdateProductFromExcel(dircPath, AdminuserInfo.UserID, fExt);
                        if (result == 0)
                            throw new Exception("Product uploading failed!");
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lnkPrd_Click(object sender, EventArgs e)
        {

            DataTable dt = GetDataTable();  
            string fileName = string.Format("ProductPrice.xlsx");
            string fullFileName = string.Format("{0}/{1}-{2}", Path.GetTempPath(), Guid.NewGuid(), fileName);
           
            ExcelHelper helper = new ExcelHelper();
            helper.ExportDataTable(dt, fullFileName,false);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", fileName));
            Response.WriteFile(fullFileName);
            Response.End();

        }

        public DataTable GetDataTable()
        {
            var list = _oProduct.GetProductList(ddlCategory.SelectedValue);
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(GetProductList_Result));
            object[] values = new object[props.Count];
            using (DataTable table = new DataTable())
            {
                long _pCt = props.Count;
                for (int i = 0; i < _pCt; ++i)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, prop.PropertyType);
                }
                foreach (var item in list)
                {
                    long _vCt = values.Length;
                    for (int i = 0; i < _vCt; ++i)
                    {
                        values[i] = props[i].GetValue(item);
                    }
                    table.Rows.Add(values);
                }
                return table;
            }


        } 

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}