﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class Categories : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        readonly ManageProduct _product = new ManageProduct();
        public string tab = string.Empty;
        List<RepeaterListItem> list = new List<RepeaterListItem>();
        db_1TrackEntities _db = new db_1TrackEntities();
        string CategoryImagePath = string.Empty;
        public string AdminUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            btnSubmit.Enabled = true;
            if (!Page.IsPostBack)
            {
                DropDown();
                FillMenu();
                GetValueForEdit();
            }
        }

        #region Page Load Function
        private void FillMenu()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        private void DropDown()
        {
            //--I have manage categories tree for the two level 
            const int treeLevel = 2;
            _SiteID = Master.SiteID;
            ddlCategory.DataSource = _product.GetAssociateCategoryList(treeLevel, _SiteID);
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("--Category--", "0"));

            var listLang = _master.GetLanguangesList();
            Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
            ddlLanguage.DataSource = listLang.Where(x => x.ID == langid);
            ddlLanguage.DataTextField = "Name";
            ddlLanguage.DataValueField = "ID";
            ddlLanguage.DataBind();
            ViewState["NoOfItems"] = 0;


        }
        #endregion

        #region Add/Cancel Category
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Valid())
                {
                    ShowMessage(2, "You have selected one Language multiple time");
                    return;
                }
                bool isDefault = false;
                foreach (RepeaterItem item in rptrCategories.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var txtCat = item.FindControl("txtCatName") as TextBox;
                        var rdoMis = item.FindControl("rdoMissing") as RadioButton;
                        if ((rdoMis.Checked && !String.IsNullOrEmpty(txtCat.Text.Trim())) || (rdoMissing.Checked && !String.IsNullOrEmpty(txtCatName.Text.Trim())))
                            isDefault = true;
                    }
                }
                if (rptrCategories.Items.Count == 0)
                    isDefault = true;

                var isSite = trSites.Nodes.Cast<TreeNode>().Any(node => node.Checked);
                if (isDefault && isSite)
                {
                    UploadFile();
                    if (Request.QueryString["id"] != null)
                        EdiCategories();
                    else
                        AddCategories();

                }
                else
                    ShowMessage(2, "Please select Site or Default When Missing.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        protected bool Valid()
        {
            var lngIdList = new List<Guid>();
            foreach (RepeaterItem item in rptrCategories.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    var txtCat = item.FindControl("txtCatName") as TextBox;
                    var id = Guid.Parse(ddlLang.SelectedValue);

                    if (!String.IsNullOrEmpty(txtCat.Text.Trim()) && lngIdList.Any(x => x.ToString() == id.ToString()) == false)
                    {
                        lngIdList.Add(id);
                    }
                    else if (!String.IsNullOrEmpty(txtCat.Text.Trim()))
                    {
                        return false;
                    }

                }
            }
            return true;
        }
        void AddCategories()
        {
            try
            {
                //--tblCategory
                var parentId = ddlCategory.SelectedValue.Trim() == "0" ? Guid.NewGuid() : Guid.Parse(ddlCategory.SelectedValue);
                int treeLevel = _product.GetTreeLevelOfCategory(parentId) + 1;
                var id = _product.AddCategory(new tblCategory
                {
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    IsActive = chkIsActv.Checked,
                    IsPrintQueueEnable = chkEurail.Checked,
                    IsInterRailPass = chkInterRail.Checked,
                    IsFOCAD75 = chkFOCAD75.Checked,
                    IsDOB = chkDOB.Checked,
                    IsBritRailPass = chkBritRail.Checked,
                    TreeLevel = treeLevel,
                    ParentID = parentId,
                    TermCondition = txtTerm.InnerHtml,
                    Description = txtDesc.InnerHtml,
                    Validity = txtValidity.InnerHtml,
                    Eligibility = txtEligibility.InnerHtml,
                    EligibilityforSaver = txtEligibilityforSaver.InnerHtml,
                    Discount = txtDiscount.InnerHtml,
                    GreatPassDescription = txtGreatPass.InnerHtml,
                    CategoryImage = CategoryImagePath.Replace("~/", ""),
                    Announcement = txtareaAnnouncement.InnerHtml,
                    AnnouncementTitle = txtAnnouncement.Text
                });

                //--tblCategorySiteLookUp
                if (!id.ToString().Contains("0000"))
                {
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            _product.AddCategorySiteLookUp(new tblCategorySiteLookUp
                            {
                                SiteID = Guid.Parse(node.Value),
                                CategoryID = id,
                                ModifyBy = AdminuserInfo.UserID,
                                ModifyOn = DateTime.Now,
                            });
                        }
                    }
                }

                //--tblCategoriesName
                if (txtCatName.Text.Trim() != "")
                    _product.AddCategoryName(new tblCategoriesName
                    {
                        CategoryID = id,
                        LanguageID = Guid.Parse(ddlLanguage.SelectedValue),
                        Name = txtCatName.Text,
                        DefaultWhenMissing = rdoMissing.Checked
                    });

                //--tblCategoriesName
                foreach (RepeaterItem item in rptrCategories.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                        var txtCat = item.FindControl("txtCatName") as TextBox;
                        var rdoMis = item.FindControl("rdoMissing") as RadioButton;
                        if (txtCat != null && txtCat.Text.Trim() != "")
                            _product.AddCategoryName(new tblCategoriesName
                            {
                                CategoryID = id,
                                LanguageID = Guid.Parse(ddlLang.SelectedValue),
                                Name = txtCat.Text,
                                DefaultWhenMissing = rdoMis.Checked
                            });
                    }
                }

                ShowMessage(1, "You have successfully Added.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void EdiCategories()
        {
            try
            {
                var catId = Guid.Parse(Request.QueryString["id"]);
                if (Session["reovedItemList"] != null)
                {
                    var removeList = Session["reovedItemList"] as List<RepeaterListItem>;
                    if (removeList != null)
                        foreach (var cat in removeList)
                        {
                            if (!String.IsNullOrEmpty(cat.Name))
                                _product.DeleteCategoriesNameOfDiffLang(catId, Guid.Parse(cat.LanguageId), cat.Name);
                        }
                    Session["reovedItemList"] = null;
                }

                var db = new db_1TrackEntities();
                var parentId = ddlCategory.SelectedValue != "0" ? Guid.Parse(ddlCategory.SelectedValue) : Guid.NewGuid();
                int treeLevel = _product.GetTreeLevelOfCategory(parentId) + 1;
                //--Edit tblCategory table record
                var ocat = db.tblCategories.FirstOrDefault(x => x.ID == catId);
                if (ocat != null)
                {
                    ocat.ModifiedBy = AdminuserInfo.UserID;
                    ocat.ModifiedOn = DateTime.Now;
                    ocat.ID = catId;
                    ocat.ParentID = parentId;
                    ocat.TreeLevel = treeLevel;
                    ocat.IsActive = chkIsActv.Checked;
                    ocat.IsPrintQueueEnable = chkEurail.Checked;
                    ocat.IsInterRailPass = chkInterRail.Checked;
                    ocat.IsFOCAD75 = chkFOCAD75.Checked;
                    ocat.IsDOB = chkDOB.Checked;
                    ocat.IsBritRailPass = chkBritRail.Checked;
                    ocat.TermCondition = txtTerm.InnerHtml;
                    ocat.Description = txtDesc.InnerHtml;
                    ocat.Validity = txtValidity.InnerHtml;
                    ocat.Eligibility = txtEligibility.InnerHtml;
                    ocat.EligibilityforSaver = txtEligibilityforSaver.InnerHtml;
                    ocat.Discount = txtDiscount.InnerHtml;
                    ocat.GreatPassDescription = txtGreatPass.InnerHtml;
                    if (upCategoryImage.HasFile)
                        ocat.CategoryImage = CategoryImagePath.Replace("~/", "");
                    ocat.Announcement = txtareaAnnouncement.InnerHtml;
                    ocat.AnnouncementTitle = txtAnnouncement.Text;
                    _product.AddCategory(ocat);
                }
                //--Colocet checked sites
                List<Guid> listSiteId = new List<Guid>();
                foreach (TreeNode node in trSites.Nodes)
                {
                    if (node.Checked)
                    {
                        Guid sid = Guid.Parse(node.Value);
                        listSiteId.Add(sid);
                    }
                }

                //--First time i have deleted all nodes of site from look up table 
                List<tblCategorySiteLookUp> tcslkp = _product.GetLookUpSiteById(catId);
                foreach (var item in tcslkp)
                {
                    if (!listSiteId.Contains(item.SiteID) && catId == item.CategoryID)
                    {
                        var oSite = db.tblCategorySiteLookUps.FirstOrDefault(x => x.ID == item.ID);
                        db.DeleteObject(oSite);
                        db.SaveChanges();
                    }
                }


                //--Second time i have added new site id within lookup table
                foreach (TreeNode node in trSites.Nodes)
                {
                    Guid sid = Guid.Parse(node.Value);
                    if (node.Checked && !db.tblCategorySiteLookUps.Any(x => x.SiteID == sid && x.CategoryID == catId))
                    {
                        _product.AddCategorySiteLookUp(new tblCategorySiteLookUp
                        {
                            SiteID = sid,
                            CategoryID = catId,
                            ModifyBy = AdminuserInfo.UserID,
                            ModifyOn = DateTime.Now,
                        });
                    }
                }

                //--Edit tblCategoriesName table record
                if (txtCatName.Text.Trim() != "")
                {
                    var langid = ddlLanguage != null ? Guid.Parse(ddlLanguage.SelectedValue) : new Guid();
                    var oCat = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == catId && x.LanguageID == langid);
                    if (oCat != null)
                    {
                        oCat.DefaultWhenMissing = rdoMissing.Checked;
                        oCat.Name = txtCatName.Text;
                        _product.AddCategoryName(oCat);

                        ManageRoutes objroute = new ManageRoutes();
                        objroute.UpdateRouteCount(Route.CategoryRoute, 0);
                        objroute.UpdateRouteCount(Route.ProductRoute, 0);

                        db.SaveChanges();
                    }
                }

                //--tblCategoriesName
                foreach (RepeaterItem item in rptrCategories.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                        var txtCat = item.FindControl("txtCatName") as TextBox;
                        var rdoMis = item.FindControl("rdoMissing") as RadioButton;

                        if (txtCat.Text.Trim() != "")
                        {
                            //--Categories is exist then edit else Add
                            var langid = ddlLang != null ? Guid.Parse(ddlLang.SelectedValue) : new Guid();
                            var oCat = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == catId && x.LanguageID == langid);
                            if (oCat != null)
                            {
                                oCat.DefaultWhenMissing = rdoMis.Checked;
                                oCat.Name = txtCat.Text;
                                _product.AddCategoryName(oCat);
                            }
                            else
                            {
                                _product.AddCategoryName(new tblCategoriesName
                                {
                                    CategoryID = catId,
                                    LanguageID = Guid.Parse(ddlLang.SelectedValue),
                                    Name = txtCat.Text,
                                    DefaultWhenMissing = rdoMis.Checked
                                });
                            }
                            db.SaveChanges();
                        }
                    }
                }

                //add tblProductSiteLookUp
                var productList = _db.tblProductCategoriesLookUps.Where(x => x.CategoryID == catId).ToList();
                foreach (var prd in productList)
                {
                    db.tblProductSiteLookUps.Where(x => x.ProductID == prd.ProductID && !listSiteId.Contains(x.SiteID)).ToList().ForEach(db.tblProductSiteLookUps.DeleteObject);
                    db.SaveChanges();

                    foreach (TreeNode node in trSites.Nodes)
                    {

                        if (node.Checked)
                        {
                            Guid sid = Guid.Parse(node.Value);
                            if (!db.tblProductSiteLookUps.Any(x => x.ProductID == prd.ProductID && x.SiteID == sid))
                                _product.AddPrdSiteLookUp(new tblProductSiteLookUp
                                {
                                    SiteID = sid,
                                    ProductID = prd.ProductID,
                                    ModifyBy = AdminuserInfo.UserID,
                                    ModifyOn = DateTime.Now,
                                });
                        }
                    }
                }

                //--Show Message
                ShowMessage(1, "You have successfully updated this record.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CategoriesList.aspx");
        }

        #endregion

        #region BindRepeter And Add More
        protected void btnAddMore_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["NoOfItems"] = Convert.ToInt32(ViewState["NoOfItems"]) + 1;
                var newpage = new RepeaterListItem { NoOfItems = Convert.ToInt32(ViewState["NoOfItems"]) };
                list.Add(newpage);
                list = AddRepeaterItemInList();
                Session["rptItmlist"] = list;
                BindRepeter(list);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptrCategories.Items.Count > 0)
                {
                    list = AddRepeaterItemInList();

                    //--Add Removed Record in list at edit time
                    if (Request.QueryString["id"] != null)
                    {
                        var record = list.FirstOrDefault();
                        if (Session["reovedItemList"] == null)
                        {
                            var removeList = new List<RepeaterListItem> { record };
                            Session["reovedItemList"] = removeList;
                        }
                        else
                        {
                            var removeList = Session["reovedItemList"] as List<RepeaterListItem>;
                            if (removeList != null) removeList.Add(record);
                            Session["reovedItemList"] = removeList;
                        }
                    }
                    //---

                    list.RemoveAt(0);
                    Session["rptItmlist"] = list;
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        List<RepeaterListItem> AddRepeaterItemInList()
        {
            foreach (RepeaterItem item in rptrCategories.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    var txtCatName = item.FindControl("txtCatName") as TextBox;
                    var rdoMissing = item.FindControl("rdoMissing") as RadioButton;
                    var page = new RepeaterListItem
                    {
                        NoOfItems = item.ItemIndex,
                        LanguageId = ddlLang.SelectedValue,
                        Name = txtCatName.Text,
                        IsMissing = rdoMissing.Checked
                    };
                    list.Add(page);
                }
            }
            return list;
        }

        public void BindRepeter(List<RepeaterListItem> listItm)
        {
            try
            {
                if (listItm == null)
                {
                    int numOfItem = Convert.ToInt32(ViewState["NoOfItems"]);
                    for (int i = 0; i < numOfItem; i++)
                    {
                        var page = new RepeaterListItem { NoOfItems = i };
                        list.Add(page);
                    }
                    Session["rptItmlist"] = list;
                    rptrCategories.DataSource = list;
                }
                else
                    rptrCategories.DataSource = listItm;
                rptrCategories.DataBind();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        #endregion

        #region Fill Edit Record
        public void GetValueForEdit()
        {
            if (Request.QueryString["id"] != null)
            {
                ddlCategory.Enabled = false;
                btnSubmit.Text = "Update";
                var catId = Guid.Parse(Request.QueryString["id"]);
                var oCatNameList = _product.GetCategoriesNameById(catId);
                var oSiteId = _product.GetSiteIdById(catId);
                var oTblCategory = _product.GetParentCategoryById(catId);
                ddlCategory.SelectedValue = oTblCategory.ParentID.ToString();
                BindTermCondition(catId);
                chkIsActv.Checked = oTblCategory.IsActive;
                chkEurail.Checked = oTblCategory.IsPrintQueueEnable;
                chkInterRail.Checked = oTblCategory.IsInterRailPass;
                chkFOCAD75.Checked =(bool) oTblCategory.IsFOCAD75;
                chkDOB.Checked = oTblCategory.IsDOB;
                chkBritRail.Checked = oTblCategory.IsBritRailPass;
                txtTerm.InnerHtml = oTblCategory.TermCondition;
                txtDesc.InnerHtml = oTblCategory.Description;
                txtValidity.InnerHtml = oTblCategory.Validity;
                txtDiscount.InnerHtml = oTblCategory.Discount;
                txtEligibility.InnerHtml = oTblCategory.Eligibility;
                txtEligibilityforSaver.InnerHtml = oTblCategory.EligibilityforSaver;
                txtGreatPass.InnerHtml = oTblCategory.GreatPassDescription;
                txtareaAnnouncement.InnerHtml = oTblCategory.Announcement;
                txtAnnouncement.Text = oTblCategory.AnnouncementTitle;

                if (!string.IsNullOrEmpty(oTblCategory.CategoryImage))
                    imgCategoryImage.ImageUrl = AdminUrl + oTblCategory.CategoryImage;

                var langId = _product.GetLanguageId();
                ddlLanguage.SelectedValue = langId.ToString();
                var ocat = oCatNameList.FirstOrDefault(x => x.LanguageID == langId);
                if (ocat != null)
                {
                    txtCatName.Text = ocat.Name;
                    rdoMissing.Checked = ocat.DefaultWhenMissing;
                }

                oCatNameList.Remove(ocat);
                var oList = oCatNameList;

                var listItm = new List<RepeaterListItem>();
                listItm.AddRange(oList.Select(x => new RepeaterListItem
                    {
                        Name = x.Name,
                        IsMissing = x.DefaultWhenMissing,
                        LanguageId = x.LanguageID.ToString(),
                        NoOfItems = oList.Count
                    }));

                //--Bind Site Tree
                foreach (var sItem in oSiteId)
                {
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Value == sItem.ToString())
                            node.Checked = true;
                    }
                }
                if (listItm.Count > 0)
                    BindRepeter(listItm);
                else
                {
                    ViewState["NoOfItems"] = 0;
                    BindRepeter(null);
                }
            }
            else
            {
                ViewState["NoOfItems"] = 0;
                BindRepeter(null);
            }
        }
        #endregion

        #region Item command
        protected void rptrCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var ddlLang = e.Item.FindControl("ddlLanguage") as DropDownList;
                    var hdnLangId = e.Item.FindControl("hdnLangId") as HiddenField;

                    var langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                    var list = _master.GetLanguangesList();
                    if (list != null)
                    {
                        list.RemoveAll(x => x.ID == langid);
                        if (ddlLang != null) ddlLang.DataSource = list;
                    }
                    if (ddlLang != null)
                    {
                        ddlLang.DataTextField = "Name";
                        ddlLang.DataValueField = "ID";
                        ddlLang.DataBind();

                        if (hdnLangId != null && !String.IsNullOrEmpty(hdnLangId.Value))
                            ddlLang.SelectedValue = hdnLangId.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        #endregion

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            var id = ddlCategory.SelectedValue == "0" ? new Guid() : Guid.Parse(ddlCategory.SelectedValue);
            BindTermCondition(id);
        }

        void BindTermCondition(Guid id)
        {
            var oTblCategory = _product.GetCategoryTermsById(id);
            if (oTblCategory == null)
                return;

            txtTerm.InnerHtml = oTblCategory.TermCondition;
            txtDesc.InnerHtml = oTblCategory.Description;
            txtValidity.InnerHtml = oTblCategory.Validity;
            txtDiscount.InnerHtml = oTblCategory.Discount;
            txtEligibility.InnerHtml = oTblCategory.Eligibility;
            txtEligibilityforSaver.InnerHtml = oTblCategory.EligibilityforSaver;
            txtGreatPass.InnerHtml = oTblCategory.GreatPassDescription;
            txtAnnouncement.Text = oTblCategory.AnnouncementTitle;
            txtareaAnnouncement.InnerHtml = oTblCategory.Announcement;
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (upCategoryImage.HasFile)
                {
                    if (upCategoryImage.PostedFile.ContentLength > 1048576)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Category Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = upCategoryImage.FileName.Substring(upCategoryImage.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    CategoryImagePath = "~/Uploaded/CategoryImage/";
                    CategoryImagePath = CategoryImagePath + oCom.CropImage(upCategoryImage, CategoryImagePath, 200, 340);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnRemoveImage_Click(object sender, EventArgs e)
        {
            var catId = Guid.Parse(Request.QueryString["id"]);
            var data = _db.tblCategories.FirstOrDefault(x => x.ID == catId);
            if (data != null)
            {
                data.CategoryImage = null;
                _db.SaveChanges();
                imgCategoryImage.ImageUrl = "~/CMSImages/sml-noimg.jpg";
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}
