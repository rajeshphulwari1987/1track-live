﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Product.aspx.cs" Inherits="IR_Admin.Product.Product" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script src="../date-time-Picker/moment.js"></script>
    <style type="text/css">
        .ajax__calendar_day, .ajax__calendar_dayname, .ajax__calendar_title, .ajax__calendar_footer, .ajax__calendar_month, .ajax__calendar_year {
            font-size: 13px !important;
            line-height: normal;
        }

        .ajax__calendar_today, .ajax__calendar_footer {
            margin-bottom: 5px;
            line-height: normal;
        }

        .ajax__calendar .ajax__calendar_hover .ajax__calendar_day {
            line-height: normal;
        }

        .ajax__calendar div {
            padding: 0px;
        }

        .gridP {
            background: #666;
        }

        #validdatediv input[type='text'] {
            width: 155px !important;
        }

        input[type='text'] {
            padding-left: 5px;
        }

        #MainContent_prdtDescription {
            height: 188px;
            width: 395px;
            float: left;
            border: 1px solid #ccc;
            margin: 0 0 3px 3px;
            text-align: left;
            overflow-y: scroll;
        }

        #MainContent_dtlCtPermitted tr td {
            vertical-align: top;
        }

        #divCmsEligforSaver {
            width: 98%;
            cursor: pointer;
            margin: 1%;
            float: left;
            background: #FBDEE6;
            padding-left: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            margin: 5px 0 0 0;
            width: 100%;
            width: 975px;
            font-weight: bold;
            border: 1px solid #b53859;
        }

        #divCmsGreatPass {
            width: 98%;
            cursor: pointer;
            margin: 1%;
            float: left;
            background: #FBDEE6;
            padding-left: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            margin: 5px 0 0 0;
            width: 100%;
            width: 975px;
            font-weight: bold;
            border: 1px solid #b53859;
        }

        #divCmsAncText {
            width: 98%;
            cursor: pointer;
            margin: 1%;
            float: left;
            background: #FBDEE6;
            padding-left: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            margin: 5px 0 0 0;
            width: 100%;
            width: 975px;
            font-weight: bold;
            border: 1px solid #b53859;
        }

        .hide {
            display: none;
        }
        /*new design css*/
        .header-background {
            background: #00aeef;
            padding-top: 5px;
            padding-bottom: 5px;
            margin: 0px;
            width: 100%;
        }

        .publish-img {
            width: 18px;
            height: 18px;
            position: absolute;
            margin: 5px 0 0 -36px;
        }
    </style>
    <script type="text/javascript">
        function DefltMiss(obj) {
            $("INPUT[type='radio']").each(function () {

                if (this.id != obj.children[0].id) {
                    this.checked = false;
                }
                else {
                    this.checked = true;
                }
            });
            return false;
        }
        function chkmaxleng(Len, obj) {
            if ($(obj).val().length > Len)
                $(obj).val($(obj).val().substring(0, Len));
        }
    </script>
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtFromDate").val();
            var ldate = $("#txtToDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select To Date, date bigger than From Date!');
                    return false;
                }
            }
        }
        function checkDatevisibility(sender) {
            var sdate = $("#txtVisibleFromDate").val();
            var ldate = $("#txtVisibleToDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Visible To Date, date bigger than Visible From Date!');
                    return false;
                }
            }
        }
        $(function () {
            $("#txtFromDate, #txtToDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
        function pageLoad(sender, args) {
            $('#MainContent_txtProductDesc').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtEligibility').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtEligibilityforSaver').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtDiscount').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtValidity').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtTerm').redactor({ iframe: true, minHeight: 200 });
            $('#txtProductDecription').redactor({ iframe: true, minHeight: 100 });
            $('#MainContent_txtGreatPass').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtareaAnnouncement').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtDesciption').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".grid-sec2").find(".cat-outer-cms").hide();

            $("#divCmsAncText").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsAncText").next().show();
            });

            $("#divCmsProd").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsProd").next().show();
            });

            $("#divCmsElig").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsElig").next().show();
            });

            $("#divCmsEligforSaver").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsEligforSaver").next().show();
            });

            $("#divCmsValid").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsValid").next().show();
            });

            $("#divCmsDisc").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsDisc").next().show();
            });

            $("#divCmsTerm").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsTerm").next().show();
            });

            $("#divCmsGreatPass").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsGreatPass").next().show();
            });
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            checkUncheckCountyTree();
            checkUncheckPermittedCntTree();
        });

        function checkUncheckCountyTree() {
            var checkBoxSelector = '#<%=dtlCountry.ClientID%> input[id*="chkRegion"]:checkbox';
            $(checkBoxSelector).click(function () {
                var chkId = (this.id);
                var last = chkId.slice(-1);
                var idtree = "MainContent_dtlCountry_trSites_" + last;

                if (this.checked) {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', true);
                    });
                }
                else {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });
        }

        function checkUncheckPermittedCntTree() {
            var checkBoxSelector = '#<%=dtlCtPermitted.ClientID%> input[id*="chkRegion"]:checkbox';
            $(checkBoxSelector).click(function () {
                var chkId = (this.id);
                var last = chkId.slice(-1);
                var idtree = "MainContent_dtlCtPermitted_trPSites_" + last;

                if (this.checked) {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', true);
                    });
                }
                else {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>Product</h2>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="ProductList.aspx" class="">List</a></li>
                    <li><a id="aNew" href="Product.aspx" class="current">New/Edit</a> </li>
                </ul>
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" />
                        </div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divNew" runat="server" style="display: block;">
                            <asp:HiddenField runat="server" ID="hdnProdNmId" />
                            <asp:HiddenField runat="server" ID="hdnProdId" />
                            <table class="tblMainSection" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: right;" class="valdreq">Fields marked with * are mandatory
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><b>Supplier</b></legend>
                                            <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                                <div class="cat-inner-alt">
                                                    <table style="width: 870px">
                                                        <tr>
                                                            <td style="width: 179px;">Supplier Name:
                                                            </td>
                                                            <td style="width: 270px;">
                                                                <asp:DropDownList ID="ddlSupName" runat="server" class="select" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddlSupName_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 169px;">Supplier Category:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlSupCat" runat="server" class="select">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Category </b></legend>
                                            <div class="cat-inner-alt" style="text-align: center; width: 100%; border-bottom: 2px dashed #FBDEE6;">
                                                <asp:DropDownList runat="server" ID="ddlCategory" AutoPostBack="True" Width="500px"
                                                    OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                                                <asp:RequiredFieldValidator ID="reqCategory" runat="server" ErrorMessage="*" ValidationGroup="submit"
                                                    InitialValue="0" ControlToValidate="ddlCategory" CssClass="valdreq" SetFocusOnError="True" />
                                            </div>
                                            <div class="cat-outer-ProdCom" style="min-height: 0px; max-height: 150px; margin: 0px; width: 100%; overflow-y: auto;">
                                                <asp:DataList ID="rptSite" runat="server" RepeatDirection="Vertical" RepeatColumns="3"
                                                    Width="100%">
                                                    <ItemStyle CssClass="cat-inner-dtl"></ItemStyle>
                                                    <AlternatingItemStyle CssClass="cat-inner-dtl"></AlternatingItemStyle>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <%#Eval("DisplayName")%>
                                                        <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%#Eval("ID")%>' />
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Product available for travel between</b></legend>
                                            <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                                <div class="cat-inner-alt">
                                                    <table style="width: 870px">
                                                        <tr>
                                                            <td>From Date(DD/MM/YYYY):<span style="color: red">* </span>
                                                            </td>
                                                            <td style="width: 270px;">
                                                                <asp:TextBox ID="txtFromDate" runat="server" class="input" ClientIDMode="Static"
                                                                    MaxLength="10" />
                                                                &nbsp;
                                                                <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />
                                                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                                                    ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtFromDate" />
                                                            </td>
                                                            <td>To Date(DD/MM/YYYY):<span style="color: red">* </span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtToDate" runat="server" class="input" ClientIDMode="Static" MaxLength="10" />
                                                                &nbsp;
                                                                <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />
                                                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                                                    ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtToDate" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Product available for purchase between</b></legend>
                                            <div id="validdatediv" class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                                <div class="cat-inner-alt">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 230px;">From Date(DD/MM/YYYY)-(GMT): <span style="color: red !important;">* </span>
                                                            </td>
                                                            <td style="width: 200px;">
                                                                <asp:TextBox ID="txtVisibleFromDate" runat="server" class="input" ClientIDMode="Static"
                                                                    MaxLength="10" />
                                                                &nbsp;
                                                                <asp:Image ID="ImageVisible1" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                                    Display="Dynamic" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtVisibleFromDate" />
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkVisibleFromDate" runat="server" Checked="true" /></td>
                                                            <td style="width: 230px;">To Date(DD/MM/YYYY)-(GMT): <span style="color: red !important;">* </span>
                                                            </td>
                                                            <td style="width: 200px;">
                                                                <asp:TextBox ID="txtVisibleToDate" runat="server" class="input" ClientIDMode="Static"
                                                                    MaxLength="10" />
                                                                &nbsp;
                                                                <asp:Image ID="ImageVisible2" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                                    Display="Dynamic" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtVisibleToDate" />
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkVisibleToDate" runat="server" Checked="true" /></td>
                                                        </tr>
                                                    </table>
                                                    <div id="tableDynamicPurchageDate">
                                                        <asp:Repeater ID="rptDynamicPurchageDate" runat="server">
                                                            <ItemTemplate>
                                                                <table style="width: 100%;">
                                                                    <tr>
                                                                        <td style="width: 230px;">
                                                                            <asp:Label ID="lblSiteNameFrom" runat="server" Text='<%#Eval("SiteName") +(string.IsNullOrEmpty(Eval("GmtTimeZone")+"")?"":" ("+Eval("GmtTimeZone")+")")%>' Style="color: black!important;"></asp:Label>
                                                                            <span style="color: red !important;">* </span>
                                                                            <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%#Eval("SiteId") %>' />
                                                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("Id") %>' />
                                                                        </td>
                                                                        <td style="width: 200px;" class="date-from-txt">
                                                                            <asp:TextBox ID="txtVisibleDateFrom" runat="server" Text='<%#(Eval("ProductEnableFromDate")!=null?String.Format("{0:dd/MM/yyyy HH:mm}",Eval("ProductEnableFromDate")):"") %>' class="input" MaxLength="10" CssClass="image-visible-from" />
                                                                            &nbsp;
                                                                            <asp:Image ID="ImageVisibleFrom" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDateFrom" runat="server" ErrorMessage="*"
                                                                                Display="Dynamic" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtVisibleDateFrom" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkVisibleDateFrom" runat="server" Checked='<%#Eval("IsProductEnableFromDate") %>' CssClass="chk-from" />
                                                                        </td>
                                                                        <td style="width: 230px;">
                                                                            <asp:Label ID="lblSiteNameTo" runat="server" Text='<%#Eval("SiteName") +(string.IsNullOrEmpty(Eval("GmtTimeZone")+"")?"":" ("+Eval("GmtTimeZone")+")")%>' Style="color: black!important;"></asp:Label>
                                                                            <span style="color: red !important;">* </span>
                                                                        </td>
                                                                        <td class="date-from-txt" style="width: 200px;">
                                                                            <asp:TextBox ID="txtVisibleDateTo" runat="server" Text='<%#(Eval("ProductEnableToDate")!=null?String.Format("{0:dd/MM/yyyy HH:mm}",Eval("ProductEnableToDate") ):"") %>' class="input" CssClass="image-visible-to"
                                                                                MaxLength="10" />
                                                                            &nbsp;
                                                                            <asp:Image ID="ImageVisibleTo" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDateTo" runat="server" ErrorMessage="*"
                                                                                Display="Dynamic" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtVisibleDateTo" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkVisibleDateTo" runat="server" Checked='<%#Eval("IsProductEnableToDate") %>' CssClass="chk-to" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2" id="isBritrailPromo" runat="server" visible="false">
                                            <legend><b>Britrail free day promo available for printing between</b></legend>
                                            <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                                <div class="cat-inner-alt">
                                                    <table style="width: 870px">
                                                        <tr>
                                                            <td style="width: 179px;">From Date(DD/MM/YYYY):<span style="color: red">* </span>
                                                            </td>
                                                            <td style="width: 270px;">
                                                                <asp:TextBox ID="txtBritrailFromDate" runat="server" class="input" ClientIDMode="Static"
                                                                    MaxLength="10" />
                                                                &nbsp;
                                                                <asp:Image ID="imgBritrailFrom" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                    Display="Dynamic" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtBritrailFromDate" />
                                                            </td>
                                                            <td style="width: 169px;">To Date(DD/MM/YYYY):<span style="color: red">* </span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtBritrailToDate" runat="server" class="input" ClientIDMode="Static"
                                                                    MaxLength="10" />
                                                                &nbsp;
                                                                <asp:Image ID="imgBritrailTo" runat="server" ImageUrl="~/images/icon-calender.png"
                                                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: 6px 5px 0 0; height: 19px;" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                                    Display="Dynamic" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txtBritrailToDate" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Product Name</b></legend>
                                            <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0px;">
                                                <div class="cat-inner-alt" style="padding-left: 5px;">
                                                    <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <img src="../images/publish.png" class="publish-img" alt="Printed Name" id="imgProductName" runat="server" visible="false" />Printed Name:<span style="color: red">* </span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtProductName" MaxLength="180" />
                                                                <asp:RequiredFieldValidator ID="reqPass" runat="server" ErrorMessage="*" ValidationGroup="submit"
                                                                    ControlToValidate="txtProductName" CssClass="valdreq" SetFocusOnError="True" />
                                                            </td>
                                                            <td>
                                                                <img src="../images/publish.png" class="publish-img" alt="Language" id="imgLanguage" runat="server" visible="false" />Language:
                                                            </td>
                                                            <td class="style1">
                                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" Enabled="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img src="../images/publish.png" class="publish-img" alt="Currency" id="imgCurrency" runat="server" visible="false" />Currency: <span style="color: red">* </span>
                                                            </td>
                                                            <td class="style1">
                                                                <asp:DropDownList runat="server" ID="ddlCurrency" Width="209px" />
                                                                <asp:RequiredFieldValidator ID="reqCurrency" runat="server" ErrorMessage="*" ValidationGroup="submit"
                                                                    InitialValue="-1" ControlToValidate="ddlCurrency" CssClass="valdreq" SetFocusOnError="True" />
                                                            </td>
                                                            <td>
                                                                <img src="../images/publish.png" class="publish-img" alt="Months Valid" id="imgMonthValid" runat="server" visible="false" />Months Valid
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlMonthValid" runat="server" Width="209px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                        <tr style="padding: 2px;">
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Default When Missing" id="imgMissing" runat="server" visible="false" />
                                                                <asp:RadioButton ID="rdoMissing" runat="server" onchange="DefltMiss(this)" />
                                                                Default When Missing
                                                            </td>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Is Active" id="imgIsActv" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkIsActv" runat="server" />
                                                                Is Active ?
                                                            </td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Is Twin Pass" id="imgTwinPass" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkTwinPass" runat="server" />
                                                                Is Twin Pass ?
                                                            </td>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Passport Valid" id="imgPassport" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkpassport" runat="server" />
                                                                Passport Valid ?
                                                            </td>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Shipping applicable" id="imgShipping" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkShipping" runat="server" Checked='<%#Eval("IsShippingAplicable")%>' />
                                                                Shipping applicable ?
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Allow Refund" id="imgAllowRefund" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkAllowRefund" runat="server" />
                                                                Allow Refund ?
                                                            </td>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Allow Middle Name" id="imgAllowMiddleName" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkAllowMiddleName" runat="server" />
                                                                Allow Middle Name ?
                                                            </td>
                                                            <td colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Allow product to be visible Admin site only" id="imgAllowAdminPass" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkAllowAdminPass" runat="server" />
                                                                Allow product to be visible Admin site only ?
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6">
                                                                <img src="../images/publish.png" class="publish-img" alt="Allow product to be visible Admin site only" id="imgIsPromoPass" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkIsPromoPass" runat="server" />
                                                                Use the Promo template when printing (Contact IT for details about specific text/layout to be used)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;" colspan="2">
                                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer" id="imgSpecialOffer" runat="server" visible="false" />
                                                                <asp:RadioButtonList ID="rdnSpecialOffer" runat="server" RepeatDirection="Horizontal"
                                                                    Style="margin-left: -8px;">
                                                                    <asp:ListItem Value="0">Special Offer</asp:ListItem>
                                                                    <asp:ListItem Value="1">Best Seller</asp:ListItem>
                                                                    <asp:ListItem Value="2">Normal</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td>
                                                                <img src="../images/publish.png" class="publish-img" alt="Nationality Valid" id="imgNationality" runat="server" visible="false" />
                                                                <asp:CheckBox ID="chkNationality" runat="server" />Nationality Valid ?
                                                            </td>
                                                            <td style="padding-left: 50px;">
                                                                <img src="../images/publish.png" class="publish-img" alt="Sort Order" id="imgShortOrder" runat="server" visible="false" />Sort Order
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:DropDownList ID="ddlShortOrder" runat="server" />
                                                            </td>
                                                            <td style="text-align: left;">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6" style="text-align: left;">
                                                                <span style="width: 12%; float: left; color: Black !important;">
                                                                    <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgSpecialOfferText" runat="server" visible="false" />Special Offer Text
                                                                </span><span style="width: 83%; float: left">
                                                                    <asp:TextBox ID="txtSpecialOfferText" runat="server" MaxLength="500" Text='<%#Eval("Name")%>'
                                                                        TextMode="MultiLine" Width="100%" />
                                                                    <span></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6" style="text-align: left;">
                                                                <span style="width: 12%; float: left; color: Black !important;">
                                                                    <img src="../images/publish.png" class="publish-img" alt="Start Day Info" id="imgStartDayText" runat="server" visible="false" />Start Day Info.
                                                                </span><span style="width: 83%; float: left">
                                                                    <asp:TextBox ID="TxtStartDayText" onkeyup="chkmaxleng(3900,this)" runat="server"
                                                                        TextMode="MultiLine" Width="100%" />
                                                                    <span></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6" style="text-align: left;">
                                                                <span style="width: 12%; float: left; color: Black !important;">
                                                                    <img src="../images/publish.png" class="publish-img" alt="Is Electronic pass" id="imgIsElectronic" runat="server" visible="false" />Is Electronic pass
                                                                </span><span style="width: 83%; float: left">
                                                                    <asp:CheckBox runat="server" ID="chkIsElectronic" />
                                                                    <span></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6" style="text-align: left;">
                                                                <span style="width: 12%; float: left; color: Black !important;">
                                                                    <img src="../images/publish.png" class="publish-img" alt="Electronic Note" id="imgElectronic" runat="server" visible="false" />Electronic Note
                                                                </span><span style="width: 83%; float: left">
                                                                    <asp:TextBox ID="TxtElectronic" onkeyup="chkmaxleng(3900,this)" runat="server" TextMode="MultiLine"
                                                                        Width="100%" />
                                                                    <span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <asp:Repeater ID="rptProduct" runat="server" OnItemDataBound="rptProduct_ItemDataBound">
                                                <ItemTemplate>
                                                    <div class="cat-inner">
                                                        <table style="width: 873px">
                                                            <tr>
                                                                <td>Language:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" />
                                                                    <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                                    <asp:HiddenField ID="hdnProdNmId" runat="server" Value='<%#Eval("Id")%>' />
                                                                </td>
                                                                <td>&nbsp; &nbsp; Name:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox runat="server" ID="txtProductName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                                                </td>
                                                                <td>Default When Missing:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                        onchange="DefltMiss(this)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>
                                                <AlternatingItemTemplate>
                                                    <div class="cat-inner-alt">
                                                        <table style="width: 873px">
                                                            <tr>
                                                                <td>Language:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" />
                                                                    <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                                    <asp:HiddenField ID="hdnProdNmId" runat="server" Value='<%#Eval("Id")%>' />
                                                                </td>
                                                                <td>&nbsp; &nbsp; Name:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox runat="server" ID="txtProductName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                                                </td>
                                                                <td>Default When Missing:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                        onchange="DefltMiss(this)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </AlternatingItemTemplate>
                                            </asp:Repeater>
                                            <div style="float: right;">
                                                <asp:Button ID="btnAddMore" runat="server" CssClass="button" Text="+ Add More" Width="89px"
                                                    Visible="False" OnClick="btnAddMore_Click" />
                                                <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="-- Remove" Width="89px"
                                                    Visible="False" OnClick="btnRemove_Click" />
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Upload Image </b></legend>
                                            <table>
                                                <tr>
                                                    <td width="15%">Country Image:
                                                    </td>
                                                    <td width="25%">
                                                        <asp:FileUpload ID="fupCountryImg" runat="server" />
                                                    </td>
                                                    <td width="15%">Banner Image :
                                                    </td>
                                                    <td width="25%">
                                                        <asp:FileUpload ID="fupBannerImg" runat="server" />
                                                    </td>
                                                    <td width="20%">
                                                        <asp:HiddenField ID="hdnCountryImg" runat="server" />
                                                        <asp:HiddenField ID="hdnBannerImg" runat="server" />
                                                        <a href="#" id="lnkCountryImg" class="clsLink" runat="server">View Image </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pnlMap" runat="server" CssClass="popup">
                                                <div class="dvPopupHead" style="width: 900px;">
                                                    <div class="dvPopupTxt">
                                                        Product Banner & Country Image
                                                    </div>
                                                    <a href="#" id="btnClose" runat="server" style="color: #fff; padding-right: 5px;">X
                                                    </a>
                                                </div>
                                                <div class="dvImg">
                                                    <img id="imgBanner" runat="server" alt="" border="0" style="height: 190px; width: 700px;">
                                                    <img id="imgMap" runat="server" alt="" border="0" style="height: 190px; width: 190px;">
                                                </div>
                                            </asp:Panel>
                                            <asp:ModalPopupExtender ID="mdpupStock" runat="server" TargetControlID="lnkCountryImg"
                                                CancelControlID="btnClose" PopupControlID="pnlMap" BackgroundCssClass="modalBackground" />
                                            <table>
                                                <tr valign="top">
                                                    <td width="15%">Product Image:
                                                    </td>
                                                    <td width="25%">
                                                        <asp:FileUpload ID="FupldPdtImg" runat="server" />
                                                    </td>
                                                    <td width="15%">Product Image 2:
                                                    </td>
                                                    <td width="25%">
                                                        <asp:FileUpload ID="FupldPdtImg2" runat="server" />
                                                    </td>
                                                    <td width="20%">
                                                        <asp:HiddenField ID="hdnprdtImage" runat="server" />
                                                        <asp:HiddenField ID="hdnprdtImage2" runat="server" />
                                                        <a href="#" id="LnkPrdtImg" class="clsLink" runat="server">View Product Image </a>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td width="15%">Product Alt Tag:<span runat="server" id="SpanPrdtAltTag1" style="color: red">* </span>
                                                    </td>
                                                    <td width="25%">
                                                        <asp:TextBox ID="txtPrdtAltTag1" runat="server" MaxLength="300"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredPrdtAltTag1" runat="server" ErrorMessage="*"
                                                            ValidationGroup="submit" ControlToValidate="txtPrdtAltTag1" CssClass="valdreq"
                                                            SetFocusOnError="True" />
                                                    </td>
                                                    <td width="15%">Product Alt Tag 2:<span runat="server" id="SpanPrdtAltTag2" style="color: red">*
                                                    </span>
                                                    </td>
                                                    <td width="25%">
                                                        <asp:TextBox ID="txtPrdtAltTag2" runat="server" MaxLength="300"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredPrdtAltTag2" runat="server" ErrorMessage="*"
                                                            ValidationGroup="submit" ControlToValidate="txtPrdtAltTag2" CssClass="valdreq"
                                                            SetFocusOnError="True" />
                                                    </td>
                                                    <td width="20%"></td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pnlprdtImg" runat="server" CssClass="popup" Style="left: 358px; overflow: hidden;">
                                                <div class="dvPopupHead" style="width: 400px;">
                                                    <div class="dvPopupTxt">
                                                        Product Image
                                                    </div>
                                                    <a href="#" id="btlcloseprdtImg" runat="server" style="color: #fff; padding-right: 5px;">X </a>
                                                </div>
                                                <div class="dvImg">
                                                    <img id="prdtImg" runat="server" alt="" border="0" style="height: 190px; width: 200px; float: left;">
                                                    <img id="prdtImg2" runat="server" alt="" border="0" style="height: 190px; width: 200px; float: left;">
                                                </div>
                                            </asp:Panel>
                                            <asp:ModalPopupExtender ID="mdlpopPrdtImg" runat="server" TargetControlID="LnkPrdtImg"
                                                CancelControlID="btlcloseprdtImg" PopupControlID="pnlprdtImg" BackgroundCssClass="modalBackground" />
                                            <table>
                                                <tr valign="top">
                                                    <td width="15%">Homepage Product Image:
                                                    </td>
                                                    <td width="25%">
                                                        <asp:FileUpload ID="FupldHomeprdtimg" runat="server" />
                                                    </td>
                                                    <td width="20%">
                                                        <asp:HiddenField ID="hdnHomepageprodtimage" runat="server" />
                                                        <a href="#" id="LnkHomePrdtImg" class="clsLink" runat="server">View Homepage Product
                                                            Image </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pnlhomeprdtImg" runat="server" CssClass="popup" Style="left: 358px;">
                                                <div class="dvPopupHead" style="width: 220px;">
                                                    <div class="dvPopupTxt">
                                                        Homepage Product Image
                                                    </div>
                                                    <a href="#" id="btlclosehomeprdtImg" runat="server" style="color: #fff; padding-right: 5px;">X </a>
                                                </div>
                                                <div class="dvImg">
                                                    <img id="HomepageprdtImg" runat="server" alt="" border="0" style="height: 220px; width: 220px; float: left;">
                                                </div>
                                            </asp:Panel>
                                            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="LnkHomePrdtImg"
                                                CancelControlID="btlclosehomeprdtImg" PopupControlID="pnlhomeprdtImg" BackgroundCssClass="modalBackground" />
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Product Content (Only for IR Rail)</b></legend>
                                            <table width="983px">
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgTitle" runat="server" visible="false" />Title
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTitle" runat="server" Width="100%" MaxLength="2000"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgDesciption" runat="server" visible="false" />Desciption
                                                    </td>
                                                    <td>
                                                        <textarea id="txtDesciption" runat="server" style="width: 100%;"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Country</b></legend>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td style="width: 30%; text-align: right">Country Combination <span style="color: red">* </span>:
                                                    </td>
                                                    <td style="width: 70%">
                                                        <asp:DropDownList runat="server" ID="ddlCountryRange" Width="300" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlCountryRange_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">--Select Country Pass--</asp:ListItem>
                                                            <asp:ListItem Value="9999">Global</asp:ListItem>
                                                            <asp:ListItem Value="1001-1999">One Country Pass</asp:ListItem>
                                                            <asp:ListItem Value="2001-2999"> Regional Pass</asp:ListItem>
                                                            <asp:ListItem Value="3001-3999"> Select Pass 3 Country</asp:ListItem>
                                                            <asp:ListItem Value="4001-4999"> Select Pass 4 Country </asp:ListItem>
                                                            <asp:ListItem Value="5001-5999"> Select Pass 5 Country</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="reqCountryRange" runat="server" ErrorMessage="*"
                                                            ValidationGroup="save" InitialValue="0" ControlToValidate="ddlCountryRange" CssClass="valdreq"
                                                            SetFocusOnError="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right; width: 30%; line-height: 35px;">
                                                        <asp:Literal runat="server" ID="ltrRegContryName" Text="" Visible="False" />
                                                    </td>
                                                    <td style="width: 70%">
                                                        <asp:DropDownList ID="ddlRegionalCountryPass" runat="server" Width="300px" Visible="False" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div style="min-height: 20px; max-height: 300px; overflow-y: auto; width: 100%;">
                                                            <asp:DataList ID="dtlCountry" runat="server" Visible="False" RepeatColumns="2" CssClass="treeborder"
                                                                OnItemDataBound="dtlCountry_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRegion" runat="server" Font-Bold="True" Text='<%#Eval("RegionName") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnRegion" Value='<%#Eval("RegionID") %>' />
                                                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                                                    </asp:TreeView>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Countries Permitted</b></legend>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <div style="min-height: 20px; max-height: 300px; overflow-y: auto; width: 100%; vertical-align: top">
                                                            <asp:DataList ID="dtlCtPermitted" runat="server" RepeatColumns="2" Width="99%" OnItemDataBound="dtlCtPermitted_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRegion" runat="server" Font-Bold="True" Text='<%#Eval("RegionName") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnRegion" Value='<%#Eval("RegionID") %>' />
                                                                    <asp:TreeView ID="trPSites" runat="server" ShowCheckBoxes="All">
                                                                    </asp:TreeView>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Pricing</b></legend>
                                            <div class="cat-outer header-background">
                                                <table style="width: 100%; font-weight: bold;">
                                                    <tr>
                                                        <td style="width: 13%; text-align: center;">Pass Type Code
                                                        </td>
                                                        <td style="width: 20%; text-align: center;">Traveller Validity
                                                        </td>
                                                        <td style="width: 17%; text-align: center;">Validity Note
                                                        </td>
                                                        <td style="width: 15%; text-align: center;">Class
                                                        </td>
                                                        <td style="width: 15%; text-align: center;">Traveller
                                                        </td>
                                                        <td style="width: 10%; text-align: center;">Price
                                                        </td>
                                                        <td style="width: 10%; text-align: center;" <%=showpricebind %>>Price Band
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="cat-outer" style="margin: 0px; width: 100%;">
                                                <asp:Repeater ID="rptProductPrice" runat="server" OnItemCommand="rptProductPrice_ItemCommand"
                                                    OnItemDataBound="rptProductPrice_ItemDataBound">
                                                    <ItemTemplate>
                                                        <div class="cat-inner" style="font-size: 13px;">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 13%; text-align: left">
                                                                        <img class="edit" title="Edit Price" id="imgEditPrice" src="../images/edit.png" style="height: 12px;" onclick='EditPrice("<%#Eval("ID")%>",this)' />
                                                                        <img class='<%#Eval("IsActive").ToString()=="True" ?"active-flag":"inactive-flag" %>'
                                                                            title="<%#Eval("IsActive").ToString()=="True" ?"Active":"Inactive" %>" id="imgActivePrice"
                                                                            src="<%#Eval("IsActive").ToString()=="True" ?"../images/active.png":"../images/inactive.png" %>"
                                                                            style="height: 12px;" onclick='ActiveInactivePrice("<%#Eval("ID")%>",this)' />
                                                                        <asp:ImageButton ID="imgBtnRemove" CommandName="Remove" CommandArgument='<%#Eval("ID")%>'
                                                                            OnClientClick="return  confirm('Are you sure you want to delete this price ?')"
                                                                            Height="12" ToolTip="Remove Price" ImageUrl="../images/error.png" runat="server" />
                                                                        &nbsp;
                                                                        <asp:Label ID="lblPassTypeCode" runat="server" Text=' <%#Eval("PassTypeCode")%>'></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%; text-align: center;">
                                                                        <%#Eval("TravellerValidName")%>
                                                                    </td>
                                                                    <td style="width: 17%; text-align: center;">
                                                                        <%#Eval("ValidityNote")%>
                                                                    </td>
                                                                    <td style="width: 15%; text-align: center">
                                                                        <%#Eval("ClassName")%>
                                                                    </td>
                                                                    <td style="width: 15%; text-align: right">
                                                                        <asp:Label ID="lblTravellerName" runat="server" Text='<%#Eval("TravellerName")+" "+ (Eval("Note") == null ? "" : "( " + Eval("Note") + " )")%>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlTravellerName" runat="server" Style="width: 150px;" CssClass="hide">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="lblTravellerId" runat="server" Text=' <%#Eval("TravellerId")%>' CssClass="hide"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 10%; text-align: right">
                                                                        <asp:Label runat="server" ID="lblPriceShow" Text='<%#Eval("Price")%>' />
                                                                        <asp:TextBox runat="server" Width="60" CssClass="hide" ID="txtpriceEdit" Text='<%#Eval("Price")%>' />
                                                                    </td>
                                                                    <td style="width: 10%; text-align: right" <%=showpricebind %>>
                                                                        <asp:HiddenField runat="server" ID="hdnpriceband" Value='<%#Eval("PriceBand")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%#Eval("ID")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnClassID" Value='<%#Eval("ClassID")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnValidityID" Value='<%#Eval("TravellerValidId")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnUniqueProductID" Value='<%#Eval("UniqueProductID")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnIsActive" Value='<%#Eval("IsActive")%>' />
                                                                        <asp:Label runat="server" ID="lblpriceband" Text='<%#Eval("PriceBandName")%>' />
                                                                        <asp:DropDownList runat="server" Width="90" CssClass="hide" ID="ddlpriceband" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <div class="cat-inner-alt" style="font-size: 13px; width: 99.5%;">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 13%; text-align: left">
                                                                        <img class="edit" id="imgEditPrice" title="Edit Price" src="../images/edit.png" style="height: 12px;"
                                                                            onclick='EditPrice("<%#Eval("ID")%>",this)' />
                                                                        <img class='<%#Eval("IsActive").ToString()=="True" ?"active-flag":"inactive-flag" %>'
                                                                            title="<%#Eval("IsActive").ToString()=="True" ?"Active":"Inactive" %>" id="imgActivePrice"
                                                                            src="<%#Eval("IsActive").ToString()=="True" ?"../images/active.png":"../images/inactive.png" %>"
                                                                            style="height: 12px;" onclick='ActiveInactivePrice("<%#Eval("ID")%>",this)' />
                                                                        <asp:ImageButton ID="imgBtnRemove" CommandName="Remove" CommandArgument='<%#Eval("ID")%>'
                                                                            OnClientClick="return  confirm('Are you sure you want to delete this price ?')"
                                                                            Height="12" ToolTip="Remove Price" ImageUrl="../images/error.png" runat="server" />
                                                                        &nbsp;
                                                                        <asp:Label ID="lblPassTypeCode" runat="server" Text=' <%#Eval("PassTypeCode")%>'></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%; text-align: center;">
                                                                        <%#Eval("TravellerValidName")%>
                                                                    </td>
                                                                    <td style="width: 17%; text-align: center;">
                                                                        <%#Eval("ValidityNote")%>
                                                                    </td>
                                                                    <td style="width: 15%; text-align: center">
                                                                        <%#Eval("ClassName")%>
                                                                    </td>
                                                                    <td style="width: 15%; text-align: right">
                                                                        <asp:Label ID="lblTravellerName" runat="server" Text='<%#Eval("TravellerName") + " " + (Eval("Note") == null ? "" : "( " + Eval("Note") + " )")%>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlTravellerName" runat="server" Style="width: 150px;" CssClass="hide">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="lblTravellerId" runat="server" Text=' <%#Eval("TravellerId")%>' CssClass="hide"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 10%; text-align: right">
                                                                        <asp:Label runat="server" ID="lblPriceShow" Text='<%#Eval("Price")%>' />
                                                                        <asp:TextBox runat="server" Width="60" CssClass="hide" ID="txtpriceEdit" Text='<%#Eval("Price")%>' />
                                                                    </td>
                                                                    <td style="width: 10%; text-align: right" <%=showpricebind %>>
                                                                        <asp:HiddenField runat="server" ID="hdnpriceband" Value='<%#Eval("PriceBand")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%#Eval("ID")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnClassID" Value='<%#Eval("ClassID")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnValidityID" Value='<%#Eval("TravellerValidId")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnUniqueProductID" Value='<%#Eval("UniqueProductID")%>' />
                                                                        <asp:HiddenField runat="server" ID="hdnIsActive" Value='<%#Eval("IsActive")%>' />
                                                                        <asp:Label runat="server" ID="lblpriceband" Text='<%#Eval("PriceBandName")%>' />
                                                                        <asp:DropDownList runat="server" Width="90" CssClass="hide" ID="ddlpriceband" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </AlternatingItemTemplate>
                                                </asp:Repeater>
                                                <br />
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 10%">
                                                            <asp:TextBox ID="txtPassTypeCode" runat="server" Width="80px" MaxLength="5" />
                                                            <asp:TextBoxWatermarkExtender ID="wmextPassTypeCode" WatermarkCssClass="waterMask"
                                                                TargetControlID="txtPassTypeCode" WatermarkText="Pass Type" runat="server" />
                                                            <asp:RequiredFieldValidator ID="reqPassTypeCode" ValidationGroup="save" ControlToValidate="txtPassTypeCode"
                                                                CssClass="valdreq" runat="server" ErrorMessage="*" />
                                                            <asp:FilteredTextBoxExtender ID="ftbPassTypeCode" TargetControlID="txtPassTypeCode"
                                                                ValidChars="0123456789" runat="server" />
                                                        </td>
                                                        <td style="width: 30%; text-align: left;">
                                                            <asp:DropDownList runat="server" ID="ddlProductValidUpTo" Width="360px" />
                                                            <asp:RequiredFieldValidator ID="reqProdValidTo" ValidationGroup="save" InitialValue="0"
                                                                ControlToValidate="ddlProductValidUpTo" CssClass="valdreq" runat="server" ErrorMessage="*" />
                                                        </td>
                                                        <td style="width: 15%; text-align: center;">
                                                            <asp:DropDownList runat="server" ID="ddlClass" Width="150px" />
                                                            <asp:RequiredFieldValidator ID="reqClass" ValidationGroup="save" InitialValue="0"
                                                                ControlToValidate="ddlClass" CssClass="valdreq" runat="server" ErrorMessage="*" />
                                                        </td>
                                                        <td style="width: 15%; text-align: center;">
                                                            <asp:DropDownList runat="server" ID="ddlTraveler" Width="133px" />
                                                            <asp:RequiredFieldValidator ID="reqTraveler" ValidationGroup="save" InitialValue="0"
                                                                ControlToValidate="ddlTraveler" CssClass="valdreq" runat="server" ErrorMessage="*" />
                                                        </td>
                                                        <td style="width: 10%; text-align: center;">
                                                            <asp:TextBox ID="txtPrice" runat="server" Width="80px" MaxLength="10" />
                                                            <asp:TextBoxWatermarkExtender ID="wmextPrice" TargetControlID="txtPrice" WatermarkCssClass="waterMask"
                                                                WatermarkText="Price" runat="server" />
                                                            <asp:RequiredFieldValidator ID="reqtPrice" ValidationGroup="save" ControlToValidate="txtPrice"
                                                                CssClass="valdreq" runat="server" ErrorMessage="*" />
                                                            <asp:FilteredTextBoxExtender ID="ftbPrice" TargetControlID="txtPrice" ValidChars=".0123456789"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 10%"></td>
                                                        <td colspan="4" style="width: 60%; text-align: right;">
                                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="89px"
                                                                ValidationGroup="save" OnClick="btnSave_Click" />
                                                            <asp:Button ID="btnSaveNew" runat="server" CssClass="button" Text="Save & Add New"
                                                                Width="150px" ValidationGroup="save" OnClick="btnSaveNew_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Cross Sale Adsense</b></legend>
                                            <div class="cat-inner-alt">
                                                <asp:DropDownList runat="server" ID="ddlCrossSaleAdsense" Width="88%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqcross" ValidationGroup="cross" ControlToValidate="ddlCrossSaleAdsense"
                                                    CssClass="valdreq" runat="server" InitialValue="0" ErrorMessage="*" />
                                                <asp:Button ID="btnSaveCrossSale" runat="server" CssClass="button" Text="Add" Width="89px"
                                                    ValidationGroup="cross" OnClick="btnSaveCrossSale_Click" />
                                            </div>
                                            <asp:Repeater runat="server" ID="rptCrossSaleAdsense">
                                                <ItemTemplate>
                                                    <div class="cat-inner">
                                                        <div style="width: 70%; float: left;">
                                                            <asp:HiddenField runat="server" ID="lblId" Value='<%#Eval("Id")%>'></asp:HiddenField>
                                                            <%#Eval("Name") %>
                                                        </div>
                                                        <div style="width: 10%; float: left;">
                                                            <%#Eval("Class") %>
                                                        </div>
                                                        <div style="width: 10%; float: left;">
                                                            <%#Eval("Price") %>
                                                        </div>
                                                        <div style="width: 9%; float: left; text-align: right;">
                                                            <a id="imgBtnRemove1" onclick="RemoveCrossSale('<%#Eval("Id")%>')" style="cursor: pointer;">
                                                                <img src="../images/error.png" style="height: 12px;" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <AlternatingItemTemplate>
                                                    <div class="cat-inner-alt">
                                                        <div style="width: 70%; float: left;">
                                                            <asp:HiddenField runat="server" ID="lblId" Value='<%#Eval("Id")%>'></asp:HiddenField>
                                                            <%#Eval("Name") %>
                                                        </div>
                                                        <div style="width: 10%; float: left;">
                                                            <%#Eval("Class") %>
                                                        </div>
                                                        <div style="width: 10%; float: left;">
                                                            <%#Eval("Price") %>
                                                        </div>
                                                        <div style="width: 9%; float: left; text-align: right;">
                                                            <a id="imgBtnRemove1" onclick="RemoveCrossSale('<%#Eval("Id")%>')" style="cursor: pointer;">
                                                                <img src="../images/error.png" style="height: 12px;" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </AlternatingItemTemplate>
                                            </asp:Repeater>
                                        </fieldset>
                                        <fieldset class="grid-sec2">
                                            <legend><b>Contents</b></legend>
                                            <div id="divCmsAncText">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgAnnouncement" runat="server" visible="false" />
                                                Announcement text
                                            </div>
                                            <div class="cat-outer-cms">
                                                <asp:TextBox runat="server" ID="txtAnnouncement" Width="99%"></asp:TextBox>
                                                <textarea id="txtareaAnnouncement" runat="server"></textarea>
                                            </div>
                                            <div id="divCmsGreatPass">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgGreatPass" runat="server" visible="false" />
                                                This pass is great because..
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtGreatPass" runat="server"></textarea>
                                            </div>
                                            <div id="divCmsProd">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgProductDesc" runat="server" visible="false" />

                                                Product Description
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtProductDesc" runat="server"></textarea>
                                            </div>
                                            <div id="divCmsElig">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgEligibility" runat="server" visible="false" />

                                                Eligibility
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtEligibility" runat="server"></textarea>
                                            </div>

                                            <div id="divCmsEligforSaver">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgEligibilityforSaver" runat="server" visible="false" />
                                                Eligibility for Saver
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtEligibilityforSaver" runat="server"></textarea>
                                            </div>
                                            <div id="divCmsDisc">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgDiscount" runat="server" visible="false" />

                                                Discount
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtDiscount" runat="server"></textarea>
                                            </div>


                                            <div id="divCmsValid">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgValidity" runat="server" visible="false" />
                                                Validity
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtValidity" runat="server"></textarea>
                                            </div>

                                            <div id="divCmsTerm">
                                                <img src="../images/publish.png" class="publish-img" alt="Special Offer Text" id="imgTerm" runat="server" visible="false" />
                                                Terms & Condition
                                            </div>
                                            <div class="cat-outer-cms">
                                                <textarea id="txtTerm" runat="server"></textarea>
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:Button ID="btnPageSave" runat="server" CssClass="button" Text="Save" Width="89px"
                                            OnClick="btnPageSave_Click" />
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Save & Publish" Width="135px"
                                            ValidationGroup="submit" OnClick="btnSubmit_Click" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Exit" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                                <tr id="tr_publish_text1" runat="server" visible="false">
                                    <td style="text-align: center; color: #e80101; font-weight: bold;">There are saved sections on this page that have not been published</td>
                                </tr>
                                <tr id="tr_publish_text2" runat="server" visible="false">
                                    <td style="text-align: center; color: #0e8e0e; font-weight: bold;">Last published: <span id="span_publish" runat="server"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnCrossSaleRemoveId" ClientIDMode="Static" runat="server" />
            <asp:Button ID="brnRemoveCrossSaleByid" ClientIDMode="Static" Style="display: none;"
                runat="server" Width="0" OnClick="brnRemoveCrossSaleByid_Click" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
    <link href="../date-time-Picker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="../date-time-Picker/jquery.js" type="text/javascript"></script>
    <script src="../date-time-Picker/jquery.datetimepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $.noConflict();
        jQuery(document).ready(function ($) {
            padeloadcall();
        });

        function padeloadcall() {
            jQuery(document).ready(function ($) {
                $("#MainContent_chkIsElectronic").change(function () {
                    if ($(this).is(":checked")) {
                        $("#MainContent_TxtElectronic").val("Your " + ($("#MainContent_txtProductName").val().split(' ')[0]) + " Electronic passes will be sent to you via email within 24 business hours");
                    }
                    else {
                        $("#MainContent_TxtElectronic").val('');
                    }
                });
                $('#txtFromDate, #txtToDate, #txtVisibleFromDate, #txtVisibleToDate, #txtBritrailFromDate, #txtBritrailToDate').datetimepicker();

                $("#MainContent_imgCalender1").click(function () {
                    $("#txtFromDate").datetimepicker('show');
                });

                $("#MainContent_imgCalender2").click(function () {
                    $("#txtToDate").datetimepicker('show');
                });

                $("#MainContent_ImageVisible1").click(function () {
                    $("#txtVisibleFromDate").datetimepicker('show');
                });

                $("#MainContent_ImageVisible2").click(function () {
                    $("#txtVisibleToDate").datetimepicker('show');
                });

                $("#MainContent_imgBritrailFrom").click(function () {
                    $("#txtBritrailFromDate").datetimepicker('show');
                });

                $("#MainContent_imgBritrailTo").click(function () {
                    $("#txtBritrailToDate").datetimepicker('show');
                });

                $("[id*=ImageVisibleFrom]").click(function () {
                    $(this).parent().find('input').datetimepicker('show');
                });

                $("[id*=ImageVisibleTo]").click(function () {
                    $(this).parent().find('input').datetimepicker('show');
                });

                checkVisible();
                $('#MainContent_chkVisibleFromDate').click(function () {
                    $('#MainContent_chkVisibleToDate').prop('checked', $(this).is(':checked'));
                    checkVisible();
                });
                $('#MainContent_chkVisibleToDate').click(function () {
                    $('#MainContent_chkVisibleFromDate').prop('checked', $(this).is(':checked'));
                    checkVisible();
                });

                $('#tableDynamicPurchageDate').find('input[type="checkbox"]').click(function () {
                    if ($('#MainContent_chkVisibleFromDate').is(':checked') && $('#MainContent_chkVisibleToDate').is(':checked')) {
                    }
                    else {
                        $('#tableDynamicPurchageDate').find('input[type="checkbox"]').prop('checked', true);
                    }
                });
            });
        }

        function RemoveCrossSale(id) {
            var result = confirm("Are you sour? you want to delete this item?");
            if (result) {
                $("#hdnCrossSaleRemoveId").val(id);
                $("#brnRemoveCrossSaleByid").trigger('click');
            }
        }

        function checkVisible() {
            if ($('#MainContent_chkVisibleFromDate').is(':checked') || $('#MainContent_chkVisibleToDate').is(':checked')) {
                $('#tableDynamicPurchageDate').find('input[type="checkbox"]').prop('checked', false);
                $('[id*=RequiredFieldValidatorDateFrom]').each(function () {
                    ValidatorEnable($(this)[0], false);
                });
                $('[id*=RequiredFieldValidatorDateTo]').each(function () {
                    ValidatorEnable($(this)[0], false);
                });
                $('#tableDynamicPurchageDate').hide();
            }
            else {
                $('#tableDynamicPurchageDate').find('input[type="checkbox"]').prop('checked', true);
                $('[id*=RequiredFieldValidatorDateFrom]').each(function () {
                    ValidatorEnable($(this)[0], true);
                });
                $('[id*=RequiredFieldValidatorDateTo]').each(function () {
                    ValidatorEnable($(this)[0], true);
                });
                $('#tableDynamicPurchageDate').find('[id*=txtVisibleDateFrom]').each(function (key, value) {
                    if ($(this).val() == '') {
                        $(this).val($('#txtVisibleFromDate').val());
                    }
                });
                $('#tableDynamicPurchageDate').find('[id*=txtVisibleDateTo]').each(function (key, value) {
                    if ($(this).val() == '') {
                        $(this).val($('#txtVisibleToDate').val());
                    }
                });
                $('#tableDynamicPurchageDate').show();
                calldatefun();
            }
        }
    </script>
    <script type="text/javascript">
        function calldatefun() {
            jQuery(document).ready(function () {
                jQuery(".image-visible-from").each(function () {
                    var d = jQuery(this).val()//'dd/mm/yy hh:MM:ss';
                    var d1 = d.split(" ");
                    var date = (window.location.host == "localhost" ? d1[0].split("-") : d1[0].split("/"));
                    var time = d1[1].split(":");
                    var dd = date[0];
                    var mm = date[1] - 1;
                    var yy = date[2];
                    var hh = time[0];
                    var min = time[1];
                    var fromdt = new Date(yy, mm, dd, hh, min);
                    //console.log(fromdt);
                    jQuery(this).datetimepicker({
                        startDate: fromdt
                    });
                });
                jQuery(".image-visible-to").each(function () {
                    var d = jQuery(this).val()//'dd/mm/yy hh:MM:ss';
                    var d1 = d.split(" ");
                    var date = (window.location.host == "localhost" ? d1[0].split("-") : d1[0].split("/"));
                    var time = d1[1].split(":");
                    var dd = date[0];
                    var mm = date[1] - 1;
                    var yy = date[2];
                    var hh = time[0];
                    var min = time[1];
                    var fromdt = new Date(yy, mm, dd, hh, min);
                    jQuery(this).datetimepicker({
                        startDate: fromdt
                    });

                });
            });
        }

        function EditPrice(Id, obj) {
            var casetype = $(obj).attr("class");
            var selectrow = $(obj).parent().parent();
            if (casetype == 'edit') {
                $(obj).attr("title", "Update Price");
                $(obj).attr("class", "update");
                $(obj).attr("src", "../images/update.jpg");

                $(selectrow).find("[id*=lblpriceband]").addClass("hide");
                $(selectrow).find("[id*=lblTravellerName]").addClass("hide");
                $(selectrow).find("[id*=lblPriceShow]").addClass("hide");

                $(selectrow).find("[id*=ddlpriceband]").removeClass("hide");
                $(selectrow).find("[id*=ddlTravellerName]").removeClass("hide");
                $(selectrow).find("[id*=txtpriceEdit]").removeClass("hide");
            }
            else if (casetype == 'update') {
                var hostUrl = window.location.origin + window.location.pathname + '/updateprice';
                var TravelerID = $(selectrow).find("[id*=ddlTravellerName] :selected").val();
                var Price = parseFloat($(selectrow).find("[id*=txtpriceEdit]").val()).toFixed("2");
                var PriceBand = $(selectrow).find("[id*=ddlpriceband] :selected").val();
                //                console.log(TravelerID + "; " + Price + "; " + PriceBand + "; " + Price);
                if (TravelerID != "0") {
                    $.ajax({
                        url: hostUrl,
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        data: "{'Id':'" + Id + "','TravelerID':'" + TravelerID + "','Price':'" + Price + "','PriceBand':'" + PriceBand + "'}",
                        success: function (Pdata) {
                            console.log(Pdata.d);

                            if (Pdata.d == "yes") {
                                $(obj).attr("title", "Edit Price");
                                $(obj).attr("class", "edit");
                                $(obj).attr("src", "../images/edit.png");

                                $(selectrow).find("[id*=ddlpriceband]").addClass("hide");
                                $(selectrow).find("[id*=ddlTravellerName]").addClass("hide");
                                $(selectrow).find("[id*=txtpriceEdit]").addClass("hide");

                                $(selectrow).find("[id*=lblpriceband]").removeClass("hide").text($(selectrow).find("[id*=ddlpriceband] :selected").text());
                                $(selectrow).find("[id*=lblTravellerName]").removeClass("hide").text($(selectrow).find("[id*=ddlTravellerName] :selected").text());
                                $(selectrow).find("[id*=lblPriceShow]").removeClass("hide").text(parseFloat($(selectrow).find("[id*=txtpriceEdit]").val()).toFixed("2"));
                            }
                        },
                        error: function () {
                            console.log("error on request method");
                        }
                    });
                }
                else {
                    $(selectrow).find("[id*=ddlTravellerName]").css("background", "#ffbfbf");
                }
            }
        }

        function ActiveInactivePrice(Id, obj) {
            var casetype = $(obj).attr("class");
            var hostUrl = window.location.origin + window.location.pathname + '/activeInactiveProductPrice';
            if (Id != undefined && Id != '') {
                $.ajax({
                    url: hostUrl,
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    data: "{'Id':'" + Id + "'}",
                    success: function (Pdata) {
                        console.log(Pdata.d);
                        if (Pdata.d == "yes" && casetype == 'active-flag') {
                            $(obj).attr("title", "Inactive");
                            $(obj).attr("class", "inactive-flag");
                            $(obj).attr("src", "../images/inactive.png");
                        }
                        else if (Pdata.d == "yes" && casetype == 'inactive-flag') {
                            $(obj).attr("title", "Active");
                            $(obj).attr("class", "active-flag");
                            $(obj).attr("src", "../images/active.png");
                        }
                    },
                    error: function () {
                        console.log("error on request method");
                    }
                });
            }
            else {
                if (casetype == 'active-flag') {
                    $(obj).attr("title", "Inactive");
                    $(obj).attr("class", "inactive-flag");
                    $(obj).attr("src", "../images/inactive.png");
                }
                else if (casetype == 'inactive-flag') {
                    $(obj).attr("title", "Active");
                    $(obj).attr("class", "active-flag");
                    $(obj).attr("src", "../images/active.png");
                }
            }
        }
    </script>
</asp:Content>
