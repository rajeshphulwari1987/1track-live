﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DownloadProduct.aspx.cs"
    Inherits="IR_Admin.Product.DownloadProduct" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        #divprogress{ display: none!important;}
        .grid-sec2
        {
            width: 900px;
        }
        .grid-sec2 legend
        {
            color: #941e34;
            font-family: 'Trebuchet MS' , Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divNew" runat="server" style="display: block;">
                    <div class="heading">
                        Product List Update
                    </div>
                  
                    <div class="content-in">
                      <div class="searchDiv" style="text-align: center;border:none;">
                        <asp:DropDownList ID="ddlCategory" runat="server" Width="510px"/>
                    </div>
                        <fieldset class="grid-sec2">
                            <legend>Download Product</legend>
                            <div class="colum-one">
                                &nbsp;</div>
                            <div class="colum-two">
                                <div class="file-container" style="width: 200px; height: 80px;">
                                    <img src="../images/product-temp.png" /><br />
                                    <asp:LinkButton ID="lnkPrd" runat="server" onclick="lnkPrd_Click">Download Product </asp:LinkButton>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="grid-sec2">
                            <legend>Update and Upload Product</legend>
                            <div class="colum-one">
                                &nbsp;</div>
                            <div class="colum-two">
                                <asp:FileUpload ID="fupProduct" runat="server" /></div>
                            <div class="colum-two">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="submit" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
