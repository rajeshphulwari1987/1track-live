﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class ProductMarkupList : System.Web.UI.Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            FillMarkups();
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FillMarkups();
        }

        public void FillMarkups()
        {
            try
            {
                _SiteID = this.Master.SiteID;
                //List<spShowAllMarkups_Result> lst = _oProduct.GetAllMarkup(_SiteID);
                //if (lst.Count() > 0)
                //{
                //    var lstnew = from x in lst  select new { x.MarkUpId, x.CategoryID, Category = (x.CategoryName == null ? x.SubCategoryName : x.CategoryName + " -> " + x.SubCategoryName), ProductName = (x.ProductName == null ? "-" : x.ProductName), PassTypeCode = (x.PassTypeCode == null ? "-" : "<a href='#' title='Traveller Validity : " + x.Validity + " Class : " + x.ClassName + " Traveller : " + x.TravelerTypeName + " Price : " + x.Price.ToString() + "' class='yellow colorTipContainer'>" + x.PassTypeCode.ToString() + "</a>"), x.MarkUpAmount, x.MarkUpPercent };
                //    rptProductMarkUp.DataSource = lstnew;
                //}
                //else
                //    rptProductMarkUp.DataSource = null;
                List<GetCategoryMarkup_Result> lst = _oProduct.GetAllCategoriseMarkup(_SiteID);
                if (lst.Count() > 0)
                    rptProductMarkUp.DataSource = lst;
                else
                    rptProductMarkUp.DataSource = null;
                rptProductMarkUp.DataBind();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
                ShowMessage(2, "Unexpected Error");
            }
        }

        protected void rptProductMarkUp_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    _SiteID = Master.SiteID;
                    bool res = _oProduct.DeleteProductMarkup(Guid.Parse(e.CommandArgument.ToString()), _SiteID);
                    if (res)
                        ShowMessage(1, "Record deleted successfully");
                    FillMarkups();

                }
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
                ShowMessage(2, "Unexpected Error");
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}