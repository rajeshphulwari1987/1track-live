﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductCommissionList.aspx.cs" Inherits="IR_Admin.Product.ProductCommissionList" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="<%=ConfigurationSettings.AppSettings["HttpHost"].ToString() %>Styles/colortip-1.0-jquery.css"
        rel="stylesheet" type="text/css" />
    <script src="<%=ConfigurationSettings.AppSettings["HttpHost"].ToString() %>Scripts/colortip-1.0-jquery.js"
        type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.yellow').colorTip({ color: 'yellow' });
        });
    </script>
    <style type="text/css">
        .style1
        {
            width: 682px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlProductCommission" runat="server">
        <ContentTemplate>
            <h2>
                Product Commission List</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="ProductCommissionList.aspx" class="current">List</a></li>
                    <li style="font-size: 14px;">
                        <asp:HyperLink ID="aNew" NavigateUrl="ProductCommssion.aspx" CssClass="" runat="server">New</asp:HyperLink>
                    </li>
                </ul>
                <br />
            </div>
            <div class="clear">
            </div>
            <div id="dvSearch" class="searchDiv" runat="server">
                <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                <table width="100%" style="line-height: 10px; font-size: 14px">
                    <tr>
                        <td>
                            Product Category :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCategory" runat="server" ValidationGroup="rv" Width="500px"
                                AutoPostBack="true" OnTextChanged="btnSubmit_Click" ToolTip="Select Product Category" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- tab "panes" -->
            <div class="full mr-tp1" style="margin-top: 0px">
                <div class="panes">
                    <div id="divNew" runat="server" style="display: block;">
                        <asp:HiddenField runat="server" ID="hdnPath" />
                        <asp:Repeater ID="rptProductCommission" runat="server" OnItemCommand="rptProductCommission_ItemCommand">
                            <HeaderTemplate>
                                <table style="width: 100%; font-weight: bold;" class="grid-head2">
                                    <tr>
                                        <td style="width: 15%;">
                                            Agent
                                        </td>
                                        <td style="width: 20%;">
                                            Product
                                        </td>
                                        <td style="width: 30%;">
                                            Pass Details
                                        </td>
                                        <td style="width: 15%;">
                                            Commission (%)
                                        </td>
                                        <td style="width: 10%; text-align: center;">
                                            Action
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <AlternatingItemTemplate>
                                <div class="cat-inner-alt" style="margin: 0px; width: 100%">
                                    <div style="width: 15%; float: left;">
                                        <%#Eval("Agent")%>
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        <%#Eval("ProductName")%>
                                    </div>
                                    <div style="width: 30%; float: left; padding-left: 2%;">
                                        <strong>Traveller Validity:</strong>
                                        <%#Eval("Validity") %><br />
                                        <strong>Class:</strong>
                                        <%#Eval("ClassName")%><br />
                                        <strong>Traveller:</strong>
                                        <%#Eval("TravelerName")%><br />
                                        <strong>Price:</strong>
                                        <%#Eval("Price")%><br />
                                        <strong>PassTypeCode:</strong>
                                        <%#Eval("PassTypeCode")%>
                                    </div>
                                    <div style="width: 18%; float: left; text-align: center;">
                                        <%#Eval("Percentage", "{0:n2}")%>
                                    </div>
                                    <div style="width: 10%; float: left; text-align: right">
                                        <a href='ProductCommssion.aspx?id=<%#Eval("CommissionId") %>'>
                                            <img src="../images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("CommissionID") %>' CommandName="Delete" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                            <ItemTemplate>
                                <div class="cat-inner" style="margin: 0px; width: 100%">
                                    <div style="width: 15%; float: left;">
                                        <%#Eval("Agent")%>
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        <%#Eval("ProductName")%>
                                    </div>
                                    <div style="width: 30%; float: left; padding-left: 2%;">
                                        <strong>Traveller Validity:</strong>
                                        <%#Eval("Validity") %><br />
                                        <strong>Class:</strong>
                                        <%#Eval("ClassName")%><br />
                                        <strong>Traveller:</strong>
                                        <%#Eval("TravelerName")%><br />
                                        <strong>Price:</strong>
                                        <%#Eval("Price")%><br />
                                        <strong>PassTypeCode:</strong>
                                        <%#Eval("PassTypeCode")%>
                                    </div>
                                    <div style="width: 18%; float: left; text-align: center;">
                                        <%#Eval("Percentage","{0:n2}")%>
                                    </div>
                                    <div style="width: 10%; float: left; text-align: right">
                                        <a href='ProductCommssion.aspx?id=<%#Eval("CommissionId") %>'>
                                            <img src="../images/edit.png" /></a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("CommissionID") %>' CommandName="Delete" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div id="divRecordNotfnd" runat="server" class="warning" style="display: none;">
                            Record not found.
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
