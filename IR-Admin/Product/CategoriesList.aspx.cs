﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class CategoriesList : System.Web.UI.Page
    {
        private readonly ManageProduct _oProduct = new ManageProduct();
        private readonly Masters _oMasters = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        Guid Pid = new Guid();

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            hdnPath.Value = string.Empty;
            FillRptCatories(new Guid(), _SiteID);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!IsPostBack)
            {
                _SiteID = this.Master.SiteID;
                SiteSelected();
                FillRptCatories(Pid, _SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillRptCatories(Pid,_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }


        void FillRptCatories(Guid pid, Guid siteid)
        {
            try
            {
                if(siteid==new Guid())
                    siteid = _SiteID = this.Master.SiteID;

                var list = _oProduct.GetCategorySiteList(pid, siteid);
                if (list != null && list.Count > 0)
                {
                    rptCatoriesList.DataSource = list.OrderBy(x => x.Name);
                    rptCatoriesList.DataBind();
                    divRecordNotfnd.Style.Add("display", "none");
                }
                else if(pid==new Guid())
                {
                    rptCatoriesList.DataSource = null;                    
                    rptCatoriesList.DataBind();
                    divRecordNotfnd.Style.Add("display", "block");
                  
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void rptCatoriesList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                Guid Id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "Delete")
                {
                    Pid = _oProduct.GetParentIdforChildId(Id);
                    int result = _oProduct.DeleteCategory(Id);
                    if (result > 0)
                        ShowMessage(2, "Category is already in use.");
                    else
                        ShowMessage(1, "Record deleted successfully");
                    FillRptCatories(Pid, _SiteID);
                }
                if (e.CommandName == "FINDSUBCATEGORY")
                {
                    LinkButton lnkbtn = e.Item.FindControl("lnkPId") as LinkButton;
                    hdnPath.Value = lnkbtn.Text;
                    FillRptCatories(Id, _SiteID);
                }
                if (e.CommandName == "ActiveInActive")
                {
                    _oProduct.ActiveInactiveCategory(Id);
                    FillRptCatories(new Guid(), _SiteID);
                    Response.Redirect(Request.Url.ToString());
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
         
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void rptCatoriesList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if(e.Item.ItemType==ListItemType.AlternatingItem ||e.Item.ItemType==ListItemType.Item)
            {
                LinkButton lnkcat = e.Item.FindControl("lnkPId") as LinkButton;

                lnkcat.Text = string.IsNullOrEmpty(hdnPath.Value) ? lnkcat.Text : hdnPath.Value + " - " + lnkcat.Text;
            }
        }

    }

}
