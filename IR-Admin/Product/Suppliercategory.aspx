﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Suppliercategory.aspx.cs" Inherits="IR_Admin.Product.Suppliercategory" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            if ('<%=tab%>' == '1') {
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
                $("#aList").addClass("current");
                $("#aNew").removeClass("current");
            }
            else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
                $("#aNew").addClass("current");
                $("#aList").removeClass("current");
            }

            $(".list a").click(function (e) {
                if ($(this).attr("id") == 'aList') {
                    document.getElementById('MainContent_divlist').style.display = 'Block';
                    document.getElementById('MainContent_divNew').style.display = 'none';
                    $("#aList").addClass("current");
                    $("#aNew").removeClass("current");
                }
                else {
                    document.getElementById('MainContent_divlist').style.display = 'none';
                    document.getElementById('MainContent_divNew').style.display = 'Block';
                    $("#aNew").addClass("current");
                    $("#aList").removeClass("current");
                }
            });

        });
    </script>
    <style type="text/css">
        .grid-head2 tr td table tr td
        {
            padding: 0px 5px 0px 5px;
            font-size: 13px;
            font-weight: 600;
        }
        .grid-head2 tr td table tr td span
        {
            background: #ccc;
            padding: 11px;
        }
        td
        {
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Supplier category</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" class="current" style="cursor: pointer;">List</a></li>
            <li><a id="aNew" style="cursor: pointer;">New/Edit</a></li>
        </ul>
    </div>
    <!-- tab "panes" -->
    <div class="full mr-tp1">
        <div class="panes">
            <div id="divlist" runat="server" style="display: none;">
                <div class="crushGvDiv">
                    <asp:GridView ID="grdSupplier" runat="server" CellPadding="4" CssClass="grid-head2"
                        ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowPaging="true"
                        PageSize="10" OnRowCommand="grdSupplier_OnRowCommand" OnPageIndexChanging="grdSupplier_PageIndexChanging"
                        OnRowDataBound="grdSupplier_RowDataBound">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataTemplate>
                            Data not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Supplier Name">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Eval("Name")%>' ID="lblName"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Supplier Category">
                                <ItemTemplate>
                                    <asp:Repeater ID="rptinnerlist" runat="server">
                                        <ItemTemplate>
                                            <div>
                                                <%#Eval("Name")%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Supplier Commission">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Eval("Commission")%>' ID="lblCommission"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Supplier Currency">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Eval("Currency")%>' ID="lblCurrency"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("Name")%>' CommandName="Modify" ImageUrl="../images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Name")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div id="divNew" runat="server" style="display: Block;">
                <div class="divMain">
                    <div class="divleft">
                        Supplier Name:
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtSName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtCNameRequiredFieldValidator" runat="server" ControlToValidate="txtSName"
                            CssClass="valdreq" ErrorMessage="*" ValidationGroup="SupForm"></asp:RequiredFieldValidator>
                    </div>
                    <div class="divleft">
                        Supplier currency:
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlCurrency" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rvCurrency" runat="server" ControlToValidate="ddlCurrency"
                            CssClass="valdreq" ErrorMessage="*" ValidationGroup="SiteForm" InitialValue="0" />
                    </div>
                    <div class="divleft">
                        Commission received (%):
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtcommission" runat="server" style="width: 200px;
    height: 20px;"></asp:TextBox>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Repeater runat="server" ID="rptSCName" OnItemCommand="rptSCName_ItemCommand">
                                <ItemTemplate>
                                    <div class="divleft">
                                        Supplier category name
                                        <asp:Label runat="server" ID="lblNo" Text='<%#Eval("No")%>'></asp:Label>:
                                    </div>
                                    <div class="divright">
                                        <asp:TextBox ID="txtSCName" runat="server" Text='<%#Eval("Name")%>'></asp:TextBox>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Eval("Id")%>' />
                                        <asp:ImageButton runat="server" ID="btnDelete" ImageUrl="../images/delete.png" ToolTip="Remove"
                                            CommandArgument='<%#Eval("Id")%>' CommandName="Remove" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div class="divleft">
                                        &nbsp;
                                    </div>
                                    <div class="divright">
                                        <asp:Button ID="lnkAddNew" runat="server" Text="Add “+”" CommandName="Add" CommandArgument="Add"
                                            CssClass="button" />
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="divleft">
                        Is Active?
                    </div>
                    <div class="divright">
                        <asp:CheckBox ID="chkactive" runat="server" />
                    </div>
                    <div class="divleft">
                        Is Britrail Supplier?
                    </div>
                    <div class="divright">
                        <asp:CheckBox ID="chkBritrail" runat="server" />
                    </div>
                    <div class="divleft">
                        Is Interrail Supplier?
                    </div>
                    <div class="divright">
                        <asp:CheckBox ID="chkInterrail" runat="server" />
                    </div>
                    <div class="divleftbtn">
                        .
                    </div>
                    <div class="divrightbtn">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                            ValidationGroup="SupForm" OnClick="btnSubmit_Click" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
