﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web;

namespace IR_Admin.Product
{
    public partial class ProductList : Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private Masters _oMasters = new Masters();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        public bool RoleAdmin = false;
        public bool RoleSTAAdministrator = false;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));
            BindCategory();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (AdminuserInfo.RoleId.Value == Guid.Parse("14F08452-5C69-44A8-B4D1-585BE6B10B16"))
                    RoleSTAAdministrator = true;
                RoleAdmin = EmailSchedular.ExpiredRoleActivated();
                if (!RoleAdmin)
                    rdbExpiry.Visible = false;
                ShowMessage(0, null);
                _SiteID = Master.SiteID;
                if (!Page.IsPostBack)
                {
                    BindCategory();
                    if (Session["showAllPages"] != null && Session["showAllPages"].ToString() == "All")
                    {
                        ddlPaging.SelectedValue = "5000";
                        BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));
                    }
                    else
                        BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));

                    for (int i = 0; i <= _oProduct.GetProductCount(); i++)
                    {
                        ddlShortOrder.Items.Insert(0, new ListItem(i.ToString(), i.ToString()));
                    }
                    ddlShortOrder.Items.Insert(0, new ListItem("--Select--", "-1"));
                    BindCurrency();
                    BindProduct();
                    hdnSiteId.Value = _SiteID.ToString();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "searchData", "resetAdvancedSearch();clearAdvanceSearch();", true);
        }

        void BindCategory()
        {
            try
            {
                _SiteID = Master.SiteID;
                const int treeLevel = 3;
                ddlCategory.DataSource = _oProduct.GetAssociateCategoryList(treeLevel, _SiteID);
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindGrid(int pageSize)
        {
            try
            {
                _SiteID = Master.SiteID;
                var categoriesId = new List<Guid>();
                if (ddlCategory.SelectedIndex > 0)
                    categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };

                var list = _oProduct.GetProductListMarkUp(_SiteID, categoriesId);
                ddlPaging.SelectedValue = pageSize.ToString();
                grvProduct.PageSize = pageSize;
                grvProduct.DataSource = _oProduct.GetProductList(_SiteID, categoriesId);
                grvProduct.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindCurrency()
        {
            ddlCurrency.DataSource = _oMasters.GetCurrencyList().Where(x => x.IsActive == true);
            ddlCurrency.DataTextField = "Name";
            ddlCurrency.DataValueField = "ID";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, new ListItem("--Select--", "-1"));
        }

        void BindProduct()
        {
            _SiteID = Master.SiteID;
            var categoriesId = new List<Guid>();
            if (ddlCategory.SelectedIndex > 0)
                categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };
            ddlProductName.DataSource = _oProduct.GetProductList(_SiteID, categoriesId).ToList();
            ddlProductName.DataValueField = "ID";
            ddlProductName.DataTextField = "Name";
            ddlProductName.DataBind();
            ddlProductName.Items.Insert(0, new ListItem("--Select--", "-1"));
        }

        protected void grvProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvProduct.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(hidAdvanceSearchType.Value))
                    BindSearchingProduct();
                else
                    BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "holdOldData", "holdOldData();changeProductColor();shortProductOrder();", true);
        }

        protected void grvProduct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    int res = _oProduct.DeleteProduct(id);
                    if (res > 0)
                        ShowMessage(2, "Product is already in use.");
                    else
                        ShowMessage(1, "Product deleted successfully");
                }
                else if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _oProduct.ActiveInactiveProduct(id);
                }
                else if (e.CommandName == "IsTopRailPass")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _oProduct.IsTopRailPass(id);
                }
                //else if (e.CommandName == "IsPromoPass")
                //{
                //    var id = Guid.Parse(e.CommandArgument.ToString());
                //    _oProduct.IsPromoPass(id);
                //}
                else if (e.CommandName == "IsItxPass")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _oProduct.IsItxPrpduct(id);
                }
                else if (e.CommandName == "TemplateNote")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    mdpupTemplateNote.Show();
                    txtTemplateNote.Text = _oProduct.ProductTemplateNote(id);
                    hdnProductID.Value = id.ToString();
                }
                if (!string.IsNullOrEmpty(hidAdvanceSearchType.Value))
                    BindSearchingProduct();
                else
                    BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "holdOldData", "holdOldData();changeProductColor();shortProductOrder();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid ID = Guid.Parse(hdnProductID.Value);
                _oProduct.ProductTemplateNoteUpdate(ID, txtTemplateNote.Text);
                mdpupTemplateNote.Hide();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "holdOldData", "holdOldData();changeProductColor();shortProductOrder();", true);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            BindSearchingProduct();
        }

        void BindSearchingProduct()
        {
            try
            {
                var categoriesId = new List<Guid>();
                if (ddlCategory.SelectedIndex > 0)
                    categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };
                var list = _oProduct.GetProductList(_SiteID, categoriesId).ToList();
                if (list != null && list.Count > 0)
                {
                    if (!string.IsNullOrEmpty(hidAdvanceSearchType.Value))
                    {
                        switch (hidAdvanceSearchType.Value)
                        {
                            case "shortorder":
                                list = ddlShortOrder.SelectedValue != "-1" ? list.Where(x => x.Sortorder == Convert.ToInt32(ddlShortOrder.SelectedValue)).ToList() : list;
                                break;
                            case "product":
                                list = ddlProductName.SelectedValue != "-1" ? list.Where(x => x.Name.Contains(ddlProductName.SelectedItem.Text)).ToList() : list;
                                break;
                            case "currency":
                                list = ddlCurrency.SelectedValue != "-1" ? list.Where(x => x.Currency.Contains(ddlCurrency.SelectedItem.Text)).ToList() : list;
                                break;
                            case "top":
                                list = list.Where(x => x.IsTopRailPass == Convert.ToBoolean(ddlActive.SelectedValue)).ToList();
                                break;
                            case "SpecialandBest":
                                list = list.Where(x => x.IsSpecialandBest == Convert.ToBoolean(ddlActive.SelectedValue)).ToList();
                                break;
                            case "itx":
                                list = list.Where(x => x.IsItxPass == Convert.ToBoolean(ddlActive.SelectedValue)).ToList();
                                break;
                            case "active":
                                list = list.Where(x => x.IsActive == Convert.ToBoolean(ddlActive.SelectedValue)).ToList();
                                break;
                            case "expiry":
                                list = list.Where(x => x.CircleImgType == Convert.ToInt32(ddlExpiry.SelectedValue)).ToList();
                                break;
                        }
                    }
                    grvProduct.DataSource = list;
                    grvProduct.DataBind();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "holdOldData", "holdOldData();changeProductColor();shortProductOrder();", true);
        }

        [WebMethod(EnableSession = true)]
        public static string UpdateProductOrder(string Value, string SiteId, string pageNo, int pageSize)
        {
            try
            {
                db_1TrackEntities db = new db_1TrackEntities();
                Guid SiteIds = Guid.Parse(SiteId);
                string ProductIds = Value.TrimEnd(',');
                string[] NewProductIds = ProductIds.Split(',');
                if (NewProductIds.Length > 0)
                {
                    int i = 1;
                    int page = Convert.ToInt32(pageNo);
                    if (page >= 1)
                        i = Convert.ToInt32(Convert.ToString(page - 1)) * pageSize + 1;

                    foreach (var item in NewProductIds)
                    {
                        Guid Products = Guid.Parse(item);
                        var result = db.tblProductSiteLookUps.FirstOrDefault(x => x.ProductID == Products && x.SiteID == SiteIds);
                        if (result != null)
                            result.ShortOrder = i;
                        db.SaveChanges();
                        i++;
                    }
                    HttpContext.Current.Session["showAllPages"] = "All";
                    return "Yes";
                }
                else
                    return "No";
            }
            catch (Exception ex) { return "No"; }
        }

        protected void grvProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                    if (AdminuserInfo.RoleId.HasValue)
                        if (AdminuserInfo.RoleId.Value == Guid.Parse("14F08452-5C69-44A8-B4D1-585BE6B10B16"))
                            grvProduct.Columns[1].Visible = grvProduct.Columns[4].Visible = grvProduct.Columns[5].Visible = grvProduct.Columns[6].Visible = grvProduct.Columns[7].Visible = grvProduct.Columns[8].Visible = false;
                        else
                            grvProduct.Columns[9].Visible = false;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void ddlPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid(Convert.ToInt32(ddlPaging.SelectedValue));
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "searchData", "resetAdvancedSearch();clearAdvanceSearch();", true);
        }
    }
}
