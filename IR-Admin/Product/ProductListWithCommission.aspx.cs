﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;

namespace IR_Admin.Product
{
    public partial class ProductListWithCommission : System.Web.UI.Page
    {
        readonly ManagePrintQueue _oQueue = new ManagePrintQueue();
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private Masters _oMasters = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            PageLoadEvent();

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["edit"] != null)
            {
                // Boolean 
            }
            if (!IsPostBack)
            {
                _SiteID = this.Master.SiteID;
                BindOffices();
                PageLoadEvent();

            }
        }
        void PageLoadEvent()
        {
            try
            {
                BindCategory();
                BindProductGrid();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void BindOffices()
        {
            ddlOffices.DataSource = _oQueue.GetOfficeList();
            ddlOffices.DataValueField = "ID";
            ddlOffices.DataTextField = "Name";
            ddlOffices.DataBind();
            ddlOffices.Items.Insert(0, new ListItem("--Select Office--", "0"));
        }
        void BindCategory()
        {
            if (ddlOffices.SelectedIndex > 0)
            {
                const int treeLevel = 3;
                var list = _oProduct.GetAssociateCategoryListWithOutSiteId(treeLevel, Guid.Parse(ddlOffices.SelectedValue));
                grvCategories.DataSource = list != null ? list.OrderBy(x => x.Name).ToList() : null;
                grvCategories.DataBind();

                Session["CatIds"] = list.Select(x => x.ID).ToList();
                ddlCategory.DataSource = list;
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));

                if (ViewState["CategoryIndx"] != null)
                    ddlCategory.SelectedIndex = Convert.ToInt32(ViewState["CategoryIndx"].ToString());
            }
            else
            {
                ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
                grvCategories.DataSource = null;
                grvCategories.DataBind();
            }


        }
        protected void grvCategories_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCategories.PageIndex = e.NewPageIndex;
            BindCategory();
        }


        void BindProductGrid()
        {
            //if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
            //    _SiteID = this.Master.SiteID;
            //else
            //    _SiteID = Guid.Parse(ddlSites.SelectedValue);
            if (ddlCategory.SelectedIndex > 0)
            {
                List<Guid> categoriesId = new List<Guid>();
                if (ddlCategory.SelectedIndex > 0)
                    categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };
                else
                {
                    if (Session["CatIds"] != null)
                        categoriesId = Session["CatIds"] as List<Guid>;
                }

                var list = _oProduct.GetProductListUsingCategoryId(categoriesId, Guid.Parse(ddlOffices.SelectedValue));
                grvProduct.DataSource = list;
                grvProduct.DataBind();

                ddlProducts.DataSource = list;
                ddlProducts.DataTextField = "Name";
                ddlProducts.DataValueField = "ID";
                ddlProducts.DataBind();
                ddlProducts.Items.Insert(0, new ListItem("--Select Product--", "0"));
                Session["ProductIds"] = list.Select(x => x.ID).ToList();

                if (ViewState["ProductIndx"] != null)
                    ddlProducts.SelectedIndex = Convert.ToInt32(ViewState["ProductIndx"].ToString());
            }
            else
            {
                ddlProducts.Items.Insert(0, new ListItem("--Select Product--", "0"));
                grvProduct.DataSource = null;
                grvProduct.DataBind();
            }
        }
        protected void grvProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProduct.PageIndex = e.NewPageIndex;
            BindProductGrid();
        }

        void BindProductPriceGrid()
        {
            List<Guid> pids = new List<Guid>();
            //if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
            //    _SiteID = this.Master.SiteID;
            //else
            //    _SiteID = Guid.Parse(ddlSites.SelectedValue);

            if (ddlProducts.SelectedIndex > 0 && ddlProducts.SelectedValue.Trim() != "0")
                pids = new List<Guid> { Guid.Parse(ddlProducts.SelectedValue) };
            else
            {
                if (Session["ProductIds"] != null)
                    pids = Session["ProductIds"] as List<Guid>;
            }
            if (ddlCategory.SelectedIndex > 0)
                grvProductPrice.DataSource = _oProduct.GetProductsPricesWithProductNameByProductId(pids, Guid.Parse(ddlCategory.SelectedValue), Guid.Parse(ddlOffices.SelectedValue));
            else
                grvProductPrice.DataSource = null;

            grvProductPrice.DataBind();

        }

        protected void grvProductPrice_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProductPrice.PageIndex = e.NewPageIndex;
            BindProductPriceGrid();
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender;
                HiddenField hdnFlag = btnUpdate.Parent.FindControl("hdnFlag") as HiddenField;
                HiddenField hdnId = btnUpdate.Parent.FindControl("hdnId") as HiddenField;
                TextBox txtAmount = btnUpdate.Parent.FindControl("txtMarkUp") as TextBox;
                // CheckBox chkPercent = btnUpdate.Parent.FindControl("chkIsPercent") as CheckBox;
                Guid Id = Guid.Parse(hdnId.Value);
                Int32 flag = Convert.ToInt32(hdnFlag.Value);
                Decimal Commission = Convert.ToDecimal(txtAmount.Text);
                if (flag == 1)
                {
                    _oProduct.ManageCategoryCommission(Id, Guid.Parse(ddlOffices.SelectedValue), Commission);
                }
                else if (flag == 2)
                {
                    HiddenField hdnCategoryID = btnUpdate.Parent.FindControl("hdnCategoryID") as HiddenField;

                    _oProduct.ManageProductCommission(Guid.Parse(hdnCategoryID.Value.Trim()), Id, Guid.Parse(ddlOffices.SelectedValue), Commission);
                }
                else if (flag == 3)
                {

                    HiddenField hdnCategoryID = btnUpdate.Parent.FindControl("hdnCategoryID") as HiddenField;
                    HiddenField hdnProductID = btnUpdate.Parent.FindControl("hdnProductID") as HiddenField;
                    _oProduct.ManagePriceCommission(Guid.Parse(hdnCategoryID.Value.Trim()), Guid.Parse(hdnProductID.Value.Trim()), Id, Guid.Parse(ddlOffices.SelectedValue), Commission);
                }
                //if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                //    _SiteID = this.Master.SiteID;
                //else
                //    _SiteID = Guid.Parse(ddlSites.SelectedValue);

                // _oProduct.UpdateMarkup(Id, markup, chkPercent.Checked, _SiteID, flag);
                ShowMessage(1, "Commission update successfully.");
                BindCategory();
                BindProductGrid();
                BindProductPriceGrid();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }


        //protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ViewState["CategoryIndx"] = null;
        //    ShowMessage(0, null);
        //    if (ddlSites.SelectedIndex > 0)
        //        BindCategory();
        //    grvProduct.DataSource = null;
        //    grvProduct.DataBind();
        //    grvProductPrice.DataSource = null;
        //    grvProductPrice.DataBind();
        //}

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["CategoryIndx"] = ddlCategory.SelectedIndex;
            ViewState["ProductIndx"] = null;
            BindProductGrid();
            grvProductPrice.DataSource = null;
            grvProductPrice.DataBind();
        }

        protected void ddlProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["ProductIndx"] = ddlProducts.SelectedIndex;
            BindProductPriceGrid();

        }
        protected void ddlOffices_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["CategoryIndx"] = null;
            ShowMessage(0, null);
            //if (ddlSites.SelectedIndex > 0)
            BindCategory();
            ddlCategory_SelectedIndexChanged(null, null);
        }

        protected void grvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnId = e.Row.FindControl("hdnId") as HiddenField;
                Label lblHaveDifferentComm = e.Row.FindControl("lblHaveDifferentComm") as Label;
                if (hdnId != null && lblHaveDifferentComm != null)
                {
                    Boolean rval = false;
                    if (ddlOffices.SelectedIndex > 0)
                    {
                        rval = _oProduct.CategoryHaveCommission(Guid.Parse(hdnId.Value.Trim()), Guid.Parse(ddlOffices.SelectedValue.Trim()));
                    }
                    if (rval == true)
                        lblHaveDifferentComm.Text = "<a href='#' title='Child Comission is different.'><img src='../images/info.png' alt='' width='25px' style='float:left;' /></a>";
                    else
                        lblHaveDifferentComm.Text = "";
                    //rval.ToString();
                }
            }
        }
        protected void grvProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnId = e.Row.FindControl("hdnId") as HiddenField;
                HiddenField hdnCategoryID = e.Row.FindControl("hdnCategoryID") as HiddenField;
                Label lblHaveDifferentComm = e.Row.FindControl("lblHaveDifferentComm") as Label;
                if (hdnId != null && lblHaveDifferentComm != null)
                {
                    Boolean rval = false;
                    if (ddlOffices.SelectedIndex > 0)
                    {
                        rval = _oProduct.CategoryHaveCommissionForProduct(Guid.Parse(hdnCategoryID.Value.Trim()), Guid.Parse(hdnId.Value.Trim()), Guid.Parse(ddlOffices.SelectedValue.Trim()));
                    }
                    if (rval == true)
                        lblHaveDifferentComm.Text = "<a href='#' title='Child Comission is different.'><img src='../images/info.png' alt='' width='25px' style='float:left;' /></a>";
                    else
                        lblHaveDifferentComm.Text = "";
                    //lblHaveDifferentComm.Text = "<a href='#' title='Child Comission is different.'><img src='../images/info.png' alt='' width='25px' /></a>";
                    //lblHaveDifferentComm.Text = rval.ToString();
                }
            }
        }

    }
}