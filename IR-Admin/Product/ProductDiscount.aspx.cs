﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Product
{
    public partial class ProductDiscount : Page
    {
        readonly ManageProduct _product = new ManageProduct();
        private readonly MasterProductDiscount _oMasters = new MasterProductDiscount();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            FillGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            if (!IsPostBack)
            {
                btnSubmit.Enabled = true;
                if (Request["id"] != null)
                {
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                PageLoadEvent();
                GetValueForEdit();

                _SiteID = Master.SiteID;
                SiteSelected();
                FillGrid(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        private void PageLoadEvent()
        {
            IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void FillGrid(Guid siteID)
        {
            grdPrdDiscount.DataSource = _oMasters.GetProductDiscount(siteID);
            grdPrdDiscount.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var tn = trSites.CheckedNodes.Count;
            if (tn == 0)
            {
                ShowMessage(2, "Please Select Site");
                tab = "2";
                return;
            }
            AddEditDiscount();
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            ClearControls();
            ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "You have successfully updated product discount." : "You have successfully added product discount.");

            tab = "1";
            ViewState["tab"] = "1";
        }

        private void ClearControls()
        {
            txtName.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtDesc.Text = string.Empty;
            chkIsActv.Checked = false;
            chkIsPerc.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductDiscount.aspx");
        }

        public void AddEditDiscount()
        {
            try
            {
                var id = string.IsNullOrEmpty(Request.QueryString["id"]) ? new Guid() : Guid.Parse(Request.QueryString["id"]);
                var listSitId = (from TreeNode node in trSites.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();

                DateTime? DiscountFromDate = null;
                DateTime? DiscountToDate = null;
                if (!string.IsNullOrEmpty(txtDiscountFromDate.Text))
                    DiscountFromDate = DateTime.ParseExact((txtDiscountFromDate.Text), "dd/MM/yyyy HH:mm", null);
                if (!string.IsNullOrEmpty(txtDiscountToDate.Text))
                    DiscountToDate = DateTime.ParseExact((txtDiscountToDate.Text), "dd/MM/yyyy HH:mm", null);

                var res = _oMasters.AddEditDiscount(new MasterProductDiscount.ClsProductDiscount
                {
                    ID = id,
                    Name = txtName.Text.Trim(),
                    Amount = Convert.ToDecimal(txtAmount.Text),
                    Code = txtCode.Text.Trim(),
                    Description = txtDesc.Text,
                    IsActive = chkIsActv.Checked,
                    IsPercentage = chkIsPerc.Checked,
                    CreatedBy = AdminuserInfo.UserID,
                    ListSiteId = listSitId,
                    DiscountFromDate = DiscountFromDate,
                    DiscountToDate = DiscountToDate
                });
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        public void GetValueForEdit()
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    return;

                btnSubmit.Text = "Update";
                var id = Guid.Parse(Request.QueryString["id"]);

                var rec = _oMasters.GetDiscountById(id);
                txtName.Text = rec.Name;
                txtAmount.Text = rec.Amount.ToString();
                txtCode.Text = rec.Code;
                txtDesc.Text = rec.Description;
                chkIsPerc.Checked = rec.IsPercentage;
                chkIsActv.Checked = rec.IsActive;
                txtDiscountFromDate.Text = rec.DiscountFromDate.HasValue ? rec.DiscountFromDate.Value.ToString("dd/MM/yyyy HH:mm") : "";
                txtDiscountToDate.Text = rec.DiscountToDate.HasValue ? rec.DiscountToDate.Value.ToString("dd/MM/yyyy HH:mm") : "";
                foreach (TreeNode itm in trSites.Nodes)
                {
                    itm.Checked = rec.ListSiteId.Contains(Guid.Parse(itm.Value));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void grdPrdDiscount_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                bool res = _oMasters.DeleteDiscount(id);
                if (res)
                    ShowMessage(1, "Successfully deleted record.");
            }
            if (e.CommandName == "ActiveInActive")
            {
                _oMasters.ActiveInActiveDiscount(id);
                tab = "1";
            }
            if (e.CommandName == "IsPercent")
            {
                _oMasters.ActiveInActivePercentage(id);
                tab = "1";
            }
            _SiteID = Master.SiteID;
            SiteSelected();
            FillGrid(_SiteID);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdPrdDiscount_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPrdDiscount.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            FillGrid(_SiteID);
        }
    }
}