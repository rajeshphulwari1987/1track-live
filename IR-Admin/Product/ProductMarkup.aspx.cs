﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;

namespace IR_Admin.Product
{
    public partial class ProductMarkup1 : System.Web.UI.Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private Masters _oMasters = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            PageLoadEvent();

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _SiteID = this.Master.SiteID;
                PageLoadEvent();

            }
        }
        void PageLoadEvent()
        {
            try
            {
                _SiteID = Master.SiteID;
                ddlSites.Items.Clear();
                ddlSites.DataSource = _oMasters.Branchlist().Where(x => x.IsActive == true);
                ddlSites.DataTextField = "DisplayName";
                ddlSites.DataValueField = "ID";
                ddlSites.DataBind();
                ddlSites.Items.Insert(0, new ListItem("--Select Site--", "0"));
                ddlSites.SelectedValue = _SiteID.ToString();
                BindCategory();
               
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }


        void BindCategory()
        {
            if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                _SiteID = this.Master.SiteID;
            else
                _SiteID = Guid.Parse(ddlSites.SelectedValue);

            const int treeLevel = 3;
            var list = _oProduct.GetAssociateCategoryList(treeLevel, _SiteID);
            grvCategories.DataSource = list!=null ?list.OrderBy(x => x.Name).ToList():null;
            grvCategories.DataBind();

            Session["CatIds"] = list.Select(x => x.ID).ToList();
            ddlCategory.DataSource = list;
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("--Select Category--","0"));
            if (ViewState["CategoryIndx"] != null)
                ddlCategory.SelectedIndex = Convert.ToInt32(ViewState["CategoryIndx"].ToString());
           
        }
        protected void grvCategories_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCategories.PageIndex = e.NewPageIndex;            
            BindCategory();
        }


        void BindProductGrid()
        {
            if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                _SiteID = this.Master.SiteID;
            else
                _SiteID = Guid.Parse(ddlSites.SelectedValue);

            List<Guid> categoriesId = new List<Guid>();
            if (ddlCategory.SelectedIndex > 0)
                categoriesId = new List<Guid> { Guid.Parse(ddlCategory.SelectedValue) };
            else
            {
                if (Session["CatIds"] != null)
                    categoriesId = Session["CatIds"] as List<Guid>;
            }

            var list = _oProduct.GetProductListMarkUp(_SiteID, categoriesId);
            grvProduct.DataSource = list;
            grvProduct.DataBind();

            ddlProducts.DataSource = list;
            ddlProducts.DataTextField = "Name";
            ddlProducts.DataValueField = "ID";
            ddlProducts.DataBind();
            ddlProducts.Items.Insert(0, new ListItem("--Select Product--", "0"));
            Session["ProductIds"] = list.Select(x => x.ID).ToList();

            if (ViewState["ProductIndx"] != null)
                ddlProducts.SelectedIndex = Convert.ToInt32(ViewState["ProductIndx"].ToString()); 
        }
        protected void grvProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProduct.PageIndex = e.NewPageIndex;
            BindProductGrid();
        }

        void BindProductPriceGrid()
        {
            List<Guid> pids = new List<Guid>();
            if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                _SiteID = this.Master.SiteID;
            else
                _SiteID = Guid.Parse(ddlSites.SelectedValue);

            if (ddlProducts.SelectedIndex > 0)
                pids = new List<Guid> { Guid.Parse(ddlProducts.SelectedValue) };
            else
            {
                if (Session["ProductIds"] != null)
                    pids = Session["ProductIds"] as List<Guid>;
            }

            grvProductPrice.DataSource = _oProduct.GetProductsPricesWithProductNameById(pids, _SiteID);
            grvProductPrice.DataBind();
        }

        protected void grvProductPrice_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProductPrice.PageIndex = e.NewPageIndex;
            BindProductPriceGrid();
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender; 
                HiddenField hdnFlag = btnUpdate.Parent.FindControl("hdnFlag") as HiddenField;
                HiddenField hdnId = btnUpdate.Parent.FindControl("hdnId") as HiddenField;
                TextBox txtAmount = btnUpdate.Parent.FindControl("txtMarkUp") as TextBox;
                CheckBox chkPercent = btnUpdate.Parent.FindControl("chkIsPercent") as CheckBox;
                Guid Id = Guid.Parse(hdnId.Value);
                Int32 flag = Convert.ToInt32(hdnFlag.Value);
                Decimal markup = Convert.ToDecimal(txtAmount.Text);
                if (Guid.Parse(ddlSites.SelectedValue) == new Guid())
                    _SiteID = this.Master.SiteID;
                else
                    _SiteID = Guid.Parse(ddlSites.SelectedValue);

                _oProduct.UpdateMarkup(Id, markup, chkPercent.Checked, _SiteID, flag, AdminuserInfo.UserID);
                ShowMessage(1, "Markup update successfully.");
                BindCategory();
                BindProductGrid();
                BindProductPriceGrid();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }


        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["CategoryIndx"] = null;
            ShowMessage(0, null);
            if (ddlSites.SelectedIndex > 0)
                BindCategory();
            grvProduct.DataSource = null;  
            grvProduct.DataBind();
            grvProductPrice.DataSource = null;
            grvProductPrice.DataBind();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["CategoryIndx"] = ddlCategory.SelectedIndex;
            ViewState["ProductIndx"] = null;
            BindProductGrid();
            grvProductPrice.DataSource = null;
            grvProductPrice.DataBind();
        }

        protected void ddlProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["ProductIndx"] = ddlProducts.SelectedIndex;
            BindProductPriceGrid();
          
        }
        protected void grvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnId = e.Row.FindControl("hdnId") as HiddenField;
                Label lblHaveDifferentComm = e.Row.FindControl("lblHaveDifferentComm") as Label;
                if (hdnId != null && lblHaveDifferentComm != null)
                {
                    Boolean rval = false;
                    rval = _oProduct.CheckDiffMarkUpForCategory(Guid.Parse(hdnId.Value.Trim()), this.Master.SiteID);
                   
                    if (rval == true)
                        lblHaveDifferentComm.Text = "<a href='#' title='Child markup is different.'><img src='../images/info.png' alt='' width='25px' style='float:left;' /></a>";
                    else
                        lblHaveDifferentComm.Text = "";
                }
            }
        }
        protected void grvProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnId = e.Row.FindControl("hdnId") as HiddenField;
                HiddenField hdnCategoryID = e.Row.FindControl("hdnCategoryID") as HiddenField;
                Label lblHaveDifferentComm = e.Row.FindControl("lblHaveDifferentComm") as Label;
                if (hdnId != null && lblHaveDifferentComm != null)
                {
                    Boolean rval = false;
                    rval = _oProduct.CheckDiffMarkUpForProduct(Guid.Parse(hdnCategoryID.Value.Trim()), Guid.Parse(hdnId.Value.Trim()), this.Master.SiteID);
                    if (rval == true)
                        lblHaveDifferentComm.Text = "<a href='#' title='Child markup is different.'><img src='../images/info.png' alt='' width='25px' style='float:left;' /></a>";
                    else
                        lblHaveDifferentComm.Text = "";
                  
                }
            }
        }
    }
}