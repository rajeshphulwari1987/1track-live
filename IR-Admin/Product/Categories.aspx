﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Categories.aspx.cs" Inherits="IR_Admin.Product.Categories" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtTerm').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtEligibility').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtEligibilityforSaver').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtDiscount').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtValidity').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtGreatPass').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtareaAnnouncement').redactor({ iframe: true, minHeight: 200 });
        }
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            onload();

            $("#divCmsAncText").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsAncText").next().show();
            });

        });

        function onload() {
            $(".grid-sec2").find(".cat-outer-cms").hide();

            $("#divCmsAncText").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsAncText").next().show();
            });

            $("#divCmsProd").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsProd").next().show();
            });

            $("#divCmsElig").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsElig").next().show();
            });

            $("#divCmsEligforSaver").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsEligforSaver").next().show();
            });

            $("#divCmsValid").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsValid").next().show();
            });

            $("#divCmsDisc").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsDisc").next().show();
            });

            $("#divCmsTerm").click(function () {

                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsTerm").next().show();
            });

            $("#divCmsGreatPass").click(function () {

                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsGreatPass").next().show();
            });
        }
    </script>
    <script type="text/javascript">

        function DefltMiss(obj) {

            $("INPUT[type='radio']").each(function () {

                if (this.id != obj.children[0].id) {
                    this.checked = false;
                }
                else {
                    this.checked = true;
                }
            });

            return false;
        }     
        
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        .div-cat
        {
            float: left;
            width: 30%;
            border-bottom: 1px dashed #ccc;
            line-height: 37px;
        }
        .div-cat-left
        {
            float: left;
            width: 69%;
            border-bottom: 1px dashed #ccc;
            line-height: 37px;
        }
        #divCmsGreatPass
        {
            background: none repeat scroll 0 0 #fbdee6;
            border: 1px solid #b53859;
            cursor: pointer;
            float: left;
            font-weight: bold;
            margin: 5px 0 0;
            padding-bottom: 5px;
            padding-left: 5px;
            padding-top: 5px;
            width: 975px;
        }
          #divCmsAncText
        {
            width: 98%;
            cursor: pointer;
            margin: 1%;
            float: left;
            background: #FBDEE6;
            padding-left: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            margin: 5px 0 0 0;
            width: 100%;
            font-weight: bold;
            border: 1px solid #b53859;
        }
    </style>
    <script type="text/javascript">

        function IsPrinting(obj) {
            var holdchk = $(obj).prop("checked");
            $(".printing").each(function (key, value) {
                $(this).find("input[Type=checkbox]").prop('checked',false);
            });
            $(obj).prop("checked", holdchk);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                Product Category</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="CategoriesList.aspx" class="">List</a></li>
                    <li>
                        <asp:HyperLink ID="aNew" NavigateUrl="Categories.aspx" CssClass="current" runat="server">New/Edit</asp:HyperLink></li>
                </ul>
            </div>
            <!-- tab "panes" -->
            <div class="full mr-tp1">
                <div class="panes">
                    <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                        <table class="tblMainSection">
                            <tr>
                                <td style="width: 70%; vertical-align: top;">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <div class="div-cat">
                                                Category:</div>
                                            <div class="div-cat-left">
                                                <asp:DropDownList ID="ddlCategory" runat="server" Width="410px" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                                            </div>
                                            <div class="div-cat">
                                                Name:</div>
                                            <div class="div-cat-left">
                                                <asp:TextBox ID="txtCatName" runat="server" MaxLength="180" Text='<%#Eval("Name")%>'
                                                    Width="408px" />
                                                <asp:RequiredFieldValidator ID="reqCat" runat="server" ControlToValidate="txtCatName"
                                                    CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                            </div>
                                            <div class="div-cat">
                                                Language:
                                            </div>
                                            <div class="div-cat-left">
                                                <asp:DropDownList ID="ddlLanguage" runat="server" Enabled="False" Width="205px" />
                                            </div>
                                            <div class="div-cat">
                                                Default When Missing:</div>
                                            <div class="div-cat-left">
                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                    onchange="DefltMiss(this)" /></div>
                                            <div class="div-cat">
                                                Is Eurail Pass :</div>
                                            <div class="div-cat-left">
                                                <asp:CheckBox ID="chkEurail" runat="server" CssClass="printing" onclick="IsPrinting(this);" /></div>
                                            <div class="div-cat">
                                                Is Brit Rail Pass :</div>
                                            <div class="div-cat-left">
                                                <asp:CheckBox ID="chkBritRail" runat="server" CssClass="printing" onclick="IsPrinting(this);" /></div>
                                                <div class="div-cat">
                                                Is InterRail Pass :</div>
                                            <div class="div-cat-left">
                                                <asp:CheckBox ID="chkInterRail" runat="server" CssClass="printing" onclick="IsPrinting(this);" /></div>
                                            <div class="div-cat">
                                                Is FOC/AD75 Pass :</div>
                                            <div class="div-cat-left">
                                                <asp:CheckBox ID="chkFOCAD75" runat="server" /></div>
                                            <div class="div-cat">
                                                DOB visible :</div>
                                            <div class="div-cat-left">
                                                <asp:CheckBox ID="chkDOB" runat="server" /></div>
                                            <div class="div-cat">
                                                Is Active :</div>
                                            <div class="div-cat-left">
                                                <asp:CheckBox ID="chkIsActv" runat="server" Checked="True" /></div>
                                            <div class="div-cat">
                                                Upload Category Image :</div>
                                            <div class="div-cat-left">
                                                <asp:FileUpload ID="upCategoryImage" runat="server" Width="200px" />
                                                <asp:LinkButton ID="lnkcategoryImage" runat="server" Text="View Image" CssClass="clsLink"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="float: right; display: none;">
                                        <asp:Repeater ID="rptrCategories" runat="server" OnItemDataBound="rptrCategories_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="cat-inner">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Language:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="150px" />
                                                                <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Name:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtCatName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Default When Missing:
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                    onchange="DefltMiss(this)" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <div class="cat-inner-alt">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Language:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="150px" />
                                                                <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Name:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtCatName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Default When Missing:
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                    onchange="DefltMiss(this)" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                        <asp:Button ID="btnAddMore" runat="server" CssClass="button" Text="+ Add More" Width="89px"
                                            OnClick="btnAddMore_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="-- Remove" Width="89px"
                                            OnClick="btnRemove_Click" />
                                    </div>
                                    <div class="cat-outer" style="margin: .5%; position: relative;">
                                        <div id="divCmsAncText">
                                            Announcement text
                                        </div>
                                        <div class="cat-outer-cms">
                                            <asp:TextBox runat="server" ID="txtAnnouncement" Width="99%"></asp:TextBox>
                                            <textarea id="txtareaAnnouncement" runat="server"></textarea>
                                        </div>
                                        <div id="divCmsGreatPass" style="width: 100%">
                                            This pass is great because..
                                        </div>
                                        <div class="cat-outer-cms">
                                            <textarea id="txtGreatPass" runat="server"></textarea>
                                        </div>
                                        <div id="divCmsProd" style="width: 100%">
                                            Description
                                        </div>
                                        <div class="cat-outer-cms">
                                            <textarea id="txtDesc" runat="server"></textarea></div>
                                        <div id="divCmsElig" style="width: 100%">
                                            Eligibility
                                        </div>
                                        <div class="cat-outer-cms" s>
                                            <textarea id="txtEligibility" runat="server"></textarea>
                                        </div>
                                        <div id="divCmsEligforSaver">
                                            Eligibility for Saver
                                        </div>
                                        <div class="cat-outer-cms">
                                            <textarea id="txtEligibilityforSaver" runat="server"></textarea>
                                        </div>
                                        <div id="divCmsDisc" style="width: 100%">
                                            Discount
                                        </div>
                                        <div class="cat-outer-cms">
                                            <textarea id="txtDiscount" runat="server"></textarea>
                                        </div>
                                        <div id="divCmsValid" style="width: 100%">
                                            Validity
                                        </div>
                                        <div class="cat-outer-cms">
                                            <textarea id="txtValidity" runat="server"></textarea>
                                        </div>
                                        <div id="divCmsTerm" style="width: 100%">
                                            Terms & Condition
                                        </div>
                                        <div class="cat-outer-cms">
                                            <textarea id="txtTerm" runat="server"></textarea>
                                        </div>
                                    </div>
                    </div>
                    </td>
                    <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                        <b>&nbsp;Select Site</b>
                        <div style="width: 95%; height: 350px; overflow-y: auto;">
                            <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                            All
                            <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                <NodeStyle ChildNodesPadding="5px" />
                            </asp:TreeView>
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="submit" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </td>
                        <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                        </td>
                    </tr>
                    </table>
                </div>
            </div>
            </div>
            <asp:Panel ID="pnlCategoryImage" runat="server" CssClass="popup pBannerImg">
                <div class="dvPopupHead">
                    <div class="dvPopupTxt">
                        Category Image</div>
                    <a href="#" id="btnClose3" runat="server" style="color: #fff;">X </a>
                </div>
                <div class="dvImg">
                    <asp:Image ID="imgCategoryImage" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg" />
                    <br />
                    <br />
                    <asp:Button ID="btnRemoveImage" Text="Remove" runat="server" CssClass="button" OnClick="btnRemoveImage_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mdpupBanner" runat="server" TargetControlID="lnkcategoryImage"
                CancelControlID="btnClose3" PopupControlID="pnlCategoryImage" BackgroundCssClass="modalBackground" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
