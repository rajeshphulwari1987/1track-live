﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin.Product
{
    public partial class ProductCategoryURL : System.Web.UI.Page
    {
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
        public string siteURL = "";
        #region [ Page InIt must write on every page of CMS ]
       
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(AdminuserInfo.SiteID);
            BindCategory();
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                siteURL = _oWebsitePage.GetSiteURLbySiteId(AdminuserInfo.SiteID);
                BindCategory();
            }
        }
        void BindCategory()
        { 
            const int treeLevel = 3;
            grvProductCategory.DataSource = _oProduct.GetAssociateCategoryList(treeLevel, AdminuserInfo.SiteID);
            grvProductCategory.DataBind();

        }

    }
}