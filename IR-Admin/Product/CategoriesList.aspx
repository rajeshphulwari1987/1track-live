﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CategoriesList.aspx.cs" Inherits="IR_Admin.Product.CategoriesList" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });


    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 682px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                Product Category List</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="CategoriesList.aspx" class="current">List</a></li>
                    <li style="font-size: 14px;">
                        <asp:HyperLink ID="aNew" NavigateUrl="Categories.aspx" CssClass="" runat="server">New/Edit</asp:HyperLink>
                    </li>
                </ul>
                <br />
            </div>
            <!-- tab "panes" -->
            <div class="full mr-tp1" style="margin-top: 0px">
                <div class="panes">
                    <div id="divNew" runat="server" style="display: block;">
                        <asp:HiddenField runat="server" ID="hdnPath" />
                        <asp:Repeater ID="rptCatoriesList" runat="server" OnItemCommand="rptCatoriesList_ItemCommand"
                            OnItemDataBound="rptCatoriesList_ItemDataBound">
                            <HeaderTemplate>
                                <table style="width: 100%; font-weight: bold;" class="grid-head2">
                                    <tr>
                                        <td style="width: 650px">
                                            Category Name
                                        </td>
                                        <td style="width: 200px">
                                            Site's Name
                                        </td>
                                        <td style="width: 10%">
                                            Action
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <AlternatingItemTemplate>
                                <div class="cat-inner-alt" style="margin: 0px; width: 100%">
                                    <div style="width: 650px; float: left; padding-left: 1%">
                                        <asp:LinkButton ID="lnkPId" runat="server" CssClass="lnkcat" CommandArgument='<%#Eval("Id")%>'
                                            CommandName="FINDSUBCATEGORY" Text='<%#Eval("Name")%>' /></div>
                                    <div style="width: 200px; float: left; padding-left: 1%">
                                        <asp:Literal ID="ltrSite" runat="server" Text='<%#Eval("SiteName")%>' /></div>
                                    <div style="width: 10%; float: left; text-align: center">
                                        <div style="width: 18px; float: left; text-align: center">
                                            <a href="Categories.aspx?id=<%#Eval("Id")%>" title="Edit">
                                                <img alt="edit" src="../images/edit.png" />
                                            </a>
                                        </div>
                                        <div style="width: 18px; float: left; text-align: center">
                                            <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                                CommandArgument='<%#Eval("Id")%>' CommandName="Delete" ImageUrl="../images/delete.png"
                                                OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        </div>
                                        <div style="width: 18px; float: left; text-align: center">
                                            <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                                Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        </div>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                            <ItemTemplate>
                                <div class="cat-inner" style="margin: 0px; width: 100%">
                                    <div style="width: 650px; float: left; padding-left: 1%">
                                        <asp:LinkButton ID="lnkPId" runat="server" CssClass="lnkcat" CommandArgument='<%#Eval("Id")%>'
                                            CommandName="FINDSUBCATEGORY" Text='<%#Eval("Name")%>' /></div>
                                    <div style="width: 200px; float: left; padding-left: 1%">
                                        <asp:Literal ID="ltrSite" runat="server" Text='<%#Eval("SiteName")%>' /></div>
                                    <div style="width: 10%; float: left; text-align: center">
                                        <div style="width: 18px; float: left; text-align: center">
                                            <a href="Categories.aspx?id=<%#Eval("Id")%>" title="Edit">
                                                <img alt="edit" src="../images/edit.png" />
                                            </a>
                                        </div>
                                        <div style="width: 18px; float: left; text-align: center">
                                            <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                                CommandArgument='<%#Eval("Id")%>' CommandName="Delete" ImageUrl="../images/delete.png"
                                                OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        </div>
                                        <div style="width: 18px; float: left; text-align: center">
                                            <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                                Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                          <div id="divRecordNotfnd" runat="server" class="warning" style="display: block;">
                     
                     Record not found.
                     </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
