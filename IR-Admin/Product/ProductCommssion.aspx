﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductCommssion.aspx.cs" Inherits="IR_Admin.Product.ProductCommssion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
    .cms
    {
        color:Black!important;
        display:inline;
        margin-left:50px;
        }
    </style>
    <script type="text/javascript">
        function ResetDiv() {
                       document.getElementById('aList').className = 'current';
                        document.getElementById('MainContent_divlist').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        function editOffice(id) {
            document.getElementById("<%= hdnBranchid.ClientID %>").value = id;
            __doPostBack("<%= btneditOffice.UniqueID %>", "");
        }
        function getConfirm() {
            return confirm('Are you sure you want to delete this record?');
        }

    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btneditOffice" runat="server" OnClick="btneditOffice_Click" CssClass="hidebtn" />
    <h2>
        Commission</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ProductCommssion.aspx" class="current">List</a></li>
            <li></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <asp:TreeView ID="trBranch" runat="server" OnSelectedNodeChanged="trBranch_SelectedNodeChanged"
                        ShowLines="True">
                        <LeafNodeStyle CssClass="leafNode" />
                        <NodeStyle CssClass="treeNode" />
                        <RootNodeStyle CssClass="rootNode" />
                        <SelectedNodeStyle CssClass="selectNode" />
                        <DataBindings>
                            <asp:TreeNodeBinding TextField="Text" DataMember="System.Data.DataRowView" ValueField="ID" />
                        </DataBindings>
                    </asp:TreeView>
                </div>
            </div>
        </div>
    </div>
     <asp:HiddenField ID="hdnBranchid" runat="server" />
    <div style="display: none;">
        <asp:LinkButton ID="lnkPopup" runat="server" Text="lnk" />
         <asp:LinkButton ID="lnkNew" runat="server" Text="New" CssClass="lnkNew" />
    </div>
    <div id="pnlStockAdd" style="display: none; width: 982px;">
        <div class="heading">
            Add/Edit Commission
        </div>
        <div class="divMain" style="margin-top: -2px;">
            <div class="divleft">
                Commission :</div>
            <div class="divright">
                <asp:TextBox runat="server" ID="txtCommission" MaxLength="5" />%
                <asp:RequiredFieldValidator ID="reqStockFrom" runat="server" ErrorMessage="*" CssClass="valdreq"
                    ControlToValidate="txtCommission" ValidationGroup="submit" />
                <asp:FilteredTextBoxExtender ID="ftbStockFrom" TargetControlID="txtCommission" ValidChars="0123456789."
                    runat="server" />
            </div>
            <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                .
            </div>
            <div class="divrightbtn" style="padding-top: 10px;">
                <asp:Button ID="btnSubmit" runat="server" CssClass="button1" Text="Save & Allocated To Child" OnClick="btnSubmit_Click"
                    ValidationGroup="submit" />
                &nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button1" OnClick="btnCancel_Click"
                    Text="Cancel" />
                <div class="clear">
                </div>
                <span>
                    <asp:Label ID="lblMessage" ForeColor="green" runat="server" /></span>
            </div>
        </div>
    </div>
    <asp:ModalPopupExtender ID="mdpupStock" runat="server" TargetControlID="lnkNew" CancelControlID="btnCancel"
        PopupControlID="pnlStockAdd" BackgroundCssClass="modalBackground" />
</asp:Content>
