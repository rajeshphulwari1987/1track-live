﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductsShipping.aspx.cs" Inherits="IR_Admin.Product.ProductsShipping" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        .align {
            text-align: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>Products Shipping</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" />
                </div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <!-- tab "panes" -->
            <div class="full mr-tp1">
                <div class="panes">
                    <div id="divlist" runat="server">
                        <div class="crushGvDiv" style="font-size: 13px;">
                            <table class="tblMainSection" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: right;" class="valdreq">Fields marked with * are mandatory
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <fieldset class="grid-sec2">
                                            <legend><b>Site </b></legend>
                                            <div class="cat-inner-alt" style="text-align: center; width: 100%; border-bottom: 2px dashed #FBDEE6;">
                                                <asp:DropDownList runat="server" ID="ddlSites" AutoPostBack="True" Width="500px"
                                                    OnSelectedIndexChanged="ddlSites_SelectedIndexChanged" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                    ValidationGroup="submit" InitialValue="0" ControlToValidate="ddlSites" CssClass="valdreq"
                                                    SetFocusOnError="True" />
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <div>
                                            <fieldset class="grid-sec2">
                                                <legend><b>Product </b></legend>
                                                <div class="searchDiv" style="text-align: center">
                                                    <asp:DropDownList ID="ddlCategory" runat="server" Width="510px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                                                </div>
                                                <asp:GridView ID="grvProduct" runat="server" AutoGenerateColumns="False" PageSize="10"
                                                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                                    OnPageIndexChanging="grvProduct_PageIndexChanging" AllowPaging="True">
                                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                                    <PagerStyle CssClass="paging"></PagerStyle>
                                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        Record not found.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <%#Eval("ProductName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="35%"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Category Name">
                                                            <ItemTemplate>
                                                                <%#Eval("CategoryName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="25%"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last edited">
                                                            <ItemTemplate>
                                                                <div style="padding: 0px !important; margin: 0px; line-height: 16px;">
                                                                    <%#Eval("ModifyName")%>
                                                                </div>
                                                                <div style="padding: 0px !important; margin: 0px; line-height: 16px;">
                                                                    <%#Eval("ModifyOn", "{0:dd MMM yyyy HH:mm}")%>
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="15%" HorizontalAlign="Center" CssClass="align" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Amount">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtDiscountPrice" runat="server" Text='<%#Eval("DiscountPrice")%>' Width="50px" Style="float: left;" />
                                                                <asp:FilteredTextBoxExtender ID="ftbDiscount" runat="server" TargetControlID="txtDiscountPrice" ValidChars="0123456789." />
                                                                <asp:RequiredFieldValidator ID="reqDiscount" runat="server" ControlToValidate="txtDiscountPrice" ErrorMessage="*" Display="Dynamic" ValidationGroup="vg" CssClass="valdreq" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Shipping(%)">
                                                            <ItemTemplate>
                                                                <asp:CheckBox runat="server" ID="chkIsPercent" Checked='<%#Eval("IsPercent")%>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnFlag" runat="server" Value="2" />
                                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ProductId")%>' />
                                                                <asp:Button ID="btnUpdate" ValidationGroup="vg" runat="server" Text="Update" CommandName="UpdateDiscount" OnClick="btnUpdate_Click" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </fieldset>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
