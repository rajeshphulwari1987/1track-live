﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="EurailCountry.aspx.cs" Inherits="IR_Admin.EurailCountry" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
             if(<%=tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }
        });   
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
     <style type="text/css">
     #MainContent_upnl
     {
      float:left;   
     }
     #MainContent_upnl .divright
     {
         width:694px;
     }
     
     </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Eurail Country</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="EurailCountry.aspx" class="current">List</a></li>
            <li><a id="aNew" href="EurailCountry.aspx" class="current">New/Edit</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grvEurailCountry" runat="server" AutoGenerateColumns="False" PageSize="50"
                            AllowPaging="True" CssClass="grid-head2" CellPadding="4" ForeColor="#333333"
                            GridLines="None" Width="100%" OnPageIndexChanging="grvEurailCountry_PageIndexChanging"
                            OnRowCommand="grvEurailCountry_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Country Code">
                                    <ItemTemplate>
                                        <%#Eval("CountryCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country1">
                                    <ItemTemplate>
                                        <%#Eval("Country1")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country2">
                                    <ItemTemplate>
                                        <%#Eval("Country2")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country3">
                                    <ItemTemplate>
                                        <%#Eval("Country3")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country4">
                                    <ItemTemplate>
                                        <%#Eval("Country4")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country5">
                                    <ItemTemplate>
                                        <%#Eval("Country5")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price Band">
                                    <ItemTemplate>
                                        <%#Eval("Level")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Other Country">
                                    <ItemTemplate>
                                        <%#Eval("OtherCountry")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField runat="server" ID="hdnId" />
                    </div>
                </div>
                <div id="divNew"   runat="server" style="display: Block;">
                    <div class="divMain">
                        <asp:UpdatePanel runat="server" ID="upnl" style="float:left;">
                            <ContentTemplate>
                                <div class="divleft">
                                    Country Code:
                                </div>
                                <div class="divright">
                                    <asp:TextBox ID="txtCountryCode" runat="server" MaxLength="7" 
                                        AutoPostBack="True" ontextchanged="txtCountryCode_TextChanged" />
                                    <asp:FilteredTextBoxExtender ID="ftbCountryCode" TargetControlID="txtCountryCode"
                                        ValidChars="0123456789" runat="server" />
                                    &nbsp;<asp:RequiredFieldValidator ID="reqCountryCode" runat="server" ControlToValidate="txtCountryCode"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                                </div>
                                <div class="divleft" id="dvcl1" runat="server" visible="false">
                                    Price Band:
                                </div>
                                <div class="divright" id="dvcl2" runat="server" visible="false">
                                    <asp:DropDownList runat="server" ID="ddllevel">
                                    </asp:DropDownList>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="divleft">
                            Country 1:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCountry1" runat="server" MaxLength="200" />
                            &nbsp;<asp:RequiredFieldValidator ID="reqCountry1" runat="server" ControlToValidate="txtCountry1"
                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                        </div>
                        <div class="divleft">
                            Country 2:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCountry2" runat="server" MaxLength="200" />
                        </div>
                        <div class="divleft">
                            Country 3:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCountry3" runat="server" MaxLength="200" />
                        </div>
                        <div class="divleft">
                            Country 4:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCountry4" runat="server" MaxLength="200" />
                        </div>
                        <div class="divleft">
                            Country 5:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCountry5" runat="server" MaxLength="200" />
                        </div>
                        
                        <div class="divleft">
                            Other Country?
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtOtherCountry" runat="server"  MaxLength="500"/> <span style="color: Red">Note: Please enter ',' comma seprated value</span>
                        </div>
                        <div class="divleft">
                            Is Active?
                        </div>
                        <div class="divright">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="divleftbtn">
                            .
                        </div>
                        <div class="divrightbtn">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="vg" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
