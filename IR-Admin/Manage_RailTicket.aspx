﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_RailTicket.aspx.cs"
    Inherits="IR_Admin.Manage_RailTicket" %>

<%@ Register Src="usercontrol/CustomBannerImageManager.ascx" TagName="CustomBannerImageManager"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>International Rail</title>
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:400" rel="stylesheet"
        type="text/css" async="" />
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet"
        type="text/css" async="" />
    <link href="Styles/ircss/css/bootstrap.min.css" rel="stylesheet">
    <link href="Styles/ircss/css/font-awesome.min.css" rel="stylesheet">
    <link href="Styles/ircss/css/internationalrail.css" rel="stylesheet">
    <link href="Styles/ircss/css/layout.css" rel="stylesheet">
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <style type="text/css">
        .railsectionbg
        {
            background-color: #eee;
            border: solid 1px #ccc;
            position: relative;
            top: -160px;
            margin-bottom: -160px;
            margin-left: 10px;
        }
        
        .railsectionbg label
        {
            font-size: 15px;
            clear: both;
            width: 100%;
        }
        
        .railsectionbg
        {
            padding: 8px 15px;
        }
        
        .railsectionbg input
        {
            width: 100%;
            padding: 6px 10px;
        }
        
        .railselectformblk
        {
            margin-bottom: 10px;
        }
        .linknavblk00
        {
            margin: 0px;
            display: inline-block;
            width: 100%;
        }
        
        .linknavblk00 table label
        {
            padding-bottom: 5px;
            padding-top: 5px;
        }
        a.onewaylink
        {
            text-align: center;
            background-color: #8e3030;
            color: #fff;
            padding: 5px;
            margin-top: 0px;
            width: 98%;
            display: block;
        }
        
        a.returnlink
        {
            background-color: #ccc;
            color: #fff;
            padding: 5px;
            margin-top: 0px;
            text-align: center;
            width: 98%;
            display: block;
        }
        
        .railselectformblk a:hover
        {
            text-decoration: none;
        }
        
        .whoisgoingblk
        {
            border: solid 1px #ccc;
        }
        
        .whoisgoingblk h4
        {
            background-color: #8e3030;
            padding: 8px 10px;
            color: #fff;
            font-weight: 400;
            text-transform: none;
        }
        
        .whoisblk
        {
            padding: 0 10px;
        }
        
        .whoisblk p
        {
            font-size: 12px;
            width: 101%;
        }
        
        .whoisblk select
        {
            width: 100%;
            padding: 4px 7px;
        }
        
        
        .whoisbottom
        {
            clear: both;
        }
        
        
        .contenttitleblk h4
        {
            background-color: #8e3030;
            padding: 10px;
            color: #fff;
            margin-bottom: 10px;
        }
        
        .contenttitleblk
        {
            padding: 17px 0;
        }
        
        .whoblk
        {
            padding: 0;
            margin-top: 10px;
            line-height: 10px;
        }
        
        .whoblk p
        {
            margin-top: 9px;
        }
        
        .railsectionbg select
        {
            height: 32px;
            padding: 5px 7px;
        }
        
        .ui-datepicker-calendar
        {
            margin-left: 4px;
        }
        .mintext
        {
            font-size: 12px;
            display: inline;
        }
        #_bindDivData
        {
            width: 57%;
        }
        .pnlpopup
        {
            z-index: 111 !important;
            position: fixed !important;
            left: 23%;
            top: 25%;
        }
        .modalBackgroundnew
        {
            position: fixed !important;
            left: 0px !important;
            top: 0px !important;
            z-index: 110 !important;
            width: 100% !important;
            height: 100% !important;
        }
        .pnl-pop .starail-YourBooking-table
        {
            display: inline-block;
            max-height: 250px;
            overflow-x: auto;
        }
        .starail-Form-datePicker i
        {
            right: 25px;
            top: 8px;
        }
        .banner90 h1
        {
            color: #fff !important;
            font-size: 23px;
        }
        
        .linknavblk00 label
        {
            position: relative;
            z-index: 2;
        }
        
        .linknavblk00 label
        {
            padding-bottom: 0;
        }
        .inner-banner
        {
            height: 190px;
            width: 900px;
        }
        .starail-Form-row h2
        {
            font-size: 20px;
            text-transform: uppercase;
            color: #8e3030;
        }
    </style>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
        
        .editbtn1
        {
            top: -28px;
            left: -28px;
        }
        
        .editbtn2
        {
            top: -16px;
            right: 441px;
        }
        
        .editbtn3
        {
            bottom: 0px;
            left: 0px;
        }
        
        .editbtn4
        {
            bottom: 44px;
            right: 804px;
        }
        
        .editbtn5
        {
            top: -29px;
            left: 354px;
        }
        
        .editbtn6
        {
            top: -77px;
            left: 0px;
        }
        
        .editbtn7
        {
            top: -30px;
            left: 3px;
        }
    </style>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtContent1').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent2').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent3').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent4').redactor({ iframe: true, minHeight: 200 });

            //Edit banner images
            $(".editbtn2").click(function () {
                $("#ContentBanner1").slideToggle("slow");
            });
            $(".editbtn3").click(function () {
                $("#ContentBanner2").slideToggle("slow");
            });
            $(".editbtn5").click(function () {
                $("#ContentBanner3").slideToggle("slow");
            });
            $("#btnUploadBanner").click(function () {   //open upload banner div 
                $("#divUploadBanner").slideToggle("slow");
            });
            //edit heading
            $(".editbtn1").click(function () {
                $("#divContent1").slideToggle("slow");
            });
            $(".editbtn4").click(function () {
                $("#divContent2").slideToggle("slow");
            });
            $(".editbtn6").click(function () {
                $("#divContent3").slideToggle("slow");
            });
            $(".editbtn7").click(function () {
                $("#divContent4").slideToggle("slow");
            });

            //----------Edit heading---------//
            $(".editbtn1").click(function () {
                var value;
                $("#divContent1").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdContent1').contents().find('body').html(value);
            });
            $(".editbtn4").click(function () {
                var value;
                $("#divContent2").slideDown("slow");
                value = $('#ContentValue2').html();
                $('.redactor_rdContent2').contents().find('body').html(value);
            });
            $(".editbtn6").click(function () {
                var value;
                $("#divContent3").slideDown("slow");
                value = $('#ContentValue3').html();
                $('.redactor_rdContent3').contents().find('body').html(value);
            });
            $(".editbtn7").click(function () {
                var value;
                $("#divContent4").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent4').contents().find('body').html(value);
            });

            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner1") {
                    $("#ContentBanner1").hide();
                }
                else if ($(this).attr("rel") == "ContentBanner2") {
                    $("#ContentBanner2").hide();
                }
                else if ($(this).attr("rel") == "ContentBanner3") {
                    $("#ContentBanner3").hide();
                }
                else if ($(this).attr("rel") == "divcloseBanner") {  //close upload banner div 
                    $("#divUploadBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent1") {
                    $("#divContent1").hide();
                }
                else if ($(this).attr("rel") == "divContent2") {
                    $("#divContent2").hide();
                }
                else if ($(this).attr("rel") == "divContent3") {
                    $("#divContent3").hide();
                }
                else if ($(this).attr("rel") == "divContent4") {
                    $("#divContent4").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner1").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner1 input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs1").val("0");
                } else {
                    $("#hdnBannerIDs1").val(myIds);
                }
                $("#ContentBanner1").hide();
            });

            $(".btnSaveBanner2").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner2 input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs2").val("0");
                } else {
                    $("#hdnBannerIDs2").val(myIds);
                }
                $("#ContentBanner2").hide();
            });

            $(".btnSaveBanner3").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner3 input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner3").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divContent1") {
                    value = $('textarea[name=txtContent1]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divContent1").hide();
                }
                else if ($(this).attr("rel") == "divContent2") {
                    value = $('textarea[name=txtContent2]').val();
                    if (value != "")
                        $('#ContentValue2').html(value);
                    $("#divContent2").hide();
                }
                else if ($(this).attr("rel") == "divContent3") {
                    value = $('textarea[name=txtContent3]').val();
                    if (value != "")
                        $('#ContentValue3').html(value);
                    $("#divContent3").hide();
                }
                else if ($(this).attr("rel") == "divContent4") {
                    value = $('textarea[name=txtContent4]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent4").hide();
                }
            });

            $('.checkvalue').on('change', function () {
                $('.checkvalue').not(this).prop('checked', false);
            });
            $('.innercheckvalue').on('change', function () {
                $('.innercheckvalue').not(this).prop('checked', false);
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs1" runat="server" />
    <asp:HiddenField ID="hdnBannerIDs2" runat="server" />
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <div class="page-wrap">
        <div class="navbarbg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img src="Styles/ircss/images/top.jpg" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="Styles/ircss/images/logo-panel.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="navouter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                        <div class="navouter bdrbtm">
                            <div class="pull-right pos-rel agent_login">
                                <i class="fa fa-user fa-2x search "></i><a href="#">Agent Login</a>
                            </div>
                            <div class="clearfix visible-xs">
                            </div>
                            <div class=" collapse navbar-collapse pull-left">
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="active"><span></span>HOME </a></li>
                                    <li><a href="#"><span></span>RAIL PASSES </a></li>
                                    <li><a href="#"><span></span>RAIL TICKETS </a></li>
                                    <li><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                        id="dropdownMenu2"><span></span>COUNTRY </a></li>
                                    <li class="dropdown dropdown-large "><a href="#"><span></span>SPECIAL TRAINS </a>
                                        <ul class="dropdown-menu dropdown-menu-large row">
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Glyphicons</li>
                                                    <li><a href="#">Available glyphs</a></li>
                                                    <li class="disabled"><a href="#">How to use</a></li>
                                                    <li><a href="#">Examples</a></li>
                                                    <li><a href="#">Example</a></li>
                                                    <li><a href="#">Aligninment options</a></li>
                                                    <li><a href="#">Headers</a></li>
                                                    <li><a href="#">Disabled menu items</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Button groups</li>
                                                    <li><a href="#">Basic example</a></li>
                                                    <li><a href="#">Button toolbar</a></li>
                                                    <li><a href="#">Sizing</a></li>
                                                    <li><a href="#">Nesting</a></li>
                                                    <li><a href="#">Vertical variation</a></li>
                                                    <li><a href="#">Single button dropdowns</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Input groups</li>
                                                    <li><a href="#">Basic example</a></li>
                                                    <li><a href="#">Sizing</a></li>
                                                    <li><a href="#">Checkboxes and radio addons</a></li>
                                                    <li><a href="#">Tabs</a></li>
                                                    <li><a href="#">Pills</a></li>
                                                    <li><a href="#">Justified</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Navbar</li>
                                                    <li><a href="#">Default navbar</a></li>
                                                    <li><a href="#">Buttons</a></li>
                                                    <li><a href="#">Text</a></li>
                                                    <li><a href="#">Non-nav links</a></li>
                                                    <li><a href="#">Component alignment</a></li>
                                                    <li><a href="#">Fixed to top</a></li>
                                                    <li><a href="#">Fixed to bottom</a></li>
                                                    <li><a href="#">Static top</a></li>
                                                    <li><a href="#">Inverted navbar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><span></span>CONTACT US </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="DivLeftOne" runat="server">
                <%--<div class="innner-banner">
                 <div class="bannerLft">
                        <div class="banner90">
                            <h1 id="ContentHead" runat="server">
                                Rail Tickets</h1>
                        </div>
                        <div class="editbaner">
                            <div class="editbtn1">
                                <a href="#" class="edit-btn">Edit</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="float-lt" style="width: 73%">
                            <asp:Image ID="imgBannerLeft" CssClass="scale-with-grid" alt="" border="0" runat="server"
                                Width="901px" Height="190px" ImageUrl="images/train-ticket-left-banner.jpg" /></div>
                        <div class="editbaner">
                            <div class="editbtn2">
                                <a href="#" class="edit-btn">Edit</a></div>
                        </div>
                        <div class="float-rt" style="width: 27%; text-align: right;">
                            <asp:Image ID="imgBannerRight" CssClass="scale-with-grid" alt="" border="0" runat="server"
                                Width="100%" Height="190px" ImageUrl="images/train-ticket-right-banner.jpg" />
                            <div class="editbaner">
                                <div class="editbtn3">
                                    <a href="#" class="edit-btn">Edit</a></div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <div style="clear: both;">
                </div>
                <div class="starail-BookingDetails-form">
                    <div class="starail-Form-row">
                        <h1 id="ContentValue2" runat="server">
                            Travel selection
                        </h1>
                        <div class="editbaner">
                            <div class="editbtn4">
                                <a href="#" class="edit-btn">Edit</a></div>
                        </div>
                    </div>
                    <div class="trainbanner01">
                        <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/train-ticket-inner-banner.jpg"
                            CssClass="scale-with-grid inner-banner" Style="width: 900px;" />
                        <div class="editbaner">
                            <div class="editbtn5">
                                <a href="#" class="edit-btn">Edit</a></div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="col-sm-12">
                        <div class="row railselection-ar">
                            <div class="col-sm-5">
                                <div class="railsectionbg">
                                    <img src="images/train-ticket-journey.png" />
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="contenttitleblk">
                                        <h4 id="ContentValue3" runat="server">
                                            Content Title
                                        </h4>
                                        <div class="editbaner">
                                            <div class="editbtn6">
                                                <a href="#" class="edit-btn">Edit</a></div>
                                        </div>
                                        <p id="ContentText" runat="server">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In imperdiet ligula ex,
                                            non sagittis augue egestas id. Praesent viverra turpis lectus, id accumsan nisl
                                            mattis vel. Phasellus rhoncus volutpat sollicitudin. Nulla vehicula iaculis condimentum.
                                            Phasellus rutrum mauris id dapibus maximus. Phasellus a tempor metus. Etiam odio
                                            lectus, pharetra vitae lorem eget, iaculis venenatis augue. Nam in efficitur enim.
                                        </p>
                                        <div class="editbaner">
                                            <div class="editbtn7">
                                                <a href="#" class="edit-btn">Edit</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerwraper">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Destinations</h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/australia">Australia</a></li>
                                <li><a href="https://www.internationalrail.com/italy">Italy</a></li>
                                <li><a href="https://www.internationalrail.com/japan">Japan</a></li>
                                <li><a href="https://www.internationalrail.com/new-zealand">New Zealand</a></li>
                                <li><a href="https://www.internationalrail.com/united-states">USA</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Rail Companies
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/eurostar">Eurostar</a></li>
                                <li><a href="https://www.internationalrail.com/thalys">Thalys</a></li>
                                <li><a href="https://www.internationalrail.com/thello">Thello</a></li>
                                <li><a href="https://www.internationalrail.com/trenitalia">Trenitalia</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Travel the world
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/Balkan-FlexiPass-(EUR)/balkan-flexipass">
                                    Balkan Flexi-Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Japanese-Rail-Passes/japan-rail-pass">
                                    Japan Rail Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Korean-Rail-Passes/korea-rail-pass">Korea
                                    Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Spanish-Rail-Passes/renfe-spain-pass">
                                    Spain Pass</a></li>
                                <li><a href="https://www.internationalrail.com/USA-Rail-Passes/usa-rail-pass">USA Rail
                                    Pass</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Legal information
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/contact-us">Contact us</a> </li>
                                <li><a href="https://www.internationalrail.com/conditions-of-use">Conditions of Use</a></li>
                                <li><a href="https://www.internationalrail.com/Cookies">Cookies</a></li>
                                <li><a href="https://www.internationalrail.com/Booking-Conditions">General terms and
                                    conditions</a></li>
                                <li><a href="https://www.internationalrail.com/privacy-policy">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                            <ul>
                                <li><a href="https://www.internationalrail.com/">Home </a></li>
                                <li><a href="https://www.internationalrail.com/Contact-Us">Contact Us </a></li>
                                <li><a href="https://www.internationalrail.com/About-Us">About Us </a></li>
                                <li><a href="https://www.internationalrail.com/Booking-Conditions">Booking Conditions
                                </a></li>
                                <li><a href="https://www.internationalrail.com/Privacy-Policy">Privacy Policy </a>
                                </li>
                                <li><a href="https://www.internationalrail.com/Conditions-of-Use">Conditions of Use
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                            <p class="copyright">
                                © Copyright International Rail Ltd. 2015 - All rights reserved. A company registered
                                in England and Wales, company number: 3060803 with registered offices at International
                                Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire.
                                SO53 4AR.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center social_link">
                            <a href="#"><i class=" fa fa-facebook-square fa-2x"></i></a><a href="#"><i class="fa fa-twitter-square fa-2x">
                            </i></a><a href="#"><i class=" fa fa-rss-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--Banner Div--%>
    <div id="ContentBanner1" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 203px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner1" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner1_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server"
                        class="checkvalue" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="btnCancelBanner1" value="Cancel" class="button btnClose"
                rel="ContentBanner1" />
            <input type="button" id="btnSaveBanner1" value="Save" class="button btnSaveBanner1"
                rel="ContentBanner1" />
        </div>
    </div>
    <div id="ContentBanner2" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 203px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner2" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner2_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server"
                        class="innercheckvalue" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="btnUploadBanner" value="Upload Banner" class="button" />
            <input type="button" id="btnCancelBanner2" value="Cancel" class="button btnClose"
                rel="ContentBanner2" />
            <input type="button" id="btnSaveBanner2" value="Save" class="button btnSaveBanner2"
                rel="ContentBanner2" />
        </div>
    </div>
    <div id="ContentBanner3" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 203px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Rail tickets page banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner3" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner3_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="btnCancelBanner3" value="Cancel" class="button btnClose"
                rel="ContentBanner3" />
            <input type="button" id="btnSaveBanner3" value="Save" class="button btnSaveBanner3"
                rel="ContentBanner3" />
        </div>
    </div>
    <div id="divUploadBanner" class="PopUpSample" style="display: none; width: 850px;
        height: 450px; left: 40px; top: 203px; position: fixed; z-index: 99999 !important;">
        <uc1:CustomBannerImageManager ID="CustomBannerImageManager" runat="server" />
    </div>
    <%--Text Div--%>
    <div class="PopUpSample" id="divContent1" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Heading Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent1" name="txtContent1" cols="10" rows="5" class="rdContent1"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc1" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnCancel1" value="Cancel" class="button btnClose" rel="divContent1" />
                    <input type="button" id="btnSave1" value="Save" class="button btnsave" rel="divContent1" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent2" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Heading Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent2" name="txtContent2" cols="10" rows="5" class="rdContent2"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc2" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnCancel2" value="Cancel" class="button btnClose" rel="divContent2" />
                    <input type="button" id="btnSave2" value="Save" class="button btnsave" rel="divContent2" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent3" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Heading Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent3" name="txtContent3" cols="10" rows="5" class="rdContent3"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc3" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnCancel3" value="Cancel" class="button btnClose" rel="divContent3" />
                    <input type="button" id="btnSave3" value="Save" class="button btnsave" rel="divContent3" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent4" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Paragraph Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent4" name="txtContent4" cols="10" rows="5" class="rdContent4"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc4" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnCancel4" value="Cancel" class="button btnClose" rel="divContent4" />
                    <input type="button" id="btnSave4" value="Save" class="button btnsave" rel="divContent4" />
                </td>
            </tr>
        </table>
    </div>
    <script src="Styles/ircss/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            },
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            });
        });
    </script>
    </form>
</body>
</html>
