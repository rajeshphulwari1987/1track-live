﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class OrderDetails : Page
    {
        readonly ManageBooking _masterBooking = new ManageBooking();
        readonly ManageAffiliateUser _masterAffiliate = new ManageAffiliateUser();
        Guid siteId;
        public Guid? CurrencyID;
        public string CurrCode = "";
        public static string unavailableDates1 = "";
        public string script;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
            BindCurrency();
            FillOffice();
            FillCommition();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "isAgent", "isAgent();", true);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            BindCurrency();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal", "LoadCal();", true);
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);

            if (!IsPostBack)
            {
                unavailableDates1 = "[";
                if (siteDDates.Count() > 0)
                {
                    foreach (var it in siteDDates)
                    {
                        unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
                }
                unavailableDates1 += "]";
                tblAdminUser objA = _masterBooking.GetAdminUserbyId(AdminuserInfo.UserID);
                if (objA.MenualP2PIsActive == true)
                {
                    pnlP2PMsg.Visible = false;
                    //pnlOrderAdd.Visible = true;
                }
                else
                {
                    pnlP2PMsg.Visible = true;
                    //pnlOrderAdd.Visible = false;
                }
                Session["P2PBookings"] = null;

                //for Senior
                for (int j = 99; j >= 0; j--)
                {
                    ddlChildren.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlSeniors.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                }

                for (int k = 0; k <= 59; k++)
                {
                    ddlRetSec.Items.Add((k < 10 ? "0" : "") + k.ToString());
                    ddlDepSec.Items.Add((k < 10 ? "0" : "") + k.ToString());
                }
                ddlAdult.SelectedValue = "1";
                BindGrid();
                FillAffiliate();
                FillOffice();
                FillCommition();
            }
        }

        public void FillAffiliate()
        {
            try
            {
                siteId = Master.SiteID;
                var list = _masterAffiliate.GetAffiliateBySiteId(siteId);
                ddlaffiliate.DataSource = list;
                ddlaffiliate.DataTextField = "Name";
                ddlaffiliate.DataValueField = "Code";
                ddlaffiliate.DataBind();
                ddlaffiliate.Items.Insert(0, new ListItem("-Please select-", "0"));
            }
            catch (Exception ex) { throw ex; }
        }

        public void FillCommition()
        {
            try
            {
                siteId = Master.SiteID;
                var list = new ManageOrder().GetManualP2PCommitionbySiteid(siteId).OrderBy(x => x.Name).ToList();
                ddlCommition.DataSource = list;
                ddlCommition.DataTextField = "Name";
                ddlCommition.DataValueField = "Id";
                ddlCommition.DataBind();
                ddlCommition.Items.Insert(0, new ListItem("-Please select-", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FillOffice()
        {
            try
            {
                siteId = Master.SiteID;
                var list = new ManageOrder().GetAllBranchlistBySiteId(siteId).OrderBy(t => t.PathName).ToList();
                if (list.Count > 0)
                {
                    ddlOffice.DataSource = list;
                    ddlOffice.DataTextField = "PathName";
                    ddlOffice.DataValueField = "Id";
                    ddlOffice.DataBind();
                    ddlOffice.Items.Insert(0, new ListItem("-All Office-", "0"));
                    tblAdminUser objA = _masterBooking.GetAdminUserbyId(AdminuserInfo.UserID);
                    if (objA != null)
                    {
                        bool isDel = objA.IsDeleted.HasValue ? objA.IsDeleted.Value : true;
                        bool isActive = objA.IsActive.HasValue ? objA.IsActive.Value : false;
                        bool isAgent = objA.IsAgent.HasValue ? objA.IsAgent.Value : false;
                        if (!isDel && isActive && isAgent)
                        {
                            ddlOffice.SelectedValue = Convert.ToString(objA.BranchID.Value);
                            BindAgent();
                            ddlAgent.SelectedValue = Convert.ToString(objA.ID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindAgent()
        {
            try
            {
                if (ddlOffice.SelectedValue != "0")
                {
                    var branchid = Guid.Parse(ddlOffice.SelectedValue);
                    var list = new Masters().GetAdminUserListByBranchId(branchid).ToList();
                    ddlAgent.Enabled = true;
                    ddlAgent.DataSource = list;
                    ddlAgent.DataTextField = "Name";
                    ddlAgent.DataValueField = "ID";
                    ddlAgent.DataBind();
                    ddlAgent.Items.Insert(0, new ListItem("-All Agent-", "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAgent();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "isAgent", "isAgent();", true);
        }

        public void BindCurrency()
        {
            siteId = Master.SiteID;
            CurrencyID = _masterBooking.GetCurrencyIdBySiteId(siteId);
            CurrCode = new FrontEndManagePass().GetCurrencyShortCode(CurrencyID.HasValue ? CurrencyID.Value : Guid.NewGuid());
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlAdult.SelectedValue == "0" && ddlChildren.SelectedValue == "0" &&
                    ddlSeniors.SelectedValue == "0" && ddlYouth.SelectedValue == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('Select aleast one passenger.')", true);
                    return;
                }

                var lstP2PSale = new List<tblP2PSale>();
                if (Session["P2PBookings"] != null)
                    lstP2PSale = (List<tblP2PSale>)Session["P2PBookings"];
                var objBruc = new tblP2PSale
                    {
                        ID = Guid.NewGuid(),
                        From = txtFrom.Text.Trim(),
                        To = txtTo.Text.Trim(),
                        DateTimeDepature = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", CultureInfo.InstalledUICulture),
                        DepartureTime = ddldepTime.SelectedValue + ":" + ddlDepSec.SelectedValue,
                        DateTimeArrival = DateTime.ParseExact(txtReturnDate.Text, "dd/MMM/yyyy", CultureInfo.InstalledUICulture),
                        ArrivalTime = ddlReturnTime.SelectedValue + ":" + ddlRetSec.SelectedValue,
                        TrainNo = txtTrainNumber.Text,
                        Senior = ddlSeniors.SelectedValue,
                        Adult = ddlAdult.SelectedValue,
                        Children = ddlChildren.SelectedValue,
                        Youth = ddlYouth.SelectedValue,
                        Passenger =
                            ddlAdult.SelectedValue + " Adult " + ddlChildren.SelectedValue + " Children " +
                            ddlSeniors.SelectedValue + " Seniors " + ddlYouth.SelectedValue + " Youth",
                        SeviceName = txtTicketType.Text,
                        NetPrice = Convert.ToDecimal(txtprice.Text),
                        Price = Convert.ToDecimal(txtprice.Text),
                        Class = ddlClass.SelectedValue,
                        CurrencyId = CurrencyID,
                        ApiName = ddlCommition.SelectedItem.ToString(),
                        CommissionFee = GetActualCommition(Guid.Parse(ddlCommition.SelectedValue), Convert.ToDecimal(txtprice.Text))
                    };
                lstP2PSale.Add(objBruc);
                Session["P2PBookings"] = lstP2PSale;
                BindGrid();
                ClearP2PData();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "isAgent", "isAgent();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetActualCommition(Guid Ids, decimal Price)
        {
            try
            {
                return _masterBooking.GetCommitionP2PManualBooking(Ids, Price);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                if (Session["P2PBookings"] != null)
                {
                    var lstP2PSale1 = (List<tblP2PSale>)Session["P2PBookings"];
                    if (lstP2PSale1.Any())
                    {
                        grdOrders.DataSource = lstP2PSale1;
                        btnContinue.Visible = true;
                        dvRef.Visible = true;
                    }
                    else
                    {
                        grdOrders.DataSource = null;
                        btnContinue.Visible = false;
                        dvRef.Visible = false;
                    }
                    grdOrders.DataBind();
                }
                else
                {
                    btnContinue.Visible = false;
                    dvRef.Visible = false;
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void grdOrders_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var ID = Guid.Parse(e.CommandArgument.ToString());
                if (Session["P2PBookings"] != null)
                {
                    var lstP2PSale1 = (List<tblP2PSale>)Session["P2PBookings"];
                    lstP2PSale1.RemoveAll(a => a.ID == ID);
                    Session["P2PBookings"] = lstP2PSale1;
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["P2PBookings"] != null)
                {
                    var lstP2PSale1 = (List<tblP2PSale>)Session["P2PBookings"];
                    if (lstP2PSale1.Any())
                    {
                        Guid Agent = Guid.Empty;
                        Guid User = Guid.Empty;
                        string AffiliateCode = string.Empty;
                        if (rdbtnAgent.Checked)
                            Agent = Guid.Parse(ddlAgent.SelectedValue == "0" ? Agent.ToString() : ddlAgent.SelectedValue);
                        if (rdbtnPublic.Checked)
                            AffiliateCode = ddlaffiliate.SelectedValue == "0" ? string.Empty : ddlaffiliate.SelectedValue;

                        Session["Adminp2pBookingAgent"] = Agent;
                        Session["P2POrderID"] = _masterBooking.CreateOrder(AffiliateCode, Agent, User, siteId, "P2P", txtAgentRefNo.Text.Trim(), "AdminOD");
                        long POrderID = Convert.ToInt64(Session["P2POrderID"]);
                        //_masterBooking.UpdateIsManualBooking(POrderID);
                        foreach (tblP2PSale objP2P in lstP2PSale1)
                        {
                            /*Get Apiname start*/
                            GetStationDetailsByStationName objStationDeptDetail = _masterBooking.GetStationDetailsByStationName(objP2P.From);
                            string railcode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
                            string str = "BE";
                            if (railcode != null)
                                str = railcode == "0" ? "BE" : "TI";
                            /*Get Apiname end*/

                            //site currency//
                            objP2P.ApiPrice = objP2P.Price.Value;
                            //if (str == "BE")
                            //    objP2P.ApiName = "MANUAL";
                            //else if (str == "TI")
                            //    objP2P.ApiName = "MANUAL";

                            objP2P.ApiName = objP2P.ApiName;
                            objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
                            objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
                            objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
                            objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
                            objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
                            objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
                            objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
                            objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
                            objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
                            objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
                            objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
                            objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
                            objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
                            objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
                            objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
                            objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
                            objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

                            objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
                            objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);
                            _masterBooking.AddP2PBookingOrder(objP2P, POrderID);
                        }
                        Session.Remove("P2PBookings");
                        Response.Redirect("~/P2POrderDetail.aspx");
                    }
                }
                Response.Redirect("orderdetails.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClearP2PData()
        {
            ddlCommition.SelectedValue = "0";
            txtFrom.Text = "";
            txtTo.Text = "";
            txtDepartureDate.Text = "";
            ddldepTime.SelectedValue = "09";
            ddlDepSec.SelectedValue = "00";
            txtReturnDate.Text = "";
            ddlReturnTime.SelectedValue = "09";
            ddlRetSec.SelectedValue = "00";
            txtTrainNumber.Text = "";
            ddlSeniors.SelectedIndex = 0;
            ddlAdult.SelectedIndex = 0;
            ddlChildren.SelectedIndex = 0;
            ddlYouth.SelectedIndex = 0;
            txtTicketType.Text = "";
            txtprice.Text = "";
            ddlClass.SelectedValue = "1";
        }
    }
}