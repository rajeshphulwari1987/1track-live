﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using IR_Admin.OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Xml;
using System.Net;

namespace IR_Admin
{
    public partial class P2PPayment : Page
    {
        #region Global Variables
        ManageBooking _masterBooking = new ManageBooking();
        Guid siteId;
        long P2pOrderId = 0;
        Guid P2PAgentID = Guid.Empty;
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;

            //if (Session["siteId"] != null)
            //    siteId = Guid.Parse(Session["siteId"].ToString());
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["P2POrderID"] != null)
                P2pOrderId = Convert.ToInt64(Session["P2POrderID"]);
            if (Session["Adminp2pBookingAgent"] != null)
                P2PAgentID = Guid.Parse(Session["Adminp2pBookingAgent"].ToString());
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            _masterBooking.UpdateOrderStatusForManualBooking(3, P2pOrderId, P2PAgentID);
            Session.Remove("Adminp2pBookingAgent");
            Session.Remove("P2POrderID");
            Response.Redirect("ManualBooking/P2POrderView.aspx?id=" + P2pOrderId);
        }
    }
}