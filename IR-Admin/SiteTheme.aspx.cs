﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class SiteTheme : Page
    {
        readonly private ManageFrontWebsitePage _master = new ManageFrontWebsitePage();
        readonly Masters _Master = new Masters();
        public string Imagepath = string.Empty;

        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                BindGrid();
                BindDDl();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    ddlSite.Enabled = false;
                    GetThemeForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid();
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        public void BindDDl()
        {
            ddlSite.DataSource = _Master.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));

            ddlTheme.DataSource = _Master.GetThemeList();
            ddlTheme.DataTextField = "ThemeName";
            ddlTheme.DataValueField = "ID";
            ddlTheme.DataBind();
            ddlTheme.Items.Insert(0, new ListItem("---Theme---", "0"));
        }

        void BindGrid()
        {
            Tab = "1";
            grdTheme.DataSource = _master.GetSiteTheme();
            grdTheme.DataBind();
        }

        protected void grdTheme_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdTheme.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void grdTheme_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteSiteTheme(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                _master.AddSiteTheme(new tblSiteTheme
                {
                    ID = Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid(),
                    Name = txtName.Text.Trim(),
                    CssFolderName = "Sta",
                    CssHeaderFooterName = ddlStyle.SelectedValue.Trim(),
                    HeaderHtml = ddlHeader.SelectedValue.Trim(),
                    FooterHtml = ddlFooter.SelectedValue.Trim(),
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    ThemeID = Guid.Parse(ddlTheme.SelectedValue)
                });

                ShowMessage(1, Request["id"] == null ? "You have successfully added the record." : "You have successfully updated the record.");
                BindGrid();
                ddlSite.SelectedIndex = 0;
                ddlTheme.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SiteTheme.aspx");
        }

        public void GetThemeForEdit(Guid id)
        {
            var obj = _master.GetSiteThemeById(id);
            if (obj != null)
            {
                ddlSite.SelectedValue = obj.SiteID.ToString();
                ddlTheme.SelectedValue = obj.ThemeID.ToString();
                txtName.Text = obj.Name;
                
                ddlStyle.SelectedValue = obj.CssHeaderFooterName;
                ddlHeader.SelectedValue = obj.HeaderHtml;
                ddlFooter.SelectedValue = obj.FooterHtml;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}