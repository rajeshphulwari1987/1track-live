﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_Contact.aspx.cs"
    Inherits="IR_Admin.Manage_Contact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>:: International Rail :: </title>
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="width" />
    <script type="text/javascript" src="Scripts/html5.js"></script>
    <script src="editor/jquery.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important; /*background-color: #FBDEE6;*/
        }
        .edit-btn
        {
            background: #c5456a url("images/icon-edit.png")10px 5px no-repeat;
            border: medium none;
            border-radius: 0 0 0 0 !important;
            box-shadow: 3px 3px 3px 0 #000000;
            color: #fff;
            cursor: pointer;
            font-size: 14px;
            font-weight: bold;
            height: 28px;
            padding: 0 15px 0 35px;
            position: absolute;
            width: 70px;
            left: 10px;
            top: 10px;
        }
        
        .wrapper
        {
            width: 1004px;
        }
        .clsDisable
        {
            background-color: #D4D4D4;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 700px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        .clsRead
        {
            float: right;
            font-size: 11px;
            color: #951f35;
        }
        .read
        {
            background: url(images/page/arr-red.jpg) no-repeat left;
            padding-left: 10px;
        }
        .clsAbs
        {
            display: block;
            position: absolute;
        }
        .pTop40
        {
            padding-top: 40px;
        }
        .pTop5
        {
            padding-top: 5px;
        }
        .button
        {
            min-width: 60px;
            width: auto;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px;
            border-right: 1px thin #a1a1a1;
            border: thin none;
            font-weight: bold;
            color: white;
            cursor: pointer;
            height: 30px;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(
	        function () {
	            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
	            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
	            $('#txtRtPanel').redactor({ iframe: true, minHeight: 200 });
	            $('#txtCall').redactor({ iframe: true, minHeight: 200 });
	        }
        );

        $(document).ready(function () {

            $("input[class=rdCallID]").attr('name', "img");
            $("input[class=rdrtID]").attr('name', "img");

            //----------Edit heading---------//
            $(".edit").click(function () {
                var value;
                if ($(this).attr("rel") == "ContentHead") {
                    $("#divHeading").slideToggle("slow");
                    value = $('#ContentHead').html();
                    $('.redactor_rdHead').contents().find('body').html(value);
                } else if ($(this).attr("rel") == "ContentText") {

                    $("#divContent").slideToggle("slow");
                    value = $('#ContentText').html();
                    $('.redactor_rdContent').contents().find('body').html(value);
                } else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRt").slideToggle("slow");
                }
            });


            //------Edit banner images------//
            $(".editBanner").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });

            //-----Close-----//
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRight").hide();
                }
                else if ($(this).attr("rel") == "ContentRightTxt") {
                    $("#ContentRightTxt").hide();
                }
                else if ($(this).attr("rel") == "ContentCall") {
                    $("#ContentCall").hide();
                }
                else if ($(this).attr("rel") == "ContentCallTxt") {
                    $("#ContentCallTxt").hide();
                }
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }

                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }

                $("#ContentBanner").hide();
            });


            //-------Edit right panel images-------//
            $(".editRtPanel").click(function () {
                $("#ContentRight").slideToggle("slow");
                $('.hdnRight').val($(this).attr("rel"));
            });

            //------Save right panel images------//
            $("#btnSaveRtPanel").click(function () {
                var imagename;
                $('div#ContentRight input[type=radio]').each(function () {
                    if ($('.hdnRight').val() == "rtPanelImgHome") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactHome").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgCall") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactCall").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgEmail") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactEmail").attr("src", imagename);
                        }
                    }
                    $("#ContentRight").hide();
                });
            });

            //-------Edit right panel images text-------//
            $(".editRtPanelText").click(function () {
                $("#ContentRightTxt").slideToggle("slow");
                $('.hdnrtPanelTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "contactHomeTxt") {
                    value = $('#contactHomeTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactCallTxt") {
                    value = $('#contactCallTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactEmailTxt") {
                    value = $('#contactEmailTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
            });

            //-----Save right panel text------//
            $(".btnSaveRttxt").click(function () {
                var value = $('textarea[name=txtRtPanel]').val();
                if ($('.hdnrtPanelTxt').val() == "contactHomeTxt") {
                    if (value != "") {
                        $('#contactHomeTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactCallTxt") {
                    if (value != "") {
                        $('#contactCallTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactEmailTxt") {
                    if (value != "") {
                        $('#contactEmailTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                $("#ContentRightTxt").hide();
            });


            //-------Edit call center images-------//
            $(".editRtCall").click(function () {
                $("#ContentCall").slideToggle("slow");
                $('.hdnCall').val($(this).attr("rel"));
            });

            //-------Edit call center images text-------//
            $(".editRtCallText").click(function () {
                $("#ContentCallTxt").slideToggle("slow");
                $('.hdnrtCallTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "callHead") {
                    value = $('#callHead').html();
                    $('.redactor_rdCall').contents().find('body').html(value);
                }
                if ($(this).attr("rel") == "callText") {
                    value = $('#callText').html();
                    $('.redactor_rdCall').contents().find('body').html(value);
                }
            });

            //------Save call center images------//
            $(".btnSaveCall").click(function () {
                var imagename;
                $('div#ContentCall input[type=radio]').each(function () {
                    if ($('.hdnCall').val() == "rtCallImg") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgRtCall").attr("src", imagename);
                        }
                    }
                    $("#ContentCall").hide();
                });
            });

            //-----Save call center text------//
            $(".btnSaveCallTxt").click(function () {
                var value = $('textarea[name=txtCall]').val();
                if ($('.hdnrtCallTxt').val() == "callHead") {
                    if (value != "")
                        $('#callHead').html(value);
                }
                else if ($('.hdnrtCallTxt').val() == "callText") {
                    if (value != "")
                        $('#callText').html(value);
                }
                $("#ContentCallTxt").hide();
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="wrapper">
<header>
    <div class="logo"> 
        <img src="images/page/logo.png" class="scale-with-grid" alt="" border="0" />
    </div>
    <div class="top-rightblock">
        <div class="toplink">
        <a href="#"> Feedback </a> | <a href="#"> Sitemap   </a>| <a href="#"> Security  </a> | <a href="#"> Agent </a>
        </div>
        <div class="call"> 
        <img src="images/page/icon-call.png" alt="" border="0" class="scale-with-grid" />
        <span> +44 (0) 871 231 0790 </span>
        </div>
    </div>
</header>
<nav>
<ul> 
<li><a> Home  </a> </li>
<li><a> Countries </a> </li>
<li><a> Rail Passes   </a> </li>
<li><a> Speciality Trains  </a> </li>
<li><a> Accommodation  </a> </li>
</ul>
<div class="agentlogin"> <a href="#"> <img src="images/page/icon-basket.png" alt="" border="0" /> Basket </a></div>
</nav>
<section class="content-inner">
<div class="breadcrumb"> <a href="#">Home </a> >>  Contact Us  </div>

<div class="banner">
    <%--Banner section--%>
    <div id="dvBanner">
        <div id="div3" style="display: block; position: absolute;">
            <input type="button" class="editBanner edit-btn" value="Edit"/>
        </div>
        <img id="imgMainBanner" src="images/page/banner.jpg" alt="" border="0" width="974px"/>
        <asp:HiddenField ID="hdnBannerIDs" runat="server"/>
    </div>
    <%--Banner section end--%>
</div>

<div style="float:left;width:658px">

<div class="cms">
   <div id="divList1" class="clsAbs">
        <input type="button" class="edit edit-btn" value="Edit" rel="ContentHead"/>
   </div>
   <div class="pTop40">
       <div id="ContentHead" runat="server">
           <h1><asp:Label ID="lblHead" runat="server" Text="Contact us at International Rail HQ"></asp:Label></h1>
       </div>
    </div>
</div>
<div class="cms">
   <div id="div1" class="clsAbs">
         <input type="button" class="edit edit-btn" value="Edit" rel="ContentText"/>
      </div>
   <div class="pTop40">
         <div id="ContentText" runat="server">
        <p><asp:Label ID="lblContent" runat="server" Text="If you have any comments or queries about International Rail Limited or any of our products featured on these pages, please do not hesitate to contact us."/></p>
        </div>
      </div>
</div>

<div class="formblock" style="width:45%;">
    <img src="images/page/ContactWatermark.png" />
</div>
<div class="address-block" id="addBlock" runat="server">
    
<div class="fullrow">
<div id="rtPanelImgHome">
   <div class="clsAbs">
       <input type="button" class="editRtPanel edit-btn" value="Edit" rel="rtPanelImgHome" style="top:-20px!important"/>
   </div>
   <div class="pTop5">
       <div class="imgblock"> 
            <img id="imgContactHome" class="imground scale-with-grid" src="" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnRight" id="Hidden3" />
       </div>
   </div>
</div>

<div class="clsAbs">
    <input type="button" class="editRtPanelText edit-btn" value="Edit" rel="contactHomeTxt" style="top:-24px!important;left:87px"/>
</div>
<div id="contactHomeTxt" style="width:300px;">
    <span class="inline">
      <asp:Label ID="lblRtPanelHead" runat="server" Text="International Rail Limited, PO Box 153, Alresford, SO24 4AQ. United Kingdom" style="line-height:25px!important;"/>
    </span>
</div>
</div>

<div class="clear"></div>
<br/>
<div class="fullrow">
<div id="rtPanelImgCall">
   <div class="clsAbs">
       <input type="button" class="editRtPanel edit-btn" value="Edit" rel="rtPanelImgCall" style="top:-20px!important"/>
   </div>
   <div class="pTop5">
       <div class="imgblock"> 
            <img id="imgContactCall" class="imground scale-with-grid" src="" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnRight" id="Hidden6" />
       </div>
   </div>
</div>
<div class="clsAbs">
    <input type="button" class="editRtPanelText edit-btn" value="Edit" rel="contactCallTxt" style="top:-24px!important;left:87px"/>
</div>
<div id="contactCallTxt" style="width:300px;">
    <span class="inline">
        <asp:Label ID="Label1" runat="server" Text="Phone: +44 (0) 871 231 0790*"/>
    </span>
</div>

</div>
<div class="clear"></div>
<br/>

<%--<div class="fullrow">
<div id="rtPanelImgFax">
   <div class="clsAbs">
       <input type="button" class="editRtPanel edit-btn" value="Edit" rel="rtPanelImgFax" style="top:-20px!important"/>
   </div>
   <div class="pTop5">
       <div class="imgblock"> 
            <img id="imgContactFax" class="imground scale-with-grid" src="" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnRight" id="Hidden1" runat="server" />
       </div>
   </div>
</div>

<div class="clsAbs">
    <input type="button" class="editRtPanelText edit-btn" value="Edit" rel="contactFaxTxt" style="top:-24px!important;left:87px"/>
</div>
<div id="contactFaxTxt" style="width:300px;">
    <span class="inline">
        <asp:Label ID="Label2" runat="server" Text="+44 (0) 871 231 0791*"/>
    </span>
</div>
</div>

<div class="clear"></div>
<br/>--%>

<div class="fullrow">
<div id="rtPanelImgEmail">
   <div class="clsAbs">
       <input type="button" class="editRtPanel edit-btn" value="Edit" rel="rtPanelImgEmail" style="top:-20px!important"/>
   </div>
   <div class="pTop5">
       <div class="imgblock"> 
            <img id="imgContactEmail" class="imground scale-with-grid" src="" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnRight" id="Hidden2" />
       </div>
   </div>
</div> 
<div class="clsAbs">
    <input type="button" class="editRtPanelText edit-btn" value="Edit" rel="contactEmailTxt" style="top:-24px!important;left:87px"/>
</div>
<div id="contactEmailTxt" style="width:300px;">
    <span class="inline">
        <asp:Label ID="Label3" runat="server" Text="sales@internationalrail.com"/>
    </span>
</div>

</div>
</div>
</div>

<div id="callBlock" class="map-block" style="float:right;" runat="server">
    
    <div id="rtCall" runat="server">
    <div class="sml-detail-block-outer" style="width:275px;border:0px;">
    
    <div class="clsAbs">
        <input type="button" class="editRtCallText edit-btn" value="Edit" rel="callHead"/>
    </div>
    <div id="callHead" class="pTop40">
        <div class="hd-title"> <asp:Label ID="lblRtCallHead" runat="server" Text="Call Center"/></div>
    </div>

    <div class="sml-detail-block-inner">
        <div id="rtCallImg">
            <div class="clsAbs">
            <input type="button" class="editRtCall edit-btn" value="Edit" rel="rtCallImg"/>
        </div>
            <div class="pTop5">
            <img id="imgRtCall" class="imground scale-with-grid" src="" alt="" border="0" runat="server" />
            <input type="hidden" class="hdnCall" id="Hidden4" />
        </div>
        </div>

        <div class="clsAbs">
            <input type="button" class="editRtCallText edit-btn" value="Edit" rel="callText"/>
        </div>
        <div id="callText" class="pTop40">
            <p> <asp:Label ID="lblRtCallTxt" runat="server" Text="Call Center »"/>  </p>
        </div>
    </div> 
    </div>
</div>
</div>

</section>

</section>
    <footer>
<div class="wrapper">
<div class="foot-col1"> 
<h3> Destinations </h3>
<ul> 
<li> <a href="#"> Paris  </a></li> 
<li> <a href="#"> Disneyland Paris  </a></li> 
<li> <a href="#"> Brussels  </a></li> 
<li> <a href="#"> Lille  </a></li> 
<li> <a href="#"> Bruges </a></li> </ul>
</div>
<div class="foot-col1"> 
<h3> Offers </h3>
<ul> 
<li> <a href="#"> Thalys Unmissables </a></li> 
<li> <a href="#"> Favourites </a></li> 
<li> <a href="#"> Flanders Battlefields   </a></li> 
</ul>
</div>
<div class="foot-col1"> 
<h3> Rail Companies </h3>
<ul> 
<li> <a href="#"> Eurostar  </a></li> 
 <li> <a href="#">  Thalys  </a></li> 
 <li> <a href="#">  ICE  </a></li> 
 <li> <a href="#">  InterCity-EuroCity </a></li>
</ul>
</div>
<div class="foot-col1"> 
<h3> Travel the world  </h3>
<ul> 
<li> <a href="#"> Canada Rail Pass  </a></li>
<li> <a href="#">  USA Rail Pass  </a></li>
<li> <a href="#">  Australia Rail Pass  </a></li>
 <li> <a href="#">  New Zealand Pass  </a></li>
 <li> <a href="#">  InterRail Global   </a></li>
</ul>
</div>
<div class="foot-col1"> 
<h3> Practical   </h3>
<ul> 
<li> <a href="#"> Your tickets  </a></li>
 <li> <a href="#"> Contact us  </a></li>
 <li> <a href="#"> Station information  </a></li>
 <li> <a href="#"> Refunds for delays  </a></li>
 <li> <a href="#"> FAQ   </a></li>
</ul>
</div>
<div class="foot-col2"> 
<h3> Legal information    </h3>
<ul> 
<li> <a href="#"> Passenger rights </a></li>
 <li> <a href="#"> Privacy and cookies </a></li>
 <li> <a href="#"> Conditions of use </a></li>
 <li> <a href="#"> General terms and conditions </a></li> 
</ul>
</div>
</div>
<section class="bottom-section">
<div class="wrapper">
<div class="f-links">
<p> 
  <a href="aboutus.html">  About us </a>  |   <a href="contactus.html"> Contact us </a>  |   <a href="privacy-policy.html"> Privacy policy  </a> |   <a href="condition-of-use.html"> Conditions of use  </a> |   <a href="booking-condition.html"> Booking conditions </a>  |   <a href="Delivery and Service.html"> Delivery & service  </a>|   <a href="#">  Unsubscribe </a>
 </p>
 <span> * Calls from UK landlines cost £0.10p per minute. Call charges from mobiles and international numbers may vary. <br />
 © Copyright 2013 International Rail Ltd.
 </span>
</div>
<div class="social-links">
<p>Follow us online </p>
<a href="#"><img src="images/page/fb.png" alt="" class="scale-with-grid" border="0" /></a> 
<a href="#"><img src="images/page/tw.png" alt="" class="scale-with-grid" border="0" /></a> 
<a href="#"><img src="images/page/blog.png" alt="" class="scale-with-grid" border="0" /></a> 
<a href="#"><img src="images/page/linkdin.png" alt="" class="scale-with-grid" border="0" /></a> 
</div>
</div>
</section>
</footer>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 460px;
        height: auto; left: 277px; top: 150px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="208"
                        Height="119px" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 277px; top: ">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 277px; top: 180px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentRight" class="PopUpSampleIMG" style="display: none; width: 220px;
        height: auto; top: 300px; left: 100px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll">
            <asp:DataList ID="dtRtPanel" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtRtPanel_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="18px"
                        Height="18px" CssClass="bGray" />
                    <br />
                    <input id="rdrtID" class="rdrtID" runat="server" type="radio" name="img" value='<%#Eval("ImagePath")%>'
                        style="margin: 2px 0 2px 0" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button2" value="Cancel" class="button btnClose" rel="ContentRight" />
            <input type="button" id="btnSaveRtPanel" value="Save" class="button btnSaveRtPanel"
                rel="ContentRight" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentRightTxt" style="display: none; left: 260px;
        top: 300px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtRtPanel" name="txtRtPanel" cols="10" rows="5" class="rdRight"></textarea>
                        <input type="hidden" class="hdnrtPanelTxt" id="Hidden7" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button3" value="Cancel" class="button btnClose" rel="ContentRightTxt" />
                    <input type="button" id="btnSaveRttxt" value="Save" class="button btnSaveRttxt" rel="ContentRightTxt" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentCall" class="PopUpSampleIMG" style="display: none; width: 460px;
        height: auto; top: 500px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll;">
            <asp:DataList ID="dtRtCall" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtRtCall_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="208px"
                        Height="119px" CssClass="bGray" />
                    <br />
                    <input id="rdCallID" runat="server" type="radio" name="img" value='<%#Eval("ImagePath")%>'
                        class="rdCallID" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="Button5" value="Cancel" class="button btnClose" rel="ContentCall" />
            <input type="button" id="Button6" value="Save" class="button btnSaveCall" rel="ContentCall" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentCallTxt" style="display: none; left: 260px; top: 500px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtCall" name="txtCall" cols="10" rows="5" class="rdCall"></textarea>
                        <input type="hidden" class="hdnrtCallTxt" id="Hidden5" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button7" value="Cancel" class="button btnClose" rel="ContentCallTxt" />
                    <input type="button" id="Button8" value="Save" class="button btnSaveCallTxt" rel="ContentCallTxt" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
