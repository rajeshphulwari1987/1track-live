﻿<%@ Page Title="Admin Users" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="~/ManageAdminUsers.aspx.cs" Inherits="IR_Admin.AdminuserPage" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            $.each($("[id*='chkStock']"), function (k, v) {
                if (!$(this).is(":checked")) {
                    $(this).closest('tr').find("[type=radio]").attr('disabled', true);
                }
            });

            $("[id*='chkStock']").change(function () {
                if ($(this).is(":checked"))
                    $(this).closest('tr').find("[type=radio]").removeAttr('disabled');
                else
                    $(this).closest('tr').find("[type=radio]").attr('disabled', true).removeAttr('checked');
            });
            $(".chkStock").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("[id*='chkStock']").attr("checked", "checked");
                    $(this).closest('div').find("[type=radio]").removeAttr('disabled');
                }
                else {
                    $("[id*='chkStock']").removeAttr("checked");
                    $(this).closest('div').find("[type=radio]").attr('disabled', true).removeAttr('checked');
                }
            });

            $("[id*=radiostock]").click(function () {
                $("[id*=radiostock]").removeAttr("checked");
                $(this).attr("checked", true);
            });
            $("#MainContent_ddlRoles").attr("title", $("#MainContent_ddlRoles option:selected").text())
            $("#MainContent_ddlRoles").change(function () {
                $("#MainContent_ddlRoles").attr("title", $("#MainContent_ddlRoles option:selected").text())
            });

            $("#MainContent_ddlOffices").attr("title", $("#MainContent_ddlOffices option:selected").text())
            $("#MainContent_ddlOffices").change(function () {
                $("#MainContent_ddlOffices").attr("title", $("#MainContent_ddlOffices option:selected").text())
            });

            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });

            if ("<%=tab%>" == 1) {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");

                document.getElementById('MainContent_divlist').style.display = 'block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                $("ul.tabs").tabs("div.inner-tabs-container > div");

                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }

            AdvacnceSearch();
        });

        function toggleTreeView(ev, hide) {
            var ddTreeView = document.getElementById("ddTreeView");
            ddTreeView.style.display = (hide == null && ddTreeView.style.display == "none") || hide == false ? "" : "none";
            ev = ev ? ev : window.event;
            if (ev) ev.cancelBubble = true;
        }

        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }

        function AdvacnceSearch() {
            $('#divExpandCollapseAdvanceSearch').click(function () {
                try {
                    var div = $('#divAdvancedSearch');
                    var img = $('#imgUpDown');
                    if (!div.is(':visible')) {
                        div.slideDown(100, function () { img.attr('src', 'images/ar-up.png'); });
                        $('#divAdvancedSearch').show();
                    }
                    else {
                        div.slideUp(100);
                        img.attr('src', 'images/ar-down.png');
                        $('#divAdvancedSearch').hide();
                    }
                }
                catch (ex) {
                    alert(ex);
                    return false;
                }
            });
        }

        function showExcelDiv() {
            var div = $('#divAdvancedSearch');
            var img = $('#imgUpDown');
            $('#divAdvancedSearch').slideDown(100, function () { img.attr('src', 'images/ar-up.png'); });
            $('#divAdvancedSearch').show();
        }

        function setdefaultprinter(obj) {
            console.log('[value="' + obj["value"] + '"]');
            console.log($('[value="' + obj["value"] + '"]').parent().parent().find("type='checkbox'").attr("id"));
        }
    </script>
    <style type="text/css">
        .collapse-tab {
            width: 960px;
        }

            .collapse-tab .innertb-content, .pass-coloum-two {
                width: 100%;
            }

        .default-print {
            width: 1300px;
        }
        .headertbl tr td{
            padding-right :10px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>Users</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ManageAdminUsers.aspx" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" NavigateUrl="ManageAdminUsers.aspx" runat="server">New</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px; padding: 0px 1px; font-size: 14px; display: none;">
                        <div class="pass-coloum-one" style="width: 50px; float: left; line-height: 25px;">
                            Site:
                        </div>
                        <div class="pass-coloum-two" style="width: 200px; float: left;">
                            <asp:DropDownList runat="server" ID="ddlSitesSearch" Width="90%" />
                        </div>
                        <div class="pass-coloum-one" style="width: 50px; float: left; line-height: 25px;">
                            e-mail:
                        </div>
                        <div class="pass-coloum-two" style="width: 200px; float: left;">
                            <asp:TextBox ID="txtEmailSearch" runat="server" class="input" ClientIDMode="Static"
                                Width="150px" />
                        </div>
                        <div class="pass-coloum-one" style="width: 80px; float: left; line-height: 25px;">
                            User name:
                        </div>
                        <div class="pass-coloum-two" style="width: 202px; float: left;">
                            <asp:TextBox ID="txtUserNameSearch" runat="server" class="input" ClientIDMode="Static"
                                Width="150px" />
                        </div>
                        <div style="float: right; margin-right: 2px;">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" />
                        </div>
                    </div>
                    <div class="crushGvDiv">
                        <table class="searchDiv" width="100%">
                            <tr>
                                <td>All Users
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </td>
                                <td>UserName:
                                    <asp:TextBox runat="server" ID="txtSearchUserName" MaxLength="50"></asp:TextBox>
                                </td>
                                <td>Email Address:
                                    <asp:TextBox runat="server" ID="txtSearchEmail" MaxLength="50"></asp:TextBox>
                                </td>
                                <td>User Type:
                                    <asp:DropDownList ID="ddlUserType" runat="server" Width="95px">
                                        <asp:ListItem Value="-1">--User Type--</asp:ListItem>
                                        <asp:ListItem Value="0">Admin</asp:ListItem>
                                        <asp:ListItem Value="1">Agent</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnSearchSubmit" Text="Submit" CssClass="button" OnClick="btnSearchSubmit_Click" />
                                </td>
                            </tr>
                            <tr id="showHideExportExcelDiv" runat="server" visible="false">
                                <td colspan="5">
                                    <div id="dvSearch2" class="searchDiv" runat="server" style="float: left; width: 962px; padding: 0px 1px; font-size: 14px;">
                                        <%--Advanced Search panel starts here..--%>
                                        <div class="collapse-tab">
                                            <div id="divExpandCollapseAdvanceSearch" style="width: 960px; float: left; cursor: pointer; padding-left: 8px !important; color: white;">
                                                <img src="images/ar-down.png" id="imgUpDown" class="icon" style="margin: 5px 15px 0px 0px;" />
                                                <strong>Advance Search </strong>
                                            </div>
                                            <div id="divAdvancedSearch" style="display: none;" class="innertb-content">
                                                <div class="pass-coloum-two" style="float: left; padding-bottom: 8px; padding-top: 10px; padding-left: 10px;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label style="padding-left: 10px;">
                                                                    User Role
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlUserRole" runat="server" Width="100%" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlUserRole_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnExportExcel" Text="Export To Excel" runat="server" OnClick="btnExportExcel_Click"
                                                                    CssClass="button" data-export="export" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <%--Advanced Search panel ends here..--%>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="grdUsers" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="50" ForeColor="#333333" GridLines="None" DataKeyNames="ID" AutoGenerateColumns="False"
                            OnRowCommand="grdusers_RowCommand" AllowPaging="True" OnPageIndexChanging="grdUsers_PageIndexChanging"
                            OnRowDataBound="grdUsers_RowDataBound">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="UserName" HeaderText="UserName">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EmailAddress" HeaderText="EmailAddress">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Name" HeaderText="Name">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="OfficeName" HeaderText="OfficeName">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RoleName" HeaderText="Role">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Note" HeaderText="Note">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Is Agent">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgAgent" CommandArgument='<%#Eval("ID")%>' Height="16"
                                            CommandName="IsAgent" AlternateText="IsAgent" ImageUrl='<%#Eval("IsAgent").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsAgent").ToString()=="True" ?"Agent":"Not Agent" %>' />
                                    </ItemTemplate>
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this user?');"
                                            Visible="False" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <HeaderStyle VerticalAlign="Top" />
                                    <ItemStyle Width="8%" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No records found !
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:GridView ID="grdExportExcel" runat="server" CellPadding="4" CssClass="grid-head2"
                            Visible="false" ForeColor="#333333" GridLines="None" DataKeyNames="ID" AutoGenerateColumns="False">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:BoundField DataField="UserName" HeaderText="User Name">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Password" HeaderText="Password">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EmailAddress" HeaderText="Email Address">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Name" HeaderText="Name">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="OfficeName" HeaderText="Office Name">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RoleName" HeaderText="Role">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Note" HeaderText="Note">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IsAgent" HeaderText="Is Agent">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IsActive" HeaderText="Is Active">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ManualPassIsActive" HeaderText="Is Manual Pass">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="MenualP2PIsActive" HeaderText="Is Manual P2P">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No records found !
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" style="width: 30%">User Name
                            </td>
                            <td class="col" style="width: 40%">
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtUserNameRequiredFieldValidator" runat="server"
                                    CssClass="valdreq" ControlToValidate="txtUserName" ErrorMessage="*" ValidationGroup="AUForm"></asp:RequiredFieldValidator>
                            </td>
                            <td rowspan="13" valign="top" class="col" style="width: 30%; border-left: 1px dashed #B1B1B1;">&nbsp; <b>Select Site</b>
                                <div runat="server" id="hideSiteSelection" style="width: 297px; z-index: 100; position: absolute;"
                                    visible="false">
                                </div>
                                <div style="width: 95%; height: 520px; overflow-y: auto; margin-left: 5px;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="col">Password
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="reqpasswordvalid" ControlToValidate="txtPassword"
                                    ValidationGroup="AUForm" runat="server" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="rvPass" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                                    ValidationGroup="AUForm" />
                                <a href="#" id="shwpwmsg" title="password minimum length must be 8 to 20 character. should be alphanumeric with lower and upper case.">
                                    <img src="images/info.png" alt="" width="25px" style="margin-top: 2px; position: absolute;" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Salutation
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlTitle" runat="server" Style="padding: 0 0;">
                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Mrs." Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Ms" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ReqTitle" runat="server" ControlToValidate="ddlTitle"
                                    CssClass="valdreq" ErrorMessage="select title" ToolTip="Title is required." ValidationGroup="AUForm"
                                    InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Forename
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtForename" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Surname
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Email Address
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfEmail" runat="server" CssClass="valdreq" ControlToValidate="txtEmail"
                                    ErrorMessage="*" ValidationGroup="AUForm" />
                                <asp:RegularExpressionValidator ID="txtEmailRegularExpressionValidator" runat="server"
                                    ForeColor="red" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Address"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="AUForm"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Role
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlRoles" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfRole" runat="server" ControlToValidate="ddlRoles"
                                    CssClass="valdreq" ErrorMessage="select role" ToolTip="Role is required." ValidationGroup="AUForm"
                                    InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Office
                            </td>
                            <td class="col">
                                <asp:DropDownList runat="server" ID="ddlOffices" Width="205" />
                            </td>
                            <td class="col">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Is Agent
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkAgent" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Manual P2P Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkMenualP2P" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Manual Pass Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkManualPass" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">Allow To
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlAllowTo" runat="server">
                                    <asp:ListItem Value="0">--Select Allow To--</asp:ListItem>
                                    <asp:ListItem Value="1">Front Site</asp:ListItem>
                                    <asp:ListItem Value="2">Admin Site</asp:ListItem>
                                    <asp:ListItem Value="3">Both</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfAllowTo" runat="server" ControlToValidate="ddlAllowTo"
                                    CssClass="valdreq" ErrorMessage="Select allow to" ToolTip="Allow To is required."
                                    ValidationGroup="AUForm" InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">User Note
                            </td>
                            <td class="col" colspan="2">
                                <asp:TextBox ID="txtUserNote" TextMode="MultiLine" Width="276" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">Select Default Printer
                                <div style="height: 400px; margin-left: 20px; width: 936px;">
                                    <table class="headertbl">
                                        <tr>
                                            <td><span>InterRail</span></td>
                                            <td><span>BritRail</span></td>
                                            <td><span>Eurail</span></td>
                                            <td>
                                                <input type="checkbox" value="Select / DeSelect All" class="chkStock" />Select/DeSelect
                                    All</td>
                                        </tr>
                                    </table>
                                    <div style="height: 350px;overflow-y: auto; width: 100%;">
                                        <asp:Repeater runat="server" ID="rptStocks">
                                            <HeaderTemplate>
                                                <table class="default-print">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 58px;padding-left: 12px;">
                                                        <input type="radio" runat="server" class="rdnstockInterrail" name="rdnstockInterrail" id="rdnstockInterrail" checked='<%#Convert.ToBoolean(Eval("InterrailDefaultPrinter")) %>' value='<%#Eval("ID") %>' />
                                                    </td>
                                                    <td style="width: 55px;">
                                                        <input type="radio" runat="server" class="rdnstockBritRail" name="rdnstockBritRail" id="rdnstockBritRail" checked='<%#Convert.ToBoolean(Eval("BritrailDefaultPrinter")) %>' value='<%#Eval("ID") %>' />
                                                    </td>
                                                    <td style="width: 55px;">
                                                        <input type="radio" runat="server" class="rdnstockEurail" name="rdnstockEurail" id="rdnstockEurail" checked='<%#Convert.ToBoolean(Eval("EurailDefaultPrinter")) %>' value='<%#Eval("ID") %>' />
                                                    </td>
                                                    <td style=" ">
                                                        <asp:CheckBox runat="server" ID="chkStock" Text='<%#Eval("BRANCHNAME") %>' Checked='<%#Convert.ToBoolean(Eval("ISACTIVE")) %>' onclick="" />
                                                        <span class='clsStock'>(<%#Eval("PRINTERCODE")%>)</span>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />&nbsp;
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="AUForm" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.default-print').find('.rdnstockInterrail').click(function () {
                $('.default-print').find('.rdnstockInterrail').prop('checked', false);
                $(this).prop('checked', true);
            });
            $('.default-print').find('.rdnstockBritRail').click(function () {
                $('.default-print').find('.rdnstockBritRail').prop('checked', false);
                $(this).prop('checked', true);
            });
            $('.default-print').find('.rdnstockEurail').click(function () {
                $('.default-print').find('.rdnstockEurail').prop('checked', false);
                $(this).prop('checked', true);
            });
        });
    </script>
</asp:Content>
