﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_StaContact.aspx.cs"
    Inherits="IR_Admin.Manage_StaContact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title>STA Rail | STA Travel Rail</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="Styles/stacss/assets/css/main.css">
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:400" rel="stylesheet"
        type="text/css">
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet"
        type="text/css">
    <link href="//www.statravel.com/static/us_division_web_live/css/partner.css" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" href="//www.statravel.com/static/us_division_web_live/css/jquery.mmenu.all.css"
        type="text/css" />
    <link href="Styles/stacss/assets/img/icons/touch-icon.png" rel="apple-touch-icon-precomposed"
        sizes="180x180" type="image/png" />
    <link href="Styles/stacss/assets/img/icons/favicon.png" rel="shortcut icon" type="image/png" />
    <link href="Styles/stacss/assets/img/icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="Styles/stacss/assets/css/layout.css" rel="stylesheet" type="text/css" />
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
            $('#txtRtPanel').redactor({ iframe: true, minHeight: 200 });

            $("input[class=rdrtID]").attr('name', "img");

            //Edit banner images
            $(".editbtn1").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });
            $(".editbtn6").click(function () {
                $("#divHeading").slideToggle("slow");
            });
            $(".editbtn7").click(function () {
                $("#divContent").slideToggle("slow");
            });

            //----------Edit heading---------//
            $(".editbtn6").click(function () {
                var value;
                $("#divHeading").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdHead').contents().find('body').html(value);
            });

            $(".editbtn7").click(function () {
                var value;
                $("#divContent").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent').contents().find('body').html(value);
            });


            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRight").hide();
                }
                else if ($(this).attr("rel") == "ContentRightTxt") {
                    $("#ContentRightTxt").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

            //-------Edit right panel images-------//
            $(".editRtPanel").click(function () {
                $("#ContentRight").slideToggle("slow");
                $('.hdnRight').val($(this).attr("rel"));
            });

            //------Save right panel images------//
            $("#btnSaveRtPanel").click(function () {
                var imagename;
                $('div#ContentRight input[type=radio]').each(function () {
                    if ($('.hdnRight').val() == "rtPanelImgHome") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactHome").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgCall") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactCall").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgEmail") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactEmail").attr("src", imagename);
                        }
                    }
                    $("#ContentRight").hide();
                });
            });

            //-------Edit right panel images text-------//
            $(".editRtPanelText").click(function () {
                $("#ContentRightTxt").slideToggle("slow");
                $('.hdnrtPanelTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "contactHomeTxt") {
                    value = $('#contactHomeTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactCallTxt") {
                    value = $('#contactCallTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactEmailTxt") {
                    value = $('#contactEmailTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
            });

            //-----Save right panel text------//
            $(".btnSaveRttxt").click(function () {
                var value = $('textarea[name=txtRtPanel]').val();
                if ($('.hdnrtPanelTxt').val() == "contactHomeTxt") {
                    if (value != "") {
                        $('#contactHomeTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactCallTxt") {
                    if (value != "") {
                        $('#contactCallTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactEmailTxt") {
                    if (value != "") {
                        $('#contactEmailTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                $("#ContentRightTxt").hide();
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <input type='hidden' id='sta-page-responsive' value='true' />
    <div class="sta-mobile" id="sta-mobile-header">
        <a href="#sta-nav-wrap-copy"></a><a class="sta-logo" href="//www.statravel.com/home.htm">
        </a><a class="sta-right" href="#sta-nav-top-copy"></a>
    </div>
    <div id="sta-top-header">
        <div class="setheading-wrap">
            <img src="Styles/stacss/assets/img/topbg.jpg" alt="" class="img-responsive">
        </div>
    </div>
    <div id='sta-full-wrap'>
        <div id='sta-page-wrap'>
            <header class="sta-clearfix sta-header" role="banner" id="sta-header">
                <div class="sta-desktop" itemscope itemtype="http://schema.org/Organization" id="sta-tagline-call-info">
                    <div id="sta-logo">
                        <a itemprop="url" href="//www.statravel.com/home.htm"><span itemprop="name">STA Travel</span></a>
                    </div>
                    <div id="sta-tagline">
                        <span>Start the adventure</span>
                    </div>
                    <div class="sta-clearfix sta-call-info">
                        <p>Toll Free at</p>
                        <h2 itemprop="telephone">800.781.4040</h2>
                        <br>
                        <span></span>
                        <noscript>
                            <h2></h2>
                        </noscript>
                    </div>
                </div>
                <div id="sta-page-overlay"></div>
                <!-- Meganav20140814-421s -->
                <nav class="sta-clear sta-clearfix" id="sta-nav-wrap">
                    <ul class="sta-clearfix sta-navigation" role="navigation" id="sta-nav">
                        <li class="sta-right-menu-heading sta-mobile"> <span>STA Travel</span> </li>
                        <li>
                            <a href="//www.statravel.com/flights.htm">Flights</a>
                            <!-- L1 PageGUID:5c87e8d3-8ded-469e-998f-e0cbb24daca3 -->
                            <ul class="sta-shadow sta-medium-menu sta-mainNavSubMenu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Flights</span>
                                    <p>STA Travel negotiates special flight discounts for Students, Teachers &amp; Travelers under 26. We partner with major airlines like AA, United, Virgin and all the best, to get you the cheapest prices you’ll find on the web - guaranteed. Happy Jetting!</p>
                                    <a href="//www.statravel.com/flights.htm">Search Flights</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Cheap Flights</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/cheap-flights-to-london.htm">Flights to London</a></li>
                                                <li><a href="http://www.statravel.com/cheap-flights-to-barcelona.htm">Flights to Barcelona</a></li>
                                                <li><a href="http://www.statravel.com/cheap-flights-to-paris.htm">Flights to Paris</a></li>
                                                <li><a href="http://www.statravel.com/cheap-flights-to-bangkok.htm">Flights to Bangkok</a></li>
                                                <li><a href="http://www.statravel.com/cheap-flights-to-china.htm">Flights to China</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top Regions</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.co.uk/cheap-flights-to-europe.htm">Europe</a></li>
                                                <li><a href="http://www.statravel.co.uk/cheap-flights-to-latin-america.htm">Latin America</a></li>
                                                <li><a href="http://www.statravel.co.uk/cheap-flights-to-asia.htm">Asia</a></li>
                                                <li><a href="http://www.statravel.co.uk/cheap-flights-to-africa.htm">Africa</a></li>
                                                <li><a href="http://www.statravel.co.uk/cheap-flights-to-australasia.htm">Australasia</a></li>
                                                <li><a href="http://www.statravel.co.uk/cheap-flights-to-north-america.htm">North America</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Airlines</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/united-promo-code.htm">United Airlines</a></li>
                                                <li><a href="//www.statravel.com/virgin-atlantic-student-discount.htm">Virgin Atlantic</a></li>
                                                <li><a href="//www.statravel.com/qantas.htm">Qantas</a></li>
                                                <li><a href="//www.statravel.com/emirates-student-discounts.htm">Emirates</a></li>
                                                <li><a href="//www.statravel.com/british-airways-student-discount.htm">British Airways</a></li>
                                                <li><a href="//www.statravel.com/korean-air-student-discount.htm">Korean Air</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Flights On Sale</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/cheap-flights.htm">Free Discount Card with Flights Over $400</a></li>
                                                <li><a href="http://www.statravel.com/british-airways-student-discount.htm">20% Off Student Flights to Europe</a></li>
                                                <li><a href="http://www.statravel.com/last-minute-deals.htm#flights">Last Minute Deals</a></li>
                                                <li><a href="http://www.statravel.com/book-now-pay-later-special.htm">Our $49 Flight Deposit</a></li>
                                                <li><a href="http://www.statravel.com/price-guarantee.htm">Price Beat Guarantee</a></li>
                                                <li><a href="http://www.statravel.com/blue-ticket.htm">BlueTicket Fares</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <img class="sta-nav-lazy" src="" style="width:221px; height:380px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-medium-flights-221x380.jpg">
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/round-the-world.htm">Round the World</a>
                            <!-- L1 PageGUID:4d194fd9-acae-4639-a68b-57d55003df19 -->
                            <ul class="sta-shadow sta-mainNavSubMenu sta-small-menu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Round the World</span>
                                    <p>Explore the big beautiful world with a Round the World trip. Create your own itinerary and we'll find the multi-stop flights to suit you.</p>
                                    <a href="//www.statravel.com/round-the-world.htm">Explore Round the World</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Sample Itineraries</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/round-the-world-beginner.htm">The Beginner</a></li>
                                                <li><a href="//www.statravel.com/round-the-world-quickie.htm">The Quickie</a></li>
                                                <li><a href="//www.statravel.com/round-the-world-deep-dive.htm">The Deep Dive</a></li>
                                                <li><a href="//www.statravel.com/round-the-world-foodie.htm">The Foodie</a></li>
                                                <li><a href="//www.statravel.com/round-the-world-nature-junkie.htm">The Nature Junkie</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <img class="sta-nav-lazy" src="" style="width:221px; height:188px" data-original="//www.statravel.com/static/us_division_web_live/assets/round-the-world.jpg">
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/hotels-and-hostels.htm">Hotels &amp; Hostels</a>
                            <!-- L1 PageGUID:9a3efc05-5891-42b2-bb53-2de425ce2189 -->
                            <ul class="sta-shadow sta-medium-menu sta-mainNavSubMenu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Hotels &amp; Hostels</span>
                                    <p>Whatever your budget, shoestring to luxury, our hostels &amp; hostels worldwide will keep you booking with ease and traveling in comfort.</p>
                                    <a href="//www.statravel.com/hotels-and-hostels.htm">See all Hotels &amp; Hostels</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top Cities</span></a>
                                            <ul>
                                                <li><a href="http://hotels.statravel.com/templates/457378/hotels/list?pagename=ToSearchResults&amp;lang=en&amp;currency=USD&amp;secureUrlFromDataBridge=https://www.travelnow.com&amp;requestVersion=V2&amp;destination=London,+United+Kingdom">London</a></li>
                                                <li><a href="http://hotels.statravel.com/templates/457378/hotels/list?pagename=ToSearchResults&amp;lang=en&amp;currency=USD&amp;secureUrlFromDataBridge=https://www.travelnow.com&amp;requestVersion=V2&amp;destination=Rome,+Italy">Rome</a></li>
                                                <li><a href="http://hotels.statravel.com/templates/457378/hotels/list?pagename=ToSearchResults&amp;lang=en&amp;currency=USD&amp;secureUrlFromDataBridge=https://www.travelnow.com&amp;requestVersion=V2&amp;destination=Sydney,+Australia">Sydney</a></li>
                                                <li><a href="http://hotels.statravel.com/templates/457378/hotels/list?pagename=ToSearchResults&amp;lang=en&amp;currency=USD&amp;secureUrlFromDataBridge=https://www.travelnow.com&amp;requestVersion=V2&amp;destination=Tokyo,+Japan">Tokyo</a></li>
                                                <li><a href="http://hotels.statravel.com/templates/457378/hotels/list?pagename=ToSearchResults&amp;lang=en&amp;currency=USD&amp;secureUrlFromDataBridge=https://www.travelnow.com&amp;requestVersion=V2&amp;destination=Los+Angeles,+California,+United+States">Los Angeles</a></li>
                                                <li><a href="http://hotels.statravel.com/templates/457378/hotels/list?pagename=ToSearchResults&amp;lang=en&amp;currency=USD&amp;secureUrlFromDataBridge=https://www.travelnow.com&amp;requestVersion=V2&amp;destination=New+York,+New+York,+United+States">New York</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Everyday Discounts</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/about-us.htm#exclusive">Discounts for Travelers under 26</a></li>
                                                <li><a href="http://www.statravel.com/price-guarantee.htm">Price Beat Guarantee</a></li>
                                                <li><a href="http://www.statravel.com/airfare-deposit-program.htm">Book Now Pay Later</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Hotels On Sale</span></a>
                                            <ul>
                                                <li><a href="http://hotels.statravel.com/campaign/page/?campaignId=101037&amp;cid=457378&amp;page=1">Last Minute Hotels</a></li>
                                                <li><a href="http://hotels.statravel.com/campaign/page/?campaignId=100913&amp;cid=457378&amp;page=1">US Hotels on Sale</a></li>
                                                <li><a href="http://hotels.statravel.com/campaign/page/?campaignId=100906&amp;cid=457378&amp;page=1">Winter Fun in the Sun</a></li>
                                                <li><a href="http://hotels.statravel.com/campaign/page/?campaignId=101036&amp;cid=457378&amp;page=1">Cancun &amp; Cabo</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <img class="sta-nav-lazy" src="" style="width:221px; height:380px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-hotels-medium-221x380.jpg">
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/tours-worldwide.htm">Tours</a>
                            <!-- L1 PageGUID:aee808c3-a9a0-4bbe-9170-0108ebece09d -->
                            <ul class="sta-shadow sta-medium-menu sta-mainNavSubMenu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Tours</span>
                                    <p>Search &amp; book adventure tours in destinations worldwide. With thousands of small group adventures and tours for students and young travelers to choose from, start planning your adventure here.</p>
                                    <a href="//www.statravel.com/tours-worldwide.htm">See all Tours</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Tours by Region</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/tours-europe.htm">Europe</a></li>
                                                <li><a href="http://www.statravel.com/tours-asia.htm">Asia</a></li>
                                                <li><a href="http://www.statravel.com/tours-australia-and-new-zealand.htm">Australia &amp; New Zealand</a></li>
                                                <li><a href="http://www.statravel.com/tours-south-america.htm">South America</a></li>
                                                <li><a href="http://www.statravel.com/tours-africa.htm">Africa</a></li>
                                                <li><a href="http://www.statravel.com/tours-central-america.htm">Central America</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top Tours</span></a>
                                            <ul>
                                                <li><a href="http://g.statravel.com/trips/inca-discovery/PID/">Inca Discovery</a></li>
                                                <li><a href="http://g.statravel.com/trips/london-to-rome-adventure/EELR/">London to Rome Adventure</a></li>
                                                <li><a href="http://g.statravel.com/trips/vietnam-on-a-shoestring/AVRV/">Vietnam on a Shoestring</a></li>
                                                <li><a href="http://g.statravel.com/trips/indochina-discovery/ATID/">IndoChina Discovery</a></li>
                                                <li><a href="http://g.statravel.com/trips/sailing-turkey-bodrum-to-fethiye/ETVB/">Sailing Turkey</a></li>
                                                <li><a href="http://g.statravel.com/trips/costa-rica-adventure/CRA/">Costa Rica Adventure</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Tour Types</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/tours-results.htm?a=1">Sailing</a></li>
                                                <li><a href="http://www.statravel.com/tours-festivals.htm">Festivals</a></li>
                                                <li><a href="http://www.statravel.com/volunteering.htm">Volunteer</a></li>
                                                <li><a href="http://www.statravel.com/tours-results.htm?a=10">Beach</a></li>
                                                <li><a href="http://www.statravel.com/tours-cheap.htm">Tours Under $700</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Tours on Sale</span></a>
                                            <ul>
                                                <li><a href="http://g.statravel.com/trips/asia/?tag=Last+Minute+Specials">Up to 30% off Asia Tours</a></li>
                                                <li><a href="http://g.statravel.com/trips/?promotions=Peru+Adventures">20% Off Peru Adventures</a></li>
                                                <li><a href="http://g.statravel.com/trips/?promotions=Explore+North+America">15% Off North America</a></li>
                                                <li><a href="http://g.statravel.com/trips/?promotions=Explore+China">15% China Adventures</a></li>
                                                <li><a href="http://g.statravel.com/trips/?promotions=European+Adventures">15% European Tours</a></li>
                                                <li><a href="http://g.statravel.com/trips/?promotions=Discover+Australia">Discover Australia - 15% Off</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <a href="http://www.statravel.com/spring-sale.htm"><img class="sta-nav-lazy" src="" style="width:221px; height:380px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-creative-221x380.jpg"></a>
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/rail-passes.htm">Rail</a>
                            <!-- L1 PageGUID:dd2f0c59-0026-4c6f-9653-2e33bf252f7c -->
                            <ul class="sta-shadow sta-medium-menu sta-mainNavSubMenu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Rail</span>
                                    <p>Traveling by rail can be the quickest, safest and most comfortable way to get around when you're abroad. Book your rail passes for Europe, Japan, Korea, Australia.</p>
                                    <a href="//www.statravel.com/rail-passes.htm">Book Rail Passes</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Rail By Region</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/europe-rail-passes.htm">Europe</a></li>
                                                <li><a href="https://rail.statravel.com/railpass-two.aspx?cid=1de715bf-5668-41bf-8cdd-71d86919a102">Britrail</a></li>
                                                <li><a href="https://rail.statravel.com/railpass-two.aspx?cid=1dfd18cc-2f8b-4ee3-98e2-2fc583b670e7">Japan Rail</a></li>
                                                <li><a href="https://rail.statravel.com/railpass-two.aspx?cid=ac644d73-839c-45c5-a9b9-aa84a814bf61">Australian Rail</a></li>
                                                <li><a href="https://rail.statravel.com/railpass-two.aspx?cid=965b51b4-ad87-4555-a20e-13b6227b48b5">Korean Rail</a></li>
                                                <li><a href="https://rail.statravel.com/railpass-two.aspx?cid=42736dfc-0669-4d89-9c25-fb820be32f29">New Zealand</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top European Rail Passes</span></a>
                                            <ul>
                                                <li><a href="https://rail.statravel.com/ebca-Eurail-Four-Country-Select-Pass/eurail-four-country-select-pass">Europe Select Passes</a></li>
                                                <li><a href="https://rail.statravel.com/eea3-Eurail-Global-Pass/834f-eurail-global-pass">Eurail Global Passes</a></li>
                                                <li><a href="https://rail.statravel.com/4049-eurail-regional-pass">Eurail Regional Passes</a></li>
                                                <li><a href="//www.statravel.com https://rail.statravel.com/a625-eurail-one-country-pass">Eurail One Country Passes</a></li>
                                                <li><a href="https://rail.statravel.com/european-east-pass-(us)">European East Passes</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Rail Essentials</span></a>
                                            <ul>
                                                <li><a href="https://rail.statravel.com/RailPassSection1.aspx">On Track Protection</a></li>
                                                <li><a href="https://rail.statravel.com/RailPassSection2.aspx">Why Choose a Eurail Pass</a></li>
                                                <li><a href="https://rail.statravel.com/GeneralInformation.aspx">Rail Pass Delivery</a></li>
                                                <li><a href="http://www.statravel.com/sample-rail-itineraries.htm">Sample Rail Journeys</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Rail Adventures</span></a>
                                            <ul>
                                                <li><a href="http://g.statravel.com/trips/russia/?trip_style=Rail">Russia By Rail</a></li>
                                                <li><a href="http://g.statravel.com/trips/mongolia/?trip_style=Rail">Trans-Mongolian Railway</a></li>
                                                <li><a href="http://g.statravel.com/trips/?trip_style=Rail&amp;country=China">China By Rail</a></li>
                                                <li><a href="http://g.statravel.com/trips/india/?trip_style=Rail">India Rail Adventures</a></li>
                                                <li><a href="http://g.statravel.com/trips/epic-trans-siberian-journey/ARVR/2015/">Epic Trans Siberian Journey</a></li>
                                                <li><a href="http://g.statravel.com/trips/switzerland/?trip_style=Rail">Swiss Rail Tours</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <a href="http://www.statravel.com/rail-passes.htm"><img class="sta-nav-lazy" src="" style="width:221px; height:380px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-rail.jpg"></a>
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/travel-insurance.htm">Insurance</a>
                            <!-- L1 PageGUID:863563db-490a-4374-8f09-c18d7bc5cdae -->
                            <ul class="sta-shadow sta-mainNavSubMenu sta-small-menu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Insurance</span>
                                    <p>A must have for all adventurers! Ensure smooth sailing and happy travels when the unexpected hits with travel insurance from STA Travel.</p>
                                    <a href="//www.statravel.com/travel-insurance.htm">Compare our Policies</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Get a Quote</span></a>
                                            <ul>
                                                <li><a href="https://purchase.sevencorners.com/policygroup/HW7C69M/HGPCCP8/">International Travel Insurance</a></li>
                                                <li><a href="https://purchase.sevencorners.com/policygroup/HW7C69K/HGPCCP8/">Explorer Plus Policy Coverage</a></li>
                                                <li><a href="https://purchase.sevencorners.com/policygroup/HW7C69P/HGPCCP8/">Air Only Policy Coverage</a></li>
                                                <li><a href="http://www.statravel.com/isic-explorer-card.htm">ISIC Explorer Insurance</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Insurance Essentials</span></a>
                                            <ul>
                                                <li><a href="http://marketing.statravel.com/Web_site/Insurance/April-STA-Intl-AMT484-May2014.pdf">Full coverage details</a></li>
                                                <li><a href="http://www.statravel.com/travel-insurance-help.htm">Claim details</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <img class="sta-nav-lazy" src="" style="width:221px; height:188px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-small-passport-stamps-221x188.jpg">
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/discount-cards.htm">ISIC Cards</a>
                            <!-- L1 PageGUID:c8f140ad-136d-4a1c-a109-c7955716cbe9 -->
                            <ul class="sta-shadow sta-mainNavSubMenu sta-small-menu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">ISIC Cards</span>
                                    <p>Get access to over 125,000 discounts at home and abroad for students, youths and teachers! Save on airfare, accommodation, shopping and so much more with the International Student ID Card.</p>
                                    <a href="//www.statravel.com/discount-cards.htm">Get a Discount Card</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Discount Cards</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/teacher-discount-card.htm">Teacher Cards</a></li>
                                                <li><a href="//www.statravel.com/youth-travel-card.htm">Youth Cards</a></li>
                                                <li><a href="//www.statravel.com/isic-explorer-card.htm">ISIC Explorer</a></li>
                                                <li><a href="http://www.statravel.com/student-discount-card.htm">ISIC Card</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Offers</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/isic-discounts.htm">Latest Discounts</a></li>
                                                <li><a href="http://www.statravel.com/cheap-flights.htm?WT.ac=&quot;meganav-ISIC-flights&quot;">Free ISIC Card with Flights Over $400</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <img class="sta-nav-lazy" src="" style="width:221px; height:188px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-small-isic-221x188.jpg">
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/travel-destinations.htm">Destinations</a>
                            <!-- L1 PageGUID:58181cdf-179e-4717-9d5f-0ec7a93274e6 -->
                            <ul class="sta-shadow sta-mainNavSubMenu sta-featured-menu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Destinations</span>
                                    <p>Every year we send STA Travel Experts to experience global destinations first-hand. This year, they are visiting Eastern Europe, Italy, New Zealand, India, Ecuador and Barcelona to learn the ins and outs on how to get around, what to see and what to do.</p>
                                    <a href="//www.statravel.com/travel-destinations.htm">See all Destinations</a>
                                </div>
                                <div class="sta-feature-column sta-desktop">
                                    <span class="sta-nav-heading2">Visit the Gold Coast</span>
                                    <a href="//www.statravel.com/visit-gold-coast.htm"><img class="sta-nav-lazy" src="" style="width:214px; height:129px;" data-original="//www.statravel.com/static/us_division_web_live/assets/GCT-featured.jpg"></a>
                                    <p>The Gold Coast is the epitome of fun-loving Australia. Get 25% off amazing experiences and 3 for 2 on Gold Coast accommodation.</p>
                                    <a class="sta-all-link" href="//www.statravel.com/visit-gold-coast.htm">Explore the Gold Coast</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top Regions</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/best-of-europe.htm">Europe</a></li>
                                                <li><a href="http://www.statravel.com/asia-destinations.htm">Asia</a></li>
                                                <li><a href="http://www.statravel.com/south-pacific-destinations.htm">South Pacific</a></li>
                                                <li><a href="http://www.statravel.com/top-destinations.htm">Latin America</a></li>
                                                <li><a href="http://www.statravel.com/north-america-destinations.htm">North America</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top Cities</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/visit-london-england.htm">London</a></li>
                                                <li><a href="http://www.statravel.com/travel-paris.htm">Paris</a></li>
                                                <li><a href="http://www.statravel.com/madrid-travel.htm">Madrid</a></li>
                                                <li><a href="http://www.statravel.com/travel-sydney.htm">Sydney</a></li>
                                                <li><a href="http://www.statravel.com/travel-thailand.htm">Bangkok</a></li>
                                                <li><a href="http://www.statravel.com/travel-japan.htm">Tokyo</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Top Destinations</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/travel-japan.htm">Japan</a></li>
                                                <li><a href="//www.statravel.com/travel-mexico.htm">Mexico</a></li>
                                                <li><a href="//www.statravel.com/travel-peru.htm">Peru</a></li>
                                                <li><a href="//www.statravel.com/travel-south-africa.htm">South Africa</a></li>
                                                <li><a href="//www.statravel.com/travel-thailand.htm">Thailand</a></li>
                                                <li><a href="http://www.statravel.com/top-destinations.htm">See all top destinations</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Destination Guides</span></a>
                                            <ul>
                                                <li><a href="http://destination.statravel.com/visit_britain">United Kingdom</a></li>
                                                <li><a href="http://destination.statravel.com/australia_destination_guide">Australia</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/trip-planning.htm">Planning</a>
                            <!-- L1 PageGUID:8320b4fd-92a9-4dea-86e1-65187b7e5b23 -->
                            <ul class="sta-shadow sta-large-menu sta-mainNavSubMenu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Planning</span>
                                    <p>Got an idea of what to do and where to go, well you best get planning! All the resources you need to get your adventure started are right here.</p>
                                    <a href="//www.statravel.com/trip-planning.htm">Start Planning</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Work Abroad</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/alliance-abroad.htm">Australia Job Placement</a></li>
                                                <li><a href="http://www.bunac.org/usa/interninbritain">Intern in Britian</a></li>
                                                <li><a href="http://www.bunac.org/usa/workaustralia">Australia Working Holiday Visa</a></li>
                                                <li><a href="http://www.bunac.org/usa/worknewzealand">Work in New Zealand</a></li>
                                                <li><a href="http://www.bunac.org/usa/workinireland">Work in Ireland</a></li>
                                                <li><a href="http://blog.geovisions.org/STA-Travel-Au-Pair-Form?utm_campaign=Au+Pair&amp;utm_medium=STA+Travel+Website&amp;utm_source=STA">Be an Au Pair</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Study Abroad</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/study-abroad-programs.htm">Study Abroad Programs</a></li>
                                                <li><a href="//www.statravel.com/globalinks-study-abroad.htm">Global Links</a></li>
                                                <li><a href="//www.statravel.com/isa-study-abroad.htm">ISA</a></li>
                                                <li><a href="//www.statravel.com/study-abroad-basic-information.htm">Basic Info</a></li>
                                                <li><a href="//www.statravel.com/scholarships.htm">Scholarships</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Travel Essentials</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/car-rental.htm">Car Rental</a></li>
                                                <li><a href="//www.statravel.com/air-and-hotel-packages.htm">Vacation Packages</a></li>
                                                <li><a href="//www.statravel.com/stopovers.htm">Stopovers</a></li>
                                                <li><a href="//www.statravel.com/airline-fees-chart.htm">Airline Fees</a></li>
                                                <li><a href="http://www.statravel.com/visas-and-passports.htm">Passports and Visas</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Everyday Discounts</span></a>
                                            <ul>
                                                <li><a href="http://www.statravel.com/about-us.htm#exclusive">Discounts for Travelers under 26</a></li>
                                                <li><a href="http://www.statravel.com/price-guarantee.htm">Price Beat Guarantee</a></li>
                                                <li><a href="http://www.statravel.com/airfare-deposit-program.htm">Book Now Pay Later </a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <a href="http://www.statravel.com/trip-planning.htm"><img class="sta-nav-lazy" src="" style="width:221px; height:570px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-planning-large-221x570.jpg"></a>
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/specials.htm">Deals</a>
                            <!-- L1 PageGUID:98b51229-b5cf-44d3-8584-2ce2f785a657 -->
                            <ul class="sta-shadow sta-mainNavSubMenu sta-featured-menu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Deals</span>
                                    <p>Get the most out of your trip and stretch that dollar when you snap up our hot flight, accommodation, travel &amp; tour sales!</p>
                                    <a href="//www.statravel.com/specials.htm">See all Deals</a>
                                </div>
                                <div class="sta-feature-column sta-desktop">
                                    <span class="sta-nav-heading2">Paint Your World Blue Spring Sale</span>
                                    <a href="//www.statravel.com/spring-sale.htm"><img class="sta-nav-lazy" src="" style="width:214px; height:129px;" data-original="//www.statravel.com/static/us_division_web_live/assets/STA216-Jan-Sale_Tours_for-meganav-214x129.jpg"></a>
                                    <p>Check our amazing offers and save up to 45% on Tours to discover Europe and more!</p>
                                    <a class="sta-all-link" href="//www.statravel.com/spring-sale.htm">See all offers</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">On Sale Now</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/last-minute-deals.htm">Last Minute Deals</a></li>
                                                <li><a href="//www.statravel.com/europe-buy-early-sale.htm">Europe Buy Early</a></li>
                                                <li><a href="http://www.statravel.com/italy-on-sale.htm">Italy On Sale!</a></li>
                                                <li><a href="http://www.statravel.com/british-airways-student-discount.htm">20% Off Student Europe Flights</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Tours on Sale</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/last-minute-tours.htm">Last Minute Tours</a></li>
                                                <li><a href="http://www.statravel.com/18-35-tours.htm">Half Price Europe Tours</a></li>
                                                <li><a href="http://g.statravel.com/trips/africa/?tag=Last+Minute+Specials">Up to 25% Off Africa Tours</a></li>
                                                <li><a href="http://g.statravel.com/trips/south-pacific/?tag=Last+Minute+Specials">Up to 20% Off Australia Tours</a></li>
                                                <li><a href="http://g.statravel.com/trips/central-america/?tag=Last+Minute+Specials">Up to 30% Off Central America</a></li>
                                                <li><a href="http://g.statravel.com/trips/asia/?tag=Last+Minute+Specials">Up to 30% Off Asia Tours</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Everyday Discounts</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/sta-travel-app.htm">Download Our App</a></li>
                                                <li><a href="//www.statravel.com/sweepstakes.htm">Sweepstakes</a></li>
                                                <li><a href="//www.statravel.com/teacher-discounts.htm">Teacher Discounts</a></li>
                                                <li><a href="http://www.statravel.com/about-us.htm#exclusive">Discounts for Travelers under 26</a></li>
                                                <li><a href="http://www.statravel.com/price-guarantee.htm">Price Beat Guarantee</a></li>
                                                <li><a href="http://www.statravel.com/book-now-pay-later-special.htm">Book Now Pay Later</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                            </ul>
                        </li>
                        <li>
                            <a href="//www.statravel.com/spring-break.htm">Spring Break</a>
                            <!-- L1 PageGUID:5447dffd-9fc8-4847-96f0-29125a96ff16 -->
                            <ul class="sta-shadow sta-mainNavSubMenu sta-small-menu">
                                <div class="sta-desp-column sta-desktop">
                                    <span class="sta-nav-heading1">Spring Break</span>
                                    <p>Get the best spring break prices, most popular hotels, cheapest flights and VIP Party Passes with STA Travel. Check out our amazing hotels in Cancun, Punta Cana, and Cabo San Lucas!</p>
                                    <a href="//www.statravel.com/spring-break.htm">Book Spring Break</a>
                                </div>
                                <div class="sta-nav-column">
                                    <div class="sta-topspacer sta-desktop">
                                        &nbsp;
                                    </div>
                                    <div class="sta-single-nav">
                                        <li class="sta-nav-li">
                                            <a href="#" class="sta-nolink"><span class="sta-nav-heading2">Spring Break Destinations</span></a>
                                            <ul>
                                                <li><a href="//www.statravel.com/spring-break-cancun.htm">Spring Break Cancun</a></li>
                                                <li><a href="//www.statravel.com/spring-break-cabo-san-lucas.htm">Spring Break Cabo</a></li>
                                                <li><a href="//www.statravel.com/spring-break-panama-city-beach.htm">Spring Break Panama City Beach</a></li>
                                                <li><a href="//www.statravel.com/spring-break-punta-cana.htm">Spring Break Punta Cana</a></li>
                                            </ul>
                                        </li>
                                    </div>
                                </div>
                                <div class="sta-img-column sta-desktop">
                                    <img class="sta-nav-lazy" src="" style="width:221px; height:188px" data-original="//www.statravel.com/static/us_division_web_live/assets/meganav-spring-break-girl-smiling-221x188.jpg">
                                </div>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </header>
            <!-- END: STA Global Header -->
            <main class="starail-Wrapper starail-Wrapper--main" role="main">
                <div class="starail-Grid starail-Grid--mobileFull">
                    <div class="starail-Grid-col starail-Grid-col--nopadding ">
                        <div class="starail-Grid--mobileFull imgfullbox editbaner">
                            <div class="editbtn1"><a class="edit-btn" href="#">Edit</a></div>
                            <div class="starail-Grid-col starail-Grid-col--nopadding">
                                <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                                    <img src='http://admin.1tracktest.com/CMSImages/Japan-electric-train.png' class="starail-HomeHero-img dots-header"
                                         id="imgMainBanner" runat="server" alt="." border="0" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Section starail-Section--nopadding">
                            <div style="display:none;">
                                <div class="starail-Switcher">

                                    <ul class="starail-Switcher-tabs">
                                        <li class="starail-Switcher-tab starail-Switcher-tab--active">
                                            <a href="#starail-passes" class="js-starail-Switcher-trigger">Rail Passes</a>
                                        </li>
                                        <li class="starail-Switcher-tab">
                                            <a href="#starail-tickets" class="js-starail-Switcher-trigger">Rail Tickets</a>
                                        </li>
                                    </ul>
                                    <div id="starail-passes" class="starail-Switcher-content">
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>
                                            <img src="http://lorempixel.com/220/220/city" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>
                                            <img src="http://lorempixel.com/220/220/nature" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>
                                            <img src="http://lorempixel.com/220/220/city" />
                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">From £89/pp! <br /> Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">
                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div id="starail-tickets" class="starail-Switcher-content starail-Switcher-content--hidden">
                                        <div class="starail-SearchTickets">
                                            <h2 class="starail-SearchTickets-title starail-u-alpha">Tell us <span class="starail-u-hideMobile">more </span>about your trip</h2>
                                            <div class="starail-Box starail-Box--whiteMobile starail-Box--noPaddingBottomMobile">
                                                <div action="#" method="post" class="starail-Form starail-Form--onBoxNarrow starail-SearchTickets-form">
                                                    <div class="starail-Form-row starail-SearchTickets-destination">
                                                        <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--start">
                                                            <label for="starail-startlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">From</label>
                                                            <div class="starail-SearchTickets-destinationInput">
                                                                <input class="starail-Form-input" type="text" name="starail-startlocation" id="starail-startlocation" placeholder="Enter a start location" value="" />
                                                            </div>
                                                        </div>
                                                        <div class="starail-DestinationIcon">
                                                            <div class="starail-DestinationIcon-line"></div>
                                                            <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled"></div>
                                                            <div class="starail-DestinationIcon-circle"></div>
                                                        </div>
                                                        <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--end">
                                                            <label for="starail-endlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">To</label>
                                                            <div class="starail-SearchTickets-destinationInput">
                                                                <input class="starail-Form-input" type="text" name="starail-endlocation" id="starail-endlocation" placeholder="Enter a destination" value="" />
                                                            </div>
                                                        </div>
                                                        <a href="#" class="starail-SearchTickets-switch-trigger"><i class="starail-Icon starail-Icon-reverse"></i><span class="starail-u-visuallyHidden">Switch direction</span></a>
                                                    </div>
                                                    <div class="starail-Form-row">
                                                        <label for="" class="starail-Form-label starail-u-invisible starail-u-visuallyHiddenMobile">Journey Type</label>
                                                        <div class="starail-u-cf starail-Form-switchRadioGroup">
                                                            <label for="starail-oneway">
                                                                <input type="radio" id="starail-oneway" name="journey" value="one way" checked>
                                                                <span>One way</span>
                                                            </label>
                                                            <label for="starail-return">
                                                                <input type="radio" id="starail-return" name="journey" value="return">
                                                                <span>Return</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-u-cf">
                                                        <label for="leaving" class="starail-Form-label">Leaving</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                                            <div class="starail-Form-datePicker">
                                                                <input class="starail-Form-input" name="leaving" id="leaving" placeholder="DD/MM/YYYY" value="" />
                                                                <i class="starail-Icon-datepicker"></i>
                                                            </div>
                                                            <select class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last" name="outbound-time" id="outbound-time">
                                                                <option value="09:00">09:00</option>
                                                                <option value="09:15">09:15</option>
                                                                <option value="09:30">09:30</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="starail-Form-row starail-u-cf">
                                                        <label for="returning" class="starail-Form-label">Returning</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                                            <div class="starail-Form-datePicker">
                                                                <input class="starail-Form-input" type="text" name="returning" id="returning" placeholder="DD/MM/YYYY" value="" />
                                                                <i class="starail-Icon-datepicker"></i>
                                                            </div>
                                                            <select class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last" name="inbound-time" id="inbound-time">
                                                                <option value="09:00">09:00</option>
                                                                <option value="09:15">09:15</option>
                                                                <option value="09:30">09:30</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                                                        <label for="" class="starail-Form-label">Who's going?</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-adult" id="starail-adult">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Adults (26-65 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-children" id="starail-children">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-children">Children (under 17 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-youths" id="starail-youths">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Youths (17-25 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-seniors" id="starail-seniors">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Seniors (over 66 at time of travel)</label>
                                                            </div>
                                                        </div><!-- starail-Form-selectGroup -->
                                                    </div>
                                                    <hr />
                                                    <div class="starail-Form-row">
                                                        <button type="submit" class="starail-Button starail-Form-button starail-Form-button--primary">Search Tickets</button>
                                                    </div>
                                                </div><!-- .starail-Form -->
                                            </div>
                                            <div class="starail-Box starail-Box--whiteMobile starail-ContactForm">
                                                <h3>We don't have that journey online</h3>
                                                <p>Call us now on <strong>0871 984 7783</strong> to book! Or fill in some details and we'll call you back:</p>
                                                <div action="#" method="post" class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
                                                    <div class="starail-Form-row">
                                                        <label for="starail-firstname" class="starail-Form-label">Name <span class="starail-Form-required">*</span></label>

                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input class="starail-Form-input" type="text" name="starail-firstname" id="starail-firstname" placeholder="First name" value="" />
                                                            </div>
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input class="starail-Form-input" type="text" name="starail-lastname" id="starail-lastname" placeholder="Last name" value="" />
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <label for="starail-email" class="starail-Form-label">Email <span class="starail-Form-required">*</span></label>
                                                        <div class="starail-Form-inputContainer">
                                                            <input class="starail-Form-input starail-Form-error" type="email" name="starail-email" id="starail-email" placeholder="Email" value="" /><!-- .starail-Form-error class used for errors -->
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <label for="starail-phone" class="starail-Form-label">Phone <span class="starail-Form-required">*</span></label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div><input class="starail-Form-input" type="tel" name="starail-phone" id="starail-phone" placeholder="Phone" value="" /></div>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-ContactForm-contactPreference">
                                                        <label for="" class="starail-Form-label starail-ContactForm-contactPreferenceLabel">How would you prefer us to contact you?</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input type="radio" id="starail-preference-email" name="starail-contact-preference" value="email" checked>
                                                                <label for="starail-preference-email">Email</label>
                                                            </div>
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input type="radio" id="starail-preference-phone" name="starail-contact-preference" value="phone">
                                                                <label for="starail-preference-phone">Phone</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="starail-Form-row">
                                                        <button type="submit" class="starail-Button starail-Form-button">Help Me Book!</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Box starail-Box--whiteMobile starail-ContactForm ">
                                <div style="width:100%;" id="MainContent_divSta ">
                                </div>
                                <h3 class="editbaner">
                                    <div id="ContentHead" runat="server">
                                        <h1><span id="lblHead">Contact us at STA Travel<br></span></h1>

                                    </div>
                                    <div class="editbtn6"><a class="edit-btn" href="#">Edit</a></div>
                                </h3>
                               
                                <div class="editbaner" >
                                    <div id="ContentText" runat="server"><p>
                                       If you have any rail related question about any of our products featured on these pages, please do not hesitate to contact us.</span>
                                    </p></div>
                                        <div class="editbtn7"><a class="edit-btn" href="#">Edit</a></div>
                                </div>
                                <br>
                                <p></p>
                                <div class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
                                     <div class="floatt starail-u-size8of12 contact-img">
                                        <img src="Styles/stacss/assets/img/contactform.jpg">
                                    </div>
                                      <div class="floatt starail-u-size4of12 pl10 contact-txt">
                                       <div class="address-block" id="addBlock" runat="server">


                                            <div class="fullrow box1">
                                                <div id="rtPanelImgHome" class="cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="rtPanelImgHome" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactHome" runat="server" src="http://admin.1tracktest.com/CMSImages/home-blue.png">
                                                            <input type="hidden" id="Hidden3" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
												<div class="con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="contactHomeTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactHomeTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span  id="lblRtPanelHead"><a href="http://www.statravel.com.au/stores.htm">Contact your nearest store</a></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="fullrow box2">
                                                <div id="rtPanelImgCall" class=" cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="rtPanelImgCall" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactCall" runat="server" src="http://admin.1tracktest.com/CMSImages/icon-call-blue.png">
                                                            <input type="hidden" id="Hidden6" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="contactCallTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactCallTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span id="Label1">phone: 134 782</span>
                                                        </span>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="fullrow box3">
                                                <div id="rtPanelImgEmail" class=" cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="rtPanelImgEmail" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactEmail" runat="server" src="http://admin.1tracktest.com/CMSImages/icon-email.png">
                                                            <input type="hidden" id="Hidden2" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="contactEmailTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactEmailTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span id="Label3">email: <a href="mailto:webquotes@statravel.com.au">webquotes@statravel.com.au</a></span>
                                                        </span><br>
                                                    </div>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <hr>
                                    <img src="Styles/stacss/assets/img/submitbtn.jpg" alt="">
                                    <div class="starail-Form-row" style="display:none;">
                                        <input type="submit" class="starail-Button starail-Form-button" id="MainContent_btnSubmit" onclick="return SaveContactUs();WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$btnSubmit&quot;, &quot;&quot;, true, &quot;vs&quot;, &quot;&quot;, false, false))" value="SUBMIT" name="ctl00$MainContent$btnSubmit" data-ga-category="http://staau.1tracktest.com/Contact-Us" data-ga-action="click" data-ga-label="staau.1tracktest.com">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </main>
        </div>
    </div>
    <footer id="sta-footer">
        <section class="sta-footer-wrap">
            <section class="sta-clearfix" id="sta-footer-spread-social">
                <h2>Like STA Travel? Spread the word!</h2>
                <div class="sta-social-buttons">
                    <a href="https://www.facebook.com/STATravelUS" type="button" data-role="none" target="social"><img alt="" title="Facebook" src="//www.statravel.com/static/us_division_web_live/assets/social-facebook.png"></a>
                    <a href="https://twitter.com/statravelus" type="button" data-role="none" target="social"><img alt="" title="Twitter" src="//www.statravel.com/static/us_division_web_live/assets/social-twitter.png"></a>
                    <a href="http://instagram.com/statravelus/" type="button" data-role="none" target="social"><img alt="" title="Instagram" src="//www.statravel.com/static/us_division_web_live/assets/social-instagram.png"></a>
                    <a href="https://www.youtube.com/channel/UCsV991FCc_7Ak5YZDelTWPg" type="button" data-role="none" target="social"><img alt="" title="Youtube" src="//www.statravel.com/static/us_division_web_live/assets/social-youtube.png"></a>
                    <a href="https://plus.google.com/u/0/+statravel/posts" type="button" data-role="none" target="social"><img alt="" title="Google+" src="//www.statravel.com/static/us_division_web_live/assets/social-google.png"></a>
                    <a href="http://www.pinterest.com/statravelus/" type="button" data-role="none" target="social"><img alt="" title="Pinterest" src="//www.statravel.com/static/us_division_web_live/assets/social-pinterest.png"></a>
                    <a href="http://www.statravel.com/blog/" type="button" data-role="none" target="social"><img alt="" title="STA Travel Blog" src="//www.statravel.com/static/us_division_web_live/assets/web-social-icon-blog-30x30.png"></a>
                </div>
            </section>
            <section class="sta-footer-panels">
                <section class="sta-mobile" id="sta-footer-panel1"></section>
                <section id="sta-footer-panel2"></section>
                <section id="sta-footer-panel3"></section>
                <div class="sta-clearfix"></div>
                <div class="sta-small-print sta-desktop">
                    <p><span style="FONT-SIZE: 9px">AIRFARE TERMS AND CONDITIONS:<br>Sample airfares posted on statravel.com are per person and include all applicable taxes, fees and surcharges including, but not limited to: September 11th Security Fee of up to $5.00 for each flight segment originating at a U.S. airport; Passenger Facility Charges of up to $18, depending on itinerary; Federal Segment Fees of $3.90 per segment; airline-imposed fuel surcharges of up to $800; foreign and U.S. Government-imposed charges of up to $400 per international round-trip flight; and STA Travel Booking Fees of up to $20; Taxes, fees and surcharges vary depending on routing, cabin of service, and destination. A flight segment is defined as one takeoff and one landing. Fares are subject to availability and change without notice.<br><br>Any airfares shown on this page reflect airfare recently seen and/or purchased at statravel.com and were valid at time of publication. Prices may vary based on availability, routing, fluctuations in currency, and day of week. If the listed airfare is not available, different fares and/or airlines may be offered. Any airline logos shown, if applicable, reflect the ticketing carrier for recently seen and/or purchased fare. Many air carriers offer itineraries include codeshare service with other air carriers, including foreign carriers and/or commuter carriers operating turboprop aircraft. Any codeshare service, if applicable, will be display on the flights results page immediately after an airfare search.<br><br>STA Travel provides air tickets for customers of all ages; however, some of our airfare is available just to full-time students, Teachers at accredited schools, and youth travelers under the age of 26. These special fares may have age restrictions and/or other eligibility requirements including possession of valid ID Cards. Additional airline imposed fees for optional services including </span><a href="http://www.statravel.com/airline-fees-chart.htm"><span style="FONT-SIZE: 9px">baggage</span></a><span style="FONT-SIZE: 9px">, seat assignments, meals and other products may apply and are payable at check-in.</span></p>
                </div>
                <div class="sta-mobile sta-small-print">
                    <a href="//www.statravel.com/home.htm">View Full Site</a> |
                    <a class="sta-mobile-call" href="tel:8007814040">8007814040</a>
                </div>
                <div class="sta-accreditation-bar sta-desktop">
                    <ul>
                        <li class="sta-logo"><a href="https://www.arccorp.com"><img border="0" width="83" height="34" src="//www.statravel.com/static/us_division_web_live/assets/ARC_logo_blue.jpg"></a></li>
                        <li class="sta-logo"><a href="http://www.iata.org/"><img border="0" width="44" height="31" src="//www.statravel.com/static/us_division_web_live/assets/logo_iata.png"></a></li>
                    </ul>
                </div>
            </section>
            <section style="display:none;" id="sta-footer-resources">
                <section id="sta-footer-newsletter">
                    <h4 class="sta-btn-collapse-mini sta-no-padding">Sign Up To Our Newsletter<span>To Get The Latest Deals</span></h4>
                    <!-- b -->
                    <div class="sta-mobile-extra">
                        <div id="sta-footer-subscribe-form">
                            <input type="hidden" value="http://www.statravel.com/preference-center.htm?email=" id="sta-newsletterSecondPage">
                            <input type="hidden" value="us03" id="sta-marketCodeNewsletter">
                            <input type="text" name="footer-email" placeholder=" EMAIL ADDRESS" class="sta-footer-email" id="sta-footer-email">
                            <input type="submit" name="submit" value="Subscribe to Newsletter" class="sta-btn sta-btn-secondary" id="sta-footer-subscribe-btn">
                        </div>
                    </div>
                </section>
                <section id="sta-footer-contact-us">
                    <h4>Contact Us</h4>
                    <div class="sta-contact-box">
                        <ul>
                            <li class="sta-contact-phone"><a href="tel:8007814040" id="sta-telf">Call 800 781 4040</a></li>
                            <li class="sta-contact-email"><a href="//www.statravel.com/get-quote.htm">Get A Quote</a></li>
                            <li class="sta-contact-store"><a href="//www.statravel.com/find-a-store.htm">Store Finder</a></li>
                            <li class="sta-contact-appt"><a href="//www.statravel.com/call-me-back.htm">Book an Appointment</a></li>
                        </ul>
                    </div>
                </section>
                <section id="sta-footer-specialist-travel">
                    <h4>STA Travel Solutions</h4>
                    <ul>
                        <li><a href="http://www.statravel.com/solutions/group-travel.htm">Groups &amp; Programs</a></li>
                        <li><a href="http://www.statravel.com/solutions/the-isic-card-and-your-program.htm">ISIC Card Issuing Offices</a></li>
                        <li><a href="http://www.statravel.com/solutions/corporate-travel.htm">Corporate Travel</a></li>
                    </ul>
                </section>
                <section id="sta-footer-request-brochure">
                    <a href="//www.statravel.com/sta-travel-app.htm"><h4>Get our Mobile App</h4></a>
                </section>
                <section id="sta-footer-about-us">
                    <h4>About STA Travel</h4>
                    <ul>
                        <li><a href="//www.statravel.com/about-us.htm">About Us</a></li>
                        <li><a href="//www.statravel.com/media-press-room.htm">Media Hub</a></li>
                        <li><a href="//www.statravel.com/terms-of-use.htm">Term and Conditions</a></li>
                        <li><a href="//www.statravel.com/privacy-policy.htm">Privacy Policy</a></li>
                        <li><a href="//www.statravel.com/become-affiliate.htm">Become an affiliate</a></li>
                        <li><a href="//www.statravel.com/jobs.htm">Work with US</a></li>
                        <li><a href="http://www.staagents.com">Travel Agents</a></li>
                        <li><a href="//www.statravel.com/sitemap.htm">Site Map</a></li>
                    </ul>
                </section>
                <section id="sta-footer-support">
                    <h4>SUPPORT</h4>
                    <ul>
                        <li><a href="//www.statravel.com/travel-help.htm">Travel Help</a></li>
                        <li><a href="//www.statravel.com/travel-in-america.htm">Customer Service</a></li>
                    </ul>
                </section>
                <section class="sta-desktop" id="sta-footer-blog">
                    <a href="http://www.statravel.com/blog/"> <img alt="STA Travel Blog" src="//www.statravel.com/static/us_division_web_live/assets/Web-Social-Icons-BLOG-38x38.png"> <h4>STA Travel Blog</h4> <p>Explore the STA Travel Blog</p> </a>
                </section>
            </section>
        </section>
    </footer>
    <script>        window.jQuery || document.write('<script src="Styles/stacss/assets/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/partner.js"
        type="text/javascript"></script>
    <script type="text/javascript" src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.hammer.min.js"></script>
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.mmenu-partner.js"
        type="text/javascript"></script>
    <script src="Styles/stacss/assets/js/main.min.js"></script>
    <div class="starail-Lightbox" id="starail-ticket-info">
        <div class="starail-Lightbox-content">
            <div class="starail-Lightbox-closeContainer">
                <a href="#" class="starail-Lightbox-close js-lightboxClose"><i class="starail-Icon starail-Icon-close">
                </i></a>
            </div>
            <div class="starail-Lightbox-content-inner">
                <div class="starail-Lightbox-text">
                    <h3 class="starail-Lightbox-textTitle">
                        Ticket Info</h3>
                    <h4>
                        Availability</h4>
                    <p>
                        Yes</p>
                    <h4>
                        Class &amp; seats</h4>
                    <p>
                        Class AW, Seat 1-10</p>
                    <h4>
                        Spring Promo</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                        minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip
                        ex ea commodo consequat.</p>
                    <p>
                        Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                        nisl ut aliquip ex ea commodo consequat.</p>
                    <p>
                        Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                        nisl ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 203px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="padding-right: 50px; float: right; padding-top: 20px;">
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentRight" class="PopUpSampleIMG" style="display: none; width: 300px;
        height: auto; top: 697px; left: 190px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll">
            <asp:DataList ID="dtRtPanel" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="18px"
                        Height="18px" CssClass="bGray" />
                    <br />
                    <input id="rdrtID" class="rdrtID" runat="server" type="radio" name="img" value='<%#Eval("ImagePath")%>'
                        style="margin: 2px 0 2px 0" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="float: right; padding-top: 10px; padding-right: 20px;">
            <input type="button" id="Button2" value="Cancel" class="button btnClose" rel="ContentRight" />
            <input type="button" id="btnSaveRtPanel" value="Save" class="button btnSaveRtPanel"
                rel="ContentRight" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentRightTxt" style="display: none; left: 90px;
        top: 697px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtRtPanel" name="txtRtPanel" cols="10" rows="5" class="rdRight"></textarea>
                        <input type="hidden" class="hdnrtPanelTxt" id="Hidden7" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="float-rt" style="float: right; padding-top: 10px; padding-right: 20px;">
            <tr>
                <td>
                    <input type="button" id="Button3" value="Cancel" class="button btnClose" rel="ContentRightTxt" />
                    <input type="button" id="btnSaveRttxt" value="Save" class="button btnSaveRttxt" rel="ContentRightTxt" />
                </td>
            </tr>
            </table>
        </div>
    </form>
</body>
</html>
