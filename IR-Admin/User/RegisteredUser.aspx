﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="true" CodeBehind="RegisteredUser.aspx.cs" Inherits="IR_Admin.User.RegisteredUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .panes div
        {
            display: block;
            font-size: 14px;
            padding: 0 1px;
        }
        .ajax__calendar .ajax__calendar_day
        {
            border: 1px solid #FFFFFF;
            height: 16px;
            line-height: 16px;
            padding: 0;
            font-size: 12px;
        }
        .ajax__calendar_title
        {
            cursor: pointer;
            font-weight: bold;
            height: 22px;
            line-height: 22px;
            margin-left: 15px;
            margin-right: 15px;
            font-size: 12px;
        }
        
        .ajax__calendar .ajax__calendar_dayname
        {
            border-bottom: 1px solid #F5F5F5;
            height: 22px;
            line-height: 22px;
            font-size: 12px;
        }
        .ajax__calendar .ajax__calendar_footer
        {
            border-top: 1px solid #F5F5F5;
            height: 22px;
            line-height: 22px;
            font-size: 12px;
        }
        .ajax__calendar_month
        {
            cursor: pointer;
            height: 40px;
            line-height: normal !important;
            overflow: hidden;
            text-align: center;
            width: 35px;
            padding: 0px !important;
            font-size: 12px;
        }
        .ajax__calendar .ajax__calendar_year
        {
            border: 1px solid #FFFFFF;
            padding: 0px !important;
            height: 40px;
            line-height: normal !important;
            overflow: hidden;
            text-align: center;
            width: 35px;
            font-size: 12px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {

            $("#MainContent_txtDOB").keypress(function (event) { event.preventDefault(); });

            if ('<%=tab%>' == '1') {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            } else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }

            $(".list a").click(function (e) {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                $('input:text').val('');
                if ($(this).attr("id") == 'aList') {
                    document.getElementById('MainContent_divlist').style.display = 'Block';
                    document.getElementById('MainContent_divNew').style.display = 'none';
                }
                else {

                    document.getElementById('MainContent_divlist').style.display = 'none';
                    document.getElementById('MainContent_divNew').style.display = 'Block';
                }
            });

            function clearControls() {
                document.getElementById('MainContent_txtCode').value = '';
                document.getElementById('MainContent_txtName').value = '';
                document.getElementById('MainContent_divlist').value = '';
                document.getElementById('MainContent_divlist').value = '';
            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
        function checkDate(sender) {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnAffUserID" runat="server" />
    <h2>
        Registered User</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li id="lilist"><a id="aList" href="RegisteredUser.aspx" class="current">List</a></li>
            <li id="linew"><a id="aNew" href="#" class="">New</a> </li>
        </ul>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv">
                        <table class="searchDiv" width="100%">
                            <tr>
                                <td>
                                    UserName:
                                    <asp:TextBox runat="server" ID="txtSearchUserName" MaxLength="50"></asp:TextBox>
                                </td>
                                <td>
                                    Email Address:
                                    <asp:TextBox runat="server" ID="txtSearchEmail" MaxLength="50"></asp:TextBox>
                                </td>
                                <td>
                                    Country:
                                    <asp:DropDownList ID="ddlSearchCountry" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td style="display: none;">
                                    Site:
                                    <asp:DropDownList ID="ddlSearchSite" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td style="vertical-align: bottom;">
                                    <asp:Button runat="server" ID="btnSearchSubmit" Text="Submit" CssClass="button" OnClick="btnSearchSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="grvAffiliateUser" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="20" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"
                            OnRowCommand="grvAffiliateUser_OnRowCommand" AllowPaging="True" OnPageIndexChanging="grvAffiliateUser_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <%#Eval("ContactEmail")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country">
                                    <ItemTemplate>
                                        <%#Eval("Country")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Remove"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            Visible="false" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>' ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server">
                    <table width="100%">
                        <tr>
                            <td style="vertical-align: top;">
                                <table class="tblMainSection" width="100%">
                                    <tr>
                                        <td class="col" width="25%">
                                            Site Name:
                                        </td>
                                        <td class="col" width="25%">
                                            <asp:DropDownList ID="ddlSite" ClientIDMode="Static" runat="server" Width="202px" />
                                            &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                ControlToValidate="ddlSite" ValidationGroup="affForm" InitialValue="-1" />
                                        </td>
                                        <td class="col" width="25%">
                                            &nbsp;
                                        </td>
                                        <td class="col" width="25%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                           <b> User Details</b>
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            First Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtName" runat="server" />
                                            <asp:RequiredFieldValidator ID="txtNameRequiredFieldValidator" runat="server" ControlToValidate="txtName"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                        <td class="col">
                                            Last Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtLastName" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Contact Email
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtContactEmail" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtContactEmail"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                            <asp:RegularExpressionValidator ID="txtemailregexp" runat="server" CssClass="valdreq"
                                                ValidationGroup="affForm" ControlToValidate="txtContactEmail"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                        </td>
                                        <td class="col">
                                            Phone
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtPhone" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Date of Birth
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtDOB" runat="server" />
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDOB"
                                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px;
                                                0 0;" />
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Password
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" MaxLength="50" />
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                           <b> Billing address</b>
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                    </tr>
                                     <tr>
                                        <td class="col">
                                            Line1
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtLine1" runat="server" />
                                                 <asp:RequiredFieldValidator ID="txtLine1RequiredFieldValidator" runat="server" ControlToValidate="txtLine1"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                        <td class="col">
                                            Line2
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtLine2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Town
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtCity" runat="server" />
                                        </td>
                                        <td class="col">
                                            County/state
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtState" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Postal/Zip code
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtPostCode" runat="server" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPostCode"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                        <td class="col">
                                            Country
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqCountry" runat="server" ControlToValidate="ddlCountry"
                                                InitialValue="0" CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Is Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkisActive" runat="server" />
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                        </td>
                                        <td class="col">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_OnClick"
                                                Text="Submit" Width="89px" ValidationGroup="affForm" />
                                            &nbsp;
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                        </td>
                                         <td class="col">
                                            &nbsp;
                                        </td>
                                        <td class="col">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
