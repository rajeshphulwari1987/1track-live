﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

namespace IR_Admin.User.Reports
{
    public partial class UserReport : Page
    {
        public string currency = "$";
        private bool _isExport;
        readonly private ManageOrder _master = new ManageOrder();
        readonly private db_1TrackEntities db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
            BindGrid(_siteId);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _siteId = Master.SiteID;
                BindGrid(_siteId);
            }
        }

        void BindGrid(Guid siteId)
        {
            try
            {
                if (_isExport)
                    btnExportToExcel.Visible = true;
                var list = _master.GetCRMUserList(siteId);
                if (!string.IsNullOrEmpty(txtEmail.Text))
                    list = list.Where(x => x.EMAIL.ToLower() == txtEmail.Text.ToLower()).ToList();
                if (!string.IsNullOrEmpty(txtFirstName.Text))
                    list = list.Where(x => x.FIRSTNAME.ToLower().StartsWith(txtFirstName.Text.ToLower())).ToList();
                grdOrders.DataSource = list;
                grdOrders.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated!");// Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void grdOrders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdOrders.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGrid(_siteId);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _siteId = Master.SiteID;
            _isExport = true;
            BindGrid(_siteId);
        }
        
        #region Export To Excel
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            grdOrders.AllowPaging = false;
            _siteId = Master.SiteID;
            BindGrid(_siteId);
            grdOrders.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#d06b95");
            Context.Response.ClearContent();
            Context.Response.ContentType = "application/ms-excel";
            Context.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xls", "CRM User Reports"));
            Context.Response.Charset = "";
            var stringwriter = new System.IO.StringWriter();
            var htmlwriter = new HtmlTextWriter(stringwriter);

            grdOrders.RenderControl(htmlwriter);
            Context.Response.Write(stringwriter.ToString());
            Context.Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        #endregion
         
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}