﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data;

namespace IR_Admin.User
{
    public partial class ViewProfile : System.Web.UI.Page
    {
        public string name = string.Empty;
        readonly Masters _Master = new Masters();
        db_1TrackEntities db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {

            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //branch tree view
                BindBranchTreeView();
                //dropdowns
                CommonddlControls();
                //fill menu
                FillMenu(); 
                _SiteID = Master.SiteID; 
                //Edit Mode
                //if ((Request["edit"] != null) && (Request["edit"] != ""))
                if(Session["UserID"]!=null)
                {
                    BindUserListForEdit(Guid.Parse(Session["UserID"].ToString()));
                } 
            }
        } 
       
        protected void BindBranchTreeView()
        {
            db_1TrackEntities db = new db_1TrackEntities();
            var query = (from m in db.tblBranches
                         where m.IsActive == true
                         select new { ID = m.TreeID, ParentID = m.TreeParentBranchID, Text = m.OfficeName }).ToList();

            DataSet dataSet = new DataSet();
            dataSet.Tables.Add("Table");
            dataSet.Tables[0].Columns.Add("ID", typeof(Int32));
            dataSet.Tables[0].Columns.Add("ParentID", typeof(Int32));
            dataSet.Tables[0].Columns.Add("Text", typeof(string));

            foreach (var item in query)
            {
                DataRow row = dataSet.Tables[0].NewRow();
                row["ID"] = item.ID;
                if (item.ParentID != null)
                {
                    row["ParentID"] = item.ParentID;
                }
                row["Text"] = item.Text;
                dataSet.Tables[0].Rows.Add(row);
            }

            trBranch.DataSource = new HierarchicalDataSet(dataSet, "ID", "ParentID");
            trBranch.DataBind();
        }

        public void CommonddlControls()
        {
            try
            {
                //Region dropdownlist
                ddlRoles.DataSource = _Master.GetRolesList();
                ddlRoles.DataTextField = "RoleName";
                ddlRoles.DataValueField = "RoleID";
                ddlRoles.DataBind();

                //Branch Region
                ddlBranch.DataSource = _Master.GetAllBranchlist();
                ddlBranch.DataTextField = "PathName";
                ddlBranch.DataValueField = "ID";
                ddlBranch.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        private void FillMenu()
        {
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                TreeNode trCat = new TreeNode();
                trCat.Text = oSite.DisplayName;
                trCat.Value = oSite.ID.ToString();
                trCat.SelectAction = TreeNodeSelectAction.None;
                trSites.Nodes.Add(trCat);
            }
        }

        public void BindUserListForEdit(Guid ID)
        {
            try
            {
                var result = _Master.GetUserListEdit(ID);
                name = result.UserName;
                txtUserName.Text = result.UserName;
                txtSurname.Text = result.Surname;
                txtForename.Text = result.Forename;
                txtEmail.Text = result.EmailAddress;
                chkActive.Checked = Convert.ToBoolean(result.IsActive) ? true : false;
                chkAgent.Checked = Convert.ToBoolean(result.IsAgent) ? true : false;
                txtUserNote.Text = result.Note;
                if (ddlBranch.Items.FindByValue(result.BranchID.ToString()) != null)
                {
                    ddlBranch.Items.FindByValue(result.BranchID.ToString()).Selected = true;
                }

                if (ddlTitle.Items.FindByValue(result.Salutation.ToString()) != null)
                {
                    ddlTitle.Items.FindByValue(result.Salutation.ToString()).Selected = true;
                }

                if (ddlRoles.Items.FindByValue(result.RoleID.ToString()) != null)
                {
                    ddlRoles.Items.FindByValue(result.RoleID.ToString()).Selected = true;
                }

                // Look up  Sites
                var LookUpSites = db.tblAdminUserLookupSites.Where(x => x.AdminUserID == ID).ToList();
                foreach (var Lsites in LookUpSites)
                {
                    foreach (TreeNode Pitem in trSites.Nodes)
                    {
                        if (Guid.Parse(Pitem.Value.ToString()) == Lsites.SiteID)
                            Pitem.Checked = true;
                    }
                }

                //Select Branch
                Guid BranchID = Guid.Parse(result.BranchID.ToString());
                var BranchName = db.tblBranches.Where(x => x.ID == BranchID).SingleOrDefault();
                if (BranchName != null)
                {
                    txtddl.Text = BranchName.OfficeName;
                    hdnddlValue.Value = BranchName.TreeID.ToString();
                }
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(hdnddlValue.Value))
                {
                    ShowMessage(2, "Please select office.");
                    return;
                }
                Guid branchId = _Master.GetBranchGUID(int.Parse(hdnddlValue.Value));

                AdminUser _AdminUser = new AdminUser();
                _AdminUser.UserName = txtUserName.Text;
                _AdminUser.Salutation = Convert.ToInt32(ddlTitle.SelectedItem.Value);
                _AdminUser.Surname = txtSurname.Text;
                _AdminUser.Forename = txtForename.Text;
                _AdminUser.EmailAddress = txtEmail.Text;
                _AdminUser.ModifiedOn = Convert.ToDateTime(System.DateTime.Now);
                _AdminUser.BranchID = branchId;
                _AdminUser.RoleID = Guid.Parse(ddlRoles.SelectedItem.Value.ToString());
                _AdminUser.IsActive = chkActive.Checked == true ? true : false;
                _AdminUser.IsAgent = chkAgent.Checked == true ? true : false;
                _AdminUser.ID = (Session["UserID"]!=null?(Guid.Parse(Session["UserID"].ToString())):new Guid());
                _AdminUser.Note = txtUserNote.Text;
                Guid res = _Master.UpdateAdminUser(_AdminUser);
                if (res != null)
                {
                    //Delete Existing Record
                    db.tblAdminUserLookupSites.Where(w => w.AdminUserID == res).ToList().ForEach(db.tblAdminUserLookupSites.DeleteObject);
                    db.SaveChanges();

                    //Get All Seletected Site
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked != false)
                        {
                            AdminUserLookSites AdminUserLookSites = new AdminUserLookSites();
                            AdminUserLookSites.ID = Guid.NewGuid();
                            AdminUserLookSites.AdminUserID = res;
                            AdminUserLookSites.SiteID = Guid.Parse(node.Value);
                            int LookupResult = _Master.AddUserLookupSites(AdminUserLookSites);
                            if (LookupResult > 0)
                            {
                            }
                        }
                    }

                    ShowMessage(1, "You have successfully updated.");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void trBranch_SelectedNodeChanged(object sender, EventArgs e)
        {
            txtddl.Text = trBranch.SelectedNode.Text;
            hdnddlValue.Value = trBranch.SelectedNode.Value;
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewProfile.aspx");
        } 
    }
}
