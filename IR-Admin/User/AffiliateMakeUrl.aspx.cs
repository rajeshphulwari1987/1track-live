﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.User
{
    public partial class AffiliateMakeUrl : Page
    {
        public string UserName = string.Empty;
        readonly Masters _master = new Masters();
        readonly ManageProduct _product = new ManageProduct();
        private readonly ManageAffiliateUser _AffiliateUser = new ManageAffiliateUser();
        private readonly Masters _oMasters = new Masters();
        public string tab = "1";
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                UserName = Session["AffUserName"] != null ? Session["AffUserName"].ToString() : string.Empty;
                PageLoadEvent();
                BindAffiliateUserList();
            }
        }
        void PageLoadEvent()
        {
            //--I have manage categories tree for the two level 
            const int treeLevel = 3;
            _SiteID = Master.SiteID;
            ddlCategory.DataSource = _product.GetAssociateCategoryList(treeLevel, _SiteID);
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();

        }
        public void BindAffiliateUserList()
        {
            try
            {
                Guid AID = (Request.QueryString["Id"] != null ? Guid.Parse(Request.QueryString["Id"]) : new Guid());
                Guid CID = ((ddlCategory.SelectedValue != "0" && ddlCategory.SelectedValue != "") ? Guid.Parse(ddlCategory.SelectedValue) : Guid.Empty);
                var userlist = _AffiliateUser.GetMakeURLData(AID, CID, Master.SiteID);


                grvAffiliateUser.DataSource = userlist != null && userlist.Count > 0 ? userlist : null;
                grvAffiliateUser.DataBind();

                var list = _AffiliateUser.GetProductListByCatID(Master.SiteID, CID, AID);
                grdProduct.DataSource = list != null && list.Count > 0 ? list : null;
                grdProduct.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void grvAffiliateUser_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    string[] spliddata = e.CommandArgument.ToString().Split('ñ');
                    bool result = _AffiliateUser.UpdateMakeURL(new tblAffiliateMakeURL
                    {
                        Id = (spliddata[0] == new Guid().ToString() ? new Guid() : Guid.Parse(spliddata[0])),
                        AffiliateID = Guid.Parse(spliddata[1]),
                        Name = spliddata[2],
                        URL = spliddata[3],
                        IsActive = Convert.ToBoolean(spliddata[4]) == true ? false : true,
                        ProductId = Guid.Parse(spliddata[5])
                    });
                    BindAffiliateUserList();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void grdProduct_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    string[] spliddata = e.CommandArgument.ToString().Split('ñ');
                    bool result = _AffiliateUser.UpdateMakeURL(new tblAffiliateMakeURL
                    {
                        Id = (spliddata[0] == new Guid().ToString() ? new Guid() : Guid.Parse(spliddata[0])),
                        AffiliateID = Guid.Parse(spliddata[1]),
                        Name = spliddata[2],
                        URL = spliddata[3],
                        IsActive = Convert.ToBoolean(spliddata[4]) == true ? false : true,
                        CategoryId = Guid.Parse(spliddata[5])
                    });
                    BindAffiliateUserList();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grvAffiliateUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAffiliateUser.PageIndex = e.NewPageIndex;
            BindAffiliateUserList();
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAffiliateUserList();
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

    }
}