﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Web.UI;
using System.Text;
using System.Xml;
using System.Threading;
using System.Web;

namespace IR_Admin.User
{
    public partial class UserCancelOrder : Page
    {
        private readonly ManageUser _User = new ManageUser();
        public string tab = "1";
        readonly Masters _master = new Masters();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string siteURL;
        private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
        public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        #region [ Page InIt must write on every page of CMS ]
        public Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            GetCancelOrderList(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _SiteID = Master.SiteID;
                ShowMessage(0, null);
                if (!Page.IsPostBack)
                {
                    GetCancelOrderList(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void BindOrderStatus()
        {
            try
            {
                ddlOrderStatus.DataSource = new ManageOrder().GetStatusList().OrderBy(a => a.Name).ToList();
                ddlOrderStatus.DataValueField = "ID";
                ddlOrderStatus.DataTextField = "Name";
                ddlOrderStatus.DataBind();
                ddlOrderStatus.Items.Insert(0, new ListItem("--Select Order--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void GetCommentsList(int OrderId)
        {
            try
            {
                BindOrderStatus();
                var data = _User.UserCancelOrderByOrderID(OrderId);
                if (data != null) ddlOrderStatus.SelectedValue = _User.OrderByOrderID(OrderId).Status.ToString();
                rptComment.DataSource = _User.GetUseCancelOrderChatList(OrderId).OrderByDescending(x => x.CreatedOn).ToList();
                rptComment.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void GetCancelOrderList(Guid SiteID)
        {
            try
            {
                grdCancelOrder.DataSource = _User.GetusercancelorderList(SiteID);
                grdCancelOrder.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdCancelOrder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string IsActive = e.CommandName;
                if (e.CommandName == "ActiveInActive")
                {
                    int Id = Convert.ToInt32(e.CommandArgument.ToString());
                    _User.GetusercancelorderActive(Id);
                }
                else if (e.CommandName == "ViewOrder")
                {
                    int OrderId = Convert.ToInt32(e.CommandArgument.ToString());
                    GetCommentsList(OrderId);
                    lblOrderNo.Text = OrderId.ToString();
                    tab = "2";
                }
                GetCancelOrderList(_SiteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                _User.AddUseCancelOrderChat(Convert.ToInt32(lblOrderNo.Text), AdminuserInfo.UserID, txtComment.Text, Convert.ToInt32(ddlOrderStatus.SelectedValue));
                GetCommentsList(Convert.ToInt32(lblOrderNo.Text));
                SendEmail(Convert.ToInt32(lblOrderNo.Text));
                txtComment.Text = "";
                tab = "2";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void SendEmail(long OrderID)
        {
            try
            {
                _SiteID = Master.SiteID;
                Guid UserId = _db.tblUseCancelOrders.FirstOrDefault(x => x.OrderId == OrderID).UserId;
                var CommunicationList = _User.GetUseCancelOrderChatList(OrderID, _SiteID, UserId).ToList();
                if (CommunicationList != null)
                {
                    var siteName = _db.tblSites.FirstOrDefault(x => x.ID == _SiteID);
                    string htmfile = Server.MapPath("~/MailTemplate/CRMCancelEmailTemplate.htm");
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml.ToString();
                    StringBuilder bodycommitment = new StringBuilder();
                    if (siteName.IsSTA.HasValue ? siteName.IsSTA.Value : false)
                        body = body.Replace("##headercolor##", "#0c6ab8");
                    else
                        body = body.Replace("##headercolor##", "#941e34");
                    body = body.Replace("##siteimagepath##", LoadSiteLogo());
                    body = body.Replace("#Blank#", "&nbsp;");
                    body = body.Replace("##OrderNo##", "Order " + OrderID + " Cancel Request");
                    foreach (var data in CommunicationList)
                    {
                        string color = (data.IsUser == "clsUser" ? "#cccccc" : "#a1e5ff");
                        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                        TextInfo textInfo = cultureInfo.TextInfo;
                        bodycommitment.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0' style='border: 1px solid #ccc;'><tr>");
                        bodycommitment.Append("<td bgcolor='" + color + "' width='100%' valign='top' style='border: none;margin-left:-10px;padding:5px;font-family: Arial, Helvetica, sans-serif;font-size: 15px;'>");
                        bodycommitment.Append("<div style='font-weight:bold;'>");
                        bodycommitment.Append(textInfo.ToTitleCase(data.Name));
                        bodycommitment.Append("</div><div style='font-size: 11px;'>");
                        bodycommitment.Append(data.CreatedOn.ToString("dd/MMM/yyyy h:mm tt"));
                        bodycommitment.Append("</div></td></tr><tr>");
                        bodycommitment.Append("<td width='100%' valign='top' style='padding:9px; font-family: Arial, Helvetica, sans-serif;line-height: 25px; font-size: 14px; background: white;'>");
                        bodycommitment.Append(data.Commitment.Replace("\r\n", "<br />"));
                        bodycommitment.Append("</td></tr></table><br/>");
                    }
                    body = body.Replace("##Commitment##", Convert.ToString(bodycommitment));
                    string Subject = "Order " + OrderID + " cancel Request";
                    var emailSetting = _master.GetEmailSettingDetail(_SiteID);
                    if (emailSetting != null)
                    {
                        _master.SendCancelOrderMail(_SiteID, Subject, body, emailSetting.Email, _db.tblUserLogins.FirstOrDefault(x => x.ID == UserId).Email);
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public string LoadSiteLogo()
        {
            _SiteID = Master.SiteID;
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_SiteID);
            var data = _db.tblSites.FirstOrDefault(x => x.ID == _SiteID && x.IsActive == true);
            if (data != null)
            {
                if (!string.IsNullOrEmpty(data.LogoPath))
                    return AdminSiteUrl + data.LogoPath;
                else
                    return siteURL + "assets/img/branding/logo.png";
            }
            return "https://www.internationalrail.com/images/logo.png";
        }

        protected void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

    }
}