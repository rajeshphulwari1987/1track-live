﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AffiliateUser.aspx.cs" Inherits="IR_Admin.User.AffiliateUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });
        $(document).ready(function () {
            if ('<%=tab.ToString()%>' == '1') {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }

            $(".list a").click(function (e) {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                $('input:text').val('');
                if ($(this).attr("id") == 'aList') {
                    document.getElementById('MainContent_divlist').style.display = 'Block';
                    document.getElementById('MainContent_divNew').style.display = 'none';
                }
                else {

                    document.getElementById('MainContent_divlist').style.display = 'none';
                    document.getElementById('MainContent_divNew').style.display = 'Block';
                }
            });

            function clearControls() {
                document.getElementById('MainContent_txtCode').value = '';
                document.getElementById('MainContent_txtName').value = '';
                document.getElementById('MainContent_divlist').value = '';
                document.getElementById('MainContent_divlist').value = '';

            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnAffUserID" runat="server" />
    <h2>
        Affiliate User</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li id="lilist"><a id="aList" href="AffiliateUser.aspx" class="current">List</a></li>
            <li id="linew"><a id="aNew" href="#" class="">New</a> </li>
        </ul>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grvAffiliateUser" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grvAffiliateUser_OnRowCommand"
                            AllowPaging="True" OnPageIndexChanging="grvAffiliateUser_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <%#Eval("Code")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <%#Eval("ContactEmail")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Days in program">
                                    <ItemTemplate>
                                        <%#Eval("DaysInProgram")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country">
                                    <ItemTemplate>
                                        <%#Eval("Country")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Remove"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgMakeurl" AlternateText="Makeurl" ToolTip="Make Url"
                                            CommandArgument='<%#Eval("ID")+"ñ"+Eval("Name")%>' CommandName="Makeurl" ImageUrl="~/images/url.gif"
                                            Width="30" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server">
                    <table>
                        <tr>
                            <td style="vertical-align: top:">
                                <table class="tblMainSection">
                                    <tr>
                                        <td class="col">
                                            Code
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtCode" runat="server" />
                                            <asp:RequiredFieldValidator ID="txtCodeRequiredFieldValidator" runat="server" ControlToValidate="txtCode"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtName" runat="server" />
                                            <asp:RequiredFieldValidator ID="txtNameRequiredFieldValidator" runat="server" ControlToValidate="txtName"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            User Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtUserName" runat="server" MaxLength="200" />
                                            <asp:RequiredFieldValidator ID="reqUserName" runat="server" ControlToValidate="txtUserName"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Password
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="20"/>
                                            <asp:RequiredFieldValidator ID="reqPassword" runat="server" ControlToValidate="txtPassword"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="rvPass" runat="server" ErrorMessage="*" ForeColor="Red"
                                                ControlToValidate="txtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                                                ValidationGroup="AUForm" />
                                            <a href="#" id="shwpwmsg" title="password minimum length must be 8 to 20 character. should be alphanumeric with lower and upper case.">
                                                <img src="../images/info.png" alt="" width="25px" style="margin-top: 2px; position: absolute;" />
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Contact Email
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtContactEmail" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtContactEmail"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                            <asp:RegularExpressionValidator ID="txtemailregexp" runat="server" CssClass="valdreq"
                                                ErrorMessage="Invalid E-mail Address." ValidationGroup="affForm" ControlToValidate="txtContactEmail"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Country
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqCountry" runat="server" ControlToValidate="ddlCountry"
                                                InitialValue="0" CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Days in program
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtdays" runat="server" onkeypress="return isNumberKey(event)" MaxLength="5" />
                                            <asp:RequiredFieldValidator ID="txtdaysRequiredFieldValidator" runat="server" ControlToValidate="txtdays"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="affForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Is Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkisActive" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_OnClick"
                                                Text="Submit" Width="89px" ValidationGroup="affForm" />
                                            &nbsp;
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 20%; border-left: 1px solid #ccc; line-height: 30px;">
                                <div style="padding: 15px 0 0 0;">
                                </div>
                                <b>&nbsp;Select Site</b>
                                <div style="width: 119%; height: 250px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
</asp:Content>
