﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ViewProfile.aspx.cs" Inherits="IR_Admin.User.ViewProfile" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });

        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        })
 
    </script>
    <script type="text/javascript">
        var treeviewID = '<%=trBranch.ClientID%>';
        function toggleTreeView(ev, hide) {
            var ddTreeView = document.getElementById("ddTreeView");
            ddTreeView.style.display = (hide == null && ddTreeView.style.display == "none") || hide == false ? "" : "none";
            ev = ev ? ev : window.event;
            if (ev) ev.cancelBubble = true;
        }     
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        <%=name %></h2>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" style="width: 30%">
                                User Name
                            </td>
                            <td class="col" style="width: 40%">
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtUserNameRequiredFieldValidator" runat="server"
                                    CssClass="valdreq" ControlToValidate="txtUserName" ErrorMessage="*" ValidationGroup="AUForm"></asp:RequiredFieldValidator>
                            </td>
                            <td rowspan="11" valign="top" class="col" style="width: 30%; border-left: 1px dashed #B1B1B1;">
                                &nbsp; <b>Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto; margin-left: 5px;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Salutation
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlTitle" runat="server" Style="padding: 0 0;">
                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Mrs." Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Ms" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ReqTitle" runat="server" ControlToValidate="ddlTitle"
                                    CssClass="valdreq" ErrorMessage="select title" ToolTip="Title is required." ValidationGroup="AUForm"
                                    InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Forename
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtForename" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Surname
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Email Address
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="txtEmailRegularExpressionValidator" runat="server"
                                    ControlToValidate="txtEmail" ErrorMessage="Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="AUForm"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Role
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlRoles" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trbramch" runat="server" visible="false">
                            <td class="col">
                                Branch
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlBranch" runat="server" Width="236">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Offices
                            </td>
                            <td class="col">
                                <table cellpadding="0" style="border-collapse: collapse">
                                    <tbody>
                                        <tr valign="top">
                                            <td style="width: 260px">
                                                <asp:TextBox ID="txtddl" value="---Select---" ReadOnly="true" Style="width: 100%"
                                                    runat="server"></asp:TextBox>
                                                <asp:HiddenField ID="hdnddlValue" runat="server" Value="" />
                                            </td>
                                            <td valign="top" onmousedown="toggleTreeView(event); event.cancelBubble = true;">
                                                <img onmouseout="this.src = '../schemes/internationalrail/images/dropdown_over.gif'"
                                                    onmouseover="this.src = '../schemes/internationalrail/images/dropdown_over.gif'"
                                                    style="width: 19px; height: 25px; border-style: none; vertical-align: top; margin-top: 3px;"
                                                    alt="" src="../schemes/internationalrail/images/dropdown_over.gif">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="ddTreeView" style="position: absolute; background: white; border: 1px solid gray;
                                    border-width: 1px; height: 180px; overflow-y: auto; width: 275px; display: none;">
                                    <asp:TreeView ID="trBranch" runat="server" EnableFullRowEvents="true" OnSelectedNodeChanged="trBranch_SelectedNodeChanged"
                                        ShowLines="True">
                                        <DataBindings>
                                            <asp:TreeNodeBinding TextField="Text" DataMember="System.Data.DataRowView" ValueField="ID" />
                                        </DataBindings>
                                    </asp:TreeView>
                                </div>
                            </td>
                            <td class="col">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Agent
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkAgent" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                User Note
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtUserNote" TextMode="MultiLine" Width="276" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Update" Width="89px" ValidationGroup="AUForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
