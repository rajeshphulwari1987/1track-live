﻿using System;
using System.Linq;
using Business;
using System.Net.Mail;

namespace IR_Admin.User
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        readonly ManageUser _manageUser = new ManageUser();
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                bool res = _manageUser.ChangePassword(AdminuserInfo.UserID, txtPass.Text.Trim(), txtNewPass.Text.Trim());
                if (res)
                {
                    SendMail();
                    ShowMessage(1, "Password changed successfully.");
                }
                txtNewPass.Text = txtCPass.Text = string.Empty;

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                // host Address
                string SmtpHost = System.Configuration.ConfigurationManager.AppSettings["SmtpHost"].ToString();
                // user name
                string SmtpUser = System.Configuration.ConfigurationManager.AppSettings["SmtpUser"].ToString();
                //Password 
                string SmtpPassword = System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"].ToString();
                int SmtpPort = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SmtpPort"].ToString());
                // Address from where send the mail
                string FromEmail = System.Configuration.ConfigurationManager.AppSettings["FromEmail"].ToString();
                tblAdminUser objUser = _manageUser.GetAdminUserDetails(AdminuserInfo.UserID);
                
                if (objUser == null)
                    return false;

                string Name = objUser.Forename + " " + objUser.Surname, EmailAddress = objUser.EmailAddress, body = "";
                string ToEmail = EmailAddress;
                body = "Hi " + Name + ",<br /><br />You have successfully reset your passsword !<br /><br /> Your new password is: " + txtNewPass.Text + "<br /><br />Thanks<br />International Rail"; MailAddress fromAddres = new MailAddress(FromEmail, FromEmail);
              
                smtpClient.Host = SmtpHost;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(SmtpUser, SmtpPassword);
                message.From = fromAddres;
                message.To.Add(ToEmail);
                message.Subject = "Forgot Password!";
                message.IsBodyHtml = true;
                message.Body = body;

                smtpClient.Send(message);
                retVal = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retVal;
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChangePassword.aspx");
        }
    }
}