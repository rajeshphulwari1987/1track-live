﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin.User
{
    public partial class AffiliateUser : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        private readonly ManageAffiliateUser _AffiliateUser = new ManageAffiliateUser();
        private readonly Masters _oMasters = new Masters();
        public string tab = "1";
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                FillMenu();
                PageLoadEvent();
                BindAffiliateUserList();
            }
        }
        private void FillMenu()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                tblAffiliateUser objaffUser = new tblAffiliateUser();
                objaffUser.ID = string.IsNullOrEmpty(hdnAffUserID.Value.ToString()) ? Guid.NewGuid() : Guid.Parse(hdnAffUserID.Value);
                objaffUser.Code = txtCode.Text.Trim();
                objaffUser.Name = txtName.Text.Trim();
                objaffUser.CountryID = Guid.Parse(ddlCountry.SelectedValue);
                objaffUser.ContactEmail = txtContactEmail.Text.Trim();
                objaffUser.DaysInProgram = Convert.ToInt32(txtdays.Text);
                objaffUser.CreatedOn = System.DateTime.UtcNow;
                objaffUser.IsActive = chkisActive.Checked;
                objaffUser.CreatedBy = AdminuserInfo.UserID;
                objaffUser.ModifiedBy = AdminuserInfo.UserID;
                objaffUser.ModifiedOn = System.DateTime.UtcNow;
                objaffUser.UserName = txtUserName.Text;
                objaffUser.Password = txtPassword.Text;


                Guid affid = _AffiliateUser.SaveAffiliateUser(objaffUser);

                if (affid != new Guid())
                {

                    _AffiliateUser.SaveAffilateSiteLookUp(new tblAffiliateUserLookupSite { AffiliateUserID = affid }, true);
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            _AffiliateUser.SaveAffilateSiteLookUp(new tblAffiliateUserLookupSite
                            {
                                SiteID = Guid.Parse(node.Value),
                                AffiliateUserID = affid,
                                ID = Guid.NewGuid()
                            }, false);
                        }
                    }
                    BindAffiliateUserList();
                    tab = "1";
                    ShowMessage(1, "Affiliate User Saved Successfully");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                tab = "2";
            }
        }

        void PageLoadEvent()
        {
            List<tblCountriesMst> countryList = _oMasters.GetCountryList().Where(x => x.IsActive == true).ToList();
            ddlCountry.DataSource = countryList != null && countryList.Count > 0 ? countryList : null;
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        }
        public void BindAffiliateUserList()
        {
            try
            {
                var userlist = _AffiliateUser.GetAffiliateUserList();
                grvAffiliateUser.DataSource = userlist != null && userlist.Count > 0 ? userlist : null;
                grvAffiliateUser.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grvAffiliateUser_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] data = e.CommandArgument.ToString().Split('ñ');
                Guid affuserID = string.IsNullOrEmpty(data[0].ToString()) ? Guid.NewGuid() : Guid.Parse(data[0].ToString());
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                switch (e.CommandName)
                {
                    case "Modify":
                        {
                            EditAffiliateUser(affuserID);
                            break;
                        }
                    case "Remove":
                        {
                            var res = _AffiliateUser.DeleteAffiliateUser(affuserID);
                            if (res)
                                ShowMessage(1, "Record deleted successfully.");
                            BindAffiliateUserList();
                            tab = "1";
                            break;
                        }
                    case "ActiveInActive":
                        {
                            bool result = _AffiliateUser.ActivateInActivateAffiliateUser(affuserID);
                            BindAffiliateUserList();
                            tab = "1";
                            break;
                        }

                    case "Makeurl":
                        {
                            Session["AffUserName"] = data[1];
                            Response.Redirect("~\\User\\AffiliateMakeUrl.aspx?Id=" + data[0].ToString());
                            tab = "1";
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void grvAffiliateUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAffiliateUser.PageIndex = e.NewPageIndex;
            BindAffiliateUserList();

        }
        public void EditAffiliateUser(Guid ID)
        {
            var affuser = _AffiliateUser.GetAffiliateUserbyID(ID);
            if (affuser != null)
            {
                hdnAffUserID.Value = ID.ToString();
                tab = "2";
                txtCode.Text = affuser.Code;
                txtName.Text = affuser.Name;
                ddlCountry.SelectedValue = affuser.CountryID.ToString();
                txtContactEmail.Text = affuser.ContactEmail;
                txtdays.Text = affuser.DaysInProgram.ToString();
                chkisActive.Checked = affuser.IsActive;
                txtUserName.Text = affuser.UserName;
                txtPassword.Attributes.Add("value", affuser.Password);
            }

            var oSiteId = _AffiliateUser.GetAffiliateUserByID(ID);
            trSites.Nodes.Clear();
            FillMenu();
            foreach (var sItem in oSiteId)
            {
                foreach (TreeNode node in trSites.Nodes)
                {
                    if (node.Value == sItem.ToString())
                        node.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}