﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="true" CodeBehind="UserCancelOrder.aspx.cs" Inherits="IR_Admin.User.UserCancelOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .panes div
        {
            font-size: 14px;
        }
        .txt
        {
    background-color: white;
    padding: 5px;
        margin-bottom: 24px;
        }
        .headerlbl
        {
    padding: 5px;
        }
        .clsUser
        {
            background-color: #a1e5ff;
        }
        .clsAdmin
        {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript" language="javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            if ('<%=tab%>' == '1') {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            } else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnAffUserID" runat="server" />
    <h2>
        User Cancel Order</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
          <ul class="list">
            <li id="lilist"><a id="aList" href="UserCancelOrder.aspx" class="current">List</a></li>
        </ul>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdCancelOrder" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="20" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"
                            AllowPaging="True" OnRowCommand="grdCancelOrder_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("FirstName")%>
                                        <%#Eval("LastName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <%#Eval("Email")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Id">
                                    <ItemTemplate>
                                        <%#Eval("OrderId")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <%#Eval("StatusName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="ImageButton1" CommandArgument='<%#Eval("OrderId")%>'
                                            Height="16" CommandName="ViewOrder" AlternateText="status" ImageUrl='~/images/view.png'
                                            ToolTip='View status/Comments' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" cclass="tblMainSection" runat="server">
                    <fieldset class="grid-sec2">
                        <legend><strong>Order <asp:Label ID="lblOrderNo" runat="server"></asp:Label></strong></legend>
                        <div class="fieldset-chart">
                         <div> <strong>Order Status</strong>
                                <asp:DropDownList ID="ddlOrderStatus" runat="server">
                                </asp:DropDownList>
                            </div>
                            <hr />
                            <div>
                                <div>
                                    <strong>Comment</strong>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtComment" runat="server" Rows="10" Columns="70" placeholder="enter comment"
                                        TextMode="MultiLine" Style="width: 100%"></asp:TextBox></div>
                            </div>
                            <div>
                                <div>
                                    <asp:Button ID="btnAdd" runat="server" CssClass="button1" Text="Submit" 
                                        onclick="btnAdd_Click" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="grid-sec2">
                        <legend><strong>Comments</strong></legend>
                        <asp:Repeater ID="rptComment" runat="server">
                            <ItemTemplate>
                                <div class="<%#Eval("IsUser")%>">
                                    <div class="headerlbl">
                                        <strong>
                                            <%#Eval("Name")%>
                                        </strong>
                                        <div style="float: right;">
                                            <%# String.Format("{0:dd/MMM/yyyy h:mm tt}", Eval("CreatedOn"))%></div>
                                    </div>
                                    <div class="txt">
                                        <%#Eval("Commitment").ToString().Replace("\r\n","<br />")%>
                                        <br />
                                    </div>
                                </div>
                                <hr />
                            </ItemTemplate>
                        </asp:Repeater>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
