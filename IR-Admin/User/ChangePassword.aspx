﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs"
    Inherits="IR_Admin.User.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });
        $(document).ready(function () {
            function clearControls() {
                document.getElementById('MainContent_txtCode').value = '';
                document.getElementById('MainContent_txtName').value = '';
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnAffUserID" runat="server" />
    <h2>
        Change Password</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divNew" class="grid-sec2" runat="server">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                Enter Current Password
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPass" runat="server" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="rfPass" runat="server" ControlToValidate="txtPass"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="vcChange" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Choose a New Password
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtNewPass" TextMode="Password" runat="server" MaxLength="20"/>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="rfNew" runat="server" ControlToValidate="txtNewPass"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="vcChange" />
                                <asp:RegularExpressionValidator ID="rvPass" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtNewPass" Display="Dynamic" ValidationExpression="(?=^.{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" ValidationGroup="vcChange" />
                                <a href="#" id="shwpwmsg" title="password minimum length must be 8 to 20 character. should be alphanumeric with lower and upper case.">
                                    <img src="../images/info.png" alt="" width="25px" style="margin-top: 2px; position: absolute;" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Confirm Password
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtCPass" TextMode="Password" runat="server" MaxLength="20"/>
                                <asp:RequiredFieldValidator ID="rfCPass" runat="server" ControlToValidate="txtCPass"
                                    CssClass="valdreq" ErrorMessage="*" ValidationGroup="vcChange" />
                                <asp:CompareValidator ID="cm" runat="server" ForeColor="Red" ErrorMessage="Confirmed password do not match new password."
                                    ControlToValidate="txtCPass" ControlToCompare="txtNewPass" ValidationGroup="vcChange" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_OnClick"
                                    Text="Submit" Width="89px" ValidationGroup="vcChange" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
