﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Web.UI;

namespace IR_Admin.User
{
    public partial class RegisteredUser : Page
    {
        readonly Masters _master = new Masters();
        private readonly ManageUser _User = new ManageUser();
        private readonly Masters _oMasters = new Masters();
        public string tab = "1";
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindAffiliateUserList(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                PageLoadEvent();
                BindSite();
                BindAffiliateUserList(_SiteID);
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = ddlSearchSite.DataSource = _master.GetActiveSiteList();
            ddlSite.DataTextField = ddlSearchSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = ddlSearchSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
            ddlSearchSite.DataBind();
            ddlSearchSite.Items.Insert(0, new ListItem("--Site--", "0"));
        }
        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                var objUser = new tblUserLogin();
                objUser.ID = string.IsNullOrEmpty(hdnAffUserID.Value) ? Guid.NewGuid() : Guid.Parse(hdnAffUserID.Value);
                objUser.FirstName = txtName.Text;
                objUser.LastName = txtLastName.Text;
                objUser.City = txtCity.Text;
                objUser.Country = Guid.Parse(ddlCountry.SelectedValue);
                objUser.Email = txtContactEmail.Text;
                objUser.Phone = txtPhone.Text;
                objUser.PostCode = txtPostCode.Text;
                objUser.IsActive = chkisActive.Checked;
                objUser.Password = txtPassword.Text;
                objUser.SiteId = Guid.Parse(ddlSite.SelectedValue);

                objUser.Address = txtLine1.Text;
                objUser.Address2 = txtLine2.Text;
                objUser.State = txtState.Text;

                if (!string.IsNullOrEmpty(txtDOB.Text))
                    objUser.Dob = Convert.ToDateTime(txtDOB.Text);
                var affid = _User.SaveUser(objUser);
                if (affid != new Guid())
                {
                    _SiteID = Master.SiteID;
                    BindAffiliateUserList(_SiteID);
                    tab = "1";
                    ClearControls();
                    ShowMessage(1, (string.IsNullOrEmpty(hdnAffUserID.Value) ? "User Record Saved Successfully." : "User Record Updated Successfully."));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                tab = "2";
            }
        }

        void ClearControls()
        {
            ddlSite.SelectedIndex = 0;
            txtName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtContactEmail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            ddlCountry.SelectedIndex = 0;
            txtCity.Text = string.Empty;
            txtPostCode.Text = string.Empty;
            txtDOB.Text = string.Empty;
            chkisActive.Checked = false;

            txtLine1.Text = string.Empty;
            txtLine2.Text = string.Empty;
            txtState.Text = string.Empty;
        }

        void PageLoadEvent()
        {
            var countryList = _oMasters.GetCountryList().Where(x => x.IsActive == true).ToList();
            ddlCountry.DataSource = ddlSearchCountry.DataSource = countryList != null && countryList.Count > 0 ? countryList : null;
            ddlCountry.DataTextField = ddlSearchCountry.DataTextField = "CountryName";
            ddlCountry.DataValueField = ddlSearchCountry.DataValueField = "CountryID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            ddlSearchCountry.DataBind();
            ddlSearchCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        }

        public void BindAffiliateUserList(Guid siteid)
        {
            try
            {
                if (siteid == new Guid())
                    siteid = Master.SiteID;
                var userlist = _User.GetAllUserLogin(siteid);
                if (userlist != null && userlist.Count > 0)
                {
                    userlist = userlist.Where(x => x.Name.ToLower().Contains(txtSearchUserName.Text.ToLower()) && x.ContactEmail.ToLower().Contains(txtSearchEmail.Text.ToLower())).ToList();
                    if (ddlSearchCountry.SelectedValue != "0")
                        userlist = userlist.Where(x => x.Country.ToLower().Contains(ddlSearchCountry.SelectedItem.Text.ToLower())).ToList();
                    if (ddlSearchSite.SelectedValue != "0")
                        userlist = userlist.Where(x => x.SiteName.ToLower().Contains(ddlSearchSite.SelectedItem.Text.ToLower())).ToList();
                    grvAffiliateUser.DataSource = userlist;
                    grvAffiliateUser.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grvAffiliateUser_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                switch (e.CommandName)
                {
                    case "Modify":
                        {
                            btnSubmit.Text = "Update";
                            var affuserID = string.IsNullOrEmpty(e.CommandArgument.ToString()) ? Guid.NewGuid() : Guid.Parse(e.CommandArgument.ToString());
                            EditAffiliateUser(affuserID);
                            break;
                        }
                    case "Remove":
                        {
                            var affuserID = string.IsNullOrEmpty(e.CommandArgument.ToString()) ? Guid.NewGuid() : Guid.Parse(e.CommandArgument.ToString());
                            var res = _User.DeleteLoginUser(affuserID);
                            if (res)
                                ShowMessage(1, "Record deleted successfully.");
                            BindAffiliateUserList(_SiteID);
                            tab = "1";
                            break;
                        }
                    case "ActiveInActive":
                        {
                            var affuserID = string.IsNullOrEmpty(e.CommandArgument.ToString()) ? Guid.NewGuid() : Guid.Parse(e.CommandArgument.ToString());
                            bool? result = _User.ActivateInActivateUser(affuserID);
                            BindAffiliateUserList(_SiteID);
                            tab = "1";
                            break;
                        }
                    //case "ViewOrder":
                    //    {
                    //        var affuserID = string.IsNullOrEmpty(e.CommandArgument.ToString()) ? Guid.NewGuid() : Guid.Parse(e.CommandArgument.ToString());
                    //        Response.Redirect("~/Reports/UserOrders.aspx?ID=" + affuserID.ToString(), true);
                    //        break;
                    //    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grvAffiliateUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAffiliateUser.PageIndex = e.NewPageIndex;
            BindAffiliateUserList(new Guid());
        }

        public void EditAffiliateUser(Guid ID)
        {
            var affuser = _User.GetUserbyID(ID);
            if (affuser != null)
            {
                hdnAffUserID.Value = ID.ToString();
                tab = "2";
                ddlSite.SelectedValue = affuser.SiteId.ToString();
                txtName.Text = affuser.FirstName;
                txtLastName.Text = affuser.LastName;
                txtPassword.Attributes["value"] = affuser.Password;
                txtContactEmail.Text = affuser.Email;
                txtCity.Text = affuser.City;
                txtPostCode.Text = affuser.PostCode;
                txtDOB.Text = affuser.Dob.HasValue ? Convert.ToDateTime(affuser.Dob.Value).ToString("dd/MM/yyyy") : "";
                ddlCountry.SelectedValue = affuser.Country.ToString();
                txtPhone.Text = affuser.Phone;
                chkisActive.Checked = Convert.ToBoolean(affuser.IsActive);


                txtLine1.Text = affuser.Address;
                txtLine2.Text = affuser.Address2;
                txtState.Text = affuser.State;
                
            }
        }

        protected void btnSearchSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                _SiteID = Master.SiteID;
                BindAffiliateUserList(_SiteID);
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}