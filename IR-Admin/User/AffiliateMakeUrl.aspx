﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AffiliateMakeUrl.aspx.cs" Inherits="IR_Admin.User.AffiliateMakeUrl" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });
        $(document).ready(function () {
            if ('<%=tab.ToString()%>' == '1') {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                document.getElementById('MainContent_divlist').style.display = 'Block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }

            $(".list a").click(function (e) {
                document.getElementById('<%= hdnAffUserID.ClientID %>').value = '';
                $('input:text').val('');
                if ($(this).attr("id") == 'aList') {
                    document.getElementById('MainContent_divlist').style.display = 'Block';
                    document.getElementById('MainContent_divNew').style.display = 'none';
                }
                else {

                    document.getElementById('MainContent_divlist').style.display = 'none';
                    document.getElementById('MainContent_divNew').style.display = 'Block';
                }
            });

            function clearControls() {
                document.getElementById('MainContent_txtCode').value = '';
                document.getElementById('MainContent_txtName').value = '';
                document.getElementById('MainContent_divlist').value = '';
                document.getElementById('MainContent_divlist').value = '';
            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnAffUserID" runat="server" />
    <h2>
        Affiliate User :
        <%=UserName %>
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li id="lilist"><a id="aList" href="AffiliateUser.aspx" class="current">List</a></li>
        </ul>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv" style="font-size: 14px;">
                        <div id="MainContent_dvSearch" class="searchDiv">
                            Category:
                            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                                Width="50%">
                            </asp:DropDownList>
                            <a href="AffiliateUser.aspx" class="button" style="text-align: center; text-decoration: none;
                                font-size: 13px; margin: 4px 4px 0 0; border: none; float: right;">Goto Back</a>
                        </div>
                        <div class="searchDiv" style="font-weight: bold; margin-top: 10px;padding-left:5px !important">
                            Category Urls:
                        </div>
                        <asp:GridView ID="grdProduct" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdProduct_OnRowCommand"
                            AllowPaging="True">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Category Name">
                                    <ItemTemplate>
                                        <%#Eval("Category")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Make URL">
                                    <ItemTemplate>
                                        <%#Eval("url").ToString().Replace("//","/")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")+"ñ"+Eval("AffiliateId")+"ñ"+Eval("Category")+"ñ"+Eval("url")+"ñ"+Eval("IsActive")+"ñ"+Eval("CategoryID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div style="clear:both;"></div>
                        <div class="searchDiv" style="font-weight: bold; margin-top: 10px;padding-left:5px !important">
                            Product Urls:
                        </div>
                        <asp:GridView ID="grvAffiliateUser" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grvAffiliateUser_OnRowCommand"
                            AllowPaging="True" OnPageIndexChanging="grvAffiliateUser_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Make URL">
                                    <ItemTemplate>
                                        <%#Eval("url").ToString().Replace("//","/")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")+"ñ"+Eval("AffiliateId")+"ñ"+Eval("Name")+"ñ"+Eval("url")+"ñ"+Eval("IsActive")+"ñ"+Eval("ProductID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
