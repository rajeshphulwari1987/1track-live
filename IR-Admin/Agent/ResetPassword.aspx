﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ResetPassword.aspx.cs" Inherits="Agent_ResetPassword" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Reset Password </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Reset Password</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="../images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="../images/innerMap.gif" />
    </div>
</div>
</div>
 <div class="left-content">
 <div id="login-sec" style="font-size:12px">
                 <div class="hd-title"> Generate New Password</div>
                <div class="full mr-tp2">
                    <div class="full corner">
                       </div>
                    <div class="login-data" style="background:none; border:1px solid #ccc;border-radius:2px; ">
                      
                        <div id="divLogin" class="login">
                        <div class="full">
                                <div class="login-col1" style="padding-left:40px;width:115px;">
                                    First Name: </div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtFirstName" class="login" runat="server"   MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="reqFirstName" ControlToValidate="txtFirstName" ForeColor="Red"
                                        runat="server" ErrorMessage="*" ValidationGroup="resetpassword"/>
                                       
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col1" style="padding-left:40px;width:115px;">
                                   Last Name: </div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtLastName" runat="server" class="login"   MaxLength="50"/>
                                    <asp:RequiredFieldValidator ID="reqLastName" ControlToValidate="txtLastName" ForeColor="Red"
                                        runat="server" ErrorMessage="*" ValidationGroup="resetpassword"/>
                                     
                                </div>
                            </div>

                            <div class="full">
                                <div class="login-col1" style="padding-left:40px;width:115px;">
                                    New Password: </div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtnewpass" class="login" runat="server" TextMode="Password"  MaxLength="20"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="AgentLogintxtUsername" ControlToValidate="txtnewpass" ForeColor="Red"
                                        runat="server" ErrorMessage="*" ValidationGroup="resetpassword" />
                                         <asp:RegularExpressionValidator ID="rvPass" runat="server" ErrorMessage="Invalid Password" ForeColor="Red" ControlToValidate="txtnewpass" Display="Dynamic" 
                                         ValidationExpression="(?=^.{8,20}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()_+}{':;'?/>.<,])(?!.*\s).*$" ValidationGroup="resetpassword"/>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col1" style="padding-left:40px;width:115px;">
                                    Confirm Password: </div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtconfpass" runat="server" class="login" TextMode="Password" MaxLength="20"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="AgentLogintxtPassword" ControlToValidate="txtconfpass" ForeColor="Red"
                                        runat="server" ErrorMessage="*" ValidationGroup="resetpassword"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cval" runat="server" ErrorMessage="Confirm Password do not match." ControlToCompare="txtnewpass" ForeColor="Red" ValidationGroup="resetpassword"
                                        ControlToValidate="txtconfpass" />
                                </div>
                            </div>
                             
                            <div class="full mr-tp3">
                                    <div class="login-col2" style=" margin:5px 50px 0 0; float:right;">
                                    <asp:Button ID="BtnSubmit" OnClick="BtnSubmit_Click" runat="server" ValidationGroup="resetpassword" Text="Submit" />
                                        <br/>
                                          <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" 
                                            ForeColor="#FF3300"/>
                                            <asp:Label ID="lblErrorMsg" runat="server" CssClass="valdreq" 
                                            ForeColor="red"/>
                                </div>
                                <div class="full mr-tp3">
                                  <div class="login-col2" style="width:100%;padding-left:10px;">
                            <b>Hint:</b>
                            <ul>                            
                            <li>
                             1. Password should contain minimum 8 characters.
                            </li>
                            <li>
                              2. Password should have atleast 1 Upper or Lower case character, Numeric & Special character.
                            </li>
                            <li>
                              3. Password should Not based on personal info.
                            </li>
                            </ul>
                              <br />
                                 </div>
                                   </div>
                            </div>
                        </div> 
                </div>
                <div class="full corner"></div>
                </div>
            </div>
 </div>

<div class="right-content">
<a href='<%=siteURL%>TrainResults.aspx' id="trainresults">
  <div id="rtPannel1" runat="server"></div>
</a>
<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
<a href="<%=siteURL%>GeneralInformation.aspx">
<uc:GeneralInformation ID="GInfo" runat="server" /></a>
<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />

</div>


</section>
</asp:Content>
