﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Agent_Default" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/jscript">
        $(document).ready(function () {
            $("#forgotpass").click(function () {
                $("#MainContent_divLogin").hide();
                $("#MainContent_divforgot").show();

            });
            $("#Signin").click(function () {
                $("#MainContent_divLogin").show();
                $("#MainContent_divforgot").hide();
            });
        });
    </script>
    <script type="text/javascript">
        function PasswordSent() {
            document.getElementById('MainContent_divLogin').style.display = 'none';
            document.getElementById('MainContent_divforgot').style.display = 'Block';
            alert('Email Sent on Registered Email Id');
        }

        function PasswordErrorSent() {
            document.getElementById('MainContent_divLogin').style.display = 'none';
            document.getElementById('MainContent_divforgot').style.display = 'Block';
            alert('Email is not Registered with System');
        }
    
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Agent Login </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Agent Login</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="../images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="../images/innerMap.gif" />
    </div>
</div>
</div>
 <div class="left-content">
 <div id="login-sec">
                <div class="full">
                    <span class="clsAgentLogin"></span></div>
                <div class="full mr-tp2">
                    <div class="full corner">
                        <img src="../images/login-top-bar.jpg" alt="" /></div>
                    <div class="login-data">
                        
                        <div id="divLogin" runat="server" style="display: block;" class="login">
                            <div class="full">
                                <div class="login-col1">
                                    Username</div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtUsername" class="login" runat="server"  ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="AgentLogintxtUsername" ControlToValidate="txtUsername" ForeColor="Red" 
                                        runat="server" ErrorMessage="*" ValidationGroup="loginform"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col1">
                                    Password</div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtPassword" runat="server" class="login" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col1">
                                    &nbsp;</div>
                                <div class="login-col2">
                                    <asp:CheckBox ID="chkRememberMe" runat="server" />
                                    <label for="checkbox">
                                    </label>
                                    Remember my login details</div>
                            </div>
                            <div class="full mr-tp3">
                                     <div class="login-col2 shade5"  style=" margin:10px 0;">
                                    <a href="#" id="forgotpass">Forgot your password?</a></div>
                            </div>
                            <div class="full mr-tp3"  >
                                    <div class="login-col2" style=" margin:5px 50px 0 0; float:right;">
                                    <asp:ImageButton ID="BtnSubmit" ImageUrl="../images/btn-login.jpg" OnClick="BtnSubmit_Click"
                                        runat="server" ValidationGroup="loginform" />
                                    <asp:ImageButton ID="btnCancel" ImageUrl="../images/btn-cancel.jpg" runat="server"
                                        OnClick="btnCancel_Click" />
                                        <br/>
                                          <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" 
                                            ForeColor="#FF3300"/>
                                              <br/>
                                         <b>Note:</b> If you don't have password please leave blank
                                </div>
                              
                            </div>
                        </div>
                        <div style="display: none;" id="divforgot" runat="server" class="login-row1">
                            <div class="full mr-tp1">
                                <div class="login-col1">
                                    Username:
                                    </div>
                                    <div class="login-col2">
                                        <asp:TextBox ID="txtUser" class="login" runat="server"/>
                                         <asp:RequiredFieldValidator ID="AgentLogintxtUser" ControlToValidate="txtUser"
                                        runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="FLoginForm"/>
                                    </div>
                                <div class="login-col1">
                                    Email Id:</div>
                                <div class="login-col2">
                                    <asp:TextBox ID="txtEmail" class="login" runat="server"/>
                                    <asp:RequiredFieldValidator ID="AgentLogintxtEmail" ControlToValidate="txtEmail"
                                        runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="FLoginForm"/>
                                    <asp:RegularExpressionValidator ID="AgentLoginregxemail" runat="server"
                                        ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="FLoginForm"/>
                                </div>
                            </div>
                            <div class="full mr-tp1">
                                <div class="login-col1">
                                    &nbsp;</div>
                                <div class="login-col2 light3 shade1">
                                    Click here <span style="font-size: 14px;">»</span> <a id="Signin" href="#">Sign In
                                    </a>
                                </div>
                            </div>
                            <div class="full mr-tp1">
                                     <div class="login-col2" style=" margin:5px 50px 0 0; float:right;">
                                    <asp:ImageButton ID="btnFSubmit" ValidationGroup="FLoginForm" ImageUrl="../images/btn-request.jpg"
                                        OnClick="btnFSubmit_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                             
                </div>
                <div class="full corner">
                        <img src="../images/login-bot-bar.jpg" alt="" /></div>
                         </div>
            </div>
                        
 </div>

<div class="right-content">
<a href='<%=siteURL%>TrainResults.aspx' id="trainresults">
  <div id="rtPannel1" runat="server"></div>
</a>
<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
<a href="<%=siteURL%>GeneralInformation.aspx">
<uc:GeneralInformation ID="GInfo" runat="server" /></a>
<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />

</div>

</section>
</asp:Content>
