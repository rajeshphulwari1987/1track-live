﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AgentPaymentConfirm.aspx.cs" Inherits="Agent_AgentPaymentConfirm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        /*booking detail*/
        
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
        .center
        {
            padding-left: 55px;
            padding-right: 55px;
        }
        .backcolor
        {
            background-color: #E9E9E9;
            height: 85px;
        }
        .col1, .col2
        {
            padding: 15px;
            float: left;
            width: 45%;
        }
    </style>
    <script type="text/javascript">
        window.universal_variable = 
        {           
            version: "1.1.0"
        }; 
        window.universal_variable.transaction =  <%=products %>;
 
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="scm1" runat="server">
    </asp:ScriptManager>
    <div class="clear">
    </div>
    <div class="center">
        <h1>
            Thanks for your order!
        </h1>
        <p>
            Please print or save this page for your records. You will also recieve a receipt
            via email.
        </p>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div id="DivStock" runat="server" class="backcolor" style="padding: 5px; width: 882px;
            height: 40px; float: left;">
            <asp:DropDownList ID="ddlPrntQ" runat="server" Width="500px" Height="25" />
            <asp:RequiredFieldValidator ID="AgentLoginPrintddlPrntQ" runat="server" ErrorMessage="Please select print queue."
                CssClass="validreq" ControlToValidate="ddlPrntQ" ValidationGroup="vp" InitialValue="-1" />
            <asp:Button ID="btnCancel" runat="server" CssClass="button clsBtn" Text="Cancel"
                Width="80px" ValidationGroup="vp" />&nbsp;<asp:Button ID="btnSend" runat="server"
                    CssClass="button clsBtn" Text="Send pass to print queue" ValidationGroup="vp"
                    Width="250px" OnClick="btnSend_Click" />
        </div>
        <div id="divPrintGrid">
            <div class="round-titles">
                Order Information
            </div>
            <div class="booking-detail-in">
                <asp:Panel runat="server" ID="tblOrderInfo">
                    <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" width="50%">
                                Pay To :
                            </td>
                            <td width="50%">
                                One Track Booking System
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Email Address :
                            </td>
                            <td>
                                <asp:Label ID="lblEmailAddress" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td>
                                <b>Delivery Address</b>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblDeliveryAddress" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Order Summary:</b>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Order Date:
                            </td>
                            <td>
                                <asp:Label ID="lblOrderDate" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Order No.:
                            </td>
                            <td>
                                <asp:Label ID="lblOrderNumber" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Reference/Folder number:
                            </td>
                            <td>
                                <asp:Label ID="lblStaRefNo" runat="server" Text="" />
                            </td>
                        </tr>
                        <asp:Repeater ID="rptOrderInfo" runat="server" OnItemDataBound="rptOrderInfo_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td align="left">
                                        Journey
                                        <%# Container.ItemIndex + 1 %>:
                                    </td>
                                    <td>
                                        <%#Eval("ProductDesc")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        Name:
                                    </td>
                                    <td>
                                        <%#Eval("Name")%>
                                    </td>
                                </tr>
                                <tr id="trNettPrice" runat="server">
                                    <td align="left">
                                        <asp:Label ID="lblPrice" runat="server"/>:
                                    </td>
                                    <td>
                                        <%=currency %>
                                        <%#Eval("Price").ToString()%>
                                    </td>
                                </tr>
                                <tr id="trNettPricePass" runat="server">
                                    <td align="left">
                                        Nett Price:
                                    </td>
                                    <td>
                                        <%=currency %>
                                        <%#Eval("NetPrice").ToString()%>
                                    </td>
                                </tr>
                                <%#(Eval("TktPrtCharge")) != "" ? "<tr><td>Ticket Protection: </td><td>" + currency +" "+ Eval("TktPrtCharge")+"</td></tr>" : ""%>
                                <tr id="trCommission" runat="server">
                                    <td align="left">
                                        Commission Fee:
                                    </td>
                                    <td>
                                        <%=currency %>
                                        <%#(Eval("CommissionFee")) != null ? Eval("CommissionFee").ToString() : "0:00"%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td align="left">
                                Gross Price:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblGrossPrice" runat="server" Text="" />
                            </td>
                        </tr>
                        <%-- <tr id="trNetPrice" runat="server">
                            <td align="left">
                                Nett Price:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblNetPrice" runat="server" Text="" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="left">
                                Shipping:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblShippingAmount" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Booking Fee:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblBookingFee" runat="server" Text="" />
                            </td>
                        </tr>
                        <%--<tr id="trComm" runat="server">
                            <td align="left">
                                Commission Fee:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblCommissionFee" runat="server" Text="" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="left">
                                <b>Total Price:</b>
                            </td>
                            <td>
                                <b>
                                    <%=currency %>
                                    <asp:Label ID="lblGrandTotal" runat="server" Text="" /></b>
                            </td>
                        </tr>
                        <tr id="trRegional" runat="server">
                            <td align="left">
                                <b>Notes:</b>
                            </td>
                            <td>
                                This PNR is a Ticket On Departure, meaning you must collect your ticket from the
                                self service ticket machines at your station of departure.<br />
                                To print at the self service ticket machine you will need your PNR number, along
                                with your surname.<br />
                                Once you have printed your paper ticket in the self servive machine - you will need
                                to validate it at a validation machine. YOU MUST DO THIS PRIOR TO BOARDING YOUR
                                TRAIN.<br />
                                The validation machines are green & white on the front with Trenitalia written across
                                the top and have a red backing. They are located on the train station platforms.<br />
                                Failure to have tickets printed & validated prior to boarding will result in a fine
                                payable to the conductor.
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
        <asp:Button ID="btnPrintTicket" OnClick="btnPrintTicket_Click" runat="server" Text="Print Ticket"
            CssClass="btn-red-cart w184 f-right" Visible="false" />
    </div>
</asp:Content>
