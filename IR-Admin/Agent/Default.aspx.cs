﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_Default : Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid siteId;
    public string script = "<script></script>";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _master.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList =
                    (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", SiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSuccessMsg.Text = string.Empty;
        if (!IsPostBack)
        {
            if (Request.QueryString["UserId"] != null)
                AgenLogin();

            if (Session["AgentUsername"] != null)
            {
                    removeAgentSession();
                Response.Redirect(siteURL + "home");
            }
        
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);
            QubitOperationLoad();
        }
    }
    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    public void removeAgentSession()
    {
        Session.Remove("RailPassData");
        Session.Remove("DetailRailPass");
        Session.Remove("AgentUsername");
        Session.Remove("AgentRoleId");
        Session.Remove("AgentUserID");
        Session.Remove("AgentSiteID");
        Session.Remove("PopupDisplayed");
        Session.Remove("OrderID");
        Session.Remove("P2POrderID");
    }
    public void AgenLogin()
    {
        try
        {
            tblAdminUser UserExist = null;
            if (Request.QueryString["UserId"] != null)
            {
                UserExist = _ManageUser.AgentLoginById(Guid.Parse(Request.QueryString["UserId"]));
            }
            else if (!String.IsNullOrEmpty(txtUsername.Text) && !String.IsNullOrEmpty(txtPassword.Text))
            {
                UserExist = _ManageUser.AgentLogin(txtUsername.Text, txtPassword.Text, siteId);
            }
            if (UserExist != null)
            {
                //Login Agent User Information
                AgentuserInfo.RoleId = UserExist.RoleID;
                AgentuserInfo.UserID = UserExist.ID;
                AgentuserInfo.Username = UserExist.UserName;
                //Remember  Me 
                if (chkRememberMe.Checked)
                {
                    HttpCookie cookie = new HttpCookie("Agentlogin");
                    Response.Cookies.Add(cookie);
                    string pass = Common.Encrypt(txtPassword.Text.Trim(), true);
                    cookie.Values.Add("AgentUserName", txtUsername.Text);
                    cookie.Values.Add("AgentPassword", pass);
                    cookie.Values.Add("AgentRemember", chkRememberMe.Checked.ToString());
                    cookie.Expires = DateTime.Now.AddHours(2);
                }
                else
                {
                    HttpContext.Current.Response.Cookies.Remove("Agentlogin");
                    Response.Cookies["Agentlogin"].Expires = DateTime.Now;
                }

                if (Request.QueryString["UserId"] != null)
                    Response.Redirect(siteURL + "Printing/PrintingOrders");
                else if (Session["redirectpage"] != null)
                    Response.Redirect(siteURL + "PaymentProcess" + Session["redirectpage"]);
                else
                    Response.Redirect(siteURL + "home");
            }
            else if (Request.QueryString["UserId"] != null || !String.IsNullOrEmpty(txtUsername.Text))
            {
                var list = _ManageUser.AgentDetailsByUserName(txtUsername.Text);
                if (list != null && string.IsNullOrEmpty(list.Password))
                {
                    Session.Add("emailaddressuser", list.ID);
                    Response.Redirect("SendPassword.aspx");
                }
                lblSuccessMsg.Text = "Invalid username or password.";
            }
        }
        catch (Exception ex)
        {
            if (Request.QueryString["UserId"] != null)
                Response.Redirect(siteURL + "Printing/PrintingOrders");
            else if (Session["redirectpage"] != null)
                Response.Redirect(siteURL + "PaymentProcess" + Session["redirectpage"]);
            else if (Session["emailaddressuser"] != null)
                Response.Redirect("SendPassword.aspx");
            else
                Response.Redirect(siteURL + "home", false);
        }
    }
    protected void BtnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(txtUsername.Text))
            AgenLogin();
    }
    void SetPassword()
    {
        if (Request.Cookies["Agentlogin"] != null)
        {
            HttpCookie getCookie = Request.Cookies.Get("Agentlogin");
            if (getCookie != null && getCookie["AgentRemember"] != null && getCookie["AgentRemember"].ToLower() == "true")
            {
                string pass = Common.Decrypt(getCookie.Values["AgentPassword"].Trim(), true);
                txtUsername.Text = getCookie.Values["AgentUserName"].Trim();
                txtPassword.Attributes.Add("value", pass);
                chkRememberMe.Checked = true;
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtUsername.Text = " ";
        txtPassword.Text = " ";
        lblSuccessMsg.Text = string.Empty;
    }
    protected void btnFSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(txtEmail.Text) && !String.IsNullOrEmpty(txtUser.Text))
        {
            var UserExist = _ManageUser.CheckEmail(txtUser.Text, txtEmail.Text);
            if (UserExist != null)
            {
                string Subject = "Reset Password";

                string Body = "<html><head><title></title></head><body><p> Dear " + UserExist.UserName + " , <br /> <br /> Your password is reset on International Rail <br />" +
                          "<br />	Your Agent login details are as below:<br />	<br />	User Name : " + UserExist.UserName + "<br />	Password : " + UserExist.Password + "<br /><br />	Thanks" +
                          "<br />International Rail</p></body></html>";

                var SendEmail = ManageUser.SendMail(Subject, Body, txtEmail.Text, siteId);
                if (SendEmail)
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>PasswordSent();</script>", false);
                    txtUser.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>PasswordErrorSent();</script>", false);
            }
        }
    }
    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect(siteURL + "home");
    }
}