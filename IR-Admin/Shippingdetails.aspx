﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Shippingdetails.aspx.cs" Inherits="IR_Admin.Shippingdetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            checkUncheckCountyTree();
            checkUncheckPassCountyTree();
        });

        function checkUncheckCountyTree() {
            var checkBoxSelector = '#<%=dtlCountry.ClientID%> input[id*="chkRegion"]:checkbox';
            $(checkBoxSelector).click(function () {
                var chkId = (this.id);
                var last = chkId.slice(-1);
                var idtree = "MainContent_dtlCountry_trCountry_" + last;

                if (this.checked) {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', true);
                    });
                }
                else {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });
        }

        function checkUncheckPassCountyTree() {
            var checkBoxSelector = '#<%=dtlPassCountry.ClientID%> input[id*="chkRegion"]:checkbox';
            $(checkBoxSelector).click(function () {
                var chkId = (this.id);
                var last = chkId.slice(-1);
                var idtree = "MainContent_dtlPassCountry_trCountry_" + last;

                if (this.checked) {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', true);
                    });
                }
                else {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });
        }  
    </script>
    <style type="text/css">
        #MainContent_dtlPassCountry tbody tr td
        {
            padding-left: 4px;
            padding-top: 5px;
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Shipping Detail</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Shippingdetails.aspx" class="current">List</a></li>
            <li><a id="aNew" href="Shippingdetails.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdShipping" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="grdShipping_PageIndexChanging" OnRowCommand="grdShipping_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <%#Eval("ShippingName")%>
                                </ItemTemplate>
                                <ItemStyle Width="25%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SiteName">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="25%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price">
                                <ItemTemplate>
                                    <%#Eval("Price")%>
                                </ItemTemplate>
                                <ItemStyle Width="8%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Is front site shipping">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgVisible" CommandArgument='<%#Eval("ID")+","+Eval("IsVisibleFront")%>'
                                        Height="16" CommandName="VisibleFront" AlternateText="visiblefront" ImageUrl='<%#Eval("IsVisibleFront").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsVisibleFront").ToString()=="True" ?"Visible Front":"Not-VisibleFront" %>' />
                                </ItemTemplate>
                                <ItemStyle Width="15%" HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Default">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgDefault" CommandArgument='<%#Eval("ID")+","+Eval("DefaultWhenMissing")%>'
                                        Height="16" CommandName="DefaultIfMissing" AlternateText="default" ImageUrl='<%#Eval("DefaultWhenMissing").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("DefaultWhenMissing").ToString()=="True" ?"Default":"Not-Default" %>' />
                                </ItemTemplate>
                                <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='Shippingdetails.aspx?id=<%#Eval("ID")%>'>
                                        <img src="images/edit.png" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")+","+Eval("IsActive")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                </ItemTemplate>
                                <ItemStyle Width="8%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Title:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTitle" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Price:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPrice" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfPrice" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtPrice" ValidationGroup="rv" />
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtPrice"
                                                            ValidChars="0123456789." runat="server">
                                                        </asp:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="10" Columns="5" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        P2P Country:
                                                    </td>
                                                    <td>
                                                        <div class="cat-inner" style="min-height: 150px; max-height: 250px; margin: 0; padding-top: 0px !important;
                                                            overflow-y: auto;">
                                                            <fieldset class="grid-Region" style="width: 96%">
                                                                <asp:DataList ID="dtlCountry" runat="server" RepeatColumns="2" Width="99%  " OnItemDataBound="dtlCountry_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkRegion" runat="server" Font-Bold="True" Text='<%#Eval("RegionName") %>' />
                                                                        <asp:HiddenField runat="server" ID="hdnRegion" Value='<%#Eval("RegionID") %>' />
                                                                        <asp:TreeView ID="trCountry" runat="server" ShowCheckBoxes="All">
                                                                        </asp:TreeView>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </fieldset>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Pass Country:
                                                    </td>
                                                    <td>
                                                        <div class="cat-inner" style="min-height: 150px; max-height: 250px; padding-top: 0px !important;
                                                            overflow-y: auto;">
                                                            <fieldset class="grid-Region" style="width: 96%">
                                                                <asp:DataList ID="dtlPassCountry" runat="server" RepeatColumns="2" Width="99%" OnItemDataBound="dtlPassCountry_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkRegion" runat="server" Font-Bold="True" Text='<%#Eval("RegionName") %>' />
                                                                        <asp:HiddenField runat="server" ID="hdnRegion" Value='<%#Eval("RegionID") %>' />
                                                                        <asp:TreeView ID="trCountry" runat="server" ShowCheckBoxes="All">
                                                                        </asp:TreeView>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </fieldset>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        IsActive:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox runat="server" ID="chkIsactive" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
