﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Security : Page
    {
        private readonly Masters _oMasters = new Masters();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            FillGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            if (!IsPostBack)
            {
                btnSubmit.Enabled = true;
                if (Request["id"] != null)
                {
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                PageLoadEvent();
                GetDetailsByEdit();

                _SiteID = Master.SiteID;
                SiteSelected();
                FillGrid(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        private void PageLoadEvent()
        {
            IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void FillGrid(Guid siteID)
        {
            grdSecurity.DataSource = _oMasters.GetSecurityList(siteID);
            grdSecurity.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditSecurity();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Security.aspx");
        }

        public void AddEditSecurity()
        {
            try
            {
                var id = string.IsNullOrEmpty(Request.QueryString["id"]) ? new Guid() : Guid.Parse(Request.QueryString["id"]);
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    tab = "2";
                    ViewState["tab"] = "2";
                    return;
                }
                else
                {
                    var listSitId = (from TreeNode node in trSites.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();
                    var res = _oMasters.AddEditSecurity(new SecurityField
                    {
                        ID = id,
                        ListSiteId = listSitId,
                        Title = txtTitle.Text.Trim(),
                        Description = txtDesp.InnerHtml,
                        IsActive = chkIsActv.Checked
                    });
                    ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "Security details updated successfully." : "Security details added successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ClearControls();
                    tab = "1";
                    ViewState["tab"] = "1";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            txtTitle.Text = string.Empty;
            txtDesp.InnerHtml = string.Empty;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        public void GetDetailsByEdit()
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    return;

                btnSubmit.Text = "Update";
                var id = Guid.Parse(Request.QueryString["id"]);
                var rec = _oMasters.GetSecurityById(id);
                txtTitle.Text = rec.Title;
                txtDesp.InnerHtml = rec.Description;
                chkIsActv.Checked = rec.IsActive;
                foreach (TreeNode itm in trSites.Nodes)
                {
                    itm.Checked = rec.ListSiteId.Contains(Guid.Parse(itm.Value));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void grdSecurity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                bool res = _oMasters.DeleteSecurity(id);
                if (res)
                    ShowMessage(1, "Record deleted Successfully.");
            }
            if (e.CommandName == "ActiveInActive")
            {
                bool res = _oMasters.ActiveInactiveSecurity(id);
            }
            _SiteID = Master.SiteID;
            SiteSelected();
            FillGrid(_SiteID);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdSecurity_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSecurity.PageIndex = e.NewPageIndex;
            FillGrid(_SiteID);
        }
    }
}