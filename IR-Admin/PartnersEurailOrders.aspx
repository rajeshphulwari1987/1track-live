﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="PartnersEurailOrders.aspx.cs" Inherits="IR_Admin.Reports.PartnersEurailOrders" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            if (sdate != "" && ldate != "")
                if (Date.parse(sdate) > Date.parse(ldate)) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Partners Eurail Report</h2>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                   <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div class="pass-coloum-two" style="width: 962px; float: left; padding-bottom:8px;">Office:
                           <asp:DropDownList runat="server" ID="ddlOffice" Width="70%"></asp:DropDownList>
                           <div style="float:right;">
                            &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                ValidationGroup="rv" OnClick="btnSubmit_Click" />
                           &nbsp;<asp:Button ID="btnReset" runat="server" Width="85" OnClick="btnCancel_Click" class="button" Text="Reset"/></div>
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" 
                                ClientIDMode="Static" Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin:-1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                            <asp:RegularExpressionValidator ID="regx" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtStartDate" ValidationGroup="rv" ForeColor="Red" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" 
                                ClientIDMode="Static" Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                            <asp:RegularExpressionValidator ID="regx2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtLastDate" ForeColor="Red" ValidationGroup="rv" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div style="float: right;margin-right:2px;">
                           
                            &nbsp;<asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                                Width="159px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div>
                    </div>
                    <div class="grid-head2" style="float: left; width: 100%; min-height: 180px; overflow-x: scroll;
                        overflow-y: hidden; height: auto">
                        <asp:GridView ID="grdPartnersReport" runat="server" AutoGenerateColumns="False" PageSize="10"
                            CellPadding="4" ForeColor="#333333" GridLines="None" Width="270%" AllowPaging="True"
                            OnPageIndexChanging="grdPartnersReport_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Agent">
                                    <ItemTemplate>
                                        <%#Eval("Agent")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent Office">
                                    <ItemTemplate>
                                        <%#Eval("AgentName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Issue Date">
                                    <ItemTemplate>
                                        <%# Eval("TicketIssueDate", "{0: dd/MMM/yyyy}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent reference">
                                    <ItemTemplate>
                                        <%#Eval("STARef")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="1track order number">
                                    <ItemTemplate>
                                        <%#Eval("IROrderNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer">
                                    <ItemTemplate>
                                        <%# Eval("Customer")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Number of pax">
                                    <ItemTemplate>
                                        <%# Eval("NoOfPax")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product supplier">
                                    <ItemTemplate>
                                        <%#Eval("ProductSupplier")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product description">
                                    <ItemTemplate>
                                        <%#Eval("ProductDescription")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales currency">
                                    <ItemTemplate>
                                        <%#Eval("Currency")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Gross Price">
                                    <ItemTemplate>
                                        <%#Eval("GrossPrice", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Protection">
                                    <ItemTemplate>
                                        <%#Eval("TicketProtectionAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Price">
                                    <ItemTemplate>
                                        <%#Eval("TotalPrice", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Commission">
                                    <ItemTemplate>
                                        <%#Eval("TotalCommission", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Markup Amount in Sales Currency">
                                    <ItemTemplate>
                                        <%#Eval("MarkupAmount", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NET Price of Product in Sales Currency">
                                    <ItemTemplate>
                                        <%#Eval("NetPriceOfProduct", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
