﻿using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;
using System.Linq;

namespace IR_Admin
{
    public partial class Manage_StaContact : Page
    {
        readonly Masters _master = new Masters();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        db_1TrackEntities _db = new db_1TrackEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindImages();
                BindPage();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindImages()
        {
            try
            {
                var resultBanner = _master.GetImageListByID(1); //inner Banner images
                if (resultBanner != null)
                {
                    dtBanner.DataSource = resultBanner;
                    dtBanner.DataBind();
                }

                var resultContact = _master.GetImageListByID(7); //contact images
                if (resultContact != null)
                {
                    dtRtPanel.DataSource = resultContact;
                    dtRtPanel.DataBind();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindPage()
        {
            try
            {
                imgMainBanner.Src = SiteUrl + "images/page/banner.jpg";
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        ContentHead.InnerHtml = result.PageHeading;
                        ContentText.InnerHtml = result.PageContent;
                        addBlock.InnerHtml = result.ContactPanel;

                        string[] bannerimgid = result.BannerIDs.TrimEnd(',').Split(',');
                        int bimgid = 0;
                        foreach (string obj in bannerimgid)
                        {
                            if (obj != "")
                                bimgid = Convert.ToInt32(obj);
                        }
                        var bimgpath = _db.tblImages.FirstOrDefault(x => x.ID == bimgid);
                        if (bimgpath != null)
                            imgMainBanner.Src = bimgpath.ImagePath;
                    }
                    else
                        BindStaticImgs();
                }
                else
                    BindStaticImgs();
            }
            catch (Exception ex) { throw ex; }
        }

        protected void dtBanner_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string url = null;
                    if (Session["url"] != null)
                        url = Session["url"].ToString();

                    if (url != string.Empty)
                    {
                        var result = _master.GetPageDetailsByUrl(url);
                        if (result != null)
                        {
                            var bannerid = result.BannerIDs.Trim();
                            string[] arrID = bannerid.Split(',');
                            foreach (string id in arrID)
                            {
                                var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                                if (cbID != null)
                                {
                                    if (id == cbID.Value)
                                    {
                                        cbID.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private void BindStaticImgs()
        {
            try
            {
                imgContactHome.Src = SiteUrl + "images/page/home-red.png";
                imgContactCall.Src = SiteUrl + "images/page/icon-call-red.png";
                imgContactEmail.Src = SiteUrl + "images/page/icon-email-red.png";
            }
            catch (Exception ex) { throw ex; }
        }
    }
}