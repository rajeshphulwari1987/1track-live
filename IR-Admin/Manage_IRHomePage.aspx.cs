﻿using System;
using System.Web.UI;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;

namespace IR_Admin
{
    public partial class Manage_IRHomePage : Page
    {
        readonly Masters _master = new Masters();
        db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        List<BannerImageUrls> BannerItemList = new List<BannerImageUrls>();
        public string liItems = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindImages();
                BindPage();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindImages()
        {
            try
            {
                var resultbanner = _master.GetImageListByID(1); //banner images
                dtBanner.DataSource = resultbanner;
                dtBanner.DataBind();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindPage()
        {
            try
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        ContentHead.InnerHtml = result.HomeSearchText;
                        ContentText.InnerHtml = result.HomeCountryText;

                        if (!string.IsNullOrEmpty(result.HomePageHeading1))
                            ContentHeading1.InnerHtml = result.HomePageHeading1;
                        if (!string.IsNullOrEmpty(result.HomePageHeading2))
                            ContentHeading2.InnerHtml = result.HomePageHeading2;
                        if (!string.IsNullOrEmpty(result.HomePageDescription1))
                            ContentDescription1.InnerHtml = result.HomePageDescription1;
                        if (!string.IsNullOrEmpty(result.HomePageDescription2))
                            ContentDescription2.InnerHtml = result.HomePageDescription2;

                        if (!string.IsNullOrEmpty(result.BannerIDs))
                        {
                            string[] bannerimgid = result.BannerIDs.TrimEnd(',').Split(',');
                            int[] bannerNewImgId = Array.ConvertAll(bannerimgid, s => int.Parse(s));

                            BannerItemList = _db.tblImages.Where(x => bannerNewImgId.Contains(x.ID)).Select(y => new BannerImageUrls
                            {
                                Id = y.ID,
                                ImageUrl = y.ImagePath
                            }).ToList();

                            if (BannerItemList != null && BannerItemList.Count > 0)
                            {
                                rptEurailPromotion.DataSource = BannerItemList.ToList();
                                rptEurailPromotion.DataBind();

                                div_EurailPromotion.Visible = false;
                                div_EurailPromotionShow.Visible = true;
                            }

                            if (BannerItemList != null && BannerItemList.Count > 0)
                            {
                                if (BannerItemList.Count > 0)
                                {
                                    for (int i = 0; i < BannerItemList.Count; i++)
                                    {
                                        if (i == 0)
                                            liItems += "<li data-target='#div_EurailPromotionShow' data-slide-to='" + i + "' class='active'></li>";
                                        else
                                            liItems += "<li data-target='#div_EurailPromotionShow' data-slide-to='" + i + "'></li>";
                                    }
                                    listItem.InnerHtml = liItems;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        protected void dtBanner_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string url = null;
                    if (Session["url"] != null)
                        url = Session["url"].ToString();

                    if (url != string.Empty)
                    {
                        tblPage result = _master.GetPageDetailsByUrl(url);
                        if (result != null)
                        {
                            var bannerid = result.BannerIDs.Trim();
                            string[] arrId = bannerid.Split(',');
                            foreach (string id in arrId)
                            {
                                var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                                if (cbID != null)
                                {
                                    if (id == cbID.Value)
                                    {
                                        cbID.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}

public class BannerImageUrls
{
    public int Id { get; set; }
    public string ImageUrl { get; set; }
}