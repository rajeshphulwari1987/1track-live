﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ViewMenu : Page
    {
        readonly Masters _Master = new Masters();
        db_1TrackEntities db = new db_1TrackEntities();
        public string tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                FillParentCategoryDDl();
                FillMenu(Guid.Parse("00000000-0000-0000-0000-000000000000"));
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    tab = "2";
                    BindNavigationListForEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        public void BindNavigationListForEdit(Guid ID)
        {
            try
            {
                var result = _Master.GetNavigationListEdit(ID);
                txtName.Text = result.Name;
                ddlParentCategory.ClearSelection();
                txtPageurl.Text = result.PageName;
                btnSubmit.Text = "Update";
                chkCorp.Checked = Convert.ToBoolean(result.IsCorporate);
                chkActive.Checked = Convert.ToBoolean(result.IsActive);
                chkIsTop.Checked = Convert.ToBoolean(result.IsTop);
                chkIsBottom.Checked = Convert.ToBoolean(result.IsBottom);
                var parentIdFirst = db.tblNavigations.FirstOrDefault(x => x.ID == ID);
                if (parentIdFirst != null)
                {
                    Guid id = Guid.Parse("00000000-0000-0000-0000-000000000000");
                    var parentIdSecond = db.tblNavigations.FirstOrDefault(x => x.ID == parentIdFirst.ParentID && x.ParentID != id);
                    if (parentIdSecond != null)
                    {
                        ddlParentCategory.Items.FindByValue(parentIdSecond.ParentID.ToString()).Selected = true;
                        fillChildCAregory(Guid.Parse(ddlParentCategory.SelectedValue));
                        ddlSecondParentCategory.ClearSelection();
                        ddlSecondParentCategory.Items.FindByValue(parentIdFirst.ParentID.ToString()).Selected = true;
                        ddlParentCategory.Enabled = false;
                        ddlSecondParentCategory.Enabled = false;
                    }
                    else
                    {
                        ddlParentCategory.Enabled = false;
                        ddlParentCategory.Items.FindByValue(result.ParentID.ToString()).Selected = true;
                    }
                }
                else
                {
                    ddlParentCategory.Enabled = true;
                    ddlSecondParentCategory.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void FillParentCategoryDDl()
        {
            try
            {
                ddlParentCategory.DataSource = _Master.GetParentNavigationList(Guid.Parse("00000000-0000-0000-0000-000000000000")).ToList().OrderBy(x => x.Name);
                ddlParentCategory.DataTextField = "Name";
                ddlParentCategory.DataValueField = "ID";
                ddlParentCategory.DataBind();
                ddlParentCategory.Items.Insert(0, new ListItem("--Category--", "00000000-0000-0000-0000-000000000000"));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSubmit.Text == "Submit")
                {
                    var _Navigation = new Navigation();
                    _Navigation.Name = txtName.Text;
                    _Navigation.ID = Guid.NewGuid();
                    _Navigation.IsCorporate = chkCorp.Checked;
                    _Navigation.IsActive = chkActive.Checked;
                    _Navigation.PageName = txtPageurl.Text;

                    if (ddlParentCategory.SelectedIndex == 0)
                        _Navigation.ParentID = Guid.Parse("00000000-0000-0000-0000-000000000000");
                    else if (ddlParentCategory.SelectedIndex > 0 && ddlSecondParentCategory.SelectedIndex > 0)
                        _Navigation.ParentID = Guid.Parse(ddlSecondParentCategory.SelectedValue);
                    else if (ddlParentCategory.SelectedIndex > 0)
                        _Navigation.ParentID = Guid.Parse(ddlParentCategory.SelectedValue);

                    if (ddlParentCategory.SelectedIndex != 0)
                    {
                        var _ParentID = Guid.Parse(ddlParentCategory.SelectedItem.Value.ToString());
                        var MaxSortOrder = db.tblNavigations.Where(x => x.ParentID == _ParentID).Max(t => t.CSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.CSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.CSortOrder = 1;
                        }

                        _Navigation.PSortOrder = 0;
                    }
                    else
                    {
                        var MaxSortOrder = db.tblNavigations.Max(t => t.PSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.PSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.PSortOrder = 1;
                        }

                        _Navigation.CSortOrder = 0;
                    }

                    _Navigation.IsTop = chkIsTop.Checked;
                    _Navigation.IsBottom = chkIsBottom.Checked;
                    int res = _Master.AddNavigation(_Navigation);
                    if (res > 0)
                    {
                        ShowMessage(1, "You have successfully Added.");
                        txtPageurl.Text = string.Empty;
                        txtName.Text = string.Empty;
                        ddlParentCategory.SelectedIndex = 0;
                        ddlSecondParentCategory.Items.Clear();
                        Response.Redirect("ViewMenu.aspx");
                    }
                }
                else if (btnSubmit.Text == "Update")
                {
                    var _Navigation = new Navigation();
                    _Navigation.Name = txtName.Text;
                    _Navigation.ID = Guid.Parse(Request["edit"]);
                    _Navigation.IsCorporate = chkCorp.Checked;
                    _Navigation.IsActive = chkActive.Checked;
                    _Navigation.PageName = txtPageurl.Text;
                    _Navigation.IsTop = chkIsTop.Checked;
                    _Navigation.IsBottom = chkIsBottom.Checked;

                    if (ddlParentCategory.SelectedIndex == 0)
                        _Navigation.ParentID = Guid.Parse("00000000-0000-0000-0000-000000000000");
                    else if (ddlParentCategory.SelectedIndex > 0 && ddlSecondParentCategory.SelectedIndex > 0)
                        _Navigation.ParentID = Guid.Parse(ddlSecondParentCategory.SelectedValue);
                    else if (ddlParentCategory.SelectedIndex > 0)
                        _Navigation.ParentID = Guid.Parse(ddlParentCategory.SelectedValue);


                    var ID = Guid.Parse(Request["edit"]);
                    var ParentId = db.tblNavigations.Where(x => x.ID == ID).SingleOrDefault();
                    if (ParentId.ParentID != null)
                    {
                        hdID.Value = ParentId.ParentID.ToString();
                    }

                    if (ddlParentCategory.SelectedIndex != 0)
                    {
                        var _ParentID = Guid.Parse(ddlParentCategory.SelectedItem.Value.ToString());
                        var MaxSortOrder = db.tblNavigations.Where(x => x.ParentID == _ParentID).Max(t => t.CSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.CSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.CSortOrder = 1;
                        }
                        _Navigation.PSortOrder = 0;
                    }
                    else
                    {
                        var MaxSortOrder = db.tblNavigations.Max(t => t.PSortOrder);
                        if (MaxSortOrder != null)
                        {
                            _Navigation.PSortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                        }
                        else
                        {
                            _Navigation.PSortOrder = 1;
                        }

                        _Navigation.CSortOrder = 0;
                    }

                    int res = _Master.UpdateNavigation(_Navigation);
                    if (res > 0)
                    {
                        ShowMessage(1, "You have successfully Updated.");
                        txtPageurl.Text = string.Empty;
                        txtName.Text = string.Empty;
                        SetIndex();
                        ddlParentCategory.SelectedIndex = 0;
                        ddlSecondParentCategory.Items.Clear();
                        Response.Redirect("ViewMenu.aspx");
                    }
                }
                FillMenu(Guid.Parse("00000000-0000-0000-0000-000000000000"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewMenu.aspx");
        }

        protected void SetIndex()
        {
            using (var db = new db_1TrackEntities())
            {
                if (hdID.Value != "00000000-0000-0000-0000-000000000000")
                {
                    Guid Id = Guid.Parse(hdID.Value.ToString());
                    var obj = db.tblNavigations.ToList().Where(x => x.ParentID == Id).OrderBy(t => t.CSortOrder);
                    int i = 1;
                    foreach (var o in obj)
                    {
                        tblNavigation oMenu = db.tblNavigations.SingleOrDefault(t => t.ID == o.ID);
                        oMenu.CSortOrder = i;
                        db.SaveChanges();
                        i += 1;
                    }
                }
                else
                {
                    Guid Id = Guid.Parse(hdID.Value.ToString());
                    var obj = db.tblNavigations.ToList().Where(x => x.ParentID == Id).OrderBy(t => t.PSortOrder);
                    int i = 1;
                    foreach (var o in obj)
                    {
                        tblNavigation oMenu = db.tblNavigations.SingleOrDefault(t => t.ID == o.ID);
                        oMenu.PSortOrder = i;
                        db.SaveChanges();
                        i += 1;
                    }
                }
            }
        }

        public string GetMenuName(Guid M_Id)
        {
            string menuname = "";
            if (M_Id != null)
            {
                var Menus = db.tblNavigations.FirstOrDefault(x => x.ID == M_Id);
                menuname = Menus != null ? Menus.Name : "";
            }
            return menuname;
        }

        private void FillMenu(Guid ParentID)
        {
            List<tblNavigation> menu_result = db.tblNavigations.Where(x => x.ParentID == ParentID).OrderBy(x => x.PSortOrder).ToList();
            List<tblNavigation> menulist = new List<tblNavigation>();
            foreach (var item in menu_result)
            {
                tblNavigation menu = new tblNavigation();
                menu = item;
                menulist.Add(menu);
                var submenu_result = db.tblNavigations.Where(x => x.ParentID == item.ID).OrderBy(x => x.CSortOrder).ToList();
                if (submenu_result != null && submenu_result.Count > 0)
                {
                    foreach (var subitem in submenu_result)
                    {
                        tblNavigation submenu = new tblNavigation();
                        submenu = subitem;
                        menulist.Add(submenu);
                        var subchildbmenu_result = db.tblNavigations.Where(x => x.ParentID == subitem.ID).OrderBy(x => x.CSortOrder).ToList();
                        if (subchildbmenu_result != null && subchildbmenu_result.Count > 0)
                        {
                            foreach (var subchilditem in subchildbmenu_result)
                            {
                                tblNavigation subchildmenu = new tblNavigation();
                                subchildmenu = subchilditem;
                                menulist.Add(subchildmenu);
                            }
                        }
                    }
                }
            }
            grdMenu.DataSource = menulist;
            grdMenu.DataBind();
            HideIndex();
        }

        private void HideIndex()
        {
            if (grdMenu.Rows.Count > 0)
            {
                (grdMenu.Rows[0].FindControl("imgUp")).Visible = false;
                (grdMenu.Rows[grdMenu.Rows.Count - 1].FindControl("imgDown")).Visible = false;
            }
        }

        protected void grdMenu_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");

                if (e.CommandName == "Modify")
                {
                    Guid M_ID = Guid.Parse(e.CommandArgument.ToString());
                    Response.Redirect("ViewMenu.aspx?edit=" + M_ID);
                }
                if (e.CommandName == "Remove")
                {
                    string[] CommandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid ID = Guid.Parse(CommandArgumentValues[0]);
                    int CSortorder = Convert.ToInt32(CommandArgumentValues[3]);
                    Guid ParentID = Guid.Parse(CommandArgumentValues[1]);
                    int PSortorder = Convert.ToInt32(CommandArgumentValues[2]);

                    if (ParentID != Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        var submenu_result = db.tblNavigations.Where(x => x.ParentID == ID).ToList();
                        if (submenu_result.Count > 0)
                        {
                            ShowMessage(1, "First Delete The Child Navigation");
                        }
                        else
                        {
                            tblNavigation menu = db.tblNavigations.First(f => f.ID == ID);
                            db.tblNavigations.DeleteObject(menu);
                            db.SaveChanges();
                            var obj = db.tblNavigations.ToList().Where(x => x.ParentID == ParentID).OrderBy(t => t.CSortOrder);
                            int i = 1;
                            foreach (var o in obj)
                            {
                                tblNavigation oMenu = db.tblNavigations.SingleOrDefault(t => t.ID == o.ID);
                                oMenu.CSortOrder = i;
                                db.SaveChanges();
                                i += 1;
                            }
                        }
                    }
                    else
                    {
                        var submenu_result = db.tblNavigations.Where(x => x.ParentID == ID).ToList();
                        if (submenu_result.Count > 0)
                        {
                            ShowMessage(1, "First Delete The Child Navigation");
                        }
                        else
                        {
                            tblNavigation menu = db.tblNavigations.First(f => f.ID == ID);
                            db.tblNavigations.DeleteObject(menu);
                            db.SaveChanges();
                            var obj = db.tblNavigations.ToList().Where(x => x.ParentID == ParentID).OrderBy(t => t.PSortOrder);
                            int i = 1;
                            foreach (var o in obj)
                            {
                                tblNavigation oMenu = db.tblNavigations.SingleOrDefault(t => t.ID == o.ID);
                                oMenu.PSortOrder = i;
                                db.SaveChanges();
                                i += 1;
                            }
                        }
                    }
                    FillMenu(Guid.Parse("00000000-0000-0000-0000-000000000000"));
                }
                if (e.CommandName == "Up")
                {
                    string[] CommandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid FirstID = Guid.Parse(CommandArgumentValues[1]);
                    int CSortorder = Convert.ToInt32(CommandArgumentValues[0]);
                    Guid FirstParentID = Guid.Parse(CommandArgumentValues[2]);
                    int PSortorder = Convert.ToInt32(CommandArgumentValues[3]);

                    if (FirstParentID != Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        int IncrementedSortOrder = CSortorder - 1;
                        var submenu_result = db.tblNavigations.Where(x => x.ParentID == FirstParentID && x.CSortOrder == IncrementedSortOrder).SingleOrDefault();
                        if (submenu_result != null)
                        {
                            submenu_result.CSortOrder = submenu_result.CSortOrder + 1;
                        }
                        else
                        {
                            goto DontDo;
                        }
                        db.SaveChanges();
                        var oMenu = db.tblNavigations.FirstOrDefault(x => x.ID == FirstID);
                        if (oMenu != null)
                        {
                            oMenu.CSortOrder = oMenu.CSortOrder - 1;
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        int IncrementedSortOrder = PSortorder - 1;
                        var submenu_result = db.tblNavigations.Where(x => x.ParentID == FirstParentID && x.PSortOrder == IncrementedSortOrder).SingleOrDefault();
                        if (submenu_result != null)
                        {
                            submenu_result.PSortOrder = submenu_result.PSortOrder + 1;
                        }
                        else
                        {
                            goto DontDo;
                        }
                        db.SaveChanges();
                        var oMenu = db.tblNavigations.FirstOrDefault(x => x.ID == FirstID);
                        if (oMenu != null)
                        {
                            oMenu.PSortOrder = oMenu.PSortOrder - 1;
                        }

                        db.SaveChanges();
                    }
                }
                if (e.CommandName == "Down")
                {
                    string[] CommandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid FirstID = Guid.Parse(CommandArgumentValues[1]);
                    int CSortorder = Convert.ToInt32(CommandArgumentValues[0]);
                    Guid FirstParentID = Guid.Parse(CommandArgumentValues[2]);
                    int PSortorder = Convert.ToInt32(CommandArgumentValues[3]);

                    if (FirstParentID != Guid.Parse("00000000-0000-0000-0000-000000000000"))
                    {
                        int IncrementedSortOrder = CSortorder + 1;
                        var First_result = db.tblNavigations.Where(x => x.ParentID == FirstParentID && x.CSortOrder == IncrementedSortOrder).SingleOrDefault();
                        if (First_result != null)
                        {
                            First_result.CSortOrder = CSortorder;
                        }
                        else
                        {
                            goto DontDo;
                        }
                        db.SaveChanges();
                        var Second_result = db.tblNavigations.FirstOrDefault(x => x.ID == FirstID);
                        if (Second_result != null)
                        {
                            Second_result.CSortOrder = CSortorder + 1;
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        int IncrementedSortOrder = PSortorder + 1;
                        var First_result = db.tblNavigations.Where(x => x.ParentID == FirstParentID && x.PSortOrder == IncrementedSortOrder).SingleOrDefault();
                        if (First_result != null)
                        {
                            First_result.PSortOrder = PSortorder;
                        }
                        else
                        {
                            goto DontDo;
                        }
                        db.SaveChanges();
                        var Second_result = db.tblNavigations.FirstOrDefault(x => x.ID == FirstID);
                        if (Second_result != null)
                        {
                            Second_result.PSortOrder = PSortorder + 1;
                        }

                        db.SaveChanges();
                    }
                }
                if (e.CommandName == "ActiveInActive")
                {
                    Guid M_ID = Guid.Parse(e.CommandArgument.ToString());
                    tblNavigation menu = db.tblNavigations.First(f => f.ID == M_ID);
                    menu.IsActive = !(menu.IsActive);
                    db.SaveChanges();
                }

            DontDo:
                FillMenu(Guid.Parse("00000000-0000-0000-0000-000000000000"));
                FillMenu(Guid.Parse("00000000-0000-0000-0000-000000000000"));
            }
            catch (Exception ee)
            {
                //  ShowMessage(1, ee.ToString());
            }
        }

        public void fillChildCAregory(Guid CategoryId)
        {
            ddlSecondParentCategory.DataSource = _Master.GetParentNavigationList(CategoryId);
            ddlSecondParentCategory.DataTextField = "Name";
            ddlSecondParentCategory.DataValueField = "ID";
            ddlSecondParentCategory.DataBind();
            ddlSecondParentCategory.Items.Insert(0, new ListItem("--Category--", "0"));
        }

        protected void ddlParentCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                fillChildCAregory(Guid.Parse(ddlParentCategory.SelectedValue));
                tab = "2";
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }
    }
}
