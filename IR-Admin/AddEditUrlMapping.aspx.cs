﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class AddEditUrlMapping : Page
    {
        readonly Masters _oM = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            FillCommonddl();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillCommonddl();
                if (Request.QueryString["ID"] != null)
                    FillDetail(Convert.ToInt32(Request.QueryString["ID"]));
            }
        }

        public void FillCommonddl()
        {
            try
            {
                ddlSite.DataSource = _oM.GetActiveSiteList();
                ddlSite.DataTextField = "DisplayName";
                ddlSite.DataValueField = "ID";
                ddlSite.DataBind();
                ddlSite.Items.Insert(0, new ListItem("--Site--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        private void FillDetail(int id)
        {
            var oObj = _oM.GetUrlMapingTable(id);
            ddlSite.SelectedValue = Convert.ToString(oObj.SiteId);
            txtOldURL.Text = oObj.OldURL;
            txtNewURL.Text = oObj.NewURL;
            txtDefaultURL.Text = oObj.DefaultURL;
            txtNote.Text = oObj.Note;
            if (oObj.IsActive != null) chkActive.Checked = oObj.IsActive.Value;
            btnSubmit.Text = "Update";
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int id = 0;
                var siteId = Guid.Parse(ddlSite.SelectedValue);
                if (Request.QueryString["Id"] != null)
                    id = Convert.ToInt32(Request.QueryString["Id"]);
                var um = new clsUrlMaping { ID = id, OldURL = txtOldURL.Text, SiteID = siteId, NewURL = txtNewURL.Text, DefaultURL = txtDefaultURL.Text, IsActive = chkActive.Checked, CreatedBy = AdminuserInfo.UserID, CreatedOn = DateTime.Now, ModifyBy = AdminuserInfo.UserID, ModifyOn = DateTime.Now, Note = txtNote.Text};
                _oM.AddUpdateUrlMaping(um);
                Response.Redirect("UrlMaping.aspx", false);
            }
            catch (Exception Ex)
            {
                ShowMessage(2, Ex.Message);
            }
        }
    }
}