﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ProductValidUpTo : Page
    {
        readonly private Masters _oMasters = new Masters();

        List<RepeaterListItem> list = new List<RepeaterListItem>();
        List<ValidUpTo> gridlist = new List<ValidUpTo>();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            divlist.Style.Add("display", "block");
            divNew.Style.Add("display", "none");
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(hdnProdId.Value))
                {
                    divlist.Style.Add("display", "none");
                    divNew.Style.Add("display", "block");
                }
                PageLoadEvent();
            }
        }

        private void PageLoadEvent()
        {
            try
            {
                var listLang = _oMasters.GetLanguangesList();
                var langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                ddlLanguage.DataSource = listLang.Where(x => x.ID == langid);
                ddlLanguage.DataTextField = "Name";
                ddlLanguage.DataValueField = "ID";
                ddlLanguage.DataBind();
                ViewState["NoOfItems"] = 0;

                FillGrid();
                BindRepeter(null);



                for (int j = 1; j <= 50; j++)
                {
                    ddlDay.Items.Add(new ListItem(j.ToString(), j.ToString()));
                    if (j <= 10)
                        ddlYear.Items.Add(new ListItem(j.ToString(), j.ToString()));
                    if (j <= 12)
                        ddlMonth.Items.Add(new ListItem(j.ToString(), j.ToString()));
                }
                ddlDay.Items.Insert(0, new ListItem("Day", "0"));
                ddlMonth.Items.Insert(0, new ListItem("Month", "0"));
                ddlYear.Items.Insert(0, new ListItem("Year", "0"));

            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        public void BindRepeter(List<RepeaterListItem> listItm)
        {
            try
            {
                if (listItm == null)
                {
                    int numOfItem = Convert.ToInt32(ViewState["NoOfItems"]);
                    for (int i = 0; i < numOfItem; i++)
                    {
                        var page = new RepeaterListItem { NoOfItems = i };
                        list.Add(page);
                    }
                    Session["rptProdList"] = list;
                    rptProductValidUpTo.DataSource = list;
                }
                else
                    rptProductValidUpTo.DataSource = listItm;
                rptProductValidUpTo.DataBind();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected List<RepeaterListItem> AddRepeaterItemInList()
        {
            foreach (RepeaterItem item in rptProductValidUpTo.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    var idpnam = item.FindControl("hdnProdValUpToNameId") as HiddenField;
                    var txtPass = item.FindControl("txtPassName") as TextBox;
                    var rdoMissing = item.FindControl("rdoMissing") as RadioButton;
                    var page = new RepeaterListItem
                    {
                        Id = !String.IsNullOrEmpty(idpnam.Value) ? Guid.Parse(idpnam.Value) : new Guid(),
                        NoOfItems = item.ItemIndex,
                        LanguageId = ddlLang.SelectedValue,
                        Name = txtPass.Text,
                        IsMissing = rdoMissing.Checked
                    };
                    list.Add(page);
                }
            }
            return list;
        }

        protected void FillGrid()
        {
            try
            {

                gridlist = _oMasters.GetProductValidUpToNameList();
                if (gridlist != null && gridlist.Count > 0)
                {
                    ddlValidity.DataSource = gridlist.Select(x => new { x.Name }).Distinct().ToList();
                    ddlValidity.DataTextField = "Name";
                    ddlValidity.DataValueField = "Name";
                    ddlValidity.DataBind();
                    ddlValidity.Items.Insert(0, new ListItem("--Validity--", "-1"));

                    ddlPromoText.DataSource = gridlist.Where(x => !string.IsNullOrEmpty(x.Note)).Select(x => new { x.Note }).Distinct().ToList();
                    ddlPromoText.DataTextField = "Note";
                    ddlPromoText.DataValueField = "Note";
                    ddlPromoText.DataBind();
                    ddlPromoText.Items.Insert(0, new ListItem("--Note--", "-1"));

                    grvProductValidUpTo.DataSource = gridlist;
                    grvProductValidUpTo.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Repeater / Grid command
        protected void grvProductValidUpTo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProductValidUpTo.PageIndex = e.NewPageIndex;
            FilterResult();
            divlist.Style.Add("display", "block");
            divNew.Style.Add("display", "none");
        }
        protected void grvProductValidUpTo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Modify")
                {
                    btnSubmit.Text = "Update";
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var opvut = _oMasters.GetProductValidUpToById(id);
                    hdnProdId.Value = id.ToString();
                    GetValueForEdit();
                    divlist.Style.Add("display", "none");
                    divNew.Style.Add("display", "block");
                }
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    bool result = _oMasters.DeleteProductValidUpTo(id);
                    if (result)
                        ShowMessage(1, "Record deleted successfully");
                    FillGrid();
                    divlist.Style.Add("display", "block");
                    divNew.Style.Add("display", "none");
                }
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _oMasters.ActiveInactivecProductValidUpTo(id);
                    FillGrid();
                    divlist.Style.Add("display", "block");
                    divNew.Style.Add("display", "none");
                }
                if (e.CommandName == "IsFlexi")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _oMasters.IsFlexiProductValidUpTo(id);
                    FillGrid();
                    divlist.Style.Add("display", "block");
                    divNew.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }

        }
        protected void rptProductValidUpTo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var ddlLang = e.Item.FindControl("ddlLanguage") as DropDownList;
                    var hdnLangId = e.Item.FindControl("hdnLangId") as HiddenField;

                    var list = _oMasters.GetLanguangesList();
                    if (list != null)
                    {
                        var langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                        list.RemoveAll(x => x.ID == langid);
                        ddlLang.DataSource = list;
                    }
                    ddlLang.DataTextField = "Name";
                    ddlLang.DataValueField = "ID";
                    ddlLang.DataBind();

                    if (hdnLangId != null && !String.IsNullOrEmpty(hdnLangId.Value))
                        ddlLang.SelectedValue = hdnLangId.Value;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        #endregion

        protected void btnAddMore_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["NoOfItems"] != null)
                {
                    ViewState["NoOfItems"] = Convert.ToInt32(ViewState["NoOfItems"]) + 1;
                    var newpage = new RepeaterListItem { NoOfItems = Convert.ToInt32(ViewState["NoOfItems"]) };
                    list.Add(newpage);
                    list = AddRepeaterItemInList();
                    Session["rptProdList"] = list;
                    BindRepeter(list);

                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptProductValidUpTo.Items.Count > 0)
                {
                    list = AddRepeaterItemInList();

                    //--Add Removed Record in list at edit time
                    if (!String.IsNullOrEmpty(hdnProdId.Value))
                    {
                        var record = list.FirstOrDefault();
                        if (Session["reovedProdList"] == null)
                        {
                            var removeList = new List<RepeaterListItem> { record };
                            Session["reovedProdList"] = removeList;
                        }
                        else
                        {
                            var removeList = Session["reovedProdList"] as List<RepeaterListItem>;
                            if (removeList != null) removeList.Add(record);
                            Session["reovedProdList"] = removeList;
                        }
                    }

                    list.RemoveAt(0);
                    Session["rptProdList"] = list;
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Valid())
                {
                    AddEditProductValidUpTo();
                    divlist.Style.Add("display", "block");
                    divNew.Style.Add("display", "none");
                }
                else
                {
                    ShowMessage(2, "You have selected one Language multiple time");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductValidUpTo.aspx");
        }

        void AddEditProductValidUpTo()
        {
            try
            {
                var id = !String.IsNullOrEmpty(hdnProdId.Value) ? Guid.Parse(hdnProdId.Value) : new Guid();
                var idname = !String.IsNullOrEmpty(hdnProdValUpToNameId.Value) ? Guid.Parse(hdnProdValUpToNameId.Value) : new Guid();
                RemoveProductValidToUpNameOfDiffLang(id);

                if (txtPassName.Text.Trim() != "")
                {
                    id = _oMasters.AddProductValidUpTo(new tblProductValidUpTo
                        {
                            ID = !String.IsNullOrEmpty(hdnProdId.Value) ? Guid.Parse(hdnProdId.Value) : new Guid(),
                            CreatedBy = AdminuserInfo.UserID,
                            CreatedOn = DateTime.Now,
                            IsActive = chkIsActv.Checked,
                            Day = Convert.ToInt32(ddlDay.SelectedValue),
                            Month = Convert.ToInt32(ddlMonth.SelectedValue),
                            Year = Convert.ToInt32(ddlYear.SelectedValue),
                            EurailCode = Convert.ToInt32(txtEurailCode.Text),
                            PromoPassText = txtPromoText.Text,
                            Note = txtnote.Text
                        });

                    _oMasters.AddProductValidUpToName(new tblProductValidUpToName
                        {
                            ID = idname,
                            DefaultWhenMissing = rdoMissing.Checked,
                            LanguageID = Guid.Parse(ddlLanguage.SelectedValue),
                            Name = txtPassName.Text.Trim(),
                            ProductValidUpToID = id
                        });
                }

                foreach (RepeaterItem item in rptProductValidUpTo.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                        var idpnam = item.FindControl("hdnProdValUpToNameId") as HiddenField;
                        var txtPass = item.FindControl("txtPassName") as TextBox;
                        var rdoMis = item.FindControl("rdoMissing") as RadioButton;
                        if (txtPass.Text.Trim() != "")
                            _oMasters.AddProductValidUpToName(new tblProductValidUpToName
                            {
                                DefaultWhenMissing = rdoMis.Checked,
                                LanguageID = Guid.Parse(ddlLang.SelectedValue),
                                Name = txtPass.Text.Trim(),
                                ProductValidUpToID = id,
                                ID = !String.IsNullOrEmpty(idpnam.Value) ? Guid.Parse(idpnam.Value) : new Guid()
                            });
                    }
                }

                ShowMessage(1, !String.IsNullOrEmpty(hdnProdId.Value) ? "Traveller Validity updated successfully." : "Traveller Validity added successfully.");
                hdnProdId.Value = txtnote.Text = string.Empty;
                hdnProdValUpToNameId.Value = string.Empty;
                FillGrid();
                divlist.Style.Add("display", "block");
                divNew.Style.Add("display", "none");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void RemoveProductValidToUpNameOfDiffLang(Guid id)
        {
            if (Session["reovedProdList"] != null)
            {
                var removeList = Session["reovedProdList"] as List<RepeaterListItem>;
                if (removeList != null)
                    foreach (var item in removeList.Where(item => !String.IsNullOrEmpty(item.Name)))
                    {
                        _oMasters.DeleteProductValidUpToName(id, Guid.Parse(item.LanguageId), item.Name);
                    }
            }

            Session["reovedProdList"] = null;
        }

        void GetValueForEdit()
        {
            try
            {
                var listResult = _oMasters.GetProductValidUpToNameById(Guid.Parse(hdnProdId.Value));
                var listResultP = _oMasters.GetProductValidUpToById(Guid.Parse(hdnProdId.Value));
                if (listResult == null)
                    return;

                var idLang = _oMasters.GetLanguageId();
                var defltRec = listResult.FirstOrDefault(x => x.LanguageID == idLang);
                if (defltRec != null)
                {
                    hdnProdValUpToNameId.Value = defltRec.ID.ToString();
                    txtPassName.Text = defltRec.Name;
                    ddlLanguage.SelectedValue = defltRec.LanguageID.ToString();
                    rdoMissing.Checked = defltRec.DefaultWhenMissing;
                    chkIsActv.Checked = listResultP.IsActive;

                    ddlDay.SelectedValue = listResultP.Day.ToString();
                    ddlMonth.SelectedValue = listResultP.Month.ToString();
                    ddlYear.SelectedValue = listResultP.Year.ToString();
                    txtPromoText.Text = listResultP.PromoPassText;
                    txtEurailCode.Text = listResultP.EurailCode.ToString();
                    txtnote.Text = listResultP.Note;
                    listResult.Remove(defltRec);

                }

                var listItm = new List<RepeaterListItem>();
                foreach (var item in listResult)
                {
                    listItm.Add(new RepeaterListItem
                    {
                        Id = item.ID,
                        Name = item.Name,
                        IsMissing = item.DefaultWhenMissing,
                        LanguageId = item.LanguageID.ToString(),
                        NoOfItems = listItm.Count
                    });
                }
                BindRepeter(listItm);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected bool Valid()
        {
            var lngIdList = new List<Guid>();
            foreach (RepeaterItem item in rptProductValidUpTo.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    var txtPass = item.FindControl("txtPassName") as TextBox;
                    var id = Guid.Parse(ddlLang.SelectedValue);

                    if (!String.IsNullOrEmpty(txtPass.Text.Trim()) && lngIdList.Any(x => x.ToString() == id.ToString()) == false)
                    {
                        lngIdList.Add(id);
                    }
                    else if (!String.IsNullOrEmpty(txtPass.Text.Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddlPromoText_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterResult();
        }

        protected void ddlValidity_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterResult();
        }

        void FilterResult()
        {
            gridlist = _oMasters.GetProductValidUpToNameList();
            if (gridlist != null && gridlist.Count > 0)
            {
                if (ddlPromoText.SelectedValue != "-1" && ddlValidity.SelectedValue != "-1")
                    gridlist = gridlist.Where(x => x.Note == ddlPromoText.SelectedValue && x.Name == ddlValidity.SelectedValue).ToList();

                if (ddlPromoText.SelectedValue != "-1" && ddlValidity.SelectedValue == "-1")
                    gridlist = gridlist.Where(x => x.Note == ddlPromoText.SelectedValue).ToList();

                if (ddlPromoText.SelectedValue == "-1" && ddlValidity.SelectedValue != "-1")
                    gridlist = gridlist.Where(x => x.Name == ddlValidity.SelectedValue).ToList();

                grvProductValidUpTo.DataSource = gridlist;
                grvProductValidUpTo.DataBind();

                
            }
        }

    }
}