﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="EmailSettings.aspx.cs"
    Inherits="IR_Admin.EmailSettings" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">   
        $(function () {                  
            if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            $(".tabs a").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).parent("li").addClass("active");
            });
           
            if(<%=Tab%>=="2")
            {
                $(".list a").click(function () {
                    $("ul.list").tabs("div.panes > div");
                });
            }
            
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        img
        {
            vertical-align: middle;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Email Settings</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="EmailSettings.aspx" class="current">List</a></li>
            <li><a id="aNew" href="EmailSettings.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdSettings" runat="server" CellPadding="4" DataKeyNames="ID" CssClass="grid-head2"
                        OnRowCommand="grdSettings_RowCommand" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:BoundField DataField="SmtpHost" HeaderText="Smtp Host" />
                            <asp:BoundField DataField="SmtpUser" HeaderText="Smtp User" />
                            <asp:BoundField DataField="SmtpPort" HeaderText="Smtp Port" />
                            <asp:TemplateField HeaderText="Site's Name">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="30%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <!-- tab "panes" -->
                                <div class="inner-tabs-container">
                                    <div id="tabs-inner-1" class="grid-sec2" style="display: block; width: 700px;">
                                        <table class="tblMainSection">
                                            <tr>
                                                <td class="col">
                                                    Smtp Host
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtSmtpHost" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rvSmtpHost" runat="server" ControlToValidate="txtSmtpHost"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Smtp User
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtSmtpUser" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rvSmtpUser" runat="server" ControlToValidate="txtSmtpUser"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Smtp Password
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtSmtpPass" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rvSmtpPass" runat="server" ControlToValidate="txtSmtpPass"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Smtp Port
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtSmtpPort" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rvSmtpPort" runat="server" ControlToValidate="txtSmtpPort"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Email Address
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rvEmail" runat="server" ControlToValidate="txtEmail"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                    <asp:RegularExpressionValidator ID="regExpEmail" runat="server" CssClass="valdreq"
                                                        ErrorMessage="Invalid E-mail Address." ValidationGroup="CForm" ControlToValidate="txtEmail"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Enable SSL
                                                </td>
                                                <td class="col">
                                                    <asp:CheckBox ID="chkSSl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Is Active
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkActive" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="CForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="BtnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td class="col">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
