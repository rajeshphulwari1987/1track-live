﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using ResizeImage;

namespace IR_Admin
{
    public partial class RailPassSection3 : Page
    {
        readonly private ManageFrontWebsitePage _master = new ManageFrontWebsitePage();
        readonly Masters _oMaster = new Masters();
        public string Imagepath = string.Empty;

        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                BindGrid(_SiteID);

                BindSite();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetinfoForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = _oMaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void BindGrid(Guid _SiteID)
        {
            Tab = "1";
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;

            grdinfo.DataSource = _master.GetRailPassSec3(_SiteID);
            grdinfo.DataBind();
        }

        protected void grdinfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdinfo.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }

        protected void grdinfo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteRailPassSec3(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UploadFile();
                _master.AddRailPassSec3(new tblRailPassSec3
                {
                    ID = Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid(),
                    Name = txtTitle.Text.Trim(),
                    Description = txtDesc.InnerHtml,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    Imagepath = Imagepath.Replace("~/", ""),
                    SiteId = Guid.Parse(ddlSite.SelectedValue)
                });

                if (Request["id"] != null)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, Request["id"] == null ? "Record added successfully." : "Record updated successfully.");
                BindGrid(new Guid());
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("RailPassSection3.aspx");
        }

        public void GetinfoForEdit(Guid id)
        {
            var obj = _master.GetRailPassSec3ById(id);
            if (obj != null)
            {
                ddlSite.SelectedValue = obj.SiteId.ToString();
                txtTitle.Text = obj.Name;
                txtDesc.InnerHtml = obj.Description;
                hdnimage.Value = obj.Imagepath;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupImage.HasFile)
                {
                    if (fupImage.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupImage.FileName.Substring(fupImage.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    Imagepath = "~/Uploaded/RailPassSec3/";
                    Imagepath = Imagepath + oCom.CropImage(fupImage, Imagepath, 118, 208);
                }
                else
                {
                    Imagepath = hdnimage.Value;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CropImage(FileUpload fileUpload1, string location, float height, float width)
        {
            string strImage = fileUpload1.PostedFile.FileName;
            if (!string.IsNullOrEmpty(strImage))
            {
                try
                {
                    var myImage = System.Drawing.Image.FromStream(fileUpload1.PostedFile.InputStream);
                    float imgHeight = myImage.Height;
                    float imgWidth = myImage.Width;

                    string fileExt = Path.GetExtension(strImage);
                    strImage = Guid.NewGuid() + "_" + Path.GetFileNameWithoutExtension(strImage) + fileExt;
                    string strFilePathTemp = HttpContext.Current.Server.MapPath(location + strImage);
                    fileUpload1.PostedFile.SaveAs(strFilePathTemp);

                    var newHeight = imgHeight < height ? imgHeight : height;
                    var factor = imgHeight / newHeight;
                    var newWidth = imgWidth / factor;
                    newWidth = newWidth > width ? width : newWidth;

                    string strNewImagename = Guid.NewGuid() + fileExt;
                    ResizeImage(strFilePathTemp, HttpContext.Current.Server.MapPath(location) + strNewImagename, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
                    strImage = strNewImagename;

                    if (File.Exists(strFilePathTemp))
                        File.Delete(strFilePathTemp);
                }
                catch
                {
                    strImage = "";
                }
            }
            else
                strImage = "";
            return strImage;
        }

        public void ResizeImage(string sourceFile, string targetFile, int outputWidth, int outputHeight)
        {
            ImageResize.ResizeFix(sourceFile, targetFile, outputWidth, outputHeight);
        }
    }
}