﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="TicketProtection.aspx.cs" Inherits="IR_Admin.TicketProtection" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <style>
        .grid-sec2
        {
            width: 965px;
            padding:7px;
        }
        .heading
        {
            margin:20px 0 10px 0;
        }
    </style>
      <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/jscript">
        function pageLoad(sender, args) {
            $('#txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        $(document).ready(function () {
            $("#ddlSite").change(function () {
                $("#txtAmount").val('');
                $("#txtDesc").text('');
                $("#ddlCurrency").prop('selectedIndex', 0);
                $("#chkIsActive").removeAttr("checked");
                $('iframe').contents().find('body p').remove();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="heading">
            Ticket Protection</div>
    <div class="grid-sec2">
        <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
            <div class="cat-inner-alt">
                <table style="width: 870px;padding-left:5px;" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td>
                                Site:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSite" ClientIDMode="Static" runat="server" Style="width: 252px;height:25px;">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqsite" runat="server" ValidationGroup="tktprot" ForeColor="Red" InitialValue="0" ControlToValidate="ddlSite" ErrorMessage="Select Site."/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Amount:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAmount" ClientIDMode="Static" runat="server" Style="width: 250px;" MaxLength="5"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqamount" runat="server"  ValidationGroup="tktprot" ForeColor="Red" Display="Dynamic" ControlToValidate="txtAmount" ErrorMessage="Enter Amount."/>                               
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID ="txtAmount" ValidChars="0123456789."/>
                                 
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Currency:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCurrency" ClientIDMode="Static" runat="server" Style="width: 252px;height:25px;">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqcurrency" runat="server" ForeColor="Red"  InitialValue="0" ValidationGroup="tktprot"  ControlToValidate="ddlCurrency" ErrorMessage="Select Currency."/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Description:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesc" ClientIDMode="Static" TextMode="MultiLine" runat="server" Rows="15" Columns="10"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqdesc" runat="server" InitialValue="<p><br></p>" ValidationGroup="tktprot"  ForeColor="Red" ControlToValidate="txtDesc" ErrorMessage="Enter Description." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                &nbsp;IsActive:</td>
                            <td>
                                <asp:CheckBox ClientIDMode="Static" ID="chkIsActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" 
                                    ValidationGroup="tktprot" onclick="btnSubmit_Click" />
                                <asp:Button ID="btnCancle" CssClass="button" runat="server" Text="Cancle" 
                                    onclick="btnCancle_Click" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
