﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;
using System.Text.RegularExpressions;
using System.Linq;

namespace IR_Admin
{
    public partial class Manage_RailTicket : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindImages();
                BindPage();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindImages()
        {
            try
            {
                var resultbanner = _master.GetImageListByID(1); //banner images
                dtBanner1.DataSource = dtBanner3.DataSource = resultbanner;
                dtBanner1.DataBind();
                dtBanner3.DataBind();
                dtBanner2.DataSource = _master.GetImageListByID(11);
                dtBanner2.DataBind();
            }
            catch (Exception ex) { throw ex; }
        }

        public void BindPage()
        {
            try
            {
                imgInnerBanner.ImageUrl = SiteUrl + "images/train-ticket-inner-banner.jpg";
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    var result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        ContentText.InnerHtml = result.PageContent;
                        ContentValue2.InnerHtml = result.RailTicketInnerHeading;
                        ContentValue3.InnerHtml = result.RailTicketSubInnerLower;
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        protected void dtBanner3_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string url = null;
                    if (Session["url"] != null)
                        url = Session["url"].ToString();

                    if (url != string.Empty)
                    {
                        tblPage result = _master.GetPageDetailsByUrl(url);
                        if (result != null)
                        {
                            var bannerid = result.BannerIDs.Trim();
                            string[] arrId = bannerid.Split(',');
                            foreach (string id in arrId)
                            {
                                var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                                if (cbID != null)
                                {
                                    if (id == cbID.Value)
                                    {
                                        cbID.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        protected void dtBanner1_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string url = null;
                    if (Session["url"] != null)
                        url = Session["url"].ToString();

                    if (url != string.Empty)
                    {
                        tblPage result = _master.GetPageDetailsByUrl(url);
                        if (result != null)
                        {
                            var bannerid = result.LeftBannerImage.Trim();
                            string[] arrId = bannerid.Split(',');
                            foreach (string id in arrId)
                            {
                                var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                                if (cbID != null)
                                {
                                    if (id == cbID.Value)
                                    {
                                        cbID.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        protected void dtBanner2_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string url = null;
                    if (Session["url"] != null)
                        url = Session["url"].ToString();

                    if (url != string.Empty)
                    {
                        tblPage result = _master.GetPageDetailsByUrl(url);
                        if (result != null)
                        {
                            var bannerid = result.RightBanner.Trim();
                            string[] arrId = bannerid.Split(',');
                            foreach (string id in arrId)
                            {
                                var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                                if (cbID != null)
                                {
                                    if (id == cbID.Value)
                                    {
                                        cbID.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}