﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="PassSalesReport.aspx.cs" Inherits="IR_Admin.Reports.PassSalesReport_s" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }
        function showAdvancedSearch() {
            var img = $('#imgUpDown');
            var div = $('#divAdvancedSearch');
            div.slideDown(100, function () { img.attr('src', '../images/ar-up.png'); });
            SearchCriteriaChanged($('#hidAdvanceSearchType').val());
        }
        function resetAdvancedSearch() {
            try {
                $('#rdbAgentUserName').removeAttr("checked");
                $('#rdbProductName').removeAttr("checked");
                $('#rdbOrderNo').removeAttr("checked");
                $('#rdbProductCategory').removeAttr("checked");
                $('#divAgentSearch').hide();
                $('#divTextSearch').hide();
                clearAdvanceSearch();
            }
            catch (ex) {
                alert(ex);
            }
        }
        function clearAdvanceSearch() {
            $("#ddlSiteAgents").val('0');
            $('#txtTextSearch').val('');
        }
        $(function () {
            if ($('#hidAdvancedSearchToggle').val() != '') {
                showAdvancedSearch();
            }
            $('#divExpandCollapseAdvanceSearch').click(function () {
                try {

                    var div = $('#divAdvancedSearch');
                    var img = $('#imgUpDown');
                    if (!div.is(':visible')) {
                        div.slideDown(100, function () { img.attr('src', '../images/ar-up.png'); });
                        $('#hidAdvancedSearchToggle').val('show');
                    }
                    else {
                        div.slideUp(100);
                        img.attr('src', '../images/ar-down.png');
                        $('#hidAdvancedSearchToggle').val('');
                        $('#hidAdvanceSearchType').val('');
                        resetAdvancedSearch();
                    }
                }
                catch (ex) {
                    alert(ex);
                    return false;
                }
            });
        });

        function SearchCriteriaChanged(obj) {
            if (obj.toString() != $('#hidAdvanceSearchType').val().toString()) {
                clearAdvanceSearch();
            }
            switch (obj) {
                case 'AgentUserName':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('AgentUser Name : ');
                    $('#hidAdvanceSearchType').val('AgentUserName');
                    break;
                case 'ProductName':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Product Name : ');
                    $('#hidAdvanceSearchType').val('ProductName');
                    break;
                case 'orderno':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Order No : ');
                    $('#hidAdvanceSearchType').val('orderno');
                    break;
                case 'ProductCategory':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Product Category : ');
                    $('#hidAdvanceSearchType').val('ProductCategory');
                    break;
            }
        }
        function showHideSearchCriteria(obj) {
            switch (obj) {
                case 'agent':
                    $('#divAgentSearch').show();
                    $('#divTextSearch').hide();
                    break;
                case 'other':
                    $('#divTextSearch').show();
                    $('#divAgentSearch').hide();
                    break;
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hidAdvancedSearchToggle" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidAdvanceSearchType" runat="server" ClientIDMode="Static" />
    <h2>
        Pass Sale Report IR Internal</h2>
    <div class="full mr-tp1"> 
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div style="height: 30px;" id="div_RoleVisible" runat="server">
                            All STA site's record:
                            <asp:CheckBox runat="server" ID="chkStaSite" />
                            <div runat="server" id="divsyswide" visible="false" style="float:left;margin-right:10px;">
                                System wide report<asp:CheckBox runat="server" ID="chkSysWide" /></div>
                        </div>
                        <div class="pass-coloum-two" style="width: 962px; float: left; padding-bottom: 8px;">
                            <span style="margin-right: 13px;">Site:</span>
                            <asp:DropDownList runat="server" ID="ddlSites" Width="673px" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlSites_SelectedIndexChanged">
                            </asp:DropDownList>
                            <div style="float: right;">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" runat="server" Width="85" OnClick="btnCancel_Click"
                                    class="button" Text="Reset" />
                            </div>
                        </div>
                        <div style="float: right; margin-right: 2px;">
                            &nbsp;<asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                                data-export="export" Width="206px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static"
                                Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                            <asp:RegularExpressionValidator ID="regx" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtStartDate" ValidationGroup="rv" ForeColor="Red" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static"
                                Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                            <asp:RegularExpressionValidator ID="regx2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtLastDate" ForeColor="Red" ValidationGroup="rv" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <%--Advanced Search panel starts here..--%>
                        <div class="collapse-tab">
                            <div id="divExpandCollapseAdvanceSearch" style="width: 938px; float: left; cursor: pointer;">
                                <img src="../images/ar-down.png" id="imgUpDown" class="icon" />
                                <strong>Advanced Search </strong>
                            </div>
                            <div id="divAdvancedSearch" style="display: none;" class="innertb-content">
                                <div class="pass-coloum-two" style="width: 916px; float: left;">
                                    <asp:RadioButton ID="rdbOrderNo" runat="server" Text="1track Order No" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('orderno')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbProductCategory" runat="server" Text="ProductCategory" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('ProductCategory')" ClientIDMode="Static" />
                                    <asp:RadioButton ID="rdbProductName" runat="server" Text="ProductName" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('ProductName')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbAgentUserName" runat="server" Text="AgentUserName" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('AgentUserName')" ClientIDMode="Static" />&nbsp;&nbsp;
                                </div>
                                <div id="divTextSearch" class="pass-coloum-two" style="width: 962px; float: left;
                                    padding-bottom: 8px; padding-top: 10px; display: none">
                                    <label style="padding-left: 10px;">
                                        <div id="spnTextSearchLabel" style="width: 110px !important; float: left;">
                                        </div>
                                    </label>
                                    <asp:TextBox ID="txtTextSearch" runat="server" class="input" ClientIDMode="Static"
                                        MaxLength="50" Width="585px" />
                                </div>
                            </div>
                        </div>
                        <%--Advanced Search panel ends here..--%>
                    </div>
                    <div class="grid-head2" style="float: left; width: 100%; min-height: 180px; overflow-x: scroll;
                        overflow-y: hidden; height: auto">
                        <asp:GridView ID="grdpasssale" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            ForeColor="#333333" GridLines="None" Width="550%">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Agent username">
                                    <ItemTemplate>
                                        <%#Eval("AgentUserName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent office">
                                    <ItemTemplate>
                                        <%#Eval("AgentOffice")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Affiliate Code">
                                    <ItemTemplate>
                                        <%#Eval("AffiliateCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Approved">
                                    <ItemTemplate>
                                        <%# Eval("OrderCreated")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent reference">
                                    <ItemTemplate>
                                        <%#Eval("AgentReferenceNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="1track order number">
                                    <ItemTemplate>
                                        <%#Eval("OrderID")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sale or refund">
                                    <ItemTemplate>
                                        <%#Eval("SaleRefund")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Passholder name">
                                    <ItemTemplate>
                                        <%# Eval("PassholderName")%>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Product category">
                                    <ItemTemplate>
                                        <%# Eval("ProductCategory")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product name">
                                    <ItemTemplate>
                                        <%#Eval("ProductName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product validity">
                                    <ItemTemplate>
                                        <%#Eval("ProductValidity")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product class">
                                    <ItemTemplate>
                                        <%#Eval("ProductClass")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Passholder type">
                                    <ItemTemplate>
                                        <%#Eval("PassholderType")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales currency">
                                    <ItemTemplate>
                                        <%#Eval("SalesCurrency")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product GROSS sales amount in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("GrossSalesAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket protection in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("TicketProtection")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Admin fee in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("AdminFee")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                             
                                <asp:TemplateField HeaderText="Cxl/Extra Charge in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("AdminExtraCharge")%>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Booking fee in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("BookingFee")%>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Shipping amount in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("ShippingAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discount Code Used">
                                    <ItemTemplate>
                                        <%#Eval("DiscountCode")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discounted Amount In Sales Currency">
                                    <ItemTemplate>
                                        <%#Eval("OrderDiscountAmount")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order grand total in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("GrossSalesAmountAndTicketProtection")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Commission in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("Commition")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement Amount in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("SettlementAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="System cover charge in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("SystemCoverCharge")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fulfilment fee in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("FulfilmentFee")%>
                                    </ItemTemplate>
                                </asp:TemplateField>  
                                <asp:TemplateField HeaderText="Database currency">
                                    <ItemTemplate>
                                        <%#Eval("DATABASECURRENCY")%>
                                    </ItemTemplate>
                                </asp:TemplateField>  
                                <asp:TemplateField HeaderText="Database Amount">
                                    <ItemTemplate>
                                        <%#Eval("DATABASEAMOUNT")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                               
                                <asp:TemplateField HeaderText="Supplier">
                                    <ItemTemplate>
                                        <%#Eval("SUPPLIERNAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier Category">
                                    <ItemTemplate>
                                        <%#Eval("SUPPLIERCATEGORY")%>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Site">
                                    <ItemTemplate>
                                        <%#Eval("SITENAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>           
                                <asp:TemplateField HeaderText="Category Booking Fee">
                                    <ItemTemplate>
                                        <%#Eval("CPBOOKINGFEE")%>
                                    </ItemTemplate>                  
                                </asp:TemplateField> 
                                 <asp:TemplateField HeaderText="Special Offer">
                                    <ItemTemplate>
                                        <%#Eval("SPECIALOFFER")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                    <div class="clear">
                    </div>
                    <table width="100%" style="padding-top: 10px;">
                        <tr class="paging">
                            <td style="float: left; margin-right: 3px;">
                                <asp:LinkButton ID="lnkPrevious" Text='<< Previous' runat="server" OnClick="lnkPrevious_Click" />
                            </td>
                            <asp:Repeater ID="DLPageCountItem" runat="server">
                                <ItemTemplate>
                                    <td style="float: left; margin-right: 3px;">
                                        <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                            Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                    </td>
                                </ItemTemplate>
                            </asp:Repeater>
                            <td style="float: left; margin-right: 3px;">
                                <asp:LinkButton ID="lnkNext" Text='Next >>' runat="server" OnClick="lnkNext_Click" />
                            </td>
                            <td style="float: right;">
                                <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
