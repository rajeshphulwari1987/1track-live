﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.IO;
using ClosedXML.Excel;

namespace IR_Admin.Reports
{
    public partial class P2P : System.Web.UI.Page
    {
        public string currency = "$";
        private bool _isExport;
        private int _recordsPerPage = 50;
        private Int64 _totalRows = 0;
        readonly Masters _Master = new Masters();
        readonly private ManageOrder _MOrder = new ManageOrder();
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        public List<SpP2PReport> HoldP2Plist = new List<SpP2PReport>();
        public List<P2PSummary> HoldP2PSummarylist = new List<P2PSummary>();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);

            //change site drop down selected value..
            ddlSites.SelectedValue = _siteId.ToString();

            ResetSearch();

            PageloadEvent();
            BindGrid(1);
            BindPager(1);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divsyswide.Visible = chkSysWide.Checked = _Master.GetSystemReportByUserId(AdminuserInfo.UserID);
                BindSites();
                PageloadEvent();
                BindGrid(1);
                BindPager(1);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
                ShowHideRoleDiv();
            }
        }

        public void BindSites()
        {
            _siteId = Master.SiteID;
            var result = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID);
            if (result == null)
                return;
            if (result.Count() > 0)
            {
                ddlSites.DataSource = result;
                ddlSites.DataTextField = "DisplayName";
                ddlSites.DataValueField = "ID";
                ddlSites.DataBind();

                if (AdminuserInfo.SiteID == new Guid())
                    ddlSites.SelectedIndex = 0;
                else
                    ddlSites.SelectedValue = _siteId.ToString();
            }
        }

        public void PageloadEvent()
        {
            try
            {
                _siteId = Guid.Parse(ddlSites.SelectedValue);
                var agentList = new ManageOrder().GetAgentDetails(_siteId, Guid.Empty).OrderBy(x => x.Name);
                ddlSiteAgents.DataSource = agentList;
                ddlSiteAgents.DataTextField = "Name";
                ddlSiteAgents.DataValueField = "Id";
                ddlSiteAgents.DataBind();
                ddlSiteAgents.Items.Insert(0, new ListItem("-All Agents-", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindGrid(int pageNo)
        {
            try
            {
                CurrentPage = pageNo;
                if (_isExport)
                    btnExportToExcel.Visible = true;
                if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
                {
                    d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                #region Advanced search filter
                string Supplier = "";
                string OrderNoFilter = "";
                string AgentReferenceFilter = "";
                string Station = "";
                string AgentSelectedId = "0";
                if (rdbProductType.Checked)
                    Supplier = txtTextSearch.Text.Trim();
                else if (rdbOrderNo.Checked)
                    OrderNoFilter = txtTextSearch.Text.Trim();
                else if (rdbAgentReference.Checked)
                    AgentReferenceFilter = txtTextSearch.Text.Trim();
                else if (rdbProductCategory.Checked)
                    Station = txtTextSearch.Text.Trim();
                else if (rdbAgent.Checked)
                    AgentSelectedId = ddlSiteAgents.SelectedValue;
                #endregion

                string SiteId = ddlSites.SelectedValue;
                if (chkSysWide.Checked)
                    SiteId = "2";
                else if (chkStaSite.Checked)
                    SiteId = "1";
                var list = _MOrder.P2PReport(SiteId, d1, d2, pageNo, _recordsPerPage, AgentSelectedId, Supplier, OrderNoFilter, AgentReferenceFilter, Station, AdminuserInfo.UserID);
                if (list.Count() > 0)
                    _totalRows = Convert.ToInt64(list.Select(x => x.TotalRows).First());
                else
                    _totalRows = 0;
                GetP2PsummaryTable(_MOrder.P2PReport(SiteId, d1, d2, pageNo, 50000, AgentSelectedId, Supplier, OrderNoFilter, AgentReferenceFilter, Station, AdminuserInfo.UserID));
                grdP2P.DataSource = HoldP2Plist = list;
                grdP2P.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        public void GetP2PsummaryTable(List<SpP2PReport> list)
        {
            try
            {
                var Salelist = list.Where(x => x.TransactionType == "Sale").ToList();
                var Refundlist = list.Where(x => x.TransactionType == "Refund").ToList();
                CultureInfo usEnglish = new CultureInfo("en-US");
                DateTimeFormatInfo englishInfo = usEnglish.DateTimeFormat;
                string MonthName = englishInfo.MonthNames[Convert.ToInt32(d1.Month) - 1] + " (" + d1.Month + ")";
                var P2PSummaryList = new List<P2PSummary>();
                /*Sale calculation*/
                decimal ATotalOrder = Salelist.Select(x => x.OrderId).Distinct().Count();
                int A1stClass = Salelist.Where(x => x.Class.ToLower() == "1st class").AsEnumerable().Sum(x => Convert.ToInt32(x.Adult) + Convert.ToInt32(x.Children) + Convert.ToInt32(x.Youth) + Convert.ToInt32(x.Senior));
                int A2ndClass = Salelist.Where(x => x.Class.ToLower() == "2nd class").AsEnumerable().Sum(x => Convert.ToInt32(x.Adult) + Convert.ToInt32(x.Children) + Convert.ToInt32(x.Youth) + Convert.ToInt32(x.Senior));
                int AOtherClass = Salelist.Where(x => x.Class.ToLower() != "2nd class" && x.Class.ToLower() != "1st class").AsEnumerable().Sum(x => Convert.ToInt32(x.Adult) + Convert.ToInt32(x.Children) + Convert.ToInt32(x.Youth) + Convert.ToInt32(x.Senior));
                int AAdult = Salelist.AsEnumerable().Sum(x => Convert.ToInt32(x.Adult));
                int AChild = Salelist.AsEnumerable().Sum(x => Convert.ToInt32(x.Children));
                int AYouth = Salelist.AsEnumerable().Sum(x => Convert.ToInt32(x.Youth));
                int ASenior = Salelist.AsEnumerable().Sum(x => Convert.ToInt32(x.Senior));

                P2PSummaryList.Add(new P2PSummary { Row = 1, Title = "No of sales orders (unique order numbers)", Month = MonthName, Total = ATotalOrder, Class1st = 0, Class2nd = 0, ClassOther = 0, Adult = 0, Children = 0, Youth = 0, Senior = 0 });
                decimal BTotalOrder = Salelist.Count();
                P2PSummaryList.Add(new P2PSummary { Row = 2, Title = "No of sales legs (Separate lines/journeys)", Month = MonthName, Total = (AAdult + AChild + AYouth + ASenior), Class1st = A1stClass, Class2nd = A2ndClass, ClassOther = AOtherClass, Adult = AAdult, Children = AChild, Youth = AYouth, Senior = ASenior });

                /*Refund calculation*/
                decimal CTotalOrder = Refundlist.Select(x => x.OrderId).Distinct().Count();
                int C1stClass = Refundlist.Where(x => x.Class.ToLower() == "1st class").AsEnumerable().Sum(x => Convert.ToInt32(x.Adult) + Convert.ToInt32(x.Children) + Convert.ToInt32(x.Youth) + Convert.ToInt32(x.Senior));
                int C2ndClass = Refundlist.Where(x => x.Class.ToLower() == "2nd class").AsEnumerable().Sum(x => Convert.ToInt32(x.Adult) + Convert.ToInt32(x.Children) + Convert.ToInt32(x.Youth) + Convert.ToInt32(x.Senior));
                int COtherClass = Refundlist.Where(x => x.Class.ToLower() != "2nd class" && x.Class.ToLower() != "1st class").AsEnumerable().Sum(x => Convert.ToInt32(x.Adult) + Convert.ToInt32(x.Children) + Convert.ToInt32(x.Youth) + Convert.ToInt32(x.Senior));
                int CAdult = Refundlist.AsEnumerable().Sum(x => Convert.ToInt32(x.Adult));
                int CChild = Refundlist.AsEnumerable().Sum(x => Convert.ToInt32(x.Children));
                int CYouth = Refundlist.AsEnumerable().Sum(x => Convert.ToInt32(x.Youth));
                int CSenior = Refundlist.AsEnumerable().Sum(x => Convert.ToInt32(x.Senior));

                P2PSummaryList.Add(new P2PSummary { Row = 3, Title = "No of refunded orders (unique order numbers)", Month = MonthName, Total = CTotalOrder, Class1st = 0, Class2nd = 0, ClassOther = 0, Adult = 0, Children = 0, Youth = 0, Senior = 0 });
                decimal DTotalOrder = list.Where(x => x.TransactionType == "Refund").Count();
                P2PSummaryList.Add(new P2PSummary { Row = 4, Title = "No of refunded legs (separate lines/journeys)", Month = MonthName, Total = (CAdult + CChild + CYouth + CSenior), Class1st = C1stClass, Class2nd = C2ndClass, ClassOther = COtherClass, Adult = CAdult, Children = CChild, Youth = CYouth, Senior = CSenior });

                /*Total calculation*/
                decimal ETotalSale = Salelist.Sum(x => x.SalesAmount.Value);
                P2PSummaryList.Add(new P2PSummary { Row = 5, Title = "Total Sales £", Month = MonthName, Total = ETotalSale, Class1st = 0, Class2nd = 0, ClassOther = 0, Adult = 0, Children = 0, Youth = 0, Senior = 0 });

                HoldP2PSummarylist = P2PSummaryList;
                grdP2PSummary.DataSource = P2PSummaryList;
                grdP2PSummary.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _isExport = true;
            BindGrid(1);
            BindPager(1);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("P2P.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            _recordsPerPage = 50000;
            BindGrid(1);
            int countrow = 1;
            MemoryStream MyMemoryStream = new MemoryStream();
            XLWorkbook wb = new XLWorkbook();

            wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.LeftBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.TopBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));

            var ws1 = wb.Worksheets.Add("P2P Report");
            ws1.TabColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#f9c"));
            ws1.RowHeight = 19;
            ws1.Style.Font.FontSize = 9;
            ws1.Style.Alignment.WrapText = true;
            ws1.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;

            #region P2P Report
            if (HoldP2Plist != null && HoldP2Plist.Count > 1)
            {
                var OrderId = ws1.Cell("A1");
                OrderId = OrderId.SetValue("1track order number");
                var Affiliate = ws1.Cell("B1");
                Affiliate = Affiliate.SetValue("Affiliate");
                var Reference = ws1.Cell("C1");
                Reference = Reference.SetValue("Agent reference");
                var TransactionType = ws1.Cell("D1");
                TransactionType = TransactionType.SetValue("Transaction type");
                var BookedDate = ws1.Cell("E1");
                BookedDate = BookedDate.SetValue("Payment date");
                var BookedTime = ws1.Cell("F1");
                BookedTime = BookedTime.SetValue("Payment time");
                var RefundDate = ws1.Cell("G1");
                RefundDate = RefundDate.SetValue("Refund Date	");
                var BookedOn = ws1.Cell("H1");
                BookedOn = BookedOn.SetValue("Website booked on	");
                var Office = ws1.Cell("I1");
                Office = Office.SetValue("Office	");
                var Agent = ws1.Cell("J1");
                Agent = Agent.SetValue("Agent	");
                var Supplier = ws1.Cell("K1");
                Supplier = Supplier.SetValue("Supplier");
                var DnrOrPnr = ws1.Cell("L1");
                DnrOrPnr = DnrOrPnr.SetValue("DNR/PNR reference");
                var LeadPassenger = ws1.Cell("M1");
                LeadPassenger = LeadPassenger.SetValue("Lead passenger name	");
                var From = ws1.Cell("N1");
                From = From.SetValue("From	");
                var To = ws1.Cell("O1");
                To = To.SetValue("To");
                var DepartureDate = ws1.Cell("P1");
                DepartureDate = DepartureDate.SetValue("Date of departure	");
                var DepartureTime = ws1.Cell("Q1");
                DepartureTime = DepartureTime.SetValue("Time of departure	");
                var ArrivalDate = ws1.Cell("R1");
                ArrivalDate = ArrivalDate.SetValue("Date of arrival");
                var ArrivalTime = ws1.Cell("S1");
                ArrivalTime = ArrivalTime.SetValue("Time of arrival	");
                var Adult = ws1.Cell("T1");
                Adult = Adult.SetValue("Number of adults");
                var Youth = ws1.Cell("U1");
                Youth = Youth.SetValue("Number of youths");
                var Children = ws1.Cell("V1");
                Children = Children.SetValue("Number of children");
                var Senior = ws1.Cell("W1");
                Senior = Senior.SetValue("Number of seniors");
                var Class = ws1.Cell("X1");
                Class = Class.SetValue("Class of travel	");
                var TicketType = ws1.Cell("Y1");
                TicketType = TicketType.SetValue("Ticket type");
                var DeliveryMethod = ws1.Cell("Z1");
                DeliveryMethod = DeliveryMethod.SetValue("Delivery method	");
                var SalesCurrency = ws1.Cell("AA1");
                SalesCurrency = SalesCurrency.SetValue("Sales currency");
                var SalesAmount = ws1.Cell("AB1");
                SalesAmount = SalesAmount.SetValue("Sales amount in sales currency");
                var CommissionFee = ws1.Cell("AC1");
                CommissionFee = CommissionFee.SetValue("Commission amount in sales currency	");
                var NetAmountInSalesCurrency = ws1.Cell("AD1");
                NetAmountInSalesCurrency = NetAmountInSalesCurrency.SetValue("NET amount in sales currency");
                var SupplierCurrency = ws1.Cell("AE1");
                SupplierCurrency = SupplierCurrency.SetValue("Supplier currency	");
                var SupplierAmountInSuplierRCurrency = ws1.Cell("AF1");
                SupplierAmountInSuplierRCurrency = SupplierAmountInSuplierRCurrency.SetValue("Supplier amount in supplier currency	");
                var TicketProtectionFee = ws1.Cell("AG1");
                TicketProtectionFee = TicketProtectionFee.SetValue("Ticket protection in sales currency	");
                var BookingFee = ws1.Cell("AH1");
                BookingFee = BookingFee.SetValue("Booking fee in sales currency");
                var AdminFee = ws1.Cell("AI1");
                AdminFee = AdminFee.SetValue("Admin fee in sales currency	");
                var AdminExtraCharge = ws1.Cell("AJ1");
                AdminExtraCharge = AdminExtraCharge.SetValue("Cxl/Extra Charge in sales currency");
                var ShippingFee = ws1.Cell("AK1");
                ShippingFee = ShippingFee.SetValue("Shipping in sales currency	");
                var DiscountCode = ws1.Cell("AL1");
                DiscountCode = DiscountCode.SetValue("Discount Code Used");
                var OrderDiscountAmount = ws1.Cell("AM1");
                OrderDiscountAmount = OrderDiscountAmount.SetValue("Discounted Amount In Sales Currency	");
                var GrandTotal = ws1.Cell("AN1");
                GrandTotal = GrandTotal.SetValue("Order grand total in sales currency");

                ws1.Range("A1:AN1").Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#408ed2"));
                ws1.Range("A1:AN1").Row(1).Style.Font.FontColor = XLColor.White;
                ws1.Row(1).Height = 28;
                ws1.Range("A1:AN1").Row(1).Style.Font.SetBold(true);
                ws1.Column(3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                
                foreach (var x in HoldP2Plist)
                {
                    decimal value = 0;
                    OrderId = OrderId.CellBelow().SetValue(x.OrderId);
                    Affiliate = Affiliate.CellBelow().SetValue(x.Affiliate);
                    if (Decimal.TryParse(x.Reference, out value))
                        Reference = Reference.CellBelow().SetValue(Convert.ToDecimal(x.Reference));
                    else
                        Reference = Reference.CellBelow().SetValue(string.IsNullOrEmpty(x.Reference) ? "-" : x.Reference);
                    TransactionType = TransactionType.CellBelow().SetValue(x.TransactionType);
                    BookedDate = BookedDate.CellBelow().SetValue(x.BookedDate);
                    BookedTime = BookedTime.CellBelow().SetValue(x.BookedTime);
                    RefundDate = RefundDate.CellBelow().SetValue(x.RefundDate);
                    BookedOn = BookedOn.CellBelow().SetValue(x.BookedOn);
                    Office = Office.CellBelow().SetValue(x.Office);
                    Agent = Agent.CellBelow().SetValue(x.Agent);
                    Supplier = Supplier.CellBelow().SetValue(x.Supplier);
                    DnrOrPnr = DnrOrPnr.CellBelow().SetValue(x.DnrOrPnr);
                    LeadPassenger = LeadPassenger.CellBelow().SetValue(x.LeadPassenger);
                    From = From.CellBelow().SetValue(x.From);
                    To = To.CellBelow().SetValue(x.To);
                    DepartureDate = DepartureDate.CellBelow().SetValue(x.DepartureDate);
                    DepartureTime = DepartureTime.CellBelow().SetValue(x.DepartureTime);
                    ArrivalDate = ArrivalDate.CellBelow().SetValue(x.ArrivalDate);
                    ArrivalTime = ArrivalTime.CellBelow().SetValue(x.ArrivalTime);
                    Adult = Adult.CellBelow().SetValue(Convert.ToDecimal(x.Adult));
                    Youth = Youth.CellBelow().SetValue(Convert.ToDecimal(x.Youth));
                    Children = Children.CellBelow().SetValue(Convert.ToDecimal(x.Children));
                    Senior = Senior.CellBelow().SetValue(Convert.ToDecimal(x.Senior));
                    Class = Class.CellBelow().SetValue(x.Class);
                    TicketType = TicketType.CellBelow().SetValue(x.Tickettype);
                    DeliveryMethod = DeliveryMethod.CellBelow().SetValue(x.DeliveryMethod);
                    SalesCurrency = SalesCurrency.CellBelow().SetValue(x.SalesCurrency);
                    SalesAmount = SalesAmount.CellBelow().SetValue(x.SalesAmount);
                    CommissionFee = CommissionFee.CellBelow().SetValue(x.CommissionFee);
                    NetAmountInSalesCurrency = NetAmountInSalesCurrency.CellBelow().SetValue(x.NetAmountInSalesCurrency);
                    SupplierCurrency = SupplierCurrency.CellBelow().SetValue(x.SupplierCurrency);
                    SupplierAmountInSuplierRCurrency = SupplierAmountInSuplierRCurrency.CellBelow().SetValue(x.SupplierAmountInSuplierRCurrency);
                    TicketProtectionFee = TicketProtectionFee.CellBelow().SetValue(x.TicketProtectionFee);
                    BookingFee = BookingFee.CellBelow().SetValue(x.BookingFee);
                    AdminFee = AdminFee.CellBelow().SetValue(x.AdminFee);
                    AdminExtraCharge = AdminExtraCharge.CellBelow().SetValue(x.AdminExtraCharge);
                    ShippingFee = ShippingFee.CellBelow().SetValue(x.ShippingFee);
                    DiscountCode = DiscountCode.CellBelow().SetValue(x.DiscountCode);
                    OrderDiscountAmount = OrderDiscountAmount.CellBelow().SetValue(Convert.ToDecimal(x.OrderdIsCountAmount));
                    GrandTotal = GrandTotal.CellBelow().SetValue(x.GrandTotal);
                    countrow++;
                    if (countrow % 2 == 1)
                        ws1.Range("A" + countrow + ":AN" + countrow).Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d7e7f6"));
                    else
                        ws1.Range("A" + countrow + ":AN" + countrow).Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#b7d2e9"));
                }
                ws1.Columns(1, 40).Width = 25;
                ws1.Column(9).Width = 50;
                ws1.Columns(14, 15).Width = 40;
            }
            #endregion

            var ws2 = wb.Worksheets.Add("P2P Summary Report");
            ws2.TabColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#cfc"));
            ws2.RowHeight = 19;
            ws2.Style.Font.FontSize = 9;
            ws2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
            countrow = 1;

            #region  P2P Summary Report
            if (HoldP2PSummarylist != null && HoldP2PSummarylist.Count > 0)
            {
                var Title = ws2.Cell("A1");
                Title = Title.SetValue("");
                var Month = ws2.Cell("B1");
                Month = Month.SetValue("Month");
                var Total = ws2.Cell("C1");
                Total = Total.SetValue("Total");
                var Class1st = ws2.Cell("D1");
                Class1st = Class1st.SetValue("1st Class");
                var Class2nd = ws2.Cell("E1");
                Class2nd = Class2nd.SetValue("2nd Class");
                var OtherClass = ws2.Cell("F1");
                OtherClass = OtherClass.SetValue("Other Class");
                var Adult = ws2.Cell("G1");
                Adult = Adult.SetValue("Adult");
                var Child = ws2.Cell("H1");
                Child = Child.SetValue("Child");
                var Senior = ws2.Cell("I1");
                Senior = Senior.SetValue("Senior");
                var Youth = ws2.Cell("J1");
                Youth = Youth.SetValue("Youth");

                ws2.Range("A1:J1").Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#408ed2"));
                ws2.Range("A1:J1").Row(1).Style.Font.FontColor = XLColor.White;
                ws2.Row(1).Height = 28;
                ws2.Range("A1:J1").Row(1).Style.Font.SetBold(true);

                foreach (var x in HoldP2PSummarylist)
                {
                    Title = Title.CellBelow().SetValue(x.Title);
                    Month = Month.CellBelow().SetValue(x.Month);
                    Total = Total.CellBelow().SetValue(x.Total);
                    if (x.Row == 2 || x.Row == 4)
                    {
                        Class1st = Class1st.CellBelow().SetValue(x.Class1st);
                        Class2nd = Class2nd.CellBelow().SetValue(x.Class2nd);
                        OtherClass = OtherClass.CellBelow().SetValue(x.ClassOther);
                        Adult = Adult.CellBelow().SetValue(x.Adult);
                        Child = Child.CellBelow().SetValue(x.Children);
                        Senior = Senior.CellBelow().SetValue(x.Senior);
                        Youth = Youth.CellBelow().SetValue(x.Youth);
                    }
                    else
                    {
                        Class1st = Class1st.CellBelow().SetValue(x.Class1st > 1 ? (dynamic)x.Class1st : "");
                        Class2nd = Class2nd.CellBelow().SetValue(x.Class2nd > 1 ? (dynamic)x.Class2nd : "");
                        OtherClass = OtherClass.CellBelow().SetValue(x.ClassOther > 1 ? (dynamic)x.ClassOther : "");
                        Adult = Adult.CellBelow().SetValue(x.Adult > 1 ? (dynamic)x.Adult : "");
                        Child = Child.CellBelow().SetValue(x.Children > 1 ? (dynamic)x.Children : "");
                        Senior = Senior.CellBelow().SetValue(x.Senior > 1 ? (dynamic)x.Senior : "");
                        Youth = Youth.CellBelow().SetValue(x.Youth > 1 ? (dynamic)x.Youth : "");
                    }
                    countrow++;
                    if (countrow % 2 == 1)
                        ws2.Range("A" + countrow + ":j" + countrow).Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d7e7f6"));
                    else
                        ws2.Range("A" + countrow + ":J" + countrow).Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#b7d2e9"));
                }
                ws2.Column(1).Width = 50;
                ws2.Columns(2, 10).Width = 12;
            }
            #endregion

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "P2P Report"));
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }

        #region Paging
        public void BindPager(int selectedPageNo)
        {
            try
            {
                Int64 newpagecount = _totalRows / _recordsPerPage + ((_totalRows % _recordsPerPage) > 0 ? 1 : 0);
                var oPageList = new List<ClsPageCount>();
                lnkPrevious.Visible = true;
                lnkNext.Visible = true;
                litTotalPages.Visible = true;
                Int64 StartRange = selectedPageNo - 5;
                if (StartRange < 2)
                    StartRange = 1;
                Int64 EndRange = StartRange + 9;
                if (EndRange > newpagecount)
                {
                    EndRange = newpagecount;
                    StartRange = EndRange - 9;
                }
                if (StartRange < 2)
                    StartRange = 1;
                if (selectedPageNo == 1)
                    lnkPrevious.Visible = false;
                if (selectedPageNo == newpagecount)
                    lnkNext.Visible = false;
                if (newpagecount < 1)
                {
                    lnkPrevious.Visible = false;
                    lnkNext.Visible = false;
                    litTotalPages.Visible = false;
                    litTotalPages.Text = "";
                }
                else
                    litTotalPages.Text = "Showing page " + CurrentPage + " of " + newpagecount;
                for (Int64 i = StartRange; i <= EndRange; i++)
                {
                    var oPage = new ClsPageCount { PageCount = i.ToString() };
                    oPageList.Add(oPage);
                }
                DLPageCountItem.DataSource = oPageList;
                DLPageCountItem.DataBind();
                foreach (RepeaterItem item1 in DLPageCountItem.Items)
                {
                    var lblPage = (LinkButton)item1.FindControl("lnkPage");
                    if (lblPage != null)
                        lblPage.Attributes.Add("class", "activepaging");
                    break;
                }
                foreach (RepeaterItem item1 in DLPageCountItem.Items)
                {
                    var lblPage = (LinkButton)item1.FindControl("lnkPage");
                    if (lblPage.Text.Trim() == (selectedPageNo).ToString(CultureInfo.InvariantCulture))
                        lblPage.Attributes.Add("class", "activepaging");
                    else
                        lblPage.Attributes.Remove("class");
                }
            }
            catch (Exception ex) { }
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument);
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex).ToString(CultureInfo.InvariantCulture))
                    lblPage.Attributes.Add("class", "activepaging");
                else
                    lblPage.Attributes.Remove("class");
            }
            CurrentPage = pageIndex;
            BindGrid(CurrentPage);
            BindPager(CurrentPage);
        }
        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage - 1;
            BindGrid(CurrentPage);
            BindPager(CurrentPage);
        }
        protected void lnkNext_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage + 1;
            BindGrid(CurrentPage);
            BindPager(CurrentPage);
        }

        private int CurrentPage
        {
            get
            {
                return ((ViewState["PageIndex"] == null || ViewState["PageIndex"].ToString() == "") ? 1 : int.Parse(ViewState["PageIndex"].ToString()));
            }
            set
            {
                ViewState["PageIndex"] = value;
            }
        }
        #endregion

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetSearch();
            PageloadEvent();
            BindGrid(1);
            BindPager(1);
        }

        private void ResetSearch()
        {
            rdbAgent.Checked = false;
            rdbProductType.Checked = false;
            rdbOrderNo.Checked = false;
            rdbAgentReference.Checked = false;
            rdbProductCategory.Checked = false;
            txtTextSearch.Text = "";
            txtStartDate.Text = "";
            txtLastDate.Text = "";
            hidAdvancedSearchToggle.Value = "";
            hidAdvanceSearchType.Value = "";
        }

        protected void ddlOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageloadEvent();
        }

        private void ShowHideRoleDiv()
        {
            var data = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                if (data.IsStaRecordsAllow)
                    div_RoleVisible.Visible = true;
                else
                    div_RoleVisible.Visible = false;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}

public class P2PSummary
{
    public int Row { get; set; }
    public string Title { get; set; }
    public string Month { get; set; }
    public decimal Total { get; set; }
    public int Class1st { get; set; }
    public int Class2nd { get; set; }
    public int ClassOther { get; set; }
    public int Adult { get; set; }
    public int Children { get; set; }
    public int Senior { get; set; }
    public int Youth { get; set; }
}