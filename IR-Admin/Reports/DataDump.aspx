﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="DataDump.aspx.cs" Inherits="IR_Admin.Reports.DataDump" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .BindGridScrolle {
            float: left;
            width: 100%;
            height: 100%;
            position: relative;
            overflow-x: scroll;
            border-radius: 7px;
        }
    </style>
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Data Dump</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px; padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div class="pass-coloum-two" style="width: 100%; float: left; padding-bottom: 8px;">
                            <div style="float: left;" id="div_RoleVisible" runat="server">
                                All STA site’s record:
                                <asp:CheckBox ID="chkStaOffice" runat="server" />
                            </div>
                            <div class="pass-coloum-two" style="float: right;">
                                Go to page no.:
                                <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged"
                                    Width="90px" />
                            </div>
                        </div>
                        <div class="pass-coloum-two" style="width: 757px; float: left; padding-bottom: 8px;">
                            Office:<asp:DropDownList runat="server" ID="ddlOffice" Width="90%" Style="margin-left: 27px;" />
                        </div>
                        <div style="float: right">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Reset" Width="89px"
                                CausesValidation="false" OnClick="btnReset_Click" />
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-top: 5px;">
                            <div class="pass-coloum-two" style="width: 254px; float: left;">
                                StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static"
                                Width="150" />
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                    PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px 0 0;" />
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                    Enabled="false" ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                            </div>
                            <div class="pass-coloum-two" style="width: 249px; float: left;">
                                LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static"
                                Width="150" />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                                <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                    BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px 0 0;" />
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                    Enabled="false" ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                            </div>
                            <div class="pass-coloum-two" style="width: 250px; float: left;">
                                Order Status:
                            <asp:DropDownList ID="ddlOrderStatus" runat="server" ValidationGroup="rv" Width="160px"
                                ToolTip="Order Status">
                                <asp:ListItem Value="0" Selected="True">Paid Orders</asp:ListItem>
                                <asp:ListItem Value="1">UnPaid Orders</asp:ListItem>
                            </asp:DropDownList>
                            </div>
                            <div style="float: right">
                                <asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                                    data-export="export" Visible="false" Width="206px" nClick="btnExportToExcel_Click"
                                    OnClick="btnExportToExcel_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="BindGridScrolle">
                        <asp:GridView ID="grdDump" runat="server" AutoGenerateColumns="False" PageSize="50"
                            CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="500%"
                            AllowPaging="True" OnPageIndexChanging="grdDump_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" VerticalAlign="Top" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" VerticalAlign="Top" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <ItemTemplate>
                                        <%# Eval("Site")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product category">
                                    <ItemTemplate>
                                        <%#Eval("ProductCategory")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sale Currency">
                                    <ItemTemplate>
                                        <%#Eval("SaleCurrency")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sale Amount">
                                    <ItemTemplate>
                                        <%#Eval("SaleAmount", "{0:0.00}")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Price In Budget USD">
                                    <ItemTemplate>
                                        <%#Eval("SalesPriceInBudgetUSD","{0:0.00}")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement  Currency">
                                    <ItemTemplate>
                                        <%#Eval("SettlementCurrency")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement  Amount">
                                    <ItemTemplate>
                                        <%#Eval("SettlementAmount","{0:0.00}")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement Amount in Budget USD">
                                    <ItemTemplate>
                                        <%#Eval("SettlementAmountInBudgetUSD","{0:0.00}")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Number Of Passengers">
                                    <ItemTemplate>
                                        <%#Eval("NoOfPax")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Travel Date Month">
                                    <ItemTemplate>
                                        <%#Eval("TravelDateMonth")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Travel Date Year">
                                    <ItemTemplate>
                                        <%#Eval("TravelDateYear")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="USD Gross Sales YOY Change">
                                    <ItemTemplate>
                                        <%#Eval("USDGrossSalesYOYChange")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="USD Nett Cost YOY Change">
                                    <ItemTemplate>
                                        <%#Eval("USDNettCostYOYChange")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No Pax YOY Change">
                                    <ItemTemplate>
                                        <%#Eval("NoPaxYOYChange")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Type">
                                    <ItemTemplate>
                                        <%#Eval("ProductType")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Name">
                                    <ItemTemplate>
                                        <%#Eval("ProductName")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booked Date Month">
                                    <ItemTemplate>
                                        <%# Eval("BookedDateMonth")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booked Date Year">
                                    <ItemTemplate>
                                        <%# Eval("BookedDateYear")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent Name">
                                    <ItemTemplate>
                                        <%# Eval("AgentName")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Branch">
                                    <ItemTemplate>
                                        <%# Eval("Branch")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Class">
                                    <ItemTemplate>
                                        <%#Eval("Class")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Type">
                                    <ItemTemplate>
                                        <%#Eval("TicketType")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order No.">
                                    <ItemTemplate>
                                        <%# Eval("OrderNo")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Reference">
                                    <ItemTemplate>
                                        <%#Eval("OrderReference")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refund Date">
                                    <ItemTemplate>
                                        <%#Eval("RefundDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Passenger Name (Lead Passenger)">
                                    <ItemTemplate>
                                        <%#Eval("LeadPassenger")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Status">
                                    <ItemTemplate>
                                        <%#Eval("OrderStatus")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booked Date">
                                    <ItemTemplate>
                                        <%#Eval("BookedDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Departure Date">
                                    <ItemTemplate>
                                        <%#Eval("DepartureDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
