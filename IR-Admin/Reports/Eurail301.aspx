﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Eurail301.aspx.cs" Inherits="IR_Admin.Reports.Eurail301" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="workingpage" id="report">
        <div class="pageframe">
            <h2>
                Eurail 301 Accounting Report</h2>
            <br />
            
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
            <table width="100%" cellspacing="3" cellpadding="3" class="searchDiv">
                <tr>
                    <td colspan="4">
                        <div style="height: 30px;" id="div_RoleVisible" runat="server">
                            All STA site's record:
                            <asp:CheckBox runat="server" ID="chkStaSite" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        Month:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server" />
                    </td>
                    <td>
                        Year:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server" />
                    </td>
                    <td>
                        <asp:Button CssClass="button" Text="Export Txt" ID="btnbtnTxt" runat="server" Width="120"
                            data-export="export" OnClick="btnTxt_Click" />
                        &nbsp;<asp:Button CssClass="button" Text="Export Pdf" ID="btnPdf" runat="server"
                            Width="120" data-export="export" OnClick="btnPdf_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
