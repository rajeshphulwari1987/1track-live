﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Web;
using System.IO;
using System.Linq;
//using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Business;

namespace IR_Admin.Reports.InterRail
{
    public partial class Eurail301 : System.Web.UI.Page
    {
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;
        readonly private db_1TrackEntities db = new db_1TrackEntities();

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDropDown();
                ShowHideRoleDiv();
            }
        }

        public void FillDropDown()
        {
            for (int i = 1; i < 13; i++)
            {
                ddlMonth.Items.Add(new ListItem { Value = i.ToString(), Text = "(" + i + ") " + System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
            }

            for (int i = 2018; i < 2018 + 10; i++)
            {
                ddlYear.Items.Add(new ListItem { Value = i.ToString(), Text = i.ToString() });
            }
        }

        protected void btnTxt_Click(object sender, EventArgs e)
        {
            try{
            string month = ("0" + ddlMonth.SelectedValue);
            month = month.Substring(month.Length - 2, 2);
            string year = ddlYear.SelectedValue;

            string thedate = month + "/01" + "/" + year + " 00:00:00";
            DateTime monthstart = DateTime.Parse(thedate);
            DateTime monthend = monthstart.AddMonths(1);

            Response.Buffer = true;
            string connStr = ConfigurationManager.ConnectionStrings["db_Entities"].ConnectionString;
            SqlConnection conn = new SqlConnection(connStr);
            SqlCommand cmd;

            conn.Open();
            cmd = new SqlCommand("INTERRAIL_301_REPORT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.Date)).Value = monthstart;
            cmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.Date)).Value = monthend;
            cmd.Parameters.Add(new SqlParameter("@IsSta", SqlDbType.Bit)).Value = chkStaSite.Checked;
            cmd.Parameters.Add(new SqlParameter("@AGENTNAME", SqlDbType.NVarChar)).Value ="";

            StringBuilder sb = new StringBuilder();
            SqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                do
                {
                    while (rdr.Read())
                    {
                        for (var cols = 0; cols < rdr.FieldCount; cols++)
                        {
                            sb.Append(rdr[cols].ToString());
                        }
                        sb.Append(System.Environment.NewLine);
                    }
                }
                while (rdr.NextResult());
            }
            if (!rdr.IsClosed) { rdr.Close(); }
            conn.Close();
            Response.Write(sb.ToString());
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=InterRail 301.txt");
            Response.End();
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void btnPdf_Click(object sender, EventArgs e)
        {
            try{
            string month = ("0" + ddlMonth.SelectedValue);
            month = month.Substring(month.Length - 2, 2);
            string year = ddlYear.SelectedValue;

            string thedate = month + "/01" + "/" + year + " 00:00:00";
            DateTime monthstart = DateTime.Parse(thedate);
            DateTime monthend = monthstart.AddMonths(1);

            Response.Buffer = true;
            string connStr = ConfigurationManager.ConnectionStrings["db_Entities"].ConnectionString;
            SqlConnection conn = new SqlConnection(connStr);
            SqlCommand cmd;

            conn.Open();
            cmd = new SqlCommand("INTERRAIL_301_REPORT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = monthstart;
            cmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = monthend;
            cmd.Parameters.Add(new SqlParameter("@IsSta", SqlDbType.Bit)).Value = chkStaSite.Checked;
            cmd.Parameters.Add(new SqlParameter("@AGENTNAME", SqlDbType.NVarChar)).Value = "";

            StringBuilder sb = new StringBuilder();
            SqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                do
                {
                    while (rdr.Read())
                    {
                        for (var cols = 0; cols < rdr.FieldCount; cols++)
                        {
                            sb.Append(rdr[cols].ToString());
                        }
                        sb.Append(System.Environment.NewLine);
                    }
                }
                while (rdr.NextResult());
            }
            if (!rdr.IsClosed) { rdr.Close(); }
            conn.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=InterRail 301.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringReader sr = new StringReader(sb.ToString());
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 2f, 2f, 2f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            //htmlparser.Parse(sr);

            iTextSharp.text.Chunk c = new iTextSharp.text.Chunk(sb.ToString(), iTextSharp.text.FontFactory.GetFont("ARIAL", 8, 0, color: iTextSharp.text.Color.BLACK));
            iTextSharp.text.Paragraph p = new iTextSharp.text.Paragraph();
            p.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            p.Add(c);
            pdfDoc.Add(p);

            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End(); }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        private void ShowHideRoleDiv()
        {
            var data = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                if (data.IsStaRecordsAllow)
                    div_RoleVisible.Visible = true;
                else
                    div_RoleVisible.Visible = false;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}