﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using ClosedXML.Excel;

namespace IR_Admin.Reports.InterRail
{
    public partial class InterRailSeparatelyReportSales : Page
    {
        public string currency = "£";
        public string MonthName = "";
        private bool hold = false;
        readonly InterRailReports _InterRail = new InterRailReports();
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        public List<InterRailSettlement> InterrailSettlementList = new List<InterRailSettlement>();
        public List<INTERRAILSummaryReport_Result> InterrailEOMList = new List<INTERRAILSummaryReport_Result>();
        public List<INTERRAILPASSSALE_Result> InterrailPassSaleList = new List<INTERRAILPASSSALE_Result>();
        public string Commition = "10.00%";


        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, "");
            if (!Page.IsPostBack)
            {
                FillDropDown();
                _siteId = Master.SiteID;
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void FillDropDown()
        {
            for (int i = 1; i < 13; i++)
                ddlMonth.Items.Add(new ListItem { Value = i.ToString(), Text = "(" + i + ") " + System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
            for (int i = 2018; i < 2018 + 10; i++)
                ddlYear.Items.Add(new ListItem { Value = i.ToString(), Text = i.ToString() });
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                datadiv.Visible = true;
                BindGridSettlement();
                BindGridEOM();
                BindGridPassSale();
                btnExportToExcel.Visible = true;
                ScriptManager.RegisterStartupScript(Page, GetType(), "tabheader", ";ActiveMenu('" + hdnlastactive.Value + "')", true);
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        #region Interrail Settlement Report
        void BindGridSettlement()
        {
            try
            {
                var list = new List<InterRailSettlement>();
                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                int year = Convert.ToInt32(ddlYear.SelectedValue);

                CultureInfo usEnglish = new CultureInfo("en-US");
                DateTimeFormatInfo englishInfo = usEnglish.DateTimeFormat;
                MonthName = englishInfo.MonthNames[Convert.ToInt32(month) - 1] + " (" + ddlYear.SelectedValue + ")";
                _siteId = Master.SiteID;
                var data = db.tblSites.FirstOrDefault(t => t.ID == _siteId);
                list.Add(new InterRailSettlement { Id = 1, Title = "To:  Rail Settlement Plan Ltd." });
                list.Add(new InterRailSettlement { Id = 2, Title = "cc: ATOC", Other = "Fax to be:  " + (data != null ? data.PhoneNumber : "011-44-207-841-8262") });
                list.Add(new InterRailSettlement { Id = 3, Title = "INTERRAIL SETTLEMENT FOR THE MONTH OF:", Other = MonthName });
                list.AddRange(_InterRail.GetINTERRAILSettlementReport(month,year));
                list.Add(new InterRailSettlement { Id = 97, Title = "GRAND TOTAL", Amount = list.FirstOrDefault(x => x.Title == "Net amount due to RSP").Amount });
                list.Add(new InterRailSettlement { Id = 98, Title = "For any questions regarding the payment, please do not hesitate to contact - Agent Accounts Payable contact." });
                GrdSettlement.DataSource = InterrailSettlementList = list;
                GrdSettlement.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");

            }
        }

        protected void GrdSettlement_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.CssClass = "Alternet";
                    e.Row.Cells[1].Attributes.Add("style", "text-align: right;padding-right: 24%;");
                    var lbldata = e.Row.FindControl("lbldata") as Label;
                    int Id = ((InterRailSettlement)(e.Row.DataItem)).Id;
                    string Other = ((InterRailSettlement)(e.Row.DataItem)).Other;
                    decimal Amount = ((InterRailSettlement)(e.Row.DataItem)).Amount;
                    if (Id == 3 || Id == 97)
                        e.Row.Font.Bold = true;
                    if (Id == 1 || Id == 2 || Id == 3)
                        lbldata.Text = Other;
                    else
                        lbldata.Text = Amount.ToString();
                    if (Id == 4)
                    {
                        e.Row.Font.Bold = true;
                        lbldata.Text = "";
                    }
                    if (Id == 98)
                    {
                        e.Row.Cells[0].ColumnSpan = 2;
                        e.Row.Cells[1].Attributes.Add("style", "display:none;");
                    }
                    if (Id == 97)
                        e.Row.Attributes.Add("style", "border-top:2px solid;border-bottom:2px solid;");
                }
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }
        #endregion

        #region Brintrail EOM Report

        void BindGridEOM()
        {
            try
            {
                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                int year = Convert.ToInt32(ddlYear.SelectedValue);
                var list = _InterRail.GetINTERRAILSummaryReport(month,year);
                grdSummary.DataSource = InterrailEOMList = (list.Count > 1 ? list : null);
                grdSummary.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void grdSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSummary.PageIndex = e.NewPageIndex;
            BindGridEOM();
        }

        protected void grdSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string Continent = ((Business.INTERRAILSummaryReport_Result)(e.Row.DataItem)).CONTINENT;
                    string Categoryname = ((Business.INTERRAILSummaryReport_Result)(e.Row.DataItem)).CATEGORYNAME;
                    if (Continent == "NORTH AMERICA" || Continent == "EMERGING MARKETS")
                    {
                        e.Row.Cells[0].Font.Bold = true;
                    }
                    if (Categoryname == "Total Sales" || Categoryname == "Total Refunds" || Categoryname == "Sales Less Refunds")
                    {
                        e.Row.Font.Bold = true;
                    }
                    int Filter = (int)((Business.INTERRAILSummaryReport_Result)(e.Row.DataItem)).FILTER;
                    if (Filter == 1)
                        hold = !hold;
                    if (hold)
                        e.Row.CssClass = "Alternet";
                    else
                        e.Row.CssClass = "Alternetnot";
                    if (Filter == 0)
                        e.Row.CssClass = "Totlecss";
                    if (Categoryname == "Sales Less Refunds")
                        e.Row.Attributes.Add("style", "border-top:1px solid #111;border-bottom:2px solid #111");
                }
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        #endregion

        #region Interrail PassSale Report
        void BindGridPassSale()
        {
            int month = Convert.ToInt32(ddlMonth.SelectedValue);
            int year = Convert.ToInt32(ddlYear.SelectedValue);
            var list = _InterRail.GetINTERRAILPASSSALE(month,year);
            grdPassSale.DataSource = InterrailPassSaleList = list;
            grdPassSale.DataBind();
        }

        protected void grdPassSale_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPassSale.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGridPassSale();
            ScriptManager.RegisterStartupScript(Page, GetType(), "tabheader", ";ActiveMenu('" + hdnlastactive.Value + "')", true);
        }
        #endregion

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            try
            {
                BindGridSettlement();
                BindGridEOM();
                BindGridPassSale();
                /********************/
                MemoryStream MyMemoryStream = new MemoryStream();
                XLWorkbook wb = new XLWorkbook();

                wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.LeftBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
                wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
                wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.TopBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
                wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));

                var ws1 = wb.Worksheets.Add("SUMMARY");//Interrail Settlement Report
                ws1.TabColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#f9c"));
                ws1.RowHeight = 20;
                ws1.Style.Font.FontSize = 10;

                #region Interrail Settlement Report
                if (InterrailSettlementList != null && InterrailSettlementList.Count > 1)
                {
                    var Title = ws1.Cell("A1").CellBelow();
                    var Name = ws1.Cell("B1").CellBelow();
                    var Amount = ws1.Cell("D1").CellBelow();
                    var Curr = ws1.Cell("E1").CellBelow();
                    foreach (var item in InterrailSettlementList)
                    {
                        switch (item.Id)
                        {
                            case 1:
                                Title = Title.CellBelow().SetValue(item.Title);
                                Name = Name.CellBelow().SetValue(item.Other);
                                Amount = Amount.CellBelow();
                                Curr = Curr.CellBelow();
                                break;
                            case 2:
                                Title = Title.CellBelow().SetValue(item.Title);
                                Name = Name.CellBelow().SetValue(item.Other);
                                Name.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                Amount = Amount.CellBelow();
                                Curr = Curr.CellBelow();

                                Title = Title.CellBelow();
                                Name = Name.CellBelow();
                                Amount = Amount.CellBelow();
                                Curr = Curr.CellBelow();
                                break;
                            case 3:
                                Title = Title.CellBelow().SetValue(item.Title);
                                Title.Style.Font.Bold = true;
                                Name = Name.CellBelow();
                                Amount = Amount.CellBelow().SetValue(item.Other);
                                Amount.Style.Font.Bold = true;
                                Amount.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                Curr = Curr.CellBelow();
                                Amount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c4d79b"));

                                Title = Title.CellBelow().CellBelow();
                                Name = Name.CellBelow().CellBelow();
                                Amount = Amount.CellBelow().CellBelow();
                                Curr = Curr.CellBelow().CellBelow();
                                break;
                            case 4:
                                Title = Title.CellBelow().SetValue(item.Title);
                                Title.Style.Font.Bold = true;
                                Name = Name.CellBelow();
                                Amount = Amount.CellBelow();
                                Curr = Curr.CellBelow();
                                break;
                            case 5:
                                Title = Title.CellBelow();
                                Name = Name.CellBelow().SetValue(item.Title);
                                Amount = Amount.CellBelow().SetValue(item.Amount);
                                Curr = Curr.CellBelow().SetValue("£");
                                Amount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#95b3d7"));
                                break;
                            case 6:
                                Title = Title.CellBelow();
                                Name = Name.CellBelow().SetValue(item.Title);
                                Amount = Amount.CellBelow().SetValue(item.Amount);
                                Curr = Curr.CellBelow().SetValue("£");
                                Amount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#da9694"));
                                break;
                            case 7:
                                Title = Title.CellBelow();
                                Name = Name.CellBelow().SetValue(item.Title);
                                Amount = Amount.CellBelow().SetValue(item.Amount);
                                Curr = Curr.CellBelow().SetValue("£");
                                Amount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#b1a0c7"));
                                Amount.Style.Font.FontColor = XLColor.Red;
                                break;
                            case 8:
                                Title = Title.CellBelow();
                                Name = Name.CellBelow().SetValue(item.Title);
                                Amount = Amount.CellBelow().SetValue(item.Amount);
                                Curr = Curr.CellBelow().SetValue("£");
                                Amount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#fabf8f"));
                                Amount.Style.Font.FontColor = XLColor.Red;
                                break;
                            case 9:
                                Title = Title.CellBelow();
                                Name = Name.CellBelow().SetValue(item.Title);
                                Amount = Amount.CellBelow().SetValue(item.Amount);
                                Curr = Curr.CellBelow().SetValue("£");
                                break;
                            case 97:
                                Title = Title.CellBelow().CellBelow();
                                Name = Name.CellBelow().CellBelow().SetValue(item.Title);
                                Amount = Amount.CellBelow().CellBelow().SetValue(item.Amount);
                                Curr = Curr.CellBelow().CellBelow().SetValue("£");
                                Name.Style.Font.Bold = true;
                                Amount.Style.Font.Bold = true;
                                Name.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

                                ws1.Cell(16, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                ws1.Cell(16, 4).Style.Border.BottomBorderColor = XLColor.Black;
                                ws1.Cell(16, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                                ws1.Cell(16, 4).Style.Border.LeftBorderColor = XLColor.Black;
                                ws1.Cell(16, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                                ws1.Cell(16, 4).Style.Border.TopBorderColor = XLColor.Black;
                                ws1.Cell(16, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                                ws1.Cell(16, 4).Style.Border.RightBorderColor = XLColor.Black;

                                Title = Title.CellBelow().CellBelow();
                                Name = Name.CellBelow().CellBelow();
                                Amount = Amount.CellBelow().CellBelow();
                                Curr = Curr.CellBelow().CellBelow();
                                break;
                            case 98:
                                Title = Title.CellBelow().SetValue(item.Title);
                                Title.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c4d79b"));
                                ws1.Range("A1:D1").Row(19).Merge();
                                ws1.Style.Alignment.WrapText = true;
                                //ws2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                                break;
                        }
                    }
                    //ws1.Cell(17, 1).FormulaA1 = "=(B9)-SUM(B11+B14)";
                    var cols1 = ws1.Column(1);
                    cols1.Width = 35;
                    var cols2 = ws1.Column(2);
                    cols2.Width = 35;
                    var cols3 = ws1.Column(3);
                    cols3.Width = 2;
                    var cols4 = ws1.Column(4);
                    cols4.Width = 20;
                    var cols5 = ws1.Column(5);
                    cols5.Width = 5;
                }
                #endregion

                var ws2 = wb.Worksheets.Add("SUMMARY2");//Interrail EOM Report
                ws2.TabColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffcc99"));
                ws2.RowHeight = 25;
                ws2.Style.Font.FontSize = 10;
                ws2.Range("B1:H1").Row(4).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                ws2.Range("B1:H1").Row(4).Style.Font.SetBold(true);
                ws2.Range("B1:H1").Row(5).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                ws2.Range("B1:H1").Row(5).Style.Font.SetBold(true);
                ws2.Style.Alignment.WrapText = true;
                ws2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;

                #region Interrail EOM Report
                if (InterrailEOMList != null && InterrailEOMList.Count > 1)
                {
                    var firstRow = ws2.Cell("A1").CellBelow().CellBelow().CellBelow();
                    var Continent = ws2.Cell("B1").CellBelow().CellBelow().CellBelow();
                    var Categoryname = ws2.Cell("C1").CellBelow().CellBelow().CellBelow();
                    var Units = ws2.Cell("D1").CellBelow().CellBelow().CellBelow();
                    var Price = ws2.Cell("E1").CellBelow().CellBelow().CellBelow();
                    var Commission = ws2.Cell("F1").CellBelow().CellBelow().CellBelow();
                    var Isitxpass = ws2.Cell("G1").CellBelow().CellBelow().CellBelow();
                    var Netamount = ws2.Cell("H1").CellBelow().CellBelow().CellBelow();

                    #region Top border
                    Continent.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    Categoryname.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    Units.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    Price.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    Commission.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    Isitxpass.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    Netamount.Style.Border.TopBorder = XLBorderStyleValues.Medium;

                    Continent.Style.Border.TopBorderColor = XLColor.Black;
                    Categoryname.Style.Border.TopBorderColor = XLColor.Black;
                    Units.Style.Border.TopBorderColor = XLColor.Black;
                    Price.Style.Border.TopBorderColor = XLColor.Black;
                    Commission.Style.Border.TopBorderColor = XLColor.Black;
                    Isitxpass.Style.Border.TopBorderColor = XLColor.Black;
                    Netamount.Style.Border.TopBorderColor = XLColor.Black;

                    Continent.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    Continent.Style.Border.LeftBorderColor = XLColor.Black;
                    Netamount.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    Netamount.Style.Border.RightBorderColor = XLColor.Black;

                    Categoryname.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    Categoryname.Style.Border.LeftBorderColor = XLColor.Black;
                    Categoryname.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    Categoryname.Style.Border.RightBorderColor = XLColor.Black;

                    Units.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    Price.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    Commission.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    Isitxpass.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    Netamount.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    #endregion

                    Continent = Continent.SetValue("").CellBelow();
                    Categoryname = Categoryname.SetValue("").CellBelow();
                    Units = Units.SetValue("Units").CellBelow();
                    Price = Price.SetValue("Total Gross Sales").CellBelow();
                    Commission = Commission.SetValue("Commission").CellBelow().SetValue(Commition);
                    Isitxpass = Isitxpass.SetValue("NET Portion ITX").CellBelow();
                    Netamount = Netamount.SetValue("Net Amount Due to RSP").CellBelow();

                    #region Bottom border
                    Commission.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    Continent.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    Continent.Style.Border.LeftBorderColor = XLColor.Black;
                    Netamount.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    Netamount.Style.Border.RightBorderColor = XLColor.Black;

                    Categoryname.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    Categoryname.Style.Border.LeftBorderColor = XLColor.Black;
                    Categoryname.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    Categoryname.Style.Border.RightBorderColor = XLColor.Black;

                    Continent.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    Categoryname.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    Units.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    Price.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    Commission.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    Isitxpass.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    Netamount.Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                    Continent.Style.Border.BottomBorderColor = XLColor.Black;
                    Categoryname.Style.Border.BottomBorderColor = XLColor.Black;
                    Units.Style.Border.BottomBorderColor = XLColor.Black;
                    Price.Style.Border.BottomBorderColor = XLColor.Black;
                    Commission.Style.Border.BottomBorderColor = XLColor.Black;
                    Isitxpass.Style.Border.BottomBorderColor = XLColor.Black;
                    Netamount.Style.Border.BottomBorderColor = XLColor.Black;

                    #endregion

                    bool IsNorthAmerica = false;
                    bool IsNext = false;
                    int CurrentRow = 5;
                    int HoldstartRow = 6;
                    bool MatchTotal = true;
                    bool GroupSection = false;
                    bool ṬotalGroupSection = false;
                    foreach (var item in InterrailEOMList)
                    {
                        CurrentRow++;
                        if (item.CATEGORYNAME == "Total Sales" || item.CATEGORYNAME == "Total Refunds" || item.CATEGORYNAME == "Sales Less Refunds")
                        {
                            bool isRefund = item.CATEGORYNAME == "Total Refunds";
                            firstRow = firstRow.CellBelow();
                            Continent = Continent.CellBelow().SetValue(item.CONTINENT);
                            Categoryname = Categoryname.CellBelow().SetValue(item.CATEGORYNAME);
                            Units = Units.CellBelow().SetValue(isRefund ? -item.UNITS : item.UNITS);
                            Price = Price.CellBelow().SetValue(isRefund ? -item.PRICE : item.PRICE);
                            Commission = Commission.CellBelow().SetValue(isRefund ? -item.COMMISSION : item.COMMISSION);
                            Isitxpass = Isitxpass.CellBelow().SetValue(isRefund ? -Convert.ToDecimal(item.ISITXPASS) : Convert.ToDecimal(item.ISITXPASS));
                            Netamount = Netamount.CellBelow().SetValue(isRefund ? -item.NETAMOUNT : item.NETAMOUNT);

                            #region Other style .........................
                            Price.Style.NumberFormat.Format = "£0.00";
                            Price.DataType = XLCellValues.Number;
                            Commission.Style.NumberFormat.Format = "£0.00";
                            Commission.DataType = XLCellValues.Number;
                            Isitxpass.Style.NumberFormat.Format = "£0.00";
                            Isitxpass.DataType = XLCellValues.Number;
                            Netamount.Style.NumberFormat.Format = "£0.00";
                            Netamount.DataType = XLCellValues.Number;

                            Continent.Style.Font.SetBold(true);
                            Categoryname.Style.Font.SetBold(true);
                            Units.Style.Font.SetBold(true);
                            Price.Style.Font.SetBold(true);
                            Commission.Style.Font.SetBold(true);
                            Isitxpass.Style.Font.SetBold(true);
                            Netamount.Style.Font.SetBold(true);

                            Continent.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            Continent.Style.Border.LeftBorderColor = XLColor.Black;
                            Netamount.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            Netamount.Style.Border.RightBorderColor = XLColor.Black;
                            Categoryname.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            Categoryname.Style.Border.LeftBorderColor = XLColor.Black;
                            Categoryname.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            Categoryname.Style.Border.RightBorderColor = XLColor.Black;
                            Units.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            Units.Style.Border.RightBorderColor = XLColor.Black;

                            if (MatchTotal)
                            {
                                Continent.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Categoryname.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Units.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Commission.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Isitxpass.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Netamount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));

                                Continent.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Continent.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Categoryname.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Categoryname.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Units.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Units.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Price.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Price.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Commission.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Commission.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Isitxpass.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Isitxpass.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Netamount.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Netamount.Style.Border.BottomBorder = XLBorderStyleValues.None;
                            }
                            if (!MatchTotal && item.CATEGORYNAME == "Sales Less Refunds")
                            {
                                Commission.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#fabf8f"));
                                Isitxpass.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#da9694"));
                            }
                            if (!MatchTotal && (item.CATEGORYNAME == "Total Sales" || item.CATEGORYNAME == "Total Refunds"))
                            {
                                if (item.CATEGORYNAME == "Total Sales")
                                    Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#95b3d7"));
                                else if (item.CATEGORYNAME == "Total Refunds")
                                    Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#b1a0c7"));

                                Units.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                                Price.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                                Commission.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                                Isitxpass.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                                Netamount.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                                Units.Style.Border.TopBorderColor = XLColor.Black;
                                Price.Style.Border.TopBorderColor = XLColor.Black;
                                Commission.Style.Border.TopBorderColor = XLColor.Black;
                                Isitxpass.Style.Border.TopBorderColor = XLColor.Black;
                                Netamount.Style.Border.TopBorderColor = XLColor.Black;
                            }
                            if (item.CATEGORYNAME == "Sales Less Refunds")
                            {
                                ws2.Range("B" + HoldstartRow + ":B" + CurrentRow + "").Column(1).Merge();
                                IsNext = true;

                                Units.Style.Border.TopBorder = XLBorderStyleValues.Double;
                                Price.Style.Border.TopBorder = XLBorderStyleValues.Double;
                                Commission.Style.Border.TopBorder = XLBorderStyleValues.Double;
                                Isitxpass.Style.Border.TopBorder = XLBorderStyleValues.Double;
                                Netamount.Style.Border.TopBorder = XLBorderStyleValues.Double;
                                Units.Style.Border.TopBorderColor = XLColor.Black;
                                Price.Style.Border.TopBorderColor = XLColor.Black;
                                Commission.Style.Border.TopBorderColor = XLColor.Black;
                                Isitxpass.Style.Border.TopBorderColor = XLColor.Black;
                                Netamount.Style.Border.TopBorderColor = XLColor.Black;

                                Continent.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Categoryname.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Units.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Price.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Commission.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Isitxpass.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Netamount.Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                                Continent.Style.Border.BottomBorderColor = XLColor.Black;
                                Categoryname.Style.Border.BottomBorderColor = XLColor.Black;
                                Units.Style.Border.BottomBorderColor = XLColor.Black;
                                Price.Style.Border.BottomBorderColor = XLColor.Black;
                                Commission.Style.Border.BottomBorderColor = XLColor.Black;
                                Isitxpass.Style.Border.BottomBorderColor = XLColor.Black;
                                Netamount.Style.Border.BottomBorderColor = XLColor.Black;
                            }
                            if (IsNorthAmerica)
                            {
                                Units.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Isitxpass.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Continent.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Categoryname.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Commission.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Netamount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                            }
                            #endregion

                            if (ṬotalGroupSection && (item.CATEGORYNAME == "Total Sales" || item.CATEGORYNAME == "Total Refunds"))
                            {
                                firstRow = firstRow.CellBelow();
                                Continent = Continent.CellBelow();
                                Categoryname = Categoryname.CellBelow();
                                Units = Units.CellBelow();
                                Price = Price.CellBelow();
                                Commission = Commission.CellBelow();
                                Isitxpass = Isitxpass.CellBelow();
                                Netamount = Netamount.CellBelow();


                                Continent.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                                Continent.Style.Border.LeftBorderColor = XLColor.Black;
                                Netamount.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                                Netamount.Style.Border.RightBorderColor = XLColor.Black;
                                Categoryname.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                                Categoryname.Style.Border.LeftBorderColor = XLColor.Black;
                                Categoryname.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                                Categoryname.Style.Border.RightBorderColor = XLColor.Black;
                                Units.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                                Units.Style.Border.RightBorderColor = XLColor.Black;
                            }
                        }
                        else
                        {
                            #region Other style .........................
                            if (IsNext)
                            {
                                IsNext = false;
                                firstRow = firstRow.CellBelow();
                                Continent = Continent.CellBelow();
                                Categoryname = Categoryname.CellBelow();
                                Units = Units.CellBelow();
                                Price = Price.CellBelow();
                                Commission = Commission.CellBelow();
                                Isitxpass = Isitxpass.CellBelow();
                                Netamount = Netamount.CellBelow();
                                ws2.Row(CurrentRow).Height = 25;

                                Continent.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Categoryname.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Units.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Price.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Commission.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Isitxpass.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                                Netamount.Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                                Continent.Style.Border.BottomBorderColor = XLColor.Black;
                                Categoryname.Style.Border.BottomBorderColor = XLColor.Black;
                                Units.Style.Border.BottomBorderColor = XLColor.Black;
                                Price.Style.Border.BottomBorderColor = XLColor.Black;
                                Commission.Style.Border.BottomBorderColor = XLColor.Black;
                                Isitxpass.Style.Border.BottomBorderColor = XLColor.Black;
                                Netamount.Style.Border.BottomBorderColor = XLColor.Black;
                                CurrentRow++;
                                HoldstartRow = CurrentRow;
                            }
                            #endregion

                            bool isRefund = item.CATEGORYNAME.Contains("Refunds");

                            if (item.CONTINENT == "Total")
                                ṬotalGroupSection = true;

                            firstRow = firstRow.CellBelow();
                            Continent = Continent.CellBelow().SetValue(item.CONTINENT);
                            Categoryname = Categoryname.CellBelow().SetValue(item.CATEGORYNAME);
                            Units = Units.CellBelow().SetValue(isRefund ? (item.UNITS == 0 ? (dynamic)"" : -item.UNITS) : item.UNITS);
                            Price = Price.CellBelow().SetValue(isRefund ? (item.PRICE == 0 ? (dynamic)"" : -item.PRICE) : (item.PRICE == 0 ? (dynamic)"" : item.PRICE));
                            Commission = Commission.CellBelow().SetValue(isRefund ? -item.COMMISSION : item.COMMISSION);
                            Isitxpass = Isitxpass.CellBelow().SetValue(isRefund ? (Convert.ToDecimal(item.ISITXPASS) == 0 ? (dynamic)"" : -Convert.ToDecimal(item.ISITXPASS)) : (Convert.ToDecimal(item.ISITXPASS) == 0 ? (dynamic)"" : Convert.ToDecimal(item.ISITXPASS)));
                            Netamount = Netamount.CellBelow().SetValue(isRefund ? -item.NETAMOUNT : item.NETAMOUNT);

                            #region  Other style..................
                            Continent.Style.Alignment.TextRotation = 90;
                            Continent.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            Continent.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            Continent.Style.Alignment.TextRotation = 90;
                            Continent.Style.Font.FontSize = 15;
                            Continent.Style.Font.SetBold(true);
                            if (item.CONTINENT == "Total")
                                MatchTotal = false;
                            if (MatchTotal)
                            {
                                Units.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c4d79b"));
                                Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c4d79b"));
                                Isitxpass.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c4d79b"));
                                Continent.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Categoryname.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Commission.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));
                                Netamount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d9d9d9"));

                                Continent.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Continent.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Categoryname.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Categoryname.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Units.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Units.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Price.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Price.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Commission.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Commission.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Isitxpass.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Isitxpass.Style.Border.BottomBorder = XLBorderStyleValues.None;
                                Netamount.Style.Border.TopBorder = XLBorderStyleValues.None;
                                Netamount.Style.Border.BottomBorder = XLBorderStyleValues.None;
                            }
                            Continent.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            Continent.Style.Border.LeftBorderColor = XLColor.Black;
                            Netamount.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            Netamount.Style.Border.RightBorderColor = XLColor.Black;
                            Categoryname.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            Categoryname.Style.Border.LeftBorderColor = XLColor.Black;
                            Categoryname.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            Categoryname.Style.Border.RightBorderColor = XLColor.Black;
                            Units.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            Units.Style.Border.RightBorderColor = XLColor.Black;

                            if (item.CONTINENT == "North America")
                                IsNorthAmerica = true;
                            else if (IsNorthAmerica && !string.IsNullOrEmpty(item.CONTINENT))
                                IsNorthAmerica = false;
                            if (IsNorthAmerica)
                            {
                                Units.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Isitxpass.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Continent.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Categoryname.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Commission.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                                Netamount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#963634"));
                            }
                            #endregion
                        }
                        if (GroupSection && !string.IsNullOrEmpty(item.CONTINENT))
                        {
                            GroupSection = false;
                        }
                        if (item.CONTINENT == "NORTH AMERICA" || item.CONTINENT == "EMERGING MARKETS" || GroupSection)
                        {
                            GroupSection = true;
                            Continent.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                            Categoryname.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                            Units.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                            Price.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                            Commission.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                            Isitxpass.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                            Netamount.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c65911"));
                        }
                    }

                    var cols1 = ws2.Column(1);
                    cols1.Width = 2;
                    var cols2 = ws2.Column(2);
                    cols2.Width = 18;
                    var cols3 = ws2.Column(3);
                    cols3.Width = 45;
                    var cols4 = ws2.Column(4);
                    cols4.Width = 8;
                    var cols5 = ws2.Column(5);
                    cols5.Width = 15;
                    var cols6 = ws2.Column(6);
                    cols6.Width = 15;
                    var cols7 = ws2.Column(7);
                    cols7.Width = 15;
                    var cols8 = ws2.Column(8);
                    cols8.Width = 20;
                }
                #endregion

                var ws3 = wb.Worksheets.Add("DETAIL_ATOC");//Interrail Pass Sale Report
                ws3.TabColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#cfc"));
                ws3.RowHeight = 19;
                ws3.Style.Font.FontSize = 9;

                #region Interrail Passsale Report
                if (InterrailPassSaleList != null && InterrailPassSaleList.Count > 0)
                {
                    var PassTypeCode = ws3.Cell("A1").SetValue(MonthName).CellBelow();
                    PassTypeCode = PassTypeCode.SetValue("Code");
                    var PassDefinition = ws3.Cell("B1").CellBelow();
                    PassDefinition = PassDefinition.SetValue("Pass Definition/Description");
                    var Class = ws3.Cell("C1").CellBelow();
                    Class = Class.SetValue("Class");
                    var Duration = ws3.Cell("D1").CellBelow();
                    Duration = Duration.SetValue("Duration");
                    var TType = ws3.Cell("E1").CellBelow();
                    TType = TType.SetValue("Type");
                    var Units = ws3.Cell("F1").CellBelow();
                    Units = Units.SetValue("Units");
                    var ITXBIP = ws3.Cell("G1").CellBelow();
                    ITXBIP = ITXBIP.SetValue("ITX/BIP");
                    var Total = ws3.Cell("H1").CellBelow();
                    Total = Total.SetValue("Total GBB");
                    var Orders = ws3.Cell("I1").CellBelow();
                    Orders = Orders.SetValue("1track Order Id");

                    ws3.Range("A1:I1").Row(1).Merge();
                    ws3.Range("A1:I1").Row(1).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    ws3.Range("A1:I1").Rows(1, 2).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#c4d79b"));
                    ws3.Range("A1:I1").Rows(1, 2).Style.Font.SetBold(true);
                    ws3.Range("A1:I1").Row(2).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    ws3.Range("A1:I1").Row(2).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    ws3.Range("A1:I1").Row(2).SetAutoFilter();
                    int x = 2;
                    foreach (var item in InterrailPassSaleList)
                    {
                        x++;
                        PassTypeCode = PassTypeCode.CellBelow().SetValue(Convert.ToInt64(item.PASSTYPECODE.Trim()));
                        PassDefinition = PassDefinition.CellBelow().SetValue(item.PASSDEFINITION);
                        Class = Class.CellBelow().SetValue(item.CLASS);
                        Duration = Duration.CellBelow().SetValue(item.DURATION);
                        TType = TType.CellBelow().SetValue(item.TTYPE);
                        Units = Units.CellBelow().SetValue(item.UNITS);
                        ITXBIP = ITXBIP.CellBelow().SetValue(item.ITXBIP.Value ? "Yes" : "No");
                        Total = Total.CellBelow().SetValue(item.TOTAL);
                        Orders = Orders.CellBelow().SetValue(item.ORDERIDS);

                        ws3.Range("A1:I1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d8e4bc"));
                        ws3.Range("A1:I1").Row(x).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                        ws3.Range("A1:I1").Row(x).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    }
                    ws3.Columns().AdjustToContents();
                }
                #endregion

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "InterRail Report (sale date)"));
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlMonth.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            BindGridSettlement();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}
