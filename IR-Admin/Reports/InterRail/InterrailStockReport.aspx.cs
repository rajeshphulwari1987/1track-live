﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using ClosedXML.Excel;
using System.Collections.Generic;

namespace IR_Admin.Reports.InterRail
{
    public partial class InterrailStockReport : Page
    {
        public string currency = "$";
        private bool _isExport;
        readonly InterRailReports _master = new InterRailReports();
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;
        List<INTERRAILSTOCKREPORT_Result> list = new List<INTERRAILSTOCKREPORT_Result>();
        readonly private db_1TrackEntities db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
            PageloadEvent();
            BindGrid(_siteId);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PageloadEvent();
                _siteId = Master.SiteID;
                BindGrid(_siteId);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
                ShowHideRoleDiv();
            }
        }

        public void PageloadEvent()
        {
            try
            {
                _siteId = Master.SiteID;
                var list = new ManageOrder().GetAllBranchlistBySiteId(_siteId).OrderBy(t => t.PathName);
                ddlOffice.DataSource = list;
                ddlOffice.DataTextField = "PathName";
                ddlOffice.DataValueField = "Id";
                ddlOffice.DataBind();
                ddlOffice.Items.Insert(0, new ListItem("-All Office-", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void BindGrid(Guid siteId)
        {
            try
            {
                if (_isExport)
                    btnExportToExcel.Visible = true;
                if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
                {
                    d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                }
                list = _master.GetInterrailStockReport(_siteId, d1, d2, ddlOffice.SelectedValue.Trim(), chkStaSite.Checked);
                grdStock.DataSource = list;
                grdStock.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void grdStock_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdStock.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGrid(_siteId);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _siteId = Master.SiteID;
            _isExport = true;
            BindGrid(_siteId);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("InterrailStockReport.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            _siteId = Master.SiteID;
            BindGrid(_siteId);
            MemoryStream MyMemoryStream = new MemoryStream();
            XLWorkbook wb = new XLWorkbook();

            wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.LeftBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.TopBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));

            var ws1 = wb.Worksheets.Add("Interrail Stock Report");
            ws1.RowHeight = 20;
            ws1.Style.Font.FontSize = 10;

            #region Interrail Stock Report

            if (list != null && list.Count > 0)
            {
                var STOCKNUMBER = ws1.Cell("A1").SetValue("Stock Number");
                var LOCATION = ws1.Cell("B1").SetValue("Location");
                var PRINTEDDATETIME = ws1.Cell("C1").SetValue("Date & Time");
                var ORDERID = ws1.Cell("D1").SetValue("Order Number");
                var PASSENGERNAME = ws1.Cell("E1").SetValue("Passenger Name");
                var PRODUCTNAME = ws1.Cell("F1").SetValue("Product Name");
                var SALESPRICE = ws1.Cell("G1").SetValue("Sales Price");
                var SALESCURRENCY = ws1.Cell("H1").SetValue("Sales Currency");
                var PRODUCTNETEURPRICE = ws1.Cell("I1").SetValue("Net EUR Price");
                var SALESEURPRICE = ws1.Cell("J1").SetValue("Sale EUR Price");
                var STATUS = ws1.Cell("K1").SetValue("Status");

                ws1.Range("A1:K1").Row(1).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                ws1.Range("A1:K1").Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#00aeef"));
                ws1.Range("A1:K1").Row(1).Style.Font.FontColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                ws1.Range("A1:K1").Row(1).Style.Font.SetBold(true);
                int x = 2;
                foreach (var item in list)
                {
                    STOCKNUMBER = STOCKNUMBER.CellBelow().SetValue(item.STOCKNUMBER);
                    LOCATION = LOCATION.CellBelow().SetValue(item.LOCATION);
                    PRINTEDDATETIME = PRINTEDDATETIME.CellBelow().SetValue(item.PRINTEDDATETIME.Value.ToString("dd/MMM/yyyy HH:mm"));
                    ORDERID = ORDERID.CellBelow().SetValue(item.ORDERID);
                    PASSENGERNAME = PASSENGERNAME.CellBelow().SetValue(" " + item.PASSENGERNAME);
                    PRODUCTNAME = PRODUCTNAME.CellBelow().SetValue(item.PRODUCTNAME);
                    SALESPRICE = SALESPRICE.CellBelow().SetValue(item.SALESPRICE);
                    SALESCURRENCY = SALESCURRENCY.CellBelow().SetValue(item.SALESCURRENCY);
                    PRODUCTNETEURPRICE = PRODUCTNETEURPRICE.CellBelow().SetValue(item.PRODUCTNETEURPRICE);
                    SALESEURPRICE = SALESEURPRICE.CellBelow().SetValue(item.SALESEURPRICE);
                    STATUS = STATUS.CellBelow().SetValue(item.STATUS);
                    if (x % 2 == 0)
                        ws1.Range("A1:K1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#a1e5ff"));
                    else
                        ws1.Range("A1:K1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ececec"));
                    ws1.Range("A1:K1").Row(x).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                    ws1.Range("A1:K1").Row(x).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                    x++;
                }
                ws1.Columns().AdjustToContents();
            }
            #endregion

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "Interrail Stock Report"));
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
       
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlOffice.SelectedIndex = 0;
            txtLastDate.Text = "";
            txtStartDate.Text = "";
            _siteId = Master.SiteID;
            BindGrid(_siteId);
        }

        private void ShowHideRoleDiv()
        {
            var data = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                if (data.IsStaRecordsAllow)
                    div_RoleVisible.Visible = true;
                else
                    div_RoleVisible.Visible = false;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}