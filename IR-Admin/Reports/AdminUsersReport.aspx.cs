﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using ClosedXML.Excel;
using System.Data;
using System.IO;

namespace IR_Admin.Reports
{
    public partial class AdminUsersReport : System.Web.UI.Page
    {
        public string currency = "$";
        private bool _isExport;
        private int _recordsPerPage = 50;
        private Int64 _totalRows = 0;
        readonly Masters _Master = new Masters();
        readonly private ManageOrder _MOrder = new ManageOrder();
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        List<AdminUsersReportsRoleBased_Result> list = new List<AdminUsersReportsRoleBased_Result>();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindRoles();
                BindGrid();
            }
        }

        public void BindRoles()
        {
            ddlRoles.DataSource = _Master.GetRolesList();
            ddlRoles.DataTextField = "RoleName";
            ddlRoles.DataValueField = "RoleID";
            ddlRoles.DataBind();
            ddlRoles.Items.Insert(0, new ListItem("--All Role--", "00000000-0000-0000-0000-000000000000"));
        }

        void BindGrid()
        {
            try
            {
                if (_isExport)
                    btnExportToExcel.Visible = true;
                list = _MOrder.AdminUsersReportsRoleBased(Guid.Parse(ddlRoles.SelectedValue));
                grdAdminusers.DataSource = list;
                grdAdminusers.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _isExport = true;
            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminUsersReport.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            list = _MOrder.AdminUsersReportsRoleBased(Guid.Parse(ddlRoles.SelectedValue));

            MemoryStream MyMemoryStream = new MemoryStream();
            XLWorkbook wb = new XLWorkbook();

            wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.LeftBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.TopBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));

            var ws1 = wb.Worksheets.Add("Admin Users Report");
            //ws1.TabColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#f9c"));
            ws1.RowHeight = 20;
            ws1.Style.Font.FontSize = 10;

            #region Admin Users Report

            if (list != null && list.Count > 0)
            {
                var USERNAME = ws1.Cell("A1").SetValue("User Name");
                var EMAILADDRESS = ws1.Cell("B1").SetValue("Email Address");
                var NAME = ws1.Cell("C1").SetValue("Name");
                var OFFICENAME = ws1.Cell("D1").SetValue("Office Name");
                var ROLENAME = ws1.Cell("E1").SetValue("Role Name");
                var NOTE = ws1.Cell("F1").SetValue("Note");
                var ISAGENT = ws1.Cell("G1").SetValue("IsAgent");
                var ISDELETED = ws1.Cell("H1").SetValue("IsDeleted");
                var MENUALP2P = ws1.Cell("I1").SetValue("Manual P2P");
                var MANUALPASS = ws1.Cell("J1").SetValue("Manual Pass");
                var SITENAME = ws1.Cell("K1").SetValue("Site Name");

                ws1.Range("A1:K1").Row(1).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                ws1.Range("A1:K1").Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#90689e"));
                ws1.Range("A1:K1").Row(1).Style.Font.FontColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                ws1.Range("A1:K1").Row(1).Style.Font.SetBold(true);
                //ws1.Range("A1:AB1").Row(2).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                //ws1.Range("A1:AB1").Row(2).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                //ws1.Range("A1:AB1").Row(1).SetAutoFilter();
                int x = 2;
                foreach (var item in list)
                {
                    USERNAME = USERNAME.CellBelow().SetValue(item.USERNAME);
                    EMAILADDRESS = EMAILADDRESS.CellBelow().SetValue(item.EMAILADDRESS);
                    NAME = NAME.CellBelow().SetValue(item.NAME);
                    OFFICENAME = OFFICENAME.CellBelow().SetValue(item.OFFICENAME);
                    ROLENAME = ROLENAME.CellBelow().SetValue(" " + item.ROLENAME);
                    NOTE = NOTE.CellBelow().SetValue(item.NOTE);
                    ISAGENT = ISAGENT.CellBelow().SetValue(item.ISAGENT);
                    ISDELETED = ISDELETED.CellBelow().SetValue(item.ISDELETED);
                    MENUALP2P = MENUALP2P.CellBelow().SetValue(item.MENUALP2P);
                    MANUALPASS = MANUALPASS.CellBelow().SetValue(item.MANUALPASS);
                    SITENAME = SITENAME.CellBelow().SetValue(item.SITENAME);

                    if (x % 2 == 0)
                        ws1.Range("A1:K1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#e1dae3"));
                    else
                        ws1.Range("A1:K1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d2bfd9"));
                    ws1.Range("A1:K1").Row(x).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    ws1.Range("A1:K1").Row(x).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    x++;
                }
                //Range.Rows.AutoFit();
                //Range.Columns.AutoFit();
                ws1.Columns().AdjustToContents();
            }
            #endregion

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "Admin Users Report ("+ddlRoles.SelectedItem.Text+")"));
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdAdminusers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAdminusers.PageIndex = e.NewPageIndex;
            BindGrid();
        }
    }
}