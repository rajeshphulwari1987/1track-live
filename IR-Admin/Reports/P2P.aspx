﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="P2P.aspx.cs" Inherits="IR_Admin.Reports.P2P" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }
        function showAdvancedSearch() {
            var img = $('#imgUpDown');
            var div = $('#divAdvancedSearch');
            div.slideDown(100, function () { img.attr('src', '../images/ar-up.png'); });
            SearchCriteriaChanged($('#hidAdvanceSearchType').val());
        }
        function resetAdvancedSearch() {
            try {
                $('#rdbAgent').removeAttr("checked");
                $('#rdbProductType').removeAttr("checked");
                $('#rdbOrderNo').removeAttr("checked");
                $('#rdbAgentReference').removeAttr("checked");
                $('#rdbProductCategory').removeAttr("checked");
                $('#divAgentSearch').hide();
                $('#divTextSearch').hide();
                clearAdvanceSearch();
            }
            catch (ex) {
                alert(ex);
            }
        }
        function clearAdvanceSearch() {
            $("#ddlSiteAgents").val('0');
            $('#txtTextSearch').val('');
        }
        $(function () {
            if ($('#hidAdvancedSearchToggle').val() != '') {
                showAdvancedSearch();
            }
            $('#divExpandCollapseAdvanceSearch').click(function () {
                try {

                    var div = $('#divAdvancedSearch');
                    var img = $('#imgUpDown');
                    if (!div.is(':visible')) {
                        div.slideDown(100, function () { img.attr('src', '../images/ar-up.png'); });
                        $('#hidAdvancedSearchToggle').val('show');
                    }
                    else {
                        div.slideUp(100);
                        img.attr('src', '../images/ar-down.png');
                        $('#hidAdvancedSearchToggle').val('');
                        $('#hidAdvanceSearchType').val('');
                        resetAdvancedSearch();
                    }
                }
                catch (ex) {
                    alert(ex);
                    return false;
                }
            });
        });

        function SearchCriteriaChanged(obj) {
            if (obj.toString() != $('#hidAdvanceSearchType').val().toString()) {
                clearAdvanceSearch();
            }
            switch (obj) {
                case 'agent':
                    showHideSearchCriteria('agent');
                    $('#hidAdvanceSearchType').val('agent');
                    break;
                case 'ProductType':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Product Type : ');
                    $('#hidAdvanceSearchType').val('ProductType');
                    break;
                case 'orderno':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Order No : ');
                    $('#hidAdvanceSearchType').val('orderno');
                    break;
                case 'agentref':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Order Reference : ');
                    $('#hidAdvanceSearchType').val('agentref');
                    break;
                case 'ProductCategory':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Product Category : ');
                    $('#hidAdvanceSearchType').val('ProductCategory');
                    break;
            }
        }
        function showHideSearchCriteria(obj) {
            switch (obj) {
                case 'agent':
                    $('#divAgentSearch').show();
                    $('#divTextSearch').hide();
                    break;
                case 'other':
                    $('#divTextSearch').show();
                    $('#divAgentSearch').hide();
                    break;
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        } 
    </script>
    <style type="text/css">
        .tabdiv
        {
            display: none;
        }
        .tabmenu ul
        {
            padding: 0px;
            margin: 0px;
        }
        .tabmenu ul li
        {
            padding: 5px 5px 5px 5px;
            float: left;
            list-style: none;
            margin: 0px 3px 0 0;
            background: #F1F1F1;
            border-left: 1px solid #00aeef;
            border-right: 1px solid #00aeef;
            border-top: 1px solid #00aeef;
            position: relative;
            border-radius: 8px 8px 0 0;
            -o-border-radius: 8px 8px 0 0;
            -ms-border-radius: 8px 8px 0 0;
            -webkit-border-radius: 8px 8px 0 0;
            -moz-border-radius: 8px 8px 0 0;
            behavior: url(PIE.htc);
            cursor: pointer;
        }
        .tabmenu ul li:hover, .tabmenu ul li.active
        {
            background: #DADADA;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hidAdvancedSearchToggle" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidAdvanceSearchType" runat="server" ClientIDMode="Static" />
    <h2>
        P2P Report</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div style="height: 30px;" id="div_RoleVisible" runat="server">
                            All STA site's record:
                            <asp:CheckBox runat="server" ID="chkStaSite" />
                            <div runat="server" id="divsyswide" visible="false" style="float: left; margin-right: 10px;">
                                System wide report<asp:CheckBox runat="server" ID="chkSysWide" /></div>
                        </div>
                        <div class="pass-coloum-two" style="width: 962px; float: left; padding-bottom: 8px;">
                            <span style="margin-right: 13px;">Site:</span>
                            <asp:DropDownList runat="server" ID="ddlSites" Width="673px" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlSites_SelectedIndexChanged">
                            </asp:DropDownList>
                            <div style="float: right;">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" runat="server" Width="85" OnClick="btnCancel_Click"
                                    class="button" Text="Reset" />
                            </div>
                        </div>
                        <div style="float: right; margin-right: 2px;">
                            &nbsp;<asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                                data-export="export" Width="206px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static"
                                Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                            <asp:RegularExpressionValidator ID="regx" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtStartDate" ValidationGroup="rv" ForeColor="Red" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static"
                                Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                            <asp:RegularExpressionValidator ID="regx2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtLastDate" ForeColor="Red" ValidationGroup="rv" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <%--Advanced Search panel starts here..--%>
                        <div class="collapse-tab">
                            <div id="divExpandCollapseAdvanceSearch" style="width: 938px; float: left; cursor: pointer;">
                                <img src="../images/ar-down.png" id="imgUpDown" class="icon" />
                                <strong>Advanced Search </strong>
                            </div>
                            <div id="divAdvancedSearch" style="display: none;" class="innertb-content">
                                <div class="pass-coloum-two" style="width: 916px; float: left;">
                                    <asp:RadioButton ID="rdbAgent" runat="server" Text="Agent Name" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('agent')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbOrderNo" runat="server" Text="1track Order No" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('orderno')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbAgentReference" runat="server" Text="Agent Reference" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('agentref')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbProductType" runat="server" Text="Supplier" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('ProductType')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbProductCategory" runat="server" Text="Station" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('ProductCategory')" ClientIDMode="Static" />
                                </div>
                                <div id="divAgentSearch" class="pass-coloum-two" style="float: left; padding-bottom: 8px;
                                    padding-top: 10px; padding-left: 10px; display: none">
                                    <asp:UpdatePanel ID="updAgentPanel" runat="server">
                                        <ContentTemplate>
                                            Agent Name :
                                            <asp:DropDownList runat="server" ID="ddlSiteAgents" Width="655px" class="input" ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div id="divTextSearch" class="pass-coloum-two" style="width: 962px; float: left;
                                    padding-bottom: 8px; padding-top: 10px; display: none">
                                    <label style="padding-left: 10px;">
                                        <div id="spnTextSearchLabel" style="width: 110px !important; float: left;">
                                        </div>
                                    </label>
                                    <asp:TextBox ID="txtTextSearch" runat="server" class="input" ClientIDMode="Static"
                                        MaxLength="50" Width="585px" />
                                </div>
                            </div>
                        </div>
                        <%--Advanced Search panel ends here..--%>
                    </div>
                    <div class="tabmenu">
                        <ul>
                            <li onclick="ActiveMenu('first')" id="first" class="active">P2P Report</li>
                            <li onclick="ActiveMenu('second')" id="second">P2P Summary Report</li>
                        </ul>
                    </div>
                    <br />
                    <div id="divfirst" class="grid-head2 tabdiv" style="float: left; width: 100%; min-height: 180px;
                        overflow-x: scroll; overflow-y: hidden; height: auto">
                        <asp:GridView ID="grdP2P" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            ForeColor="#333333" GridLines="None" Width="800%">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="1track order number">
                                    <ItemTemplate>
                                        <%#Eval("OrderId")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Affiliate">
                                    <ItemTemplate>
                                        <%#Eval("Affiliate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent reference">
                                    <ItemTemplate>
                                        <%#Eval("Reference")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Transaction type">
                                    <ItemTemplate>
                                        <%# Eval("TransactionType")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment date">
                                    <ItemTemplate>
                                        <%#Eval("BookedDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment time">
                                    <ItemTemplate>
                                        <%#Eval("BookedTime")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refund Date">
                                    <ItemTemplate>
                                        <%#Eval("RefundDate")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Website booked on">
                                    <ItemTemplate>
                                        <%# Eval("BookedOn")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Office">
                                    <ItemTemplate>
                                        <%# Eval("Office")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent">
                                    <ItemTemplate>
                                        <%#Eval("Agent")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier">
                                    <ItemTemplate>
                                        <%#Eval("Supplier")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DNR/PNR reference">
                                    <ItemTemplate>
                                        <%#Eval("DnrOrPnr")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lead passenger name">
                                    <ItemTemplate>
                                        <%#Eval("LeadPassenger")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="From">
                                    <ItemTemplate>
                                        <%#Eval("From")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="To">
                                    <ItemTemplate>
                                        <%#Eval("To")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date of departure">
                                    <ItemTemplate>
                                        <%#Eval("DepartureDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time of departure">
                                    <ItemTemplate>
                                        <%#Eval("DepartureTime")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date of arrival">
                                    <ItemTemplate>
                                        <%#Eval("ArrivalDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time of arrival">
                                    <ItemTemplate>
                                        <%#Eval("ArrivalTime")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Number of adults">
                                    <ItemTemplate>
                                        <%#Eval("Adult")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Number of youths">
                                    <ItemTemplate>
                                        <%#Eval("Youth")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Number of children">
                                    <ItemTemplate>
                                        <%#Eval("Children")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Number of seniors">
                                    <ItemTemplate>
                                        <%#Eval("Senior")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Class of travel">
                                    <ItemTemplate>
                                        <%#Eval("Class")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket type">
                                    <ItemTemplate>
                                        <%#Eval("TicketType")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delivery method">
                                    <ItemTemplate>
                                        <%#Eval("DeliveryMethod")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales currency">
                                    <ItemTemplate>
                                        <%#Eval("SalesCurrency")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales amount in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("SalesAmount")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Commission amount in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("CommissionFee")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NET amount in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("NetAmountInSalesCurrency")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier currency">
                                    <ItemTemplate>
                                        <%#Eval("SupplierCurrency")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier amount in supplier currency">
                                    <ItemTemplate>
                                        <%#Eval("SupplierAmountInSuplierRCurrency")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket protection in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("TicketProtectionFee")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booking fee in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("BookingFee")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Admin fee in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("AdminFee")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cxl/Extra Charge in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("AdminExtraCharge")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Shipping in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("ShippingFee")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discount Code Used">
                                    <ItemTemplate>
                                        <%#Eval("DiscountCode")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discounted Amount In Sales Currency">
                                    <ItemTemplate>
                                        <%#Eval("OrderDiscountAmount")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order grand total in sales currency">
                                    <ItemTemplate>
                                        <%#Eval("GrandTotal")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                        <div class="clear">
                        </div>
                        <table width="100%" style="padding-top: 10px;">
                            <tr class="paging">
                                <td style="float: left; margin-right: 3px;">
                                    <asp:LinkButton ID="lnkPrevious" Text='<< Previous' runat="server" OnClick="lnkPrevious_Click" />
                                </td>
                                <asp:Repeater ID="DLPageCountItem" runat="server">
                                    <ItemTemplate>
                                        <td style="float: left; margin-right: 3px;">
                                            <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <td style="float: left; margin-right: 3px;">
                                    <asp:LinkButton ID="lnkNext" Text='Next >>' runat="server" OnClick="lnkNext_Click" />
                                </td>
                                <td style="float: right;">
                                    <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divsecond" class="grid-head2 tabdiv" style="float: left; width: 100%; min-height: 180px;
                        height: auto">
                        <asp:GridView ID="grdP2PSummary" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            ForeColor="#333333" GridLines="None" Width="100%">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText=" ">
                                    <ItemTemplate>
                                        <%#Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Month">
                                    <ItemTemplate>
                                        <%#Eval("Month")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total">
                                    <ItemTemplate>
                                        <%#Eval("Total")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="1st Class">
                                    <ItemTemplate>
                                        <%# Eval("Class1st")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="2nd Class">
                                    <ItemTemplate>
                                        <%#Eval("Class2nd")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Other Class">
                                    <ItemTemplate>
                                        <%#Eval("ClassOther")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Adult">
                                    <ItemTemplate>
                                        <%#Eval("Adult")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Child">
                                    <ItemTemplate>
                                        <%# Eval("Children")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Senior">
                                    <ItemTemplate>
                                        <%#Eval("Senior")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Youth">
                                    <ItemTemplate>
                                        <%# Eval("Youth")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnlastactive" />
    <script type="text/javascript">
        ActiveMenu('first');
        function ActiveMenu(obj) {
            switch (obj) {
                case 'first':
                    $('.tabmenu ul li').removeClass('active');
                    $(".tabdiv").hide();
                    $('#' + obj).addClass('active');
                    $('#div' + obj).show();
                    break;
                case 'second':
                    $('.tabmenu ul li').removeClass('active');
                    $(".tabdiv").hide();
                    $('#' + obj).addClass('active');
                    $('#div' + obj).show();
                    break;
                case 'third':
                    $('.tabmenu ul li').removeClass('active');
                    $(".tabdiv").hide();
                    $('#' + obj).addClass('active');
                    $('#div' + obj).show();
                    break;
            }
            $("#hdnlastactive").val(obj);
        }
    </script>
</asp:Content>
