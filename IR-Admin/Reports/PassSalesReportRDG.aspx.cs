﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.IO;
using ClosedXML.Excel;

namespace IR_Admin.Reports
{
    public partial class PassSalesReportRDG :System.Web.UI.Page
    {
        public string currency = "$";
        private bool _isExport;
        private int _recordsPerPage = 50;
        private Int64 _totalRows = 0;
        readonly Masters _Master = new Masters();
        readonly private ManageOrder _MOrder = new ManageOrder();
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        List<PASSSALESREPORTBYSUPPLIER_Result> list = new List<PASSSALESREPORTBYSUPPLIER_Result>();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);

            //change site drop down selected value..
            ddlSites.SelectedValue = _siteId.ToString();

            ResetSearch();

            //PageloadEvent();
            BindGrid(1);
            BindPager(1);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divsyswide.Visible = chkSysWide.Checked = _Master.GetSystemReportByUserId(AdminuserInfo.UserID);
                BindSites();
                //PageloadEvent();
                BindGrid(1);
                BindPager(1);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
                ShowHideRoleDiv();
            }
        }

        public void BindSites()
        {
            _siteId = Master.SiteID;

            var result = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID);
            if (result == null)
                return;

            if (result.Count() > 0)
            {
                ddlSites.DataSource = result;
                ddlSites.DataTextField = "DisplayName";
                ddlSites.DataValueField = "ID";
                ddlSites.DataBind();

                if (AdminuserInfo.SiteID == new Guid())
                    ddlSites.SelectedIndex = 0;
                else
                    ddlSites.SelectedValue = _siteId.ToString();
            }
        }

        void BindGrid(int pageNo)
        {
            try
            {
                CurrentPage = pageNo;
                if (_isExport)
                    btnExportToExcel.Visible = true;
                if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
                {
                    d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                #region Advanced search filter
                string ProductName = "";
                string OrderNo = "";
                string ProductCategory = "";
                string AgentUserName = "";


                if (rdbProductName.Checked)
                    ProductName = txtTextSearch.Text.Trim();
                else if (rdbOrderNo.Checked)
                    OrderNo = txtTextSearch.Text.Trim();
                else if (rdbProductCategory.Checked)
                    ProductCategory = txtTextSearch.Text.Trim();
                else if (rdbAgentUserName.Checked)
                    AgentUserName = txtTextSearch.Text.Trim();
                #endregion

                string SiteId = ddlSites.SelectedValue;
                if (chkSysWide.Checked)
                    SiteId = "2";
                else if (chkStaSite.Checked)
                    SiteId = "1";

                list = _MOrder.PassSaleReportBySupplierId(SiteId, d1, d2, pageNo, _recordsPerPage, ProductCategory, ProductName, OrderNo, AdminuserInfo.UserID, "RDG (UK)");
                if (list.Count() > 0)
                    _totalRows = Convert.ToInt64(list.Select(x => x.TOTALROWS).First());
                else
                    _totalRows = 0;
                grdpasssale.DataSource = list;
                grdpasssale.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _isExport = true;
            BindGrid(1);
            BindPager(1);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CardRevenueReport.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
            {
                d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            #region Advanced search filter
            string PName = "";
            string OrderNo = "";
            string Category = "";

            if (rdbProductName.Checked)
                PName = txtTextSearch.Text.Trim();
            else if (rdbOrderNo.Checked)
                OrderNo = txtTextSearch.Text.Trim();
            else if (rdbProductCategory.Checked)
                Category = txtTextSearch.Text.Trim();
            #endregion

            string SiteId = ddlSites.SelectedValue;
            if (chkSysWide.Checked)
                SiteId = "2";
            else if (chkStaSite.Checked)
                SiteId = "1";

            list = _MOrder.PassSaleReportBySupplierId(SiteId, d1, d2, 1, 50000, Category, PName, OrderNo, AdminuserInfo.UserID, "RDG (UK)");

            MemoryStream MyMemoryStream = new MemoryStream();
            XLWorkbook wb = new XLWorkbook();

            wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.LeftBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.TopBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));

            var ws1 = wb.Worksheets.Add("Pass Sales Report Client");
            ws1.RowHeight = 20;
            ws1.Style.Font.FontSize = 10;

            #region Pass Sales Report Client

            if (list != null && list.Count > 0)
            {
                var PAYMENTDATE = ws1.Cell("A1").SetValue("Order Completed Date");
                var OrderCreated = ws1.Cell("B1").SetValue("Order Approved");
                var AgentReferenceNo = ws1.Cell("C1").SetValue("Agent reference");
                var OrderID = ws1.Cell("D1").SetValue("1track order number");
                var SaleRefund = ws1.Cell("E1").SetValue("Sale or refund");
                var ProductCategory = ws1.Cell("F1").SetValue("Product category");
                var ProductName = ws1.Cell("G1").SetValue("Product name");
                var ProductValidity = ws1.Cell("H1").SetValue("Product validity");
                var ProductClass = ws1.Cell("I1").SetValue("Product class");
                var PassholderType = ws1.Cell("J1").SetValue("Passholder type");
                var SalesCurrency = ws1.Cell("K1").SetValue("Sales currency");
                var GrossSalesAmount = ws1.Cell("L1").SetValue("Product GROSS sales amount in sales currency");
                var Commition = ws1.Cell("M1").SetValue("Commission in sales currency");
                var SettlementAmount = ws1.Cell("N1").SetValue("Settlement Amount in sales currency");
                var Supplier = ws1.Cell("O1").SetValue("Supplier");
                var Site = ws1.Cell("P1").SetValue("Site");
                var DATABASECURRENCY = ws1.Cell("Q1").SetValue("Database currency");
                var DATABASEAMOUNT = ws1.Cell("R1").SetValue("Database Amount");
                var STOCKNUMBER = ws1.Cell("S1").SetValue("Stock Number");

                ws1.Range("A1:S1").Row(1).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                ws1.Range("A1:S1").Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#90689e"));
                ws1.Range("A1:S1").Row(1).Style.Font.FontColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                ws1.Range("A1:S1").Row(1).Style.Font.SetBold(true);
                int x = 2;
                foreach (var item in list)
                {
                    PAYMENTDATE = PAYMENTDATE.CellBelow().SetValue(item.PAYMENTDATE);
                    OrderCreated = OrderCreated.CellBelow().SetValue(item.ORDERCREATED);
                    AgentReferenceNo = AgentReferenceNo.CellBelow().SetValue(" " + item.AGENTREFERENCENO);
                    OrderID = OrderID.CellBelow().SetValue(item.ORDERID);
                    SaleRefund = SaleRefund.CellBelow().SetValue(item.SALEREFUND);
                    ProductCategory = ProductCategory.CellBelow().SetValue(item.PRODUCTCATEGORY);
                    ProductName = ProductName.CellBelow().SetValue(item.PRODUCTNAME);
                    ProductValidity = ProductValidity.CellBelow().SetValue(item.PRODUCTVALIDITY);
                    ProductClass = ProductClass.CellBelow().SetValue(item.PRODUCTCLASS);
                    PassholderType = PassholderType.CellBelow().SetValue(item.PASSHOLDERTYPE);
                    SalesCurrency = SalesCurrency.CellBelow().SetValue(item.SALESCURRENCY);
                    GrossSalesAmount = GrossSalesAmount.CellBelow().SetValue(item.GROSSSALESAMOUNT);
                    Commition = Commition.CellBelow().SetValue(item.COMMITION);
                    SettlementAmount = SettlementAmount.CellBelow().SetValue(item.SETTLEMENTAMOUNT);
                    Supplier = Supplier.CellBelow().SetValue(item.SUPPLIERNAME);
                    Site = Site.CellBelow().SetValue(item.SITENAME);
                    DATABASECURRENCY = DATABASECURRENCY.CellBelow().SetValue(item.DATABASECURRENCY);
                    DATABASEAMOUNT = DATABASEAMOUNT.CellBelow().SetValue(item.DATABASEAMOUNT);
                    STOCKNUMBER = STOCKNUMBER.CellBelow().SetValue(item.STOCKNUMBER);

                    if (x % 2 == 0)
                        ws1.Range("A1:S1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#e1dae3"));
                    else
                        ws1.Range("A1:S1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#d2bfd9"));
                    ws1.Range("A1:S1").Row(x).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    ws1.Range("A1:S1").Row(x).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                    x++;
                }
                ws1.Columns().AdjustToContents();
            }
            #endregion

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "Supplier Report (RDG)"));
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        #region Paging
        public void BindPager(int selectedPageNo)
        {
            try
            {
                Int64 newpagecount = _totalRows / _recordsPerPage + ((_totalRows % _recordsPerPage) > 0 ? 1 : 0);
                var oPageList = new List<ClsPageCount>();
                lnkPrevious.Visible = true;
                lnkNext.Visible = true;
                litTotalPages.Visible = true;
                Int64 StartRange = selectedPageNo - 5;
                if (StartRange < 2)
                    StartRange = 1;
                Int64 EndRange = StartRange + 9;
                if (EndRange > newpagecount)
                {
                    EndRange = newpagecount;
                    StartRange = EndRange - 9;
                }
                if (StartRange < 2)
                    StartRange = 1;
                if (selectedPageNo == 1)
                    lnkPrevious.Visible = false;
                if (selectedPageNo == newpagecount)
                    lnkNext.Visible = false;
                if (newpagecount < 1)
                {
                    lnkPrevious.Visible = false;
                    lnkNext.Visible = false;
                    litTotalPages.Visible = false;
                    litTotalPages.Text = "";
                }
                else
                    litTotalPages.Text = "Showing page " + CurrentPage + " of " + newpagecount;
                for (Int64 i = StartRange; i <= EndRange; i++)
                {
                    var oPage = new ClsPageCount { PageCount = i.ToString() };
                    oPageList.Add(oPage);
                }
                DLPageCountItem.DataSource = oPageList;
                DLPageCountItem.DataBind();
                foreach (RepeaterItem item1 in DLPageCountItem.Items)
                {
                    var lblPage = (LinkButton)item1.FindControl("lnkPage");
                    if (lblPage != null)
                        lblPage.Attributes.Add("class", "activepaging");
                    break;
                }
                foreach (RepeaterItem item1 in DLPageCountItem.Items)
                {
                    var lblPage = (LinkButton)item1.FindControl("lnkPage");
                    if (lblPage.Text.Trim() == (selectedPageNo).ToString(CultureInfo.InvariantCulture))
                        lblPage.Attributes.Add("class", "activepaging");
                    else
                        lblPage.Attributes.Remove("class");
                }
            }
            catch (Exception ex) { }
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument);
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex).ToString(CultureInfo.InvariantCulture))
                    lblPage.Attributes.Add("class", "activepaging");
                else
                    lblPage.Attributes.Remove("class");
            }
            CurrentPage = pageIndex;
            BindGrid(CurrentPage);
            BindPager(CurrentPage);
        }
        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage - 1;
            BindGrid(CurrentPage);
            BindPager(CurrentPage);
        }
        protected void lnkNext_Click(object sender, EventArgs e)
        {
            CurrentPage = CurrentPage + 1;
            BindGrid(CurrentPage);
            BindPager(CurrentPage);
        }

        private int CurrentPage
        {
            get
            {
                return ((ViewState["PageIndex"] == null || ViewState["PageIndex"].ToString() == "") ? 1 : int.Parse(ViewState["PageIndex"].ToString()));
            }
            set
            {
                ViewState["PageIndex"] = value;
            }
        }
        #endregion

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetSearch();
            //PageloadEvent();
            BindGrid(1);
            BindPager(1);
        }

        private void ResetSearch()
        {
            rdbAgentUserName.Checked = false;
            rdbProductName.Checked = false;
            rdbOrderNo.Checked = false;
            rdbProductCategory.Checked = false;
            txtTextSearch.Text = "";
            txtStartDate.Text = "";
            txtLastDate.Text = "";
            hidAdvancedSearchToggle.Value = "";
            hidAdvanceSearchType.Value = "";
        }

        private void ShowHideRoleDiv()
        {
            var data = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                if (data.IsStaRecordsAllow)
                    div_RoleVisible.Visible = true;
                else
                    div_RoleVisible.Visible = false;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}