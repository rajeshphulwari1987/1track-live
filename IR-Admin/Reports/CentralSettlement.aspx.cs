﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace IR_Admin.Reports
{
    public partial class CentralSettlement : Page
    {
        public string currency = "$";
        private bool _isExport;
        readonly private ManageOrder _masterorder = new ManageOrder();
        readonly private ManageProduct _oProduct = new ManageProduct();
        readonly private ManageBooking _master = new ManageBooking();
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;
        readonly private db_1TrackEntities db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
            PageloadEvent();
            BindGrid(_siteId);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _siteId = Master.SiteID;
                PageloadEvent();
                BindGrid(_siteId);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
                ShowHideRoleDiv();
            }
        }
        public void PageloadEvent()
        {
            try
            {
                _siteId = Master.SiteID;
                var list = new ManageOrder().GetAllBranchlistBySiteId(_siteId).OrderBy(t => t.PathName);
                ddlOffice.DataSource = list;
                ddlOffice.DataTextField = "PathName";
                ddlOffice.DataValueField = "Id";
                ddlOffice.DataBind();
                ddlOffice.Items.Insert(0, new ListItem("-All Office-", "0"));

                ddlSupName.DataSource = _oProduct.GetSupplierList().Where(x => x.IsActive).GroupBy(x => new { x.Name }).Select(x => new { x.Key.Name }).ToList();
                ddlSupName.DataTextField = "Name";
                ddlSupName.DataValueField = "Name";
                ddlSupName.DataBind();
                ddlSupName.Items.Insert(0, new ListItem("--All Supplier--", ""));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void BindGrid(Guid Siteid)
        {
            try
            {
                if (_isExport)
                    btnExportToExcel.Visible = true;
                if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
                {
                    d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                var lstCentral = _master.GetCentralSettlement(d1, d2, ddlOffice.SelectedValue.Trim(), chkStaSite.Checked, ddlSupName.SelectedValue);

                grdCentralSettlement.DataSource = lstCentral;
                grdCentralSettlement.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void grdCentralSettlement_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCentralSettlement.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGrid(_siteId);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _isExport = true;
            _siteId = Master.SiteID;
            BindGrid(_siteId);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CentralSettlement.aspx");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }
        public void ExportToExcel()
        {
            grdCentralSettlement.AllowPaging = false;
            _siteId = Master.SiteID;
            BindGrid(_siteId);
            if (grdCentralSettlement.Rows.Count > 50000)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('Report is too large and may not be possible to be Exported to Excel, Please select shorter period.')", true);
            }
            else
            {
                grdCentralSettlement.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#d06b95");
                Context.Response.ClearContent();
                Context.Response.ContentType = "application/ms-excel";
                Context.Response.AddHeader("content-disposition",
                                           string.Format("attachment;filename={0}.xls", "Central Settlement"));
                Context.Response.Charset = "";
                var stringwriter = new System.IO.StringWriter();
                var htmlwriter = new HtmlTextWriter(stringwriter);
                grdCentralSettlement.RenderControl(htmlwriter);
                Context.Response.Write(stringwriter.ToString());
                Context.Response.End();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        private void ShowHideRoleDiv()
        {
            var data = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == AdminuserInfo.RoleId);
            if (data != null)
            {
                if (data.IsStaRecordsAllow)
                    div_RoleVisible.Visible = true;
                else
                    div_RoleVisible.Visible = false;
            }
        }
    }
}