﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="EurailStockReturnReport.aspx.cs" Inherits="IR_Admin.Reports.EurailStockReturnReport"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .BindGridScrolle
        {
            float: left;
            width: 100%;
            height: 100%;
            position: relative;
            overflow-x: scroll;
            border-radius: 7px;
        }
    </style>
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        function Selectall() {
            if ($("#MainContent_chkall").is(":checked")) {
                $("#MainContent_chklstPrintQueue").find(':checkbox').prop('checked', true)
            }
            else
                $("#MainContent_chklstPrintQueue").find(':checkbox').prop('checked', false)
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Eurail Stock Return Report</h2>
    <div class="full mr-tp1">
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div class="pass-coloum-two" style="width: 315px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px 0 6px;" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                        </div>
                        <div class="pass-coloum-two" style="width: 300px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px 0 6px;" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                        </div>
                        <div style="float: left;width: 100%;">
                            <p>
                                Choose the print queues you want to report on:</p>
                                <p><asp:CheckBox runat="server" ID="chkall" Text="Select All" onclick="Selectall()"/></p>
                            <div style="overflow-x: auto;max-height:250px; margin: 0px 0px 15px 0px;">
                                <asp:CheckBoxList runat="server" ID="chklstPrintQueue">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <div style="float: right">
                         <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Reset" Width="89px"
                                CausesValidation="false" OnClick="btnReset_Click" />
                                <br />
                            <asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel" style="margin-top:5px;" data-export="export" Width="206px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div>
                    </div>
                    <div class="BindGridScrolle">
                        <asp:GridView ID="grdStock" runat="server" AutoGenerateColumns="False" PageSize="50"
                            CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="190%"
                            AllowPaging="True" OnPageIndexChanging="grdStock_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Stock number">
                                    <ItemTemplate>
                                        <%#Eval("StockNumber")%>
                                    </ItemTemplate>  
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="Order number">
                                    <ItemTemplate>
                                        <%#Eval("OrderId")%>
                                    </ItemTemplate>
                                </asp:TemplateField>    
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <%#Eval("Status")%>
                                    </ItemTemplate>
                                </asp:TemplateField>     
                                <asp:TemplateField HeaderText="Print queue">
                                    <ItemTemplate>
                                        <%#Eval("QueueLocation")%>
                                    </ItemTemplate>
                                </asp:TemplateField>        
                                <asp:TemplateField HeaderText="Date printed">
                                    <ItemTemplate>
                                        <%#Eval("PrintedDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="Time printed">
                                    <ItemTemplate>
                                        <%#Eval("PrintedTime")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="Printed by">
                                    <ItemTemplate>
                                        <%#Eval("PrintedBy")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="Date refunded">
                                    <ItemTemplate>
                                        <%#Eval("RefundedDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time refunded">
                                    <ItemTemplate>
                                        <%#Eval("RefundedTime")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="Refunded by">
                                    <ItemTemplate>
                                        <%#Eval("RefundedBy")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="Passenger name">
                                    <ItemTemplate>
                                        <%#Eval("PassemgerName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>                               
                                <asp:TemplateField HeaderText="Product name">
                                    <ItemTemplate>
                                        <%#Eval("ProductName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
