﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using ClosedXML.Excel;

namespace IR_Admin.Reports
{
    public partial class StockAssignedTo : Page
    {
        private bool _isExport;
        readonly private ManageBooking _master = new ManageBooking();
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        List<StockAssignedToReport_Result> list = new List<StockAssignedToReport_Result>();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
            PageloadEvent();
            BindGrid();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PageloadEvent();
                _siteId = Master.SiteID;
                BindGrid();
            }
            ShowMessage(0,"");
        }

        public void PageloadEvent()
        {
            try
            {
                _siteId = Master.SiteID;
                var list = _master.GetAllPrinters();
                ddlPrinter.DataSource = list;
                ddlPrinter.DataTextField = "NAME";
                ddlPrinter.DataValueField = "BRANCHID";
                ddlPrinter.DataBind();
                ddlPrinter.Items.Insert(0, new ListItem("-All Printer Location-", "00000000-0000-0000-0000-000000000000"));

                var StockStatuslist = new List<SelectDDLList>();
                StockStatuslist.Add(new SelectDDLList { Id = "82DE1DBB-05D2-4AC2-BE59-569DDC9BE292", Name = "Pending Delivery" });
                StockStatuslist.Add(new SelectDDLList { Id = "1C3A2DCB-5D0F-48AE-8584-6C213362CDBB", Name = "Used" });
                StockStatuslist.Add(new SelectDDLList { Id = "BD82C702-8E84-4C9D-A666-99A3E1D5EE10", Name = "Held" });
                StockStatuslist.Add(new SelectDDLList { Id = "AF1D088B-03B9-4657-B231-C2787BA53425", Name = "Printed" });
                StockStatuslist.Add(new SelectDDLList { Id = "DE05D0A6-785C-4DA0-B650-CB3CAFCB5330", Name = "VOID" });
                StockStatuslist.Add(new SelectDDLList { Id = "7100B83E-07ED-418B-97E6-E5C6B746DA3B", Name = "Distroy" });
                StockStatuslist.Add(new SelectDDLList { Id = "0E758C24-D919-43D6-A80A-E60D9DA8C53A", Name = "Unused" });

                ddlStockStatus.DataSource = StockStatuslist.OrderBy(x => x.Name);
                ddlStockStatus.DataTextField = "Name";
                ddlStockStatus.DataValueField = "Id";
                ddlStockStatus.DataBind();
                ddlStockStatus.Items.Insert(0, new ListItem("-All Stock Status-", "00000000-0000-0000-0000-000000000000"));

                var PrinterTypelist = new List<SelectDDLList>();
                PrinterTypelist.Add(new SelectDDLList { Id = "1", Name = "Eurail" });
                PrinterTypelist.Add(new SelectDDLList { Id = "2", Name = "Britrail" });
                PrinterTypelist.Add(new SelectDDLList { Id = "3", Name = "Interrail" });

                ddlPrinterType.DataSource = PrinterTypelist.OrderBy(x => x.Name);
                ddlPrinterType.DataTextField = "Name";
                ddlPrinterType.DataValueField = "Id";
                ddlPrinterType.DataBind();
                ddlPrinterType.Items.Insert(0, new ListItem("-Printer Type-", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void BindGrid()
        {
            try
            {
                if (_isExport)
                    btnExportToExcel.Visible = true;

                int PrinterType = Convert.ToInt32(ddlPrinterType.SelectedValue);
                Guid StockStatus = Guid.Parse(ddlStockStatus.SelectedValue);
                Guid BranchId = Guid.Parse(ddlPrinter.SelectedValue);

                list = _master.GetStockAssignedTo(PrinterType, StockStatus, BranchId);
                grdStock.DataSource = list;
                grdStock.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdStock_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdStock.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGrid();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _siteId = Master.SiteID;
            _isExport = true;
            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("StockAssignedTo.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            _siteId = Master.SiteID;
            BindGrid();
            MemoryStream MyMemoryStream = new MemoryStream();
            XLWorkbook wb = new XLWorkbook();

            wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.LeftBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.TopBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));
            wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ccc"));

            var ws1 = wb.Worksheets.Add("Stock Assigned To Report");
            ws1.RowHeight = 20;
            ws1.Style.Font.FontSize = 10;

            #region Stock Assigned To Report

            if (list != null && list.Count > 0)
            {
                var STOCKNO = ws1.Cell("A1").SetValue("Stock Number");
                var STATUS = ws1.Cell("B1").SetValue("Status");
                var PRINTERLOCATION = ws1.Cell("C1").SetValue("Printer Location");

                ws1.Range("A1:C1").Row(1).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                ws1.Range("A1:C1").Row(1).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#00aeef"));
                ws1.Range("A1:C1").Row(1).Style.Font.FontColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                ws1.Range("A1:C1").Row(1).Style.Font.SetBold(true);
                int x = 2;
                foreach (var item in list)
                {
                    STOCKNO = STOCKNO.CellBelow().SetValue(item.STOCKNO);
                    STATUS = STATUS.CellBelow().SetValue(item.STATUS);
                    PRINTERLOCATION = PRINTERLOCATION.CellBelow().SetValue(item.PRINTERLOCATION);
                    if (x % 2 == 0)
                        ws1.Range("A1:C1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#a1e5ff"));
                    else
                        ws1.Range("A1:C1").Row(x).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ececec"));
                    ws1.Range("A1:C1").Row(x).Style.Border.BottomBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                    ws1.Range("A1:C1").Row(x).Style.Border.RightBorderColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#ffffff"));
                    x++;
                }
                ws1.Columns().AdjustToContents();
            }
            #endregion

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", ddlPrinterType.SelectedItem.Text+" Stock Assigned To Report"));
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
    }
}