﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="StockAssignedTo.aspx.cs" Inherits="IR_Admin.Reports.StockAssignedTo"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .BindGridScrolle {
            float: left;
            width: 100%;
            height: 100%;
            position: relative;
            overflow-x: scroll;
            border-radius: 7px;
        }
    </style>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Stock Assigned To Report</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="Panel1" runat="server">
            <div id="Div1" runat="server" class="success" style="display: none;">
                <asp:Label ID="Label1" runat="server" />
            </div>
            <div id="Div2" runat="server" class="error" style="display: none;">
                <asp:Label ID="Label2" runat="server" />
            </div>
        </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <br />
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px; padding: 0px 1px; font-size: 14px;">
                        <div class="pass-coloum-two" style="width: 730px; float: left; padding-bottom: 8px;">
                            Printer Type:
                            <asp:DropDownList runat="server" ID="ddlPrinterType" Style="margin-left: 5px; width: 36%;">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfv1" InitialValue="0" runat="server" ErrorMessage="*" Display="Static"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="ddlPrinterType" />
                            Stock Status:
                             <asp:DropDownList runat="server" ID="ddlStockStatus" Style="margin-left: 5px; width: 36%;">
                             </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfv2" InitialValue="0" runat="server" ErrorMessage="*" Display="Static"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="ddlStockStatus" />
                        </div>
                        <div style="float: right">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                OnClick="btnSubmit_Click" ValidationGroup="rv" />
                            <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Reset" Width="89px"
                                CausesValidation="false" OnClick="btnCancel_Click" />
                        </div>
                        <div style="margin-top: 38px;">
                            Printer Location:
                            <asp:DropDownList runat="server" ID="ddlPrinter" Style="margin-left: 5px; width: 63%; height: 28px;">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfv3" InitialValue="0" runat="server" ErrorMessage="*" Display="Static"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="ddlPrinter" />
                            <div style="float: right">
                                <asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                                    data-export="export" Width="206px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                    Visible="False" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div id="divfirst" class="grid-head2 tabdiv" style="height: auto;">
                        <asp:GridView ID="grdStock" runat="server" AutoGenerateColumns="False" PageSize="50"
                            CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                            AllowPaging="True" OnPageIndexChanging="grdStock_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Stock Number">
                                    <ItemTemplate>
                                        <%#Eval("STOCKNO")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <%#Eval("STATUS")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Printer Location">
                                    <ItemTemplate>
                                        <%# Eval("PRINTERLOCATION")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
