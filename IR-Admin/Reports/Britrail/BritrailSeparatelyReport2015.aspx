﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="BritrailSeparatelyReport2015.aspx.cs" Inherits="IR_Admin.Reports.Britrail.BritrailSeparatelyReport2015"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .dvMain
        {
            width: 980px;
        }
        .tabdiv
        {
            display: none;
        }
        .BindGridScrolle
        {
            float: left;
            width: 100%;
            height: 100%;
            border-radius: 7px;
        }
        .rightside
        {
            text-align: right;
        }
        .Alternet
        {
            background-color: #a1e5ff;
            border: 1px solid #FFF;
        }
        .Alternetnot
        {
            background-color: #ECECEC;
        }
        .Totlecss
        {
            background-color: #9E9E9E;
        }
        th
        {
            text-align: center !important;
        }
        th, td
        {
            font-size: 13px;
            padding:0px 7px 0px 7px;
            height: 40px;
        }
        .tabmenu ul
        {
            padding: 0px;
            margin: 0px;
        }
        .tabmenu ul li
        {
            padding: 5px 5px 5px 5px;
            float: left;
            list-style: none;
            margin: 0px 3px 0 0;
            background: #F1F1F1;
            border-left: 1px solid #00aeef;
            border-right: 1px solid #00aeef;
            border-top: 1px solid #00aeef;
            position: relative;
            border-radius: 8px 8px 0 0;
            -o-border-radius: 8px 8px 0 0;
            -ms-border-radius: 8px 8px 0 0;
            -webkit-border-radius: 8px 8px 0 0;
            -moz-border-radius: 8px 8px 0 0;
            behavior: url(PIE.htc);
            cursor: pointer;
        }
        .tabmenu ul li:hover, .tabmenu ul li.active
        {
            background: #DADADA;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        BritRail Report 2015</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="dvMain">
        <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
            padding: 0px 1px; font-size: 14px; margin-bottom:10px">
            <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
            <div style="float: right">
                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                    OnClick="btnSubmit_Click" />
                <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Reset" Width="89px"
                    CausesValidation="false" OnClick="btnReset_Click" />
            </div>
            <div class="pass-coloum-two" style="width: 315px; float: left;">
                Month:
                <asp:DropDownList ID="ddlMonth" runat="server" />
            </div>
            <div class="pass-coloum-two" style="width: 300px; float: left;">
                Year:
                <asp:DropDownList ID="ddlYear" runat="server" />
            </div>
            <div style="float: right; padding-top: 35px; width: 0px;">
                <asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                    data-export="export" Width="203px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                    Visible="false" />
            </div>
        </div>
        <div class="BindGridScrolle" runat="server" visible="false" id="datadiv" >
            <div class="tabmenu">
                <ul>
                    <li onclick="ActiveMenu('first')" id="first" class="active">BritRail Settlement Report</li>
                    <li onclick="ActiveMenu('second')" id="second">Britrail End Of Month Sale Report</li>
                    <li onclick="ActiveMenu('third')" id="third">BritRail Pass Sale Report</li>
                </ul>
            </div>
            <br />
            <div id="divfirst" class="tabdiv">
                <asp:GridView ID="GrdSettlement" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                    CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="false" OnRowDataBound="GrdSettlement_RowDataBound">
                    <PagerStyle CssClass="paging"></PagerStyle>
                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#00aeef" />
                    <RowStyle HorizontalAlign="Left" ForeColor="Black" />
                    <EmptyDataRowStyle HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        Record not found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%#Eval("Title")%>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lbldata" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                </asp:GridView>
            </div>
            <div id="divsecond" class="tabdiv" style="overflow-x: scroll; width: 100%;">
                <asp:GridView ID="grdSummary" runat="server" AutoGenerateColumns="False" PageSize="50"
                    CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="130%"
                    AllowPaging="false" OnPageIndexChanging="grdSummary_PageIndexChanging" OnRowDataBound="grdSummary_RowDataBound">
                    <PagerStyle CssClass="paging"></PagerStyle>
                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#00aeef" />
                    <RowStyle HorizontalAlign="Left" ForeColor="Black" />
                    <EmptyDataRowStyle HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        Record not found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <%#Eval("Continent")%>
                            </ItemTemplate>
                            <ItemStyle Width="15%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <%# Eval("Categoryname")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Units">
                            <ItemTemplate>
                                <%#Eval("Units")%>
                            </ItemTemplate>
                            <ItemStyle CssClass="rightside" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Gross Sales">
                            <ItemTemplate>
                                <%#Eval("Price")%>
                            </ItemTemplate>
                            <ItemStyle CssClass="rightside" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Commission 9.00%">
                            <ItemTemplate>
                                <%#Eval("Commission")%>
                            </ItemTemplate>
                            <ItemStyle CssClass="rightside" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NET Portion ITX">
                            <ItemTemplate>
                                <%#Eval("Isitxpass")%>
                            </ItemTemplate>
                            <ItemStyle CssClass="rightside" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Net Amount Due to RSP">
                            <ItemTemplate>
                                <%#Eval("Netamount")%>
                            </ItemTemplate>
                            <ItemStyle CssClass="rightside" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                </asp:GridView>
            </div>
            <div id="divthird" class="tabdiv">
                <asp:GridView ID="grdPassSale" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    AllowPaging="true" PageSize="50" ForeColor="#333333" GridLines="None" Width="100%"
                    OnPageIndexChanging="grdPassSale_PageIndexChanging">
                    <AlternatingRowStyle BackColor="#a1e5ff" />
                    <PagerStyle CssClass="paging"></PagerStyle>
                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#00aeef" />
                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                        BorderColor="#FFFFFF" BorderWidth="1px" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <EmptyDataRowStyle HorizontalAlign="Left" />
                    <EmptyDataTemplate>
                        Record not found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Code">
                            <ItemTemplate>
                                <%#Eval("PassTypeCode")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pass Definition/Description">
                            <ItemTemplate>
                                <%#Eval("PassDefinition")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <ItemTemplate>
                                <%# Eval("Class")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Duration">
                            <ItemTemplate>
                                <%#Eval("Duration")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <%#Eval("TType")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Units">
                            <ItemTemplate>
                                <%#Eval("Units")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ITX/BIP">
                            <ItemTemplate>
                                <%# (Convert.ToBoolean(Eval("ITXBIP"))==true?"Yes":"No")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total GBB">
                            <ItemTemplate>
                                <%# Eval("Total", "{0:0.00}") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnlastactive" />
    <script src="../../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        ActiveMenu('first');
        function ActiveMenu(obj) {
            switch (obj) {
                case 'first':
                    $('.tabmenu ul li').removeClass('active');
                    $(".tabdiv").hide();
                    $('#' + obj).addClass('active');
                    $('#div' + obj).show();
                    break;
                case 'second':
                    $('.tabmenu ul li').removeClass('active');
                    $(".tabdiv").hide();
                    $('#' + obj).addClass('active');
                    $('#div' + obj).show();
                    break;
                case 'third':
                    $('.tabmenu ul li').removeClass('active');
                    $(".tabdiv").hide();
                    $('#' + obj).addClass('active');
                    $('#div' + obj).show();
                    break;
            }
            $("#hdnlastactive").val(obj);
            console.log($("#hdnlastactive").val() + "; " + obj);
        }
    </script>
</asp:Content>
