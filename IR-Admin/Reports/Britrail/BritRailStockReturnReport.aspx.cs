﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using ClosedXML.Excel;

namespace IR_Admin.Reports.Britrail
{
    public partial class BritRailStockReturnReport : Page
    {
        public string currency = "$";
        private bool _isExport;
        readonly private BritrailReport _Britrail = new BritrailReport();
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        public List<BritrailStockReturnReports> list = new List<BritrailStockReturnReports>();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteId = Guid.Parse(selectedValue);
            BindGrid();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PageloadEvent();
                txtStartDate.Text = (d1.ToString("dd/MM/yyyy").Replace("-","/"));
                txtLastDate.Text = (d2.ToString("dd/MM/yyyy").Replace("-","/"));
            }
        }

        public void PageloadEvent()
        {
            try
            {
                var data = _Britrail.GetBritrailPrintqueue(AdminuserInfo.UserID);
                chklstPrintQueue.DataSource = data;
                chklstPrintQueue.DataValueField = "Id";
                chklstPrintQueue.DataTextField = "Queuelocation";
                chklstPrintQueue.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindGrid()
        {
            try
            {
                bool SelectQueue = (chklstPrintQueue.Items.Cast<ListItem>()).Any(li => li.Selected);
                if (!SelectQueue)
                {
                    ShowMessage(2, "Please select any print queues.");
                    return;
                }
                else
                {
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    if (_isExport)
                        btnExportToExcel.Visible = true;
                    if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
                    {
                        d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                    }
                    var selected = chklstPrintQueue.Items.Cast<ListItem>().Where(li => li.Selected).Select(x => x.Value).ToList();
                    string Queueids = string.Join(",", selected);
                    list = _Britrail.BritRailStockReturnReport(Queueids, d1, d2);
                    grdStock.DataSource = list;
                    grdStock.DataBind();
                }
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void grdStock_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdStock.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _isExport = true;
            BindGrid();
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            grdStock.AllowPaging = false;
            BindGrid();
            if (grdStock.Rows.Count > 50000)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('Report is too large and may not be possible to be Exported to Excel, Please select shorter period.')", true);
            }
            else
            {
                MemoryStream MyMemoryStream = new MemoryStream();
                XLWorkbook wb = new XLWorkbook();
                var LineColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B4CADC"));
                wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.LeftBorderColor = LineColor;
                wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.RightBorderColor = LineColor;
                wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.TopBorderColor = LineColor;
                wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                wb.Style.Border.BottomBorderColor = LineColor;
                var ws3 = wb.Worksheets.Add("Britrail Stock Return Report");

                #region Britrail Settlement Report
                if (list != null && list.Count > 1)
                {
                    var StockNumber = ws3.Cell("A1");
                    var OrderId = ws3.Cell("B1");
                    var Status = ws3.Cell("C1");
                    var QueueLocation = ws3.Cell("D1");
                    var PrintedDate = ws3.Cell("E1");
                    var PrintedTime = ws3.Cell("F1");
                    var PrintedBy = ws3.Cell("G1");
                    var RefundedDate = ws3.Cell("H1");
                    var RefundedTime = ws3.Cell("I1");
                    var RefundedBy = ws3.Cell("J1");
                    var PassemgerName = ws3.Cell("K1");
                    var ProductName = ws3.Cell("L1");

                    StockNumber = StockNumber.SetValue("Britrail Stock Return Report").CellBelow().CellBelow();
                    OrderId = OrderId.CellBelow().CellBelow();

                    ws3.Cell(1, 1).Style.Font.Bold = true;
                    ws3.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                    ws3.Cell(1, 1).Style.Font.FontSize = 16;

                    for (int o = 1; o <= 2; o++)
                    {
                        ws3.Cell(3, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#FF822D"));
                        ws3.Cell(4, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B7D2E9"));
                        ws3.Cell(5, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#D7E7F6"));
                        ws3.Cell(6, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B7D2E9"));
                    }
                   
                    StockNumber = StockNumber.CellBelow().SetValue("From");
                    StockNumber = StockNumber.CellBelow().SetValue("To");
                    StockNumber = StockNumber.CellBelow().SetValue("Number of coupons");
                    //OrderId = OrderId.CellBelow().SetValue<string>(Convert.ToString(d1.ToString("dd/MMM/yy")));
                    //OrderId = OrderId.CellBelow().SetValue<string>(Convert.ToString(d2.ToString("dd/MMM/yy")));
                    //OrderId = OrderId.CellBelow().SetValue<string>(Convert.ToString(list.Count));
                    OrderId = OrderId.CellBelow().SetValue(d1.Date);
                    OrderId = OrderId.CellBelow().SetValue(d2.Date);
                    OrderId = OrderId.CellBelow().SetValue(list.Count);

                    StockNumber = StockNumber.CellBelow().CellBelow();
                    OrderId = OrderId.CellBelow().CellBelow();
                    Status = Status.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    QueueLocation = QueueLocation.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    PrintedDate = PrintedDate.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    PrintedTime = PrintedTime.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    PrintedBy = PrintedBy.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    RefundedDate = RefundedDate.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    RefundedTime = RefundedTime.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    RefundedBy = RefundedBy.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    PassemgerName = PassemgerName.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();
                    ProductName = ProductName.CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow().CellBelow();

                    StockNumber = StockNumber.SetValue("Stock number");
                    OrderId = OrderId.SetValue("Order number");
                    Status = Status.SetValue("Status");
                    QueueLocation = QueueLocation.SetValue("Print queue");
                    PrintedDate = PrintedDate.SetValue("Date printed");
                    PrintedTime = PrintedTime.SetValue("Time printed");
                    PrintedBy = PrintedBy.SetValue("Printed by");
                    RefundedDate = RefundedDate.SetValue("Date refunded");
                    RefundedTime = RefundedTime.SetValue("Time refunded");
                    RefundedBy = RefundedBy.SetValue("Refunded by");
                    PassemgerName = PassemgerName.SetValue("Passenger name");
                    ProductName = ProductName.SetValue("Product name");

                    foreach (var item in list)
                    {
                        StockNumber = StockNumber.CellBelow().SetValue(item.StockNumber);
                        OrderId = OrderId.CellBelow().SetValue(item.OrderId);
                        Status = Status.CellBelow().SetValue(item.Status);
                        QueueLocation = QueueLocation.CellBelow().SetValue(item.QueueLocation);
                        PrintedDate = PrintedDate.CellBelow().SetValue(item.PrintedDate);
                        PrintedTime = PrintedTime.CellBelow().SetValue(item.PrintedTime);
                        PrintedBy = PrintedBy.CellBelow().SetValue(item.PrintedBy);
                        RefundedDate = RefundedDate.CellBelow().SetValue(item.RefundedDate);
                        RefundedTime = RefundedTime.CellBelow().SetValue(item.RefundedTime);
                        RefundedBy = RefundedBy.CellBelow().SetValue(item.RefundedBy);
                        PassemgerName = PassemgerName.CellBelow().SetValue(item.PassemgerName);
                        ProductName = ProductName.CellBelow().SetValue(item.ProductName);
                    }

                    int Awidth = 25;
                    int Bwidth = 70;
                    ws3.Column(1).Width = Awidth;
                    ws3.Column(2).Width = Awidth;
                    ws3.Column(3).Width = Awidth;
                    ws3.Column(4).Width = Bwidth;
                    ws3.Column(5).Width = Awidth;
                    ws3.Column(6).Width = Awidth;
                    ws3.Column(7).Width = Awidth;
                    ws3.Column(8).Width = Awidth;
                    ws3.Column(9).Width = Awidth;
                    ws3.Column(10).Width = Awidth;
                    ws3.Column(11).Width = 50;
                    ws3.Column(12).Width = Bwidth;
                    for (int k = 1; k <= list.Count; k++)
                        for (int o = 1; o <= 12; o++)
                        {
                            if (k % 2 == 0)
                                ws3.Cell(k + 8, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#D7E7F6"));
                            else
                                ws3.Cell(k + 8, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B7D2E9"));
                        }
                    for (int o = 1; o <= 12; o++)
                    {
                        ws3.Cell(8, o).Style.Font.Bold = true;
                        ws3.Cell(8, o).Style.Font.FontColor = XLColor.Black;
                        ws3.Cell(8, o).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#FF822D"));
                    }
                }
                #endregion

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "BritRailStockReturnReport"));
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("BritRailStockReturnReport.aspx");
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}