﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;

namespace IR_Admin.Reports.Britrail
{
    public partial class PassSale : Page
    {
        readonly Masters _Master = new Masters();
        readonly BritrailReport _Britrail = new BritrailReport();
        readonly private db_1TrackEntities db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;
        public string currency = "£";
        private bool _isExport;
        DateTime d1 = DateTime.Now.AddDays(-30);
        DateTime d2 = DateTime.Now;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
            BindGrid();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void BindGrid()
        {
            try{
            if (_isExport)
                btnExportToExcel.Visible = true;
            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
            {
                d1 = DateTime.ParseExact((txtStartDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                d2 = DateTime.ParseExact((txtLastDate.Text), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            #region Advanced search filter
            string PassCode = "";
            string Defination = "";
            string Duration = "";
            string Type = "";
            string Category = "";
            string Class = "";

            if (rdbCategory.Checked)
                Category = txtTextSearch.Text.Trim();
            if (rdbPassCode.Checked)
                PassCode = txtTextSearch.Text.Trim();
            else if (rdbPass.Checked)
                Defination = txtTextSearch.Text.Trim();
            else if (rdbDuration.Checked)
                Duration = txtTextSearch.Text.Trim();
            else if (rdbType.Checked)
                Type = txtTextSearch.Text.Trim();
            else if (rdbClass.Checked)
                Class = txtTextSearch.Text.Trim();
            #endregion

            Int32 SiteFilter = 3;

            var list = _Britrail.GetBritrailPassSale(Guid.Empty, d1, d2, SiteFilter, Category, PassCode, Defination, Class, Duration, Type);
            grdBritRail.DataSource = list;
            grdBritRail.DataBind(); }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void grdBritRail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdBritRail.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            _isExport = true;
            BindGrid();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("PassSale.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            grdBritRail.AllowPaging = false;
            BindGrid();
            grdBritRail.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#B7D2E9");
            grdBritRail.RowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#D7E7F6");
            if (grdBritRail.Rows.Count > 50000)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('Report is too large and may not be possible to be Exported to Excel, Please select shorter period.')", true);
            }
            else
            {
                grdBritRail.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#5395CF");
                grdBritRail.HeaderStyle.ForeColor = System.Drawing.Color.White;
                Context.Response.ClearContent();
                Context.Response.ContentType = "application/ms-excel";
                Context.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xls", "BritRailPassSale"));
                Context.Response.Charset = "";
                var stringwriter = new System.IO.StringWriter();
                var htmlwriter = new HtmlTextWriter(stringwriter);
                grdBritRail.RenderControl(htmlwriter);
                Context.Response.Write(stringwriter.ToString());
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}