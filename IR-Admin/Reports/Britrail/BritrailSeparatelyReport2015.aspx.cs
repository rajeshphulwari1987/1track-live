﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using ClosedXML.Excel;

namespace IR_Admin.Reports.Britrail
{
    public partial class BritrailSeparatelyReport2015 : Page
    {
        public string currency = "£";
        private bool hold = false;
        readonly BritrailReport _Britrail = new BritrailReport();
        readonly private db_1TrackEntities db = new db_1TrackEntities();
        public List<BritrailSettlement> BritrailSettlementList = new List<BritrailSettlement>();
        public List<Britrailsummaryreport> BritrailEOMList = new List<Britrailsummaryreport>();
        public List<BritrailPassSale> BritrailPassSaleList = new List<BritrailPassSale>();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteId;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteId = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillDropDown();
                _siteId = Master.SiteID;
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    dvSearch.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void FillDropDown()
        {
            for (int i = 1; i < 13; i++)
                ddlMonth.Items.Add(new ListItem { Value = i.ToString(), Text = "(" + i + ") " + System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
            ddlYear.Items.Add(new ListItem { Value = "2015", Text = "2015" });
        }

        #region Britrail Settlement Report
        void BindGridSettlement()
        {
            try
            {
                var list = new List<BritrailSettlement>();
                string month = ("0" + ddlMonth.SelectedValue);
                month = month.Substring(month.Length - 2, 2);
                string year = ddlYear.SelectedValue;
                string thedate = month + "/01" + "/" + year + " 00:00:00";
                DateTime monthstart = DateTime.Parse(thedate);
                DateTime monthend = monthstart.AddMonths(1).AddDays(-1);
                Int32 SiteFilter = 3;
                CultureInfo usEnglish = new CultureInfo("en-US");
                DateTimeFormatInfo englishInfo = usEnglish.DateTimeFormat;
                string monthName = englishInfo.MonthNames[Convert.ToInt32(month) - 1];
                _siteId = Master.SiteID;
                var data = db.tblSites.FirstOrDefault(t => t.ID == _siteId);
                list.Add(new BritrailSettlement { Id = 100, Title = "To:  Rail Settlement Plan Ltd." });
                list.Add(new BritrailSettlement { Id = 99, Title = "cc: ATOC", Other = "Fax number:  " + (data != null ? data.PhoneNumber : "011-44-207-841-8262") });
                list.Add(new BritrailSettlement { Id = 98, Title = "BRITRAIL SETTLEMENT FOR THE MONTH OF:", Other = monthName + " (" + ddlYear.SelectedValue + ")" });
                list.AddRange(_Britrail.GetBritrailSettlementReport(monthstart, monthend, SiteFilter, _siteId));
                list.Add(new BritrailSettlement { Id = 97, Title = "For any questions regarding the payment, please do not hesitate to contact - Agent Accounts Payable contact." });
                GrdSettlement.DataSource = BritrailSettlementList = list;
                GrdSettlement.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");

            }
        }

        protected void GrdSettlement_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.CssClass = "Alternet";
                e.Row.Cells[1].Attributes.Add("style", "text-align: right;padding-right: 24%;");
                var lbldata = e.Row.FindControl("lbldata") as Label;
                int Id = ((Business.BritrailSettlement)(e.Row.DataItem)).Id;
                string Other = ((Business.BritrailSettlement)(e.Row.DataItem)).Other;
                decimal Amount = ((Business.BritrailSettlement)(e.Row.DataItem)).Amount;
                if (Id == 98 || Id == 101 || Id == 102)
                {
                    e.Row.Font.Bold = true;
                }
                if (97 <= Id && 101 >= Id)
                    lbldata.Text = Other;
                else
                    lbldata.Text = Amount.ToString();
                if (Id == 97)
                {
                    e.Row.Cells[0].ColumnSpan = 2;
                    e.Row.Cells[1].Attributes.Add("style", "display:none;");
                }
                if (Id == 102)
                    e.Row.Attributes.Add("style", "border-top:2px solid;border-bottom:2px solid;");
            }
        }
        #endregion

        #region Brintrail EOM Report
        void BindGridEOM()
        {
            try
            {
                string month = ("0" + ddlMonth.SelectedValue);
                month = month.Substring(month.Length - 2, 2);
                string year = ddlYear.SelectedValue;
                string thedate = month + "/01" + "/" + year + " 00:00:00";
                DateTime monthstart = DateTime.Parse(thedate);
                DateTime monthend = monthstart.AddMonths(1).AddDays(-1);
                Int32 SiteFilter = 3;
                _siteId = Master.SiteID;
                var list = _Britrail.GetBritrailsummaryreport(monthstart, monthend, SiteFilter, _siteId);
                grdSummary.DataSource = BritrailEOMList = (list.Count > 1 ? list : null);
                grdSummary.DataBind();
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");

            }
        }

        protected void grdSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSummary.PageIndex = e.NewPageIndex;
            BindGridEOM();
        }

        protected void grdSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Categoryname = ((Business.Britrailsummaryreport)(e.Row.DataItem)).CATEGORYNAME;
                if (Categoryname == "Total Sales" || Categoryname == "Total Refunds" || Categoryname == "Sales Less Refunds")
                {
                    e.Row.Font.Bold = true;
                }
                int Filter = (int)((Business.Britrailsummaryreport)(e.Row.DataItem)).FILTER;
                if (Filter == 1)
                    hold = !hold;
                if (hold)
                    e.Row.CssClass = "Alternet";
                else
                    e.Row.CssClass = "Alternetnot";
                if (Filter == 0)
                    e.Row.CssClass = "Totlecss";
                if (Categoryname == "Sales Less Refunds")
                    e.Row.Attributes.Add("style", "border-top:1px solid #111;border-bottom:2px solid #111");
            }
        }

        #endregion

        #region Britrail PassSale Report
        void BindGridPassSale()
        {
            string month = ("0" + ddlMonth.SelectedValue);
            month = month.Substring(month.Length - 2, 2);
            string year = ddlYear.SelectedValue;
            string thedate = "01/" + month + "/"+year;
            DateTime monthstart = DateTime.Parse(thedate);
            DateTime monthend = monthstart.AddMonths(1).AddDays(-1);
            Int32 SiteFilter = 3;
            var list = _Britrail.GetBritrailPassSale(Guid.Empty, monthstart, monthend, SiteFilter, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
            grdPassSale.DataSource = BritrailPassSaleList = list;
            grdPassSale.DataBind();
        }

        protected void grdPassSale_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPassSale.PageIndex = e.NewPageIndex;
            _siteId = Master.SiteID;
            BindGridPassSale();
            ScriptManager.RegisterStartupScript(Page, GetType(), "tabheader", ";ActiveMenu('" + hdnlastactive.Value + "')", true);
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try{
            datadiv.Visible = true;
            BindGridSettlement();
            BindGridEOM();
            BindGridPassSale();
            btnExportToExcel.Visible = true;
            ScriptManager.RegisterStartupScript(Page, GetType(), "tabheader", ";ActiveMenu('" + hdnlastactive.Value + "')", true);
            }
            catch (Exception exp)
            {
                ShowMessage(2, "Operation has been terminated! Please choose start date and end date between 1 or 2 months time period.");
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        public void ExportToExcel()
        {
            BindGridSettlement();
            BindGridEOM();
            BindGridPassSale();
            /********************/
            MemoryStream MyMemoryStream = new MemoryStream();
            XLWorkbook wb = new XLWorkbook();

            wb.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.LeftBorderColor = XLColor.Black;
            wb.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.RightBorderColor = XLColor.Black;
            wb.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.TopBorderColor = XLColor.Black;
            wb.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            wb.Style.Border.BottomBorderColor = XLColor.Black;
            wb.Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#27B2FF"));

            var ws1 = wb.Worksheets.Add("Britrail Settlement Report");
            #region Britrail Settlement Report
            if (BritrailSettlementList != null && BritrailSettlementList.Count > 1)
            {
                var Title = ws1.Cell("A1");
                var Name = ws1.Cell("B1");
                foreach (var item in BritrailSettlementList)
                {
                    if (item.Id == 98 || item.Id == 101 || item.Id == 102)
                    {
                        Title = Title.CellBelow().SetValue(item.Title);
                        Title.Style.Font.Bold = true;
                    }
                    else if (item.Title == "Total Gross Sales<BR/>(NON-ITX + ITX Passes)")
                        Title = Title.CellBelow().SetValue("Total Gross Sales").CellBelow().SetValue("(NON-ITX + ITX Passes)");
                    else
                        Title = Title.CellBelow().SetValue(item.Title);

                    if (97 <= item.Id && 101 >= item.Id)
                    {
                        if (item.Id == 98 || item.Id == 101)
                        {
                            Name = Name.CellBelow().SetValue(item.Other);
                            Name.Style.Font.Bold = true;
                        }
                        else
                            Name = Name.CellBelow().SetValue(item.Other);
                    }
                    else if (item.Id == 102)
                    {
                        Name = Name.CellBelow().SetValue(item.Amount);
                        Name.Style.Font.Bold = true;
                    }
                    else if (item.Title == "Total Gross Sales<BR/>(NON-ITX + ITX Passes)")
                        Name = Name.CellBelow().SetValue(item.Amount).CellBelow().SetValue("");
                    else
                        Name = Name.CellBelow().SetValue(item.Amount);
                }
                var cols1 = ws1.Column(1);
                cols1.Width = 55;
                var cols2 = ws1.Column(2);
                cols2.Width = 30;
                //for (int i = 1; i < BritrailSettlementList.Count + 1; i++)
                //{
                //    var cols = ws.Column(1);
                //    cols.Width = 70;
                //}
                for (int col = 1; col < 20; col++)
                {
                    ws1.Cell(1, col).Style.Font.Bold = true;
                    ws1.Cell(1, col).Style.Font.FontColor = XLColor.White;
                    ws1.Cell(1, col).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B53859"));
                }
            }
            #endregion

            var ws2 = wb.Worksheets.Add("Britrail EOM Report");
            #region Britrail EOM Report
            if (BritrailEOMList != null && BritrailEOMList.Count > 1)
            {
                var Continent = ws2.Cell("A1");
                Continent = Continent.SetValue("");
                var Categoryname = ws2.Cell("B1");
                Categoryname = Categoryname.SetValue("");
                var Units = ws2.Cell("C1");
                Units = Units.SetValue("Units");
                var Price = ws2.Cell("D1");
                Price = Price.SetValue("Total Gross Sales");
                var Commission = ws2.Cell("E1");
                Commission = Commission.SetValue("Commission 9.00%");
                var Isitxpass = ws2.Cell("F1");
                Isitxpass = Isitxpass.SetValue("NET Portion ITX");
                var Netamount = ws2.Cell("G1");
                Netamount = Netamount.SetValue("Net Amount Due to RSP");
                foreach (var item in BritrailEOMList)
                {
                    if (item.CATEGORYNAME == "Total Sales" || item.CATEGORYNAME == "Total Refunds" || item.CATEGORYNAME == "Sales Less Refunds")
                    {
                        Continent = Continent.CellBelow().SetValue(item.CONTINENT);
                        Continent.Style.Font.Bold = true;
                        Categoryname = Categoryname.CellBelow().SetValue(item.CATEGORYNAME);
                        Categoryname.Style.Font.Bold = true;
                        Units = Units.CellBelow().SetValue(item.UNITS);
                        Units.Style.Font.Bold = true;
                        Price = Price.CellBelow().SetValue(item.PRICE);
                        Price.Style.Font.Bold = true;
                        Commission = Commission.CellBelow().SetValue(item.COMMISSION);
                        Commission.Style.Font.Bold = true;
                        Isitxpass = Isitxpass.CellBelow().SetValue(Convert.ToDecimal(item.ISITXPASS));
                        Isitxpass.Style.Font.Bold = true;
                        Netamount = Netamount.CellBelow().SetValue(item.NETAMOUNT);
                        Netamount.Style.Font.Bold = true;
                    }
                    else
                    {
                        Continent = Continent.CellBelow().SetValue(item.CONTINENT);
                        Categoryname = Categoryname.CellBelow().SetValue(item.CATEGORYNAME);
                        Units = Units.CellBelow().SetValue(item.UNITS);
                        Price = Price.CellBelow().SetValue(item.PRICE);
                        Commission = Commission.CellBelow().SetValue(item.COMMISSION);
                        Isitxpass = Isitxpass.CellBelow().SetValue(Convert.ToDecimal(item.ISITXPASS));
                        Netamount = Netamount.CellBelow().SetValue(item.NETAMOUNT);
                    }
                }
                var cols1 = ws2.Column(1);
                cols1.Width = 30;
                var cols2 = ws2.Column(2);
                cols2.Width = 30;
                var cols3 = ws2.Column(3);
                cols3.Width = 15;
                var cols4 = ws2.Column(4);
                cols4.Width = 15;
                var cols5 = ws2.Column(5);
                cols5.Width = 15;
                var cols6 = ws2.Column(6);
                cols6.Width = 15;
                var cols7 = ws2.Column(7);
                cols7.Width = 15;
                var cols8 = ws2.Column(8);
                cols8.Width = 20;
                for (int col = 1; col < 20; col++)
                {
                    ws2.Cell(1, col).Style.Font.Bold = true;
                    ws2.Cell(1, col).Style.Font.FontColor = XLColor.White;
                    ws2.Cell(1, col).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B53859"));
                }
            }
            #endregion

            var ws3 = wb.Worksheets.Add("Britrail Pass Sale Report");
            #region Britrail EOM Report
            if (BritrailPassSaleList != null && BritrailPassSaleList.Count > 0)
            {
                var PassTypeCode = ws3.Cell("A1");
                PassTypeCode = PassTypeCode.SetValue("Code");
                var PassDefinition = ws3.Cell("B1");
                PassDefinition = PassDefinition.SetValue("Pass Definition/Description");
                var Class = ws3.Cell("C1");
                Class = Class.SetValue("Class");
                var Duration = ws3.Cell("D1");
                Duration = Duration.SetValue("Duration");
                var TType = ws3.Cell("E1");
                TType = TType.SetValue("Type");
                var Units = ws3.Cell("F1");
                Units = Units.SetValue("Units");
                var ITXBIP = ws3.Cell("G1");
                ITXBIP = ITXBIP.SetValue("ITX/BIP");
                var Total = ws3.Cell("H1");
                Total = Total.SetValue("Total GBB");
                //var GeoArea = ws3.Cell("I1");
                //GeoArea = GeoArea.SetValue("Geo Area");
                foreach (var item in BritrailPassSaleList)
                {
                    PassTypeCode = PassTypeCode.CellBelow().SetValue(Convert.ToInt64(item.PASSTYPECODE.Trim()));
                    PassDefinition = PassDefinition.CellBelow().SetValue(item.PASSDEFINITION);
                    Class = Class.CellBelow().SetValue(item.CLASS);
                    Duration = Duration.CellBelow().SetValue(item.DURATION);
                    TType = TType.CellBelow().SetValue(item.TTYPE);
                    Units = Units.CellBelow().SetValue(item.UNITS);
                    ITXBIP = ITXBIP.CellBelow().SetValue(item.ITXBIP ? "Yes" : "No");
                    Total = Total.CellBelow().SetValue(item.TOTAL);
                    //GeoArea = GeoArea.CellBelow().SetValue(item.CONTINENT);
                }
                var cols1 = ws3.Column(1);
                cols1.Width = 15;
                var cols2 = ws3.Column(2);
                cols2.Width = 30;
                var cols3 = ws3.Column(3);
                cols3.Width = 15;
                var cols4 = ws3.Column(4);
                cols4.Width = 15;
                var cols5 = ws3.Column(5);
                cols5.Width = 15;
                var cols6 = ws3.Column(6);
                cols6.Width = 15;
                var cols7 = ws3.Column(7);
                cols7.Width = 15;
                var cols8 = ws3.Column(8);
                cols8.Width = 15;
                //var cols9 = ws3.Column(9);
                //cols9.Width = 15;
                for (int col = 1; col < 20; col++)
                {
                    ws3.Cell(1, col).Style.Font.Bold = true;
                    ws3.Cell(1, col).Style.Font.FontColor = XLColor.White;
                    ws3.Cell(1, col).Style.Fill.BackgroundColor = XLColor.FromColor(System.Drawing.ColorTranslator.FromHtml("#B53859"));
                }
            }
            #endregion

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", "BritrailReport2015"));
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlMonth.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            BindGridSettlement();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}
 