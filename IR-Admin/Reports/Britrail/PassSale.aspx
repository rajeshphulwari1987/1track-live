﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PassSale.aspx.cs"
    Inherits="IR_Admin.Reports.Britrail.PassSale" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }
        function showAdvancedSearch() {
            var img = $('#imgUpDown');
            var div = $('#divAdvancedSearch');
            div.slideDown(100, function () { img.attr('src', '../../images/ar-up.png'); });
            SearchCriteriaChanged($('#hidAdvanceSearchType').val());
        }
        function resetAdvancedSearch() {
            try {
                $('#rdbClass').removeAttr("checked");
                $('#rdbDuration').removeAttr("checked");
                $('#rdbType').removeAttr("checked");
                $('#rdbPassCode').removeAttr("checked");
                $('#rdbPass').removeAttr("checked");
                $('#rdbCategory').removeAttr("checked");
                $('#divAgentSearch').hide();
                $('#divTextSearch').hide();
                clearAdvanceSearch();
            }
            catch (ex) {
                alert(ex);
            }
        }
        function clearAdvanceSearch() {
            $("#ddlSiteAgents").val('0');
            $('#txtTextSearch').val('');
        }
        $(function () {
            if ($('#hidAdvancedSearchToggle').val() != '') {
                showAdvancedSearch();
            }
            $('#divExpandCollapseAdvanceSearch').click(function () {
                try {

                    var div = $('#divAdvancedSearch');
                    var img = $('#imgUpDown');
                    if (!div.is(':visible')) {
                        div.slideDown(100, function () { img.attr('src', '../../images/ar-up.png'); });
                        $('#hidAdvancedSearchToggle').val('show');
                    }
                    else {
                        div.slideUp(100);
                        img.attr('src', '../../images/ar-down.png');
                        $('#hidAdvancedSearchToggle').val('');
                        $('#hidAdvanceSearchType').val('');
                        resetAdvancedSearch();
                    }
                }
                catch (ex) {
                    alert(ex);
                    return false;
                }
            });
        });

        function SearchCriteriaChanged(obj) {
            if (obj.toString() != $('#hidAdvanceSearchType').val().toString()) {
                clearAdvanceSearch();
            }
            switch (obj) {
                case 'type':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Type : ');
                    $('#hidAdvanceSearchType').val('type');
                    break;
                case 'duration':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Duration : ');
                    $('#hidAdvanceSearchType').val('duration');
                    break;
                case 'passcode':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Pass Code : ');
                    $('#hidAdvanceSearchType').val('passcode');
                    break;
                case 'passdefination':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Pass Defination : ');
                    $('#hidAdvanceSearchType').val('pass');
                    break;
                case 'class':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Class : ');
                    $('#hidAdvanceSearchType').val('class');
                    break;
                case 'category':
                    showHideSearchCriteria('other');
                    $('#spnTextSearchLabel').text('Category Name: ');
                    $('#hidAdvanceSearchType').val('category');
                    break;
            }
        }
        function showHideSearchCriteria(obj) {
            switch (obj) {
                case 'agent':
                    $('#divAgentSearch').show();
                    $('#divTextSearch').hide();
                    break;
                case 'other':
                    $('#divTextSearch').show();
                    $('#divAgentSearch').hide();
                    break;
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hidAdvancedSearchToggle" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidAdvanceSearchType" runat="server" ClientIDMode="Static" />
    <h2>
        BritRail Pass Sale Report</h2>
    <div class="full mr-tp1">
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                         <div style="float: right;">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" runat="server" Width="85" OnClick="btnReset_Click"
                                    class="button" Text="Reset" />
                           <br />
                            <asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel" style="margin-top:5px;margin-left: 5px;"
                                data-export="export" Width="159px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input" ClientIDMode="Static"
                                Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                            <asp:RegularExpressionValidator ID="regx" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtStartDate" ValidationGroup="rv" ForeColor="Red" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static"
                                Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                            <asp:RegularExpressionValidator ID="regx2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtLastDate" ForeColor="Red" ValidationGroup="rv" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <%--Advanced Search panel starts here..--%>
                        <div class="collapse-tab">
                            <div id="divExpandCollapseAdvanceSearch" style="width: 938px; float: left; cursor: pointer;">
                                <img src="../../images/ar-down.png" id="imgUpDown" class="icon" />
                                <strong>Advanced Search </strong>
                            </div>
                            <div id="divAdvancedSearch" style="display: none;" class="innertb-content">
                                <div class="pass-coloum-two" style="width: 916px; float: left;">
                                    <asp:RadioButton ID="rdbCategory" runat="server" Text="Category Name" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('category')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbPassCode" runat="server" Text="Pass Code" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('passcode')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbPass" runat="server" Text="Pass Defination" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('passdefination')" ClientIDMode="Static" />
                                    <asp:RadioButton ID="rdbClass" runat="server" Text="Class" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('class')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbDuration" runat="server" Text="Duration" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('duration')" ClientIDMode="Static" />&nbsp;&nbsp;
                                    <asp:RadioButton ID="rdbType" runat="server" Text="Type" GroupName="AdvanceSearch"
                                        onclick="SearchCriteriaChanged('type')" ClientIDMode="Static" />&nbsp;&nbsp;
                                </div>
                                <div id="divTextSearch" class="pass-coloum-two" style="width: 962px; float: left;
                                    padding-bottom: 8px; padding-top: 10px; display: none">
                                    <label style="padding-left: 10px;">
                                        <div id="spnTextSearchLabel" style="width: 110px !important; float: left;">
                                        </div>
                                    </label>
                                    <asp:TextBox ID="txtTextSearch" runat="server" class="input" ClientIDMode="Static"
                                        MaxLength="50" Width="585px" />
                                </div>
                            </div>
                        </div>
                        <%--Advanced Search panel ends here..--%>
                    </div>
                    <div class="grid-head2" style="float: left; width: 100%; min-height: 180px; height: auto">
                        <asp:GridView ID="grdBritRail" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            AllowPaging="true" PageSize="50" ForeColor="#333333" GridLines="None" Width="100%"
                            OnPageIndexChanging="grdBritRail_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <%#Eval("PassTypeCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pass Definition">
                                    <ItemTemplate>
                                        <%#Eval("PassDefinition")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Class">
                                    <ItemTemplate>
                                        <%# Eval("Class")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration">
                                    <ItemTemplate>
                                        <%#Eval("Duration")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <%#Eval("TType")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Units">
                                    <ItemTemplate>
                                        <%#Eval("Units")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ITX/BIP">
                                    <ItemTemplate>
                                        <%# (Convert.ToBoolean(Eval("ITXBIP"))==true?"Yes":"No")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total GBB">
                                    <ItemTemplate>
                                        <%# Eval("Total", "{0:0.00}") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
