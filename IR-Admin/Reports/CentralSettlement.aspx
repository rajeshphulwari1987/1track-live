﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="CentralSettlement.aspx.cs" Inherits="IR_Admin.Reports.CentralSettlement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        .BindGridScrolle {
            float: left;
            width: 100%;
            height: 100%;
            position: relative;
            overflow-x: scroll;
            border-radius: 7px;
        }
    </style>
    <script type="text/javascript">
        function name(date) {
            var sptdate = String(date).split("/");
            var myDay = sptdate[0];
            var myMonth = sptdate[1];
            var myYear = sptdate[2];
            sdate = myMonth + '/' + myDay + '/' + myYear;
            return new Date(sdate);

        }
        function checkDate(sender) {
            var sdate = $("#txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');
            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }
        $(function () {
            $("#txtStartDate, #txtLastDate").bind("cut copy paste keydown", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Central Settlement</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px; padding: 0px 1px; font-size: 14px;">
                        <asp:HiddenField ID="hdnSiteId" ClientIDMode="Static" runat="server" />
                        <div style="height: 30px;" id="div_RoleVisible" runat="server">All STA site's record:
                            <asp:CheckBox runat="server" ID="chkStaSite" />
                        </div>
                        <div class="pass-coloum-two" style="width: 962px; float: left; padding-bottom: 8px;">
                            Office:
                           <asp:DropDownList runat="server" ID="ddlOffice" Width="310px"></asp:DropDownList>
                            Supplier:
                             <asp:DropDownList ID="ddlSupName" runat="server" Width="310px"> </asp:DropDownList>
                            <div style="float: right;">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px" OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" runat="server" Width="85" OnClick="btnCancel_Click" class="button" Text="Reset" />
                            </div>
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            StartDate:
                            <asp:TextBox ID="txtStartDate" runat="server" class="input"
                                ClientIDMode="Static" Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStartDate" />
                            <asp:RegularExpressionValidator ID="regx" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtStartDate" ValidationGroup="rv" ForeColor="Red" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div class="pass-coloum-two" style="width: 365px; float: left;">
                            LastDate:
                            <asp:TextBox ID="txtLastDate" runat="server" class="input"
                                ClientIDMode="Static" Width="253px" />
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLastDate"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalender2" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate" />
                            <asp:Image ID="imgCalender2" runat="server" ImageUrl="~/images/icon-calender.png"
                                BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -1px 2px 8px 5px" />
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtLastDate" />
                            <asp:RegularExpressionValidator ID="regx2" runat="server" ErrorMessage="*" Display="Dynamic"
                                ControlToValidate="txtLastDate" ForeColor="Red" ValidationGroup="rv" ValidationExpression="\d{2}/\d{2}/\d{4}" />
                        </div>
                        <div style="float: right; margin-right: 2px;">
                            &nbsp;<asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel" data-export="export"
                                Width="206px" nClick="btnExportToExcel_Click" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div>
                    </div>
                    <div class="BindGridScrolle">
                        <asp:GridView ID="grdCentralSettlement" runat="server" AutoGenerateColumns="False"
                            PageSize="50" CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None"
                            Width="300%" AllowPaging="True" OnPageIndexChanging="grdCentralSettlement_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#b53859" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Company">
                                    <ItemTemplate>
                                        <%#Eval("Company")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Stock Number">
                                    <ItemTemplate>
                                        <%#Eval("StockNumber")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agent Reference">
                                    <ItemTemplate>
                                        <%#Eval("AgentReference")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="InternationalRail Order">
                                    <ItemTemplate>
                                        <%#Eval("OrderNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Content">
                                    <ItemTemplate>
                                        <%#Eval("Content")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Class">
                                    <ItemTemplate>
                                        <%# Eval("Class")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Currency">
                                    <ItemTemplate>
                                        <%# Eval("SalesCurrency")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Price">
                                    <ItemTemplate>
                                        <%#Eval("SalesPrice", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement Amount In Sales Currency">
                                    <ItemTemplate>
                                        <%#Eval("SettlementAmountInSalesCurrency")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement Price">
                                    <ItemTemplate>
                                        <%#Eval("SettlementPrice", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Settlement Price In STA Euro">
                                    <ItemTemplate>
                                        <%#Eval("SettlementPriceInEuro", "{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Passenger Name">
                                    <ItemTemplate>
                                        <%#Eval("PassengerLastName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date printed">
                                    <ItemTemplate>
                                        <%#Eval("DatePrinted", "{0:dd-MMM-yy}")%>
                                    </ItemTemplate>
                                    <HeaderStyle Width="10%"></HeaderStyle>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pass">
                                    <ItemTemplate>
                                        <%#Eval("Pass")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sale Company">
                                    <ItemTemplate>
                                        <%#Eval("SaleCompany")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Printer Queue">
                                    <ItemTemplate>
                                        <%#Eval("PrinterCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Num Seniors">
                                    <ItemTemplate>
                                        <%#Eval("Seniors")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Num Adults">
                                    <ItemTemplate>
                                        <%#Eval("Adults")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Num Children">
                                    <ItemTemplate>
                                        <%#Eval("Children")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Num Youth">
                                    <ItemTemplate>
                                        <%#Eval("Youth")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refund Date">
                                    <ItemTemplate>
                                        <%#Eval("Refunddate")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier Name">
                                    <ItemTemplate>
                                        <%#Eval("SUPPLIERNAME")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
