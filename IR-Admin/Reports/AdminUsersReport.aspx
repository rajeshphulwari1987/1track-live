﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="AdminUsersReport.aspx.cs" Inherits="IR_Admin.Reports.AdminUsersReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Admin Users Report</h2>
    <div class="full mr-tp1"> 
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes1">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px;">
                        <div class="pass-coloum-two" style="width: 962px; float: left; padding-bottom: 8px;">
                            <span style="margin-right: 13px;">Site:</span>
                            <asp:DropDownList runat="server" ID="ddlRoles" Width="673px">
                            </asp:DropDownList>
                            <div style="float: right;">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" runat="server" Width="85" OnClick="btnCancel_Click"
                                    class="button" Text="Reset" />
                            </div>
                        </div>
                        <div style="float: right; margin-right: 2px;">
                            &nbsp;<asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                                data-export="export" Width="206px" OnClick="btnExportToExcel_Click"
                                Visible="False" />
                        </div> 
                    </div>
                    <div class="grid-head2" style="float: left; width: 100%; min-height: 180px; overflow-x: scroll;
                        overflow-y: hidden; height: auto">
                        <asp:GridView ID="grdAdminusers" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50"
                            ForeColor="#333333" GridLines="None" Width="150%"
                        AllowPaging="True" OnPageIndexChanging="grdAdminusers_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Left" />
                            <EmptyDataTemplate>
                                Record not found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="User Name">
                                    <ItemTemplate>
                                        <%#Eval("USERNAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <%#Eval("EMAILADDRESS")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("NAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Office Name">
                                    <ItemTemplate>
                                        <%# Eval("OFFICENAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role Name">
                                    <ItemTemplate>
                                        <%#Eval("ROLENAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Note">
                                    <ItemTemplate>
                                        <%#Eval("NOTE")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsAgent">
                                    <ItemTemplate>
                                        <%#Eval("ISAGENT")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsDeleted">
                                    <ItemTemplate>
                                        <%#Eval("ISDELETED")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Manual P2P">
                                    <ItemTemplate>
                                        <%#Eval("MENUALP2P")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Manual Pass">
                                    <ItemTemplate>
                                        <%#Eval("MANUALPASS")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SITENAME")%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
