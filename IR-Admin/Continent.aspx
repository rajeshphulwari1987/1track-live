﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Continent.aspx.cs" Inherits="IR_Admin.Continent" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
       
        $(function () {                  
            if(<%=tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }
        });   
       
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Continent</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="EurailCountry.aspx" class="current">List</a></li>
            <li><a id="aNew" href="EurailCountry.aspx" class="current">New/Edit</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grvContinent" runat="server" AutoGenerateColumns="False" PageSize="50"
                            AllowPaging="True" CssClass="grid-head2" CellPadding="4" ForeColor="#333333"
                            GridLines="None" Width="100%" OnPageIndexChanging="grvContinent_PageIndexChanging"
                            OnRowCommand="grvContinent_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="90%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="9%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField runat="server" ID="hdnId" />
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: Block;">
                    <div class="divMain">
                        <div class="divleft">
                            Name:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="150" />
                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName"
                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="vg" />
                        </div>
                        <div class="divleft">
                            Is Active?
                        </div>
                        <div class="divright">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="divleftbtn">
                            .
                        </div>
                        <div class="divrightbtn">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="vg" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
