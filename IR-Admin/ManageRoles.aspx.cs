﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data.SqlClient;
using System.Data;

namespace IR_Admin
{
    public partial class AddEditRoles : Page
    {
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        string AllKeys = string.Empty;
        public string tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                BindParentNavigation();
                BindRoles();
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    tab = "2";

                    RolesEdit(Guid.Parse(Request["edit"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        #region ControlEvents

        protected void repParent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblCategory = null;
                lblCategory = (Label)e.Item.FindControl("lblCategory");

                Label MainId = null;
                MainId = (Label)e.Item.FindControl("lblParentID");
                var ChildRepeater = (Repeater)e.Item.FindControl("repChild");

                ChildRepeater.DataSource = _master.GetParentNavigationList(Guid.Parse(MainId.Text.ToString())).OrderBy(x => x.Name).ToList();
                ChildRepeater.DataBind();
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (BtnSubmit.Text == "Submit")
            {
                //loop for parent Repeater
                for (int i = 0; i < repParent.Items.Count; i++)
                {
                    //Parent Repeater Permissions Checkbox
                    var chkPPermissions = (CheckBox)repParent.Items[i].FindControl("chkPPermissions");
                    if (chkPPermissions.Checked)
                    {
                        AllKeys += chkPPermissions.ToolTip.ToString() + ',';
                    }
                    //Parent Repeater visible Checkbox
                    var chkPVisible = (CheckBox)repParent.Items[i].FindControl("chkPVisible");
                    if (chkPVisible.Checked)
                    {
                        AllKeys += chkPVisible.ToolTip.ToString() + ',';
                    }

                    var ChildRepeater = (Repeater)repParent.Items[i].FindControl("repChild");
                    for (int j = 0; j < ChildRepeater.Items.Count; j++)
                    {
                        //Child Repeater Permissions Checkbox
                        var chkCPermissions = (CheckBox)ChildRepeater.Items[j].FindControl("chkCPermissions");
                        if (chkCPermissions.Checked)
                        {
                            AllKeys += chkCPermissions.ToolTip.ToString() + ',';
                        }

                        //Child Repeater Visible Checkbox
                        var chkCvisible = (CheckBox)ChildRepeater.Items[j].FindControl("chkCvisible");
                        if (chkCvisible.Checked)
                        {
                            AllKeys += chkCvisible.ToolTip.ToString() + ',';
                        }

                        var repInnerChild = (Repeater)ChildRepeater.Items[j].FindControl("repInnerChild");
                        if (repInnerChild != null && repInnerChild.Items.Count > 0)
                        {
                            for (int k = 0; k < repInnerChild.Items.Count; k++)
                            {
                                //Child Repeater Permissions Checkbox
                                var chkCInnerPermissions = (CheckBox)repInnerChild.Items[k].FindControl("chkCPermissions");
                                if (chkCInnerPermissions.Checked)
                                {
                                    AllKeys += chkCInnerPermissions.ToolTip.ToString() + ',';
                                }

                                //Child Repeater Visible Checkbox
                                var chkCInnervisible = (CheckBox)repInnerChild.Items[k].FindControl("chkCvisible");
                                if (chkCInnervisible.Checked)
                                {
                                    AllKeys += chkCInnervisible.ToolTip.ToString() + ',';
                                }
                            }
                        }
                    }
                }
                string Role = txtRoleName.Text.Trim();
                if (!Roles.RoleExists(Role))
                {
                    Roles.CreateRole(Role);
                    var roleId = _db.aspnet_Roles.Where(x => x.RoleName == Role).SingleOrDefault();

                    string[] GetallKeys = AllKeys.Split(',');
                    foreach (var item in GetallKeys)
                    {
                        if (item != "")
                        {
                            string MenuType = item.Substring(0, 1).ToString();
                            string ChildMenuType = item.Substring(0, 5).ToString();
                            string ChildInnerMenuType = item.Substring(0, 5).ToString();
                            var _Ap_RoleMenuDetail = new Ap_RoleMenuDetail();
                            _Ap_RoleMenuDetail.Id = Guid.NewGuid();
                            _Ap_RoleMenuDetail.RoleId = roleId.RoleId;

                            //For Parent Only 
                            if (MenuType == "P" && GetallKeys.Contains("V" + item.Substring(1, item.Length - 1).ToString()))
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                                _Ap_RoleMenuDetail.Permission = true;
                                _Ap_RoleMenuDetail.Visible = true;

                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Created Successfullly");
                                }
                            }
                            else if (MenuType == "P")
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                                Guid id = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                                var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Permission == true && x.RoleId == roleId.RoleId).SingleOrDefault();
                                if (checkExist == null)
                                {
                                    _Ap_RoleMenuDetail.Permission = true;
                                    _Ap_RoleMenuDetail.Visible = false;
                                    int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                    if (res > 0)
                                    {
                                        ShowMessage(1, "Role Created Successfullly");
                                    }
                                }
                            }
                            else if (MenuType == "V")
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                                Guid id = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                                var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Visible == true && x.RoleId == roleId.RoleId).SingleOrDefault();
                                if (checkExist == null)
                                {
                                    _Ap_RoleMenuDetail.Permission = false;
                                    _Ap_RoleMenuDetail.Visible = true;
                                    int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                    if (res > 0)
                                    {
                                        ShowMessage(1, "Role Created Successfullly");
                                    }
                                }
                            }

                            //if Both permission and visible is True
                            if (ChildMenuType == "Chk_P" && GetallKeys.Contains("Chk_V" + item.Substring(5, 36).ToString()))
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                                _Ap_RoleMenuDetail.Permission = true;
                                _Ap_RoleMenuDetail.Visible = true;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Created Successfullly");
                                }
                            }

                            //If only permission is True

                            else if (ChildMenuType == "Chk_P")
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                                Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                                var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Permission == true && x.RoleId == roleId.RoleId).SingleOrDefault();
                                if (checkExist == null)
                                {
                                    _Ap_RoleMenuDetail.Permission = true;
                                    _Ap_RoleMenuDetail.Visible = false;
                                    int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                    if (res > 0)
                                    {
                                        ShowMessage(1, "Role Created Successfullly");
                                    }
                                }
                            }
                            //If only visible is True
                            else if (ChildMenuType == "Chk_V")
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                                Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                                var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Visible == true && x.RoleId == roleId.RoleId).SingleOrDefault();
                                if (checkExist == null)
                                {
                                    _Ap_RoleMenuDetail.Permission = false;
                                    _Ap_RoleMenuDetail.Visible = true;
                                    int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                    if (res > 0)
                                    {
                                        ShowMessage(1, "Role Created Successfullly");
                                    }
                                }
                            }

                            //if Both permission and visible is True
                            if (ChildInnerMenuType == "ChkPP" && GetallKeys.Contains("ChkVV" + item.Substring(5, 36).ToString()))
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                                _Ap_RoleMenuDetail.Permission = true;
                                _Ap_RoleMenuDetail.Visible = true;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Created Successfullly");
                                }
                            }

                            //If only permission is True
                            else if (ChildInnerMenuType == "ChkPP")
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                                Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                                var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Permission == true && x.RoleId == roleId.RoleId).SingleOrDefault();
                                if (checkExist == null)
                                {
                                    _Ap_RoleMenuDetail.Permission = true;
                                    _Ap_RoleMenuDetail.Visible = false;
                                    int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                    if (res > 0)
                                    {
                                        ShowMessage(1, "Role Created Successfullly");
                                    }
                                }
                            }
                            //If only visible is True
                            else if (ChildInnerMenuType == "ChkVV")
                            {
                                _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                                Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                                var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Visible == true && x.RoleId == roleId.RoleId).SingleOrDefault();
                                if (checkExist == null)
                                {
                                    _Ap_RoleMenuDetail.Permission = false;
                                    _Ap_RoleMenuDetail.Visible = true;
                                    int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                    if (res > 0)
                                    {
                                        ShowMessage(1, "Role Created Successfullly");
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else if (BtnSubmit.Text == "Update")
            {
                Guid RoleId = Guid.Parse(Request["edit"]);

                //Delete Existing Records
                _db.ap_RoleMenuDetail.Where(w => w.RoleId == RoleId).ToList().ForEach(_db.ap_RoleMenuDetail.DeleteObject);
                _db.SaveChanges();

                //Update Name
                var Result = _db.aspnet_Roles.Where(x => x.RoleId == RoleId).SingleOrDefault();
                if (Result != null)
                {
                    Result.RoleName = txtRoleName.Text;
                }
                _db.SaveChanges();

                //loop for parent Repeater
                for (int i = 0; i < repParent.Items.Count; i++)
                {
                    //Parent Repeater Permissions Checkbox
                    CheckBox chkPPermissions = (CheckBox)repParent.Items[i].FindControl("chkPPermissions");
                    if (chkPPermissions.Checked)
                    {
                        AllKeys += chkPPermissions.ToolTip.ToString() + ',';
                    }
                    //Parent Repeater visible Checkbox
                    CheckBox chkPVisible = (CheckBox)repParent.Items[i].FindControl("chkPVisible");
                    if (chkPVisible.Checked)
                    {
                        AllKeys += chkPVisible.ToolTip.ToString() + ',';
                    }

                    Repeater ChildRepeater = (Repeater)repParent.Items[i].FindControl("repChild");
                    for (int j = 0; j < ChildRepeater.Items.Count; j++)
                    {
                        //Child Repeater Permissions Checkbox
                        CheckBox chkCPermissions = (CheckBox)ChildRepeater.Items[j].FindControl("chkCPermissions");
                        if (chkCPermissions.Checked)
                        {
                            AllKeys += chkCPermissions.ToolTip.ToString() + ',';
                        }

                        //Child Repeater Visible Checkbox
                        CheckBox chkCvisible = (CheckBox)ChildRepeater.Items[j].FindControl("chkCvisible");
                        if (chkCvisible.Checked)
                        {
                            AllKeys += chkCvisible.ToolTip.ToString() + ',';
                        }

                        var repInnerChild = (Repeater)ChildRepeater.Items[j].FindControl("repInnerChild");
                        if (repInnerChild != null && repInnerChild.Items.Count > 0)
                        {
                            for (int k = 0; k < repInnerChild.Items.Count; k++)
                            {
                                //Child Repeater Permissions Checkbox
                                var chkCInnerPermissions = (CheckBox)repInnerChild.Items[k].FindControl("chkCPermissions");
                                if (chkCInnerPermissions.Checked)
                                {
                                    AllKeys += chkCInnerPermissions.ToolTip.ToString() + ',';
                                }

                                //Child Repeater Visible Checkbox
                                var chkCInnervisible = (CheckBox)repInnerChild.Items[k].FindControl("chkCvisible");
                                if (chkCInnervisible.Checked)
                                {
                                    AllKeys += chkCInnervisible.ToolTip.ToString() + ',';
                                }
                            }
                        }
                    }
                }

                string[] GetallKeys = AllKeys.Split(',');
                foreach (var item in GetallKeys)
                {
                    if (item != "")
                    {
                        string MenuType = item.Substring(0, 1).ToString();
                        string ChildMenuType = item.Substring(0, 5).ToString();
                        string ChildInnerMenuType = item.Substring(0, 5).ToString();
                        Ap_RoleMenuDetail _Ap_RoleMenuDetail = new Ap_RoleMenuDetail();
                        _Ap_RoleMenuDetail.Id = Guid.NewGuid();
                        _Ap_RoleMenuDetail.RoleId = RoleId;

                        //For Parent Only 
                        if (MenuType == "P" && GetallKeys.Contains("V" + item.Substring(1, item.Length - 1).ToString()))
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                            _Ap_RoleMenuDetail.Permission = true;
                            _Ap_RoleMenuDetail.Visible = true;

                            int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                            if (res > 0)
                            {
                                ShowMessage(1, "Role Updated Successfullly");
                            }
                        }
                        else if (MenuType == "P")
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                            Guid id = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                            var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Permission == true && x.RoleId == RoleId).SingleOrDefault();
                            if (checkExist == null)
                            {
                                _Ap_RoleMenuDetail.Permission = true;
                                _Ap_RoleMenuDetail.Visible = false;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Updated Successfullly");
                                }
                            }
                        }
                        else if (MenuType == "V")
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                            Guid id = Guid.Parse(item.Substring(1, item.Length - 1).ToString());
                            var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Visible == true && x.RoleId == RoleId).SingleOrDefault();
                            if (checkExist == null)
                            {
                                _Ap_RoleMenuDetail.Permission = false;
                                _Ap_RoleMenuDetail.Visible = true;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Updated Successfullly");
                                }
                            }
                        }

                        //if Both permission and visible is True
                        if (ChildMenuType == "Chk_P" && GetallKeys.Contains("Chk_V" + item.Substring(5, 36).ToString()))
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                            _Ap_RoleMenuDetail.Permission = true;
                            _Ap_RoleMenuDetail.Visible = true;
                            int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                            if (res > 0)
                            {
                                ShowMessage(1, "Role Updated Successfullly");
                            }
                        }

                        //If only permission is True

                        else if (ChildMenuType == "Chk_P")
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                            Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                            var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Permission == true && x.RoleId == RoleId).SingleOrDefault();
                            if (checkExist == null)
                            {
                                _Ap_RoleMenuDetail.Permission = true;
                                _Ap_RoleMenuDetail.Visible = false;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Updated Successfullly");
                                }
                            }
                        }
                        //If only visible is True
                        else if (ChildMenuType == "Chk_V")
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                            Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                            var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Visible == true && x.RoleId == RoleId).SingleOrDefault();
                            if (checkExist == null)
                            {
                                _Ap_RoleMenuDetail.Permission = false;
                                _Ap_RoleMenuDetail.Visible = true;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Updated Successfullly");
                                }
                            }
                        }

                        //if Both permission and visible is True
                        if (ChildInnerMenuType == "ChkPP" && GetallKeys.Contains("ChkVV" + item.Substring(5, 36).ToString()))
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                            _Ap_RoleMenuDetail.Permission = true;
                            _Ap_RoleMenuDetail.Visible = true;
                            int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                            if (res > 0)
                            {
                                ShowMessage(1, "Role Updated Successfullly");
                            }
                        }

                        //If only permission is True

                        else if (ChildInnerMenuType == "ChkPP")
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                            Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                            var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Permission == true && x.RoleId == RoleId).SingleOrDefault();
                            if (checkExist == null)
                            {
                                _Ap_RoleMenuDetail.Permission = true;
                                _Ap_RoleMenuDetail.Visible = false;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Updated Successfullly");
                                }
                            }
                        }
                        //If only visible is True
                        else if (ChildInnerMenuType == "ChkVV")
                        {
                            _Ap_RoleMenuDetail.MenuId = Guid.Parse(item.Substring(5, 36).ToString());
                            Guid id = Guid.Parse(item.Substring(5, 36).ToString());
                            var checkExist = _db.ap_RoleMenuDetail.Where(x => x.MenuId == id && x.Visible == true && x.RoleId == RoleId).SingleOrDefault();
                            if (checkExist == null)
                            {
                                _Ap_RoleMenuDetail.Permission = false;
                                _Ap_RoleMenuDetail.Visible = true;
                                int res = _master.AddRoleMenu(_Ap_RoleMenuDetail);
                                if (res > 0)
                                {
                                    ShowMessage(1, "Role Updated Successfullly");
                                }
                            }
                        }

                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageRoles.aspx");
        }

        protected void grdCountry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                var mID = Guid.Parse(e.CommandArgument.ToString());
                Response.Redirect("ManageRoles.aspx?edit=" + mID);
            }
            else if (e.CommandName == "ActiveInActivePrinting")
            {
                Guid roleiD = Guid.Parse(e.CommandArgument.ToString());
                aspnet_Roles objrole = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == roleiD);
                objrole.IsPrintingAllow = !objrole.IsPrintingAllow;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            else if (e.CommandName == "ActiveInActiveSystemReport")
            {
                Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
                var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
                if (rec != null)
                    rec.SystemReport = !rec.SystemReport;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            else if (e.CommandName == "ActiveInActiveOrderAttachment")
            {
                Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
                var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
                if (rec != null)
                    rec.IsOrderAttachmentAllow = !rec.IsOrderAttachmentAllow;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            else if (e.CommandName == "ActiveInActiveRole")
            {
                Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
                var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
                if (rec != null)
                    rec.IsStaRecordsAllow = !rec.IsStaRecordsAllow;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            else if (e.CommandName == "ActiveInActiveRefund")
            {
                Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
                var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
                if (rec != null)
                    rec.IsAllowHundredPercentRefund = !rec.IsAllowHundredPercentRefund;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            else if (e.CommandName == "ActiveInActiveProductExpiryEmail")
            {
                Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
                var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
                if (rec != null)
                    rec.IsProductExpiryEmail = !rec.IsProductExpiryEmail;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            else if (e.CommandName == "IsEditableBookingFee")
            {
                Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
                var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
                if (rec != null)
                    rec.IsEditableBookingFee = !rec.IsEditableBookingFee;
                _db.SaveChanges();
                BindRoles();
                Response.Redirect("ManageRoles.aspx");
            }
            //else if (e.CommandName == "ActiveInActiveAllowStockSearch")
            //{
            //    Guid RoleID = Guid.Parse(e.CommandArgument.ToString());
            //    var rec = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == RoleID);
            //    if (rec != null)
            //        rec.IsAllowStockSearch = !rec.IsAllowStockSearch;
            //    _db.SaveChanges();
            //    BindRoles();
            //    Response.Redirect("ManageRoles.aspx");
            //}
        }
        #endregion

        #region UserDefinedEvents
        public void RolesEdit(Guid roleID)
        {
            //Role Name
            var roleNameResult = _db.aspnet_Roles.Where(x => x.RoleId == roleID).SingleOrDefault();
            if (roleNameResult != null) txtRoleName.Text = roleNameResult.RoleName;

            //Get Selected Role Menus 
            List<ap_RoleMenuDetail> rolesResult = _db.ap_RoleMenuDetail.Where(x => x.RoleId == roleID).ToList();
            foreach (var item in rolesResult)
            {
                for (int i = 0; i < repParent.Items.Count; i++)
                {
                    //Parent Repeater Permissions Checkbox
                    var chkPPermissions = (CheckBox)repParent.Items[i].FindControl("chkPPermissions");
                    var chkPVisible = (CheckBox)repParent.Items[i].FindControl("chkPVisible");
                    var menuid = item.MenuId.ToString();
                    var isVisible = item.Visible.ToString();
                    var isPermission = item.Permission.ToString();
                    if (chkPPermissions.ToolTip == "P" + menuid && isPermission == "True")
                    {
                        chkPPermissions.Checked = true;
                    }

                    if (chkPVisible.ToolTip == "V" + menuid && isVisible == "True")
                    {
                        chkPVisible.Checked = true;
                    }

                    var childRepeater = (Repeater)repParent.Items[i].FindControl("repChild");
                    for (int j = 0; j < childRepeater.Items.Count; j++)
                    {
                        //Child Repeater Permissions Checkbox
                        var chkCPermissions = (CheckBox)childRepeater.Items[j].FindControl("chkCPermissions");
                        var chkCvisible = (CheckBox)childRepeater.Items[j].FindControl("chkCvisible");
                        if (chkCPermissions.ToolTip == "Chk_P" + menuid && isVisible == "True" && isPermission == "True")
                        {
                            chkCPermissions.Checked = true;
                            chkCvisible.Checked = true;
                        }
                        else if (chkCPermissions.ToolTip == "Chk_P" + menuid && isPermission == "True")
                        {
                            chkCPermissions.Checked = true;
                        }
                        else if (chkCPermissions.ToolTip == "Chk_P" + menuid && isVisible == "True")
                        {
                            chkCvisible.Checked = true;
                        }

                        var repInnerChild = (Repeater)childRepeater.Items[j].FindControl("repInnerChild");
                        if (repInnerChild != null && repInnerChild.Items.Count > 0)
                        {
                            for (int k = 0; k < repInnerChild.Items.Count; k++)
                            {
                                //Inner Child Repeater Permissions Checkbox
                                var chkCInnerPermissions = (CheckBox)repInnerChild.Items[k].FindControl("chkCPermissions");
                                var chkCInnerVisible = (CheckBox)repInnerChild.Items[k].FindControl("chkCvisible");
                                if (chkCInnerPermissions.ToolTip == "ChkPP" + menuid && isVisible == "True" && isPermission == "True")
                                {
                                    chkCInnerPermissions.Checked = true;
                                    chkCInnerVisible.Checked = true;
                                }
                                else if (chkCInnerPermissions.ToolTip == "ChkPP" + menuid && isPermission == "True")
                                {
                                    chkCInnerPermissions.Checked = true;
                                }
                                else if (chkCInnerPermissions.ToolTip == "ChkPP" + menuid && isVisible == "True")
                                {
                                    chkCInnerVisible.Checked = true;
                                }
                            }
                        }
                    }
                }
            }

            BtnSubmit.Text = "Update";
        }

        public void BindParentNavigation()
        {
            repParent.DataSource = _master.GetParentNavigationList(Guid.Parse("00000000-0000-0000-0000-000000000000")).OrderBy(x => x.Name).ToList();
            repParent.DataBind();
        }

        public string Getname(string value)
        {
            return value;
        }

        private void BindRoles()
        {
            var roles = _db.aspnet_Roles.ToList();
            var userdetail = _db.tblAdminUsers.FirstOrDefault(x => x.ID == AdminuserInfo.UserID);
            string usertype = "";
            if (userdetail != null)
            {
                var res = _db.aspnet_Roles.FirstOrDefault(x => x.RoleId == userdetail.RoleID);
                if (res != null)
                    usertype = res.RoleName;
            }
            grdRoles.DataSource = usertype.ToLower().Trim() == "admin" ? roles : roles.AsEnumerable().Where(x => x.RoleName.ToLower().Trim() != "admin");
            grdRoles.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        #endregion

        protected void rptParentMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var rptSubMenu = (Repeater)e.Item.FindControl("rptSubMenu");
                var hdnParentID = (HiddenField)e.Item.FindControl("hdnParentID");
                if (hdnParentID != null)
                {
                    var parentID = Guid.Parse(hdnParentID.Value);
                    rptSubMenu.DataSource = _db.tblNavigations.Where(x => x.ParentID == parentID && x.IsActive == true).OrderBy(x => x.Name).ToList();
                    rptSubMenu.DataBind();
                }
            }
        }

        protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var rptPermission = (Repeater)e.Item.FindControl("rptPermission");
                var hdnID = (HiddenField)e.Item.FindControl("hdnID");
                if (hdnID != null)
                {
                    var menuID = Guid.Parse(hdnID.Value);
                    var roleID = Guid.Parse(hdnID.Value);
                    var result = _db.ap_RoleMenuDetail.Where(x => x.MenuId == menuID);
                    rptPermission.DataSource = result;
                    rptPermission.DataBind();
                }
            }
        }

        #region Export Roles
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                BindGridExportUserRoles();
                grdRoleExport.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#d06b95");
                Context.Response.ClearContent();
                Context.Response.ContentType = "application/ms-excel";
                Context.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xls", "AdminRole"));
                Context.Response.Charset = "";
                var stringwriter = new System.IO.StringWriter();
                var htmlwriter = new HtmlTextWriter(stringwriter);
                grdRoleExport.RenderControl(htmlwriter);
                Context.Response.Write(stringwriter.ToString());
                Context.Response.End();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        void BindGridExportUserRoles()
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
            string sql = "spGetAdminUserRole";
            SqlConnection conn = new SqlConnection(connString);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da.Fill(ds);

                grdRoleExport.DataSource = ds;
                grdRoleExport.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                conn.Close();
            }

        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        #endregion

        protected void repChild_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnParentId = (HiddenField)e.Item.FindControl("hdnParentId");
                Repeater repInnerChild = (Repeater)e.Item.FindControl("repInnerChild");
                repInnerChild.DataSource = _master.GetParentNavigationList(Guid.Parse(hdnParentId.Value)).OrderBy(x => x.Name).ToList();
                repInnerChild.DataBind();
            }
        }
    }
}