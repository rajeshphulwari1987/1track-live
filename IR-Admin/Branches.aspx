﻿<%@ Page Title="Office Master" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Branches.aspx.cs" Inherits="IR_Admin.BranchPage" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/jscript">
        function editOffice(id) {
            document.getElementById("<%= hdnBranchid.ClientID %>").value = id;
            __doPostBack("<%= btneditOffice.UniqueID %>", "");
        }
        function UpLoadOffice(id) {

            document.getElementById("<%= hdnBranchid.ClientID %>").value = id;
            __doPostBack("<%= btnUploadBranches.UniqueID %>", "");
        }

        function addSubOffice(id) {
            document.getElementById("<%= hdnBranchid.ClientID %>").value = id;
            __doPostBack("<%= btnAddOffice.UniqueID %>", "");
        }
        function deleteOffice(id) {

            if (getConfirm()) {
                document.getElementById("<%= hdnBranchid.ClientID %>").value = id;
                __doPostBack("<%= btnDeleteOffice.UniqueID %>", "");
            }
            return false;
        }

        function getConfirm() {
            return confirm('Are you sure you want to delete this record?');
        }

    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btneditOffice" runat="server" OnClick="btneditOffice_Click" CssClass="hidebtn" />
    <asp:Button ID="btnUploadBranches" runat="server" OnClick="btnUploadBranches_Click"
        CssClass="hidebtn" />
    <asp:Button ID="btnAddOffice" runat="server" OnClick="btnaddOffice_Click" CssClass="hidebtn" />
    <asp:Button ID="btnDeleteOffice" runat="server" OnClick="btnDeleteOffice_Click" CssClass="hidebtn" />
    <h2>
        Offices</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Branches.aspx" class="current">List</a></li>
            <li><a id="aNew" href="BranchesAddEdit.aspx" class=" ">New/Edit</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <asp:TreeView ID="trBranch" runat="server" OnSelectedNodeChanged="trBranch_SelectedNodeChanged"
                        ShowLines="True">
                        <LeafNodeStyle CssClass="leafNode" />
                        <NodeStyle CssClass="treeNode" />
                        <RootNodeStyle CssClass="rootNode" />
                        <SelectedNodeStyle CssClass="selectNode" />
                        <DataBindings>
                            <asp:TreeNodeBinding TextField="Text" DataMember="System.Data.DataRowView" ValueField="ID" />
                        </DataBindings>
                    </asp:TreeView>
                </div>
                <asp:HiddenField ID="hdnBranchid" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
