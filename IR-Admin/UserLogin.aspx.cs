﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

public partial class UserLogin : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    readonly ManageUser _ManageUser = new ManageUser();
    private readonly Masters _oMasters = new Masters();
    private readonly ManageBooking _MasterBooking = new ManageBooking();
    public static List<getRailPassData> list = new List<getRailPassData>();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private Guid siteId;
    string siteURL;
    public string script = "<script></script>";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSuccessMsg.Text = string.Empty;
        if (!IsPostBack)
        {
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);

            lblerrorReg.Text = string.Empty;
            PageLoadEvent();
            SetPassword();
            QubitOperationLoad();
        }
        if (Session["AgentUsername"] != null)
        {
            removeAgentSession();
        }
        if (Session["USERUsername"] != null)
        {
            removeAgentSession();
            Response.Redirect(siteURL + "home");
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _oMasters.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _oMasters.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void removeAgentSession()
    {
        Session.Remove("RailPassData");
        Session.Remove("DetailRailPass");

        Session.Remove("AgentUsername");
        Session.Remove("AgentRoleId");
        Session.Remove("AgentUserID");
        Session.Remove("AgentSiteID");
        Session.Remove("Agentreferenceno");

        Session.Remove("PopupDisplayed");
        Session.Remove("USERUsername");
        Session.Remove("USERRoleId");
        Session.Remove("USERUserID");
        Session.Remove("USERSiteID");
    }

    void PageLoadEvent()
    {
        List<tblCountriesMst> countryList = _oMasters.GetCountryList().Where(x => x.IsActive == true).ToList();
        ddlCountry.DataSource = countryList != null && countryList.Count > 0 ? countryList : null;
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataValueField = "CountryID";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
    }

    public void BindUSER(bool UserID)
    {
        try
        {
            var UserExist = _ManageUser.UserLogin(txtUsername.Text, txtPassword.Text, true);
            if (UserExist != null)
            {
                //Login USER User Information
                USERuserInfo.UserEmail = UserExist.Email;
                USERuserInfo.ID = UserExist.ID;
                USERuserInfo.Username = UserExist.FirstName;
                USERuserInfo.SiteID = UserExist.SiteId;

                //Remember  Me 
                if (chkRememberMe.Checked)
                {
                    HttpCookie cookie = new HttpCookie("USERlogin");
                    Response.Cookies.Add(cookie);
                    string pass = Common.Encrypt(txtPassword.Text.Trim(), true);
                    cookie.Values.Add("USERUserName", txtUsername.Text);
                    cookie.Values.Add("USERPassword", pass);
                    cookie.Values.Add("USERRemember", chkRememberMe.Checked.ToString());
                    cookie.Expires = DateTime.Now.AddHours(2);
                }
                else
                {
                    HttpContext.Current.Response.Cookies.Remove("USERlogin");
                    Response.Cookies["USERlogin"].Expires = DateTime.Now;
                }
                Response.Redirect("PassDetail.aspx");
            }
            else
            {
                lblSuccessMsg.Text = "Invalid User Name And Password";
            }
        }
        catch (Exception ex)
        {
            if (Request.Cookies["USERlogin"] != null)
            {
                Response.Cookies["USERlogin"].Expires = DateTime.Now.AddDays(-1);
            }
        }
    }

    protected void BtnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(txtUsername.Text) && !String.IsNullOrEmpty(txtPassword.Text))
        {
            BindUSER(true);
        }
    }

    void SetPassword()
    {
        if (Request.Cookies["USERlogin"] != null)
        {
            HttpCookie getCookie = Request.Cookies.Get("USERlogin");
            if (getCookie != null && getCookie["USERRemember"] != null && getCookie["USERRemember"].ToLower() == "true")
            {
                string pass = Common.Decrypt(getCookie.Values["USERPassword"].Trim(), true);
                txtUsername.Text = getCookie.Values["USERUserName"].Trim();
                txtPassword.Attributes.Add("value", pass);
                chkRememberMe.Checked = true;
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("home");
    }

    protected void btnFSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(txtEmail.Text))
        {
            var UserExist = _ManageUser.CheckEmailUser(txtEmail.Text);
            if (UserExist != null)
            {
                string Subject = "Reset Password";
                string Body = "<html><head><title></title></head><body><p> Dear " + (UserExist.FirstName + " " + UserExist.LastName) + " , <br /> <br /> Your password is reset on International Rail <br />" +
                          "<br />	Your USERlogin details are as below:<br />	<br />	User Name : " + UserExist.Email + "<br />	Password : " + UserExist.Password + "<br /><br />	Thanks" +
                          "<br />International Rail</p></body></html>";

                var SendEmail = ManageUser.SendMail(Subject, Body, txtEmail.Text, siteId);
                if (SendEmail == true)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>PasswordSent();</script>", false);
                    txtEmail.Text = string.Empty;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>PasswordErrorSent();</script>", false);
            }
        }
    }

    protected void btnregsubSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string sCountry = ddlCountry.SelectedValue;
            var dob = DateTime.ParseExact((txtdob.Text), "dd/MMM/yyyy", CultureInfo.InvariantCulture);
            bool result = _MasterBooking.AddLoginUSer(new tblUserLogin
            {
                ID = Guid.NewGuid(),
                FirstName = txtfname.Text,
                LastName = txtlname.Text,
                Phone = txtphone.Text,
                Email = txtremail.Text,
                Password = txtpass.Text,
                Address = txtAddress.Text,
                Dob = dob,
                Country = Guid.Parse(ddlCountry.SelectedValue == "0" ? new Guid().ToString() : ddlCountry.SelectedValue),
                City = txtcity.Text,
                PostCode = txtpost.Text,
                SiteId = siteId,
                IsActive = true
            });

            if (result)
            {
                //lblerrorReg.Text = "Email Already Exists.";
                ShowMessage(2,"Email Already Exists.");
            }
            else
            {
                txtUsername.Text = txtremail.Text;
                txtPassword.Text = txtpass.Text;
                BindUSER(true);
                Response.Redirect("PassDetail.aspx");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}