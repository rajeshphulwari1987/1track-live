﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class P2PBookingFee : System.Web.UI.Page
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        readonly private Masters _oMasters = new Masters();
        public ManageBooking booking = new ManageBooking();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                PageLoadEvent();
        }
        protected void PageLoadEvent()
        {
            try
            {
                var list = booking.GetP2PBookingFee().OrderBy(t=>t.Name).ToList();
                grvBookingFee.DataSource = list;
                grvBookingFee.DataBind();
                 
                ddlSitelist.DataSource=_oMasters.Branchlist().Where(x => x.IsActive == true && x.IsDelete==false).OrderBy(x=>x.DisplayName).ToList();
                ddlSitelist.DataValueField="ID";
                ddlSitelist.DataTextField="DisplayName";;
                ddlSitelist.DataBind();
                PageLoadEventAPIBooking();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void PageLoadEventAPIBooking()
        {
            try
            {
                var ApiList = booking.GetP2PAPIBookingFee(Guid.Parse(ddlSitelist.SelectedValue));
                if (!ApiList.Any(x => x.Name == "BENE"))
                {
                    ApiList.Add(new P2PbookingFee
                    {
                        Fee = "0.00",
                        ID = Guid.NewGuid().ToString(),
                        IsApplicable = false.ToString(),
                        IsPercentage = "0.00",
                        Name = "BENE",
                        SiteID = ddlSitelist.SelectedValue
                    });
                }
                if (!ApiList.Any(x => x.Name == "ITALIA"))
                {
                    ApiList.Add(new P2PbookingFee
                    {
                        Fee = "0.00",
                        ID = Guid.NewGuid().ToString(),
                        IsApplicable = false.ToString(),
                        IsPercentage = "0.00",
                        Name = "ITALIA",
                        SiteID = ddlSitelist.SelectedValue
                    });
                }
                GrdAPIBooking.DataSource = ApiList;
                GrdAPIBooking.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void grvBookingFee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvBookingFee.PageIndex = e.NewPageIndex;
            PageLoadEvent();
        }
        protected void imgApplicable_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgApplicable = ((ImageButton)sender);
                HiddenField hdnSiteId = imgApplicable.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = imgApplicable.Parent.FindControl("hdnId") as HiddenField;
                TextBox txtFee = imgApplicable.Parent.FindControl("txtFee") as TextBox;
                TextBox txtIsPercent = imgApplicable.Parent.FindControl("txtIsPercent") as TextBox;
                Guid Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value);
                Guid SiteId = string.IsNullOrEmpty(hdnSiteId.Value) ? Guid.Empty : Guid.Parse(hdnSiteId.Value);
                decimal fee = string.IsNullOrEmpty(txtFee.Text) ? (decimal)0.00 : Convert.ToDecimal(txtFee.Text);
                decimal IsPercent = string.IsNullOrEmpty(txtIsPercent.Text) ? (decimal)0.00 : Convert.ToDecimal(txtIsPercent.Text);
                if (IsPercent > (decimal)99.99)
                {
                    ShowMessage(2, "Fee in (%) is not 100%.");
                    return;
                }

                var Booking = new tblP2PBookingFee
                {
                    ID = (Id == Guid.Empty ? Guid.NewGuid() : Id),
                    SiteId = SiteId,
                    Fee = fee,
                    IsPercentage = IsPercent,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    IsApplicable = true,
                    IsSite=true
                };
                int reault = booking.updateP2PBookingFeeIsApplicable(Booking);
                PageLoadEvent();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender;
                HiddenField hdnSiteId = btnUpdate.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = btnUpdate.Parent.FindControl("hdnId") as HiddenField;
                TextBox txtFee = btnUpdate.Parent.FindControl("txtFee") as TextBox;
                TextBox txtIsPercent = btnUpdate.Parent.FindControl("txtIsPercent") as TextBox;
                Guid Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value);
                Guid SiteId = string.IsNullOrEmpty(hdnSiteId.Value) ? Guid.Empty : Guid.Parse(hdnSiteId.Value);
                decimal fee = string.IsNullOrEmpty(txtFee.Text) ? (decimal)0.00 : Convert.ToDecimal(txtFee.Text);
                decimal IsPercent = string.IsNullOrEmpty(txtIsPercent.Text) ? (decimal)0.00 : Convert.ToDecimal(txtIsPercent.Text);
                if (IsPercent > (decimal)99.99)
                {
                    ShowMessage(2, "Fee is not grater then 100%.");
                    return;
                }
                var Booking = new tblP2PBookingFee
                {
                    ID = (Id == Guid.Empty ? Guid.NewGuid() : Id),
                    SiteId = SiteId,
                    Fee = fee,
                    IsPercentage = IsPercent,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    IsSite=true
                };
                int reault = booking.AddUpdateP2PBookingFee(Booking);
                if (reault > 0)
                {
                    ShowMessage(1, "Booking Fee update successfully.");
                }
                PageLoadEvent();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }              
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        protected void ddlSitelist_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageLoadEventAPIBooking();
        }
        protected void imgAPIBookingApplicable_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgApplicable = ((ImageButton)sender);
                HiddenField hdnSiteId = imgApplicable.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = imgApplicable.Parent.FindControl("hdnId") as HiddenField;
                HiddenField hdnApiName = imgApplicable.Parent.FindControl("hdnApiName") as HiddenField;
                TextBox txtFee = imgApplicable.Parent.FindControl("txtFee") as TextBox;
                TextBox txtIsPercent = imgApplicable.Parent.FindControl("txtIsPercent") as TextBox;
                Guid Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value);
                Guid SiteId = string.IsNullOrEmpty(hdnSiteId.Value) ? Guid.Empty : Guid.Parse(hdnSiteId.Value);
                decimal fee = string.IsNullOrEmpty(txtFee.Text) ? (decimal)0.00 : Convert.ToDecimal(txtFee.Text);
                decimal IsPercent = string.IsNullOrEmpty(txtIsPercent.Text) ? (decimal)0.00 : Convert.ToDecimal(txtIsPercent.Text);
                if (IsPercent > (decimal)99.99)
                {
                    ShowMessage(2, "Fee in (%) is not 100%.");
                    return;
                }

                var Booking = new tblP2PBookingFee
                {
                    ID = Id,
                    SiteId = SiteId,
                    Fee = fee,
                    IsPercentage = IsPercent,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    IsApplicable = true,
                    ApiName = hdnApiName.Value,
                    IsSite=false
                };
                int reault = booking.AddUpdateP2PAPIBookingFee(Booking, 1);
                PageLoadEventAPIBooking();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void btnUpdateAPIBooking_Click(object sender, EventArgs e)
        {
            try
            {
                Button Buttons = ((Button)sender);
                HiddenField hdnSiteId = Buttons.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = Buttons.Parent.FindControl("hdnId") as HiddenField;
                HiddenField hdnApiName = Buttons.Parent.FindControl("hdnApiName") as HiddenField;
                TextBox txtFee = Buttons.Parent.FindControl("txtFee") as TextBox;
                TextBox txtIsPercent = Buttons.Parent.FindControl("txtIsPercent") as TextBox;
                Guid Id = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value);
                Guid SiteId = string.IsNullOrEmpty(hdnSiteId.Value) ? Guid.Empty : Guid.Parse(hdnSiteId.Value);
                decimal fee = string.IsNullOrEmpty(txtFee.Text) ? (decimal)0.00 : Convert.ToDecimal(txtFee.Text);
                decimal IsPercent = string.IsNullOrEmpty(txtIsPercent.Text) ? (decimal)0.00 : Convert.ToDecimal(txtIsPercent.Text);
                if (IsPercent > (decimal)99.99)
                {
                    ShowMessage(2, "Fee in (%) is not 100%.");
                    return;
                }

                var Booking = new tblP2PBookingFee
                {
                    ID = Id,
                    SiteId = SiteId,
                    Fee = fee,
                    IsPercentage = IsPercent,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    ApiName = hdnApiName.Value,
                    IsSite=false
                };
                int reault = booking.AddUpdateP2PAPIBookingFee(Booking, 2);
                if (reault > 0)
                {
                    ShowMessage(1, "Booking Fee update successfully.");
                }
                PageLoadEventAPIBooking();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
    }
}