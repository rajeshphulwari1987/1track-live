﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="TermsAndConditions.aspx.cs"
    Inherits="IR_Admin.TermsAndConditions" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/Tab/jquery.js"></script>
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesp').redactor({ iframe: true, minHeight: 200 });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Terms And Conditions
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="TermsAndConditions.aspx" class="current">List</a></li>
            <li><a id="aNew" href="TermsAndConditions.aspx" class=" ">New</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdTerms" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdTerms_RowCommand"
                            AllowPaging="True" OnPageIndexChanging="grdTerms_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Page Name">
                                    <ItemTemplate>
                                        <%#Eval("Title")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="35%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="35%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="TermsAndConditions.aspx?id=<%#Eval("Id")%>" title="Edit" style="text-decoration: none;">
                                            <img alt="edit" src="images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <table width="100%">
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Title
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTitle" runat="server" Width="500px" />
                                            <asp:RequiredFieldValidator ID="reqTitle" runat="server" ControlToValidate="txtTitle"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col" valign="top">
                                            Description
                                        </td>
                                        <td class="col">
                                            <textarea id="txtDesp" runat="server"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsActv" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="submit" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
