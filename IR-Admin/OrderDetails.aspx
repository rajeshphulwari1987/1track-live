﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="OrderDetails.aspx.cs" Inherits="IR_Admin.OrderDetails" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="Styles/jquery.ui.datepicker.css">
    <link rel="stylesheet" href="Styles/jquery.ui.all.css">
    <script type="text/javascript" src="Scripts/DatePicker/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="Scripts/DatePicker/jquery.ui.core.js"></script>
    <script type="text/javascript" src="Scripts/DatePicker/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="Scripts/DatePicker/jquery.ui.widget.js"></script>
    <style type="text/css">
        .grid-head2
        {
            height: auto;
            line-height: 16px;
        }
        .ui-datepicker
        {
            background-color: White;
        }
        .number
        {
        }
        .bordernone
        {
            border: none;
        }
        .agentsec, .affiliatesec
        {
            display: none;
        }
        .isAffliate
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function ManualShowHideAffliateDdl() {
            if ($('#MainContent_rdnIsAffliate_0').click(function () {
                if ($(this).is(":checked")) {
                    $(".affiliatesec").show();
                    ValidatorEnable($('[id*=reqAffiliate]')[0], true);
                }
            }));
            if ($('#MainContent_rdnIsAffliate_1').click(function () {
                if ($(this).is(":checked")) {
                    $(".affiliatesec").hide();
                    ValidatorEnable($('[id*=reqAffiliate]')[0], false);
                }
            }));
        }
        function DefaultShowHideAffliateDdl() {
            if ($('#MainContent_rdnIsAffliate_0').is(":checked")) {
                $(".affiliatesec").show();
                ValidatorEnable($('[id*=reqAffiliate]')[0], true);
            }
            if ($('#MainContent_rdnIsAffliate_1').is(":checked")) {
                $(".affiliatesec").hide();
                ValidatorEnable($('[id*=reqAffiliate]')[0], false);
            }
        }
    </script>
    <script type="text/javascript">
        var count = 0;
        var countKey = 1;
        var conditionone = 0;
        var conditiontwo = 0;
        var tabkey = 0;
        $(document).ready(function () {
            isAgent();
            LoadCal();
            ManualShowHideAffliateDdl();
            if ($("#txtFrom").val() != '') {
                //                alert(localStorage.getItem("spantxtTo"));
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                //                alert(localStorage.getItem("spantxtFrom"));
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            $(window).click(function (event) {
                $('#_bindDivData').remove();
            });
            $(window).keydown(function (event) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            });
            $("#txtFrom , #txtTo").on('keydown', function (event) {
                //40,38
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;

                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
            ValidatorEnable($('[id*=reqOffice]')[0], false);
            ValidatorEnable($('[id*=reqAgent]')[0], false);

            var showref = $('#MainContent_reqrefxxcc').length;
            if (showref == 1) { ValidatorEnable($('[id*=reqref]')[0], false); }

            $("#MainContent_rdbtnAgent").click(function () {
                if ($(this).is(":checked")) {
                    $(".isAffliate").hide();
                    $(".affiliatesec").hide();
                    $("#agentreff").show();
                    $(".agentsec").show();
                    ValidatorEnable($('[id*=reqAffiliate]')[0], false);
                    ValidatorEnable($('[id*=reqOffice]')[0], true);
                    ValidatorEnable($('[id*=reqAgent]')[0], true);
                    var showref = $('#MainContent_reqrefxxcc').length;
                    if (showref == 1) { ValidatorEnable($('[id*=reqref]')[0], true); }
                }
            });
            $("#MainContent_rdbtnPublic").click(function () {
                if ($(this).is(":checked")) {
                    $(".agentsec").hide();
                    $("#agentreff").hide();
                    $(".isAffliate").show();
                    $(".affiliatesec").show();
                    $('#MainContent_rdnIsAffliate_0').prop("checked", true);
                    ValidatorEnable($('[id*=reqOffice]')[0], false);
                    ValidatorEnable($('[id*=reqAgent]')[0], false);
                    ValidatorEnable($('[id*=reqAffiliate]')[0], true);
                    var showref = $('#MainContent_reqrefxxcc').length;
                    if (showref == 1) { ValidatorEnable($('[id*=reqref]')[0], false); }
                    ManualShowHideAffliateDdl();
                }
            });
        });
        function isAgent() {
            $(document).ready(function () {
                if ($("#MainContent_rdbtnAgent").is(":checked")) {
                    $("#agentreff").show();
                    $(".agentsec").show();
                    $(".isAffliate").hide();
                    ValidatorEnable($('[id*=reqAffiliate]')[0], false);
                    ValidatorEnable($('[id*=reqOffice]')[0], true);
                    ValidatorEnable($('[id*=reqAgent]')[0], true);
                    var showref = $('#MainContent_reqrefxxcc').length;
                    if (showref == 1) { ValidatorEnable($('[id*=reqref]')[0], true); }
                }
                else {
                    $(".agentsec").hide();
                    $("#agentreff").hide();
                    $(".isAffliate").show();
                    ValidatorEnable($('[id*=reqOffice]')[0], false);
                    ValidatorEnable($('[id*=reqAgent]')[0], false);
                    var showref = $('#MainContent_reqrefxxcc').length;
                    if (showref == 1) { ValidatorEnable($('[id*=reqref]')[0], false); }
                    DefaultShowHideAffliateDdl();
                }
            });
        }
        function selectpopup(e) {
            $(function () {
                if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                    var $this = $(e);
                    var data = $this.val();
                    var station = '';
                    var hostName = window.location.host;
                    var url = "http://" + hostName;
                    if (window.location.toString().indexOf("https:") >= 0)
                        url = "https://" + hostName;
                    if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                        $('#spantxtFrom').text('');
                        $('#spantxtTo').text('');
                    }
                    var filter = $("#span" + $this.attr('id') + "").text();
                    if (filter == "" && $this.val() != "")
                        filter = $("#hdnFilter").val();
                    $("#hdnFilter").val(filter);
                    if ($this.attr('id') == 'txtTo') {
                        station = $("#txtFrom").val();
                    }
                    else {
                        station = $("#txtTo").val();
                    }
                    if (hostName == "localhost")
                        url = "http://" + hostName + "/IR-Admin";
                    else if (hostName == "admin.1tracktest.com")
                        url = "http://" + hostName;
                    var hostUrl = url + "/StationList.asmx/getStationsXList";
                    data = data.replace(/[']/g, "♥");
                    var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
                    $.ajax({
                        type: "POST",
                        url: hostUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                        success: function (msg) {
                            $('#_bindDivData').remove();
                            var lentxt = data.length;
                            $.each(msg.d, function (key, value) {
                                var splitdata = value.split('ñ');
                                var lenfull = splitdata[0].length; ;
                                var txtupper = splitdata[0].substring(0, lentxt);
                                var txtlover = splitdata[0].substring(lentxt, lenfull);
                                $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                            });
                            $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                        },
                        error: function () {
                            //     alert("Wait...");
                        }
                    });
                }
            });
        }
        function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
            $('#_bindDivData').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }
        function CheckCardVal(sender, args) {
            if (document.getElementById('MainContent_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        //var unavailableDates = '[25/01/2013]';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
        function LoadCal() {
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                onClose: function (selectedDate) {
                    $("#MainContent_txtReturnDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#MainContent_txtReturnDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0
            });
            $("#MainContent_txtReturnDate").datepicker("option", "minDate", $("#MainContent_txtDepartureDate").datepicker('getDate'));
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });
            $(".imgCal").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });
            $(".imgCal1, #MainContent_txtReturnDate").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked'))
                    $("#MainContent_txtReturnDate").datepicker('show');
            });
        }
        function calenable() {
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('enable');
        }
        function caldisable() {
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('disable');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
    <asp:Panel ID="pnlP2PMsg" runat="server" Visible="true">
        <h2>
            You are not permitted to book P2P journey.
        </h2>
    </asp:Panel>
    <asp:Panel ID="pnlOrderAdd" runat="server" Visible="true">
        <h2>
            P2P Order
        </h2>
        <div class="full mr-tp1" style="font-size: 14px;">
            <div class="panes">
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="divMain" style="margin-bottom: 10px!important;">
                    <div class="divleft">
                        Order Being Placed is
                    </div>
                    <div class="divright">
                        <asp:RadioButton runat="server" ID="rdbtnPublic" Checked="true" GroupName="rdb" />For Public
                        <asp:RadioButton runat="server" ID="rdbtnAgent" GroupName="rdb" />On behalf of an Agent
                    </div>
                    <div class="divleft isAffliate" style="margin-top: 6px;">
                        Is this Affiliate booking?
                    </div>
                    <div class="divright isAffliate">
                        <asp:RadioButtonList ID="rdnIsAffliate" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1" >Yes</asp:ListItem>
                            <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="divleft affiliatesec">
                        Affiliate names
                    </div>
                    <div class="divright affiliatesec ">
                        <asp:DropDownList ID="ddlaffiliate" runat="server" class="inputsl" ValidationGroup="vgsRef" />
                        <asp:RequiredFieldValidator ID="reqAffiliate" runat="server" ControlToValidate="ddlaffiliate"
                            ValidationGroup="vgs" InitialValue="0" ErrorMessage="Please select affiliate name."
                            Text="*" ForeColor="Red" Display="Dynamic" />
                    </div>
                    <div class="divleft agentsec">
                        Ordering Agent Office
                    </div>
                    <div class="divright agentsec">
                        <asp:DropDownList ID="ddlOffice" runat="server" class="inputsl" ValidationGroup="vgsRef"
                            ToolTip="This is the office of the agent who has taken this order and the order will be allocated to that office within 1Track"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlOffice_SelectedIndexChanged" />
                        <asp:RequiredFieldValidator ID="reqOffice" runat="server" ControlToValidate="ddlOffice"
                            ValidationGroup="vgs" InitialValue="0" ErrorMessage="Please select Office." Text="*"
                            ForeColor="Red" Display="Dynamic" />
                    </div>
                    <div class="divleft agentsec">
                        Agent
                    </div>
                    <div class="divright agentsec">
                        <asp:DropDownList ID="ddlAgent" runat="server" class="inputsl" Enabled="false" />
                        <asp:RequiredFieldValidator ID="reqAgent" runat="server" ControlToValidate="ddlAgent"
                            ValidationGroup="vgs" InitialValue="0" ErrorMessage="Please select Agent." Text="*"
                            ForeColor="Red" Display="Dynamic" />
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div class="divMain">
                    <div class="divleft">
                        Supplier
                    </div>
                    <div class="divright">
                        <asp:DropDownList ID="ddlCommition" runat="server" class="inputsl">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqCommition" runat="server" ControlToValidate="ddlCommition"
                            ValidationGroup="vgs" InitialValue="0" ErrorMessage="Please select Commition."
                            Text="*" ForeColor="Red" Display="Dynamic" />
                    </div>
                    <div class="divleft">
                        From
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtFrom" ClientIDMode="Static" onkeyup="selectpopup(this)" runat="server"
                            MaxLength="40" class="input" autocomplete="off" Style="margin-left: -3px;" />
                        <span id="spantxtFrom" style="display: none"></span>
                        <asp:HiddenField ID="hdnFrm" runat="server" />
                        <asp:RequiredFieldValidator ID="reqFrom" runat="server" ForeColor="red" ValidationGroup="vgs"
                            Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                            Text="*" />
                    </div>
                    <div class="divleft">
                        To
                    </div>
                    <div class="divright">
                        <asp:TextBox ClientIDMode="Static" ID="txtTo" runat="server" onkeyup="selectpopup(this)"
                            class="input" autocomplete="off" Style="margin-left: -3px;" MaxLength="40" />
                        <span id="spantxtTo" style="display: none"></span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="red"
                            ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                            Text="*" CssClass="font14" />
                    </div>
                    <div class="divleft" style="width: 100%;">
                        <div style="float: left; width: 25%">
                            Date of departure
                        </div>
                        <div style="float: right; width: 74%">
                            <asp:TextBox ID="txtDepartureDate" runat="server" class="input" Style="margin-right: 4px;
                                float: left;" autocomplete="off" MaxLength="11" />
                            <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select DepartureDate."
                                border="0" alt="">
                            <asp:RequiredFieldValidator ID="reqDepartureDate" runat="server" ForeColor="red"
                                ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtDepartureDate"
                                ErrorMessage="Please enter departure date." Text="*" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDepartureDate"
                                ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                                ValidationGroup="vgs">*</asp:RegularExpressionValidator>
                            <asp:DropDownList ID="ddldepTime" runat="server" class="inputsl" Style="width: 90px !important;
                                margin-left: 0px; float: left;">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem Selected="True">09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlDepSec" runat="server" class="inputsl" Style="width: 90px !important;
                                margin-left: 5px; float: left;">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divleft" style="width: 100%;">
                        <div style="float: left; width: 25%">
                            Date of arrival
                        </div>
                        <div style="float: right; width: 74%">
                            <asp:TextBox ID="txtReturnDate" runat="server" class="input" Style="margin-right: 4px;
                                float: left;" autocomplete="off" MaxLength="11" />
                            <asp:RequiredFieldValidator ID="reqReturnDate" runat="server" ForeColor="red" ValidationGroup="vgs"
                                Display="Dynamic" ControlToValidate="txtReturnDate" ErrorMessage="Please enter return date."
                                Text="*" />
                            <asp:RegularExpressionValidator ID="regReturnDate" runat="server" ControlToValidate="txtReturnDate"
                                ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid return date"
                                ValidationGroup="vgs" Enabled="False">*</asp:RegularExpressionValidator>
                            <asp:DropDownList ID="ddlReturnTime" runat="server" class="inputsl" Style="width: 90px !important;
                                margin-left: 0px; float: left;">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem Selected="True">09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlRetSec" runat="server" class="inputsl" Style="width: 90px !important;
                                margin-left: 5px; float: left;">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divleft" style="width: 100%;">
                        <div style="float: left; width: 25%">
                            Train number(s)
                        </div>
                        <div style="float: right; width: 74%">
                            <asp:TextBox ID="txtTrainNumber" runat="server" class="input" autocomplete="off"
                                MaxLength="20" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="red"
                                ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTrainNumber" ErrorMessage="Please enter train number."
                                Text="*" />
                        </div>
                    </div>
                    <div class="divleft" style="width: 100%;">
                        <div style="float: left; width: 25%">
                            Class Preference
                        </div>
                        <div style="float: right; width: 74%">
                            <asp:RadioButtonList ID="ddlClass" runat="server" RepeatDirection="Horizontal" Width="150px">
                                <asp:ListItem Value="1" Selected="True">1st</asp:ListItem>
                                <asp:ListItem Value="2">2nd</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="divleft" style="width: 100%;">
                        <div style="float: left; width: 26%">
                            Number of adults
                        </div>
                        <div style="float: left; width: 17%">
                            <asp:DropDownList ID="ddlAdult" runat="server" class="inputsl m-none" Width="90px">
                            </asp:DropDownList>
                        </div>
                        <div style="float: left; width: 15%">
                            Number of children
                        </div>
                        <div style="float: left; width: 28%">
                            <asp:DropDownList ID="ddlChildren" runat="server" class="inputsl m-none" Width="90px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divleft" style="width: 100%;">
                        <div style="float: left; width: 26%">
                            Number of seniors
                        </div>
                        <div style="float: left; width: 17%">
                            <asp:DropDownList ID="ddlSeniors" runat="server" class="inputsl m-none" Width="90px">
                            </asp:DropDownList>
                        </div>
                        <div style="float: left; width: 15%">
                            Number of youths
                        </div>
                        <div style="float: left; width: 28%">
                            <asp:DropDownList ID="ddlYouth" runat="server" class="inputsl m-none" Width="90px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divleft">
                        Ticket type*
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtTicketType" runat="server" class="input" autocomplete="off" Style="margin-left: -3px;"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="red"
                            ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTicketType" ErrorMessage="Please enter ticket type."
                            Text="*" />
                    </div>
                    <div class="divleft">
                        Price*
                    </div>
                    <div class="divright">
                        <asp:TextBox ID="txtprice" ClientIDMode="Static" MaxLength="10" runat="server" class="input"
                            Style="margin-left: -3px;" autocomplete="off" CssClass="number" />
                        <%=CurrCode%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="red"
                            ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtprice" ErrorMessage="Please enter price."
                            Text="*" />
                        <asp:RegularExpressionValidator runat="server" ErrorMessage="Incorrect Price." Text="*"
                            ForeColor="Red" ControlToValidate="txtprice" ValidationExpression="\b(\d+(?:\.(?:[^0]\d|\d|\d|\d[1,2]))?)\b"
                            ValidationGroup="vgs" />
                        <%--ValidationExpression="\b(\d+(?:\.(?:[^0]\d|\d|\d|\d[^0]))?)\b"--%>
                    </div>
                    <div style="width: 100%;">
                        <div style="float: left; width: 25%">
                        </div>
                        <div style="float: right; width: 74%">
                            <asp:Button ID="btnCheckout" runat="server" Text="ADD" CssClass="button1" ValidationGroup="vgs"
                                Style="width: 15%!important; margin-top: 15px; float: right;" OnClick="btnCheckout_Click" />
                            <asp:ValidationSummary ID="VSum" ValidationGroup="vgs" DisplayMode="List" ShowSummary="false"
                                ShowMessageBox="true" runat="server" />
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div id="divlist" runat="server" style="margin-top: 10px!important;">
                    <asp:GridView ID="grdOrders" runat="server" AutoGenerateColumns="False" PageSize="10"
                        PagerStyle-CssClass="paging" CssClass="grid-head2" CellPadding="4" ForeColor="#333333"
                        GridLines="None" Width="100%" OnRowCommand="grdOrders_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Train No">
                                <ItemTemplate>
                                    <%#Eval("TrainNo")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Station From-To">
                                <ItemTemplate>
                                    <%#Eval("From") + " - " + Eval("To")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dept. Date">
                                <ItemTemplate>
                                    <%# Eval("DateTimeDepature", "{0: MMM dd, yyyy}") + " " + Eval("DepartureTime")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Arrival Date">
                                <ItemTemplate>
                                    <%# Eval("DateTimeArrival", "{0: MMM dd, yyyy}") + " " + Eval("ArrivalTime")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Class">
                                <ItemTemplate>
                                    <%# Eval("Class")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Passenger">
                                <ItemTemplate>
                                    <%# Eval("Passenger")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NetPrice">
                                <ItemTemplate>
                                    <%# Eval("NetPrice","{0:0.00}")%>&nbsp;<%=CurrCode%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CommissionFee">
                                <ItemTemplate>
                                    <%# Eval("CommissionFee", "{0:0.00}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <%#Eval("P2PType")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                    </asp:GridView>
                </div>
                <div style="clear: both;">
                </div>
                <div class="divMain" id="dvRef" runat="server" style="margin-top: 10px!important;">
                    <div id="agentreff" style="display: none;">
                        <div class="divleft">
                            Agent Reference
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtAgentRefNo" runat="server" class="input" MaxLength="45" />
                            <asp:RequiredFieldValidator ID="reqrefxxcc" runat="server" ForeColor="red" ValidationGroup="vgsRef"
                                Display="Dynamic" ControlToValidate="txtAgentRefNo" ErrorMessage="Please enter agent reference."
                                Text="*" />
                        </div>
                    </div>
                    <div class="" style="width: 100%;">
                        <div style="float: left; width: 25%">
                        </div>
                        <div style="float: right; width: 74%">
                            <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="button1" ValidationGroup="vgsRef"
                                Style="width: 15%!important; float: right; margin-top: 15px;" OnClick="btnContinue_Click" />
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="vgsRef" DisplayMode="List"
                                ShowSummary="false" ShowMessageBox="true" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
