﻿<%@ Page Title="Add Edit Roles" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ManageRoles.aspx.cs" Inherits="IR_Admin.AddEditRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function(){
            $('.chk-p-parent').change(function(){
                if($(this).parent().find(':checkbox').is(':checked'))
                {
                    var id=$(this).attr('title').substr(1,36);
                    var tableid='RepeaterTab_C'+id;
                    $('#'+tableid).find('span.chk-p-child').each(function(key,value){
                        $(value).children().attr('checked',true);
                    });
                }
                else{
                    var id=$(this).attr('title').substr(1,36);
                    var tableid='RepeaterTab_C'+id;
                    $('#'+tableid).find('span.chk-p-child').each(function(key,value){
                        $(value).children().attr('checked',false);
                    });
                }
            });
            $('.chk-v-parent').change(function(){
                if($(this).parent().find(':checkbox').is(':checked'))
                {
                    var id=$(this).attr('title').substr(1,36);
                    var tableid='RepeaterTab_C'+id;
                    $('#'+tableid).find('span.chk-v-child').each(function(key,value){
                        $(value).children().attr('checked',true);
                    });
                }
                else{
                    var id=$(this).attr('title').substr(1,36);
                    var tableid='RepeaterTab_C'+id;
                    $('#'+tableid).find('span.chk-v-child').each(function(key,value){
                        $(value).children().attr('checked',false);
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=tab.ToString()%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
            }
        });
    </script>
    <script type="text/javascript">
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <style type="text/css">
        .newheader {
            vertical-align: top;
            line-height: 19px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Roles</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="ManageRoles.aspx" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" NavigateUrl="#" CssClass=" " runat="server">New/Edit</asp:HyperLink></li>
        </ul>
        <div style="float: right">
            <asp:Button ID="btnExportToExcel" runat="server" CssClass="button" Text="Export To Excel"
                data-export="export" Width="159px" OnClick="btnExportToExcel_Click" />
        </div>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdRoles" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" DataKeyNames="RoleId" AutoGenerateColumns="False"
                            OnRowCommand="grdCountry_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" CssClass="newheader" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("RoleId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="RoleName" HeaderText="Role Name" ItemStyle-Width="22%" />

                                <asp:TemplateField HeaderText="Product Expiry" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgProductExpiryEmail" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActiveProductExpiryEmail" AlternateText="status"
                                            ImageUrl='<%#Eval("IsProductExpiryEmail").ToString()=="True" ?"images/active.png":"images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsProductExpiryEmail").ToString()=="True"?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%"></HeaderStyle>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Edit Booking Fee">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgIsEditableBookingFee" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="IsEditableBookingFee" AlternateText="status" ImageUrl='<%#Eval("IsEditableBookingFee").ToString()=="True" ?"images/active.png":"images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsEditableBookingFee").ToString()=="True"?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%"></HeaderStyle>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Allow 100% Refund">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActiveRefund" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActiveRefund" AlternateText="status" ImageUrl='<%#Eval("IsAllowHundredPercentRefund").ToString()=="True" ?"images/active.png":"images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsAllowHundredPercentRefund").ToString()=="True"?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sta Records">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActiveRole" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActiveRole" AlternateText="status" ImageUrl='<%#Eval("IsStaRecordsAllow").ToString()=="True" ?"images/active.png":"images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsStaRecordsAllow").ToString()=="True"?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Printing">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActivePrinting" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActivePrinting" AlternateText="status" ImageUrl='<%#Eval("IsPrintingAllow").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsPrintingAllow").ToString()=="True" ?"Printing allowed for this role":"Printing not allowed for this role" %>' /></div>
                                        <itemstyle width="9%" horizontalalign="Center"></itemstyle>
                                        <headerstyle width="9%"></headerstyle>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sysetm Report">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActiveSystemReport" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActiveSystemReport" AlternateText="status" ImageUrl='<%#Eval("SystemReport").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("SystemReport").ToString()=="True" ?"System reports allowed for this role":"System reports not allowed for this role" %>' /></div>
                                        <itemstyle width="10%" horizontalalign="Center"></itemstyle>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Allow Order Attachment">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActiveOrderAttachment" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActiveOrderAttachment" AlternateText="status"
                                            ImageUrl='<%#Eval("IsOrderAttachmentAllow").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsOrderAttachmentAllow").ToString()=="True" ?"Order Attachment allowed for this role":"Order Attachment not allowed for this role" %>' /></div>
                                        <itemstyle width="10%" horizontalalign="Center"></itemstyle>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--  <asp:TemplateField HeaderText="Allow Stock Search">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActiveAllowStockSearch" CommandArgument='<%#Eval("RoleId")%>'
                                            Height="16" CommandName="ActiveInActiveAllowStockSearch" AlternateText="status"
                                            ImageUrl='<%#Eval("IsAllowStockSearch").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsAllowStockSearch").ToString()=="True" ?"Stock Search allowed for this role":"Stock Search not allowed for this role" %>' /></div>
                                        <itemstyle width="10%" horizontalalign="Center"></itemstyle>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnEdit" runat="server" CommandArgument='<%#Eval("RoleId") %>'
                                            CommandName="Modify" ImageUrl="images/edit.png" ToolTip="Edit"></asp:ImageButton>&nbsp;
                                        <itemstyle width="9%"></itemstyle>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <EmptyDataTemplate>
                                No records found !
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table id="Maintable" class="tblMainSection">
                        <tr>
                            <td class="col">Role Name:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtRoleName" runat="server"></asp:TextBox>
                                &nbsp;<asp:RequiredFieldValidator ID="txtRoleNameRequiredFieldValidator" runat="server"
                                    CssClass="valdreq" SetFocusOnError="true" ErrorMessage="*" ControlToValidate="txtRoleName"
                                    ValidationGroup="RForm"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="col">Menu:
                            </td>
                            <td class="col">
                                <div style="height: 400px; overflow-y: scroll;">
                                    <div style="min-height: 380px;">
                                        <asp:Repeater ID="repParent" runat="server" OnItemDataBound="repParent_ItemDataBound">
                                            <ItemTemplate>
                                                <table width="600" id='RepeaterTab_P<%# DataBinder.Eval(Container.DataItem, "ID") %>'>
                                                    <tr class="tr-row">
                                                        <td style="width: 400px;">
                                                            <asp:Label ID="lblParentID" runat="server" Visible="false" Text='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("name") %>'></asp:Label></b>
                                                        </td>
                                                        <td style="width: 100px;">
                                                            <asp:CheckBox name="chkPPermissions" runat="server" class="CheckBoxClass chk-p-parent" ID="chkPPermissions"
                                                                ToolTip='<%#Getname("P"+Eval("ID").ToString())%>' value='<%# Eval("ID") %>' />
                                                            Permissions
                                                        </td>
                                                        <td style="width: 100px;">
                                                            <asp:CheckBox name="chkPVisible" runat="server" class="CheckBoxClass chk-v-parent" ID="chkPVisible"
                                                                ToolTip='<%#Getname("V"+Eval("ID").ToString())%>' value='<%# Eval("ID") %>' />
                                                            Visible
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <table width="600px" id="RepeaterTab_C<%# Eval("ID") %>">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="repChild" runat="server" OnItemDataBound="repChild_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <div style="margin-left: 33px;">
                                                                            <table width="600px" id="RepeaterTab">
                                                                                <tr>
                                                                                    <td align="left" style="width: 400px;">
                                                                                        <asp:HiddenField ID="hdnParentId" runat="server" Value='<%# Eval("ID") %>' />
                                                                                        <img src="schemes/images/grid-bullet.jpg" />
                                                                                        <%# Eval("name")%>
                                                                                    </td>
                                                                                    <td align="left" style="width: 100px;">
                                                                                        <asp:CheckBox name="chkCPermissions" runat="server" class="UCheckBoxClass chk-p-child" ID="chkCPermissions"
                                                                                            ToolTip='<%#Getname("Chk_P" + Eval("id").ToString())%>' data-title='<%# Eval("ParentId")%>'
                                                                                            value='<%# Eval("id")%>' />
                                                                                    </td>
                                                                                    <td align="left" style="width: 100px;">
                                                                                        <asp:CheckBox name="chkCvisible" runat="server" class="UCheckBoxClass chk-v-child" ID="chkCvisible"
                                                                                            ToolTip='<%#Getname("Chk_V" + Eval("id").ToString())%>' data-title='<%# Eval("ParentId")%>'
                                                                                            value='<%# Eval("id")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <asp:Repeater ID="repInnerChild" runat="server">
                                                                                <ItemTemplate>
                                                                                    <div style="margin-left: 33px;">
                                                                                        <table width="555px" id="RepeaterTab">
                                                                                            <tr>
                                                                                                <td align="left" style="width: 357px;">
                                                                                                    <img src="schemes/images/grid-bullet.jpg" alt="" />
                                                                                                    <%# Eval("name")%>
                                                                                                </td>
                                                                                                <td align="left" style="width: 98px;">
                                                                                                    <asp:CheckBox name="chkCPermissions" runat="server" class="UCheckBoxClass chk-p-child" ID="chkCPermissions"
                                                                                                        ToolTip='<%#Getname("ChkPP" + Eval("id").ToString())%>' data-title='<%# Eval("ParentId")%>'
                                                                                                        value='<%# Eval("id")%>' />
                                                                                                </td>
                                                                                                <td align="left" style="width: 85px;">
                                                                                                    <asp:CheckBox name="chkCvisible" runat="server" class="UCheckBoxClass chk-v-child" ID="chkCvisible"
                                                                                                        ToolTip='<%#Getname("ChkVV" + Eval("id").ToString())%>' data-title='<%# Eval("ParentId")%>'
                                                                                                        value='<%# Eval("id") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="BtnSubmit" runat="server" CssClass="button" OnClick="BtnSubmit_Click"
                                    ValidationGroup="RForm" Text="Submit" Width="120px" />
                                &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" Width="68px"
                                    OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <%--Export to excel--%>
        <asp:GridView ID="grdRoleExport" runat="server" CssClass="grid-head2" ForeColor="#333333"
            GridLines="None">
            <AlternatingRowStyle BackColor="#FBDEE6" />
            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                BorderColor="#FFFFFF" BorderWidth="1px" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        </asp:GridView>
    </div>
</asp:Content>
