﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RailPasses.aspx.cs" MasterPageFile="~/Site.master"
    Culture="en-GB" Inherits="RailPasses" %>

<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        li p
        {
            line-height: 20px;
        }
        .clsRailPassName
        {
            float: left !important;
            background: none !important;
            padding: 0px !important;
            font-size: 17px !important;
        }
        .clsRailPassDesp
        {
            background: none !important;
            font-size: 13px !important;
            color: #4d4d4d !important;
            padding: 0px !important;
        }
        .clsRpDesp
        {
            height: 100px;
            margin-top: 5px;
        }.clsImgSize{ width: 210px;height: 120px;}
    </style>
    <script type="text/javascript">
        function callthisevent() {
            $("#txtFrom , #txtTo").on('keydown', function (event) {
                //40,38
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;

                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Rail Passes  </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Rail Passes</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content " onmousemove="LoadCal()">
<h1>Rail Passes</h1>
<p>
    Below is a list of all the rail passes that we offer around the world, pick a pass and start your journey!
</p>
<br/>
     <div class="RailPasslist countrylistRp">
     <asp:Repeater ID="rptCat" runat="server" OnItemCommand="rptCat_ItemCommand" OnItemCreated="rptCat_OnItemCreated">
      <ItemTemplate>
            <asp:LinkButton ID="lnkCat" runat="server"  CommandName="RailPass" CommandArgument='<%#Eval("ID") %>'>
                <span>
                    <%#Eval("Name")%>
                </span>
            </asp:LinkButton>
      </ItemTemplate>
  </asp:Repeater>  
</div>
<div id="ulPasses" Visible="False" runat="server">
<ul id="Ul1" class="countrylist countrylist02" runat="server" >
<asp:Repeater ID="rptPasses" runat="server" OnItemDataBound="rptPasses_ItemDataBound" OnItemCommand="rptPasses_ItemCommand">
      <ItemTemplate>
          <li  style="background:none;">
          <a href="RailpassDetail.aspx?Id=<%#Eval("ID")%>">
           <div class="description" style="min-height: 144px;max-height: 144px"> 
                <img src='<%# (!string.IsNullOrEmpty(Convert.ToString(Eval("ProductImage")))?ConfigurationManager.AppSettings["HttpAdminHost"]+Eval("ProductImage"):"images/NoproductImage.png") %>' width="130" height="80" style="float:left;margin-right:3px;"/>
                <p class="clsRailPassName"><b><%#Eval("Name")%></b></p>
                <%# Eval("ProductDescription") !=null?((HttpContext.Current.Server.HtmlDecode((string)Eval("ProductDescription")).Length > 160) ? ((HttpContext.Current.Server.HtmlDecode((string)Eval("ProductDescription"))).Substring(0, 160) + "...") : (HttpContext.Current.Server.HtmlDecode((string)Eval("ProductDescription")))):""%>
           </div>         
           </a>  
          </li>
      </ItemTemplate>
      <FooterTemplate> 
        <asp:Label ID="lblerrmsg" runat="server" ForeColor="Red" Visible="false" Text="Record Not Found." Font-Size="12px"></asp:Label>
        <asp:LinkButton ID="lnkGotoback" CommandName="Redirect" Visible="false" ForeColor="Black" runat="server" Font-Size="12px" >Goto Back</asp:LinkButton> 
      </FooterTemplate>
  </asp:Repeater>
  </ul>
  </div>
    
<div class="clear"> &nbsp;</div>
 
<div class="smlblock">
    <img id="imgRail1" src="images/NoproductImage.png" runat="server" alt="" border="0" class="clsImgSize"/>
    <a href="RailPassSection1.aspx" style="text-decoration: none"><h1> <asp:Label ID="lblRailTitle1" CssClass="clsRailPassName" Text="RailPass1" runat="server"/> </h1>
    <div class="clsRpDesp">
        <p> <asp:Label ID="lblRailDesp1" runat="server"/> </p></div></a>
    <span>
    <a href="RailPassSection1.aspx"> Read more </a>
    </span>
</div>
<div class="smlblock marg-lr">
    <img id="imgRail2" src="images/NoproductImage.png" runat="server" alt="" border="0" class="clsImgSize"/>
    <a href="RailPassSection2.aspx" style="text-decoration: none"><h1> <asp:Label ID="lblRailTitle2" CssClass="clsRailPassName" Text="RailPass2" runat="server"/>  </h1>
    <div class="clsRpDesp">
        <p> <asp:Label ID="lblRailDesp2" CssClass="clsRailPassDesp" runat="server"/> </p></div></a>
    <span>
    <a href="RailPassSection2.aspx"> Read more </a>
    </span>
</div>
<div class="smlblock">
    <img id="imgRail3" src="images/NoproductImage.png" runat="server" alt="" border="0" class="clsImgSize"/>
    <a href="RailPassSection3.aspx" style="text-decoration: none"><h1> <asp:Label ID="lblRailTitle3" CssClass="clsRailPassName" Text="RailPass2" runat="server"/> </h1>
    <div class="clsRpDesp">
    <p> <asp:Label ID="lblRailDesp3" CssClass="clsRailPassDesp" runat="server"/> </p></div></a>
    <span >
    <a href="RailPassSection3.aspx"> Read more </a>
    </span>
</div>
</div>
 <div class="right-content" > 
<div class="ticketbooking" style="padding-top:0px" onclick="callthisevent()">
    <div class="list-tab divEnableP2P">
        <ul>
            <li><a href="#" class="active">Rail Tickets </a></li>
            <li><a href="rail-passes">Rail Passes </a></li>
        </ul>
    </div>
<uc1:TrainSearch ID="ucTrainSearch" runat="server" />
</div>
 
</div> 
</section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
