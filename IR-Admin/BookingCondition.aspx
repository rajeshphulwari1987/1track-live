﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="BookingCondition.aspx.cs"
    Inherits="IR_Admin.BookingCondition" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script src="editor/jquery.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtContract').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtBookRes').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtValidity').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtFarePay').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtTckRef').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtLostDam').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtBaggage').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtForce').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtGLaw').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtBoarding').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtTimeTable').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtVariations').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".grid-sec2").find(".cat-outer-cms").hide();

            $("#divCmsCon").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsCon").next().show();
            });

            $("#divCmsBook").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsBook").next().show();
            });

            $("#divCmsValid").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsValid").next().show();
            });

            $("#divCmsFare").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsFare").next().show();
            });

            $("#divCmsTck").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsTck").next().show();
            });

            $("#divCmsLost").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsLost").next().show();
            });

            $("#divCmsBag").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsBag").next().show();
            });

            $("#divCmsForce").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsForce").next().show();
            });

            $("#divCmsLaw").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsLaw").next().show();
            });

            $("#divCmsBoard").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsBoard").next().show();
            });

            $("#divCmsTime").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsTime").next().show();
            });

            $("#divCmsVar").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsVar").next().show();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Booking Conditions</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="BookingConditionList.aspx" class="">List</a></li>
            <li><a id="aNew" href="BookingCondition.aspx" class="current">New/Edit</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="panes">
                <div id="divNew" runat="server" style="display: block;">
                    <asp:HiddenField runat="server" ID="hdnProdNmId" />
                    <asp:HiddenField runat="server" ID="hdnBookId" />
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: right;" class="valdreq">
                                Fields marked with * are mandatory
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <legend><b>Description</b></legend>
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Title: <span style="color: red">* </span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTitle" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Width="450px"  Rows="10" Columns="5" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfDesc" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is Active ?
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:CheckBox ID="chkIsActv" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><b>Contents</b></legend>
                                    <div id="divCmsCon">
                                        The Contract
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtContract" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsBook">
                                        Bookings & Reservations
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtBookRes" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsValid">
                                        Validity
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtValidity" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsFare">
                                        Fares and payment
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtFarePay" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsTck">
                                        Changes to your ticket and refunds
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtTckRef" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsLost">
                                        Lost or Damaged Tickets
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtLostDam" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsBag">
                                        Baggage
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtBaggage" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsForce">
                                        Force Majeure
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtForce" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsLaw">
                                        Governing Law
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtGLaw" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsBoard">
                                        Boarding times
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtBoarding" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsTime">
                                        Timetable information
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtTimeTable" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsVar">
                                        Variations
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtVariations" runat="server"></textarea>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
