﻿<%@ Page Title="Cross Sale Adsense" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="CrossSaleAdsense.aspx.cs" Inherits="IR_Admin.CrossSaleAdsense" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">   
        function pageLoad(sender, args) {$('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 });}

        $(function () {                  
            if(<%=tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {              
                $("ul.tabs").tabs("div.inner-tabs-container > div");
            }         
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            $(".tabs a").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).parent("li").addClass("active");
            });
           
            if(<%=tab%>=="2")
            {
                $(".list a").click(function () {               
                    $("ul.list").tabs("div.panes > div");
                });
            }
            
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
                return false;
            return true;
        }
    </script>
    <style type="text/css">
        img
        {
            vertical-align: middle;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Cross Sale Adsense</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="CrossSaleAdsense.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CrossSaleAdsense.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdCrossSaleAdsense" runat="server" CellPadding="4" DataKeyNames="Id"
                        CssClass="grid-head2" OnRowCommand="grdCrossSaleAdsense_RowCommand" ForeColor="#333333"
                        GridLines="None" AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("Id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:TemplateField HeaderText="Price">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Currency+" "+Eval("Price") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ShortDescription" HeaderText="Short Description" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                        CommandArgument='<%#Eval("Id")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%# String.Format("{0}", Eval("Id")) %>' CommandName="Remove"
                                        ImageUrl="images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <!-- tab "panes" -->
                                <div class="inner-tabs-container">
                                    <div id="tabs-inner-1" class="grid-sec2" style="display: block; width: 700px;">
                                        <table class="tblMainSection">
                                            <tr>
                                                <td class="col">
                                                    Name
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Class
                                                </td>
                                                <td class="col">
                                                    <asp:DropDownList ID="ddlClass" runat="server"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqClass" runat="server" ControlToValidate="ddlClass"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" InitialValue="0"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Price
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtPrice" runat="server" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="reqPrice" runat="server" ControlToValidate="txtPrice"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    URL Link
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtUrl" runat="server" MaxLength="500"></asp:TextBox> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Short Description
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtSDescription" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqSDescription" runat="server" ControlToValidate="txtSDescription"
                                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col" style="vertical-align: top;">
                                                    Long Description
                                                </td>
                                                <td class="col">
                                                    <asp:TextBox ID="txtLDescription" Width="450px" runat="server" TextMode="MultiLine" Rows="10" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Banner Image
                                                </td>
                                                <td class="col">
                                                    <asp:FileUpload ID="fupBannerImg" runat="server" Width="200px" />
                                                    <asp:LinkButton ID="lnkBanner" runat="server" Text="View Banner Image" CssClass="clsLink"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Is Active
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkactive" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr align="center">
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="CForm" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="BtnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td class="col">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlBanner" runat="server" CssClass="popup pBannerImg">
        <div class="dvPopupHead">
            <div class="dvPopupTxt">
                Country Banner Image</div>
            <a href="#" id="btnClose3" runat="server" style="color: #fff;">X </a>
        </div>
        <div class="dvImg">
            <asp:Image ID="imgBanner" runat="server" ImageUrl="~/CMSImages/sml-noimg.jpg" />
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdpupBanner" runat="server" TargetControlID="lnkBanner"
        CancelControlID="btnClose3" PopupControlID="pnlBanner" BackgroundCssClass="modalBackground" />
</asp:Content>
