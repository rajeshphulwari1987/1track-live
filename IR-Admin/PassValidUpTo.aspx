﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="PassValidUpTo.aspx.cs" Inherits="IR_Admin.PassValidUpTo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">

        function DefltMiss(obj) {

            $("INPUT[type='radio']").each(function () {

                if (this.id != obj.children[0].id) {
                    this.checked = false;
                }
                else {
                    this.checked = true;
                }
            });

            return false;
        }     
        
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                Product Valid Up To</h2>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="#" class="current">List</a></li>
                    <li>
                        <asp:HyperLink ID="aNew" CssClass=" " NavigateUrl="#" runat="server">New/Edit</asp:HyperLink></li>
                </ul>
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divlist" runat="server" style="display: none;">
                            <div class="crushGvDiv">
                                <asp:GridView ID="grvProductValidUpTo" runat="server" AutoGenerateColumns="False"
                                    PageSize="10" CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None"
                                    Width="100%" OnPageIndexChanging="grvProductValidUpTo_PageIndexChanging" OnRowCommand="grvProductValidUpTo_RowCommand">
                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        Record not found.</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Class Name">
                                            <ItemTemplate>
                                                <%#Eval("Name")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="60%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Age">
                                            <ItemTemplate>
                                                <%#Eval("FromAge")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="15%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Age">
                                            <ItemTemplate>
                                                <%#Eval("ToAge")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="15%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                                    CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="~/images/edit.png" />
                                                <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                    CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                                <img height="16" src='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    title='<%#Eval("IsActive").ToString()=="True" ?"Active":"De-Active" %>' alt="status" /></div>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:HiddenField runat="server" ID="hdnId" />
                            </div>
                        </div>
                        <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                            <table class="tblMainSection">
                                <tr>
                                    <td style="width: 70%; vertical-align: top;">
                                        <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                            <div class="cat-inner-alt">
                                                <table style="width: 90%">
                                                    <tr>
                                                        <td>
                                                            Language:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" Enabled="False" />
                                                        </td>
                                                        <td>
                                                            Name:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtCatName" MaxLength="180" Text='<%#Eval("CategoryName")%>' />
                                                            <asp:RequiredFieldValidator ID="reqCat" runat="server" ErrorMessage="*" ValidationGroup="submit"
                                                                ControlToValidate="txtCatName" CssClass="valdreq" SetFocusOnError="True" />
                                                        </td>
                                                        <td>
                                                            Default When Missing:
                                                        </td>
                                                     <td style="text-align: left;">
                                                            <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                onchange="DefltMiss(this)" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Age From:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddlAgeFrom" Width="205px" />
                                                        </td>
                                                        <td>
                                                            Age To:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddlAgeTo" Width="205px" />
                                                        </td>
                                                       
                                                        <td>
                                                            Is Active ?
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:CheckBox runat="server" ID="chkIsActv" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptProductValidUpTo" runat="server" OnItemDataBound="rptProductValidUpTo_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="cat-inner">
                                                      <table style="width: 90%">
                                                        <tr>
                                                            <td>
                                                                Language:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" />
                                                                <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Name:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtCatName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Default When Missing:
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                    onchange="DefltMiss(this)" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <div class="cat-inner-alt">
                                                    <table style="width: 90%">
                                                        <tr>
                                                            <td>
                                                                Language:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" />
                                                                <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Name:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtCatName" MaxLength="180" Text='<%#Eval("CategoryName")%>' />
                                                            </td>
                                                            <td>
                                                                &nbsp; Default When Missing:
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                                    onchange="DefltMiss(this)" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                        <div style="float: right;">
                                            <asp:Button ID="btnAddMore" runat="server" CssClass="button" Text="+ Add More" 
                                                Width="89px" onclick="btnAddMore_Click"
                                           />
                                            &nbsp;
                                            <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="-- Remove" 
                                                Width="89px" onclick="btnRemove_Click"
                                                 />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="button"  
                                            Text="Submit" Width="89px" ValidationGroup="submit" 
                                            onclick="btnSubmit_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button"  
                                            Text="Cancel" onclick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
