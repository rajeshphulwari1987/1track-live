﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ContactInfo.aspx.cs"
    Inherits="IR_Admin.ContactInfo" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        .vtop{
            vertical-align: top;
            border: 1px solid #B3B3B3;
        }
    </style>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Contact Details
    </h2>
    <div class="full mr-tp1">
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server">
                    <div class="crushGvDiv1">
                        <div class="train-detail-in">
                            <asp:DataList ID="dtlContactInfo" runat="server" RepeatColumns="2" Width="100%" RepeatDirection="Horizontal"
                                BorderColor="#B3B3B3" BorderStyle="Solid" Font-Size="14px" OnItemCommand="dtlContactInfo_ItemCommand">
                                <ItemStyle CssClass="vtop"></ItemStyle>
                                <ItemTemplate>
                                    <div class="train-detail-block">
                                        <div class="trainblockleft" style="width: 450px">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Name:
                                                    </td>
                                                    <td>
                                                        <%#Eval("Name")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Email:
                                                    </td>
                                                    <td>
                                                        <div class="Titletext">
                                                            <%#Eval("Email")%></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Phone:
                                                    </td>
                                                    <td>
                                                        <%#Eval("Phone") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Order Number:
                                                    </td>
                                                    <td>
                                                        <%#Eval("OrderNumber") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <%#Eval("Description") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Action:
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                                            OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <table width="100%">
                                <tr class="paging">
                                    <asp:Repeater ID="DLPageCountItem" runat="server">
                                        <ItemTemplate>
                                            <td style="float: left">
                                                <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                    Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
