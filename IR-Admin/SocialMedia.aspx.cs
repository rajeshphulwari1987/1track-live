﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin
{
    public partial class SocialMedia : System.Web.UI.Page
    {
        ManageSocialMediaLink social = new ManageSocialMediaLink();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue); 
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            if (!IsPostBack) 
                FillGrid();
            
        }

        void FillGrid()
        {
            try
            {
                grvSocial.DataSource = social.GetScriptsList();
                grvSocial.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                AddEditScript();
                ShowMessage(1, "Scripts updated successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }

        }
        void AddEditScript()
        {
            
          
                foreach (GridViewRow row in grvSocial.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hdnSiteId = row.FindControl("hdnSiteId") as HiddenField;
                        TextBox txtScript = row.FindControl("txtScript") as TextBox;
                        var result = social.AddScirpts(new tblSocialMediaScript
                        {
                            CreatedBy = AdminuserInfo.UserID,
                            CreatedOn = DateTime.Now,
                            ID = Guid.NewGuid(),
                            Script = txtScript.Text.Trim(),
                            SiteID = Guid.Parse(hdnSiteId.Value)
                        });
                    }
                }
            
        }

        protected void grvSocial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "ActiveInActive")
                {
                    bool result = social.ActiveScirpt(id);
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}