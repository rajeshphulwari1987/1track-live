﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class CountryLinkedCountrys : System.Web.UI.Page
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindDataStartup(Guid.Empty);
        }
        public void BindDataStartup(Guid lastCountry)
        {
            try
            {
                var ddlcountry=new List<tblProductEurailCountire>();
                int levels = Convert.ToInt32(ddlCountrycombinationLevel.SelectedValue);
                var lstCountry = _db.tblProductEurailCountires.OrderBy(t => t.Country).ToList();
                if (lstCountry.Count > 0)
                {
                    ddlcountry.Add(new tblProductEurailCountire { Country = "All Countries", EurailCountryID = Guid.Empty });
                    ddlcountry.AddRange(lstCountry);
                    ddlCountrycombination.DataSource = ddlcountry; 
                    ddlCountrycombination.DataTextField = "Country";
                    ddlCountrycombination.DataValueField = "EurailCountryID";
                    ddlCountrycombination.DataBind();
                    ddlCountrycombination.SelectedValue = lastCountry.ToString();

                    Guid CID = Guid.Parse(ddlCountrycombination.SelectedValue);
                    if (CID == Guid.Empty)
                    {
                        chkcountryEnable.Visible = false;
                        var lstLink = _db.tblProductEurailCountiresFirstCountryLinkeds.Where(t => t.Levels == levels).ToList();
                        var lstRelation = lstCountry.Select(t => new
                        {
                            Country = t.Country,
                            EurailCountryID = t.EurailCountryID,
                            EnableLinkedCountry = lstLink.Any(x => x.EurailCountryID == t.EurailCountryID)
                        }).ToList();
                        rptcountryLinkedId.DataSource = lstRelation;
                        rptcountryLinkedId.DataBind();
                    }
                    else
                    {
                        chkcountryEnable.Checked = lstCountry.FirstOrDefault(t => t.EurailCountryID == CID).SelectPass.Value;
                        var lstLink = _db.tblProductEurailCountiresLinkedCountires.Where(t => t.EurailCountryID == CID && t.Levels == levels).ToList();
                        var lstRelation = lstCountry.Select(t => new
                        {
                            Country = t.Country,
                            EurailCountryID = t.EurailCountryID,
                            EnableLinkedCountry = lstLink.Any(x => x.EurailCountryLinkedID == t.EurailCountryID)
                        }).ToList();
                        rptcountryLinkedId.DataSource = lstRelation;
                        rptcountryLinkedId.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlCountrycombination_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid CID = Guid.Parse(ddlCountrycombination.SelectedValue);
            BindDataStartup(CID);
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int levels = Convert.ToInt32(ddlCountrycombinationLevel.SelectedValue);
            Guid CID = Guid.Parse(ddlCountrycombination.SelectedValue);
            if (CID == Guid.Empty)
            {
                _db.tblProductEurailCountiresFirstCountryLinkeds.Where(x => x.Levels == levels).ToList().ForEach(_db.tblProductEurailCountiresFirstCountryLinkeds.DeleteObject);
                foreach (RepeaterItem it in rptcountryLinkedId.Items)
                {
                    var chkEnable = it.FindControl("chkEnable") as CheckBox;
                    var hdncountryId = it.FindControl("hdncountryId") as HiddenField;
                    Guid LnkdCID = Guid.Parse(hdncountryId.Value);
                    if (chkEnable.Checked)
                    {
                        var obj = new tblProductEurailCountiresFirstCountryLinked();
                        obj.Id = Guid.NewGuid();
                        obj.EurailCountryID = LnkdCID;
                        obj.Levels = levels;
                        _db.tblProductEurailCountiresFirstCountryLinkeds.AddObject(obj);
                    }
                }
                _db.SaveChanges();
            }
            else
            {
                var countryobj = _db.tblProductEurailCountires.FirstOrDefault(t => t.EurailCountryID == CID);
                if (chkcountryEnable.Checked)
                    countryobj.SelectPass = true;
                else
                    countryobj.SelectPass = false;

                foreach (RepeaterItem it in rptcountryLinkedId.Items)
                {
                    Guid productid = Guid.Empty;
                    var chkEnable = it.FindControl("chkEnable") as CheckBox;
                    var hdncountryId = it.FindControl("hdncountryId") as HiddenField;
                    Guid LnkdCID = Guid.Parse(hdncountryId.Value);

                    var data = _db.tblProductEurailCountiresLinkedCountires.FirstOrDefault(t => t.EurailCountryID == CID && t.EurailCountryLinkedID == LnkdCID && t.Levels == levels);
                    if (data != null && !chkEnable.Checked)
                    {
                        _db.tblProductEurailCountiresLinkedCountires.Where(t => t.EurailCountryID == CID && t.EurailCountryLinkedID == LnkdCID && t.Levels == levels).ToList().ForEach(_db.tblProductEurailCountiresLinkedCountires.DeleteObject);
                    }
                    else if (chkEnable.Checked)
                    {
                        tblProductEurailCountiresLinkedCountire obj = new tblProductEurailCountiresLinkedCountire();
                        obj.ID = Guid.NewGuid();
                        obj.EurailCountryID = CID;
                        obj.EurailCountryLinkedID = LnkdCID;
                        obj.Levels = levels;
                        _db.tblProductEurailCountiresLinkedCountires.AddObject(obj);
                    }
                }
                _db.SaveChanges();
            }
            BindDataStartup(CID);
        }

        protected void ddlCountrycombinationLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Guid CID = Guid.Parse(ddlCountrycombination.SelectedValue);
            BindDataStartup(Guid.Empty);
        }
    }
}