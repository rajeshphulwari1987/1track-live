﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_OmegaTravelHomePage.aspx.cs"
    Inherits="IR_Admin.Manage_OmegaTravelHomePage" %>

<%@ Register Src="usercontrol/BannerImageManager.ascx" TagName="BannerImageManager"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title>STA Rail | STA Travel Rail</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="Styles/omegacss/assets/css/main-agent.css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:400" rel="stylesheet"
        type="text/css">
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="//www.statravel.com/static/us_division_web_live/css/jquery.mmenu.all.css"
        type="text/css" />
    <link href="//www.statravel.com/static/us_division_web_live/css/partner.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/travelcutscss/assets/img/icons/touch-icon.png" rel="apple-touch-icon-precomposed"
        sizes="180x180" type="image/png" />
    <link href="Styles/omegacss/assets/img/icons/favicon.png" rel="shortcut icon" type="image/png" />
    <link href="Styles/omegacss/assets/img/icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="Styles/omegacss/assets/css/layout.css" rel="stylesheet" />
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
        .checkvalue
        {
            padding: 0px;
        }
        #sta-top-header a:hover, #sta-newsletter a:hover, #sta-header a:hover, #sta-footer a:hover
        {
            outline: 0;
            color: #29166f;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent').redactor({ iframe: true, minHeight: 200 });

            //Edit banner images
            $(".editbtn1").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });
            $(".editbtn2").click(function () {
                $("#divHeading").slideToggle("slow");
            });
            $(".editbtn3").click(function () {
                $("#divContent").slideToggle("slow");
            });
            $("#btnUploadBanner").click(function () {   //open upload banner div 
                $("#divUploadBanner").slideToggle("slow");
            });


            //----------Edit heading---------//
            $(".editbtn2").click(function () {
                var value;
                $("#divHeading").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdHead').contents().find('body').html(value);
            });

            $(".editbtn3").click(function () {
                var value;
                $("#divContent").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent').contents().find('body').html(value);
            });


            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divcloseBanner") {  //close upload banner div 
                    $("#divUploadBanner").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <input type='hidden' id='sta-page-responsive' value='true' />
    <div id='sta-full-wrap'>
        <div id='sta-page-wrap'>
            <header class="sta-clearfix sta-header" role="banner" id="sta-header">
                <div class="starail-Nav-mobile starail-u-hideDesktop">
                </div>
                <div class="starail-Outer-wrap">
                    <div class="js-starail-nav-trigger starail-u-hideDesktop starail-Nav-mobileClose">
                    </div>

                    <div class='starail-Full-wrap'>
                        <div class='starail-Page-wrap'>
                            <header class="starail-clearfix starail-Header starail-u-hideMobile" role="banner">
                                <div class="starail-u-hideMobile starail-u-cf starail-Header-topRow" itemscope itemtype="http://schema.org/Organization">
                                    <div class="starail-Header-logo">
                                        <a itemprop="url" href="#">
                                              <img src="Styles/omegacss/assets/img/omegalogo.jpg" />
                                        </a>
                                    </div>

                                    <div class="starail-Header-contact">
                                        <p class="starail-Header-contact-title">Omega Travel</p>
                                        <h2 class="starail-Header-contact-phone" itemprop="telephone">1.800.667.2887</h2>
                                        <p class="starail-Header-contact-disclaimer"></p>
                                    </div>
                                </div>
                                <nav class="starail-u-cf starail-Nav-wrap" id="sta-nav-wrap">
                                    <ul class="starail-u-cf starail-Nav" role="navigation" id="starail-nav">
                                        <li>
                                            <a href="#"> Rail Home   </a>
                                        </li>
                                        <li><a href="#">Print Queue </a> </li>
                                        <li><a href="#">FOC / AD75</a></li>
                                        <li>
                                            <a href="#"> FAQ   </a>
                                        </li>
                                        <li>
                                            <a href="#"> Feedback </a>
                                        </li>
                                        <li><a href="#"> Security  </a></li>
                                    </ul>
                                </nav>
                            </header>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: STA Global Header -->
            <main class="starail-Wrapper starail-Wrapper--main" role="main">

                <div class="starail-Grid starail-Grid--mobileFull">
                    <div class="starail-Grid-col starail-Grid-col--nopadding editbaner">

                        <div class="editbtn1">
                            <a class="edit-btn" href="#">Edit</a>
                        </div>
                        <div class="editbtn2">
                            <a class="edit-btn" href="#">Edit</a>
                        </div>
                        <div class="editbtn3">
                            <a class="edit-btn" href="#">Edit</a>
                        </div>

                        <div class="starail-Section starail-HomeHero starail-Section--nopadding">
                            <img class="starail-HomeHero-img" src="http://lorempixel.com/960/300/nature" alt="Home hero" id="imgMainBanner" runat="server"/>

                            <div class="starail-HomeHero-search">
                              <div id="ContentHead" runat="server">  <h2 class="starail-HomeHero-title">Where do you want to go?</h2></div>

                                <div class="starail-HomeHero-form" action="#" name="search" method="get" target="_top" id="search">
                                    <input class="starail-Form-input starail-HomeHero-formInput" type="text" name="search" id="search" placeholder="Enter a continent or country" value="" />
                                    <button class="starail-Button starail-HomeHero-formSubmit" type="submit">
                                        <i class="starail-Icon starail-Icon-magglass"></i>
                                        <span class="starail-u-visuallyHidden">Go</span>
                                    </button>
                                </div>

                                <p class="starail-HomeHero-allLink">
                                    <a href="#"><span  id="ContentText" runat="server">See all the Countries you could explore</span></a>
                                </p>
                            </div>


                        </div>
                        <div class="starail-Section starail-Section--nopadding">
                            <div style="display:none;">
                                <div class="starail-Switcher">

                                    <ul class="starail-Switcher-tabs">
                                        <li class="starail-Switcher-tab starail-Switcher-tab--active">
                                            <a href="#starail-passes" class="js-starail-Switcher-trigger">Rail Passes</a>
                                        </li>
                                        <li class="starail-Switcher-tab">
                                            <a href="#starail-tickets" class="js-starail-Switcher-trigger">Rail Tickets</a>
                                        </li>
                                    </ul>

                                    <div id="starail-passes" class="starail-Switcher-content">


                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">

                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>


                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">
                                                        From £89/pp!
                                                        <br /> Find out more
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">

                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">

                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>


                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">
                                                        From £89/pp!
                                                        <br /> Find out more
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>


                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">
                                                        From £89/pp!
                                                        <br /> Find out more
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/city" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title</span>
                                                </p>
                                            </div>


                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipise.</p>
                                                    <div class="starail-Button">
                                                        From £89/pp!
                                                        <br /> Find out more
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="starail-ImageLink" href="#">

                                            <div class="starail-Tag starail-Tag--specialOffer">
                                            </div>

                                            <img src="http://lorempixel.com/220/220/nature" />

                                            <div class="starail-ImageLink-titleContainer">
                                                <p class="starail-ImageLink-title">
                                                    <span class="highlight">Rail Pass Title on two lines</span>
                                                </p>
                                            </div>
                                            <div class="starail-ImageLink-overlay">
                                                <div class="starail-ImageLink-overlay-content">
                                                    <p>This is a hover state. Lorem ipsum dolor sit amet stetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                                    <div class="starail-Button">Find out more</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div id="starail-tickets" class="starail-Switcher-content starail-Switcher-content--hidden">
                                        <div class="starail-SearchTickets">
                                            <h2 class="starail-SearchTickets-title starail-u-alpha">Tell us <span class="starail-u-hideMobile">more </span>about your trip</h2>
                                            <div class="starail-Box starail-Box--whiteMobile starail-Box--noPaddingBottomMobile">
                                                <div action="#" method="post" class="starail-Form starail-Form--onBoxNarrow starail-SearchTickets-form">
                                                    <div class="starail-Form-row starail-SearchTickets-destination">
                                                        <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--start">
                                                            <label for="starail-startlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">From</label>
                                                            <div class="starail-SearchTickets-destinationInput">
                                                                <input class="starail-Form-input" type="text" name="starail-startlocation" id="starail-startlocation" placeholder="Enter a start location" value="" />
                                                            </div>
                                                        </div>
                                                        <div class="starail-DestinationIcon">
                                                            <div class="starail-DestinationIcon-line"></div>
                                                            <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled"></div>
                                                            <div class="starail-DestinationIcon-circle"></div>
                                                        </div>



                                                        <div class="starail-SearchTickets-destination-row starail-SearchTickets-destination-row--end">
                                                            <label for="starail-endlocation" class="starail-Form-label starail-u-visuallyHiddenMobile">To</label>
                                                            <div class="starail-SearchTickets-destinationInput">
                                                                <input class="starail-Form-input" type="text" name="starail-endlocation" id="starail-endlocation" placeholder="Enter a destination" value="" />
                                                            </div>
                                                        </div>

                                                        <a href="#" class="starail-SearchTickets-switch-trigger"><i class="starail-Icon starail-Icon-reverse"></i><span class="starail-u-visuallyHidden">Switch direction</span></a>

                                                    </div>

                                                    <div class="starail-Form-row">

                                                        <label for="" class="starail-Form-label starail-u-invisible starail-u-visuallyHiddenMobile">Journey Type</label>

                                                        <div class="starail-u-cf starail-Form-switchRadioGroup">

                                                            <label for="starail-oneway">
                                                                <input type="radio" id="starail-oneway" name="journey" value="one way" checked>
                                                                <span>One way</span>
                                                            </label>


                                                            <label for="starail-return">
                                                                <input type="radio" id="starail-return" name="journey" value="return">
                                                                <span>Return</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-u-cf">
                                                        <label for="leaving" class="starail-Form-label">Leaving</label>

                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                                            <div class="starail-Form-datePicker">
                                                                <input class="starail-Form-input" name="leaving" id="leaving" placeholder="DD/MM/YYYY" value="" />
                                                                <i class="starail-Icon-datepicker"></i>
                                                            </div>
                                                            <select class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last" name="outbound-time" id="outbound-time">
                                                                <option value="09:00">09:00</option>
                                                                <option value="09:15">09:15</option>
                                                                <option value="09:30">09:30</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-u-cf">
                                                        <label for="returning" class="starail-Form-label">Returning</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">

                                                            <div class="starail-Form-datePicker">
                                                                <input class="starail-Form-input" type="text" name="returning" id="returning" placeholder="DD/MM/YYYY" value="" />
                                                                <i class="starail-Icon-datepicker"></i>
                                                            </div>

                                                            <select class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last" name="inbound-time" id="inbound-time">
                                                                <option value="09:00">09:00</option>
                                                                <option value="09:15">09:15</option>
                                                                <option value="09:30">09:30</option>
                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                                                        <label for="" class="starail-Form-label">Who's going?</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-adult" id="starail-adult">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Adults (26-65 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-children" id="starail-children">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-children">Children (under 17 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-youths" id="starail-youths">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Youths (17-25 at time of travel)</label>
                                                            </div>

                                                            <div class="starail-Form-inputContainer-col">
                                                                <select class="starail-Form-select starail-Form-select--narrow" name="starail-seniors" id="starail-seniors">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                                <label for="starail-adult">Seniors (over 66 at time of travel)</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />

                                                    <div class="starail-Form-row">
                                                        <button type="submit" class="starail-Button starail-Form-button starail-Form-button--primary">Search Tickets</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="starail-Box starail-Box--whiteMobile starail-ContactForm">
                                                <h3>We don't have that journey online</h3>
                                                <p>Call us now on <strong>0871 984 7783</strong> to book! Or fill in some details and we'll call you back:</p>


                                                <div action="#" method="post" class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
                                                    <div class="starail-Form-row">
                                                        <label for="starail-firstname" class="starail-Form-label">
                                                            Name <span class="starail-Form-required">*</span>
                                                        </label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input class="starail-Form-input" type="text" name="starail-firstname" id="starail-firstname" placeholder="First name" value="" />
                                                            </div>
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input class="starail-Form-input" type="text" name="starail-lastname" id="starail-lastname" placeholder="Last name" value="" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <label for="starail-email" class="starail-Form-label">
                                                            Email <span class="starail-Form-required">*</span>
                                                        </label>
                                                        <div class="starail-Form-inputContainer">
                                                            <input class="starail-Form-input starail-Form-error" type="email" name="starail-email" id="starail-email" placeholder="Email" value="" />
                                                            <!-- .starail-Form-error class used for errors -->
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <label for="starail-phone" class="starail-Form-label">
                                                            Phone <span class="starail-Form-required">*</span>
                                                        </label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div>
                                                                <input class="starail-Form-input" type="tel" name="starail-phone" id="starail-phone" placeholder="Phone" value="" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row starail-ContactForm-contactPreference">
                                                        <label for="" class="starail-Form-label starail-ContactForm-contactPreferenceLabel">How would you prefer us to contact you?</label>
                                                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input type="radio" id="starail-preference-email" name="starail-contact-preference" value="email" checked>
                                                                <label for="starail-preference-email">Email</label>
                                                            </div>
                                                            <div class="starail-Form-inputContainer-col">
                                                                <input type="radio" id="starail-preference-phone" name="starail-contact-preference" value="phone">
                                                                <label for="starail-preference-phone">Phone</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="starail-Form-row">
                                                        <button type="submit" class="starail-Button starail-Form-button">Help Me Book!</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="Styles/travelcutscss/assets/img/Travelhome.jpg" />
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <!-- END: STA Global Footer -->
    <script type="text/javascript">
        window.jQuery || document.write('<script src="Styles/travelcutscss/assets/js/vendor/jquery-1.11.1.min.js"><\/script>')
    </script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"
        type="text/javascript"></script>
    <!-- STA GLOBAL BOTTOMJS -->
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/partner.js"
        type="text/javascript"></script>
    <script type="text/javascript" src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.hammer.min.js"></script>
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.mmenu-partner.js"
        type="text/javascript"></script>
    <!-- /STA GLOBAL BOTTOMJS -->
    <script src="Styles/omegacss/assets/js/main.min.js" type="text/javascript"></script>
    <footer class="starail-Footer">
        <div class="starail-Footer-outerWrap">
            <div class="starail-Footer-wrap">
               <section class="starail-Footer-links">
                    <p>
                        <a href='#'> Home </a>  <span>| </span>
                        <a href='#'> Contact Us </a>  <span>| </span>
                        <a href='#'> About Us </a>  <span>| </span>
                        <a href='#'> Booking Conditions </a>  <span>| </span>
                        <a href='#'> Privacy Policy </a>  <span>| </span>
                        <a href='#'> Conditions of Use </a>  <span> </span>
                    </p>
                </section>
                <p>
                    © Copyright International Rail Ltd. 2015 - All rights reserved. A company registered in England and Wales, company number: 3060803 with registered offices at International Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire. SO53 4AR.
                </p>
            </div>
        </div>
    </footer>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 141px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 300px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server"
                        class="checkvalue" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="padding-right: 50px; float: right; padding-top: 20px;">
            <input type="button" id="btnUploadBanner" value="Upload Banner" class="button" />
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 240px; top: 141px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Search Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 240px; top: 141px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Country Search Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divUploadBanner" class="PopUpSample" style="display: none; width: 850px;
        height: 450px; left: 44px; top: 142px; position: fixed; z-index: 99999 !important;">
        <uc1:BannerImageManager ID="ucBannerImageManager" runat="server" />
    </div>
    </form>
</body>
</html>
