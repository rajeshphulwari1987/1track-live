﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;

namespace IR_Admin
{
    public partial class P2PStation : Page
    {
        readonly private ManageBooking _master = new ManageBooking();
        readonly Masters _oMaster = new Masters();
        public string Tab = string.Empty;
        string Bannerpath = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                ShowMessage(0, null);
                BindSite();
                BindGrid(_SiteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetP2PCountryForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            Tab = "1";
            grdP2PCountry.DataSource = _master.GetP2PCountry(_SiteID);
            grdP2PCountry.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdP2PCountry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdP2PCountry.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }

        protected void grdP2PCountry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactiveP2PCountry(id);
                    Tab = "1";
                    ViewState["tab"] = "1";
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
                else if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteP2PCountry(id);
                    if (res)
                        ShowMessage(1, "Station has been deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = _oMaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddCountry()
        {
            try
            {
                _master.AddP2PCountry(new tblP2PCountry
                {
                    Id = Guid.NewGuid(),
                    CountryName = txtCountryName.Text.Trim(),
                    PageContent = txtDesc.Text,
                    SiteId = Guid.Parse(ddlSite.SelectedValue),
                    IsActive = chkIsActve.Checked,
                    Logo = Bannerpath.Replace("~/", "")
                });
                ShowMessage(1, "Station has been added successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdateCountry()
        {
            try
            {
                var id = Guid.Parse(Request["id"]);
                var txt = new tblP2PCountry
                {
                    Id = id,
                    CountryName = txtCountryName.Text.Trim(),
                    PageContent = txtDesc.Text,
                    SiteId = Guid.Parse(ddlSite.SelectedValue),
                    IsActive = chkIsActve.Checked,
                };
                if (!string.IsNullOrEmpty(Bannerpath))
                    txt.Logo = Bannerpath.Replace("~/", "");

                _master.UpdateP2PCountry(txt);
                ShowMessage(1, "Station has been updated successfully.");
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UploadFile();
                if (Request.QueryString["id"] == null)
                    AddCountry();
                else
                    UpdateCountry();
                Tab = "1";
                ClearControls();
                _SiteID = Master.SiteID;
                SiteSelected();
                BindGrid(_SiteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlSite.SelectedIndex = 0;
            txtCountryName.Text = string.Empty;
            txtDesc.Text = string.Empty;
            chkIsActve.Checked = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("P2PStation.aspx");
        }

        public void GetP2PCountryForEdit(Guid id)
        {
            tblP2PCountry oP = _master.GetP2PCountryById(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = oP.SiteId.ToString();
                txtCountryName.Text = oP.CountryName;
                txtDesc.Text = oP.PageContent;
                chkIsActve.Checked = oP.IsActive;
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                string[] ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupImg.HasFile)
                {
                    if (fupImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }
                    string fileName = fupImg.FileName.Substring(fupImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    Bannerpath = "~/Uploaded/P2PStations/";
                    Bannerpath = Bannerpath + oCom.CropImage(fupImg, Bannerpath, 181, 350);
                }
                return true;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); return false; }
        }
    }
}