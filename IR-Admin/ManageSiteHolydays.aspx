﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ManageSiteHolydays.aspx.cs" Inherits="IR_Admin.ManageSiteHolydays"
    Culture="en-AU" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .panes div
        {
            display: block;
            font-size: 14px;
            padding: 0 1px;
        }
        .ajax__calendar .ajax__calendar_day
        {
            border: 1px solid #FFFFFF;
            height: 16px;
            line-height: 16px;
            font-size: 12px;
            padding: 0;
        }
        .ajax__calendar_title
        {
            cursor: pointer;
            font-weight: bold;
            height: 22px;
            line-height: 22px;
            margin-left: 15px;
            margin-right: 15px;
            font-size: 12px;
        }
        
        .ajax__calendar .ajax__calendar_dayname
        {
            border-bottom: 1px solid #F5F5F5;
            height: 22px;
            line-height: 22px;
            font-size: 12px;
        }
        .ajax__calendar .ajax__calendar_footer
        {
            border-top: 1px solid #F5F5F5;
            height: 22px;
            line-height: 22px;
            font-size: 12px;
        }
        .ajax__calendar_month
        {
            cursor: pointer;
            height: 40px;
            line-height: normal !important;
            overflow: hidden;
            text-align: center;
            width: 35px;
            padding: 0px !important;
            font-size: 12px;
        }
        .ajax__calendar .ajax__calendar_year
        {
            border: 1px solid #FFFFFF;
            padding: 0px !important;
            height: 40px;
            line-height: normal !important;
            overflow: hidden;
            text-align: center;
            width: 35px;
            font-size: 12px;
        }
    </style>
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_txtDate").keypress(function (event) { event.preventDefault(); });
        });

        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Holidays</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ManageSiteHolydays.aspx" class="current">List</a></li>
            <li><a id="aNew" href="ManageSiteHolydays.aspx?status=add" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdHolyday" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="grdHolyday_PageIndexChanging" OnRowCommand="grdHolyday_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Holiday">
                                <ItemTemplate>
                                    <%#Eval("HolidayName")%>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date of Holiday">
                                <ItemTemplate>
                                    <%#Eval("DateofHoliday","{0:dd/MM/yyyy}")%>
                                </ItemTemplate>
                                <ItemStyle Width="30%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='ManageSiteHolydays.aspx?id=<%#Eval("ID")%>'>
                                        <img src="images/edit.png" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Holiday Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtHoliday" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfHName" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtHoliday" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Holiday Date:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDate" runat="server" Width="250px" />
                                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDate"
                                                            PopupButtonID="imgCalender1" Format="dd/MM/yyyy" PopupPosition="BottomLeft" />
                                                        <asp:Image ID="imgCalender1" runat="server" ImageUrl="~/images/icon-calender.png"
                                                            BorderWidth="0" AlternateText="Calendar" Style="position: absolute; margin: -2px 5px;
                                                            0 0;" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtDate" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is Active:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkIsActive" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
