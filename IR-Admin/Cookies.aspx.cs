﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Cookies : Page
    {
        readonly private ManageCookie _master = new ManageCookie();
        readonly Masters _oMaster = new Masters();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                BindGrid(_SiteID);

                BindSite();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetPrivacyForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
            if (IsPostBack)
            {
                _SiteID = Master.SiteID;
                BindGrid(_SiteID);
            }
        }

        public void BindSite()
        {
            ddlSite.DataSource = _oMaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void BindGrid(Guid _SiteID)
        {
            Tab = "1";
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;

            grdPolicy.DataSource = _master.GetCookieList(_SiteID);
            grdPolicy.DataBind();
        }

        protected void grdPolicy_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPolicy.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            BindGrid(_SiteID);
        }

        protected void grdPolicy_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeleteCookiePolicy(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;

                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                _master.AddCookie(new tblCookie
                {
                    ID = Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid(),
                    Name = txtTitle.Text.Trim(),
                    Description = txtDesc.InnerHtml,
                    Keywords = txtKeyword.Text.Trim(),
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    SiteId = Guid.Parse(ddlSite.SelectedValue)
                });

                if (Request["id"] != null)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, Request["id"] == null ? "Cookie policy added successfully." : "Cookie policy updated successfully.");
                ClearControls();
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        public void ClearControls()
        {
            txtTitle.Text = string.Empty;
            txtDesc.InnerHtml = string.Empty;
            txtKeyword.Text = string.Empty;
            ddlSite.SelectedIndex = 0;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Cookies.aspx");
        }

        public void GetPrivacyForEdit(Guid id)
        {
            var oCookie = _master.GetCookieById(id);
            if (oCookie != null)
            {
                ddlSite.SelectedValue = oCookie.SiteId.ToString();
                txtTitle.Text = oCookie.Name;
                txtDesc.InnerHtml = oCookie.Description;
                txtKeyword.Text = oCookie.Keywords;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}