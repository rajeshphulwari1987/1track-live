﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Drawing;
using Image = System.Drawing.Image;
using System.Text.RegularExpressions;

namespace IR_Admin.CorporateSite
{
    public partial class CorpOffice : System.Web.UI.Page
    {
        public string tab = string.Empty;
        readonly Masters _Master = new Masters();
        readonly ManageCorpOffice corpoffice = new ManageCorpOffice();
        private int width, imgWd;
        private int height, imgHt;
        private int x, y = 0;
        readonly ManageCorp objManageCorp = new ManageCorp();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            tab = Request.QueryString["id"] != null ? "2" : "1";


            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                FillSiteMenu();
                FillCountryDDl();
                BindParentOffice();
                BindOfficeList();
                FillOfficeDetailForEdit();
            }
            _SiteID = Master.SiteID;
        }

        void BindParentOffice()
        {
            Guid parentId = Request.QueryString["id"] != null ? Guid.Parse(Request.QueryString["id"].ToString()) : Guid.Empty;
            ddlOffice.DataSource = corpoffice.GetOfficeDDLList(_SiteID, parentId);
            ddlOffice.DataTextField = "OfficeName";
            ddlOffice.DataValueField = "ID";
            ddlOffice.DataBind();
            ddlOffice.Items.Insert(0, new ListItem("--Select office--", "-1"));
        }

        void FillSiteMenu()
        {
            trSites.Nodes.Clear();
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList().Where(x => x.IsCorporate).ToList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        void FillCountryDDl()
        {
            try
            {
                ddlCountry.DataSource = _Master.GetCountryList();
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("--Country--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void BindOfficeList()
        {
            grdOffice.DataSource = corpoffice.GetOfficeList(_SiteID);
            grdOffice.DataBind();
        }

        void FillOfficeDetailForEdit()
        {
            if (Request.QueryString["id"] != null)
            {
                var result = corpoffice.GetOfficeDetailById(Guid.Parse(Request.QueryString["id"]));
                txtAddress1.Text = result.Address1;
                txtAddress2.Text = result.Address2;
                txtCity.Text = result.City;
                txtDescription.Text = result.Description;
                ddlCountry.SelectedValue = result.Country.ToString();
                txtDescription.Text = result.Description;
                txtEmail.Text = result.Email;
                ddlOffice.SelectedValue = result.ParentBranchID.ToString();
                txtEmail.Text = result.Email;
                txtTelephone.Text = result.Telephone;
                txtCounty.Text = result.County;
                txtTown.Text = result.Town;
                txtOfficeName.Text = result.OfficeName;
                txtPostCode.Text = result.Postcode;
                chkActive.Checked = result.IsActive.Value;
                txtTitle.Text = result.LogoTitle;
                ddlColor.SelectedValue = ddlColor.Items.FindByText(Convert.ToString(result.Color) == null ? "-- Select Color --" : Convert.ToString(result.Color)).Value;
                if (!string.IsNullOrEmpty(result.LogoPath))
                {
                    trImg.Visible = true;
                    imgFile.ImageUrl = SiteUrl + result.LogoPath;
                }

                foreach (var lsites in corpoffice.GetBranchLookupSites(Guid.Parse(Request.QueryString["id"])).ToList())
                {
                    foreach (TreeNode pitem in trSites.Nodes)
                    {
                        if (Guid.Parse(pitem.Value) == lsites.SiteID)
                            pitem.Checked = true;
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    tab = "2";
                    return;
                }

                var res = corpoffice.AddEditCorpOffice(new tblBranch
                {
                    OfficeName = txtOfficeName.Text.Trim(),
                    Address1 = txtAddress1.Text.Trim(),
                    Address2 = txtAddress2.Text.Trim(),
                    Town = txtTown.Text.Trim(),
                    City = txtCity.Text.Trim(),
                    Country = Guid.Parse(ddlCountry.SelectedValue),
                    Description = txtDescription.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    ID = Request.QueryString["id"] == null ? Guid.NewGuid() : Guid.Parse(Request.QueryString["id"]),
                    ParentBranchID = ddlOffice.SelectedValue == "-1" ? Guid.Empty : Guid.Parse(ddlOffice.SelectedValue),
                    County = txtCounty.Text.Trim(),
                    Telephone = txtTelephone.Text.Trim(),
                    Postcode = txtPostCode.Text.Trim(),
                    LogoTitle = txtTitle.Text.Trim(),
                    LogoPath = Session["logoUpload"] != null ? "CorporateSite/CorpLogo/" + Session["logoUpload"] : null,
                    IsActive = chkActive.Checked,
                    Color = ddlColor.SelectedValue == "0" ? null : ddlColor.SelectedItem.Text
                });

                corpoffice.DeleteCorpSiteOffice(res);
                foreach (TreeNode node in trSites.Nodes)
                {
                    if (node.Checked)
                    {
                        var branchLookSites = new tblBranchLookupSite { ID = Guid.NewGuid(), BranchID = res, SiteID = Guid.Parse(node.Value) };
                        corpoffice.AddCorpBranchLookupSites(branchLookSites);
                    }
                }

                ShowMessage(1, Request.QueryString["id"] == null ? "Office created successfully." : "Office updated successfully.");
                Response.Redirect("CorpOffice.aspx");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CorpOffice.aspx");
        }

        protected void grdOffice_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "Remove")
                {
                    bool result = corpoffice.DeleteOffice(id);
                    if (result)
                        ShowMessage(1, "Record has been deleted successfully.");
                }
                if (e.CommandName == "ActiveInActive")
                {
                    bool result = corpoffice.ActiveInActiveOffice(id);
                }
                BindOfficeList();
                tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnUp_Click(object sender, EventArgs e)
        {
            if (imgUpload.HasFile)
            {
                try
                {
                    tab = "2";
                    string imgType = Path.GetExtension(imgUpload.FileName);
                    if (imgType == ".jpg" || imgType == ".jpeg" || imgType == ".png" || imgType == ".bmp")
                    {
                        using (var myImage = Image.FromStream(imgUpload.PostedFile.InputStream))
                        {
                            imgWd = myImage.Width;
                            imgHt = myImage.Height;
                        }

                        if (imgWd < width || imgHt < height)
                            ShowMessage(1, "Image Size should be equal or more than " + width + "*" + height);
                        else
                        {
                            string fileName = Guid.NewGuid().ToString() + Path.GetExtension(imgUpload.FileName);
                            imgUpload.SaveAs(Server.MapPath("~/CorporateSite/CorpLogo/") + fileName);
                            Session["logoUpload"] = fileName;
                            trImg.Visible = true;
                            imgFile.ImageUrl = SiteUrl + "CorporateSite/CorpLogo/" + fileName;
                        }
                    }
                    else
                        ShowMessage(2, "Upload only image file.");
                }
                catch (Exception ex) { ShowMessage(2, ex.Message); }
            }
            else
            {
                tab = "2";
                ShowMessage(2, "Upload File");
            }
        }

        void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}