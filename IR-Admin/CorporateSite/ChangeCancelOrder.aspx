﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ChangeCancelOrder.aspx.cs"
    Inherits="IR_Admin.CorporateSite.ChangeCancelOrder" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Change/Cancel Request
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="panes">
            <div id="divlist" runat="server">
                <div class="crushGvDiv">
                    <asp:GridView ID="grdCorp" runat="server" CellPadding="4" CssClass="grid-head2" ForeColor="#333333"
                        PageSize="20" GridLines="None" AutoGenerateColumns="False" OnPageIndexChanging="grdCorp_PageIndexChanging"
                        OnRowCommand="grdCorp_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <%#Eval("Firstname")%>
                                    <%#Eval("Lastname")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone">
                                <ItemTemplate>
                                    <%#Eval("Phone")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <%#Eval("Email")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IATA reference">
                                <ItemTemplate>
                                    <%#Eval("IATAref")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Request Type">
                                <ItemTemplate>
                                    <%#(Eval("TicketOption").ToString() == "0") ? "Cancel" : "Change"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Request Date">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd/MM/yyyy")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgView" AlternateText="View" ToolTip="View Details"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="View" ImageUrl="~/images/view.png"
                                        Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div id="divDetail" runat="server">
                <div class="cat-inner-ord">
                    <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="25%">
                                <strong>Name: </strong>
                                <asp:Label ID="lblName" runat="server" />
                            </td>
                            <td width="25%">
                                <strong>Phone: </strong>
                                <asp:Label ID="lblPhn" runat="server" />
                            </td>
                            <td width="25%">
                                <strong>Email:</strong>
                                <asp:Label ID="lblEmail" runat="server" />
                            </td>
                            <td width="25%" style="text-align: center">
                                <a href="ChangeCancelOrder.aspx" class="valdreq">Go Back</a>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>Lead Passenger Name: </strong>
                                <asp:Label ID="lblLeadPassNm" runat="server" />
                            </td>
                            <td width="25%">
                                <strong>IATA reference:</strong>
                                <asp:Label ID="lblIATAref" runat="server" />
                            </td>
                            <td width="25%">
                                <strong>AMX reference:</strong>
                                <asp:Label ID="lblAmxRef" runat="server" />
                            </td>
                            <td width="25%">
                                <strong>Ticket/Order Request:</strong>
                                <asp:Label ID="lblActionType" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
