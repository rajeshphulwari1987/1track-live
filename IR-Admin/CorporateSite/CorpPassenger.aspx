﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CorpPassenger.aspx.cs"
    Inherits="IR_Admin.CorporateSite.CorpPassenger" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 500);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Corporate Passenger Information</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="CorpPassenger.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CorpPassenger.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdPass" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="grdPass_PageIndexChanging" OnRowCommand="grdPass_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Country">
                                <ItemTemplate>
                                    <%#Eval("CountryName")%>
                                </ItemTemplate>
                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Passenger type">
                                <ItemTemplate>
                                    <%#Eval("PassengerType")%>
                                </ItemTemplate>
                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Passenger age">
                                <ItemTemplate>
                                    <%#Eval("PassengerAge")%>
                                </ItemTemplate>
                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='CorpPassenger.aspx?id=<%#Eval("ID")%>'>
                                        <img src="../images/edit.png" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Country:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlCountry" runat="server" Width="300px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfCountry" runat="server" ErrorMessage="*"
                                                            ForeColor="Red" ControlToValidate="ddlCountry" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Passenger Type:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPType" runat="server" Width="300px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfPType" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtPType" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Passenger Age:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPAge" runat="server" Width="300px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rvPAge" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtPAge" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        IsActive:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkActive" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
