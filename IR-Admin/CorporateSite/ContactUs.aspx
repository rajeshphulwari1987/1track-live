﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
    CodeBehind="ContactUs.aspx.cs" Inherits="IR_Admin.CorporateSite.ContactUs" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2>
      Conatact Us </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="panes">
            <div id="divlist" runat="server">
                <div class="crushGvDiv">
                    <asp:GridView ID="grdCorp" runat="server" CellPadding="4" CssClass="grid-head2" ForeColor="#333333"
                        PageSize="20" GridLines="None" AutoGenerateColumns="False" OnPageIndexChanging="grdCorp_PageIndexChanging"
                        OnRowCommand="grdCorp_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <%#Eval("Name")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <%#Eval("Email")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone">
                                <ItemTemplate>
                                    <%#Eval("Phone")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgView" AlternateText="View" ToolTip="View Details"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="View" ImageUrl="~/images/view.png"
                                        Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div id="divDetail" runat="server">
                <div class="cat-inner-ord">
                 
                    <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="30%">
                                <strong>Name: </strong>
                                <%= this.name %>
                            </td>
                            <td width="35%">
                                <strong>Email:</strong>
                                <%= this.email%>
                            </td>
                            <td width="25%">
                                <strong>Phone: </strong> 
                                <%= this.phone%>  
                            </td>

                            <td width="15%"  style="text-align:center">
                               <a href="ContactUs.aspx" class="valdreq">Go Back</a> 
                            </td> 
                        </tr>
                        <tr>
                            <td colspan="4">
                                <strong>Message:</strong>
                                <div>
                                    <%= this.message%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
