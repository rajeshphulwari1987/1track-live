﻿using System;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using Image = System.Drawing.Image;
using System.Text.RegularExpressions;

namespace IR_Admin.CorporateSite
{
    public partial class CorpBanner : System.Web.UI.Page
    {
        readonly ManageCorp _master = new ManageCorp();
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        private int width=Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CropBannerWidth"]), imgWd;
        private int height = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CropBannerHeight"]), imgHt;
        private int x, y = 0;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                BindBannerList();
                BindSites();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    tab = "2";
                    btnSubmit.Text = "Update";
                    BindBannerById(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
            ShowMessage(0, null);
        }

        void BindSites()
        {
            ddlSiteNm.DataSource = _master.GetCorpSiteList();
            ddlSiteNm.DataTextField = "DisplayName";
            ddlSiteNm.DataValueField = "ID";
            ddlSiteNm.DataBind();
            ddlSiteNm.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        public void BindBannerList()
        {
            try
            {
                tab = "1";
                grdBanner.DataSource = _master.GetBannerList();
                grdBanner.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindBannerById(Guid id)
        {
            try
            {
                var result = _master.GetBannerByID(id);
                if (result != null)
                {
                    ddlSiteNm.SelectedValue = result.SiteID.ToString();
                    txtTitle.Text = result.Title;
                    imgFile.ImageUrl = "../" + result.LogoPath;
                    chkactive.Checked = Convert.ToBoolean(result.IsActive);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSubmit.Text == "Submit")
                {
                    if (Session["BannerUpload"] != null)
                    {
                        _master.AddBanner(new tblCorpBanner
                        {
                            ID = Guid.NewGuid(),
                            Title = txtTitle.Text.Trim(),
                            SiteID = Guid.Parse(ddlSiteNm.SelectedValue),
                            LogoPath = "CorporateSite/CorpBanner/" + Session["BannerUpload"],
                            IsActive = chkactive.Checked
                        });
                        ShowMessage(1, "Record added successfully.");
                        BindBannerList();
                        ClearControls();
                        tab = "1";
                    }
                    else
                    {
                        ShowMessage(2, "File not Uploaded.");
                    }
                }
                else if (btnSubmit.Text == "Update")
                {
                    _master.UpdateBanner(new tblCorpBanner
                    {
                        ID = Guid.Parse(Request["id"]),
                        SiteID = Guid.Parse(ddlSiteNm.SelectedValue),
                        Title = txtTitle.Text.Trim(),
                        LogoPath = Session["BannerUpload"] != null ? "CorporateSite/CorpBanner/" + Session["BannerUpload"] : null,
                        IsActive = chkactive.Checked
                    });
                    ShowMessage(1, "Record updated successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    BindBannerList();
                    ClearControls();
                    tab = "1";
                }
            }
            catch (Exception ex)
            {
                hdnCrop.Value = "0";
                ShowMessage(2, ex.Message);
            }
        }

        void ClearControls()
        {
            ddlSiteNm.SelectedIndex = 0;
            txtTitle.Text = string.Empty;
            Session["imgUpload"] = string.Empty;
            chkactive.Checked = false;
            imgFile.ImageUrl = SiteUrl + "CMSImages/sml-noimg.jpg";
            hdnId.Value = string.Empty;
            btnSubmit.Text = "Submit";
            tab = "1";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CorpBanner.aspx");
        }

        protected void btnUp_Click(object sender, EventArgs e)
        {
            if (imgUpload.HasFile)
            {
                try
                {
                    tab = "2";
                    string imgType = Path.GetExtension(imgUpload.FileName);
                    if (imgType == ".jpg" || imgType == ".jpeg" || imgType == ".png" || imgType == ".bmp")
                    {
                        using (var myImage = Image.FromStream(imgUpload.PostedFile.InputStream))
                        {
                            imgWd = myImage.Width;
                            imgHt = myImage.Height;
                        }

                        if (imgWd < width || imgHt < height)
                            ShowMessage(1, "Image Size should be equal or more than " + width + "*" + height);
                        else
                        {
                            string fileName = Path.GetFileName(imgUpload.FileName);
                            fileName = Guid.NewGuid().ToString() + fileName;
                            imgUpload.SaveAs(Server.MapPath("~/CorporateSite/CorpBanner/") + fileName);
                            Session["BannerUpload"] = fileName;
                            imgFile.ImageUrl = SiteUrl + "CorporateSite/CorpBanner/" + fileName;
                        }
                    }
                    else
                    {
                        ShowMessage(2, "Upload only image file.");
                    }

                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
            }
            else
            {
                tab = "2";
                ShowMessage(2, "Upload File");
            }
        }

        protected void grdBanner_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Remove")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                var result = _db.tblCorpBanners.FirstOrDefault(x => x.ID == id);
                if (result != null)
                {
                    var path = "~/" + result.LogoPath;
                    if (File.Exists(Server.MapPath(path)))
                        File.Delete(Server.MapPath(path));
                }
                _master.DeleteCorpBanner(id);
                ShowMessage(1, "Record deleted successfully.");
            }
            if (e.CommandName == "ActiveInActive")
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                _master.ActiveInactiveBanner(id);
            }
            BindBannerList();
        }
    }
}