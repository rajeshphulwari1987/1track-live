﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Linq;
using System.Data.Objects;
using System.Globalization;

namespace IR_Admin.CorporateSite
{
    public partial class JourneyBooked : Page
    {
        readonly ManageCorp _master = new ManageCorp();
        readonly db_1TrackEntities db = new db_1TrackEntities();
        Guid _siteID;
        const string DateFormat = "dd/MM/yyyy";
        public string name, phone, email, IATAno, agency, request, tickettype, ticketdelv, SiteName, CreatedOn, CreatedBy, IPAddress;
        public string child, youth, adult, senior, lname, dob, discount, discountno, discountdate, notes;

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindGrid(_siteID);
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
                ViewState["PreSiteID"] = _siteID;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            divlist.Style.Add("display", "block");
            divDetail.Style.Add("display", "none");
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                SiteSelected();
                BindGrid(_siteID);
            }
        }

        void BindGrid(Guid siteID)
        {
            grdCorp.DataSource = _master.GetJourneyBookingList(siteID);
            grdCorp.DataBind();
        }

        protected void grdCorp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCorp.PageIndex = e.NewPageIndex;
            _siteID = Master.SiteID;
            SiteSelected();
            BindGrid(_siteID);
        }

        protected void grdCorp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "View")
            {
                var result = _master.GetJourneyBookingByID(id);
                if (result != null)
                {
                    name = result.Firstname + " " + result.Lastname;
                    phone = result.Phone;
                    email = result.Email;
                    IATAno = result.IATAno;
                    agency = result.Agencyname;
                    request = result.Requesttype;
                    tickettype = result.TickettypeName;
                    ticketdelv = result.TicketdelvName;
                    SiteName = result.SiteName;
                    IPAddress = result.IPAddress;
                    CreatedOn = result.CreatedOn.ToString("dd MMM yyyy, hh:mm"); ;
                    CreatedBy = result.CreatedBy;
                    child = result.Children;
                    youth = result.Youths;
                    adult = result.Adults;
                    senior = result.Seniors;

                    lname = result.Lname;
                    dob = result.Dob != null ? Convert.ToDateTime(result.Dob).ToString("dd/MM/yyyy") : "";
                    discount = result.Discount;
                    discountno = result.Discountcardno;
                    discountdate = result.Discountexp != null ? Convert.ToDateTime(result.Discountexp).ToString("dd/MM/yyyy") : "";
                    notes = result.Note;

                    var journey = _master.GetJourneyBookingByPassengerID(result.Id);
                    if (journey != null)
                    {
                        rptJourney.DataSource = journey;
                        rptJourney.DataBind();
                    }

                    divDetail.Style.Add("display", "block");
                    divlist.Style.Add("display", "none");
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            _siteID = Master.SiteID;
            var query = db.tblCorpJourneyPassengerBookeds.Where(x => x.SiteId == _siteID);
            if (txtName.Text != string.Empty)
                query = query.Where(x => x.Firstname.ToLower().Contains(txtName.Text.ToLower()));

            if (txtEmail.Text != string.Empty)
                query = query.Where(x => x.Email.ToLower().Contains(txtEmail.Text.ToLower()));

            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtLastDate.Text))
            {
                var fromDate = DateTime.ParseExact(txtStartDate.Text, DateFormat, new CultureInfo("en-US")).Date;
                var toDate = DateTime.ParseExact(txtLastDate.Text, DateFormat, new CultureInfo("en-US")).Date;
                query = query.Where(x => EntityFunctions.TruncateTime(x.CreatedOn) >= fromDate && EntityFunctions.TruncateTime(x.CreatedOn) <= toDate);
            }
            else if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                var fromDate = DateTime.ParseExact(txtStartDate.Text, DateFormat, new CultureInfo("en-US")).Date;
                query = query.Where(x => EntityFunctions.TruncateTime(x.CreatedOn) >= fromDate);
            }
            else if (!string.IsNullOrEmpty(txtLastDate.Text))
            {
                var toDate = DateTime.ParseExact(txtLastDate.Text, DateFormat, new CultureInfo("en-US")).Date;
                query = query.Where(x => EntityFunctions.TruncateTime(x.CreatedOn) <= toDate);
            }
            grdCorp.DataSource = query.OrderByDescending(x => x.CreatedOn).ToList();
            grdCorp.DataBind();
        }
    }
}