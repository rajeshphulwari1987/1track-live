﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CorpOffice.aspx.cs" Inherits="IR_Admin.CorporateSite.CorpOffice" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
            if ("<%=tab%>" == 1) {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");

                document.getElementById('MainContent_divlist').style.display = 'block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                $("ul.tabs").tabs("div.inner-tabs-container > div");

                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }
        });

        function toggleTreeView(ev, hide) {
            var ddTreeView = document.getElementById("ddTreeView");
            ddTreeView.style.display = (hide == null && ddTreeView.style.display == "none") || hide == false ? "" : "none";
            ev = ev ? ev : window.event;
            if (ev) ev.cancelBubble = true;
        }

        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }  
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Corporate Offices</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="CorpOffice.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CorpOffice.aspx">New</a> </li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdOffice" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdOffice_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Office Name">
                                    <ItemTemplate>
                                        <%#Eval("OfficeName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Office Logo">
                                    <ItemTemplate>
                                        <asp:Image ID="imgLogo" runat="server" ImageUrl='<%#Eval("LogoPath")%>' Width="90px"
                                            Height="50px" />
                                    </ItemTemplate>
                                    <ItemStyle Width="20%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address">
                                    <ItemTemplate>
                                        <%#Eval("Address")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone No.">
                                    <ItemTemplate>
                                        <%#Eval("Telephone")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgRemove" AlternateText="Remove" ToolTip="Edit"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="../images/delete.png" />
                                        <a href="CorpOffice.aspx?id=<%#Eval("ID")%>" style="text-decoration: none">
                                            <img src="../images/edit.png" alt="edit" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: none;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" colspan="2" valign="top">
                                <table class="tblMainSection">
                                    <tr>
                                        <td style="width: 30%" class="col">
                                            Choose Parent Office
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlOffice" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%" class="col">
                                            Office Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtOfficeName" runat="server" MaxLength="150"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqOfficeName" runat="server" ControlToValidate="txtOfficeName"
                                                Display="Dynamic" CssClass="valdreq" ErrorMessage="Please enter office name."
                                                ToolTip="Title is required." ValidationGroup="AUForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Address 1
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtAddress1" runat="server" TextMode="MultiLine" Width="202px" MaxLength="150" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Address 2
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtAddress2" runat="server" TextMode="MultiLine" Width="202px" MaxLength="150" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Town
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTown" runat="server" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            City
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtCity" runat="server" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            County/State
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtCounty" runat="server" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Country
                                        </td>
                                        <td class="col">
                                            <asp:DropDownList ID="ddlCountry" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqCountry" runat="server" ControlToValidate="ddlCountry"
                                                Display="Dynamic" InitialValue="0" ErrorMessage="Please select country" CssClass="valdreq"
                                                ValidationGroup="AUForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Postcode/Zip code
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtPostCode" runat="server" MaxLength="15" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Telephone
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTelephone" runat="server" MaxLength="15" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Email
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" />
                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                                CssClass="valdreq" ErrorMessage="Email address is not valid" Display="Dynamic"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="AUForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Description
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Width="98%" Height="50" runat="server"></asp:TextBox>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Logo Title
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTitle" runat="server" />
                                            <asp:RequiredFieldValidator ID="rvTitle" runat="server" ControlToValidate="txtTitle"
                                                Display="Dynamic" CssClass="valdreq" ErrorMessage="Please enter logo title."
                                                ToolTip="Logo title is required." ValidationGroup="AUForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Choose Color
                                        </td>
                                        <td class="col">
                                            <%--<select id="ddlColor" name="ddlColor" runat="server">
                                                <option value="">-- Select Color -- </option>
                                                <option value="1" style="background-color: #ceafed">#ceafed </option>
                                                <option value="2" style="background-color: #ffff66">#ffff66 </option>
                                                <option value="3" style="background-color: #ff6666">#ff6666 </option>
                                                <option value="4" style="background-color: #adff2f">#adff2f </option>
                                                <option value="5" style="background-color: #efef5f">#efef5f </option>
                                            </select>--%>
                                            <asp:DropDownList ID="ddlColor" runat="server">
                                                <asp:ListItem Text="-- Select Color --" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="#ceafed" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="#ffff66" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="#ff6666" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="#adff2f" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="#efef5f" Value="5"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Image/Photo
                                        </td>
                                        <td class="col">
                                            <asp:FileUpload ID="imgUpload" runat="server" Width="195px" CssClass="btnupload" />
                                        </td>
                                    </tr>
                                    <tr id="trImg" runat="server" visible="false">
                                        <td class="col">
                                            Logo Image
                                        </td>
                                        <td class="col">
                                            <div class="divMainImgManger" style="width: 130px; text-align: center; min-height: 100px;">
                                                <asp:Image ID="imgFile" runat="server" Width="130" Height="100" ImageUrl="~/CMSImages/sml-noimg.jpg" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                        </td>
                                        <td class="col">
                                            <asp:Button ID="btnUp" runat="server" Text="Upload & Crop" CssClass="btncrop" ValidationGroup="AUForm"
                                                OnClick="btnUp_Click" OnClientClick="cropClk()" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Is Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkActive" runat="server" />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                                Text="Cancel" />&nbsp;
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                                Text="Submit" Width="89px" ValidationGroup="AUForm" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td rowspan="4" valign="top" class="col" style="width: 30%; border-left: 1px dashed #B1B1B1;">
                                &nbsp; <b>Select Site</b>
                                <div style="width: 95%; height: 380px; overflow-y: auto; margin-left: 5px;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
