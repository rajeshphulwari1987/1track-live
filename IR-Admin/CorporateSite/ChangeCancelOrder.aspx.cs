﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.CorporateSite
{
    public partial class ChangeCancelOrder : Page
    {
        readonly ManageCorp _master = new ManageCorp();
        Guid _siteID;

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindGrid(_siteID);
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
                ViewState["PreSiteID"] = _siteID;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            divlist.Style.Add("display", "block");
            divDetail.Style.Add("display", "none");
            ShowMessage(0, null);
            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                SiteSelected();
                BindGrid(_siteID);
            }
        }

        void BindGrid(Guid siteId)
        {
            grdCorp.DataSource = _master.GetChangeCancelList(siteId);
            grdCorp.DataBind();
        }

        protected void grdCorp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCorp.PageIndex = e.NewPageIndex;
            _siteID = Master.SiteID;
            SiteSelected();
            BindGrid(_siteID);
        }

        protected void grdCorp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Remove")
            {
                _master.DeleteChangeOrder(id);
                ShowMessage(1, "Record deleted successfully.");
                _siteID = Master.SiteID;
                SiteSelected();
                BindGrid(_siteID);
            }
            if (e.CommandName == "View")
            {
                var result = _master.GetChangeOrderByID(id);
                if (result != null)
                {
                    lblName.Text = result.Firstname + " " + result.Lastname;
                    lblPhn.Text = result.Phone;
                    lblEmail.Text = result.Email;
                    lblLeadPassNm.Text = result.LeadPassFirstname + " " + result.LeadPassLastname;

                    lblIATAref.Text = result.IATAref;
                    lblAmxRef.Text = result.AMXref;
                    lblActionType.Text = result.TicketOption.ToString() == "0" ? "cancel" : "change";
                    divDetail.Style.Add("display", "block");
                    divlist.Style.Add("display", "none");
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}