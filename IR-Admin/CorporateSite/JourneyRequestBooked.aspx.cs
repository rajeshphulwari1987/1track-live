﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Mail;
using System.Xml;
using Business;
using System.Configuration;

namespace IR_Admin.CorporateSite
{
    public partial class JourneyRequestBooked : Page
    {
        readonly ManageCorp _master = new ManageCorp();
        readonly ManageCorpFront manage = new ManageCorpFront();
        readonly db_1TrackEntities db = new db_1TrackEntities();
        private readonly Masters _masterPage = new Masters();
        Guid _siteID, requestId;
        private string htmfile = string.Empty;
        const string dateFormat = "dd/MMM/yyyy";

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
                ViewState["PreSiteID"] = _siteID;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                requestId = Guid.Parse(Request.QueryString["id"]);
            }
            if (!IsPostBack)
            {
                PageloadEvent();
                FillForm();
                _siteID = Master.SiteID;
            }
        }

        public void PageloadEvent()
        {
            try
            {
                string hh = string.Empty, mm = string.Empty;
                for (int i = 0; i < 10; i++)
                {
                    ddladults.Items.Add(new ListItem(i.ToString()));
                    ddlchildren.Items.Add(new ListItem(i.ToString()));
                    ddlyouths.Items.Add(new ListItem(i.ToString()));
                    ddlseniors.Items.Add(new ListItem(i.ToString()));
                }
                for (int i = 0; i < 25; i++)
                {
                    if (i.ToString().Length < 2)
                        hh = "0" + i;
                    else
                        hh = i.ToString();
                    ddldeptimef.Items.Add(new ListItem(hh));
                    ddlarrtimef.Items.Add(new ListItem(hh));
                    ddldeptwof.Items.Add(new ListItem(hh));
                    ddlarrtwof.Items.Add(new ListItem(hh));
                }
                for (int i = 0; i < 60; i++)
                {
                    if (i.ToString().Length < 2)
                        mm = "0" + i;
                    else
                        mm = i.ToString();
                    ddldeptimes.Items.Add(new ListItem(mm));
                    ddlarrtimes.Items.Add(new ListItem(mm));
                    ddldeptwos.Items.Add(new ListItem(mm));
                    ddlarrtwos.Items.Add(new ListItem(mm));
                }

                ddltickettype.DataSource = manage.GetTicketTypeList();
                ddltickettype.DataTextField = "Name";
                ddltickettype.DataValueField = "Id";
                ddltickettype.DataBind();
                ddltickettype.Items.Insert(0, new ListItem("-Select-", "0"));

                ddlticketdelivered.DataSource = manage.GetTicketDeliverList();
                ddlticketdelivered.DataTextField = "Name";
                ddlticketdelivered.DataValueField = "Id";
                ddlticketdelivered.DataBind();
                ddlticketdelivered.Items.Insert(0, new ListItem("-Select-", "0"));

                ddlcardtype.DataSource = manage.GetLoyaltyCardList();
                ddlcardtype.DataTextField = "Name";
                ddlcardtype.DataValueField = "Id";
                ddlcardtype.DataBind();
                ddlcardtype.Items.Insert(0, new ListItem("-Select-", "0"));

                ddlclasstwo.DataSource = ddlclass.DataSource = manage.GetClassList();
                ddlclasstwo.DataTextField = ddlclass.DataTextField = "Name";
                ddlclasstwo.DataValueField = ddlclass.DataValueField = "Id";
                ddlclasstwo.DataBind();
                ddlclasstwo.Items.Insert(0, new ListItem("-Select-", "0"));
                ddlclass.DataBind();
                ddlclass.Items.Insert(0, new ListItem("-Select-", "0"));

                ddlaccomodationtwo.DataSource = ddlacomodation.DataSource = manage.GetAccomodationsList();
                ddlaccomodationtwo.DataTextField = ddlacomodation.DataTextField = "Name";
                ddlaccomodationtwo.DataValueField = ddlacomodation.DataValueField = "Id";
                ddlaccomodationtwo.DataBind();
                ddlaccomodationtwo.Items.Insert(0, new ListItem("-Select-", "0"));
                ddlacomodation.DataBind();
                ddlacomodation.Items.Insert(0, new ListItem("-Select-", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FillForm()
        {
            if (requestId != Guid.Empty)
            {
                var CorpJP = _master.GetJourneyByID(requestId);
                if (CorpJP != null)
                {
                    hdnRequestId.Value = CorpJP.Id.ToString();
                    txtFirst.Text = CorpJP.Firstname;
                    txtLast.Text = CorpJP.Lastname;
                    txtemail.Text = CorpJP.Email;
                    txtphone.Text = CorpJP.Phone;
                    txtagency.Text = CorpJP.Agencyname;
                    txtIATA.Text = CorpJP.IATAno;
                    ddlrequest.SelectedValue = CorpJP.Requesttype;
                    ddltickettype.SelectedValue = CorpJP.Tickettype.ToString();
                    ddlticketdelivered.SelectedValue = CorpJP.Ticketdelivered.ToString();
                    ddlchildren.SelectedValue = CorpJP.Children;
                    ddladults.SelectedValue = CorpJP.Adults;
                    ddlyouths.SelectedValue = CorpJP.Youths;
                    ddlseniors.SelectedValue = CorpJP.Seniors;
                    ddltitle.SelectedValue = CorpJP.LPTitle;
                    txtLPF.Text = CorpJP.LPFname;
                    txtLPL.Text = CorpJP.LPLname;
                    txtLPS.Text = CorpJP.LPSuffix;
                    txtdob.Text = CorpJP.Dob != null ? CorpJP.Dob.Value.ToString(dateFormat) : String.Empty;
                    ddlcardtype.SelectedValue = CorpJP.Discountcardtype != null ? CorpJP.Discountcardtype.Value.ToString() : string.Empty;
                    txtloycardno.Text = CorpJP.Discountcardno;
                    txtloyexpdate.Text = CorpJP.Discountexp != null ? CorpJP.Discountexp.Value.ToString(dateFormat) : String.Empty;
                    txtnote.Text = CorpJP.Note;
                    hdnUserId.Value = CorpJP.UserId.ToString();
                    var journeys = _master.GetJourneyByPassengerID(requestId);
                    for (int i = 0; i < journeys.Count; i++)
                    {
                        ///*journey trip*/
                        if (i == 0)
                        {
                            hdnJourneyId1.Value = journeys[i].Id.ToString();
                            txtfrom.Text = journeys[i].From;
                            txtto.Text = journeys[i].To;
                            txtfirstvia.Text = journeys[i].Via;
                            txtfirstdate.Text = journeys[i].Date.ToString(dateFormat);
                            ddldeptimef.SelectedValue = journeys[i].Deptime.ToString("hh");
                            ddldeptimes.SelectedValue = journeys[i].Deptime.ToString("mm");
                            ddlarrtimef.SelectedValue = journeys[i].Arrtime.ToString("hh");
                            ddlarrtimes.SelectedValue = journeys[i].Arrtime.ToString("mm");
                            txtfirsttrainno.Text = journeys[i].Trainno;
                            ddlclass.SelectedValue = journeys[i].Class.ToString();
                            ddlacomodation.SelectedValue = journeys[i].Accomodation.ToString();
                        }
                        else
                        {
                            ddltriptype.SelectedIndex = 1;
                            hdnIsReturned.Value = "true";
                            hdnJourneyId2.Value = journeys[i].Id.ToString();
                            txtfromtwo.Text = journeys[i].From;
                            txttotwo.Text = journeys[i].To;
                            txtdatetwo.Text = journeys[i].Date.ToString(dateFormat);
                            ddldeptwof.SelectedValue = journeys[i].Deptime.ToString("hh");
                            ddldeptwos.SelectedValue = journeys[i].Deptime.ToString("mm");
                            ddlarrtwof.SelectedValue = journeys[i].Arrtime.ToString("hh");
                            ddlarrtwos.SelectedValue = journeys[i].Arrtime.ToString("mm");
                            txttrainnotwo.Text = journeys[i].Trainno;
                            ddlclasstwo.SelectedValue = journeys[i].Class.ToString();
                            ddlaccomodationtwo.SelectedValue = journeys[i].Accomodation.ToString();
                        }
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var obj = new tblCorpJourneyPassengerBooked();
                var j1obj = new tblCorpJourneyBooked();
                var j2obj = new tblCorpJourneyBooked();
                DateTime? dob = (string.IsNullOrEmpty(txtdob.Text.Replace("DD/MMM/YYYY", string.Empty)) ? null : (DateTime?)Convert.ToDateTime(txtdob.Text));
                DateTime? expdate = (string.IsNullOrEmpty(txtloyexpdate.Text.Replace("DD/MMM/YYYY", string.Empty)) ? null : (DateTime?)Convert.ToDateTime(txtloyexpdate.Text));
                /*passanger detail*/
                obj.Id = requestId;
                obj.Firstname = txtFirst.Text;
                obj.Lastname = txtLast.Text;
                obj.Email = txtemail.Text;
                obj.Phone = txtphone.Text;
                obj.Agencyname = txtagency.Text;
                obj.IATAno = txtIATA.Text;
                obj.Requesttype = ddlrequest.SelectedValue;
                obj.Tickettype = Guid.Parse(ddltickettype.SelectedValue);
                obj.Ticketdelivered = Guid.Parse(ddlticketdelivered.SelectedValue);
                obj.Children = ddlchildren.SelectedValue;
                obj.Adults = ddladults.SelectedValue;
                obj.Youths = ddlyouths.SelectedValue;
                obj.Seniors = ddlseniors.SelectedValue;
                obj.LPTitle = ddltitle.SelectedValue;
                obj.LPFname = txtLPF.Text;
                obj.LPLname = txtLPL.Text;
                obj.LPSuffix = txtLPS.Text;
                obj.Dob = dob;
                obj.Discountcardtype = (ddlcardtype.SelectedValue == "0" ? null : (Guid?)Guid.Parse(ddlcardtype.SelectedValue));
                obj.Discountcardno = txtloycardno.Text;
                obj.Discountexp = expdate;
                obj.CreatedOn = DateTime.Now;
                obj.CreatedBy = AdminuserInfo.UserID;
                obj.SiteId = Session["siteId"] != null ? Guid.Parse(Session["siteId"].ToString()) : new Guid();
                obj.Note = txtnote.Text;
                obj.CreatedBy = Guid.Parse(hdnUserId.Value);
                /*journey trip*/
                j1obj.Id = Guid.NewGuid();
                j1obj.PassengerId = requestId;
                j1obj.From = txtfrom.Text;
                j1obj.To = txtto.Text;
                j1obj.Via = txtfirstvia.Text;
                j1obj.Date = Convert.ToDateTime(txtfirstdate.Text);
                j1obj.Deptime = Convert.ToDateTime(ddldeptimef.SelectedValue + ":" + ddldeptimes.SelectedValue).TimeOfDay;
                j1obj.Arrtime = Convert.ToDateTime(ddlarrtimef.SelectedValue + ":" + ddlarrtimes.SelectedValue).TimeOfDay;
                j1obj.Trainno = txtfirsttrainno.Text;
                j1obj.Class = Guid.Parse(ddlclass.SelectedValue);
                j1obj.Accomodation = Guid.Parse(ddlacomodation.SelectedValue);
                j1obj.JourneyOrder = 1;
                if (ddltriptype.SelectedIndex == 1)
                {
                    j2obj.Id = Guid.NewGuid();
                    j2obj.PassengerId = requestId;
                    j2obj.From = txtfromtwo.Text;
                    j2obj.To = txttotwo.Text;
                    j2obj.Via = j1obj.Via;
                    j2obj.Date = Convert.ToDateTime(txtdatetwo.Text);
                    j2obj.Deptime = Convert.ToDateTime(ddldeptwof.SelectedValue + ":" + ddldeptwos.SelectedValue).TimeOfDay;
                    j2obj.Arrtime = Convert.ToDateTime(ddlarrtwof.SelectedValue + ":" + ddlarrtwos.SelectedValue).TimeOfDay;
                    j2obj.Trainno = txttrainnotwo.Text;
                    j2obj.Class = Guid.Parse(ddlclasstwo.SelectedValue);
                    j2obj.Accomodation = Guid.Parse(ddlaccomodationtwo.SelectedValue);
                    j2obj.JourneyOrder = 2;
                }

                manage.AddJourneyBookingPassenger(obj);
                manage.AddJourneyBooking(j1obj);

                if (ddltriptype.SelectedIndex == 1)
                    manage.AddJourneyBooking(j2obj);

                var data = db.tblCorpJourneyPassengers.FirstOrDefault(x => x.Id == requestId);
                if (data != null)
                {
                    data.IsBooked = true;
                    data.Status = 4;
                    db.SaveChanges();
                }
                SendMail();
                Response.Redirect("JourneyBooked.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                _siteID = Master.SiteID;
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = db.tblSites.FirstOrDefault(x => x.ID == _siteID);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    string Subject = "Booking Confirmation";
                    htmfile = Server.MapPath("~/CorporateSite/MailTemplate.htm");

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);

                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml.ToString();
                    string UserName, EmailAddress, Phone, Journey, OrderDate, SiteLogo;
                    UserName = EmailAddress = Phone = Journey = OrderDate = SiteLogo = "";
                    int i = 1;

                    #region Passenger details
                    var requestDetails = db.tblCorpJourneyPassengers.FirstOrDefault(x => x.Id == requestId);
                    if (requestDetails != null)
                    {
                        EmailAddress = requestDetails.Email.Trim();
                        UserName = requestDetails.Firstname + " " + requestDetails.Lastname;
                        Phone = requestDetails.Phone;
                        OrderDate = requestDetails.CreatedOn.ToString("dd/MMM/yyyy");

                        #region Journey details
                        var journeyInfo = db.tblCorpJourneys.Where(x => x.PassengerId == requestId).OrderBy(x => x.JourneyOrder).ToList();
                        if (journeyInfo != null)
                        {
                            foreach (var item in journeyInfo)
                            {
                                if (i == 1)
                                {
                                    Journey += "<table style='width:100%'>";
                                    Journey += "<tr><td colspan='2' style='font-size: 15px' width='100%'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>Journey Details " + (journeyInfo.Count == 1 ? "" : "1") + "</b></font></td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>From </td><td style='font-size: 12px' width='50%'>" + item.From.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>To </td><td style='font-size: 12px' width='50%'>" + item.To.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Via/Change </td><td style='font-size: 12px' width='50%'>" + item.Via.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Date </td><td style='font-size: 12px' width='50%'>" + item.Date.ToString("dd/MM/yyyy").Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Departure Time </td><td style='font-size: 12px' width='50%'>" + item.Deptime + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Arrival Time </td><td style='font-size: 12px' width='50%'>" + item.Arrtime + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Train number </td><td style='font-size: 12px' width='50%'>" + item.Trainno.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Class </td><td style='font-size: 12px' width='50%'>" + item.tblCorpClassMst.Name + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Accomodation </td><td style='font-size: 12px' width='50%'>" + item.tblCorpAccomodation.Name + "</td></tr>";
                                    Journey += "</table>";
                                }
                                else if (i == 2)
                                {
                                    Journey += "<table style='width:100%'>";
                                    Journey += "<tr><td colspan='2' style='font-size: 15px' width='100%'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>Journey Details 2</b></font></td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>From </td><td style='font-size: 12px' width='50%'>" + item.From.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>To </td><td style='font-size: 12px' width='50%'>" + item.To.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Via/Change </td><td style='font-size: 12px' width='50%'>" + item.Via.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Date </td><td style='font-size: 12px' width='50%'>" + item.Date.ToString("dd/MM/yyyy").Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Departure Time </td><td style='font-size: 12px' width='50%'>" + item.Deptime + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Arrival Time </td><td style='font-size: 12px' width='50%'>" + item.Arrtime + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Train number </td><td style='font-size: 12px' width='50%'>" + item.Trainno.Trim() + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Class </td><td style='font-size: 12px' width='50%'>" + item.tblCorpClassMst.Name + "</td></tr>";
                                    Journey += "<tr><td style='font-size: 12px' width='50%'>Accomodation </td><td style='font-size: 12px' width='50%'>" + item.tblCorpAccomodation.Name + "</td></tr>";
                                    Journey += "</table>";
                                }
                                i++;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    body = body.Replace("##UserName##", UserName.Trim());
                    body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                    body = body.Replace("##Phone##", Phone.Trim());
                    body = body.Replace("##OrderDate##", OrderDate.Trim());
                    body = body.Replace("#Blanck#", "&nbsp;");
                    body = body.Replace("##Journey##", Journey);

                    /*Get smtp details*/
                    var result = _masterPage.GetEmailSettingDetail(_siteID);
                    if (result != null)
                    {
                        var fromAddres = new MailAddress(result.Email, result.Email);
                        smtpClient.Host = result.SmtpHost;
                        smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                        smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                        message.From = fromAddres;
                        message.To.Add(EmailAddress);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        smtpClient.Send(message);
                        retVal = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Failed to send E-mail confimation!");
            }
            return retVal;
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}