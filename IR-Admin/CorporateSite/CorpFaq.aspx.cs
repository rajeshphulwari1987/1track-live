﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.CorporateSite
{
    public partial class CorpFaq : Page
    {
        readonly ManageCorp _objFaq = new ManageCorp();
        public string Tab = "1";
        Guid _siteID;
        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindFaqs(10, _siteID);
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
                ViewState["PreSiteID"] = _siteID;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            ShowMessage(0, null);
            if (!IsPostBack)
            {
                _siteID = Master.SiteID;
                SiteSelected();
                BindSites();
                BindFaqs(10, _siteID);
            }
        }

        void BindSites()
        {
            ddlSiteNm.DataSource = _objFaq.GetCorpSiteList();
            ddlSiteNm.DataTextField = "DisplayName";
            ddlSiteNm.DataValueField = "ID";
            ddlSiteNm.DataBind();
            ddlSiteNm.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void BindFaqs(int totalItem, Guid _siteID)
        {
            if (totalItem > 50)
                dtlFaq.DataSource = _objFaq.GetCorpFaqList(_siteID);
            else
                dtlFaq.DataSource = _objFaq.GetCorpFaqList(_siteID).Skip(0).Take(totalItem);
            dtlFaq.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtQuestion.Text))
                    return;

                var faq = new tblCorpFaq
                {
                    ID = hdnId.Value == "0" ? 0 : Convert.ToInt32(hdnId.Value),
                    SiteID = Guid.Parse(ddlSiteNm.SelectedValue),
                    CreatedOn = DateTime.Now,
                    CreatedBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now,
                    ModifyBy = AdminuserInfo.UserID,
                    Answer = txtmessage.InnerText,
                    Question = txtQuestion.Text
                };
                _objFaq.AddEditCorpFaq(faq);
                ShowMessage(1, hdnId.Value == "0" ? "Record added successfully." : "Record updated successfully.");

                hdnId.Value = "0";
                ddlSiteNm.SelectedIndex = 0;
                txtmessage.InnerText = txtQuestion.Text = string.Empty;
                _siteID = Master.SiteID;
                SiteSelected();
                BindFaqs(10, _siteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void dtlFaq_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                btnSubmit.Text = "Update";
                var faq = _objFaq.GetCorpFaqByID(Convert.ToInt32(e.CommandArgument.ToString()));
                if (faq != null)
                {
                    ddlSiteNm.SelectedValue = Guid.Parse(faq.SiteID.ToString()).ToString();
                    txtmessage.InnerText = faq.Answer;
                    txtQuestion.Text = faq.Question;
                    hdnId.Value = faq.ID.ToString(CultureInfo.InvariantCulture);
                    Tab = "2";
                }
            }

            if (e.CommandName == "Remove")
            {
                _objFaq.DeleteCorpFaqByID(Convert.ToInt32(e.CommandArgument.ToString()));
                ShowMessage(1, "Record deleted successfully.");
                _siteID = Master.SiteID;
                SiteSelected();
                BindFaqs(10, _siteID);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddlPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteID = Master.SiteID;
            SiteSelected();
            if (ddlPages.SelectedValue != "0")
                BindFaqs(Convert.ToInt32(ddlPages.SelectedValue), _siteID);
        }
    }
}