﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CorpLogo.aspx.cs"
    Inherits="IR_Admin.CorporateSite.CorpLogo" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
            if(<%=tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {
                $("ul.tabs").tabs("div.inner-tabs-container > div");               
            }         
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Corporate Logo
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="CorpLogo.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CorpLogo.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdLogo" runat="server" CellPadding="4" CssClass="grid-head2" ForeColor="#333333"
                            GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdLogo_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Thumbnail">
                                    <ItemTemplate>
                                        <asp:Image ID="imgLogo" runat="server" ImageUrl='<%#Eval("LogoPath")%>' Width="90px"
                                            Height="50px" />
                                    </ItemTemplate>
                                    <ItemStyle Width="20%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
                                        <%#Eval("Title")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="CorpLogo.aspx?id=<%#Eval("Id")%>" style="text-decoration: none">
                                            <img title="Edit" alt="Edit" src="../images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%# String.Format("{0},{1}", Eval("ID"),Eval("SiteID")) %>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <asp:HiddenField ID="hdnId" runat="server" />
                <div id="divNew" runat="server" style="display: Block;">
                    <div class="divMainImgManger">
                        <div class="divleftImgManger">
                            Site Name:
                        </div>
                        <div class="divrightImgManger">
                            <asp:DropDownList ID="ddlSiteNm" runat="server" Width="250px" />
                            &nbsp;<asp:RequiredFieldValidator ID="rvSite" runat="server" ControlToValidate="ddlSiteNm"
                                ValidationGroup="rvSave" InitialValue="-1" />
                        </div>
                        <div class="divleftImgManger">
                            Title:
                        </div>
                        <div class="divrightImgManger">
                            <asp:TextBox ID="txtTitle" runat="server" />
                            &nbsp;<asp:RequiredFieldValidator ID="rvTitle" runat="server" ControlToValidate="txtTitle"
                                ValidationGroup="rvSave" />
                        </div>
                        <div class="divleftImgManger">
                            Image/Photo:
                        </div>
                        <div class="divrightImgManger">
                            <asp:FileUpload ID="imgUpload" runat="server" Width="195px" CssClass="btnupload" />
                        </div>
                        <div class="divleftImgManger" style="color: transparent;">
                            .
                        </div>
                        <div class="divrightImgManger">
                            <asp:Button ID="btnUp" runat="server" Text="Upload & Crop" CssClass="btncrop" OnClick="btnUp_Click"
                                ValidationGroup="rvSave" OnClientClick="cropClk()" />
                        </div>
                        <div class="divleftImgManger">
                            Is Active:
                        </div>
                        <div class="divrightImgManger">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="divleftbtnImgManger" style="color: transparent;">
                            .
                        </div>
                        <div class="divrightbtnImgManger">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                OnClientClick="return isCropedImg()" Text="Submit" Width="89px" ValidationGroup="rvSave" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" /><asp:HiddenField ID="hdnCrop" runat="server" Value="0" />
                        </div>
                    </div>
                    <div class="divMainImgManger" style="width: 200px; margin-left: 50px; text-align: center;
                        min-height: 150px;">
                        <asp:Image ID="imgFile" runat="server" Width="140" Height="150" ImageUrl="~/CMSImages/sml-noimg.jpg" />
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div id="DivWarning" runat="server" class="warning" style="display: none;">
                    <asp:Label ID="lblMsg" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
