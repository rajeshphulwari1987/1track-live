﻿#region Using
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin.CorporateSite
{
    public partial class CorpCmsPages : Page
    {
        readonly ManageCorp _mCorp = new ManageCorp();
        public string Tab = string.Empty;
        public string Pagecontent;
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        Guid _siteID;

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindCorpPages(_siteID);
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                SiteSelected();
                BindSites();
                BindCorpPages(_siteID);
                Session["url"] = string.Empty;
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    BindCorpPageByID(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }
        #endregion

        #region Control Events
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var pageUrl = hdnUrl.Value.Trim() + txtPageName.Text.Trim().Replace(" ", "-");
                _mCorp.AddCorpPage(new tblCorpCm
                {
                    ID = Request["id"] != null ? Guid.Parse(Request["id"]) : Guid.NewGuid(),
                    SiteId = Guid.Parse(ddlSiteNm.SelectedValue),
                    PageName = txtPageName.Text.Trim(),
                    Url = pageUrl,
                    MetaTitle = txtMetaTitle.Text.Trim(),
                    Keyword = txtKeyWord.Text.Trim(),
                    MetaDescription = txtMetaDesp.Text.Trim(),
                    Description = txtDescContent.InnerHtml,
                    IsActive = chkActive.Checked,
                    IsPrimary = chkPrimary.Checked
                });

                if (Request["id"] != null)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, Request["id"] == null ? "Record added successfully." : "Record updated successfully.");
                _siteID = Master.SiteID;
                SiteSelected();
                BindCorpPages(_siteID);
                Tab = "1";
                ClearControls();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CorpCmsPages.aspx");
        }

        protected void ddlSiteNm_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tab = "2";
            ShowMessage(0, "");
            txtPageName.Text = string.Empty;
            lblUrl.Text = string.Empty;
            if (ddlSiteNm.SelectedValue != "-1")
            {
                txtPageName.Enabled = true;
                GetRootUrl(Guid.Parse(ddlSiteNm.SelectedValue));
            }
            else
            {
                txtPageName.Enabled = false;
            }
        }

        protected void grdCorp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "ActiveInActive")
            {
                _mCorp.ActiveInactiveCorpPage(Guid.Parse(id.ToString()));
            }
            if (e.CommandName == "Remove")
            {
                _mCorp.DeleteCorpPage(Guid.Parse(id.ToString()));
            }
            _siteID = Master.SiteID;
            SiteSelected();
            BindCorpPages(_siteID);
        }
        #endregion

        #region UserDefind function
        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
                ViewState["PreSiteID"] = _siteID;
        }

        void BindSites()
        {
            ddlSiteNm.DataSource = _mCorp.GetCorpSiteList();
            ddlSiteNm.DataTextField = "DisplayName";
            ddlSiteNm.DataValueField = "ID";
            ddlSiteNm.DataBind();
            ddlSiteNm.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        public void BindCorpPages(Guid siteID)
        {
            try
            {
                grdCorp.DataSource = _mCorp.GetCorpPageList(siteID);
                grdCorp.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindCorpPageByID(Guid id)
        {
            try
            {
                ddlSiteNm.Enabled = false;
                txtPageName.Enabled = true;
                var result = _mCorp.GetCorpPageById(id);
                if (result != null)
                {
                    var st = _db.tblSites.FirstOrDefault(x => x.ID == result.SiteId);
                    ddlSiteNm.SelectedValue = Guid.Parse(result.SiteId.ToString()).ToString();
                    txtPageName.Text = result.PageName;
                    lblUrl.Text = result.Url;
                    txtDescContent.InnerHtml = result.Description;
                    chkActive.Checked = Convert.ToBoolean(result.IsActive);
                    chkPrimary.Checked = Convert.ToBoolean(result.IsPrimary);
                    txtMetaTitle.Text = result.MetaTitle;
                    txtKeyWord.Text = result.Keyword;
                    txtMetaDesp.Text = result.MetaDescription;
                    Session["url"] = result.Url.Replace(" ", "-");
                    hdnUrl.Value = st.SiteURL;
                    lblUrl.Text = result.Url.Replace(" ", "-");
                }
                btnSubmit.Text = "Update";
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        public static string StringReplace(string strtext)
        {
            strtext = strtext.Replace(" ", "-");
            return strtext;
        }

        public void ResetRoute()
        {
            System.Web.Routing.RouteTable.Routes.Clear();
        }

        private void GetRootUrl(Guid siteId)
        {
            var rootUrl = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (rootUrl != null)
            {
                if (rootUrl.SiteURL != null)
                    lblUrl.Text = rootUrl.SiteURL + txtPageName.Text.Trim();
                hdnUrl.Value = lblUrl.Text;
            }
        }

        private void ClearControls()
        {
            txtPageName.Text = string.Empty;
            lblUrl.Text = string.Empty;
            txtMetaTitle.Text = string.Empty;
            txtKeyWord.Text = string.Empty;
            txtMetaDesp.Text = string.Empty;
            txtDescContent.InnerHtml = string.Empty;
            chkActive.Checked = false;
            chkPrimary.Checked = false;
        }
        #endregion
    }
}