﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CorpCmsPages.aspx.cs"
    Inherits="IR_Admin.CorporateSite.CorpCmsPages" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="../editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab.ToString()%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }

        window.scrollTo = function() {
        };
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDescContent').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }

        function bindUrl() {
            var value = $('[id$=MainContent_hdnUrl]').val() + $('input[name="ctl00$MainContent$txtPageName"]').val();
            $('[id$=MainContent_lblUrl]').text(value.split(' ').join('-'));
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Corporate Pages Content
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="CorpCmsPages.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CorpCmsPages.aspx" class="">New/Edit </a></li>
        </ul>
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="full mr-tp1">
                <div class="panes">
                    <div id="divlist" runat="server" style="display: none;">
                        <div class="crushGvDiv">
                            <asp:GridView ID="grdCorp" runat="server" CellPadding="4" CssClass="grid-head2" ForeColor="#333333"
                                GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdCorp_RowCommand">
                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Site Name">
                                        <ItemTemplate>
                                            <%#Eval("SiteName")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Page Name">
                                        <ItemTemplate>
                                            <%#Eval("PageName")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <a href="CorpCmsPages.aspx?id=<%#Eval("Id")%>" style="text-decoration: none">
                                                <img title="Edit" alt="Edit" src="../images/edit.png" />
                                            </a>
                                            <asp:ImageButton runat="server" ID="imgDelete" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                                CommandName="Remove" ImageUrl="~/images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                            <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="10%"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No records found !</EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                        <div class="alternate-block02">
                            <table id="Maintable" class="tblMainSection" style="font-size: 14px">
                                <tr>
                                    <td width="80%" valign="top" style="line-height: 35px">
                                        <table width="100%" cellpadding="5" cellspacing="5">
                                            <tr>
                                                <td>
                                                    Site Name :
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlSiteNm" runat="server" Width="250px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlSiteNm_SelectedIndexChanged" />
                                                    &nbsp;<asp:RequiredFieldValidator ID="rvSite" runat="server" ErrorMessage="Select Site Name"
                                                        CssClass="err" ControlToValidate="ddlSiteNm" ValidationGroup="rForm" InitialValue="-1" ForeColor="Red" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Page Name:
                                                </td>
                                                <td width="80%">
                                                    <asp:TextBox ID="txtPageName" runat="server" Width="250px" onkeyup="bindUrl()" AutoComplete="off" Enabled="False" />
                                                    &nbsp;<asp:RequiredFieldValidator ID="rvPageName" runat="server" ErrorMessage="Enter Page Name"
                                                        CssClass="err" ControlToValidate="txtPageName" ValidationGroup="rForm" ForeColor="Red"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    URL:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUrl" runat="server" />
                                                    <asp:HiddenField ID="hdnUrl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    IsPrimary:
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkPrimary" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    IsActive:
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkActive" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:Image ID="imgTemplate" Width="200px" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="alternate-block" id="dvFrame" runat="server" visible="True">
                            <table style="width: 100%;" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td>
                                        <h2>
                                            Content
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea id="txtDescContent" runat="server"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="alternate-block02">
                            <table width="100%" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td colspan="2">
                                        <h2>
                                            Meta Data</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Meta Title:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMetaTitle" runat="server" Width="600px" />
                                        &nbsp;<asp:RequiredFieldValidator ID="rvMeta" runat="server" ErrorMessage="Enter Meta Title"
                                            CssClass="err" ControlToValidate="txtMetaTitle" ValidationGroup="rForm" ForeColor="Red"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Keyword:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtKeyWord" runat="server" TextMode="MultiLine" Columns="60" Rows="5"
                                            Width="600px" />
                                        &nbsp;<asp:RequiredFieldValidator ID="rvKeyword" runat="server" ErrorMessage="Enter Keyword"
                                            ControlToValidate="txtKeyWord" ValidationGroup="rForm" Display="Dynamic" CssClass="err" ForeColor="Red"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Meta Description:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMetaDesp" runat="server" TextMode="MultiLine" Columns="60" Rows="5"
                                            Width="600px" />
                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Keyword"
                                            ControlToValidate="txtKeyWord" ValidationGroup="rForm" Display="Dynamic" CssClass="err" ForeColor="Red"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                            OnClientClick="saveFinal();" ValidationGroup="rForm" Text="SAVE" CommandName="Submit"
                                            Width="160px" />
                                        <asp:ValidationSummary ID="vs" runat="server" ValidationGroup="rForm" ShowSummary="False"
                                            ShowMessageBox="True" EnableClientScript="True" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
