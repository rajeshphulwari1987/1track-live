﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True"
    CodeBehind="JourneyRequest.aspx.cs" Inherits="IR_Admin.CorporateSite.JourneyRequest" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="../Styles/jquery.ui.datepicker.css" />
    <link rel="stylesheet" href="../Styles/jquery.ui.all.css">
    <script type="text/javascript" src="../Scripts/DatePicker/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../Scripts/DatePicker/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../Scripts/DatePicker/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/DatePicker/jquery.ui.widget.js"></script>
    <script type="text/javascript">
        var count = 0;
        var countKey = 1;
        var conditionone = 0;
        var conditiontwo = 0;
        var tabkey = 0;

        $(document).ready(function () {
            LoadCal();
            //round trip
            calvalidation($("#hdnIsReturned").attr('value'));
            $("#MainContent_ddltriptype").change(function () {
                var datacase = $(this).prop("selectedIndex");
                if (datacase == 1) {
                    calvalidation(true);
                }
                else {
                    calvalidation(false);
                }
            });

            if ($("#txtfrom").val() != '') {
                $('#spantxtto').text(localStorage.getItem("spantxtto"));
            }
            if ($("#txtto").val() != '') {
                $('#spantxtfrom').text(localStorage.getItem("spantxtfrom"));
            }
            $(window).click(function (event) {
                $('#_bindDivData').remove();
            });
            $(window).keydown(function (event) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            });
            $("#txtfrom , #txtto").on('keydown', function (event) {
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;

                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
        });

        function selectpopup(e) {
            $(function () {
                if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                    var $this = $(e);
                    var data = $this.val();
                    var station = '';
                    var hostName = window.location.host;
                    var url = "http://" + hostName;
                    if (window.location.toString().indexOf("https:") >= 0)
                        url = "https://" + hostName;
                    if ($("#txtfrom").val() == '' && $("#txtto").val() == '') {
                        $('#spantxtfrom').text('');
                        $('#spantxtto').text('');
                    }
                    var filter = $("#span" + $this.attr('id') + "").text();
                    if (filter == "" && $this.val() != "")
                        filter = $("#hdnFilter").val();
                    $("#hdnFilter").val(filter);
                    if ($this.attr('id') == 'txtto') {
                        station = $("#txtfrom").val();
                    }
                    else {
                        station = $("#txtto").val();
                    }
                    if (hostName == "localhost")
                        url = "http://" + hostName + "/IR-Admin";
                    else if (hostName == "admin.1tracktest.com")
                        url = "http://" + hostName;
                    var hostUrl = url + "/StationList.asmx/getStationsXList";
                    data = data.replace(/[']/g, "♥");
                    var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
                    $.ajax({
                        type: "POST",
                        url: hostUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                        success: function (msg) {
                            $('#_bindDivData').remove();
                            var lentxt = data.length;
                            $.each(msg.d, function (key, value) {
                                var splitdata = value.split('ñ');
                                var lenfull = splitdata[0].length; ;
                                var txtupper = splitdata[0].substring(0, lentxt);
                                var txtlover = splitdata[0].substring(lentxt, lenfull);
                                $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                            });
                            $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                        },
                        error: function () {
                        }
                    });
                }
            });
        }
        function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtto') {
                $('#spantxtfrom').text($(e).find('div').text());
                localStorage.setItem("spantxtfrom", $(e).find('div').text());
            }
            else {
                $('#spantxtto').text($(e).find('div').text());
                localStorage.setItem("spantxtto", $(e).find('div').text());
            }
            $('#_bindDivData').remove();
        }

        function calvalidation(isreturned) {
            if (isreturned) {
                ValidatorEnable($('[id*=reqtxtfromtwo]')[0], true);
                ValidatorEnable($('[id*=reqtxttotwo]')[0], true);
                ValidatorEnable($('[id*=reqtxtdatetwo]')[0], true);
                ValidatorEnable($('[id*=reqddlclassservectwo]')[0], true);
                ValidatorEnable($('[id*=reqddlaccomodationtwo]')[0], true);
                ValidatorEnable($('[id*=RegularExpressionValidator4]')[0], true);
                $("#returnjourney").show();
            } else {
                ValidatorEnable($('[id*=reqtxtfromtwo]')[0], false);
                ValidatorEnable($('[id*=reqtxttotwo]')[0], false);
                ValidatorEnable($('[id*=reqtxtdatetwo]')[0], false);
                ValidatorEnable($('[id*=reqddlclassservectwo]')[0], false);
                ValidatorEnable($('[id*=reqddlaccomodationtwo]')[0], false);
                ValidatorEnable($('[id*=RegularExpressionValidator4]')[0], false);
                $("#returnjourney").hide();
            }
        }
    </script>
    <script type="text/javascript">
        var count = 0;
        var countKey = 1;
        var conditionone = 0;
        var conditiontwo = 0;
        var tabkey = 0;

        $(document).ready(function () {
            if ($("#txtfromtwo").val() != '') {
                $('#spantxttotwo').text(localStorage.getItem("spantxttotwo"));
            }
            if ($("#txttotwo").val() != '') {
                $('#txtfromtwo').text(localStorage.getItem("txtfromtwo"));
            }
            $(window).click(function (event) {
                $('#_bindDivData').remove();
            });
            $(window).keydown(function (event) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            });
            $("#txtfromtwo , #txttotwo").on('keydown', function (event) {
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;

                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
        });

        function selectpopup2(e) {
            $(function () {
                if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                    var $this = $(e);
                    var data = $this.val();
                    var station = '';
                    var hostName = window.location.host;
                    var url = "http://" + hostName;
                    if (window.location.toString().indexOf("https:") >= 0)
                        url = "https://" + hostName;
                    if ($("#txtfromtwo").val() == '' && $("#txttotwo").val() == '') {
                        $('#spantxtfromtwo').text('');
                        $('#spantxttotwo').text('');
                    }
                    var filter = $("#span" + $this.attr('id') + "").text();
                    if (filter == "" && $this.val() != "")
                        filter = $("#hdnFiltertwo").val();
                    $("#hdnFiltertwo").val(filter);
                    if ($this.attr('id') == 'txttotwo') {
                        station = $("#txtfromtwo").val();
                    }
                    else {
                        station = $("#txttotwo").val();
                    }
                    if (hostName == "localhost")
                        url = "http://" + hostName + "/IR-Admin";
                    else if (hostName == "admin.1tracktest.com")
                        url = "http://" + hostName;
                    var hostUrl = url + "/StationList.asmx/getStationsXList";
                    data = data.replace(/[']/g, "♥");
                    var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass2(this)'/>");
                    $.ajax({
                        type: "POST",
                        url: hostUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                        success: function (msg) {
                            $('#_bindDivData').remove();
                            var lentxt = data.length;
                            $.each(msg.d, function (key, value) {
                                var splitdata = value.split('ñ');
                                var lenfull = splitdata[0].length; ;
                                var txtupper = splitdata[0].substring(0, lentxt);
                                var txtlover = splitdata[0].substring(lentxt, lenfull);
                                $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue2(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                            });
                            $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                        },
                        error: function () {
                        }
                    });
                }
            });
        }
        function _removehoverclass2(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }
        function _Bindthisvalue2(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txttotwo') {
                $('#spantxtfromtwo').text($(e).find('div').text());
                localStorage.setItem("spantxtfromtwo", $(e).find('div').text());
            }
            else {
                $('#spantxttotwo').text($(e).find('div').text());
                localStorage.setItem("spantxttotwo", $(e).find('div').text());
            }
            $('#_bindDivData').remove();
        }
        
        function LoadCal() {
            $("#MainContent_txtdob,#MainContent_txtloyexpdate,#MainContent_txtfirstdate,#MainContent_txtdatetwo").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            $("#MainContent_txtdob").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                changeMonth:true,
                changeYear:true,
                firstDay: 1
            });
            $("#MainContent_txtloyexpdate").datepicker({
                 numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                changeMonth:true,
                changeYear:true,
                firstDay: 1
            });
            $("#MainContent_txtfirstdate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                changeMonth:true,
                changeYear:true,
                firstDay: 1,
                minDate: 0,
            });
            $("#MainContent_txtdatetwo").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                changeMonth:true,
                changeYear:true,
                firstDay: 1,
                minDate: 0,
            });
            $("#MainContent_txtdob").click(function () {
                $("#MainContent_txtdob").datepicker('show');
            });
             $("#MainContent_txtloyexpdate").click(function () {
                $("#MainContent_txtloyexpdate").datepicker('show');
            });
              $("#MainContent_txtfirstdate").click(function () {
                $("#MainContent_txtfirstdate").datepicker('show');
            });
            $("#MainContent_txtdatetwo").click(function () {
                $("#MainContent_txtdatetwo").datepicker('show');
            });
        }        
    </script>
    <style type="text/css">
        #ui-datepicker-div
        {
            background-color: #fff !important;
        }
        
        fieldset
        {
            border: 1px solid #ccc;
            font-size: 13px;
            background: #efefef;
            margin-top: 8px;
        }
        legend
        {
            background: #ECECEC;
            border-radius: 0;
            padding: 2px 5px;
            margin-top: 5px;
            color: #100505;
            font-weight: bolder;
            font-size: 13px;
            border: 1px solid #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdnRequestId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsReturned" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnJourneyId1" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnJourneyId2" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnFiltertwo" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnUserId" runat="server" ClientIDMode="Static" />
    <h2>
        Booking Request</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="panes">
            <div id="divDetail" runat="server">
                <div class="step1">
                    <fieldset>
                        <legend>Booking Request Form</legend><span style="margin-left: 20px;">you don't require
                            a 2nd or 3rd leg to your request just miss those sections out and continue to the
                            end of the form</span>
                        <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Name
                                </td>
                                <td colspan="3">
                                    <asp:TextBox MaxLength="50" CssClass="input w-div" runat="server" ID="txtFirst" Placeholder="First Name"
                                        TabIndex="1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirst"
                                        ErrorMessage="The First Name field is required." Display="Dynamic" ForeColor="Red"
                                        ValidationGroup="journeyfrm" Text="*" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox MaxLength="50"
                                        CssClass="input w-div" runat="server" ID="txtLast" Placeholder="Last Name" TabIndex="2" />
                                    <asp:RequiredFieldValidator ID="reqtxtLast" runat="server" ControlToValidate="txtLast"
                                        ErrorMessage="The Last Name field is required." Display="Dynamic" ForeColor="Red"
                                        ValidationGroup="journeyfrm" Text="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 19%">
                                    Email
                                </td>
                                <td style="width: 25%">
                                    <asp:TextBox MaxLength="80" CssClass="input width" runat="server" ID="txtemail" />
                                    <asp:RequiredFieldValidator ID="reqtxtemail" runat="server" ControlToValidate="txtemail"
                                        Text="*" Display="Dynamic" ErrorMessage="The Email field is required." ForeColor="Red"
                                        ValidationGroup="journeyfrm" />
                                    <asp:RegularExpressionValidator ID="regxtxtemail" runat="server" ControlToValidate="txtemail"
                                        Text="*" Display="Dynamic" ErrorMessage="Invalid Email address." ForeColor="Red"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                </td>
                                <td style="width: 22%">
                                    Phone no
                                </td>
                                <td style="width: 25%">
                                    <asp:TextBox MaxLength="20" CssClass="input width" runat="server" ID="txtphone" />
                                    <asp:RequiredFieldValidator ID="regtxtphone" runat="server" ControlToValidate="txtphone"
                                        Display="Dynamic" ErrorMessage="The Phone No. field is required." ForeColor="Red"
                                        Text="*" ValidationGroup="journeyfrm" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    American express agency for
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtagency" />
                                    <asp:RequiredFieldValidator ID="regtxtagency" runat="server" ControlToValidate="txtagency"
                                        Display="Dynamic" ErrorMessage="The American express field is required." ForeColor="Red"
                                        Text="*" ValidationGroup="journeyfrm" />
                                </td>
                                <td>
                                    IATA number
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtIATA" />
                                    <asp:RequiredFieldValidator ID="regtxtIATA" runat="server" ControlToValidate="txtIATA"
                                        Text="*" Display="Dynamic" ErrorMessage="The IATA Number field is required."
                                        ForeColor="Red" ValidationGroup="journeyfrm" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Type of request
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlrequest" runat="server">
                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                        <asp:ListItem>Quote</asp:ListItem>
                                        <asp:ListItem>Order</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="regddlrequest" runat="server" ControlToValidate="ddlrequest"
                                        Display="Dynamic" ErrorMessage="Please select Type of request." InitialValue="0"
                                        Text="*" ForeColor="Red" ValidationGroup="journeyfrm" />
                                </td>
                                <td>
                                    Type of ticket
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddltickettype" runat="server" />
                                    <asp:RequiredFieldValidator ID="reqddltickettype" runat="server" InitialValue="0"
                                        Text="*" Display="Dynamic" ErrorMessage="Please select Type of ticket." ControlToValidate="ddltickettype"
                                        ForeColor="Red" ValidationGroup="journeyfrm" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    How would you like the ticket delivered?
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlticketdelivered" runat="server" />
                                    <asp:RequiredFieldValidator ID="reqddlticketdelivered" InitialValue="0" runat="server"
                                        Text="*" Display="Dynamic" ErrorMessage="Please select ticket delivered? option."
                                        ControlToValidate="ddlticketdelivered" ForeColor="Red" ValidationGroup="journeyfrm" />
                                </td>
                                <td>
                                    Status
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlStatus" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div class="step2">
                    <fieldset>
                        <legend>Passengers</legend><span style="margin-left: 20px;">NOTE: If you are travelling
                            in a group and can't enter the number of travellers in the selections, please enter
                            the details in the notes field at the end of the form.</span>
                        <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 19%">
                                    Children (aged 4 - 11)
                                </td>
                                <td style="width: 25%">
                                    <asp:DropDownList CssClass="select" ID="ddlchildren" runat="server" />
                                </td>
                                <td style="width: 22%">
                                    Adults (26 or over)
                                </td>
                                <td style="width: 25%">
                                    <asp:DropDownList CssClass="select" ID="ddladults" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Youths (25 or under)
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlyouths" runat="server" />
                                </td>
                                <td>
                                    Seniors (60 or over)
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlseniors" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Lead passenger name
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList CssClass="select title" ID="ddltitle" runat="server">
                                        <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Mrs." Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Ms" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:TextBox MaxLength="50" CssClass="input namesingle" runat="server" ID="txtLPF"
                                        Placeholder="First Name" />
                                    <asp:RequiredFieldValidator ID="reqtxtLPF" runat="server" ControlToValidate="txtLPF"
                                        Display="Dynamic" ErrorMessage="The lead passenger  First Name is required."
                                        ForeColor="Red" Text="*" ValidationGroup="journeyfrm" />&nbsp;&nbsp;&nbsp;
                                    <asp:TextBox MaxLength="50" CssClass="input namesingle" runat="server" ID="txtLPL"
                                        Placeholder="Last Name" />
                                    <asp:RequiredFieldValidator ID="reqtxtLPL" runat="server" ControlToValidate="txtLPL"
                                        Text="*" Display="Dynamic" ErrorMessage="The lead passenger  Last Name is required."
                                        ForeColor="Red" ValidationGroup="journeyfrm" />
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtLPS" Placeholder="Suffix" />
                                    <asp:RequiredFieldValidator ID="reqtxtLPS" runat="server" ControlToValidate="txtLPS"
                                        Display="Dynamic" ErrorMessage="The lead passenger  Suffix is required." ForeColor="Red"
                                        Text="*" ValidationGroup="journeyfrm" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Passenger date of birth
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="10" CssClass="input cal" runat="server" ID="txtdob" />
                                    <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select Dob."
                                        border="0" alt="">
                                    <asp:RequiredFieldValidator ID="reqdob" runat="server" ForeColor="red" ValidationGroup="journeyfrm"
                                        Display="Dynamic" ControlToValidate="txtdob" ErrorMessage="Please enter dob."
                                        Text="*" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtdob"
                                        ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                        Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid dob"
                                        ValidationGroup="journeyfrm">*</asp:RegularExpressionValidator>
                                </td>
                                <td>
                                    Discount/ loyalty card type
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlcardtype" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Discount/ loyalty card number
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtloycardno" />
                                </td>
                                <td>
                                    Discount/ loyalty card expiry date
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="10" CssClass="input cal" runat="server" ID="txtloyexpdate" />
                                    <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select Dob."
                                        border="0" alt="">
                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="red"
                                        ValidationGroup="journeyfrm" Display="Dynamic" ControlToValidate="txtloyexpdate"
                                        ErrorMessage="Please enter card expiry date." Text="*" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtloyexpdate"
                                        ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                        Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid card expiry date"
                                        ValidationGroup="journeyfrm">*</asp:RegularExpressionValidator>--%>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div class="step3">
                    <fieldset>
                        <legend>Journey Details</legend><span style="margin-left: 20px; font-weight: bold;">
                            First journey</span>
                        <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 19%">
                                    From
                                </td>
                                <td style="width: 25%">
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtfrom" Placeholder="From"
                                        autocomplete="off" onkeyup="selectpopup(this)" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="reqtxtfrom" runat="server" ControlToValidate="txtfrom"
                                        Display="Dynamic" ErrorMessage="The first journey From station name is required ."
                                        Text="*" ForeColor="Red" ValidationGroup="journeyfrm" />
                                    <span id="spantxtFrom" style="display: none"></span>
                                </td>
                                <td style="width: 22%">
                                    To
                                </td>
                                <td style="width: 25%">
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtto" onkeyup="selectpopup(this)"
                                        autocomplete="off" Placeholder="To" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="reqtxtto" runat="server" ControlToValidate="txtto"
                                        Display="Dynamic" ErrorMessage="The first journey To station name is required ."
                                        Text="*" ForeColor="Red" ValidationGroup="journeyfrm" />
                                    <span id="spantxtTo" style="display: none"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Via/change
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtfirstvia" />
                                </td>
                                <td>
                                    Date
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="10" CssClass="input cal" runat="server" ID="txtfirstdate" />
                                    <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select Dob."
                                        border="0" alt="">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="red"
                                        ValidationGroup="journeyfrm" Display="Dynamic" ControlToValidate="txtfirstdate"
                                        ErrorMessage="The first journey date is required." Text="*" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtfirstdate"
                                        ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                        Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid first journey date"
                                        ValidationGroup="journeyfrm">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Time of departure
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select stime" ID="ddldeptimef" runat="server" Width="100px" />
                                    <asp:DropDownList CssClass="select stime" ID="ddldeptimes" runat="server" Width="100px" />
                                </td>
                                <td>
                                    Time of arrival
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select stime" ID="ddlarrtimef" runat="server" Width="100px" />
                                    <asp:DropDownList CssClass="select stime" ID="ddlarrtimes" runat="server" Width="100px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Train number (if known)
                                </td>
                                <td>
                                    <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtfirsttrainno" />
                                </td>
                                <td>
                                    Class of service
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlclass" runat="server" />
                                    <asp:RequiredFieldValidator ID="reqddlclass" InitialValue="0" runat="server" ControlToValidate="ddlclass"
                                        Display="Dynamic" ErrorMessage="Please select Class of first journey." ForeColor="Red"
                                        Text="*" ValidationGroup="journeyfrm" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Type of accomodation
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddlacomodation" runat="server" />
                                    <asp:RequiredFieldValidator ID="reqddlacomodation" InitialValue="0" runat="server"
                                        Text="*" Display="Dynamic" ErrorMessage="Please select Type of accomodation of first journey."
                                        ControlToValidate="ddlacomodation" ForeColor="Red" ValidationGroup="journeyfrm" />
                                </td>
                                <td>
                                    Return trip or 2nd leg required?
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="select" ID="ddltriptype" runat="server">
                                        <asp:ListItem Selected="True">No thank you</asp:ListItem>
                                        <asp:ListItem>Yes please!</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <div class="returnjourney" id="returnjourney" runat="server" clientidmode="Static">
                            <span style="margin-left: 20px; font-weight: bold;">Second journey</span>
                            <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 19%">
                                        From
                                    </td>
                                    <td style="width: 25%">
                                        <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txtfromtwo"
                                            autocomplete="off" Placeholder="From" onkeyup="selectpopup2(this)" ClientIDMode="Static" />
                                        <asp:RequiredFieldValidator ID="reqtxtfromtwo" runat="server" ControlToValidate="txtfromtwo"
                                            Text="*" Display="Dynamic" ErrorMessage="The second journey From station is required."
                                            ForeColor="Red" ValidationGroup="journeyfrm" />
                                        <span id="spantxtfromtwo" style="display: none"></span>
                                    </td>
                                    <td style="width: 22%">
                                        To
                                    </td>
                                    <td style="width: 25%">
                                        <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txttotwo" onkeyup="selectpopup2(this)"
                                            autocomplete="off" Placeholder="To" ClientIDMode="Static" />
                                        <asp:RequiredFieldValidator ID="reqtxttotwo" runat="server" ControlToValidate="txttotwo"
                                            Text="*" Display="Dynamic" ErrorMessage="The second journey To station is required."
                                            ForeColor="Red" ValidationGroup="journeyfrm" />
                                        <span id="spantxttotwo" style="display: none"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="10" CssClass="input cal" runat="server" ID="txtdatetwo" Text="DD/MMM/YYYY" />
                                        <img class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select Dob."
                                            border="0" alt="">
                                        <asp:RequiredFieldValidator ID="reqtxtdatetwo" runat="server" ForeColor="red" ValidationGroup="journeyfrm"
                                            Display="Dynamic" ControlToValidate="txtdatetwo" ErrorMessage="The second journey date is required."
                                            Text="*" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtdatetwo"
                                            ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                            Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid second journey date"
                                            ValidationGroup="journeyfrm">*</asp:RegularExpressionValidator>
                                    </td>
                                    <td>
                                        Time of departure
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="select stime" ID="ddldeptwof" runat="server" Width="100px" />
                                        <asp:DropDownList CssClass="select stime" ID="ddldeptwos" runat="server" Width="100px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Time of arrival
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="select stime" ID="ddlarrtwof" runat="server" Width="100px" />
                                        <asp:DropDownList CssClass="select stime" ID="ddlarrtwos" runat="server" Width="100px" />
                                    </td>
                                    <td>
                                        Train number (if known)
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="50" CssClass="input width" runat="server" ID="txttrainnotwo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Class of service
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="select" ID="ddlclasstwo" runat="server" />
                                        <asp:RequiredFieldValidator ID="reqddlclassservectwo" InitialValue="0" runat="server"
                                            Text="*" Display="Dynamic" ErrorMessage="Please select Class of second journey."
                                            ControlToValidate="ddlclasstwo" ForeColor="Red" ValidationGroup="journeyfrm" />
                                    </td>
                                    <td>
                                        Type of accomodation
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="select" ID="ddlaccomodationtwo" runat="server" />
                                        <asp:RequiredFieldValidator ID="reqddlaccomodationtwo" InitialValue="0" runat="server"
                                            Text="*" Display="Dynamic" ErrorMessage="Please select Type of accomodation of second journey."
                                            ControlToValidate="ddlaccomodationtwo" ForeColor="Red" ValidationGroup="journeyfrm" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table style="margin-left: 15px;">
                                <tr>
                                    <td>
                                        If you require any further journeys or wish to make any notes for the request, please
                                        feel free to add these below (Note):
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtnote" TextMode="MultiLine" Height="100" Style="width: 910px;"
                                            MaxLength="1000" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="text-align: center; margin-top: 10px;">
                            <asp:Button runat="server" CssClass="button" ID="btnSubmit" ValidationGroup="journeyfrm"
                                Text="Submit" OnClick="btnSubmit_Click" />
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="journeyfrm" DisplayMode="List"
                                ShowSummary="false" ShowMessageBox="true" runat="server" />
                        </div>
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <legend>Comments</legend>
                        <table style="margin-left: 15px;">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtComment" TextMode="MultiLine" Height="100" Style="width: 910px;"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <asp:Button runat="server" CssClass="button" ID="btnComment" Text="Add Comment" OnClick="btnComment_Click" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div id="div_Request" runat="server">
                </div>
                <div id="div_Comment" runat="server">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
