﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CorporateUsers.aspx.cs" Inherits="IR_Admin.CorporateSite.CorporateUsers" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");
                    });
                }
                else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
            if ("<%=tab%>" == 1) {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");

                document.getElementById('MainContent_divlist').style.display = 'block';
                document.getElementById('MainContent_divNew').style.display = 'none';
            }
            else {
                $("ul.tabs").tabs("div.inner-tabs-container > div");

                document.getElementById('MainContent_divlist').style.display = 'none';
                document.getElementById('MainContent_divNew').style.display = 'Block';
            }
        });

        function toggleTreeView(ev, hide) {
            var ddTreeView = document.getElementById("ddTreeView");
            ddTreeView.style.display = (hide == null && ddTreeView.style.display == "none") || hide == false ? "" : "none";
            ev = ev ? ev : window.event;
            if (ev) ev.cancelBubble = true;
        }

        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Corporate Users</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="corporateusers.aspx" class="">List</a></li>
            <li><a id="aNew" href="corporateusers.aspx" class="current">New</a> </li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <div id="dvSearch" class="searchDiv1" runat="server" style="float: left; width: 962px;
                        padding: 0px 1px; font-size: 14px; display: none;">
                        <div class="pass-coloum-one" style="width: 50px; float: left; line-height: 25px;">
                            Site:
                        </div>
                        <div class="pass-coloum-two" style="width: 200px; float: left;">
                            <asp:DropDownList runat="server" ID="ddlSitesSearch" Width="90%" />
                        </div>
                        <div class="pass-coloum-one" style="width: 50px; float: left; line-height: 25px;">
                            e-mail:</div>
                        <div class="pass-coloum-two" style="width: 200px; float: left;">
                            <asp:TextBox ID="txtEmailSearch" runat="server" class="input" ClientIDMode="Static"
                                Width="150px" />
                        </div>
                        <div class="pass-coloum-one" style="width: 80px; float: left; line-height: 25px;">
                            User name:</div>
                        <div class="pass-coloum-two" style="width: 202px; float: left;">
                            <asp:TextBox ID="txtUserNameSearch" runat="server" class="input" ClientIDMode="Static"
                                Width="150px" />
                        </div>
                        <div style="float: right; margin-right: 2px;">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" />
                        </div>
                    </div>
                    <div class="crushGvDiv">
                        <table class="searchDiv" width="100%">
                            <tr>
                                <td>
                                    All Users
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </td>
                                <td>
                                    UserName:
                                    <asp:TextBox runat="server" ID="txtSearchUserName" MaxLength="50"></asp:TextBox>
                                </td>
                                <td>
                                    Email Address:
                                    <asp:TextBox runat="server" ID="txtSearchEmail" MaxLength="50"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnSearchSubmit" Text="Submit" CssClass="button" OnClick="btnSearchSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="grdUsers" runat="server" CellPadding="4" CssClass="grid-head2"
                            PageSize="50" ForeColor="#333333" GridLines="None" DataKeyNames="ID" AutoGenerateColumns="False"
                            AllowPaging="True" OnRowCommand="grdUsers_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="UserName" HeaderText="UserName">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EmailAddress" HeaderText="EmailAddress">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FullName" HeaderText="Name">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Note" HeaderText="Note">
                                    <HeaderStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="../images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="../images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this user?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"../images/active.png":"../images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True"?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <HeaderStyle VerticalAlign="Top" />
                                    <ItemStyle Width="8%" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No records found !</EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col" style="width: 30%">
                                User Name
                            </td>
                            <td class="col" style="width: 40%">
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtUserNameRequiredFieldValidator" runat="server"
                                    CssClass="valdreq" ControlToValidate="txtUserName" ErrorMessage="*" ValidationGroup="AUForm"></asp:RequiredFieldValidator>
                            </td>
                            <td rowspan="13" valign="top" class="col" style="width: 30%; border-left: 1px dashed #B1B1B1;">
                                &nbsp; <b>Select Site</b>
                                <div runat="server" id="hideSiteSelection" style="width: 297px; height: 511px; z-index: 100;
                                    position: absolute;" visible="false">
                                </div>
                                <div style="width: 95%; height: 520px; overflow-y: auto; margin-left: 5px;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Password
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="reqpasswordvalid" ControlToValidate="txtPassword"
                                    ValidationGroup="AUForm" runat="server" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="rvPass" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtPassword" Display="Dynamic" ValidationExpression="(?=^.{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                                    ValidationGroup="AUForm" />
                                <a href="#" id="shwpwmsg" title="password minimum length must be 8 to 20 character. should be alphanumeric with lower and upper case.">
                                    <img src="../images/info.png" alt="" width="25px" style="margin-top: 2px; position: absolute;" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Salutation
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlTitle" runat="server" Style="padding: 0 0;">
                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Mrs." Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Ms" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ReqTitle" runat="server" ControlToValidate="ddlTitle"
                                    CssClass="valdreq" ErrorMessage="select title" ToolTip="Title is required." ValidationGroup="AUForm"
                                    InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Forename
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtForename" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Surname
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Email Address
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfEmail" runat="server" CssClass="valdreq" ControlToValidate="txtEmail"
                                    ErrorMessage="*" ValidationGroup="AUForm" />
                                <asp:RegularExpressionValidator ID="txtEmailRegularExpressionValidator" runat="server"
                                    ForeColor="red" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Address"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="AUForm"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                User Office
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlOffice" runat="server" />
                                <asp:RequiredFieldValidator ID="reqddlOffice" runat="server" CssClass="valdreq" ControlToValidate="ddlOffice"
                                    ErrorMessage="*" ValidationGroup="AUForm" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                User Note
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtUserNote" TextMode="MultiLine" Width="276" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />&nbsp;
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="AUForm" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
