﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.CorporateSite
{
    public partial class CorpPassenger : Page
    {
        readonly private ManageCorp _master = new ManageCorp();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                ShowMessage(0, null);
                BindCountry();
                BindGrid();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetCorpPassForEdit(Convert.ToInt32(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid()
        {
            Tab = "1";
            grdPass.DataSource = _master.GetCorpPassengerList();
            grdPass.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdPass_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPass.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void grdPass_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var id = Convert.ToInt32(e.CommandArgument.ToString());
                if (e.CommandName == "ActiveInActive")
                {
                    _master.ActiveInactiveCorpPass(id);
                }
                if (e.CommandName == "Remove")
                {
                    var res = _master.DeleteCorpPass(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                }
                BindGrid();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void BindCountry()
        {
            ddlCountry.DataSource = _master.GetActiveCountryList();
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataValueField = "ID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select a country--", "-1"));
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var ID = Convert.ToInt32(Request["id"]);
                _master.AddCorpPassenger(new tblCorpPassergerInfo
                {
                    ID = ID,
                    CountryID = Guid.Parse(ddlCountry.SelectedValue),
                    PassengerType = txtPType.Text.Trim(),
                    PassengerAge = txtPAge.Text.Trim(),
                    IsActive = chkActive.Checked
                });

                if (Request["id"] != null)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, Request["id"] == null ? "Record added successfully." : "Record updated successfully.");
                BindGrid();
                Tab = "1";
                ClearControls();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlCountry.SelectedIndex = 0;
            txtPType.Text = string.Empty;
            txtPAge.Text = string.Empty;
            chkActive.Checked = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CorpPassenger.aspx");
        }

        public void GetCorpPassForEdit(int id)
        {
            var oP = _master.GetCorpPassById(id);
            if (oP != null)
            {
                ddlCountry.SelectedValue = oP.CountryID.ToString();
                txtPType.Text = oP.PassengerType;
                txtPAge.Text = oP.PassengerAge;
                chkActive.Checked = oP.IsActive;
            }
        }
    }
}