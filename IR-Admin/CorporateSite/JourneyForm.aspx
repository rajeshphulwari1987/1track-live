﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="JourneyForm.aspx.cs"
    Inherits="IR_Admin.CorporateSite.JourneyForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        fieldset
        {
            border: 1px solid #999;
        }
        legend
        {
            font-size: 14px;
        }
        #ui-datepicker-div
        {
            background-color: #fff;
        }
    </style>
    <link rel="stylesheet" href="../Styles/jquery.ui.datepicker.css" />
    <link rel="stylesheet" href="../Styles/jquery.ui.all.css">
    <script type="text/javascript" src="../Scripts/DatePicker/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../Scripts/DatePicker/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../Scripts/DatePicker/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/DatePicker/jquery.ui.widget.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            LoadCal();
        });

        function checkDate() {
            var sdate = $("#MainContent_txtStartDate").val();
            var ldate = $("#txtLastDate").val();
            var date1 = sdate.split('/');
            var date2 = ldate.split('/');

            sdate = (new Date(date1[2], date1[1] - 1, date1[0]));
            ldate = (new Date(date2[2], date2[1] - 1, date2[0]));
            if (sdate != "" && ldate != "") {
                if (sdate > ldate) {
                    alert('Select Lastdate, date bigger than StartDate!');
                    return false;
                }
            }
        }

        function LoadCal() {
            $("#MainContent_txtStartDate, #txtLastDate").bind("cut copy paste", function (e) {
                var key = e.keyCode;
                if (key != 46)
                    e.preventDefault();
            });

            $("#MainContent_txtStartDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                firstDay: 1
            });
            $("#txtLastDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                firstDay: 1
            });
            $("#MainContent_imgCal1 ").click(function () {
                $("#MainContent_txtStartDate").datepicker('show');
            });
            $("#MainContent_imgCal2").click(function () {
                $("#txtLastDate").datepicker('show');
            });
        }        
    </script>
    <style>
        fieldset
        {
            border: 1px solid #ccc;
            font-size: 13px;
            background: #efefef;
            margin-top: 8px;
        }
        legend
        {
            background: #ECECEC;
            border-radius: 0;
            padding: 2px 5px;
            margin-top: 5px;
            color: #100505;
            font-weight: bolder;
            font-size: 13px;
            border: 1px solid #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Booking Request</h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="panes">
            <div id="divlist" runat="server">
                <div class="searchDiv">
                    <table width="100%" style="line-height: 20px">
                        <tr>
                            <td>
                                Passenger Name :
                            </td>
                            <td>
                                Email Address:
                            </td>
                            <td>
                                From Date:
                            </td>
                            <td>
                                To Date:
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" Width="180px" placeholder="Enter Name." />
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" Width="180px" placeholder="Enter Email." />
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" runat="server" class="input" Width="151" autocomplete="off" />
                                <asp:Image ID="imgCal1" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                                    AlternateText="Calendar" Style="position: absolute; margin: -3px 0 0 2px;" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtLastDate" runat="server" class="input" ClientIDMode="Static"
                                    autocomplete="off" Width="151" />
                                <asp:Image ID="imgCal2" runat="server" ImageUrl="~/images/icon-calender.png" BorderWidth="0"
                                    AlternateText="Calendar" Style="position: absolute; margin: -3px 0 0 2px;" />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button1" OnClientClick="checkDate();"
                                    OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="crushGvDiv">
                    <asp:GridView ID="grdCorp" runat="server" CellPadding="4" CssClass="grid-head2" ForeColor="#333333"
                        PageSize="20" GridLines="None" AutoGenerateColumns="False" OnPageIndexChanging="grdCorp_PageIndexChanging"
                        OnRowCommand="grdCorp_RowCommand" OnRowDataBound="grdCorp_RowDataBound">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <%#Eval("Title")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <%#Eval("Firstname")%>
                                    <%#Eval("Lastname")%>
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%#Eval("Status") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone">
                                <ItemTemplate>
                                    <%#Eval("Phone")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <%#Eval("Email")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IATA number">
                                <ItemTemplate>
                                    <%#Eval("IATAno")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Request Date">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd/MM/yyyy")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgView" AlternateText="View" ToolTip="View Details"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="View" ImageUrl="~/images/view.png"
                                        Width="20px" />
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="ibtnEdit" AlternateText="Edit" ToolTip="Edit Details"
                                        Visible='<%# Convert.ToBoolean(Eval("IsBooked"))?false:true %>' CommandArgument='<%#Eval("ID")%>'
                                        CommandName="Modify" ImageUrl="~/images/edit.png" Width="20px" />
                                    <asp:ImageButton runat="server" ID="lbnBooked" AlternateText="Edit" ToolTip="Book your request"
                                        Visible='<%# Convert.ToBoolean(Eval("IsBooked"))?false:true %>' CommandArgument='<%#Eval("ID")%>'
                                        CommandName="Booked" ImageUrl="~/images/pagi-next.png" Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="12%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div id="divDetail" runat="server">
                <fieldset>
                    <legend>Journey Request Form</legend>
                    <div style="float: right">
                        <a href="JourneyForm.aspx" class="valdreq">Go Back</a>
                    </div>
                    <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="25%">
                                <strong>Name: </strong>
                                <%=name %>
                            </td>
                            <td width="25%">
                                <strong>Phone: </strong>
                                <%=phone %>
                            </td>
                            <td width="25%">
                                <strong>Email:</strong>
                                <%=email %>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>IATA no.:</strong>
                                <%=IATAno%>
                            </td>
                            <td width="25%">
                                <strong>Agency name:</strong>
                                <%=agency%>
                            </td>
                            <td width="25%">
                                <strong>Request type:</strong>
                                <%=request%>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>Ticket Type:</strong>
                                <%=tickettype%>
                            </td>
                            <td width="25%">
                                <strong>Ticket Delivered Type:</strong>
                                <%=ticketdelv%>
                            </td>
                            <td width="25%">
                                <strong>Site Name:</strong>
                                <%=SiteName%>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>IP Address:</strong>
                                <%=IPAddress%>
                            </td>
                            <td width="25%">
                                <strong>Created By:</strong>
                                <%=CreatedBy%>
                            </td>
                            <td width="25%">
                                <strong>Created On:</strong>
                                <%=CreatedOn%>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Passengers</legend>
                    <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="25%">
                                <strong>Number of children:</strong>
                            </td>
                            <td width="25%">
                                <%=child %>
                            </td>
                            <td width="25%">
                                <strong>Number of youths:</strong>
                            </td>
                            <td width="25%">
                                <%=youth %>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>Number of adults:</strong>
                            </td>
                            <td width="25%">
                                <%=adult %>
                            </td>
                            <td width="25%">
                                <strong>Number of seniors:</strong>
                            </td>
                            <td width="25%">
                                <%=senior %>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>Lead passenger name:</strong>
                            </td>
                            <td width="25%">
                                <%=lname %>
                            </td>
                            <td width="25%">
                                <strong>Passenger date of birth:</strong>
                            </td>
                            <td width="25%">
                                <%=dob %>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>Discount/ loyalty card type:</strong>
                            </td>
                            <td width="25%">
                                <%=discount %>
                            </td>
                            <td width="25%">
                                <strong>Discount/ loyalty card number:</strong>
                            </td>
                            <td width="25%">
                                <%=discountno %>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">
                                <strong>Discount/ loyalty card expiry date:</strong>
                            </td>
                            <td width="25%">
                                <%=discountdate%>
                            </td>
                            <td width="25%">
                            </td>
                            <td width="25%">
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Journey Details</legend>
                    <asp:Repeater ID="rptJourney" runat="server">
                        <ItemTemplate>
                            <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="25%">
                                        <strong>From:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("From") %>
                                    </td>
                                    <td width="25%">
                                        <strong>To:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("To") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <strong>Via:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("Via") %>
                                    </td>
                                    <td width="25%">
                                        <strong>Date:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Convert.ToDateTime(Eval("Date")).ToString("dd/MM/yyyy") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <strong>Time of departure:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("Deptime")%>
                                    </td>
                                    <td width="25%">
                                        <strong>Time of arrival:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("Arrtime")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <strong>Train number:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("Trainno")%>
                                    </td>
                                    <td width="25%">
                                        <strong>Class of service:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("ClassName")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <strong>Type of accomodation:</strong>
                                    </td>
                                    <td width="25%">
                                        <%#Eval("AccomodationName")%>
                                    </td>
                                    <td width="25%">
                                    </td>
                                    <td width="25%">
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </fieldset>
                <fieldset>
                    <legend>Notes:</legend>
                    <table class="table-stock tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <%=notes %>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
