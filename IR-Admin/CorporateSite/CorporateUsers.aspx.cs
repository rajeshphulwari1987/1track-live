﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.CorporateSite
{
    public partial class CorporateUsers : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        readonly ManageCorp manageCorp = new ManageCorp();
        readonly ManageCorpOffice corpoffice = new ManageCorpOffice();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindUsers(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            tab = "1";

            if (!Page.IsPostBack)
            {
                FillSiteMenu();
                _SiteID = Master.SiteID;
                BindUsers(_SiteID);
                BindParentOffice();
            }
            _SiteID = Master.SiteID;
        }
        void BindParentOffice()
        {
            ddlOffice.DataSource = corpoffice.GetOfficeList(_SiteID);
            ddlOffice.DataTextField = "OfficeName";
            ddlOffice.DataValueField = "ID";
            ddlOffice.DataBind();
            ddlOffice.Items.Insert(0, new ListItem("--Select office--", "-1"));
        }
        void FillSiteMenu()
        {
            trSites.Nodes.Clear();
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList().Where(x => x.IsCorporate).ToList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        void BindUsers(Guid siteID)
        {
            if (siteID == new Guid())
                _SiteID = Master.SiteID;
            grdUsers.DataSource = manageCorp.GetCorporateUserList(siteID).ToList();
            grdUsers.DataBind();
        }

        public void BindCorporateUserSiteLookUpforEdit(Guid ID)
        {
            var LookUpSites = _db.tblAdminUserLookupSites.Where(x => x.AdminUserID == ID).ToList();
            foreach (var Lsites in LookUpSites)
            {
                foreach (TreeNode Pitem in trSites.Nodes)
                {
                    if (Guid.Parse(Pitem.Value) == Lsites.SiteID)
                        Pitem.Checked = true;
                }
            }
        }

        public void BindtblCorpUserListForEdit(Guid ID)
        {
            try
            {
                var result = manageCorp.GettblCorpUserListEdit(ID);
                txtUserName.Text = result.UserName;
                txtForename.Text = result.Forename;
                txtSurname.Text = result.Surname;
                ddlTitle.SelectedValue = result.Salutation.ToString();
                txtEmail.Text = result.EmailAddress;
                txtUserNote.Text = result.Note;
                txtPassword.Attributes.Add("value", result.Password);
                chkActive.Checked = Convert.ToBoolean(result.IsActive);
                ddlOffice.SelectedValue = string.IsNullOrEmpty(result.BranchID.ToString()) ? "-1" : result.BranchID.ToString();
                hdnId.Value = result.ID.ToString();
            }
            catch (Exception ex) { ShowMessage(1, ex.Message); }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (trSites.CheckedNodes.Count < 1)
                {
                    ViewState["tab"] = tab = "2";
                    ShowMessage(2, "Please select at least one site.");
                    return;
                }
                Guid userid = manageCorp.InsertUpdateCorporateUser(new tblAdminUser
                {
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    EmailAddress = txtEmail.Text.Trim(),
                    Forename = txtForename.Text.Trim(),
                    Note = txtUserNote.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    Salutation = Convert.ToInt32(ddlTitle.SelectedValue),
                    Surname = txtSurname.Text.Trim(),
                    UserName = txtUserName.Text.Trim(),
                    IsActive = chkActive.Checked,
                    BranchID = Guid.Parse(ddlOffice.SelectedValue),
                    ID = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value),
                    IsAgent = false,
                    IsCorporate = true,
                    IsEQOAgent = false,
                    AssignTo = 1,
                    ManualPassIsActive = false,
                    MenualP2PIsActive = false,
                    RoleID = null,
                    IsDeleted = false
                });

                if (!string.IsNullOrEmpty(hdnId.Value))
                {
                    var id = Guid.Parse(hdnId.Value);
                    _db.tblAdminUserLookupSites.Where(w => w.AdminUserID == id).ToList().ForEach(_db.tblAdminUserLookupSites.DeleteObject);
                    _db.SaveChanges();
                }

                foreach (TreeNode node in trSites.Nodes)
                {
                    if (node.Checked)
                    {
                        var CorpSiteUserLookUp = new tblAdminUserLookupSite();
                        CorpSiteUserLookUp.ID = Guid.NewGuid();
                        CorpSiteUserLookUp.AdminUserID = userid;
                        CorpSiteUserLookUp.SiteID = Guid.Parse(node.Value);
                        manageCorp.InsertUpdateCorporateUserSiteLookup(CorpSiteUserLookUp);
                    }
                }
                ClearControls();
                BindUsers(_SiteID);
                ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Record added successfully." : "Record updated successfully.");
                hdnId.Value = string.Empty;
                tab = "1";
                ViewState["tab"] = "1";
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CorporateUsers.aspx");
        }

        protected void btnSearchSubmit_Click(object sender, EventArgs e)
        {
            _SiteID = Master.SiteID;
            BindCorporateUser(_SiteID);
        }

        public void ClearControls()
        {
            ddlTitle.SelectedValue = "0";
            txtForename.Text = txtSurname.Text = txtUserName.Text = txtPassword.Text = txtUserNote.Text = txtEmail.Text = txtSearchEmail.Text = txtSearchUserName.Text = "";
            chkActive.Checked = false;
            chkAll.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
                if (node.Checked)
                    node.Checked = false;
        }

        void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    manageCorp.ActiveInactiveCorporateUser(id);
                    BindUsers(_SiteID);
                    tab = "1";
                    ViewState["tab"] = "1";
                }

                if (e.CommandName == "Modify")
                {
                    Guid mId = Guid.Parse(e.CommandArgument.ToString());
                    BindtblCorpUserListForEdit(mId);
                    BindCorporateUserSiteLookUpforEdit(mId);
                    tab = "2";
                }

                if (e.CommandName == "Remove")
                {
                    string[] commandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid cid = Guid.Parse(commandArgumentValues[0]);

                    _db.tblAdminUserLookupSites.Where(w => w.AdminUserID == cid).ToList().ForEach(_db.tblAdminUserLookupSites.DeleteObject);
                    _db.SaveChanges();

                    tblAdminUser deleteCorpUser = _db.tblAdminUsers.First(f => f.ID == cid);
                    _db.tblAdminUsers.DeleteObject(deleteCorpUser);
                    _db.SaveChanges();

                    BindUsers(_SiteID);
                    ShowMessage(1, "Record deleted successfully.");
                    tab = "1";
                    ViewState["tab"] = "1";
                }
            }
            catch (Exception ee) { ShowMessage(2, ee.Message); }
        }

        private void BindCorporateUser(Guid siteID)
        {
            if (chkAll.Checked)
            {
                var list = (from cu in _db.tblAdminUsers
                            join csul in _db.tblAdminUserLookupSites on cu.ID equals csul.AdminUserID into temp
                            from g in temp.DefaultIfEmpty()
                            where cu.IsCorporate
                            select new CorpUserModel
                            {
                                ID = cu.ID,
                                IsActive = cu.IsActive,
                                UserName = cu.UserName,
                                Forename = cu.Forename,
                                Surname = cu.Surname,
                                EmailAddress = cu.EmailAddress,
                                Salutation = cu.Salutation,
                                Note = cu.Note,
                                Password = cu.Password,
                                FullName = cu.Forename + " " + cu.Surname
                            }
                            ).ToList().OrderBy(x => x.UserName).ToList();

                list = list.Where(x => x.UserName.ToLower().Contains(txtSearchUserName.Text.ToLower()) && x.EmailAddress.ToLower().Contains(txtSearchEmail.Text.ToLower())).ToList();
                grdUsers.DataSource = list;
                grdUsers.DataBind();
            }
            else
            {
                var list = (from cu in _db.tblAdminUsers
                            join csul in _db.tblAdminUserLookupSites on cu.ID equals csul.AdminUserID into temp
                            from g in temp.DefaultIfEmpty()
                            where g.SiteID == siteID && cu.IsCorporate
                            select new CorpUserModel
                            {
                                ID = cu.ID,
                                IsActive = cu.IsActive,
                                UserName = cu.UserName,
                                Forename = cu.Forename,
                                Surname = cu.Surname,
                                EmailAddress = cu.EmailAddress,
                                Salutation = cu.Salutation,
                                Note = cu.Note,
                                Password = cu.Password,
                                FullName = cu.Forename + " " + cu.Surname
                            }
                            ).ToList().OrderBy(x => x.UserName).ToList();

                list = list.Where(x => x.UserName.ToLower().Contains(txtSearchUserName.Text.ToLower()) && x.EmailAddress.ToLower().Contains(txtSearchEmail.Text.ToLower())).ToList();
                grdUsers.DataSource = list;
                grdUsers.DataBind();
            }
        }
    }
}
public class CorpUserModel : tblCorpUser
{
    public string FullName { get; set; }
}