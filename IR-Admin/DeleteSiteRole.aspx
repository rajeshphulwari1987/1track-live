﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DeleteSiteRole.aspx.cs" Inherits="IR_Admin.DeleteSiteRole" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script>    
     $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });    
        $(function () {                  
             if(<%=tab.ToString()%>=='1')
            {
               $("ul.list").tabs("div.panes > div");
            }
           
        });
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
       Role Delete Sites
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="DeleteSiteRole.aspx" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" CssClass=" " NavigateUrl="#" runat="server">New/Edit</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:GridView ID="grdSites" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdSites__RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                              <asp:TemplateField HeaderText="UserName">
                                    <ItemTemplate>
                                        <%#Eval("UserName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Remove"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                      <tr>
                            <td class="col" width="25%">
                                Select User
                            </td>
                            <td class="col" width="75%">
                                 <asp:DropDownList ID="ddlUser" runat="server"  AutoPostBack="true"
                                     onselectedindexchanged="ddlUser_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvUser" runat="server" ForeColor="Red" ValidationGroup="vgR"  ErrorMessage="Please select user." Display="Dynamic" ControlToValidate="ddlUser" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Site Name
                            </td>
                            <td class="col">
                                <asp:CheckBoxList ID="chkSite" runat="server" RepeatColumns="3">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" 
                                    Text="Submit" Width="89px"  ValidationGroup="vgR" onclick="btnSubmit_Click" 
                                     />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click" CausesValidation="false"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
