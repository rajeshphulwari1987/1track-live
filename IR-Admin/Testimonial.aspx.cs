﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class Testimonial : Page
    {
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        readonly ManageTestimonial objTestimonial = new ManageTestimonial();
        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            EmailSettingsList();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["edit"] != null)
            {
                Tab = "2";
                if (ViewState["Tab"] != null)
                    Tab = ViewState["Tab"].ToString();
            }

            if (!Page.IsPostBack)
            {
                BindSite();
                _siteID = Master.SiteID;
                SiteSelected();
                EmailSettingsList();
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindTestimonialForEdit(Guid.Parse(Request["edit"]));
                    BindLookupforEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                EmailSettingsList();
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindSite()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        private void EmailSettingsList()
        {
            var result = objTestimonial.GetTestimonialList();
            grdTestimonial.DataSource = result.ToList();
            grdTestimonial.DataBind();
        }

        public void BindTestimonialForEdit(Guid id)
        {
            try
            {
                var result = objTestimonial.GetTestimonialById(id);
                if (result != null)
                {
                    txtTitle.Text = result.Title;
                    txtCustomerName.Text = result.CustomerName;
                    txtWebsite.Text = result.Website;
                    txtMessage.Text = result.Message;
                    btnSubmit.Text = "Update";
                    chkActive.Checked = Convert.ToBoolean(result.IsActive);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindLookupforEdit(Guid id)
        {
            var lookUpSites = _db.tblTestimonialSiteLookUps.Where(x => x.TestimonialId == id).ToList();
            foreach (var lsites in lookUpSites)
            {
                foreach (TreeNode pitem in trSites.Nodes)
                {
                    if (Guid.Parse(pitem.Value) == lsites.SiteId)
                        pitem.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    Tab = "2";
                    return;
                }
                else
                {
                    var sid = objTestimonial.AddUpdateTestimonial(new tblTestimonial
                        {
                            ID = string.IsNullOrEmpty(Request["edit"]) ? new Guid() : Guid.Parse(Request["edit"]),
                            Title = txtTitle.Text.Trim(),
                            CustomerName = txtCustomerName.Text.Trim(),
                            Website = txtWebsite.Text.Trim(),
                            Message = txtMessage.Text.Trim(),
                            IsActive = chkActive.Checked,
                        });

                    var lstSiteLookup = new List<tblTestimonialSiteLookUp>();
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            lstSiteLookup.Add(new tblTestimonialSiteLookUp
                                {
                                    SiteId = Guid.Parse(node.Value),
                                    TestimonialId = sid
                                });
                        }
                    }
                    if (lstSiteLookup.Count > 0)
                        objTestimonial.AddTestimonialSiteLookUp(lstSiteLookup);
                    ShowMessage(1, string.IsNullOrEmpty(Request["edit"]) ? "Testimonial added successfully." : "Testimonial updated successfully.");
                    ClearControls();
                    Response.Redirect("Testimonial.aspx");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Testimonial.aspx");
        }

        protected void grdTestimonial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "ActiveInActive")
                {
                    objTestimonial.ActiveInActiveTestimonial(id);
                    Tab = "1";
                    ViewState["Tab"] = "1";
                    EmailSettingsList();
                }

                if (e.CommandName == "Modify")
                {
                    Tab = "2";
                    ViewState["Tab"] = "2";
                    Response.Redirect("Testimonial.aspx?edit=" + id);
                }

                if (e.CommandName == "Remove")
                {
                    var res = objTestimonial.DeleteTestimonial(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    SiteSelected();

                    Tab = "1";
                    ViewState["Tab"] = "1";
                    EmailSettingsList();
                }
            }
            catch (Exception ee)
            {
                ShowMessage(1, ee.ToString());
            }
        }

        public void ClearControls()
        {
            txtTitle.Text = txtMessage.Text = txtWebsite.Text = txtWebsite.Text = string.Empty;
            foreach (TreeNode node in trSites.Nodes)
            {
                if (node.Checked)
                    node.Checked = false;
            }
        }
    }
}