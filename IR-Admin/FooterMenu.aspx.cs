﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class FooterMenu : Page
    {
        private readonly Masters _oMasters = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
            FillGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            if (!IsPostBack)
            {
                btnSubmit.Enabled = true;
                if (Request["id"] != null)
                {
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                PageLoadEvent();
                GetDetailsByEdit();

                _SiteID = Master.SiteID;
                SiteSelected();
                FillGrid(_SiteID);
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        private void PageLoadEvent()
        {
            IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void FillGrid(Guid siteID)
        {
            grdFooter.DataSource = _oMasters.GetFooterMenuList(siteID).OrderBy(x => x.SortOrder).ToList();
            grdFooter.DataBind();
            HideIndex();
        }

        private void HideIndex()
        {
            if (grdFooter.Rows.Count > 0)
            {
                (grdFooter.Rows[0].FindControl("imgUp")).Visible = false;
                (grdFooter.Rows[grdFooter.Rows.Count - 1].FindControl("imgDown")).Visible = false;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditFooterMenu();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("FooterMenu.aspx");
        }

        public void AddEditFooterMenu()
        {
            try
            {
                var id = string.IsNullOrEmpty(Request.QueryString["id"]) ? new Guid() : Guid.Parse(Request.QueryString["id"]);
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    tab = "2";
                    ViewState["tab"] = "2";
                }
                else
                {
                    var MaxSortOrder = _db.tblFooterMenus.Max(t => t.SortOrder);
                    int sortOrd;
                    if (MaxSortOrder != null)
                    {
                       sortOrd = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                    }
                    else
                    {
                        sortOrd = 1;
                    }

                    var listSitId = (from TreeNode node in trSites.Nodes where node.Checked select Guid.Parse(node.Value)).ToList();
                    _oMasters.AddEditFooterMenu(new Business.FooterMenu
                    {
                        ID = id,
                        ListSiteId = listSitId,
                        MenuName = txtMenuName.Text.Trim(),
                        IsActive = chkIsActv.Checked,
                        SortOrder = sortOrd
                    });
                    ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "Footer menu updated successfully." : "Footer menu added successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ClearControls();
                    tab = "1";
                    ViewState["tab"] = "1";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            txtMenuName.Text = string.Empty;
            chkIsActv.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        public void GetDetailsByEdit()
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    return;

                btnSubmit.Text = "Update";
                var id = Guid.Parse(Request.QueryString["id"]);
                var rec = _oMasters.GetFooterMenuById(id);
                txtMenuName.Text = rec.MenuName;
                chkIsActv.Checked = rec.IsActive;
                foreach (TreeNode itm in trSites.Nodes)
                {
                    itm.Checked = rec.ListSiteId.Contains(Guid.Parse(itm.Value));
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void grdFooter_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Up")
            {
                var commandArgumentValues = e.CommandArgument.ToString().Split(',');
                var firstID = Guid.Parse(commandArgumentValues[0]);
                var sortorder = commandArgumentValues[1] != "" ? Convert.ToInt32(commandArgumentValues[1]) : 0;

                var incrementedSortOrder = sortorder - 1;
                var submenuResult = _db.tblFooterMenus.Where(x => x.SortOrder == incrementedSortOrder).SingleOrDefault();
                if (submenuResult != null)
                {
                    submenuResult.SortOrder = submenuResult.SortOrder + 1;
                }
                else
                {
                    goto DontDo;
                }
                _db.SaveChanges();
                var oMenu = _db.tblFooterMenus.FirstOrDefault(x => x.ID == firstID);
                if (oMenu != null)
                {
                    oMenu.SortOrder = oMenu.SortOrder - 1;
                }

                _db.SaveChanges();
                tab = "1";
                ViewState["tab"] = "1";
            }
            if (e.CommandName == "Down")
            {
                var commandArgumentValues = e.CommandArgument.ToString().Split(',');
                var firstID = Guid.Parse(commandArgumentValues[0]);
                var sortorder = commandArgumentValues[1] != "" ? Convert.ToInt32(commandArgumentValues[1]) : 0;

                var incrementedSortOrder = sortorder + 1;
                var firstResult = _db.tblFooterMenus.Where(x => x.SortOrder == incrementedSortOrder).SingleOrDefault();
                if (firstResult != null)
                {
                    firstResult.SortOrder = sortorder;
                }
                else
                {
                    goto DontDo;
                }
                _db.SaveChanges();
                var secondResult = _db.tblFooterMenus.FirstOrDefault(x => x.ID == firstID);
                if (secondResult != null)
                {
                    secondResult.SortOrder = sortorder + 1;
                }

                _db.SaveChanges();
                tab = "1";
                ViewState["tab"] = "1";
            }
            if (e.CommandName == "Remove")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                var res = _oMasters.DeleteFooterMenu(id);

                var obj = _db.tblFooterMenus.ToList().OrderBy(t => t.SortOrder);
                int i = 1;
                foreach (var o in obj)
                {
                    var oMenu = _db.tblFooterMenus.SingleOrDefault(t => t.ID == o.ID);
                    oMenu.SortOrder = i;
                    _db.SaveChanges();
                    i += 1;
                }
                if (res)
                    ShowMessage(1, "Record deleted Successfully.");
            }
            if (e.CommandName == "ActiveInActive")
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                _oMasters.ActiveInactiveFooterMenu(id);
            }
        DontDo:
            tab = "1";
            ViewState["tab"] = "1";
            _SiteID = Master.SiteID;
            SiteSelected();
            FillGrid(_SiteID);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}