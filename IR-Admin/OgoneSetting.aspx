﻿<%@ Page Title="Ogone Setting " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="OgoneSetting.aspx.cs" Inherits="IR_Admin.OgoneSetting" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">   
//        function pageLoad(sender, args) {$('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 });}

        $(function () {                  
             if(<%=tab.ToString()%>=="1")
            {
                  $("ul.list").tabs("div.panes > div");
                  $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {              
              $("ul.tabs").tabs("div.inner-tabs-container > div");               
                          
            }         
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            $(".tabs a").click(function () {
                $("ul.tabs li").removeClass("active");
                $(this).parent("li").addClass("active");

            });
           
            if(<%=tab.ToString()%>=="2")
            {
            $(".list a").click(function () {
               
                $("ul.list").tabs("div.panes > div");
            });
            }
        });
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 10000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        img
        {
            vertical-align: middle;
            border: none;
        }
        .notespn
        {
            margin-left: 0px;
            font-size: 13px;
            color: #f00;
            width: 300px;
        }
        .select-new
        {
            height: 37px !important;
            width: 369px !important;
        }
        
        .input-new
        {
            width: 357px !important;
            padding: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Ogone Setting</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="OgoneSetting.aspx" class="current">List</a></li>
            <li><a id="aNew" href="OgoneSetting.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdOgone" runat="server" CellPadding="4" DataKeyNames="ID" CssClass="grid-head2"
                        OnRowCommand="grdOgone_RowCommand" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%#Eval("ID")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="OgoneID" HeaderText="REFID" />
                            <asp:BoundField DataField="UserName" HeaderText="UserID" />
                            <asp:BoundField DataField="Language" HeaderText="Language" Visible="False" />
                            <asp:BoundField DataField="Currency" HeaderText="Currency" />
                            <asp:TemplateField HeaderText="Action">
                                <HeaderTemplate>
                                    Language</HeaderTemplate>
                                <ItemTemplate>
                                    <%#GetluangageName(Eval("Language").ToString())%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='Ogonesetting.aspx?id=<%#Eval("ID")%>'>
                                        <img src="images/edit.png" style="margin-top: -6px;" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this record?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this Record?');" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No records found !</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="">
                    <div class="divMain">
                        <table class="tblMainSection">
                            <tr>
                                <td class="col" style="width: 300px;">
                                    Site:
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddlSite" runat="server" Width="205px" CssClass="select-new" />
                                    &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                        ControlToValidate="ddlSite" ValidationGroup="CForm" InitialValue="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    PSPID:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtPspid" runat="server" CssClass="input-new" />
                                    <asp:RequiredFieldValidator ID="reqPspid" runat="server" ControlToValidate="txtPspid"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    REFID:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtOgoneID" runat="server" CssClass="input-new" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOgoneID"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    UserID:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="input-new" />
                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtUserName"
                                        CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Password:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="input-new" />
                                    <br />
                                    <span class="notespn" style="font-size: 12px">Note: Password should be genrated API
                                        A/c PWD (it will not own/login PWD).</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Ogone URL:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtOgoneURL" runat="server" CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Tokenization Ogone URL:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtOgoneTokenizationURL" runat="server" MaxLength="500" CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Return URL Accept:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtReturnURLAccept" runat="server" CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Return URL Decline:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtReturnURLDecline" runat="server" CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Return URL Exception:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtReturnURLException" runat="server" CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Ogone ShaIn Pass Phrase:
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtOgoneShaInPassPhrase" runat="server" CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Ogone ShaIn Pass Phrase (e-commerce/hosted token):
                                </td>
                                <td class="col">
                                    <asp:TextBox ID="txtOgoneShaInPassPhraseTokenization" runat="server" MaxLength="100"
                                        CssClass="input-new" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Language:
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddllanguange" runat="server" Width="205px" CssClass="select-new">
                                        <asp:ListItem Value="-1">--Language--</asp:ListItem>
                                        <asp:ListItem Value="en_GB">English</asp:ListItem>
                                        <asp:ListItem Value="fr">French</asp:ListItem>
                                        <asp:ListItem Value="nl">Dutch</asp:ListItem>
                                        <asp:ListItem Value="it">Italian</asp:ListItem>
                                        <asp:ListItem Value="de">German</asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;<asp:RequiredFieldValidator ID="reqforLanguage" runat="server" ErrorMessage="*"
                                        ForeColor="Red" ControlToValidate="ddllanguange" ValidationGroup="CForm" InitialValue="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Currency:
                                </td>
                                <td class="col">
                                    <asp:DropDownList ID="ddlcurrency" runat="server" CssClass="select-new">
                                    </asp:DropDownList>
                                    &nbsp;<asp:RequiredFieldValidator ID="reqfroCurrency" runat="server" ErrorMessage="*"
                                        ForeColor="Red" ControlToValidate="ddlcurrency" ValidationGroup="CForm" InitialValue="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Is Active?
                                </td>
                                <td class="col">
                                    <asp:CheckBox ID="chkactive" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="col">
                                    Is 3-D Secure payment?
                                </td>
                                <td class="col">
                                    <asp:CheckBox ID="chk3DSecure" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                        Text="Submit" Width="89px" ValidationGroup="CForm" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="BtnCancel_Click"
                                        Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                        </>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
