﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class EurailPromotion : Page
    {
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string Tab = string.Empty;
        string _imagePath = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            EurailPromotionList(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["edit"] != null)
            {
                Tab = "2";
                if (ViewState["Tab"] != null)
                {
                    Tab = ViewState["Tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                BindSite();
                _siteID = Master.SiteID;
                SiteSelected();
                EurailPromotionList(_siteID);
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindListForEdit(Guid.Parse(Request["edit"]));
                    BindLookupforEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _siteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                EurailPromotionList(_siteID);
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindSite()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void BindListForEdit(Guid id)
        {
            try
            {
                var result = _master.GetPromotionListEdit(id);
                txtTitle.Text = result.Title;
                txtNavUrl.Text = result.NavUrl;
                txtDesp.InnerText = Server.HtmlDecode(result.Description);
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
                if (!string.IsNullOrEmpty(result.ImageUrl))
                    imgPromotion.ImageUrl = result.ImageUrl;
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindLookupforEdit(Guid id)
        {
            var lookUpSites = _db.tblEurailPromotionSiteLookups.Where(x => x.PromotionID == id).ToList();
            foreach (var lsites in lookUpSites)
            {
                foreach (TreeNode pitem in trSites.Nodes)
                {
                    if (Guid.Parse(pitem.Value) == lsites.SiteID)
                        pitem.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UploadFile();
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    Tab = "2";
                    return;
                }

                if (btnSubmit.Text == "Submit")
                {
                    var newId = Guid.NewGuid();
                    var promotion = new tblEurailPromotion
                        {
                            ID = newId,
                            Title = txtTitle.Text.Trim(),
                            NavUrl = txtNavUrl.Text.Trim(),
                            Description = txtDesp.InnerHtml,
                            IsActive = chkactive.Checked,
                            ImageUrl = _imagePath.Replace("~/", "")
                        };

                    var pid = _master.AddPromotion(promotion);
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            var promotionLookSites = new tblEurailPromotionSiteLookup { ID = Guid.NewGuid(), PromotionID = pid, SiteID = Guid.Parse(node.Value) };
                            _master.AddPromotionLookupSites(promotionLookSites);
                        }
                    }

                    ShowMessage(1, "Record Added successfully.");
                }
                else if (btnSubmit.Text == "Update")
                {
                    var promotion = new tblEurailPromotion
                        {
                            Title = txtTitle.Text.Trim(),
                            NavUrl = txtNavUrl.Text.Trim(),
                            Description = txtDesp.InnerHtml,
                            ID = Guid.Parse(Request["edit"]),
                            IsActive = chkactive.Checked,
                        };

                    if (!string.IsNullOrEmpty(_imagePath))
                        promotion.ImageUrl = _imagePath.Replace("~/", "");

                    var res = _master.UpdatePromotion(promotion);
                    if (res != null)
                    {
                        //Delete Existing Record
                        var id = Guid.Parse(Request["edit"]);
                        _db.tblEurailPromotionSiteLookups.Where(w => w.PromotionID == id).ToList().ForEach(_db.tblEurailPromotionSiteLookups.DeleteObject);
                        _db.SaveChanges();

                        //Get All Seletected Site
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                var promotionSiteLookUp = new tblEurailPromotionSiteLookup { ID = Guid.NewGuid(), PromotionID = res, SiteID = Guid.Parse(node.Value) };
                                _master.AddPromotionLookupSites(promotionSiteLookUp);
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ShowMessage(1, "Record Updated successfully.");
                }
                ClearControls();
                Tab = "1";
                ViewState["Tab"] = "1";
                EurailPromotionList(_siteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        private void ClearControls()
        {
            txtTitle.Text = string.Empty;
            txtDesp.InnerHtml = string.Empty;
            chkactive.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("EurailPromotion.aspx");
        }
        private void EurailPromotionList(Guid siteId)
        {
            List<Business.Masters.PromotionList> lstdata = new List<Business.Masters.PromotionList>();
            Guid swapid=Guid.Empty;
            siteId = Master.SiteID;
            var result = _master.GetEurailPromotion(siteId);
            grdPromotion.DataSource = result.OrderBy(t => t.sortorder).ThenBy(t => t.Title).ToList();
            grdPromotion.DataBind();
            shortorderpnl.Attributes.Add("style", "display:none");
        }

        protected void grdPromotion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "sortorder")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var result = _master.GetEurailPromotion(Master.SiteID).FirstOrDefault(t => t.ID == id);
                    if (result != null)
                    {
                        hdnsortorderid.Value = result.ID.ToString();
                        lblTitle.Text = result.Title;
                        lblSortorder.Text = result.sortorder;
                        txtSortorder.Text = result.sortorder;
                        if (string.IsNullOrEmpty(result.sortorder))
                            txtSortorder.Attributes.Add("placeholder", "noorder");
                    }
                    lblerrorshortorder.Text = string.Empty;
                    diverrorshortorder.Visible = false;
                    shortorderpnl.Attributes.Add("style", "display:block");
                    Tab = "1";
                    ViewState["Tab"] = "1";
                }
                else
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactivePromotion(id);
                    Tab = "1";
                    ViewState["Tab"] = "1";
                    EurailPromotionList(_siteID);
                }
                else
                if (e.CommandName == "Modify")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    Tab = "2";
                    ViewState["Tab"] = "2";
                    Response.Redirect("EurailPromotion.aspx?edit=" + id);
                }
                else
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _master.DeletePromotion(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    SiteSelected();
                    Tab = "1";
                    ViewState["Tab"] = "1";
                    EurailPromotionList(_siteID);
                }
            }
            catch (Exception ee)
            {
                ShowMessage(2, ee.ToString());
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupPromotionImg.HasFile)
                {
                    if (fupPromotionImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Promotion Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupPromotionImg.FileName.Substring(fupPromotionImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    _imagePath = "~/Uploaded/PromotionImg/";
                    _imagePath = _imagePath + oCom.CropImage(fupPromotionImg, _imagePath, 200, 300);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                Guid ID = Guid.Parse(hdnsortorderid.Value);
                if (!string.IsNullOrEmpty(txtSortorder.Text))
                {
                    int SortOrder = Convert.ToInt32(txtSortorder.Text);
                    _master.updatePromotionSortingorderByIdandSiteId(Master.SiteID, ID, SortOrder);
                }
                shortorderpnl.Attributes.Add("style", "display:none");
                diverrorshortorder.Visible = false;
                lblerrorshortorder.Text = txtSortorder.Text = lblSortorder.Text = lblTitle.Text = hdnsortorderid.Value = string.Empty;
                Tab = "1";
                ViewState["Tab"] = "1";
                EurailPromotionList(_siteID);
            }
            catch (Exception ex)
            {
                shortorderpnl.Attributes.Add("style", "display:block");
                Tab = "1";
                ViewState["Tab"] = "1";
                diverrorshortorder.Visible = true;
                lblerrorshortorder.Text = ex.Message;
            }
        }
    }
}