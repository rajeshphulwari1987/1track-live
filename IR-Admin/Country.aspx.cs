﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class countryPage : Page
    {
        readonly Masters _Master = new Masters();
        db_1TrackEntities db = new db_1TrackEntities();
        public string tab = string.Empty;
        string cFlagImgpath = string.Empty;
        string cBannerpath = string.Empty;
        string cImagepath = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            CountryList(new Guid(), new Guid(), _siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (Request["edit"] != null)
            {
                tab = "2";
                if (ViewState["tab"] != null)
                {
                    tab = ViewState["tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                BindDropDown();
                BindSite();
                _siteID = Master.SiteID;
                SiteSelected();
                CountryList(new Guid(), new Guid(), _siteID);

                liCD.Visible = false;
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindCountryListForEdit(Guid.Parse(Request["edit"]));
                    BindCountryInfoforEdit(Guid.Parse(Request["edit"]));
                    BindCountryLookupforEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                CountryList(new Guid(), new Guid(), _siteID);
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindSite()
        {
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        public void BindDropDown()
        {
            try
            {
                var id = new Guid();
                //Region wise dropdownlist
                ddlRegionWiseCountry.DataSource = _Master.GetRegionList();
                ddlRegionWiseCountry.DataTextField = "RegionName";
                ddlRegionWiseCountry.DataValueField = "RegionID";
                ddlRegionWiseCountry.DataBind();
                ddlRegionWiseCountry.Items.Insert(0, new ListItem("--Select Region--", id.ToString()));

                //Get Region For Add/Edit
                ddlRegion.DataSource = _Master.GetRegionList();
                ddlRegion.DataTextField = "RegionName";
                ddlRegion.DataValueField = "RegionID";
                ddlRegion.DataBind();

                var list = _Master.GetContinentList().Where(x => x.IsActive).ToList();
                ddlContinent.DataSource = list;
                ddlContinent.DataTextField = "Name";
                ddlContinent.DataValueField = "ID";
                ddlContinent.DataBind();
                ddlContinent.Items.Insert(0, new ListItem("--Select Continent--", "0"));

                ddlContinentSearch.DataSource = list;
                ddlContinentSearch.DataTextField = "Name";
                ddlContinentSearch.DataValueField = "ID";
                ddlContinentSearch.DataBind();
                ddlContinentSearch.Items.Insert(0, new ListItem("--Select Continent--", id.ToString()));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindCountryListForEdit(Guid ID)
        {
            try
            {
                var result = _Master.GetcountryListEdit(ID);
                txtOtherCName.Text = result.OtherCountry;
                txtIsoCode.Text = result.IsoCode;
                txtEurailCode.Text = castEurailCode(result.EurailCode.ToString());
                txtCName.Text = result.CountryName;
                txtCCode.Text = result.CountryCode;
                txtDEScode.Text = result.DespatchExpressCountryCode;
                txtRBScode.Text = result.RBSCode;
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
                chkVisible.Checked = Convert.ToBoolean(result.IsVisible);

                if (!string.IsNullOrEmpty(result.CountryFlagImg))
                    imgFlag.ImageUrl = result.CountryFlagImg;

                if (!string.IsNullOrEmpty(result.CountryImg))
                    imgCountry.ImageUrl = result.CountryImg;

                if (!string.IsNullOrEmpty(result.BannerImg))
                    imgBanner.ImageUrl = result.BannerImg;

                ddlContinent.SelectedValue = result.ContinentID == null ? "0" : result.ContinentID.ToString();
                if (ddlRegion.Items.FindByValue(result.RegionID.ToString()) != null)
                {
                    ddlRegion.Items.FindByValue(result.RegionID.ToString()).Selected = true;
                }
                lblCountryDetail.Text = " | " + ddlRegion.SelectedItem.Text + " -- " + txtCName.Text;
                liCD.Visible = true;
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindCountryInfoforEdit(Guid ID)
        {
            try
            {
                var result = _Master.GetCountryInfoEdit(ID);
                if (result != null)
                {
                    txtCapital.Text = result.Capital;
                    txtPopulation.Text = result.Population;
                    txtLanguage.Text = result.Language;
                    txtCurrency.Text = result.Currency;
                    txtTrainOperator.Text = result.TrainOperator;
                    txtTouristBoard.Text = result.TouristBoard;
                    txtTimeZone.Text = result.TimeZone;
                    txtCallingCode.Text = result.CallingCode;
                    txtVisas.Text = result.Visas;
                    txtInternetTLD.Text = result.InternetTLD;
                    txtContent.InnerText = result.CountryInfo;
                    txtIRContent.InnerText = result.IrCountryInfo;
                    txtTitle.Text = result.Title;
                    if (result.Drives != "")
                    {
                        rdbtnDrivesOn.SelectedValue = result.Drives;
                    }
                }
                else
                {
                    rdbtnDrivesOn.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindCountryLookupforEdit(Guid ID)
        {
            // Look up  Sites
            var LookUpSites = db.tblCountrySiteLookUps.Where(x => x.CountryID == ID).ToList();
            foreach (var Lsites in LookUpSites)
            {
                foreach (TreeNode Pitem in trSites.Nodes)
                {
                    if (Guid.Parse(Pitem.Value) == Lsites.SiteID)
                        Pitem.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UploadFile();

                if (btnSubmit.Text == "Submit")
                {
                    var newId = Guid.NewGuid();
                    var _country = new Country();
                    _country.CountryName = txtCName.Text;
                    _country.CountryID = newId;
                    _country.IsActive = chkactive.Checked;
                    _country.IsVisible = chkVisible.Checked;

                    _country.CountryFlagImg = cFlagImgpath.Replace("~/", "");
                    _country.BannerImg = cBannerpath.Replace("~/", "");
                    _country.CountryImg = cImagepath.Replace("~/", "");

                    _country.OtherCountry = txtOtherCName.Text;
                    _country.DespatchExpressCountryCode = txtDEScode.Text;
                    _country.RBSCode = txtRBScode.Text;
                    _country.CountryCode = txtCCode.Text;
                    _country.EurailCode = Convert.ToInt32(txtEurailCode.Text);
                    _country.IsoCode = txtIsoCode.Text;
                    _country.RegionID = Guid.Parse(ddlRegion.SelectedValue);
                    _country.ContinentID = Guid.Parse(ddlContinent.SelectedValue);
                    var parentId = Guid.Parse(ddlRegion.SelectedValue);
                    var MaxSortOrder = db.tblCountriesMsts.Where(x => x.RegionID == parentId).Max(t => t.SortOrder);
                    if (MaxSortOrder != null)
                    {
                        _country.SortOrder = Convert.ToInt32(MaxSortOrder.ToString()) + 1;
                    }
                    else
                    {
                        _country.SortOrder = 1;
                    }

                    Guid cid = _Master.AddCountry(_country);
                    if (cid != null)
                    {
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                var CountryLookSites = new tblCountrySiteLookUp();
                                CountryLookSites.ID = Guid.NewGuid();
                                CountryLookSites.CountryID = cid;
                                CountryLookSites.SiteID = Guid.Parse(node.Value);
                                _Master.AddCountryLookupSites(CountryLookSites);
                            }
                        }
                    }

                    //ShowMessage(1, "Country details added successfully.");

                    //Country Info
                    var _tblCountriesInfo = new CountriesInfo();
                    _tblCountriesInfo.ID = Guid.NewGuid();
                    _tblCountriesInfo.Capital = txtCapital.Text;
                    _tblCountriesInfo.Population = txtPopulation.Text;
                    _tblCountriesInfo.Language = txtLanguage.Text;
                    _tblCountriesInfo.Currency = txtCurrency.Text;
                    _tblCountriesInfo.TrainOperator = txtTrainOperator.Text;
                    _tblCountriesInfo.TouristBoard = txtTouristBoard.Text;
                    _tblCountriesInfo.TimeZone = txtTimeZone.Text;
                    _tblCountriesInfo.CallingCode = txtCallingCode.Text;
                    _tblCountriesInfo.Drives = rdbtnDrivesOn.SelectedItem != null ? rdbtnDrivesOn.SelectedItem.Text : String.Empty;
                    _tblCountriesInfo.Visas = txtVisas.Text;
                    _tblCountriesInfo.CountryID = newId;
                    _tblCountriesInfo.InternetTLD = txtInternetTLD.Text;
                    _tblCountriesInfo.CountryInfo = txtContent.InnerText;
                    _tblCountriesInfo.IrCountryInfo = txtIRContent.InnerText;
                    _tblCountriesInfo.Title = txtTitle.Text;
                    int resinfo = _Master.AddCountryInfo(_tblCountriesInfo);
                    if (resinfo > 0)
                    {
                        ShowMessage(1, "Country details added successfully.");
                    }
                }
                else if (btnSubmit.Text == "Update")
                {
                    var _country = new Country();
                    _country.CountryName = txtCName.Text;
                    _country.CountryID = Guid.Parse(Request["edit"]);
                    _country.ContinentID = Guid.Parse(ddlContinent.SelectedValue);
                    _country.IsActive = chkactive.Checked;
                    _country.IsVisible = chkVisible.Checked;

                    if (!string.IsNullOrEmpty(cFlagImgpath))
                        _country.CountryFlagImg = cFlagImgpath.Replace("~/", "");
                    if (!string.IsNullOrEmpty(cBannerpath))
                        _country.BannerImg = cBannerpath.Replace("~/", "");
                    if (!string.IsNullOrEmpty(cImagepath))
                        _country.CountryImg = cImagepath.Replace("~/", "");

                    _country.OtherCountry = txtOtherCName.Text;
                    _country.DespatchExpressCountryCode = txtDEScode.Text;
                    _country.RBSCode = txtRBScode.Text;
                    _country.EurailCode = Convert.ToInt32(txtEurailCode.Text);
                    _country.IsoCode = txtIsoCode.Text;
                    _country.CountryCode = txtCCode.Text;
                    _country.RegionID = Guid.Parse(ddlRegion.SelectedValue);

                    var res = _Master.UpdateCountry(_country);

                    //Edit Country info
                    var _tblCountriesInfo = new CountriesInfo();
                    _tblCountriesInfo.Capital = txtCapital.Text;
                    _tblCountriesInfo.Population = txtPopulation.Text;
                    _tblCountriesInfo.Language = txtLanguage.Text;
                    _tblCountriesInfo.Currency = txtCurrency.Text;
                    _tblCountriesInfo.TrainOperator = txtTrainOperator.Text;
                    _tblCountriesInfo.TouristBoard = txtTouristBoard.Text;
                    _tblCountriesInfo.TimeZone = txtTimeZone.Text;
                    _tblCountriesInfo.CallingCode = txtCallingCode.Text;
                    _tblCountriesInfo.Drives = rdbtnDrivesOn.SelectedItem != null ? rdbtnDrivesOn.SelectedItem.Text : String.Empty;
                    _tblCountriesInfo.Visas = txtVisas.Text;
                    _tblCountriesInfo.CountryID = Guid.Parse(Request["edit"]);
                    _tblCountriesInfo.InternetTLD = txtInternetTLD.Text;
                    _tblCountriesInfo.CountryInfo = txtContent.InnerText;
                    _tblCountriesInfo.IrCountryInfo = txtIRContent.InnerText;
                    _tblCountriesInfo.Title = txtTitle.Text;
                    int resE = _Master.UpdateCountryInfo(_tblCountriesInfo);

                    if (resE == 0)
                    {
                        _tblCountriesInfo.ID = Guid.NewGuid();
                        resE = _Master.AddCountryInfo(_tblCountriesInfo);
                    }
                    if (resE != null)
                    {
                        //Delete Existing Record
                        var id = Guid.Parse(Request["edit"]);
                        db.tblCountrySiteLookUps.Where(w => w.CountryID == id).ToList().ForEach(db.tblCountrySiteLookUps.DeleteObject);
                        db.SaveChanges();

                        //Get All Seletected Site
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                var countrySiteLookUp = new tblCountrySiteLookUp
                                    {ID = Guid.NewGuid(), CountryID = res, SiteID = Guid.Parse(node.Value)};
                                _Master.AddCountryLookupSites(countrySiteLookUp);
                            }
                        }
                    }
                    ShowMessage(1, "Country details updated successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                }
                ClearControls();
                tab = "1";
                ViewState["tab"] = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ClearControls()
        {
            txtOtherCName.Text = string.Empty;
            txtCCode.Text = string.Empty;
            txtDEScode.Text = string.Empty;
            txtRBScode.Text = string.Empty;
            txtCName.Text = string.Empty;
            txtEurailCode.Text = string.Empty;
            txtIsoCode.Text = string.Empty;
        }
        
        protected void ddlRegionWiseCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteID = Master.SiteID;
            SiteSelected();
            CountryList(Guid.Parse(ddlRegionWiseCountry.SelectedValue), Guid.Parse(ddlContinentSearch.SelectedValue), _siteID);
        }

        protected void ddlContinentSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteID = Master.SiteID;
            SiteSelected();
            CountryList(Guid.Parse(ddlRegionWiseCountry.SelectedValue), Guid.Parse(ddlContinentSearch.SelectedValue), _siteID);
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Country.aspx");
        }

        private void CountryList(Guid rid, Guid contiId, Guid siteId)
        {
            siteId = Master.SiteID;
            rid = Guid.Parse(ddlRegionWiseCountry.SelectedValue);
            var countryResult = (from c in db.tblCountriesMsts
                                 join tcs in db.tblCountrySiteLookUps on c.CountryID equals tcs.CountryID
                                 join ts in db.tblSites on tcs.SiteID equals ts.ID
                                 where ts.ID == siteId
                                 select new { c, tcs, ts });

            var result = (from itm in countryResult
                          select new CountryFields
                              {
                                  ContinentID = itm.c.ContinentID,
                                  CountryID = itm.c.CountryID,
                                  CountryCode = itm.c.CountryCode,
                                  CountryName = itm.c.CountryName,
                                  EurailCode = itm.c.EurailCode,
                                  IsoCode = itm.c.IsoCode,
                                  DespatchExpressCountryCode = itm.c.DespatchExpressCountryCode,
                                  RBSCode = itm.c.RBSCode,
                                  RegionID = itm.c.RegionID,
                                  IsActive = itm.c.IsActive,
                                  CountryFlagImg = itm.c.CountryFlagImg
                              }).ToList();

            if (rid != new Guid())
                result = result.Where(t => t.RegionID == rid).AsEnumerable().Select(x => new CountryFields
                {
                    ContinentID = x.ContinentID,
                    CountryID = x.CountryID,
                    CountryCode = x.CountryCode,
                    CountryName = x.CountryName,
                    EurailCode = x.EurailCode,
                    IsoCode = x.IsoCode,
                    DespatchExpressCountryCode = castEurailCode(x.DespatchExpressCountryCode),
                    RBSCode = x.RBSCode,
                    RegionID = x.RegionID,
                    IsActive = x.IsActive,
                    CountryFlagImg = x.CountryFlagImg
                }).ToList();
            if (contiId != new Guid())
                result = result.Where(t => t.ContinentID == contiId).AsEnumerable().Select(x => new CountryFields
                {
                    ContinentID = x.ContinentID,
                    CountryID = x.CountryID,
                    CountryCode = x.CountryCode,
                    CountryName = x.CountryName,
                    EurailCode = x.EurailCode,
                    IsoCode = x.IsoCode,
                    DespatchExpressCountryCode = castEurailCode(x.DespatchExpressCountryCode),
                    RBSCode = x.RBSCode,
                    RegionID = x.RegionID,
                    IsActive = x.IsActive,
                    CountryFlagImg = x.CountryFlagImg
                }).ToList();

            grdCountry.DataSource = result.OrderBy(x => x.CountryName).ToList();
            grdCountry.DataBind();
        }

        public string castEurailCode(string Ecode)
        {
            if (Ecode.Length == 0)
            {
                return "";
            }
            else if (Ecode.Length == 1)
            {
                return "00" + Ecode;
            }
            else if (Ecode.Length == 2)
            {
                return "0" + Ecode;
            }
            else
            {
                return Ecode;
            }
        }

        protected void grdCountry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ActiveInactiveCountry(id);
                    tab = "1";
                    ViewState["tab"] = "1";
                    CountryList(Guid.Parse(ddlRegionWiseCountry.SelectedValue), Guid.Parse(ddlContinentSearch.SelectedValue), _siteID);
                }

                if (e.CommandName == "Modify")
                {
                    Guid mId = Guid.Parse(e.CommandArgument.ToString());
                    tab = "2";
                    ViewState["tab"] = "2";
                    Response.Redirect("Country.aspx?edit=" + mId);
                }

                if (e.CommandName == "Remove")
                {
                    string[] commandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid cid = Guid.Parse(commandArgumentValues[0]);
                    Guid rid = Guid.Parse(commandArgumentValues[1]);

                    //Delete Existing Record
                    db.tblCountrySiteLookUps.Where(w => w.CountryID == cid).ToList().ForEach(db.tblCountrySiteLookUps.DeleteObject);
                    db.SaveChanges();

                    //Delete Record From TblCountryMst
                    tblCountriesInfo deleteCountryInfo = db.tblCountriesInfoes.First(f => f.CountryID == cid);
                    db.tblCountriesInfoes.DeleteObject(deleteCountryInfo);
                    db.SaveChanges();

                    //Delete Record From TblCountryMst
                    tblCountriesMst deleteCountry = db.tblCountriesMsts.First(f => f.CountryID == cid);
                    db.tblCountriesMsts.DeleteObject(deleteCountry);
                    db.SaveChanges();

                    //Set Index
                    var obj = db.tblCountriesMsts.ToList().Where(x => x.RegionID == rid).OrderBy(t => t.SortOrder);
                    int i = 1;
                    foreach (var o in obj)
                    {
                        tblCountriesMst oMenu = db.tblCountriesMsts.SingleOrDefault(t => t.CountryID == o.CountryID);
                        if (oMenu != null) oMenu.SortOrder = i;
                        db.SaveChanges();
                        i += 1;
                    }

                    tab = "1";
                    ViewState["tab"] = "1";
                    CountryList(Guid.Parse(ddlRegionWiseCountry.SelectedValue), Guid.Parse(ddlContinentSearch.SelectedValue), _siteID);
                }
            }
            catch (Exception ee)
            {
                //ShowMessage(1, ee.ToString());
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                //Guid id = Guid.NewGuid();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupFlagCountryImg.HasFile)
                {
                    if (fupFlagCountryImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Country Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupFlagCountryImg.FileName.Substring(fupFlagCountryImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    cFlagImgpath = "~/Uploaded/CountryFlagImg/";
                    cFlagImgpath = cFlagImgpath + oCom.CropImage(fupFlagCountryImg, cFlagImgpath, 26, 41);
                }

                if (fupCountryImg.HasFile)
                {
                    if (fupCountryImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Country Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupCountryImg.FileName.Substring(fupCountryImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    cImagepath = "~/Uploaded/CountryImg/";
                    cImagepath = cImagepath + oCom.CropImage(fupCountryImg, cImagepath, 190, 272);
                }

                if (fupBannerImg.HasFile)
                {
                    if (fupBannerImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupBannerImg.FileName.Substring(fupBannerImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    cBannerpath = "~/Uploaded/CountryBannerImg/";
                    cBannerpath = cBannerpath + oCom.CropImage(fupBannerImg, cBannerpath, 200, 736);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class CountryFields
    {
        public Guid? ContinentID { get; set; }
        public Guid CountryID { get; set; }
        public string CountryCode { get; set; }
        public string CountryFlagImg { get; set; }
        public string CountryName { get; set; }
        public int? EurailCode { get; set; }
        public string IsoCode { get; set; }
        public string DespatchExpressCountryCode { get; set; }
        public string RBSCode { get; set; }
        public Guid? RegionID { get; set; }
        public bool? IsActive { get; set; }
    }
}
