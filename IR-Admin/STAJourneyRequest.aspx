﻿<%@ Page Title="STA Journey Request" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="STAJourneyRequest.aspx.cs" Inherits="IR_Admin.STAGurneyRequest" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        STA Journey Feedback</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="STAJourneyRequest.aspx" class="current">List</a></li>
            <li><a id="aNew" href="STAJourneyRequest.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdSTAJourneyRequest" runat="server" AutoGenerateColumns="False"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        OnRowCommand="grdSTAJourneyRequest_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <%#Eval("Name")%>
                                </ItemTemplate>
                                <ItemStyle Width="18%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <%#Eval("Email")%>
                                </ItemTemplate>
                                <ItemStyle Width="18%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone">
                                <ItemTemplate>
                                    <%#Eval("Phone")%>
                                </ItemTemplate>
                                <ItemStyle Width="18%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact Us">
                                <ItemTemplate>
                                    <%#Eval("Contact")%>
                                </ItemTemplate>
                                <ItemStyle Width="21%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="ImgUpdateRecord" AlternateText="UpdateRecord"
                                        ToolTip="Update Record" CommandArgument='<%#Eval("Id")%>' CommandName="UpdateRecord"
                                        ImageUrl="~/images/edit.png" />
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                        CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                <b>Site:</b>
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" ClientIDMode="Static" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>First Name:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtFName" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="req1" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtFName" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Last Name:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtLName" runat="server" Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Email:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtEmail" runat="server" Width="500px" />
                                <asp:RequiredFieldValidator ID="req2" runat="server" ValidationGroup="rv" ErrorMessage="*"
                                    ForeColor="Red" Display="Dynamic" ControlToValidate="txtEmail" />
                                <asp:RegularExpressionValidator ID="req3" Display="Dynamic" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(Email Address)$"
                                    ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Phone:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPhone" runat="server" Width="500px" MaxLength="15" onkeypress="return isNumberKey(event)" />
                                <asp:RequiredFieldValidator ID="req4" runat="server" ValidationGroup="rv" ErrorMessage="*"
                                    ForeColor="Red" Display="Dynamic" ControlToValidate="txtPhone" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Contact Us:</b>
                            </td>
                            <td class="col">
                                <asp:RadioButton ID="rdnEmail" runat="server" Text="Email" GroupName="Contact" Checked="true" />
                                <asp:RadioButton ID="rdnPhone" runat="server" Text="Phone" GroupName="Contact" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="new-background">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2">
                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
