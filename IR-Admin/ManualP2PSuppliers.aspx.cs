﻿#region Using
using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;
#endregion

namespace IR_Admin
{
    public partial class ManualP2PSuppliers : Page
    {
        public string Tab = string.Empty;
        Guid ID = Guid.Empty;
        readonly Masters _master = new Masters();
        public ManageOrder _manageorder = new ManageOrder();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID, currID, siteid;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            FillCommition(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if ((Request["id"] != null) && (Request["id"] != ""))
            {
                ID = Guid.Parse(Request["id"]);
                Tab = "2";
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
            }
            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;

                ddlCurrency.DataSource = _master.GetCurrencyList().Where(x => x.IsActive == true);
                ddlCurrency.DataTextField = "ShortCode";
                ddlCurrency.DataValueField = "ID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("--Currency--", "-1"));

                FillMenu();
                FillCommition(_siteID);
                txtcommFee.Attributes.Add("onkeypress", "return keycheck()");
                if (ID != Guid.Empty)
                {
                    BindBookingFeeForEdit(ID);
                }
            }
        }

        private void FillMenu()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }
        public void FillCommition(Guid siteID)
        {
            try
            {
                grdCommition.DataSource = _manageorder.GetManualP2PCommitionbySiteid(siteID);
                grdCommition.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }
        public void BindBookingFeeForEdit(Guid Id)
        {
            try
            {
                btnSubmit.Text = "Update";
                var data = _manageorder.GetManualP2PCommitionById(Id);

                List<Guid> SiteIds = _manageorder.GetSiteIdByManualP2PCommID(Id);
                if (SiteIds != null)
                    foreach (var sItem in SiteIds)
                    {
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Value == sItem.ToString())
                                node.Checked = true;
                        }
                    }

                if (data != null)
                {
                    txtcomname.Text = data.Name;
                    txtcommFee.Text = Convert.ToString(data.Commition);
                    chkisper.Checked = data.Ispercentage;
                    ddlCurrency.SelectedValue = data.CurrencyId.ToString();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var objcomm = new tblManualP2PCommition();
                if (ID == Guid.Empty)
                {
                    objcomm.Id = Guid.NewGuid();
                    objcomm.Name = txtcomname.Text;
                    objcomm.Commition = Convert.ToDecimal(txtcommFee.Text);
                    objcomm.Ispercentage = chkisper.Checked;
                    objcomm.CreatedOn = DateTime.Now;
                    objcomm.CreatedBy = AdminuserInfo.UserID;
                    objcomm.CurrencyId = Guid.Parse(ddlCurrency.SelectedValue);
                }
                else
                {
                    objcomm.Id = ID;
                    objcomm.Name = txtcomname.Text;
                    objcomm.Commition = Convert.ToDecimal(txtcommFee.Text);
                    objcomm.Ispercentage = chkisper.Checked;
                    objcomm.CreatedOn = DateTime.Now;
                    objcomm.CreatedBy = AdminuserInfo.UserID;
                    objcomm.CurrencyId = Guid.Parse(ddlCurrency.SelectedValue);
                }
                if (trSites.Nodes.Cast<TreeNode>().Any(node => node.Checked))
                {
                    //--Add Update Commission

                    _manageorder.AddUpdateManualP2PCommition(ID, objcomm);

                    //--Add Tree LookUp
                    //if (!ID.ToString().Contains("0000"))
                    //{
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            _manageorder.AddManualP2PCommitionSiteLookUp(new tblManualP2PComSiteLookUp
                            {
                                SiteID = Guid.Parse(node.Value),
                                P2PCommissionID = objcomm.Id
                            });
                        }
                    }
                    //}
                    //ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    ShowMessage(1, "Record " + btnSubmit.Text + " successfully.");
                }
                else
                {
                    ShowMessage(2, "Please select at least one Site.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                    Tab = "2";
                    return;
                }

                _siteID = Master.SiteID;
                FillCommition(_siteID);
                ClearControls();
                Tab = "1";
            }
            catch (Exception ex)
            {
                Tab = "2";
                ShowMessage(2, ex.Message);
            }
        }

        protected void grdCommition_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _manageorder.DeleteManualP2PCommition(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                }
                _siteID = Master.SiteID;
                FillCommition(_siteID);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManualP2PSuppliers.aspx");
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        void ClearControls()
        {
            txtcomname.Text = string.Empty;
            txtcommFee.Text = string.Empty;
            chkisper.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }
    }
}