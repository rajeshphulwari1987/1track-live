﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductValidUpTo.aspx.cs" Inherits="IR_Admin.ProductValidUpTo" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {

            $("ul.list").tabs("div.panes > div");

        });   
    </script>
    <script type="text/javascript">
        function DefltMiss(obj) {
            $("INPUT[type='radio']").each(function () {

                if (this.id != obj.children[0].id) {
                    this.checked = false;
                }
                else {
                    this.checked = true;
                }
            });
            return false;
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style>
        .div-row
        {
            font-size: 13px;
            width: 950px;
            padding: 10px;
            float: left;
            border: 1px solid #ccc;
            margin-bottom: 15px;
            background-color: #ECECEC;
        }
        .div-col1
        {
            width: 49%;
            float: left;
        }
        .div-col2
        {
            width: 49%;
            float: left;
        }
        
        select1
        {
            padding: 10px;
            padding: 0.71429rem;
            font-size: 16px;
            font-size: 1.14286rem;
            border: 1px solid #d5d7d8;
            color: #222;
            background-color: #FFF;
            width: 100%;
            text-align: left;
            transition: border 300ms ease-in-out;
            border-radius: 1px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                Traveller Validity</h2>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="ProductValidUpTo.aspx" class="current">List</a></li>
                    <li><a id="aNew" href="ProductValidUpTo.aspx" class="">New</a></li>
                </ul>
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divlist" runat="server" style="display: none;">
                            <div class="crushGvDiv">
                                <div class="div-row" style="padding: 13px !important; margin-bottom: 5px;">
                                    <div class="div-col1">
                                        Traveller Validity:
                                        <asp:DropDownList ID="ddlValidity" runat="server" CssClass="select1" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlValidity_SelectedIndexChanged" />
                                    </div>
                                    <div class="div-col2">
                                        Note:
                                        <asp:DropDownList ID="ddlPromoText" runat="server" CssClass="select1" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlPromoText_SelectedIndexChanged" />
                                    </div>
                                </div>
                            <asp:GridView ID="grvProductValidUpTo" runat="server" AutoGenerateColumns="False"
                                PageSize="10" CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None"
                                Width="100%" OnPageIndexChanging="grvProductValidUpTo_PageIndexChanging" OnRowCommand="grvProductValidUpTo_RowCommand"
                                AllowPaging="True">
                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                <PagerStyle CssClass="paging"></PagerStyle>
                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    Record not found.</EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Traveller Validity">
                                        <ItemTemplate>
                                            <%#Eval("Name")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="20%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Note">
                                        <ItemTemplate>
                                            <%#Eval("Note")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="25%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Promo Pass Text">
                                        <ItemTemplate>
                                            <%#Eval("PromoPassText")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="30%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Eurail Code">
                                        <ItemTemplate>
                                            <%#Eval("EurailCode")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsFlexi">
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" ID="imgIsFlexi" CommandArgument='<%#Eval("ID")%>'
                                                Height="16" CommandName="IsFlexi" AlternateText="status" ImageUrl='<%#Eval("IsFlexi").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                ToolTip='<%#Eval("IsFlexi").ToString()=="True" ?"Flexi":"Continuous" %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                                CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                            <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                                OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                            <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' /></div>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField runat="server" ID="hdnProdValUpToNameId" />
                            <asp:HiddenField runat="server" ID="hdnProdId" />
                        </div>
                    </div>
                    <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                        <table class="tblMainSection">
                            <tr>
                                <td style="width: 70%; vertical-align: top;">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 15%; float: left">
                                                        Language:
                                                    </td>
                                                    <td style="width: 35%; float: left">
                                                        <asp:DropDownList runat="server" ID="ddlLanguage" Width="99%" Enabled="False" />
                                                    </td>
                                                    <td style="width: 15%; float: left">
                                                        Name:
                                                    </td>
                                                    <td style="width: 34%; float: left">
                                                        <asp:TextBox runat="server" ID="txtPassName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                                        <asp:RequiredFieldValidator ID="reqPass" runat="server" ErrorMessage="*" ValidationGroup="submit"
                                                            ControlToValidate="txtPassName" CssClass="valdreq" SetFocusOnError="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; float: left">
                                                        Eurail Code:
                                                    </td>
                                                    <td style="width: 35%; float: left">
                                                        <asp:TextBox ID="txtEurailCode" runat="server" MaxLength="5" Width="99%" />
                                                        <asp:RequiredFieldValidator ID="reqEurailCode" runat="server" ControlToValidate="txtEurailCode"
                                                            Display="Dynamic" CssClass="valdreq" ErrorMessage="*" ValidationGroup="submit" />
                                                        <asp:FilteredTextBoxExtender ID="ftbEurailCode" TargetControlID="txtEurailCode" ValidChars=".0123456789"
                                                            runat="server" />
                                                    </td>
                                                    <td style="width: 15%; float: left">
                                                        Default When Missing:
                                                    </td>
                                                    <td style="width: 34%; float: left">
                                                        <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                            onchange="DefltMiss(this)" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; float: left">
                                                        Day / Month / Year
                                                    </td>
                                                    <td style="width: 35%; float: left">
                                                        <asp:DropDownList runat="server" ID="ddlDay" Width="80px" />
                                                        <asp:DropDownList runat="server" ID="ddlMonth" Width="80px" />
                                                        <asp:DropDownList runat="server" ID="ddlYear" Width="80px" />
                                                    </td>
                                                    <td style="width: 14.5%; float: left">
                                                        Is Active ?
                                                    </td>
                                                    <td style="width: 34%; float: left">
                                                        <asp:CheckBox ID="chkIsActv" runat="server" Style="margin-left: 6px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; float: left">
                                                        Promo Text:
                                                    </td>
                                                    <td style="width: 84%; float: left" colspan="3">
                                                        <asp:TextBox ID="txtPromoText" runat="server" Width="100%" MaxLength="110" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; float: left">
                                                        Note:
                                                    </td>
                                                    <td style="width: 84%; float: left" colspan="3">
                                                        <asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine" Width="100%" Height="40px"
                                                            MaxLength="500" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                        ValidationGroup="submit" OnClick="btnSubmit_Click" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                        <asp:Repeater ID="rptProductValidUpTo" runat="server" OnItemDataBound="rptProductValidUpTo_ItemDataBound">
                            <ItemTemplate>
                                <div class="cat-inner">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 8%">
                                                Language:
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" />
                                                <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                <asp:HiddenField ID="hdnProdValUpToNameId" runat="server" Value='<%#Eval("ID")%>' />
                                            </td>
                                            <td style="width: 10%">
                                                Name:
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox runat="server" ID="txtPassName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                            </td>
                                            <td style="width: 15%">
                                                Default When Missing:
                                            </td>
                                            <td style="text-align: left; width: 23%">
                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                    onchange="DefltMiss(this)" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="cat-inner-alt">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 8%">
                                                Language:
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList runat="server" ID="ddlLanguage" Width="205px" />
                                                <asp:HiddenField ID="hdnLangId" runat="server" Value='<%#Eval("LanguageId")%>' />
                                                <asp:HiddenField ID="hdnProdValUpToNameId" runat="server" Value='<%#Eval("ID")%>' />
                                            </td>
                                            <td style="width: 10%">
                                                Name:
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox runat="server" ID="txtPassName" MaxLength="180" Text='<%#Eval("Name")%>' />
                                            </td>
                                            <td style="width: 15%">
                                                Default When Missing:
                                            </td>
                                            <td style="text-align: left; width: 23%">
                                                <asp:RadioButton ID="rdoMissing" runat="server" Checked='<%#Eval("IsMissing")%>'
                                                    onchange="DefltMiss(this)" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                        <div style="float: right; display: none;">
                            <asp:Button ID="btnAddMore" runat="server" CssClass="button" Text="+ Add More" Width="89px"
                                OnClick="btnAddMore_Click" />
                            &nbsp;
                            <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="-- Remove" Width="89px"
                                OnClick="btnRemove_Click" />
                        </div>
                    </div>
                    </table>
                </div>
            </div>
            </div> </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
