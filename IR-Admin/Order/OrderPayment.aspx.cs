﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Order_OrderPayment : System.Web.UI.Page
{
    public string OrderID = string.Empty;
    public string linkURL = string.Empty;
    public string FormURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null)
        {
            OrderID = Request.Params["req"];
            linkURL = "PaymentAccepted.aspx?req=" + OrderID; 
            FormURL = "OrderPayment.aspx?req=" + OrderID;
        }
        else if (Session["OrderID"] != null)
        {
            OrderID = Session["OrderID"].ToString();
            linkURL = "PaymentAccepted.aspx"; 
            FormURL = "OrderPayment.aspx";
        }

        if (!IsPostBack)
            ShowMessage(0, null);
    }
    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}