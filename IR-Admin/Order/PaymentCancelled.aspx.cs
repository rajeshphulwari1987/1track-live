﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication;
using System.Xml;
using System.Net.Mail;
using Business;

public partial class Order_PaymentCancelled : System.Web.UI.Page
{
    private Guid _siteId;
    public string OrderID = string.Empty;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Params["req"] != null)
            OrderID = Request.Params["req"];
        else if (Session["OrderID"] != null)
            OrderID = Session["OrderID"].ToString();

        var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        string rSiteName = st.SiteURL + "home";
        if (!string.IsNullOrEmpty(OrderID))
        {
            ManageBooking obj = new ManageBooking();
            new ManageBooking().UpdateOrderStatus(6, Convert.ToInt64(OrderID));
        }
        else
            Response.Redirect(rSiteName);
        Session["OrderID"] = null;
        Session["P2POrderID"] = null;
        Session["RailPassData"] = null;

        if (st != null)
        {
            string SiteName = SiteName = st.SiteURL + "OrderCancel";
            Response.Redirect(SiteName, true);
        }
        //Response.Redirect("../OrderCancel");
    }
}