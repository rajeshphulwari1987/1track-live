﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderPayment.aspx.cs" Inherits="Order_OrderPayment" %>

<%@ Import Namespace="OgoneIR" %>
<%@ Import Namespace="Business" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Net.Mail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src='../Scripts/html5.js'></script>
    <script type="text/javascript" src='../Scripts/jquery.nivo.slider.js'></script>
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function redirect() {
            window.parent.location.href = '<%=linkURL %>';
        }
        function checkValidattions() {
            var flag = 1;

            if ($.trim($("#cardnumber").val()) == "") {
                alert('Please enter card number.');
                flag = 0;
                return false;

            }
            else {
                if ($.trim($("#cardnumber").val()).length != 16) {
                    alert('Card number not valid.');
                    flag = 0;
                    return false;
                }
                var checkNum = isNaN($.trim($("#cardnumber").val()));
                if (checkNum == true) {
                    alert('Card number not valid.');
                    flag = 0;
                    return false;
                }
            }
            if ($.trim($("#cardname").val()) == "") {
                alert('Please enter card holder name.');
                flag = 0;
                return false;

            }

            if ($.trim($("#cvc").val()) == "") {
                alert('Please enter card verification code.');
                flag = 0;
                return false;

            }
            var currentYear = (new Date).getFullYear();
            var currentMonth = (new Date).getMonth() + 1;
            var sm = $("#expiremonth").val();
            var sy = $("#expireyear").val();
            if (sm == 0 || sy == 0) {
                alert('Please select expiry date.');
                flag = 0;
                return false;

            }
            if (sy == currentYear) {
                if (parseInt(sm) < parseInt(currentMonth)) {
                    alert('Invalid expiry date.');
                    flag = 0;
                    return false;
                }
            }
            if (flag == 0) {
                return false;
            }
        }
    </script>
</head>
<body>
    <style type="text/css">
body{font-family:	"Trebuchet MS",​Arial,​Helvetica,​sans-serif;
     font-size:13px; padding:0; margin:0;}
.main-column-fieldset{border:solid 1px #cecece; padding:10px;}
.form-row{font-size:13px;  color:#333333;}
.fieldLabel{float:left; width:300px; padding-bottom:10px;}
.fieldLabel .contourIndicator{color:red;}
.form-row input[type="text"] {border: 1px solid #ADB9C2; color: #424242; font-size: 13px; height: 16px; padding:3px; line-height: 16px; margin-bottom: 12px; width: 150px;}
.form-row select{border:1px solid #ADB9C2; color: #424242; font-size: 13px; height: 22px; padding:1px; line-height: 22px; margin-bottom: 12px; width: 76px;}


.form-row{*zoom:1;}
.form-row:before, .form-row:after{content:""; display:table;}
.form-row:after{clear:both;}
/*.form-row input[type="submit"]{padding-bottom:3px; border:1px solid #b42232; background:url(../images/btn-redbg.png) repeat-x; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size:17px;  behavior:url(PIE.htc);  color:#FFF;cursor: pointer;}*/
.form-row input[type="submit"]{padding-bottom:3px; border:1px solid #000; background:url(../images/gray-btn-bg.jpg) repeat-x; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size:17px;  behavior:url(PIE.htc);  color:#FFF;cursor: pointer;}
.form-row.buttons{text-align:right; margin:10px 0; padding-right:2px;}

</style>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div>
        <%
            try
            {
                if (!string.IsNullOrEmpty(OrderID))
                {
                    OrderID = OrderID;
                }
                else
                {
                    Response.Write("<script type='text/javascript'>redirect();</script>");
                }
                string cardtype = "";
                if ((Request["cardtype"] != null) && (Request["cardtype"].ToString() != ""))
                {
                    cardtype = Request["cardtype"].ToString();
                }
                string cardnumber = "";
                string cardname = "";
                string expiremonth = "";
                string expireyear = "";
                string cvc = "";
                string address = "";
                string address2 = "";
                string town = "";
                string postcode = "";
                string country = "";
                string email = "";
                string refe = "";
                string Amount = "";
                int amount = 0;
                if ((Request["cardtype"] != null) && (Request["cardtype"].ToString() != ""))
                {
                    cardtype = Request["cardtype"].ToString();
                }
                if ((Request["cardnumber"] != null) && (Request["cardnumber"].ToString() != ""))
                {
                    cardnumber = Request["cardnumber"].ToString();
                }
                if ((Request["cardname"] != null) && (Request["cardname"].ToString() != ""))
                {
                    cardname = Request["cardname"].ToString();
                }
                if ((Request["expiremonth"] != null) && (Request["expiremonth"].ToString() != ""))
                {
                    expiremonth = Request["expiremonth"].ToString();
                }
                if ((Request["expireyear"] != null) && (Request["expireyear"].ToString() != ""))
                {
                    expireyear = Request["expireyear"].ToString();
                }
                if ((Request["cvc"] != null) && (Request["cvc"].ToString() != ""))
                {
                    cvc = Request["cvc"].ToString();
                }
                if (Session["CustomerData"] != null)
                {
                    string sessionStr = Session["CustomerData"].ToString();
                    string[] str = sessionStr.Split(';');
                    address = str[0];
                    town = str[1];
                    postcode = str[2];
                    country = str[3];
                    email = str[4];
                }
                if (Session["Amount"] != null)
                {
                    Amount = Session["Amount"].ToString();
                    amount = Convert.ToInt32(Convert.ToDecimal(Amount) * 100);
                }
                if ((Request["submitBtn"] != null) && (Request["submitBtn"].ToString() != ""))
                {
                    string ProductName = "Rail Pass";
                    string expirydate = ("0" + expiremonth).Substring(expiremonth.Length - 1, 2) + '/' + expireyear.Substring(2, 2);
                    string expirydateForDB = ("0" + expiremonth).Substring(expiremonth.Length - 1, 2) + expireyear.Substring(2, 2);
                    string fulladdressline = address;
                    if (address2 != "")
                    {
                        fulladdressline = fulladdressline + " " + address2;
                    }

                    string OgoneID = "";
                    string OgoneUserName = "";
                    string OgonePassword = "";
                    string OgoneURL = "";
                    string OgoneShaInPassPhrase = "";
                    string currency = "";
                    string OgoneReturnURL = "";
                    string OgoneLanguage = "";

                    if (Session["siteId"] != null)
                    {
                        db_1TrackEntities _db = new db_1TrackEntities();
                        
                        Guid siteid = Guid.Parse(Session["siteId"].ToString());

                        var OgoneSetting = _db.tblOgoneMsts.Where(x => x.IsActive == (Boolean)true).FirstOrDefault(x => x.SiteID == siteid);
                        if (OgoneSetting != null)
                        {
                            OgoneID = OgoneSetting.OgoneID.Trim();
                            OgoneUserName = OgoneSetting.UserName.Trim();
                            OgonePassword = OgoneSetting.Password.Trim();
                            OgoneURL = OgoneSetting.OgoneURL.Trim();
                            OgoneReturnURL = OgoneSetting.ReturnURLAccept.Trim();
                            OgoneShaInPassPhrase = OgoneSetting.OgoneShaInPassPhrase.Trim();
                            OgoneLanguage = OgoneSetting.Language.Trim();
                            currency = OgoneSetting.Currency.Trim();
                        }
                        else
                        { 
                            OgoneID = "girishksharma";
                            OgoneUserName = "IRWebApi";
                            OgonePassword = "deepak@123";
                            OgoneURL = "https://secure.ogone.com/ncol/test/orderdirect.asp";
                            OgoneReturnURL = "http://windowsdemo.projectstatus.co.uk/1Track/order/testPage.aspx";
                            OgoneShaInPassPhrase = "dotsquarestechnology123#";
                            OgoneLanguage = "en_GB";
                            if (Session["currencyCode"] != null)
                            {
                                currency = Session["currencyCode"].ToString().Trim();
                            }
                            else
                            {
                                currency = "GBP";
                            }
                        
                        }
                    }                  

                    

                    OgoneOrderRequest objOrderRequest = new OgoneOrderRequest(OgoneID, OgoneUserName, OgonePassword, OgoneURL, OgoneShaInPassPhrase);
                    OgoneOrderResponse objOrderResponse = new OgoneOrderResponse();

                    //Mandatory
                    objOrderRequest.ORDERID = "InternationalRailWEBTest" + OrderID;
                    objOrderRequest.AMOUNT = amount;
                    objOrderRequest.CURRENCY = currency;
                    objOrderRequest.CARDNO = cardnumber;
                    objOrderRequest.ED = expirydate;
                    objOrderRequest.CVC = cvc;
                    objOrderRequest.OPERATION = TransactionType.SAL;

                    //Optional
                    objOrderRequest.COM = ProductName;
                    objOrderRequest.CN = cardname;
                    objOrderRequest.EMAIL = email;
                    objOrderRequest.ECOM_PAYMENT_CARD_VERIFICATION = "";
                    objOrderRequest.OWNERADDRESS = fulladdressline;
                    objOrderRequest.OWNERZIP = postcode;
                    objOrderRequest.OWNERTOWN = town;
                    objOrderRequest.OWNERCTY = country;
                    objOrderRequest.OWNERTELNO = "";
                    objOrderRequest.GLOBORDERID = "";
                    objOrderRequest.WITHROOT = "";
                    objOrderRequest.REMOTE_ADDR = "";
                    objOrderRequest.RTIMEOUT = 30;
                    objOrderRequest.ECI = ElectronicCommerceIndicator.ECommerceWithSSLencryption;
                    string strHtmlOutput = "";

                    string strParams = "SessionID=" + this.Session.SessionID;
                    string strComPlus = objOrderRequest.ORDERID;

                    string returnURL = OgoneReturnURL;

                    string returnURLAccept = returnURL + "?type=A";
                    string returnURLDecline = returnURL + "?type=D";
                    string returnURLException = returnURL + "?type=E";


                    objOrderRequest.Set3DSecureParameters(Window3DPageType.MAINW, Request.ServerVariables["HTTP_ACCEPT"], Request.ServerVariables["HTTP_USER_AGENT"], returnURLAccept, returnURLDecline, returnURLException, strParams, strComPlus, OgoneLanguage, false);

                    objOrderResponse = objOrderRequest.GenerateOrderRequest();

                    if (objOrderResponse.RequiresCVVCheck)
                    {
                        strHtmlOutput = objOrderResponse.HTML_ANSWER;
                        Response.Write(strHtmlOutput);
                    }
                    else
                    {
                        strHtmlOutput = "Error: " + objOrderResponse.NCERROR.ToString() + " : " + objOrderResponse.NCERRORPLUS;
                        //Response.Write(strHtmlOutput + ":OrderNumber:" + OrderNumber.ToString());
                        if (Convert.ToInt32(objOrderResponse.NCSTATUS) != 0)
                        {
                            //Response.Write("Error");
                        }
                        else
                        {
                            if ((objOrderResponse.STATUS == TransactionResponseStatus.Authorized) ||
                                (objOrderResponse.STATUS == TransactionResponseStatus.Payment_requested)
                                )
                            {
                                new ManageBooking().UpdateOrderPaymentDetail(Convert.ToInt64(OrderID), cardname, cardnumber, objOrderResponse.PAYID, expirydateForDB, "VISA");
                                Response.Write("<script type='text/javascript'>redirect();</script>");
                                //Response.Redirect("~/Order/PaymentAccepted.aspx");
                            }
                            else
                            {

                            }
                        }
                    }
                }
                if ((Request["cancel"] != null) && (Request["cancel"].ToString() != ""))
                {

                    if (Request.Params["req"] != null)
                        Response.Write("<script type='text/javascript'> window.parent.location.href = 'PaymentCancelled.aspx?req=" + OrderID+"';</script>");
                    else
                        Response.Write("<script type='text/javascript'> window.parent.location.href = 'PaymentCancelled.aspx';</script>");
                }
          
        %>
        <form action='<%=FormURL %>' method="post" id="payment" name="payment">
        <input type="hidden" name="id" value="<%=OrderID%>" />
        <% if (!string.IsNullOrEmpty(OrderID))
           { %>
        <input type="hidden" name="OrderId" id="OrderId" value="<%=OrderID%>" />
        <%}
           else
           {
               Response.Write("<script type='text/javascript'>redirect();</script>");
           } %>
        <fieldset class="main-column-fieldset form-container">
            <%--  <legend>Pay with : </legend>
            <div class="form-row red">
                <p class="instruction">
                    Enter credit / debit card details:</p>
            </div>--%>
            <div class="field-holder">
                <div class="form-row">
                    <label for="" class="fieldLabel">
                        Order Ref : <span class="contourIndicator"></span>
                    </label>
                    <% if (!string.IsNullOrEmpty(OrderID))
                       { %>
                    IRWEB<%=OrderID%>
                    <%}
                       else
                       {
                           Response.Write("<script type='text/javascript'>redirect();</script>");
                       } %>
                </div>
                <div class="form-row">
                    <label for="" class="fieldLabel">
                        Amount : <span class="contourIndicator"></span>
                    </label>
                    <%=Session["currencyCode"].ToString()%>
                    &nbsp;<%=Session["Amount"].ToString()%>
                </div>
                <div class="form-row">
                    <label for="" class="fieldLabel">
                        Pay with <span class="contourIndicator">*</span></label>
                    <img src="../images/VISA_choice.gif" />
                    <img src="../images/icon_mastercard.png" />
                    <select name="cardtype" id="" style="display: none;" class="text" onchange="this.form.action='OrderPayment.aspx?id=<%=OrderID.ToString() %>';this.form.submit();">
                        <option selected="selected" value="">Select credit / debit card type</option>
                        <option value="9F2A1E40-FCCA-4CB4-A282-3C14ED213600" selected="selected">VISA</option>
                    </select>
                </div>
                <div class="form-row">
                    <label for="cardnumber" class="fieldLabel">
                        Card Number (long 16 digit number) <span class="contourIndicator">*</span></label>
                    <input name="cardnumber" type="text" id="cardnumber" maxlength="16" class="text"
                        autocomplete="off" value="<%= cardnumber %>" />
                </div>
                <div class="form-row">
                    <label for="cardname" class="fieldLabel">
                        Cardholder's name <span class="contourIndicator">*</span></label>
                    <input name="cardname" type="text" id="cardname" class="text" maxlength="100" value="<%= cardname %>"
                        autocomplete="off" />
                </div>
                <div class="form-row">
                    <label for="" class="fieldLabel">
                        Expiry date (mm/yyyy) <span class="contourIndicator">*</span></label>
                    <span class="date-input">
                        <select name="expiremonth" id="expiremonth" class="month">
                            <option selected="selected" value="0">mm</option>
                            <%
                for (int count = 1; count < 13; count++)
                {
                            %><option <%= (count.ToString()==expiremonth)?"selected=\"selected\"":"" %> value="<%= count %>">
                                <%= count%></option>
                            <%
                }
                            %>
                        </select>
                        <select name="expireyear" id="expireyear" class="year">
                            <option selected="selected" value="0">yyyy</option>
                            <%
                for (int count = DateTime.Today.Year; count < (DateTime.Today.Year + 8); count++)
                {
                            %><option <%= (count.ToString()==expireyear)?"selected=\"selected\"":"" %> value="<%= count %>">
                                <%= count%></option>
                            <%
                }
                            %>
                        </select>
                    </span>
                </div>
                <div class="form-row">
                    <label for="cvc" class="fieldLabel">
                        Card verification code <span class="contourIndicator">*</span></label>
                    <input name="cvc" type="text" id="cvc" class="text" maxlength="4" autocomplete="off" />
                </div>
            </div>
        </fieldset>
        <div class="form-row buttons">
            <label class="right-aligned-form-label hiddenElement">
            </label>
            <span class="submitButton">
                <input type="submit" name="cancel" id="cancel" value="Cancel" />
                <input type="submit" name="submitBtn" value="Pay &amp; complete order" id="submitBtn"
                    class="clsOrderPayBtn contourButton contourNext contourSubmit" onclick="return checkValidattions();" />
            </span>
        </div>
        <fieldset class="main-column-fieldset form-container">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                       <%-- <img border="0" src="https://secure.ogone.com/images/ACQUIRER.gif" hspace="5" alt="ACQUIRER"
                            title="ACQUIRER" id="NCOLACQ">--%>
                    </td>
                    <td align="center">
                        <a href="https://secure.ogone.com/ncol/PSPabout.asp?lang=1&amp;pspid=girishksharma&amp;branding=OGONE&amp;CSRFSP=%2Fncol%2Ftest%2Forderstandard%2Easp&amp;CSRFKEY=4BA7F5BCD719B7D81BFD1F7E9A441013225246BE&amp;CSRFTS=20131220120638"
                            target="_blank">
                            <img border="0" src="https://secure.ogone.com/images/PP_ogone1.gif" alt="Payment processed by Ogone"
                                title="Payment processed by Ogone" vspace="2" id="NCOLPP"></a>
                    </td>
                    <td align="center">
                        <%--<a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=SECURE.OGONE.COM&amp;lang=en"
                            target="VRSN_Splash">
                            <img name="seal" border="0" src="https://secure.ogone.com/images/verisign_EN.gif"
                                alt="Verisign"></a>--%>
                    </td>
                </tr>
            </table>
        </fieldset>
        </form>
        <%  }
            catch (Exception ex)
            {
                ShowMessage(2, "Operation failed. Please try after some time");
            } %>
    </div>
</body>
</html>
