﻿using System;
using System.Linq;
using Business;

public partial class Order_PaymentException : System.Web.UI.Page
{
    private Guid _siteId;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        new ManageBooking().UpdateOrderStatus(6, Convert.ToInt64(Session["OrderID"]));
        Session["OrderID"] = null;
        Session["RailPassData"] = null;
        var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (st != null)
        {
            string SiteName = st.SiteURL + "OrderCancel";
            Response.Redirect(SiteName, true);
        }
        //Response.Redirect("~/OrderCancel");
    }
}