﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using IR_Admin.OneHubServiceRef;
using Currency = Business.Currency;

namespace IR_Admin
{
    public partial class WorldPaySetting : Page
    {
        readonly Masters _Master = new Masters();
        readonly private ManageBooking _masterBook = new ManageBooking();
        db_1TrackEntities db = new db_1TrackEntities();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            BindGrid(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";

            if (Request["id"] != null)
            {
                tab = "2";
                if (ViewState["tab"] != null)
                {
                    tab = ViewState["tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                BindSite();
                BindGrid(_siteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    tab = "2";
                    GetWorldPayEdit();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void BindSite()
        {
            //Site
            ddlSite.DataSource = _Master.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));

            //Currency
            ddlcurrency.DataSource = _Master.GetCurrencyList().Where(x => x.IsActive == true);
            ddlcurrency.DataTextField = "Name";
            ddlcurrency.DataValueField = "ShortCode";
            ddlcurrency.DataBind();
            ddlcurrency.Items.Insert(0, new ListItem("--Currency--", "-1"));
        }

        void BindGrid(Guid _SiteID)
        {
            tab = "1";
            grdWorldPay.DataSource = _Master.GetWorldPayList(_SiteID);
            grdWorldPay.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddWorldPaySetting();
                else
                    UpdateWorldPaySetting();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        public static string GetluangageName(string code)
        {
            switch (code)
            {
                case "en_gb":
                    return "English";

                case "fr":
                    return "French";

                case "nl":
                    return "Dutch";

                case "it":
                    return "Italian";

                case "de":
                    return "German";

                default:
                    return "English";
            }
        }

        void AddWorldPaySetting()
        {
            try
            {
                _Master.AddWorldPaySetting(new tblWorldPayMst
                {
                    ID = Guid.NewGuid(),
                    MerchantCode = txtMerchantCode.Text.Trim(),
                    InstallationId = txtInstallation.Text.Trim(),
                    UserName = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    WorldPayURL = txtWorldPayURL.Text.Trim(),
                    Language = ddllanguange.SelectedValue,
                    Currency = ddlcurrency.SelectedValue,
                    CreatedBy = AdminuserInfo.UserID,
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    IsActive = chkactive.Checked,
                    IsEnableThreeD = chk3DSecure.Checked
                });
                Response.Redirect("WorldPaySetting.aspx");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message + ex.InnerException);
            }
        }

        void UpdateWorldPaySetting()
        {
            try
            {
                Guid id = Guid.Parse(Request["id"]);
                var objworld = new tblWorldPayMst
                {
                    ID = id,
                    MerchantCode = txtMerchantCode.Text.Trim(),
                    InstallationId = txtInstallation.Text.Trim(),
                    UserName = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    WorldPayURL = txtWorldPayURL.Text.Trim(),
                    Language = ddllanguange.SelectedValue,
                    Currency = ddlcurrency.SelectedValue,
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    ModifiedBy = AdminuserInfo.UserID,
                    IsActive = chkactive.Checked,
                    IsEnableThreeD = chk3DSecure.Checked,
                };
                _Master.UpdateWorldPay(objworld);
                ShowMessage(1, "You have successfully updated WorldPay Settings.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void GetWorldPayEdit()
        {
            if (Request.QueryString["id"] != null)
            {
                Guid id = Guid.Parse(Request.QueryString["id"]);
                tblWorldPayMst oWorldpay = _Master.GetWorldPayEdit(id);
                if (oWorldpay != null)
                {
                    txtMerchantCode.Text = oWorldpay.MerchantCode;
                    txtInstallation.Text = oWorldpay.InstallationId;
                    txtUserName.Text = oWorldpay.UserName;
                    txtPassword.Text = oWorldpay.Password;
                    txtWorldPayURL.Text = oWorldpay.WorldPayURL;
                    ddlcurrency.SelectedValue = oWorldpay.Currency;
                    ddllanguange.SelectedValue = oWorldpay.Language;
                    ddlSite.SelectedValue = oWorldpay.SiteID.ToString();
                    chk3DSecure.Checked = oWorldpay.IsEnableThreeD;
                    if (oWorldpay.IsActive != null) chkactive.Checked = (bool)oWorldpay.IsActive;
                }
                btnSubmit.Text = "Update";
                ddlSite.Enabled = false;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("WorldPaySetting.aspx");
        }

        protected void grdWorldPay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ActiveInactiveWorldPay(id);
                    _siteID = Master.SiteID;
                    BindGrid(_siteID);
                }

                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _Master.DeleteWorldPay(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    BindGrid(_siteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
    }
}
