﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class AddMenu : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        readonly Common _Common = new Common();
        db_1TrackEntities db = new db_1TrackEntities();
        public string tab = string.Empty;
        Guid _SiteID;

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            SiteSelected();
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (ViewState["tab"] != null)
            {
                tab = ViewState["tab"].ToString();
            }
            _SiteID = Master.SiteID;
            SiteSelected();
            if (!Page.IsPostBack)
            {
                FillMenu();
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _SiteID;
            }
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                FillMenu();
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        
        public string GetMenuName(Guid M_Id)
        {
            string menuname = "";
            if (M_Id != null)
            {
                var Menus = db.tblWebMenus.FirstOrDefault(x => x.ID == M_Id);
                menuname = Menus != null ? Menus.Name : "";
            }
            return menuname;
        }

        private void FillMenu()
        {
            var menuResult = db.tblWebMenus.Where(x => x.IsAllowTopBottom == true).OrderBy(x => x.PSortOrder).ToList();
            grdWebMenu.DataSource = menuResult;
            grdWebMenu.DataBind();
            HideIndex();
        }

        private void HideIndex()
        {
            if (grdWebMenu.Rows.Count > 0)
            {
                (grdWebMenu.Rows[0].FindControl("imgUp")).Visible = false;
                (grdWebMenu.Rows[grdWebMenu.Rows.Count - 1].FindControl("imgDown")).Visible = false;
            }
        }

        protected void grdWebMenu_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = 0;
                GridViewRow gvrow;
                GridViewRow previousRow;
                GridViewRow DownRow;
                if (e.CommandName == "Up")
                {
                    string[] CommandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid FirstID = Guid.Parse(CommandArgumentValues[0]);
                    int PSortorder = Convert.ToInt32(CommandArgumentValues[1]);
                    int CSortorder = Convert.ToInt32(CommandArgumentValues[2]);

                    int IncrementedSortOrder = PSortorder - 1;
                    var submenu_result = db.tblWebMenus.Where(x => x.PSortOrder == IncrementedSortOrder).SingleOrDefault();
                    if (submenu_result != null)
                    {
                        submenu_result.PSortOrder = submenu_result.PSortOrder + 1;
                    }
                    else
                    {
                        goto DontDo;
                    }
                    db.SaveChanges();
                    var oMenu = db.tblWebMenus.FirstOrDefault(x => x.ID == FirstID);
                    if (oMenu != null)
                    {
                        oMenu.PSortOrder = oMenu.PSortOrder - 1;
                    }

                    db.SaveChanges();
                    tab = "1";
                    ViewState["tab"] = "1";
                }

                if (e.CommandName == "Down")
                {
                    string[] CommandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid FirstID = Guid.Parse(CommandArgumentValues[0]);
                    int PSortorder = Convert.ToInt32(CommandArgumentValues[1]);
                    int CSortorder = Convert.ToInt32(CommandArgumentValues[2]);
                    int IncrementedSortOrder = PSortorder + 1;
                    var First_result = db.tblWebMenus.Where(x => x.PSortOrder == IncrementedSortOrder).SingleOrDefault();
                    if (First_result != null)
                    {
                        First_result.PSortOrder = PSortorder;
                    }
                    else
                    {
                        goto DontDo;
                    }
                    db.SaveChanges();
                    var secondResult = db.tblWebMenus.FirstOrDefault(x => x.ID == FirstID);
                    if (secondResult != null)
                    {
                        secondResult.PSortOrder = PSortorder + 1;
                    }

                    db.SaveChanges();
                    tab = "1";
                    ViewState["tab"] = "1";
                }
                if (e.CommandName == "IsTop")
                {
                    var M_ID = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ChangeISTop(M_ID);
                    tab = "1";
                    ViewState["tab"] = "1";
                }
                if (e.CommandName == "IsBottom")
                {
                    var M_ID = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ChangeISBottom(M_ID);
                    tab = "1";
                    ViewState["tab"] = "1";
                }
                if (e.CommandName == "ActiveInActive")
                {
                    var M_ID = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ActivateInActivateWebMenu(M_ID);
                    tab = "1";
                    ViewState["tab"] = "1";
                }

            DontDo:
                FillMenu();
                tab = "1";
                ViewState["tab"] = "1";
                FillMenu();
            }
            catch (Exception ee)
            {
                //  ShowMessage(1, ee.ToString());
            }
        }
    }
}
