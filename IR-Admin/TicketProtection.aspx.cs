﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class TicketProtection : Page
    {
        readonly private Masters _oMasters = new Masters();
        readonly private ManageBooking _master = new ManageBooking();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindSite();
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                BindSite();
            }
        }
        public void BindSite()
        {
            //Site
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "0"));
            _SiteID = Master.SiteID;
            ddlSite.SelectedValue = _SiteID.ToString();

            ddlCurrency.DataSource = _oMasters.GetCurrencyList().Where(x => x.IsActive == true);
            ddlCurrency.DataTextField = "Name";
            ddlCurrency.DataValueField = "ID";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, new ListItem("--Currency--", "0"));
            
            GetTicketProtectionData();
        }

        public void GetTicketProtectionData()
        {
            _SiteID = Master.SiteID;
            var list = _master.bindTicketProtection(_SiteID);
            if (list != null)
            {
                ddlCurrency.SelectedValue = list.CurrencyID.ToString();
                txtAmount.Text = Convert.ToString(list.Amount);
                txtDesc.Text = list.Description;
                chkIsActive.Checked = list.IsActive;
            }
            else
            {
                ddlCurrency.SelectedValue ="0";
                txtAmount.Text =string.Empty;
                txtDesc.Text = string.Empty;
                chkIsActive.Checked = false;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
         bool result=  _master.AddTicketProtection(new tblTicketProtection{
                ID=Guid.NewGuid(),
                SiteID=Guid.Parse(ddlSite.SelectedValue),
                CurrencyID=Guid.Parse(ddlCurrency.SelectedValue),
                Amount=Convert.ToDecimal(txtAmount.Text),
                Description=txtDesc.Text,
                IsActive=chkIsActive.Checked,
                IpAddress=_master.GetIpAddress(),
                CreatedBy=AdminuserInfo.UserID,
                CreatedOn=DateTime.Now
            });
        }
        protected void btnCancle_Click(object sender, EventArgs e)
        {
            ddlSite.SelectedValue = "0";
            ddlCurrency.SelectedValue = "0";
            txtAmount.Text = string.Empty;
            txtDesc.Text = string.Empty;
            chkIsActive.Checked = false;
        }
    }
}