﻿using System.Collections.Generic;
using System.Web;
using System.Text;

namespace WebApplication
{
    public class RemotePost
    {
        private readonly Dictionary<string, string> values;

        public string Url { get; set; }
        public string Method { get; set; }
        public string FormName { get; set; }

        public RemotePost()
        {
            Method = "POST";
            FormName = "form1";
            values = new Dictionary<string, string>();
        }

        public void Add(string key, string value)
        {
            values.Add(key, value);
        }

        public void Post()
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
   
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><head>");
            sb.Append(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
            sb.Append(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));

            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "TITLE", "#FF002A"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BGCOLOR", "#FFFFFF"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "TXTCOLOR", "#FF002A"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "TBLBGCOLOR", "#CCCCCC"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "TBLTXTCOLOR", "#000000"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BUTTONBGCOLOR", "#951F35"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BUTTONTXTCOLOR", "#ffffff"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "FONTTYPE", "Verdana"));
            //sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "LOGO", "http://www.1tracktest.com/images/logo.png"));
          
            foreach (KeyValuePair<string, string> element in values)
            {
                sb.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", element.Key.ToUpper().Trim(), element.Value.Trim())); 
            }
             
            sb.Append("</form>");
            sb.Append("</body></html>");

            context.Response.Write(sb);

            context.Response.End();
        }
    }
}
