﻿using System;
using System.Web.Routing;
using Business;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
/// <summary>
/// Summary Description for PageUrls
/// </summary>
public static class PageUrls
{
    public static void DefaultSiteUrl(RouteCollection routes)
    {
        using (db_1TrackEntities db = new db_1TrackEntities())
        {
            var siteUrl = db.tblSites.ToList();
            var page = db.tblWebMenus.FirstOrDefault(x => x.Name.Contains("home"));
            foreach (var item in siteUrl)
            {
                try
                {
                    if (!String.IsNullOrEmpty(item.SiteURL))
                    {
                        if (RouteTable.Routes[item.SiteURL] == null)
                            if (page != null)
                                routes.MapPageRoute(item.SiteURL, item.SiteURL, "~/" + page.PageName, false, new RouteValueDictionary { { "PageId", page.ID } });
                    }
                }
                catch (Exception e)
                {
                }
            }
        }
    }

    public static void MasterSiteUrl(RouteCollection routes)
    {
        try
        {
            using (db_1TrackEntities db = new db_1TrackEntities())
            {
                var oMenu = (from web in db.tblWebMenus
                             join lkp in db.tblWebMenuSiteLookups
                                 on web.ID equals lkp.PageID
                             join st in db.tblSites
                                on lkp.SiteID equals st.ID
                             select new { web, lkp, st }).ToList();

                foreach (var oM in oMenu)
                {
                    try
                    {
                        if (!String.IsNullOrEmpty(oM.web.PageName))
                        {
                            if (RouteTable.Routes[oM.web.PageName] == null)
                                routes.MapPageRoute(oM.st.SiteURL + StringReplace(oM.web.Name), StringReplace(oM.web.Name), "~/" + oM.web.PageName, false, new RouteValueDictionary { { "PageId", oM.web.ID }, { "SiteId", oM.lkp.SiteID } });
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private static string StringReplace(string strUrl)
    {
        strUrl = strUrl.Replace(" ", "-");
        strUrl = strUrl.Replace("&", "and");
        return strUrl;
    }

    public static List<clsURl> GetOldNewUrl()
    { 
        db_1TrackEntities db=new db_1TrackEntities();
        return (from U in db.tblUrlMapings
               join S in db.tblSites on U.SiteId equals S.ID
               where U.IsActive == true// && U.SiteId==SiteId
               select new clsURl { SiteID = S.ID, SiteName=S.DisplayName,OldUrl= U.OldURL,NewURL= U.NewURL,DefaultURL= U.DefaultURL }).ToList();  
    }

    public class clsURl
    { 
     public Guid SiteID{get;set;}
     public string SiteName{get;set;}
     public string OldUrl{get;set;}
     public string NewURL{get;set;}
     public string DefaultURL{get;set;}
    }
}
