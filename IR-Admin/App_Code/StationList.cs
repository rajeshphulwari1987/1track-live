﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Business;

/// <summary>
/// Summary description for StationList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]

public class StationList : System.Web.Services.WebService
{
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static DateTime curentDateTime;
    public ManageBooking _booking = new ManageBooking();
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public StationList()
    {
        if (DateTime.Now > curentDateTime)
        {
            curentDateTime = DateTime.Now.AddDays(10);
            getStaionList();
        }
    }
    public void getStaionList()
    {
        list = _booking.GetAllStaionList().ToList();
    }

    [WebMethod]
    public string[] getStationsXList(string prefixText, string filter, string station)
   {
        prefixText = prefixText.Replace("♥", "'");
        List<getStationList_Result> listStation = list;
        List<getStationList_Result> listStation2 = new List<getStationList_Result>();
        string[] resultdata = { };
        if (filter != string.Empty)
        {
            listStation2.Clear();
            string[] code = filter.Split(',');
            for (int i = 0; i < code.Length; i++)
            {
                var data = list.Where(x => x.StationFilterCode == code[i]).ToList();
                foreach (var item in data)
                {
                    getStationList_Result obj = new getStationList_Result();
                    obj.StationEnglishName = item.StationEnglishName;
                    obj.StationCode = item.StationCode;
                    obj.StationName = item.StationName;
                    obj.StationCodeList = item.StationCodeList;
                    listStation2.Add(obj);
                }
            }
            listStation = listStation2;
        }
        resultdata = listStation.Where(x => x.StationEnglishName != station && x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
        return resultdata;
    }
}
 