﻿using System.Collections.Specialized;

namespace OgoneIR
{


	public class OgoneMaintenanceRequest : OgoneBase
	{
		public OgoneMaintenanceRequest(string ID, string UserName, string Password, string URL, string OgoneShaInPassPhrase)
			: base(ID, UserName, Password, URL, OgoneShaInPassPhrase)
		{

		}

		/// <summary>
		/// </summary>
		/// <param name="MO">Operation Type</param>
		/// <param name="OgonePayId">You can send the PAYID or the orderID to identify the original order. We recommend the use of the PAYID.</param>
		/// <param name="OrderId">You can send the PAYID or the orderID to identify the original order. We recommend the use of the PAYID.</param>
		/// <param name="Amount">
		/// Order amount multiplied by 100. Is only
		/// required when the amount of the
		/// maintenance differs from the amount of the
		/// original authorization. However, Ogone
		/// recommends its use in all cases.
		/// 
		/// The Ogone system will check if the maintenance
		/// transaction amount is not too high in
		/// comparison with the amount of the
		/// authorization/payment.
		/// </param>
		public OgoneMaintenanceResponse GenerateMaintenanceRequest(MaintenanceOperation MO, string OgonePayId, string OrderId, int Amount)
		{
			NameValueCollection NVC = new NameValueCollection();
			//N.B. These MUST be in alphabetical order
			NVC.Add("AMOUNT", Amount.ToString());
			NVC.Add("OPERATION", MO.ToString());
			NVC.Add("ORDERID", OrderId);
			NVC.Add("PAYID", OgonePayId);
			NVC.Add("PSPID", base.PSPID);
			NVC.Add("PSWD", base.PSWD);
			NVC.Add("USERID", base.USERID);
			string strReturn = base.GenerateRequest(NVC);
			return new OgoneMaintenanceResponse(strReturn);
		}
	}

	/// <summary>
	/// REN,	 renewal of authorization, if the original authorization is no longer valid.
	///	DEL,	 delete authorization, leaving the transaction open for possible further maintenance operations.
	///	DES,	 delete authorization, closing the transaction after this operation
	///	SAL,	 partial data capture (payment), leaving the transaction open for a possible other data capture.
	///	SAS,	 (last) partial or full data capture (payment), closing the transaction (for further data captures) after this data capture.
	///	RFD,	 partial refund (on a paid order), leaving the transaction open for a possible other refund.
	///	RFS		 (last) partial or full refund (on a paid order), closing the transaction after this refund.
	/// </summary>
	public enum MaintenanceOperation
	{
		/// <summary>
		/// renewal of authorization, if the original authorization is no longer valid.
		/// </summary>
		REN,
		/// <summary>
		/// delete authorization, leaving the transaction open for possible further maintenance operations.
		/// </summary>
		DEL,
		/// <summary>
		/// delete authorization, closing the transaction after this operation
		/// </summary>
		DES,
		/// <summary>
		/// partial data capture (payment), leaving the transaction open for a possible other data capture.
		/// </summary>	
		SAL,
		/// <summary>
		/// (last) partial or full data capture (payment), closing the transaction (for further data captures) after this data capture.
		/// </summary>	
		SAS,
		/// <summary>
		/// partial refund (on a paid order), leaving the transaction open for a possible other refund.
		/// </summary>	
		RFD,
		/// <summary>
		/// (last) partial or full refund (on a paid order), closing the transaction after this refund.
		/// </summary>
		RFS
	}

}