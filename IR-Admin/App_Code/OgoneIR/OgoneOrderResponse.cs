﻿using System;
using System.Xml;
using System.Text;

namespace OgoneIR
{

	public class OgoneOrderResponse
	{
		private string _orderID;
		private string _PAYID;
		private string _NCSTATUS;
		private string _NCERROR;
		private string _NCERRORPLUS;
		private string _ACCEPTANCE;
		private string _STATUS;
		private string _SCO_CATEGORY;
		private string _amount;
		private string _currency;
		private string _PM;
		private string _BRAND;

		private string _IPCTY;
		private string _CCCTY;
		private string _ECI;
		private string _CVCCheck;
		private string _AAVCheck;
		private string _VC;

		private string _HTML_ANSWER;

		private string strResponse;

		private bool bolCVVCheck;

		private XmlDocument xDoc;

		public OgoneOrderResponse()
		{
			SetDefaults();
		}

		public OgoneOrderResponse(string Response)
		{
			strResponse = "";
			LoadFromXMLString(Response);
		}

		private void SetDefaults()
		{
			_orderID = "IRTestOrder1";
			_PAYID = "";
			_NCSTATUS = "";
			_NCERROR = "";
			_NCERRORPLUS = "";
			_ACCEPTANCE = "";
			_STATUS = "0";
			_SCO_CATEGORY = "";
			_amount = "";
			_currency = "";
			_PM = "";
			_BRAND = "";

			_IPCTY = "";
			_CCCTY = "";
			_ECI = "";
			_CVCCheck = "";
			_AAVCheck = "";
			_VC = "";
			_HTML_ANSWER = "";
			bolCVVCheck = false;
		}

		public void LoadFromXMLString(string Response)
		{
			strResponse = Response;
			xDoc = new XmlDocument();
			xDoc.LoadXml(Response);
			_orderID = GetValue("orderID");
			_PAYID = GetValue("PAYID");
			_NCSTATUS = GetValue("NCSTATUS");
			_NCERROR = GetValue("NCERROR");
			_NCERRORPLUS = GetValue("NCERRORPLUS");
			_ACCEPTANCE = GetValue("ACCEPTANCE");
			_STATUS = GetValue("STATUS");
			_SCO_CATEGORY = GetValue("SCO_CATEGORY");
			_amount = GetValue("amount");
			_currency = GetValue("currency");
			_PM = GetValue("PM");
			_BRAND = GetValue("BRAND");
			_IPCTY = GetValue("IPCTY");
			_CCCTY = GetValue("CCCTY");
			_ECI = GetValue("ECI");
			_CVCCheck = GetValue("CVCCheck");
			_AAVCheck = GetValue("AAVCheck");
			_VC = GetValue("VC");
			_HTML_ANSWER = GetElementValue("HTML_ANSWER"); //this will be BASE64 encoded

			if (_HTML_ANSWER.Length > 0)
			{
				System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
				System.Text.Decoder utf8Decode = encoder.GetDecoder();

				byte[] todecode_byte = Convert.FromBase64String(_HTML_ANSWER);
				int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
				char[] decoded_char = new char[charCount];
				utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
				_HTML_ANSWER = new String(decoded_char);
				bolCVVCheck = true;
			}
		}

		private string GetValue(string Field)
		{
			if (xDoc.DocumentElement.Attributes[Field] != null)
				return xDoc.DocumentElement.Attributes[Field].Value;
			return "";
		}

		private string GetElementValue(string ElementName)
		{
			if (xDoc.DocumentElement.GetElementsByTagName(ElementName)[0] != null)
				return xDoc.DocumentElement.GetElementsByTagName(ElementName)[0].InnerXml;
			return "";
		}

		public string ORDERID { get { return _orderID; } }
		public string PAYID { get { return _PAYID; } }
		public string NCSTATUS { get { return _NCSTATUS; } }
		public string NCERROR { get { return _NCERROR; } }
		public string NCERRORPLUS { get { return _NCERRORPLUS; } }
		public string ACCEPTANCE { get { return _ACCEPTANCE; } }
		public TransactionResponseStatus STATUS { get { return (TransactionResponseStatus)Convert.ToInt32(_STATUS); } }
		public string SCO_CATEGORY { get { return _SCO_CATEGORY; } }
		public string amount { get { return _amount; } }
		public string currency { get { return _currency; } }
		public string PM { get { return _PM; } }
		public string BRAND { get { return _BRAND; } }
		public string IPCTY { get { return _IPCTY; } }
		public string CCCTY { get { return _CCCTY; } }
		public string ECI { get { return _ECI; } }
		public string CVCCheck { get { return _CVCCheck; } }
		public string AAVCheck { get { return _AAVCheck; } }
		public string VC { get { return _VC; } }
		public string HTML_ANSWER { get { return _HTML_ANSWER; } }
		public bool RequiresCVVCheck { get { return bolCVVCheck; } }
		public bool IsDuplicateRequest { get { if (_STATUS == "0" && _NCERROR == "50001113")return true; return false; } }

		public string Response { get { return strResponse; } }
	}

}