﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ScriptingTag : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        readonly ManageScriptingTag objScriptingTag = new ManageScriptingTag();
        public string Tab = string.Empty;
        Guid _siteID;

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            FillGrid(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";

            if (!Page.IsPostBack)
            {
                FillCommonddl();
                ddlSite.SelectedValue = _siteID == new Guid() ? Master.SiteID.ToString() : _siteID.ToString();
                _siteID = Master.SiteID;
                FillGrid(_siteID);
            }
        }

        public void FillGrid(Guid siteId)
        {
            try
            {
                if (siteId == new Guid())
                    _siteID = Master.SiteID;
                grdScriptingTag.DataSource = objScriptingTag.GetListScriptingTagBySiteId(_siteID).OrderBy(x => x.PageType).ToList();
                grdScriptingTag.DataBind();
                Tab = "1";
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ScriptingTag.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool res = objScriptingTag.AddScriptingTag(new tblScriptingTag
                    {
                        ID = string.IsNullOrEmpty(hdnId.Value) ? Guid.Empty : Guid.Parse(hdnId.Value),
                        Script = txtScript.Text.Trim(),
                        SiteID = Guid.Parse(ddlSite.SelectedValue),
                        IsActive = chkIsActive.Checked,
                        PageType = ddlPages.SelectedValue
                    });
                if (res == true)
                    ClearControls();
                FillGrid(_siteID);
                ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Record added successfully." : "Record updated successfully.");
                hdnId.Value = string.Empty;
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void FillCommonddl()
        {
            try
            {
                ddlSite.DataSource = _master.GetActiveSiteList();
                ddlSite.DataTextField = "DisplayName";
                ddlSite.DataValueField = "ID";
                ddlSite.DataBind();
                ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
            }
            catch (Exception ex) { ShowMessage(1, ex.Message); }
        }

        protected void grdp2pSetting_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Guid id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "ActiveInActive")
                {
                    objScriptingTag.ActiveScriptingTag(id);
                    FillGrid(_siteID);
                }
                else if (e.CommandName == "Remove")
                {
                    objScriptingTag.RomoveScriptingTag(id);
                    FillGrid(_siteID);
                    ShowMessage(1, "Record delete successfully.");
                }
                else if (e.CommandName == "UpdateRecord")
                {
                    var result = objScriptingTag.GetScriptingTagById(id);
                    if (result != null)
                    {
                        ddlSite.SelectedValue = result.SiteID.ToString();
                        txtScript.Text = result.Script;
                        chkIsActive.Checked = result.IsActive;
                        ddlPages.SelectedValue = result.PageType;
                        hdnId.Value = id.ToString();
                        Tab = "2";
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        public void ClearControls()
        {
            txtScript.Text = string.Empty;
            chkIsActive.Checked = false;
            ddlPages.SelectedIndex = 0;
        }
    }
}