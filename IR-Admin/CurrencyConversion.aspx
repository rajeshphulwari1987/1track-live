﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CurrencyConversion.aspx.cs"
    MasterPageFile="~/Site.master" Inherits="IR_Admin.CurrencyConversion" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <script type="text/javascript">
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
                return true;
            return false;
        }
    </script>
    <script type="text/javascript">
        function confirms() {
            return Confirm('Are you sure? You want to update currency setting.');
        }
    </script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <style type="text/css">
        #aList
        {
            width: 43px;
        }
        .trCurr
        {
            height: 34px;
            line-height: 34px;
            background-color: #ECECEC;
        }
        .float-rt
        {
            float: right;
        }
        .clsOdd
        {
            background-color: #FBDEE6;
            vertical-align:top;
        }
        .clsEven
        {
            background-color: #ECECEC;
            vertical-align:top;
        }
        .clsArr
        {
            padding: 4px 0 0 3px;
            float: left;
        }
        .clsTxt
        {
            font-weight: bold;
            font-size: 13px;
            float: left;
        }
        .mrgnLt12
        {
            margin-left: 12px;
        }
        .mrgnB5
        {
            margin-bottom: 5px;
        }
        .content
        {
            width: 89%;
            padding: 1%;
        }
        .grid-outer_scroll{ width:98%; overflow:scroll; }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Currency Exchange Rates
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        </asp:Panel>
    </div>
    <div class="grid-outer_scroll">
    <table id="tblCurr" cellpadding="5" cellspacing="0" width="100%" runat="server" visible="False">
        <tr class="trCurr">
            <td>
            <div style="width:130px; float:left; line-height:32.9px;">
                <span class="clsTxt">Source</span> <span style="float: left">
                    <asp:Image ID="Image1" runat="server" ImageUrl="schemes/images/downArrow.png" Width="10px" /></span>
                <span class="clsTxt" style="margin-left: 10px">Target</span> <span style="float: left">
                    <asp:Image ID="Image2" runat="server" ImageUrl="schemes/images/rightArrow.png" Width="10px" /></span>
                <asp:Repeater ID="rptCurr" runat="server">
                    <ItemTemplate>
                        <br />
                        <span style="font-size: 12px; font-weight: bold;">
                            <%#Eval("ShortCode")%>
                            (<%#Eval("Symbol") %>)</span>
                    </ItemTemplate>
                </asp:Repeater> </div>
            </td>
            <td >
                <asp:Repeater ID="rptCurrInner" runat="server" OnItemDataBound="rptCurrInner_ItemDataBound">
                    <ItemTemplate>
                        <td align="center" class='<%#(Container.ItemIndex % 2 == 0) ? "clsOdd" : "clsEven" %>'>
                            <span style="font-size: 12px; font-weight: bold;">
                                <%#Eval("ShortCode")%>
                                (<%#Eval("Symbol") %>) </span>
                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("Id") %>' />
                            <asp:Repeater ID="rptCurrRate" runat="server" OnItemDataBound="rptCurrRate_ItemDataBound">
                                <ItemTemplate>
                                    <br />
                                    <asp:HiddenField ID="hdnCurrID" runat="server" Value='<%#Eval("id") %>' />
                                    <asp:HiddenField ID="hdnTrgID" runat="server" Value='<%#Eval("TrgID") %>' />
                                    <asp:TextBox ID="txtMultiplier" runat="server" Text='<%#Eval("Multiplier")%>' Width="65px"
                                        ForeColor='<%#Eval("IsEnabled").ToString()=="True" ? System.Drawing.Color.Green:System.Drawing.Color.Black %>'
                                        Enabled='<%#Eval("IsEnabled")%>' />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    </div>
    <div id="dvSave" runat="server" width="100%" class="float-rt" visible="False">
        <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Save" Width="89px"
            OnClientClick="return confirms()" ValidationGroup="CurForm" OnClick="btnSubmit_Click" />
    </div>
</asp:Content>
