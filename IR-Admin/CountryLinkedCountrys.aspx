﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CountryLinkedCountrys.aspx.cs" Inherits="IR_Admin.CountryLinkedCountrys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        .col
        {
            float: left;
            width: 239px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Linked eurail countries</h2>
    <div class="grid-sec2">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="float: left; padding-left: 10px;">
                    <div class="col" style="width: 100%;">
                        <asp:DropDownList runat="server" ID="ddlCountrycombinationLevel" Style="width: 250px;
                            height: 26px; padding: 4px; margin: 5px;" AutoPostBack="true" OnSelectedIndexChanged="ddlCountrycombinationLevel_SelectedIndexChanged">
                            <asp:ListItem Value="2" Text="2nd country combination"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3rd country combination"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4th country combination"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col" style="width: 100%;">
                        <asp:DropDownList runat="server" ID="ddlCountrycombination" AutoPostBack="True" OnSelectedIndexChanged="ddlCountrycombination_SelectedIndexChanged"
                            Style="width: 250px; height: 26px; padding: 4px; margin: 5px;" />
                        <asp:CheckBox runat="server" ID="chkcountryEnable" Text="IsActive" />
                    </div>
                    <div class="col" style="width: 100%; font-size: 12px">
                        <span style="color: #00aeef;">Note: </span>
                        <br />
                        <b>Step1: </b>Choose 'All Countries' in dropdown and select listed countries for
                        displaying 'Choose your first country' dropdown on pass selection
                        page and click on 'Submit' button.
                        <br />
                        <b>Step2: </b>Choose specific country and select related combination in listed countries
                        and then click on 'Submit' button.
                    </div>
                    <asp:Repeater runat="server" ID="rptcountryLinkedId">
                        <ItemTemplate>
                            <div class="col">
                                <asp:CheckBox runat="server" ID="chkEnable" Checked='<%#Eval("EnableLinkedCountry") %>' />
                                <asp:Label runat="server" ID="lblcountry" Text='<%#Eval("Country")%>'></asp:Label>
                                <asp:HiddenField runat="server" ID="hdncountryId" Value='<%#Eval("EurailCountryID")%>'>
                                </asp:HiddenField>
                            </div>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <div style="float: left; width: 100%; margin: 10px;">
            <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click"
                CssClass="button" /></div>
    </div>
</asp:Content>
