﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CMSPages.aspx.cs" Inherits="IR_Admin.CMSPages" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <link href="Styles/slider.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/thumbnail-slider.js" type="text/javascript"></script>
    <link href="Styles/FaqtblQAstyle.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 10000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        .trainblockbanner img
        {
            height: 80px;
            width: 120px;
        }
        #mcts1 > *
        {
            display: block;
        }
        #mcts1 div.item
        {
            padding: 2px;
            margin-right: 20px;
            background-color: white;
            text-align: center;
            position: relative;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnimage" runat="server" />
    <h2>
        CMS Pages</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="CMSPages.aspx" class="current">List</a></li>
            <li><a id="aNew" href="CMSPages.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdEurail" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnRowCommand="grdEurail_RowCommand" OnPageIndexChanging="grdEurail_PageIndexChanging">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Page Name">
                                <ItemTemplate>
                                    <%#Eval("Name")%>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link">
                                <ItemTemplate>
                                    <a target="_blank" href='<%#SiteUrl + Eval("ID").ToString().Substring(0, 2) + "-" + Eval("Name").ToString().Replace(" - ", "-").Replace("/", "-").Replace("&", "and").Replace(" ", "-").Replace("'", "").ToLower()%>'>
                                        <%#SiteUrl + Eval("ID").ToString().Substring(0, 2) + "-" + Eval("Name").ToString().Replace(" - ", "-").Replace("/", "-").Replace("&", "and").Replace(" ", "-").Replace("'", "").ToLower()%>
                                    </a>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="P2P widget">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsWidgetVisible").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsWidgetVisible").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='CMSPages.aspx?id=<%#Eval("ID")%>'>
                                        <img alt="edit" src="images/edit.png" /></a>
                                    <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                <b>Site:</b>
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" ClientIDMode="Static" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Page Name:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtPageName" runat="server" Width="500px" MaxLength="50" />
                                &nbsp;<asp:RequiredFieldValidator ID="reqtxtPageName" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="txtPageName" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Title:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtTitle" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtTitle" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Keyword:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="500px" TextMode="MultiLine" Height="100" />
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="txtKeyword" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Image:</b>
                            </td>
                            <td class="col">
                                <asp:FileUpload ID="fupImage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Visible P2P widget:</b>
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkVisible" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <legend><b>Description</b></legend>
                                <div class="cat-outer-cms">
                                    <textarea id="txtDesc" runat="server"></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Gallery Image:
                            </td>
                            <td>
                                <asp:Label runat="server" ID="myThrobber_tq" Style="display: none;">
                                <img align="absmiddle" alt="" src='images/uploading.gif'/>
                                </asp:Label>
                                <asp:AjaxFileUpload ID="AjaxFileUpload2" runat="server" padding-bottom="4" ContextKeys="2"
                                    padding-left="2" padding-right="1" padding-top="4" ThrobberID="myThrobber_tq"
                                    OnUploadComplete="AjaxFileUpload2_OnUploadComplete" />
                                <br />
                                <div id="testuploaded" style="display: none; padding: 4px; border: gray 1px solid;">
                                    <h4>
                                        Uploaded files:</h4>
                                    <hr />
                                    <div id="fileList">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="trainblockbanner" style="width: 690px; margin-top: 5px;">
                                    <div id="mcts1" style="width: 99%; height: 120px; padding: 10px 7px 10px 1px; border: 0px solid #ccc;">
                                        <asp:Repeater ID="rptGallery" runat="server" OnItemCommand="rptGallery_ItemCommand">
                                            <ItemTemplate>
                                                <div class="clsMtImg">
                                                    <div style="float: right; height: 18px; background: #EEEEEE; width: 100%; text-align: right;">
                                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/cross.png"
                                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                                    </div>
                                                    <img src='<%#Eval("GalleryImage") %>' alt="" border="0" class="clsImg" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Meta Title:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtMetaTitle" runat="server" MaxLength="500" Width="600px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Meta Keyword:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtMetaKeyword" runat="server" TextMode="MultiLine" Columns="60"
                                    MaxLength="1000" Rows="5" Width="600px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Meta Description:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtMetaDescription" runat="server" TextMode="MultiLine" Columns="60"
                                    Rows="5" Width="600px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
