﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using ResizeImage;

namespace IR_Admin
{
    public partial class CMSPages : Page
    {
        readonly private ManageCMSPage _master1 = new ManageCMSPage();
        readonly Masters _master = new Masters();
        List<RepeaterListFaqItem> list = new List<RepeaterListFaqItem>();
        static List<FileContent> fileList = new List<FileContent>();
        public string Tab = string.Empty;
        public string Imagepath = string.Empty;
        public string SiteUrl = "";
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            //Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";

            if (!Page.IsPostBack)
            {
                ViewState["NoOfItems"] = 5;
                _SiteID = Master.SiteID;
                BindSite();
                BindGrid(_SiteID);

                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    GetPagesForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;

            db_1TrackEntities _db = new db_1TrackEntities();
            SiteUrl = _db.tblSites.FirstOrDefault(x => x.ID == _SiteID).SiteURL;

            grdEurail.DataSource = _master1.GetPageCMSList(_SiteID);
            grdEurail.DataBind();
            Tab = "1";
        }
        
        public void BindSite()
        {
            ddlSite.DataSource = _master.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddCmsPage()
        {
            var pageId = Guid.NewGuid();
            try
            {
                UploadFile();
                var id = _master1.AddCMS(new tblPagesCM
                {
                    ID = pageId,
                    Name = txtPageName.Text.Trim(),
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.InnerHtml,
                    Image = Imagepath.Replace("~/", ""),
                    Keyword = txtKeyword.Text.Trim(),
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    SiteId = Guid.Parse(ddlSite.SelectedValue),
                    IsWidgetVisible = chkVisible.Checked,
                });

                _master1.AddPageMetaInfoCMS(new tblCMSPageMetaInfo
                {
                    ID = Guid.NewGuid(),
                    CMSPageID = pageId,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    Title = txtMetaTitle.Text,
                    Keywords = txtMetaKeyword.Text,
                    Description = txtMetaDescription.Text
                });

                if (fileList != null)
                {
                    foreach (var item in fileList)
                    {
                        _master1.AddPageGlry(new tblPagesCMSGallery
                        {
                            ID = new Guid(),
                            PageID = id,
                            GalleryImage = item.filePath.Replace("~/", "")
                        });
                    }
                    fileList = null;
                    ShowMessage(1, "You have successfully added the record.");
                }
                //else
                //    ShowMessage(2, "Already added record.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void UpdateCmsPage()
        {
            var pageId = Guid.Parse(Request["id"]);
            try
            {
                UploadFile();
                var id = _master1.UpdateCMS(new tblPagesCM
                {
                    ID = pageId,
                    Name = txtPageName.Text.Trim(),
                    Title = txtTitle.Text.Trim(),
                    Description = txtDesc.InnerHtml,
                    Image = Imagepath.Replace("~/", ""),
                    Keyword = txtKeyword.Text.Trim(),
                    ModifyBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now,
                    SiteId = Guid.Parse(ddlSite.SelectedValue),
                    IsWidgetVisible = chkVisible.Checked
                });

                _master1.UpdatePageMetaInfoCMS(new tblCMSPageMetaInfo
                {
                    ID = Guid.NewGuid(),
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    CMSPageID = pageId,
                    ModifyBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now,
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    Title = txtMetaTitle.Text,
                    Keywords = txtMetaKeyword.Text,
                    Description = txtMetaDescription.Text
                });

                if (fileList != null)
                {
                    foreach (var item in fileList)
                    {
                        _master1.AddPageGlry(new tblPagesCMSGallery
                        {
                            ID = new Guid(),
                            PageID = id,
                            GalleryImage = item.filePath.Replace("~/", "")
                        });
                    }
                    fileList = null;
                    ShowMessage(1, "You have successfully added the record.");
                }
                ShowMessage(1, "You have successfully added the record.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddCmsPage();
                else
                    UpdateCmsPage();
                BindGrid(_SiteID);
                txtPageName.Text = string.Empty;
                txtTitle.Text = string.Empty;
                txtDesc.InnerHtml = string.Empty;
                txtKeyword.Text = string.Empty;
                ddlSite.SelectedIndex = 0;
                txtMetaDescription.Text = txtMetaKeyword.Text = txtMetaTitle.Text = string.Empty;
                ViewState["NoOfItems"] = 5;
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CMSPages.aspx");
        }

        public void GetPagesForEdit(Guid id)
        {
            btnSubmit.Text = "Update";
            tblPagesCM oFaq = _master1.GetCMSById(id);
            if (oFaq != null)
            {
                ddlSite.SelectedValue = oFaq.SiteId.ToString();
                txtPageName.Text = oFaq.Name;
                txtTitle.Text = oFaq.Title;
                txtDesc.InnerHtml = oFaq.Description;
                txtKeyword.Text = oFaq.Keyword;
                hdnimage.Value = oFaq.Image;
                chkVisible.Checked = oFaq.IsWidgetVisible;

                var data = _master1.GetPageMetaInfoCMSByPageId(id, oFaq.SiteId.Value);
                if (data != null)
                {
                    txtMetaTitle.Text = data.Title;
                    txtMetaKeyword.Text = data.Keywords;
                    txtMetaDescription.Text = data.Description;
                }
            }

            BindGalleryOnEdit(id);
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdEurail_RowCommand(object sender, GridViewCommandEventArgs e)
        {


            if (e.CommandName == "Remove")
            {
                var pageId = Guid.Parse(e.CommandArgument.ToString());
                //--Deleted Images from Directory
                var list = _master1.GetGalleryListByPageID(pageId);
                RemoveReadOnlyFile();
                foreach (var item in list)
                {
                    string cImgpath = "~/" + item.GalleryImage.Trim();
                    if (File.Exists(Server.MapPath(cImgpath)))
                        File.Delete(Server.MapPath(cImgpath));
                }

                bool res = _master1.DeleteCMS(pageId);
                if (res)
                    ShowMessage(1, "Record deleted successfully.");

            }

            if (e.CommandName == "ActiveInActive")
            {
                var pageId = Guid.Parse(e.CommandArgument.ToString());
                bool res = _master1.ActiveInactive(pageId);
            }

            Tab = "1";
            BindGrid(new Guid());

        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupImage.HasFile)
                {
                    if (fupImage.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupImage.FileName.Substring(fupImage.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }
                    Imagepath = "~/Uploaded/CMSImage/";
                    Imagepath = Imagepath + CropImage(fupImage, Imagepath, 150, 236);
                }
                else
                    Imagepath = hdnimage.Value;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CropImage(FileUpload FileUpload1, string Location, float height, float width)
        {
            string strImage = FileUpload1.PostedFile.FileName;
            if (!string.IsNullOrEmpty(strImage))
            {
                try
                {
                    System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                    var fileExt = Path.GetExtension(strImage);
                    strImage = Guid.NewGuid() + "_" + Path.GetFileNameWithoutExtension(strImage) + fileExt;
                    var strFilePathTemp = HttpContext.Current.Server.MapPath(Location + strImage);
                    FileUpload1.PostedFile.SaveAs(strFilePathTemp);

                    var newHeight = height;
                    var newWidth = width;

                    var strNewImagename = Guid.NewGuid() + fileExt;
                    ResizeImage(strFilePathTemp, HttpContext.Current.Server.MapPath(Location) + strNewImagename, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
                    strImage = strNewImagename;

                    if (File.Exists(strFilePathTemp))
                        File.Delete(strFilePathTemp);
                }
                catch
                {
                    strImage = "";
                }
            }
            else
            {
                strImage = "";
            }
            return strImage;
        }

        public void ResizeImage(string sourceFile, string targetFile, int outputWidth, int outputHeight)
        {
            ImageResize.ResizeFix(sourceFile, targetFile, outputWidth, outputHeight);
        }

        public void AjaxFileUpload2_OnUploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            var ext = new string[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
            string fileName = e.FileName.Substring(e.FileName.LastIndexOf("."));

            if (!ext.Contains(fileName.ToUpper()))
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
            else
            {
                fileName = Guid.NewGuid().ToString() + fileName;
                CreateImageFolder();
                string path = "~/Uploaded/CmsBannerImgs/" + fileName;

                if (File.Exists(Server.MapPath(path)))
                    File.Delete(Server.MapPath(path));
                else
                    AjaxFileUpload2.SaveAs(Server.MapPath(path));
                if (fileList == null)
                    fileList = new List<FileContent>();

                fileList.Add(new FileContent
                {
                    filePath = path
                });
            }
        }

        void BindGalleryOnEdit(Guid id)
        {
            try
            {
                rptGallery.DataSource = _master1.GetGalleryListByPageID(id);
                rptGallery.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void rptGallery_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                string imgpath = _master1.DeleteGalleryImagesById(Guid.Parse(e.CommandArgument.ToString()));
                string cImgpath = "~/" + imgpath.Trim();
                if (File.Exists(Server.MapPath(cImgpath)))
                    File.Delete(Server.MapPath(cImgpath));
            }
            GetPagesForEdit(Guid.Parse(Request["id"]));
            Tab = "2";
        }

        private void CreateImageFolder()
        {
            if (!Directory.Exists(Server.MapPath("~/Uploaded/CmsBannerImgs")))
                Directory.CreateDirectory(Server.MapPath("~Uploaded/CmsBannerImgs"));
        }

        public void RemoveReadOnlyFile()
        {
            const string cntPath = "~/Uploaded/CmsBannerImgs/";
            foreach (string cntImg in Directory.GetFiles(Server.MapPath(cntPath), "*.*", SearchOption.AllDirectories))
            {
                new FileInfo(cntImg) { Attributes = FileAttributes.Normal };
            }
        }
        
        protected void grdEurail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdEurail.PageIndex = e.NewPageIndex;
            BindGrid(_SiteID);
        }

    }
}
public class FileContent
        {
            public string filePath { get; set; }
        }