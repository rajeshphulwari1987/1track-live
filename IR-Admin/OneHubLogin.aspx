﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="OneHubLogin.aspx.cs" Inherits="IR_Admin.OneHubLogin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #divTooltip
        {
            position: absolute;
            z-index: 4000;
            border: 1px solid #222;
            background-color: #EEE18D;
            padding: 6px;
            opacity: 0.85;
        }
    </style>
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        One Hub Login</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <!-- tab "panes" -->
    <div class="full mr-tp1">
        <div class="panes">
            <div id="divlist" runat="server">
                <div class="crushGvDiv" style="font-size: 13px;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <legend><b>Select Site </b></legend>
                                    <asp:DropDownList ID="ddlSite" runat="server" Width="500px" AutoPostBack="True" OnSelectedIndexChanged="ddlSite_SelectedIndexChanged" />
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><b>Production Account</b></legend>
                                    <asp:GridView ID="grvOneHubPrd" runat="server" AutoGenerateColumns="False" PageSize="10"
                                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                        OnRowCommand="grvOneHubPrd_RowCommand" >
                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                        <PagerStyle CssClass="paging"></PagerStyle>
                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                            BorderColor="#FFFFFF" BorderWidth="1px" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            Record not found.</EmptyDataTemplate>
                                        <Columns>
                                          <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                <a class="tooltips" href="#"> 
                                                  <img src="images/info.png" alt="" width="25px" style="float:left; cursor:pointer"> <span> <%#Eval("ToolTip")%>       </span></a>                                                  
                                                   &nbsp;   <%#Eval("TrainName")%>                                           
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Account Type">
                                                <ItemTemplate>
                                                  <%#Eval("AccountType")%>                                    
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")+","+Eval("TrainName")%>'
                                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="1" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to active/in-active this account?);" />
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><b>Test Account</b></legend>
                                    <asp:GridView ID="grdOneHubTest" runat="server" AutoGenerateColumns="False" PageSize="10"
                                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                        OnRowCommand="grdOneHubTest_RowCommand">
                                        <AlternatingRowStyle BackColor="#FBDEE6" />
                                        <PagerStyle CssClass="paging"></PagerStyle>
                                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                            BorderColor="#FFFFFF" BorderWidth="1px" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            Record not found.</EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                <a class="tooltips" href="#"> 
                                                  <img src="images/info.png" alt="" width="25px" style="float:left; cursor:pointer"> <span> <%#Eval("ToolTip")%>       </span></a>                                                  
                                                   &nbsp;   <%#Eval("TrainName")%>                                           
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Account Type">
                                                <ItemTemplate>
                                                  <%#Eval("AccountType")%>                                           
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")+","+Eval("TrainName")%>'
                                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="1" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to active/in-active this account?);" />
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
