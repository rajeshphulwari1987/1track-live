﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class PassValidUpTo : System.Web.UI.Page
    {
        readonly private Masters _oMasters = new Masters();
        public string tab = string.Empty;
        List<RepeaterListItem> list = new List<RepeaterListItem>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PageLoadEvent();
            }
        }

        private void PageLoadEvent()
        {
            try
            {
                tab = "1";
                if (!String.IsNullOrEmpty(hdnId.Value))
                    tab = "2";

                var listLang = _oMasters.GetLanguangesList();
                ddlLanguage.DataSource = listLang.Where(x => x.Name.Trim().ToUpper() == "ENGLISH"); ;
                ddlLanguage.DataTextField = "Name";
                ddlLanguage.DataValueField = "ID";
                ddlLanguage.DataBind();

                for (int i = 0; i < 100; i++)
                {
                    ddlAgeFrom.Items.Insert(i, new ListItem(i.ToString(), i.ToString()));
                    ddlAgeTo.Items.Insert(i, new ListItem(i.ToString(), i.ToString()));
                }
              
                if (listLang.Count > 5)
                    ViewState["NoOfItems"] = 5;
                else
                    ViewState["NoOfItems"] = listLang.Count - 1;
               
                FillGrid();
                BindRepeter(null);
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        public void BindRepeter(List<RepeaterListItem> listItm)
        {
            try
            {
                if (listItm == null)
                {
                    int numOfItem = Convert.ToInt32(ViewState["NoOfItems"]);
                    for (int i = 0; i < numOfItem; i++)
                    {
                        RepeaterListItem page = new RepeaterListItem { NoOfItems = i };
                        list.Add(page);
                    }
                    Session["rptProdist"] = list;
                    rptProductValidUpTo.DataSource = list;
                }
                else
                    rptProductValidUpTo.DataSource = listItm;
                rptProductValidUpTo.DataBind();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        protected List<RepeaterListItem> AddRepeaterItemInList()
        {
            foreach (RepeaterItem item in rptProductValidUpTo.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    DropDownList ddlLang = item.FindControl("ddlLanguage") as DropDownList;
                    TextBox txtCatName = item.FindControl("txtCatName") as TextBox;
                    RadioButton rdoMissing = item.FindControl("rdoMissing") as RadioButton;
                    RepeaterListItem page = new RepeaterListItem
                    {
                        NoOfItems = item.ItemIndex,
                        LanguageId = ddlLang.SelectedValue,
                        Name = txtCatName.Text,
                        IsMissing = rdoMissing.Checked
                    };
                    list.Add(page);
                }
            }
            return list;
        }

        protected void FillGrid()
        {
            var list = _oMasters.GetProductValidUpToNameList();
            if (list != null && list.Count > 0)
            { grvProductValidUpTo.DataSource = list; } grvProductValidUpTo.DataBind();
        }

        #region Repeater / Grid command
        protected void grvProductValidUpTo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProductValidUpTo.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        protected void grvProductValidUpTo_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            Guid id = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Modify")
            {
                var opvut = _oMasters.GetProductValidUpToById(id);
                hdnId.Value = id.ToString();
            }
            if (e.CommandName == "Remove")
            {
                bool result = _oMasters.DeleteClass(id);
                if (result)
                    ShowMessage(1, "Record deleted successfully");
                FillGrid();
                tab = "1";
            }
        }
        protected void rptProductValidUpTo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DropDownList ddlLang = e.Item.FindControl("ddlLanguage") as DropDownList;
                    HiddenField hdnLangId = e.Item.FindControl("hdnLangId") as HiddenField;

                    var list = _oMasters.GetLanguangesList();
                    if (list != null)
                    {
                        list.RemoveAll(x => x.Name.Trim().ToUpper() == "ENGLISH");
                        ddlLang.DataSource = list;
                    }
                    ddlLang.DataTextField = "Name";
                    ddlLang.DataValueField = "ID";
                    ddlLang.DataBind();

                    if (hdnLangId != null && !String.IsNullOrEmpty(hdnLangId.Value))
                        ddlLang.SelectedValue = hdnLangId.Value;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        #endregion

        protected void btnAddMore_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["NoOfItems"] = Convert.ToInt32(ViewState["NoOfItems"]) + 1;
                RepeaterListItem newpage = new RepeaterListItem { NoOfItems = Convert.ToInt32(ViewState["NoOfItems"]) };
                list.Add(newpage);
                list = AddRepeaterItemInList();
                Session["rptProdist"] = list;
                BindRepeter(list);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptProductValidUpTo.Items.Count > 0)
                {
                    list = AddRepeaterItemInList();

                    //--Add Removed Record in list at edit time
                    if (!String.IsNullOrEmpty(hdnId.Value))
                    {
                        var record = list.FirstOrDefault();
                        if (Session["reovedItemList"] == null)
                        {
                            List<RepeaterListItem> removeList = new List<RepeaterListItem> { record };
                            Session["reovedItemList"] = removeList;
                        }
                        else
                        {
                            List<RepeaterListItem> removeList = Session["reovedItemList"] as List<RepeaterListItem>;
                            if (removeList != null) removeList.Add(record);
                            Session["reovedItemList"] = removeList;
                        }
                    }
                    //---

                    list.RemoveAt(0);
                    Session["rptProdist"] = list;
                    BindRepeter(list);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}