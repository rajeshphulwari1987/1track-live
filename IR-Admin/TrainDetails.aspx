﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="TrainDetails.aspx.cs" Inherits="IR_Admin.TrainDetails" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <link href="Styles/slider.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/thumbnail-slider.js" type="text/javascript"></script>
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">   
         $(document).ready(function () {
            checkUncheckCntTree();
        });
         
        function pageLoad(sender, args) 
        {
            $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtTrainInfo').redactor({ iframe: true, minHeight: 100 });
        }
        
        $(function () {                  
            if(<%=tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {
                $("ul.tabs").tabs("div.inner-tabs-container > div");               
            }         
        }); 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }   
        function checkUncheckCntTree() {
            var checkBoxSelector = '#<%=dtlCountry.ClientID%> input[id*="chkRegion"]:checkbox';
            $(checkBoxSelector).click(function () {
                var chkId = (this.id);
                var last = chkId.slice(-1);
                var idtree = "MainContent_dtlCountry_trCntry_" + last;

                if (this.checked) {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', true);
                    });
                }
                else {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 5000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
    </script>
    <style type="text/css">
        img
        {
            vertical-align: middle;
            border: none;
        }
        .trainblockbanner img
        {
            height: 80px;
            width: 120px;
        }
        #mcts1 > *
        {
            display: block;
        }
        #mcts1 div.item
        {
            padding: 2px;
            margin-right: 20px;
            background-color: white;
            text-align: center;
            position: relative;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Train Details</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="TrainDetails.aspx" class="current">List</a></li>
            <li><a id="aNew" href="TrainDetails.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv1" style="border: 1px solid #ccc;">
                        <div class="train-detail-in">
                            <asp:DataList ID="dtlTrains" runat="server" RepeatColumns="2" Width="100%" RepeatDirection="Horizontal"
                                OnItemCommand="dtlTrains_ItemCommand">
                                <ItemTemplate>
                                    <div class="train-detail-block">
                                        <div class="trainblock">
                                            <img alt="map" src='<%#Eval("BannerImage").ToString()=="" ? "images/sml-noimg.jpg":Eval("BannerImage")%>' />
                                        </div>
                                        <div class="trainblockleft">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Name:
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hdnTrainId" runat="server" Value='<%#Eval("ID")%>' />
                                                        <%#Eval("Name")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Action:
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                    <table width="100%">
                        <tr class="paging">
                            <asp:Repeater ID="DLPageCountItem" runat="server">
                                <ItemTemplate>
                                    <td style="float: left">
                                        <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                            Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                    </td>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </table>
                </div>
                <asp:HiddenField ID="hdnId" runat="server" />
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;" width="70%">
                                <table>
                                    <tr>
                                        <td class="col">
                                            Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtTName" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtTName"
                                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="CForm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Navigation Url
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtUrl" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col" valign="top">
                                            Country
                                        </td>
                                        <td class="col">
                                            <div style="min-height: 20px; max-height: 325px; overflow-y: auto; width: 100%; vertical-align: top">
                                                <asp:DataList ID="dtlCountry" runat="server" RepeatColumns="2" Width="99%" OnItemDataBound="dtlCountry_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRegion" runat="server" Font-Bold="True" Text='<%#Eval("RegionName") %>' />
                                                        <asp:HiddenField runat="server" ID="hdnRegion" Value='<%#Eval("RegionID") %>' />
                                                        <asp:TreeView ID="trCntry" runat="server" ShowCheckBoxes="All">
                                                        </asp:TreeView>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col" valign="top">
                                            Train Information
                                        </td>
                                        <td class="col">
                                            <textarea id="txtTrainInfo" runat="server" width="200"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Train Image
                                        </td>
                                        <td class="col">
                                            <asp:FileUpload ID="fupCountryImg" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col">
                                            Is Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkactive" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col" valign="top">
                                            Description
                                        </td>
                                        <td class="col">
                                            <textarea id="txtContent" runat="server"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Gallery Image:
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="myThrobber_tq" Style="display: none;">
                                
                                 <img align="absmiddle" alt="" src='images/uploading.gif'/>
                                            </asp:Label>
                                            <asp:AjaxFileUpload ID="AjaxFileUpload2" runat="server" padding-bottom="4" ContextKeys="2"
                                                padding-left="2" padding-right="1" padding-top="4" ThrobberID="myThrobber_tq"
                                                OnUploadComplete="AjaxFileUpload2_OnUploadComplete" />
                                            <br />
                                            <div id="testuploaded" style="display: none; padding: 4px; border: gray 1px solid;">
                                                <h4>
                                                    Uploaded files:</h4>
                                                <hr />
                                                <div id="fileList">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="trainblockbanner" style="width: 690px; margin-top: 5px;">
                                                <div id="mcts1" style="width: 99%; height: 90px; padding: 10px 7px 10px 1px; border: 0px solid #ccc;">
                                                    <asp:Repeater ID="rptGallery" runat="server" OnItemCommand="rptGallery_ItemCommand">
                                                        <ItemTemplate>
                                                            <div class="clsMtImg">
                                                                <div style="float: right; height: 18px; background: #EEEEEE; width: 100%; text-align: right;">
                                                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/cross.png"
                                                                        OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                                                </div>
                                                                <img src='<%#Eval("GalleryImage") %>' alt="" border="0" class="clsImg" />
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center; height: 35px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <tr>
                                    <td colspan="2" style="text-align: center; height: 35px;">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" ValidationGroup="CForm"
                                            OnClick="btnSubmit_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
</asp:Content>
