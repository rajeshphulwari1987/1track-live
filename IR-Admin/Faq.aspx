﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Faq.aspx.cs" Inherits="IR_Admin.Faq" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <link href="Styles/FaqtblQAstyle.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc,.txtans').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
     <style type="text/css">
        /*new design css*/
        #MainContent_divNew table tbody tr:nth-child(5) td
        {
            background: #00aeef !important;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        FAQ Policy</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Faq.aspx" class="current">List</a></li>
            <li><a id="aNew" href="Faq.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdFAQ" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnRowCommand="grdFAQ_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <%#Eval("Name")%>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SiteName">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="30%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='Faq.aspx?id=<%#Eval("ID")%>'>
                                        <img alt="edit" src="images/edit.png" /></a>
                                    <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="Remove" ToolTip="Delete"  OnClientClick="return confirm('Are you sure you want to delete this item?');" 
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;" class="grid-sec2">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                <b>Site:</b>
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" ClientIDMode="Static" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Title:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtTitle" runat="server" Width="500px" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtTitle" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                <b>Keyword:</b>
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="500px" TextMode="MultiLine" Height="100" />
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="txtKeyword" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <legend><b>Description</b></legend>
                                <div class="cat-outer-cms">
                                    <textarea id="txtDesc" runat="server"></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 1px; background: red">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="cat-outer" style="width: 99%; margin: 0%; min-height: 20px;">
                                    <asp:Repeater ID="rptProduct" runat="server">
                                        <ItemTemplate>
                                            <div class="cat-inner tblFaqQA">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 200px;">
                                                            <b>Question:</b>
                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("Id")%>' />
                                                        </td>
                                                         <td>
                                                            <asp:TextBox runat="server" ID="txtQuestion" MaxLength="200" Width="98%" Text='<%#Eval("Question")%>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                       <td style="width: 200px;">
                                                            <b>Answer:</b>
                                                        </td>
                                                        <td>
                                                         <textarea class="txtans" id="txtAnswer" runat="server"><%#Eval("Answer")%></textarea>
                                                           <%-- <asp:TextBox runat="server" ID="txtAnswer" TextMode="MultiLine" Height="100" Width="98%"
                                                                Text='<%#Eval("Answer")%>' />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <div class="cat-inner-alt tblFaqQA">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 200px;">
                                                            <b>Question:</b>
                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("Id")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtQuestion" MaxLength="200" Width="98%" Text='<%#Eval("Question")%>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px;">
                                                            <b>Answer:</b>
                                                        </td>
                                                        <td>
                                                         <textarea class="txtans" id="txtAnswer" runat="server"><%#Eval("Answer")%></textarea>
                                                            <%--<asp:TextBox runat="server" ID="txtAnswer" TextMode="MultiLine" Height="100" Width="98%"
                                                                Text='<%#Eval("Answer")%>' />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </AlternatingItemTemplate>
                                    </asp:Repeater>
                                    <div style="float: right;">
                                        <asp:Button ID="btnAddMore" runat="server" CssClass="button" Text="+ Add More" Width="89px"
                                            OnClick="btnAddMore_Click" />
                                        <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="-- Remove" Width="89px" OnClick="btnRemove_Click" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
