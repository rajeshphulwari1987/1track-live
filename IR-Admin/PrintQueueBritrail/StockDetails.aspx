﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="StockDetails.aspx.cs" Inherits="IR_Admin.BritRail.PrintQueueBritrail.StockDetails" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.7.2.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CallParent(id) {
            window.location.href = 'StockDetails.aspx?id=' + id;
        }
        var F, T;
        function FillValue(value1, value2) {
            F = value1;
            T = value2;
            try {
                $('#txtStockFrom').val(value1);
                $('#txtStockTo').val(value2);
                $("#txtStockFrom").blur(function () {
                    var FromVal = $("#txtStockFrom").val();
                    if (parseInt(FromVal) < parseInt(F)) {
                        $('#lblMessage').text('Invalid stock range, stock number less From: ' + value1);
                        $('#txtStockFrom').val(value1);
                    }
                    else
                        $('#lblMessage').val('');
                });
                $("#txtStockTo").blur(function () {
                    var ToVal = $("#txtStockTo").val();
                    if (parseInt(ToVal) > parseInt(T)) {
                        $('#lblMessage').text('Invalid stock range, stock number larger from: ' + value2);
                        $('#txtStockTo').val(value2);
                    }
                    else
                        $('#lblMessage').val('');
                });
            }
            catch (e) {
                alert(e);
            }
            return false;
        }

        function FillValueUnused(value1, value2) {
            F = value1;
            T = value2;
            try {
                $('#txtStockFromUnused').val(value1);
                $('#txtStockToUnused').val(value2);
                $("#txtStockFromUnused").blur(function () {
                    var FromVal = $("#txtStockFromUnused").val();
                    if (parseInt(FromVal) < parseInt(F)) {
                        $('#lblMessageUnused').text('Invalid stock range, stock number less From: ' + value1);
                        $('#txtStockFromUnused').val(value1);
                    }
                    else
                        $('#lblMessageUnused').val('');
                });

                $("#txtStockToUnused").blur(function () {
                    var ToVal = $("#txtStockToUnused").val();
                    if (parseInt(ToVal) > parseInt(T)) {
                        $('#lblMessageUnused').text('Invalid stock range, stock number larger from: ' + value2);
                        $('#txtStockToUnused').val(value2);
                    }
                    else
                        $('#lblMessageUnused').val('');
                });
            }
            catch (e) {
                alert(e);
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnBranchId" runat="server" />
        BritRail Stock Detail's</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <div class="panes">
            <table runat="server" id="tblDetail" width="100%">
                <tr>
                    <td>
                        <div class="cat-inner" style="margin: 0px 0 10px 0; width: 100%; border-radius: 5px;">
                            <table class="table-stock" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" class="heading-color">
                                        <b>Root: </b>
                                        <asp:Literal ID="ltrOfficeTree" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Allocated From: </b>
                                        <asp:Literal ID="ltrAllocatedFrom" runat="server" />
                                    </td>
                                    <td>
                                        <b>Stock Number From: </b>
                                        <asp:Literal ID="ltrStockNoFrom" runat="server" />
                                    </td>
                                    <td>
                                        <b>Stock Number To: </b>
                                        <asp:Literal ID="ltrStockNoTo" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Create Date: </b>
                                        <asp:Literal ID="ltrCreatedOn" runat="server" />
                                    </td>
                                    <td>
                                        <b>Created By: </b>
                                        <asp:Literal ID="ltrCreatedBy" runat="server" />
                                    </td>
                                    <td>
                                        <b>IP Address: </b>
                                        <asp:Literal ID="ltrIP" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <fieldset class="grid-sec2" style="width: 97%">
                <legend><strong>Stock Details</strong></legend>
                <asp:GridView ID="grdStock" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnRowDataBound="grdStock_RowDataBound">
                    <AlternatingRowStyle BackColor="#FBDEE6" />
                    <PagerStyle CssClass="paging"></PagerStyle>
                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                        BorderColor="#FFFFFF" BorderWidth="1px" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <EmptyDataRowStyle HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        Record not found.</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Stock From">
                            <ItemTemplate>
                                <asp:Literal ID="ltrFrom" runat="server" Text='<%#Eval("StockFrom")%>' />
                            </ItemTemplate>
                            <ItemStyle Width="10%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stock To">
                            <ItemTemplate>
                                <asp:Literal ID="ltrTo" runat="server" Text='<%#Eval("StockTo")%>' />
                            </ItemTemplate>
                            <ItemStyle Width="10%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Allocated To">
                            <ItemTemplate>
                                <a class="aStock" href='StockDetails.aspx?id=<%#Eval("BranchID")%>'>
                                    <%#Eval("AllocatedTo")%></a>
                            </ItemTemplate>
                            <ItemStyle Width="23%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Allocated From">
                            <ItemTemplate>
                                <a class="aStock" href='<%#Eval("ParentBranchID").ToString()=="00000000-0000-0000-0000-000000000000"?"StockAllocation.aspx":"StockDetails.aspx?id="+Eval("ParentBranchID")%>'>
                                    <%#Eval("Allocated_From")%>
                                </a>
                            </ItemTemplate>
                            <ItemStyle Width="23%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <%#Eval("Status")%>
                            </ItemTemplate>
                            <ItemStyle Width="15%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Panel runat="server" ID="pnl" Visible='<%#Eval("StatusID").ToString()=="bd82c702-8e84-4c9d-a666-99a3e1d5ee10"? true:false %>'>
                                    <asp:LinkButton ID="lnkDelivered" runat="server" CommandArgument='<%#Eval("StockFrom")+","+Eval("StockTo") %>'
                                        OnClick="lnkDelivered_Click">Set stock as delivered</asp:LinkButton>
                                </asp:Panel>
                                <asp:ModalPopupExtender ID="mdpupStock" runat="server" TargetControlID="lnkNew" PopupControlID="pnlStockAdd"
                                    BackgroundCssClass="modalBackground" />
                                <asp:LinkButton ID="lnkNew" runat="server" Text="Make Void Stock" CssClass="lnkNew"
                                    Visible='<%#Eval("StatusID").ToString().ToLower()=="0e758c24-d919-43d6-a80a-e60d9da8c53a"? true:false %>' />
                                <asp:ModalPopupExtender ID="mdpupStockUnused" runat="server" TargetControlID="lnkUnused"
                                    PopupControlID="pnlStockAddUnused" BackgroundCssClass="modalBackground" />
                                <asp:LinkButton ID="lnkUnused" runat="server" Text="Make Unused Stock" CssClass="lnkNew"
                                    Visible='<%#Eval("StatusID").ToString().ToLower()=="82de1dbb-05d2-4ac2-be59-569ddc9be292"? true:false %>' />
                            </ItemTemplate>
                            <ItemStyle Width="19%"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="grdAllStocks" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
                    <AlternatingRowStyle BackColor="#FBDEE6" />
                    <PagerStyle CssClass="paging"></PagerStyle>
                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                        BorderColor="#FFFFFF" BorderWidth="1px" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <EmptyDataRowStyle HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        Record not found.</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Stock Number">
                            <ItemTemplate>
                                <%#Eval("StockNo")%>
                            </ItemTemplate>
                            <ItemStyle Width="19%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Print Date">
                            <ItemTemplate>
                                <%#Eval("PrintDate")%>
                            </ItemTemplate>
                            <ItemStyle Width="27%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Printed By">
                            <ItemTemplate>
                                <%#Eval("PrintedBy")%>
                            </ItemTemplate>
                            <ItemStyle Width="27%"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <%#Eval("Status")%>
                            </ItemTemplate>
                            <ItemStyle Width="27%"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </fieldset>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnstockrange" />
    <div id="pnlStockAdd" style="width: 402px; display: none;">
        <div class="heading">
            Make Void Stock
        </div>
        <div class="divMain" style="width: 370px; border-radius: 0px">
            <div class="divleft" style="width: 370px;">
                From:
                <asp:TextBox runat="server" ID="txtStockFrom" MaxLength="18" ClientIDMode="Static" />
                <asp:RequiredFieldValidator ID="reqStockFrom" runat="server" ErrorMessage="*" CssClass="valdreq"
                    ControlToValidate="txtStockFrom" ValidationGroup="submit" />
                <asp:FilteredTextBoxExtender ID="ftbStockFrom" TargetControlID="txtStockFrom" ValidChars="0123456789"
                    runat="server" />
            </div>
            <div class="divleft" style="width: 370px;">
                To:&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtStockTo" MaxLength="18" ClientIDMode="Static" />
                <asp:RequiredFieldValidator ID="reqStockTo" runat="server" ErrorMessage="*" CssClass="valdreq"
                    ControlToValidate="txtStockTo" ValidationGroup="submit" />
                <asp:FilteredTextBoxExtender ID="ftbStockTo" TargetControlID="txtStockTo" ValidChars="0123456789"
                    runat="server" />
            </div>
            <div class="divleft" style="width: 100%; line-height: 10px; border: 0px; font-size: 12px;
                color: Red;">
                <asp:CompareValidator runat="server" ID="cmpNumbers" ControlToValidate="txtStockFrom"
                    ControlToCompare="txtStockTo" Operator="LessThan" Type="Integer" ErrorMessage="The Stock Number From will be larger than Stock Number To."
                    ValidationGroup="submit" />
                <br />
                <br />
                <asp:Label ID="lblMessage" runat="server" ClientIDMode="Static" />
            </div>
            <div class="divrightbtn" style="padding-top: 10px;">
                <asp:Button ID="btnSubmit" runat="server" CssClass="button1" OnClick="btnSubmit_OnClick"
                    Text="Submit" ValidationGroup="submit" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button1" Text="Cancel" />
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <div id="pnlStockAddUnused" style="width: 402px; display: none;">
        <div class="heading" style="width: 392px;">
            Make Unused Stock
        </div>
        <div class="divMain" style="width: 370px; border-radius: 0px;">
            <div class="divleft" style="width: 370px;">
                From:
                <asp:TextBox runat="server" ID="txtStockFromUnused" MaxLength="18" ClientIDMode="Static" />
                <asp:RequiredFieldValidator ID="reqf" runat="server" ErrorMessage="*" CssClass="valdreq"
                    ControlToValidate="txtStockFromUnused" ValidationGroup="submitUnused" />
                <asp:FilteredTextBoxExtender ID="ftexfrom" TargetControlID="txtStockFromUnused" ValidChars="0123456789"
                    runat="server" />
            </div>
            <div class="divleft" style="width: 370px;">
                To:&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtStockToUnused" MaxLength="18" ClientIDMode="Static" />
                <asp:RequiredFieldValidator ID="reqt" runat="server" ErrorMessage="*" CssClass="valdreq"
                    ControlToValidate="txtStockToUnused" ValidationGroup="submitUnused" />
                <asp:FilteredTextBoxExtender ID="ftexto" TargetControlID="txtStockToUnused" ValidChars="0123456789"
                    runat="server" />
            </div>
            <div class="divleft" style="width: 100%; line-height: 10px; border: 0px; font-size: 12px;
                color: Red;">
                <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtStockFromUnused"
                    ControlToCompare="txtStockToUnused" Operator="LessThan" Type="Integer" ErrorMessage="The Stock Number From will be larger than Stock Number To."
                    ValidationGroup="submitUnused" />
                <br />
                <br />
                <asp:Label ID="lblMessageUnused" runat="server" ClientIDMode="Static" />
            </div>
            <div class="divrightbtn" style="padding-top: 10px;">
                <asp:Button ID="btnSubmitUnused" runat="server" CssClass="button1" OnClick="btnSubmitUnused_OnClick"
                    Text="Submit" ValidationGroup="submitUnused" />
                <asp:Button ID="CancelUnused" runat="server" CssClass="button1" Text="Cancel" />
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
