﻿using System;
using Business;

public partial class TrainSegmentSearchResult : System.Web.UI.Page
{
    public string siteURL;
    private Guid _siteId;
    BookingRequestUserControl objBRUC;
    readonly Masters _master = new Masters();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
        if (!IsPostBack)
        {
            FillPageInfo();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _siteId = Guid.Parse(Session["siteId"].ToString());
        siteURL = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
    }
    public void FillPageInfo()
    {
        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            hdnPassenger.Value = objBRUC.Adults.ToString() + " x Adults," + objBRUC.Boys.ToString() + " x Children," + objBRUC.Seniors.ToString() + " x Seniors," +
                objBRUC.Youths.ToString() + " x Youths";

            lblSDetail.Text = "<div class='hd'> <span> >> Your Detail </span> <i>&nbsp;</i></div><div class='booking-status'>" +
                "<p>" + objBRUC.FromDetail + " to " + objBRUC.ToDetail + "<br>Departs " + objBRUC.depdt.ToString("dd/MMM/yyyy") + "<br>" +
                objBRUC.Adults.ToString() + " x Adults <br>" +
                objBRUC.Seniors.ToString() + " x Seniors <br>" +
                objBRUC.Boys.ToString() + " x Children <br>" +
                objBRUC.Youths.ToString() + " x Youths <br>" +
                "Please select train</p></div>";
        }

    }
}