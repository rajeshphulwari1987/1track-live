﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using IR_Admin.OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.Web.Services;
using System.Configuration;

namespace IR_Admin
{
    public partial class P2PBookingCart : Page
    {
        #region Global Variables
        readonly Masters _masterPage = new Masters();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        private double _total = 0;
        readonly ManageUser _ManageUser = new ManageUser();
        private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
        ManageTrainDetails _master = new ManageTrainDetails();
        ManageBooking _masterBooking = new ManageBooking();
        readonly private ManageOrder _masterOrder = new ManageOrder();
        public static string currency = "$";
        public static Guid currencyID = new Guid();
        private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        Guid siteId;
        private string htmfile = string.Empty;
        public string script = "";
        string curID;
        public string siteURL;
        long P2pOrderId = 0;
        Guid P2PAgentID = Guid.Empty;
        private string HtmlFileSitelog = string.Empty;
        public string SiteHeaderColor = string.Empty;
        public bool isEvolviBooking = false;
        public bool isNTVBooking = false;
        public bool isBene = false;
        public bool isTI = false;
        public string EvOtherCharges = "0";
        public string logo = "";
        public string EvolviTandC = "";
        public string billingEmailAddress;
        public string shippingEmailAddress;
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            siteId = Guid.Parse(selectedValue);
            PageLoadEvent();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["P2POrderID"] != null)
                P2pOrderId = Convert.ToInt64(Session["P2POrderID"]);
            if (Session["Adminp2pBookingAgent"] != null)
                P2PAgentID = Guid.Parse(Session["Adminp2pBookingAgent"].ToString());
            siteId = Master.SiteID;
            //Session["siteId"] = siteId;
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "restirctCopy", "restrictCopy();", true);
            txtZip.Attributes.Add("onkeypress", "return keycheck()");
            ShowMessage(0, string.Empty);
            if (!IsPostBack)
            {
                if (P2pOrderId != 0 && P2PAgentID != Guid.Empty)
                {
                    var p2porderAgentid = P2PAgentID;
                    var Agentlist = _ManageUser.AgentDetailsById(p2porderAgentid);
                    var AgentNameAndEmail = _ManageUser.AgentNameEmailById(p2porderAgentid);
                    if (Agentlist != null && AgentNameAndEmail != null)
                    {
                        txtFirst.Text = AgentNameAndEmail.Forename;
                        txtLast.Text = AgentNameAndEmail.Surname;
                        txtEmail.Text = AgentNameAndEmail.EmailAddress;
                        txtConfirmEmail.Text = AgentNameAndEmail.EmailAddress;
                        ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                        txtCity.Text = Agentlist.Town;
                        txtAdd.Text = Agentlist.Address1;
                        txtAdd2.Text = Agentlist.Address2;
                        txtZip.Text = Agentlist.Postcode;

                        txtshpfname.Text = Agentlist.FirstName;
                        txtshpLast.Text = Agentlist.LastName;
                        txtshpEmail.Text = Agentlist.Email;
                        txtshpConfirmEmail.Text = Agentlist.Email;
                        ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                        txtCity.Text = Agentlist.Town;
                        txtAdd.Text = Agentlist.Address1;
                        txtAdd2.Text = Agentlist.Address2;
                        txtZip.Text = Agentlist.Postcode;
                    }
                }
                Session["BOOKING-REPLY"] = null;
                PageLoadEvent();
            }
            GetCurrencyCode();
        }

        void PageLoadEvent()
        {
            ddlshpCountry.DataSource = ddlCountry.DataSource = _master.GetCountryDetail();
            ddlshpCountry.DataValueField = ddlCountry.DataValueField = "CountryID";
            ddlshpCountry.DataTextField = ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            ddlshpCountry.DataBind();
            ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            AddItemInShoppingCart();
            GetCurrencyCode();
            if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
            {
                var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
                string countryID = cookie.Values["_cuntryId"];
                ddlCountry.SelectedValue = countryID;
                ddlshpCountry.SelectedValue = countryID;
            }
            FillShippingData();
            PrepouplateSeniorAndAdult();
        }

        public void FillShippingData()
        {
            if (ddlCountry.SelectedValue != "0")
            {
                var countryid = Guid.Parse(ddlCountry.SelectedValue);
                var lstShip = _masterBooking.getAllShippingDetailAdmin(siteId, countryid);
                if (lstShip.Any())
                    rptShippings.DataSource = lstShip;
                else
                {
                    var lstDefaultShip = _masterBooking.getDefaultShippingDetailAdmin(siteId);
                    if (lstDefaultShip.Any())
                        rptShippings.DataSource = lstDefaultShip;
                    else
                        rptShippings.DataSource = null;
                }
                rptShippings.DataBind();
            }
        }

        void PrepouplateSeniorAndAdult()
        {
            if (Session["AgentUserID"] == null && Session["RailPassData"] != null)
            {
                var list = Session["RailPassData"] as List<getRailPassData>;
                var listr = list.OrderBy(x => x.TravellerName);
                foreach (var rec in listr)
                {
                    if (rec.TravellerName.Contains("Adult") || rec.TravellerName.Contains("Senior "))
                    {
                        ddlMr.SelectedValue = rec.Title;
                        txtFirst.Text = rec.FirstName;
                        txtLast.Text = rec.LastName;
                        break;
                    }
                }
            }
        }

        public void AddItemInShoppingCart()
        {
            try
            {
                rptTrainTcv.DataSource = null;
                rptTrainTcv.DataBind();
                var lstPassDetail = _masterBooking.GetAllPassSale(P2pOrderId, "", getsitetickprotection()).ToList(); ;
                if (lstPassDetail.Count > 0)
                {
                    rptTrain.DataSource = lstPassDetail.OrderBy(x => x.OrderIdentity);
                    rptTrain.DataBind();
                }
                else
                {
                    rptTrain.DataSource = null;
                    rptTrain.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected BillingAddress GetBillingInfo()
        {
            return new BillingAddress
            {
                Address = txtAdd.Text,
                City = txtCity.Text,
                Country = ddlCountry.SelectedItem.Text,
                Email = txtEmail.Text.Trim(),
                FirstName = txtFirst.Text.Trim(),
                Lastname = txtLast.Text.Trim(),
                ZipCode = txtZip.Text.Trim(),
                State = txtState.Text.Trim()
            };
        }

        protected void DeleteTicketInfo(string id)
        {
            try
            {
                var cartList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;

                if (cartList.Count > 0 && bookingList.Count > 0)
                {
                    cartList.RemoveAll(x => x.Id.ToString() == id);
                    bookingList.RemoveAll(x => x.Id.ToString() == id);
                    Session["SHOPPINGCART"] = cartList;
                    Session["BOOKING-REQUEST"] = bookingList;
                    ShowMessage(1, "You have successfully deleted ticket from list view");
                }
                else
                {
                    Session["SHOPPINGCART"] = null;
                    Session["BOOKING-REQUEST"] = null;
                }
                AddItemInShoppingCart();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var chk = e.Item.FindControl("chkTicketProtection") as CheckBox;
                var lblPrice = e.Item.FindControl("lblPrice") as Label;
                var lbltpPrice = e.Item.FindControl("lbltpPrice") as Label;
                var currsyb = e.Item.FindControl("currsyb") as Label;
                var lblTckProc = e.Item.FindControl("lblTckProc") as Label;
                var divTckProt = e.Item.FindControl("divTckProt") as HtmlGenericControl;
                var istckProt = _masterPage.IsTicketProtection(siteId);
                if (divTckProt != null)
                    divTckProt.Visible = istckProt;

                if (lblTckProc != null)
                    lblTckProc.Visible = istckProt;
                GetCurrencyCode();
                if (chk.Checked)
                {
                    lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                    _total = _total + Convert.ToDouble(lblPrice.Text);
                    lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                    currsyb.Text = oManageClass.GetCurrency(Guid.Parse(curID));
                    currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                }
                else
                {
                    _total = _total + Convert.ToDouble(lblPrice.Text);
                    lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                    currsyb.Text = oManageClass.GetCurrency(Guid.Parse(curID));
                    currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                var lblTotal = e.Item.FindControl("lblTotal") as Label;
                var txtBookingFee = e.Item.FindControl("txtBookingFee") as TextBox;
                double totalAmount;
                string SiteId = Session["siteId"].ToString();
                var lst = _masterBooking.GetBooingFees(Convert.ToDecimal(_total), Guid.Parse(SiteId));
                if (lst.Count() > 0 && (SiteId == "a5f8e257-d4e1-48e9-bfa4-59a582ebe56f" || SiteId == "302d426f-6c40-4b71-a565-5d89d91c037e"))
                {
                    decimal amountAfterConv = Convert.ToDecimal(lst.FirstOrDefault().BookingFee) * Convert.ToDecimal(lst.FirstOrDefault().convRate);
                    txtBookingFee.Text = amountAfterConv.ToString("F2");
                    hdnBookingFee.Value = amountAfterConv.ToString("F2");
                }
                else
                {
                    txtBookingFee.Text = "0.00";
                    hdnBookingFee.Value = "0.00";
                }
                totalAmount = _total + Convert.ToDouble(hdnBookingFee.Value.Trim());
                lblTotal.Text = totalAmount.ToString("F2");
                lblTotalCost.Text = totalAmount.ToString("F2");
                /*update CommissionFeeAmount*/
                //_masterBooking.P2PGetCommissionFee(P2pOrderId);
            }
        }

        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
            var chk = (CheckBox)sender;
            var lbltpPrice = chk.Parent.FindControl("lbltpPrice") as Label;
            var lblPrice = chk.Parent.FindControl("lblPrice") as Label;
            var lblPassSaleID = chk.Parent.FindControl("lblPassSaleID") as Label;
            var lblHidPriceValue = chk.Parent.FindControl("lblHidPriceValue") as Label;
            var currsyb = chk.Parent.FindControl("currsyb") as Label;
            var passsaleid = new Guid();
            if (!string.IsNullOrEmpty(lblPassSaleID.Text))
                passsaleid = Guid.Parse(lblPassSaleID.Text);

            if (chk.Checked)
            {
                _masterBooking.SetTicketProtectionAmt(passsaleid, Convert.ToDecimal(lbltpPrice.Text));
                lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            }
            else
            {
                _masterBooking.SetTicketProtectionAmt(passsaleid, 0);
                lblPrice.Text = lblHidPriceValue.Text;
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
            }
            ScriptManager.RegisterStartupScript(Page, GetType(), "temp", "getdata()", true);
        }

        protected void rptTrainTcv_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    string id = e.CommandArgument.ToString();
                    if (e.CommandName == "Remove")
                        DeleteTicketInfo(id);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void rptTrainTcv_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var lblp = e.Item.FindControl("lblPrice") as Label;
                if (lblp != null)
                {
                    string prc = lblp.Text.Replace("$", "");
                    _total = _total + Convert.ToDouble(prc);
                }
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                var lblTotal = e.Item.FindControl("lblTotal") as Label;
                if (lblTotal != null) lblTotal.Text = "Total:" + currency + _total.ToString("F2");
            }
        }

        public decimal getsitetickprotection()
        {
            try
            {
                var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
                if (list != null)
                {
                    divpopupdata.InnerHtml = list.Description;
                    return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
                }
                else return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetCurrencyCode()
        {
            var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (currencyID != null)
            {
                curID = currencyID.DefaultCurrencyID.ToString();
                currency = oManageClass.GetCurrency(Guid.Parse(curID));
                hdnCurrencySign.Value = currency;
            }
        }

        protected void chkShippingfill_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShippingfill.Checked)
            {
                pnlbindshippping.Visible = true;
                ddlshpMr.SelectedValue = ddlMr.SelectedValue;
                txtshpfname.Text = txtFirst.Text;
                txtshpLast.Text = txtLast.Text;
                txtshpEmail.Text = txtEmail.Text;
                txtshpConfirmEmail.Text = txtConfirmEmail.Text;
                txtShpPhone.Text = txtBillPhone.Text;
                txtshpAdd.Text = txtAdd.Text;
                txtshpAdd2.Text = txtAdd2.Text;
                txtshpCity.Text = txtCity.Text;
                txtshpState.Text = txtState.Text;
                txtshpZip.Text = txtZip.Text;
                ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
            }
            else
            {
                pnlbindshippping.Visible = false;
            }
        }

        [WebMethod]
        public static bool chk_Discount(string DiscountCode)
        {
            int P2pOrderId = 0;
            if (HttpContext.Current.Session["P2POrderID"] != null)
                P2pOrderId = Convert.ToInt32(HttpContext.Current.Session["P2POrderID"]);
            ManageBooking _masterBookingd = new ManageBooking();
            Guid SiteId = Guid.Empty;
            if (HttpContext.Current.Session["siteId"] != null)
                SiteId = Guid.Parse(HttpContext.Current.Session["siteId"].ToString());
            bool result = _masterBookingd.ValidDiscountCode(DiscountCode, SiteId, P2pOrderId);
            return result;
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                Guid ss = siteId;
                #region Save Billing and Shipping Address
                Session["ShipMethod"] = hdnShipMethod.Value.Trim();
                Session["ShipDesc"] = hdnShipDesc.Value.Trim();
                /*Add discount*/
                if (txtDiscountCode.Text.Length > 0)
                    _masterBooking.AddOrderDiscountData(txtDiscountCode.Text, siteId, P2pOrderId, Convert.ToDecimal(lblTotalCost.Text));

                AddP2PBookingInLocalDB();
                Response.Redirect("P2PPayment.aspx?id=" + P2pOrderId);
                #endregion
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void AddP2PBookingInLocalDB()
        {
            bool isReg = false;
            if (Session["IsRegional"] != null)
                isReg = Convert.ToBoolean(Session["IsRegional"]);

            foreach (RepeaterItem it in rptTrain.Items)
            {
                var chkTicketProtection = it.FindControl("chkTicketProtection") as CheckBox;
                var lbltpPrice = it.FindControl("lbltpPrice") as Label;
                var lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
                if (chkTicketProtection != null && chkTicketProtection.Checked)
                {
                    if (lbltpPrice != null)
                        if (lblPassSaleID != null)
                            _masterBooking.UpdateTicketProtection(Guid.Parse(lblPassSaleID.Text.Trim()), Convert.ToDecimal(lbltpPrice.Text.Trim()));
                }
            }
            foreach (RepeaterItem it in rptTrain.Items)
            {
                var lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
                var objBooking = _masterBooking;
                if (Session["BOOKING-REPLY"] != null)
                {
                    var oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                    if (oldList != null)
                        foreach (var item in oldList)
                        {
                            if (lblPassSaleID != null)
                                objBooking.UpdateReservationCodebyP2PID(Guid.Parse(lblPassSaleID.Text.Trim()), P2pOrderId, item.ReservationCode, item.PinCode, "Manual Booking", false, item.DepStationName, isReg);
                        }
                }
            }

            var objBillingAddress = new tblOrderBillingAddress
                {
                    ID = Guid.NewGuid(),
                    OrderID = P2pOrderId,
                    Title = ddlMr.SelectedItem.Text,
                    FirstName = txtFirst.Text,
                    LastName = txtLast.Text,
                    Phone = txtBillPhone.Text,
                    Address1 = txtAdd.Text,
                    Address2 = txtAdd2.Text,
                    EmailAddress = txtEmail.Text,
                    City = txtCity.Text,
                    State = txtState.Text,
                    Country = ddlCountry.SelectedItem.Text,
                    Postcode = txtZip.Text,
                    IsVisibleEmailAddress = chkVisibleEmailAddress.Checked
                };

            if (!chkShippingfill.Checked)
            {
                ddlshpMr.SelectedValue = ddlMr.SelectedValue;
                txtshpfname.Text = txtFirst.Text;
                txtshpLast.Text = txtLast.Text;
                txtshpEmail.Text = txtEmail.Text;
                txtshpConfirmEmail.Text = txtConfirmEmail.Text;
                txtShpPhone.Text = txtBillPhone.Text;
                txtshpAdd.Text = txtAdd.Text;
                txtshpAdd2.Text = txtAdd2.Text;
                txtshpCity.Text = txtCity.Text;
                txtshpState.Text = txtState.Text;
                txtshpZip.Text = txtZip.Text;
                ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
            }

            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;

            if (txtDateOfDepature.Text.Trim() != "DD/MM/YYYY")
                _masterBooking.UpdateDepatureDate(P2pOrderId, Convert.ToDateTime(txtDateOfDepature.Text));

            _masterBooking.AddOrderBillingAddress(objBillingAddress);
            if (Request.QueryString["req"] != null)
            {
                if (Request.QueryString["req"].Trim() == "IT")
                    hdnShippingCost.Value = "0";
            }
            _masterBooking.UpdateOrderData(Convert.ToDecimal(hdnShippingCost.Value), hdnShipMethod.Value, hdnShipDesc.Value, "", P2pOrderId);
            _masterBooking.UpdateOrderBookingFee(Convert.ToDecimal(hdnBookingFee.Value.Trim()), P2pOrderId);
            _masterBooking.UpdatetblP22BookingFee(Convert.ToDecimal(hdnBookingFee.Value.Trim()), P2pOrderId);
            SendMail();
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillShippingData();
        }

        public bool SendMail()
        {
            bool retVal = false;
            try
            {
                siteId = Master.SiteID;
                int orderID = Convert.ToInt32(P2pOrderId);
                GetCurrencyCode();
                bool isDiscountSite = _masterOrder.GetOrderDiscountVisibleBySiteId(siteId);
                var smtpClient = new SmtpClient();
                var message = new MailMessage();
                var st = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (st != null)
                {
                    string SiteName = st.SiteURL + "Home";
                    string Subject = "Order Confirmation #" + orderID;

                    ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");
                    HtmlFileSitelog = string.IsNullOrEmpty(objMailTemp.GetMailTemplateBySiteId(siteId)) ? "IRMailTemplate.htm" : Server.MapPath(objMailTemp.GetMailTemplateBySiteId(siteId));

                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(htmfile);
                    var list = xmlDoc.SelectNodes("html");
                    string body = list[0].InnerXml.ToString();

                    string UserName, EmailAddress, DeliveryAddress, BillingAddress, OrderDate, OrderNumber, Total, ShippingAmount, GrandTotal, NetTotal, Discount, AdminFee;
                    decimal BookingFee = 0; UserName = Discount = NetTotal = EmailAddress = DeliveryAddress = BillingAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = AdminFee = "";
                    string OtherCharges = string.Empty;
                    string BookingRefNO = string.Empty;
                    string PassProtection = "0.00";
                    string PassProtectionHtml = "";
                    string DiscountHtml = "";
                    bool isBeneDeliveryByMail = false;
                    bool isAgentSite = false;

                    var lst = new ManageBooking().GetAllCartData(P2pOrderId).OrderBy(a => a.OrderIdentity).ToList();
                    var lst1 = from a in lst
                               select
                                   new
                                   {
                                       Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                       ProductDesc =
                               (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                                " " + a.LastName),
                                       TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                   };
                    if (lst.Count > 0)
                    {
                        EmailAddress = lst.FirstOrDefault().EmailAddress;
                        UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                        DeliveryAddress = UserName + "<br>" +
                        lst.FirstOrDefault().Address1 + "<br>" +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + "<br>" : string.Empty) +
                        (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + "<br>" : string.Empty) +
                        lst.FirstOrDefault().DCountry + "<br>" +
                        lst.FirstOrDefault().Postcode + "<br>" +
                        GetBillingAddressPhoneNo(P2pOrderId);

                        OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                        Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                        ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                        BookingFee = lst.FirstOrDefault().BookingFee != null ? (decimal)lst.FirstOrDefault().BookingFee : 0;
                        Discount = _masterOrder.GetOrderDiscountByOrderId(P2pOrderId);

                        AdminFee = new ManageAdminFee().GetOrderAdminFeeByOrderID(orderID).ToString();
                        NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) +
                            (lst.FirstOrDefault().ShippingAmount.HasValue ? lst.FirstOrDefault().ShippingAmount.Value : 0) +
                            (lst.FirstOrDefault().BookingFee.HasValue ? lst.FirstOrDefault().BookingFee.Value : 0) +
                            Convert.ToDecimal(AdminFee) + Convert.ToDecimal(EvOtherCharges)).ToString();
                        GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");

                        var billAddress = _masterBooking.GetBillingShippingAddress(Convert.ToInt64(orderID));
                        if (billAddress != null)
                        {
                            BillingAddress = (!string.IsNullOrEmpty(billAddress.Address1) ? billAddress.Address1 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Address2) ? billAddress.Address2 + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.City) ? billAddress.City + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.State) ? billAddress.State + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Postcode) ? billAddress.Postcode + "<br>" : string.Empty) +
                                (!string.IsNullOrEmpty(billAddress.Country) ? billAddress.Country + "<br>" : string.Empty);
                        }
                    }
                    // to address
                    string ToEmail = EmailAddress;
                    GetReceiptLoga();
                    body = body.Replace("##headerstyle##", SiteHeaderColor);
                    body = body.Replace("##sitelogo##", logo);
                    body = body.Replace("##OrderNumber##", OrderNumber);
                    body = body.Replace("##UserName##", UserName.Trim());
                    body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                    body = body.Replace("##OrderDate##", OrderDate.Trim());
                    body = body.Replace("##Items##", currency + " " + Total.Trim());
                    body = body.Replace("##BookingCondition##", st.SiteURL + "Booking-Conditions");

                    var tblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == P2pOrderId);
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                    isAgentSite = objsite.IsAgent.Value;

                    GetSiteType(orderID);
                    if (isEvolviBooking)
                        EvolviTandC = objsite.SiteURL + "uk-ticket-collection";

                    string AdminFeeRow = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Admin Fee: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + AdminFee.Trim() + "</td></tr>";
                    if (AdminFee == "0.00")
                        AdminFeeRow = string.Empty;
                    if (isDiscountSite)
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        if (Discount == "0.00")
                            body = body.Replace("##Discount##", "");
                        else
                        {
                            DiscountHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Discount: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + Discount.Trim() + "</td></tr>";
                            body = body.Replace("##Discount##", DiscountHtml);
                        }
                    }
                    else
                    {
                        body = body.Replace("##AdminFee##", AdminFeeRow);
                        body = body.Replace("##Discount##", "");
                    }

                    if (isEvolviBooking)
                    {
                        OtherCharges = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Other Charges:</td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + EvOtherCharges + "</td></tr>";
                        BookingRefNO = "<tr><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong>Booking Ref: </strong></td><td align='left' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'>" + EvolviBookingRefNO(P2pOrderId) + "</td></tr>";
                    }

                    body = body.Replace("##BookingRef##", BookingRefNO);
                    body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##DeliveryFee##", currency + " " + ShippingAmount.Trim());
                    body = body.Replace("##BookingFee##", currency + " " + BookingFee);
                    body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());

                    if (tblord != null)
                    {
                        Session["ShipMethod"] = tblord.ShippingMethod;
                        Session["ShipDesc"] = tblord.ShippingDescription;
                        Session["CollectStation"] = tblord.CollectionStation;
                    }

                    var P2PSaleList = new ManageBooking().GetP2PSaleListByOrderID(P2pOrderId);
                    if (isEvolviBooking)
                        body = body.Replace("##ShippingMethod##", "Ticket on collection");
                    else if (isBene)
                    {
                        if (P2PSaleList != null && !string.IsNullOrEmpty(P2PSaleList.FirstOrDefault().DeliveryOption))
                        {
                            string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                            if (P2PSaleList.Any(x => x.DeliveryOption.ToLower() == "delivery by mail"))
                            {
                                body = body.Replace("##ShippingMethod##", !string.IsNullOrEmpty(shippingdesc) ? shippingdesc : "Delivery by mail");
                                isBeneDeliveryByMail = true;
                            }
                            else
                                body = body.Replace("##ShippingMethod##", P2PSaleList != null ? P2PSaleList.FirstOrDefault().DeliveryOption : "");
                        }
                    }
                    else
                    {
                        if (Session["ShipMethod"] != null)
                        {
                            string shippingdesc = (Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "") + (Session["ShipDesc"] != null ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "");
                            body = body.Replace("##ShippingMethod##", shippingdesc);
                        }
                        else
                            body = body.Replace("##ShippingMethod##", "");
                    }
                    body = body.Replace("#Blanck#", "&nbsp;");


                    var lstC = new ManageBooking().GetAllCartData(P2pOrderId).OrderBy(a => a.OrderIdentity).ToList();
                    var lstNew = (from a in lstC
                                  select new
                                  {
                                      a,
                                      Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                      ProductDesc = a.ProductType == "P2P"
                                      ? (_masterBooking.getP2PDetailsForEmail(a.PassSaleID, _masterBooking.GetPrintResponse((Guid)a.PassSaleID), a.ProductType, isEvolviBooking, isAgentSite, EvolviTandC))
                                      : (_masterBooking.getPassDetailsForEmail(a.PassSaleID, _masterBooking.GetPrintResponse((Guid)a.PassSaleID), a.ProductType, EvolviTandC)),
                                      TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                      CommissionFee = a.CommissionFee,
                                      Terms = a.terms
                                  }).ToList();

                    string strProductDesc = "";
                    int i = 1;
                    if (lstNew.Count() > 0)
                    {
                        lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                        body = body.Replace("##HeaderText##", "Delivery address");
                        body = body.Replace("##HeaderDeliveryText##", "Your order will be sent to:");
                        body = body.Replace("##DeliveryAddress##", DeliveryAddress);

                        foreach (var x in lstNew)
                        {
                            strProductDesc += "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth' align='center' bgcolor='#eeeeee'><tr><td bgcolor='#ffffff' height='5'></td></tr><tr><td style='padding: 10px;' align='center' valign='top' class='pad'><table width='100%' border='0' cellspacing='0' cellpadding='0' align='left' class='deviceWidth'>";
                            if (isAgentSite)
                                strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", "Net Amount :" + currency + " " + (x.Price + x.TktPrtCharge).ToString()).Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));
                            else
                                strProductDesc += x.ProductDesc.Replace("##Price##", currency + " " + x.Price.ToString()).Replace("##NetAmount##", "").Replace("##Name##", x.a.Title + " " + x.a.FirstName + (!string.IsNullOrEmpty(x.a.MiddleName) ? " " + x.a.MiddleName : "") + " " + (!string.IsNullOrEmpty(x.a.LastName) ? x.a.LastName.Split(new[] { "<BR/>" }, StringSplitOptions.None)[0] : ""));

                            strProductDesc += (HttpContext.Current.Session["CollectStation"] != null ? "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Collect at ticket desk:</strong></td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Session["CollectStation"] == null ? "" : HttpContext.Current.Session["CollectStation"].ToString() + "</td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif;'><strong></strong></td></tr>" : "");
                            if (P2PSaleList != null && P2PSaleList.Count > 0 && P2PSaleList.Any(z => z.ApiName.ToUpper() == "ITALIA"))
                            {
                                if (string.IsNullOrEmpty(P2PSaleList.FirstOrDefault(z => z.ID == x.a.PassSaleID).PdfURL))
                                    strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Important:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>This PNR is issued as a ticket on depart and must be printed from one of the self-service ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine.</td></tr>";
                            }
                            PassProtection = (Convert.ToDecimal(PassProtection) + x.TktPrtCharge).ToString();
                            if (x.Terms != null)
                                strProductDesc += "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Ticket Terms:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + HttpContext.Current.Server.HtmlDecode(x.Terms) + "</td></tr>";
                            if (isEvolviBooking)
                            {
                                var P2PData = _db.tblP2PSale.FirstOrDefault(Q => Q.ID == x.a.PassSaleID.Value);
                                if (P2PData != null)
                                {
                                    if (!string.IsNullOrEmpty(P2PData.EvSleeper))
                                    {
                                        strProductDesc += "<tr><td width='22%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'><strong>Reservation Details:</strong></td><td colspan='2' width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>";
                                        foreach (var item in P2PData.EvSleeper.Split('@'))
                                        {
                                            string ss = item.Replace("Seat Allocation:", "").Replace("Berth Allocation:", "");
                                            strProductDesc += "<div>" + HttpContext.Current.Server.HtmlDecode(ss) + "</div>";
                                        }
                                        strProductDesc += "</td></tr>";
                                    }
                                }
                            }
                            strProductDesc += "</table></td></tr></table>";
                            i++;
                        }
                    }
                    PassProtectionHtml = "<tr><td width='20%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>Pass Protection: </td><td width='45%' align='left' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'></td><td width='35%' align='right' valign='top' style='font-size: 13px; color: #5c5c5c;font-family: Arial, Helvetica, sans-serif;'>" + currency + " " + PassProtection.Trim() + "</td></tr>";
                    body = body.Replace("##PassProtection##", PassProtection == "0.00" ? "" : PassProtectionHtml);
                    body = body.Replace("##OrderDetails##", strProductDesc);
                    body = body.Replace("##BillingAddress##", BillingAddress);

                    var getDeliveryType = _masterBooking.getDeliveryOption(orderID);
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<table width='650' border='0' cellspacing='0' cellpadding='0' class='deviceWidth '><tr><td style='padding: 10px; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif;'colspan='2'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</td></tr></table>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                    body = body.Replace("../images/", SiteName + "images/");

                    var billing = _masterOrder.GetBillingInfo(orderID);
                    if (billing.Any())
                    {
                        var firstOrDefault = billing.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            billingEmailAddress = firstOrDefault.EmailAddress;
                            shippingEmailAddress = firstOrDefault.EmailAddressShpg;
                        }
                    }

                    /*Get smtp details*/
                    var result = _masterPage.GetEmailSettingDetail(siteId);
                    if (result != null)
                    {
                        var fromAddres = new MailAddress(result.Email, result.Email);
                        smtpClient.Host = result.SmtpHost;
                        smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                        smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                        message.From = fromAddres;
                        message.To.Add(shippingEmailAddress != string.Empty ? shippingEmailAddress : billingEmailAddress);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                        message.Subject = Subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        smtpClient.Send(message);
                        retVal = true;
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Email sent successfully.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Failed to send E-mail confimation!");
            }
            return retVal;
        }

        public void GetReceiptLoga()
        {
            string Name = "";
            if (HtmlFileSitelog.ToLower().Contains("irmailtemplate"))
                Name = "IR";
            else if (HtmlFileSitelog.ToLower().Contains("travelcut"))
                Name = "TC";
            else if (HtmlFileSitelog.ToLower().Contains("stamail"))
                Name = "STA";
            else if (HtmlFileSitelog.ToLower().Contains("MeritMail"))
                Name = "MM";

            switch (Name)
            {
                case "TC":
                    logo = "https://www.internationalrail.com/images/travelcutslogo.png";
                    SiteHeaderColor = "#0f396d";
                    break;
                case "STA":
                    logo = "https://www.internationalrail.com/images/mainLogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                case "MM":
                    logo = "https://www.internationalrail.com/images/meritlogo.png";
                    SiteHeaderColor = "#0c6ab8";
                    break;
                default:
                    logo = "https://www.internationalrail.com/images/logo.png";
                    SiteHeaderColor = "#941e34";
                    break;
            }
        }

        public string GetBillingAddressPhoneNo(long OrderID)
        {
            string BillingPhoneNo = string.Empty;
            var result = _db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
            if (result != null)
            {
                BillingPhoneNo += !string.IsNullOrEmpty(result.Phone) ? result.Phone : "";
                BillingPhoneNo += result.IsVisibleEmailAddress ? "<br/>" + result.EmailAddress : "";
            }
            return BillingPhoneNo;
        }

        public void GetSiteType(long OrderID)
        {
            var apiTypeList = new ManageBooking().GetAllApiType(OrderID);
            if (apiTypeList != null)
            {
                isEvolviBooking = apiTypeList.isEvolvi;
                isNTVBooking = apiTypeList.isNTV;
                isBene = apiTypeList.isBene;
                isTI = apiTypeList.isTI;
            }
        }

        public string EvolviBookingRefNO(long OrderID)
        {
            var result = _db.tblEvolviOtherChargesLookUps.FirstOrDefault(x => x.OrderId == OrderID);
            if (result != null)
            {
                return !string.IsNullOrEmpty(result.BookingRef) ? result.BookingRef : "";
            }
            return "";
        }

        public string GetCCEmailID()
        {
            try
            {
                string ccEmail = string.Empty;
                if (Session["AgentUserID"] != null)
                {
                    var agentId = Guid.Parse(Session["AgentUserID"].ToString());
                    var result = _masterPage.GetAgentOfficeEmailID(agentId).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.chkEmail && result.Email != string.Empty)
                            ccEmail = result.Email + ",";
                        if (result.chkSecondaryEmail && result.SecondaryEmail != string.Empty)
                            ccEmail = ccEmail + result.SecondaryEmail;
                    }
                }
                return ccEmail.TrimEnd(',');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}