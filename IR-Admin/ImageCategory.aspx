﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageCategory.aspx.cs"
    MasterPageFile="~/Site.Master" Inherits="IR_Admin.ImageCategory" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
             if(<%=Tab%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }
        });
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Image Category
    </h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ImageCategory.aspx" class="current">List</a></li>
            <li><asp:HyperLink ID="aNew" NavigateUrl="#" CssClass=" " runat="server">New</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdImgCategory" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdImgCategory_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Category Name">
                                    <ItemTemplate>
                                        <%#Eval("CategoryName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Width">
                                    <ItemTemplate>
                                        <%#Eval("Width")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Height">
                                    <ItemTemplate>
                                        <%#Eval("Height")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="ImageCategory.aspx?edit=<%#Eval("Id")%>" style="text-decoration: none">
                                            <img title="Edit" alt="Edit" src="images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: Block;">
                    <div class="divMain">
                        <div class="divleft">
                            Navigation Name:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtCName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfCname" runat="server" ControlToValidate="txtCName"
                                CssClass="valdreq" ErrorMessage="*" ValidationGroup="CurForm"></asp:RequiredFieldValidator>
                        </div>
                        <div class="divleft">
                            Width:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtWidth" runat="server" />
                        </div>
                        <div class="divleft">
                            Height:
                        </div>
                        <div class="divright">
                            <asp:TextBox ID="txtHeight" runat="server" />
                        </div>
                        <div class="divleft">
                            Is Active ?
                        </div>
                        <div class="divright">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                            .
                        </div>
                        <div class="divrightbtn" style="padding-top: 10px;">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="CurForm" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
