﻿<%@ Page Language="C#" Title="Login" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
    Inherits="IR_Admin.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>International Rail</title>
    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
    <style type="text/css">
        .starail-Form-error
        {
            background-color: #ffedee;
            border-color: #e6462e !important;
        }
    </style>
    <script type="text/jscript">
        $(document).ready(function () {
            var width = $(window).height() - 62;
            $("#container").attr("style", "min-height: " + width + "px;");
        });
        $(document).ready(function () {
            $("#forgotpass").click(function () {
                $("#divLogin").hide();
                $("#divforgot").show();
            });
            $("#Signin").click(function () {
                $("#divLogin").show();
                $("#divforgot").hide();
            });

            $('input[type="submit"]').click(function () {
                callvalerror();
            });
        });
        function callvalerror() {
            $("span[id*=reqvalerror]").each(function (key, value) {
                var style = $(this).css('display');
                var visibility = $(this).css('visibility');
                if (style == 'none' || visibility == 'hidden') {
                    $(this).parent().parent().find('input,select').removeClass('starail-Form-error');
                }
                else {
                    $(this).parent().parent().find('input,select').addClass('starail-Form-error');
                }
            });
            $("span[id*=reqcustvalerror]").each(function (key, value) {
                if ($(this).prev('span[id*=reqvalerror]').css('display') == 'none' || $(this).prev('span[id*=reqvalerror]').css('visibility') == 'hidden') {
                    var style = $(this).css('display');
                    var visibility = $(this).css('visibility');
                    if (style == 'none' || visibility == 'hidden') {
                        $(this).parent().parent().find('input,select').removeClass('starail-Form-error');
                    }
                    else {
                        $(this).parent().parent().find('input,select').addClass('starail-Form-error');
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        function PasswordSent() {
            document.getElementById('divLogin').style.display = 'none';
            document.getElementById('divforgot').style.display = 'Block';
            alert('Email Sent on Registered Email Id');
        }
        function PasswordErrorSent() {
            document.getElementById('divLogin').style.display = 'none';
            document.getElementById('divforgot').style.display = 'Block';
            alert('Email is not Registered with System');
        }
    </script>
</head>
<body>
    <div id="container">
        <div class="content">
            <form id="form2" runat="server">
            <div id="login-sec">
                <div class="login-logo">
                    <img src="images/Logo.png" /></div>
                <div class="full mr-tp2">
                    <div class="login-data">
                        <div id="divLogin" runat="server" style="display: block;" class="login">
                            <div class="full">
                                <div class="login-col2">
                                    <asp:TextBox ID="txtUsername" class="login" placeholder="Username" runat="server"></asp:TextBox>
                                    <div class="validate-msg">
                                        <asp:RequiredFieldValidator ID="reqvalerrorUsername" ControlToValidate="txtUsername"
                                            runat="server" ErrorMessage="*" ValidationGroup="loginform"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col2">
                                    <asp:TextBox ID="txtPassword" runat="server" class="login" placeholder="Password"
                                        TextMode="Password"></asp:TextBox>
                                    <div class="validate-msg">
                                        <asp:RequiredFieldValidator ID="reqvalerrorPassword" ControlToValidate="txtPassword"
                                            runat="server" ErrorMessage="*" ValidationGroup="loginform"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="remember-me">
                                    <asp:CheckBox ID="chkRememberMe" runat="server" />
                                    <label for="checkbox">
                                    </label>
                                    Remember me</div>
                                <div class="forgot-pass">
                                    <a href="#" id="forgotpass">Forgotten password</a></div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-btn">
                                    <asp:Button ID="BtnSubmit" runat="server" Text="LOGIN" ValidationGroup="loginform"
                                        OnClick="BtnSubmit_Click" />
                                    <br />
                                    <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" />
                                </div>
                            </div>
                        </div>
                        <div style="display: none;" id="divforgot" runat="server" class="login-row1">
                            <div class="full">
                                <div class="login-col2">
                                    <asp:TextBox ID="txtUser" class="login" runat="server" placeholder="Username" />
                                    <div class="validate-msg">
                                        <asp:RequiredFieldValidator ID="reqvalerrorUser" ControlToValidate="txtUser" runat="server"
                                            ErrorMessage="*" ForeColor="Red" ValidationGroup="FLoginForm" />
                                    </div>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col2">
                                    <asp:TextBox ID="txtEmail" class="login" runat="server" placeholder="Email"></asp:TextBox>
                                    <div class="validate-msg">
                                        <asp:RequiredFieldValidator ID="reqvalerrorEmail" ControlToValidate="txtEmail" ForeColor="Red"
                                            runat="server" ErrorMessage="*" ValidationGroup="FLoginForm"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reqcustvalerrorreEmail" runat="server" ErrorMessage="Invalid Email"
                                            ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="FLoginForm"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="full mr-tp1">
                                <div class="remember-me">
                                    Click here <span style="font-size: 14px;">»</span> <a id="Signin" href="#">Sign In
                                    </a>
                                </div>
                            </div>
                            <div class="full mr-tp1">
                                <div class="login-btn">
                                    <asp:Button ID="btnFSubmit" runat="server" Text="Request Password" ValidationGroup="FLoginForm"
                                        OnClick="btnFSubmit_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div id="footer">
        <div class="main-footer shade2">
            <div class="footer-lt">
                Copyright &copy; 2013 International Rail Ltd. All rights reserved.</div>
            <div class="footer-rt">
                <a href="#" title="Privacy Policy">Privacy Policy</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#" title="Terms &amp; Conditions">Terms &amp; Conditions</a></div>
        </div>
    </div>
</body>
</html>
