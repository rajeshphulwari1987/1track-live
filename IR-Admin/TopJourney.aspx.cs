﻿#region Using
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;
#endregion

namespace IR_Admin
{
    public partial class TopJourney : Page
    {
        readonly private ManageJourney _master = new ManageJourney();
        readonly private Masters _mmaster = new Masters();
        public string Tab = string.Empty;
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        string _cFromPath = string.Empty;
        string _cToPath = string.Empty;
        string _cBannerPath = string.Empty;
        private const int PageSize = 10;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            BindGrid();
            BindPager();
        }
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowMessage(0, null);
            Tab = "1";

            if (!Page.IsPostBack)
            {
                PageLoadEvent();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetTopJourneysForEdit(Guid.Parse(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }
        #endregion
        void PageLoadEvent()
        {
            BindSite();
            GetAllCurrency();
            BindFlag();
            BindGrid();
            txtPrice.Attributes.Add("onkeypress", "return keycheck()");
            txtHr.Attributes.Add("onkeypress", "return keycheck()");
            txtMin.Attributes.Add("onkeypress", "return keycheck()");
            BindPager();
        }
        #region Control Events
        protected void dtlTopJourney_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactiveJourneys(id);
                }

                if (e.CommandName == "TopJourney")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactiveTopJourney(id);
                }

                if (e.CommandName == "Popular")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _master.ActiveInactivePopular(id);
                }

                if (e.CommandName == "Remove")
                {
                    RemoveReadOnlyFile();
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    string bannerImage = _master.GetJourneyById(id).BannerImg;
                    string bPath = "~/" + bannerImage.Trim();
                    if (File.Exists(Server.MapPath(bPath)))
                        File.Delete(Server.MapPath(bPath));

                    var res = _master.DeleteJourneys(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                }

                _siteID = Master.SiteID;
                BindGrid();
                BindPager();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void rdFlagFrom_CheckedChanged(object sender, EventArgs e)
        {
            Tab = "2";
            foreach (DataListItem item in dtlFrmFlag.Items)
            {
                var rb = item.FindControl("rdFlagFrom") as RadioButton;
                var imgFlag = item.FindControl("imgFlag") as Image;
                if (rb != null && rb.Checked)
                {
                    if (imgFlag != null) _cFromPath = imgFlag.ImageUrl;
                    ViewState["_cFromPath"] = _cFromPath;
                }
            }
        }

        protected void rdFlagTo_CheckedChanged(object sender, EventArgs e)
        {
            Tab = "2";
            foreach (DataListItem item in dtlToFlag.Items)
            {
                var rb = item.FindControl("rdFlagTo") as RadioButton;
                var imgFlag = item.FindControl("imgFlag") as Image;
                if (rb != null && rb.Checked)
                {
                    if (imgFlag != null) _cToPath = imgFlag.ImageUrl;
                    ViewState["_cToPath"] = _cToPath;
                }
            }
        }

        protected void dtlFrmFlag_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            var rdb = (RadioButton)e.Item.FindControl("rdFlagFrom");
            var lbl = (Image)e.Item.FindControl("imgFlag");

            string script = "SetSingleFromFlag('" + lbl.ImageUrl + "',this)";
            rdb.Attributes.Add("onclick", script);
        }

        protected void dtlToFlag_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            var rdb = (RadioButton)e.Item.FindControl("rdFlagTo");
            var lbl = (Image)e.Item.FindControl("imgFlag");

            string script = "SetSingleToFlag('" + lbl.ImageUrl + "',this)";
            rdb.Attributes.Add("onclick", script);
        }
        #endregion



        #region Add/Edit
        public void BindSite()
        {
            ddlSite.DataSource = _mmaster.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "0"));

            ddlCountry.DataSource = _mmaster.GetCountryList().Where(x => x.IsActive == true).ToList();
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Country--", "0"));

            var totalRecords = _db.tblJourneys.Count();
            if (totalRecords != 0)
            {
                for (int i = (totalRecords + 10); i >= 0; i--)
                {
                    ddlSort.Items.Insert(0, new ListItem(i.ToString(CultureInfo.InvariantCulture), i.ToString(CultureInfo.InvariantCulture)));
                }
            }
        }

        void AddJourneys()
        {
            try
            {
                string host = Dns.GetHostName();
                var ip = Dns.GetHostEntry(host);
                string ipAdd = ip.AddressList[0].ToString();

                if (ViewState["_cFromPath"] != null)
                    _cFromPath = ViewState["_cFromPath"].ToString();

                if (ViewState["_cToPath"] != null)
                    _cToPath = ViewState["_cToPath"].ToString();

                if (ViewState["_cBannerPath"] != null)
                    _cBannerPath = ViewState["_cBannerPath"].ToString();

                var jid = _master.AddJourney(new tblJourney
                {
                    ID = Guid.NewGuid(),
                    CountryID = Guid.Parse(ddlCountry.SelectedValue),
                    From = txtFrom.Text.Trim(),
                    FromFlag = _cFromPath.Replace("~/", ""),
                    To = txtTo.Text.Trim(),
                    ToFlag = _cToPath.Replace("~/", ""),
                    Price = txtPrice.Text.Trim(),
                    DurationHr = Convert.ToInt32(txtHr.Text.Trim()),
                    DurationMin = Convert.ToInt32(txtMin.Text.Trim()),
                    BannerImg = _cBannerPath.Replace("~/", ""),
                    Description = txtDesc.Text.Trim(),
                    IpAddress = ipAdd,
                    IsActive = chkIsActv.Checked,
                    IsTopJourney = chkIsTopjourney.Checked,
                    IsPopular = chkPopular.Checked,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    SortOrder = Convert.ToInt32(ddlSort.SelectedValue),
                    CurrencyId = Guid.Parse(ddlCurrency.SelectedValue)
                });

                _master.AddJourneySiteLookup(new tblJourneySiteLookup
                {
                    ID = Guid.NewGuid(),
                    JourneyID = jid,
                    SiteID = Guid.Parse(ddlSite.SelectedValue)
                });

                ShowMessage(1, "Journeys added successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void UpdateJourneys()
        {
            try
            {
                var id = Guid.Parse(Request["id"]);
                string host = Dns.GetHostName();
                var ip = Dns.GetHostEntry(host);
                string ipAdd = ip.AddressList[0].ToString();

                if (ViewState["_cFromPath"] != null)
                    _cFromPath = ViewState["_cFromPath"].ToString();

                if (ViewState["_cToPath"] != null)
                    _cToPath = ViewState["_cToPath"].ToString();

                if (ViewState["_cBannerPath"] != null)
                    _cBannerPath = ViewState["_cBannerPath"].ToString();

                var journey = new tblJourney
                {
                    ID = id,
                    CountryID = Guid.Parse(ddlCountry.SelectedValue),
                    From = txtFrom.Text.Trim(),
                    FromFlag = _cFromPath.Replace("~/", ""),
                    To = txtTo.Text.Trim(),
                    ToFlag = _cToPath.Replace("~/", ""),
                    Price = txtPrice.Text.Trim(),
                    DurationHr = Convert.ToInt32(txtHr.Text.Trim()),
                    DurationMin = Convert.ToInt32(txtMin.Text.Trim()),
                    BannerImg = _cBannerPath.Replace("~/", ""),
                    Description = txtDesc.Text.Trim(),
                    IpAddress = ipAdd,
                    ModifyBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now,
                    IsActive = chkIsActv.Checked,
                    IsTopJourney = chkIsTopjourney.Checked,
                    IsPopular = chkPopular.Checked,
                    SortOrder = Convert.ToInt32(ddlSort.SelectedValue),
                    CurrencyId = Guid.Parse(ddlCurrency.SelectedValue)
                };
                var res = _master.UpdateJourney(journey);
                {
                    //Delete Existing Record
                    var journeyId = res;
                    _db.tblJourneySiteLookups.Where(w => w.JourneyID == journeyId).ToList().ForEach(_db.tblJourneySiteLookups.DeleteObject);
                    _db.SaveChanges();

                    var jsite = new tblJourneySiteLookup { ID = Guid.NewGuid(), JourneyID = journeyId, SiteID = Guid.Parse(ddlSite.SelectedValue) };
                    _master.AddJourneySiteLookup(jsite);
                }
                ShowMessage(1, "Journeys updated successfully.");
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = UploadFile();
                if (result)
                {
                    _siteID = Master.SiteID;
                    if (Request.QueryString["id"] == null)
                    {
                        var sortOrd = _master.IsExistSortOrder(_siteID, Convert.ToInt32(ddlSort.SelectedValue));
                        if (sortOrd)
                        {
                            Tab = "2";
                            divNew.Style.Add("display", "block");
                            ShowMessage(2, "Sort Order Already Exists");
                        }
                        else
                        {
                            AddJourneys();
                            BindGrid();
                            BindPager();
                            ClearControls();
                            Tab = "1";
                        }
                    }
                    else
                    {
                        var sortOrd = _master.IsExistSortOrderUpdate(Guid.Parse(Request.QueryString["id"]), _siteID, Convert.ToInt32(ddlSort.SelectedValue));
                        if (sortOrd)
                        {
                            Tab = "2";
                            divNew.Style.Add("display", "block");
                            ShowMessage(2, "Sort Order Already Exists");
                        }
                        else
                        {
                            UpdateJourneys();
                            BindGrid();
                            BindPager();
                            ClearControls();
                            Tab = "1";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void ClearControls()
        {
            ddlSite.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            txtFrom.Text = string.Empty;
            txtTo.Text = string.Empty;
            txtPrice.Text = string.Empty;
            txtHr.Text = string.Empty;
            txtMin.Text = string.Empty;
            chkIsActv.Checked = false;
            chkIsTopjourney.Checked = false;
            chkPopular.Checked = false;
            txtDesc.Text = string.Empty;
            foreach (DataListItem item in dtlFrmFlag.Items)
            {
                var rb = item.FindControl("rdFlagFrom") as RadioButton;
                if (rb != null)
                {
                    rb.Checked = false;
                }
            }
            foreach (DataListItem item in dtlToFlag.Items)
            {
                var rb = item.FindControl("rdFlagTo") as RadioButton;
                if (rb != null)
                {
                    rb.Checked = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }

        public void GetTopJourneysForEdit(Guid id)
        {
            var oP = _master.GetJourneyById(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = Guid.Parse(oP.SiteId.ToString()).ToString();
                txtFrom.Text = oP.From;
                txtTo.Text = oP.To;
                txtPrice.Text = oP.Price;
                txtHr.Text = oP.DurationHr.ToString();
                txtMin.Text = oP.DurationMin.ToString();
                txtDesc.Text = oP.Description;
                ViewState["_cFromPath"] = oP.FromFlag;
                ViewState["_cToPath"] = oP.ToFlag;
                ddlCountry.SelectedValue = string.IsNullOrEmpty(oP.CountryID.ToString()) ? "0" : oP.CountryID.ToString();
                ddlCurrency.SelectedValue = string.IsNullOrEmpty(oP.CurrencyId.ToString()) ? "0" : oP.CurrencyId.ToString();
                ddlSort.SelectedValue = oP.SortOrder.ToString();
                ViewState["_cBannerPath"] = oP.BannerImg;
                _cBannerPath = oP.BannerImg;

                foreach (DataListItem item in dtlFrmFlag.Items)
                {
                    var rb = item.FindControl("rdFlagFrom") as RadioButton;
                    var imgFlag = item.FindControl("imgFlag") as Image;
                    if (imgFlag != null && imgFlag.ImageUrl == oP.FromFlag)
                    {
                        if (rb != null) rb.Checked = true;
                    }
                }

                foreach (DataListItem item in dtlToFlag.Items)
                {
                    var rb = item.FindControl("rdFlagTo") as RadioButton;
                    var imgFlag = item.FindControl("imgFlag") as Image;
                    if (imgFlag != null && imgFlag.ImageUrl == oP.ToFlag)
                    {
                        if (rb != null) rb.Checked = true;
                    }
                }

                if (oP.IsActive != null) chkIsActv.Checked = (bool)oP.IsActive;
                if (oP.IsTopJourney != null) chkIsTopjourney.Checked = (bool)oP.IsTopJourney;
                if (oP.IsPopular != null) chkPopular.Checked = (bool)oP.IsPopular;
            }
        }
        #endregion
        #region UserDefined Functions
        void BindGrid()
        {
            Tab = "1";
            _siteID = Master.SiteID;
            var list = _master.GetJourneyList(_siteID, txtStation.Text, (ddlTopJourney.SelectedValue == "-1" ? null : ddlTopJourney.SelectedValue), (ddlPopular.SelectedValue == "-1" ? null : ddlPopular.SelectedValue), (ddlActive.SelectedValue == "-1" ? null : ddlActive.SelectedValue), ViewState["PageIndex"] == null ? 0 : int.Parse(ViewState["PageIndex"].ToString()), PageSize).ToList();
            dtlTopJourney.DataSource = list;
            dtlTopJourney.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected bool UploadFile()
        {
            var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
            if (fuBannerImg.HasFile)
            {
                if (fuBannerImg.PostedFile.ContentLength > 1048576)
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                    return false;
                }

                string fileName = fuBannerImg.FileName.Substring(fuBannerImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                if (!ext.Contains(fileName.ToUpper()))
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                    return false;
                }

                _cBannerPath = "~/Uploaded/SpJourneyImg/";
                var oCom = new Common();
                _cBannerPath = _cBannerPath + oCom.CropImage(fuBannerImg, _cBannerPath, 160, 155);
                ViewState["_cBannerPath"] = _cBannerPath;
            }

            return true;
        }

        public void BindFlag()
        {
            var result = _db.tblCountriesMsts.OrderBy(x => x.CountryName).ToList();
            dtlFrmFlag.DataSource = dtlToFlag.DataSource = result;
            dtlFrmFlag.DataBind();
            dtlToFlag.DataBind();
        }

        public void RemoveReadOnlyFile()
        {
            const string cntPath = "~/Uploaded/SpJourneyImg/";
            foreach (string cntImg in Directory.GetFiles(Server.MapPath(cntPath), "*.*", SearchOption.AllDirectories))
            {
                new FileInfo(cntImg) { Attributes = FileAttributes.Normal };
            }
        }
        #endregion
        #region Paging
        public void BindPager()
        {
            /*Bind Pager*/
            var oPageList = new List<ClsPageCount>();
            _siteID = Master.SiteID;
            var list = _master.TotalNumberOfRecord(_siteID, txtStation.Text, (ddlTopJourney.SelectedValue == "-1" ? null : ddlTopJourney.SelectedValue), (ddlPopular.SelectedValue == "-1" ? null : ddlPopular.SelectedValue), (ddlActive.SelectedValue == "-1" ? null : ddlActive.SelectedValue), ViewState["PageIndex"] == null ? 0 : int.Parse(ViewState["PageIndex"].ToString()), PageSize).ToList();
            int cnt = list.Count();
            int newpagecount = (cnt) / PageSize + (((cnt) % PageSize) > 0 ? 1 : 0);
            for (int i = 1; i <= newpagecount; i++)
            {
                var oPage = new ClsPageCount { PageCount = i.ToString(CultureInfo.InvariantCulture) };
                oPageList.Add(oPage);
            }
            DLPageCountItem.DataSource = oPageList;
            DLPageCountItem.DataBind();

            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage != null)
                    lblPage.Attributes.Add("class", "activepaging");
                break;
            }
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument) - 1;
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex + 1).ToString(CultureInfo.InvariantCulture))
                {
                    lblPage.Attributes.Add("class", "activepaging");
                }
                else
                {
                    lblPage.Attributes.Remove("class");
                }
            }

            ViewState["PageIndex"] = pageIndex * 10;
            BindGrid();
        }
        #endregion

        public void GetAllCurrency()
        {
            var currencyList = _master.GetCurrency().ToList();
            if (currencyList != null)
            {
                ddlCurrency.DataSource = currencyList;
                ddlCurrency.DataTextField = "ShortCode";
                ddlCurrency.DataValueField = "Id";
                ddlCurrency.DataBind();
            }
            ddlCurrency.Items.Insert(0, new ListItem("--Currency--", "0"));
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            _siteID = Master.SiteID;
            ViewState["PageIndex"] = null;
            BindGrid();
            BindPager();
        }
    }
}