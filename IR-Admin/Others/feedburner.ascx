﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="feedburner.ascx.cs"
    Inherits="IR_Admin.Others.feedburner" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".more-link").attr("target", "_blank");
        $(".more-link").attr("target", "_blank");
    });
</script>
<div class="dashboard-block-outer">
    <div class="dashboard-block">
        <h3>
            <asp:Label ID="lblFeedTitle" runat="server" />
        </h3>
        <div class="innner-dashboard">
            <asp:ListView ID="lstViewNewsFeeds" runat="server" OnItemDataBound="lstViewNewsFeeds_ItemDataBound">
                <LayoutTemplate>
                    <div runat="server" id="ItemPlaceHolder" style="width: 100%">
                    </div>
                    <div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <asp:Panel ID="pnlHeader" runat="server" CssClass="pnlNewsContentPanel">
                        <h4>
                            <asp:HyperLink ID="hlNewsTitle" runat="server"></asp:HyperLink><br />
                            Date:
                            <asp:Label ID="lblLastUpdateTime" runat="server"></asp:Label></h4>
                        <asp:Panel ID="panelNewsDescription" runat="server" CssClass="pnlNewsDescription">
                        </asp:Panel>
                    </asp:Panel>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <div>
                        Sorry, there are no items to display.
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
            <asp:DataPager ID="dataPagerFeeds" runat="server" PagedControlID="lstViewNewsFeeds">
                <Fields>
                </Fields>
            </asp:DataPager>
        </div>
    </div>
    <img src="images/shadow.png" alt="" width="100%" />
</div>
