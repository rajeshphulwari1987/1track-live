﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FaqList.ascx.cs" Inherits="IR_Admin.Others.FaqList" %>
<script type="text/javascript">
    $(document).ready(function () {
        onload();
    });

    function onload() {
        $('.QuesText').click(function () {
            $('.dvClsAns').hide();
            $(this).next('div').toggle();
        });
    };
</script>
<div class="dashboard-block-outer-faq">
    <div class="dashboard-block">
        <h3>
            1Track-Admin Faqs
        </h3>
        <div class="innner-dashboard-faq">
            <asp:Repeater ID="rptFaqList" runat="server">
                <ItemTemplate>
                    <div id="dvQues" runat="server" class="clsFaq">
                        <div class="QuesText">
                            <h4>
                                <span style="color: #b43758">
                                    <%#Eval("Question")%></span>
                            </h4>
                        </div>
                        <div id="dvAns" class="dvClsAns" style="display: none">
                            <ul>
                                <li>
                                    <%#Eval("Answer").ToString().Length > 120 ? HttpContext.Current.Server.HtmlDecode(Eval("Answer").ToString().Substring(0, 120) + "...") : HttpContext.Current.Server.HtmlDecode(Eval("Answer").ToString())%>
                                </li>
                            </ul>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <img src="images/shadow.png" alt="" width="100%" />
</div>
