﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Others
{
    public partial class Faq : Page
    {
        readonly ManageFaq _objFaq = new ManageFaq();
        public string Tab = "1";

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            ShowMessage(0, null);
            if (!IsPostBack)
                BindFaqs(10);
        }

        void BindFaqs(int totalItem)
        {
            if (totalItem > 50)
                dtlFaq.DataSource = _objFaq.GetAdminFaqList();
            else
                dtlFaq.DataSource = _objFaq.GetAdminFaqList().Skip(0).Take(totalItem);
            dtlFaq.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtQuestion.Text))
                    return;

                var faq = new tblAdminFaq
                {
                    ID = hdnId.Value == "0" ? 0 : Convert.ToInt32(hdnId.Value),
                    CreatedOn = DateTime.Now,
                    CreatedBy = AdminuserInfo.UserID,
                    ModifyOn = DateTime.Now,
                    ModifyBy = AdminuserInfo.UserID,
                    Answer = txtmessage.InnerText,
                    Question = txtQuestion.Text
                };
                _objFaq.AddEditAdminFaq(faq);
                ShowMessage(1, hdnId.Value == "0" ? "Record added successfully." : "Record updated successfully.");

                hdnId.Value = "0";
                txtmessage.InnerText = txtQuestion.Text = string.Empty;
                BindFaqs(10);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void dtlFaq_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                var faq = _objFaq.GetAdminFaqByID(Convert.ToInt32(e.CommandArgument.ToString()));
                txtmessage.InnerText = faq.Answer;
                txtQuestion.Text = faq.Question;
                hdnId.Value = faq.ID.ToString();
                Tab = "2";
            }

            if (e.CommandName == "Remove")
            {
                bool faq = _objFaq.DeleteAdminFaqByID(Convert.ToInt32(e.CommandArgument.ToString()));
                ShowMessage(1, "Record deleted successfully.");
                BindFaqs(10);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddlPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPages.SelectedValue != "0")
                BindFaqs(Convert.ToInt32(ddlPages.SelectedValue));
        }
    }
}