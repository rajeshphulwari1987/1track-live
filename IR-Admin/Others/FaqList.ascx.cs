﻿using System;
using System.Web.UI;
using System.Linq;
using Business;

namespace IR_Admin.Others
{
    public partial class FaqList : UserControl
    {
        readonly ManageFaq _objFaq = new ManageFaq();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                GetFaqList();
        }

        public void GetFaqList()
        {
            rptFaqList.DataSource = _objFaq.GetAdminFaqList().Skip(0).Take(5);
            rptFaqList.DataBind();
        }
    }
}