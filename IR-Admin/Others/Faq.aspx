﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Faq.aspx.cs" Inherits="IR_Admin.Others.Faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">      
  window.setTimeout("closeDiv();", 5000);
  function closeDiv() 
  {
    $("#MainContent_DivSuccess").fadeOut("slow", null);
    $("#MainContent_DivError").fadeOut("slow", null);

  }  

  $(function () 
  {
     if(<%=Tab%>=='1')
        {
           $("ul.list").tabs("div.panes > div");
        }
  });
 
  function ResetDiv() {
    document.getElementById('MainContent_aNew').className = 'current';
    document.getElementById('aList').className = ' ';
    document.getElementById('MainContent_divlist').style.display = 'none';
    document.getElementById('MainContent_divNew').style.display = 'Block';
  }
    
    </script>
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtmessage').redactor({ iframe: true, minHeight: 200 });
        });
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Admin Faq's
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Faq.aspx" class="current">List</a></li>
            <li><a id="aNew" href="Faq.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div style="height: auto; background:gray;border-radius:2px;padding:1px;">
                        <asp:DropDownList ID="ddlPages" runat="server" Style="width: 95px; float: right;
                            margin: 5px 0px;" AutoPostBack="True" OnSelectedIndexChanged="ddlPages_SelectedIndexChanged">
                            <asp:ListItem Value="0">--Page Item--</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="50">50</asp:ListItem>
                            <asp:ListItem Value="100">100 +</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DataList ID="dtlFaq" runat="server" Width="100%" OnItemCommand="dtlFaq_ItemCommand"
                            BackColor="#eeeeee">
                            <ItemTemplate>
                                <div class="parentF">
                                    <div class="parentQ">
                                        <asp:ImageButton runat="server" ID="imgDelete" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                            Style="float: right;" CommandName="Remove" ImageUrl="~/images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this faq?')" />
                                        <asp:ImageButton runat="server" ID="imgEdit" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                            Style="float: right;" CommandName="Edit" ImageUrl="~/images/edit.png" />
                                        <%#Eval("Question")%>
                                    </div>
                                    <div class="parentA">
                                        <%# Eval("Answer").ToString().Length > 200 ? (Eval("Answer") as string).Substring(0, 200) : Eval("Answer")%>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                Question:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtQuestion" runat="server" MaxLength="480" Width="500px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" valign="top">
                                Answer:
                            </td>
                            <td class="col">
                                <textarea id="txtmessage" runat="server" clientidmode="Static"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="rv" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                <asp:HiddenField ID="hdnId" Value="0" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
