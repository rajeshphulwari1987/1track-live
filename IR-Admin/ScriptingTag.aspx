﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ScriptingTag.aspx.cs" Inherits="IR_Admin.ScriptingTag" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">    
        function keycheck() {
        var KeyID = event.keyCode;
        if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
            return true;
        return false;
    }
        
     window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }  
        $(function () {                  
             if(<%=Tab%>=='1')
            {
               $("ul.list").tabs("div.panes > div");
            }
        });
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    
    </script>
    <style type="text/css">
        .col
        {
            border-bottom: 1px dashed #B1B1B1;
            line-height: 30px;
            padding-top: 6px;
        }
        .train-detail-block
        {
            width: 100% !important;
        }
        
        #MainContent_grdScriptingTag table
        {
            width: 100%;
        }
        #MainContent_grdScriptingTag table tr td
        {
            text-align: left;
            vertical-align: top;
            padding: 10px;
        }
        #MainContent_grdScriptingTag table tr th
        {
            text-align: left;
            vertical-align: top;
            padding: 10px;
            font-size: 13px;
            color: White;
            background: url(./schemes/images/grid-head-bg.jpg) no-repeat 0px 0px;
        }
        #MainContent_grdScriptingTag table tr:nth-child(even)
        {
            background-color: #ECECEC;
            border: 1px solid #ECECEC;
        }
        #MainContent_grdScriptingTag table tr:nth-child(odd)
        {
            background-color: #FBDEE6;
            border: 1px solid #FBDEE6;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Scripting Tag
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ScriptingTag.aspx" class="current">List</a></li>
            <li><a id="aNew" href="ScriptingTag.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:GridView ID="grdScriptingTag" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                            CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnRowCommand="grdp2pSetting_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Record not found.</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Page Name">
                                    <ItemTemplate>
                                        <%#Eval("PageType")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="18%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="ImgUpdateRecord" AlternateText="UpdateRecord"
                                            ToolTip="Update Record" CommandArgument='<%#Eval("ID")%>' CommandName="UpdateRecord"
                                            ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?')"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                Site Name:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" runat="server" Width="40%" />
                                <asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Page Name:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlPages" runat="server" Width="40%">
                                    <asp:ListItem Value="0">Select Page</asp:ListItem>
                                    <asp:ListItem Value="Home">Home</asp:ListItem>
                                    <asp:ListItem Value="Contact Us">Contact Us</asp:ListItem>
                                    <asp:ListItem Value="Passes">Passes</asp:ListItem>
                                    <asp:ListItem Value="Pass Inner">Pass Inner</asp:ListItem>
                                    <asp:ListItem Value="Pass Details">Pass Details</asp:ListItem>
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfPages" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlPages" ValidationGroup="rv" InitialValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Script:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtScript" runat="server" TextMode="MultiLine" MaxLength="2000"
                                    Width="95%" Rows="15" />
                                <asp:RequiredFieldValidator ID="rfScript" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtScript" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkIsActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="rv" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
