﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ContactInfo : Page
    {
        private readonly Masters _oMasters = new Masters();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        private const int PageSize = 6;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : PageSize * int.Parse(ViewState["PageIndex"].ToString()), PageSize);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPager();
                _siteID = Master.SiteID;
                BindList(_siteID, ViewState["PageIndex"] == null ? 0 : PageSize * int.Parse(ViewState["PageIndex"].ToString()), PageSize);
            }
        }

        private void BindList(Guid siteId, int start, int end)
        {
            try
            {
                if (siteId == new Guid())
                    siteId = Master.SiteID;
                var list = _oMasters.GetContactsList(siteId, start, end);
                dtlContactInfo.DataSource = list != null && list.Count > 0 ? list : null;
                dtlContactInfo.DataBind();
            }
            catch (Exception ex)
            {
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void dtlContactInfo_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _oMasters.DeleteContactInfo(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                }

                _siteID = Master.SiteID;
                BindList(_siteID, ViewState["PageIndex"] == null ? 0 : PageSize * int.Parse(ViewState["PageIndex"].ToString()), PageSize);
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
        #region Paging
        public void BindPager()
        {
            var oPageList = new List<ClsPageCount>();
            int cnt = _oMasters.TotalNumberOfContacts();
            int newpagecount = cnt / PageSize + ((cnt % PageSize) > 0 ? 1 : 0);
            for (int i = 1; i <= (newpagecount < 5 ? newpagecount : 5); i++)
            {
                var oPage = new ClsPageCount {PageCount = i.ToString(CultureInfo.InvariantCulture)};
                oPageList.Add(oPage);
            }
            DLPageCountItem.DataSource = oPageList;
            DLPageCountItem.DataBind();
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : PageSize * int.Parse(ViewState["PageIndex"].ToString()), PageSize);

            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage != null)
                    lblPage.Attributes.Add("class", "activepaging");
                break;
            }
        }

        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int pageIndex = Convert.ToInt32(e.CommandArgument) - 1;
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex + 1).ToString(CultureInfo.InvariantCulture))
                {
                    lblPage.Attributes.Add("class", "activepaging");
                }
                else
                {
                    lblPage.Attributes.Remove("class");
                }
            }
            ViewState["PageIndex"] = pageIndex;
            BindList(_siteID, ViewState["PageIndex"] == null ? 0 : PageSize * int.Parse(ViewState["PageIndex"].ToString()), PageSize);
        }
        #endregion
    }
}