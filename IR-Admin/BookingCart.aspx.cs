﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;

public partial class BookingCart : Page
{
    #region Global Variables
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private double _total = 0;
    readonly ManageUser _ManageUser = new ManageUser();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    ManageTrainDetails _master = new ManageTrainDetails();
    ManageBooking _masterBooking = new ManageBooking();
    public static string currency = "$";
    public static Guid currencyID = new Guid();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid pageID, siteId;
    public string script = "<script></script>";
    public string siteURL;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "restirctCopy", "restrictCopy();", true);
        txtZip.Attributes.Add("onkeypress", "return keycheck()");
        ShowMessage(0, string.Empty);
        if (!IsPostBack)
        {
            Session.Remove("P2POrderID");
            BindPageMandatoryFields();
            GetCurrencyCode();
            GetTheme(siteId);

            if (Session["AgentUserID"] != null)
            {
                var IDuser = Guid.Parse(Session["AgentUserID"].ToString());
                var Agentlist = _ManageUser.AgentDetailsById(IDuser);
                var AgentNameAndEmail = _ManageUser.AgentNameEmailById(IDuser);
                if (Agentlist != null)
                {
                    ddlMr.SelectedValue = Convert.ToInt32(AgentNameAndEmail.Salutation).ToString();
                    txtFirst.Text = AgentNameAndEmail.Forename;
                    txtLast.Text = AgentNameAndEmail.Surname;
                    txtEmail.Text = AgentNameAndEmail.EmailAddress;
                    txtConfirmEmail.Text = AgentNameAndEmail.EmailAddress;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;

                    txtshpfname.Text = Agentlist.FirstName;
                    txtshpLast.Text = Agentlist.LastName;
                    txtshpEmail.Text = Agentlist.Email;
                    txtshpConfirmEmail.Text = Agentlist.Email;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;
                }
            }
            else if (Session["USERUserID"] != null)
            {
                var IDuser = Guid.Parse(Session["USERUserID"].ToString());
                var userlist = _ManageUser.GetUserbyID(IDuser);
                if (userlist != null)
                {
                    txtFirst.Text = userlist.FirstName;
                    txtLast.Text = userlist.LastName;
                    txtEmail.Text = userlist.Email;
                    txtConfirmEmail.Text = userlist.Email;
                    ddlCountry.SelectedValue = Convert.ToString(userlist.Country);
                    txtCity.Text = userlist.City;
                    txtAdd.Text = userlist.Address;
                    txtZip.Text = userlist.PostCode;

                    txtshpfname.Text = userlist.FirstName;
                    txtshpLast.Text = userlist.LastName;
                    txtshpEmail.Text = userlist.Email;
                    txtshpConfirmEmail.Text = userlist.Email;
                    ddlshpCountry.SelectedValue = Convert.ToString(userlist.Country);
                    txtshpCity.Text = userlist.City;
                    txtshpAdd.Text = userlist.Address;
                    txtshpZip.Text = userlist.PostCode;
                }
            }
            Session["BOOKING-REPLY"] = null;
            PageLoadEvent();
            QubitOperationLoad();
        }
    }

    public void BindPageMandatoryFields()
    {
        try
        {
            if (Page.RouteData.Values["PageId"] == null)
                return;

            pageID = (Guid)Page.RouteData.Values["PageId"];
            var list = _masterPage.GetMandatoryVal(siteId, pageID);

            foreach (var item in list)
            {
                if (item.ControlField.Trim() == "rfFirst")
                    BookingpassrfFirst.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfLast")
                    BookingpassrfLast.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfEmail")
                    BookingpassrfEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfvEmail2")
                    BookingpassrfvEmail2.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfAdd")
                    BookingpassrfAdd.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfZip")
                    BookingpassrfZip.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfCountry")
                    BookingpassrfCountry.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfDateOfDepature")
                    BookingpassrfDateOfDepature.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipFirstName")
                    BookingpassrfshipFirstName.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipLastName")
                    BookingpassrfshipLastName.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipEmail")
                    BookingpassrfshipEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfconfirmShipEmail")
                    BookingpassrfconfirmShipEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpAdd")
                    BookingpassrfshpAdd.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpZip")
                    BookingpassrfshpZip.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpCountry")
                    BookingpassrfshpCountry.ValidationGroup = item.ValGrp.Trim();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetTheme(Guid SiteId)
    {
        var blueThemeID = Guid.Parse("4FC1F398-5901-439B-A1E0-27B1CBE2BBEB");
        var theme = _db.tblSiteThemes.FirstOrDefault(x => x.SiteID == SiteId);
        if (theme != null)
            if (lnkBookStyle != null)
            {
                if (theme.ThemeID == blueThemeID)
                {
                    var css = "Styles/" + theme.CssFolderName + "/BookingCart.css";
                    lnkBookStyle.Attributes.Add("href", css);
                }
            }
    }

    public void QubitOperationLoad()
    {
        if (Session["siteId"] != null)
        {
            var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            if (lstQbit != null && lstQbit.Count() > 0)
            {
                var res = lstQbit.FirstOrDefault();
                if (res != null)
                    script = res.Script;
            }
        }
    }

    void PageLoadEvent()
    {
        ddlCountry.DataSource = _master.GetCountryDetail();
        ddlCountry.DataValueField = "CountryID";
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        ddlshpCountry.DataSource = _master.GetCountryDetail();
        ddlshpCountry.DataValueField = "CountryID";
        ddlshpCountry.DataTextField = "CountryName";
        ddlshpCountry.DataBind();
        ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        if (Session["siteId"] != null && Session["OrderID"] != null)
            new ManageBooking().UpdateSiteToOrder(Convert.ToInt64(Session["OrderID"]), Guid.Parse(Session["siteId"].ToString()));

        AddItemInShoppingCart();
        GetCurrencyCode();
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            string countryID = cookie.Values["_cuntryId"];
            ddlCountry.SelectedValue = countryID;
            ddlshpCountry.SelectedValue = countryID;
        }
        PrepouplateSeniorAndAdult();
        FillShippingData();
    }

    public void FillShippingData()
    {
        if (ddlCountry.SelectedValue != "0")
        {
            var countryid = Guid.Parse(ddlCountry.SelectedValue);
            var lstShip = new ManageBooking().getAllShippingDetail(siteId, countryid);
            if (lstShip.Any())
                rptShippings.DataSource = lstShip;
            else
            {
                var lstDefaultShip = new ManageBooking().getDefaultShippingDetail(siteId);
                if (lstDefaultShip.Any())
                    rptShippings.DataSource = lstDefaultShip;
                else
                    rptShippings.DataSource = null;
            }

            rptShippings.DataBind();
        }
    }

    void PrepouplateSeniorAndAdult()
    {
        if (Session["RailPassData"] != null)
        {
            var list = Session["RailPassData"] as List<getRailPassData>;
            ldlpassDate.InnerText = list.Min(t => t.PassStartDate).ToString().Substring(0, 10);
            if (Session["AgentUserID"] == null)
            {
                var listr = list.OrderBy(x => x.TravellerName);
                foreach (var rec in listr)
                {
                    if (rec.TravellerName.Contains("Adult") || rec.TravellerName.Contains("Senior "))
                    {
                        ddlMr.SelectedValue = rec.Title;
                        txtFirst.Text = rec.FirstName;
                        txtLast.Text = rec.LastName;
                        break;
                    }
                }
            }
        }
    }

    public void AddItemInShoppingCart()
    {
        try
        {
            if (Session["OrderID"] == null)
                return;
            var lstPassDetail = _masterBooking.GetAllPassSale(Convert.ToInt64(Session["OrderID"]), "", getsitetickprotection()).OrderBy(x => x.OrderIdentity).ToList();
            lstPassDetail = lstPassDetail.ToList();

            if (lstPassDetail.Count() > 0)
            {
                rptTrain.DataSource = lstPassDetail.Count > 0 ? lstPassDetail : null;
                rptTrain.DataBind();
            }
            else
            {
                rptTrain.DataSource = null;
                rptTrain.DataBind();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void bindUsersession(string Email, Guid ID, string FirstName, string LastName, Guid SiteId, string Password)
    {
        Session.Remove("USERUsername");
        Session.Remove("USERRoleId");
        Session.Remove("USERUserID");
        Session.Remove("USERSiteID");
        USERuserInfo.UserEmail = Email;
        USERuserInfo.ID = ID;
        USERuserInfo.Username = FirstName;
        USERuserInfo.SiteID = SiteId;
    }

    //protected void DeleteTicketInfo(string id)
    //{
    //    try
    //    {
    //        List<ShoppingCartDetails> cartList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
    //        List<BookingRequest> bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;

    //        if (cartList.Count > 0 && bookingList.Count > 0)
    //        {
    //            cartList.RemoveAll(x => x.Id.ToString() == id);
    //            bookingList.RemoveAll(x => x.Id.ToString() == id);
    //            Session["SHOPPINGCART"] = cartList;
    //            Session["BOOKING-REQUEST"] = bookingList;
    //            ShowMessage(1, "You have successfully deleted ticket from list view");
    //        }
    //        else
    //        {
    //            Session["SHOPPINGCART"] = null;
    //            Session["BOOKING-REQUEST"] = null;
    //        }
    //        AddItemInShoppingCart();
    //    }
    //    catch (Exception ex)
    //    {
    //        ShowMessage(2, ex.Message);
    //    }
    //}

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var chk = e.Item.FindControl("chkTicketProtection") as CheckBox;
            var lblPrice = e.Item.FindControl("lblPrice") as Label;
            var lbltpPrice = e.Item.FindControl("lbltpPrice") as Label;
            var currsyb = e.Item.FindControl("currsyb") as Label;
            var lblTckProc = e.Item.FindControl("lblTckProc") as Label;
            var divTckProt = e.Item.FindControl("divTckProt") as HtmlGenericControl;
            var istckProt = _masterPage.IsTicketProtection(siteId);
            if (divTckProt != null)
                divTckProt.Visible = istckProt;

            if (lblTckProc != null)
                lblTckProc.Visible = istckProt;

            if (chk.Checked)
            {
                lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                _total = _total + Convert.ToDouble(lblPrice.Text);
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            }
            else
            {
                _total = _total + Convert.ToDouble(lblPrice.Text);
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            var lblTotal = e.Item.FindControl("lblTotal") as Label;
            var lblBookingFee = e.Item.FindControl("lblBookingFee") as Label;
            double totalAmount;
            var lst = new ManageBooking().GetBooingFees(Convert.ToDecimal(_total), Guid.Parse(Session["siteId"].ToString()));
            if (lst.Any())
            {
                if (Session["AgentUserID"] != null)
                {
                    lblBookingFee.Text = "0.00";
                    hdnBookingFee.Value = "0.00";
                }
                else
                {
                    decimal amountAfterConv = Convert.ToDecimal(lst.FirstOrDefault().BookingFee) * Convert.ToDecimal(lst.FirstOrDefault().convRate);
                    lblBookingFee.Text = amountAfterConv.ToString("F2");
                    hdnBookingFee.Value = amountAfterConv.ToString("F2");
                }
            }
            else
            {
                lblBookingFee.Text = "0.00";
                hdnBookingFee.Value = "0.00";
            }
            totalAmount = _total + Convert.ToDouble(hdnBookingFee.Value.Trim());
            lblTotal.Text = totalAmount.ToString("F2");
            lblTotalCost.Text = totalAmount.ToString("F2");
        }
    }

    protected void chk_CheckedChanged(object sender, EventArgs e)
    {
        var chk = (CheckBox)sender;
        var lbltpPrice = chk.Parent.FindControl("lbltpPrice") as Label;
        var lblPrice = chk.Parent.FindControl("lblPrice") as Label;
        var lblPassSaleID = chk.Parent.FindControl("lblPassSaleID") as Label;
        var lblHidPriceValue = chk.Parent.FindControl("lblHidPriceValue") as Label;
        var currsyb = chk.Parent.FindControl("currsyb") as Label;

        var passsaleid = new Guid();
        if (!string.IsNullOrEmpty(lblPassSaleID.Text))
            passsaleid = Guid.Parse(lblPassSaleID.Text);

        if (chk.Checked)
        {
            lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            _masterBooking.SetTicketProtectionAmt(passsaleid, Convert.ToDecimal(lbltpPrice.Text));
            lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
            lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
        }
        else
        {
            _masterBooking.SetTicketProtectionAmt(passsaleid, 0);
            lblPrice.Text = lblHidPriceValue.Text;
            lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
            currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "getdata()", true);

        if (Session["ProductType"] != null)
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showHideShipping", "showHideShipping()", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showmessagecountry", "showmessagecountry()", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    public decimal getsitetickprotection()
    {
        try
        {
            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
            {
                divpopupdata.InnerHtml = list.Description;
                return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
            }
            else
                return 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCurrencyCode()
    {
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            siteId = Guid.Parse(cookie.Values["_siteId"]);
            Session["siteId"] = siteId;
            currencyID = Guid.Parse(cookie.Values["_curId"]);
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    protected void chkShippingfill_CheckedChanged(object sender, EventArgs e)
    {
        if (chkShippingfill.Checked)
        {
            pnlbindshippping.Visible = true;
            ddlshpMr.SelectedValue = ddlMr.SelectedValue;
            txtshpfname.Text = txtFirst.Text;
            txtshpLast.Text = txtLast.Text;
            txtshpEmail.Text = txtEmail.Text;
            txtshpConfirmEmail.Text = txtConfirmEmail.Text;
            txtshpAdd.Text = txtAdd.Text;
            txtshpAdd2.Text = txtAdd2.Text;
            txtshpCity.Text = txtCity.Text;
            txtshpState.Text = txtState.Text;
            txtshpZip.Text = txtZip.Text;
            ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
        }
        else
        {
            pnlbindshippping.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showmessagecountry", "showmessagecountry()", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            #region Agent login and Guest login
            if (Session["USERUserID"] == null && Session["AgentUsername"] == null)
            {
                var UserID = Guid.NewGuid();
                var listUser = _ManageUser.CheckEmailUser(txtEmail.Text);
                if (listUser != null && listUser.IsActive == true)
                {
                    //Login USER User Information
                    bindUsersession(listUser.Email, listUser.ID, listUser.FirstName, listUser.LastName, listUser.SiteId, listUser.Password);
                    UserID = listUser.ID;
                    if (Session["OrderID"] != null)
                    {
                        long orderID = Convert.ToInt64(Session["OrderID"]);
                        _masterBooking.SetorderUserid(orderID, UserID);
                    }
                }
                else
                {
                    //add Login USER User Information
                    if (Session["USERUserID"] != null)//For Gust Only
                    {
                        UserID = Guid.Parse(Session["USERUserID"].ToString());
                    }
                    string password = Membership.GeneratePassword(10, 3);
                    bool result = _masterBooking.AddLoginUSer(new tblUserLogin
                    {
                        ID = UserID,
                        FirstName = txtFirst.Text,
                        LastName = txtLast.Text,
                        Email = txtEmail.Text,
                        Password = password,
                        Country = Guid.Parse(ddlCountry.SelectedValue),
                        SiteId = siteId,
                        IsActive = true
                    });
                    if (result == false)
                        bindUsersession(txtEmail.Text, UserID, txtFirst.Text, txtLast.Text, siteId, password);
                }
            }
            #endregion
            Session["ShipMethod"] = hdnShipMethod.Value.Trim();
            Session["ShipDesc"] = hdnShipDesc.Value.Trim();

            int postcodelen = txtZip.Text.Trim().Replace(" ", "").Length;
            lblpmsg.Visible = postcodelen > 7;
            if (postcodelen > 7)
                return;
            AddPassBookingInLocalDB();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void AddPassBookingInLocalDB()
    {
        var objBillingAddress = new tblOrderBillingAddress();
        objBillingAddress.ID = Guid.NewGuid();
        objBillingAddress.OrderID = Convert.ToInt64(Session["OrderID"]);
        objBillingAddress.Title = ddlMr.SelectedItem.Text;
        objBillingAddress.FirstName = txtFirst.Text;
        objBillingAddress.LastName = txtLast.Text;
        objBillingAddress.Address1 = txtAdd.Text;
        objBillingAddress.Address2 = txtAdd2.Text;
        objBillingAddress.EmailAddress = txtEmail.Text;
        objBillingAddress.City = txtCity.Text;
        objBillingAddress.State = txtState.Text;
        objBillingAddress.Country = ddlCountry.SelectedItem.Text;
        objBillingAddress.Postcode = txtZip.Text;

        if (!chkShippingfill.Checked)
        {
            ddlshpMr.SelectedValue = ddlMr.SelectedValue;
            txtshpfname.Text = txtFirst.Text;
            txtshpLast.Text = txtLast.Text;
            txtshpEmail.Text = txtEmail.Text;
            txtshpConfirmEmail.Text = txtConfirmEmail.Text;
            txtshpAdd.Text = txtAdd.Text;
            txtshpAdd2.Text = txtAdd2.Text;
            txtshpCity.Text = txtCity.Text;
            txtshpState.Text = txtState.Text;
            txtshpZip.Text = txtZip.Text;
            ddlshpCountry.SelectedValue = ddlCountry.SelectedValue;
        }

        objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
        objBillingAddress.FirstNameShpg = txtshpfname.Text;
        objBillingAddress.LastNameShpg = txtshpLast.Text;
        objBillingAddress.Address1Shpg = txtshpAdd.Text;
        objBillingAddress.Address2Shpg = txtshpAdd2.Text;
        objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
        objBillingAddress.CityShpg = txtshpCity.Text;
        objBillingAddress.StateShpg = txtshpState.Text;
        objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
        objBillingAddress.PostcodeShpg = txtshpZip.Text;

        if (txtDateOfDepature.Text.Trim() != "DD/MM/YYYY")
            new ManageBooking().UpdateDepatureDate(Convert.ToInt64(Session["OrderID"]), Convert.ToDateTime(txtDateOfDepature.Text));

        new ManageBooking().AddOrderBillingAddress(objBillingAddress);
        new ManageBooking().UpdateOrderShippingAmount(Convert.ToDecimal(hdnShippingCost.Value.Trim()), Convert.ToInt64(Session["OrderID"]));
        new ManageBooking().UpdateOrderBookingFee(Convert.ToDecimal(hdnBookingFee.Value.Trim()), Convert.ToInt64(Session["OrderID"]));
        Response.Redirect("~/PaymentProcess", false);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("PassDetail", true);
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillShippingData();
    }
}