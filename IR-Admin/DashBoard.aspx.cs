﻿using System;
using System.Linq;
using System.Web.UI;
using Business;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace IR_Admin
{
    public partial class DashBoard : Page
    {
        Guid _SiteID;
        public Guid SiteID;
        readonly Masters _Master = new Masters();

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["selectedSite"] == null)
                {
                    BindSites();
                    mdpupSite.Show();
                }
                else
                {
                    pnlSite.Visible = false;
                    divload.Visible = false;
                }
            }
        }


        public void BindSites()
        {
            var result = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID).OrderBy(x => x.DISPLAYNAME);
            if (result.Any())
            {
                ddlSites.DataSource = result;
                ddlSites.DataTextField = "DisplayName";
                ddlSites.DataValueField = "ID";
                if (AdminuserInfo.SiteID == new Guid())
                    ddlSites.SelectedValue = _Master.UserSitelist().Where(x => x.ADMINUSERID == AdminuserInfo.UserID).FirstOrDefault().ID.ToString();
                else
                    ddlSites.SelectedValue = _Master.UserSitelist().Where(x => x.ID == AdminuserInfo.SiteID && x.ADMINUSERID == AdminuserInfo.UserID).FirstOrDefault().ID.ToString();
                ddlSites.DataBind();
                ddlSites.Items.Insert(0, new ListItem("--Select Site--", "-1"));
                ddlSites.SelectedValue = "-1";
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            mdpupSite.Hide();
            pnlSite.Visible = false;
            divload.Visible = false;
            var lstsites = Master.FindControl("lstsites") as DropDownList;
            if (lstsites != null)
            {
                if (ddlSites.SelectedValue != "-1")
                {
                    SiteID = Guid.Parse(ddlSites.SelectedValue);
                    lstsites.SelectedValue = SiteID.ToString();
                    Session["selectedSite"] = "1";
                    (Master as SiteMaster).BindMenu();
                }
            }
            AdminuserInfo.SiteID = SiteID;
            var MasterPage = (SiteMaster)Page.Master;
            if (EmailSchedular.ExpiredRoleActivated() && Request.Url.AbsolutePath.ToLower().Contains("dashboard"))
                MasterPage.BindCircles();
        }

        protected void btnClose_Click(object sender, ImageClickEventArgs e)
        {
            pnlSite.Visible = false;
            divload.Visible = false;
        }

        [WebMethod]
        public static string[] ServerandSiteTime(int Hours, int Minuts)
        {
            string[] data = new string[4];
            DateTime DateTimes = DateTime.Now;
            data[0] = DateTimes.ToString("dd/MM/yyyy");
            data[1] = DateTimes.ToString("HH:mm");

            DateTimes = DateTimes.AddHours(Hours);
            DateTimes = DateTimes.AddMinutes(Minuts);
            data[2] = DateTimes.ToString("dd/MM/yyyy");
            data[3] = DateTimes.ToString("HH:mm");

            return data;
        }
    }
}