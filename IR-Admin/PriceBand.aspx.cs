﻿#region Using
using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class PriceBand : Page
    {
        readonly ManagePriceBand _objPrice = new ManagePriceBand();
        public string Tab = string.Empty;
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID, currID, siteid;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            BindPriceBandList();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                BindPriceBandList();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    BindPriceBandForEdit(Convert.ToInt32(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _siteID;
            }

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                BindPriceBandList();
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindPriceBandList()
        {
            try
            {
                grPriceBand.DataSource = _objPrice.GetPriceBandList().Where(t => t.ID != 1);
                grPriceBand.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindPriceBandForEdit(int id)
        {
            try
            {
                var result = _objPrice.GetPriceBandById(id);
                if (result != null)
                {
                    txtName.Text = result.Name;
                    txtNotes.InnerHtml = result.Notes;
                    chkActive.Checked = result.IsActive;
                    btnSubmit.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AddEditPriceBand();
        }

        void AddEditPriceBand()
        {
            try
            {
                var id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : Convert.ToInt32(Request.QueryString["id"]);
                _objPrice.AddEditPriceBand(new tblPriceBand
                {
                    ID = id,
                    Name = txtName.Text.Trim(),
                    Notes = txtNotes.InnerHtml.Trim(),
                    IsActive = chkActive.Checked, 
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = AdminuserInfo.UserID,
                    ModifiedOn = DateTime.Now
                });
                ShowMessage(1, !String.IsNullOrEmpty(Request.QueryString["id"]) ? "Price Band updated successfully." : "Price Band added successfully.");
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ClearControls();
                Tab = "1";
                ViewState["tab"] = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void ClearControls()
        {
            txtName.Text = string.Empty;
            txtNotes.InnerHtml = string.Empty;
            chkActive.Checked = false;
        }

        protected void grPriceBand_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    var id = Convert.ToInt32(e.CommandArgument.ToString());
                    var res = _objPrice.DeletePriceBand(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    BindPriceBandList();
                }
                else if (e.CommandName == "ActiveInActive")
                {
                    var id = Convert.ToInt32(e.CommandArgument.ToString());
                    _objPrice.ActiveInactivePriceBand(id);
                    BindPriceBandList();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("PriceBand.aspx");
        }
    }
}