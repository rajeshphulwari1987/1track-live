﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using IR_Admin.OneHubServiceRef;
using Currency = Business.Currency;

namespace IR_Admin
{
    public partial class OgoneSetting : Page
    {
        readonly Masters _Master = new Masters();
        readonly private ManageBooking _masterBook = new ManageBooking();
        db_1TrackEntities db = new db_1TrackEntities();
        public string tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            BindGrid(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";

            if (Request["id"] != null)
            {
                tab = "2";
                if (ViewState["tab"] != null)
                {
                    tab = ViewState["tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                BindSite();
                BindGrid(_siteID);
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    tab = "2";
                    GetOgoneEdit();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void BindSite()
        {
            //Site
            ddlSite.DataSource = _Master.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));

            //Currency
            ddlcurrency.DataSource = _Master.GetCurrencyList().Where(x => x.IsActive == true);
            ddlcurrency.DataTextField = "Name";
            ddlcurrency.DataValueField = "ShortCode";
            ddlcurrency.DataBind();
            ddlcurrency.Items.Insert(0, new ListItem("--Currency--", "-1"));

        }

        void BindGrid(Guid _SiteID)
        {
            tab = "1";
            grdOgone.DataSource = _Master.GetOgoneList(_SiteID);
            grdOgone.DataBind();
        }


        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddOgoneSetting();
                else
                    UpdateOgoneSetting();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        public static string GetluangageName(string code)
        {
            switch (code)
            {
                case "en_gb":
                    return "English";

                case "fr":
                    return "French";

                case "nl":
                    return "Dutch";

                case "it":
                    return "Italian";

                case "de":
                    return "German";

                default:
                    return "English";
            }
        }

        void AddOgoneSetting()
        {
            try
            {
                _Master.AddOgoneSetting(new tblOgoneMst
                {
                    ID = Guid.NewGuid(),
                    PSPID = txtPspid.Text.Trim(),
                    OgoneID = txtOgoneID.Text.Trim(),
                    UserName = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    OgoneURL = txtOgoneURL.Text.Trim(),
                    TokenizationURL = txtOgoneTokenizationURL.Text.Trim(),
                    ReturnURLAccept = txtReturnURLAccept.Text.Trim(),
                    ReturnURLDecline = txtReturnURLDecline.Text.Trim(),
                    ReturnURLException = txtReturnURLException.Text.Trim(),
                    OgoneShaInPassPhrase = txtOgoneShaInPassPhrase.Text.Trim(),
                    OgoneShaInPassPhraseTokenization = txtOgoneShaInPassPhraseTokenization.Text.Trim(),
                    Language = ddllanguange.SelectedValue,
                    Currency = ddlcurrency.SelectedValue,
                    CreatedBy = AdminuserInfo.UserID,
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    IsActive = chkactive.Checked,
                    IsEnableThreeD = chk3DSecure.Checked
                });
                Response.Redirect("ogoneSetting.aspx");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message + ex.InnerException);
            }
        }

        void UpdateOgoneSetting()
        {
            try
            {
                Guid id = Guid.Parse(Request["id"]);
                var objOgone = new tblOgoneMst
                {
                    ID = id,
                    PSPID = txtPspid.Text.Trim(),
                    OgoneID = txtOgoneID.Text.Trim(),
                    UserName = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    OgoneURL = txtOgoneURL.Text.Trim(),
                    TokenizationURL = txtOgoneTokenizationURL.Text.Trim(),
                    ReturnURLAccept = txtReturnURLAccept.Text.Trim(),
                    ReturnURLDecline = txtReturnURLDecline.Text.Trim(),
                    ReturnURLException = txtReturnURLException.Text.Trim(),
                    OgoneShaInPassPhrase = txtOgoneShaInPassPhrase.Text.Trim(),
                    OgoneShaInPassPhraseTokenization = txtOgoneShaInPassPhraseTokenization.Text.Trim(),
                    Language = ddllanguange.SelectedValue,
                    Currency = ddlcurrency.SelectedValue,
                    SiteID = Guid.Parse(ddlSite.SelectedValue),
                    ModifiedBy = AdminuserInfo.UserID,
                    IsActive = chkactive.Checked,
                    IsEnableThreeD = chk3DSecure.Checked,

                };

                _Master.UpdateOgone(objOgone);
                ShowMessage(1, "You have successfully updated Ogone Settings.");

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void GetOgoneEdit()
        {
            if (Request.QueryString["id"] != null)
            {
                Guid id = Guid.Parse(Request.QueryString["id"]);

                tblOgoneMst oOgone = _Master.GetOgoneEdit(id);
                if (oOgone != null)
                {
                    txtOgoneID.Text = oOgone.OgoneID;
                    txtUserName.Text = oOgone.UserName;
                    txtPassword.Text = oOgone.Password;
                    txtOgoneURL.Text = oOgone.OgoneURL;
                    txtOgoneTokenizationURL.Text = oOgone.TokenizationURL;
                    txtReturnURLAccept.Text = oOgone.ReturnURLAccept;
                    txtReturnURLDecline.Text = oOgone.ReturnURLDecline;
                    txtReturnURLException.Text = oOgone.ReturnURLException;
                    txtOgoneShaInPassPhrase.Text = oOgone.OgoneShaInPassPhrase;
                    txtOgoneShaInPassPhraseTokenization.Text = oOgone.OgoneShaInPassPhraseTokenization;
                    ddlcurrency.SelectedValue = oOgone.Currency;
                    ddllanguange.SelectedValue = oOgone.Language;
                    ddlSite.SelectedValue = oOgone.SiteID.ToString();
                    txtPspid.Text = oOgone.PSPID;
                    chk3DSecure.Checked = oOgone.IsEnableThreeD;
                    if (oOgone.IsActive != null) chkactive.Checked = (bool)oOgone.IsActive;
                }

                btnSubmit.Text = "Update";
                ddlSite.Enabled = false;

            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ogoneSetting.aspx");
        }


        protected void grdOgone_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    _Master.ActiveInactiveOgone(id);
                    _siteID = Master.SiteID;
                    BindGrid(_siteID);
                }

                if (e.CommandName == "Remove")
                {
                    var id = Guid.Parse(e.CommandArgument.ToString());
                    var res = _Master.DeleteOgone(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    BindGrid(_siteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }


    }

}
