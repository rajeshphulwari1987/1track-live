﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class EmailSettings : Page
    {
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            EmailSettingsList();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["edit"] != null)
            {
                Tab = "2";
                if (ViewState["Tab"] != null)
                    Tab = ViewState["Tab"].ToString();
            }

            if (!Page.IsPostBack)
            {
                BindSite();
                _siteID = Master.SiteID;
                SiteSelected();
                EmailSettingsList();
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindListForEdit(Guid.Parse(Request["edit"]));
                    BindLookupforEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                EmailSettingsList();
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindSite()
        {
            IEnumerable<tblSite> objSite = _master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        private void EmailSettingsList()
        {
            var result = _master.GetEmailSetting();
            grdSettings.DataSource = result.ToList();
            grdSettings.DataBind();
        }

        public void BindListForEdit(Guid id)
        {
            try
            {
                var result = _master.GetEmailSettingListEdit(id);
                txtSmtpHost.Text = result.SmtpHost;
                txtSmtpUser.Text = result.SmtpUser;
                txtSmtpPass.Text = result.SmtpPass;
                txtSmtpPort.Text = result.SmtpPort;
                txtEmail.Text = result.Email;

                btnSubmit.Text = "Update";
                chkActive.Checked = Convert.ToBoolean(result.IsActive);
                chkSSl.Checked = Convert.ToBoolean(result.EnableSsl);
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindLookupforEdit(Guid id)
        {
            var lookUpSites = _db.tblEmailSettingLookups.Where(x => x.SettingID == id).ToList();
            foreach (var lsites in lookUpSites)
            {
                foreach (TreeNode pitem in trSites.Nodes)
                {
                    if (Guid.Parse(pitem.Value) == lsites.SiteID)
                        pitem.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int tn = trSites.CheckedNodes.Count;
                if (tn == 0)
                {
                    ShowMessage(2, "Please Select Site");
                    Tab = "2";
                    return;
                }
                else
                {
                    var sid = _master.AddEditEmailSetting(new tblEmailSetting
                        {
                            ID = string.IsNullOrEmpty(Request["edit"]) ? new Guid() : Guid.Parse(Request["edit"]),
                            SmtpHost = txtSmtpHost.Text.Trim(),
                            SmtpUser = txtSmtpUser.Text.Trim(),
                            SmtpPass = txtSmtpPass.Text.Trim(),
                            SmtpPort = txtSmtpPort.Text.Trim(),
                            Email = txtEmail.Text.Trim(),
                            IsActive = chkActive.Checked,
                            EnableSsl = chkSSl.Checked
                        });

                    var lstSiteLookup = new List<tblEmailSettingLookup>();
                    foreach (TreeNode node in trSites.Nodes)
                    {
                        if (node.Checked)
                        {
                            lstSiteLookup.Add(new tblEmailSettingLookup
                                {
                                    SiteID = Guid.Parse(node.Value),
                                    SettingID = sid
                                });
                        }
                    }
                    if (lstSiteLookup.Count > 0)
                        _master.AddEmailSettingLookupSites(lstSiteLookup);
                    ShowMessage(1, string.IsNullOrEmpty(Request["edit"]) ? "Email Settings added successfully." : "Email Settings updated successfully.");
                    Tab = "2";
                    ViewState["Tab"] = "2";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("EmailSettings.aspx");
        }

        protected void grdSettings_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "ActiveInActive")
                {
                    _master.ActiveInactiveEmailSettings(id);
                    Tab = "1";
                    ViewState["Tab"] = "1";
                    EmailSettingsList();
                }

                if (e.CommandName == "Modify")
                {
                    Tab = "2";
                    ViewState["Tab"] = "2";
                    Response.Redirect("EmailSettings.aspx?edit=" + id);
                }

                if (e.CommandName == "Remove")
                {
                    var res = _master.DeleteEmailSettings(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _siteID = Master.SiteID;
                    SiteSelected();

                    Tab = "1";
                    ViewState["Tab"] = "1";
                    EmailSettingsList();
                }
            }
            catch (Exception ee)
            {
                ShowMessage(1, ee.ToString());
            }
        }
    }
}