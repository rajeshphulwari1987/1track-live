﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin
{
    public partial class LanguagePage : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        public string tab ="1";
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if(ViewState["tab"]!=null)
            {
            tab=ViewState["tab"].ToString();
            
            }
            if (!Page.IsPostBack)
            {
                ViewState["tab"] = tab;
                
                BindLanguagesList();
            }
        }

        public void BindLanguagesList()
        {
            try
            {
                grdLanguage.DataSource = _Master.GetLanguangesList();
                grdLanguage.DataBind();
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindLanguageListForEdit(Guid ID)
        {
            try
            {
                var result = _Master.GetLanguageListEdit(ID);
                txtLName.Text = result.Name;
                txtICode.Text = result.Logo;
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive) ? true : false;

            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(hdnID.Value.ToString()))
                {

                    Languages _Languages = new Languages();
                    _Languages.Name = txtLName.Text;
                    _Languages.ID = Guid.NewGuid();
                    _Languages.IsActive = chkactive.Checked == true ? true : false;
                    _Languages.Logo = txtICode.Text;
                    int res = _Master.AddLanguage(_Languages);
                    if (res > 0)
                    {
                        ShowMessage(1, "You have successfully Added.");
                        txtLName.Text = string.Empty;
                        txtICode.Text = string.Empty;
                        BindLanguagesList();
                    }
                }
                else 
                {

                    Languages _Languages = new Languages();
                    _Languages.Name = txtLName.Text;
                    _Languages.ID = Guid.Parse(hdnID.Value.ToString());
                    _Languages.IsActive = chkactive.Checked == true ? true : false;
                    _Languages.Logo = txtICode.Text;

                    int res = _Master.UpdateLanguage(_Languages);
                    if (res > 0)
                    {
                        ShowMessage(1, "You have successfully Updated.");
                        txtLName.Text = string.Empty;
                        txtICode.Text = string.Empty;
                        BindLanguagesList();
                        //Response.Redirect("languages.aspx");
                    }
                }
                tab = "1";
                ViewState["tab"] = "1";

            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("languages.aspx");
        }

        protected void grdLanguage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid lngID = string.IsNullOrEmpty(e.CommandArgument.ToString()) ? Guid.NewGuid() : Guid.Parse(e.CommandArgument.ToString());
            DivError.Style.Add("display", "none");
            DivSuccess.Style.Add("display", "none");
            switch (e.CommandName)
            {
                case "Modify":
                             {
                                 tab = "2";
                                 ViewState["tab"] = "2";
                                 hdnID.Value = lngID.ToString();
                                 BindLanguageListForEdit(lngID);
                                break;
                             }

                case "ActiveInActive":
                               {
                                   _Master.ActiveInactiveLanguage(lngID);
                                   Response.Redirect("languages.aspx");

                                        break;
                               }
            
            
            }
        
        
        }

    }
}
