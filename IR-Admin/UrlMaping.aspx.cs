﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class UrlMaping : Page
    {
        readonly Masters _oM = new Masters();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
            FillDetail();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FillDetail();
        }

        private void FillDetail()
        {
            grvURL.DataSource= _oM.GetUrlMaping();
            grvURL.DataBind();
        }

        protected void imgDelete_OnCommand(object sender, CommandEventArgs e)
        {
            var id = Convert.ToInt32(e.CommandArgument);
            _oM.DeleteURL(id);
            FillDetail();
        }

        protected void imgActive_OnCommand(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            _oM.ChangeStatus(id);
            FillDetail();
        }

        protected void grvURL_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvURL.PageIndex = e.NewPageIndex;
            FillDetail();
        }
    }
}