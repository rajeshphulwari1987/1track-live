﻿<%@ Page Title="P2P setting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="OrderMailTemplates.aspx.cs" Inherits="IR_Admin.OrderMailTemplates" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
            if(<%=tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {
                $("ul.tabs").tabs("div.inner-tabs-container > div");               
            }         
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
            document.getElementById('MainContent_divNew1').style.display = 'Block';
        }
    
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= chksite.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
    <style type="text/css">
        .colum-two
        {
            width: 75% !important;
            margin-top: 8px !important;
        }
        .colum-one
        {
            margin-left: 15px !important;
            margin-top: 8px !important;
            height: 35px !important;
        }
        .heading
        {
            width: 962px !important;
        }
        .newcol
        {
            border-bottom: 1px dashed #b1b1b1;
            line-height: 35px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Order Mail Templates
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="OrderMailTemplates.aspx" class="current">List</a></li>
            <li><a id="aNew" href="OrderMailTemplates.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdMailTemplete" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" PageSize="10"
                            AllowPaging="true" OnRowCommand="grdMailTemplete_RowCommand" OnPageIndexChanging="grdMailTemplete_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="28%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Template">
                                    <ItemTemplate>
                                        <%#Eval("MailTemplateName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="28%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("SiteId")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                        <a href='<%#Eval("templateUrl")%>' title="mail" target="_blank">
                                            <img src="../images/view.png" title="View" height="18px;"></a>
                                    </ItemTemplate>
                                    <ItemStyle Width="2%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <asp:HiddenField ID="hdnId" runat="server" />
                <div id="divNew" runat="server" style="display: block;">
                    <div class="content-in">
                        <div class="colum-one newcol">
                            Select Template:
                        </div>
                        <div class="colum-two newcol">
                            <asp:DropDownList ID="ddltemplete" runat="server" OnSelectedIndexChanged="ddltemplete_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*"
                                ErrorMessage="Select template." ForeColor="Red" ControlToValidate="ddltemplete"
                                ValidationGroup="rvSave" InitialValue="0" />
                        </div>
                        <div class="clrup">
                        </div>
                         <div class="colum-one">
                            Select Site:
                        </div>
                        <div class="colum-two">
                            <div>
                                <asp:CheckBoxList ID="chksite" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" style="line-height:30px"/>
                                <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList"
                                    ValidationGroup="rvSave" ForeColor="Red" ErrorMessage="Select atleast one site."
                                    Text="Select atleast one site."></asp:CustomValidator>
                            </div>
                        </div>
                        <div class="clrup">
                        </div>
                        <div class="colum-one">
                            &nbsp;</div>
                        <div class="colum-two" style="margin-bottom: 12px;">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="rvSave" />
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="rvSave" DisplayMode="List"
                                ShowSummary="false" ShowMessageBox="true" runat="server" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
