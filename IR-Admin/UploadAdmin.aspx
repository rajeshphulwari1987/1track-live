﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadAdmin.aspx.cs" Inherits="IR_Admin.UploadAdmin" %>
 <%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script> 
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes" >
                <div id="divNew" runat="server" style="display: block;">
                    <div class="heading">
                        Upload Users
                    </div>
                    <div class="content-in">
                        <div class="colum-one">
                            Upload User File
                        </div>
                        <div class="colum-two">
                            <asp:FileUpload ID="fupUser" runat="server" />
                        </div>
                        <div class="clrup">
                        </div>
                         <div class="colum-one">
                            &nbsp;</div>
                        <div class="colum-two">
                            <div class="file-container" style="padding:33px">
                                <b>File Format:</b> .XLS,.XLSX
                            </div>
                            <div class="file-container" style="width: 200px; height: 80px;">
                                <a href="Uploaded/UserFile/UserUploadSample.xlsx">
                                    <img src="images/product-temp.png" /></br> Download User Upload Sample
                                </a>
                            </div>
                        </div
                        <div class="clrup">
                        </div>
                        <div class="colum-one">
                            &nbsp;</div>
                        <div class="colum-two">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="submit" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

