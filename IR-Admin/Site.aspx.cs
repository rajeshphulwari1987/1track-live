﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web.Services;

namespace IR_Admin
{
    public partial class SitePage : Page
    {
        readonly Masters _master = new Masters();
        public string tab = string.Empty;
        string SiteLogoPath = string.Empty;
        string SiteFaviconPath = string.Empty;
        string AttentionImg = string.Empty;
        List<SiteCurrency> currencylist = null;
        List<Guid> currencyids = new List<Guid>();
        private readonly db_1TrackEntities _db = new db_1TrackEntities();
        private ManagePrintQueue _ManagePrintQueue = new ManagePrintQueue();
        Guid _siteId = Guid.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            txtAttentionMsg.Attributes.Add("maxlength", "300");
            tab = "1";
            if ((Request["edit"] != null) && (Request["edit"] != ""))
            {
                tab = "2";
                _siteId = Guid.Parse(Request["edit"]);
            }
            if (!Page.IsPostBack)
            {
                for (int i = 0; i <= 23; i++)
                {
                    ddlGmtTimeZoneHour.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                for (int i = 0; i <= 59; i++)
                {
                    ddlGmtTimeZoneMinute.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                FillCommonddl();
                BindSiteList();
                FillPrintersList();
                if (_siteId != Guid.Empty)
                {
                    tab = "2";
                    BindSiteListForEdit(_siteId);
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void BindSiteList()
        {
            try
            {
                grdSites.DataSource = _master.Branchlist().Where(a => a.IsDelete == false).OrderBy(x => x.DisplayName).ToList();
                grdSites.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void FillCommonddl()
        {
            try
            {
                for (int j = 0; j <= 10; j++)
                    ddlBookingDayLimit.Items.Insert(0, new ListItem(j.ToString(), j.ToString()));

                //Country Dropdown List
                ddlCountry.DataSource = _master.GetCountryList();
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("---Country---", "0"));

                //Language Dropdown List
                ddlLanguage.DataSource = _master.GetLanguangesList();
                ddlLanguage.DataTextField = "Name";
                ddlLanguage.DataValueField = "ID";
                ddlLanguage.DataBind();
                ddlLanguage.Items.Insert(0, new ListItem("---Language---", "0"));

                //Currency Dropdown List
                ddlCurrency.DataSource = _master.GetCurrencyList();
                ddlCurrency.DataTextField = "Name";
                ddlCurrency.DataValueField = "ID";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, new ListItem("---Currency---", "0"));

                rptCurrency.DataSource = _master.GetCurrencyList();
                rptCurrency.DataBind();

                //Theme Dropdown List
                ddlTheme.DataSource = _master.GetThemeList();
                ddlTheme.DataTextField = "ThemeName";
                ddlTheme.DataValueField = "ID";
                ddlTheme.DataBind();
                ddlTheme.Items.Insert(0, new ListItem("---Theme---", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindSiteListForEdit(Guid id)
        {
            try
            {
                currencylist = _master.GetCurrencyBySiteId(id);
                if (currencylist != null)
                    foreach (RepeaterItem Item in rptCurrency.Items)
                    {
                        if (Item.ItemType == ListItemType.AlternatingItem || Item.ItemType == ListItemType.Item)
                        {
                            CheckBox chkCurrency = Item.FindControl("chkCurrency") as CheckBox;
                            HiddenField hdnCurrencyID = Item.FindControl("hdnCurrencyID") as HiddenField;
                            chkCurrency.Checked = currencylist.Any(x => x.CurrencyID == Guid.Parse(hdnCurrencyID.Value));
                        }
                    }


                var result = _master.GetSiteListEdit(id);
                DefaultPrinterSection.Visible = result.IsAgent.HasValue ? !result.IsAgent.Value : true;
                BGColor.Text = result.BGColor;
                BDRColor.Text = result.BDRColor;
                txtAttentionTitle.Text = result.AttentionTitle;
                txtAttentionMsg.Text = result.AttentionMsg;
                imgAttengionimg.Attributes.Add("src", string.IsNullOrEmpty(result.AttentionImg) ? "images/Seasonal.jpg" : result.AttentionImg);
                txtSName.Text = result.DisplayName;
                if (ddlCountry.Items.FindByValue(result.DefaultCountryID.ToString()) != null)
                {
                    ddlCountry.Items.FindByValue(result.DefaultCountryID.ToString()).Selected = true;
                }
                if (ddlCurrency.Items.FindByValue(result.DefaultCurrencyID.ToString()) != null)
                {
                    ddlCurrency.Items.FindByValue(result.DefaultCurrencyID.ToString()).Selected = true;
                }
                if (ddlLanguage.Items.FindByValue(result.DefaultLanguageID.ToString()) != null)
                {
                    ddlLanguage.Items.FindByValue(result.DefaultLanguageID.ToString()).Selected = true;
                }
                if (ddlTheme.Items.FindByValue(result.ThemeID.ToString()) != null)
                {
                    ddlTheme.Items.FindByValue(result.ThemeID.ToString()).Selected = true;
                }
                txtSiteAlias.Text = result.SiteAlias;
                txtOrderPrefixName.Text = result.OrderPrefix;
                txtjourneyEmail.Text = result.JourneyEmail;
                txtBccEmail.Text = result.BccEmail;
                chkIsBccEmail.Checked = Convert.ToBoolean(result.IsBccEmail);
                txtSiteUrl.Text = result.SiteURL;
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
                chkIsSTA.Checked = Convert.ToBoolean(result.IsSTA);
                ChkAgent.Checked = Convert.ToBoolean(result.IsAgent);
                ChkAttentionVisible.Checked = Convert.ToBoolean(result.IsAttentionVisible);

                chkNewsLetter.Checked = Convert.ToBoolean(result.IsVisibleNewsLetter);
                chkJRF.Checked = Convert.ToBoolean(result.IsVisibleJRF);
                chkGI.Checked = Convert.ToBoolean(result.IsVisibleGeneralInfo);
                chkRtImg.Checked = Convert.ToBoolean(result.IsVisibleImages);

                chkWholesale.Checked = result.IsWholeSale;
                if (result.IsTopJourney != null)
                    if ((bool)result.IsTopJourney)
                        rdbtnDefaultTab.SelectedValue = "1";
                    else
                        rdbtnDefaultTab.SelectedValue = "0";
                chkIsUsSIte.Checked = (bool)result.IsUS;
                chkHavRailPass.Checked = Convert.ToBoolean(result.HavRailPass);
                chkEnableP2P.Checked = Convert.ToBoolean(result.EnableP2P);
                chkEnableP2PWidget.Checked = Convert.ToBoolean(result.IsVisibleP2PWidget);
                chktrainticket.Checked = result.IsTrainTickets;
                chkContactEmail.Checked = Convert.ToBoolean(result.EnableContactEmail);
                chkTicketProtection.Checked = Convert.ToBoolean(result.EnableTicketProtection);
                ddlBookingDayLimit.SelectedValue = result.BookingDayLimit.ToString();
                txtPhoneNumber.Text = result.PhoneNumber;
                txtSiteHeading.Text = result.SiteHeading;
                txtSiteHeadingTitle.Text = result.SiteHeadingTitle;
                txtAnalytic.InnerHtml = result.GoogleAnalytics;
                txtManager.InnerHtml = result.GoogleManager;
                chkIdCorporate.Checked = Convert.ToBoolean(result.IsCorporate);
                rdnDateFormat.SelectedValue = result.DateFormat.ToString();
                rdnPaymentType.SelectedValue = result.PaymentType.ToString();
                ddlLayoutType.SelectedValue = result.LayoutType.ToString();
                if (!string.IsNullOrEmpty(result.GmtTimeZone) && result.GmtTimeZone.Contains(":") && result.GmtTimeZone.Split(':').Length >= 3)
                {
                    ddlGmtTimeZoneSIgn.SelectedValue = result.GmtTimeZone.Split(':')[0];
                    ddlGmtTimeZoneHour.SelectedValue = result.GmtTimeZone.Split(':')[1];
                    ddlGmtTimeZoneMinute.SelectedValue = result.GmtTimeZone.Split(':')[2];
                }
                chkAffiliateTracking.Checked = Convert.ToBoolean(result.EnableAffiliateTracking);
                if (!string.IsNullOrEmpty(result.LogoPath))
                    imgSiteLogo.ImageUrl = result.LogoPath;
                if (!string.IsNullOrEmpty(result.FaviconPath))
                    imgSiteFavicon.ImageUrl = result.FaviconPath;
                btnSubmit.Text = "Update";
                chkEnableCookie.Checked = result.IsEnableCookie;

                ddlEurailPrinter.SelectedValue = result.EurailPrinter.HasValue ? result.EurailPrinter.Value.ToString() : "0";
                ddlBritrailPrinter.SelectedValue = result.BritrailPrinter.HasValue ? result.BritrailPrinter.Value.ToString() : "0";
                ddlInterrailPrinter.SelectedValue = result.InterrailPrinter.HasValue ? result.InterrailPrinter.Value.ToString() : "0";
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var _Sites = new Sites();
                if (AttentionUpdate())
                {
                    UploadFile();

                    if (btnSubmit.Text == "Submit")
                    {
                        _Sites.DisplayName = txtSName.Text;
                        _Sites.ID = Guid.NewGuid();
                        _Sites.IsActive = chkactive.Checked;
                        _Sites.DefaultCountryID = ddlCountry.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlCountry.SelectedItem.Value);
                        _Sites.DefaultCurrencyID = ddlCurrency.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlCurrency.SelectedItem.Value);
                        _Sites.DefaultLanguageID = ddlLanguage.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlLanguage.SelectedItem.Value);
                        _Sites.ThemeID = ddlTheme.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlTheme.SelectedItem.Value);
                        _Sites.SiteURL = txtSiteUrl.Text;
                        _Sites.IsSTA = chkIsSTA.Checked;
                        _Sites.IsAgent = ChkAgent.Checked;
                        _Sites.IsAttentionVisible = ChkAttentionVisible.Checked;
                        _Sites.IsUS = chkIsUsSIte.Checked;
                        _Sites.IsVisibleNewsLetter = chkNewsLetter.Checked;
                        _Sites.IsVisibleJRF = chkJRF.Checked;
                        _Sites.IsVisibleGeneralInfo = chkGI.Checked;
                        _Sites.IsVisibleImages = chkRtImg.Checked;
                        _Sites.IsWholeSale = chkWholesale.Checked;
                        _Sites.PhoneNumber = txtPhoneNumber.Text;
                        _Sites.SiteAlias = txtSiteAlias.Text;
                        _Sites.OrderPrefix = txtOrderPrefixName.Text;
                        _Sites.SiteHeading = txtSiteHeading.Text;
                        _Sites.SiteHeadingTitle = txtSiteHeadingTitle.Text;
                        _Sites.BookingDayLimit = Convert.ToInt32(ddlBookingDayLimit.SelectedValue);
                        if (rdbtnDefaultTab.SelectedValue == "1")
                            _Sites.IsTopJourney = true;
                        else
                            _Sites.IsTopJourney = false;
                        _Sites.IsDelete = false;
                        _Sites.HavRailPass = chkHavRailPass.Checked;
                        _Sites.EnableP2P = chkEnableP2P.Checked;
                        _Sites.IsVisibleP2PWidget = chkEnableP2PWidget.Checked;
                        _Sites.IsTrainTickets = chktrainticket.Checked;
                        _Sites.EnableContactEmail = chkContactEmail.Checked;
                        _Sites.EnableTicketProtection = chkTicketProtection.Checked;
                        _Sites.JourneyEmail = txtjourneyEmail.Text;
                        _Sites.BccEmail = txtBccEmail.Text;
                        _Sites.IsBccEmail = chkIsBccEmail.Checked;
                        _Sites.GoogleAnalytics = txtAnalytic.InnerHtml.Trim();
                        _Sites.GoogleManager = txtManager.InnerHtml.Trim();
                        _Sites.IsCorporate = chkIdCorporate.Checked;
                        _Sites.DateFormat = Convert.ToByte(rdnDateFormat.SelectedValue);
                        _Sites.LogoPath = SiteLogoPath.Replace("~/", "");  //site logo
                        _Sites.FaviconPath = SiteFaviconPath.Replace("~/", "");  //site favicon
                        _Sites.EnableAffiliateTracking = chkAffiliateTracking.Checked;
                        _Sites.PaymentType = Convert.ToByte(rdnPaymentType.SelectedValue);
                        _Sites.LayoutType = Convert.ToInt32(ddlLayoutType.SelectedValue);
                        _Sites.GmtTimeZone = ddlGmtTimeZoneSIgn.SelectedValue + ":" + ddlGmtTimeZoneHour.SelectedValue + ":" + ddlGmtTimeZoneMinute.SelectedValue;
                        _Sites.IsEnableCookie = chkEnableCookie.Checked;

                        if (ddlEurailPrinter.SelectedValue != "0")
                            _Sites.EurailPrinter = Guid.Parse(ddlEurailPrinter.SelectedValue);
                        if (ddlBritrailPrinter.SelectedValue != "0")
                            _Sites.BritrailPrinter = Guid.Parse(ddlBritrailPrinter.SelectedValue);
                        if (ddlInterrailPrinter.SelectedValue != "0")
                            _Sites.InterrailPrinter = Guid.Parse(ddlInterrailPrinter.SelectedValue);
                        var ID = _master.AddSite(_Sites);
                        if (ID != null)
                        {
                            if (ID != Guid.Empty)
                            {
                                AddSitePges(ID);
                                var adminID = AdminuserInfo.UserID;
                                var isAdmin = _master.RoleIsAdmin(adminID);
                                if (isAdmin)
                                {
                                    AddAdminUserLookupSites(adminID, ID);
                                }
                                ShowMessage(1, "You have successfully Added.");
                                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                                BindSiteList();
                            }
                            else
                                ShowMessage(2, "Url already exists");
                        }
                    }
                    else if (btnSubmit.Text == "Update")
                    {
                        _Sites.DisplayName = txtSName.Text;
                        _Sites.ID = Guid.Parse(Request["edit"]);
                        _Sites.BookingDayLimit = Convert.ToInt32(ddlBookingDayLimit.SelectedValue);
                        _Sites.IsActive = chkactive.Checked;
                        _Sites.IsWholeSale = chkWholesale.Checked;
                        _Sites.IsUS = chkIsUsSIte.Checked;
                        _Sites.DefaultCountryID = ddlCountry.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlCountry.SelectedItem.Value);
                        _Sites.DefaultCurrencyID = ddlCurrency.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlCurrency.SelectedItem.Value);
                        _Sites.DefaultLanguageID = ddlLanguage.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlLanguage.SelectedItem.Value);
                        _Sites.ThemeID = ddlTheme.SelectedIndex == 0 ? Guid.Empty : Guid.Parse(ddlTheme.SelectedItem.Value);
                        _Sites.SiteURL = txtSiteUrl.Text;
                        _Sites.IsSTA = chkIsSTA.Checked;
                        _Sites.IsVisibleNewsLetter = chkNewsLetter.Checked;
                        _Sites.IsVisibleJRF = chkJRF.Checked;
                        _Sites.IsVisibleGeneralInfo = chkGI.Checked;
                        _Sites.IsVisibleImages = chkRtImg.Checked;
                        _Sites.SiteAlias = txtSiteAlias.Text;
                        _Sites.OrderPrefix = txtOrderPrefixName.Text;
                        _Sites.IsAgent = ChkAgent.Checked;
                        _Sites.IsAttentionVisible = ChkAttentionVisible.Checked;
                        _Sites.PhoneNumber = txtPhoneNumber.Text;
                        _Sites.JourneyEmail = txtjourneyEmail.Text;
                        _Sites.BccEmail = txtBccEmail.Text;
                        _Sites.IsBccEmail = chkIsBccEmail.Checked;

                        if (rdbtnDefaultTab.SelectedValue == "1")
                            _Sites.IsTopJourney = true;
                        else
                            _Sites.IsTopJourney = false;
                        _Sites.IsDelete = false;
                        _Sites.HavRailPass = chkHavRailPass.Checked;
                        _Sites.EnableP2P = chkEnableP2P.Checked;
                        _Sites.IsVisibleP2PWidget = chkEnableP2PWidget.Checked;
                        _Sites.IsTrainTickets = chktrainticket.Checked;
                        _Sites.EnableContactEmail = chkContactEmail.Checked;
                        _Sites.EnableTicketProtection = chkTicketProtection.Checked;
                        _Sites.SiteHeading = txtSiteHeading.Text;
                        _Sites.SiteHeadingTitle = txtSiteHeadingTitle.Text;
                        _Sites.GoogleAnalytics = txtAnalytic.InnerHtml.Trim();
                        _Sites.GoogleManager = txtManager.InnerHtml.Trim();
                        _Sites.IsCorporate = chkIdCorporate.Checked;
                        _Sites.DateFormat = string.IsNullOrEmpty(rdnDateFormat.SelectedValue) ? Convert.ToByte("2") : Convert.ToByte(rdnDateFormat.SelectedValue);
                        _Sites.PaymentType = string.IsNullOrEmpty(rdnPaymentType.SelectedValue) ? Convert.ToByte("1") : Convert.ToByte(rdnPaymentType.SelectedValue);
                        _Sites.LayoutType = Convert.ToInt32(ddlLayoutType.SelectedValue);
                        _Sites.IsEnableCookie = chkEnableCookie.Checked;
                        _Sites.GmtTimeZone = ddlGmtTimeZoneSIgn.SelectedValue + ":" + ddlGmtTimeZoneHour.SelectedValue + ":" + ddlGmtTimeZoneMinute.SelectedValue;

                        if (ddlEurailPrinter.SelectedValue != "0")
                            _Sites.EurailPrinter = Guid.Parse(ddlEurailPrinter.SelectedValue);
                        if (ddlBritrailPrinter.SelectedValue != "0")
                            _Sites.BritrailPrinter = Guid.Parse(ddlBritrailPrinter.SelectedValue);
                        if (ddlInterrailPrinter.SelectedValue != "0")
                            _Sites.InterrailPrinter = Guid.Parse(ddlInterrailPrinter.SelectedValue);

                        if (upUploadLogo.HasFile)
                            _Sites.LogoPath = SiteLogoPath.Replace("~/", ""); //site logo

                        if (upUploadFavicon.HasFile)
                            _Sites.FaviconPath = SiteFaviconPath.Replace("~/", ""); //site logo

                        _Sites.EnableAffiliateTracking = chkAffiliateTracking.Checked;
                        var ID = _master.UpdateSite(_Sites);
                        if (ID != null)
                        {
                            AddSitePges(ID);
                            ShowMessage(1, "You have successfully Updated.");
                            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                            BindSiteList();
                        }
                    }

                    foreach (RepeaterItem item in rptCurrency.Items)
                    {
                        if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                        {
                            CheckBox chkCurrency = item.FindControl("chkCurrency") as CheckBox;
                            HiddenField hdnCurrencyID = item.FindControl("hdnCurrencyID") as HiddenField;
                            if (chkCurrency.Checked)
                            {
                                currencyids.Add(Guid.Parse(hdnCurrencyID.Value));
                            }
                        }
                    }
                    _master.AddEditSiteCurrency(_Sites.ID, currencyids);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void AddAdminUserLookupSites(Guid adminID, Guid siteID)
        {
            var adminUserLookSites = new AdminUserLookSites { ID = Guid.NewGuid(), AdminUserID = adminID, SiteID = siteID };
            _master.AddUserLookupSites(adminUserLookSites);
        }

        public void AddSitePges(Guid id)
        {
            if (_db.tblWebMenuSiteLookups.Any(x => x.SiteID == id))
                return;

            var tblWebMenu = _db.tblWebMenus.ToList();
            foreach (var item in tblWebMenu)
            {
                var masterlkUp = new tblWebMenuSiteLookup();
                masterlkUp.ID = Guid.NewGuid();
                masterlkUp.SiteID = id;
                masterlkUp.PageID = item.ID;
                _db.AddTotblWebMenuSiteLookups(masterlkUp);
                _db.SaveChanges();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Site.aspx");
        }

        protected void grdSites_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var siteID = Guid.Parse(e.CommandArgument.ToString());
            DivError.Style.Add("display", "none");
            DivSuccess.Style.Add("display", "none");
            switch (e.CommandName)
            {
                case "IsSTA":
                    {
                        _master.ChangeSTAStatus(siteID);
                        Response.Redirect("Site.aspx");
                        break;
                    }
                case "IsAgent":
                    {
                        _master.ChangeAgentStatus(siteID);
                        Response.Redirect("Site.aspx");
                        break;
                    }
                case "Modify":
                    {
                        Response.Redirect("Site.aspx?edit=" + siteID);
                        break;
                    }
                case "ActiveInActive":
                    {
                        _master.ActivateInActivateSite(siteID);
                        break;
                    }
                case "Remove":
                    {
                        _master.DeleteSite(siteID);
                        break;
                    }
            }
            Response.Redirect("Site.aspx");
        }

        protected void grdSites_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var imgDelete = e.Row.FindControl("imgDelete") as ImageButton;
                if (imgDelete != null)
                {
                    var siteID = Guid.Parse(imgDelete.CommandArgument);
                    var userID = AdminuserInfo.UserID;
                    var vi = new Masters().RoleDeleteHaveUser(userID, siteID);
                    if (vi)
                        imgDelete.Visible = true;
                    else
                        imgDelete.Visible = false;
                }
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (upUploadLogo.HasFile)
                {
                    if (upUploadLogo.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Site Logo is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = upUploadLogo.FileName.Substring(upUploadLogo.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    SiteLogoPath = "~/Uploaded/SiteLogos/";
                    SiteLogoPath = SiteLogoPath + oCom.CropImage(upUploadLogo, SiteLogoPath, 79, 220);
                }


                if (upUploadFavicon.HasFile)
                {
                    if (upUploadFavicon.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Site Favicon is larger up to 1Mb.')</script>", false);
                        return false;
                    }
                    string fileName = Path.GetExtension(upUploadFavicon.FileName);
                    fileName = Guid.NewGuid().ToString() + fileName;
                    upUploadFavicon.SaveAs(Server.MapPath("~/Uploaded/SitesFavicon/") + fileName);
                    SiteFaviconPath = "Uploaded/SitesFavicon/" + fileName;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnRemoveLogo_Click(object sender, EventArgs e)
        {
            var site = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
            if (site != null)
            {
                site.LogoPath = null;
                _db.SaveChanges();
                imgSiteLogo.ImageUrl = "~/CMSImages/sml-noimg.jpg";
            }
            tab = "2";
        }

        protected void btnRemoveFavicon_Click(object sender, EventArgs e)
        {
            var site = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
            if (site != null)
            {
                site.FaviconPath = null;
                _db.SaveChanges();
                imgSiteFavicon.ImageUrl = "~/CMSImages/sml-noimg.jpg";
            }
            tab = "2";
        }

        protected bool AttentionUpdate()
        {
            String alertmsg = string.Empty;
            bool result = true;
            if (StripHTML(txtAttentionTitle.Text) > 45)
            {
                alertmsg = "<script>alert('Attention title max length should be 40.')</script>";
                result = false;
            }
            if (StripHTML(txtAttentionMsg.Text) > 400 && result)
            {
                alertmsg = "<script>alert('Attention message max length should be 400.')</script>";
                result = false;
            }
            AttentionImg = "images/Seasonal.jpg";
            var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
            if (flpAttentionimg.HasFile && result)
            {
                if (flpAttentionimg.PostedFile.ContentLength > 524610)
                {
                    alertmsg = "<script>alert('Uploaded attention logo is larger up to 513kb.')</script>";
                    result = false;
                }
                string fileName = flpAttentionimg.FileName.Substring(flpAttentionimg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                if (!ext.Contains(fileName.ToUpper()) && result)
                {
                    alertmsg = "<script>alert('File format (attention logo) is not specified, Please try another format.')</script>";
                    result = false;
                }
                AttentionImg = "Uploaded/SiteLogos/Attention" + _siteId + fileName;
                flpAttentionimg.SaveAs(Server.MapPath("~/" + AttentionImg));
            }
            if (result)
                _master.UpdateAttentionMsg(_siteId, txtAttentionTitle.Text, txtAttentionMsg.Text, AttentionImg, BGColor.Text, BDRColor.Text);
            else
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", alertmsg, false);
                tab = "2";
            }
            return result;
        }

        public static int StripHTML(string input)
        {
            input = Regex.Replace(input, @"[\r\n]+", "<br/>");
            return Regex.Replace(input, "<.*?>", String.Empty).Trim().Length;
        }

        public void FillPrintersList()
        {
            ddlEurailPrinter.DataSource = ddlBritrailPrinter.DataSource = ddlInterrailPrinter.DataSource = _ManagePrintQueue.GetActiveStockListByAdminId(Guid.Parse("9899686a-6ec4-4b32-a813-d9287d7e03e1"));
            ddlEurailPrinter.DataValueField = ddlBritrailPrinter.DataValueField = ddlInterrailPrinter.DataValueField = "ID";
            ddlEurailPrinter.DataTextField = ddlBritrailPrinter.DataTextField = ddlInterrailPrinter.DataTextField = "BRANCHNAME";
            ddlEurailPrinter.DataBind(); ddlBritrailPrinter.DataBind(); ddlInterrailPrinter.DataBind();
            ddlEurailPrinter.Items.Insert(0, new ListItem("Select Default Printer", "0"));
            ddlBritrailPrinter.Items.Insert(0, new ListItem("Select Default Printer", "0"));
            ddlInterrailPrinter.Items.Insert(0, new ListItem("Select Default Printer", "0"));
        }

        [WebMethod]
        public static List<PrintQueueAgentList> AjaxPrintQueueAgentList(Guid SiteId, Guid PrintQueueId)
        {
            ManagePrintQueue _ManagePrintQueue = new ManagePrintQueue();
            return _ManagePrintQueue.GetPrintQueueAgentListByQueueId(SiteId, PrintQueueId);
        }

    }
}
