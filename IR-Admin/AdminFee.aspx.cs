﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class AdminFee : System.Web.UI.Page
    {
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public ManageAdminFee ManageAdmin = new ManageAdminFee();
        Guid _SiteID;

        #region [ Page InIt must write on every page of CMS ]
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                PageLoadEvent();
        }

        protected void PageLoadEvent()
        {
            try
            {
                grvAdminFee.DataSource = ManageAdmin.GetAdminFee();
                grvAdminFee.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void grvAdminFee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAdminFee.PageIndex = e.NewPageIndex;
            PageLoadEvent();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender;
                HiddenField hdnSiteId = btnUpdate.Parent.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnId = btnUpdate.Parent.FindControl("hdnId") as HiddenField;
                TextBox txtAdminFee = btnUpdate.Parent.FindControl("txtAdminFee") as TextBox;
                CheckBox chkIsPercent = btnUpdate.Parent.FindControl("chkIsPercent") as CheckBox;

                Guid Id = Guid.Parse(hdnId.Value);
                Guid SiteId = Guid.Parse(hdnSiteId.Value);
                decimal Adminfee = Convert.ToDecimal(string.IsNullOrEmpty(txtAdminFee.Text) ? "0" : txtAdminFee.Text);
                if (chkIsPercent.Checked && Adminfee > (decimal)99.99)
                {
                    ShowMessage(2, "Admin fee is not grater then 100%.");
                    return;
                }
                var Admin = new tblAdminFee
                {
                    ID = Id,
                    SiteId = SiteId,
                    AdminFee = Adminfee,
                    IsPercentage = chkIsPercent.Checked,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now
                };
                int reault = ManageAdmin.AddUpdateAdminFee(Admin);
                if (reault > 0)
                    ShowMessage(1, "Admin Fee update successfully.");
                PageLoadEvent();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void imgApplicable_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgApplicable = ((ImageButton)sender);
                HiddenField hdnId = imgApplicable.Parent.FindControl("hdnId") as HiddenField;
                Guid Id = Guid.Parse(hdnId.Value);
                ManageAdmin.UpdateIsApplicable(Id);
                PageLoadEvent();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }
    }

}