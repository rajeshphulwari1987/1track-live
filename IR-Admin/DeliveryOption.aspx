﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="DeliveryOption.aspx.cs" Inherits="IR_Admin.DeliveryOption" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }
 
        $(function () {                  
          if(<%=tab.ToString()%>=="1")   {
                $("ul.list").tabs("div.panes > div");
            }
        });
         
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            checkUncheckCountyTree();
        });

        function checkUncheckCountyTree() {
            var checkBoxSelector = '#<%=dtlCountry.ClientID%> input[id*="chkRegion"]:checkbox';
            $(checkBoxSelector).click(function () {
                var chkId = (this.id);
                var last = chkId.slice(-1);
                var idtree = "MainContent_dtlCountry_trSites_" + last;

                if (this.checked) {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', true);
                    });
                }
                else {
                    $('#' + idtree + ' > table > tbody > tr > td > input').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });
        }  
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Delivery Options
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="DeliveryOption.aspx" class="current">List</a></li>
            <li><a id="aNew" href="DeliveryOption.aspx" class=" ">New/Edit</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdDelivery" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdDelivery_RowCommand"
                            AllowPaging="True" OnPageIndexChanging="grdDelivery_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("Name")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="35%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Base Price">
                                    <ItemTemplate>
                                        <%#Eval("BasePrice")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site's Name">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active For Admin">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgActiveForAdmin" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" CommandName="ActiveInActiveForAdmin" AlternateText="status" ImageUrl='<%#Eval("IsActiveForAdmin").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActiveForAdmin").ToString()=="True" ?"Active For Admin Only":"In-Active For Admin Only" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="15%" VerticalAlign="Top" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="DeliveryOption.aspx?id=<%#Eval("Id")%>" title="Edit" style="text-decoration: none;">
                                            <img alt="edit" src="images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Delete" ToolTip="Delete"
                                            CommandArgument='<%#Eval("Id")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: block;">
                    <table class="tblMainSection">
                        <tr>
                            <td style="width: 70%; vertical-align: top;">
                                
                                <table width="100%">
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Name
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtName" runat="server" MaxLength="180" />
                                            <asp:RequiredFieldValidator ID="reqCat" runat="server" ControlToValidate="txtName"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Base Price
                                        </td>
                                        <td class="col">
                                            <asp:TextBox ID="txtBasePrice" runat="server" MaxLength="10" />
                                            <asp:RequiredFieldValidator ID="reqBasePrice" runat="server" ControlToValidate="txtBasePrice"
                                                CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                            <asp:FilteredTextBoxExtender ID="ftbPrice" TargetControlID="txtBasePrice" ValidChars=".0123456789"
                                                runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Active
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsActv" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            Active For Admin Only
                                        </td>
                                        <td class="col">
                                            <asp:CheckBox ID="chkIsActiveForAdmin" runat="server" />
                                        </td>
                                    </tr>
                                    
                                     <tr>
                                        <td style="padding-left: 5px;" class="col">
                                            <span style="color: #941E34; padding-top: 10px; font-weight: bold">Country </span>
                                        </td>
                                        <td class="col">
                                             
                                        </td>
                                    </tr>
                                </table>
                               
                             
                                <div class="cat-inner" style="min-height: 150px; max-height: 250px;margin: 0;padding-top: 0px !important; overflow-y: auto;">
                                    <fieldset class="grid-Region" style="width: 96%">
                                        <asp:DataList ID="dtlCountry" runat="server" RepeatColumns="2" Width="99%  " OnItemDataBound="dtlCountry_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRegion" runat="server" Font-Bold="True" Text='<%#Eval("RegionName") %>' />
                                                <asp:HiddenField runat="server" ID="hdnRegion" Value='<%#Eval("RegionID") %>' />
                                                <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                                </asp:TreeView>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </fieldset>
                                </div>
                                <div class="cat-outer" style="width: 99%; margin: 2% .5% .5% .5%; position: relative;">
                                    <span style="color: #941E34; line-height: 30px; font-weight: bold">Description </span>
                                    <textarea id="txtContent" runat="server"></textarea>
                                </div>
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                        <NodeStyle ChildNodesPadding="5px" />
                                    </asp:TreeView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="submit" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                            <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                            </td>
                        </tr>
                    </table>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
