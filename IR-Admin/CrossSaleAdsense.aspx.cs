﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class CrossSaleAdsense : System.Web.UI.Page
    {
        readonly Masters _Master = new Masters();
        db_1TrackEntities db = new db_1TrackEntities();
        readonly private Masters _oMasters = new Masters();
        public string tab = string.Empty;
        string cFlagImgpath = string.Empty;
        string cBannerpath = string.Empty;
        string cImagepath = string.Empty;
        public string Currency = "$";
        public Guid CurrencyID = Guid.Empty;
        private readonly ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
            CrossSaleAdsenseList(_siteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (Request["edit"] != null)
            {
                tab = "2";
                if (ViewState["tab"] != null)
                {
                    tab = ViewState["tab"].ToString();
                }
            }

            if (!Page.IsPostBack)
            {
                BindSite();
                _siteID = Master.SiteID;
                SiteSelected();
                CrossSaleAdsenseList(_siteID);
                GetCurrencyCode();
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindtblCrossSaleAdsenseListForEdit(Guid.Parse(Request["edit"]));
                    BindCrossSaleAdsenseSiteLookUpforEdit(Guid.Parse(Request["edit"]));
                }
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _siteID;

            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                CrossSaleAdsenseList(_siteID);
                ViewState["PreSiteID"] = _siteID;
            }
        }

        public void BindSite()
        {
            IEnumerable<tblSite> objSite = _Master.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
            ddlClass.DataSource = _oMasters.GetClassList().Where(x => x.IsActive);
            ddlClass.DataTextField = "Name";
            ddlClass.DataValueField = "ID";
            ddlClass.DataBind();
            ddlClass.Items.Insert(0, new ListItem("--Select Class--", "0"));
        }

        public void BindtblCrossSaleAdsenseListForEdit(Guid ID)
        {
            try
            {
                var result = _oManageInterRailNew.GetCrossSaleAdsenseListEdit(ID);
                txtName.Text = result.Name;
                ddlClass.SelectedValue = result.ClassId.ToString();
                txtPrice.Text = result.Price.ToString();
                txtSDescription.Text = result.ShortDescription;
                txtLDescription.Text = result.LongDescription;
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
                if (!string.IsNullOrEmpty(result.AdsenceImage))
                    imgBanner.ImageUrl = result.AdsenceImage;
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindCrossSaleAdsenseSiteLookUpforEdit(Guid ID)
        {
            var LookUpSites = db.tblCrossSaleAdsenseSiteLookUps.Where(x => x.AdsenseId == ID).ToList();
            foreach (var Lsites in LookUpSites)
            {
                foreach (TreeNode Pitem in trSites.Nodes)
                {
                    if (Guid.Parse(Pitem.Value) == Lsites.SiteId)
                        Pitem.Checked = true;
                }
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (trSites.CheckedNodes.Count < 1)
                {
                    ViewState["tab"] = tab = "2";
                    ShowMessage(2, "Please select at least one site.");
                    return;
                }
                UploadFile();
                if (btnSubmit.Text == "Submit")
                {
                    var _CrossSaleAdsense = new tblCrossSaleAdsense();
                    _CrossSaleAdsense.Id = Guid.NewGuid();
                    _CrossSaleAdsense.ClassId = Guid.Parse(ddlClass.SelectedValue);
                    _CrossSaleAdsense.IsActive = chkactive.Checked;
                    _CrossSaleAdsense.AdsenceImage = cBannerpath.Replace("~/", "");
                    _CrossSaleAdsense.URLLink = txtUrl.Text;
                    _CrossSaleAdsense.Name = txtName.Text;
                    _CrossSaleAdsense.Price = Convert.ToDecimal(string.IsNullOrEmpty(txtPrice.Text) ? "0.00" : txtPrice.Text);
                    _CrossSaleAdsense.ShortDescription = txtSDescription.Text;
                    _CrossSaleAdsense.LongDescription = txtLDescription.Text;
                    _CrossSaleAdsense.CreatedBy = AdminuserInfo.UserID;
                    _CrossSaleAdsense.CreatedOn = DateTime.Now;
                    Guid cid = _oManageInterRailNew.AddCrossSaleAdsense(_CrossSaleAdsense);
                    if (cid != null)
                    {
                        foreach (TreeNode node in trSites.Nodes)
                            if (node.Checked)
                            {
                                var CrossSaleAdsenseSiteLookUp = new tblCrossSaleAdsenseSiteLookUp();
                                CrossSaleAdsenseSiteLookUp.Id = Guid.NewGuid();
                                CrossSaleAdsenseSiteLookUp.AdsenseId = cid;
                                CrossSaleAdsenseSiteLookUp.SiteId = Guid.Parse(node.Value);
                                _oManageInterRailNew.AddCrossSaleAdsenseSiteLookUp(CrossSaleAdsenseSiteLookUp);
                            }
                    }
                    ShowMessage(1, "Record added successfully.");
                }
                else if (btnSubmit.Text == "Update")
                {
                    var _CrossSaleAdsense = new tblCrossSaleAdsense();
                    _CrossSaleAdsense.Id = Guid.Parse(Request["edit"]);
                    _CrossSaleAdsense.ClassId = Guid.Parse(ddlClass.SelectedValue);
                    _CrossSaleAdsense.URLLink = txtUrl.Text;
                    _CrossSaleAdsense.IsActive = chkactive.Checked;
                    _CrossSaleAdsense.Name = txtName.Text;
                    _CrossSaleAdsense.Price = Convert.ToDecimal(string.IsNullOrEmpty(txtPrice.Text) ? "0.00" : txtPrice.Text);
                    _CrossSaleAdsense.ShortDescription = txtSDescription.Text;
                    _CrossSaleAdsense.LongDescription = txtLDescription.Text;
                    _CrossSaleAdsense.ModifyBy = AdminuserInfo.UserID;
                    _CrossSaleAdsense.ModifyOn = DateTime.Now;

                    if (!string.IsNullOrEmpty(cBannerpath))
                        _CrossSaleAdsense.AdsenceImage = cBannerpath.Replace("~/", "");

                    var res = _oManageInterRailNew.UpdateCrossSaleAdsense(_CrossSaleAdsense);
                    if (res != null)
                    {
                        //Delete Existing Record
                        var id = Guid.Parse(Request["edit"]);
                        db.tblCrossSaleAdsenseSiteLookUps.Where(w => w.AdsenseId == id).ToList().ForEach(db.tblCrossSaleAdsenseSiteLookUps.DeleteObject);
                        db.SaveChanges();

                        //Get All Seletected Site
                        foreach (TreeNode node in trSites.Nodes)
                        {
                            if (node.Checked)
                            {
                                var CrossSaleAdsenseSiteLookUp = new tblCrossSaleAdsenseSiteLookUp();
                                CrossSaleAdsenseSiteLookUp.Id = Guid.NewGuid();
                                CrossSaleAdsenseSiteLookUp.AdsenseId = res;
                                CrossSaleAdsenseSiteLookUp.SiteId = Guid.Parse(node.Value);
                                _oManageInterRailNew.AddCrossSaleAdsenseSiteLookUp(CrossSaleAdsenseSiteLookUp);
                            }
                        }
                    }
                    ShowMessage(1, "Record updated successfully.");
                }
                ClearControls();
                CrossSaleAdsenseList(_siteID);
                btnSubmit.Text = "Submit";
                tab = "1";
                ViewState["tab"] = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CrossSaleAdsense.aspx");
        }

        private void CrossSaleAdsenseList(Guid siteId)
        {
            siteId = Master.SiteID;
            var CrossSaleAdsensesResult = (from tcs in db.tblCrossSaleAdsenses
                                           join tcsl in db.tblCrossSaleAdsenseSiteLookUps on tcs.Id equals tcsl.AdsenseId
                                           join ts in db.tblSites on tcsl.SiteId equals ts.ID
                                           where ts.ID == siteId
                                           select new { tcs, tcsl, ts });

            var result = (from itm in CrossSaleAdsensesResult
                          select new CrossSaleAdsensesField
                          {
                              Id = itm.tcs.Id,
                              Name = itm.tcs.Name,
                              Price = itm.tcs.Price,
                              ShortDescription = itm.tcs.ShortDescription,
                              LongDescription = itm.tcs.LongDescription,
                              IsActive = itm.tcs.IsActive,
                              BannerImg = itm.tcs.AdsenceImage
                          }).ToList();

            grdCrossSaleAdsense.DataSource = result.OrderBy(x => x.Name).ToList();
            grdCrossSaleAdsense.DataBind();
        }

        protected void grdCrossSaleAdsense_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ActiveInActive")
                {
                    Guid id = Guid.Parse(e.CommandArgument.ToString());
                    _oManageInterRailNew.ActiveInactiveCrossSaleAdsense(id);
                    CrossSaleAdsenseList(_siteID);
                    tab = "1";
                    ViewState["tab"] = "1";
                }

                if (e.CommandName == "Modify")
                {
                    Guid mId = Guid.Parse(e.CommandArgument.ToString());
                    tab = "2";
                    ViewState["tab"] = "2";
                    Response.Redirect("CrossSaleAdsense.aspx?edit=" + mId);
                }

                if (e.CommandName == "Remove")
                {
                    string[] commandArgumentValues = e.CommandArgument.ToString().Split(',');
                    Guid cid = Guid.Parse(commandArgumentValues[0]);

                    //Delete Existing Record
                    db.tblCrossSaleAdsenseSiteLookUps.Where(w => w.AdsenseId == cid).ToList().ForEach(db.tblCrossSaleAdsenseSiteLookUps.DeleteObject);
                    db.SaveChanges();

                    //Delete Record From TblCountryMst
                    tblCrossSaleAdsense DeleteCrossSaleAdsense = db.tblCrossSaleAdsenses.First(f => f.Id == cid);
                    db.tblCrossSaleAdsenses.DeleteObject(DeleteCrossSaleAdsense);
                    db.SaveChanges();

                    CrossSaleAdsenseList(_siteID);
                    ShowMessage(1, "Record deleted successfully.");
                    tab = "1";
                    ViewState["tab"] = "1";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected bool UploadFile()
        {
            try
            {
                var oCom = new Common();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupBannerImg.HasFile)
                {
                    if (fupBannerImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string fileName = fupBannerImg.FileName.Substring(fupBannerImg.FileName.LastIndexOf(".", StringComparison.Ordinal));
                    if (!ext.Contains(fileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    cBannerpath = "~/Uploaded/CrossSaleImage/";
                    cBannerpath = cBannerpath + oCom.CropImage(fupBannerImg, cBannerpath, 300, 480);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClearControls()
        {
            ddlClass.SelectedValue = "0";
            txtName.Text = txtUrl.Text = txtPrice.Text = txtSDescription.Text = txtLDescription.Text = "";
            chkactive.Checked = false;
            foreach (TreeNode node in trSites.Nodes)
                if (node.Checked)
                    node.Checked = false;
        }

        public void GetCurrencyCode()
        {
            FrontEndManagePass oManageClass = new FrontEndManagePass();
            var Data = db.tblSites.FirstOrDefault(x => x.ID == Master.SiteID);
            if (Data != null)
            {
                CurrencyID = Data.DefaultCurrencyID.Value;
                Currency = oManageClass.GetCurrency(CurrencyID);
            }
        }
    }

    public class CrossSaleAdsensesField
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid Class { get; set; }
        public decimal? Price { get; set; }
        public string BannerImg { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public bool? IsActive { get; set; }
    }
}
