﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="SiteTheme.aspx.cs"
    Inherits="IR_Admin.SiteTheme" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab.ToString()%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 10000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnimage" runat="server" />
    <h2>
        Site Theme</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="SiteTheme.aspx" class="current">List</a></li>
            <li><a id="aNew" href="SiteTheme.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:UpdatePanel ID="UP1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="grdTheme" runat="server" AutoGenerateColumns="False" PageSize="10"
                                CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                                AllowPaging="True" OnPageIndexChanging="grdTheme_PageIndexChanging" OnRowCommand="grdTheme_RowCommand">
                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                <PagerStyle CssClass="paging"></PagerStyle>
                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    Record not found.</EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <%#Eval("Name")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="38%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SiteName">
                                        <ItemTemplate>
                                            <%#Eval("SiteName")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="30%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Theme">
                                        <ItemTemplate>
                                            <%#Eval("Theme")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="30%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <a href='SiteTheme.aspx?id=<%#Eval("ID")%>'>
                                                <img src="images/edit.png" /></a>
                                            <asp:LinkButton ID="lnkDel" CommandName="Remove" CommandArgument='<%#Eval("ID")%>'
                                                runat="server" OnClientClick="return  confirm('Are you sure you want to delete this record ?')"><img src="images/delete.png" /></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Theme:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlTheme" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                            ForeColor="Red" ControlToValidate="ddlTheme" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtName" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtName" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Header:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHeader" runat="server" Width="250px">
                                                            <asp:ListItem Value="">Default</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.com.au/en_au_default_header2012.htm">AU Header</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.co.nz/en_nz_default_header2012.htm">NZ Header</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.co.uk/en_uk_default_header2012.htm">UK Header</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.com/en_us_default_header2012.htm">US Header</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Footer :
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlFooter" runat="server" Width="250px">
                                                            <asp:ListItem Value="">Default</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.com.au/en_au_default_footer2012.htm">AU Footer</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.co.nz/en_nz_default_footer2012.htm">NZ Footer</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.co.uk/en_uk_default_footer2012.htm">UK Footer</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.com/en_us_default_footer2012.htm">US Footer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Css :
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlStyle" runat="server" Width="250px">
                                                            <asp:ListItem Value="">Default</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.com.au/static/au_division_web_live/css/core-min.css">AU Css</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.co.nz/static/nz_division_web_live/css/core-min.css">NZ Css</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.co.uk/static/uk_division_web_live/css/core-min.css">UK Css</asp:ListItem>
                                                            <asp:ListItem Value="http://www.statravel.com/static/us_division_web_live/css/core-min.css">US Css</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
