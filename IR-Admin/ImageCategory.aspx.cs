﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ImageCategory : Page
    {
        readonly Masters _master = new Masters();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (!Page.IsPostBack)
            {
                BindCategoryList();
                if ((Request["edit"] != null) && (Request["edit"] != ""))
                {
                    BindCategoryListForEdit(Convert.ToInt32(Request["edit"]));
                    Tab = "2";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
        }

        public void BindCategoryList()
        {
            try
            {
                grdImgCategory.DataSource = _master.GetImageCategoryList();
                grdImgCategory.DataBind();
            }

            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindCategoryListForEdit(int id)
        {
            try
            {
                var result = _master.GetImageCategoryListEdit(id);
                txtCName.Text = result.CategoryName;
                txtWidth.Text = result.Width.ToString();
                txtHeight.Text = result.Height.ToString();
                btnSubmit.Text = "Update";
                chkactive.Checked = Convert.ToBoolean(result.IsActive);
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSubmit.Text == "Submit")
                {
                    var category = new tblImgCategory
                        {
                            CategoryName = txtCName.Text,
                            Width = Convert.ToInt32(txtWidth.Text),
                            Height = Convert.ToInt32(txtHeight.Text),
                            IsActive = chkactive.Checked
                        };
                    int res = _master.AddImgCategory(category);
                    if (res > 0)
                    {
                        ShowMessage(1, "Image category added successfully.");
                    }
                }
                else if (btnSubmit.Text == "Update")
                {
                    var category = new tblImgCategory
                        {
                            CategoryName = txtCName.Text,
                            Width = Convert.ToInt32(txtWidth.Text),
                            Height = Convert.ToInt32(txtHeight.Text),
                            IsActive = chkactive.Checked,
                            ID = Convert.ToInt32(Request["edit"])
                        };
                    int res = _master.UpdateImgCategory(category);
                    if (res > 0)
                    {
                        ShowMessage(1, "Image category updated successfully.");
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    }
                }
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ImageCategory.aspx");
        }

        protected void grdImgCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ActiveInActive")
            {
                Tab = "1";
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                _master.ActiveInactiveImageCat(id);
                BindCategoryList();
            }
        }
    }
}