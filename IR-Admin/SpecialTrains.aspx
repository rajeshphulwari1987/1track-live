﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SpecialTrains.aspx.cs" Inherits="IR_Admin.SpecialTrains" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveClientUrl("~/editor/redactor.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }
 
        $(function () {                  
          if(<%=tab.ToString()%>=="1")   {
                $("ul.list").tabs("div.panes > div");
            }
        }); 
        function pageLoad(sender, args) { $('#MainContent_txtContent').redactor({ iframe: true, minHeight: 200 }); }
        $(document).ready(function () {
            $(".chkSites").change(function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).attr("checked", "checked");

                    });
                } else {
                    $("#MainContent_trSites input[type='checkbox']").each(function (index) {
                        $(this).removeAttr("checked");
                    });
                }
            });
        });
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        #MainContent_dtlSpecialTrains tr td
        {
            width: 300px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Special Trains
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="SpecialTrains.aspx" class="current">List</a></li>
            <li><a id="aNew" href="SpecialTrains.aspx" class=" ">New</a> </li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv1">
                        <div class="searchDiv" style="padding: 10px; margin-bottom: 10px;">
                            Search Continent
                            <asp:DropDownList ID="ListddlContinent" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ListddlContinent_SelectedIndexChanged" />
                            &nbsp;&nbsp;&nbsp;&nbsp; Search Country
                            <asp:DropDownList ID="ListddlCountry" runat="server" Enabled="false" AutoPostBack="True"
                                OnSelectedIndexChanged="ListddlCountry_SelectedIndexChanged" />
                        </div>
                        <div class="train-detail-in" style="width: 100%">
                            <asp:HiddenField ID="hdnTrainId" runat="server" />
                            <asp:DataList ID="dtlSpecialTrains" runat="server" RepeatColumns="3" Width="100%"
                                RepeatDirection="Horizontal" OnItemCommand="dtlSpecialTrains_ItemCommand">
                                <ItemTemplate>
                                    <div class="train-detail-block" style="width: 300px;">
                                        <%--<div class="trainblock">
                                            <img alt="map" src='<%#Eval("CountryImg").ToString()=="" ? "images/map.png":Eval("CountryImg")%>' />
                                        </div>--%>
                                        <div class="trainblockleft">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Name:
                                                    </td>
                                                    <td>
                                                        <asp:Literal ID="ltrName" runat="server" Text='<%#Eval("Name")%>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Title:
                                                    </td>
                                                    <td>
                                                        <div class="Titletext">
                                                            <asp:Literal ID="ltrTitle" runat="server" Text='<%#Eval("Title")%>' /></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Continent:
                                                    </td>
                                                    <td>
                                                        <asp:Literal ID="ltrContinent" runat="server" Text='<%#Eval("Continent") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Country:
                                                    </td>
                                                    <td>
                                                        <asp:Literal ID="ltrCountry" runat="server" Text='<%#Eval("Country") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="font-weight: bold">
                                                        Action:
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <asp:ImageButton runat="server" ID="imgEdit" AlternateText="Modify" ToolTip="Edit"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="images/edit.png" />
                                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="images/delete.png"
                                                            OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%--<div class="divBannertxt">  Banner:</div>
                                        <div class="trainblockbanner" style="width:100%;margin-top:5px;">
                                            <img alt="map" src='<%#Eval("BannerImg")==null? "images/banner.png":Eval("BannerImg")%>' />
                                        </div>
                                    </div>--%>
                                </ItemTemplate>
                            </asp:DataList>
                            <table width="100%">
                                <tr class="paging">
                                    <asp:Repeater ID="DLPageCountItem" runat="server">
                                        <ItemTemplate>
                                            <td style="float: left">
                                                <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                    Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: none;">
                    <table class="tblMainSection">
                        <tr>
                            <td style="text-align: right; font-size: 13px;" class="valdreq">
                                <b>Image Format:</b> .JPEG, .JPG, .GIF, .BMP, .PNG, .PGM, .PBM, .PNM, .PFM, .PPM
                            </td>
                        </tr>
                    </table>
                    <div class="grid-sec2">
                        <table class="tblMainSection">
                            <tr>
                                <td style="width: 70%; vertical-align: top;">
                                    <table width="100%">
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Train Name
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtName" runat="server" MaxLength="180" />
                                                <asp:RequiredFieldValidator ID="reqCat" runat="server" ControlToValidate="txtName"
                                                    CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Title
                                            </td>
                                            <td class="col">
                                                <asp:TextBox ID="txtTitle" runat="server" MaxLength="180" />
                                                <asp:RequiredFieldValidator ID="reqTitle" runat="server" ControlToValidate="txtTitle"
                                                    CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="submit" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Continent
                                            </td>
                                            <td class="col">
                                                <asp:DropDownList ID="ddlContinent" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlContinent_SelectedIndexChanged" />
                                                <asp:RequiredFieldValidator ID="reqContinent" runat="server" ControlToValidate="ddlContinent"
                                                    InitialValue="0" Width="205" CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True"
                                                    ValidationGroup="submit" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Country
                                            </td>
                                            <td class="col">
                                                <asp:DropDownList ID="ddlCountry" runat="server" Enabled="false" />
                                                <asp:RequiredFieldValidator ID="reqCountry" runat="server" ControlToValidate="ddlCountry"
                                                    InitialValue="0" Width="205" CssClass="valdreq" ErrorMessage="*" SetFocusOnError="True"
                                                    ValidationGroup="submit" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Country Image
                                            </td>
                                            <td class="col">
                                                <asp:FileUpload ID="fupCountryImg" runat="server" />
                                                <a href="#" id="lnkCountryImg" class="clsLink" runat="server">View Image </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Banner Image
                                            </td>
                                            <td class="col">
                                                <asp:FileUpload ID="fupBannerImg" runat="server" />
                                                <a href="#" id="lnkBannerImg" class="clsLink" runat="server">View Image </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 5px;" class="col">
                                                Active
                                            </td>
                                            <td class="col">
                                                <asp:CheckBox ID="chkIsActv" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="cat-outer" style="width: 99%; margin: 0% .5% .5% .5%; position: relative;">
                                        <span style="color: #941E34; line-height: 30px; font-weight: bold">Description </span>
                                        <textarea id="txtContent" runat="server"></textarea>
                                    </div>
                                </td>
                                <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                    <b>&nbsp;Select Site</b>
                                    <div style="width: 95%; height: 350px; overflow-y: auto;">
                                        <input type="checkbox" value="Select / DeSelect All" class="chkSites" />Select/DeSelect
                                        All
                                        <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All">
                                            <NodeStyle ChildNodesPadding="5px" />
                                        </asp:TreeView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                        Text="Submit" Width="89px" ValidationGroup="submit" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                        Text="Cancel" />
                                </td>
                                <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                </td>
                            </tr>
                        </table>
                        <div style="clear: both;">
                        </div>
                        <asp:Panel ID="pnlCountry" runat="server" CssClass="popup" Style="overflow: hidden;">
                            <div class="dvPopupHead">
                                <div class="dvPopupTxt">
                                    Special Train Country Image</div>
                                <a href="#" id="btnCloseCountry" runat="server" style="color: #fff; padding-right: 5px;">
                                    X </a>
                            </div>
                            <div class="dvImg">
                                <img id="imgCountry" runat="server" alt="" border="0" style="height: 190px; width: 200px;" />
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdpupStock" runat="server" TargetControlID="lnkCountryImg"
                            CancelControlID="btnCloseCountry" PopupControlID="pnlCountry" BackgroundCssClass="modalBackground" />
                        <asp:Panel ID="pnlBanner" runat="server" CssClass="popup" Style="overflow: hidden;">
                            <div class="dvPopupHead">
                                <div class="dvPopupTxt">
                                    Special Train Banner Image</div>
                                <a href="#" id="btnCloseBanner" runat="server" style="color: #fff; padding-right: 5px;">
                                    X </a>
                            </div>
                            <div class="dvImg">
                                <img id="imgBanner" runat="server" alt="" border="0" style="height: 190px; width: 700px;" />
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkBannerImg"
                            CancelControlID="btnCloseBanner" PopupControlID="pnlBanner" BackgroundCssClass="modalBackground" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
