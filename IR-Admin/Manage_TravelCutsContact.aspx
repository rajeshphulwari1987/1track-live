﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_TravelCutsContact.aspx.cs"
    Inherits="IR_Admin.Manage_TravelCutsContact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title>STA Rail | STA Travel Rail</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="Styles/travelcutscss/assets/css/travelcuts.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:400" rel="stylesheet"
        type="text/css">
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:700" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="//www.statravel.com/static/us_division_web_live/css/jquery.mmenu.all.css"
        type="text/css" />
    <link href="//www.statravel.com/static/us_division_web_live/css/partner.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/travelcutscss/assets/img/icons/touch-icon.png" rel="apple-touch-icon-precomposed"
        sizes="180x180" type="image/png" />
    <link href="Styles/travelcutscss/assets/img/icons/favicon.png" rel="shortcut icon"
        type="image/png" />
    <link href="Styles/travelcutscss/assets/img/icons/favicon.ico" rel="shortcut icon"
        type="image/x-icon" />
    <link href="Styles/travelcutscss/assets/css/layout.css" rel="stylesheet" />
    <style type="text/css">
        .starail-HomeHero img
        {
            width: 100%;
        }
    </style>
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
            $('#txtRtPanel').redactor({ iframe: true, minHeight: 200 });

            $("input[class=rdrtID]").attr('name', "img");

            //Edit banner images
            $(".editbtn1").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });
            $(".editbtn6").click(function () {
                $("#divHeading").slideToggle("slow");
            });
            $(".editbtn7").click(function () {
                $("#divContent").slideToggle("slow");
            });

            //----------Edit heading---------//
            $(".editbtn6").click(function () {
                var value;
                $("#divHeading").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdHead').contents().find('body').html(value);
            });

            $(".editbtn7").click(function () {
                var value;
                $("#divContent").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent').contents().find('body').html(value);
            });


            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "ContentRight") {
                    $("#ContentRight").hide();
                }
                else if ($(this).attr("rel") == "ContentRightTxt") {
                    $("#ContentRightTxt").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }
            });

            //-------Edit right panel images-------//
            $(".editRtPanel").click(function () {
                $("#ContentRight").slideToggle("slow");
                $('.hdnRight').val($(this).attr("rel"));
            });

            //------Save right panel images------//
            $("#btnSaveRtPanel").click(function () {
                var imagename;
                $('div#ContentRight input[type=radio]').each(function () {
                    if ($('.hdnRight').val() == "rtPanelImgHome") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactHome").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgCall") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactCall").attr("src", imagename);
                        }
                    }
                    else if ($('.hdnRight').val() == "rtPanelImgEmail") {
                        if ($(this).is(":checked")) {
                            imagename = $(this).attr('value');
                            $("#imgContactEmail").attr("src", imagename);
                        }
                    }
                    $("#ContentRight").hide();
                });
            });

            //-------Edit right panel images text-------//
            $(".editRtPanelText").click(function () {
                $("#ContentRightTxt").slideToggle("slow");
                $('.hdnrtPanelTxt').val($(this).attr("rel"));
                var value;
                if ($(this).attr("rel") == "contactHomeTxt") {
                    value = $('#contactHomeTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactCallTxt") {
                    value = $('#contactCallTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
                else if ($(this).attr("rel") == "contactEmailTxt") {
                    value = $('#contactEmailTxt').html();
                    $('.redactor_rdRight').contents().find('body').html(value);
                }
            });

            //-----Save right panel text------//
            $(".btnSaveRttxt").click(function () {
                var value = $('textarea[name=txtRtPanel]').val();
                if ($('.hdnrtPanelTxt').val() == "contactHomeTxt") {
                    if (value != "") {
                        $('#contactHomeTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactCallTxt") {
                    if (value != "") {
                        $('#contactCallTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                else if ($('.hdnrtPanelTxt').val() == "contactEmailTxt") {
                    if (value != "") {
                        $('#contactEmailTxt').html(value);
                        $('textarea[name=txtRtPanel]').val('');
                    }
                }
                $("#ContentRightTxt").hide();
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <input type='hidden' id='sta-page-responsive' value='true' />
    <div id='sta-full-wrap'>
        <div id='sta-page-wrap'>
            <header class="sta-clearfix sta-header" role="banner" id="sta-header">
                <div class="starail-Nav-mobile starail-u-hideDesktop">
                </div>
                <div class="starail-Outer-wrap">
                    <div class="js-starail-nav-trigger starail-u-hideDesktop starail-Nav-mobileClose">
                    </div>

                    <div class='starail-Full-wrap'>
                        <div class='starail-Page-wrap'>
                            <header class="starail-clearfix starail-Header starail-u-hideMobile" role="banner">
                                <div class="starail-u-hideMobile starail-u-cf starail-Header-topRow" itemscope itemtype="http://schema.org/Organization">
                                    <div class="starail-Header-logo">
                                          <a itemprop="url" href="#">
                                            <img src="Styles/travelcutscss/assets/img/travellogo.png" />
                                        </a>
                                    </div>

                                    <div class="starail-Header-contact">
                                        <p class="starail-Header-contact-title">Travelcuts</p>
                                        <h2 class="starail-Header-contact-phone" itemprop="telephone">1.800.667.2887</h2>
                                        <p class="starail-Header-contact-disclaimer"></p>
                                    </div>
                                </div>
                                <nav class="starail-u-cf starail-Nav-wrap" id="sta-nav-wrap">
                                    <ul class="starail-u-cf starail-Nav" role="navigation" id="starail-nav">
                                        <li>
                                            <a href="#"> Rail Home   </a>
                                        </li>
                                        <li><a href="#">Print Queue </a> </li>
                                        <li><a href="#">FOC / AD75</a></li>
                                        <li>
                                            <a href="#"> FAQ   </a>
                                        </li>
                                        <li>
                                            <a href="#"> Feedback </a>
                                        </li>
                                        <li><a href="#"> Security  </a></li>
                                    </ul>
                                </nav>
                            </header>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: STA Global Header -->
            <main class="starail-Wrapper starail-Wrapper--main" role="main">
                <div class="starail-Grid starail-Grid--mobileFull">
                    <div class="starail-Grid-col starail-Grid-col--nopadding ">
                        <div class="starail-Grid--mobileFull imgfullbox editbaner">
                            <div class="editbtn1"><a class="edit-btn" href="#">Edit</a></div>
                            <div class="starail-Grid-col starail-Grid-col--nopadding">
                                <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                                    <img src='http://admin.1tracktest.com/CMSImages/Japan-electric-train.png' class="starail-HomeHero-img dots-header"
                                         id="imgMainBanner" runat="server" alt="." border="0" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Section starail-Section--nopadding">
                            
                            <div class="starail-Box starail-Box--whiteMobile starail-ContactForm ">
                                <div style="width:100%;" id="MainContent_divSta ">
                                </div>
                                <h3 class="editbaner">
                                    <div id="ContentHead" runat="server">
                                        <h1><span id="lblHead">Contact us at STA Travel<br></span></h1>

                                    </div>
                                    <div class="editbtn6"><a class="edit-btn" href="#">Edit</a></div>
                                </h3>
                               
                                <div class="editbaner" >
                                    <div id="ContentText" runat="server"><p>
                                       If you have any rail related question about any of our products featured on these pages, please do not hesitate to contact us.</span>
                                    </p></div>
                                        <div class="editbtn7"><a class="edit-btn" href="#">Edit</a></div>
                                </div>
                                <br>
                                <p></p>
                                <div class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
                                       <div class="floatt starail-u-size8of12 contact-img">
<img src="Styles/travelcutscss/assets/img/contactform.jpg" />
                                    </div>
                                 
                                   
                                     
                                    <div class="floatt starail-u-size4of12 pl10 contact-txt">
                                       <div class="address-block" id="addBlock" runat="server">


                                            <div class="fullrow box1">
                                                <div id="rtPanelImgHome" class="cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="rtPanelImgHome" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactHome" runat="server" src="http://admin.1tracktest.com/CMSImages/home-blue.png">
                                                            <input type="hidden" id="Hidden3" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
												<div class="con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="contactHomeTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactHomeTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span  id="lblRtPanelHead"><a href="http://www.statravel.com.au/stores.htm">Contact your nearest store</a></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="fullrow box2">
                                                <div id="rtPanelImgCall" class=" cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="rtPanelImgCall" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactCall" runat="server" src="http://admin.1tracktest.com/CMSImages/icon-call-blue.png">
                                                            <input type="hidden" id="Hidden6" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="contactCallTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactCallTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span id="Label1">phone: 134 782</span>
                                                        </span>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="fullrow box3">
                                                <div id="rtPanelImgEmail" class=" cont-img">
                                                    <div class="clsAbs">
                                                        <input type="button"  rel="rtPanelImgEmail" value="Edit" class="editRtPanel edit-btn">
                                                    </div>
                                                    <div class="pTop5">
                                                        <div class="imgblock">
                                                            <img border="0" class="imground scale-with-grid" id="imgContactEmail" runat="server" src="http://admin.1tracktest.com/CMSImages/icon-email.png">
                                                            <input type="hidden" id="Hidden2" class="hdnRight" value="rtPanelImgCall">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="con-txt">
                                                    <div class="clsAbs">
                                                        <input type="button" rel="contactEmailTxt" value="Edit" class="editRtPanelText edit-btn">
                                                    </div>
                                                    <div  id="contactEmailTxt" class="editbaner">
                                                        <span class="inline">
                                                            <span id="Label3">email: <a href="mailto:webquotes@statravel.com.au">webquotes@statravel.com.au</a></span>
                                                        </span><br>
                                                    </div>
												</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="clear">
                                    </div>
                                    <hr>
                                    <img src="Styles/stacss/assets/img/submitbtn.jpg" alt="">
                                    <div class="starail-Form-row" style="display:none;">
                                        <input type="submit" class="starail-Button starail-Form-button" id="MainContent_btnSubmit" onclick="return SaveContactUs();WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$btnSubmit&quot;, &quot;&quot;, true, &quot;vs&quot;, &quot;&quot;, false, false))" value="SUBMIT" name="ctl00$MainContent$btnSubmit" data-ga-category="http://staau.1tracktest.com/Contact-Us" data-ga-action="click" data-ga-label="staau.1tracktest.com">
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
    </main>
        </div>
    </div>
    <!-- END: STA Global Footer -->
    <script type="text/javascript">
        window.jQuery || document.write('<script src="Styles/travelcutscss/assets/js/vendor/jquery-1.11.1.min.js"><\/script>')
    </script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"
        type="text/javascript"></script>
    <!-- STA GLOBAL BOTTOMJS -->
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/partner.js"
        type="text/javascript"></script>
    <script type="text/javascript" src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.hammer.min.js"></script>
    <script src="//www.statravel.com/static/us_division_web_live/Javascript/jquery.mmenu-partner.js"
        type="text/javascript"></script>
    <!-- /STA GLOBAL BOTTOMJS -->
    <script src="Styles/travelcutscss/assets/js/main.min.js" type="text/javascript"></script>
    <footer class="starail-Footer">
        <div class="starail-Footer-outerWrap">
            <div class="starail-Footer-wrap">
                <section class="starail-Footer-links">
                    <p>
                        <a href='#'> Home </a>  <span>| </span>
                        <a href='#'> Contact Us </a>  <span>| </span>
                        <a href='#'> About Us </a>  <span>| </span>
                        <a href='#'> Booking Conditions </a>  <span>| </span>
                        <a href='#'> Privacy Policy </a>  <span>| </span>
                        <a href='#'> Conditions of Use </a>  <span> </span>
                    </p>
                </section>
                <p>
                    © Copyright International Rail Ltd. 2015 - All rights reserved. A company registered in England and Wales, company number: 3060803 with registered offices at International Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire. SO53 4AR.
                </p>
            </div>
        </div>
    </footer>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 141px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="padding-right: 50px; float: right; padding-top: 20px;">
            <input type="button" id="Button4" value="Cancel" class="button btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 240px; top: 141px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 240px; top: 141px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Content</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt" style="padding: 0px; float: right;">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="ContentRight" class="PopUpSampleIMG" style="display: none; width: 300px;
        height: auto; top: 637px; left: 189px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Images</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll">
            <asp:DataList ID="dtRtPanel" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="18px"
                        Height="18px" CssClass="bGray" />
                    <br />
                    <input id="rdrtID" class="rdrtID" runat="server" type="radio" name="img" value='<%#Eval("ImagePath")%>'
                        style="margin: 2px 0 2px 0" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt" style="float: right; padding-top: 10px; padding-right: 20px;">
            <input type="button" id="Button2" value="Cancel" class="button btnClose" rel="ContentRight" />
            <input type="button" id="btnSaveRtPanel" value="Save" class="button btnSaveRtPanel"
                rel="ContentRight" />
        </div>
    </div>
    <div class="PopUpSample" id="ContentRightTxt" style="display: none; left: 90px; top: 635px">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtRtPanel" name="txtRtPanel" cols="10" rows="5" class="rdRight"></textarea>
                        <input type="hidden" class="hdnrtPanelTxt" id="Hidden7" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="float-rt" style="float: right; padding-top: 10px; padding-right: 20px;">
            <table>
                <tr>
                    <td>
                        <input type="button" id="Button3" value="Cancel" class="button btnClose" rel="ContentRightTxt" />
                        <input type="button" id="btnSaveRttxt" value="Save" class="button btnSaveRttxt" rel="ContentRightTxt" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
