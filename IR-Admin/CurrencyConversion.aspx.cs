﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class CurrencyConversion : Page
    {
        readonly Masters _master = new Masters();
        public string Tab = string.Empty;

        public class ClsCurrency
        {
            public Guid ID { get; set; }
            public Guid TrgID { get; set; }
            public string Name { get; set; }
            public string Multiplier { get; set; }
            public bool IsEnabled { get; set; }
        }

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _siteID = Guid.Parse(selectedValue);
            SiteSelected();
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            _siteID = Master.SiteID;
            SiteSelected();
            tblCurr.Visible = true;
            dvSave.Visible = true;
        }
        #endregion

        void SiteSelected()
        {
            if (!Page.IsPostBack)
            {
                ViewState["PreSiteID"] = _siteID;
                BindCurrencyConvList();
            }
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _siteID.ToString()))
            {
                BindCurrencyConvList();
                ViewState["PreSiteID"] = _siteID;
            }
        }

        #region Control Events
        protected void rptCurrRate_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var txtMultiplier = (TextBox)e.Item.FindControl("txtMultiplier");
            txtMultiplier.Attributes.Add("onkeypress", "return keycheck()");
        }

        protected void rptCurrInner_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var hdId = (HiddenField)e.Item.FindControl("hdnId");
            var id = new Guid(hdId.Value);
            var db = new db_1TrackEntities();
            var lstCurr = _master.GetActiveCurrencyList();
            var lstCurrency = new List<ClsCurrency>();
            foreach (var item in lstCurr)
            {
                var siteCid = db.tblSites.FirstOrDefault(x => x.ID == _siteID).DefaultCurrencyID;
                var lstCurrCon = db.tblCurrencyConversions.FirstOrDefault(x => x.SrcCurrID == id && x.TrgCurrID == item.ID);

                var p = new ClsCurrency();
                p.TrgID = item.ID;
                p.ID = lstCurrCon != null ? Guid.Parse(lstCurrCon.ID.ToString()) : Guid.Parse("00000000-0000-0000-0000-000000000000");
                p.Multiplier = lstCurrCon != null ? lstCurrCon.Multiplier.Value.ToString("0.00000") : "1";
                p.IsEnabled = (id != item.ID);
                if (lstCurrCon != null)
                {
                    if (lstCurrCon.SrcCurrID != siteCid && lstCurrCon.TrgCurrID != siteCid)
                        p.IsEnabled = false;
                }
                lstCurrency.Add(p);
            }

            var rptCurrRate = (Repeater)e.Item.FindControl("rptCurrRate");
            if (rptCurrRate != null)
            {
                rptCurrRate.DataSource = lstCurrency;
                rptCurrRate.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var currCon = new CurrencyConv();
            foreach (RepeaterItem item in rptCurrInner.Items)
            {
                var rptCurrRate = (Repeater)item.FindControl("rptCurrRate");
                foreach (RepeaterItem innerItem in rptCurrRate.Items)
                {
                    var hdnSrcId = (HiddenField)item.FindControl("hdnId");
                    var hdnTrgID = (HiddenField)innerItem.FindControl("hdnTrgID");
                    var hdnCurrID = (HiddenField)innerItem.FindControl("hdnCurrID");
                    var txtCurr = (TextBox)innerItem.FindControl("txtMultiplier");

                    if (hdnCurrID != null && txtCurr != null && txtCurr.Text != string.Empty)
                    {
                        currCon.ID = Guid.Parse(hdnCurrID.Value);
                        currCon.Multiplier = Convert.ToDecimal(txtCurr.Text);
                    }

                    if (hdnSrcId.Value != hdnTrgID.Value)
                    {
                        if (hdnCurrID != null)
                        {
                            var lst = _master.GetCurrencyConvInnerListWithSiteId(Guid.Parse(hdnCurrID.Value), _siteID);
                            if (lst.Count != 0)
                            {
                                _master.UpdateCurrencyConv(currCon);
                            }
                            else
                            {
                                currCon.ID = Guid.NewGuid();
                                currCon.SiteID = _siteID;
                                currCon.SrcCurrID = Guid.Parse(hdnSrcId.Value);
                                currCon.TrgCurrID = Guid.Parse(hdnTrgID.Value);
                                if (txtCurr != null) currCon.Multiplier = Convert.ToDecimal(txtCurr.Text);

                                _master.AddCurrencyConv(currCon);
                            }
                        }
                    }
                }
            }
            ShowMessage(1, "Currency Exchange Rates successfully Updated.");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CurrencyConversion.aspx");
        }
        #endregion

        #region UserDefined Events
        public void BindCurrencyConvList()
        {
            try
            {
                rptCurrInner.DataSource = rptCurr.DataSource = _master.GetActiveCurrencyList();
                rptCurr.DataBind();
                rptCurrInner.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            switch (flag)
            {
                case 1:
                    DivSuccess.Style.Add("display", "block");
                    lblSuccessMsg.Text = message;
                    break;
            }
        }
        #endregion
    }
}