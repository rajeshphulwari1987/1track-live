﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SocialMedia.aspx.cs" Inherits="IR_Admin.SocialMedia" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                Social Media Links</h2>
            <div class="full mr-tp1">
                <!-- tab "panes" -->
                <div class="full mr-tp1">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="panes">
                        <div id="divlist" runat="server" style="display: block;">
                            <div class="crushGvDiv">
                                <asp:GridView ID="grvSocial" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                                    onrowcommand="grvSocial_RowCommand"  >
                                    <AlternatingRowStyle BackColor="#FBDEE6" />
                                    <PagerStyle CssClass="paging"></PagerStyle>
                                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                        BorderColor="#FFFFFF" BorderWidth="1px" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        Record not found.</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Site Name">
                                            <ItemTemplate>
                                                <%#Eval("SiteName")%>
                                                <asp:HiddenField Value='<%#Eval("SiteID")%>' runat="server" ID="hdnSiteId" />
                                            </ItemTemplate>
                                            <ItemStyle Width="17%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Script">
                                            <ItemTemplate>
                                                <asp:TextBox TextMode="MultiLine" Text='<%#Eval("Script")%>' ID="txtScript" runat="server" Width="100%" />
                                            </ItemTemplate>
                                            <ItemStyle Width="74%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("SiteID")%>'
                                                    Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                    ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Width="8%" HorizontalAlign="Center"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div style="text-align:center"><asp:Button ID="btnSave" runat="server" 
                                    Text="Save Changes" CssClass="button" onclick="btnSave_Click"/></div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
