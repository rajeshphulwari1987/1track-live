﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="StockQueue.aspx.cs" Inherits="IR_Admin.PrintQueue.StockQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {setMenu();});
        function setMenu() {
            if ("<%=tab.ToString()%>" == "1") {
                $("ul.list").tabs("div.panes > div");
            }
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upnlCategory" runat="server">
        <ContentTemplate>
            <h2>
                <asp:HiddenField ID="hdnId" runat="server" />
                Print Queues</h2>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <ul class="list">
                <li><a id="aList" href="StockQueue.aspx" class="current">List</a></li>
                <li><a id="aNew" href="StockQueue.aspx" class="">New/Edit</a></li>
            </ul>
            <!-- tab "panes" -->
            <div class="full mr-tp1">
                <div class="panes">
                    <div id="divlist" runat="server" style="display: none;">
                        <div class="crushGvDiv">
                            <table class="tblMainSection">
                                <tr>
                                    <td>
                                        <asp:GridView ID="grvPrintQueue" runat="server" AutoGenerateColumns="False" CssClass="grid-head2"
                                            OnPageIndexChanging="grvPrintQueue_PageIndexChanging" AllowPaging="True" PageSize="15"
                                            CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnRowCommand="grvPrintQueue_RowCommand">
                                            <AlternatingRowStyle BackColor="#FBDEE6" />
                                            <PagerStyle CssClass="paging"></PagerStyle>
                                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                BorderColor="#FFFFFF" BorderWidth="1px" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <EmptyDataRowStyle HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                Record not found.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <%#Eval("Name")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="55%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type">
                                                    <ItemTemplate>
                                                        <%#Eval("QueueType")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Printer Code">
                                                    <ItemTemplate>
                                                        <%#Eval("PrinterCode")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" ID="imgModify" AlternateText="edit" ToolTip="Edit"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Modify" ImageUrl="~/images/edit.png" />
                                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                                            OnClientClick="return confirm('Are you sure you want to delete this item?');"
                                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png" />
                                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="8%"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="divNew" runat="server" style="display: block;">
                        <div class="divMain">
                            <div class="divleft">
                                Stock Queue:
                            </div>
                            <div class="divright">
                                <asp:DropDownList runat="server" ID="ddlParentStockQueue" Width="205" />
                                <asp:RequiredFieldValidator ID="reqdParentStockQueue" runat="server" ErrorMessage="*"
                                    CssClass="valdreq" ControlToValidate="ddlParentStockQueue" ValidationGroup="submit"
                                    InitialValue="0" />
                            </div>
                            <div class="divleft">
                                Type:
                            </div>
                            <div class="divright">
                                <asp:DropDownList runat="server" ID="ddlStockQueueType" Width="205" />
                                <asp:RequiredFieldValidator ID="reqStockQueueType" runat="server" ErrorMessage="*"
                                    CssClass="valdreq" ControlToValidate="ddlStockQueueType" ValidationGroup="submit"
                                    InitialValue="0" />
                            </div>
                            <div class="divleft">
                                Printer Code:</div>
                            <div class="divright">
                                <asp:TextBox runat="server" ID="txtPrintCode" />
                                <asp:RequiredFieldValidator ID="reqPrintCode" runat="server" ErrorMessage="*" CssClass="valdreq"
                                    ControlToValidate="txtPrintCode" ValidationGroup="submit" />
                            </div>
                            <div class="divleft">
                                Is Active ?</div>
                            <div class="divright">
                                <asp:CheckBox runat="server" ID="chkIsActive" />
                            </div>
                            <div style="float: left; border-bottom: 1px dashed #B1B1B1;width:100%;">
                                
                            </div>
                            <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                                .
                            </div>
                            <div class="divrightbtn" style="padding-top: 10px;">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button1" OnClick="btnSubmit_Click"
                                    Text="Submit" ValidationGroup="submit" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button1" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
