﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Business;

namespace IR_Admin.PrintQueue
{
    public partial class StockQueue : System.Web.UI.Page
    {
        readonly ManagePrintQueue _oQueue = new ManagePrintQueue();
        readonly public Masters oMaster = new Masters();
        //public static List<GetOfficeFullName> listFullStationList = null;
        public static List<GetOfficeFullName> listFullStationList = new List<GetOfficeFullName>();

        public string tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                tab = "1";
                if (!String.IsNullOrEmpty(hdnId.Value))
                    tab = "2";
                listFullStationList = null;
                FillDropDown();
                FillGrid();
            }
        }

        void FillDropDown()
        {
            try
            {
                ddlStockQueueType.DataSource = _oQueue.GetStockQueueListType();
                ddlStockQueueType.DataValueField = "ID";
                ddlStockQueueType.DataTextField = "Name";
                ddlStockQueueType.DataBind();
                ddlStockQueueType.Items.Insert(0, new ListItem("--Select Stock Queue Type--", "0"));
                if (listFullStationList == null)
                    listFullStationList = _oQueue.GetOfficeList();
                ddlParentStockQueue.DataSource = listFullStationList;
                ddlParentStockQueue.DataValueField = "ID";
                ddlParentStockQueue.DataTextField = "Name";
                ddlParentStockQueue.DataBind();
                ddlParentStockQueue.Items.Insert(0, new ListItem("--Select Stock Queue--", "0"));
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        void FillGrid()
        {
            try
            {
                grvPrintQueue.DataSource = _oQueue.GetStockQueueList();
                grvPrintQueue.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                AddEditStockQueue();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("StockQueue.aspx");
        }

        void AddEditStockQueue()
        {
            try
            {
                Guid id = string.IsNullOrEmpty(hdnId.Value) ? new Guid() : Guid.Parse(hdnId.Value);
                Guid branchID = ddlParentStockQueue.SelectedValue == "0" ? new Guid() : Guid.Parse(ddlParentStockQueue.SelectedValue);
                Guid resid = _oQueue.AddEditStockQueue(new tblStockQueue
                    {
                        ID = id,
                        BranchID = branchID,
                        IsActive = chkIsActive.Checked,
                        CreatedBy = AdminuserInfo.UserID,
                        CreatedOn = DateTime.Now,
                        PrinterCode = txtPrintCode.Text.Trim(),
                        StockQueueTypeID = Guid.Parse(ddlStockQueueType.SelectedValue)
                    });
               
                FillGrid();
                ShowMessage(1, string.IsNullOrEmpty(hdnId.Value) ? "Stock Queue created successfully" : "Stock Queue updated successfully");
                tab = "2";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void GetValueForEdit()
        {
            try
            {
                Guid id = Guid.Parse(hdnId.Value);
                var rec = _oQueue.GetStockQueueById(id);
                chkIsActive.Checked = rec.IsActive;
                txtPrintCode.Text = rec.PrinterCode;
                ddlParentStockQueue.SelectedValue = rec.BranchID.ToString();
                ddlStockQueueType.SelectedValue = rec.StockQueueTypeID.ToString();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }

        }

        protected void grvPrintQueue_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPrintQueue.PageIndex = e.NewPageIndex;
            FillGrid();
            tab = "1";
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "setMenu();", true);
        }

        protected void grvPrintQueue_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    hdnId.Value = e.CommandArgument.ToString();
                    GetValueForEdit();
                    tab = "2";
                    break;

                case "ActiveInActive":
                    {
                        Guid id = Guid.Parse(e.CommandArgument.ToString());
                        _oQueue.ActiveInactiveStockQueue(id);
                        FillGrid();
                        tab = "1";
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "setMenu();", true);
                    }
                    break;

                case "Remove":
                    try
                    {
                        Guid id = Guid.Parse(e.CommandArgument.ToString());
                        bool res = _oQueue.DeleteStockQueue(id);
                        if (res)
                            ShowMessage(1, "Record deleted successfully");
                        FillGrid();
                        tab = "1";
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "setMenu();", true);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(2, ex.Message);
                    }
                    break;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}