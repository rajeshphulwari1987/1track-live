﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Data.SqlClient;
using System.Data;

namespace IR_Admin.PrintQueue
{
    public partial class StockSearch : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        Guid _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (AdminuserInfo.RoleId == Guid.Parse("14f08452-5c69-44a8-b4d1-585be6b10b16"))
                    ddlPassType.Items.Remove(ddlPassType.Items.FindByValue("2"));

                if (!Page.IsPostBack)
                {
                    _SiteID = Master.SiteID;
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void FillStockGrid()
        {
            try
            {
                var result = _master.GetStockDetailReportByStockNo(Convert.ToInt32(txtStockNo.Text), Convert.ToInt32(ddlPassType.SelectedValue)).ToList();
                if (result != null && result.Count > 0)
                {
                    grdStock.DataSource = result;
                    grdStock.DataBind();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillStockGrid();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

    }
}
