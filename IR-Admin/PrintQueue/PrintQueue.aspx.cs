﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
namespace IR_Admin.PrintQueue
{
    public partial class PrintQueue1 : System.Web.UI.Page
    {
        ManagePrinting oPrint = new ManagePrinting();
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
            Guid siteId = this.Master.SiteID;
            string url = oPrint.GetSiteUrl(siteId);
            url = url + "Agent/Login?UserId=" + AdminuserInfo.UserID;

            string redirect = "<script>window.open('" + url + "');</script>";
            Response.Write(redirect);

            HttpRequest Request = HttpContext.Current.Request;
            string Browser = Request.Browser.Browser.ToString();
            if(Browser.ToUpper()=="IE"){
                System.Threading.Thread.Sleep(6000);
            }


            string redirect2 = "<script>window.location='" + Request.UrlReferrer.ToString() + "';</script>";
            Response.Write(redirect2);
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
    }
}