﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="StockAllocation.aspx.cs" Inherits="IR_Admin.PrintQueue.StockAllocation" %>
       <%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #MainContent_trStock table
        {
            width: 100%;
            position: relative;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            border-radius: 5px;
            -ms-border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px; /*float:left;*/
            padding: 1%;
            behavior: url(PIE.htc);
            background: #f7d5e2;
        }
        .clchild
        {
            margin-top: -10px !important;
            padding: 0px !important;
            background-color: #f7d5e2;
        }
    </style>
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script src="../Scripts/stockallocation.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(function () {
            setMenu();
        });
        function setMenu() {
            if ("<%=tab.ToString()%>" == "1") {
                $("ul.list").tabs("div.panes > div");
            }
        }
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }    
    </script>
    <script type="text/javascript">
        function checkStock() {
            var stFrom = $("#MainContent_txtStockFrom").val();
            var sTo = $("#MainContent_txtStockTo").val();
            if (parseInt(stFrom) > parseInt(sTo)) {
                $("#MainContent_txtStockFrom").val("0");
                alert("Stock Range is not valid.");
            }
        }
    </script>
    <script type="text/jscript">
        function editStockNo(id) {
            document.getElementById("<%= hdnId.ClientID %>").value = id;
            __doPostBack("<%= btneditStock.UniqueID %>", "");
        }
        function addStockNo(id) {
            document.getElementById("<%= hdnBranchId.ClientID %>").value = id;
            __doPostBack("<%= btnAddStock.UniqueID %>", "");
        }

        function deleteStockNo(id) {
            if (getConfirm()) { 
                document.getElementById("<%= hdnId.ClientID %>").value = id; 
                __doPostBack("<%= btnDeleteStock.UniqueID %>", "");
            }
            return false;
        }

        function getConfirm() {
            return confirm('Are you sure you want to delete this record?');
        }

        function ob_change_icons(temp_node, imgSrc) {
            while (temp_node != null) {
                temp_node.previousSibling.firstChild.firstChild.src = ob_icons + "/" + imgSrc;

                if (document.getElementById("chkApplyToParents") && document.getElementById("chkApplyToParents").checked == false) {
                    return;
                }
                temp_node = ob_getParentOfNode(temp_node);
            }
        }
        function divprogressHide() {
            $("#divprogress").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button ID="btneditStock" runat="server" OnClick="btneditStock_Click" CssClass="hidebtn" />
    <asp:Button ID="btnAddStock" runat="server" OnClick="btnaddStock_Click" CssClass="hidebtn" />
    <asp:Button ID="btnDeleteStock" runat="server" OnClick="btnDeleteStock_Click" CssClass="hidebtn" />
    <h2>
        <asp:HiddenField ID="hdnId" runat="server" />
       
        <asp:HiddenField ID="hdnBranchId" runat="server" />
        Stock Allocation</h2>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <ul class="list">
        <li><a id="aList" href="StockAllocation.aspx" class="current">List</a></li>
        <li></li>
    </ul>
    <asp:LinkButton ID="lnkNew" runat="server" Text="New" CssClass="lnkNew" />
    <!-- tab "panes" -->
    <div class="full mr-tp1">
        <div class="panes">
            <div id="divlist" runat="server" style="display: block;">
                <div class="crushGvDivStock">
                    <asp:TreeView ID="trStock" runat="server" LineImagesFolder="~/TreeLineImages" Width="100%">
                        <LeafNodeStyle CssClass="sub-block-first" />
                        <ParentNodeStyle CssClass="sub-block-second" />
                        <RootNodeStyle CssClass="main-block" />
                        <SelectedNodeStyle CssClass="main-block" />
                        <DataBindings>
                            <asp:TreeNodeBinding TextField="Text" DataMember="System.Data.DataRowView" ValueField="ID" />
                        </DataBindings>
                    </asp:TreeView>
                </div>
            </div>
            <div id="divNew" runat="server" style="display: block;">
            </div>
            <div style="display: none;">
                <asp:LinkButton ID="lnkPopup" runat="server" Text="lnk" />
            </div>
           
        </div>
    </div>
     <div id="pnlStockAdd" style="display:block; width:982px;">
                <div class="heading">
                    Add/Edit Stock Range<asp:HiddenField runat="server"  ID="hdnIdaddstockParent" />
                </div>
                <div class="divMain" style="margin-top: -2px;">
                    <div class="divleft">
                        Stock Queue:
                    </div>
                    <div class="divright">
                        <asp:DropDownList runat="server" ID="ddlParentStockQueue" Width="205" />
                        <asp:RequiredFieldValidator ID="reqdParentStockQueue" runat="server" ErrorMessage="*"
                            CssClass="valdreq" ControlToValidate="ddlParentStockQueue" ValidationGroup="submit"
                            InitialValue="0" />
                    </div>
                    <div class="divleft">
                        Stock Number From:</div>
                    <div class="divright">
                        <asp:TextBox runat="server" ID="txtStockFrom" MaxLength="18" onblur="checkStock()" />
                        <asp:RequiredFieldValidator ID="reqStockFrom" runat="server" ErrorMessage="*" CssClass="valdreq"
                            ControlToValidate="txtStockFrom" ValidationGroup="submit" />
                        <asp:FilteredTextBoxExtender ID="ftbStockFrom" TargetControlID="txtStockFrom" ValidChars="0123456789"
                            runat="server" />
                    </div>
                    <div class="divleft">
                        Stock Number To:</div>
                    <div class="divright">
                        <asp:TextBox runat="server" ID="txtStockTo" MaxLength="18" onblur="checkStock()" />
                        <asp:RequiredFieldValidator ID="reqStockTo" runat="server" ErrorMessage="*" CssClass="valdreq"
                            ControlToValidate="txtStockTo" ValidationGroup="submit" />
                        <asp:FilteredTextBoxExtender ID="ftbStockTo" TargetControlID="txtStockTo" ValidChars="0123456789"
                            runat="server" />
                    </div>
                    <div class="divleft">
                        Status:
                    </div>
                    <div class="divright">
                        <asp:DropDownList runat="server" ID="ddlStatus" Width="205" Enabled="false" />
                        <asp:RequiredFieldValidator ID="reqStatus" runat="server" ErrorMessage="*" CssClass="valdreq"
                            ControlToValidate="ddlStatus" ValidationGroup="submit" InitialValue="0" />
                    </div>
                    <div class="divleftbtn" style="color: #eee; padding-top: 10px;">
                        .
                    </div>
                    <div class="divrightbtn" style="padding-top: 10px;">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="button1" OnClick="btnSubmit_Click"
                            Text="Save & Allocated To Child" ValidationGroup="submit" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="button1" OnClick="btnCancel_Click" OnClientClick="divprogressHide()"
                            Text="Cancel" />

                            <div class="clear"></div>
                        <span>
                            <asp:Label ID="lblMessage" ForeColor="green" runat="server" /></span>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mdpupStock" runat="server" TargetControlID="lnkNew" CancelControlID="btnCancel"
                PopupControlID="pnlStockAdd" BackgroundCssClass="modalBackground" />
</asp:Content>
