﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockSearch.aspx.cs" MasterPageFile="~/Site.Master" Inherits="IR_Admin.PrintQueue.StockSearch" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_txtStockNo").keypress(function (event) {
                var controlKeys = [8, 9, 13, 35, 36, 37, 39];
                var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
                if (!event.which || (49 <= event.which && event.which <= 57) || (48 == event.which && $(this).attr("value")) || isControlKey) {
                    return;
                } else {
                    event.preventDefault();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Stock Search</h2>
    <asp:UpdatePanel ID="Upnl1" runat="server">
        <ContentTemplate>
            <div class="full mr-tp1">
                <ul class="list">
                    <li><a id="aList" href="StockSearch.aspx" class="current">List</a></li>
                </ul>
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="full mr-tp1">
                    <div class="panes">
                        <div id="divlist" runat="server">
                            <div id="dvSearch" class="searchDiv" runat="server">
                                <table width="100%" style="line-height: 20px">
                                    <tr>
                                        <td>Stock Number :
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtStockNo" runat="server" Width="200px" ToolTip="Enter Stock Number." MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage="*" Display="Static"
                                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="txtStockNo" />
                                        </td>
                                        <td>Pass Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPassType" runat="server" Width="200px">
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Eurail</asp:ListItem>
                                                <asp:ListItem Value="2">Britrail</asp:ListItem>
                                                <asp:ListItem Value="3">Interrail</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfv2" InitialValue="0" runat="server" ErrorMessage="*" Display="Static"
                                                ForeColor="Red" ValidationGroup="rv" ControlToValidate="ddlPassType" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="rv" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:GridView ID="grdStock" runat="server" CellPadding="4" CssClass="grid-head2"
                                ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" CssClass="newheader" />
                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <Columns>
                                    <asp:BoundField DataField="ORDERID" HeaderText="Order Number" ItemStyle-Width="20%" />
                                    <asp:BoundField DataField="STATUSNAME" HeaderText="Status" ItemStyle-Width="20%" />
                                    <asp:BoundField DataField="PRINTERLOCATION" HeaderText="Assigned Printer" ItemStyle-Width="60%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No records found !
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

