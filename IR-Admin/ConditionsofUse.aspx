﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="ConditionsofUse.aspx.cs"
    Inherits="IR_Admin.ConditionsofUse" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Tab/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {                  
            if(<%=Tab.ToString()%>=="1")
            {
               $("ul.list").tabs("div.panes > div");
            }  
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('#MainContent_txtDesc').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtWl').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtPrd').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtWeb').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtAnti').redactor({ iframe: true, minHeight: 200 });
            $('#MainContent_txtCopy').redactor({ iframe: true, minHeight: 200 });
        }
        window.setTimeout("closeDiv();", 100000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".grid-sec2").find(".cat-outer-cms").hide();

            $("#divCmsWl").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsWl").next().show();
            });

            $("#divCmsPrd").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsPrd").next().show();
            });

            $("#divCmsWeb").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsWeb").next().show();
            });

            $("#divCmsAnti").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsAnti").next().show();
            });

            $("#divCmsCopy").click(function () {
                $(".grid-sec2").find(".cat-outer-cms").hide();
                $("#divCmsCopy").next().show();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Conditions of Use</h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="ConditionsofUse.aspx" class="current">List</a></li>
            <li><a id="aNew" href="ConditionsofUse.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
        </div>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <asp:GridView ID="grdConditions" runat="server" AutoGenerateColumns="False" PageSize="10"
                        CssClass="grid-head2" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="grdConditions_PageIndexChanging" OnRowCommand="grdConditions_RowCommand">
                        <AlternatingRowStyle BackColor="#FBDEE6" />
                        <PagerStyle CssClass="paging"></PagerStyle>
                        <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                        <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                            BorderColor="#FFFFFF" BorderWidth="1px" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <%#Eval("Title")%>
                                </ItemTemplate>
                                <ItemStyle Width="38%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SiteName">
                                <ItemTemplate>
                                    <%#Eval("SiteName")%>
                                </ItemTemplate>
                                <ItemStyle Width="30%"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href='ConditionsofUse.aspx?id=<%#Eval("ID")%>'>
                                        <img src="images/edit.png" /></a>
                                    <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                        CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                        OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("Id")%>'
                                        Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                        ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' OnClientClick="return confirm('Are you sure you want to activate/deactivate this user?');" />
                                    </div>
                                </ItemTemplate>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="divNew" runat="server" style="display: block;">
                    <table class="tblMainSection" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <fieldset class="grid-sec2">
                                    <legend><b>Description</b></legend>
                                    <div class="cat-outer" style="border-bottom: 1px solid #FBDEE6; width: 100%; margin: 0%;">
                                        <div class="cat-inner-alt">
                                            <table style="width: 870px">
                                                <tr>
                                                    <td>
                                                        Site:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSite" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Title:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTitle" runat="server" Width="250px" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        Description:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="10" Columns="5" />
                                                        &nbsp;<asp:RequiredFieldValidator ID="rfDesc" runat="server" ErrorMessage="*" ForeColor="Red"
                                                            ControlToValidate="txtTitle" ValidationGroup="rv" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is Active ?
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:CheckBox ID="chkIsActv" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="grid-sec2">
                                    <legend><b>Contents</b></legend>
                                    <div id="divCmsWl">
                                       Warranty and Liability
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtWl" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsPrd">
                                        Products
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtPrd" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsWeb">
                                         Use of our website
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtWeb" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsAnti">
                                         Anti Viral Software
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtAnti" runat="server"></textarea>
                                    </div>
                                    <div id="divCmsCopy">
                                        Copyright
                                    </div>
                                    <div class="cat-outer-cms">
                                        <textarea id="txtCopy" runat="server"></textarea>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
