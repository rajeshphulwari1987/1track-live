﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Hashtags
{
    public partial class HashtagMappings : System.Web.UI.Page
    {
        readonly ManageHashTag _master = new ManageHashTag();
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        readonly db_1TrackEntities _db = new db_1TrackEntities();
        public Int64 hashtagID = 0;
        private Guid _tagSiteGuidId;
        public List<ListItem> Countries = null;
        public string productArray1 = string.Empty;
        readonly private ManageProduct _oProduct = new ManageProduct();
        Guid _SiteID;

        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if ((Request["id"] != null) && (Request["id"] != ""))
                    hashtagID = Convert.ToInt64(Request["id"]);
                if ((Request["sid"] != null) && (Request["sid"] != ""))
                    _tagSiteGuidId = Guid.Parse(Request["sid"]);
                if (!Page.IsPostBack)
                {
                    InitCall();
                    BindHasTagMapping();
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void InitCall()
        {
            FillCategory();
            FillCountry();
            BindHashtagList();
            BindProductList();
            BindSpecialTrainList();
            if (hashtagID > 0)
            {
                EditHashTagMapping(hashtagID);
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
            }
            else
            {
                divlist.Visible = true;
                divNew.Visible = false;
            }
        }

        private void BindHashtagList()
        {
            try
            {
                ddlHashTag.DataSource = _master.GetHashtagsList().ToList().OrderBy(x => x.Title).ToList();
                ddlHashTag.DataValueField = "Id";
                ddlHashTag.DataTextField = "Title";
                ddlHashTag.DataBind();
                ddlHashTag.Items.Insert(0, new ListItem("Select Keyword", "0"));
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void BindProductList()
        {
            try
            {
                _SiteID = Master.SiteID;
                List<HasTagProductList> products = new List<HasTagProductList>();
                HasTagProductList product = null;
                var _products = _master.GetProductList(_SiteID);
                foreach (var p in _products.Select(x => x.ProductId).Distinct())
                {
                    product = _products.Where(x => x.ProductId == p).FirstOrDefault();
                    product.CountryIDs = string.Join(",", _products.Where(x => x.ProductId == p).Select(x => x.CountryID).ToList());
                    products.Add(product);
                }
                grdProduct.DataSource = products.OrderBy(x => x.CategoryName).ThenBy(x => x.ProductName).ToList();
                grdProduct.DataBind();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void BindSpecialTrainList()
        {
            try
            {
                _SiteID = Master.SiteID;
                grdSpecialTrain.DataSource = _master.GetSpecialTrainList(_SiteID);
                grdSpecialTrain.DataBind();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private List<ProductIds> ProductIdList()
        {
            List<ProductIds> list = new List<ProductIds>();
            for (int i = 0; i < grdProduct.Rows.Count; i++)
            {
                CheckBox chkCheck = grdProduct.Rows[i].FindControl("chkCheck") as CheckBox;
                HiddenField hdnId = grdProduct.Rows[i].FindControl("hdnId") as HiddenField;
                var txtProOrderBy = grdProduct.Rows[i].FindControl("txtProOrderBy") as TextBox;
                if (chkCheck.Checked)
                {
                    list.Add(new ProductIds()
                    {
                        ProductId = Guid.Parse(hdnId.Value),
                        OrderBy = (txtProOrderBy != null && txtProOrderBy.Text != string.Empty) ? int.Parse(txtProOrderBy.Text) : (int?)null
                    });
                }
            }
            return list.ToList();
        }

        private List<SpecialTrainIds> SpecialTrainIdList()
        {
            List<SpecialTrainIds> list = new List<SpecialTrainIds>();
            for (int i = 0; i < grdSpecialTrain.Rows.Count; i++)
            {
                CheckBox chkCheck = grdSpecialTrain.Rows[i].FindControl("chkCheck") as CheckBox;
                HiddenField hdnId = grdSpecialTrain.Rows[i].FindControl("hdnId") as HiddenField;
                var txtSPTOrderBy = grdSpecialTrain.Rows[i].FindControl("txtSPTOrderBy") as TextBox;
                if (chkCheck.Checked)
                {

                    list.Add(new SpecialTrainIds()
                        {
                            SpecialTrainId = Guid.Parse(hdnId.Value),
                            OrderBy = (txtSPTOrderBy != null && txtSPTOrderBy.Text != string.Empty) ? int.Parse(txtSPTOrderBy.Text) : (int?)null
                        });
                }
            }
            return list.ToList();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteHashTagMapping();
                AddHashTagMapping();
                ShowMessage(1, "Record updated successfully.");
                BindHasTagMapping();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void AddHashTagMapping()
        {
            try
            {
                if (ddlHashTag.SelectedIndex > 0)
                {
                    if (ProductIdList() != null && ProductIdList().Count > 0)
                    {
                        foreach (var pid in ProductIdList())
                        {
                            _master.AddHashtagMapping(new HashtagMapping
                            {
                                HashtagId = Convert.ToInt64(ddlHashTag.SelectedValue),
                                ProductId = pid.ProductId,
                                OrderBy = pid.OrderBy,
                                IsActive = true
                            });
                        }
                    }
                    if (SpecialTrainIdList() != null && SpecialTrainIdList().Count > 0)
                    {
                        foreach (var sid in SpecialTrainIdList())
                        {
                            _master.AddHashtagMapping(new HashtagMapping
                            {
                                HashtagId = Convert.ToInt64(ddlHashTag.SelectedValue),
                                SpecialTrainId = sid.SpecialTrainId,
                                OrderBy = sid.OrderBy,
                                IsActive = true
                            });
                        }
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void DeleteHashTagMapping()
        {
            try
            {
                // TODO: delete product for specific sites 
                _db.HashtagMappings.Where(x => x.HashtagId == hashtagID).ToList().ForEach(_db.HashtagMappings.DeleteObject);
                _db.SaveChanges();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void EditHashTagMapping(Int64 HashTagId)
        {
            try
            {
                ddlHashTag.SelectedValue = HashTagId.ToString();
                var data = _db.HashtagMappings.Where(x => x.HashtagId == HashTagId).ToList();
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        for (int i = 0; i < grdProduct.Rows.Count; i++)
                        {
                            CheckBox PchkCheck = grdProduct.Rows[i].FindControl("chkCheck") as CheckBox;
                            HiddenField PhdnId = grdProduct.Rows[i].FindControl("hdnId") as HiddenField;
                            var txtProOrderBy = grdProduct.Rows[i].FindControl("txtProOrderBy") as TextBox;
                            if (PhdnId == null || item.ProductId != Guid.Parse(PhdnId.Value)) continue;
                            PchkCheck.Checked = true;
                            if (txtProOrderBy != null) txtProOrderBy.Text = item.OrderBy.ToString();
                        }
                        for (int i = 0; i < grdSpecialTrain.Rows.Count; i++)
                        {
                            var sTchkCheck = grdSpecialTrain.Rows[i].FindControl("chkCheck") as CheckBox;
                            var sThdnId = grdSpecialTrain.Rows[i].FindControl("hdnId") as HiddenField;
                            var txtSptOrder = grdSpecialTrain.Rows[i].FindControl("txtSPTOrderBy") as TextBox;
                            if (sThdnId == null || item.SpecialTrainId != Guid.Parse(sThdnId.Value)) continue;
                            sTchkCheck.Checked = true;
                            if (txtSptOrder != null) txtSptOrder.Text = item.OrderBy.ToString();
                        }
                    }
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        private void ClearControls()
        {
            for (int i = 0; i < grdProduct.Rows.Count; i++)
            {
                CheckBox chkCheck = grdProduct.Rows[i].FindControl("chkCheck") as CheckBox;
                if (chkCheck.Checked)
                    chkCheck.Checked = false;
            }
            for (int i = 0; i < grdSpecialTrain.Rows.Count; i++)
            {
                CheckBox chkCheck = grdSpecialTrain.Rows[i].FindControl("chkCheck") as CheckBox;
                if (chkCheck.Checked)
                    chkCheck.Checked = false;
            }
            ddlHashTag.SelectedIndex = 0;
        }

        private void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("HashtagMappings.aspx");
        }

        private void BindHasTagMapping()
        {
            try
            {
                grdHashTagMapping.DataSource = _master.GetHashtagList().Where(x => x.IsActive == true).OrderBy(x => x.Title).ToList();
                grdHashTagMapping.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        protected void grdHashTagMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdHashTagMapping.PageIndex = e.NewPageIndex;
                BindHasTagMapping();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }

        protected void grdHashTagMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                var id = Convert.ToInt32(e.CommandArgument.ToString());
                _master.DeleteHashtagMappingByHashId(id);
                ShowMessage(1, "Record deleted successfully.");
            }
            BindHasTagMapping();
        }

        protected void ddlHashTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlHashTag.SelectedIndex > 0)
                EditHashTagMapping(Convert.ToInt64(ddlHashTag.SelectedValue));
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
        }

        protected void ddlSites_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            EditHashTagMapping(Convert.ToInt64(ddlHashTag.SelectedValue));
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
        }

        private void FillCategory()
        {
            try
            {
                _SiteID = Master.SiteID;
                const int treeLevel = 3;
                ddlCategory.DataSource = _oProduct.GetAssociateCategoryList(treeLevel, _SiteID);
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataBind();
            }
            catch (Exception ex) { throw ex; }
        }

        private void FillCountry()
        {
            try
            {
                _SiteID = Master.SiteID;
                ddlCountry.DataSource = _master.GetCountryList(_SiteID).ToList();
                ddlCountry.DataTextField = "Name";
                ddlCountry.DataValueField = "Id";
                ddlCountry.DataBind();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
public class ProductIds
{
    public int? OrderBy { get; set; }
    public Guid ProductId { get; set; }
}

public class SpecialTrainIds
{
    public int? OrderBy { get; set; }
    public Guid SpecialTrainId { get; set; }
}
