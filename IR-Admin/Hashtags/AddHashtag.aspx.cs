﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin.Hashtags
{
    public partial class AddHashtag : System.Web.UI.Page
    {
        readonly ManageHashTag _master = new ManageHashTag();
        readonly Masters _Master = new Masters();
        public string tab = string.Empty;
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];
        readonly db_1TrackEntities _db = new db_1TrackEntities();

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!Page.IsPostBack)
            {
                BindHashtagList();
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    tab = "2";
                    btnSubmit.Text = "Update";
                    BindHashtagById(Convert.ToInt32(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>ResetDiv();</script>", false);
                }
            }
            ShowMessage(0, null);
        }

        public void BindHashtagList()
        {
            try
            {
                tab = "1";
                grdHashTag.DataSource = _master.GetHashtagList().OrderBy(x => x.Title).ToList();
                grdHashTag.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindHashtagById(int id)
        {
            try
            {
                var result = _master.GetHashtagById(id);
                if (result != null)
                {
                    txtTitle.Text = result.Title;
                    chkactive.Checked = Convert.ToBoolean(result.IsActive);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (imgUpload.HasFile)
                {
                    bool rs = UploadFile();
                    if (rs)
                    {
                        ShowMessage(1, "Hashtag uploaded successfully.");
                    }
                }
                BindHashtagList();
                tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, "Hashtag uploading failed!");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSubmit.Text == "Submit")
                {
                    _master.AddHashTag(new Hashtag()
                    {
                        Title = txtTitle.Text.Trim(),
                        IsActive = chkactive.Checked
                    });
                    BindHashtagList();
                    ClearControls();
                    tab = "1";
                }
                else if (btnSubmit.Text == "Update")
                {
                    _master.UpdateHashTag(new Hashtag()
                    {
                        Id = Convert.ToInt32(Request["id"]),
                        Title = txtTitle.Text,
                        IsActive = chkactive.Checked
                    });

                    ShowMessage(1, "Record updated successfully.");
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                    BindHashtagList();
                    ClearControls();
                    tab = "1";
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        void ClearControls()
        {
            txtTitle.Text = string.Empty;
            chkactive.Checked = false;
            hdnId.Value = string.Empty;
            btnSubmit.Text = "Submit";
            tab = "1";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddHashtag.aspx");
        }
        
        protected void grdHashTag_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                var id = Convert.ToInt32(e.CommandArgument.ToString());
                _master.DeleteHashtagMappingByHashId(id);
                _master.DeleteHashTag(id);
                ShowMessage(1, "Record deleted successfully.");
            }
            if (e.CommandName == "ActiveInActive")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                _master.ActiveInactiveHashTag(id);
            }
            BindHashtagList();
        }

        protected bool UploadFile()
        {
            try
            {
                string path;
                Guid id = Guid.NewGuid();
                string[] ext = new string[] { ".XLS", ".XLSX" };
                if (imgUpload.HasFile)
                {
                    if (imgUpload.PostedFile.ContentLength > 2097152)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('Uploaded Hashtag file is larger up to 2Mb.')</script>", false);
                        return false;
                    }
                    string fExt = imgUpload.FileName.Substring(imgUpload.FileName.LastIndexOf("."));
                    string fileExt = fExt;
                    if (!ext.Contains(fileExt.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    fileExt = id + fileExt;
                    path = "~/Hashtags/Tags/" + fileExt;
                    if (System.IO.File.Exists(Server.MapPath(path)))
                        System.IO.File.Delete(Server.MapPath(path));
                    else
                    {
                        imgUpload.SaveAs(Server.MapPath(path));
                        string dircPath = Server.MapPath(path);
                        int result = _master.UploadHashTagFromExcel(dircPath);
                        if (result == 0)
                            throw new Exception("Hashtag uploading failed!");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void grdHashTag_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdHashTag.PageIndex = e.NewPageIndex;
                BindHashtagList();
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }
}