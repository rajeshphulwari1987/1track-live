﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AddHashtag.aspx.cs" Inherits="IR_Admin.Hashtags.AddHashtag" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/Tab/jquery.js"></script>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">
        $(function () {                  
            if(<%=tab%>=="1")
            {
                $("ul.list").tabs("div.panes > div");
                $("ul.tabs").tabs("div.inner-tabs-container > div");                 
            }  
            else
            {
                $("ul.tabs").tabs("div.inner-tabs-container > div");               
            }         
        }); 
 
        function ResetDiv() {
            document.getElementById('MainContent_aNew').className = 'current';
            document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'Block';
            document.getElementById('MainContent_divNew1').style.display = 'Block';
        }
    
    </script>
    <script type="text/javascript">
        window.setTimeout("closeDiv();", 50000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <style type="text/css">
        .colum-two
        {
            width: 75% !important;
            margin-top: 8px !important;
        }
        .colum-one
        {
            margin-left: 15px !important;
            margin-top: 8px !important;
            height: 35px !important;
        }
        .heading
        {
            width: 962px !important;
        }
        .newcol
        {
            border-bottom: 1px dashed #b1b1b1;
            line-height: 35px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
       Add Hash Tag Keywords
    </h2>
    <div class="full mr-tp1">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="AddHashtag.aspx" class="current">List</a></li>
            <li><a id="aNew" href="AddHashtag.aspx" class="">New</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdHashTag" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" PageSize="10"
                            AllowPaging="true" OnRowCommand="grdHashTag_RowCommand" OnPageIndexChanging="grdHashTag_PageIndexChanging">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
                                        <%#Eval("Title")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="28%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="AddHashtag.aspx?id=<%#Eval("Id")%>" style="text-decoration: none">
                                            <img title="Edit" alt="Edit" src="../images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"../images/active.png":"../images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True"?"Active":"In-Active" %>' />
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                    </ItemTemplate>
                                    <ItemStyle Width="2%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <asp:HiddenField ID="hdnId" runat="server" />
                <div id="divNew" runat="server" style="display: block;">
                    <div class="heading">
                        <span style="padding-left: 10px;">Add New Keyword</span>
                    </div>
                    <div class="content-in">
                        <div class="colum-one newcol">
                            Keyword Name:
                        </div>
                        <div class="colum-two newcol">
                            <asp:TextBox ID="txtTitle" runat="server" />
                            &nbsp;<asp:RequiredFieldValidator ID="rvTitle" runat="server" ControlToValidate="txtTitle"
                                ValidationGroup="rvSave" ErrorMessage="Enter Hashtag" Text="*" Display="Dynamic"
                                ForeColor="Red" />
                        </div>
                        <div class="clrup">
                        </div>
                        <div class="colum-one newcol">
                            Is Active:
                        </div>
                        <div class="colum-two newcol">
                            <asp:CheckBox ID="chkactive" runat="server" />
                        </div>
                        <div class="clrup">
                        </div>
                        <div class="colum-one">
                            &nbsp;</div>
                        <div class="colum-two" style="margin-bottom: 12px;">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                Text="Submit" Width="89px" ValidationGroup="rvSave" />
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="rvSave" DisplayMode="List"
                                ShowSummary="false" ShowMessageBox="true" runat="server" />
                        </div>
                    </div>
                    <div class="heading" style="margin-top: 12px;">
                        <span style="padding-left: 10px;">OR Upload Keywords</span>
                    </div>
                    <div class="content-in">
                        <div class="colum-one newcol">
                            Select File:
                        </div>
                        <div class="colum-two newcol">
                            <asp:FileUpload ID="imgUpload" runat="server" />
                        </div>
                        <div class="clrup">
                        </div>
                        <div class="colum-one newcol" style="height: 123px !important;">
                            &nbsp;</div>
                        <div class="colum-two newcol">
                            <div class="file-container" style="padding: 33px">
                                <b>File Format:</b> .XLS,.XLSX
                            </div>
                            <div class="file-container" style="width: 240px; height: 100px;">
                                <a href="Tags/HashTagKeywordSample.xlsx">
                                    <img src="../images/product-temp.png" /><br />
                                    Download HashTag Upload Sample </a>
                            </div>
                        </div>
                        <div class="clrup">
                        </div>
                        <div class="colum-one" style="padding-bottom: 10px;">
                            &nbsp;</div>
                        <div class="colum-two">
                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Width="89px"
                                OnClick="btnUpload_Click" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </div>
                        <div class="clrup">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
