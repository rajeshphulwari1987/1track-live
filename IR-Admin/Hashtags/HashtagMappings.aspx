﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="HashtagMappings.aspx.cs" Inherits="IR_Admin.Hashtags.HashtagMappings" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%--   <script src="../Scripts/Tab/jquery.js" type="text/javascript"></script>--%>
    <link rel="stylesheet" href="../Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/chosen.jquery.js" type="text/javascript"></script>
    <link href="../Scripts/chosen.css" rel="stylesheet" type="text/css" />
    <%-- <script src="../Scripts/DataTable/jquery.dataTables.js" type="text/javascript"></script> --%>
    <script type="text/javascript">
        function ResetDiv() {
            //document.getElementById('MainContent_aNew').className = 'current';
            //document.getElementById('aList').className = ' ';
            document.getElementById('MainContent_divlist').style.display = 'none';
            document.getElementById('MainContent_divNew').style.display = 'block';
        }
        window.setTimeout("closeDiv();", 2000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
        function selectAll(obj) {
            if ($(obj)[0].checked) {
                $('td.chklsites input[type=checkbox]').each(function () {
                    $(this)[0].checked = true;
                });
            } else {
                $('td.chklsites input[type=checkbox]').each(function () {
                    $(this)[0].checked = false;
                });
            }
        }
        function fireCheckChanged(e) {
            var evnt = ((window.event) ? (event) : (e));
            var element = evnt.srcElement || evnt.target;
            if (element.tagName == "INPUT" && element.type == "checkbox" && element.checked == false) {
                __doPostBack("", "");
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <style type="text/css">
        td .orderbyst
        {
            width: 35px;
        }
         td .orderbyp
        {
            width: 35px;
        }
        .viewall
        {
            float: right;
            color: #a82e4b;
            cursor: pointer;
            font-weight: bold;
        }
        .forwhom
        {
            color: #a82e4b;
        }
        .custom-combobox
        {
            position: relative;
            display: inline-block;
        }
        .custom-combobox-toggle
        {
            position: absolute;
            top: 0;
            bottom: 0;
            margin-left: -1px;
            padding: 0;
        }
        .custom-combobox-input
        {
            margin: 0;
            padding: 5px 10px;
        }
        .ui-autocomplete
        {
            font-size: 14px;
            height: 300px;
            overflow-x: hidden;
            overflow-y: auto;
        }
        .custom-combobox-input
        {
            background: #fff;
            width: 300px;
        }
        body
        {
            font-size: 62.5%;
        }
    </style>
    <script type="text/javascript">
        (function ($) {
            $.widget("custom.combobox", {
                _create: function () {
                    this.wrapper = $("<span>")
                        .addClass("custom-combobox")
                        .insertAfter(this.element);

                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },
                _createAutocomplete: function () {
                    var selected = this.element.children(":selected"),
                        value = selected.val() ? selected.text() : "";
                    this.input = $("<input>")
                        .appendTo(this.wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy(this, "_source")
                        })
                        .tooltip({
                            tooltipClass: "ui-state-highlight"
                        });
                    this._on(this.input, {
                        autocompleteselect: function (event, ui) {
                            ui.item.option.selected = true;
                            this._trigger("select", event, {
                                item: ui.item.option
                            });
                        },
                        autocompletechange: "_removeIfInvalid"
                    });
                },
                _createShowAllButton: function () {
                    var input = this.input,
                        wasOpen = false;
                    $("<a>")
                        .attr("tabIndex", -1)
                        .attr("title", "Show All Keywords")
                        .tooltip()
                        .appendTo(this.wrapper)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("custom-combobox-toggle ui-corner-right")
                        .mousedown(function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .click(function () {
                            input.focus();
                            // Close if already visible
                            if (wasOpen) {
                                return;
                            }
                            // Pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                },
                _source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(this.element.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text)))
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }));
                },
                _removeIfInvalid: function (event, ui) {
                    // Selected an item, nothing to do
                    if (ui.item) {
                        return;
                    }
                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                    this.element.children("option").each(function () {
                        if ($(this).text().toLowerCase() === valueLowerCase) {
                            this.selected = valid = true;
                            return false;
                        }
                    });                                        // Found a match, nothing to do
                    if (valid) {
                        return;
                    }
                    // Remove invalid value
                    this.input
                        .val("")
                        .attr("title", value + " didn't match any item")
                        .tooltip("open");
                    this.element.val("");
                    this._delay(function () {
                        this.input.tooltip("close").attr("title", "");
                    }, 1000);
                    this.input.autocomplete("instance").term = "";
                },
                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });
        })(jQuery);
        $(function () {
            $("#ddlHashTag").combobox({
                select: function (event, ui) {
                    var e = document.getElementById("ddlHashTag");
                    if (e.options[e.selectedIndex].value > 0) {
                        window.location.href = "HashtagMappings.aspx?id=" + e.options[e.selectedIndex].value;
                    }
                }
            });
            $("#ddlSites").combobox({
                select: function (event, ui) {
                    var e = document.getElementById("ddlSites");
                    var e1 = document.getElementById("ddlHashTag");
                    if (e.options[e.selectedIndex].value != '0') {
                        window.location.href = "HashtagMappings.aspx?id=" + e1.options[e1.selectedIndex].value + "&sid=" + e.options[e.selectedIndex].value;
                    }
                }
            });
            //
            //MainContent_grdProduct").prepend($("<thead></thead>").append($(this).find("tr:first")));
            ////            $("#MainContent_grdProduct").prepend($("<thead></thead>").append('<tr><th>fff</th><th>fff</th><th>fff</th></tr>'));
            ////            $("#MainContent_grdProduct tbody tr:first").remove();
            ////            $("#MainContent_grdProduct").dataTable();
            //additioal filters for category
            var countryId = "";
            var categoryId = "";
            var countryRange = "";
            $("#ddlCategory").chosen({ width: "95%" });
            $("#ddlCountry").chosen({ width: "95%" });
            $("#ddlCountryRange").chosen({ width: "95%" });
            $('#ddlCategory').on('change', function (evt, params) {
                categoryId = params["selected"];  //do_something(evt, params);
                onchangeCategroy(categoryId);
            });
            $('#ddlCountry').on('change', function (evt, params) {
                //do_something(evt, params);
                countryId = params["selected"];
                onchangeCountry(countryId);
            });
            $('#ddlCountryRange').on('change', function (evt, params) {
                countryRange = params["selected"];
                onchangecountryRange(countryRange);
            }); initforcheckbox(); calculateSelected();
        });
        function onchangeCategroy(fcategoryId) {
            if (fcategoryId == 0) {
                $("#MainContent_grdProduct tr").show();
            } else if (typeof fcategoryId === "undefined") {
                $("#MainContent_grdProduct tr").show();
                $("#st-filter").html('');
                $("#pl-filter").html('');
            } else {
                $('#st-filter').html('');
                $("#pl-filter").html($('#ddlCategory option[value=' + fcategoryId + ']').text());
                $('span.pl').each(function () {
                    if ($(this).attr('data-categoryid') == fcategoryId)
                    { $(this).parent('td').parent('tr').show(); } else
                    { $(this).parent('td').parent('tr').hide(); }
                });
            } $("#chkSpecialTrianAll")[0].checked = false;
            $("#chkSpecialProductAll")[0].checked = false;
        }
        function onchangeCountry(fcountryId) {
            if (fcountryId == 0) {
                $("#MainContent_grdSpecialTrain tr").show();
            } else if (typeof fcountryId === "undefined") {
                $("#MainContent_grdSpecialTrain tr").show();
                $("#st-filter").html('');
                $("#pl-filter").html('');
            } else {
                $('#st-filter').html($('#ddlCountry option[value=' + fcountryId + ']').text());
                $("#pl-filter").html($('#ddlCountry option[value=' + fcountryId + ']').text());
                $('span.st').each(function () {
                    if ($(this).attr('data-countryid') == fcountryId)
                    { $(this).parent('td').parent('tr').show(); } else
                    { $(this).parent('td').parent('tr').hide(); }
                });
            }
            if (fcountryId == 0) {
                $("#MainContent_grdProduct tr").show();
            } else if (typeof fcountryId === "undefined") {
                $("#MainContent_grdProduct tr").show();
            } else {
                $('span.pl').each(function () {
                    var ssss = $(this).attr('data-countryid');
                    if (ssss.includes(fcountryId))
                    { $(this).parent('td').parent('tr').show(); } else
                    { $(this).parent('td').parent('tr').hide(); }
                });
            }
            $("#chkSpecialTrianAll")[0].checked = false;
            $("#chkSpecialProductAll")[0].checked = false;
        }
        function onchangecountryRange(fcountryRange) {
            if (fcountryRange == 0) {
                $("#MainContent_grdProduct tr").show();
            } else if (typeof fcountryRange === "undefined") {
                $("#MainContent_grdProduct tr").show();
                $("#st-filter").html('');
                $("#pl-filter").html('');
            } else {
                $("#pl-filter").html($('#ddlCountryRange option[value=' + fcountryRange + ']').text());
                var fcountryRanges = fcountryRange.split('-');
                var fcountrystartcode = 0;
                var fcountryendcode = 0;
                if (fcountryRanges.length > 1) {
                    fcountrystartcode = fcountryRanges[0];
                    fcountryendcode = fcountryRanges[1];
                } else {
                    fcountrystartcode = fcountryRange;
                    fcountryendcode = fcountryRange;
                }
                $('span.pl').each(function () {
                    if (parseInt($(this).attr('data-countrystartcode')) >= parseInt(fcountrystartcode)
                        && (parseInt($(this).attr('data-countrystartcode')) <= parseInt(fcountryendcode)))
                    { $(this).parent('td').parent('tr').show(); }
                    else
                    { $(this).parent('td').parent('tr').hide(); }
                });
            }
            $("#chkSpecialTrianAll")[0].checked = false;
            $("#chkSpecialProductAll")[0].checked = false;
        }
        function SelectAllSpecialTrain(chk) {
            $('span.st').each(function () {
                if ($(this).parent('td').parent('tr').is(":visible")) {
                    $(this).children('input[type="checkbox"]')[0].checked = $(chk).is(':checked');
                }
            }); calculateSelected();
        }
        function SelectAllProduct(chk) {
            $('span.pl').each(function () {
                if ($(this).parent('td').parent('tr').is(":visible")) {
                    $(this).children('input[type="checkbox"]')[0].checked = $(chk).is(':checked');
                }
            }); calculateSelected();
        }
        function ViewAllSepcialTrains() {
            $("#st-filter").html('');
            $("#MainContent_grdSpecialTrain tr").show();
        }
        function ViewAllProducts() {
            $("#pl-filter").html('');
            $("#MainContent_grdProduct tr").show();
        }
        function uncheckRelative(obj) {
            if ($(obj).attr('container')[0].id == "ddlCountry_chosen") {
                var selID = document.getElementById("ddlCountry");
                var value1 = selID.options[$(obj).attr('current_selectedIndex')].value;
                $('span.st[data-countryid="' + value1 + '"]').each(function () {
                    $(this).children('input[type="checkbox"]')[0].checked = false;
                });
                $('span.pl[data-countryid="' + value1 + '"]').each(function () {
                    $(this).children('input[type="checkbox"]')[0].checked = false;
                });
            } else if ($(obj).attr('container')[0].id == "ddlCategory_chosen") {
                var selID = document.getElementById("ddlCategory");
                var value1 = selID.options[$(obj).attr('current_selectedIndex')].value;
                $('span.pl[data-categoryid="' + value1 + '"]').each(function () {
                    $(this).children('input[type="checkbox"]')[0].checked = false;
                });
            } else {
                var selID = document.getElementById("ddlCountryRange");
                var value1 = selID.options[$(obj).attr('current_selectedIndex')].value;
                var fcountryRanges = value1.split('-');
                var fcountrystartcode = 0;
                var fcountryendcode = 0;
                if (fcountryRanges.length > 1) {
                    fcountrystartcode = fcountryRanges[0];
                    fcountryendcode = fcountryRanges[1];
                } else {
                    fcountrystartcode = fcountryRanges[0];
                    fcountryendcode = fcountryRanges[0];
                }
                $('span.pl').each(function () {
                    if (parseInt($(this).attr('data-countrystartcode')) >= parseInt(fcountrystartcode)
                        && (parseInt($(this).attr('data-countrystartcode')) <= parseInt(fcountryendcode)))
                    { $(this).children('input[type="checkbox"]')[0].checked = false; }
                });
            } calculateSelected();
        }
        //validtaion
        var valid = false;
        function CheckforAtLeast() {
            var e = document.getElementById("ddlSites");
            if (e.options[e.selectedIndex].value != '0')
            { valid = true; } else {
                valid = false;
                alert('Please select site.');
            }
            return valid;
        }
        //checked product
        function initforcheckbox() {
            $('span.st input[type="checkbox"]').change(function () {
                calculateSelected();
            });
            $('span.pl input[type="checkbox"]').change(function () {
                calculateSelected();
            });
        }
        function calculateSelected() {
            $("#STC").html($('span.st input[type="checkbox"]:checked').length);
            $("#PC").html($('span.pl input[type="checkbox"]:checked').length);
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Hash Tag Keyword Mapping</h2>
    <div class="full mr-tp1">
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <ul class="list">
            <li><a id="aList" href="HashtagMappings.aspx" class="current">List</a></li>
            <li><a id="aNew" href="HashtagMappings.aspx" class="" style="display: none;">New</a></li>
        </ul>
        <div class="full mr-tp1">
            <div class="panes">
                <div id="divlist" runat="server" style="display: block;">
                    <div class="crushGvDiv">
                        <asp:GridView ID="grdHashTagMapping" runat="server" CellPadding="4" CssClass="grid-head2"
                            OnRowCommand="grdHashTagMapping_RowCommand" ForeColor="#333333" GridLines="None"
                            AutoGenerateColumns="False" PageSize="10" OnPageIndexChanging="grdHashTagMapping_PageIndexChanging"
                            AllowPaging="true">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <PagerStyle CssClass="paging"></PagerStyle>
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
                                        <%#Eval("Title")%>
                                    </ItemTemplate>
                                    <ItemStyle Width="28%"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href="HashtagMappings.aspx?id=<%#Eval("Id")%>" style="text-decoration: none">
                                            <img title="Edit" alt="Edit" src="../images/edit.png" />
                                        </a>
                                        <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                            CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/delete.png"
                                            OnClientClick="return confirm('Are you sure you want to remove this item mapping?')" />
                                    </ItemTemplate>
                                    <ItemStyle Width="2%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" runat="server" style="display: none;">
                    <table class="tblMainSection">
                        <tr class="grid-sec2">
                            <td style="width: 100%; vertical-align: top;">
                                <div class="inner-tabs-container">
                                    <div id="tabs-inner-1" style="display: block; width: 100%;">
                                        <table class="tblMainSection">
                                            <tr>
                                                <td class="col-lg-2">
                                                    <b>Select or Find Keyword:</b>
                                                </td>
                                                <td>
                                                    <div style="overflow-y: auto; width: 690px;">
                                                        <asp:DropDownList ID="ddlHashTag" runat="server" ClientIDMode="Static" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlHashTag_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    For Bulk Mapping you may select category and country filter's
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-lg-2">
                                                    <b>Select Categories for Products:</b>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCategory" runat="server" TabIndex="-1" ClientIDMode="Static"
                                                        class="chosen-select" multiple="" data-placeholder="Select Categories">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-lg-2">
                                                    <b>Select Country for Special Trains and Products:</b>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCountry" runat="server" TabIndex="-1" ClientIDMode="Static"
                                                        class="chosen-select" multiple="" data-placeholder="Select Countries">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-lg-2">
                                                    <b>Select Country Combination Passes:</b>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCountryRange" runat="server" TabIndex="-1" ClientIDMode="Static"
                                                        class="chosen-select" multiple="" data-placeholder="Select  Country Combination Passes">
                                                        <%--  <asp:ListItem Value="0">--Select Country Pass--</asp:ListItem>--%>
                                                        <asp:ListItem Value="9999">Global</asp:ListItem>
                                                        <asp:ListItem Value="2001-3000">Regional Pass</asp:ListItem>
                                                        <asp:ListItem Value="1001-2000">One Country Pass</asp:ListItem>
                                                        <asp:ListItem Value="3001-4000"> Select Pass 3 Country</asp:ListItem>
                                                        <asp:ListItem Value="4001-5000"> Select Pass 4 Country </asp:ListItem>
                                                        <asp:ListItem Value="5001-9998"> Select Pass 5 Country</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <%--  <td valign="top" style="width: 30%; border-left: 1px solid #ccc">
                                <b>&nbsp;Select Site</b>
                                <div style="width: 95%; height: 350px; overflow-y: auto;">
                                    <input type="checkbox" value="Select / DeSelect All" class="chkSites" onchange="selectAll(this)" />Select/DeSelect
                                    All
                                    <asp:TreeView ID="trSites" runat="server" ShowCheckBoxes="All" OnTreeNodeCheckChanged="trSites_OnTreeNodeCheckChanged">
                                        <NodeStyle ChildNodesPadding="5px" CssClass="chklsites" />
                                    </asp:TreeView>
                                </div>
                            </td>--%>
                        </tr>
                        <tr class="grid-sec2">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td style="width: 40%; vertical-align: top;">
                                            <table>
                                                <tr>
                                                    <td style="font-size: 15px;">
                                                        <b>Special Trains:</b> <span id="st-filter" class="forwhom"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 15px;">
                                                        Total selected Special Trains: <span id="STC" class=""></span><span onclick="ViewAllSepcialTrains()"
                                                            class="viewall">View All</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="overflow-y: auto; height: 400px; width: 400px;">
                                                            <asp:GridView ID="grdSpecialTrain" runat="server" CellPadding="4" CssClass="grid-head2"
                                                                ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Width="100%">
                                                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Special Trains" ItemStyle-Width="75%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("Id")%>'></asp:HiddenField>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Select" ItemStyle-Width="25%" 
                                                                        ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkSpecialTrianAll" runat="server" Text="Select All"  ClientIDMode="Static"
                                                                                onclick="javascript:SelectAllSpecialTrain(this);" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkCheck" runat="server" data-countryid='<%#Eval("CountryID")%>' CssClass="st"                                                                                ClientIDMode="Static"></asp:CheckBox>
                                                                            <asp:TextBox runat="server" ID="txtSPTOrderBy"  CssClass="orderbyst" onkeypress="return isNumberKey(event)" ></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 60%; vertical-align: top;">
                                            <table>
                                                <tr>
                                                    <td style="font-size: 15px;">
                                                        <b>Products:</b> <span id="pl-filter" class="forwhom"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 15px;">
                                                        Total selected Products: <span id="PC" class=""></span><span onclick="ViewAllProducts()"
                                                            class="viewall">View All</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="overflow-y: auto; height: 400px; width: 550px;">
                                                            <asp:GridView ID="grdProduct" runat="server" CellPadding="4" CssClass="gvv grid-head2"
                                                                ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Width="100%">
                                                                <AlternatingRowStyle BackColor="#FBDEE6" />
                                                                <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                                                                <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                                                    BorderColor="#FFFFFF" BorderWidth="1px" />
                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Product Name" ItemStyle-Width="48%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("ProductName")%>' CssClass='<%#Eval("ProductId")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("ProductId")%>'></asp:HiddenField>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Category" ItemStyle-Width="35%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCategoryName" runat="server" Text='<%#Eval("CategoryName")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Select"  ItemStyle-Width="17%"
                                                                        ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkSpecialProductAll" runat="server" Text="Select All" ClientIDMode="Static"
                                                                                onclick="javascript:SelectAllProduct(this);" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkCheck" runat="server" data-categoryid='<%#Eval("CategoryID") %>'
                                                                                data-countryid='<%#Eval("CountryIDs") %>' data-countrystartcode='<%#Eval("CountryStartCode") %>'
                                                                                ClientIDMode="Static" CssClass="pl"></asp:CheckBox>
                                                                                  <asp:TextBox runat="server" ID="txtProOrderBy"  CssClass="orderbyp" onkeypress="return isNumberKey(event)" ></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="grid-sec2">
                            <td style="text-align: center; padding-left: 400px;">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" Width="89px"
                                    ValidationGroup="rv" OnClick="btnSubmit_Click" OnClientClick="return CheckforAtLeast();" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
