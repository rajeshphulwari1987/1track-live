﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.IO;

namespace IR_Admin
{
    public partial class SpecialTrains : Page
    {
        private readonly Masters _oMasters = new Masters();
        private readonly ManageSpecialTrains _oManageTrain = new ManageSpecialTrains();
        public string tab = string.Empty; 
        string cImgpath, bImgpath = string.Empty;
        string bannerpath = string.Empty;
        int pageSize = 15;

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            tab = "1";
            _SiteID = Guid.Parse(selectedValue);
            BindList(_SiteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            tab = "1";
            if (!IsPostBack)
            {
                PageLoadEvent();
                _SiteID = Master.SiteID;
                BindPager();
            }
        }

        private void PageLoadEvent()
        {
            ddlCountry.DataSource = null;
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            var contList = _oMasters.GetContinentList().Where(x => x.IsActive).ToList();
            ddlContinent.DataSource = contList != null && contList.Count > 0 ? contList : null;
            ddlContinent.DataTextField = "Name";
            ddlContinent.DataValueField = "ID";
            ddlContinent.DataBind();
            ddlContinent.Items.Insert(0, new ListItem("--Select Continent--", "0"));

            ListddlContinent.DataSource = contList != null && contList.Count > 0 ? contList : null;
            ListddlContinent.DataTextField = "Name";
            ListddlContinent.DataValueField = "ID";
            ListddlContinent.DataBind();
            ListddlContinent.Items.Insert(0, new ListItem("--All Continent--", "0"));

            ListddlCountry.DataSource = null;
            ListddlCountry.DataBind();
            ListddlCountry.Items.Insert(0, new ListItem("--All Country--", "0"));

            IEnumerable<tblSite> objSite = _oMasters.GetActiveSiteList();
            foreach (var oSite in objSite)
            {
                var trCat = new TreeNode { Text = oSite.DisplayName, Value = oSite.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
                trSites.Nodes.Add(trCat);
            }
        }

        protected void ListddlContinent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["PageIndex"] = 0;
            ShowMessage(0, "");
            ListddlCountry.Items.Clear();
            if (ListddlContinent.SelectedValue == "0")
            {
                ListddlCountry.Enabled = false;
                ListddlCountry.Items.Insert(0, new ListItem("--All Country--", "0"));
                BindPager();
                return;
            }
            else
            {
                var countryList = _oMasters.GetCountryListByContinetId(Guid.Parse(ListddlContinent.SelectedValue)).ToList();
                ListddlCountry.DataSource = countryList != null && countryList.Count > 0 ? countryList : null;
                ListddlCountry.DataTextField = "CountryName";
                ListddlCountry.DataValueField = "CountryID";
                ListddlCountry.DataBind();
                ListddlCountry.Enabled = true;
                ListddlCountry.Items.Insert(0, new ListItem("--All Country--", "0"));
                BindPager();
            }

        }

        protected void ListddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["PageIndex"] = 0;
                BindPager();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        private void BindList(Guid siteId, int start, int end)
        {
            try
            {
                Guid CountryId = Guid.Empty;
                Guid ContinentId = Guid.Empty;
                if (siteId == new Guid())
                    siteId = Master.SiteID;
                if (ListddlContinent.SelectedValue != "0")
                    ContinentId = Guid.Parse(ListddlContinent.SelectedValue);
                if (ListddlCountry.SelectedValue != "0")
                    CountryId = Guid.Parse(ListddlCountry.SelectedValue);
                var list = _oManageTrain.GetSpecialTrainList(siteId, start, end, CountryId, ContinentId);
                dtlSpecialTrains.DataSource = list != null && list.Count > 0 ? list : null;
                dtlSpecialTrains.DataBind();                
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
            tab = "1";
            divNew.Attributes.Add("style","display:block");
        }

        #region Save And Cancel Events
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool check = false;
            foreach (TreeNode node in trSites.Nodes)
            {
                if (node.Checked)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                tab = "2";
                ShowMessage(2, "Please select at least one site.");
                return;
            }
            else
            {
                bool result = UploadFile();
                if (result)
                    AddEditSpecialTrain();
                ClearControls();
                tab = "1";
            }
        }

        private void ClearControls()
        {
            txtName.Text = string.Empty;
            txtTitle.Text = string.Empty;
            txtContent.InnerText = string.Empty;
            ddlContinent.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            chkIsActv.Checked = false;
            UncheckTree();
        }

        void UncheckTree()
        {
            foreach (TreeNode node in trSites.Nodes)
            {
                node.Checked = false;
            }
        }

        public void AddEditSpecialTrain()
        {
            try
            {
                var id = _oManageTrain.AddEditSpecialTrain(new tblSpecialTrain
                {
                    ID = string.IsNullOrEmpty(hdnTrainId.Value) ? new Guid() : Guid.Parse(hdnTrainId.Value),
                    ContinentID = Guid.Parse(ddlContinent.SelectedValue),
                    CountryID = Guid.Parse(ddlCountry.SelectedValue),
                    CountryImg = cImgpath,
                    BannerImg = bannerpath,
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    Description = txtContent.InnerHtml,
                    IsActive = chkIsActv.Checked,
                    Name = txtName.Text.Trim(),
                    Title = txtTitle.Text.Trim()
                });

                var lstTrainSiteLookup = new List<tblSpecialTrainSiteLookUp>();
                foreach (TreeNode node in trSites.Nodes)
                {
                    if (node.Checked)
                    {
                        lstTrainSiteLookup.Add(new tblSpecialTrainSiteLookUp
                        {
                            SiteID = Guid.Parse(node.Value),
                            SpecialTrainID = id
                        });
                    }
                }
                if (lstTrainSiteLookup.Count > 0)
                {
                    _oManageTrain.AddEditSpecialTrainSiteLookup(lstTrainSiteLookup);
                }
                BindList(_SiteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
                tab = "1";
                ShowMessage(1, string.IsNullOrEmpty(hdnTrainId.Value) ? "Special Train added successfully." : "Special Train updated successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("SpecialTrains.aspx");
        }

        protected bool UploadFile()
        {
            try
            {
                var id = Guid.NewGuid();
                var ext = new[] { ".JPEG", ".JPG", ".GIF", ".BMP", ".PNG", ".PGM", ".PBM", ".PNM", ".PFM", ".PPM" };
                if (fupCountryImg.HasFile)
                {
                    if (fupCountryImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Country Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string FileName = fupCountryImg.FileName.Substring(fupCountryImg.FileName.LastIndexOf("."));
                    if (!ext.Contains(FileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    FileName = id + FileName;
                    cImgpath = "~/Uploaded/SPCountryImg/" + FileName;
                    if (File.Exists(Server.MapPath(cImgpath)))
                        File.Delete(Server.MapPath(cImgpath));
                    else
                        fupCountryImg.SaveAs(Server.MapPath(cImgpath));
                }

                if (fupBannerImg.HasFile)
                {
                    if (fupBannerImg.PostedFile.ContentLength > 1048576)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('Uploaded Banner Image is larger up to 1Mb.')</script>", false);
                        return false;
                    }

                    string FileName = fupBannerImg.FileName.Substring(fupBannerImg.FileName.LastIndexOf("."));
                    if (!ext.Contains(FileName.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    FileName = id + FileName;
                    bannerpath = "~/Uploaded/SPBannerImg/" + FileName;
                    if (File.Exists(Server.MapPath(bannerpath)))
                        File.Delete(Server.MapPath(bannerpath));
                    else
                        fupBannerImg.SaveAs(Server.MapPath(bannerpath));
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        protected void dtlSpecialTrains_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                hdnTrainId.Value = e.CommandArgument.ToString();
                switch (e.CommandName)
                {
                    case "Modify":
                        btnSubmit.Text = "Update";
                        var result = _oManageTrain.GetSpecialTrainById(Guid.Parse(hdnTrainId.Value));
                        var resultSite = _oManageTrain.GetSpecialTrainSiteIdListById(Guid.Parse(hdnTrainId.Value));

                        txtContent.InnerHtml = result.Description;
                        txtName.Text = result.Name;
                        txtTitle.Text = result.Title;
                        ddlContinent.SelectedValue = result.ContinentID.ToString();
                        ddlContinent_SelectedIndexChanged(source, e);
                        imgCountry.Src = result.CountryImg;
                        imgBanner.Src = result.BannerImg;
                        var item = ddlCountry.Items.FindByValue(result.CountryID.ToString());
                        if (item != null)
                            ddlCountry.SelectedValue = result.CountryID.ToString();
                        chkIsActv.Checked = (bool)result.IsActive;
                        cImgpath = result.CountryImg;
                        bannerpath = result.BannerImg;

                        foreach (var sItem in resultSite)
                        {
                            foreach (TreeNode node in trSites.Nodes)
                            {
                                if (node.Value == sItem.ToString())
                                    node.Checked = true;
                            }
                        }
                        divlist.Attributes.Add("style", "display:none");
                        divNew.Attributes.Add("style", "display:block");
                        break;
                    case "Remove":

                        /*Deleted Image Path for this record*/
                        var img = _oManageTrain.GetSpecialTrainById(Guid.Parse(hdnTrainId.Value)).CountryImg;
                        var bimg = _oManageTrain.GetSpecialTrainById(Guid.Parse(hdnTrainId.Value)).BannerImg;
                        cImgpath = "~/" + img;
                        bImgpath = "~/" + bimg;

                        RemoveReadOnlyFile();

                        if (File.Exists(Server.MapPath(cImgpath)))
                            File.Delete(Server.MapPath(cImgpath));

                        if (File.Exists(Server.MapPath(bImgpath)))
                            File.Delete(Server.MapPath(bImgpath));
                        cImgpath = string.Empty;
                        bImgpath = string.Empty;

                        /*Deleted Record*/
                        bool rec = _oManageTrain.DeleteSpecialTrain(Guid.Parse(hdnTrainId.Value));
                        if (rec)
                            ShowMessage(1, "Record deleted successfully");
                        BindList(_SiteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
                        tab = "1";
                        hdnTrainId.Value = string.Empty;

                        break;

                    case "ActiveInActive":
                        _oManageTrain.ActiveInactiveSpecialTrain(Guid.Parse(hdnTrainId.Value));
                        BindList(_SiteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
                        tab = "1";
                        hdnTrainId.Value = string.Empty;

                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                tab = "1";
            }
        }

        #region Paging
        public void BindPager()
        {
            /*Bind Pager*/
            int newpagecount = 0;
            List<ClsPageCount> oPageList = new List<ClsPageCount>();

            Guid CountryId = Guid.Empty;
            Guid ContinentId = Guid.Empty;
            if (_SiteID == new Guid())
                _SiteID = Master.SiteID;
            if (ListddlContinent.SelectedValue != "0")
                ContinentId = Guid.Parse(ListddlContinent.SelectedValue);
            if (ListddlCountry.SelectedValue != "0")
                CountryId = Guid.Parse(ListddlCountry.SelectedValue);
            int cnt = _oManageTrain.TotalNumberOfRecord(_SiteID, CountryId, ContinentId);

            newpagecount = cnt / pageSize + ((cnt % pageSize) > 0 ? 1 : 0);

            for (int i = 1; i <= newpagecount; i++)
            {
                ClsPageCount oPage = new ClsPageCount();
                oPage.PageCount = i.ToString();
                oPageList.Add(oPage);
            }
            DLPageCountItem.DataSource = oPageList;
            DLPageCountItem.DataBind();
            BindList(_SiteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);

            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                LinkButton lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage != null)
                    lblPage.Attributes.Add("class", "activepaging");
                break;
            }
        }
        protected void lnkPage_Command(object sender, CommandEventArgs e)
        {
            int PageIndex = 0;
            PageIndex = Convert.ToInt32(e.CommandArgument) - 1;

            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                LinkButton lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (PageIndex + 1).ToString())
                {
                    lblPage.Attributes.Add("class", "activepaging");
                }
                else
                {
                    lblPage.Attributes.Remove("class");
                }
            }
            ViewState["PageIndex"] = PageIndex;

            BindList(_SiteID, ViewState["PageIndex"] == null ? 0 : pageSize * int.Parse(ViewState["PageIndex"].ToString()), pageSize);
        }
        #endregion

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddlContinent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowMessage(0, "");
            ddlCountry.Items.Clear();

            if (ddlContinent.SelectedValue == "0")
            {
                ddlCountry.Enabled = false;
                ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
                tab = "2";
                return;
            }
            else
            {
                var countryList = _oMasters.GetCountryListByContinetId(Guid.Parse(ddlContinent.SelectedValue)).ToList();
                ddlCountry.DataSource = countryList != null && countryList.Count > 0 ? countryList : null;
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Enabled = true;
                ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
                tab = "2";
            }

        }

        public void RemoveReadOnlyFile()
        {
            const string cntPath = "~/Uploaded/SPCountryImg/";
            foreach (string cntImg in Directory.GetFiles(Server.MapPath(cntPath), "*.*", SearchOption.AllDirectories))
            {
                new FileInfo(cntImg) { Attributes = FileAttributes.Normal };
            }

            const string bannerPath = "~/Uploaded/SPBannerImg/";
            foreach (string bannerImg in Directory.GetFiles(Server.MapPath(bannerPath), "*.*", SearchOption.AllDirectories))
            {
                new FileInfo(bannerImg) { Attributes = FileAttributes.Normal };
            }
        }
    }
}