﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="ContactUs.aspx.cs"
    Inherits="ContactUs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#aContact").addClass("active");

            var eMail = ($('#contactEmailTxt').html());
            function extractEmails(text) {
                return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
            }

            if (eMail.indexOf('@') > "-1") {
                document.getElementById("MainContent_hdnEmail").value = extractEmails(eMail).join('\n');
            }
        });
    </script>
    <style type="text/css">
        .clsAbs
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
                return true;
            return false;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".in-box").click(function () {
                $(this).val("");
            });

            $(".in-box").blur(function () {
                $(this).val(this.value);
            });
        });       
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%--Banner section--%>
    <div id="dvBanner" runat="server" class="banner">
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%=SiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content-inner">
 
<div id="divSta" style="float:left;width:658px" runat="server">
   <div id="ContentHead" runat="server"></div>
   <div id="ContentText" runat="server"></div>
  
<div class="formblock" style="width:40%;">
    <div>
        <asp:TextBox ID="txtName" class="in-box" runat="server" Text="Your Name" />
        <asp:RequiredFieldValidator ID="rfNm" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtName" ValidationGroup="vs"/>
        <asp:RequiredFieldValidator ID="rfNm1" Display="Dynamic" runat="server" ErrorMessage="*" InitialValue="Your Name" ForeColor="Red" ControlToValidate="txtName" ValidationGroup="vs"/>
    </div>
    <div>
        <asp:TextBox ID="txtEmail" class="in-box" runat="server" value="Email Address"/>
        <asp:RequiredFieldValidator ID="rfEmail" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtEmail" ValidationGroup="vs"/>
        <asp:RequiredFieldValidator ID="rfEmail1" Display="Dynamic" runat="server" InitialValue="Email Address" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtEmail" ValidationGroup="vs"/>
        <asp:RegularExpressionValidator ID="reEmail" Display="Dynamic" runat="server" ControlToValidate="txtEmail" ForeColor="Red" Font-Size="12px"
        ErrorMessage="Invalid Email Address" ValidationExpression="^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(Email Address)$" ValidationGroup="vs"/>
   </div>
    <div>
        <asp:TextBox ID="txtPhn" class="in-box" runat="server" value="Phone No" MaxLength="15"/>
        <asp:RequiredFieldValidator ID="rfPhone" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPhn" ValidationGroup="vs"/>
        <asp:RequiredFieldValidator ID="rfPhone1" Display="Dynamic" InitialValue="Phone No" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPhn" ValidationGroup="vs"/>
    </div>
    <div>
        <asp:TextBox ID="txtOrderNo" class="in-box" runat="server" value="Order number (if applicable)"/>
        <asp:RequiredFieldValidator ID="rfOrder" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtOrderNo" ValidationGroup="vs"/>
    </div>
    <div>
    <asp:TextBox ID="txtDesp" class="txtarea-box" runat="server" TextMode="MultiLine" />
    <asp:HiddenField ID="hdnEmail" runat="server"/>
    <asp:Button id="btnSubmit" class="f-left w184" runat="server" Text="SUBMIT" 
            ValidationGroup="vs" OnClick="btnSubmit_Click" />
    </div>
</div>
<div id="ContactPanel" class="address-block" runat="server">
</div>
</div>
<div id="callBlock" class="map-block" style="float:right;" runat="server">
</div>
</section>
</asp:Content>
