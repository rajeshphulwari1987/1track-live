﻿#region Using
using System;
using System.Web.UI.WebControls;
using Business;
#endregion

namespace IR_Admin
{
    public partial class Quebit : System.Web.UI.Page
    {
        readonly Masters _master = new Masters();
        public string Tab = string.Empty;

        #region [ Page InIt must write on every page of CMS ]
        Guid _siteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _siteID = Guid.Parse(selectedValue);
            BindGrid();
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            if (Request["id"] != null)
                Tab = "2";

            if (!Page.IsPostBack)
            {
                _siteID = Master.SiteID;
                Fillddl();
                BindGrid();
            }
        }

        public void Fillddl()
        {
            try
            {
                ddlSite.DataSource = _master.GetActiveSiteList();
                ddlSite.DataTextField = "DisplayName";
                ddlSite.DataValueField = "ID";
                ddlSite.DataBind();
                ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        public void BindGrid()
        {
            try
            {
                grdQubitScript.DataSource = _master.GetQubitScriptAllList();
                grdQubitScript.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(1, ex.Message);
            }
        }

        protected void grdQubitScript_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var id = Guid.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "Remove")
                {
                    var res = _master.DeleteQubitScript(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    BindGrid();
                }
                if (e.CommandName == "Modify")
                {
                    var res = _master.GetQubitScriptById(id);
                    if (res != null)
                    {
                        hdnID.Value = res.ID.ToString();
                        chkAcive.Checked = res.IsActive;
                        txtScript.Text = res.Script;
                        ddlSite.SelectedValue = res.SiteID.ToString();
                        ddlSite.Enabled = false;
                        Tab = "2";
                    }
                }
                if (e.CommandName == "ActiveInActive")
                {
                    _master.ActiveInactiveQubitScript(id);
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var objQubit = new tblQubitScript
                {
                    ID = string.IsNullOrEmpty(hdnID.Value) ? new Guid() : Guid.Parse(hdnID.Value),
                    IsActive = chkAcive.Checked,
                    Script = txtScript.Text.Trim(),
                    CreatedBy = AdminuserInfo.UserID,
                    CreatedOn = DateTime.Now,
                    SiteID = Guid.Parse(ddlSite.SelectedValue)
                };

                _master.AddEditQubitScript(objQubit);
                ShowMessage(1, string.IsNullOrEmpty(hdnID.Value) ? "Record created successfully." : "Record updated successfully");
                ddlSite.SelectedValue = "-1";
                txtScript.Text = string.Empty;
                chkAcive.Checked = false;
                BindGrid();
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Qubit.aspx");
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSite.SelectedIndex > 0)
            {
                var res = _master.GetQubitScriptBySiteId(Guid.Parse(ddlSite.SelectedValue));
                if (res != null)
                {
                    hdnID.Value = res.ID.ToString();
                    chkAcive.Checked = res.IsActive;
                    txtScript.Text = res.Script;
                    Tab = "2";
                }
                else
                {
                    hdnID.Value = string.Empty;
                    chkAcive.Checked = false;
                    txtScript.Text = string.Empty;
                    Tab = "2";
                }
            }
        }
    }
}