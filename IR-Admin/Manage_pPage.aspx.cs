﻿using System;
using System.Web.UI;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class Manage_pPage : Page
    {
        readonly Masters _master = new Masters();
        db_1TrackEntities _db = new db_1TrackEntities();
        public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpHost"];

        protected void Page_Load(object sender, EventArgs e)
        {
            BindImages();
            BindPage();
        }

        public void BindImages()
        {
            var resultBanner = _master.GetImageListByID(1); //Banner images
            dtBanner.DataSource = resultBanner;
            dtBanner.DataBind();

            var result = _master.GetImageListByID(2); //right panel
            dtImages.DataSource = result;
            dtImages.DataBind();

            var result2 = _master.GetImageListByID(4); //homepage footer
            dtFooter.DataSource = result2;
            dtFooter.DataBind();
        }

        public void BindPage()
        {
            imgMainBanner.Src = SiteUrl + "images/page/banner.jpg";
            imgrtPanel.Src = SiteUrl + "images/page/right-banner.png";

            string url = null;
            if (Session["url"] != null)
                url = Session["url"].ToString();

            if (url != string.Empty)
            {
                var result = _master.GetPageDetailsByUrl(url);
                if (result != null)
                {
                    ContentHead.InnerHtml = result.PageHeading;
                    ContentText.InnerHtml = result.PageContent;
                    footerBlock.InnerHtml = result.FooterImg;
                    rightNav.InnerHtml = result.RightPanelNavUrl;
                    footerBlockheadertxt.InnerHtml = result.FooterHeader;

                    //BindBanner images
                    string[] bannerimgid = result.BannerIDs.TrimEnd(',').Split(',');
                    int bimgid = 0;
                    foreach (string obj in bannerimgid)
                    {
                        if (obj != "")
                            bimgid = Convert.ToInt32(obj);
                    }
                    var bimgpath = _db.tblImages.FirstOrDefault(x => x.ID == bimgid);
                    if (bimgpath != null)
                    {
                        imgMainBanner.Src = bimgpath.ImagePath;
                    }

                    //BindRightPanel images
                    string[] rtpanelimgid = result.RightPanelImgID.TrimEnd(',').Split(',');
                    int imgid = 0;
                    foreach (string obj in rtpanelimgid)
                    {
                        if (obj != "")
                            imgid = Convert.ToInt32(obj);
                    }
                    var imgpath = _db.tblImages.FirstOrDefault(x => x.ID == imgid);
                    if (imgpath != null)
                    {
                        imgrtPanel.Src = imgpath.ImagePath;
                    }
                }
                else
                {
                    BindStaticImgs();
                }
            }
            else
            {
                BindStaticImgs();
            }
        }

        protected void dtBanner_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    tblPage result = _master.GetPageDetailsByUrl(url);
                    if (result != null)
                    {
                        var bannerid = result.BannerIDs.Trim();
                        string[] arrId = bannerid.Split(',');
                        foreach (string id in arrId)
                        {
                            var cbID = (HtmlInputCheckBox)e.Item.FindControl("chkID");
                            if (cbID != null)
                            {
                                if (id == cbID.Value)
                                {
                                    cbID.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void dtImages_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string url = null;
                if (Session["url"] != null)
                    url = Session["url"].ToString();

                if (url != string.Empty)
                {
                    tblPage result = _master.GetPageDetailsByUrl(url);

                    if (result != null)
                    {
                        var rtImgId = result.RightPanelImgID.Trim();
                        string[] arrRtId = rtImgId.Split(',');
                        foreach (string idRt in arrRtId)
                        {
                            var chkrtID = (HtmlInputCheckBox)e.Item.FindControl("chkrtID");
                            if (chkrtID != null)
                            {
                                if (idRt == chkrtID.Value)
                                {
                                    chkrtID.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void BindStaticImgs()
        {
            imgFooter1.Src = SiteUrl + "images/NoImage.jpg";
            imgFooter2.Src = SiteUrl + "images/NoImage.jpg";
            imgFooter3.Src = SiteUrl + "images/NoImage.jpg";
            imgFooter4.Src = SiteUrl + "images/NoImage.jpg";
        }

        //protected void btnSaveRt_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var page = new tblPage
        //            {
        //                ID = (Guid)Session["edit"],
        //                RightPanelImgID = hdnRtImgIDs.Value
        //            };
        //        _master.UpdateRtPanelPage(page);
        //        BindPage();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowMessage(2, ex.Message);
        //    }
        //}
    }
}