﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class ManageSiteHolydays : Page
    {
        readonly private ManageBooking _master = new ManageBooking();
        readonly private ManageHolidays _holiday = new ManageHolidays();
        readonly private Masters _oMasters = new Masters();
        public string Tab = string.Empty;
        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            if (master != null) master.OnSiteSelected += MasterSelected;
        }

        private void MasterSelected(object sender, string selectedValue)
        {
            _SiteID = Guid.Parse(selectedValue);
            BindGrid(_SiteID);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Tab = "1";
            Tab = Request["id"] != null ? "2" : "1";

            if (!Page.IsPostBack)
            {
                _SiteID = Master.SiteID;
                SiteSelected();
                ShowMessage(0, null);
                BindSite();
                ddlSite.SelectedValue = _SiteID.ToString();
                BindGrid(_SiteID);
                if (Request["status"] != null)
                {
                    Tab = Request["status"].ToString().Trim() == "add" ? "2" : "1";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp",
                                                            "<script type='text/javascript'>ResetDiv();</script>", false);
                }
                if ((Request["id"] != null) && (Request["id"] != ""))
                {
                    Tab = "2";
                    btnSubmit.Text = "Update";
                    GetTextTemplateForEdit(Convert.ToInt64(Request["id"]));
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp",
                                                        "<script type='text/javascript'>ResetDiv();</script>", false);
                }
                else
                    Tab = "1";
            }
        }

        void SiteSelected()
        {
            if (!Page.IsPostBack)
                ViewState["PreSiteID"] = _SiteID;
            if (ViewState["PreSiteID"] != null && (ViewState["PreSiteID"].ToString() != _SiteID.ToString()))
            {
                BindGrid(_SiteID);
                ViewState["PreSiteID"] = _SiteID;
            }
        }

        void BindGrid(Guid _SiteID)
        {
            grdHolyday.DataSource = new ManageHolidays().GetAllHolydaysBySite(_SiteID);
            grdHolyday.DataBind();
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }

        protected void grdHolyday_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdHolyday.PageIndex = e.NewPageIndex;
            _SiteID = Master.SiteID;
            SiteSelected();
            BindGrid(_SiteID);
        }

        protected void grdHolyday_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {
                    long id = Convert.ToInt64(e.CommandArgument);
                    var res = _holiday.DeleteHolyDay(id);
                    if (res)
                        ShowMessage(1, "Record deleted successfully.");
                    _SiteID = Master.SiteID;
                    SiteSelected();
                    BindGrid(_SiteID);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        #region Add/Edit
        public void BindSite()
        {
            ddlSite.DataSource = _oMasters.GetActiveSiteList();
            ddlSite.DataTextField = "DisplayName";
            ddlSite.DataValueField = "ID";
            ddlSite.DataBind();
            ddlSite.Items.Insert(0, new ListItem("--Site--", "-1"));
        }

        void AddTemplate()
        {
            try
            {
                var obj = new tblSiteHoliday
                    {
                        HolidayName = txtHoliday.Text,
                        DateofHoliday = Convert.ToDateTime(txtDate.Text.Trim()),
                        SiteID = Guid.Parse(ddlSite.SelectedValue),
                        IsActive = chkIsActive.Checked
                    };
                new ManageHolidays().AddHoliday(obj);
                ShowMessage(1, "Holiday added successfully.");
            }
            catch (Exception ex)
            {
                if (ex.Message.Trim() == "The string was not recognized as a valid DateTime. There is an unknown word starting at index 0.")
                    ShowMessage(2, "Please enter valid date.");
                else
                    ShowMessage(2, ex.Message);
            }
        }
        void ClearControls()
        {
            ddlSite.SelectedIndex = 0;
            txtHoliday.Text = "";
            txtDate.Text = "";
            chkIsActive.Checked = false;
        }
        void UpdateTemplate()
        {
            try
            {
                long id = Convert.ToInt64(Request["id"]);
                var obj = new tblSiteHoliday
                    {
                        ID = id,
                        HolidayName = txtHoliday.Text,
                        DateofHoliday = Convert.ToDateTime(txtDate.Text.Trim()),
                        SiteID = Guid.Parse(ddlSite.SelectedValue),
                        IsActive = chkIsActive.Checked
                    };
                new ManageHolidays().UpdateHoliday(obj);
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$(location).attr('href',$(location).attr('pathname'));</script>", false);
                ShowMessage(1, "Holiday updated successfully.");
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] == null)
                    AddTemplate();
                else
                    UpdateTemplate();
                ClearControls();
                Tab = "1";
            }
            catch (Exception ex)
            {
                ShowMessage(2, ex.Message);
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageSiteHolydays.aspx");
        }
        public void GetTextTemplateForEdit(long id)
        {
            var oP = _holiday.GetHolydayByID(id);
            if (oP != null)
            {
                ddlSite.SelectedValue = oP.SiteID.ToString();
                txtHoliday.Text = oP.HolidayName;
                txtDate.Text = oP.DateofHoliday.HasValue ? oP.DateofHoliday.Value.ToString("dd/MM/yyyy") : "";
                chkIsActive.Checked = oP.IsActive.HasValue && oP.IsActive.Value;
            }
        }

        #endregion
    }
}