﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Qubit.aspx.cs" Inherits="IR_Admin.Quebit" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/Tab/jquery.js" type="text/jscript"></script>
    <link rel="stylesheet" href="Scripts/Tab/tabs.css" type="text/css" media="screen" />
    <script type="text/javascript">    
        $(function () {                  
             if(<%=Tab%>=='1')
            {
               $("ul.list").tabs("div.panes > div");
            }
        });
        
        $(window).load(function () {
            setTimeout(function () { $("#MainContent_DivSuccess").fadeOut() }, 5000);
            setTimeout(function () { $("#MainContent_DivError").fadeOut() }, 5000);
        });   
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Qubit Script's
    </h2>
    <div class="full mr-tp1">
        <ul class="list">
            <li><a id="aList" href="Qubit.aspx" class="current">List</a></li>
            <li>
                <asp:HyperLink ID="aNew" CssClass=" " NavigateUrl="#" runat="server">New</asp:HyperLink></li>
        </ul>
        <!-- tab "panes" -->
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none; margin-top: 24px;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="full mr-tp1">
            <div class="panes" style="font-size: 14px;">
                <div id="divlist" runat="server" style="display: none;">
                    <div class="crushGvDiv" style="height: auto;">
                        <asp:GridView ID="grdQubitScript" runat="server" CellPadding="4" CssClass="grid-head2"
                            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdQubitScript_RowCommand">
                            <AlternatingRowStyle BackColor="#FBDEE6" />
                            <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                                BorderColor="#FFFFFF" BorderWidth="1px" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <Columns>
                                <asp:TemplateField HeaderText="SiteName">
                                    <ItemTemplate>
                                        <%#Eval("SiteName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="URL">
                                    <ItemTemplate>
                                        <%#Eval("SiteURL")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Script">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDescription" runat="server" Text='<%#HttpUtility.HtmlEncode(Convert.ToString(Eval("Script"))) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="imgEdit" ToolTip="Edit" CommandArgument='<%#Eval("ID")%>'
                                            CommandName="Modify" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="imgDelete" ToolTip="Delete" CommandArgument='<%#Eval("ID")%>'
                                            CommandName="Remove" ImageUrl="~/images/delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                        <asp:ImageButton runat="server" ID="imgActive" CommandArgument='<%#Eval("ID")%>'
                                            Height="16" CommandName="ActiveInActive" AlternateText="status" ImageUrl='<%#Eval("IsActive").ToString()=="True" ?"~/images/active.png":"~/images/inactive.png" %>'
                                            ToolTip='<%#Eval("IsActive").ToString()=="True" ?"Active":"In-Active" %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div id="divNew" class="grid-sec2" runat="server" style="display: Block;">
                    <table class="tblMainSection">
                        <tr>
                            <td class="col">
                                Site Name:
                            </td>
                            <td class="col">
                                <asp:DropDownList ID="ddlSite" runat="server" Width="205px" AutoPostBack="True" 
                                    onselectedindexchanged="ddlSite_SelectedIndexChanged" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfSite" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="ddlSite" ValidationGroup="rv" InitialValue="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="vertical-align: top;">
                                Script Tag:
                            </td>
                            <td class="col">
                                <asp:TextBox ID="txtScript" runat="server" TextMode="MultiLine" MaxLength="500" Width="700"
                                    Height="85" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfScript" runat="server" ErrorMessage="*" ForeColor="Red"
                                    ControlToValidate="txtScript" ValidationGroup="rv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col">
                                Is Active ?:
                            </td>
                            <td class="col">
                                <asp:CheckBox ID="chkAcive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                                    Text="Submit" Width="89px" ValidationGroup="rv" />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:HiddenField ID="hdnID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
