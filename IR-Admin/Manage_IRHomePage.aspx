﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_IRHomePage.aspx.cs"
    Inherits="IR_Admin.Manage_IRHomePage" %>

<%@ Register Src="usercontrol/BannerImageManager.ascx" TagName="BannerImageManager"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>International Rail</title>
    <!-- Bootstrap -->
    <link href="Styles/ircss/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Styles/ircss/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="Styles/ircss/css/layout.css" rel="stylesheet" type="text/css" />
    <script src="Styles/ircss/js/jquery-2.1.4.js" type="text/javascript"></script>
    <link href="editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="editor/redactor.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
    <style type="text/css">
        .PopUpSampleIMG
        {
            position: fixed;
            height: 180px;
            width: 280px; /*left: 277px; top: 150px;*/
            left: 240px;
            top: 1050px;
            z-index: 100;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 50001;
        }
        .bGray
        {
            border: 1px solid gray;
        }
        
        .button
        {
            min-width: 60px !important;
            width: auto !important;
            background: url(schemes/images/btn-bar.jpg) no-repeat left -30px !important;
            border-right: 1px thin #a1a1a1 !important;
            border: thin none !important;
            font-weight: bold !important;
            color: white;
            cursor: pointer;
            height: 30px;
            -webkit-border-radius: 0px !important;
            font-size: 13px !important;
        }
        .button:hover
        {
            background: url(schemes/images/btn-bar.jpg) no-repeat left -0px;
            border-right: 1px solid #a1a1a1;
            color: White;
            cursor: pointer;
            height: 30px;
        }
        td
        {
            padding: 4px;
        }
        .PopUpSample
        {
            position: fixed;
            width: 400px;
            left: 100px;
            top: 150px;
            margin-left: 15px;
            border: 1px solid #E6E6E6;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 10px blue;
            -webkit-box-shadow: 0 0 10px blue;
            background: linear-gradient(to bottom, #F7F7F7 0%, #E2E2E2 100%) repeat scroll 0 0 transparent;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 0 5px 60px #000000;
            color: #E58A42;
            font-family: Helvetica,Arial,Verdana,Tahoma,sans-serif;
            font-size: 14px !important;
            text-shadow: 0 1px 0 #FFFFFF;
            z-index: 1000 !important; /*50001;*/
        }
        .carousel-indicators
        {
            bottom: 32px !important;
        }
        #ContentHead
        {
            color: #fff;
            text-transform: uppercase;
            font-size: 18px;
            font-weight: 700;
        }
        #ContentText
        {
            display: inline-block;
            font-size: 15px;
            text-transform: uppercase;
            font-weight: 700;
        }
        .seeall i, .seeall span
        {
            display: inline !important;
        }
        
        #ContentHeading1 h1, #ContentHeading2 h2
        {
            margin-top: 5px;
            margin-bottom: 5px;
            font-size: 22px;
            font-weight: 700;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtHeading').redactor({ iframe: true, minHeight: 200 });
            $('#txtContent').redactor({ iframe: true, minHeight: 200 });
            $('#txtHeading1').redactor({ iframe: true, minHeight: 200 });
            $('#txtHeading2').redactor({ iframe: true, minHeight: 200 });
            $('#txtDescription1').redactor({ iframe: true, minHeight: 200 });
            $('#txtDescription2').redactor({ iframe: true, minHeight: 200 });

            //Edit banner images
            $(".editbtn1").click(function () {
                $("#ContentBanner").slideToggle("slow");
            });
            $(".editbtn3").click(function () {
                $("#divHeading").slideToggle("slow");
            });
            $(".editbtn4").click(function () {
                $("#divContent").slideToggle("slow");
            });

            $(".editbtn21").click(function () {
                $("#divHomePageHeading1").slideToggle("slow");
            });
            $(".editbtn23").click(function () {
                $("#divHomePageHeading2").slideToggle("slow");
            });
            $(".editbtn22").click(function () {
                $("#divHomePageDescription1").slideToggle("slow");
            });
            $(".editbtn24").click(function () {
                $("#divHomePageDescription2").slideToggle("slow");
            });

            $("#btnUploadBanner").click(function () {   //open upload banner div 
                $("#divUploadBanner").slideToggle("slow");
            });


            //----------Edit heading---------//
            $(".editbtn3").click(function () {
                var value;
                $("#divHeading").slideDown("slow");
                value = $('#ContentHead').html();
                $('.redactor_rdHead').contents().find('body').html(value);
            });

            $(".editbtn4").click(function () {
                var value;
                $("#divContent").slideDown("slow");
                value = $('#ContentText').html();
                $('.redactor_rdContent').contents().find('body').html(value);
            });


            $(".editbtn21").click(function () {
                var value;
                $("#divHomePageHeading1").slideDown("slow");
                value = $('#ContentHeading1').html();
                $('.redactor_rdHeading1').contents().find('body').html(value);
            });
            $(".editbtn23").click(function () {
                var value;
                $("#divHomePageHeading2").slideDown("slow");
                value = $('#ContentHeading2').html();
                $('.redactor_rdHeading2').contents().find('body').html(value);
            });
            $(".editbtn22").click(function () {
                var value;
                $("#divHomePageDescription1").slideDown("slow");
                value = $('#ContentDescription1').html();
                $('.redactor_rdDescription1').contents().find('body').html(value);
            });
            $(".editbtn24").click(function () {
                var value;
                $("#divHomePageDescription2").slideDown("slow");
                value = $('#ContentDescription2').html();
                $('.redactor_rdDescription2').contents().find('body').html(value);
            });


            //Close
            $(".btnClose").click(function () {
                if ($(this).attr("rel") == "ContentBanner") {
                    $("#ContentBanner").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    $("#divContent").hide();
                }
                else if ($(this).attr("rel") == "divHeading") {
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divcloseBanner") {  //close upload banner div 
                    $("#divUploadBanner").hide();
                }
                else if ($(this).attr("rel") == "divHeading1") {
                    $("#divHomePageHeading1").hide();
                }
                else if ($(this).attr("rel") == "divHeading2") {
                    $("#divHomePageHeading2").hide();
                }
                else if ($(this).attr("rel") == "divDescription1") {
                    $("#divHomePageDescription1").hide();
                }
                else if ($(this).attr("rel") == "divDescription2") {
                    $("#divHomePageDescription2").hide();
                }
            });

            //-----Save banner images------//
            $(".btnSaveBanner").click(function () {
                var imageid;
                var arr = new Array();
                var i = 0;
                $('div#ContentBanner input[type=checkbox]').each(function () {
                    if ($(this).is(":checked")) {
                        imageid = $(this).attr('value');
                        arr[i] = imageid;
                        i++;
                    }
                });
                var myIds = "";
                for (i = 0; i < arr.length; i++) {
                    myIds = myIds + arr[i] + ",";
                }
                if (myIds == "") {
                    $("#hdnBannerIDs").val("0");
                } else {
                    $("#hdnBannerIDs").val(myIds);
                }
                $("#ContentBanner").hide();
            });

            //-----Save------//
            $(".btnsave").click(function () {
                var value;
                if ($(this).attr("rel") == "divHeading") {
                    value = $('textarea[name=txtHeading]').val();
                    if (value != "")
                        $('#ContentHead').html(value);
                    $("#divHeading").hide();
                }
                else if ($(this).attr("rel") == "divContent") {
                    value = $('textarea[name=txtContent]').val();
                    if (value != "")
                        $('#ContentText').html(value);
                    $("#divContent").hide();
                }

                else if ($(this).attr("rel") == "divHeading1") {
                    value = $('textarea[name=txtHeading1]').val();
                    if (value != "")
                        $('#ContentHeading1').html(value);
                    $("#divHomePageHeading1").hide();
                }
                else if ($(this).attr("rel") == "divHeading2") {
                    value = $('textarea[name=txtHeading2]').val();
                    if (value != "")
                        $('#ContentHeading2').html(value);
                    $("#divHomePageHeading2").hide();
                }
                else if ($(this).attr("rel") == "divDescription1") {
                    value = $('textarea[name=txtDescription1]').val();
                    if (value != "")
                        $('#ContentDescription1').html(value);
                    $("#divHomePageDescription1").hide();
                }
                else if ($(this).attr("rel") == "divDescription2") {
                    value = $('textarea[name=txtDescription2]').val();
                    if (value != "")
                        $('#ContentDescription2').html(value);
                    $("#divHomePageDescription2").hide();
                }
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnBannerIDs" runat="server" />
    <div class="page-wrap">
        <div class="navbarbg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img src="Styles/ircss/images/top.jpg" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="Styles/ircss/images/logo-panel.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="navouter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                        <div class="navouter bdrbtm">
                            <div class="pull-right pos-rel agent_login">
                                <i class="fa fa-user fa-2x search "></i><a href="#">Agent Login</a>
                            </div>
                            <div class="clearfix visible-xs">
                            </div>
                            <div class=" collapse navbar-collapse pull-left">
                                <ul class="nav navbar-nav">
                                    <li><a href="#" class="active"><span></span>HOME </a></li>
                                    <li><a href="#"><span></span>RAIL PASSES </a></li>
                                    <li><a href="#"><span></span>RAIL TICKETS </a></li>
                                    <li><a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                        id="dropdownMenu2"><span></span>COUNTRY </a></li>
                                    <li class="dropdown dropdown-large "><a href="#"><span></span>SPECIAL TRAINS </a>
                                        <ul class="dropdown-menu dropdown-menu-large row">
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Glyphicons</li>
                                                    <li><a href="#">Available glyphs</a></li>
                                                    <li class="disabled"><a href="#">How to use</a></li>
                                                    <li><a href="#">Examples</a></li>
                                                    <li><a href="#">Example</a></li>
                                                    <li><a href="#">Aligninment options</a></li>
                                                    <li><a href="#">Headers</a></li>
                                                    <li><a href="#">Disabled menu items</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Button groups</li>
                                                    <li><a href="#">Basic example</a></li>
                                                    <li><a href="#">Button toolbar</a></li>
                                                    <li><a href="#">Sizing</a></li>
                                                    <li><a href="#">Nesting</a></li>
                                                    <li><a href="#">Vertical variation</a></li>
                                                    <li><a href="#">Single button dropdowns</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Input groups</li>
                                                    <li><a href="#">Basic example</a></li>
                                                    <li><a href="#">Sizing</a></li>
                                                    <li><a href="#">Checkboxes and radio addons</a></li>
                                                    <li><a href="#">Tabs</a></li>
                                                    <li><a href="#">Pills</a></li>
                                                    <li><a href="#">Justified</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header">Navbar</li>
                                                    <li><a href="#">Default navbar</a></li>
                                                    <li><a href="#">Buttons</a></li>
                                                    <li><a href="#">Text</a></li>
                                                    <li><a href="#">Non-nav links</a></li>
                                                    <li><a href="#">Component alignment</a></li>
                                                    <li><a href="#">Fixed to top</a></li>
                                                    <li><a href="#">Fixed to bottom</a></li>
                                                    <li><a href="#">Static top</a></li>
                                                    <li><a href="#">Inverted navbar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><span></span>CONTACT US </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="editbaner">
                <div class="editbtn1">
                    <a href="#" class="edit-btn">Edit</a></div>
                <div class="editbtn3">
                    <a href="#" class="edit-btn">Edit</a></div>
                <div class="editbtn4">
                    <a href="#" class="edit-btn">Edit</a></div>
                <div id="div_EurailPromotion" runat="server" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#div_EurailPromotion" data-slide-to="0" class="active"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="Styles/ircss/images/banner.jpg" alt="First slide">
                            <div class="carousel-caption">
                                <h3>
                                    Promotional heading text
                                </h3>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s,
                                </p>
                                <a class="linkbtn" href="#">view more detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="div_EurailPromotionShow" class="carousel slide" data-ride="carousel" runat="server"
                    visible="false">
                    <ol class="carousel-indicators" id="listItem" runat="server" clientidmode="Static">
                    </ol>
                    <div class="carousel-inner">
                        <asp:Repeater ID="rptEurailPromotion" runat="server">
                            <ItemTemplate>
                                <div class="item">
                                    <asp:Image ID="imgBanner" runat="server" ImageUrl='<%#Eval("ImageUrl") %>' />
                                    <div class="carousel-caption">
                                        <h3>
                                            Promotional heading text
                                        </h3>
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                            Ipsum has been the industry's standard dummy text ever since the 1500s,
                                        </p>
                                        <a class="linkbtn" href="#">view more detail</a>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="searchbar">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="ContentHead" runat="server">
                        <h4>
                            Where do you want to go ?
                        </h4>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    Go</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 seeall">
                        <span class="cornertop">&nbsp;</span> <a href="#"><span id="ContentText" runat="server">
                            SEE ALL THE COUNTRIES YOU COULD EXPLORE </span><i>
                                <img src="Styles/ircss/images/icon_arrow.png" alt=""></i></a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row " style="position: relative;">
                    <div class="editbtn21">
                        <a href="#" class="edit-btn">Edit</a></div>
                    <div class="editbtn22">
                        <a href="#" class="edit-btn">Edit</a></div>
                    <div class="newboxmian-data">
                        <div class="newboxdata-heading" id="ContentHeading1" runat="server">
                            <h1>
                                Heading 1</h1>
                        </div>
                        <div id="ContentDescription1" runat="server">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                unknown printer took a galley of type and scrambled it to make a type specimen book.
                                It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row ">
                    <img src="Styles/ircss/images/midetab.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="container">
                <div class="row " style="position: relative;">
                    <div class="editbtn23">
                        <a href="#" class="edit-btn">Edit</a></div>
                    <div class="editbtn24">
                        <a href="#" class="edit-btn">Edit</a></div>
                    <div class="newboxmian-data">
                        <div class="newboxdata-heading" id="ContentHeading2" runat="server">
                            <h2>
                                Heading 2</h2>
                        </div>
                        <div id="ContentDescription2" runat="server">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                unknown printer took a galley of type and scrambled it to make a type specimen book.
                                It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerwraper">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Destinations</h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/australia">Australia</a></li>
                                <li><a href="https://www.internationalrail.com/italy">Italy</a></li>
                                <li><a href="https://www.internationalrail.com/japan">Japan</a></li>
                                <li><a href="https://www.internationalrail.com/new-zealand">New Zealand</a></li>
                                <li><a href="https://www.internationalrail.com/united-states">USA</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Rail Companies
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/eurostar">Eurostar</a></li>
                                <li><a href="https://www.internationalrail.com/thalys">Thalys</a></li>
                                <li><a href="https://www.internationalrail.com/thello">Thello</a></li>
                                <li><a href="https://www.internationalrail.com/trenitalia">Trenitalia</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Travel the world
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/Balkan-FlexiPass-(EUR)/balkan-flexipass">
                                    Balkan Flexi-Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Japanese-Rail-Passes/japan-rail-pass">
                                    Japan Rail Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Korean-Rail-Passes/korea-rail-pass">Korea
                                    Pass</a></li>
                                <li><a href="https://www.internationalrail.com/Spanish-Rail-Passes/renfe-spain-pass">
                                    Spain Pass</a></li>
                                <li><a href="https://www.internationalrail.com/USA-Rail-Passes/usa-rail-pass">USA Rail
                                    Pass</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <h3>
                                Legal information
                            </h3>
                            <ul>
                                <li><a href="https://www.internationalrail.com/contact-us">Contact us</a> </li>
                                <li><a href="https://www.internationalrail.com/conditions-of-use">Conditions of Use</a></li>
                                <li><a href="https://www.internationalrail.com/Cookies">Cookies</a></li>
                                <li><a href="https://www.internationalrail.com/Booking-Conditions">General terms and
                                    conditions</a></li>
                                <li><a href="https://www.internationalrail.com/privacy-policy">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                            <ul>
                                <li><a href="https://www.internationalrail.com/">Home </a></li>
                                <li><a href="https://www.internationalrail.com/Contact-Us">Contact Us </a></li>
                                <li><a href="https://www.internationalrail.com/About-Us">About Us </a></li>
                                <li><a href="https://www.internationalrail.com/Booking-Conditions">Booking Conditions
                                </a></li>
                                <li><a href="https://www.internationalrail.com/Privacy-Policy">Privacy Policy </a>
                                </li>
                                <li><a href="https://www.internationalrail.com/Conditions-of-Use">Conditions of Use
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                            <p class="copyright">
                                © Copyright International Rail Ltd. 2015 - All rights reserved. A company registered
                                in England and Wales, company number: 3060803 with registered offices at International
                                Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire.
                                SO53 4AR.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center social_link">
                            <a href="#"><i class=" fa fa-facebook-square fa-2x"></i></a><a href="#"><i class="fa fa-twitter-square fa-2x">
                            </i></a><a href="#"><i class=" fa fa-rss-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ContentBanner" class="PopUpSampleIMG" style="display: none; width: 580px;
        height: auto; left: 172px; top: 203px;">
        <table>
            <tr>
                <td class="clsHeadColor">
                    <b>Edit Banner</b>
                </td>
            </tr>
        </table>
        <div style="overflow-y: scroll; height: 290px;">
            <asp:DataList ID="dtBanner" runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                CellPadding="2" CellSpacing="2" OnItemDataBound="dtBanner_ItemDataBound">
                <ItemTemplate>
                    <asp:Image ID="imgBanner" ImageUrl='<%#Eval("ImagePath")%>' runat="server" Width="250"
                        Height="120" CssClass="bGray" />
                    <br />
                    <input id="chkID" type="checkbox" name="img" value='<%#Eval("ID")%>' runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="float-rt">
            <input type="button" id="btnUploadBanner" value="Upload
    Banner" class="button" />
            <input type="button" id="Button4" value="Cancel" class="button
    btnClose" rel="ContentBanner" />
            <input type="button" id="btnSaveBanner" value="Save" class="button btnSaveBanner"
                rel="ContentBanner" />
        </div>
    </div>
    <div class="PopUpSample" id="divHeading" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Search Heading</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading" name="txtHeading" cols="10" rows="5" class="rdHead"></textarea>
                        <input type="hidden" class="hdnHead" id="hdnHead" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnHeadingClose" value="Cancel" class="button btnClose"
                        rel="divHeading" />
                    <input type="button" id="btnHeadingSave" value="Save" class="button btnsave" rel="divHeading" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divContent" style="display: none; left: 240px; top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Country Search Text</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtContent" name="txtContent" cols="10" rows="5" class="rdContent"></textarea>
                        <input type="hidden" class="hiddenc" value="test" id="hiddenc" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="Button1" value="Cancel" class="button btnClose" rel="divContent" />
                    <input type="button" id="btnSave" value="Save" class="button btnsave" rel="divContent" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divUploadBanner" class="PopUpSample" style="display: none; width: 850px;
        height: 450px; left: 55px; top: 203px; position: fixed; z-index: 99999 !important;">
        <uc1:BannerImageManager ID="ucBannerImageManager" runat="server" />
    </div>
    <%--Start Home Page Heading--%>
    <div class="PopUpSample" id="divHomePageHeading1" style="display: none; left: 240px;
        top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading 1</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading1" name="txtHeading1" cols="10" rows="5" class="rdHeading1"></textarea>
                        <input type="hidden" class="hdnHead1" id="hdnHead1" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnHeading1Close" value="Cancel" class="button btnClose"
                        rel="divHeading1" />
                    <input type="button" id="btnHeading1Save" value="Save" class="button
    btnsave" rel="divHeading1" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divHomePageHeading2" style="display: none; left: 240px;
        top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Heading 2</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtHeading2" name="txtHeading2" cols="10" rows="5" class="rdHeading2"></textarea>
                        <input type="hidden" class="hdnHead2" id="hdnHead2" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnHeading2Close" value="Cancel" class="button btnClose"
                        rel="divHeading2" />
                    <input type="button" id="btnHeading2Save" value="Save" class="button btnsave" rel="divHeading2" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divHomePageDescription1" style="display: none; left: 240px;
        top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Description 1</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtDescription1" name="txtDescription1" cols="10" rows="5" class="rdDescription1"></textarea>
                        <input type="hidden" class="hdnDescription1" id="hdnDescription1" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnDescription1Close" value="Cancel" class="button
    btnClose" rel="divDescription1" />
                    <input type="button" id="btnDescription1Save" value="Save" class="button btnsave"
                        rel="divDescription1" />
                </td>
            </tr>
        </table>
    </div>
    <div class="PopUpSample" id="divHomePageDescription2" style="display: none; left: 240px;
        top: 203px;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tblclass">
            <tbody>
                <tr>
                    <td colspan="2" class="clsHeadColor">
                        <b>Edit Description 2</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <textarea id="txtDescription2" name="txtDescription2" cols="10" rows="5" class="rdDescription2"></textarea>
                        <input type="hidden" class="hdnDescription2" id="hdnDescription2" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="float-rt">
            <tr>
                <td>
                    <input type="button" id="btnDescription2Close" value="Cancel" class="button
    btnClose" rel="divDescription2" />
                    <input type="button" id="btnDescription2Save" value="Save" class="button btnsave"
                        rel="divDescription2" />
                </td>
            </tr>
        </table>
    </div>
    <%--End Home Page Heading--%>
    <script src="Styles/ircss/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">        $(function () {
            $(".dropdown").hover(function () {
                $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                $(this).toggleClass('open'); $('b', this).toggleClass("caret caret-up");
            }, function 
    () {
                $('.dropdown-menu', this).stop(true, true).fadeOut("fast"); $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            });
        }); </script>
    <script type="text/javascript">
        $(document).ready(function () { $('div.item').eq(0).addClass('active').end(); });
        function activeCrousal() {
            $(".carousel-inner div:first-child").addClass("active");
        } </script>
    </form>
</body>
</html>
