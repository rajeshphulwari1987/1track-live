﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

namespace IR_Admin
{
    public partial class UploadAdmin : System.Web.UI.Page
    {

        #region [ Page InIt must write on every page of CMS ]
        Guid _SiteID;
        protected void Page_Init(object sender, EventArgs e)
        {
            var master = (SiteMaster)Page.Master;
            master.OnSiteSelected += MasterSelected;
        }
        private void MasterSelected(object sender, string selectedValue)
        {
            // Here you can handle the master's event and update your content page according to site selected
            _SiteID = Guid.Parse(selectedValue);

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (fupUser.PostedFile.FileName != "")
                {
                    bool rs = UploadFile();
                    if (rs)
                        ShowMessage(1, "User uploaded successfully.");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(2, "UnExpected Error!");
                ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadAdmin.aspx");

        }

        protected bool UploadFile()
        {
            try
            {
                string path;
                Guid id = Guid.NewGuid();
                string[] ext = new string[] { ".XLS", ".XLSX" };
                if (fupUser.HasFile)
                {
                    if (fupUser.PostedFile.ContentLength > 2097152)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('Uploaded User file is larger up to 2Mb.')</script>", false);
                        return false;
                    }
                    string fExt = fupUser.FileName.Substring(fupUser.FileName.LastIndexOf("."));
                    string fileExt = fExt;
                    if (!ext.Contains(fileExt.ToUpper()))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script>alert('File format is not specified, Please try another format.')</script>", false);
                        return false;
                    }

                    fileExt = id + fileExt;
                    path = "~/Uploaded/UserFile/" + fileExt;
                    if (System.IO.File.Exists(Server.MapPath(path)))
                        System.IO.File.Delete(Server.MapPath(path));
                    else
                    {
                        fupUser.SaveAs(Server.MapPath(path));
                        string dircPath = Server.MapPath(path);
                        ManageProduct oPrduct = new ManageProduct();
                        int result = oPrduct.UploadUsersFromExcel(dircPath, AdminuserInfo.UserID, fExt);
                        if (result == 0)
                            throw new Exception("User uploading failed!");
                    } 
                } 
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ShowMessage(int flag, string message)
        {
            //0: Display none all div
            //1: Display block success div
            //2: Display block error div
            switch (flag)
            {
                case 0:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = string.Empty;
                    break;
                case 1:
                    DivError.Style.Add("display", "none");
                    DivSuccess.Style.Add("display", "block");
                    lblErrorMsg.Text = string.Empty;
                    lblSuccessMsg.Text = message;
                    break;
                case 2:
                    DivError.Style.Add("display", "block");
                    DivSuccess.Style.Add("display", "none");
                    lblErrorMsg.Text = message;
                    lblSuccessMsg.Text = string.Empty;
                    break;
            }
        }
    }
}