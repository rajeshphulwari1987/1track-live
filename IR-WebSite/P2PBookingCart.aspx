﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="P2PBookingCart.aspx.cs" Inherits="P2PBookingCart"
    UICulture="en" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucTicketDelivery" Src="~/UserControls/ucTicketDelivery.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link id="lnkBookStyle" runat="server" href="Styles/BookingCart.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], false);
            $("#MainContent_txtDateOfDepature").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            showHideShipping();
            $("#radioChkShp").attr("checked", "checked");
            ShowCall();
            getdata();
            restrictCopy();

            $(".imgCal").click(function () {
                $("#MainContent_txtDateOfDepature").datepicker('show');
            });
            $(".imgCal1").click(function () {
                if ($('#MainContent_ucTrainSearch_rdBookingType_1').is(':checked')) {
                    $("#txtReturnDate").datepicker('show');
                }
            });
        });

        function callshipping() {
            getdata();
            alert("Please choose shipping amount.");
        }
        function restrictCopy() {


            $(function () {
                $('#MainContent_txtConfirmEmail').bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
                $('#MainContent_txtshpConfirmEmail').bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
            });
        }
        function ShowCall() {

            $(".calDate, #calImg").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0
            });
            $(".calDate").keypress(function (event) { event.preventDefault(); });
            var date = $(".dateofDeparture:eq(0)").text();
            date = date.split('/');
            var d = new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0]));
            $(".calDate, #calImg").datepicker("option", "maxDate", d);
            $(".calDate, #calImg").val("DD/MM/YYYY");
        }
        function getdata() {
            var countPrice = 0.00;
            var bookingFee = 0.00;
            $(".calculatePrice").each(function () {
                var price = $(this).text() == '' ? 0 : $(this).text();
                countPrice = countPrice + parseFloat(price);
            });
            bookingFee = parseFloat($.trim($("#MainContent_rptTrain_lblBookingFee").text()));
            countPrice = bookingFee + countPrice;
            $("#lblTotal").text(countPrice.toFixed(2));
            $(".shipping").each(function () {
                if ($(this).is(":checked")) {
                    var str = $(this).val().split('^');

                    var price = str[0];
                    var smethod = str[1];
                    var sdesc = str[2];
                    sdesc = $('<div/>').html(sdesc).text();

                    $("#MainContent_hdnShippingCost").attr('value', price.toString());
                    $("#MainContent_hdnShipMethod").attr('value', smethod.toString());
                    $("#MainContent_hdnShipDesc").attr('value', sdesc.toString());

                    var deliveryMethod = ($("#MainContent_ucTicketDelivery_hiddenDileveryMethod").attr('value'));
                    if (deliveryMethod == "TA") {
                        countPrice = countPrice + parseFloat(price);
                    }
                }
            });
            $("#MainContent_lblTotalCost").text(countPrice.toFixed(2));
        } 
    </script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function showHideShipping() {
            if ($("#MainContent_ucTicketDelivery_hiddenDileveryMethod").val() == "DH" || $("#MainContent_ucTicketDelivery_hiddenDileveryMethod").val() == "ST" || $("#MainContent_ucTicketDelivery_hiddenDileveryMethod").val() == "TL") {
                $("#MainContent_pnlShipping").hide();
                $("#MainContent_hdnShippingCost").val("0");
                $("#MainContent_lblTotalCost").text($("#lblTotal").text());
            }
            else {
                getdata();
                $("#MainContent_pnlShipping").show();
                var shipping = $("#MainContent_hdnShippingCost").val();
                var subTotal = $("#lblTotal").text();
                var total = parseFloat(subTotal) + parseFloat(shipping);
                $("#MainContent_lblTotalCost").text(total.toFixed(2));
            }
        }

        function showmessagecountry() {
            $(".customSelect").remove();
            $(".chkcheckbox,.uncheckbox").remove();
            $(".uncheckradiobox,.chkcheckradiobox").remove();
        }

        function customselect() {
            var sta = $("span").hasClass("customSelect");
            if (sta) {
                $(".customSelect").remove();
                $('select').customSelect();
            }
        }
        $(function () {
            /*shipping details*/
            $("#MainContent_chkShippingfill").click(function () {
                if ($(this).is(':checked')) {
                    $("#MainContent_pnlbindshippping").show();
                    ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], true);
                    $("#MainContent_ddlshpMr").prop("selectedIndex", $("#MainContent_ddlMr").prop("selectedIndex"));
                    $("#MainContent_txtshpfname").val($("#txtFirst").val());
                    $("#MainContent_txtshpLast").val($("#MainContent_txtLast").val());
                    $("#MainContent_txtshpEmail").val($("#MainContent_txtEmail").val());
                    $("#MainContent_txtshpConfirmEmail").val($("#MainContent_txtConfirmEmail").val());
                    $("#MainContent_txtShpPhone").val($("#MainContent_txtBillPhone").val());
                    $("#MainContent_txtshpAdd").val($("#MainContent_txtAdd").val());
                    $("#MainContent_txtshpAdd2").val($("#MainContent_txtAdd2").val());
                    $("#MainContent_txtshpCity").val($("#MainContent_txtCity").val());
                    $("#MainContent_txtshpState").val($("#MainContent_txtState").val());
                    $("#MainContent_txtshpZip").val($("#MainContent_txtZip").val());
                    $("#MainContent_ddlshpCountry").prop("selectedIndex", $("#MainContent_ddlCountry").prop("selectedIndex"));
                    var sta = $("span").hasClass("customSelect");
                    if (sta) {
                        $(".customSelect").remove();
                        $('select').customSelect();
                    }
                }
                else {
                    $("#MainContent_pnlbindshippping").hide();
                    ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], false);
                }
            });

            /*tricketprotecton*/
            $('.calculateTotal').click(function () {
                var pricewithTicketprotection = Priceupdate = 0;
                var chktrue = $(this).find('input[id*=chkTicketProtection]').prop('checked');
                var currsyb = $(this).parent().find('span[id*=currsyb]');
                var priceTP = $(this).parent().find('span[id*=lbltpPrice]');
                var price = $(this).parent().next().find('span[id*=lblPrice]');
                var PassSaleID = $(this).parent().next().find('span[id*=lblPassSaleID]');
                var HidPriceValue = $(this).parent().next().find('span[id*=lblHidPriceValue]');
                var passID = (PassSaleID.text() == '' ? '00000000-0000-0000-0000-000000000000' : PassSaleID.text());
                if (chktrue) {
                    pricewithTicketprotection = parseFloat(priceTP.text()) + parseFloat(price.text());
                    Priceupdate = (parseFloat(priceTP.text() == '' ? 0 : priceTP.text()).toFixed(2));
                    currsyb.css('color', '#000');
                    priceTP.css('color', '#000');
                }
                else {
                    pricewithTicketprotection = parseFloat(HidPriceValue.text());
                    Priceupdate = 0;
                    currsyb.css('color', '#ccc');
                    priceTP.css('color', '#ccc');
                }
                price.text(pricewithTicketprotection.toFixed(2));
                getdata();
            });
        });
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdnBookingFee" runat="server" Value="0.00" />
    <asp:HiddenField ID="hdnShipMethod" runat="server" Value="" />
    <asp:HiddenField ID="hdnShipDesc" runat="server" Value="" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="content">
        <%-- <div class="breadcrumb">
            <a href="Home">Home </a>
        </div>--%>
        <div class="left-content" style="width: 100%">
            <h1>
                Booking Details
            </h1>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="round-titles">
                Review
            </div>
            <div class="booking-detail-in" style="padding: 0; width: 989px;">
                <asp:Repeater ID="rptTrain" runat="server" OnItemDataBound="rptTrain_ItemDataBound">
                    <HeaderTemplate>
                        <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="100%" colspan="3" style="background-color: #5e5e5e; color: White;">
                                <%#Eval("PassDesc")%>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%">
                                <%#Eval("Traveller")%>
                            </td>
                            <td width="20%" style="vertical-align: bottom">
                                <asp:Label ID="lblTckProc" runat="server" Text="Ticket Protection" />
                            </td>
                            <td width="40%" style="vertical-align: bottom">
                                <asp:UpdatePanel ID="updTrain" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        &nbsp;&nbsp;&nbsp;
                                        <div id="divTckProt" runat="server" style="float: left">
                                            <asp:Label runat="server" ID="currsyb" Text='<%#currency%>'></asp:Label>
                                            <asp:Label ID="lbltpPrice" runat="server" Text='<%#Eval("TicketProtectionPrice") %>' />
                                            <asp:CheckBox ID="chkTicketProtection" CssClass="calculateTotal" runat="server" Checked='<%#Eval("TicketProtection")%>' />
                                            <a onclick="showthis()" href="#">
                                                <img src="images/info.png" width="20" /></a>
                                        </div>
                                        <div style="float: right; line-height: 8px;">
                                            <br />
                                            <%=currency%>
                                            <asp:Label ID="lblPrice" CssClass="calculatePrice" runat="server" Text='<%#Eval("Price")%>'
                                                Style="margin-right: 8px;" />
                                            <asp:Label ID="lblPassSaleID" runat="server" Text='<%#Eval("ProductID")%>' Style="display: none;" />
                                            <asp:Label ID="lblHidPriceValue" runat="server" Text='<%#Eval("Price")%>' Style="display: none;" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="chkTicketProtection" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" colspan="3" style="background-color: #5e5e5e; color: White;">
                                    Booking Fee
                                </td>
                            </tr>
                            <tr>
                                <td width="40%">
                                    &nbsp;
                                </td>
                                <td width="20%">
                                    &nbsp;
                                </td>
                                <td width="40%">
                                    <div style="float: right">
                                        <%=currency%>
                                        <asp:Label ID="lblBookingFee" runat="server" Text="" Style="margin-right: 8px;"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="total">
                            Sub Total:
                            <%=currency%>
                            <asp:Label ID="lblTotal" ClientIDMode="Static" runat="server"></asp:Label>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:Repeater ID="rptTrainTcv" runat="server" OnItemCommand="rptTrainTcv_ItemCommand"
                    OnItemDataBound="rptTrainTcv_ItemDataBound">
                    <HeaderTemplate>
                        <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr style="background-color: #5e5e5e;">
                                <th width="50%">
                                    &nbsp;
                                </th>
                                <th width="15%">
                                    &nbsp;
                                </th>
                                <th width="10%">
                                    &nbsp;
                                </th>
                                <th width="25%" valign="middle" colspan="2">
                                    Ticket Protection <a onclick="showthis()" href="#">
                                        <img src="images/info.png" width="17" /></a>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                Ticket (TCV):
                                <asp:Label ID="lblFrm1" runat="server" Text='<%#Eval("DepartureStation")%>' />
                                -
                                <asp:Label ID="lblTo1" runat="server" Text='<%#Eval("ArrivalStation")%>' />
                                <br />
                                From:
                                <asp:Label ID="lblFrm" runat="server" Text='<%#Eval("DepartureStation")%>' />
                                <br />
                                To:
                                <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ArrivalStation")%>' />
                                <br />
                                <asp:Label ID="lblService" runat="server" Text='<%#Eval("ServiceName")%>' />
                                <asp:Label ID="lblPngrType" runat="server" Text='<%#Eval("Passenger")%>' />
                                <br />
                                <asp:Label ID="lblFare" runat="server" Text='<%#Eval("Fare")%>' />
                            </td>
                            <td>
                                <%#Eval("Title")%>
                                <%#Eval("FirstName")%>
                                <%#Eval("LastName")%>
                            </td>
                            <td>
                                <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("Price")%>' />
                            </td>
                            <td width="10%" valign="middle">
                                <input type="checkbox" />
                            </td>
                            <td width="15%" valign="middle">
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%#Eval("Id")%>' CommandName="Remove"><img src='images/icon-delete-new.png' /></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        <div class="total">
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc:ucTicketDelivery ID="ucTicketDelivery" runat="server" />
            <div class="round-titles">
                Billing Address
                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="vgs1" DisplayMode="List"
                    ShowSummary="false" ShowMessageBox="true" runat="server" />
            </div>
            <div class="booking-detail-in delvery" style="height: 383px; width: 969px; height: 425px">
                <div class="colum-label-first">
                    Title
                </div>
                <div class="colum-label-second">
                    <asp:DropDownList ID="ddlMr" runat="server" class="inputsl">
                        <asp:ListItem Text="Mr." Value="1">
                        </asp:ListItem>
                        <asp:ListItem Text="Miss" Value="2">
                        </asp:ListItem>
                        <asp:ListItem Text="Mrs." Value="3">
                        </asp:ListItem>
                        <asp:ListItem Text="Ms" Value="4">
                        </asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="colum-label-third">
                    First Name<span class="readerror">*</span></div>
                <div class="colum-label-forth">
                    <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                    <asp:RequiredFieldValidator ID="BookingpassrfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                        ControlToValidate="txtFirst" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-fifth">
                    Last name<span class="readerror">*</span></div>
                <div class="colum-label-sixth">
                    <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" />
                    <asp:RequiredFieldValidator ID="BookingpassrfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                        ControlToValidate="txtLast" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-first">
                    Email<span class="readerror">*</span></div>
                <div class="colum-label-second">
                    &nbsp;
                </div>
                <div class="colum-label-third">
                    &nbsp;
                </div>
                <div class="colum-label-forth">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" autocomplete="off"  style='text-transform:lowercase'>
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="BookingpassrfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                        ControlToValidate="txtEmail" ForeColor="#eaeaea" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="BookingpassrevEmail" runat="server" Text="*"
                        ErrorMessage="Please enter a valid email." ControlToValidate="txtEmail" ForeColor="#eaeaea"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                        ValidationGroup="vgs1" />
                </div>
                <div class="colum-label-fifth">
                    Confirm Email<span class="readerror">*</span></div>
                <div class="colum-label-sixth">
                    <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="input validcheck" autocomplete="off"  style='text-transform:lowercase' />
                    <asp:RequiredFieldValidator ID="BookingpassrfvEmail2" runat="server" Text="*" ErrorMessage="Please enter confirm email."
                        ControlToValidate="txtConfirmEmail" ForeColor="#eaeaea" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="BookingpassregEmail2" runat="server" Text="*"
                        ErrorMessage="Please enter valid confirm email." ControlToValidate="txtConfirmEmail"
                        ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic" ValidationGroup="vgs1" />
                    <asp:CompareValidator ID="cmpEmailCompare" ForeColor="#eaeaea" ControlToCompare="txtEmail"
                        Display="Dynamic" ControlToValidate="txtConfirmEmail" runat="server" Text="*"
                        ValidationGroup="vgs1" ErrorMessage="Email and confirm email not matched.">
                    </asp:CompareValidator>
                </div>
                <div class="colum-label-seven">
                    Phone Number<span class="readerror">*</span>
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtBillPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                        MaxLength="20" onkeypress="return isNumberKey(event)" />
                    <asp:RequiredFieldValidator ID="BookingpassrfPhone" runat="server" Text="*" ErrorMessage="Please enter phone number."
                        ControlToValidate="txtBillPhone" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Address Line 1 Or Company Name<span class="readerror">*</span>
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" MaxLength="50" />
                    <asp:RequiredFieldValidator ID="BookingpassrfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                        ControlToValidate="txtAdd" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Address Line 2
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" MaxLength="50"/>
                </div>
                <div class="colum-label-seven">
                    Town / City</div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="input" />
                </div>
                <div class="colum-label-seven">
                    County / State</div>
                <div class="colum-label-eight ">
                    <asp:TextBox ID="txtState" runat="server" CssClass="input" />
                </div>
                <div class="colum-label-seven">
                    Postal/Zip Code<span class="readerror">*</span></div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" Width="44%" MaxLength="10" />
                    <asp:RequiredFieldValidator ID="BookingpassrfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                        ControlToValidate="txtZip" ForeColor="#eaeaea" />
                    <asp:Label ID="lblpmsg" runat="server" Visible="false" ForeColor="Red" Text="Please enter maximum 7 character postal/zip code." />
                </div>
                <div class="colum-label-seven">
                    Country<span class="readerror">*</span></div>
                <div class="colum-label-eight countory">
                    <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <asp:RequiredFieldValidator ID="BookingpassrfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                        ControlToValidate="ddlCountry" InitialValue="0" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Amend Shipping Details if different to billing address
                </div>
                <div class="colum-label-eight">
                    <asp:CheckBox ID="chkShippingfill" runat="server" />
                </div>
                <asp:Label runat="server" ID="lblmsg" />
            </div>
            <%--Shipping Address Start--%>
            <asp:Panel ID="pnlbindshippping" runat="server" Style="display: none;">
                <div class="round-titles">
                    Shipping Address
                </div>
                <div class="booking-detail-in delvery" style="width: 969px">
                    <div class="colum-label-first">
                        Title
                    </div>
                    <div class="colum-label-second">
                        <asp:DropDownList ID="ddlshpMr" runat="server" class="inputsl">
                            <asp:ListItem Text="Mr." Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="Miss" Value="2">
                            </asp:ListItem>
                            <asp:ListItem Text="Mrs." Value="3">
                            </asp:ListItem>
                            <asp:ListItem Text="Ms" Value="4">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="colum-label-third">
                        First Name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-forth">
                        <asp:TextBox ID="txtshpfname" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipFirstName" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping first name." ControlToValidate="txtshpfname"
                            ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-fifth">
                        Last name<span class="readerror">*</span></div>
                    <div class="colum-label-sixth">
                        <asp:TextBox ID="txtshpLast" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipLastName" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping last name." ControlToValidate="txtshpLast"
                            ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-first">
                        Email<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-second">
                        &nbsp;
                    </div>
                    <div class="colum-label-third">
                        &nbsp;
                    </div>
                    <div class="colum-label-forth">
                        <asp:TextBox ID="txtshpEmail" runat="server" CssClass="input validcheck" autocomplete="off"  style='text-transform:lowercase' />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipEmail" runat="server" Text="*" ErrorMessage="Please enter Shipping email address."
                            ControlToValidate="txtshpEmail" ForeColor="#eaeaea" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="regx1" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                            ControlToValidate="txtshpEmail" ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                    </div>
                    <div class="colum-label-fifth">
                        Confirm Email<span class="readerror">*</span></div>
                    <div class="colum-label-sixth">
                        <asp:TextBox ID="txtshpConfirmEmail" runat="server" CssClass="input validcheck" autocomplete="off"  style='text-transform:lowercase' />
                        <asp:RequiredFieldValidator ID="BookingpassrfconfirmShipEmail" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                            ForeColor="#eaeaea" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="*"
                            ErrorMessage="Please enter valid Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                            ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                        <asp:CompareValidator ID="CompareValidator1" ForeColor="#eaeaea" ControlToCompare="txtshpEmail"
                            Display="Dynamic" ControlToValidate="txtshpConfirmEmail" runat="server" Text="*"
                            ErrorMessage="Shipping Email and confirm email not matched." ValidationGroup="vgs1">
                        </asp:CompareValidator>
                    </div>
                    <div class="colum-label-seven">
                        Phone Number<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtShpPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                            MaxLength="20" onkeypress="return isNumberKey(event)" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpPhone" runat="server" Text="*" ErrorMessage="Please enter Shipping phone number."
                            ControlToValidate="txtShpPhone" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Address Line 1 Or Company Name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtshpAdd" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpAdd" runat="server" Text="*" ErrorMessage="Please enter Shipping address."
                            ControlToValidate="txtshpAdd" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Address Line 2</div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtshpAdd2" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        Town / City</div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpCity" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        County / State</div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpState" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        Postal/Zip Code<span class="readerror">*</span></div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpZip" runat="server" CssClass="input validcheck" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpZip" runat="server" Text="*" ErrorMessage="Please enter Shipping postal/zip code."
                            ControlToValidate="txtshpZip" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Country<span class="readerror">*</span></div>
                    <div class="colum-label-eight countory">
                        <asp:DropDownList ID="ddlshpCountry" runat="server" class="inputsl" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpCountry" runat="server" Text="*"
                            ErrorMessage="Please select Shipping country." ControlToValidate="ddlshpCountry"
                            InitialValue="0" ForeColor="#eaeaea" />
                    </div>
                    <asp:Label runat="server" ID="lblshpmsg" />
                </div>
            </asp:Panel>
            <%--Shipping Address End--%>
            <div class="round-titles">
                Date for Departure
            </div>
            <div class="booking-detail-in delvery" style="width: 969px">
                <div class="colum-label-seven">
                    Date of Departure
                    <br />
                    (for fulfillment purposes)</div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtDateOfDepature" runat="server" class="input validcheck calDate"
                        Text="DD/MM/YYYY" Style="margin-right: 4px;" />
                    <asp:RequiredFieldValidator ID="BookingpassrfDateOfDepature" runat="server" Text="*"
                        ErrorMessage="Please enter Date Of Depature." ControlToValidate="txtDateOfDepature"
                        ForeColor="#eaeaea" InitialValue="DD/MM/YYYY" />
                    <span class="imgCal calIcon" id="imgcal2" style="float: left;" title="Select Date of Departure.">
                    </span>
                    <br />
                    <br />
                    &nbsp;If left empty the ticket start date will be used as delivery date.
                </div>
            </div>
            <asp:Panel ID="pnlShipping" runat="server">
                <div class="round-titles">
                    Shipping Option
                </div>
                <div class="booking-detail-in" style="width: 969px">
                    <asp:Repeater ID="rptShippings" runat="server">
                        <ItemTemplate>
                            <div class="colum-input">
                                <input type="radio" id="radioChkShp" class="shipping" value='<%#Eval("Price")+"^"+Eval("ShippingName")+"^"+ Server.HtmlDecode(Eval("description").ToString())%>'
                                    name="chkshiping" onclick="getdata()" />
                                <span>
                                    <%#Eval("ShippingName") %></span>
                            </div>
                            <div class="colum-label t-right">
                                <strong>
                                    <%=currency%>
                                    <%#Eval("Price")%></strong></div>
                            <div style="background-color: #eaeaea; padding: 7px; width: 98%; float: left; line-height: 30px;
                                color: Black; font-size: 13px;">
                                <%#Eval("description")%></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
            <div class="round-titles">
                Total
            </div>
            <div class="booking-detail-in" style="width: 969px">
                <div class="colum-input">
                    &nbsp;</div>
                <div class="colum-label t-right">
                    <strong>
                        <%=currency%>
                        <asp:Label ID="lblTotalCost" runat="server" /></strong></div>
            </div>
            <div>
                <asp:Button ID="btnBack" ClientIDMode="Static" runat="server" Text="Back" CssClass="btn-red-cart f-left"
                    OnClick="btnBack_Click" Width="120px" CausesValidation="False" />
                <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Proceed To Payment"
                    CssClass="btn-red-cart w184 f-right" OnClick="btnCheckout_Click" ValidationGroup="vgs1" />
            </div>
        </div>
    </div>
    <div id="popupDiv">
        <div class="modalBackground progessposition">
        </div>
        <div class="privacy-block-outer pass progess-inner">
            <div class="red-title" style="text-align: left;">
                <span>Ticket Protection</span> <a id="btnOK" href="#" onclick="searchEvent()">X</a>
            </div>
            <div id="divpopupdata" runat="server" class="discription-block" style="text-align: left;
                overflow-x: scroll; height: 300px; font-size: 13px;">
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnShippingCost" runat="server" Value="0" />
    <script type="text/javascript">
        function showthis() {
            document.getElementById('popupDiv').style.display = 'block';
        }

        function searchEvent() {
            document.getElementById('popupDiv').style.display = 'none';
        } 
    </script>
</asp:Content>
