﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Cookies.aspx.cs" Inherits="Cookies" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Cookies </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Cookies</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content">
    <h1><asp:Literal ID="ltrTitle" runat="server" /></h1>
    <p>   <asp:Literal ID="ltrDesc" runat="server" />   </p>
    <p>&nbsp; </p>
    <div class="clear"> &nbsp;</div>     
</div>
<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
