﻿using System;
using System.Linq;
using Business;
using System.Configuration;
using System.Web;

public partial class RailPassSection3 : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    readonly Masters _master = new Masters();
    private Guid _siteId;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            trainresults.HRef = _db.tblSites.Any(x => x.ID == _siteId && x.IsTrainTickets) ? siteURL + "traintickets" : siteURL + "trainresults";
            Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
            divJRF.Visible = _master.IsVisibleJRF(_siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindInfo(_siteId);
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, _siteId);
        }
    }

    public void BindInfo(Guid siteId)
    {
        var result = _db.tblRailPassSec3.FirstOrDefault(ty => ty.SiteId == siteId);
        if (result != null)
        {
            lblTitle.Text = result.Name;
            if (!string.IsNullOrEmpty(result.Imagepath))
                imgRt.Src = adminSiteUrl + result.Imagepath;
            divDescription.InnerHtml = Server.HtmlDecode((result.Description));
        }
    }
    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);
                rtPannel.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}