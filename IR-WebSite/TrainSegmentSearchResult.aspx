﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TrainSegmentSearchResult.aspx.cs" Inherits="TrainSegmentSearchResult"
    Culture="en-GB" %>

<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Src="RegionalTrainsUc.ascx" TagName="RegionalTrainsUc" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
 <script src="Scripts/gmap.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var start = document.getElementById('txtFrom').value;
            var matchSt = null;
            if (start.indexOf('(') > "-1") {
                var regExp1 = /\(([^)]+)\)/;
                matchSt = regExp1.exec(start);
            }

            if (((start.toLowerCase()).indexOf('st ') > "-1") || ((start.toLowerCase()).indexOf('le ') > "-1") || ((start.toLowerCase()).indexOf('la ') > "-1") || ((start.toLowerCase()).indexOf('s.') > "-1")) {
                start = (start.replace(" ", "-"));
                start = (start.replace(".", ""));
                start = (start.replace(/'/g, ""));

                //if (start.indexOf('(') > "-1") {
                //start = (start.substring(0, start.indexOf('(')));
                //}
            } else {
                if (start.indexOf(' ') > "-1") {
                    start = (start.substring(0, start.indexOf(' ')));
                }
            }

            if ((start.toLowerCase().indexOf('naples') > "-1") || (start.toLowerCase().indexOf('alba') > "-1")) {
                start = start + " italy";
            }
            if (matchSt != null)
                start = start + " " + matchSt[1];
            //alert(start);
            
            /***************************************************/

            var end = document.getElementById('txtTo').value;
            var matchEnd = null;
            if (end.indexOf('(') > "-1") {
                var regExp = /\(([^)]+)\)/;
                matchEnd = regExp.exec(end);
            }

            if (((end.toLowerCase()).indexOf('st ') > "-1") || ((end.toLowerCase()).indexOf('le ') > "-1") || ((end.toLowerCase()).indexOf('la ') > "-1") || ((end.toLowerCase()).indexOf('s.') > "-1")) {
                end = (end.replace(" ", "-"));
                end = (end.replace(".", ""));
                end = (end.replace(/'/g, ""));
                //if (end.indexOf('(') > "-1") {
                //end = (end.substring(0, end.indexOf('(')));
                //}
            }
            else {
                if (end.indexOf(' ') > "-1") {
                    end = (end.substring(0, end.indexOf(' ')));
                }
            }

            if ((end.toLowerCase().indexOf('naples') > "-1") || (end.toLowerCase().indexOf('alba') > "-1")) {
                end = end + " italy";
            }

            if (matchEnd != null)
                end = end + " " + matchEnd[1];
            //alert(end);

            var geocoder = new google.maps.Geocoder();
            initializeGMap();
            geocoder.geocode({ 'address': start }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
            geocoder.geocode({ 'address': end }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }); 
    </script>
    <style type="text/css">
        .clsError
        {
            color: #D8000C !important;
            border: 1px solid red;
            background-color: #FFBABA;
            -webkit-border-radius: 5px 5px 5px 5px;
            -ms-border-radius: 5px 5px 5px 5px;
        }
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsAbs
        {
            display: none;
        }
        .btnInactive
        {
            background: #f6f8f9 !important; /* Old browsers */
            background: -moz-linear-gradient(top,  #f6f8f9 0%, #f2f5f6 49%, #dde4e7 50%, #edeeee 100%) !important; /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f6f8f9), color-stop(49%,#f2f5f6), color-stop(50%,#dde4e7), color-stop(100%,#edeeee)) !important; /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* IE10+ */
            background: linear-gradient(to bottom,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* W3C */
            -pie-background: linear-gradient(#f6f8f9, #edeeee) !important;
            font-size: 13px !important;
        }
        .btnactive
        {
            border-radius: 5px 5px 0 0 !important;
            -moz-border-radius: 5px 5px 0 0 !important;
            -webkit-border-radius: 5px 5px 0 0 !important;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            position: relative;
            cursor: pointer;
            border: 1px solid #b3b3b3 !important;
            border-bottom: 0px;
            background: #b1085e !important; /* Old browsers */
            background: -moz-linear-gradient(top,  #b1085e 0%, #75043d 100%) !important; /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b1085e), color-stop(100%,#75043d)) !important; /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #b1085e 0%,#75043d 100%) !important; /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #b1085e 0%,#75043d 100%) !important; /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #b1085e 0%,#75043d 100%) !important; /* IE10+ */
            background: linear-gradient(to bottom,  #b1085e 0%,#75043d 100%) !important; /* W3C */
            -pie-background: linear-gradient(#b1085e, #75043d) !important;
            font-size: 13px !important;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
        .banner
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .d-opt
        {
            opacity: 0.5;
            cursor: default;
        }
        .m-none
        {
            margin-left: 0 !important;
        }
        .input
        {
            margin-left: 0px !important;
        }
        .loading
        {
            background-image: url(images/loading3.gif);
            background-position: right;
            background-repeat: no-repeat;
        }
        .modalBackground
        {
            position: fixed;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            overflow: hidden;
            padding: 0;
            margin: 0;
            background-color: #000;
            filter: alpha(opacity=50);
            opacity: 0.5;
            text-align: center;
            float: left;
            z-index: 99;
        }
        .progessposition
        {
            padding-top: 20%;
        }
        
        
        .full-row
        {
            clear: both;
            width: 100%;
        }
        .colum03
        {
            color: #424242;
            float: left;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            padding: 5px 0;
            width: 45%;
        }
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
            font-size: 13px;
            color: #424242;
            position: relative;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        .aspNetDisabled
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 24px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
            margin-right: 5px;
        }
        select.aspNetDisabled
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 25px;
            width: 95%; /*padding: 0.5%;*/
            padding: 5px 3px !important;
        }
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 40%; /*padding: 0.5%;*/
            padding: 5px 3px !important;
            margin-left: 5px;
        }
        .booking-detail-in .full-row .colum03 .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 50%; /*padding: 0.5%;*/
            padding: 5px 3px !important;
        }
        .lblock
        {
            background-color: #FFF;
            color: #424242;
            font-size: 13px;
            padding: 10px;
        }
        
        .divLoy
        {
            float: left;
            margin: 5px;
            width: 250px;
        }
        .divLoyleft
        {
            float: left;
            margin-bottom: 5px;
            width: 95px;
        }
        .divLoyright
        {
            float: left;
            margin-bottom: 5px;
            width: 150px;
        }
        .divLoyleft
        {
            float: left;
            margin-bottom: 5px;
            width: 95px;
        }
        .divLoyright
        {
            float: left;
            margin-bottom: 5px;
            width: 150px;
        }
        .divLoy input[type="text"]
        {
            background: none repeat scroll 0 0 #D5D4D4;
            border: 0 none;
            color: #333333;
            float: left;
            height: 25px !important;
            line-height: 25px !important;
            text-indent: 5px;
            width: 150px !important;
        }
        .txtLoyleft
        {
            background: none repeat scroll 0 0 #696969 !important;
            border: 0 none;
            color: #000000;
            float: left;
            height: 25px !important;
            line-height: 25px !important;
            text-indent: 5px;
            width: 90px !important;
        }
        .bprice1
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
    </asp:ToolkitScriptManager>
    <%--Banner section--%>
    <div class="banner">
        <div id="dvBanner" class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content">
<div class="left-content">
<h1> Train Search Result </h1>
<p id="pCls" runat="server">
 <asp:Label ID="lblMessageEarlierTrain" runat="server" Text="" Visible="false"></asp:Label>
</p>
<uc2:RegionalTrainsUc ID="RegionalTrainsUc1" runat="server" />
<div class="clear"> &nbsp;</div>
<div class="clear"> &nbsp;</div>
<div class="country-block-outer" style="display:none!important;;">
    <div class="country-block-inner">
        <div id="footerBlock" runat="server"></div>
    </div>
</div>
</div><div class="right-content">
        <div class="ticketbooking" style="padding-top:0px">
            <div class="list-tab divEnableP2P">
                <ul>
                    <li><a href="#" class="active">Rail Tickets </a></li>
                    <li><a href="rail-passes">Rail Passes </a></li>
                </ul>
            </div>
            <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
            <img src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
        </div>
         <div class="newsletter-outer">
      <div class="title bg-red">
    Route
</div>
<div class="newsletter-inner width-n">
    <div class="results-map">
        <div id="mymap" class="results-map" style="height: 300px; width: 263px">
        </div>
    </div>
</div> 
    <img src='<%=siteURL%>images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
</div>
<div class="newsletter-outer">
    <asp:HiddenField ID="hdnPassenger" runat="server"></asp:HiddenField>
    <div class="title bg-red">  My Itinerary <i> <%--<a href="#"><img src="<%=siteURL%>images/pin-left.png" alt="" border="0" /></a>--%> </i>  </div>
    <div class="newsletter-inner width-n">
        <asp:Label ID="lblSDetail" runat="server" Text=""></asp:Label>  
    <input id="btnSummarySubmit" name="btnSummarySubmit" type="submit" value="Continue" class="f-right btn-continue" style="display:none;" />
    </div>
    <img src='<%=siteURL%>images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
</div>
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
