﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Business;
using OneHubServiceRef;
/// <summary>
/// Summary description for StationList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]

public class StationList : System.Web.Services.WebService
{
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static DateTime curentDateTime;
    public ManageBooking _booking = new ManageBooking();
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public StationList()
    {
        if (DateTime.Now > curentDateTime)
        {
            curentDateTime = DateTime.Now.AddDays(10);
            getStaionList();
        }
    }

    public void getStaionList()
    {
        list = _booking.GetAllStaionList().ToList();
    }

    [WebMethod(EnableSession = true)]
    public string[] getStationsXList(string prefixText, string filter, string station)
    {

        string SiteId = "";
        if (System.Web.HttpContext.Current.Session.Keys.Count == 0)
        {
            System.Web.HttpContext.Current.Session["siteId"] = "668de883-c0e4-47df-bc04-4c4e999a7f5f";
            SiteId = "668de883-c0e4-47df-bc04-4c4e999a7f5f";
        }
        else
            SiteId = (prefixText.Split('*').Count() > 1) ? prefixText.Split('*')[2] : System.Web.HttpContext.Current.Session["siteId"].ToString();



        prefixText = prefixText.Split('*')[0];
        List<getStationList_Result> listStation = list;
        prefixText = prefixText.Replace("♥", "'");
        List<getStationList_Result> listStation2 = new List<getStationList_Result>();
        string[] resultdata = { };
        if (filter != string.Empty)
        {
            listStation2.Clear();
            string[] code = filter.Split(',');
            for (int i = 0; i < code.Length; i++)
            {
                var data = list.Where(x => x.StationFilterCode == code[i]).ToList();
                foreach (var item in data)
                {
                    getStationList_Result obj = new getStationList_Result();
                    obj.StationEnglishName = item.StationEnglishName;
                    obj.StationCode = item.StationCode;
                    obj.StationName = item.StationName;
                    obj.StationCodeList = item.StationCodeList;
                    obj.RailName = item.RailName;
                    listStation2.Add(obj);
                }
            }
            listStation = listStation2;
        }


        listStation = CheckActiveAPI(listStation, SiteId);

        if (listStation != null)
        {

            if ("668de883-c0e4-47df-bc04-4c4e999a7f5f" == SiteId)
            {
                //Australia P2P site has blocked TI search and below Brussel stations  
                List<string> listBlockedStation = new List<string> { "BEMEI", "BEMER", "BESHU", "BEABT", "BEBCE", "BEBCO", "BEBNO", "BEBQL", "BEBXW", "BECON", "BEDEL" };
                resultdata = listStation.Where(x => x.RailName.Contains("BENE") && !listBlockedStation.Contains(x.StationCode) && x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
            }
            else
                resultdata = listStation.Where(x => x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();

            if (resultdata.Length == 0)
                return new string[] { "Station not found !" };
            return resultdata;
        }
        else
            return new string[] { "Station not found !" };
    }

    private List<getStationList_Result> CheckActiveAPI(List<getStationList_Result> list, string siteid)
    {
        try
        {
            db_1TrackEntities db = new db_1TrackEntities();
            Guid siteId = Guid.Parse(siteid);
            var listApiLogin = db.tblApiLoginDetailSiteLookups.Where(x => x.SiteID == siteId).ToList();

            if (listApiLogin.Count < 1)
                return null;

            bool isBeNe = false;
            bool isTi = false;
            foreach (var item in listApiLogin)
            {
                if (item.TrainName.Trim() == "TrenItalia")
                    isTi = true;

                else if (item.TrainName.Trim() == "BeNe")
                    isBeNe = true;
            }

            List<Guid> siteList = new List<Guid> { Guid.Parse("F72318EC-CB00-421C-9DA5-2F1D48C7BFE5"), Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D"), Guid.Parse("F06549A9-139A-44F9-8BEE-A60DC5CC2E05"), Guid.Parse("FF3A353E-CD84-42CF-855D-D3AF92128647"), Guid.Parse("107F17AC-580C-4D94-A7F7-E858ADADAB4F") };
            if (isTi && !isBeNe)
                return list.Where(x => x.StationCodeList == null).ToList();
            else if (!isTi && isBeNe)
                return list.Where(x => x.StationCodeList != null).ToList();
            else if (isTi && isBeNe)
            {
                list = db.tblSites.Any(x => x.ID == siteId && x.IsAgent == false) ? list.Where(x => x.StationCode != "GBSPX" & x.StationCode != "GBLWB").ToList() : list;
                if (siteList.Contains(siteId))
                    list = list.Where(x => x.StationCode != "GBASI" & x.StationCode != "GBEBF").ToList();

                return list;
            }


            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


   [WebMethod(EnableSession = true)]
    public FareDetailsBene[] GetFare()
    {
        FareRequest objrequest = System.Web.HttpContext.Current.Session["farerequest"] as FareRequest; 
        var client = new OneHubRailOneHubClient();
        FareDetailsBene[] response = client.GetFare(objrequest);
        return response;
    }
    


}
