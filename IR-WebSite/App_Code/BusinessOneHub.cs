﻿using System;
using OneHubServiceRef;
using System.Collections.Generic;
using System.Xml.Serialization;

public static class BusinessOneHub
{
    public static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;

        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        return isNum;
    }

    public static string CurrencyConverterRate()
    {
        try
        {
            return "";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}

public class Passanger
{
    public string PassangerType { get; set; }
    public string cardnumber { get; set; }
}
public class ShoppingCartDetails
{
    public Guid Id { get; set; }

    public string DepartureStation { get; set; }
    public string DepartureDate { get; set; }
    public string DepartureTime { get; set; }

    public string ArrivalStation { get; set; }
    public string ArrivalDate { get; set; }
    public string ArrivalTime { get; set; }

    public string TrainNo { get; set; }
    public string ServiceName { get; set; } //premium
    public string Passenger { get; set; }
    public string Fare { get; set; } //offer 
    public string Title { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Price { get; set; }
    public decimal netPrice { get; set; }

}

public class BookingRequest
{
    public PurchasingForServiceRequest PurchasingForServiceRequest { get; set; }
    public PurchasingForServiceOwnerRequest PurchasingForServiceOwnerRequest { get; set; }
    public bool IsInternational { get; set; }
    public Guid Id { get; set; }
    public string DepartureStationName { get; set; }
}
public class StationInfo
{

    public Stationst Station { get; set; }
}
public class DBStation
{

    public string OldStationName { get; set; }
    public string NewStationName { get; set; }
}
public class Stationst
{

    public string StationName { get; set; }

    public BCode BCode { get; set; }

    public ICode ICode { get; set; }

    public bool IsActive { get; set; }

    public int Preference { get; set; }
}

public class BCode
{

    public string StationName { get; set; }

    public string RailwayCode { get; set; }

    public string StationCode { get; set; }
}

public class ICode
{

    public string StationName { get; set; }

    public string RailwayCode { get; set; }

    public string StationCode { get; set; }
}

public class BookingResponse
{
    public string ReservationCode { get; set; }
    public string ChangeReservationCode { get; set; }

    public bool Issued { get; set; }//TI
    public long UnitOfWork { get; set; }//TI
    public List<string> PdfUrl { get; set; }//TI

    public string Pnr { get; set; }//BE
    public string PinCode { get; set; }//BE
    public string DepStationName { get; set; }//BE
   
    public BillingAddress BillingAddress { get; set; }
}
public class BookingRequestUserControl
{
    public bool isIhaveRailPass { get; set; }

    public string FromDetail { get; set; }
    public string ToDetail { get; set; }
    public DateTime depdt { get; set; }
    public DateTime depTime { get; set; }
    public string depRCode { get; set; }
    public string depstCode { get; set; }
    public string arrRCode { get; set; }
    public string arrstCode { get; set; }
    public int ClassValue { get; set; }
    public int Adults { get; set; }
    public int Boys { get; set; }
    public int Seniors { get; set; }
    public int Youths { get; set; }
    public int Transfare { get; set; }
    public string ReturnDate { get; set; }
    public string ReturnTime { get; set; }
    public Boolean Loyalty { get; set; }
    public string Journeytype { get; set; }
    public Guid ClassId { get; set; }
    public Guid CurrencyId { get; set; }
    public List<Loyaltycard> lstLoyalty { get; set; }
    public string OneHubServiceName { get; set; }
    public bool IsReturnJurney { get; set; }
}
public class PaymentGateWayTransffer
{
    public double Amount { get; set; }
    public string customerEmail { get; set; }
    public string orderReference { get; set; }
    public string currencyCode { get; set; }
}
public class TrainInformationListNew
{

    [XmlElement]
    public string TrainNumber { get; set; }
    [XmlElement]
    public string TrainDescr { get; set; }
    [XmlElement]
    public string TrainCategoryCode { get; set; }
    [XmlElement]
    public string TrainCategory { get; set; }

    [XmlElement]
    public string DepartureStationCode { get; set; }
    [XmlElement]
    public string DepartureStationName { get; set; }
    [XmlElement]
    public string DepartureRailwayCode { get; set; }
    [XmlElement]
    public string DepartureDate { get; set; }
    [XmlElement]
    public string DepartureTime { get; set; }

    [XmlElement]
    public string ArrivalStationCode { get; set; }
    [XmlElement]
    public string ArrivalStationName { get; set; }
    [XmlElement]
    public string ArrivalRailwayCode { get; set; }
    [XmlElement]
    public string ArrivalDate { get; set; }
    [XmlElement]
    public string ArrivalTime { get; set; }

    [XmlElement]
    public string TravelTimeDuration { get; set; }

    [XmlElement]
    public string JourneySolutionCode { get; set; }

    [XmlElement]
    public string TripType { get; set; }

    [XmlElement]
    public string Route { get; set; }

    [XmlElement]
    public string NumberOfTransfer { get; set; }

    [XmlElement]
    public string Routesummaryid { get; set; }

    [XmlElement]
    public List<PriceResponse> PriceInfo { get; set; }

    [XmlElement]
    public List<TrainInfoSegment> TrainInfoSegment { get; set; }

    [XmlElement]
    public List<BookingRequestInfo> BookingRequestInfo { get; set; } //--BeNe


}



