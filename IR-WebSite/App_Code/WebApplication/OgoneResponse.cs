﻿using System;
using System.Configuration;
using System.Web;

namespace WebApplication
{
    public static class OgoneResponse
    {
        public static string OrderId { get { return HttpContext.Current.Request["orderid"]; } }
        public static string Currency { get { return HttpContext.Current.Request["currency"]; } }
        public static string Amount { get { return HttpContext.Current.Request["amount"]; } }
        public static string PaymentMethod { get { return HttpContext.Current.Request["pm"]; } }
        public static string AcceptanceCode { get { return HttpContext.Current.Request["acceptance"]; } }
        public static string CardNumber { get { return HttpContext.Current.Request["cardno"]; } }
        public static string PaymentId { get { return HttpContext.Current.Request["payid"]; } }
        public static string NcError { get { return HttpContext.Current.Request["ncerror"]; } }
        public static string Brand { get { return HttpContext.Current.Request["brand"]; } }
        public static string CVC { get { return HttpContext.Current.Request["cvc"]; } }
        public static string ED { get { return HttpContext.Current.Request["ed"]; } }
        public static string CVCCheck { get { return HttpContext.Current.Request["cvccheck"]; } }
        public static string CN { get { return HttpContext.Current.Request["cn"]; } }
        public static string ShaSign { get { return HttpContext.Current.Request["shasign"]; } }

        public static OgonePaymentStatus Status
        {
            get
            {
                int status;
                if (!Int32.TryParse(HttpContext.Current.Request["status"], out status))
                {
                    return OgonePaymentStatus.IncompleteOrInvalid;
                }
                return (OgonePaymentStatus)status;
            }
        }
        public static string rCardNumber
        {
            get
            {
                return HttpContext.Current.Request["cardno"].ToString();
            }
        }
        public static bool IsShaSignValid
        {
            get
            {
                string key = OrderId + Currency + Amount + AcceptanceCode + (int)Status + CardNumber +
                             PaymentId + NcError ;
                return true; 
                    //ShaSign == Ogone.GenerateHash(key);
            }
        }
    }
}
