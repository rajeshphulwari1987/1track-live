﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="BookingCart.aspx.cs" Inherits="BookingCart"
    UICulture="en" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucPassDetails" Src="~/UserControls/UCpassdetalis.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link id="lnkBookStyle" runat="server" href="Styles/BookingCart.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .popuppostcodefilter div
        {
            width: 100%;
            cursor: pointer;
        }
        .left-content p
        {
            background-color: transparent;
        }
        .popuppostcodefilter div:hover
        {
            background-color: #ccc;
        }
        .popuppostcodefilter
        {
            height: 380px;
            overflow-y: auto;
            width: 450px;
            background: #fff !important;
            position: relative;
            z-index: 999;
            border: 1px solid #d3d3d3;
            padding: 10px;
            padding-right: 16px;
        }
        .popupselect
        {
            color: #000 !important;
            padding: 1px;
            z-index: 80;
            padding-left: 5px;
            background: #fff;
            position: relative;
            border-top: 1px dashed #d3d3d3;
        }
    </style>
    <script type="text/javascript">
        var dshiop = '';
        $(document).ready(function () {
            $(document).keypress(function (e) { if (e.keyCode === 13) { e.preventDefault(); return false; } });
            ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], false);
            $("#MainContent_txtDateOfDepature").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            ShowCall();
            getdata();
            restrictCopy();
            $(".imgCal").click(function () {
                $("#MainContent_txtDateOfDepature").datepicker('show');
            });
            $(".imgCal1").click(function () {
                if ($('#MainContent_ucTrainSearch_rdBookingType_1').is(':checked')) {
                    $("#txtReturnDate").datepicker('show');
                }
            });
            $(".GetMinPassDate").change(function () {
                var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                var Datelist = new Array();
                $(".GetMinPassDate").each(function (key, value) {
                    var SelectDate = $(this).val();
                    var MM = 0;
                    if (SelectDate != 'DD/MMM/YYYY') {
                        var date = SelectDate.split('/');
                        $.each(m_names, function (key, value) {
                            if (value == date[1]) {
                                MM = key;
                            }
                        });
                        var d = new Date(Number(date[2]), (MM), Number(date[0]));

                        Datelist[key] = d;
                    }
                });
                var maxDate = new Date(Math.min.apply(null, Datelist));
                var dep_date = maxDate.getDate();
                var dep_month = maxDate.getMonth();
                var dep_year = maxDate.getFullYear();
                var depDt = dep_date + "/" + m_names[dep_month] + "/" + dep_year;
                $(".calDateDOD").datepicker("option", "maxDate", maxDate);
                $(".calDateDOD").val(depDt);
            });
            /*shipping details*/
            $("#MainContent_chkShippingfill").click(function () {
                if ($(this).is(':checked')) {
                    $("#MainContent_pnlbindshippping").show();
                    ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], true);
                    $("#MainContent_ddlshpMr").prop("selectedIndex", $("#MainContent_ddlMr").prop("selectedIndex"));
                    $("#MainContent_txtshpfname").val($("#txtFirst").val());
                    $("#MainContent_txtshpLast").val($("#MainContent_txtLast").val());
                    $("#MainContent_txtshpEmail").val($("#MainContent_txtEmail").val());
                    $("#MainContent_txtshpConfirmEmail").val($("#MainContent_txtConfirmEmail").val());
                    $("#MainContent_txtShpPhone").val($("#MainContent_txtBillPhone").val());
                    $("#MainContent_txtshpAdd").val($("#MainContent_txtAdd").val());
                    $("#MainContent_txtshpAdd2").val($("#MainContent_txtAdd2").val());
                    $("#MainContent_txtshpCity").val($("#MainContent_txtCity").val());
                    $("#MainContent_txtshpState").val($("#MainContent_txtState").val());
                    $("#MainContent_txtshpZip").val($("#MainContent_txtZip").val());
                    $("#MainContent_ddlshpCountry").prop("selectedIndex", $("#MainContent_ddlCountry").prop("selectedIndex"));
                }
                else {
                    $("#MainContent_pnlbindshippping").hide();
                    ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], false);
                }
                customselect();
            });
        });
        function postgetpostcodedetail(e) {
            var currentparentid = $(e).parent().prev().attr('id');
            var data = $(e).text();
            var _dataS = data.split(',');
            if (currentparentid == 'MainContent_txtZip') {
                $("#MainContent_txtAdd").val(_dataS[0]);
                if (_dataS.length == 3) {
                    $("#MainContent_txtAdd2").val(_dataS[2]);
                    $("#MainContent_txtCity").val(_dataS[1]);
                }
                else {
                    $("#MainContent_txtCity").val(_dataS[1]);
                }
            }
            if (currentparentid == 'MainContent_txtshpZip') {
                $("#MainContent_txtshpAdd").val(_dataS[0]);
                if (_dataS.length == 3) {
                    $("#MainContent_txtshpAdd2").val(_dataS[2]);
                    $("#MainContent_txtshpCity").val(_dataS[1]);
                }
                else {
                    $("#MainContent_txtshpCity").val(_dataS[1]);
                }
            }
        }
        function callshipping() {
            getdata();
            alert("Please choose shipping amount.");
        }
        function restrictCopy() {
            $(function () {
                $('#MainContent_txtConfirmEmail,#MainContent_txtshpConfirmEmail').bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
            });
        }
        function ShowCall() {
            $(".calDateDOD").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0
            });
            $(".calDateDOD").keypress(function (event) { event.preventDefault(); });
        }
        function lastshipping() {
            if (dshiop != '')
                $("#" + dshiop).attr("checked", "checked");
            else
                $("#radioChkShp1").attr("checked", "checked");
        }
        function getdata() {
            $(document).ready(function () {
                lastshipping();
                var countPrice = 0.00;
                var bookingFee = 0.00;
                var counttickprotectionAmt = 0;
                $(".calculateTotal").each(function () {
                    var AmountTPK = $(this).prev().text();
                    if ($(this).find('input').is(":checked"))
                        counttickprotectionAmt += parseFloat(AmountTPK);
                });
                $(".passprice").each(function () {
                    countPrice += parseFloat($(this).val());
                });
                $("#hdntotalSaleamount").val(countPrice);
                countPrice = parseFloat($("#hdntotalSaleamount").val());
                JSONcall(countPrice + counttickprotectionAmt);
                bookingFee = parseFloat($("#MainContent_lblBookingFee").text());
                countPrice = bookingFee + countPrice + counttickprotectionAmt;
                $("#lblTotal").text(countPrice.toFixed(2));
                $(".shipping").each(function () {
                    if ($(this).is(":checked")) {
                        var str = $(this).val().split('^');
                        var price = str[0];
                        var smethod = str[1];
                        var sdesc = str[2];
                        sdesc = $('<div/>').html(sdesc).text();
                        $("#MainContent_hdnShippingCost").attr('value', price.toString());
                        $("#MainContent_hdnShipMethod").attr('value', smethod.toString());
                        $("#MainContent_hdnShipDesc").attr('value', sdesc.toString());
                        countPrice = countPrice + parseFloat(price);
                    }
                });
                var Adminfeeval = 0;
                if ($("#hdnAdminFee").val().match(/sum/i)) {
                    Adminfeeval = parseFloat($("#hdnAdminFee").val().replace("sum", ''));
                }
                else if ($("#hdnAdminFee").val().match(/mult/i)) {
                    Adminfeeval = parseFloat($("#hdnAdminFee").val().replace("mult", '')) * countPrice;
                }
                $("#hdnAdminFeeAmount").val(Adminfeeval.toFixed(2));
                $("#MainContent_lblAdminFee").text(Adminfeeval.toFixed(2));
                $("#HdnTotalCost").val((Adminfeeval + countPrice).toFixed(2));
                $("#MainContent_lblTotalCost").text((Adminfeeval + countPrice).toFixed(2));

                var Discount = $("#MainContent_txtDiscountCode").val();
                if (Discount.length > 0) {

                    var hostUrl = window.location + '.aspx/chk_Discount';
                    $.ajax({
                        url: hostUrl,
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        data: "{'DiscountCode':'" + Discount + "'}",
                        success: function (Pdata) {
                            if (Pdata.d) {
                                $("#Errormsg").hide();
                                $("#btnCheckout").removeAttr("disabled");
                            }
                            else {
                                $("#Errormsg").show();
                                $("#btnCheckout").attr('disabled', 'disabled');
                            }
                        },
                        error: function () {
                        }
                    });
                }
            });
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function customselect() {
            var sta = $("span").hasClass("customSelect");
            if (sta) {
                $(".customSelect").remove();
                $('select').customSelect();
            }
        }
        function JSONcall(TotalSalePrice) {
            var hostUrl = window.location + '.aspx/chk_CheckedChanged';
            $(document).ready(function () {
                $.ajax({
                    url: hostUrl,
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    data: "{'TotalSalePrice':'" + TotalSalePrice + "'}",
                    success: function (Pdata) {
                        $("#MainContent_lblBookingFee").val(Pdata.d);
                    },
                    error: function () {
                    }
                });
            });
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdntotalSaleamount" runat="server" Value="0.00" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnShipMethod" runat="server" Value="" />
    <asp:HiddenField ID="hdnShipDesc" runat="server" Value="" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptGlobalization="true">
    </asp:ScriptManager>
    <div class="content">
        <div class="left-content" style="width: 100%">
            <h1>
                Booking Details
            </h1>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <uc:ucPassDetails runat="server" ID="UcPassDetail" />
            <div class="round-titles">
                Billing Address
                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="vgs1" DisplayMode="List"
                    ShowSummary="false" ShowMessageBox="true" runat="server" />
            </div>
            <div class="booking-detail-in delvery" style="height: 383px; width: 969px; height: 425px">
                <div class="colum-label-first">
                    Title
                </div>
                <div class="colum-label-second">
                    <asp:DropDownList ID="ddlMr" runat="server" class="inputsl">
                        <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                        <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Mrs." Value="3"></asp:ListItem>
                        <asp:ListItem Text="Ms" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="colum-label-third">
                    First Name<span class="readerror">*</span></div>
                <div class="colum-label-forth">
                    <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                    <asp:RequiredFieldValidator ID="BookingpassrfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                        ControlToValidate="txtFirst" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-fifth">
                    Last name<span class="readerror">*</span></div>
                <div class="colum-label-sixth">
                    <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" />
                    <asp:RequiredFieldValidator ID="BookingpassrfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                        ControlToValidate="txtLast" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-first">
                    Email<span class="readerror">*</span></div>
                <div class="colum-label-second">
                    &nbsp;
                </div>
                <div class="colum-label-third">
                    &nbsp;
                </div>
                <div class="colum-label-forth">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" autocomplete="off" style='text-transform:lowercase'>
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="BookingpassrfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                        ControlToValidate="txtEmail" ForeColor="#eaeaea" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="BookingpassrevEmail" runat="server" Text="*"
                        ErrorMessage="Please enter a valid email." ControlToValidate="txtEmail" ForeColor="#eaeaea"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                        ValidationGroup="vgs1" />
                </div>
                <div class="colum-label-fifth">
                    Confirm Email<span class="readerror">*</span></div>
                <div class="colum-label-sixth">
                    <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="input validcheck" autocomplete="off" style='text-transform:lowercase'/>
                    <asp:RequiredFieldValidator ID="BookingpassrfvEmail2" runat="server" Text="*" ErrorMessage="Please enter confirm email."
                        ControlToValidate="txtConfirmEmail" ForeColor="#eaeaea" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="BookingpassregEmail2" runat="server" Text="*"
                        ErrorMessage="Please enter valid confirm email." ControlToValidate="txtConfirmEmail"
                        ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic" ValidationGroup="vgs1" />
                    <asp:CompareValidator ID="BookingpasscmpEmailCompare" ForeColor="#eaeaea" ControlToCompare="txtEmail"
                        Display="Dynamic" ControlToValidate="txtConfirmEmail" runat="server" Text="*"
                        ValidationGroup="vgs1" ErrorMessage="Email and confirm email not matched."></asp:CompareValidator>
                </div>
                <div class="colum-label-seven">
                    Phone Number<span class="readerror">*</span>
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtBillPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                        MaxLength="20" onkeypress="return isNumberKey(event)" />
                    <asp:RequiredFieldValidator ID="BookingpassrfPhone" runat="server" Text="*" ErrorMessage="Please enter phone number."
                        ControlToValidate="txtBillPhone" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Address Line 1 Or Company Name<span class="readerror">*</span>
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" />
                    <asp:RequiredFieldValidator ID="BookingpassrfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                        ControlToValidate="txtAdd" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Address Line 2
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" />
                </div>
                <div class="colum-label-seven">
                    Town / City</div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="input" />
                </div>
                <div class="colum-label-seven">
                    County / State</div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtState" runat="server" CssClass="input" />
                </div>
                <div class="colum-label-seven">
                    Postal/Zip Code<span class="readerror">*</span></div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" Width="44%" MaxLength="10"
                        autocomplete="off" />
                    <asp:RequiredFieldValidator ID="BookingpassrfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                        ControlToValidate="txtZip" ForeColor="#eaeaea" />
                    <asp:Label ID="lblpmsg" runat="server" Visible="false" ForeColor="Red" Text="Please enter maximum 7 character postal/zip code." />
                </div>
                <div class="colum-label-seven">
                    Country<span class="readerror">*</span></div>
                <div class="colum-label-eight countory">
                    <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <asp:RequiredFieldValidator ID="BookingpassrfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                        ControlToValidate="ddlCountry" InitialValue="0" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Amend Shipping Details if different to billing address
                </div>
                <div class="colum-label-eight">
                    <asp:CheckBox ID="chkShippingfill" runat="server" />
                </div>
                <asp:Label runat="server" ID="lblmsg" />
            </div>
            <asp:Panel ID="pnlbindshippping" runat="server" Style="display: none;">
                <div class="round-titles">
                    Shipping Address
                </div>
                <div class="booking-detail-in delvery" style="width: 969px;">
                    <div class="colum-label-first">
                        Title
                    </div>
                    <div class="colum-label-second">
                        <asp:DropDownList ID="ddlshpMr" runat="server" class="inputsl">
                            <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                            <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Mrs." Value="3"></asp:ListItem>
                            <asp:ListItem Text="Ms" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="colum-label-third">
                        First Name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-forth">
                        <asp:TextBox ID="txtshpfname" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipFirstName" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping first name." ControlToValidate="txtshpfname"
                            ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-fifth">
                        Last name<span class="readerror">*</span></div>
                    <div class="colum-label-sixth">
                        <asp:TextBox ID="txtshpLast" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipLastName" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping last name." ControlToValidate="txtshpLast"
                            ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-first">
                        Email<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-second">
                        &nbsp;
                    </div>
                    <div class="colum-label-third">
                        &nbsp;
                    </div>
                    <div class="colum-label-forth">
                        <asp:TextBox ID="txtshpEmail" runat="server" CssClass="input validcheck" autocomplete="off" style='text-transform:lowercase'/>
                        <asp:RequiredFieldValidator ID="BookingpassrfshipEmail" runat="server" Text="*" ErrorMessage="Please enter Shipping email address."
                            ControlToValidate="txtshpEmail" ForeColor="#eaeaea" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="Bookingpassregx1" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                            ControlToValidate="txtshpEmail" ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                    </div>
                    <div class="colum-label-fifth">
                        Confirm Email<span class="readerror">*</span></div>
                    <div class="colum-label-sixth">
                        <asp:TextBox ID="txtshpConfirmEmail" runat="server" CssClass="input validcheck" autocomplete="off" style='text-transform:lowercase'/>
                        <asp:RequiredFieldValidator ID="BookingpassrfconfirmShipEmail" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                            ForeColor="#eaeaea" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="BookingpassRegularExpressionValidator2" runat="server"
                            Text="*" ErrorMessage="Please enter valid Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                            ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                        <asp:CompareValidator ID="BookingpassCompareValidator1" ForeColor="#eaeaea" ControlToCompare="txtshpEmail"
                            Display="Dynamic" ControlToValidate="txtshpConfirmEmail" runat="server" Text="*"
                            ErrorMessage="Shipping Email and confirm email not matched." ValidationGroup="vgs1"></asp:CompareValidator>
                    </div>
                    <div class="colum-label-seven">
                        Phone Number<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtShpPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                            MaxLength="20" onkeypress="return isNumberKey(event)" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpPhone" runat="server" Text="*" ErrorMessage="Please enter Shipping phone number."
                            ControlToValidate="txtShpPhone" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Address Line 1 Or Company Name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtshpAdd" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpAdd" runat="server" Text="*" ErrorMessage="Please enter Shipping address."
                            ControlToValidate="txtshpAdd" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Address Line 2</div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtshpAdd2" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        Town / City</div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpCity" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        County / State</div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpState" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        Postal/Zip Code<span class="readerror">*</span></div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpZip" runat="server" CssClass="input validcheck" MaxLength="10"
                            autocomplete="off" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpZip" runat="server" Text="*" ErrorMessage="Please enter Shipping postal/zip code."
                            ControlToValidate="txtshpZip" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Country<span class="readerror">*</span></div>
                    <div class="colum-label-eight countory">
                        <asp:DropDownList ID="ddlshpCountry" runat="server" class="inputsl" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpCountry" runat="server" Text="*"
                            ErrorMessage="Please select Shipping country." ControlToValidate="ddlshpCountry"
                            InitialValue="0" ForeColor="#eaeaea" />
                    </div>
                    <asp:Label runat="server" ID="lblshpmsg" />
                </div>
            </asp:Panel>
            <%--Shipping Address End--%>
            <div class="round-titles">
                Date for Departure
            </div>
            <div class="booking-detail-in delvery" style="width: 969px;">
                <div class="colum-label-seven">
                    Date of Departure
                    <br />
                    (for fulfillment purposes)</div>
                <div class="colum-label-eight" style="height: auto;">
                    <label runat="server" id="ldlpassDate" style="display: none">
                    </label>
                    <asp:TextBox ID="txtDateOfDepature" runat="server" class="input validcheck calDate calDateDOD"
                        Text="DD/MM/YYYY" autocomplete="off" Style="margin-right: 4px;" />
                    <asp:RequiredFieldValidator ID="BookingpassrfDateOfDepature" runat="server" Text="*"
                        ErrorMessage="Please enter Date of Depature." ControlToValidate="txtDateOfDepature"
                        ForeColor="#eaeaea" InitialValue="DD/MM/YYYY" />
                    <span class="imgCal calIcon" id="imgcal2" style="float: left;" title="Select Date of Departure.">
                    </span>
                    <br />
                    <br />
                    &nbsp;If left empty the ticket start date will be used as delivery date.
                </div>
            </div>
            <asp:Panel ID="pnlShipping" runat="server" Visible="false">
                <div class="round-titles">
                    Shipping Option
                </div>
                <div class="booking-detail-in" style="width: 969px">
                    <asp:Repeater ID="rptShippings" runat="server">
                        <ItemTemplate>
                            <div class="colum-input">
                                <input type="radio" id="radioChkShp<%# Container.ItemIndex + 1 %>" class="shipping" value='<%#Eval("Price")+"^"+Eval("ShippingName")+"^"+ Server.HtmlDecode(Eval("description").ToString())%>'
                                    name="chkshiping" onclick="getdata()" />
                                <span>
                                    <%#Eval("ShippingName") %></span>
                            </div>
                            <div class="colum-label t-right">
                                <strong>
                                    <%=currency%>
                                    <%#Eval("Price")%></strong></div>
                            <div style="background-color: #eaeaea; padding: 7px; width: 98%; float: left; line-height: 30px;
                                color: Black; font-size: 13px;">
                                <%#Eval("description")%></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
            <div class="round-titles">
                Booking Fee
            </div>
            <div class="booking-detail-in" style="padding: 0; width: 989px;">
                <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="40%">
                            &nbsp;
                        </td>
                        <td width="20%">
                            &nbsp;
                        </td>
                        <td width="40%">
                            <div style="float: right">
                                <%=currency%>
                                <asp:Label ID="lblBookingFee" runat="server" Text="0.00" Style="margin-right: 8px;"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Panel ID="pnladminfee" runat="server">
            <div class="round-titles">
                Admin Fee
            </div>
            <div class="booking-detail-in" style="padding: 0; width: 989px;">
                <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="40%">
                            &nbsp;
                        </td>
                        <td width="20%">
                            &nbsp;
                        </td>
                        <td width="40%">
                            <div style="float: right">
                                <%=currency%>
                                <asp:HiddenField ID="hdnAdminFeeAmount" runat="server" ClientIDMode="Static"/>
                                <asp:HiddenField ID="hdnAdminFee" runat="server" ClientIDMode="Static"/>
                                <asp:Label ID="lblAdminFee" runat="server" Text="0.00" Style="margin-right: 8px;"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            </asp:Panel>
            <div class="round-titles">
                Total
            </div>
            <div class="booking-detail-in" style="width: 969px">
                <div class="colum-input" style="width: 500px;">
                    &nbsp;</div>
                <div class="colum-label t-right" style="width: 466px;">
                    <div runat="server" id="OrderDiscount" style="float: left; width: 350px;">
                        <div style="float: left;">
                            Discount Code</div>
                        <asp:TextBox runat="server" ID="txtDiscountCode" class="input" Style="width: 160px;
                            float: left;" MaxLength="50" onchange="getdata()"></asp:TextBox>
                        <div id="Errormsg" style="display:none;color: Red; float: left; margin-left: 5px;">
                            invalid code.</div>
                    </div>
                    <strong style="float: right">
                        <%=currency%>
                        <asp:Label ID="lblTotalCost" runat="server" /></strong> 
                        <asp:HiddenField runat="server" ID="HdnTotalCost" ClientIDMode="Static" />
                </div>
            </div>
            <div>
                <asp:Button ID="btnBack" ClientIDMode="Static" runat="server" Text="Back" CssClass="btn-red-cart f-left"
                    OnClick="btnBack_Click" Width="120px" CausesValidation="False" />
                <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Proceed To Payment"
                    CssClass="btn-red-cart w184 f-right" OnClick="btnCheckout_Click" ValidationGroup="vgs1" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnShippingCost" runat="server" Value="0" />
    <script type="text/javascript">
        function searchEvent() {
            document.getElementById('popupDiv').style.display = 'none';
        } 
    </script>
</asp:Content>
