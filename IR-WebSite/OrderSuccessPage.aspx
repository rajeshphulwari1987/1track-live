﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="OrderSuccessPage.aspx.cs" Inherits="OrderSuccessPage" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <meta name="WT.si_cs" content="1" />
    <meta name="WT.tx_e" content="p" />
    <%=WTpnsku%>
    <%=WTtxu%>
    <%=WTtxs%>
    <%=WTtxI%>
    <%=WTtxid%>
    <%=WTtxit%>
    <style type="text/css">
        /*booking detail*/
        .toogle
        {
            display: block !important;
        }
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
    </style>
    <script type="text/javascript">
        window.history.forward();
        window.history.forward();
    </script>
    <script type="text/javascript">
        window.universal_variable = 
        {           
            version: "1.1.0"
        }; 
        window.universal_variable.transaction =  <%=products %>;
 
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="content">
        <div class="breadcrumb">
            <a href="Home">Home </a>>><a href="TrainResults">Train Results</a>>><a href="BookingCart">Booking
                Cart</a>>>Payment Details
        </div>
        <div class="innner-banner">
            <div class="bannerLft">
                <div class="banner90">
                    <asp:Label ID="lblBannerTxt" runat="server" Text="Rail Passes" /></div>
            </div>
            <div>
                <div class="float-lt" style="width: 73%">
                    <asp:Image ID="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="901px" Height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
                <div class="float-rt" style="width: 27%; text-align: right;">
                    <asp:Image ID="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="100%" Height="190px" ImageUrl="images/innerMap.gif" />
                </div>
            </div>
        </div>
        <div class="left-content">
            <h1>
                Thanks for your order,&nbsp;<asp:Label ID="lblClientName1" runat="server" Text=""></asp:Label>
            </h1>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div id="divPrintGrid" runat="server">
                <div class="round-titles">
                    Order Information
                </div>
                <div class="booking-detail-in">
                    <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" width="50%">
                                Name :
                            </td>
                            <td width="50%">
                                <asp:Label ID="lblClientName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Email Address :
                            </td>
                            <td>
                                <asp:Label ID="lblEmailAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Delivery Address</b>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblDeliveryAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Order Summary:</b>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Order Date:
                            </td>
                            <td>
                                <asp:Label ID="lblOrderDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Order No.:
                            </td>
                            <td>
                                <asp:Label ID="lblOrderNumber" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptOrderInfo" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td align="left">
                                        Journey
                                        <%# Container.ItemIndex + 1 %>:
                                    </td>
                                    <td>
                                        <%#Eval("ProductDesc")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Price :
                                    </td>
                                    <td>
                                        <%=currency %>
                                        <%#Eval("Price").ToString()%>
                                    </td>
                                </tr>
                                <%#(Eval("TktPrtCharge")) != "" ? "<tr><td>Ticket Protection: </td><td>" + currency +" "+ Eval("TktPrtCharge")+"</td></tr>" : ""%>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td align="left">
                                Gross Price:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Shipping:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblShippingAmount" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Booking Fee:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblBookingFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Order Total :</b>
                            </td>
                            <td>
                                <b>
                                    <%=currency %>
                                    <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label></b>
                            </td>
                        </tr>
                        <tr id="trTrenItalia" runat="server" visible="false">
                            <td align="left" colspan="2" style="color: Red">
                                <b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from
                                one of the self-service ticket machines located at all main Trenitalia stations
                                (which are green, white and red). Once you have printed your ticket you must validate
                                it in one of the validation machines located near to the entrance of each platform
                                (they are green and white with Trenitalia written on the front and usually mounted
                                on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will
                                result in a fine.
                            </td>
                        </tr>
                        <tr id="trRegional" runat="server">
                            <td align="left" colspan="2" style="color: Red">
                                <b>Notes:</b> This PNR is a Ticket On Departure, meaning you must collect your ticket
                                from the self service ticket machines at your station of departure. To print at
                                the self service ticket machine you will need your PNR number, along with your surname.
                                Once you have printed your paper ticket in the self servive machine - you will need
                                to validate it at a validation machine. YOU MUST DO THIS PRIOR TO BOARDING YOUR
                                TRAIN. The validation machines are green & white on the front with Trenitalia written
                                across the top and have a red backing. They are located on the train station platforms.
                                Failure to have tickets printed & validated prior to boarding will result in a fine
                                payable to the conductor.
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:Button ID="btnpdf" OnClick="btnPdf_Click" runat="server" Text="Print Ticket"
                CssClass="btn-red-cart w184 f-right" Visible="false" />
        </div>
        <div class="right-content">
            <uc:ucRightContent ID="ucRightContent" runat="server" />
        </div>
    </div>
    <asp:HiddenField ID="hdnUserName" runat="server" />

    <script type="text/javascript">
        / <![CDATA[ /
        var google_conversion_id = 957158359;
        var google_conversion_language = "en";
        var google_conversion_format = "1";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "-l74CJad5FgQ16e0yAM";
        var google_remarketing_only = false;
        / ]]> /
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="//www.googleadservices.com/pagead/conversion/957158359/?label=-l74CJad5FgQ16e0yAM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
</asp:Content>
