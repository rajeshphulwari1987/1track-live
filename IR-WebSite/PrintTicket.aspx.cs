﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PrintTicket : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid _siteId;
    public string script;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ProductType"] != null)
            lblBannerTxt.Text = "Rail Tickets";

        if (!IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Session["TicketBooking-REPLY"] != null && hdnExec.Value != "1")
            {
                List<PrintResponse> list = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                if (list.Count != 0)
                {
                    ltrTicketMsg.Visible = !string.IsNullOrEmpty(list.FirstOrDefault().URL);

                    if (list.Any(x => x.URL.Contains("#")))
                    {
                        List<PrintResponse> newlist = new List<PrintResponse>();
                        foreach (var item in list)
                        {
                            string[] urls = item.URL.Split('#');
                            foreach (var url in urls)
                            {
                                if (url.Length > 32)
                                    newlist.Add(new PrintResponse
                                    {
                                        URL = url
                                    });
                            }
                        }
                        list = newlist;
                    }

                    rptBeNePrint.DataSource = list;
                    rptBeNePrint.DataBind();
                    hdnExec.Value = "1";
                }
            }
        }
    }

    protected void rptBeNePrint_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var divPrintAtHome = e.Item.FindControl("divPrintAtHome") as HtmlControl;
            if (Session["showPrintAtHome"] != null)
            {
                if (Session["showPrintAtHome"].ToString() == "1")
                    if (divPrintAtHome != null)
                        divPrintAtHome.Visible = true;
            }
            Session.Remove("divPrintAtHome");
        }
    }
    
    
}