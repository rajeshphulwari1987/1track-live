﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="TrainDetails.aspx.cs"
    Inherits="TrainDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="SliderGallery/thumb-scroller.css" rel="stylesheet" type="text/css" />
    <script src="SliderGallery/preview.js" type="text/javascript"></script>
    <script src="SliderGallery/jquery.thumb-scroller.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#aSpTrain").addClass("active");
        });
    </script>
    <style type="text/css">
        .float-lt
        {
            float: left;
        }
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
        .left-content .country-block-outer
        {
            position: relative;
        }
        .left-content .country-block-inner
        {
            background: #fff;
            width: 100%;
            float: left;
            padding: 0;
        }
        .clsImg
        {
            width: 143px !important;
            height: 77px !important;
        }
        .thumb-scroller img
        {
            height: 170px !important;
        }
    </style>
    <script type="text/javascript">
        $(window).load(function () {
            $('#slider').nivoSlider();
        });
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <%--Banner section--%>
    <div class="banner" style="height: 253px">
        <div id="dvBanner" class="slider-wrapper theme-default">
            <img src='images/banner.jpg' class="scale-with-grid" alt="" border="0" />
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content">
<div class="left-content">
<asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
<asp:Repeater ID="rptTrain" runat="server" OnItemDataBound="rptTrain_ItemDataBound">
<ItemTemplate>
<asp:HiddenField ID="hdnTrainId" runat="server" Value='<%#Eval("ID") %>'/>
    <div class="cms">
        <div id="ContentHead" runat="server">
            <div class="float-lt" style="width:100%">
                <div class="float-lt" style="width:60%">
                <h1><%#Eval("Name") %></h1>
                <p><%#HttpContext.Current.Server.HtmlDecode((string)Eval("ShortDescription"))%></p>
                </div>
                <div class="float-rt" style="width:40%">
                    <div style="height:330px; overflow:hidden;margin-bottom:5px;">
                    <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImage") %>' width="270px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"> &nbsp;</div>
    <%--Gallery section--%>
    <div style="width: 700px; float:left; position:relative;">
    <div id="banner-slide" style="height:200px;">
    <!-- start Basic Jquery Slider -->
            <div id="ca-container" class="ca-container" style="background-color:White;height:135px;">
                <div class="ca-wrapper">
                    <asp:Repeater ID="rptGallery" runat="server">
                    
            <HeaderTemplate>
                <div id="ts" class="thumb-scroller" style="height:195px !important;">
                    <ul class="ts-list">
            </HeaderTemplate>
                <ItemTemplate>
                        <li>
                            <img src="<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("GalleryImage") %>" alt="" width="99% !important;" height="170px"/>
                        </li>  
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </div>
                </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
    </div>
    </div>
    <%--Gallery section end--%>
    <div class="clear"> &nbsp;</div>
    <div id="Content" runat="server" style="margin-top:10px">
    <p><%#HttpContext.Current.Server.HtmlDecode((string)Eval("LongDescription"))%></p>
</div>

</ItemTemplate>
</asp:Repeater>
</div>

<%--Right section--%>
<div class="right-content">
    <div class="ticketbooking" style="padding-top:0px">
        <div class="list-tab divEnableP2P">
            <ul>
                <li><a href="#" class="active">Rail Tickets </a></li>
                <li><a href="rail-passes">Rail Passes </a></li>
            </ul>
        </div>
        <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
        <img src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
    </div>
<uc:Newsletter ID="Newsletter1" runat="server" />
<div class="right">
    <div id="dvRight" class="slider-wrapper theme-default">
        <div id="sliderRt">
            <asp:Repeater ID="rptRtImg" runat="server">
                <ItemTemplate>
                    <img src='<%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
</div>
<%--Right section end--%>
</section>
</asp:Content>
