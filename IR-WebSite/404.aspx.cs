﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class errorpage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["URL"] != null)
        {
            InvalidURL.Style.Add("display", "none");
            errorTicktURL.Style.Add("display", "block");
        }
        else
        {
            InvalidURL.Style.Add("display", "block");
            errorTicktURL.Style.Add("display", "none");
        }

    }
}