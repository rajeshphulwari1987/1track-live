﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PromotionDetails.aspx.cs" Inherits="PromotionDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <section class="content">
<div class="left-content">
<asp:Panel ID="pnlErrSuccess" runat="server">
    <div id="DivSuccess" runat="server" class="success" style="display: none;">
        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
    <div id="DivError" runat="server" class="error" style="display: none;">
        <asp:Label ID="lblErrorMsg" runat="server" />
    </div>
</asp:Panel>
<div style="float:left;width: 500px">
     <h1><asp:Label ID="lblTitle" runat="server"/></h1>
     <p><asp:Label ID="lblDesp" runat="server"/></p>
</div>
<div class="float-rt">
     <asp:Image ID="imgPromo" runat="server" width="168"/></div>
</div>
<div class="right-content">
<div class="ticketbooking" style="padding-top:0px">
    <div class="list-tab divEnableP2P">
        <ul>
            <li><a href="#" class="active">Rail Tickets </a></li>
            <li><a href="rail-passes">Rail Passes </a></li>
        </ul>
    </div>
    <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
    <img src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
</div>
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
