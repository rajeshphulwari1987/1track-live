﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_ResetPassword : Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid siteId;
    public string script = "<script></script>";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
            divJRF.Visible = _master.IsVisibleJRF(siteId);
            divGeneralInfo.Visible = _master.IsVisibleGeneralInfo(siteId);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSuccessMsg.Text = string.Empty;
        if (!IsPostBack)
        {
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);
            QubitOperationLoad();
        }
    }
    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _master.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList =
                    (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", SiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params["reqId"] != null)
            {
                var id = Guid.Parse(Request.Params["reqId"]);
                var admin = _db.tblAdminUsers.FirstOrDefault(x => x.ID == id);
                if (admin != null)
                {
                    if (!string.IsNullOrEmpty(admin.Forename) && txtnewpass.Text.ToLower().Contains(admin.Forename))
                        lblErrorMsg.Text = "Password should not contain first name.";
                    else if (!string.IsNullOrEmpty(admin.Surname) && txtnewpass.Text.ToLower().Contains(admin.Surname))
                        lblErrorMsg.Text = "Password should not contain last name.";
                    else
                    {
                        var result = _ManageUser.ChangeAgentPassword(id, txtconfpass.Text,txtFirstName.Text,txtLastName.Text);
                        if (result) Response.Redirect("Login");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}