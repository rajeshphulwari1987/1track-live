﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SendPassword.aspx.cs" Inherits="Agent_SendPassword" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <section class="content">
    <asp:HiddenField ID="hdnId" runat="server"></asp:HiddenField>
<div class="breadcrumb"> <a href="#">Home </a> >> Agent Generate Password </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Generate Password</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="../images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="../images/innerMap.gif" />
    </div>
</div>
</div>
 <div class="left-content">
 <div id="login-sec">
    <div   class="info">
        Please generate a new password for login.
    </div>
    <div class="full mr-tp2">
        <div class="full corner">
            <img src="../images/login-top-bar.jpg" alt="" /></div>
              <div class="login-data" style="background:none;min-height:150px">
                  <table width="100%" style="font-size:12px;">
                  <tr>
                      <td style="padding:10px; width:150px;">
                       Generate Password Link:
                      </td>
                  <td>
                   <asp:TextBox ID="txtEmail" class="login" runat="server" placeholder="Enter registered email" Width="250" />
                        <asp:RequiredFieldValidator ID="AgentLogintxtUsername" ControlToValidate="txtEmail" CssClass="validreq" runat="server" ErrorMessage="Please enter registered email. " ValidationGroup="Email" />
                  </td>
                  </tr>
                  <tr>
                      <td>
                      </td>
                      <td>
                        <asp:Button ID="BtnSubmit" OnClick="BtnSubmit_Click" Text="Generate Password" runat="server" ValidationGroup="Email" />
                      </td>
                  </tr>
                  <tr>
                      <td colspan="2" style="padding-left: 30%;padding-top: 10px;">
                        <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq" ForeColor="#FF3300"/>
                      </td>
                  </tr>
                  </table>
              </div>
    <div class="full corner">
        <img src="../images/login-bot-bar.jpg" alt="" /></div>
    </div>
</div>
</div>

<div class="right-content">
<div id="divJRF" runat="server"> 
<a href='<%=siteURL%>TrainResults.aspx' id="trainresults">
  <div id="rtPannel1" runat="server"></div>
</a>
<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
</div>
<div id="divGeneralInfo" runat="server" Visible="False">
<a href="<%=siteURL%>GeneralInformation.aspx">
<uc:GeneralInformation ID="GInfo" runat="server" /></a>
<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
</div>
</div>
</section>
</asp:Content>
