﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Agentpayment.aspx.cs" Inherits="Agentpayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="../editor/jquery.js" type="text/javascript"></script>
    <link href="../editor/redactor.css" rel="stylesheet" type="text/css" />
    <script src="../editor/redactor.js" type="text/javascript"></script>
    <style type="text/css">
        .toogle{ display: block!important;}
        .grid-sec2{
            width: 980px;
            float: left;
            margin-bottom: 10px;
            border: 1px solid #d5d5d5;
            background: #EEEEEE;
            border-radius: 3px;
            padding: 5px;
        }
        .btncontshop{
            background: url('../images/gray-btn-bg.jpg') repeat-x 0 -8px !important;
        }
        .redactor_box{
            width: 550px;
        }
        .input{
            margin-left: 0 !important;
        }
    </style>
    <%=script%>
    <script src="../Scripts/jquery-1.7.2.js" type="text/javascript"></script>
    <script type="text/javascript">
        function chkreq(e) {
            if ($("#txtfolderno").val() != '')
                return true;
            else {
                $("#req").show();
                return false;
            }
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnAgentId" runat="server" />
    <div class="content">
        <h1>
            Payment Method
        </h1>
        <p>
            Enter your payment details below and click 'pay & complete order' to finalise your
            booking.
        </p>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div style="margin-top: 80px; width: 992px;">
            <div style="float: left;">
                <h1>
                    The total value of your order is
                    <%=currency%>
                    <asp:Label ID="lbltotalamount" runat="server" Text=""></asp:Label>
                </h1>
            </div>
        </div>
        <fieldset class="grid-sec2">
            <legend class="clsAgLegend"><b>pay an account</b></legend>
            <div style="padding: 8px;">
                <table>
                    <tr>
                        <td width="25%" style="vertical-align: top;">
                            Your reference/Folder number
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfolderno" ClientIDMode="Static" CssClass="input"
                                Width="430px" Height="20"></asp:TextBox>
                            <div style="clear: both;">
                            </div>
                            &nbsp;
                            <div id="req" style="color: Red; display: none">
                                Enter Reference No.</div>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%" style="vertical-align: top;">
                            Note
                        </td>
                        <td>
                            <textarea runat="server" id="txtNote" title="Enter Note."></textarea>
                            <div style="clear: both;">
                                <br />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="btnCancle" runat="server" Text="Cancel" OnClick="btnCancle_Click"
                                Width="80px" />
                            <asp:Button ID="btnpaycompliteorder" OnClientClick=" return chkreq(this);" runat="server"
                                Text="Pay & Complete Order" Width="180px" OnClick="btnpaycompliteorder_Click" />
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </fieldset>
    </div>
</asp:Content>
