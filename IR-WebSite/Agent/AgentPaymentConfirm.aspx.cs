﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using Business;
using iTextSharp.text;
using System.Web.UI.WebControls;

public partial class Agent_AgentPaymentConfirm : Page
{
    Guid siteId;
    public string currency = "$";
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private readonly ManageBooking oBooking = new ManageBooking();
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();

    readonly private ManageOrder _master = new ManageOrder();
    public static Guid currencyID = new Guid();
    public string ReservationCode = null;
    public string script = "<script></script>";
    public string products = "";
    public string printUrl = "";
    public string pdfName = "";
    public string FileURL = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        long order = Convert.ToInt32(Request["oid"]);
        new ManageBooking().UpdateOrderStatus(3, Convert.ToInt64(order));
        long orderID = 0;
        if (Session["OrderID"] != null)
            orderID = Convert.ToInt32(Session["OrderID"]);

        if (Session["P2POrderID"] != null)
            btnPrintTicket.Visible = oBooking.CheckDilveryOptionType(Convert.ToInt64(Session["P2POrderID"]));
        if (Session["P2POrderID"] != null && Session["IsRegional"] != null)
            btnPrintTicket.Visible = false;
         
        if (Session["SegmentType"] != null)
        {
            trTrenItalia.Visible = !(Session["SegmentType"].ToString().Trim() == "M");
            btnPrintTicket.Visible = (Session["SegmentType"].ToString().Trim() == "M");
        }


        GetCurrencyCode();
        QubitOperationLoad();
        if (!IsPostBack)
        {
            Session["IsCheckout"] = null;
            if (Session["AgentUserID"] == null)
            {
                Response.Redirect("~/agent/");
            }
            ShowPaymentDetails();
            PageLoadEvent();
            Session.Remove("SegmentType");
        }

        #region Stop to sent queue for role Agent Branch (STA)
        if (Session["P2POrderID"] == null)
        {
            if (Session["AgentUsername"] != null)
            {
                db_1TrackEntities db = new db_1TrackEntities();
                string agentUser = Session["AgentUsername"].ToString();
                var rec = db.tblAdminUsers.FirstOrDefault(x => x.UserName == agentUser);
                if (rec.RoleID != null)
                {
                    /*Hide print stock for nonEurail*/
                    bool isEurail = new ManageBooking().GetAllCartData(Convert.ToInt64(Request["oid"])).ToList().Any(ty => ty.Eurail == "yes");
                    if (!isEurail)
                        DivStock.Visible = false;
                    else
                        DivStock.Visible = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == rec.RoleID).IsPrintingAllow;

                    btnSend_Click(sender, e);
                }
                else
                    DivStock.Visible = false;
            }
        }
        #endregion

        if (order == orderID)
        {
            Session.Remove("RailPassData");
            Session.Remove("OrderID");
            Session.Remove("P2POrderID"); 
        }
       
        Session.Remove("IsRegional");
        Session.Remove("SegmentType");
    }

    public void QubitOperationLoad()
    {
        var orderID = Convert.ToInt32(Request["oid"]);
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;

        var lst = new ManageBooking().GetAllCartData(orderID).ToList();
        if (lst.Count() > 0)
        {
            var lstNew = from a in lst
                         select new { category = (new ManageBooking().getPassDescForQbit(a.PassSaleID, a.ProductType, "Category")), currency = currencyCode, name = (new ManageBooking().getPassDescForQbit(a.PassSaleID, a.ProductType, "")), sku_code = "SKU", unit_price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)), unit_sale_price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)) };

            var listProduct = new List<ProuductLineItem>();
            if (lstNew.Count() > 0)
            {
                foreach (var litem in lstNew)
                {
                    var obj = new ProuductLineItem
                    {
                        product = new ProductItem
                        {
                            category = litem.category,
                            currency = litem.currency,
                            name = litem.name,
                            sku_code = litem.sku_code,
                            unit_price = litem.unit_price.ToString(),
                            unit_sale_price = litem.unit_sale_price.ToString(),
                        },
                        quantity = 1,
                        subtotal = lstNew.Sum(a => a.unit_sale_price)
                    };
                    listProduct.Add(obj);
                }
            }

            decimal shipingAMt = lst.FirstOrDefault().ShippingAmount.HasValue ? Convert.ToDecimal(lst.FirstOrDefault().ShippingAmount) : 0;
            var cart = new BookingCartInfo
            {
                currency = currencyCode,
                Date_of_order = lst.FirstOrDefault().CreatedOn.ToString("MM/dd/yyyy"),
                line_items = listProduct.ToList(),
                order_id = Request["oid"] != null ? Convert.ToString(Request["oid"]) : "",
                shipping_cost = shipingAMt,
                shipping_method = Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "",
                subtotal = listProduct.Sum(x => x.subtotal),
                tax = 0,
                Time_of_order = lst.FirstOrDefault().CreatedOn.ToString("HH:mm:ss"),
                total = shipingAMt + listProduct.Sum(x => x.subtotal),
                voucher = "",
                voucher_discount = 0
            };

            var json_serializer = new JavaScriptSerializer();
            products = json_serializer.Serialize(cart);
        }
    }

    private void PageLoadEvent()
    {
        var stockList = _oPrint.GetStockList(AgentuserInfo.UserID).ToList();
        ddlPrntQ.DataSource = stockList;
        ddlPrntQ.DataTextField = "Name";
        ddlPrntQ.DataValueField = "ID";
        ddlPrntQ.DataBind();
        ddlPrntQ.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Stock--", "-1"));
    }

    public void ShowPaymentDetails()
    {
        try
        {
            if (Request["oid"] != null)
            {
                var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Request["oid"])).ToList();
                /*Hide print stock for p2p*/
                if (Session["P2POrderID"] != null)
                    DivStock.Visible = false;
                if (lst != null && lst.Count() == 0)
                {
                    DivStock.Visible = false;
                    lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Request["oid"])).OrderBy(a => a.OrderIdentity).ToList();

                    long order = Convert.ToInt32(Request["oid"]);
                    long orderID = 0;
                    if (Session["OrderID"] != null)
                        orderID = Convert.ToInt32(Session["OrderID"]);
                    if (order == orderID)
                    {
                        Session.Remove("RailPassData");
                        Session.Remove("P2POrderID");
                        Session.Remove("OrderID");
                    }
                } 

                long ordID = Convert.ToInt32(Request["oid"]);
                var lstNew = from a in lst
                             select new { Title = a.Title, Name = a.FirstName + " " + a.LastName, Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)), ProductDesc = (new ManageBooking().getPassDescType(a.PassSaleID, a.ProductType, (string)(Session["TrainType"]))), TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0), CommissionFee = a.CommissionFee, NetPrice = _master.GetEurailAndNonEurailPrice(a.PassSaleID.Value) };
                if (lst.Count > 0)
                {
                    rptOrderInfo.DataSource = lstNew;
                    lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                    lblDeliveryAddress.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName + ", " + lst.FirstOrDefault().Address1 + ", " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + ", " : string.Empty) + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry + ", " + lst.FirstOrDefault().Postcode;
                    lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();
                    lblGrossPrice.Text = (lstNew.Sum(a => a.Price)).ToString();

                    lblShippingAmount.Text = lst.FirstOrDefault().ShippingAmount.ToString();
                    lblBookingFee.Text = lst.FirstOrDefault().BookingFee.ToString();
                    lblGrandTotal.Text = ((lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount + lst.FirstOrDefault().BookingFee).ToString();
                    lblStaRefNo.Text = Request.QueryString["stRef"] ?? Request.QueryString["stRef"];
                     
                }
                else
                    rptOrderInfo.DataSource = null;
                rptOrderInfo.DataBind();
            }
            else
                Response.Redirect("~/Home", false);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
                siteId = Guid.Parse(Session["siteId"].ToString());
            AddUpdatePrintQueue(siteId);
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    void AddUpdatePrintQueue(Guid siteId)
    {
        try
        {

            StockQueueField RoleAgentBranchQueue = _oPrint.GetStockList(AgentuserInfo.UserID).FirstOrDefault();
            if (RoleAgentBranchQueue == null)
                return;

            var ordId = Convert.ToInt32(Request["oid"]);
            var list = _db.tblPassSales.Where(x => x.OrderID == ordId).ToList();
            foreach (var item in list)
            {
                _oPrint.AddUpdatePrintQ(ordId, item.ID, siteId);

                _oPrint.AddPrintQItems(new tblPrintQueueItem
                {
                    ID = Guid.NewGuid(),
                    SiteID = siteId,
                    OrderID = ordId,
                    CategoryID = item.CategoryID,
                    ProductID = item.ProductID,
                    PassSaleID = item.ID,
                    Status = Guid.Parse("6100b83e-07ed-418b-97e6-e5c6b746da3b"),
                    AdminUserID = AgentuserInfo.UserID,
                    DateTimeStamp = DateTime.Now,
                    QueueID = DivStock.Visible ? Guid.Parse(ddlPrntQ.SelectedValue) : RoleAgentBranchQueue.ID
                });
            }
            if (DivStock.Visible)
                Response.Redirect("../Printing/PrintingOrders");
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void btnPrintTicket_Click(object sender, EventArgs e)
    {
        Response.Redirect("../PrintTicket.aspx");
    }
    protected void rptOrderInfo_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            var trNettPrice = e.Item.FindControl("trNettPrice");
            var trNettPricePass = e.Item.FindControl("trNettPricePass");
            var trCommission = e.Item.FindControl("trCommission");
            var lblPrice = e.Item.FindControl("lblPrice") as Label;
            var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (lblPrice != null)
            {
                if (Session["P2POrderID"] != null)
                    lblPrice.Text = "Nett Price";
                else
                    lblPrice.Text = "Price";
            }
            if (trNettPrice != null)
            {
                if (objsite != null && objsite.IsAgent == true)
                    trNettPrice.Visible = true;
                else
                    trNettPrice.Visible = false;
            }
            if (trNettPricePass != null)
            {
                if (objsite != null && objsite.IsAgent == true)
                {
                    if (Session["P2POrderID"] == null)
                        trNettPricePass.Visible = true;
                    else
                        trNettPricePass.Visible = false;
                }
                else
                    trNettPricePass.Visible = false;
            }
            if (trCommission != null)
            {
                if (Session["P2POrderID"] != null)
                {
                    if (objsite != null && objsite.IsAgent == true)
                        trCommission.Visible = true;
                    else
                        trCommission.Visible = false;
                }
                else
                    trCommission.Visible = false;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
}

public class BookingCartInfo
{
    // A unique identifier for the order
    public string order_id { get; set; }
    // The standard letter code in capitals for the currency type in which the order is being paid, eg: EUR, USD, GBP
    public string currency { get; set; }
    // A valid number with the total cost of the basket including any known tax per item, but not including shipping or discounts
    public Decimal subtotal { get; set; }
    // A boolean true or false to indicate whether subtotal includes tax
    public bool subtotal_include_tax { get; set; }
    // A valid number with the total amount of potential tax included in the order
    public Decimal tax { get; set; }
    // A valid number with the total amount of potential shipping costs included in the order
    public Decimal shipping_cost { get; set; }
    // Optional. Describes the shipping method
    public string shipping_method { get; set; }
    // A valid number with the total cost of the basket including any known tax, shipping and discounts
    public Decimal total { get; set; }
    //Product list
    public List<ProuductLineItem> line_items { get; set; }
    // If voucher used
    public string voucher { get; set; }
    // A valid number with the total amount of discount due to the voucher entered
    public Decimal voucher_discount { get; set; }
    // Date of Order (MM/DD/YYYY)
    public string Date_of_order { get; set; }
    // Time of Order (HH:MM:SS)
    public string Time_of_order { get; set; }
}

public class ProuductLineItem
{
    public ProductItem product { get; set; }
    //quantity:1
    public int quantity { get; set; }
    //subtotal:5000.00
    public Decimal subtotal { get; set; }
}

public class ProductItem
{
    //sku_code: "SKU",
    public string sku_code { get; set; }

    //name: "PRODUCT-NAME",
    public string name { get; set; }

    //category: "PRODUCT-CATEGORY",
    public string category { get; set; }
    // currency: "GBP",
    public string currency { get; set; }

    //unit_price:151.53
    public string unit_price { get; set; }

    //unit_sale_price:151.53
    public string unit_sale_price { get; set; }
}