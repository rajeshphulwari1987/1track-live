﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Xml;
using Business;
using OneHubServiceRef;
using System.Text.RegularExpressions;
using System.Configuration;
public partial class Agentpayment : Page
{
    Guid siteId;
    public string currency = "$";
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static Guid currencyID = new Guid();
    private readonly ManageBooking oBooking = new ManageBooking();
    readonly private ManageOrder _master = new ManageOrder();
    readonly Masters _masterPage = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string script = "<script></script>";
    public string ReservationCode = null;
    public string printUrl = "";
    public string pdfName = "";
    public string FileURL = "";
    private string htmfile = string.Empty;
    ManageUser mngUser = new ManageUser();
    public bool isEvolviBooking = false;
    public bool isNTVBooking = false;
    public bool isBene = false;
    public bool isTI = false;

    #region PageEvents
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ShowMessage(0, null);
        if (!IsPostBack)
        {
            QubitOperationLoad();
            if (Session["AgentUserID"] == null)
            {
                Response.Redirect("~/agent/");
            }
            else
                hdnAgentId.Value = Session["AgentUserID"].ToString();

            if (Request["amt"] != null && Request["oid"] != null)
            {
                lbltotalamount.Text = Request["amt"];
            }
            GetCurrencyCode();
        }

        var apiTypeList = new ManageBooking().GetAllApiType(Convert.ToInt32(Request["oid"].ToString()));
        if (apiTypeList != null)
        {
            isEvolviBooking = apiTypeList.isEvolvi;
            isNTVBooking = apiTypeList.isNTV;
            isBene = apiTypeList.isBene;
            isTI = apiTypeList.isTI;
        }
    }
    #endregion

    #region Agent Payment Code
    public void QubitOperationLoad()
    {
        try
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnCancle_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Home");
    }

    protected void btnpaycompliteorder_Click(object sender, EventArgs e)
    {
        try
        {
            var objOrder = new ManageOrder();
            if (Request["oid"] != null)
            {
                bool res = objOrder.IsExistAgentRefrenceNo(siteId, txtfolderno.Text.Trim());
                if (res)
                    ShowMessage(2, "STA reference number is alerady used for another devision/site");
                else
                {
                    Int32 oid = Convert.ToInt32(Request["oid"].ToString());
                    if (Session["P2POrderID"] != null || _db.tblOrders.Any(x => x.OrderID == oid && x.TrvType.Trim() == "P2P"))
                    {
                        Session["P2POrderID"] = Request["oid"].ToString();
                        PrintTicketInfo();
                        var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                        if (getDeliveryType != null && (getDeliveryType == "Thayls Ticketless" || getDeliveryType == "TrenItaila Printing"))
                            ConfirmP2PBooking();
                    }
                    else
                    {
                        Session.Remove("CollectStation");
                        SendMail();
                    }

                    objOrder.UpdateAgentRefrenceByOrderNo(Convert.ToInt32(Request["oid"].ToString()), txtfolderno.Text.Trim(), txtNote.InnerText);
                    Response.Redirect("~/agent/AgentPaymentConfirm.aspx?stRef=" + txtfolderno.Text.Trim() + "&oid=" + Request["oid"]);
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                //btnNext.Visible = false;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
    #endregion

    #region Print & confirm P2P ticket

    private void PrintTicketInfo()
    {
        try
        {
            if (Session["BOOKING-REPLY"] == null)
                return;

            var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
            if (list.Any(x => x.PinCode != null))
            {
                if (Session["P2POrderID"] != null)
                {
                    var chkDeliveryType = oBooking.CheckDilveryOptionType(Convert.ToInt64(Session["P2POrderID"]));
                    if (chkDeliveryType)
                        Printickets();
                    else
                    {
                        TicketBooking();
                        SendMail();
                    }
                }
            }
            else
                PrintTicketItalia();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
    public void TicketBooking()
    {
        try
        {
            var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;
            var item = listbookingReply.FirstOrDefault();
            var objHeader = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
            };

            var client = new OneHubRailOneHubClient();
            var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
            ApiLogin(objRequest);
            var tktBooking = client.TicketBookingRequest(objRequest);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? tktBooking.PinCode : item.PinCode);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public void Printickets()
    {
        try
        {
            if (Session["BOOKING-REPLY"] != null)
            {
                var bookingList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                if (bookingList == null)
                    return;
                var client = new OneHubRailOneHubClient();
                var listResponse = new List<PrintResponse>();
                foreach (var item in bookingList)
                {
                    var objHeader = new Header
                    {
                        onehubusername = "#@dots!squares",
                        onehubpassword = "#@dots!squares",
                        unitofwork = 0,
                    };

                    #region TicketBookingRequest

                    var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
                    ApiLogin(objRequest);
                    var tktBooking = client.TicketBookingRequest(objRequest);
                    oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? tktBooking.PinCode : item.PinCode);
                    #endregion

                    #region Ticket Print

                    if (tktBooking != null)
                    {
                        var printRequest = new TicketPrintRequest
                            {
                                Header = objHeader,
                                BillingAddress = item.BillingAddress != null ? new BillingAddress
                                    {
                                        Address = item.BillingAddress.Address,
                                        City = item.BillingAddress.City,
                                        Country = item.BillingAddress.Country,
                                        CrisNumber = item.BillingAddress.CrisNumber,
                                        DeliveryDate = item.BillingAddress.DeliveryDate,
                                        Email = item.BillingAddress.Email,
                                        FirstName = item.BillingAddress.FirstName,
                                        Lastname = item.BillingAddress.Lastname,
                                        Phone = item.BillingAddress.Phone,
                                        State = item.BillingAddress.State,
                                        ZipCode = item.BillingAddress.ZipCode
                                    } : null,
                                ReservationCode = item.ReservationCode,
                                ChangeReservationCode = item.ChangeReservationCode,
                                PinCode = tktBooking.PinCode,
                                TicketType = "PDF_DT",
                                ApiInformation = new ApiInformation
                                {
                                    ApiName = GetApiName()
                                }
                            };

                        ApiLoginPrint(printRequest);
                        var printResponse = client.TicketPrint(printRequest);
                        if (printResponse != null)
                        {
                            listResponse.Add(new PrintResponse
                                {
                                    ReservationCode = printRequest.ReservationCode,
                                    status = String.IsNullOrEmpty(printResponse.PrintIndication) ? "False" : printResponse.PrintIndication,
                                    URL = String.IsNullOrEmpty(printResponse.TicketUrl) ? "Ticket print URL not found." : printResponse.TicketUrl
                                });
                        }
                    }
                    #endregion
                }

                var newList = new List<PrintResponse>();
                foreach (var resp in listResponse)
                {
                    if (resp != null)
                    {
                        printUrl = resp.URL;
                        if (resp.URL.Contains("pdf"))
                            printUrl = BeNePdfService(printUrl, resp.ReservationCode);

                        if (!string.IsNullOrEmpty(printUrl))
                            oBooking.UpdateTicketUrlByReservationCode(resp.ReservationCode, printUrl);

                        newList.Add(new PrintResponse { ReservationCode = resp.ReservationCode, status = resp.status, URL = printUrl });
                    }
                }
                Session["TicketBooking-REPLY"] = newList;

                if (listResponse.Count != 0)
                    SendMail();
                else
                    ShowMessage(2, "Ticket Print not Available");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public string BeNePdfService(string url, string resCode)
    {
        try
        {
            string outputFile = Server.MapPath("~/PdfService/" + resCode + ".pdf");
            var webClient = new WebClient();
            using (var webStream = webClient.OpenRead(url))
            using (var fileStream = new FileStream(outputFile, FileMode.Create))
            {
                var buffer = new byte[32768];
                int bytesRead;

                while (webStream != null && (bytesRead = webStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, bytesRead);
                }
            }
            string siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            return siteURL + "PdfService/" + resCode + ".pdf";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnPdf_Click(object sender, EventArgs e)
    {
        Response.Redirect("PrintTicket.aspx");
    }

    public void PrintTicketItalia()
    {
        try
        {
            var newList = new List<PrintResponse>();
            if (Session["BOOKING-REPLY"] != null)
            {
                var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
                foreach (var item in list)
                    if (item.PdfUrl != null)
                    {
                        string newUrl = "";
                        foreach (var url in item.PdfUrl)
                        {
                            newUrl += url + "#";
                        }
                        oBooking.UpdateTicketUrlByReservationCode(item.ReservationCode, newUrl);
                        newList.Add(new PrintResponse { ReservationCode = item.ReservationCode, status = "PRINT REQUEST SUBMITTED", URL = newUrl });
                    }
            }
            Session["TicketBooking-REPLY"] = newList;
            SendMail();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }


    public bool ConfirmP2PBooking()
    {
        bool result = false;
        if (Session["BOOKING-REPLY"] == null)
            return result;

        var client = new OneHubRailOneHubClient();
        var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;

        foreach (var item in listbookingReply)
        {
            var objResponse = new ConfirmResponse();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = item.UnitOfWork,
                language = Language.nl_BE,
            };

            var objRequest = new ConfirmRequest { Header = objHeader, ReservationCode = item.ReservationCode, ApiInformation = new ApiInformation { ApiName = GetApiName() } };
            ApiLoginConfirm(objRequest);
            objResponse = client.Confirm(objRequest);

            if (objResponse != null)
            {
                string res = objResponse.ConfirmOk;
                result = res == "Ok" || res == "True";
            }
            oBooking.UpdateReservationCode(Convert.ToInt32(Session["P2POrderID"]), item.ReservationCode, result);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, objResponse.PinCode != null ? objResponse.PinCode : item.UnitOfWork.ToString());
        }
        return result;
    }
    #endregion

    public bool SendMail()
    {
        bool retVal = false;
        try
        {
            bool isDiscountSite = _master.GetOrderDiscountVisibleBySiteId(siteId);
            if (Session["siteId"] != null)
                siteId = Guid.Parse(Session["siteId"].ToString());
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var st = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (st != null)
            {
                string SiteName = st.SiteURL + "Home";
                currency = oManageClass.GetCurrency(st.DefaultCurrencyID.HasValue ? (st.DefaultCurrencyID.Value) : new Guid());
                var orderID = Convert.ToInt32(Request["oid"]);
                string Subject = "Order Confirmation #" + orderID;
                string dir = HttpContext.Current.Request.PhysicalApplicationPath;
                if (st.IsSTA ?? false)
                    htmfile = Server.MapPath("~/MailTemplate/BlueMailTemplate.htm");
                else
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);

                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                string UserName,
                       EmailAddress,
                       DeliveryAddress,
                       OrderDate,
                       OrderNumber,
                       Total,
                       ShippingAmount,
                       GrandTotal,
                       BookingFee,
                       NetTotal,
                       Discount;
                UserName = Discount = NetTotal = EmailAddress = DeliveryAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = BookingFee = "";

                var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(Request.Params["oid"])).OrderBy(a => a.OrderIdentity).ToList();
                var lst1 = from a in lst
                           select
                               new
                               {
                                   Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                   ProductDesc =
                           (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                            " " + a.LastName),
                                   TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                               };
                if (lst.Count > 0)
                {
                    EmailAddress = lst.FirstOrDefault().EmailAddress;
                    DeliveryAddress = lst.FirstOrDefault().Address1 + ", " +
                                      (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2)
                                           ? lst.FirstOrDefault().Address2 + ", "
                                           : string.Empty) + "<br>" +
                                      (!string.IsNullOrEmpty(lst.FirstOrDefault().City)
                                           ? lst.FirstOrDefault().City + ", " + lst.FirstOrDefault().Postcode +
                                             ", <br>"
                                           : string.Empty) +
                                      (!string.IsNullOrEmpty(lst.FirstOrDefault().State)
                                           ? lst.FirstOrDefault().State + ",<br> "
                                           : string.Empty) + lst.FirstOrDefault().DCountry;
                    OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                    Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                    ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                    BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                    Discount = _master.GetOrderDiscountByOrderId(Convert.ToInt64(orderID));
                    NetTotal = ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount + lst.FirstOrDefault().BookingFee).ToString();
                    GrandTotal = (Convert.ToDecimal(NetTotal) - Convert.ToDecimal(Discount)).ToString("F2");
                    UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " +
                               lst.FirstOrDefault().DLastName;
                }
                // to address
                string ToEmail = EmailAddress;
                body = body.Replace("##UserName##", UserName.Trim());
                body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                body = body.Replace("##OrderDate##", OrderDate.Trim());
                body = body.Replace("##OrderNumber##", OrderNumber.Trim());
                body = body.Replace("##Items##", currency + " " + Total.Trim());

                if (Session["IsRegional"] != null)
                    Session["SegmentType"] = "N";

                body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                body = body.Replace("##BookingFee##", currency + " " + BookingFee.Trim());
                body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());

                if (isDiscountSite)
                {
                    string DiscouontData = ("<tr><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>Net Total:</b> </font></td><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>" + currency + " " + NetTotal.Trim() + "</b></font></td></tr><tr><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>Discount:</b></font></td><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'>" + currency + " " + Discount.Trim() + "</font></td></tr>");
                    body = body.Replace("##Discount##", DiscouontData);
                }
                else
                    body = body.Replace("##Discount##", "");



                string britrailpromoTerms = isBritRailPromoPass(orderID) ? "<br/><tr><td style='font-size: 12px' valign='top'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>* Terms and condition for:</b></font> </td><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'>BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a></font></td><tr>" : "";
                if (Session["ShipMethod"] != null)
                {
                    string shippingdesc = "<tr><td style='font-size: 12px' valign='top'>" +
                          "<font face='Arial, Helvetica, sans-serif' color='#000000'><b>Shipping:</b></font>" +
                          "</td><td style='font-size: 12px'>" +
                            "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + Session["ShipMethod"] + "</font><br/>" +
                            "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + ((string)Session["ShipDesc"] != "" ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "") + "</font>" +
                        "</td>" + "</tr>";
                    body = body.Replace("##ShippingMethod##", shippingdesc + britrailpromoTerms);
                }
                else
                    body = body.Replace("##ShippingMethod##", britrailpromoTerms);


                body = body.Replace("#Blanck#", "&nbsp;");

                var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                var lstC = new ManageBooking().GetAllCartData(orderID).OrderBy(a => a.OrderIdentity).ToList();
                var lstNew = from a in lstC
                             select new
                             {
                                 a,
                                 Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                 ProductDesc =
                             (new ManageBooking().getPassDescMail(a.PassSaleID, a.ProductType, PrintResponselist, (string)(Session["TrainType"]))),
                                 TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                 CommissionFee = a.CommissionFee,
                                 Terms = a.terms
                             };
                string strProductDesc = "";
                int i = 1;
                var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (lstNew.Count() > 0)
                {
                    lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                    foreach (var x in lstNew)
                    {
                        if (x.a.ProductType != "P2P")
                        {
                            strProductDesc += "<tr>" +
                                              "<td style='font-size: 12px; vertical-align:top;'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'> <b>Journey " + i.ToString() + ":</b></font>" + "</td>" +
                                              "<td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.ProductDesc + "</font></td></tr>" +

                                              (Session["CollectStation"] != null ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Collect at ticket desk</b></font></td><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' color='#000' valign='top'>" +
                                                     Session["CollectStation"].ToString() + "</td></tr>" : "") +

                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Name</b><td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.a.Title + " " + x.a.FirstName +
                                              " " + x.a.LastName + "</font></td></tr>" +
                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Price</b></font></td><td style='font-size: 12px;color:#000;' valign='top' color='#000'>" +
                                              currency + " " + x.Price.ToString() + "</td></tr>" +

                                              ((objsite != null && objsite.IsAgent == true) ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Nett Price</b></font></td><td style='font-size: 12px;color:#000;' valign='top' color='#000'>" +
                                              currency + " " + _master.GetEurailAndNonEurailPrice(x.a.PassSaleID.Value) + "</td></tr>" : "") +

                                              (x.TktPrtCharge > 0 ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Ticket Protection</b></font></td><td style='font-size: 12px;color:#000;' color='#000' valign='top'>" + currency + " " + Convert.ToString(x.TktPrtCharge) : "") + "</td></tr>";
                        }
                        else
                        {
                            strProductDesc += "<tr>" +
                                              "<td style='font-size: 12px; vertical-align:top;' colspan='2'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'> <b>Journey " +
                                              i.ToString() + ":</b></font>" + "</td></tr>" +
                                              "<tr><td style='font-size: 12px;color:#000;' valign='top'><td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.ProductDesc + "</font></td></tr>" +

                                              (Session["CollectStation"] != null ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Collect at ticket desk</b></font></td><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' color='#000' valign='top'>" +
                                                     Session["CollectStation"].ToString() + "</td></tr>" : "") +

                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Name</b><td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.a.Title + " " + x.a.FirstName +
                                              " " + x.a.LastName + "</font></td></tr>" +
                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Nett Price</b></font></td><td style='font-size: 12px;color:#000;' valign='top' color='#000'>" +
                                              currency + " " + x.Price.ToString() + "</td></tr>" + GetSegmentNote() +

                                              (x.TktPrtCharge > 0 ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Ticket Protection</b></font></td><td style='font-size: 12px;color:#000;' color='#000' valign='top'>" + currency + " " + Convert.ToString(x.TktPrtCharge) : "") +

                                              (objsite != null && objsite.IsAgent == true ? (x.CommissionFee > 0 ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Commission Fee:</b></font></td><td style='font-size: 12px;color:#000;' color='#000' valign='top'>" + currency + " " + Convert.ToString(x.CommissionFee) : "") : "")
                                              + "</td></tr>" +

                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' colspan='2' valign='top'><b>Ticket Terms:</b></font>" + Server.HtmlDecode(x.Terms.Replace("<div class='toogle' style='display:none'>", "<div class='toogle' style='display:block'>")) + "</td></tr>";
                        }
                        i++;
                    }
                }
                body = body.Replace("##OrderDetails##", strProductDesc);

                if (Session["P2POrderID"] != null)
                {
                    var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<div style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;width:440px;'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</div>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                }
                else
                    body = body.Replace("##PrintAtHome##", "");



                body = body.Replace("../images/", SiteName + "images/");
                string CCEmailID = GetCCEmailID();

                /*Send Email to Bcc email address*/
                string BccEmail = _oWebsitePage.GetBccEmail(siteId);

                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(siteId);
                if (result != null)
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToEmail);

                    if (!string.IsNullOrEmpty(CCEmailID))
                        message.CC.Add(CCEmailID);

                    if (!string.IsNullOrEmpty(BccEmail))
                    {
                        message.Bcc.Add(BccEmail);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                    }
                    else
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);

                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;

                    if (ReservationCode != null)
                    {
                        var fileNm = Server.MapPath("~/" + FileURL);
                        if (fileNm != null)
                            message.Attachments.Add(new Attachment(fileNm));
                    }

                    smtpClient.Send(message);
                    retVal = true;
                }
            }
        }
        catch
        {
            retVal = true;
        }
        return retVal;
    }

    private string GetSegmentNote()
    {
        List<PrintResponse> listTicket = Session["TicketBooking-REPLY"] as List<PrintResponse>;
        if (Session["SegmentType"] != null)
            if ((string)Session["SegmentType"] == "N" || listTicket.Any(x => string.IsNullOrEmpty(x.URL)) || listTicket.Count == 0)
            {
                return "<tr><td style='font-size: 12px; border: 1px solid red;padding: 5px' valign='top' colspan='2'>" +
                                "<font face='Arial, Helvetica, sans-serif' color='red'>" + "<b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from one of the self-service " +
                                    "ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation " +
                                    "machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine." +
                                    "</font></td></tr>";

            }
        return "";
    }
    bool isBritRailPromoPass(long orderid)
    {
        try
        {
            foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
            {
                bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                if (isPromo)
                    return true;
            }
            return false;
        }
        catch
        {
            return false;
        }

    }
    public string GetCCEmailID()
    {
        try
        {
            string ccEmail = string.Empty;
            if (Session["AgentUserID"] != null)
            {
                var agentId = Guid.Parse(Session["AgentUserID"].ToString());
                var result = _masterPage.GetAgentOfficeEmailID(agentId).FirstOrDefault();
                if (result != null)
                {
                    if (result.chkEmail && result.Email != string.Empty)
                        ccEmail = result.Email + ",";
                    if (result.chkSecondaryEmail && result.SecondaryEmail != string.Empty)
                        ccEmail = ccEmail + result.SecondaryEmail;
                }
            }
            return ccEmail.TrimEnd(',');
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string GetApiName()
    {
        if (isTI)
            return "TrainItalia";
        else if (isBene)
            return "BeNe";
        else if (isNTVBooking)
            return "Ntv";
        else if (isEvolviBooking)
            return "Evolvi";
        else
            return "";
    }

    #region Api Login
    private void ApiLogin(TicketBookingRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    private void ApiLoginPrint(TicketPrintRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    private void ApiLoginConfirm(ConfirmRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    #endregion
}