﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class Agent_SendPassword : Page
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid siteId;
    public string script = "<script></script>";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());

            divJRF.Visible = _master.IsVisibleJRF(siteId);
            divGeneralInfo.Visible = _master.IsVisibleGeneralInfo(siteId);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSuccessMsg.Text = string.Empty;
        if (!IsPostBack)
        {
            if (Session["emailaddressuser"] != null)
            {
                txtEmail.Enabled = false;
                hdnId.Value = Convert.ToString(Session["emailaddressuser"]);
                var list = _ManageUser.GetAdminUserDetails(Guid.Parse(hdnId.Value));
                if (list != null && list.IsActive == true)
                    txtEmail.Text = list.EmailAddress;
            }
            Session.Remove("emailaddressuser");
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);
            QubitOperationLoad();
        }
    }
    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList =
                    (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", SiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(txtEmail.Text))
        {
            var id = Guid.Parse(hdnId.Value);
            var admin = _db.tblAdminUsers.FirstOrDefault(x => x.ID == id); 
            string Subject = "Generate Password Link";
            if (admin != null)
            {
                string siteName = _master.GetSiteNameByID(siteId); 
                string Body = "<html><head><title></title></head><body> <p><br>Hi, <br><br>Thanks for requesting the creation of a password for your username on the 1track " + siteName + " website.<br><br>Your username is: <b> " + admin.UserName + "</b><br><br>Please click on the link below (or copy and paste into you browser) to generate your new password:<br><br><a href='" + siteURL + "Agent/ResetPassword.aspx?reqId=" + hdnId.Value + "'>" + siteURL + "Agent/ResetPassword.aspx?reqId=" + hdnId.Value + "</a><br><br>Thanks<br><br>International Rail</p><br><br></body></html>";                 
                var SendEmail = ManageUser.SendMail(Subject, Body, txtEmail.Text, siteId);
                if (SendEmail)
                {
                    txtEmail.Text = string.Empty;
                    lblSuccessMsg.Text = "Email sent on registred email.";
                }
            }
        }
        else
            lblSuccessMsg.Text = "Email is not Registered with System";
    }
}