﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Xml;
using Business;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OneHubServiceRef;
using System.Configuration;

public partial class OrderSuccessPage : Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageBooking oBooking = new ManageBooking();
    readonly private ManageOrder _master = new ManageOrder();
    public string currency = "$";
    public string ReservationCode = null;
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    public static Guid currencyID = new Guid();
    private Guid siteId;
    public string script = "<script></script>";
    public string products = "";
    public string WTpnsku = "";
    public string WTtxu = "";
    public string WTtxs = "";
    public string WTtxI = "";
    public string WTtxid = "";
    public string WTtxit = "";
    public string printUrl = "";
    public string pdfName = "";
    public string FileURL = "";
    public string OrderNo = string.Empty;
    private string htmfile = string.Empty;
    ManageUser mngUser = new ManageUser();
    public bool isEvolviBooking = false;
    public bool isNTVBooking = false;
    public bool isBene = false;
    public bool isTI = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["IsCheckout"] = null;
            #region Is order paid ?
            if (Session["IsPaymentSuccess"] == null)
                if (Request.Params["req"] != null)
                    Response.Redirect("PaymentProcess?req=" + Request.Params["req"]);
                else
                    Response.Redirect("PaymentProcess");
            else
                Session.Remove("IsPaymentSuccess");
            #endregion
        }

        Session.Remove("OgoneResponse");
        long order = 0;
        if (Request.Params["req"] != null)
        {
            OrderNo = Request.Params["req"];
            order = Convert.ToInt32(OrderNo);
        }
        else if (Session["OrderID"] != null)
        {
            OrderNo = Session["OrderID"].ToString();
            order = Convert.ToInt32(OrderNo);
        }

        var apiTypeList = new ManageBooking().GetAllApiType(Convert.ToInt32(OrderNo));
        if (apiTypeList != null)
        {
            isEvolviBooking = apiTypeList.isEvolvi;
            isNTVBooking = apiTypeList.isNTV;
            isBene = apiTypeList.isBene;
            isTI = apiTypeList.isTI;
        }

        #region If user changed query sitring data then redirct at home

        if (!IsPostBack)
        {
            if (Session["OrderID"] != null)
                ViewState["tempOrder"] = Session["OrderID"].ToString();
            else if (Session["P2POrderID"] != null)
                ViewState["tempOrder"] = Session["P2POrderID"].ToString();

            if (Session["SegmentType"] != null)
            {
                trTrenItalia.Visible = !(Session["SegmentType"].ToString().Trim() == "M");
                btnpdf.Visible = (Session["SegmentType"].ToString().Trim() == "M");
            }

        }
        if (ViewState["tempOrder"] != null && Request.Params["req"] != null && ViewState["tempOrder"].ToString() != Request.Params["req"])
            Response.Redirect("~/");

        #endregion

        if (Session["ProductType"] != null)
            lblBannerTxt.Text = "Rail Tickets";

        if (!IsPostBack)
        {
            if (Session["P2POrderID"] != null)
                btnpdf.Visible = oBooking.CheckDilveryOptionType(Convert.ToInt64(Session["P2POrderID"]));
            if (Session["P2POrderID"] != null && Session["IsRegional"] != null)
                btnpdf.Visible = false;

            GetCurrencyCode();
            if (Session["P2POrderID"] != null)
            {
                PrintTicketInfo();
                var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                if (getDeliveryType != null && (getDeliveryType == "Thayls Ticketless" || getDeliveryType == "TrenItaila Printing"))
                    ConfirmP2PBooking();
            }
            else
            {
                Session.Remove("CollectStation");
                SendMail();
            }
            ShowPaymentDetails();
        }
        QubitOperationLoad();
        long orderID = 0;
        if (Session["OrderID"] != null)
            orderID = Convert.ToInt32(Session["OrderID"]);

        if (order == orderID)
        {
            Session.Remove("RailPassData");
            Session.Remove("P2POrderID");
            Session.Remove("OrderID");
            Session.Remove("IsRegional");
        }
        Session.Remove("IsRegional");
        Session.Remove("SegmentType");
    }

    #region Print & confirm P2P ticket

    private void PrintTicketInfo()
    {
        if (Session["BOOKING-REPLY"] == null)
            return;

        var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
        if (list.Any(x => x.PinCode != null))
        {
            if (Session["P2POrderID"] != null)
            {
                var chkDeliveryType = oBooking.CheckDilveryOptionType(Convert.ToInt64(Session["P2POrderID"]));
                if (chkDeliveryType)
                    Printickets();
                else
                {
                    TicketBooking();
                    SendMail();
                }
            }
        }
        else
            PrintTicketItalia();

    }
    public void Printickets()
    {
        if (Session["BOOKING-REPLY"] != null)
        {
            var bookingList = Session["BOOKING-REPLY"] as List<BookingResponse>;
            if (bookingList == null)
                return;
            var client = new OneHubRailOneHubClient();
            var listResponse = new List<PrintResponse>();
            foreach (var item in bookingList)
            {
                #region Ticket Print
                var objHeader = new OneHubServiceRef.Header
                     {
                         onehubusername = "#@dots!squares",
                         onehubpassword = "#@dots!squares",
                         unitofwork = 0,
                     };

                #region TicketBookingRequest
                var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
                ApiLogin(objRequest);
                var tktBooking = client.TicketBookingRequest(objRequest);
                oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? tktBooking.PinCode : item.PinCode);
                #endregion

                var printRequest = new TicketPrintRequest
                {
                    Header = objHeader,
                    BillingAddress = item.BillingAddress != null ? new BillingAddress
                    {
                        Address = item.BillingAddress.Address,
                        City = item.BillingAddress.City,
                        Country = item.BillingAddress.Country,
                        CrisNumber = item.BillingAddress.CrisNumber,
                        DeliveryDate = item.BillingAddress.DeliveryDate,
                        Email = item.BillingAddress.Email,
                        FirstName = item.BillingAddress.FirstName,
                        Lastname = item.BillingAddress.Lastname,
                        Phone = item.BillingAddress.Phone,
                        State = item.BillingAddress.State,
                        ZipCode = item.BillingAddress.ZipCode
                    } : null,
                    ReservationCode = item.ReservationCode,
                    ChangeReservationCode = item.ChangeReservationCode,
                    PinCode = tktBooking.PinCode,
                    TicketType = "PDF_DT",
                    ApiInformation = new ApiInformation
                    {
                        ApiName = GetApiName()
                    }
                };

                ApiLoginPrint(printRequest);
                var printResponse = client.TicketPrint(printRequest);

                if (printResponse != null)
                {
                    listResponse.Add(new PrintResponse
                    {
                        ReservationCode = printRequest.ReservationCode,
                        status = String.IsNullOrEmpty(printResponse.PrintIndication) ? "False" : printResponse.PrintIndication,
                        URL = String.IsNullOrEmpty(printResponse.TicketUrl) ? "Ticket print URL not found." : printResponse.TicketUrl
                    });
                }
                #endregion
            }

            var newList = new List<PrintResponse>();
            foreach (var resp in listResponse)
            {
                if (resp != null)
                {
                    printUrl = resp.URL;
                    if (resp.URL.Contains("pdf"))
                        printUrl = BeNePdfService(printUrl, resp.ReservationCode);

                    if (!string.IsNullOrEmpty(printUrl))
                        oBooking.UpdateTicketUrlByReservationCode(resp.ReservationCode, printUrl);

                    newList.Add(new PrintResponse { ReservationCode = resp.ReservationCode, status = resp.status, URL = printUrl });
                }
            }
            Session["TicketBooking-REPLY"] = newList;

            if (listResponse.Count != 0)
                SendMail();
            else
                ShowMessage(2, "Ticket Print not Available");
        }
    }
    public string BeNePdfService(string url, string resCode)
    {
        try
        {
            string outputFile = Server.MapPath("~/PdfService/" + resCode + ".pdf");
            var webClient = new WebClient();
            using (var webStream = webClient.OpenRead(url))
            using (var fileStream = new FileStream(outputFile, FileMode.Create))
            {
                var buffer = new byte[32768];
                int bytesRead;

                while (webStream != null && (bytesRead = webStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, bytesRead);
                }
            }
            string siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            return siteURL + "PdfService/" + resCode + ".pdf";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnPdf_Click(object sender, EventArgs e)
    {
        Response.Redirect("PrintTicket.aspx");
    }

    public void PrintTicketItalia()
    {
        try
        {
            var newList = new List<PrintResponse>();
            if (Session["BOOKING-REPLY"] != null)
            {
                var list = Session["BOOKING-REPLY"] as List<BookingResponse>;
                foreach (var item in list)
                    if (item.PdfUrl != null)
                    {
                        string newUrl = "";
                        foreach (var url in item.PdfUrl)
                        {
                            newUrl += url + "#";
                        }
                        oBooking.UpdateTicketUrlByReservationCode(item.ReservationCode, newUrl);
                        newList.Add(new PrintResponse { ReservationCode = item.ReservationCode, status = "PRINT REQUEST SUBMITTED", URL = newUrl });
                    }
            }
            Session["TicketBooking-REPLY"] = newList;
            SendMail();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }



    public bool ConfirmP2PBooking()
    {
        bool result = false;
        if (Session["BOOKING-REPLY"] == null)
            return result;

        var client = new OneHubRailOneHubClient();
        var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;

        foreach (var item in listbookingReply)
        {
            var objResponse = new ConfirmResponse();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = item.UnitOfWork,
                language = Language.nl_BE,
            };

            var objRequest = new ConfirmRequest { Header = objHeader, ReservationCode = item.ReservationCode, ApiInformation = new ApiInformation { ApiName = GetApiName() } };
            ApiLoginConfirm(objRequest);
            objResponse = client.Confirm(objRequest);

            if (objResponse != null)
            {
                string res = objResponse.ConfirmOk;
                result = res == "Ok" || res == "True";
            }
            oBooking.UpdateReservationCode(Convert.ToInt32(Session["P2POrderID"]), item.ReservationCode, result);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, objResponse.PinCode != null ? objResponse.PinCode : item.UnitOfWork.ToString());
        }
        return result;
    }

    public void TicketBooking()
    {

        try
        {
            var listbookingReply = Session["BOOKING-REPLY"] as List<BookingResponse>;
            var item = listbookingReply.FirstOrDefault();
            var objHeader = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
            };

            var client = new OneHubRailOneHubClient();
            var objRequest = new TicketBookingRequest { Header = objHeader, ReservationCode = item.ReservationCode };
            ApiLogin(objRequest);
            var tktBooking = client.TicketBookingRequest(objRequest);
            oBooking.UpdatePinNumberByReservationCode(item.ReservationCode, tktBooking != null ? tktBooking.PinCode : item.PinCode);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    #endregion

    public void QubitOperationLoad()
    {
        try
        {
            var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;

            var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
            if (lst.Count() > 0)
            {
                var lstNew = from a in lst
                             select
                                 new
                                     {
                                         category =
                                 (new ManageBooking().getPassDescForQbit(a.PassSaleID, a.ProductType, "Category")),
                                         currency = currencyCode,
                                         name = (new ManageBooking().getPassDescForQbit(a.PassSaleID, a.ProductType, "")),
                                         sku_code = a.ProductType.ToLower().Trim() == "pass" ? "Rail Pass" : "Rail Ticket",
                                         unit_price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                         unit_sale_price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType))
                                     };

                var listProduct = new List<ProuductLineItem>();
                string skuCode = "";
                string[] WTtxu1 = new string[lst.Count()];
                string[] WTtxs1 = new string[lst.Count()];
                if (lstNew.Count() > 0)
                {
                    int i = 0;
                    foreach (var litem in lstNew)
                    {
                        skuCode = litem.sku_code;
                        var obj = new ProuductLineItem
                            {
                                product = new ProductItem
                                    {
                                        category = litem.category,
                                        currency = litem.currency,
                                        name = litem.name,
                                        sku_code = litem.sku_code,
                                        unit_price = litem.unit_price.ToString(),
                                        unit_sale_price = litem.unit_sale_price.ToString(),
                                    },
                                quantity = 1,
                                subtotal = litem.unit_sale_price
                            };
                        listProduct.Add(obj);
                        WTtxu1[i] = "1";
                        WTtxs1[i] = litem.unit_sale_price.ToString();
                        i++;
                    }
                }

                var uPriceDistinct = WTtxs1.Distinct().ToArray();
                string[] uPriceDistictCount = new string[uPriceDistinct.Count()];
                int c = 0;
                foreach (var s in uPriceDistinct)
                {
                    int cin = 0;
                    foreach (string a in WTtxs1)
                    {
                        if (a == s.Trim())
                        {
                            cin++;
                        }
                    }
                    uPriceDistictCount[c] = cin.ToString();
                    c++;
                }

                WTpnsku = String.Format("<meta name=\"WT.pn_sku\" content=\"{0}\" />",
                                        skuCode == "Rail Pass" ? "RailPass" : "RailTicket");

                //<meta name="WT.tx_u" content="1;1" />                                           
                // units, if an order contains multiple products, separate the numbers of units for each product using a semi-colon
                WTtxu = String.Format("<meta name=\"WT.tx_u\" content=\"{0}\" />",
                                      String.Join(";", uPriceDistictCount.ToArray()));

                //<meta name="WT.tx_s" content="151.13;24" /> 
                // Identifies the total cost for each product in the order, separate cost for each product using a semi-colon
                WTtxs = String.Format("<meta name=\"WT.tx_s\" content=\"{0}\" />",
                                      String.Join(";", uPriceDistinct.ToArray()));

                //<meta name="WT.tx_I" content="949" />
                // invoice number
                WTtxI = String.Format("<meta name=\"WT.tx_I\" content=\"{0}\" />", OrderNo);

                //<meta name="WT.tx_id" content="01/15/14" />
                // Date of Order (MM/DD/YYYY)  
                WTtxid = String.Format("<meta name=\"WT.tx_id\" content=\"{0}\" />",
                                       lst.FirstOrDefault().CreatedOn.ToString("MM/dd/yyyy"));

                //<meta name="WT.tx_it" content="19:07:04" /> 
                // Time of Order (HH:MM:SS)
                WTtxit = String.Format("<meta name=\"WT.tx_it\" content=\"{0}\" />",
                                       lst.FirstOrDefault().CreatedOn.ToString("HH:mm:ss"));

                decimal shipingAMt = lst.FirstOrDefault().ShippingAmount.HasValue ? Convert.ToDecimal(lst.FirstOrDefault().ShippingAmount) : 0;
                var cart = new BookingCartInfo
                    {
                        currency = currencyCode,
                        Date_of_order = lst.FirstOrDefault().CreatedOn.ToString("MM/dd/yyyy"),
                        line_items = listProduct.ToList(),
                        order_id = OrderNo,
                        shipping_cost = shipingAMt,
                        shipping_method = Session["ShipMethod"] != null ? Session["ShipMethod"].ToString() : "",
                        subtotal = listProduct.Sum(x => x.subtotal),
                        tax = 0,
                        Time_of_order = lst.FirstOrDefault().CreatedOn.ToString("HH:mm:ss"),
                        total = shipingAMt + listProduct.Sum(x => x.subtotal),
                        voucher = "",
                        voucher_discount = 0
                    };

                var json_serializer = new JavaScriptSerializer();
                products = json_serializer.Serialize(cart);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public void ShowPaymentDetails()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                List<GetAllCartData_Result> lst = new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).OrderBy(a => a.OrderIdentity).ToList();
                var lstNew = from a in lst
                             select new { Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)), ProductDesc = (new ManageBooking().getPassDescType(a.PassSaleID, a.ProductType, (string)(Session["TrainType"])) + "<tr><td>Name : </td><td>" + a.Title + " " + a.FirstName + " " + a.LastName + "</td></tr>"), TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0) };
                if (lst.Count > 0)
                {
                    rptOrderInfo.DataSource = lstNew.ToList();
                    lblClientName.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                    lblClientName1.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                    lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                    lblDeliveryAddress.Text = lst.FirstOrDefault().Address1 + " " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City : string.Empty) + " " + lst.FirstOrDefault().Postcode + ",<br> " + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry;
                    lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();
                    pdfName = lst.FirstOrDefault().OrderID.ToString();
                    lblTotal.Text = (lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TktPrtCharge)).ToString();
                    lblShippingAmount.Text = lst.FirstOrDefault().ShippingAmount.ToString();
                    lblBookingFee.Text = lst.FirstOrDefault().BookingFee.ToString();
                    lblGrandTotal.Text = ((lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount + lst.FirstOrDefault().BookingFee).ToString();
                    hdnUserName.Value = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;

                    if (Session["IsRegional"] != null)
                        trRegional.Visible = true;
                    else
                        trRegional.Visible = false;
                }
                else
                    rptOrderInfo.DataSource = null;
                rptOrderInfo.DataBind();
            }
            else
            {
                Response.Redirect("~/Home", false);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
        }
    }

    public bool SendMail()
    {
        bool retVal = false;
        try
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var st = _db.tblSites.FirstOrDefault(x => x.ID == siteId);

            if (st != null)
            {
                string SiteName = st.SiteURL + "Home";

                currency =
                    oManageClass.GetCurrency(st.DefaultCurrencyID.HasValue ? (st.DefaultCurrencyID.Value) : new Guid());
                // Passing the values and make a email formate to display

                var orderID = Convert.ToInt32(OrderNo);
                string Subject = "Order Confirmation #" + orderID;
                string dir = HttpContext.Current.Request.PhysicalApplicationPath;

                /*Get Mail template by themeid*/
                if (st.IsSTA ?? false)
                    htmfile = Server.MapPath("~/MailTemplate/BlueMailTemplate.htm");
                else
                    htmfile = Server.MapPath("~/MailTemplate/MailTemplate.htm");

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);

                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                string UserName,
                       EmailAddress,
                       DeliveryAddress,
                       OrderDate,
                       OrderNumber,
                       Total,
                       ShippingAmount,
                       GrandTotal,
                       BookingFee;
                UserName = EmailAddress = DeliveryAddress = OrderDate = OrderNumber = Total = ShippingAmount = GrandTotal = BookingFee = "";
                List<GetAllCartData_Result> lst =
                    new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).OrderBy(a => a.OrderIdentity).ToList();
                var lst1 = from a in lst
                           select new
                               {
                                   Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                   ProductDesc =
                               (new ManageBooking().getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName +
                                " " + a.LastName),
                                   TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                               };

                if (lst.Count > 0)
                {
                    EmailAddress = lst.FirstOrDefault().EmailAddress;
                    DeliveryAddress = lst.FirstOrDefault().Address1 + ", " +
                                      (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2)
                                           ? lst.FirstOrDefault().Address2 + ", "
                                           : string.Empty) + "<br>" +
                                      (!string.IsNullOrEmpty(lst.FirstOrDefault().City)
                                           ? lst.FirstOrDefault().City + ", " + lst.FirstOrDefault().Postcode + ", <br>"
                                           : string.Empty) +
                                      (!string.IsNullOrEmpty(lst.FirstOrDefault().State)
                                           ? lst.FirstOrDefault().State + ",<br> "
                                           : string.Empty) + lst.FirstOrDefault().DCountry;
                    OrderDate = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    OrderNumber = lst.FirstOrDefault().OrderID.ToString();
                    Total = (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                    ShippingAmount = lst.FirstOrDefault().ShippingAmount.ToString();
                    BookingFee = lst.FirstOrDefault().BookingFee.ToString();
                    GrandTotal =
                        ((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount +
                         lst.FirstOrDefault().BookingFee).ToString();
                    UserName = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " +
                               lst.FirstOrDefault().DLastName;
                }
                // to address
                string ToEmail = EmailAddress;
                body = body.Replace("##UserName##", UserName.Trim());
                body = body.Replace("##EmailAddress##", EmailAddress.Trim());
                body = body.Replace("##DeliveryAddress##", DeliveryAddress.Trim());
                body = body.Replace("##OrderDate##", OrderDate.Trim());
                body = body.Replace("##OrderNumber##", OrderNumber.Trim());
                body = body.Replace("##Items##", currency + " " + Total.Trim());
                ////*Nett Price*/
                var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (objsite != null && objsite.IsAgent == true)
                {
                    var prdPass = _master.GetPassSale(orderID);
                    decimal tckPCharge = prdPass.Sum(ty => ty.TicketProtection);
                    var p2p = _master.GetP2PTckP(orderID);
                    decimal p2PTckPCharge = p2p.Sum(tp => tp.TicketProtection);

                    var nettPrice = "";
                    if (Session["P2POrderID"] != null)
                        nettPrice = currency + " " + (Convert.ToDecimal(Total) - Convert.ToDecimal(p2PTckPCharge)).ToString();
                    else
                    {
                        string eurail = _master.GetEurailData(lst.FirstOrDefault().OrderID).ToString("F");
                        string nonEurail = _master.GetNonEurailData(lst.FirstOrDefault().OrderID).ToString("F");
                        nettPrice = currency + " " + (Convert.ToDecimal(eurail) + Convert.ToDecimal(nonEurail)).ToString("F");
                    }

                    body = body.Replace("##NettPrice##", "<tr><td style='font-size: 12px' valign='top'>" +
                                                             "<font face='Arial, Helvetica, sans-serif' color='#000000'><b>Nett Price:</b></font>" +
                                                             "</td><td style='font-size: 12px'>" +
                                                             "<font face='Arial, Helvetica, sans-serif' color='#000000'>" +
                                                             nettPrice + "</font>" +
                                                             "</td>" + "</tr>");
                }
                else
                    body = body.Replace("##NettPrice##", "");
                ////*end Nett price*/

                if (Session["IsRegional"] != null)
                    Session["SegmentType"] = "N";


                body = body.Replace("##Shipping##", currency + " " + ShippingAmount.Trim());
                body = body.Replace("##BookingFee##", currency + " " + BookingFee.Trim());
                body = body.Replace("##CommissionFee##", "");
                body = body.Replace("##Total##", currency + " " + GrandTotal.Trim());


                string britrailpromoTerms = isBritRailPromoPass(orderID) ? "<br/><tr><td style='font-size: 12px' valign='top'><font face='Arial, Helvetica, sans-serif' color='#000000'><b>* Terms and condition for:</b></font> </td><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'>BritRail Freeday Promo pass <a href='" + st.SiteURL + "BritRailFreedayPromoTerms.aspx' traget='_blank'> Click Here </a></font></td><tr>" : "";
                if (Session["ShipMethod"] != null)
                {
                    string shippingdesc = "<tr><td style='font-size: 12px' valign='top'>" +
                          "<font face='Arial, Helvetica, sans-serif' color='#000000'><b>Shipping:</b></font>" +
                          "</td><td style='font-size: 12px'>" +
                            "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + Session["ShipMethod"] + "</font><br/>" +
                            "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + ((string)Session["ShipDesc"] != "" ? (Server.HtmlDecode(Session["ShipDesc"].ToString())) : "") + "</font>" +
                        "</td>" + "</tr>";
                    body = body.Replace("##ShippingMethod##", shippingdesc + britrailpromoTerms);
                }
                else
                    body = body.Replace("##ShippingMethod##", britrailpromoTerms);


                body = body.Replace("#Blanck#", "&nbsp;");
                var PrintResponselist = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                List<GetAllCartData_Result> lstC = new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = from a in lstC
                             select new
                                 {
                                     a,
                                     Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)),
                                     ProductDesc =
                                 (new ManageBooking().getPassDescMail(a.PassSaleID, a.ProductType, PrintResponselist, (string)(Session["TrainType"]))),
                                     TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                     Terms = a.terms
                                 };

                string strProductDesc = "";
                int i = 1;
                if (lstNew.Count() > 0)
                {
                    lstNew = lstNew.OrderBy(ty => ty.a.OrderIdentity).ToList();
                    foreach (var x in lstNew)
                    {
                        if (x.a.ProductType == "Pass")
                        {
                            strProductDesc += "<tr>" +
                                              "<td style='font-size: 12px; vertical-align:top;'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'> <b>Journey " + i.ToString() + ":</b></font>" + "</td>" +
                                              "<td style='font-size: 12px' valign='top'>" + "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.ProductDesc +
                                              "</font></td></tr>" +

                                              (Session["CollectStation"] != null
                                                   ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Collect at ticket desk</b></font></td><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' color='#000' valign='top'>" +
                                                     Session["CollectStation"].ToString() + "</td></tr>" : "") +

                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Name</b><td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.a.Title + " " + x.a.FirstName +
                                              " " + x.a.LastName + "</font></td></tr>" +
                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Price</b></font></td><td style='font-size: 12px;color:#000;' valign='top' color='#000'>" +
                                              currency + " " + x.Price.ToString() + "</td></tr>" +
                                              (x.TktPrtCharge > 0 ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Ticket Protection</b></font></td><td style='font-size: 12px;color:#000;' color='#000' valign='top'>" +
                                                     currency + " " + Convert.ToString(x.TktPrtCharge) : "") + "</td></tr>";
                        }
                        else
                        {
                            strProductDesc += "<tr>" +
                                              "<td style='font-size: 12px; vertical-align:top;' colspan='2'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'> <b>Journey " +
                                              i.ToString() + ":</b></font>" + "</td></tr>" +
                                              "<tr><td style='font-size: 12px;color:#000;' valign='top'><td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.ProductDesc +
                                              "</font></td></tr>" +

                                              (Session["CollectStation"] != null
                                                   ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Collect at ticket desk</b></font></td><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' color='#000' valign='top'>" +
                                                     Session["CollectStation"].ToString() + "</td></tr>" : "") +

                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Name</b><td style='font-size: 12px' valign='top'>" +
                                              "<font face='Arial, Helvetica, sans-serif' color='#000'>" + x.a.Title + " " + x.a.FirstName +
                                              " " + x.a.LastName + "</font></td></tr>" +
                                              "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Price</b></font></td><td style='font-size: 12px;color:#000;' valign='top' color='#000'>" +
                                              currency + " " + x.Price.ToString() + "</td></tr>" + GetSegmentNote() +
                                              (x.TktPrtCharge > 0 ? "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' valign='top'><b>Ticket Protection</b></font></td><td style='font-size: 12px;color:#000;' color='#000' valign='top'>" +
                                                     currency + " " + Convert.ToString(x.TktPrtCharge) : "") + "</td></tr>" +
                                             "<tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;' colspan='2' valign='top'><b>Ticket Terms:</b></font>" + Server.HtmlDecode(x.Terms.Replace("<div class='toogle' style='display:none'>", "<div class='toogle' style='display:block'>")) + "</tr>";

                        }
                        i++;
                    }
                }
                body = body.Replace("##OrderDetails##", strProductDesc);
                if (Session["P2POrderID"] != null)
                {
                    var getDeliveryType = oBooking.getDeliveryOption(Convert.ToInt64(Session["P2POrderID"]));
                    if (getDeliveryType != null && getDeliveryType == "Print at Home")
                    {
                        const string printAthome = "<div style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color:#000;width:440px;'>Please ensure you have a printed copy of your PDF booking confirmation as train operators across europe will not always accept the booking from your tablet or mobile screen.</div>";
                        body = body.Replace("##PrintAtHome##", printAthome);
                        Session["showPrintAtHome"] = "1";
                    }
                    else
                        body = body.Replace("##PrintAtHome##", "");
                }
                else
                    body = body.Replace("##PrintAtHome##", "");

                body = body.Replace("../images/", SiteName + "images/");

                /*Send Email to Bcc email address*/

                string BccEmail = _oWebsitePage.GetBccEmail(siteId);

                /*Get smtp details*/
                var result = _masterPage.GetEmailSettingDetail(siteId);
                if (result != null && lstNew.Count() > 0)
                {
                    var fromAddres = new MailAddress(result.Email, result.Email);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToEmail);

                    if (!string.IsNullOrEmpty(BccEmail))
                    {
                        message.Bcc.Add(BccEmail);
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);
                    }
                    else
                        message.Bcc.Add(ConfigurationManager.AppSettings["BCCMailAddress"]);

                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    smtpClient.Send(message);
                    retVal = true;
                }
                Session["ShipMethod"] = string.Empty;
            }
        }
        catch
        {
            ShowMessage(2, "Failed to send E-mail confimation!");
        }
        return retVal;
    }
    private string GetSegmentNote()
    {
        List<PrintResponse> listTicket = Session["TicketBooking-REPLY"] as List<PrintResponse>;
        if (Session["SegmentType"] != null)
            if ((string)Session["SegmentType"] == "N" || listTicket.Any(x => string.IsNullOrEmpty(x.URL)) || listTicket.Count == 0)
            {
                return "<tr><td style='font-size: 12px; border: 1px solid red;padding: 5px' valign='top' colspan='2'>" +
                                "<font face='Arial, Helvetica, sans-serif' color='red'>" + "<b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from one of the self-service " +
                                    "ticket machines located at all main Trenitalia stations (which are green, white and red). Once you have printed your ticket you must validate it in one of the validation " +
                                    "machines located near to the entrance of each platform (they are green and white with Trenitalia written on the front and usually mounted on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will result in a fine." +
                                    "</font></td></tr>";

            }
        return "";
    }
    bool isBritRailPromoPass(long orderid)
    {
        try
        {
            foreach (var result in _db.tblPassSales.Where(x => x.OrderID == orderid).ToList())
            {
                bool isPromo = (_db.tblProducts.Any(x => x.ID == result.ProductID && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == result.ProductID && y.tblCategory.IsBritRailPass)));
                if (isPromo)
                    return true;
            }
            return false;
        }
        catch
        {
            return false;
        }

    }
    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }


    private void ApiLogin(TicketBookingRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    private void ApiLoginPrint(TicketPrintRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    private void ApiLoginConfirm(ConfirmRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }

    public string GetApiName()
    {
        if (isTI)
            return "TrainItalia";
        else if (isBene)
            return "BeNe";
        else if (isNTVBooking)
            return "Ntv";
        else if (isEvolviBooking)
            return "Evolvi";
        else
            return "";
    }
}

public class BookingCartInfo
{
    // A unique identifier for the order
    public string order_id { get; set; }
    // The standard letter code in capitals for the currency type in which the order is being paid, eg: EUR, USD, GBP
    public string currency { get; set; }
    // A valid number with the total cost of the basket including any known tax per item, but not including shipping or discounts
    public Decimal subtotal { get; set; }
    // A boolean true or false to indicate whether subtotal includes tax
    public bool subtotal_include_tax { get; set; }
    // A valid number with the total amount of potential tax included in the order
    public Decimal tax { get; set; }
    // A valid number with the total amount of potential shipping costs included in the order
    public Decimal shipping_cost { get; set; }
    // Optional. Describes the shipping method
    public string shipping_method { get; set; }
    // A valid number with the total cost of the basket including any known tax, shipping and discounts
    public Decimal total { get; set; }
    //Product list
    public List<ProuductLineItem> line_items { get; set; }
    // If voucher used
    public string voucher { get; set; }
    // A valid number with the total amount of discount due to the voucher entered
    public Decimal voucher_discount { get; set; }
    // Date of Order (MM/DD/YYYY)
    public string Date_of_order { get; set; }
    // Time of Order (HH:MM:SS)
    public string Time_of_order { get; set; }
}

public class ProuductLineItem
{
    public ProductItem product { get; set; }
    //quantity:1
    public int quantity { get; set; }
    //subtotal:5000.00
    public Decimal subtotal { get; set; }
}

public class ProductItem
{
    //sku_code: "SKU",
    public string sku_code { get; set; }

    //name: "PRODUCT-NAME",
    public string name { get; set; }

    //category: "PRODUCT-CATEGORY",
    public string category { get; set; }
    // currency: "GBP",
    public string currency { get; set; }

    //unit_price:151.53
    public string unit_price { get; set; }

    //unit_sale_price:151.53
    public string unit_sale_price { get; set; }
}