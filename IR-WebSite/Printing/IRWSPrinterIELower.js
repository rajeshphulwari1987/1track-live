﻿

function IRWSloadXMLDoc(url, handler) {
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    }
    else {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var milliseconds = new Date().getTime();
    url = url + "&datestamp=" + milliseconds;
    xhttp.open("GET", url, false);
    xhttp.onreadystatechange = handler;
    xhttp.send("");
}

function IRWSQueryPrintQueue() {
    var stockqueueID = document.getElementById("IRWSstockqueue").value;
    var siteID = document.getElementById("IRWSSiteID").value;
    var productTypeID = document.getElementById("IRWSProductTypeID").value;
    var url = "/webservices/Printer.aspx?siteID=" + siteID + "&typeID=" + productTypeID + "&qID=" + stockqueueID;
    xml = IRWSloadXMLDoc(url, IRWSCheckQueue);
}

function IRWSConverToGuid(string) {
    var GUId = string.substring(0, 8) + "-" + string.substring(8, 12) + "-" + string.substring(12, 16) + "-" + string.substring(16, 20) + "-" + string.substring(20);
    return GUId;
}

function IRWSCallPrintQueue() {
    IRWSQueryPrintQueue();
}

function IRWSCheckQueue() {
    if (xhttp.readyState == 4) {
        if (xhttp.status == 200) {
            IRWSProcessQueue(xhttp.responseXML);
        }
    }
}

function IRWSUpdateItem() {
    if (xhttp.readyState == 4) {
        if (xhttp.status == 200) {
            IRWSProcessXML(xhttp.responseXML);
        }
    }
}

function evalself(e) {
    if(e == null) e = window.event;
    return (typeof e.target != 'undefined') ? e.target : e.srcElement;
}

function IRWSaddToMYQueue(e) {
    e = evalself(e);
    var seletedid = e.parentNode.id;
    var siteID = document.getElementById("IRWSSiteID").value;
    var productTypeID = document.getElementById("IRWSProductTypeID").value;
    var url = "/webservices/Printer.aspx?sid=" + IRWSConverToGuid(seletedid.replace("IRWS", "")) + "&siteID=" + siteID + "&typeID=" + productTypeID;
    xml = IRWSloadXMLDoc(url, IRWSUpdateItem);
}

function removeItem(e) {
    var answer = confirm("Are you really sure you want to delete this order from the Print Queue?")
    if (answer) {
        e = evalself(e);
        var seletedid = e.parentNode.id;
        var siteID = document.getElementById("IRWSSiteID").value;
        var productTypeID = document.getElementById("IRWSProductTypeID").value;
        var url = "/webservices/Printer.aspx?d=r&sid=" + IRWSConverToGuid(seletedid.replace("IRWS", "")) + "&siteID=" + siteID + "&typeID=" + productTypeID;
        e.parentNode.parentNode.removeChild(e.parentNode);
        xml = IRWSloadXMLDoc(url, IRWSUpdateItem);
    }
}

function moveItem(e) {
    e = evalself(e);
    var seletedid = e.parentNode.id;
    document.getElementById("selectid").value = seletedid;

    var objqueuemove = document.getElementById("hiddenqueuearea");
    objqueuemove.style.visability = "visible";
    objqueuemove.style.display = "block";
}


function moveItemGo() {
    var seletedid = document.getElementById("selectid").value;
    var movequeueid = document.getElementById("movequeueid").value;
    var siteID = document.getElementById("IRWSSiteID").value;
    var productTypeID = document.getElementById("IRWSProductTypeID").value;
    var url = "/webservices/Printer.aspx?d=m&sid=" + IRWSConverToGuid(seletedid.replace("IRWS", "")) + "&siteID=" + siteID + "&typeID=" + productTypeID + "&movequeueid=" + movequeueid;
    xml = IRWSloadXMLDoc(url, IRWSUpdateItem);
    //document.getElementById(seletedid).parentNode.removeChild(document.getElementById(seletedid));
    moveItemCancel();
    
}

function moveItemCancel() {
    var objqueuemove = document.getElementById("hiddenqueuearea");
    objqueuemove.style.visability = "hidden";
    objqueuemove.style.display = "none";
}

function Changequeueview() {
    var IRWSTABLE = document.getElementById("IRWSTABLE");
    while (IRWSTABLE.childNodes.length > 1) {
       
        if (IRWSTABLE.lastChild.id != "IRWSROW1") {
            IRWSTABLE.removeChild(IRWSTABLE.lastChild);
        }
    }

}


function IRWSCalcStatusFunction(StatusID) {
    var StatusFunction = "";
    switch (StatusID) {
        case "0":
            StatusFunction = IRWSaddToMYQueue;
            break;
        case "10":
            StatusFunction = IRWSaddToMYQueue;
            break;
        case "20":
            StatusFunction = "";
            break;
        case "30":
            StatusFunction = "";
            break;
    }
    return StatusFunction;
}



function IRWSProcessXML(xml) {
    var table = document.getElementById("IRWSTABLE");
    var rows = document.getElementsByTagName("div");
    if (xml.documentElement) {
        for (var i = 0; i < table.childNodes.length; i++) {
            if (table.childNodes[i].childNodes.length > 0) {
                if (table.childNodes[i].id.substring(0, 4) == "IRWS") {
                    if (table.childNodes[i].id.substring(4, 7) != "ROW") {
                        id = table.childNodes[i].id.substring(4, 50);
                        found = false;
                        for (var nodecount = 0; nodecount < xml.documentElement.childNodes.length; nodecount++) {
                            var node = xml.documentElement.childNodes[nodecount];
                            var ID = node.childNodes[0].firstChild.nodeValue.replace(/-/g, "");
                            if (ID == id) {
                                found = true;
                            }
                        }
                        if (found == false) {
                            table.childNodes[i].parentNode.removeChild(table.childNodes[i]);
                        }
                    }
                }
            }

        }

        for (var nodecount = 0; nodecount < xml.documentElement.childNodes.length; nodecount++) {
            var node = xml.documentElement.childNodes[nodecount];
            var ID = node.childNodes[0].firstChild.nodeValue;
            var StatusID = node.childNodes[3].firstChild.nodeValue;
            var StatusName = node.childNodes[6].firstChild.nodeValue;
            var statusFunction = IRWSCalcStatusFunction(StatusID);
            var AdminName = node.childNodes[5].firstChild.nodeValue;
            var GroupParent = node.childNodes[9].firstChild.nodeValue;
            var PID = node.childNodes[10].firstChild.nodeValue;

            var ROW = document.getElementById("IRWS" + ID.replace(/-/g, ""))
            if (ROW) {
                if (StatusName != ROW.childNodes[3].firstChild.nodeValue) {
                    ROW.childNodes[3].firstChild.nodeValue = StatusName;
                    if (GroupParent == PID) {
                        ROW.className = "row IRWSStatus" + StatusID;
                    } else {
                        ROW.className = "rowchildproduct IRWSStatus" + StatusID;
                    }
                    ROW.childNodes[3].title = "With " + AdminName;
                }
            } else {
                var OrderID = node.childNodes[1].firstChild.nodeValue;
                var OrderNum = node.childNodes[2].firstChild.nodeValue;
                var LeadPassenger = (node.childNodes[8] && node.childNodes[8].firstChild) ? node.childNodes[8].firstChild.nodeValue : '';
                var AdminUser = (node.childNodes[5] && node.childNodes[5].firstChild) ? node.childNodes[5].firstChild.nodeValue : '';
                var ProductDescription = node.childNodes[4].firstChild.nodeValue;

                var NewRow = document.createElement("div");
                NewRow.id = "IRWS" + ID.replace(/-/g, "");
                if (GroupParent == PID) {
                    NewRow.className = "row IRWSStatus" + StatusID;
                }
                else {
                    NewRow.className = "rowchildproduct IRWSStatus" + StatusID;
                }
             
                var OrderNumber = document.createElement("div");
                var LeadName = document.createElement("div");
                var ProductName = document.createElement("div");
                var Status = document.createElement("div");
                var Move = document.createElement("div");
                var Remove = document.createElement("div");
                
		        if (statusFunction != "") {
		            OrderNumber.attachEvent("onclick", statusFunction);
		            LeadName.attachEvent("onclick", statusFunction);
		            ProductName.attachEvent("onclick", statusFunction);
		            Status.attachEvent("onclick", statusFunction);
		            Move.attachEvent("onclick", moveItem);
		            Remove.attachEvent("onclick", removeItem);
		        }

                OrderNumber.className = "IRWSorderNumber";
                LeadName.className = "IRWSLead";
                LeadName.title = LeadPassenger;
                ProductName.className = "IRWSProduct";
                ProductName.title = ProductDescription;
                Status.className = "IRWSstatus";
                Status.title = "With " + AdminName;
                Move.className = "IRWSMove";
                Move.title = "Move this pass";
                Remove.className = "IRWSRemove";
                Remove.title = "Remove this pass";
                
                OrderNumber.innerHTML = OrderNum;
                LeadName.innerHTML = LeadPassenger;
                ProductName.innerHTML = ProductDescription;
                Status.innerHTML = StatusName;
                Move.innerHTML = "Move";
                Remove.innerHTML = "Delete";

                NewRow.appendChild(OrderNumber);
                NewRow.appendChild(LeadName);
                NewRow.appendChild(ProductName);
                NewRow.appendChild(Status);
                NewRow.appendChild(Move);
                NewRow.appendChild(Remove);

                table.appendChild(NewRow);
            }
        }
    }

    
}

function IRWSprocessTable(xml) {
    var table = document.getElementById("IRWSTABLE");
    var rows = document.getElementsByTagName("TR");
    for (var rowcount = 2; rowcount < rows.length; rowcount++) {
        var found = false;
        var guid = IRWSConverToGuid(rows[rowcount].id.replace("IRWS", ""));
        for (var nodecount = 0; nodecount < xml.documentElement.childNodes.length; nodecount++) {
            var node = xml.documentElement.childNodes[nodecount];
            var ID = node.childNodes[0].firstChild.nodeValue;
            if (guid == ID) {
                found = true;
            }
        }
        if (!found) {
            rows[rowcount].removeNode(true);
        }
    }
}

function IRWSProcessQueue(xml) {
    IRWSProcessXML(xml);
    IRWSprocessTable(xml);

    setTimeout("IRWSCallPrintQueue()", 2000);
}

function IRWSPrint() {
    var siteID = document.getElementById("IRWSSiteID").value;
    var stockqueue = document.getElementById("IRWSstockqueue").value;
    
    var productTypeID = document.getElementById("IRWSProductTypeID").value;
    var url = "/Printing_Assigned.aspx?orgid=" + stockqueue + "&siteID=" + siteID + "&typeID=" + productTypeID;
    window.location = url;

}

window.onload = IRWSCallPrintQueue;
