using System;
using System.Globalization;
using System.Web.UI;
using Business;
using System.Xml;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Configuration;

public partial class Printing_PrintingAssigned : Page
{
    private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();
    private readonly ManageBooking MBooking = new ManageBooking();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static List<GetInJobedPrintQueueItems> datalist = new List<GetInJobedPrintQueueItems>();
    private Guid _siteId;
    public string SiteUrl;
    public string script = "<script></script>";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            SiteUrl = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["AgentUsername"] == null)
                Response.Redirect(SiteUrl);
            if (!Page.IsPostBack)
            {
                BindQueue();
                QubitOperationLoad();
                btnProblem.Visible = btnPrintedOK.Visible = btnNotVoidOk.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindQueue()
    {
        List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
        List<Guid> listIds = listCatIds != null ? listCatIds.Select(x => x.Id).Distinct().ToList() : null;
        if (Request.QueryString["qId"] != null && listIds != null)
        {
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            dvPrd.Visible = true;
            List<GetInJobedPrintQueueItems> list = new List<GetInJobedPrintQueueItems>();
            string strIds = "";
            foreach (var item in listIds)
                strIds = string.IsNullOrEmpty(strIds) ? item.ToString() : strIds + "," + item.ToString();

            list = _oPrint.GetPrintQueueInJobList(queueId, strIds, _siteId, AgentuserInfo.UserID);
            if (list != null && list.Count > 0)
            {
                lblRangeFrm.Text = list.Min(ty => ty.StockNo).ToString();
                lblRangeTo.Text = list.Max(ty => ty.StockNo).ToString();
                datalist = list;
                grdPrint.DataSource = list;
                grdPrint.DataBind();
            }
            else
            {
                ShowMessage(2, "Stock not available for this agent.");
                btnPrint.Visible = false;
            }

        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
            List<Guid> listIds = listCatIds.Select(x => x.Id).Distinct().ToList();
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            foreach (var item in listIds)
                _oPrint.UpdatePrintQStausFromAssigenToInJob(queueId, item, _siteId);
            PrintClick();
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt64(stkNo.Text);
                stockno.Add(stNo);
            }
            btnConfirm_Click(stockno.Min(), stockno.Max());
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnConfirm_Click(long stockfrom, long stockto)
    {
        try
        {
            Guid queueId = new Guid();
            if (Request.QueryString["qId"] != null)
                queueId = Guid.Parse(Request.QueryString["qId"]);

            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;

            string tktXML = "";
            string pqitmIds = "";
            int start = 0;
            List<Guid> ids = new List<Guid>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
                if (chkPrint.Checked)
                {
                    HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
                    Guid id = Guid.Parse(hdnID.Value);
                    ids.Add(id);
                    if (start == 0)
                        pqitmIds = id.ToString();
                    else
                        pqitmIds += "," + id.ToString();
                    start = 1;
                }
            }
            var list = _oPrint.GetPrintQueueItemsInJobList(userid, pqitmIds, queueId, stockfrom, stockto);
            foreach (var item in list)
                tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;

            if (!string.IsNullOrEmpty(tktXML))
                PrintTicket(queueId, tktXML);
            foreach (var id in ids)
                _oPrint.UpdatePrintQConfirmedStatus(id, 2); //2.printed
            UpdateEurailStockNumberStatus(2); //2.printed
        }
        catch (Exception ex)
        {
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateEurailStockNoReuse(stno);//reuse stockno
            CleareprintingQueue(4);//4.Queued tblprintqueueitems update
            UpdateOrderStatusByStatus(false, 3);//3:Approved;
            ShowMessage(2, ex.Message);
        }
    }

    public void PrintTicket(Guid branchId, string tktXML)
    {
        try
        {
            XmlDocument coupan = new XmlDocument();
            coupan.LoadXml(tktXML);
            //--Load RCT2 Template     
            var list = _oPrint.GetRCT2Template(branchId,0);
            if (list == null || list.Count == 0) return;
            XmlDocument template = new XmlDocument();
            template.LoadXml(list[0].Template);

            TicketPrinter.TicketPrinter TP = new TicketPrinter.TicketPrinter();
            TP.RootPath = Server.MapPath("");
            MemoryStream ms = new MemoryStream();
            ms = TP.Print(template, coupan);
            byte[] streampdf = ms.ToArray();
            string FileName = Guid.NewGuid().ToString();
            string file = "Printing/PrintedTicket/" + FileName + ".pdf";
            string path = Server.MapPath("~/" + file);
            File.WriteAllBytes(path, streampdf);
            ViewState["url"] = "../" + file;
            ViewState["FileURL"] = ConfigurationManager.AppSettings["PassTicketURl"] + FileName + ".pdf";//"D:\\Projects\\1Track\\IR-WebSite\\Printing\\PrintedTicket\\" 
            PrintingView.Visible = true;

            Printingiframe.Attributes["src"] = SiteUrl + file;
           

            UpdateOrderStatusByStatus(true, 7);//7:Completed;
            rangeAssign.Visible = btnPrint.Visible = btnCancel.Visible = false;
            Msgtxt.InnerHtml = "Please find below a PDF of your pass(es).  In order to print, first check the correct stock (and stock numbers) are in the printer.   Next click on the printer icon in the top right corner of the PDF window, choose your printer and make sure you have the �Legal� paper size selected, then click �OK�.   <br/> </br> Once you have printed the pass(es) successfully on the correct stock (and stock numbers), click �All passes printed OK� at the bottom of the page.";
            lblHeader.Text = "Printing";
            btnProblem.Visible = btnPrintedOK.Visible = true;
        }
        catch { }
    }

    public void PrintClick()
    {
        Guid PrintQueueIdNew = Guid.Empty;
        bool caseCount = true;
        Guid PQID = Guid.Empty;
        Guid queueId = new Guid();
        if (Request.QueryString["qId"] != null)
            queueId = Guid.Parse(Request.QueryString["qId"]);
        string oldorderId = string.Empty;
        Int32 StockNumber = 0;
        int rowcount = 0;
        int NO = grdPrint.Rows.Count;

        #region saver Case
        string PassSaleIdIn = string.Empty;
        Guid PassPrintid = Guid.Empty;
        bool SecondSaver = true;
        Guid PassPrintidSecondSaver = Guid.Empty;
        Guid Passid = Guid.Empty;
        int stockHold = 0;
        int countSaverMaxFive = 0;
        int countS = 0;
        int countMaxFivereset = 0;
        bool insertsaver = false;
        string SaverGroup = string.Empty;
        #endregion

        foreach (GridViewRow row in grdPrint.Rows)
        {
            rowcount++;
            CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            HiddenField hdnCategoryID = row.FindControl("hdnCategoryID") as HiddenField;
            HiddenField hdnSaver = row.FindControl("hdnSaver") as HiddenField;
            HiddenField hdnStatus = row.FindControl("hdnStatus") as HiddenField;
            Guid catId = Guid.Parse(hdnCategoryID.Value);
            Guid id = Guid.Parse(hdnID.Value);
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            if (Convert.ToUInt64(hdnOrderNo.Value) > 0)
                PQID = id;
            if (chkPrint.Checked && !string.IsNullOrWhiteSpace(hdnOrderNo.Value))
            {
                /*for saver pass*/
                if (hdnSaver.Value.ToLower().Contains("yes"))
                {
                    #region for saver pass case
                    Guid passSaleID = Guid.Empty;
                    if ((oldorderId != hdnOrderNo.Value) || (SaverGroup != hdnSaver.Value))
                    {
                        if ((countSaverMaxFive > 5) && (countMaxFivereset > 0) && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn))
                        {
                            # region for xml coupan create
                            string tktXML2 = "";
                            var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                            foreach (var item in list2)
                            {
                                tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                            }
                            if (!string.IsNullOrEmpty(tktXML2))
                            {
                                XmlDataDocument xDoc = new XmlDataDocument();
                                xDoc.LoadXml(tktXML2);
                                XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                                XmlNode xnr = result.SelectSingleNode("/Coupons");
                                if (xnr != null)
                                {
                                    foreach (XmlNode xn in xnr)
                                    {
                                        countS++;
                                        if (countS == 2)
                                        {
                                            StockNumber = stockHold;
                                            Guid userid = AdminuserInfo.UserID;
                                            if (AgentuserInfo.UserID != new Guid())
                                                userid = AgentuserInfo.UserID;
                                            _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                            countS = 0;
                                        }
                                    }
                                }
                                insertsaver = false;
                                oldorderId = PassSaleIdIn = string.Empty;
                                PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                                countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                            }
                            #endregion
                        }
                        if (!string.IsNullOrEmpty(oldorderId) && (countSaverMaxFive <= 5))
                        {
                            #region for xml coupan create
                            string tktXML = "";
                            var list1 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                            foreach (var item in list1)
                            {
                                tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                            }
                            if (!string.IsNullOrEmpty(tktXML))
                            {
                                XmlDataDocument xDoc = new XmlDataDocument();
                                xDoc.LoadXml(tktXML);
                                XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                                XmlNode xnr = result.SelectSingleNode("/Coupons");
                                if (xnr != null)
                                {
                                    foreach (XmlNode xn in xnr)
                                    {
                                        Guid userid = AdminuserInfo.UserID;
                                        if (AgentuserInfo.UserID != new Guid())
                                            userid = AgentuserInfo.UserID;
                                        _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, false);
                                        StockNumber++;
                                        PassPrintid = PassPrintidSecondSaver;
                                    }
                                }
                            }
                            oldorderId = PassSaleIdIn = string.Empty;
                            PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                            countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                            #endregion
                        }
                        SaverGroup = hdnSaver.Value;
                        countMaxFivereset = countSaverMaxFive = 0;
                        countMaxFivereset++;
                        countSaverMaxFive++;
                        oldorderId = hdnOrderNo.Value;
                        PassPrintid = id;
                        PassSaleIdIn = _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                        Passid = Guid.Parse(PassSaleIdIn);
                        StockNumber = stNo;
                    }
                    else
                    {
                        if (SecondSaver)
                        {
                            stockHold = stNo;
                            PassPrintidSecondSaver = id;
                            SecondSaver = false;
                        }
                        countMaxFivereset++;
                        countSaverMaxFive++;

                        if (countSaverMaxFive > 5 && countMaxFivereset == 6)
                        {
                            # region for xml coupan create
                            string tktXML2 = "";
                            countMaxFivereset = 1;
                            var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                            foreach (var item in list2)
                            {
                                tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                            }
                            if (!string.IsNullOrEmpty(tktXML2))
                            {
                                XmlDataDocument xDoc = new XmlDataDocument();
                                xDoc.LoadXml(tktXML2);
                                XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                                XmlNode xnr = result.SelectSingleNode("/Coupons");
                                if (xnr != null)
                                {
                                    foreach (XmlNode xn in xnr)
                                    {
                                        countS++;
                                        if (!insertsaver)
                                        {
                                            Guid userid = AdminuserInfo.UserID;
                                            if (AgentuserInfo.UserID != new Guid())
                                                userid = AgentuserInfo.UserID;
                                            _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                            StockNumber++;
                                            PassPrintid = PassPrintidSecondSaver;
                                        }
                                        else if (countS == 2)
                                        {
                                            StockNumber = stockHold;
                                            Guid userid = AdminuserInfo.UserID;
                                            if (AgentuserInfo.UserID != new Guid())
                                                userid = AgentuserInfo.UserID;
                                            _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                        }
                                        if (countS == 2)
                                        {
                                            insertsaver = true;
                                            countS = 0;
                                        }
                                    }
                                    PassSaleIdIn = Guid.Empty.ToString();
                                }
                            }
                            #endregion
                        }
                        PassSaleIdIn += "," + _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                    }
                    #endregion
                }

                if ((countSaverMaxFive > 5) && (countMaxFivereset > 0) && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && !hdnSaver.Value.ToLower().Contains("yes") || rowcount == grdPrint.Rows.Count && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && (countSaverMaxFive > 5) && (countMaxFivereset > 0))
                {
                    # region for xml coupan create
                    string tktXML2 = "";
                    PassSaleIdIn = PassSaleIdIn.Replace("00000000-0000-0000-0000-000000000000,", string.Empty);
                    var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                    foreach (var item in list2)
                    {
                        tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                    }
                    if (!string.IsNullOrEmpty(tktXML2))
                    {
                        XmlDataDocument xDoc = new XmlDataDocument();
                        xDoc.LoadXml(tktXML2);
                        XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                        XmlNode xnr = result.SelectSingleNode("/Coupons");
                        if (xnr != null)
                        {
                            foreach (XmlNode xn in xnr)
                            {
                                countS++;
                                if (countS == 2)
                                {
                                    StockNumber = stockHold;
                                    Guid userid = AdminuserInfo.UserID;
                                    if (AgentuserInfo.UserID != new Guid())
                                        userid = AgentuserInfo.UserID;
                                    _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                    countS = 0;
                                }
                            }
                        }
                        insertsaver = false;
                        oldorderId = PassSaleIdIn = string.Empty;
                        PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                        countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                    }
                    #endregion
                }

                if (Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && !hdnSaver.Value.ToLower().Contains("yes") && (countSaverMaxFive <= 5) || rowcount == grdPrint.Rows.Count && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && (countSaverMaxFive <= 5))
                {
                    # region for xml coupan create
                    string tktXML2 = "";
                    var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                    foreach (var item in list2)
                    {
                        tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                    }
                    if (!string.IsNullOrEmpty(tktXML2))
                    {
                        XmlDataDocument xDoc = new XmlDataDocument();
                        xDoc.LoadXml(tktXML2);
                        XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                        XmlNode xnr = result.SelectSingleNode("/Coupons");
                        if (xnr != null)
                        {
                            foreach (XmlNode xn in xnr)
                            {
                                Guid userid = AdminuserInfo.UserID;
                                if (AgentuserInfo.UserID != new Guid())
                                    userid = AgentuserInfo.UserID;
                                _oPrint.InsertStockInUsage(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, false);
                                StockNumber++;
                                PassPrintid = PassPrintidSecondSaver;
                            }
                        }
                    }
                    oldorderId = PassSaleIdIn = string.Empty;
                    PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                    countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                    #endregion
                }

                /*if non saver and void then*/
                if (!hdnSaver.Value.ToLower().Contains("yes"))
                {
                    #region For Void and non saver pass
                    string tktXML = "";
                    Guid passSaleID = (Guid)_oPrint.GetPassSaleIDByPrintQueueID(id);
                    var list = _oPrint.GetRetrieveEurailProductXML(passSaleID, passSaleID.ToString());
                    foreach (var item in list)
                    {
                        tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                    }
                    if (!string.IsNullOrEmpty(tktXML))
                    {
                        XmlDataDocument xDoc = new XmlDataDocument();
                        xDoc.LoadXml(tktXML);
                        XmlDocument result = EurailBuildTemplate(xDoc, Server.MapPath("Eurail.XML"));
                        XmlNode xnr = result.SelectSingleNode("/Coupons");
                        if (xnr != null)
                        {
                            foreach (XmlNode xn in xnr)
                            {
                                Guid userid = AdminuserInfo.UserID;
                                if (AgentuserInfo.UserID != new Guid())
                                    userid = AgentuserInfo.UserID;
                                _oPrint.InsertStockInUsage(catId, queueId, id, stNo, xn.OuterXml, userid, false);
                            }
                        }
                    }
                    else
                    {
                        if (caseCount)
                        {
                            caseCount = false;
                            PrintQueueIdNew = Guid.NewGuid();
                            _oPrint.Insertqueueitems(PrintQueueIdNew, _oPrint.getPQdata(PQID));
                        }
                        string VOID = "<Coupon name=\"Pass\"><Element type=\"text\" name=\"void\" startcol=\"1\" endcol=\"72\" startrow=\"A\" endrow=\"R\" fontsize=\"37\" valign=\"middle\" align=\"middle\" fontface=\"Verdana\" fontbold=\"true\">VOID</Element></Coupon>";
                        _oPrint.InsertStockInUsage(catId, queueId, PrintQueueIdNew, stNo, VOID, AdminuserInfo.UserID, false);
                    }
                    #endregion
                }
            }
        }
    }

    public XmlDocument EurailBuildTemplate(XmlDocument xDoc, string Path)
    {
        try
        {
            #region DECLARE FUNCATION VARIABLES
            decimal cost = 0;
            int ClassCode = 2;
            int TravellerCat = 0;
            int DaysIn = 0;
            int LastDateDaysIn = 0;
            int Months = 0;
            int CountryCodeStart = 0;
            string CIVNo = "0";
            string PassType = "";
            string Traveller = "";
            string validity = "";
            string PassengerName = "";
            string Countryofresidence = "";
            string passportno = "";
            string ValidIN = "";
            string PromoText = "";
            string currency = "EUR";
            Boolean EurailPromo = false;
            Boolean travelcalender = false;
            Passenger[] pas = new Passenger[0];
            DateTime ActivatedByDate = DateTime.MinValue;
            DateTime startdate = DateTime.MinValue;
            DateTime enddate = DateTime.MinValue;
            DateTime dateofissue = DateTime.Now;
            DateTime musstbeactivated = dateofissue.AddMonths(6);
            Boolean IsFlexi = false;
            XmlNode xnr;
            XmlDocument xTemplate = new XmlDocument();
            Guid ProductID = Guid.Empty;
            string PrintingNote = string.Empty;
            string CountryCombination = string.Empty;
            #endregion

            #region
            xTemplate.Load(Path);

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ID");
            if (xnr != null)
            {
                ProductID = Guid.Parse(xnr.InnerText);
                PrintingNote = _oPrint.GetProductPrintingNote(ProductID);//Valid with Eurail cover & passport. See conditions of use prior to travel www.eurailgroup.org/attica
                DateTime PrdtValidityDate = _oPrint.GetProductValiditydate(ProductID);
                if (musstbeactivated > PrdtValidityDate)
                {
                    musstbeactivated = PrdtValidityDate;
                }
            }

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CountryCodeStart");
            if (xnr != null) CountryCombination = xnr.InnerText;

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/IsPromo");
            if (xnr != null) EurailPromo = Convert.ToBoolean(xnr.InnerText);

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/TravellerCat");
            if ((xnr != null) && (xnr.InnerText != "")) TravellerCat = Convert.ToInt32(xnr.InnerText);

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Cost");
            if ((xnr != null) && (xnr.InnerText != ""))
            {
                try
                {
                    cost = Convert.ToDecimal(xnr.InnerText.ToString());
                }
                catch (Exception e) { }
            }

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/SupplierCurrencyID");
            if ((xnr != null) && (xnr.InnerText != ""))
            {
                Guid sourceCurId = Guid.Parse(xnr.InnerText);
                Guid targetCurrId = FrontEndManagePass.GetCurrencyID("SEU");
                if (sourceCurId != targetCurrId)
                    cost = FrontEndManagePass.GetPriceAfterConversion(cost, _siteId, targetCurrId, sourceCurId);
            }
            xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/PassStartDate");
            if ((xnr != null) && (xnr.InnerText != ""))
            {
                try
                {
                    startdate = DateTime.Parse(xnr.InnerText);
                }
                catch (Exception e) { }
            }
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ClassCode");
            if ((xnr != null) && (xnr.InnerText != "")) ClassCode = Convert.ToInt32(xnr.InnerText);
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Traveller");
            if ((xnr != null) && (xnr.InnerText != "")) Traveller = xnr.InnerText;
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CountryCodeStart");
            if (xnr != null && xnr.InnerText != "") CountryCodeStart = Convert.ToInt32(xnr.InnerText);
            if (CountryCodeStart > 0 && CountryCodeStart < 2001)// The Pass name EURAIL [Country] PASS. Spelling of the country name as in SCIC Appendix 2  
            {
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Name");
                if ((xnr != null) && (xnr.InnerText != "")) PassType = xnr.InnerText;
            }
            else
            {
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/PassType");
                if ((xnr != null) && (xnr.InnerText != "")) PassType = xnr.InnerText;
            }

            if (CountryCodeStart == 1015)// In case of a Eurail Benelux pass the text �Valid in� followed by �Belgium - Luxemburg - The Netherlands� must be added
                ValidIN = "Belgium - Luxemburg - The Netherlands";
            else if (CountryCodeStart == 1022)// In case of a Eurail Greek Islands Pass the text �Valid in� followed by �Attica Ferries� must be added
                ValidIN = "Attica Ferries";
            else if (CountryCodeStart == 1016)// In case of a Eurail Scandinavia Pass the text �Valid in� followed by �Denmark - Finland - Norway - Sweden� must be added
                ValidIN = "Denmark - Finland - Norway - Sweden";
            else if (CountryCodeStart > 1000 && CountryCodeStart < 2001)
                ValidIN = string.Empty;
            else
            {
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Countries");
                if (xnr != null)
                    ValidIN = xnr.InnerText.Replace(",", " - ");
            }
            xnr = xDoc.SelectSingleNode("/Voucher/Passengers");
            if (xnr != null)
            {
                XmlNodeList nl = xnr.ChildNodes;
                pas = new Passenger[nl.Count];
                int counter = 0;
                foreach (XmlNode xn in nl)
                {
                    pas[counter].Name = xn["Name"].InnerText;
                    pas[counter].Country = xn["Country"].InnerText;
                    pas[counter].PassportNumber = xn["PassportNo"].InnerText;
                    counter++;
                }
            }
            #endregion

            #region CHECK VALIDITY BY NAME IN DATABSE WITH DATE,FLEXI,PROMOPASSTEXT
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Vailidity");
            if ((xnr != null) && (xnr.InnerText != "")) validity = xnr.InnerText;
            string PromoPassText = FrontEndManagePass.ProductValidityByNameAndProductId(validity, ProductID);
            if (!string.IsNullOrEmpty(PromoPassText))
                PromoText = PromoPassText;

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/DaysIn");
            if ((xnr != null) && (xnr.InnerText != "")) DaysIn = (!String.IsNullOrEmpty(xnr.InnerText)) ? Convert.ToInt32(xnr.InnerText) : 0;

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Months");
            if ((xnr != null) && (xnr.InnerText != "")) Months = (!String.IsNullOrEmpty(xnr.InnerText)) ? Convert.ToInt32(xnr.InnerText) : 0;

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/IsFlexi");
            if (xnr != null) IsFlexi = Convert.ToBoolean(xnr.InnerText);

            if (DaysIn > 0 && Months == 0)
                enddate = startdate.AddDays(DaysIn);
            else if (Months > 0 && DaysIn == 0)
                enddate = startdate.AddMonths(Months);
            else if (DaysIn > 0 && Months > 0)
                enddate = startdate.AddMonths(Months);
            if (enddate > DateTime.Now)
                enddate = enddate.AddDays(-1);

            #endregion

            #region WHEN SAVER THEN EXECUTE SAVER CASE
            bool forsaver = false;
            if ((TravellerCat == 51) || (TravellerCat == 53) || (TravellerCat == 74) || (TravellerCat == 75))
            {
                forsaver = true;
                //string strid = string.Empty;
                //foreach (GridViewRow row in grdPrint.Rows)
                //{
                //    HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
                //    Guid id = Guid.Parse(hdnID.Value);
                //    strid += id + ",";
                //}
                //string datastr = _oPrint.GetFreechil(strid);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='tandc']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passdescriptiontext']");
                if (xnr != null) xnr.InnerText = "Eurail Global Pass No. " + CIVNo + " is only valid with the control voucher";
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='CIVNo']");
                if (xnr != null) xnr.InnerText = CIVNo + 1;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissue']");
                if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Product']");
                if (xnr != null) xnr.InnerText = PassType.ToUpper();
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='DurationType']");
                if (IsFlexi)
                {
                    travelcalender = true;
                    if (xnr != null) xnr.InnerText = "FLEXI";
                }
                else
                {
                    xnr.InnerText = "CONTINUOUS";
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALCALENDERTEXT']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
                if (Traveller.ToLower().Contains("saver"))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Traveller']");
                    if (xnr != null) xnr.InnerText = Traveller.Replace("Saver", "        ");
                }
                else
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='TravellerSaver']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);

                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Traveller']");
                    if (xnr != null) xnr.InnerText = Traveller;
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='validity']");
                if (xnr != null) xnr.InnerText = validity;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Class']");
                if (xnr != null) xnr.InnerText = ClassCode.ToString();
                PassengerName = "SEE CONTROL VOUCHER";
                Countryofresidence = "SEE CONTROL VOUCHER";
                passportno = "SEE CONTROL VOUCHER";
                if (pas != null)
                {
                    for (var counter = 0; counter < pas.Length; counter++)
                    {
                        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='PassengerName" + (counter + 1).ToString() + "']");
                        if (xnr != null) xnr.InnerText = pas[counter].Name;
                        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Countryofresidence" + (counter + 1).ToString() + "']");
                        if (xnr != null) xnr.InnerText = pas[counter].Country;
                        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno" + (counter + 1).ToString() + "']");
                        if (xnr != null) xnr.InnerText = pas[counter].PassportNumber;
                    }
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='musstbeactivated']");
                if (xnr != null) xnr.InnerText = "BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
            }
            else
            {
                if (Traveller.ToLower().Equals("child") && cost == 0) //child if price 0 case
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='tandc']");
                    if (xnr != null) xnr.InnerText = "This coupon is only valid with Adult Pass. Maximum 2 free Children per Adult.";
                }
                else if (!string.IsNullOrEmpty(PrintingNote))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='tandc']");
                    if (xnr != null) xnr.InnerText = PrintingNote;
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='tandccoupon']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/Name");
                if (xnr != null) PassengerName = xnr.InnerText;
                xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/Country");
                if ((xnr != null) && (xnr.InnerText != "")) Countryofresidence = xnr.InnerText;
                xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/PassportNo");
                if ((xnr != null) && (xnr.InnerText != "")) passportno = xnr.InnerText;
            }
            #endregion

            #region traveller validity country passport etc
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATED']");
            if (xnr != null) xnr.InnerText = "BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='CIVNo']");
            if (xnr != null) xnr.InnerText = CIVNo;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissue']");
            if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp6']");
            if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Product']");
            if (xnr != null) xnr.InnerText = PassType.ToUpper();
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='DurationType']");
            if (IsFlexi)
            {
                travelcalender = true;
                if (xnr != null) xnr.InnerText = "FLEXI";
            }
            else
            {
                if (xnr != null) xnr.InnerText = "CONTINUOUS";
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALCALENDERTEXT']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            if ((CountryCodeStart > 0) && (CountryCodeStart < 9999))
            {
                if (!String.IsNullOrEmpty(ValidIN))
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='countries']");
                    if (xnr != null) xnr.InnerText = ValidIN;
                }
                else
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='countries']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validin']");
                    if (xnr != null && xnr.ParentNode != null) xnr.ParentNode.RemoveChild(xnr);
                }
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validin']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            if (Traveller.ToLower().Contains("saver"))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Traveller']");
                if (xnr != null) xnr.InnerText = Traveller.Replace("Saver", "        ");
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TravellerSaver']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Traveller']");
                if (xnr != null) xnr.InnerText = Traveller;
            }
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validity']");
            if (xnr != null) xnr.InnerText = validity;
            #endregion

            #region PRINTDATE FOR COMPLIMENTARY AND AD75 PASS ONLY
            if (TravellerCat == 31 || TravellerCat == 32 || TravellerCat == 61)
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATEDTEXT']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='MUSTBEACTIVATED']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday1']");
                if (startdate.Day > 9)
                {
                    if (xnr != null) xnr.InnerText = startdate.Day.ToString().Substring(0, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = "0";
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateday2']");
                if (startdate.Day > 9)
                {
                    if (xnr != null) xnr.InnerText = startdate.Day.ToString().Substring(1, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = startdate.Day.ToString();
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth1']");
                if (startdate.Month > 9)
                {
                    if (xnr != null) xnr.InnerText = startdate.Month.ToString().Substring(0, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = "0";
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdatemonth2']");
                if (startdate.Month > 9)
                {
                    if (xnr != null) xnr.InnerText = startdate.Month.ToString().Substring(1, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = startdate.Month.ToString();
                }
                try
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear1']");
                    if (xnr != null) xnr.InnerText = startdate.Year.ToString().Substring(2, 1);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='startdateyear2']");
                    if (xnr != null) xnr.InnerText = startdate.Year.ToString().Substring(3, 1);
                }
                catch (Exception e) { }

                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/LastDateDaysIn");
                if ((xnr != null) && (xnr.InnerText != "")) LastDateDaysIn = (!String.IsNullOrEmpty(xnr.InnerText)) ? Convert.ToInt32(xnr.InnerText) : 0;
                if (LastDateDaysIn > 0 && Months == 0)
                    enddate = startdate.AddDays(LastDateDaysIn - 1);

                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday1']");
                if (enddate.Day > 9)
                {
                    if (xnr != null) xnr.InnerText = enddate.Day.ToString().Substring(0, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = "0";
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateday2']");
                if (enddate.Day > 9)
                {
                    if (xnr != null) xnr.InnerText = enddate.Day.ToString().Substring(1, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = enddate.Day.ToString();
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth1']");
                if (enddate.Month > 9)
                {
                    if (xnr != null) xnr.InnerText = enddate.Month.ToString().Substring(0, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = "0";
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddatemonth2']");
                if (enddate.Month > 9)
                {
                    if (xnr != null) xnr.InnerText = enddate.Month.ToString().Substring(1, 1);
                }
                else
                {
                    if (xnr != null) xnr.InnerText = enddate.Month.ToString();
                }
                try
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear1']");
                    if (xnr != null) xnr.InnerText = enddate.Year.ToString().Substring(2, 1);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='enddateyear2']");
                    if (xnr != null) xnr.InnerText = enddate.Year.ToString().Substring(3, 1);
                }
                catch (Exception e) { }
            }
            #endregion

            #region EURAIL PROMOTIONAL PASS
            if (!EurailPromo)
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONAL']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALTEXT']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALCALENDERTEXT']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PROMOTIONALTEXT']");
                if (xnr != null) xnr.InnerText = PromoText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='travelcalender']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            #endregion

            #region
            if ((TravellerCat == 71) && (TravellerCat == 72) && (TravellerCat == 73) && (TravellerCat == 74) && (TravellerCat == 75))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='travelcalender']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PassengerName']");
            if (xnr != null) xnr.InnerText = PassengerName;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Countryofresidence']");
            if (xnr != null) xnr.InnerText = Countryofresidence;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
            if (xnr != null) xnr.InnerText = passportno;
            if (travelcalender)
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calender']");
                if (xnr != null) xnr.Attributes["numberofdays"].InnerXml = DaysIn.ToString();
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='travelcalender']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calender']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calenderDay']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calenderMonth']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Class']");
            if (xnr != null) xnr.InnerText = ClassCode.ToString();
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='cost']");
            if (xnr != null)
            {
                //if (EurailPromo)
                //    xnr.InnerText = currency + ": ***.**";
                //else
                xnr.InnerText = currency + ": " + cost.ToString("F2");
            }
            if ((TravellerCat != 31) && (TravellerCat != 32))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='AD75']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            if ((TravellerCat != 61))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='COMPLIMENTARY']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='COMPLIMENTARYcost']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            #endregion

            #region REMOVE ELEMENT IF EURAIL PROMOTIONAL PASS
            if (EurailPromo)
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
                if ((xnr != null) && (xnr.InnerText != ""))
                {
                    if (xnr.InnerText != "SEE CONTROL VOUCHER")
                    {
                        if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    }
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno3']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno4']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno5']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            #endregion

            #region AD75 and COMPLIMENTARY STAMP xml element
            if (!(TravellerCat == 31) && !(TravellerCat == 32) && !(TravellerCat == 61))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp3']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='printername2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp5']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp6']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp7']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp8']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissuerp9']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);

                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp1']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp3']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='printername2']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp5']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp6']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp7']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp8']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissuerp9']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            #endregion

            #region
            if ((TravellerCat == 61))
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='cost']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            #endregion

            return xTemplate;
        }
        catch (Exception ex)
        {
            ShowMessage(2, "Eurail XML Error.");
            return null;
        }
    }

    protected void btnProblem_Click(object sender, EventArgs e)
    {
        try
        {
            PrintingView.Visible = false;
            rangeAssign.Visible = true;
            UpdateOrderStatusByStatus(false, 3);//3:Approved;
            if (ViewState["FileURL"] != null)
            {
                try
                {
                    if (File.Exists(ViewState["FileURL"].ToString()))
                        File.Delete(ViewState["FileURL"].ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Printing File in used.");
                }
            }
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateEurailStockNo(stno, 5, AgentuserInfo.UserID);//void
            CleareprintingQueue(4);//4.Queued tblprintqueueitems update

            btnNotVoidOk.Visible = true;
            btnProblem.Visible = btnPrintedOK.Visible = btnPrint.Visible = btnCancel.Visible = false;

            lblHeader.Text = "Void Stock";
            Msgtxt.InnerText = "Please select (tick) the stock numbers that are still usable/undamaged and press 'OK' to attempt printing again on the same stock. If the stock is unusable and cannot be used again please untick all of the stock numbers on that sheet and click 'OK' to void it.";
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Showprinting", "Showprinting()", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void UpdateOrderStatusByStatus(bool CaseValid, int status)
    {
        try
        {//1:Order Created;3:Approved;6:Cancelled;7:Completed;8:Refund Pending;9:Refund Complete;19:Confirm;20:Partial refund
            foreach (GridViewRow row in grdPrint.Rows)
            {
                HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
                if (CaseValid)
                {
                    if (MBooking.AllPassPrintedForThisOrder(Convert.ToInt64(hdnOrderNo.Value)))
                        MBooking.UpdateOrderStatus(status, Convert.ToInt64(hdnOrderNo.Value));
                }
                else
                    MBooking.UpdateOrderStatus(status, Convert.ToInt64(hdnOrderNo.Value));
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        List<long> stockno = new List<long>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
        }
        foreach (var stno in stockno)
            _oPrint.UpdateEurailStockNoReuse(stno);
        CleareprintingQueue(4);//4.Queued tblprintqueueitems update
        Response.Redirect("PrintingOrders");
    }

    void UpdateEurailStockNumberStatus(int status)
    {
        foreach (GridViewRow row in grdPrint.Rows)
        {
            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            Int32 OrderNo = Convert.ToInt32(hdnOrderNo.Value);
            if (OrderNo == 0 && status != 3)//void
                _oPrint.UpdateEurailStockNo(stNo, 5, userid);
            else
                _oPrint.UpdateEurailStockNo(stNo, status, userid);
        }
    }

    protected void btnNotVoidOk_Click(object sender, EventArgs e)
    {
        bool thisisVoid = false;
        List<long> stockno = new List<long>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
            if (!chkunused.Checked)
                thisisVoid = true;
        }
        if (thisisVoid)
        {
            foreach (var stno in stockno)
                _oPrint.UpdateEurailStockNo(stno, 5, AgentuserInfo.UserID);
        }
        else
        {
            foreach (var stno in stockno)
            {
                //_oPrint.DeleteEurailStockNo(stno);
                _oPrint.UpdateEurailStockNoReuse(stno);
            }
        }
        CleareprintingQueue(4);//4.Queued tblprintqueueitems update
        Response.Redirect("PrintingOrders");
    }

    protected void btnPrintedOK_Click(object sender, EventArgs e)
    {
        string URlPath = string.Empty;
        if (ViewState["url"] != null)
            URlPath = ViewState["url"].ToString();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            _oPrint.UpdatePrintingURL(stNo, URlPath);
            break;
        }
        Response.Redirect("PrintingOrders");
    }

    public void CleareprintingQueue(int STATUS)
    {
        List<Guid> ids = new List<Guid>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            Guid id = Guid.Parse(hdnID.Value);
            ids.Add(id);
        }
        foreach (var id in ids)
            _oPrint.UpdatePrintQConfirmedStatus(id, STATUS);
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void grdPrint_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblHideClass = e.Row.FindControl("lblHideClass") as Label;
            if (!string.IsNullOrEmpty(lblHideClass.Text))
            {
                e.Row.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
            }
        }
    }

    struct Passenger
    {
        public string Name;
        public string Country;
        public string PassportNumber;
    }
}