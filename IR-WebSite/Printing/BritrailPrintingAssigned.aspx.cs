using System;
using System.Globalization;
using System.Web.UI;
using Business;
using System.Xml;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Configuration;

public partial class Printing_BritrailPrintingAssigned : Page
{
    private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();
    private readonly ManageBooking MBooking = new ManageBooking();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static List<GetInJobedPrintQueueItemsBritrail> datalist = new List<GetInJobedPrintQueueItemsBritrail>();
    private Guid _siteId;
    public string SiteUrl;
    public string script = "<script></script>";
    public static string XMLFileName = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            SiteUrl = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["AgentUsername"] == null)
                Response.Redirect(SiteUrl);
            if (!Page.IsPostBack)
            {
                BindQueue();
                QubitOperationLoad();
                btnProblem.Visible = btnPrintedOK.Visible = btnNotVoidOk.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindQueue()
    {
        List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
        List<Guid> listIds = listCatIds != null ? listCatIds.Select(x => x.Id).Distinct().ToList() : null;
        if (Request.QueryString["qId"] != null && listIds != null)
        {
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            dvPrd.Visible = true;
            List<GetInJobedPrintQueueItemsBritrail> list = new List<GetInJobedPrintQueueItemsBritrail>();
            string strIds = string.Empty;
            foreach (var item in listIds)
                strIds = string.IsNullOrEmpty(strIds) ? item.ToString() : strIds + "," + item.ToString();

            list = _oPrint.GetPrintQueueInJobListBritrail(queueId, strIds, _siteId, AgentuserInfo.UserID);
            if (list != null && list.Count > 0)
            {
                lblRangeFrm.Text = list.Min(ty => ty.StockNo).ToString();
                lblRangeTo.Text = list.Max(ty => ty.StockNo).ToString();
                datalist = list;
                grdPrint.DataSource = list;
                grdPrint.DataBind();
            }
            else
            {
                ShowMessage(2, "Stock not available for this agent.");
                btnPrint.Visible = false;
            }

        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
            List<Guid> listIds = listCatIds.Select(x => x.Id).Distinct().ToList();
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            foreach (var item in listIds)
                _oPrint.UpdatePrintQStausFromAssigenToInJob(queueId, item, _siteId);
            PrintClick();
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt64(stkNo.Text);
                stockno.Add(stNo);
            }
            btnConfirm_Click(stockno.Min(), stockno.Max());
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnConfirm_Click(long stockfrom, long stockto)
    {
        try
        {
            Guid queueId = new Guid();
            if (Request.QueryString["qId"] != null)
                queueId = Guid.Parse(Request.QueryString["qId"]);

            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;

            string tktXML = string.Empty;
            string pqitmIds = string.Empty;
            int start = 0;
            List<Guid> ids = new List<Guid>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
                if (chkPrint.Checked)
                {
                    HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
                    Guid id = Guid.Parse(hdnID.Value);
                    ids.Add(id);
                    if (start == 0)
                        pqitmIds = id.ToString();
                    else
                        pqitmIds += "," + id.ToString();
                    start = 1;
                }
            }

            var list = _oPrint.GetPrintQueueItemsInJobListBritrail(userid, pqitmIds, queueId, stockfrom, stockto);
            foreach (var item in list)
                tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;

            if (!string.IsNullOrEmpty(tktXML))
                PrintTicket(queueId, tktXML);
            foreach (var id in ids)
                _oPrint.UpdatePrintQConfirmedStatus(id, 2); //2.printed
            UpdateBritrailStockNumberStatus(2); //2.printed
        }
        catch (Exception ex)
        {
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateBritrailStockNoReuse(stno);//reuse stockno
            CleareprintingQueue(4);//4.Queued tblprintqueueitems update
            UpdateOrderStatusByStatus(false, 3);//3:Approved;
            ShowMessage(2, ex.Message);
        }
    }

    public void PrintTicket(Guid branchId, string tktXML)
    {
        try
        {
            XmlDocument coupan = new XmlDocument();
            coupan.LoadXml(tktXML);
            //--Load RCT2 Template     
            var list = _oPrint.GetRCT2Template(branchId,0);
            if (list == null || list.Count == 0) return;
            XmlDocument template = new XmlDocument();
            template.LoadXml(list[0].Template);

            TicketPrinter.TicketPrinter TP = new TicketPrinter.TicketPrinter();
            TP.RootPath = Server.MapPath("");
            MemoryStream ms = new MemoryStream();
            ms = TP.Print(template, coupan);
            byte[] streampdf = ms.ToArray();
            string FileName = Guid.NewGuid().ToString();
            string file = "Printing/PrintedTicket/" + FileName + ".pdf";
            string path = Server.MapPath("~/" + file);
            File.WriteAllBytes(path, streampdf);
            ViewState["url"] = "../" + file;
            ViewState["FileURL"] = ConfigurationManager.AppSettings["PassTicketURl"] + FileName + ".pdf";//"D:\\Projects\\1Track\\IR-WebSite\\Printing\\PrintedTicket\\" 
            PrintingView.Visible = true;
            Printingiframe.Attributes["src"] = "web/viewer.html?" + FileName;
            UpdateOrderStatusByStatus(true, 7);//7:Completed;
            rangeAssign.Visible=btnPrint.Visible = btnCancel.Visible = false;
            Msgtxt.InnerHtml = "Please find below a PDF of your pass(es).  In order to print, first check the correct stock (and stock numbers) are in the printer.   Next click on the printer icon in the top right corner of the PDF window, choose your printer and make sure you have the �Legal� paper size selected, then click �OK�.   <br/> </br> Once you have printed the pass(es) successfully on the correct stock (and stock numbers), click �All passes printed OK� at the bottom of the page.";
            lblHeader.Text = "Printing";
            btnProblem.Visible = btnPrintedOK.Visible = true;
            //Msgtxt.InnerHtml = ViewState["FileURL"].ToString();
        }
        catch { }
    }

    public void PrintClick()
    {
        try
        {
            int HoldStockno = 0;
            int HoldCount = 0;
            string HoldPasssaleIDs = string.Empty;
            Guid PrintQueueIdNew = Guid.Empty;
            bool caseCount = true;
            Guid PQID = Guid.Empty;
            Guid queueId = new Guid();
            if (Request.QueryString["qId"] != null)
                queueId = Guid.Parse(Request.QueryString["qId"]);
            string oldorderId = string.Empty;
            Int32 StockNumber = 0;
            int rowcount = 0;
            int NO = grdPrint.Rows.Count;

            #region saver Case
            string PassSaleIdIn = string.Empty;
            Guid PassPrintid = Guid.Empty;
            bool SecondSaver = true;
            Guid PassPrintidSecondSaver = Guid.Empty;
            Guid Passid = Guid.Empty;
            int stockHold = 0;
            int countSaverMaxFive = 0;
            int countS = 0;
            int countMaxFivereset = 0;
            bool insertsaver = false;
            string SaverGroup = string.Empty;
            #endregion

            foreach (GridViewRow row in grdPrint.Rows)
            {
                rowcount++;
                CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
                HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
                HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
                HiddenField hdnCategoryID = row.FindControl("hdnCategoryID") as HiddenField;
                HiddenField hdnSaver = row.FindControl("hdnSaver") as HiddenField;
                HiddenField hdnStatus = row.FindControl("hdnStatus") as HiddenField;
                Guid catId = Guid.Parse(hdnCategoryID.Value);
                Guid id = Guid.Parse(hdnID.Value);
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int32 stNo = Convert.ToInt32(stkNo.Text);
                if (Convert.ToUInt64(hdnOrderNo.Value) > 0)
                    PQID = id;
                if (chkPrint.Checked && !string.IsNullOrWhiteSpace(hdnOrderNo.Value))
                {
                    /*for saver pass*/
                    if (hdnSaver.Value.ToLower().Contains("yes"))
                    {
                        #region for saver pass case
                        Guid passSaleID = Guid.Empty;
                        if ((oldorderId != hdnOrderNo.Value) || (SaverGroup != hdnSaver.Value))
                        {
                            if (HoldStockno != 0 && HoldCount > 7)
                            {
                                _oPrint.UpdateSaverTravellerXML(HoldStockno, HoldPasssaleIDs);
                                HoldStockno = 0;
                                HoldPasssaleIDs = string.Empty;
                            }
                            if ((countSaverMaxFive > 7) && (countMaxFivereset > 0) && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn))
                            {
                                # region for xml coupan create
                                string tktXML2 = "";
                                var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                                foreach (var item in list2)
                                {
                                    tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                                }
                                if (!string.IsNullOrEmpty(tktXML2))
                                {
                                    XmlDataDocument xDoc = new XmlDataDocument();
                                    xDoc.LoadXml(tktXML2);
                                    XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, Passid, xDoc, 0, false);
                                    XmlNode xnr = result.SelectSingleNode("/Coupons");
                                    if (xnr != null)
                                    {
                                        foreach (XmlNode xn in xnr)
                                        {
                                            countS++;
                                            if (countS == 2)
                                            {
                                                StockNumber = stockHold;
                                                Guid userid = AdminuserInfo.UserID;
                                                if (AgentuserInfo.UserID != new Guid())
                                                    userid = AgentuserInfo.UserID;
                                                _oPrint.InsertStockInUsageBritrail(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                                countS = 0;
                                            }
                                        }
                                    }
                                    insertsaver = false;
                                    oldorderId = PassSaleIdIn = string.Empty;
                                    PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                                    countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                                }
                                #endregion
                            }
                            if (!string.IsNullOrEmpty(oldorderId) && (countSaverMaxFive <= 7))
                            {
                                #region for xml coupan create
                                string tktXML = "";
                                var list1 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                                foreach (var item in list1)
                                {
                                    tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                                }
                                if (!string.IsNullOrEmpty(tktXML))
                                {
                                    XmlDataDocument xDoc = new XmlDataDocument();
                                    xDoc.LoadXml(tktXML);
                                    XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, Passid, xDoc, 0, false);
                                    XmlNode xnr = result.SelectSingleNode("/Coupons");
                                    if (xnr != null)
                                    {
                                        foreach (XmlNode xn in xnr)
                                        {
                                            Guid userid = AdminuserInfo.UserID;
                                            if (AgentuserInfo.UserID != new Guid())
                                                userid = AgentuserInfo.UserID;
                                            _oPrint.InsertStockInUsageBritrail(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, false);
                                            StockNumber++;
                                            PassPrintid = PassPrintidSecondSaver;
                                        }
                                    }
                                }
                                oldorderId = PassSaleIdIn = string.Empty;
                                PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                                countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                                #endregion
                            }
                            SaverGroup = hdnSaver.Value;
                            countMaxFivereset = countSaverMaxFive = 0;
                            countMaxFivereset++;
                            countSaverMaxFive++;
                            oldorderId = hdnOrderNo.Value;
                            PassPrintid = id;
                            HoldPasssaleIDs = PassSaleIdIn = _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                            Passid = Guid.Parse(PassSaleIdIn);
                            HoldStockno = StockNumber = stNo;
                            HoldCount = 1;
                        }
                        else
                        {
                            if (SecondSaver)
                            {
                                stockHold = stNo;
                                PassPrintidSecondSaver = id;
                                SecondSaver = false;
                            }
                            countMaxFivereset++;
                            countSaverMaxFive++;

                            if (countSaverMaxFive > 7 && countMaxFivereset == 8)
                            {
                                # region for xml coupan create
                                string tktXML2 = "";
                                countMaxFivereset = 1;
                                var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                                foreach (var item in list2)
                                {
                                    tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                                }
                                if (!string.IsNullOrEmpty(tktXML2))
                                {
                                    XmlDataDocument xDoc = new XmlDataDocument();
                                    xDoc.LoadXml(tktXML2);
                                    XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, Passid, xDoc, 0, false);
                                    XmlNode xnr = result.SelectSingleNode("/Coupons");
                                    if (xnr != null)
                                    {
                                        foreach (XmlNode xn in xnr)
                                        {
                                            countS++;
                                            if (!insertsaver)
                                            {
                                                Guid userid = AdminuserInfo.UserID;
                                                if (AgentuserInfo.UserID != new Guid())
                                                    userid = AgentuserInfo.UserID;
                                                _oPrint.InsertStockInUsageBritrail(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                                StockNumber++;
                                                PassPrintid = PassPrintidSecondSaver;
                                            }
                                            else if (countS == 2)
                                            {
                                                StockNumber = stockHold;
                                                Guid userid = AdminuserInfo.UserID;
                                                if (AgentuserInfo.UserID != new Guid())
                                                    userid = AgentuserInfo.UserID;
                                                _oPrint.InsertStockInUsageBritrail(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                            }
                                            if (countS == 2)
                                            {
                                                insertsaver = true;
                                                countS = 0;
                                            }
                                        }
                                        PassSaleIdIn = Guid.Empty.ToString();
                                    }
                                }
                                #endregion
                            }
                            string dataid = "," + _oPrint.GetPassSaleIDByPrintQueueID(id).ToString();
                            PassSaleIdIn += dataid;
                            HoldCount++;
                            HoldPasssaleIDs += dataid;
                        }
                        #endregion
                    }

                    if ((countSaverMaxFive > 7) && (countMaxFivereset > 0) && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && !hdnSaver.Value.ToLower().Contains("yes") || rowcount == grdPrint.Rows.Count && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && (countSaverMaxFive > 7) && (countMaxFivereset > 0))
                    {
                        # region for xml coupan create
                        string tktXML2 = "";
                        PassSaleIdIn = PassSaleIdIn.Replace("00000000-0000-0000-0000-000000000000,", string.Empty);
                        var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                        foreach (var item in list2)
                        {
                            tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                        }
                        if (!string.IsNullOrEmpty(tktXML2))
                        {
                            XmlDataDocument xDoc = new XmlDataDocument();
                            xDoc.LoadXml(tktXML2);
                            XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, Passid, xDoc, 0, false);
                            XmlNode xnr = result.SelectSingleNode("/Coupons");
                            if (xnr != null)
                            {
                                foreach (XmlNode xn in xnr)
                                {
                                    countS++;
                                    if (countS == 2)
                                    {
                                        StockNumber = stockHold;
                                        Guid userid = AdminuserInfo.UserID;
                                        if (AgentuserInfo.UserID != new Guid())
                                            userid = AgentuserInfo.UserID;
                                        _oPrint.InsertStockInUsageBritrail(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, insertsaver);
                                        countS = 0;
                                    }
                                }
                            }
                            insertsaver = false;
                            oldorderId = PassSaleIdIn = string.Empty;
                            PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                            countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                        }
                        #endregion
                    }
                    if (Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && !hdnSaver.Value.ToLower().Contains("yes") && (countSaverMaxFive <= 7) || rowcount == grdPrint.Rows.Count && Passid != Guid.Empty && !string.IsNullOrEmpty(PassSaleIdIn) && (countSaverMaxFive <= 7))
                    {
                        # region for xml coupan create
                        string tktXML2 = "";
                        var list2 = _oPrint.GetRetrieveEurailProductXML(Passid, PassSaleIdIn);
                        foreach (var item in list2)
                        {
                            tktXML2 += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                        }
                        if (!string.IsNullOrEmpty(tktXML2))
                        {
                            XmlDataDocument xDoc = new XmlDataDocument();
                            xDoc.LoadXml(tktXML2);
                            XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, Passid, xDoc, 0, false);
                            XmlNode xnr = result.SelectSingleNode("/Coupons");
                            if (xnr != null)
                            {
                                foreach (XmlNode xn in xnr)
                                {
                                    Guid userid = AdminuserInfo.UserID;
                                    if (AgentuserInfo.UserID != new Guid())
                                        userid = AgentuserInfo.UserID;
                                    _oPrint.InsertStockInUsageBritrail(catId, queueId, PassPrintid, StockNumber, xn.OuterXml, userid, false);
                                    StockNumber++;
                                    PassPrintid = PassPrintidSecondSaver;
                                }
                            }
                        }
                        oldorderId = PassSaleIdIn = string.Empty;
                        PassPrintid = Passid = PassPrintidSecondSaver = Guid.Empty;
                        countMaxFivereset = countSaverMaxFive = StockNumber = 0; SecondSaver = true;
                        #endregion
                    }
                    /*if non saver and void then*/
                    if (hdnSaver.Value.ToLower().Contains("no"))
                    {
                        #region For Void and non saver pass
                        string tktXML = "";
                        Guid passSaleID = (Guid)_oPrint.GetPassSaleIDByPrintQueueID(id);
                        var list = _oPrint.GetRetrieveEurailProductXML(passSaleID, passSaleID.ToString());
                        foreach (var item in list)
                        {
                            tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                        }
                        if (!string.IsNullOrEmpty(tktXML))
                        {
                            XmlDataDocument xDoc = new XmlDataDocument();
                            xDoc.LoadXml(tktXML);
                            XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, passSaleID, xDoc, stNo, false);
                            XmlNode xnr = result.SelectSingleNode("/Coupons");
                            if (xnr != null)
                            {
                                foreach (XmlNode xn in xnr)
                                {
                                    Guid userid = AdminuserInfo.UserID;
                                    if (AgentuserInfo.UserID != new Guid())
                                        userid = AgentuserInfo.UserID;
                                    _oPrint.InsertStockInUsageBritrail(catId, queueId, id, stNo, xn.OuterXml, userid, false);
                                }
                            }
                        }
                        else
                        {
                            if (caseCount)
                            {
                                caseCount = false;
                                PrintQueueIdNew = Guid.NewGuid();
                                _oPrint.Insertqueueitems(PrintQueueIdNew, _oPrint.getPQdata(PQID));
                            }
                            string VOID = "<Coupon name=\"Pass\"><Element type=\"text\" name=\"void\" startcol=\"1\" endcol=\"72\" startrow=\"A\" endrow=\"R\" fontsize=\"37\" valign=\"middle\" align=\"middle\" fontface=\"Verdana\" fontbold=\"true\">VOID</Element></Coupon>";
                            _oPrint.InsertStockInUsageBritrail(catId, queueId, PrintQueueIdNew, stNo, VOID, AdminuserInfo.UserID, false);
                        }
                        #endregion
                    }
                    if (hdnSaver.Value.ToLower().Contains("free"))
                    {
                        #region For Free pass
                        string tktXML = "";
                        Guid passSaleID = (Guid)_oPrint.GetPassSaleIDByPrintQueueID(id);
                        var list = _oPrint.GetRetrieveEurailProductXML(passSaleID, passSaleID.ToString());
                        foreach (var item in list)
                        {
                            tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                        }
                        if (!string.IsNullOrEmpty(tktXML))
                        {
                            XmlDataDocument xDoc = new XmlDataDocument();
                            xDoc.LoadXml(tktXML);
                            XmlDocument result = BritrailBuildTemplate(hdnOrderNo.Value, passSaleID, xDoc, stNo, true);
                            XmlNode xnr = result.SelectSingleNode("/Coupons");
                            if (xnr != null)
                            {
                                foreach (XmlNode xn in xnr)
                                {
                                    Guid userid = AdminuserInfo.UserID;
                                    if (AgentuserInfo.UserID != new Guid())
                                        userid = AgentuserInfo.UserID;
                                    _oPrint.InsertStockInUsageBritrail(catId, queueId, id, stNo, xn.OuterXml, userid, false);
                                }
                            }
                        }
                        #endregion
                    }
                    if (rowcount == grdPrint.Rows.Count && HoldStockno != 0 && HoldCount > 7)
                    {
                        _oPrint.UpdateSaverTravellerXML(HoldStockno, HoldPasssaleIDs);
                        HoldStockno = 0;
                        HoldPasssaleIDs = string.Empty;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public XmlDocument BritrailBuildTemplate(string OrderNo, Guid PassSaleId, XmlDocument xDoc, Int32 StockNo, Boolean IsBritRailFreeDayPromo)
    {
        try
        {
            #region /**DECLARE FUNCATION VARIABLES**/
            Guid ProductID = Guid.Empty;
            Guid CurrId = Guid.Empty;
            Guid ProductId = Guid.Empty;
            int TravellerCat = 0;
            bool IsFlexi = false;
            string PrintingNote = string.Empty;
            string DaysIn = string.Empty;
            string SourceCode = string.Empty;
            string Cost = string.Empty;
            string ProductName = string.Empty;
            string Validday = string.Empty;
            string Validtype = string.Empty;
            string Validclass = string.Empty;
            string PassengerName = string.Empty;
            string PassengerCountry = string.Empty;
            string Passport = string.Empty;
            DateTime dateofissue = DateTime.Now;
            DateTime musstbeactivated = dateofissue.AddMonths(6);
            Passenger[] pas = new Passenger[0];
            XmlNode xnr;
            XmlDocument xTemplate = new XmlDocument();
            DateTime BritrailFreeDaypromoSD = DateTime.Now;
            DateTime BritrailFreeDaypromoED = DateTime.Now;
            if (Request.Url.ToString().ToLower().Contains("localhost"))
            {
                BritrailFreeDaypromoSD = Convert.ToDateTime("08/01/2015");
                BritrailFreeDaypromoED = Convert.ToDateTime("09/18/2015");
            }
            else
            {
                BritrailFreeDaypromoSD = Convert.ToDateTime("01/08/2015");
                BritrailFreeDaypromoED = Convert.ToDateTime("18/09/2015");
            }

            //bool IsBritRailFreeDayPromo = _oPrint.GetIsBritRailFreeDayPromoByPassSaleId(PassSaleId);
            #endregion

            XMLFileName = "Britrail.xml";
            xTemplate.Load(Server.MapPath(XMLFileName));

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/IsFlexi");
            if (xnr != null) IsFlexi = Convert.ToBoolean(xnr.InnerText);

            if (IsBritRailFreeDayPromo && (BritrailFreeDaypromoSD <= DateTime.Now && BritrailFreeDaypromoED >= DateTime.Now))
            {
                #region /**START BRITRAIL FREE DAY PROME PASS **/
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='PassFreeDayPromo']/Element[@name='lastvaliddate']");
                if (xnr != null) xnr.InnerText = "MUST BE VALIDATED BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CostCurrencyID");
                if (xnr != null) CurrId = Guid.Parse(xnr.InnerText);
                SourceCode = _oPrint.GetCurrencySourceCode(CurrId);
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ID");
                if (xnr != null) ProductId = Guid.Parse(xnr.InnerText);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='PassFreeDayPromo']/Element[@name='cost']");
                if (xnr != null) xnr.InnerText = "PRICE " + SourceCode + " ***.**";
                #endregion
                #region /**REMOVE FREE DAY PROMOTION PASSES**/
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                #endregion
            }
            else
            {
                #region IF ONLY BRITRAIL PASSES
                #region /**REMOVE FREE DAY PROMOTION PASSES**/
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='PassFreeDayPromo']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                #endregion
                #region /**START NORMAL PASS**/
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='OrderNo']");
                if (xnr != null) xnr.InnerText = OrderNo;
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ID");
                if (xnr != null)
                {
                    ProductID = Guid.Parse(xnr.InnerText);
                    PrintingNote = _oPrint.GetProductPrintingNote(ProductID);
                    DateTime PrdtValidityDate = _oPrint.GetProductValiditydate(ProductID);
                    if (musstbeactivated > PrdtValidityDate)
                    {
                        musstbeactivated = PrdtValidityDate;
                    }
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='dateofissue']");
                if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Name");
                if (xnr != null) ProductName = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='productname']");
                if (xnr != null) xnr.InnerText = ProductName.ToUpper();
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Vailidity");
                if (xnr != null) Validday = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validday']");
                if (xnr != null) xnr.InnerText = Validday;
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Class");
                if (xnr != null) Validclass = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validclass']");
                if (xnr != null) xnr.InnerText = Validclass;
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Traveller");
                if (xnr != null) Validtype = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='validtype']");
                if (xnr != null) xnr.InnerText = Validtype;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='mustbeactivated']");
                if (xnr != null) xnr.InnerText = "BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
                xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/NameBritrail");
                if (xnr != null) PassengerName = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PassengerName']");
                if (xnr != null) xnr.InnerText = PassengerName;
                xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/Country");
                if (xnr != null) PassengerCountry = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Countryofresidence']");
                if (xnr != null) xnr.InnerText = PassengerCountry;
                xnr = xDoc.SelectSingleNode("/Voucher/Passengers/Passenger/PassportNo");
                if (xnr != null) Passport = xnr.InnerText;
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
                if (xnr != null) xnr.InnerText = Passport;
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/DaysIn");
                if ((xnr != null) && (xnr.InnerText != "")) DaysIn = xnr.InnerText;
                if (IsFlexi)
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calendar']");
                    if (xnr != null)
                    {
                        //if (IsBritRailFreeDayPromo)
                        //    xnr.Attributes["numberofdays"].InnerXml = (string.IsNullOrEmpty(DaysIn) ? 0 : Convert.ToInt32(DaysIn) + 1).ToString();
                        //else

                        if (xnr != null) xnr.Attributes["numberofdays"].InnerXml = DaysIn.ToString();
                    }
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='DurationType']");
                    if (xnr != null) xnr.InnerText = "FLEXI";
                }
                else
                {
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='DurationType']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calendarMonth']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calendarDay']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='calendar']");
                    if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                }
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CostCurrencyID");
                if (xnr != null) CurrId = Guid.Parse(xnr.InnerText);
                SourceCode = _oPrint.GetCurrencySourceCode(CurrId);
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Cost");
                if (xnr != null) Cost = xnr.InnerText;
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ID");
                if (xnr != null) ProductId = Guid.Parse(xnr.InnerText);
                if (_oPrint.GetITXFlagByPassSaleId(PassSaleId))
                {
                    Cost = "***.**";
                }
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='cost']");
                if (xnr != null) xnr.InnerText = SourceCode + " " + Cost;

                #endregion  /**END NORMAL PASS**/
                #region /**START CONTROL VOUCHER**/

                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/TravellerCat");
                if ((xnr != null) && (xnr.InnerText != "")) TravellerCat = Convert.ToInt32(xnr.InnerText);
                if ((TravellerCat == 51) || (TravellerCat == 53) || (TravellerCat == 74) || (TravellerCat == 75))
                {
                    /*"SEE CONTROL VOUCHER"*/
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='PassengerName']");
                    if (xnr != null) xnr.InnerText = "SEE CONTROL VOUCHER";
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='Countryofresidence']");
                    if (xnr != null) xnr.InnerText = "SEE CONTROL VOUCHER";
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='passportno']");
                    if (xnr != null) xnr.InnerText = "SEE CONTROL VOUCHER";
                    /**END SEE CONTROL VOUCHER**/

                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='dateofissue']");
                    if (xnr != null) xnr.InnerText = String.Format("{0:dd MMM yyyy}", dateofissue);
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='productname']");
                    if (xnr != null) xnr.InnerText = ProductName.ToUpper();
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='validday']");
                    if (xnr != null) xnr.InnerText = Validday;
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='validclass']");
                    if (xnr != null) xnr.InnerText = Validclass;
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='validtype']");
                    if (xnr != null) xnr.InnerText = Validtype;
                    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='mustbeactivated']");
                    if (xnr != null) xnr.InnerText = "BEFORE " + String.Format("{0:dd MMM yyyy}", musstbeactivated);
                    xnr = xDoc.SelectSingleNode("/Voucher/Passengers");
                    if (xnr != null)
                    {
                        XmlNodeList nl = xnr.ChildNodes;
                        pas = new Passenger[nl.Count];
                        int counter = 0;
                        foreach (XmlNode xn in nl)
                        {
                            pas[counter].Name = xn["Name"].InnerText;
                            pas[counter].Country = xn["Country"].InnerText;
                            pas[counter].PassportNumber = xn["PassportNo"].InnerText;
                            counter++;
                        }
                    }
                    for (var counter = 0; counter < pas.Length; counter++)
                    {
                        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='PassengerName" + (counter + 1).ToString() + "']");
                        if (xnr != null) xnr.InnerText = pas[counter].Name;
                        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='Countryofresidence" + (counter + 1).ToString() + "']");
                        if (xnr != null) xnr.InnerText = pas[counter].Country;
                        xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Control']/Element[@name='passportno" + (counter + 1).ToString() + "']");
                        if (xnr != null) xnr.InnerText = pas[counter].PassportNumber;
                    }
                }
                #endregion /**END CONTROL VOUCHER**/
                #endregion
            }
            return xTemplate;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnProblem_Click(object sender, EventArgs e)
    {
        try
        {
            PrintingView.Visible = false;
            rangeAssign.Visible = true;
            UpdateOrderStatusByStatus(false, 3);//3:Approved;
            if (ViewState["FileURL"] != null)
            {
                try
                {
                    if (File.Exists(ViewState["FileURL"].ToString()))
                        File.Delete(ViewState["FileURL"].ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Printing File in used.");
                }
            }           
            List<long> stockno = new List<long>();
            foreach (GridViewRow row in grdPrint.Rows)
            {
                CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateBritrailStockNo(stno, 5, AgentuserInfo.UserID);//void
            CleareprintingQueue(4);//4.Queued tblprintqueueitems update

            btnNotVoidOk.Visible = true;
            btnProblem.Visible = btnPrintedOK.Visible = btnPrint.Visible = btnCancel.Visible = false;

            lblHeader.Text = "Void Stock";
            Msgtxt.InnerText = "Please select (tick) the stock numbers that are still usable/undamaged and press 'OK' to attempt printing again on the same stock. If the stock is unusable and cannot be used again please untick all of the stock numbers on that sheet and click 'OK' to void it.";
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Showprinting", "Showprinting()", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void UpdateOrderStatusByStatus(bool CaseValid, int status)
    {
        try
        {//1:Order Created;3:Approved;6:Cancelled;7:Completed;8:Refund Pending;9:Refund Complete;19:Confirm;20:Partial refund
            foreach (GridViewRow row in grdPrint.Rows)
            {
                HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
                if (CaseValid)
                {
                    if (MBooking.AllPassPrintedForThisOrder(Convert.ToInt64(hdnOrderNo.Value)))
                        MBooking.UpdateOrderStatus(status, Convert.ToInt64(hdnOrderNo.Value));
                }
                else
                    MBooking.UpdateOrderStatus(status, Convert.ToInt64(hdnOrderNo.Value));
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        List<long> stockno = new List<long>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
        }
        foreach (var stno in stockno)
            _oPrint.UpdateBritrailStockNoReuse(stno);
        CleareprintingQueue(4);//4.Queued tblprintqueueitems update
        Response.Redirect("PrintingOrders");
    }

    void UpdateBritrailStockNumberStatus(int status)
    {
        foreach (GridViewRow row in grdPrint.Rows)
        {
            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            Int32 OrderNo = Convert.ToInt32(hdnOrderNo.Value);
            if (OrderNo == 0 && status != 3)//void
                _oPrint.UpdateBritrailStockNo(stNo, 5, userid);
            else
                _oPrint.UpdateBritrailStockNo(stNo, status, userid);
        }
    }
    
    protected void btnNotVoidOk_Click(object sender, EventArgs e)
    {
        bool thisisVoid = false;
        List<long> stockno = new List<long>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
            if (!chkunused.Checked)
                thisisVoid = true;
        }
        if (thisisVoid)
        {
            foreach (var stno in stockno)
                _oPrint.UpdateBritrailStockNo(stno, 5, AgentuserInfo.UserID);
        }
        else
        {
            foreach (var stno in stockno)
            {
                //_oPrint.DeleteBritrailStockNo(stno);
                _oPrint.UpdateBritrailStockNoReuse(stno);
            }
        }
        CleareprintingQueue(4);//4.Queued tblprintqueueitems update
        Response.Redirect("PrintingOrders");
    }

    protected void btnPrintedOK_Click(object sender, EventArgs e)
    {
        string URlPath = string.Empty;
        if (ViewState["url"] != null)
            URlPath = ViewState["url"].ToString();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            _oPrint.UpdatePrintingURLBritrail(stNo, URlPath);
            break;
        }
        Response.Redirect("PrintingOrders");
    }

    public void CleareprintingQueue(int STATUS)
    {
        List<Guid> ids = new List<Guid>();
        foreach (GridViewRow row in grdPrint.Rows)
        {
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            Guid id = Guid.Parse(hdnID.Value);
            ids.Add(id);
        }
        foreach (var id in ids)
            _oPrint.UpdatePrintQConfirmedStatus(id, STATUS);
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void grdPrint_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblHideClass = e.Row.FindControl("lblHideClass") as Label;
            if (!string.IsNullOrEmpty(lblHideClass.Text))
            {
                e.Row.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
            }
        }
    }

    struct Passenger
    {
        public string Name;
        public string Country;
        public string PassportNumber;
    }
}