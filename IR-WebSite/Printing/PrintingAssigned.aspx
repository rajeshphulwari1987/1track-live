﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PrintingAssigned.aspx.cs" Inherits="Printing_PrintingAssigned" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src='../Scripts/jquery-1.9.0.min.js'></script>
    <style type="text/css">
        #MainContent_grdPrint tbody tr td
        {
            line-height: 27px;
        }
        #Msgtxt
        {
            text-align:justify;
            width: 850px;
            font-size:13px !important;
        }
        input[type="checkbox"]
        {
            display: block !important;
        }
    </style>
     
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <section class="content"> 
    <h1>Print Queue - <asp:Label ID="lblHeader" ClientIDMode="Static" runat="server" Text="Printing"></asp:Label></h1>
    <p id="Msgtxt" runat="server" clientidmode="Static">Please check the stock allocations below. If the stock allocation is incorrect please contact your administrator.</p>
    <div class="clear"> &nbsp;</div>
        <asp:Panel ID="pnlErrSuccess" runat="server" Width="85%">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <div style="float:left;width:848px">
    <div id="rangeAssign" runat="server" class="privacy-block-outer pass" style="margin-top: 10px;">
    <div class="red-title">
        Print Queue Assignment</div>
    <div class="discription-block">
        <div class="clsdivPrduct">
            <div class="clsPrd"><strong>Range From : </strong> <asp:Label ID="lblRangeFrm" runat="server" /></div>
            <div class="clsPrd"><strong>Range To : </strong> <asp:Label ID="lblRangeTo" runat="server" /></div>  
        </div>
    </div>
    </div>
    <div id="PrintingView" visible="false" runat="server" style="border: 1px solid #ccc;padding: 3px;margin: 20px 0;">    

    <iframe id="Printingiframe" runat="server" height="500px" width="100%"></iframe>
     
    </div>
    <div id="dvPrd" class="grdBrd" runat="server" Visible="False">
        <div class="clsdivPrduct"> 
        <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
        <asp:GridView ID="grdPrint" runat="server" AutoGenerateColumns="False" PageSize="10"  
            CssClass="grid-head2 clsPrintColor" CellPadding="4" Width="100%"
                OnRowDataBound="grdPrint_RowDataBound" >
            <AlternatingRowStyle CssClass="clsPrintingGrid"/>
            <PagerStyle CssClass="paging"></PagerStyle> 
            <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC"/>
            <HeaderStyle CssClass="clsPrintingHeader" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <EmptyDataTemplate>
                Record not found.
            </EmptyDataTemplate>
            <Columns>
           <asp:TemplateField HeaderText="Order"> 
                    <ItemTemplate>
                        <%#Eval("ProductName").ToString() != "VOID" ? Eval("OrderNo") : ""%>
                        <asp:HiddenField ID="hdnSaver" runat="server" Value='<%#Eval("SaverCode")%>'/>
                        <asp:HiddenField ID="hdnPQItemID" runat="server" Value='<%#Eval("ID")%>'/>
                        <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryID")%>'/>
                        <asp:HiddenField ID="hdnOrderNo" runat="server" Value='<%#Eval("ProductName").ToString()!="VOID" ? Eval("OrderNo") : "0"%>'/>
                    </ItemTemplate>
               <ItemStyle Width="10%"/> 
               <HeaderStyle CssClass="clsPrintColor"/>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lead Passenger">
                    <ItemTemplate>
                      <%#Eval("LeadPassenger")%>
                    </ItemTemplate>
                    <ItemStyle Width="30%"/>
                   <HeaderStyle CssClass="clsPrintColor"/>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <%#Eval("ProductName")%>
                    </ItemTemplate>
                    <ItemStyle Width="40%"></ItemStyle>
                    <HeaderStyle CssClass="clsPrintColor"/>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Stock No.">
                    <ItemTemplate>
                     <asp:Label ID="lblStockNo" runat ="server" Text='<%#Eval("StockNo")%>'/>
                     <asp:Label ID="lblHideClass" runat ="server" Text='<%#Eval("HideClass") %>' style="display:none;"/>
                    </ItemTemplate>
                    <ItemStyle Width="10%" HorizontalAlign="Center"/>
                    <HeaderStyle Width="10%" HorizontalAlign="Center" CssClass="clsPrintColor"/>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Usable" >
                    <ItemTemplate>
                        <asp:CheckBox ID="chkPrint" runat ="server" Checked="true"/>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" CssClass="Showprinting"/>
                    <ItemStyle Width="10%" HorizontalAlign="Center" CssClass="Showprinting" ></ItemStyle>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>    
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        <div style="clear:both;"></div>
        <div style="line-height:8px">
            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn-red-cart f-right" Width="80px" style="margin:0 5px 0 5px" onclick="btnPrint_Click"  /> 
            <asp:Button ID="btnCancel" ClientIDMode="Static" runat="server" Text="Cancel" CssClass="btn-red-cart f-right" Width="80px" onclick="btnCancel_Click" style="margin:0 5px 0 5px"/>
            <asp:Button ID="btnPrintedOK" runat="server" Text="All passes printed OK" ClientIDMode="Static" CssClass="btn-red-cart f-right" Width="180px" style="margin:0 5px 0 5px" onclick="btnPrintedOK_Click" /> 
            <asp:Button ID="btnProblem" runat="server" Text="There was a problem" ClientIDMode="Static" CssClass="btn-red-cart f-right" Width="180px" style="margin:0 5px 0 5px" onclick="btnProblem_Click" /> 
            <asp:Button ID="btnNotVoidOk" runat="server" Text="OK" ClientIDMode="Static" CssClass="btn-red-cart f-right" Width="100px" style="margin:0 5px 0 5px" OnClick="btnNotVoidOk_Click"/>
        </div>
    </div>
 </div>
</section>
    <script type="text/javascript">
        $('.Showprinting').hide();
        function Showprinting() {
            $('.Showprinting').show();
        }
    </script>
</asp:Content>
