﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="GeneralInformation.aspx.cs" Inherits="GeneralInformation" %>

<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#rptMainMenu_HyperLink1_1").addClass("active");
        });
    </script>
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsAbs
        {
            display: none;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".clsEmail").keyup(function () {
                document.getElementById("divNews").style.display = 'block';
            });
        });
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%--Banner section--%>
    <div class="banner">
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%=adminSiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content">
      <div class="left-content">
      <h1 runat="server" id="headerTitle"></h1>
      <div style="float:right;width:236px;padding-left:10px;">
         <img runat="server" id="imgGinfo" />
      </div>
      <div runat="server" id="divDescription" style="padding:3px;font-size:13px;">
      </div>
         <div class="country-block-outer" id="divFooter" runat="server">
            <div class="country-block-inner row">
               <div id="footerBlock" runat="server"></div>
            </div>
         </div>
      </div>
<div class="right-content">
<div id="divJRF" runat="server">            
    <a runat="server" id="trainresults">
    <div id="rtPannel" runat="server"></div> </a>
    <img id="imgRtPanel" runat="server" src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />
</div>
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
