﻿using System;
using Business;
using System.Linq;
using System.Web;
using System.Configuration;

public partial class PrivacyPolicy : System.Web.UI.Page
{
    readonly ManageBooking _master = new ManageBooking();
    private Guid _siteId;
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Page.RouteData.Values["PageId"] != null)
                BindPrivacyPolicy(_siteId);
        }
    }

    public void BindPrivacyPolicy(Guid siteId)
    {
        var result = _master.GetPrivacyBySiteid(siteId).Where(x => x.IsActive == true);
        rptPriv.DataSource = result;
        rptPriv.DataBind();
    }
}