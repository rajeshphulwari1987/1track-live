﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;
using System.Web.Services;

[System.Web.Script.Services.ScriptService]
public partial class BookingCart : Page
{
    #region Global Variables
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    readonly ManageAdminFee ManageAdmfee = new ManageAdminFee();
    readonly ManageUser _ManageUser = new ManageUser();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    ManageTrainDetails _master = new ManageTrainDetails();
    ManageBooking _masterBooking = new ManageBooking();
    public static string currency = "$";
    public static Guid currencyID = new Guid();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid pageID, siteId;
    public string script = "<script></script>";
    public string siteURL;
    Boolean isShippingAllow = false;
    public long OrderID = 0;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["OrderID"] != null)
            OrderID = Convert.ToInt64(Session["OrderID"]);
        OrderDiscount.Visible = new ManageOrder().GetOrderDiscountVisibleBySiteId(siteId);
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "restirctCopy", "restrictCopy();", true);
        txtZip.Attributes.Add("onkeypress", "return keycheck()");
        ShowMessage(0, string.Empty);
        if (!IsPostBack)
        {
            Session.Remove("P2POrderID");
            BindPageMandatoryFields();
            GetCurrencyCode();
            GetTheme(siteId);

            if (Session["AgentUserID"] != null)
            {
                var IDuser = Guid.Parse(Session["AgentUserID"].ToString());
                var Agentlist = _ManageUser.AgentDetailsById(IDuser);
                var AgentNameAndEmail = _ManageUser.AgentNameEmailById(IDuser);
                if (Agentlist != null)
                {
                    ddlMr.SelectedValue = Convert.ToInt32(AgentNameAndEmail.Salutation).ToString();
                    txtFirst.Text = AgentNameAndEmail.Forename;
                    txtLast.Text = AgentNameAndEmail.Surname;
                    txtEmail.Text = AgentNameAndEmail.EmailAddress;
                    txtConfirmEmail.Text = AgentNameAndEmail.EmailAddress;
                    txtBillPhone.Text = Agentlist.Telephone;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;

                    txtshpfname.Text = Agentlist.FirstName;
                    txtshpLast.Text = Agentlist.LastName;
                    txtshpEmail.Text = Agentlist.Email;
                    txtshpConfirmEmail.Text = Agentlist.Email;
                    txtShpPhone.Text = Agentlist.Telephone;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;
                }
            }
            Session["BOOKING-REPLY"] = null;
            PageLoadEvent();
            QubitOperationLoad();
            GetAdminFeeData();
        }
    }

    public void GetAdminFeeData()
    {
        try 
        {
            hdnAdminFee.Value = ManageAdmfee.GetAdminFeeBySiteID(siteId);
            if (string.IsNullOrEmpty(hdnAdminFee.Value))
                pnladminfee.Attributes.Add("style", "display:none");
            else
                pnladminfee.Attributes.Add("style", "display:block");
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void BindPageMandatoryFields()
    {
        try
        {
            if (Page.RouteData.Values["PageId"] == null)
                return;

            pageID = (Guid)Page.RouteData.Values["PageId"];
            var list = _masterPage.GetMandatoryVal(siteId, pageID);

            foreach (var item in list)
            {
                if (item.ControlField.Trim() == "rfFirst")
                    BookingpassrfFirst.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfLast")
                    BookingpassrfLast.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfEmail")
                    BookingpassrfEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfvEmail2")
                    BookingpassrfvEmail2.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfPhone")
                    BookingpassrfPhone.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfAdd")
                    BookingpassrfAdd.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfZip")
                    BookingpassrfZip.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfCountry")
                    BookingpassrfCountry.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfDateOfDepature")
                    BookingpassrfDateOfDepature.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipFirstName")
                    BookingpassrfshipFirstName.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipLastName")
                    BookingpassrfshipLastName.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipEmail")
                    BookingpassrfshipEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfconfirmShipEmail")
                    BookingpassrfconfirmShipEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpPhone")
                    BookingpassrfshpPhone.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpAdd")
                    BookingpassrfshpAdd.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpZip")
                    BookingpassrfshpZip.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpCountry")
                    BookingpassrfshpCountry.ValidationGroup = item.ValGrp.Trim();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetTheme(Guid SiteId)
    {
        var blueThemeID = Guid.Parse("4FC1F398-5901-439B-A1E0-27B1CBE2BBEB");
        var theme = _db.tblSiteThemes.FirstOrDefault(x => x.SiteID == SiteId);
        if (theme != null)
            if (lnkBookStyle != null)
            {
                if (theme.ThemeID == blueThemeID)
                {
                    var css = "Styles/" + theme.CssFolderName + "/BookingCart.css";
                    lnkBookStyle.Attributes.Add("href", css);
                }
            }
    }

    public void QubitOperationLoad()
    {
        if (Session["siteId"] != null)
        {
            var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            if (lstQbit != null && lstQbit.Count() > 0)
            {
                var res = lstQbit.FirstOrDefault();
                if (res != null)
                    script = res.Script;
            }
        }
    }

    void PageLoadEvent()
    {
        ddlCountry.DataSource = _master.GetCountryDetail();
        ddlCountry.DataValueField = "CountryID";
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        ddlshpCountry.DataSource = _master.GetCountryDetail();
        ddlshpCountry.DataValueField = "CountryID";
        ddlshpCountry.DataTextField = "CountryName";
        ddlshpCountry.DataBind();
        ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        if (Session["siteId"] != null && OrderID != 0)
            _masterBooking.UpdateSiteToOrder(OrderID, Guid.Parse(Session["siteId"].ToString()));

        //AddItemInShoppingCart();
        GetCurrencyCode();
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            string countryID = cookie.Values["_cuntryId"];
            ddlCountry.SelectedValue = countryID;
            ddlshpCountry.SelectedValue = countryID;
        }
        PrepouplateSeniorAndAdult();
        FillShippingData();
    }

    public void FillShippingData()
    {
        if (Session["RailPassData"] != null)
        {
            List<getRailPassData> listPass = Session["RailPassData"] as List<getRailPassData>;
            foreach (var item in listPass)
            {
                Guid id = Guid.Parse(item.PrdtId);
                isShippingAllow = _db.tblProducts.Any(x => x.IsShippingAplicable && x.ID == id);
                if (isShippingAllow)
                    break;
            }
        }
        pnlShipping.Visible = isShippingAllow;
        if (ddlCountry.SelectedValue != "0" && isShippingAllow)
        {
            var countryid = Guid.Parse(ddlCountry.SelectedValue);
            var lstShip = _masterBooking.getAllPassShippingDetail(siteId, countryid);
            if (lstShip.Any())
                rptShippings.DataSource = lstShip;
            else
            {
                var lstDefaultShip = _masterBooking.getDefaultShippingDetail(siteId);
                if (lstDefaultShip.Any())
                    rptShippings.DataSource = lstDefaultShip;
                else
                    rptShippings.DataSource = null;
            }

            rptShippings.DataBind();
        }
    }

    void PrepouplateSeniorAndAdult()
    {
        if (Session["RailPassData"] != null)
        {
            var list = Session["RailPassData"] as List<getRailPassData>;
            decimal TotalSalePrice = list.Sum(t => Convert.ToDecimal(t.Price));
            hdntotalSaleamount.Value = TotalSalePrice.ToString("F2");
            var lst = _masterBooking.GetBooingFees(TotalSalePrice, Guid.Parse(Session["siteId"].ToString()));
            if (lst.Any())
            {
                decimal amountAfterConv = Convert.ToDecimal(lst.FirstOrDefault().BookingFee) * Convert.ToDecimal(lst.FirstOrDefault().convRate);
                lblBookingFee.Text = amountAfterConv.ToString("F2");
            }
            decimal totalAmount = TotalSalePrice + Convert.ToDecimal(lblBookingFee.Text);
            lblTotalCost.Text = totalAmount.ToString("F2");
        }
    }

    public void bindUsersession(string Email, Guid ID, string FirstName, string LastName, Guid SiteId, string Password)
    {
        Session.Remove("USERUsername");
        Session.Remove("USERRoleId");
        Session.Remove("USERUserID");
        Session.Remove("USERSiteID");
        USERuserInfo.UserEmail = Email;
        USERuserInfo.ID = ID;
        USERuserInfo.Username = FirstName;
        USERuserInfo.SiteID = SiteId;
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    [WebMethod]
    public static bool chk_Discount(string DiscountCode)
    {
        List<Guid> Productlist = new List<Guid>();
        List<Guid> Categorylist = new List<Guid>();
        int OrderID = 0;
        bool result = false;
        if (HttpContext.Current.Session["OrderID"] != null)
            OrderID = Convert.ToInt32(HttpContext.Current.Session["OrderID"]);
        if (OrderID == 0)
        {
            if (HttpContext.Current.Session["RailPassData"] != null)
            {
                List<getRailPassData> listPass = HttpContext.Current.Session["RailPassData"] as List<getRailPassData>;
                if (listPass != null && listPass.Count > 0){
                    Productlist = listPass.Select(t => Guid.Parse(t.PrdtId)).ToList();
                    Categorylist = listPass.Select(t => Guid.Parse(t.CategoryID)).ToList();
                }
            }
        }
        ManageBooking _masterBookingd = new ManageBooking();
        Guid SiteId = Guid.Empty;
        if (HttpContext.Current.Session["siteId"] != null)
            SiteId = Guid.Parse(HttpContext.Current.Session["siteId"].ToString());
        //if (OrderID == 0)
        //    result = _masterBookingd.ValidDiscountCode(DiscountCode, SiteId, Productlist, Categorylist);
        //else
        //    result = _masterBookingd.ValidDiscountCode(DiscountCode, SiteId, OrderID);
        return result;
    }

    [WebMethod]
    public static decimal chk_CheckedChanged(decimal TotalSalePrice)
    {
        ManageBooking _masterBookingx = new ManageBooking();
        Guid SiteId = Guid.Empty; decimal amountAfterConv = 0;
        if (HttpContext.Current.Session["siteId"] != null)
            SiteId = Guid.Parse(HttpContext.Current.Session["siteId"].ToString());
        var lst = _masterBookingx.GetBooingFees(TotalSalePrice, SiteId);
        if (lst.Any())
            amountAfterConv = Convert.ToDecimal(lst.FirstOrDefault().BookingFee) * Convert.ToDecimal(lst.FirstOrDefault().convRate);
        return amountAfterConv;
    }

    [WebMethod]
    public static string del_pass(Guid Id)
    {
        //ManageBooking _masterBookingx = new ManageBooking();
        //var list = new List<getRailPassData>();
        //if (HttpContext.Current.Session["RailPassData"] != null)
        //{
        //    list = HttpContext.Current.Session["RailPassData"] as List<getRailPassData>;
        //}
        //var objRPD = list.FirstOrDefault(a => a.Id == Id);
        //int count = 0;
        //if (objRPD != null && objRPD.TravellerName.ToLower().Contains("saver"))
        //{
        //    var newList = (from a in list
        //                   where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName && a.TravellerName.ToLower().Contains("saver")
        //                   select a).ToList();
        //    count = newList.Count();
        //    if (count > 2)
        //    {
        //        list.RemoveAll(ty => ty.Id == Id);
        //        _masterBookingx.DelpasssaleData(Id);
        //        HttpContext.Current.Session.Add("RailPassData", list);
        //    }
        //    else
        //    {
        //        foreach (getRailPassData a in newList)
        //        {
        //            _masterBookingx.DelpasssaleData(a.Id);
        //            list.RemoveAll(ty => ty.Id == a.Id);
        //        }
        //        HttpContext.Current.Session.Add("RailPassData", list);
        //    }
        //}
        //else
        //{
        //    list.RemoveAll(ty => ty.Id == Id);
        //    _masterBookingx.DelpasssaleData(Id);
        //    HttpContext.Current.Session.Add("RailPassData", list);
        //}
        //if (list.Count > 0)
        //    return "BookingCart";
        //else
        //{
        //    HttpContext.Current.Session["RailPassData"] = null;
        //    return "rail-passes";
        //}
        return "";
    }

    public decimal getsitetickprotection()
    {
        try
        {
            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
            {
                //divpopupdata.InnerHtml = list.Description;
                return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
            }
            else
                return 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            bool dchk = UcPassDetail.btnChkOut_Click();
            if (!dchk)
                return;

            if (Session["OrderID"] != null)
                OrderID = Convert.ToInt64(Session["OrderID"]);

            #region Agent login and Guest login
            if (Session["USERUserID"] == null && Session["AgentUsername"] == null)
            {
                var UserID = Guid.NewGuid();
                var listUser = _ManageUser.CheckEmailUser(txtEmail.Text);
                if (listUser != null && listUser.IsActive == true)
                {
                    //Login USER User Information
                    bindUsersession(listUser.Email, listUser.ID, listUser.FirstName, listUser.LastName, listUser.SiteId, listUser.Password);
                    UserID = listUser.ID;
                    if (OrderID != 0)
                    {
                        _masterBooking.SetorderUserid(OrderID, UserID);
                    }
                }
                else
                {
                    //add Login USER User Information
                    if (Session["USERUserID"] != null)//For Gust Only
                    {
                        UserID = Guid.Parse(Session["USERUserID"].ToString());
                    }
                    string password = Membership.GeneratePassword(10, 3);
                    bool result = _masterBooking.AddLoginUSer(new tblUserLogin
                    {
                        ID = UserID,
                        FirstName = txtFirst.Text,
                        LastName = txtLast.Text,
                        Email = txtEmail.Text,
                        Password = password,
                        Country = Guid.Parse(ddlCountry.SelectedValue),
                        SiteId = siteId,
                        IsActive = true
                    });
                    if (result == false)
                        bindUsersession(txtEmail.Text, UserID, txtFirst.Text, txtLast.Text, siteId, password);
                }
            }
            #endregion
            Session["ShipMethod"] = hdnShipMethod.Value.Trim();
            Session["ShipDesc"] = hdnShipDesc.Value.Trim();

            int postcodelen = txtZip.Text.Trim().Replace(" ", "").Length;
            lblpmsg.Visible = postcodelen > 7;
            if (postcodelen > 7)
                return;
            AddPassBookingInLocalDB();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void AddPassBookingInLocalDB()
    {
        ManageAdmfee.UpdateOrderAdminFeeByOrderID(OrderID, Convert.ToDecimal(hdnAdminFeeAmount.Value));
        var objBillingAddress = new tblOrderBillingAddress();
        objBillingAddress.ID = Guid.NewGuid();
        objBillingAddress.OrderID = OrderID;
        objBillingAddress.Title = ddlMr.SelectedItem.Text;
        objBillingAddress.FirstName = txtFirst.Text;
        objBillingAddress.LastName = txtLast.Text;
        objBillingAddress.Phone = txtBillPhone.Text;
        objBillingAddress.Address1 = txtAdd.Text;
        objBillingAddress.Address2 = txtAdd2.Text;
        objBillingAddress.EmailAddress = txtEmail.Text;
        objBillingAddress.City = txtCity.Text;
        objBillingAddress.State = txtState.Text;
        objBillingAddress.Country = ddlCountry.SelectedItem.Text;
        objBillingAddress.Postcode = txtZip.Text;

        if (!chkShippingfill.Checked)
        {
            objBillingAddress.TitleShpg = ddlMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtFirst.Text;
            objBillingAddress.LastNameShpg = txtLast.Text;
            objBillingAddress.EmailAddressShpg = txtEmail.Text;
            objBillingAddress.PhoneShpg = txtBillPhone.Text;
            objBillingAddress.Address1Shpg = txtAdd.Text;
            objBillingAddress.Address2Shpg = txtAdd2.Text;
            objBillingAddress.CityShpg = txtCity.Text;
            objBillingAddress.StateShpg = txtState.Text;
            objBillingAddress.CountryShpg = ddlCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtZip.Text;
        }
        else
        {
            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;
        }
        if (txtDateOfDepature.Text.Trim() != "DD/MM/YYYY")
            _masterBooking.UpdateDepatureDate(OrderID, Convert.ToDateTime(txtDateOfDepature.Text));

        _masterBooking.AddOrderBillingAddress(objBillingAddress);

        if (!string.IsNullOrEmpty(hdnShipMethod.Value))
            _masterBooking.FillShippingDefaultData(OrderID, hdnShipMethod.Value);

        _masterBooking.UpdateOrderBookingFee(Convert.ToDecimal(lblBookingFee.Text), OrderID);
        if (txtDiscountCode.Text.Length > 0)
            _masterBooking.AddOrderDiscountData(txtDiscountCode.Text, siteId, OrderID, Convert.ToDecimal(HdnTotalCost.Value));

        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (Session["AgentUsername"] == null && result != null && (result.IsWholeSale || (bool)result.IsAgent))
        {
            Session["redirectpage"] = "";
            Response.Redirect("~/Agent/login", false);
        }
        else 
            Response.Redirect("~/PaymentProcess", false);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        var detaillst = new List<UserControls_passdetalis.getpreviousShoppingData>();
        detaillst = UcPassDetail.HoldDataDetail();
        Session.Add("DetailRailPass", detaillst);
        Response.Redirect("rail-passes");
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillShippingData();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "customselect", "customselect()", true);
    }
}