﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;

public partial class ContactUs : Page
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private Guid _siteId;
    public string script;
    public string toMail;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Page.RouteData.Values["PageId"] != null)
            {
                var pageID = (Guid)Page.RouteData.Values["PageId"];
                PageContent(pageID, _siteId);
            }
        }
    }

    public void PageContent(Guid pageID, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _master.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList =
                    (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                if (list.Count > 0)
                {
                    rptBanner.DataSource = list;
                    rptBanner.DataBind();
                }
                else
                    dvBanner.Visible = false;

                ContentHead.InnerHtml = oPage.PageHeading;
                ContentText.InnerHtml = oPage.PageContent;
                ContactPanel.InnerHtml = oPage.ContactPanel.Replace("CMSImages", SiteUrl + "CMSImages");
                db_1TrackEntities db = new db_1TrackEntities();
                bool IsStaSite = db.tblSites.Any(x => x.ID == siteId && x.IsSTA == true);
                if (IsStaSite)
                {
                    callBlock.Visible = false;
                    divSta.Style.Add("width", "100%");

                }
                else
                    callBlock.InnerHtml = oPage.ContactCall.Replace("CMSImages", SiteUrl + "CMSImages");

            }
            else
            {
                callBlock.Visible = false;
                dvBanner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            var enableEmail = _master.EnableContactEmail(_siteId);
            if (enableEmail)
            {
                if (String.IsNullOrEmpty(hdnEmail.Value))
                    hdnEmail.Value = _db.tblSites.FirstOrDefault(x => x.ID == _siteId).JourneyEmail;

                SendContactEmail(_siteId);
            }
            else
            {
                _master.AddContact(new tblContact
                    {
                        ID = Guid.NewGuid(),
                        SiteID = _siteId,
                        Name = txtName.Text.Trim(),
                        Email = txtEmail.Text.Trim(),
                        Phone = txtPhn.Text.Trim(),
                        OrderNumber = txtOrderNo.Text.Trim(),
                        Description = txtDesp.Text.Trim()
                    });
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('Your comment/query has been sent successfully.')", true);
            }
            ClearData();
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void SendContactEmail(Guid siteid)
    {
        try
        {
            const string Subject = "User Comment/Query";
            var body = "<html><head><title></title></head><body><p> User Comment/Query " +
                          "<br /> <br /> User has send comment/query. <br />" +
                          "<br /> User Name : " + hdnName.Value + "<br />	Email : " + hdnFrmEmail.Value + "" +
                          "<br /> Phone : " + hdnPhn.Value + "<br /> Order Number : " + hdnOrderNo.Value +
                          "<br/> Comment/Query : " + hdnDesp.Value.Trim() +
                          "<br /><br />Thanks </p></body></html>";

            if (!String.IsNullOrEmpty(hdnEmail.Value))
            {
                var sendEmail = _master.SendContactUsMail(siteid, Subject, body, hdnFrmEmail.Value, hdnEmail.Value);
                if (sendEmail)
                {
                    divSta.Visible = false;
                    divLanding.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
    private void ClearData()
    {
        txtName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhn.Text = string.Empty;
        txtOrderNo.Text = string.Empty;
        txtDesp.Text = string.Empty;
    }
}