﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Security.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="Security" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Security </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Security</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
    <div class="left-content">
    <h1><asp:Label ID="lblTitle" runat="server"/></h1>
    <p>
        <asp:Label ID="lblDesp" runat="server"/>
        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Description"))%>
    </p>
    </div>

<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
