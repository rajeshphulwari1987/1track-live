﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="HighSpeedPass.aspx.cs" Inherits="HighSpeedPass" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .input
        {
            text-transform: capitalize;
        }
        .pass .hd-title
        {
            -moz-box-sizing: border-box;
            border-radius: 0 0 5px 5px;
            height: auto !important;
            line-height: 20px;
            width: 100%;
            padding-right: 10%;
        }
        
        .valid
        {
            float: left;
        }
        .valid3
        {
            color: red;
        }
        .valid2
        {
            color: red;
            position: absolute;
            top: 33px;
            margin-left: 7px;
        }
        .pass-coloum-forth{
            width: 200px;
        }
        .pass-coloum-three{ width: 60px;}
    </style>
    <script type="text/javascript">
        function hide() { $('.discription-block').hide(); }
        function show() { $('.discription-block').show(); }

        $(document).ready(function () {
            show();
            $('.imgOpen').click(function () {
                if ("images/arr-down.png" == $(this).attr('src').toString()) {
                    $(this).attr('src', 'images/arr-right.png');
                }
                else {
                    $(this).attr('src', 'images/arr-down.png');
                }
                $(this).parent().parent().parent().find('.discription-block').slideToggle();
            });
            $(".calDate").attr("readonly", "true");

            $(".imgCal").click(function () {
                $("#MainContent_txtStartDate").datepicker('show');
            });

            $(".imgCal1").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked')) {
                    $("#MainContent_txtReturnDate").datepicker('show');
                }
            });
        });

        function chkVal(id, e) {
            if (e == 0)
                e = 3;

            // var txtStartDateId = '#' + ($(id).find('input:first-child').attr('id'));
            var txtStartDateId = '#' + $(id).find('input[type=text]').attr('id');

            $(txtStartDateId).datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+' + e + 'm'
            });
            $(txtStartDateId).datepicker('show');
        }

        function btnclick() {
            $(".chkvalid").each(function () {
                if (($(this).val() == '') || ($(this).val() == 'DD/MM/YYYY'))
                    $(this).css("background-color", "#FCCFCF");
                else {
                    $(this).css("background-color", "#FFF");
                }
            });
        };

        function chkerrorvali(e) {
            if ($.trim($(e).val()) != '' || $.trim($(e).val()) != 'DD/MM/YYYY')
                $(e).css({ "background-Color": "#FFF" });
            else
                $(e).css({ "background-Color": "#FCCFCF" });
        }
        function showmessagecountry(e) {
            alert("Country not valid for this pass.");
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="breadcrumb" style="margin-top: 15px;">
        <a href="#">Home </a>>> High Speed Pass Detail
    </div>
    <div class="left-content" style="width: 100%">
        <h1>
            High Speed Pass Detail
        </h1>
        <p>
            Venenatis per interdum riduculus ligula placerat elit donec cammando dui. Prasent
            tortor aliquam urna Curabitur erat penatibus vel pede suscipit penatibus sadales
            per lociania risus elementum cras orci sapien
        </p>
        <p>
            &nbsp;
        </p>
        <div class="clear">
            &nbsp;</div>
        <asp:Repeater ID="rptPassDetail" runat="server" OnItemCommand="rptPassDetail_ItemCommand"
            OnItemDataBound="rptPassDetail_ItemDataBound">
            <ItemTemplate>
                <div class="privacy-block-outer pass">
                    <div class="red-title">
                        Pass&nbsp;<%# Container.ItemIndex + 1 %></div>
                    <div class="hd-title">
                        <asp:HiddenField ID="hdnPassSale" runat="server" Value='<%#Eval("PrdtId")+","+Eval("Price")+","+Eval("TravellerID")+","+Eval("ClassID")+","+Eval("ValidityID")+","+Eval("CategoryID")+","+Eval("commission")+","+Eval("MarkUp")+","+Eval("CountryStartCode")+","+Eval("CountryEndCode")+","+Eval("CountryLevelIDs")%>' />
                        <asp:HiddenField ID="HdnOriginalPrice" runat="server" Value='<%#Eval("OriginalPrice")%>' />
                        <asp:HiddenField ID="hdnPrdtId" runat="server" Value='<%#Eval("PrdtId")%>' />
                        <asp:HiddenField ID="hdnSalePrice" runat="server" Value='<%#Eval("SalePrice")%>' />
                        <asp:HiddenField ID="hdnCountryLevelIDs" runat="server" Value='<%#Eval("CountryLevelIDs")%>' />
                        <asp:HiddenField ID="hdnIsHighSpeedPass" runat="server" Value='<%#Eval("IsHighSpeedPass")%>' />
                        <asp:Label ID="lblLID" runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                        <%#Eval("PrdtName")%>
                        <asp:Label ID="lblCountryName" runat="server" />,
                        <%#Eval("ValidityName")%>,
                        <%#Eval("TravellerName")%>,
                        <%#Eval("ClassName")%>
                        -
                        <%=currency.ToString()%>
                        <%#Eval("Price")%>
                        <asp:LinkButton ID="lnkDelete" CommandName="Delete" CommandArgument='<%#Eval("Id")%>' style="display:none;"
                            runat="server" OnClientClick="return window.confirm('Are you sure? Do you want to delete this item?')"><img src='images/btn-cross.png' class="icon-close scale-with-grid" alt="" border="0" /></asp:LinkButton>
                        <a href="javascript:void(0);">
                            <img src='images/arr-down.png' class="icon scale-with-grid imgOpen" alt="" border="0" /></a>
                    </div>
                    <div class="discription-block row">
                        <div class="pass-coloum-three">
                            Title
                            <asp:DropDownList ID="ddlTitle" class="input" runat="server" Style="margin-left: -2px; Width:60px">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="pass-coloum-forth">
                            First Name<span class="valid3">*</span>
                            <asp:TextBox ID="txtFirstName" runat="server" class="input chkvalid" onkeyup="chkerrorvali(this)"
                                Text='<%#Eval("FirstName") %>' Width="190px" />
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ErrorMessage='Please enter first name'
                                CssClass="valid" Text="Please enter First Name" Display="Dynamic" ForeColor="Red"
                                ValidationGroup="vg1" ControlToValidate="txtFirstName" />
                            <br />
                            <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                        <div class="pass-coloum-forth">
                            Last Name<span class="valid3">*</span>
                            <asp:TextBox ID="txtLastName" runat="server" class="input chkvalid" onkeyup="chkerrorvali(this)"
                                Text='<%#Eval("LastName") %>' Width="190px" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="valid"
                                Text="Please enter Last Name" ValidationGroup="vg1" Display="Dynamic" ForeColor="Red"
                                ControlToValidate="txtLastName" />
                            <br />
                            <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtLastName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-" />
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="pass-coloum-forth">
                                    Country of residence
                                    <asp:HiddenField ID="hdnproductid" runat="server" Value='<%#Eval("PrdtId") %>' />
                                    <asp:DropDownList ID="ddlCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_chkcounty"
                                        runat="server" class="chkcountry countryinput" Width="190px" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="vg1"
                                        CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="ddlCountry" InitialValue="0" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCountry" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div id="Div1" class="pass-coloum-fifth" style="margin-left: -1px; width: 150px;"
                            runat="server" visible='<%#Eval("PassportIsVisible")%>'>
                            Passport Number<span class="valid3">*</span>
                            <asp:TextBox ID="txtPassportNumber" Text='<%#Eval("PassportNo") %>' class="input chkvalid"
                                runat="server" Style="margin-left: -3px;" onkeyup="chkerrorvali(this)" Width="190px" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="vg1" runat="server"
                                CssClass="valid" Text="Please enter Passport No." Display="Dynamic" ForeColor="Red"
                                ControlToValidate="txtPassportNumber" />
                        </div>
                        <div class="pass-coloum-one" style="clear: both!important;">
                            Pass Start Date<span class="valid3">*</span>
                        </div>
                        <div class="pass-coloum-two" id="test" onclick="chkVal(this,'<%#Eval("MonthValidity")%>')">
                            <asp:TextBox ID="txtStartDate" runat="server" Text='<%# (Eval("PassStartDate") != null ? Eval("PassStartDate","{0:dd/MM/yyyy}") : "DD/MM/YYYY") %>'
                                class="input chkvalid calDate" onchange="chkerrorvali(this)" />
                            <span class="imgCal calIcon" style="float: left;" title="Select Pass Start Date.">
                            </span>
                            <div style="clear: both;">
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="vg1" runat="server"
                                CssClass="valid" Text="Please enter Date" Display="Dynamic" InitialValue="DD/MM/YYYY"
                                ForeColor="Red" ControlToValidate="txtStartDate" />
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Button ID="btnChkOut" runat="server" Text="CheckOut" ValidationGroup="vg1" CssClass="btn-red-cart newbutton w184 f-right"
            OnClientClick="btnclick()" OnClick="btnChkOut_Click" />
        <asp:Button ID="btnContinue" runat="server" Text="Continue Shopping" Style="margin-right: 10px; display:none;"
            CausesValidation="false" CssClass="btn-red-cart newbutton btn-gray w184 f-right"
            OnClick="btnContinue_Click" />
    </div>
</asp:Content>


