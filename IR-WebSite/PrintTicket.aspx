﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PrintTicket.aspx.cs" Inherits="PrintTicket" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script>
        $("#lnkUrl").click(function () {
            $("#lnkUrl").next().find();
        });
    </script>
    <style type="text/css">
        /*booking detail*/
        
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        .booking-detail-in-print
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in-print table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in-print table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in-print table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        .booking-detail-in-print .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in-print .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in-print .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in-print .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
        .clsTicket
        {
            text-align: center;
            font-size: 15px;
            color: #4d4d4d;
        }
        .left-content
        {
            width: 686px;
            padding: 10px; 
        }
    </style>
    <%=script %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content">
        <div class="breadcrumb">
            <a href="Home">Home </a>>><a href="TrainResults">Train Results</a>>><a href="BookingCart">Booking
                Cart</a>>><a href="OrderSuccessPage">Order Success </a>>>Print Ticket
        </div>
        <div class="innner-banner">
            <div class="bannerLft">
                <div class="banner90">
                    <asp:Label ID="lblBannerTxt" runat="server" Text="Rail Passes" /></div>
            </div>
            <div>
                <div class="float-lt" style="width: 73%">
                    <asp:Image ID="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="901px" Height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
                <div class="float-rt" style="width: 27%; text-align: right;">
                    <asp:Image ID="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="100%" Height="190px" ImageUrl="images/innerMap.gif" />
                </div>
            </div>
        </div>
        <div class="f-left">
            <div class="left-content">
                <h1>
                    Your ticket(s)
                </h1>
                <div class="clsTicket">
                    <asp:Literal Text="Please click on the button below to download your PDF ticket(s)."
                        runat="server" ID="ltrTicketMsg" Visible="true"></asp:Literal>
                    <p style="padding: 50px 0 50px 0">
                       <asp:Repeater ID="rptBeNePrint" runat="server" OnItemDataBound="rptBeNePrint_ItemDataBound">
                            <ItemTemplate>
                                <a href='<%#Eval("URL")%>' target="_blank" id="lnkUrl" runat="server">
                                    <asp:Image ID="Image1" CssClass="scale-with-grid" alt="" border="0" runat="server"
                                        ImageUrl="images/download-ticket.png" /></a><br /><br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </p>
                    Thanks for you order - have a great trip!<br />
                    <br />
                    If we can help you with anything else please do not hesitate to contact us.<br />
                    <br />
                    <br />
                    <br />
                    <i style="font-size: 13px">You'll need Adobe Reader to view your ticket, if you don't
                        already have it click below to download it.</i>
                    <p style="padding: 50px 0 50px 0">
                        <a href="http://www.adobe.com/go/getreader" target="_blank" style="text-decoration:none;">
                            <img id="Image2" class="scale-with-grid" alt="" border="0" src="images/get_adobe_reader.gif" />
                        </a>
                    </p>
                </div>
                <div>
                    <asp:Button ID="btnContinue" Text="Continue Shopping" class="f-right" Width="160px" PostBackUrl="~/Default.aspx" runat="server"  />
                </div>
            </div>
            <div class="right-content">
                <uc:ucRightContent ID="ucRightContent" runat="server" />
            </div>
            <asp:HiddenField runat="server" ID="hdnExec" Value="0" />
        </div>
    </div>
</asp:Content>
