﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingCondition.aspx.cs"
    MasterPageFile="~/Site.master" Inherits="BookingCondition" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".privacy-block-outer").find(".discription-block").hide();
            $("#divCmsCon").find(".discription-block").show();
            $("#divCmsCon").find(".aDwn").show();
            $("#divCmsCon").find(".aRt").hide();

            $("#divCmsCon").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCon").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCon").find(".aDwn").show();
                $("#divCmsCon").find(".aRt").hide();
            });

            $("#divCmsCon").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCon").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCon").find(".aDwn").show();
                $("#divCmsCon").find(".aRt").hide();
            });

            $("#divCmsBook").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBook").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBook").find(".aDwn").show();
                $("#divCmsBook").find(".aRt").hide();
            });

            $("#divCmsBook").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBook").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBook").find(".aDwn").show();
                $("#divCmsBook").find(".aRt").hide();
            });

            $("#divCmsValid").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsValid").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsValid").find(".aDwn").show();
                $("#divCmsValid").find(".aRt").hide();
            });

            $("#divCmsValid").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsValid").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsValid").find(".aDwn").show();
                $("#divCmsValid").find(".aRt").hide();
            });

            $("#divCmsFare").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsFare").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsFare").find(".aDwn").show();
                $("#divCmsFare").find(".aRt").hide();
            });

            $("#divCmsFare").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsFare").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsFare").find(".aDwn").show();
                $("#divCmsFare").find(".aRt").hide();
            });

            $("#divCmsTck").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsTck").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsTck").find(".aDwn").show();
                $("#divCmsTck").find(".aRt").hide();
            });

            $("#divCmsTck").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsTck").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsTck").find(".aDwn").show();
                $("#divCmsTck").find(".aRt").hide();
            });

            $("#divCmsLost").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsLost").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsLost").find(".aDwn").show();
                $("#divCmsLost").find(".aRt").hide();
            });

            $("#divCmsLost").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsLost").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsLost").find(".aDwn").show();
                $("#divCmsLost").find(".aRt").hide();
            });

            $("#divCmsBag").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBag").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBag").find(".aDwn").show();
                $("#divCmsBag").find(".aRt").hide();
            });

            $("#divCmsBag").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBag").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBag").find(".aDwn").show();
                $("#divCmsBag").find(".aRt").hide();
            });

            $("#divCmsForce").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsForce").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsForce").find(".aDwn").show();
                $("#divCmsForce").find(".aRt").hide();
            });

            $("#divCmsForce").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsForce").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsForce").find(".aDwn").show();
                $("#divCmsForce").find(".aRt").hide();
            });

            $("#divCmsLaw").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsLaw").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsLaw").find(".aDwn").show();
                $("#divCmsLaw").find(".aRt").hide();
            });

            $("#divCmsLaw").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsLaw").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsLaw").find(".aDwn").show();
                $("#divCmsLaw").find(".aRt").hide();
            });

            $("#divCmsBoard").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBoard").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBoard").find(".aDwn").show();
                $("#divCmsBoard").find(".aRt").hide();
            });

            $("#divCmsBoard").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBoard").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBoard").find(".aDwn").show();
                $("#divCmsBoard").find(".aRt").hide();
            });

            $("#divCmsTime").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsTime").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsTime").find(".aDwn").show();
                $("#divCmsTime").find(".aRt").hide();
            });

            $("#divCmsTime").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsTime").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsTime").find(".aDwn").show();
                $("#divCmsTime").find(".aRt").hide();
            });

            $("#divCmsVar").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsVar").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsVar").find(".aDwn").show();
                $("#divCmsVar").find(".aRt").hide();
            });

            $("#divCmsVar").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsVar").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsVar").find(".aDwn").show();
                $("#divCmsVar").find(".aRt").hide();
            });
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Booking Condition </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Booking Condition</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content">
<asp:Repeater ID="rptBooking" runat="server">
<ItemTemplate>
<h1><%#Eval("Title")%></h1>
<p><%#Eval("Description")%></p>
<p>&nbsp; </p>
<div class="clear"> &nbsp;</div>
<div id="divBlock">
    <div id="divCmsCon" class="privacy-block-outer">
        <div class="hd-title">
            <strong> 1. The Contract </strong> 
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("Contract"))%></div>
    </div>
    <div id="divCmsBook" class="privacy-block-outer">
        <div class="hd-title">
            <strong>  2. Bookings & Reservations </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("BookingsReservations"))%></div>
    </div>
    <div id="divCmsValid" class="privacy-block-outer">
        <div class="hd-title">
        <strong>  3. Validity </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
    <div class="discription-block">
        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Validity"))%>
    </div>
    </div>
    <div id="divCmsFare" class="privacy-block-outer">
        <div class="hd-title">
            <strong>  4. Fares and payment </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("FaresPayment"))%></div>
    </div>
    <div id="divCmsTck" class="privacy-block-outer">
        <div class="hd-title">
            <strong>     5. Changes to your ticket and refunds </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("TicketRefunds"))%>
        </div>
    </div>
    <div id="divCmsLost" class="privacy-block-outer">
        <div class="hd-title">
            <strong>    6. Lost or Damaged Tickets </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("LostDamagedTickets"))%>
        </div>
    </div>
    <div id="divCmsBag" class="privacy-block-outer">
        <div class="hd-title">
            <strong>    7. Baggage </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block">
            <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Baggage"))%>
        </div>
    </div>
    <div id="divCmsForce" class="privacy-block-outer">
            <div class="hd-title">
                <strong>   8. Force Majeure</strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block">
                <%#HttpContext.Current.Server.HtmlDecode((string)Eval("ForceMajeure"))%>
            </div>
    </div>
    <div id="divCmsLaw" class="privacy-block-outer">
        <div class="hd-title">
            <strong>  9. Governing Law </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("GoverningLaw"))%></div>
    </div>
    <div id="divCmsBoard" class="privacy-block-outer">
        <div class="hd-title">
            <strong> 10. Boarding times </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("BoardingTimes"))%></div>
    </div>
    <div id="divCmsTime" class="privacy-block-outer">
        <div class="hd-title">
            <strong> 11. Timetable information </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block">
            <%#HttpContext.Current.Server.HtmlDecode((string)Eval("TimetableInformation"))%>
        </div>
    </div>
    <div id="divCmsVar" class="privacy-block-outer">
        <div class="hd-title">
            <strong> 12. Variations </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block">
            <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Variations"))%>
        </div>
    </div>
</div>
</ItemTemplate>
</asp:Repeater>
</div>
<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
