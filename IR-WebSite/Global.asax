﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<script RunAt="server">    
    void Application_Start(object sender, EventArgs e)
    {
        try
        {
            Business.ManageRoutes objroute = new Business.ManageRoutes();
            objroute.UpdateRouteCount(Business.Route.CategoryRoute, 0);
            objroute.UpdateRouteCount(Business.Route.CountryRoute, 0);
            objroute.UpdateRouteCount(Business.Route.ProductRoute, 0);
            objroute.UpdateRouteCount(Business.Route.SpecialTrainRoute, 0);
            RouteTable.Routes.Clear();
            RegisterRoutes(RouteTable.Routes);
        }
        catch
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }

    void RegisterRoutes(RouteCollection routes)
    {
        PageUrls.MasterSiteUrl(routes);
        PageUrls.DefaultSiteUrl(routes);
        PageUrls.RailPassCatUrl(routes);
        PageUrls.RailPassDetailUrl(routes);
        PageUrls.CountryDetailUrl(routes);
        PageUrls.SpecialTrainUrl(routes);
        PageUrls.PagesUrl(routes);
    }

    void Application_BeginRequest(object sender, EventArgs e)
    {
        try
        {
            Business.ManageRoutes objroute = new Business.ManageRoutes();
            if (objroute.GetRouteProductUpdateUrl())
            {
                objroute.UpdateRouteCount(Business.Route.CategoryRoute, 0);
                objroute.UpdateRouteCount(Business.Route.CountryRoute, 0);
                objroute.UpdateRouteCount(Business.Route.ProductRoute, 0);
                objroute.UpdateRouteCount(Business.Route.SpecialTrainRoute, 0);
                RegisterRoutes(RouteTable.Routes);
                objroute.DoneRouteProductUpdateUrl();
            }
            else
            {
                RegisterRoutes(RouteTable.Routes);
                SetOldNewUrl();
            }
        }
        catch
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }

    private void SetOldNewUrl()
    {
        bool chk = true;
        var list = PageUrls.GetOldNewUrl().AsEnumerable();
        var url = list.FirstOrDefault(x => x.OldUrl.ToLower().Trim() == Request.Url.ToString().ToLower().Trim());
        var url2 = list.FirstOrDefault(x => x.OldUrl.ToLower().Trim() == (Request.Url.GetLeftPart(UriPartial.Authority) + Request.RawUrl).ToLower().Trim());
        if (url != null || url2 != null)
        {
            chk = false;
            url = url == null ? url2 : url;
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", url.NewURL);
            Response.End();
        }
        else if (Request.Url.ToString().Contains("internationalrail.co.uk"))
        {
            chk = false;
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", "https://www.internationalrail.com" + Request.Url.PathAndQuery);
            Response.End();
        }
        else if (!Request.IsSecureConnection && Request.Url.Host.ToString().Contains("internationalrail.com"))
        {
            chk = false;
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", "https://www.internationalrail.com" + Request.Url.PathAndQuery);
            Response.End();
        }
        else if (Request.Url.ToString().Contains("new.bookmyrst.co.uk"))
        {
            chk = false;
            if (Request.Url.ToString().Contains("www.new.bookmyrst.co.uk"))
            {
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", "http://new.bookmyrst.co.uk" + Request.Url.PathAndQuery);
                Response.End();
            }
        }
        else if (PageUrls.DefaultProductUrl(Request.Url.ToString()).Length > 4)
        {
            chk = false;
            string Newurl = PageUrls.DefaultProductUrl(Request.Url.ToString());
            Response.Status = "301 Moved Permanently";
            if (Request.IsLocal)
                Response.AddHeader("Location", Newurl);
            else
                Response.AddHeader("Location", Newurl.Contains("https:") ? Newurl : Newurl.Replace("http:", "https:"));
            Response.End();
        }
        if (chk)
            RedirectOnHttps();
    }

    private void RedirectOnHttps()
    {
        if (!Request.IsLocal && !Request.IsSecureConnection)
        {
            string sUrl = "";
            if (Request.Url.Host.Contains("www.") || Request.Url.ToString().Contains("rail.statravel.com.sg"))
                sUrl = "https://" + Request.Url.Host + Request.Url.PathAndQuery;
            else
                sUrl = "https://www." + Request.Url.Host + Request.Url.PathAndQuery;
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", sUrl);
            Response.End();
        }
    }
    
    void Application_Error(object sender, EventArgs e)
    {
        try
        {
            var serverError = Server.GetLastError() as HttpException;
            if (serverError != null)
                if (serverError.GetHttpCode() == 404)
                {
                    Server.ClearError();
                    if (Request.Url.ToString().ToLower().Contains("pdfservice"))
                        Response.Redirect("~/404.aspx?URL=refund");
                    else
                        Response.Redirect("~/404.aspx");
                } 
        }
        catch
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }

    void Session_Start(object sender, EventArgs e)
    {
        Session.Timeout = 30;
    }     
</script>
