﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PaymentConfirmation.aspx.cs" Inherits="Order_PaymentConfirmation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
    <script type="text/javascript" src='../Scripts/jquery-1.9.0.min.js'></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
        ValidationGroup="vgs" ShowSummary="false" ShowMessageBox="true" />
    <div class="content">
        <div class="innner-banner">
            <div class="bannerLft">
                <div class="banner90">
                    <asp:Label ID="lblBannerTxt" runat="server" Text="Rail Passes" /></div>
            </div>
            <div>
                <div class="float-lt" style="width: 73%">
                    <asp:Image ID="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="901px" Height="190px" ImageUrl="../images/img_inner-banner.jpg" /></div>
                <div class="float-rt" style="width: 27%; text-align: right;">
                    <asp:Image ID="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="100%" Height="190px" ImageUrl="../images/innerMap.gif" />
                </div>
            </div>
        </div>
        <div class="left-content paymentconfirm">
            <h1>
                Payment Confirmation
            </h1>
            <iframe id="PaymentFrame" src='<%=linkURL %>' style="width: 100% !important; height: 100% !important;">
            </iframe>
        </div>
        <div class="right-content">
            <a href='<%=siteURL %>TrainResults.aspx' id="trainresults">
                <div id="rtPannel1" runat="server">
                </div>
            </a>
            <%--<img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />--%>
            <div class="sml-detail-block-outer" style="margin-top:28px">
                <div id="rightHead1" class="pTop40">
                </div>
                <div class="sml-detail-block-inner" style="border-radius: 5px 5px 5px 5px;">
                    <div id="rtPanelImgTop">
                            <img src="../images/imgPaymentConfirmation.jpg" style="width: 237px; height: 118px;border-radius: 5px 5px 5px 5px;" />
                    </div>
                </div>
            </div>
            <img src='../images/block-shadow.jpg' class="scale-with-grid" alt="" border="0" />
        </div>
    </div>
</asp:Content>
