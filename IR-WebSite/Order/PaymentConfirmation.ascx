﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PaymentConfirmation.ascx.cs"
    Inherits="Order_PaymentConfirmation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type="text/javascript" src='../Scripts/jquery-1.9.0.min.js'></script>
<script type="text/javascript" async="true" src="https://elistva.com/api/script.js?aid=10376&sid=<%=sid%>>"></script>
<noscript>
    <p style="background: url(//elistva.com/api/assets/clear.png?aid=10376&sid=<%=sid%>)">
    </p>
</noscript>
<object type="application/x-shockwave-flash" data="//elistva.com/api/udid.swf?aid=10376&sid=<%=sid%>"
    width="1" height="1">
    <param name="movie" value="//elistva.com/api/udid.swf?aid=10376&sid=<%=sid%>" />
</object>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
    ValidationGroup="vgs" ShowSummary="false" ShowMessageBox="true" />
<div class="left-content paymentconfirm" style="width: 967px; background: #fff; padding: 5px;
    height:550px;">
    <h1>
        Payment Confirmation
    </h1>
    <iframe id="PaymentFrame" src='<%=linkURL %>' style="width: 100% !important;  height: 550px !important;"
        scrolling="no"></iframe>
</div>
 