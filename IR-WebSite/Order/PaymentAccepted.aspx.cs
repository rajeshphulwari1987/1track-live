﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication;
using System.Xml;
using System.Net.Mail;
using Business;

public partial class Order_PaymentAccepted : System.Web.UI.Page
{
    private Guid _siteId;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string currency = "$";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static Guid currencyID = new Guid();
    public string OrderNo = string.Empty;
    public string linkURL = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null)
        {
            OrderNo = Request.Params["req"];
            linkURL = "OrderSuccessPage?req=" + OrderNo;
        }
        else if (Session["OrderID"] != null)
        {
            OrderNo = Session["OrderID"].ToString();
            linkURL = "OrderSuccessPage";
        }



        var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        string rSiteName = "";
        if (st != null)
            rSiteName = st.SiteURL + "home";
        else
            rSiteName = "~/home";

        if (!string.IsNullOrEmpty(OrderNo))
        {
            var status = OgoneResponse.Status;
            GetCurrencyCode();
            new ManageBooking().UpdateOrderStatus(3, Convert.ToInt64(OrderNo));
        }
        else
            Response.Redirect(rSiteName);

        if (st != null)
        {
            string SiteName = st.SiteURL + linkURL;
            Response.Redirect(SiteName, true);
        }
    }

    public string getsitetickprotection(string siteid, string tktprotCharge)
    {
        try
        {
            string StTkProtnAmt = "";
            if (!string.IsNullOrEmpty(siteid) && !string.IsNullOrEmpty(tktprotCharge) && Convert.ToBoolean(tktprotCharge))
            {
                Guid Site_id = Guid.Parse(siteid);
                List<tblSite> lstSite = new Masters().GetSiteList().Where(a => a.ID == Site_id).ToList();
                var list = FrontEndManagePass.GetTicketProtectionPrice(Site_id);
                if (list != null)
                {
                    StTkProtnAmt = FrontEndManagePass.GetPriceAfterConversion(list.Amount, _siteId, list.CurrencyID, (lstSite.FirstOrDefault().DefaultCurrencyID.HasValue ? lstSite.FirstOrDefault().DefaultCurrencyID.Value : new Guid())).ToString("F");
                }
            }
            return StTkProtnAmt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GetCurrencyCode()
    {
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            _siteId = Guid.Parse(cookie.Values["_siteId"]);
            currencyID = Guid.Parse(cookie.Values["_curId"]);
            currency = oManageClass.GetCurrency(currencyID);
        }
    }
}