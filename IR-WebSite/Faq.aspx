﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Faq.aspx.cs" Inherits="Faq" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src='Scripts/jquery-1.9.0.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".privacy-block-outer").find(".discription-block").hide();
            $("#divCmsWl").find(".discription-block").show();
            $("#divCmsWl").find(".aDwn").show();
            $("#divCmsWl").find(".aRt").hide();

            $("#divCmsWl").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWl").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWl").find(".aDwn").show();
                $("#divCmsWl").find(".aRt").hide();
            });

            $("#divCmsWl").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWl").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWl").find(".aDwn").show();
                $("#divCmsWl").find(".aRt").hide();
            });

            $("#divCmsPrd").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPrd").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPrd").find(".aDwn").show();
                $("#divCmsPrd").find(".aRt").hide();
            });

            $("#divCmsPrd").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPrd").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPrd").find(".aDwn").show();
                $("#divCmsPrd").find(".aRt").hide();
            });

            $("#divCmsWeb").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWeb").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWeb").find(".aDwn").show();
                $("#divCmsWeb").find(".aRt").hide();
            });

            $("#divCmsWeb").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWeb").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWeb").find(".aDwn").show();
                $("#divCmsWeb").find(".aRt").hide();
            });

            $("#divCmsAnti").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsAnti").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsAnti").find(".aDwn").show();
                $("#divCmsAnti").find(".aRt").hide();
            });

            $("#divCmsAnti").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsAnti").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsAnti").find(".aDwn").show();
                $("#divCmsAnti").find(".aRt").hide();
            });

            $("#divCmsCopy").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCopy").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCopy").find(".aDwn").show();
                $("#divCmsCopy").find(".aRt").hide();
            });

            $("#divCmsCopy").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCopy").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCopy").find(".aDwn").show();
                $("#divCmsCopy").find(".aRt").hide();
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".faqQactive").on('click', function () {
                var cls = $(this).attr('class');
                if (cls != 'faqQdisable') {
                    $(".faqQdisable").removeClass().addClass("faqQactive");
                    $(".faqAnswer").slideUp(500);
                    $(this).next('.faqAnswer').slideDown(500);
                    $(this).removeClass().addClass("faqQdisable");
                }

                else {
                    $(this).next('.faqAnswer').hide();
                    $(this).removeClass().addClass("faqQactive");
                }
            });
        });
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Faq </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Faq</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content">
<div id='divDescription' runat="server"></div>
<%=Server.HtmlDecode(FaqDescription)%>
<asp:Repeater ID="rptFaq" runat="server">
    <ItemTemplate>
             <div style="float:left;width:99%;  padding-top: 15px ;">
                <a class="faqQactive" href="javascript:void(0)" >► <%#Eval("Question") %></a>
                    <div class="faqAnswer">
                       <strong class="clsFaq">Answer:</strong> 
                        <%#Eval("Answer") %></div>
                 </div>
    </ItemTemplate>
</asp:Repeater>
    </div>
<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
