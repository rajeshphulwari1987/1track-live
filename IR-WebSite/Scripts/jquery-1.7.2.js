<!doctype html>
<!--[if IE 7 ]>		 <html class="no-js ie ie7 lte7 lte8 lte9" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 8 ]>		 <html class="no-js ie ie8 lte8 lte9" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 9 ]>		 <html class="no-js ie ie9 lte9>" dir="ltr" lang="en-US"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" dir="ltr" lang="en-US"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>jQuery 1.7.2 Released | Official jQuery Blog</title>

	<meta name="author" content="jQuery Foundation - jquery.org">
	<meta name="description" content="jQuery: The Write Less, Do More, JavaScript Library">

	<meta name="viewport" content="width=device-width">

	<link rel="shortcut icon" href="http://blog.jquery.com/wp-content/themes/jquery.com/i/favicon.ico">

	<link rel="stylesheet" href="http://blog.jquery.com/wp-content/themes/jquery/css/base.css?v=1">
	<link rel="stylesheet" href="http://blog.jquery.com/wp-content/themes/jquery.com/style.css">
	<link rel="pingback" href="http://blog.jquery.com/xmlrpc.php" />
	<!--[if lt IE 7]><link rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->

	<script src="http://blog.jquery.com/wp-content/themes/jquery/js/modernizr.custom.2.6.2.min.js"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write(unescape('%3Cscript src="http://blog.jquery.com/wp-content/themes/jquery/js/jquery-1.9.1.min.js"%3E%3C/script%3E'))</script>
	<script src="http://blog.jquery.com/wp-content/themes/jquery/js/jquery-migrate-1.1.1.min.js"></script>

	<script src="http://blog.jquery.com/wp-content/themes/jquery/js/plugins.js"></script>
	<script src="http://blog.jquery.com/wp-content/themes/jquery/js/main.js"></script>

	<script src="//use.typekit.net/wde1aof.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

<link rel="alternate" type="application/rss+xml" title="Official jQuery Blog &raquo; Feed" href="http://blog.jquery.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Official jQuery Blog &raquo; Comments Feed" href="http://blog.jquery.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Official jQuery Blog &raquo; jQuery 1.7.2 Released Comments Feed" href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/feed/" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://blog.jquery.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://blog.jquery.com/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Announcing the 2012 San Francisco jQuery Conference!' href='http://blog.jquery.com/2012/03/20/announcing-the-2012-san-francisco-jquery-conference/' />
<link rel='next' title='Bowling for jQuery &#8211; April 2012, Washington, D.C.' href='http://blog.jquery.com/2012/03/26/bowling-for-jquery-apr-2012-dc/' />
<meta name="generator" content="WordPress 3.4.1" />
<link rel='canonical' href='http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/' />
<link rel='shortlink' href='http://blog.jquery.com/?p=1929' />

</head>
<body class="jquery singular">

<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->

<header>
	<section id="global-nav">
		<nav>
			<div class="constrain">
				<ul class="projects">
					<li class="project jquery"><a href="http://jquery.com/" title="jQuery">jQuery</a></li>
					<li class="project jquery-ui"><a href="http://jqueryui.com/" title="jQuery UI">jQuery UI</a></li>
					<li class="project jquery-mobile"><a href="http://jquerymobile.com/" title="jQuery Mobile">jQuery Mobile</a></li>
					<li class="project sizzlejs"><a href="http://sizzlejs.com/" title="Sizzle">Sizzle</a></li>
					<li class="project qunitjs"><a href="http://qunitjs.com/" title="QUnit">QUnit</a></li>
				</ul>
				<ul class="links">
					<li><a href="http://plugins.jquery.com/">Plugins</a></li>
					<li class="dropdown"><a href="http://contribute.jquery.org/">Contribute</a>
						<ul>
							<li><a href="http://contribute.jquery.org/cla/">CLA</a></li>
							<li><a href="http://contribute.jquery.org/style-guide/">Style Guides</a></li>
							<li><a href="http://contribute.jquery.org/triage/">Bug Triage</a></li>
							<li><a href="http://contribute.jquery.org/code/">Code</a></li>
							<li><a href="http://contribute.jquery.org/documentation/">Documentation</a></li>
							<li><a href="http://contribute.jquery.org/web-sites/">Web Sites</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="http://events.jquery.org/">Events</a>
						<ul class="wide">
							<li><a href="http://events.jquery.org/2013/austin/#training">Sep 9 | jQuery Training Austin</a></li>
							<li><a href="http://events.jquery.org/2013/austin/">Sep 10-11 | jQuery Conference Austin</a></li>
							<li><a href="http://jquery.itmozg.ru/">Oct 15 | jQuery Russia 2013</a></li>
							<li><a href="http://2013.cssdevconf.com/">Oct 21-22 | CSS Dev Conf</a></li>
							<li><a href="http://javascriptsummit.com/">Nov 19-21 | JavaScript Summit</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="https://jquery.org/support/">Support</a>
						<ul>
							<li><a href="http://learn.jquery.com/">Learning Center</a></li>
							<li><a href="http://try.jquery.com/">Try jQuery</a></li>
							<li><a href="http://irc.jquery.org/">IRC/Chat</a></li>
							<li><a href="http://forum.jquery.com/">Forums</a></li>
							<li><a href="http://stackoverflow.com/tags/jquery/info">Stack Overflow</a></li>
							<li><a href="https://jquery.org/support/">Commercial Support</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="https://jquery.org/">jQuery Foundation</a>
						<ul>
							<li><a href="https://jquery.org/join/">Join</a></li>
							<li><a href="https://jquery.org/members/">Members</a></li>
							<li><a href="https://jquery.org/team/">Team</a></li>
							<li><a href="http://brand.jquery.org/">Brand Guide</a></li>
							<li><a href="https://jquery.org/donate/">Donate</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</section>
</header>

<div id="container">
	<div id="logo-events" class="constrain clearfix">
		<h2 class="logo"><a href="/" title="Official jQuery Blog">Official jQuery Blog</a></h2>

		<aside><div id="broadcast"></div></aside>
	</div>

	<nav id="main" class="constrain clearfix">
		<div class="menu-top-container">
	<ul id="menu-top" class="menu">
<li class="menu-item"><a href="http://jquery.com/download/">Download</a></li>
<li class="menu-item"><a href="http://api.jquery.com/">API Documentation</a></li>
<li class="menu-item current"><a href="http://blog.jquery.com/">Blog</a></li>
<li class="menu-item"><a href="http://plugins.jquery.com/">Plugins</a></li>
<li class="menu-item"><a href="http://jquery.com/browser-support/">Browser Support</a></li>
	</ul>
</div>

		<form method="get" class="searchform" action="http://blog.jquery.com/">
	<button type="submit" class="icon-search"><span class="visuallyhidden">search</span></button>
	<label>
		<span class="visuallyhidden">Search Official jQuery Blog</span>
		<input type="text" name="s" value=""
			placeholder="Search">
	</label>
</form>
	</nav>

	<div id="content-wrapper" class="clearfix row">


<div class="content-right twelve columns">
	<div id="content">
		<h1 class="entry-title">jQuery 1.7.2 Released</h1>
				<div class="entry-posted">
			<span class="sep">Posted on </span><a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/" title="4:13 pm" rel="bookmark"><time class="entry-date" datetime="2012-03-21T16:13:40+00:00" pubdate>March 21, 2012</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="http://blog.jquery.com/author/dmethvin/" title="View all posts by dmethvin" rel="author">dmethvin</a></span></span>		</div>
				<hr>

		
<p>jQuery 1.7.2 is looking good! The release candidate went smoothly so we&#8217;ve made only <a href="http://bugs.jquery.com/ticket/11469">one small change</a> and are releasing it to your eager hands today. You can get the oven-fresh code from the jQuery CDN now, with Google and Microsoft CDNs soon to follow:</p>
<ul>
<li><a href="http://code.jquery.com/jquery-1.7.2.min.js">http://code.jquery.com/jquery-1.7.2.min.js</a> (minified, production)</li>
<li><a href="http://code.jquery.com/jquery-1.7.2.js">http://code.jquery.com/jquery-1.7.2.js</a> (unminified, debug)</li>
</ul>
<p><em>Note:</em> If you&#8217;re using jQuery Mobile, please use jQuery 1.7.2 only with jQuery Mobile 1.1. For previous versions of jQuery Mobile, stay with jQuery core 1.7.1 or earlier. </p>
<p>You can use the <a href="http://bugs.jquery.com">bug tracker</a> to report bugs; be sure to create a test case on <a href="http://jsfiddle.net">jsFiddle</a> so we can figure it out easily. If you&#8217;re not sure it&#8217;s a bug, ask on our <a href="http://forum.jquery.com">forum</a> or on <a href="http://stackoverflow.com/questions/tagged/jquery">StackOverflow</a>. Please <em>don&#8217;t</em> use the blog comments below to report bugs.</p>
<p>As always, this release wouldn&#8217;t have been possible without the hard work of many people. First, the jQuery Core Team:  gnarf, jaubourg, mikesherov, rwldrn, and timmywil. The support of the jQuery UI, Mobile, and Infrastructure teams was greatly appreciated as well, especially danheberden&#8217;s valiant struggle against Trac. </p>
<p>Community members like adebree, caii, cmc3cn, KYSergey, mathiasbynens, miskith, MrMamen, Orkel, pasky, SineSwiper, tavelli, and vlazar pitched in by reporting, verifying, and fixing bugs. Special jQuery Gold Leaf Cluster thanks to gibson042 and sindresorhus for their work in making 1.7.2 a great release. Don&#8217;t let these people have all the fun! If you&#8217;d like to contribute to the web&#8217;s most popular Javascript library, hop onto #jquery-dev on IRC or the Developing jQuery Core section on <a href="http://forum.jquery.com">our forum</a> and say hello!</p>
<p>Many thanks to Louis-Rémi Babé, who submitted a <a href="http://bugs.jquery.com/ticket/11469">patch to fix a regression with negative margin animations</a> a mere 30 minutes before the release!</p>
<h2>jQuery 1.7.2 Change Log</h2>
<p>Here&#8217;s the change log of the 1.7.2 release.</p>
<h3>Ajax</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/4624">#4624</a>: Charset in default ajaxSettings.contentType</li>
<li><a href="http://bugs.jquery.com/ticket/10978">#10978</a>: jQuery.param() should allow non-native constructed objects as property values</li>
<li><a href="http://bugs.jquery.com/ticket/11264">#11264</a>: evalScript() uses defaults set by ajaxSetup()</li>
<li><a href="http://bugs.jquery.com/ticket/11426">#11426</a>: jQuery.ajax() always fails when requesting JPG images in IE</li>
</ul>
<h3>Attributes</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/5571">#5571</a>: Allow chaining when passing undefined to any setter in jQuery</li>
<li><a href="http://bugs.jquery.com/ticket/10828">#10828</a>: attr(&#8220;coords&#8221;) returns undefined in IE7</li>
<li><a href="http://bugs.jquery.com/ticket/10870">#10870</a>: Incorrect behaviour of $.removeAttr(&#8220;selected&#8221;)</li>
<li><a href="http://bugs.jquery.com/ticket/11316">#11316</a>: Consider looking through valHooks by element type first, then by nodeName instead of the other way around</li>
</ul>
<h3>Build</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/10692">#10692</a>: Configure the jshint options to more accurately match the style guide</li>
<li><a href="http://bugs.jquery.com/ticket/10693">#10693</a>: generalize the &#8220;test something in an iframe&#8221; code in unit tests</li>
<li><a href="http://bugs.jquery.com/ticket/10901">#10901</a>: have unit tests fail if the tester is running from file:// or doesn&#8217;t have PHP</li>
<li><a href="http://bugs.jquery.com/ticket/10902">#10902</a>: ability to test a built version of jQuery in unit tests</li>
<li><a href="http://bugs.jquery.com/ticket/10931">#10931</a>: Unit tests shouldn&#8217;t require internet access</li>
</ul>
<h3>Core</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/10466">#10466</a>: jQuery.param() mistakes wrapped primitives for deep objects</li>
</ul>
<h3>Css</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/10639">#10639</a>: outerWidth(true) and css(&#8216;margin&#8217;) returning % instead of px in Webkit</li>
<li><a href="http://bugs.jquery.com/ticket/10754">#10754</a>: have jQuery.swap return the return of the callback instead of just executing it</li>
<li><a href="http://bugs.jquery.com/ticket/10782">#10782</a>: Incorrect calculating width</li>
<li><a href="http://bugs.jquery.com/ticket/10796">#10796</a>: Bug in IE7 with $(&#8216;#el&#8217;).css.(&#8216;background-position&#8217;)</li>
<li><a href="http://bugs.jquery.com/ticket/10858">#10858</a>: css.js regular expressions are incomplete</li>
<li><a href="http://bugs.jquery.com/ticket/11119">#11119</a>: The curCSS function only need  2 arguments</li>
</ul>
<h3>Data</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/11309">#11309</a>: hexadecimal-formatted data-* attributes parsed incorrectly</li>
</ul>
<h3>Deferred</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/11306">#11306</a>: calling .disable() or .lock() on a $.Callbacks object breaks its fired() status</li>
</ul>
<h3>Dimensions</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/3838">#3838</a>: $(document).height() incorrect in IE6</li>
</ul>
<h3>Effects</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/8498">#8498</a>: Animate Hooks</li>
<li><a href="http://bugs.jquery.com/ticket/10006">#10006</a>: method show is not working as expected in all browsers when called for document fragment</li>
<li><a href="http://bugs.jquery.com/ticket/10848">#10848</a>: Animation toggling loses state tracking in certain atomic edge cases</li>
<li><a href="http://bugs.jquery.com/ticket/11415">#11415</a>: Silently ignore negative CSS values where they are illegal</li>
<li><a href="http://bugs.jquery.com/ticket/11415">#11469</a>: Negative margin in animations (.animate)</li>
</ul>
<h3>Event</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/8165">#8165</a>: .live(&#8216;click&#8217;, handler) fires on disabled buttons with child elements in Chrome</li>
<li><a href="http://bugs.jquery.com/ticket/10819">#10819</a>: Eliminate &#8220;this.on.call(this, &#8220;</li>
<li><a href="http://bugs.jquery.com/ticket/10878">#10878</a>: $(&#8220;select&#8221;).live(&#8220;change&#8221;, function(){ &#8230;broken in IE8 in jQuery 1.7</li>
<li><a href="http://bugs.jquery.com/ticket/10961">#10961</a>: Error in XRegExp using jQuery 1.7.1 in IE6-9</li>
<li><a href="http://bugs.jquery.com/ticket/10970">#10970</a>: The .on() selector parameter doesn&#8217;t work with :not(:first) selector</li>
<li><a href="http://bugs.jquery.com/ticket/10984">#10984</a>: Cannot off() custom events ($.event.special)</li>
<li><a href="http://bugs.jquery.com/ticket/11021">#11021</a>: Hover hack mangles a namespace named &#8220;hover&#8221;</li>
<li><a href="http://bugs.jquery.com/ticket/11076">#11076</a>: .clone(true) loses delegation filters</li>
<li><a href="http://bugs.jquery.com/ticket/11130">#11130</a>: jQuery.fn.on: binding map with null selector ignores data</li>
<li><a href="http://bugs.jquery.com/ticket/11145">#11145</a>: $(document).on() not working with name=&#8221;disabled&#8221;</li>
</ul>
<h3>Manipulation</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/9427">#9427</a>: Passing undefined to .text() does not trigger setter</li>
<li><a href="http://bugs.jquery.com/ticket/10753">#10753</a>: inline the evalScript function in manipulation.js as it&#8217;s only used once</li>
<li><a href="http://bugs.jquery.com/ticket/10864">#10864</a>: text() method on a document fragment always returns the empty string</li>
<li><a href="http://bugs.jquery.com/ticket/11055">#11055</a>: Update HTML5 Shim elements list to support latest html5shiv</li>
<li><a href="http://bugs.jquery.com/ticket/11217">#11217</a>: Append problem with webkit</li>
<li><a href="http://bugs.jquery.com/ticket/11291">#11291</a>: Cloning XMLDoc&#8217;s with HTML5 nodeName&#8217;s breaks on IE</li>
<li><a href="http://bugs.jquery.com/ticket/11323">#11323</a>: script tags with type=&#8221;text/ecmascript&#8221; leak into the DOM</li>
<li><a href="http://bugs.jquery.com/ticket/11356">#11356</a>: safeFragment memory leak</li>
</ul>
<h3>Misc</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/10952">#10952</a>: .fired() doesn&#8217;t work on Callbacks object when it is flagged with &#8220;once&#8221;</li>
<li><a href="http://bugs.jquery.com/ticket/11257">#11257</a>: Wrong path to source files in test suite if PHP missing</li>
</ul>
<h3>Queue</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/10967">#10967</a>: .promise() does not attach methods onto target</li>
</ul>
<h3>Support</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/7986">#7986</a>: Bug in $.support.boxModel if page has DIV-element CSS</li>
<li><a href="http://bugs.jquery.com/ticket/11048">#11048</a>: Support Tests affect layout for positioned elements in IE6-9</li>
<li><a href="http://bugs.jquery.com/ticket/11337">#11337</a>: Bug in $.support.reliableMarginRight</li>
</ul>
<h3>Traversing</h3>
<ul>
<li><a href="http://bugs.jquery.com/ticket/11370">#11370</a>: $(&#8216;&lt;div&gt;&#8217;).siblings() throws exception</li>
</ul>
			<div id="comments">
	
	
			<h2 id="comments-title">
			49 thoughts on &ldquo;<span>jQuery 1.7.2 Released</span>&rdquo;		</h2>

		
		<ol class="commentlist">
				<li class="comment even thread-even depth-1" id="li-comment-531003">
		<article id="comment-531003" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/e6f4287bfdcc106161ddd8f8123bd279?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Jason</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531003"><time pubdate datetime="2012-03-21T20:23:47+00:00">March 21, 2012 at 8:23 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Any news on jQuery UI updates? Last stable version still has some problems with touchscreen devices, but I&#8217;ve seen devel releases that have things like draggables working. They haven&#8217;t updated news on the main UI site in a long time, so I thought I could ask here if the same people are doing both. The bug tracker also seems to be down at the moment.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531018">
		<article id="comment-531018" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/709fcf7a8134426bf36af7b10c72ebc1?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">simone</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531018"><time pubdate datetime="2012-03-22T05:59:19+00:00">March 22, 2012 at 5:59 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>@Jason: as for touch devices, I use <a href="http://code.google.com/p/jquery-ui-for-ipad-and-iphone/" rel="nofollow">http://code.google.com/p/jquery-ui-for-ipad-and-iphone/</a> and it works quite well with jQuery 1.7.1 and the latest stable jQuery UI. That stated, I&#8217;m also hungrily waiting for jQuery UI 1.9!<br />
bye</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531020">
		<article id="comment-531020" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/d2e32557fbb546470c63a9911b0c3576?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Sander</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531020"><time pubdate datetime="2012-03-22T07:51:04+00:00">March 22, 2012 at 7:51 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>In 1.7.2, var id = $(this).attr(&#8220;id&#8221;).split(&#8220;_&#8221;)[0]; will get an error in Firefox and Chrome, but works in IE. Same code works fine in 1.7.1.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531022">
		<article id="comment-531022" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/17bead94dbd2bbf819dd2e7a1a8ad81c?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Eli Sand</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531022"><time pubdate datetime="2012-03-22T10:04:17+00:00">March 22, 2012 at 10:04 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>You may want to update the year in your copyright notices &#8211; they&#8217;re still dated as 2011.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531023">
		<article id="comment-531023" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/b8aa1050fad545fe278326879649a1e5?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Savageman</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531023"><time pubdate datetime="2012-03-22T10:15:26+00:00">March 22, 2012 at 10:15 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I also have exception thrown with split()</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531025">
		<article id="comment-531025" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/988a14d0bd5d78af9e5ddf4527636476?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Ghost Leader</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531025"><time pubdate datetime="2012-03-22T11:28:49+00:00">March 22, 2012 at 11:28 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>&#8220;Please don’t use the blog comments below to report bugs.&#8221;</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531038">
		<article id="comment-531038" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/7b2b0f388e64b9a75879ed7a4ba6effb?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Shawn</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531038"><time pubdate datetime="2012-03-23T03:51:49+00:00">March 23, 2012 at 3:51 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I think because this one <a href="http://dev.jquery.com/" rel="nofollow">http://dev.jquery.com/</a> doesn&#8217;t work.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531039">
		<article id="comment-531039" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/0ece9b48a4df4f52f5d2abc1fee0da2c?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">ravnos</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531039"><time pubdate datetime="2012-03-23T04:21:02+00:00">March 23, 2012 at 4:21 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>so use this: <a href="http://bugs.jquery.com" rel="nofollow">http://bugs.jquery.com</a></p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531041">
		<article id="comment-531041" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/7b2b0f388e64b9a75879ed7a4ba6effb?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Shawn</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531041"><time pubdate datetime="2012-03-23T04:36:21+00:00">March 23, 2012 at 4:36 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Then the &#8216;Bug Tracker&#8217; link in jquery home page need to be updated.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531050">
		<article id="comment-531050" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/1b1f4fb734fe04cf050b572f127de520?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://pixabay.com' rel='external nofollow' class='url'>Simon</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531050"><time pubdate datetime="2012-03-23T17:29:24+00:00">March 23, 2012 at 5:29 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Great work! Thanks a lot guys!!! I love jQuery!</p>
<p>I&#8217;m really impressed you still keep up the work for IE6, which is banned or simply ignored on most modern websites. I remember spending day after day optimizing websites for IE6. Code got blown up and time wasted. Wouldn&#8217;t it actually make sense to quit fixing and working on IE6 for jQuery?</p>
<p>Cheers,<br />
Simon</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531058">
		<article id="comment-531058" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/238a38aa9d3ecb16a9c6ca4aada980a2?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.camronbute.com' rel='external nofollow' class='url'>Camron Bute</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531058"><time pubdate datetime="2012-03-24T00:11:55+00:00">March 24, 2012 at 12:11 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Thank you, everyone. We all appreciate the work you put into this, and another release is always a good time. Congratulations on a great product that&#8217;s made programming easier for everyone.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531083">
		<article id="comment-531083" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/d79a6583a4103119b0d241cf6ab13567?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://defaulttricks.com' rel='external nofollow' class='url'>Mohit Bumb</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531083"><time pubdate datetime="2012-03-25T09:32:54+00:00">March 25, 2012 at 9:32 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>helpful article for me<br />
thanks jquery team</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531101">
		<article id="comment-531101" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/1bb60a8759f2aee420d8bfe9c608fd95?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Tim</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531101"><time pubdate datetime="2012-03-26T11:32:00+00:00">March 26, 2012 at 11:32 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>When can we expect to see the updated NuGet package? I&#8217;ve checked the history and it appears that there is no consistency to when jQuery is published to NuGet.</p>
<p>I would like to make a couple of requests:<br />
1. Create a process to ensure the NuGet package gets updated as soon as possible after each new release.<br />
2. Publish betas and release candidates on NuGet (using the -Pre parameter to install the latest pre release). This will help devs to more easily obtain pre release versions of jQuery.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531265">
		<article id="comment-531265" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/dc6b999124525346e68797a791b01fb9?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Nitin Gautam</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531265"><time pubdate datetime="2012-04-03T09:03:54+00:00">April 3, 2012 at 9:03 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Is it completely backward compatible for 1.5 version?</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531280">
		<article id="comment-531280" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/a9542aff0801aa73e187ee6585756c3b?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Teg</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531280"><time pubdate datetime="2012-04-04T12:53:42+00:00">April 4, 2012 at 12:53 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>It depends on what you coded and if API elements were changed around or depreciated.</p>
<p>First check out the changes from 1.5 to 1.6 at <a href="http://blog.jquery.com/2011/05/03/jquery-16-released/" rel="nofollow">http://blog.jquery.com/2011/05/03/jquery-16-released/</a> and then the 1.6 to 1.7 changes at <a href="http://blog.jquery.com/2011/11/03/jquery-1-7-released/" rel="nofollow">http://blog.jquery.com/2011/11/03/jquery-1-7-released/</a> to see if you need to change your code before making the switch.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531302">
		<article id="comment-531302" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/92e0dc39c7665793e8c932f3bec72627?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Alim</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531302"><time pubdate datetime="2012-04-05T21:54:37+00:00">April 5, 2012 at 9:54 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Please</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531310">
		<article id="comment-531310" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/c58ca7527a60c16de44a0759ce8fdae1?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.keyhealth.net' rel='external nofollow' class='url'>Tony Grewal</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531310"><time pubdate datetime="2012-04-06T12:01:44+00:00">April 6, 2012 at 12:01 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>We are using CRM 2011 on Windows. An asynchronous AJAX JQuery call is made using datatype JSON to get data from the server. Under XP and IE8 there is no issue. Under Windows 7 and IE9 there are problems. I believe json2.js and jQuery 1.64 are being used. Was this a known issue with these versions?</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531311">
		<article id="comment-531311" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/c58ca7527a60c16de44a0759ce8fdae1?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.keyhealth.net' rel='external nofollow' class='url'>Tony Grewal</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531311"><time pubdate datetime="2012-04-06T12:03:36+00:00">April 6, 2012 at 12:03 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>The issue is that the page freezes at times when certain drop down boxes are being populated from a selection made in another drop down box.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531365">
		<article id="comment-531365" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/c9f6bdc53f65b5c6f87be4b8aee78777?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Robert Wildling</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531365"><time pubdate datetime="2012-04-10T06:39:38+00:00">April 10, 2012 at 6:39 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Concerning the &#8220;do not post bugs here in the comments&#8221;: I actually think that errors observed with the new release SHOULD be posted here. The resulting &#8220;overview&#8221; is much for us to decide whether the update could possibly break an already running solution. (Of course, the bug report page has still to be fed with infos&#8230;). Therefore thanks to everyone who takes the time to post their experiences here! &#8211; Just my 2 cent&#8230;</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531463">
		<article id="comment-531463" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/2fab0462e97a9157ab47fa645cd90eb8?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Loque</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531463"><time pubdate datetime="2012-04-17T08:14:14+00:00">April 17, 2012 at 8:14 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>@Robert Wildling</p>
<p>Commenting !== (Discussion || bugtracking). If you honestly want to hear about issues then keep all bugs listed in the bug tracker where they can be tracked, including the discussion around them. If you need help with a particular bug, or something is causing you problems the chances are you will get better assistance on the forums or in the bugtracker. There is no better place for you to go when looking at implementing the new version than the bugtracking section.</p>
<p>Randomly reporting bugs here will mean you miss out on discussion around them, especially (as I would guess), many &#8220;bugs&#8221; that are reported, are not actually bugs.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531464">
		<article id="comment-531464" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/2fab0462e97a9157ab47fa645cd90eb8?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Loque</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531464"><time pubdate datetime="2012-04-17T08:31:30+00:00">April 17, 2012 at 8:31 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Saying all that, the bugtracker is infuriatingly slow/buggy itself, I hope it gets a buff sometime!</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531470">
		<article id="comment-531470" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/02d4d7b50c705f414f298529b441c7ae?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.mehulkanani.com' rel='external nofollow' class='url'>Mehul Kanani</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531470"><time pubdate datetime="2012-04-17T23:30:07+00:00">April 17, 2012 at 11:30 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>nice updates in jQuery 1.7.2.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531480">
		<article id="comment-531480" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/dde4566e588110f8cfc68e814924a9df?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://designsxtra.com' rel='external nofollow' class='url'>edy</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531480"><time pubdate datetime="2012-04-18T16:37:51+00:00">April 18, 2012 at 4:37 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I just wanna say,<br />
Thank you for developing this jquery.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531531">
		<article id="comment-531531" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/0f6c98d6ccc40359d82b6dc147aafbd9?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.kis-bahcesi.net' rel='external nofollow' class='url'>k?? bahçesi</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531531"><time pubdate datetime="2012-04-22T16:59:00+00:00">April 22, 2012 at 4:59 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>thank you</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531566">
		<article id="comment-531566" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/0d3c8fda571ea7a6bc028e65211627a9?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">onur</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531566"><time pubdate datetime="2012-04-26T03:31:32+00:00">April 26, 2012 at 3:31 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>wtf?<br />
whats this bok?</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531567">
		<article id="comment-531567" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/beac3b3ec707472212690ecb98de830a?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">abrutyte</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531567"><time pubdate datetime="2012-04-26T03:37:59+00:00">April 26, 2012 at 3:37 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>this is a shit it doesn&#8217;t work. F*** OFF</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531571">
		<article id="comment-531571" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/a47fa17d055978e6df69cc5f158728a3?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">kanwardeep singh</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531571"><time pubdate datetime="2012-04-26T13:34:33+00:00">April 26, 2012 at 1:34 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>please forward me the the tutorial for the Jquery.i am a begineer</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531573">
		<article id="comment-531573" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/2247b21641223533b0acae2c1bb24411?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.princepy.com' rel='external nofollow' class='url'>Prince</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531573"><time pubdate datetime="2012-04-27T06:56:22+00:00">April 27, 2012 at 6:56 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I was following jquery from 1.5, in my websites. recently I havejust updated with 1.7.2 in my development thinking that I can use latest features to make site work better, the site started breaking.</p>
<p>After analysing I found that the &#8220;attr&#8221; function is the problem center.<br />
if I switch back to 1.7.1 jquery the code<br />
$(this).attr(&#8220;for&#8221;) =&gt; will give me &#8220;divMain&#8221;<br />
and if I switch to 1.7.2 the same code is giving as [object].</p>
<p>I have downloaded the development version of both 1.7.1 &amp; 1.7.2 and found that the problem is in jquery 1.7.2.js line number 2227 &#8220;return jQuery.access( this, jQuery.attr, name, value, arguments.length &gt; 1 );&#8221;. I found out that for a code &#8220;$(this).attr(&#8216;for&#8217;)&#8221; the arguments.length = 3, again I looks at arguments.length regardless of whether the last argument was undefined, so if you make a function that passes all of it&#8217;s arguments to .attr, it would fail. </p>
<p>May be when some other js code override attr function, it may be always send arguments more the one, then code&#8217;s behind: return jQuery.access( this, jQuery.attr, name, value, arguments.length &gt; 1 ); will go not correctly, it deal as chainable. the other auguments will be undefined. it has no problem in jquery version 1.6.2. i don&#8217;t know if it should let other changing their code or change the jquery core. </p>
<p>So what am saying is that &#8220;with out any change in code of file in my project, if I Switch the the new verion of jquery (1.7.2). the site breaks. So their are some very minner problems.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531586">
		<article id="comment-531586" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/ab886e8df9b24229b6c4fb2c0302569a?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.phplinkdirectory.com/' rel='external nofollow' class='url'>David</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531586"><time pubdate datetime="2012-04-30T01:54:28+00:00">April 30, 2012 at 1:54 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>We are upgrading from 1.5. I saw a couple of possible backwards compatibility notes in 1.6. Is there any other information about backwards compatibility issues?</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531657">
		<article id="comment-531657" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/b927a4653e3cc89085ca3130caed840c?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.opisynagg.net' rel='external nofollow' class='url'>Opisy</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531657"><time pubdate datetime="2012-05-11T10:19:05+00:00">May 11, 2012 at 10:19 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I&#8217;ve upgraded from 1.6 and i can say 1.7 .2 works a lot better.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531669">
		<article id="comment-531669" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/3d77558ce85511bc7f4eb88b9f305b0e?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://unihostbrasil.com.br' rel='external nofollow' class='url'>Unihost Brasil Webhosting</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531669"><time pubdate datetime="2012-05-13T21:22:08+00:00">May 13, 2012 at 9:22 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>We&#8217;ve updated with success all our javascript apps to jQuery 1.7.2. Thanks for this new version.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-531802">
		<article id="comment-531802" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/6cb01836db99a3abceeed274a5b194cd?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://n/a' rel='external nofollow' class='url'>anon</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531802"><time pubdate datetime="2012-06-13T13:10:18+00:00">June 13, 2012 at 1:10 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>how do you download the jquery-1.7.2.min.js</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-531808">
		<article id="comment-531808" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/ae7869d5b604b881eec054518dbb74a3?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='https://www.llnl.gov' rel='external nofollow' class='url'>Shelane French</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-531808"><time pubdate datetime="2012-06-18T16:05:05+00:00">June 18, 2012 at 4:05 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>So why is it that the jQuery site is using different versions of jQuery in different places. Shouldn&#8217;t the site be using the latest?</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-532021">
		<article id="comment-532021" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/febf69b1376476ac4315752ec8d1f71c?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">John Guilbert</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532021"><time pubdate datetime="2012-07-04T10:04:35+00:00">July 4, 2012 at 10:04 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>sorry I meant to say &#8220;when I use Jquery 1.7.2&#8243;</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-532176">
		<article id="comment-532176" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/de48ed5045aea0afdd2be7d1b1cee2ad?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Colb</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532176"><time pubdate datetime="2012-07-18T13:01:18+00:00">July 18, 2012 at 1:01 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>The minified version link doens&#8217;t work :S (<a href="http://code.jquery.com/jquery-1.7.2.min.js" rel="nofollow">http://code.jquery.com/jquery-1.7.2.min.js</a>)<br />
Can you solve this problem?</p>
<p>Thanks!</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-532177">
		<article id="comment-532177" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/de48ed5045aea0afdd2be7d1b1cee2ad?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Colb</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532177"><time pubdate datetime="2012-07-18T13:14:39+00:00">July 18, 2012 at 1:14 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>It works now! :)</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-532367">
		<article id="comment-532367" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/1755e423b4d41490a4524c1579a6b61e?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Confused</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532367"><time pubdate datetime="2012-08-02T15:02:47+00:00">August 2, 2012 at 3:02 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>So why are people reporting bugs in comments if it clearly states in the blog not to? It even said &#8220;please&#8221;.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-532442">
		<article id="comment-532442" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/dcb873634fea271f1cf439c5616e29d0?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Ashutosh</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532442"><time pubdate datetime="2012-08-09T00:50:07+00:00">August 9, 2012 at 12:50 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I am using jQuery JavaScript Library v1.4.4. If I will upgrade to latest version 1.7.2, will it impact to my website.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-532443">
		<article id="comment-532443" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/dcb873634fea271f1cf439c5616e29d0?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn">Query</span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532443"><time pubdate datetime="2012-08-09T00:52:40+00:00">August 9, 2012 at 12:52 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I am using older version of jquery. Any impact on upgrading ?</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-532681">
		<article id="comment-532681" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/0b16ca81a5b1e5c3185a4b2935db7a02?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.ashwinramya.com' rel='external nofollow' class='url'>Ashwin</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-532681"><time pubdate datetime="2012-08-17T15:44:31+00:00">August 17, 2012 at 3:44 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>For folks upgrading from jQuery 1.4.4 to 1.7.2 follow the below links and make sure each and every backward incompatible change is addressed in your codebase.<br />
1.44 to 1.5 Changelog<br />
<a href="http://blog.jquery.com/2011/01/31/jquery-15-released/" rel="nofollow">http://blog.jquery.com/2011/01/31/jquery-15-released/</a></p>
<p>1.6 Changelog<br />
<a href="http://blog.jquery.com/2011/05/03/jquery-16-released/" rel="nofollow">http://blog.jquery.com/2011/05/03/jquery-16-released/</a></p>
<p>1.7 Changelog<br />
<a href="http://blog.jquery.com/2011/11/03/jquery-1-7-released/" rel="nofollow">http://blog.jquery.com/2011/11/03/jquery-1-7-released/</a></p>
<p>1.7.2 Changelog<br />
<a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/" rel="nofollow">http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/</a></p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-533098">
		<article id="comment-533098" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/75bf108f5cb9992d9f846b2fd08c7796?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.parakraminfotec.com' rel='external nofollow' class='url'>Prasath</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-533098"><time pubdate datetime="2012-09-07T00:20:37+00:00">September 7, 2012 at 12:20 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>If I use jquery, my ajax request and response(with XML) not working in all browser. any one can help me</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-533949">
		<article id="comment-533949" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/82cdf1cc58816dd8a28474b5052a9b65?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.cleveres-webdesign.de' rel='external nofollow' class='url'>Cleveres Webdesign</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-533949"><time pubdate datetime="2012-10-08T08:07:42+00:00">October 8, 2012 at 8:07 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I had some issues with the current version 1.8.2 &#8211; some older codes did not work the way they should. Guess I should have a look at the links Ashwin posted some posts before. Some people told me that this (1.7.2) is the most stable version. But maybe they also do not know the important changes to the newest version&#8230;</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-534297">
		<article id="comment-534297" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/6c8bad083a9e36a29997c238856a4dc3?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.windows8release.com' rel='external nofollow' class='url'>Windows 8 Release</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-534297"><time pubdate datetime="2012-10-19T11:57:58+00:00">October 19, 2012 at 11:57 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Thanks for the quick updates jQuery team. It has been only about 6 months since this 1.7.2 update and your team has done an amazing job to release the 1.8.2 version within such a short period. But it often creates a problem where the updates are too fast to keep up with the pace. So, is there a page with all the historical changelog in jQuery versions so that we can track them better at one place.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-534461">
		<article id="comment-534461" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/ba099772264707c04e9ef7bf7a03b1fb?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://perdeledraperiistoruri.ro' rel='external nofollow' class='url'>PerdeleDraperiiStoruri</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-534461"><time pubdate datetime="2012-10-29T04:09:02+00:00">October 29, 2012 at 4:09 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>it&#8217;s not like the others users said.it&#8217;s very easy to download and the bugs can be fixed with the bug tracker.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-534471">
		<article id="comment-534471" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/6719e9b6660253c90adcf9e3d3783525?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://aerconditionat.info.ro' rel='external nofollow' class='url'>Alexir</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-534471"><time pubdate datetime="2012-11-01T06:02:51+00:00">November 1, 2012 at 6:02 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>I have a problem too with the bugs and the bug tracker isn&#8217;t working for me.We should be able to post in our comments this issue so you could help us.</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-534505">
		<article id="comment-534505" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/887e63f197393528735f31deffe83476?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.canapele.info.ro' rel='external nofollow' class='url'>Canapele</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-534505"><time pubdate datetime="2012-11-04T10:57:35+00:00">November 4, 2012 at 10:57 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Helpful article!You made it a lot easier for me.Thank you jQuery!</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-535053">
		<article id="comment-535053" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/fccfffcb018fc855c6cd36384dcb19ac?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://www.dailysms.com' rel='external nofollow' class='url'>send sms</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-535053"><time pubdate datetime="2012-12-09T12:51:35+00:00">December 9, 2012 at 12:51 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>Can&#8217;t fault JQuery for web forms keep up the good work devs!</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-537413">
		<article id="comment-537413" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://0.gravatar.com/avatar/47c7999841ee466eaa1c4f9933b1af45?s=68&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://s3komputer.com/' rel='external nofollow' class='url'>toko komputer online</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-537413"><time pubdate datetime="2013-03-03T00:46:08+00:00">March 3, 2013 at 12:46 am</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>jQuery is very helpful, thank to its creator, your writing give me another insight about jQuery</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
	<li class="comment even thread-even depth-1" id="li-comment-539624">
		<article id="comment-539624" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<img alt='' src='http://1.gravatar.com/avatar/feb33ed1ca09bc74f6688e6fb5536aa1?s=68&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D68&amp;r=G' class='avatar avatar-68 photo' height='68' width='68' /><span class="fn"><a href='http://giriparket.com' rel='external nofollow' class='url'>harga lantai kayu</a></span> on <a href="http://blog.jquery.com/2012/03/21/jquery-1-7-2-released/comment-page-1/#comment-539624"><time pubdate datetime="2013-04-16T23:17:55+00:00">April 16, 2013 at 11:17 pm</time></a> <span class="says">said:</span>
									</div><!-- .comment-author .vcard -->

				
			</div>

			<div class="comment-content"><p>very helpful program easy to use thanks for the creator</p>
</div>

			<div class="reply">
							</div><!-- .reply -->
		</article><!-- #comment-## -->

	</li>
		</ol>

		
	
									
</div><!-- #comments -->
	</div>
	<div id="sidebar" class="widget-area" role="complementary">
	<aside class="widget">
		<h3 class="widget-title">Categories</h3>
		<ul>
				<li class="cat-item cat-item-16"><a href="http://blog.jquery.com/category/events/" title="View all posts filed under Events">Events</a>
</li>
	<li class="cat-item cat-item-20"><a href="http://blog.jquery.com/category/foundation/" title="View all posts filed under Foundation">Foundation</a>
</li>
	<li class="cat-item cat-item-3"><a href="http://blog.jquery.com/category/jquery/" title="View all posts filed under jQuery">jQuery</a>
</li>
	<li class="cat-item cat-item-2"><a href="http://blog.jquery.com/category/jquery-ui/" title="View all posts filed under jQuery UI">jQuery UI</a>
</li>
	<li class="cat-item cat-item-4"><a href="http://blog.jquery.com/category/weekly-news/" title="View all posts filed under Weekly News">Weekly News</a>
</li>
		</ul>
	</aside>
	<aside class="widget">
		<h3 class="widget-title">Recent Posts</h3>
		<ul>
			<li><a href="http://blog.jquery.com/2013/10/09/jquery-conference-set-to-roost-in-san-diego/">jQuery Conference Set to Roost in San Diego</a></li><li><a href="http://blog.jquery.com/2013/09/19/jquery-1-11-and-2-1-beta-1-released/">jQuery 1.11 and 2.1 Beta 1 Released</a></li><li><a href="http://blog.jquery.com/2013/08/06/jquery-austin-speaker-lineup/">jQuery Austin Speaker Lineup</a></li><li><a href="http://blog.jquery.com/2013/07/03/jquery-heads-to-austin/">jQuery Heads to Austin</a></li><li><a href="http://blog.jquery.com/2013/07/03/jquery-1-10-2-and-2-0-3-released/">jQuery 1.10.2 and 2.0.3 Released</a></li><li><a href="http://blog.jquery.com/2013/06/10/live-from-portland-its-jquery/">Live From Portland, It's jQuery!</a></li><li><a href="http://blog.jquery.com/2013/05/30/jquery-1-10-1-and-2-0-2-released/">jQuery 1.10.1 and 2.0.2 released</a></li><li><a href="http://blog.jquery.com/2013/05/24/jquery-1-10-0-and-2-0-1-released/">jQuery 1.10.0 and 2.0.1 Released</a></li><li><a href="http://blog.jquery.com/2013/05/10/jquery-portland-update/">jQuery Portland Update</a></li><li><a href="http://blog.jquery.com/2013/05/09/jquery-1-10-beta-1-released/">jQuery 1.10 Beta 1 Released</a></li>		</ul>
	</aside>
	<aside class="widget">
		<h3 class="widget-title">Archives</h3>
		<ul>
				<li><a href='http://blog.jquery.com/2013/10/' title='October 2013'>October 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/09/' title='September 2013'>September 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/08/' title='August 2013'>August 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/07/' title='July 2013'>July 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/06/' title='June 2013'>June 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/05/' title='May 2013'>May 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/04/' title='April 2013'>April 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/03/' title='March 2013'>March 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/02/' title='February 2013'>February 2013</a></li>
	<li><a href='http://blog.jquery.com/2013/01/' title='January 2013'>January 2013</a></li>
	<li><a href='http://blog.jquery.com/2012/12/' title='December 2012'>December 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/11/' title='November 2012'>November 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/10/' title='October 2012'>October 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/09/' title='September 2012'>September 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/08/' title='August 2012'>August 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/07/' title='July 2012'>July 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/06/' title='June 2012'>June 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/05/' title='May 2012'>May 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/04/' title='April 2012'>April 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/03/' title='March 2012'>March 2012</a></li>
	<li><a href='http://blog.jquery.com/2012/01/' title='January 2012'>January 2012</a></li>
	<li><a href='http://blog.jquery.com/2011/12/' title='December 2011'>December 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/11/' title='November 2011'>November 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/10/' title='October 2011'>October 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/09/' title='September 2011'>September 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/08/' title='August 2011'>August 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/06/' title='June 2011'>June 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/05/' title='May 2011'>May 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/04/' title='April 2011'>April 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/03/' title='March 2011'>March 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/02/' title='February 2011'>February 2011</a></li>
	<li><a href='http://blog.jquery.com/2011/01/' title='January 2011'>January 2011</a></li>
	<li><a href='http://blog.jquery.com/2010/12/' title='December 2010'>December 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/11/' title='November 2010'>November 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/10/' title='October 2010'>October 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/09/' title='September 2010'>September 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/08/' title='August 2010'>August 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/07/' title='July 2010'>July 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/06/' title='June 2010'>June 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/04/' title='April 2010'>April 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/03/' title='March 2010'>March 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/02/' title='February 2010'>February 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/01/' title='January 2010'>January 2010</a></li>
	<li><a href='http://blog.jquery.com/2009/12/' title='December 2009'>December 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/10/' title='October 2009'>October 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/08/' title='August 2009'>August 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/07/' title='July 2009'>July 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/06/' title='June 2009'>June 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/05/' title='May 2009'>May 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/04/' title='April 2009'>April 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/03/' title='March 2009'>March 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/02/' title='February 2009'>February 2009</a></li>
	<li><a href='http://blog.jquery.com/2009/01/' title='January 2009'>January 2009</a></li>
	<li><a href='http://blog.jquery.com/2008/12/' title='December 2008'>December 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/11/' title='November 2008'>November 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/10/' title='October 2008'>October 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/09/' title='September 2008'>September 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/08/' title='August 2008'>August 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/07/' title='July 2008'>July 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/06/' title='June 2008'>June 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/05/' title='May 2008'>May 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/03/' title='March 2008'>March 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/02/' title='February 2008'>February 2008</a></li>
	<li><a href='http://blog.jquery.com/2008/01/' title='January 2008'>January 2008</a></li>
	<li><a href='http://blog.jquery.com/2007/12/' title='December 2007'>December 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/11/' title='November 2007'>November 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/10/' title='October 2007'>October 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/09/' title='September 2007'>September 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/08/' title='August 2007'>August 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/07/' title='July 2007'>July 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/06/' title='June 2007'>June 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/05/' title='May 2007'>May 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/04/' title='April 2007'>April 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/03/' title='March 2007'>March 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/02/' title='February 2007'>February 2007</a></li>
	<li><a href='http://blog.jquery.com/2007/01/' title='January 2007'>January 2007</a></li>
	<li><a href='http://blog.jquery.com/2006/12/' title='December 2006'>December 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/11/' title='November 2006'>November 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/10/' title='October 2006'>October 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/09/' title='September 2006'>September 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/08/' title='August 2006'>August 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/07/' title='July 2006'>July 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/06/' title='June 2006'>June 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/05/' title='May 2006'>May 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/04/' title='April 2006'>April 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/03/' title='March 2006'>March 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/02/' title='February 2006'>February 2006</a></li>
	<li><a href='http://blog.jquery.com/2006/01/' title='January 2006'>January 2006</a></li>
		</ul>
	</aside>
</div>
</div>

	</div>
</div>

<footer class="clearfix simple">
	<div class="constrain">
		<div class="row">
			<div class="eight columns">
				<h3><span>Quick Access</span></h3>
				<div class="cdn">
					<strong>CDN</strong>
					<input value="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
				</div>
				<div class="download">
					<strong><a href="http://jquery.com/download/">Download jQuery 1.10.2 →</a></strong>
				</div>
				<ul class="footer-icon-links">
					<li><a class="icon-github" href="http://github.com/jquery/jquery">GitHub <small>jQuery <br>Source</small></a></li>
					<li><a class="icon-group" href="http://forum.jquery.com">Forum <small>Community <br>Support</small></a></li>
					<li><a class="icon-warning-sign" href="http://bugs.jquery.com">Bugs <small>Issue <br>Tracker</small></a></li>
				</ul>
			</div>

			<div class="four columns">
				<h3><span>Books</span></h3>
				<ul class="books">
					<li>
						<a href="http://link.packtpub.com/S3Fr9Q">
							<span class="bottom"><img src="http://blog.jquery.com/wp-content/themes/jquery/content/books/learning-jquery-3rd-ed.jpg" alt="Learning jQuery 3rd Edition by Karl Swedberg and Jonathan Chaffer" width="92" height="114" /></span>
							<strong>Learning jQuery Third Edition</strong><br />
							<cite>Karl Swedberg and Jonathan Chaffer</cite>
						</a>
					</li>
					<li>
						<a href="http://www.manning.com/affiliate/idevaffiliate.php?id=648_176">
							<span><img src="http://blog.jquery.com/wp-content/themes/jquery/content/books/jquery-in-action.jpg" alt="jQuery in Action by Bear Bibeault and Yehuda Katz" width="92" height="114" /></span>
							<strong>jQuery in Action</strong><br />
							<cite>Bear Bibeault and Yehuda Katz</cite>
						</a>
					</li>
					<li>
						<a href="http://www.syncfusion.com/resources/techportal/ebooks/jquery?utm_medium=BizDev-jQuery.org0513">
							<span><img src="http://blog.jquery.com/wp-content/themes/jquery/content/books/jquery-succinctly.jpg" alt="jQuery Succinctly by Cody Lindley" width="92" height="114" /></span>
							<strong>jQuery Succinctly</strong><br />
							<cite>Cody Lindley</cite>
						</a>
					</li>
				</ul>
			</div>
		</div>

		<div id="legal">
			<ul class="footer-site-links">
				<li><a class="icon-pencil" href="http://learn.jquery.com/">Learning Center</a></li>
				<li><a class="icon-group" href="http://forum.jquery.com/">Forum</a></li>
				<li><a class="icon-wrench" href="http://api.jquery.com/">API</a></li>
				<li><a class="icon-twitter" href="http://twitter.com/jquery">Twitter</a></li>
				<li><a class="icon-comments" href="http://irc.jquery.org/">IRC</a></li>
			</ul>
			<p class="copyright">
				Copyright 2013 <a href="https://jquery.org/team/">The jQuery Foundation</a>.<br>
				<span class="sponsor-line"><a href="http://mediatemple.net" rel="noindex,nofollow" class="mt-link">Web hosting by Media Temple</a> | <a href="http://www.maxcdn.com" rel="noindex,nofollow" class="mc-link">CDN by MaxCDN</a> | <a href="http://wordpress.org/" class="wp-link">Powered by WordPress</a> | Thanks: <a href="https://jquery.org/members/">Members</a>, <a href="https://jquery.org/sponsors/">Sponsors</a></span>
			</p>
		</div>
	</div>
</footer>

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-1076265-1']);
    _gaq.push(['_setDomainName', 'blog.jquery.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: http://www.w3-edge.com/wordpress-plugins/
932.142.212.85
Page Caching using disk (enhanced)

Served from: blog.jquery.com @ 2013-10-31 02:44:33 -->