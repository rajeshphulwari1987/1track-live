﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RailPasses.aspx.cs" MasterPageFile="~/Site.master"
    Culture="en-GB" Inherits="RailPasses" %>

<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        li p
        {
            line-height: 20px;
        }
        .clsRailPassName
        {
            float: left !important;
            background: none !important;
            padding: 0px !important;
            font-size: 17px !important;
        }
        .clsRailPassDesp
        {
            background: none !important;
            font-size: 13px !important;
            color: #4d4d4d !important;
            padding: 0px !important;
        }
        .clsRpDesp
        {
            height: 100px;
            margin-top: 5px;
        }
        .clsImgSize
        {
            width: 210px;
            height: 120px;
        }
        .countrylist li { font-style: normal!important;}
        .countrylist li b{ font-weight: bold!important;}
        .countrylist b{ font-weight: normal!important;}
        .smlblock {font-style: normal!important;}
        
        #siteFooter{font-style: normal !important;}
        .right-content{font-style: normal !important;}
        strong, b #siteFooter{font-weight: normal !important;}
        .clsRpDesp strong, b{font-weight: bold !important;}
    </style>
    <script type="text/javascript">

        $(function () {
            $('div').mouseover(function () {
                callthisevent();
                if ($("#txtFrom").val() != '') {
                    //                    alert(localStorage.getItem("spantxtTo"));
                    $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
                }
                if ($("#txtTo").val() != '') {
                    //                    alert(localStorage.getItem("spantxtFrom"));
                    $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
                }
            });
        });


        function callthisevent() {
            $("#txtFrom , #txtTo").on('keydown', function (event) {
                //40,38
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;

                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Rail Passes  </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Rail Passes</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content " onmousemove="LoadCal()">
<h1>Rail Passes</h1>
<p>
    Below is a list of all the rail passes that we offer around the world, pick a pass and start your journey!
</p>
<br/>
     <div class="RailPasslist countrylistRp">
  
     <asp:Repeater ID="rptCat" runat="server" OnItemCreated="rptCat_OnItemCreated">
      <ItemTemplate>
          <a href="<%#Eval("Url") %>"> <%#Eval("Name")%></a>
      </ItemTemplate>
  </asp:Repeater>  

</div> 
<div class="clear"> &nbsp;</div> 
<div class="smlblock">
    <img id="imgRail1" src="images/NoproductImage.png" runat="server" alt="" border="0" class="clsImgSize"/>
    <a href="RailPassSection1.aspx" style="text-decoration: none"><h1> <asp:Label ID="lblRailTitle1" CssClass="clsRailPassName" Text="RailPass1" runat="server"/> </h1>
    <div class="clsRpDesp">
        <p> <asp:Label ID="lblRailDesp1" runat="server"/> </p></div></a>
    <span>
    <a href="RailPassSection1.aspx"> Read more </a>
    </span>
</div>
<div class="smlblock marg-lr">
    <img id="imgRail2" src="images/NoproductImage.png" runat="server" alt="" border="0" class="clsImgSize"/>
    <a href="RailPassSection2.aspx" style="text-decoration: none"><h1> <asp:Label ID="lblRailTitle2" CssClass="clsRailPassName" Text="RailPass2" runat="server"/>  </h1>
    <div class="clsRpDesp">
        <p> <asp:Label ID="lblRailDesp2" CssClass="clsRailPassDesp" runat="server"/> </p></div></a>
    <span>
    <a href="RailPassSection2.aspx"> Read more </a>
    </span>
</div>
<div class="smlblock">
    <img id="imgRail3" src="images/NoproductImage.png" runat="server" alt="" border="0" class="clsImgSize"/>
    <a href="RailPassSection3.aspx" style="text-decoration: none"><h1> <asp:Label ID="lblRailTitle3" CssClass="clsRailPassName" Text="RailPass2" runat="server"/> </h1>
    <div class="clsRpDesp">
    <p> <asp:Label ID="lblRailDesp3" CssClass="clsRailPassDesp" runat="server"/> </p></div></a>
    <span >
    <a href="RailPassSection3.aspx"> Read more </a>
    </span>
</div>
</div>
 <div class="right-content" > 
<div class="ticketbooking" style="padding-top:0px" onclick="callthisevent()">
    <div class="list-tab divEnableP2P">
        <ul>
            <li><a href="#" class="active">Rail Tickets </a></li>
            <li><a href="rail-passes">Rail Passes </a></li>
        </ul>
    </div>
<uc1:TrainSearch ID="ucTrainSearch" runat="server" />
</div>
 
</div> 
</section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
