﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using Business;

public partial class Pages : Page
{
    readonly Masters _master = new Masters();
    readonly private ManageCMSPage _masterPage = new ManageCMSPage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid siteId;
    public string script;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(siteId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Guid id = new Guid();
        if (Page.RouteData.Values["id"] != null)
            id = (Guid)Page.RouteData.Values["id"];
        else if (Request.QueryString["id"] != null)
        {
            id = Guid.Parse(Request.QueryString["id"].ToString());
            string RD_URl = PageUrls.GetRedirectURL(Convert.ToString(Request.QueryString["id"].ToString()));
            Response.Redirect(RD_URl);
        }


        var list = _db.tblPagesCMS.FirstOrDefault(ty => ty.SiteId == siteId && ty.ID == id);
        if (list != null)
        {
            Widget.Visible = list.IsWidgetVisible;
            headerTitle.InnerText = list.Name;
            divDescription.InnerHtml = Server.HtmlDecode((list.Description));
            dvContent.Visible = true;
            imgGinfo.Src = ConfigurationManager.AppSettings["HttpAdminHost"] + list.Image;
        }
        BindBanner(id);

        if (!Page.IsPostBack)
            script = new Masters().GetQubitScriptBySId(siteId);
    }

    public void BindBanner(Guid pageId)
    {
        var list = _masterPage.GetBannerImgByID(pageId);
        rptBanner.DataSource = list;
        rptBanner.DataBind();
    }
}