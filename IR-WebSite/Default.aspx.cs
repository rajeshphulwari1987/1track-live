﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
#endregion

public partial class _Default : Page
{
    private readonly Masters _master = new Masters();
    private readonly ManageJourney _objJourney = new ManageJourney();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string script = "<script></script>";
    Guid pageID, siteId;
    public static string currency;
    public string frontSiteUrl;

    #region PageLoad Events
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(siteId);
        dvRight.Visible = _master.IsVisibleRightPanelImages(siteId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["PageId"] != null)
                {
                    pageID = (Guid)Page.RouteData.Values["PageId"];
                }
                else
                {
                    var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.Contains("home"));
                    if (pid != null)
                        pageID = pid.ID;
                }

                PageContent(pageID, siteId);
                Page.Header.DataBind();

                /* One Hub station List Create method*/
                // CreateStationXml();
                GetCurrency();

                ShowTopJourney(siteId);
                ShowP2PWidget(siteId);
                BindTopJourneys(siteId);
                BindPrormotion(siteId);
                LoadTopPasses(siteId);
                QubitOperationLoad();
                frontSiteUrl = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("errorpage.aspx");
        }
    }
    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void GetCurrency()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (result != null)
        {
            var currId = result.DefaultCurrencyID;
            if (currId != null)
                currency = oManageClass.GetCurrency(Guid.Parse(currId.ToString()));
            else
                currency = "£";
        }
        else
            currency = "£";
    }

    void ShowP2PWidget(Guid siteID)
    {
        var enableP2P = _oWebsitePage.EnableP2P(siteID);
        if (!enableP2P)
        {
            //divTopJourney.Attributes.Add("style", "display:none");
            //divTopRail.Attributes.Add("style", "display:block");
            //TabTopJourney.Attributes.Add("style", "display:none");
            //TabTopRail.Attributes.Add("class", "btnactive");
        }
    }
    #endregion

    #region Load Top Passes
    void LoadTopPasses(Guid siteId)
    {
        rptPasses.DataSource = _objJourney.GetTopPassesList(siteId);
        rptPasses.DataBind();
    }
    #endregion

    #region Control Events
    #endregion

    #region UserDefined function
    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                if (list.Count > 0)
                {
                    rptBanner.DataSource = list;
                    rptBanner.DataBind();
                }
                else
                    dvBanner.Visible = false;

                //Right images
                string[] arrRtListId = oPage.RightPanelImgID.Split(',');
                var idRtList = (from item in arrRtListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();

                var listRt = _master.GetRightImgByID(idRtList);
                if (listRt.Count > 0)
                {
                    aRightPanelUrl.HRef = oPage.RightPanelNavUrl;
                    rptRtImg.DataSource = listRt;
                    rptRtImg.DataBind();
                }
                else
                    dvRight.Visible = false;

                ContentHead.InnerHtml = oPage.PageHeading;
                ContentText.InnerHtml = oPage.PageContent;
                footerBlock.InnerHtml = oPage.FooterImg.Replace("CMSImages", SiteUrl + "CMSImages");
                if (!string.IsNullOrEmpty(oPage.FooterHeader))
                    FooterHeader.InnerHtml = oPage.FooterHeader;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri,
                                 ex.Message + "; Inner Exception:" +
                                 (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void ShowTopJourney(Guid siteID)
    {
        var istopJourney = _objJourney.IsTopJourney(siteID);
        if (istopJourney != null)
            if (istopJourney)
            {
                divTopRail.Attributes.Add("style", "display:none");
                TabTopJourney.Attributes.Add("class", "btnactive");
                TabTopRail.Attributes.Add("class", "btnInactive");
            }
            else
            {
                divTopJourney.Attributes.Add("style", "display:none");
                TabTopJourney.Attributes.Add("class", "btnInactive");
                TabTopRail.Attributes.Add("class", "btnactive");
            }
    }

    public void BindTopJourneys(Guid siteID)
    {
        var result = _objJourney.GetTopJourneyBySiteID(siteID).OrderBy(x => x.SortOrder).ThenBy(x => x.From).ToList();
        rptJourney.DataSource = result;
        rptJourney.DataBind();
    }

    public void BindPrormotion(Guid siteID)
    {
        var result = _master.GetEurailPromotion(siteID);
        rptEurailPromotion.DataSource = result.Where(t => t.IsActive == true).OrderBy(t => t.sortorder).ThenBy(t => t.Title).ToList();
        rptEurailPromotion.DataBind();
    }
    #endregion

    #region  One Hub Implementation
    /// <summary>
    /// Create the station list XML.
    /// </summary>
    /// <returns></returns>
    public string[] CreateStationXml()
    {
        var clientCode = new OneHubRailOneHubClient();
        var stationList = clientCode.GetStationInfo().ToList();
        return BindXml(ReadObjectAsString(stationList));
    }
    public string ReadObjectAsString(object o)
    {
        var xmlS = new XmlSerializer(o.GetType());
        var sw = new StringWriter();
        var tw = new XmlTextWriter(sw);
        xmlS.Serialize(tw, o);
        return sw.ToString();
    }
    /// <summary>
    /// Binds the station XML list.
    /// </summary>
    /// <param name="strXml">The STR XML.</param>
    /// <returns></returns>
    public string[] BindXml(string strXml)
    {
        try
        {
            if (strXml != "")
            {
                var doc = new XmlDocument();
                doc.LoadXml(strXml);
                string filePath = Server.MapPath(@"StationList.xml");
                doc.Save(filePath);
                return null;
            }

            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    protected void rptJourney_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Book")
        {
            var divSearch = (HtmlGenericControl)ucTrainSearch.FindControl("divSearch");
            if (divSearch != null) divSearch.Attributes.Add("class", "tab-detail-in on-focusdiv");

            var enableP2P = _oWebsitePage.EnableP2P(siteId);
            if (!enableP2P)
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);

            var isVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            if (!isVisibleP2PWidget)
                imgShadow.Visible = false;

            var id = Guid.Parse(e.CommandArgument.ToString());
            var result = _objJourney.GetJourneyById(id);

            var txtFrm = (TextBox)ucTrainSearch.FindControl("txtFrom");
            var txtTo = (TextBox)ucTrainSearch.FindControl("txtTo");

            var ddldepTime = (DropDownList)ucTrainSearch.FindControl("ddldepTime");
            var ddlReturnTime = (DropDownList)ucTrainSearch.FindControl("ddlReturnTime");
            var ddlClass = (RadioButtonList)ucTrainSearch.FindControl("ddlClass");
            var ddlTransfer = (DropDownList)ucTrainSearch.FindControl("ddlTransfer");

            if (txtFrm != null && txtTo != null)
            {
                txtFrm.Text = result.From;
                txtFrm.Attributes.CssStyle.Add("background-color", "#ECFFFF");
                txtTo.Text = result.To;
                txtTo.Attributes.CssStyle.Add("background-color", "#ECFFFF");
            }

            if (ddldepTime != null)
                ddldepTime.SelectedValue = "09:00";
            if (ddlReturnTime != null)
                ddlReturnTime.SelectedValue = "09:00";
            if (ddlClass != null)
                ddlClass.SelectedValue = "0";
            if (ddlTransfer != null)
                ddlTransfer.SelectedValue = "3";
        }
        ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "Validationxx()", true);
    }
}