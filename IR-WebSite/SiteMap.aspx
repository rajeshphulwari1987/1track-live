﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="SiteMap" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> SiteMap </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">SiteMap</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>

<div class="left-content">
<h1>Sitemap</h1>
<p>Find your way around our site.</p>
<div class="clear"></div>
<p>
<asp:SiteMapDataSource id="siteMapdtSrc" runat="server" ShowStartingNode="False" />
<asp:TreeView ID="TreeView1" runat="server" CssClass="clsSMap" DataSourceId="siteMapdtSrc" ShowExpandCollapse="False">
    <NodeStyle Font-Names="Verdana" Font-Size="9pt"/>
    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
</asp:TreeView>
</p>
</div>

<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
