﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PaymentProcess.aspx.cs" Inherits="PaymentProcess" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Order/PaymentConfirmation.ascx" TagName="PaymentConfirmation" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        /*booking detail*/        
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
    </style>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="content">
        <div class="breadcrumb">
            <a href="Home">Home </a>>><a href="TrainResults">Train Results</a>>><a href="BookingCart">Booking
                Cart</a>>>Payment Details
        </div>
        <div class="left-content" style="width: 100%">
            <h1>
                Review Your Order
            </h1>
            <p>
                By placing your order, you agree to the product terms & conditions and our <a href='<%=siteURL%>PrivacyPolicy'>
                    Privacy Policy</a>, <a href='<%=siteURL%>ConditionsofUse'>Conditions of Use.</a>
            </p>
            <br />
            <div class="round-titles">
                Order Information
            </div>
            <div class="booking-detail-in">
                <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0"> 
                    <tr>
                        <td align="left" style="width:465px">
                            Email Address :
                        </td>
                        <td>
                            <asp:Label ID="lblEmailAddress" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td>
                            <b>Delivery Address</b>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblDeliveryAddress" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b>Order Summary:</b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Order Date:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderDate" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Order No.:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderNumber" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptOrderInfo" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td align="left">
                                    Journey
                                    <%# Container.ItemIndex + 1 %>:
                                </td>
                                <td>
                                    <%#Eval("ProductDesc")%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Name:
                                </td>
                                <td>
                                     <%#Eval("Title")%> <%#Eval("Name")%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Price:
                                </td>
                                <td>
                                    <%=currency %>
                                    <%#Eval("Price").ToString()%>
                                </td>
                            </tr>
                            <%#(Eval("TktPrtCharge")) != "" ? "<tr><td>Ticket Protection: </td><td>" + currency +" "+ Eval("TktPrtCharge")+"</td></tr>" : ""%>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td align="left">
                            Booking Fee:
                        </td>
                        <td>
                            <%=currency %>
                            <asp:Label ID="lblBookingFee" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Shipping:
                        </td>
                        <td>
                            <%=currency %>
                            <asp:Label ID="lblShippingAmount" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b>Total Price:</b>
                        </td>
                        <td>
                            <b>
                                <%=currency %>
                                <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label></b>
                        </td>
                    </tr>
                </table>
            </div>
           
            <div class="booking-detail-in" style="display: none;">
                <div class="colum-label">
                    Email address*
                </div>
                <div class="colum-input">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input" />
                </div>
                <div style="float: left; padding-left: 200px; width: 100%; height: 15px; font-size: 12px !important;">
                    <asp:RequiredFieldValidator ID="rfEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail"
                        ForeColor="red" ValidationGroup="vgs" />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid Email."
                        ValidationGroup="vgs" ControlToValidate="txtEmail" ForeColor="red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic" />
                </div>
                <div class="colum-label">
                    Password*
                </div>
                <div class="colum-input">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="input"
                        MaxLength="10" />
                    <asp:RequiredFieldValidator ID="rfPhn" runat="server" ErrorMessage="*" ControlToValidate="txtPassword"
                        ValidationGroup="vgs" ForeColor="red" />
                </div>
                <div style="clear: both;">
                </div>
                <asp:Label runat="server" ID="lblmsg" />
            </div>
            <div class="booking-detail-in" id="PaymentDiv" runat="server">
                <uc1:PaymentConfirmation ID="PaymentConfirmation1" runat="server" />
            </div>  
            <div id="DivButton" runat="server" >
                <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Style="width: 15%;
                    float: left;" Text="Back" />
                <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Style="width: 15%;
                    float: right; width: 184px;" Text="Proceed to payment" ToolTip="You will now be re-directed to our Payment Gateway, Ogone to pay for your order by credit or debit card. Once completed you will automatically return to our site to receive your order confirmation." />
                <asp:Button ID="btnAgentLogon" runat="server" OnClick="btnAgentLogon_Click" Style="width: 15%;
                    margin-right: 3px; float: right; width: 184px; background: none; background-color: Red;
                    border-color: #FF5959;" Text="Agent Logon" Visible="False" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnUserName" runat="server" />
    <script type="text/javascript">
        function newPopup(url) {
            popupWindow = window.open(
        url, 'popUpWindow', 'height=400,width=800,left=25%,top=25%,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
        }
    </script>
</asp:Content>
