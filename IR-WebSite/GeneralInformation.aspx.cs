﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;

public partial class GeneralInformation : System.Web.UI.Page
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    private Guid siteId;
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            trainresults.HRef = _db.tblSites.Any(x => x.ID == siteId && x.IsTrainTickets) ? siteURL + "traintickets" : siteURL + "trainresults";
            Newsletter1.Visible = _master.IsVisibleNewsLetter(siteId);
            divJRF.Visible = _master.IsVisibleJRF(siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var list = _db.tblGeneralInformation_.FirstOrDefault(ty => ty.SiteId == siteId);
        if (list != null)
        {
            headerTitle.InnerText = list.Name;
            divDescription.InnerHtml = Server.HtmlDecode((list.Description));
            imgGinfo.Src = ConfigurationManager.AppSettings["HttpAdminHost"] + list.Imagepath;
        }

        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(siteId);
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx")).ID;
            if (pageId != null)
                PageContent(pageId, siteId);
        }
    }
    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);

                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                rptBanner.DataSource = list;
                rptBanner.DataBind();
                footerBlock.InnerHtml = oPage.FooterImg.Replace("CMSImages", adminSiteUrl + "CMSImages");
                rtPannel.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
            else
            {
                divFooter.Visible = false;
                imgRtPanel.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}