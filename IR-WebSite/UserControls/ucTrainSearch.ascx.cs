﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;

public partial class UserControls_ucTrainSearch : UserControl
{

    public static string unavailableDates1 = "";
    Guid siteId;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string siteURL;
    public int minDate = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            ShowHaveRailPass(siteId);
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);
            unavailableDates1 = "[";
            if (siteDDates.Count() > 0)
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
        }

        if (!IsPostBack)
        {
            bool isUseSite = (bool)_oWebsitePage.IsUsSite(siteId);
            bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            divSearch.Visible = IsVisibleP2PWidget;
            if (!IsVisibleP2PWidget)
                ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('.ticketbooking img').hide();</script>", false);

            minDate = _oWebsitePage.GetBookingDayLimitBySiteId(siteId);
            rdBookingType.Items[1].Text = isUseSite ? "Round Trip" : "Return";
            for (int i = 10; i >= 0; i--)
            {
                ddlAdult.Items.Insert(0, new ListItem(i.ToString(CultureInfo.InvariantCulture), i.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }

            for (int j = 10; j >= 0; j--)
            {
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
            }

            if (Session["BookingUCRerq"] != null)
                SearchTrainInfoForFill();
        }
    }

    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        divRailPass.Visible = railPass;
    }

    void SetCallander()
    {
        if (ViewState["Rdo"] == "1")
        {
            txtReturnDate.Enabled = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "calenable()", true);
        }
        else
        {
            txtReturnDate.Enabled = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "caldisable()", true);
        }
        rdBookingType.SelectedValue = ViewState["Rdo"] == null ? "0" : ViewState["Rdo"].ToString();
    }

    public void SearchTrainInfo()
    {
        try
        {
            var objBruc = new BookingRequestUserControl();
            var objBooking = new ManageBooking();

            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(txtFrom.Text.Trim());
            objBruc.depstCode = objStationDeptDetail != null ? objStationDeptDetail.StationCode : string.Empty;
            objBruc.depRCode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
            ViewState["depRCode"] = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";

            GetStationDetailsByStationName objStationArrDetail = objBooking.GetStationDetailsByStationName(txtTo.Text.Trim());
            objBruc.arrstCode = objStationArrDetail != null ? objStationArrDetail.StationCode : string.Empty;
            objBruc.arrRCode = objStationArrDetail != null ? objStationArrDetail.RailwayCode : "0";
            if (objStationDeptDetail != null)
                objBruc.OneHubServiceName = objStationDeptDetail.RailName.Trim() == "BENE" ? "BeNe" : "Trenitalia";

            objBruc.FromDetail = txtFrom.Text.Trim();
            objBruc.ToDetail = txtTo.Text.Trim();
            objBruc.depdt = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null);
            objBruc.depTime = Convert.ToDateTime(ddldepTime.SelectedValue);

            objBruc.ClassValue = Convert.ToInt32(ddlClass.SelectedValue);
            objBruc.Adults = Convert.ToInt32(ddlAdult.SelectedValue);
            objBruc.Boys = Convert.ToInt32(ddlChild.SelectedValue);
            objBruc.Seniors = Convert.ToInt32(ddlSenior.SelectedValue);
            objBruc.Youths = Convert.ToInt32(ddlYouth.SelectedValue);
            objBruc.Transfare = Convert.ToInt32(ddlTransfer.SelectedValue);
            objBruc.ReturnDate = String.IsNullOrEmpty(txtReturnDate.Text) || txtReturnDate.Text.Trim() == "DD/MM/YYYY" ? string.Empty : txtReturnDate.Text;
            objBruc.ReturnTime = ddlReturnTime.SelectedValue;
            objBruc.Loyalty = chkIsLoyaltyActive.Checked;
            objBruc.isIhaveRailPass = chkIhaveRailPass.Checked;
            objBruc.Journeytype = rdBookingType.SelectedValue;

            if (rdBookingType.SelectedValue == "1")
                objBruc.IsReturnJurney = true;

            Session["BookingUCRerq"] = objBruc;
            var currDate = DateTime.Now;

            int daysLimit = GetEurostarBookingDays(objBruc);
            if (daysLimit == 0)
                daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);

            var maxDate = currDate.AddDays(daysLimit - 1);

            if (objBruc.depdt > maxDate && objStationDeptDetail != null)
            {
                Session["ErrorMessage"] = "ErrorMaxDate";
                Session["TrainSearch"] = null;
                Response.Redirect("TrainResults.aspx");
            }
            else
                Session["ErrorMessage"] = null;

            var client = new OneHubRailOneHubClient();
            TrainInformationRequest request = TrainInformation(objBruc, 1);



            #region Allow Trenitalia search for specifc site
            //if (objBruc.OneHubServiceName == "Trenitalia")
            //{
            //List<string> listHostForTI = new List<string> { "rail.statravel.com", "rail.statravel.co.uk", "rail.statravel.com.au", "rail.statravel.co.nz", "rail.statravel.com.au" };
            //    bool IsTrenItaliaSearchAllow = listHostForTI.Contains(Request.Url.Host.Trim().ToLower());
            //    if (IsTrenItaliaSearchAllow)
            //    {
            //        Response.Redirect("TrainResults.aspx");
            //        return;
            //    }
            //}
            #endregion

            #region Bloack Station Search
            List<Guid> siteList = new List<Guid> { Guid.Parse("F72318EC-CB00-421C-9DA5-2F1D48C7BFE5"), Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D"), Guid.Parse("F06549A9-139A-44F9-8BEE-A60DC5CC2E05"), Guid.Parse("FF3A353E-CD84-42CF-855D-D3AF92128647"), Guid.Parse("107F17AC-580C-4D94-A7F7-E858ADADAB4F") };
            if (_db.tblSites.Any(x => x.ID == siteId && x.IsAgent == false) && (objBruc.arrstCode == "GBSPX" || objBruc.depstCode == "GBSPX" || objBruc.arrstCode == "GBLWB" || objBruc.depstCode == "GBLWB"))
            {
                Session["TrainSearch"] = null;
                Response.Redirect("TrainResults.aspx");

            }
            if (siteList.Contains(siteId) && (objBruc.arrstCode == "GBASI" || objBruc.depstCode == "GBASI" || objBruc.arrstCode == "GBEBF" || objBruc.depstCode == "GBEBF"))
            {
                Session["TrainSearch"] = null;
                Response.Redirect("TrainResults.aspx");
            }
            #endregion

            if (Convert.ToInt32(rdBookingType.SelectedValue) > 0 && Convert.ToInt32(objBruc.depRCode) > 0)
                request.IsReturnJourney = false;
            else if (Convert.ToInt32(rdBookingType.SelectedValue) > 0)
                request.IsReturnJourney = true;

            ApiLogin(request);
            TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);

            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
            {
                //--TreniItalia Search return request                
                if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                {
                    List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    request = TrainInformation(objBruc, 2);
                    request.IsReturnJourney = true;

                    ApiLogin(request);
                    TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                    List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    list.AddRange(listReturn);
                    if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                    {
                        List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                        List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                        List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                        pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();
                    }
                }
            }
            Session["TrainSearch"] = pInfoSolutionsResponse;
        }
        catch (Exception ex)
        {
            if (!chkIsLoyaltyActive.Checked && !ex.Message.Contains("Thread"))
            {
                Session["ErrorMessage"] = ex.Message;
                Response.Redirect("TrainResults.aspx");
            }
        }
    }

    int GetEurostarBookingDays(BookingRequestUserControl request)
    {
        List<string> listStCode = new List<string> { "GBSPX", "BEBMI", "FRPNO", "FRPAR", "GBASI", "FRLIL", "FRLLE", "FRMLV", "GBEBF" };
        if (listStCode.Contains(request.depstCode) && listStCode.Contains(request.arrstCode))
            return 180;
        else
            return 0;
    }

    public void GetRequest(BookingRequestUserControl request)
    {
        var objrequest = new TrainInformationRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            DepartureRailwayCode = request.depRCode,
            DepartureStationCode = request.depstCode,
            ArrivalRailwayCode = request.arrRCode,
            ArrivalStationCode = request.arrstCode,
            Class = request.ClassValue,
            NumAdults = request.Adults,
            NumBoys = request.Boys,
            NumSeniors = request.Seniors,
            NumYouths = request.Youths,
            NumberOfTransfare = request.Transfare,
            IsHaveRailPass = request.isIhaveRailPass,
            IsReturnJourney = request.IsReturnJurney,
            DepartureDate = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null),
            DepartureTime = Convert.ToDateTime(ddldepTime.SelectedValue),
        };

        if (!string.IsNullOrEmpty(txtReturnDate.Text) && !txtReturnDate.Text.Contains("DD"))
        {
            objrequest.ArrivalDate = DateTime.ParseExact(txtReturnDate.Text, "dd/MMM/yyyy", null);
            objrequest.ArrivalTime = Convert.ToDateTime(ddlReturnTime.SelectedValue);
        }
        Session["TrainSearchRequest"] = objrequest;
    }

    public TrainInformationRequest TrainInformation(BookingRequestUserControl request, int flag)
    {
        GetRequest(request);
        var objrequest = new TrainInformationRequest
          {
              Header = new Header
              {
                  onehubusername = "#@dots!squares",
                  onehubpassword = "#@dots!squares",
                  unitofwork = 0,
                  language = Language.nl_BE,
              },
              DepartureRailwayCode = flag == 1 ? request.depRCode : request.arrRCode,
              DepartureStationCode = flag == 1 ? request.depstCode : request.arrstCode,
              ArrivalRailwayCode = flag == 1 ? request.arrRCode : request.depRCode,
              ArrivalStationCode = flag == 1 ? request.arrstCode : request.depstCode,

              Class = request.ClassValue,
              NumAdults = request.Adults,
              NumBoys = request.Boys,
              NumSeniors = request.Seniors,
              NumYouths = request.Youths,
              NumberOfTransfare = request.Transfare,
              IsHaveRailPass = request.isIhaveRailPass,
              IsReturnJourney = request.IsReturnJurney
          };

        //--Departure IF BENE
        objrequest.DepartureDate = request.depdt;
        objrequest.DepartureTime = request.depTime;
        if (request.ReturnDate != string.Empty && flag == 1)
        {
            objrequest.IsReturnJourney = true;
            objrequest.ArrivalDate = DateTime.ParseExact(request.ReturnDate, "dd/MMM/yyyy", null);
            objrequest.ArrivalTime = Convert.ToDateTime(request.ReturnTime);
        }

        //-Return For TI
        if (request.ReturnDate != string.Empty && flag == 2)
        {
            objrequest.IsReturnJourney = true;
            objrequest.DepartureDate = DateTime.ParseExact(request.ReturnDate, "dd/MMM/yyyy", null);
            objrequest.DepartureTime = Convert.ToDateTime(request.ReturnTime);
        }
        return objrequest;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            #region If P2P Widget is not allowed for this site then searching will not happing
            bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            if (!IsVisibleP2PWidget)
                return;
            #endregion

            if (!string.IsNullOrEmpty(txtReturnDate.Text))
                if (!txtReturnDate.Text.Contains("DD/MM/YYYY"))
                    if (Convert.ToDateTime(txtDepartureDate.Text).Date > Convert.ToDateTime(txtReturnDate.Text).Date)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "callalert", "alert('Return date should be greater than the Depart date')", true);
                        rdBookingType_SelectedIndexChanged(sender, e);
                        return;
                    }

            var stList = _db.StationNameLists.Where(x => (x.StationEnglishName == txtFrom.Text.Trim() || x.StationEnglishName == txtTo.Text.Trim()) && x.IsUK).FirstOrDefault();
            var isUK = stList != null && stList.IsUK;

            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue == "0")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCa1l", "alert('Please enter at least 1 adult, senior or junior(youth) passenger.')", true);
                return;
            }
            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue != "0" && isUK)
            {
                mdPassengerUK.Show();
                return;
            }
            else if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue != "0" && !isUK)
            {
                mdPassenger.Show();
                return;
            }

            int totalAdult = Convert.ToInt32(ddlAdult.SelectedValue) * 4;
            int totalYouth = Convert.ToInt32(ddlYouth.SelectedValue) * 4;
            int totalSenior = Convert.ToInt32(ddlSenior.SelectedValue) * 4;
            int totalChilden = totalAdult + totalYouth + totalSenior;
            if (Convert.ToInt32(ddlChild.SelectedValue) > totalChilden && isUK)
            {
                mdPassengerUK.Show();
                return;
            }
            else if (Convert.ToInt32(ddlChild.SelectedValue) > totalChilden && !isUK)
            {
                mdPassenger.Show();
                return;
            }

            SearchTrainInfo();
            string str = "BE";
            if (ViewState["depRCode"] != null)
                str = ViewState["depRCode"].ToString() == "0" ? "BE" : "TI";
            Response.Redirect(!chkIsLoyaltyActive.Checked ? "TrainResults.aspx?req=" + str + "" : "rail-tickets");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ApiLogin(TrainInformationRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }

    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdBookingType.SelectedValue == "0")
        {
            txtReturnDate.Enabled = false;
            txtReturnDate.Text = "";
            ddlReturnTime.Enabled = false;
            reqReturnDate.Enabled = false;
            regReturnDate.Enabled = false;

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "caldisable()", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
        }
        else
        {
            txtReturnDate.Enabled = true;
            ddlReturnTime.Enabled = true;
            reqReturnDate.Enabled = true;
            regReturnDate.Enabled = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "calenable()", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
        }
        ViewState["Rdo"] = rdBookingType.SelectedValue;
        SetCallander();
    }

    public void SearchTrainInfoForFill()
    {
        try
        {
            BookingRequestUserControl objBRUC;
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            rdBookingType.SelectedValue = objBRUC.Journeytype == null ? "0" : objBRUC.Journeytype;
            if (rdBookingType.SelectedValue == "0")
            {
                txtReturnDate.Text = "";
                txtReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                reqReturnDate.Enabled = false;
            }
            else
            {
                txtReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                reqReturnDate.Enabled = true;
                txtReturnDate.Text = objBRUC.ReturnDate;
            }
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlYouth.SelectedValue = objBRUC.Youths.ToString();
            ddlSenior.SelectedValue = objBRUC.Seniors.ToString();
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            chkIhaveRailPass.Checked = objBRUC.isIhaveRailPass;
            chkIsLoyaltyActive.Checked = objBRUC.Loyalty;
        }
        catch (Exception ex)
        {
        }
    }
}