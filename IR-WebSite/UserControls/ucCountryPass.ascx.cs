﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

public partial class UserControls_ucCountryPass : UserControl
{
    public string currency;
    string curID;
    Guid siteId;
    public int LevelID = 0;
    public bool IsBritRail = false;
    public List<SetCountry> listcun = new List<SetCountry>();
    readonly ManageBooking _BMaster = new ManageBooking();
    public ManageProduct _Product = new ManageProduct();
    private readonly FrontEndManagePass oManagePass = new FrontEndManagePass();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            GetCurrencyCode();
            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["PrdID"] != null)
                {
                    ViewState["PrdID"] = (Guid)Page.RouteData.Values["PrdID"]; ;
                    selcunid2.Visible = false;
                    selcunid3.Visible = false;
                    selcunid4.Visible = false;
                    selcunid5.Visible = false;
                    if (Session["GetcunLvl"] != null && Convert.ToInt32(Session["GetcunLvl"]) > 2)
                    {
                        ViewState["CountryLevel"] = Session["GetcunLvl"];
                        LevelID = Convert.ToInt32(ViewState["CountryLevel"] ?? "0");
                        GetCountryLevelData(null, null, null, null, null, 0);
                        Bindcountry();
                    }
                }
                if (ViewState["PrdID"] != null)
                {
                    BindTravellerValidity(Guid.Parse(ViewState["PrdID"].ToString()));
                }
                if (Session["RailPassData"] != null)
                {
                    var oldList = Session["RailPassData"] as List<getRailPassData>;
                    lblPrice.Text = oldList.Select(x => new { CPrice = Convert.ToDecimal(x.Price) }).Sum(t => t.CPrice).ToString("F");
                    divGrd.Visible = true;
                    grdTravellerinfo.DataSource = oldList;
                    grdTravellerinfo.DataBind();
                }
            }
            LevelID = Convert.ToInt32(ViewState["CountryLevel"] ?? "0");
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    #region /** Traveler information start **/

    void BindTravellerValidity(Guid PrdID)
    {
        try
        {
            var classtype = oManagePass.GetClass(PrdID);
            if (classtype != null)
            {
                ddlClass.DataSource = classtype.OrderBy(t => t.Name);
                ddlClass.DataValueField = "ID";
                ddlClass.DataTextField = "Name";
                ddlClass.DataBind();
                ddlClass.Items.Insert(0, new ListItem("--Please choose class--", "0"));
            }
            ddlTraveller.Items.Insert(0, new ListItem("--Please choose traveller--", "0"));
            ddlValidityDays.Items.Insert(0, new ListItem("--Please choose days--", "0"));
            ddlSelectno.Items.Insert(0, new ListItem("0", "0"));
            ddlCunt1.Enabled = ddlSelectno.Enabled = ddlValidityDays.Enabled = ddlTraveller.Enabled = false;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCurrencyCode()
    {
        try
        {
            if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
            {
                var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
                curID = cookie.Values["_curId"];
                siteId = Guid.Parse(cookie.Values["_siteId"]);
                currency = oManagePass.GetCurrency(Guid.Parse(curID));
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void ClearSelection()
    {
        ddlClass.ClearSelection();
        ddlValidityDays.ClearSelection();
        ddlTraveller.ClearSelection();
        ddlSelectno.ClearSelection();
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlClass.SelectedValue != "0")
            {
                ddlValidityDays.Enabled = true;
                var classid = Guid.Parse(ddlClass.SelectedValue);
                var PrdID = Guid.Parse(ViewState["PrdID"].ToString());
                var travellerDays = oManagePass.GetTravellerNames(PrdID, classid);
                if (travellerDays != null)
                {
                    ddlValidityDays.DataSource = travellerDays.OrderBy(t => t.Name);
                    ddlValidityDays.DataValueField = "ID";
                    ddlValidityDays.DataTextField = "Name";
                    ddlValidityDays.DataBind();
                    ddlValidityDays.Items.Insert(0, new ListItem("--Please choose days--", "0"));
                }
            }
            ddlCunt1.Enabled = ddlCunt2.Enabled = ddlCunt3.Enabled = ddlCunt4.Enabled = ddlCunt5.Enabled = ddlTraveller.Enabled = ddlSelectno.Enabled = false;
            ddlCunt1.SelectedIndex = ddlCunt2.SelectedIndex = ddlCunt3.SelectedIndex = ddlCunt4.SelectedIndex = ddlCunt5.SelectedIndex = ddlTraveller.SelectedIndex = ddlSelectno.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlValidityDays_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlValidityDays.SelectedValue != "0")
            {
                ddlTraveller.Enabled = true;
                var classid = Guid.Parse(ddlClass.SelectedValue);
                var ValidityID = Guid.Parse(ddlValidityDays.SelectedValue);
                var PrdID = Guid.Parse(ViewState["PrdID"].ToString());
                var travellers = oManagePass.GetTravellerType(PrdID, classid, ValidityID);
                if (travellers != null)
                {
                    ddlTraveller.Enabled = true;
                    ddlTraveller.DataSource = travellers.OrderBy(t => t.Name);
                    ddlTraveller.DataValueField = "ID";
                    ddlTraveller.DataTextField = "Name";
                    ddlTraveller.DataBind();
                    ddlTraveller.Items.Insert(0, new ListItem("--Please choose traveller--", "0"));
                }
            }
            ddlCunt1.Enabled = ddlCunt2.Enabled = ddlCunt3.Enabled = ddlCunt4.Enabled = ddlCunt5.Enabled = ddlSelectno.Enabled = false;
            ddlCunt1.SelectedIndex = ddlCunt2.SelectedIndex = ddlCunt3.SelectedIndex = ddlCunt4.SelectedIndex = ddlCunt5.SelectedIndex =  ddlSelectno.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlTraveller_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            var PrdID = Guid.Parse(ViewState["PrdID"].ToString());
            IsBritRail = _BMaster.GetIsBritrailByProductID(PrdID);
            ddlSelectno.Items.Clear();
            if (ddlTraveller.SelectedItem.Text.Contains("Saver"))
            {
                for (int i = 5; i >= 2; i--)
                    ddlSelectno.Items.Insert(0, new ListItem(i.ToString(), i.ToString()));
                if (IsBritRail)
                    ddlSelectno.Items.RemoveAt(0);
                ddlSelectno.Items.Insert(0, new ListItem("0", "0"));
            }
            else
            {
                for (int i = 0; i <= 10; i++)
                    ddlSelectno.Items.Insert(i, new ListItem(i.ToString(), i.ToString()));
            }
            ddlCunt1.Enabled = ddlSelectno.Enabled = true;
            ddlCunt2.Enabled = ddlCunt3.Enabled = ddlCunt4.Enabled = ddlCunt5.Enabled = false;
            ddlCunt1.SelectedIndex = ddlCunt2.SelectedIndex = ddlCunt3.SelectedIndex = ddlCunt4.SelectedIndex = ddlCunt5.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void lnlAdd_Click(object sender, EventArgs e)
    {
        try
        {
            CountryLevel();
            AddGrid();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void AddGrid()
    {
        try
        {
            var PrdID = Guid.Parse(ViewState["PrdID"].ToString());
            IsBritRail = _BMaster.GetIsBritrailByProductID(PrdID);
            ShowMessage(0, string.Empty);
            var list = new List<getRailPassData>();
            Guid validityId = new Guid(), travlerId = new Guid(), classid = new Guid(), Agentid = (AdminuserInfo.UserID), productCurID = new Guid();
            string ClassName = ddlClass.SelectedItem.Text;
            string ValidityName = ddlValidityDays.SelectedItem.Text;
            string TravellerName = ddlTraveller.SelectedItem.Text;
            string[] cntrystring = null;
            string[] cntrycode = null;
            if (ddlClass.SelectedValue != "0")
                classid = Guid.Parse(ddlClass.SelectedValue);
            if (ddlValidityDays.SelectedValue != "0")
                validityId = Guid.Parse(ddlValidityDays.SelectedValue);
            if (ddlTraveller.SelectedValue != "0")
                travlerId = Guid.Parse(ddlTraveller.SelectedValue);
            if (Session["GETcountryCODE"] != null)
            {
                cntrystring = Session["GETcountryCODE"].ToString().Split('ñ');
                cntrycode = cntrystring[0].Split(',');
            }
            /**start manage price list**/
            tblProduct oProd = oManagePass.GetProductById(PrdID);
            productCurID = oProd.CurrencyID;
            bool IsPassport = (oProd.PassportValid ?? default(bool));
            List<PricewithMarkup> PriceList = oManagePass.GetproductPrice(validityId, travlerId, classid, PrdID, siteId);
            var lst = new List<PricewithMarkup>();
            foreach (var item in PriceList)
            {
                PricewithMarkup data = new PricewithMarkup();
                data.Price = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(item.PriceMarkup), siteId, productCurID, Guid.Parse(curID))));
                data.PriceMarkup = item.PriceMarkup;
                data.PriceBand = item.PriceBand;
                lst.Add(data);
            }

            var CountryPriceBand = oManagePass.GetEurailCountryPriceBandByCountryCode(Convert.ToInt32(string.IsNullOrEmpty(cntrycode[0]) ? "0" : cntrycode[0]));
            var pricematch = lst.FirstOrDefault(t => t.PriceBand == CountryPriceBand);
            if (pricematch == null)
                pricematch = lst.FirstOrDefault();
            /**End manage price list**/
            var listdata = _BMaster.GetClassAndValidityID(pricematch.PriceMarkup, ValidityName, ClassName, TravellerName, PrdID, travlerId, siteId, AdminuserInfo.UserID);
            var MonthValidity = _Product.GetMonthValidity(PrdID);
            int Qty = Convert.ToInt32(ddlSelectno.SelectedValue);

            if (Session["RailPassData"] != null)
            {
                var lstRP = Session["RailPassData"] as List<getRailPassData>;
                int FreeChildAllow = 2;
                List<getRailPassData> rest = new List<getRailPassData>();
                if (IsBritRail)
                {
                    FreeChildAllow = 1;
                    rest = lstRP.Where(x => x.TravellerName.Trim().ToLower().Contains("adult") || x.TravellerName.Trim().ToLower().Equals("senior")).ToList();
                }
                else
                    rest = lstRP.Where(x => x.TravellerName.Trim().ToLower().Contains("adult") || x.TravellerName.Trim().ToLower().Equals("adultsaver")).ToList();
                if (rest.Count > 0)
                {
                    int maxchild = rest.Count * FreeChildAllow;
                    var lstchild = lstRP.Where(x => x.TravellerName.Trim().ToLower().Equals("child") && (x.Price == "0.0" || x.Price == "0")).ToList();
                    int countchild = lstchild.Count;
                    if (ddlTraveller.SelectedItem.Text.Trim().ToLower().Equals("child") && Convert.ToDecimal(pricematch.Price) < 1)
                    {
                        countchild = countchild + Qty;
                        if (!(rest.Any(x => x.ClassID == classid.ToString() && x.ValidityID == validityId.ToString())))
                        {
                            Qty = 0;
                            if (IsBritRail)
                                ShowMessage(2, "Child passes can only be purchased with Adult or Senior passes with same travel validity and class.");
                            else
                                ShowMessage(2, "Child passes can only be purchased with Adult or Adult Saver passes with same travel validity and class.");
                            return;
                        }
                    }
                    if (countchild > maxchild)
                    {
                        Qty = 0;
                        if (IsBritRail)
                            ShowMessage(2, "With each Adult, or Senior pass (not Adult Saver, Child or Youth) you are allowed x1 Child for free pass.");
                        else
                            ShowMessage(2, "A maximum of 2 child passes is allowed per adult, for additional children please purchase a Youth pass.");
                        return;
                    }
                }
                else if (ddlTraveller.SelectedItem.Text.Trim().ToLower().Equals("child") && Convert.ToDecimal(pricematch.Price) < 1)
                {
                    Qty = 0;
                    if (IsBritRail)
                        ShowMessage(2, "Child passes can only be purchased with Adult or Senior passes with same travel validity and class.");
                    else
                        ShowMessage(2, "Child passes can only be purchased with Adult or Adult Saver passes with same travel validity and class.");
                    return;
                }
            }
            else if (ddlTraveller.SelectedItem.Text.Trim().ToLower().Equals("child") && Convert.ToDecimal(pricematch.Price) < 1)
            {
                Qty = 0;
                if (IsBritRail)
                    ShowMessage(2, "Child passes can only be purchased with Adult or Senior passes with same travel validity and class.");
                else
                    ShowMessage(2, "Child passes can only be purchased with Adult or Adult Saver passes with same travel validity and class.");
                return;
            }

            string Countries = string.Empty;
            if (ddlCunt1.Items.Count > 0)
            {
                Countries = ddlCunt1.SelectedItem.Text;
                ddlCunt1.SelectedIndex = 0;
            }
            if (ddlCunt2.Items.Count > 0)
            {
                Countries += ", " + ddlCunt2.SelectedItem.Text;
                ddlCunt2.SelectedIndex = 0;
                selcunid2.Visible = false;
            }
            if (ddlCunt3.Items.Count > 0 && ddlCunt3.SelectedValue!="0")
            {
                Countries += ", " + ddlCunt3.SelectedItem.Text;
                ddlCunt3.SelectedIndex = 0;
                selcunid3.Visible = false;
            }
            if (ddlCunt4.Items.Count > 0 && ddlCunt4.SelectedValue != "0")
            {
                Countries += ", " + ddlCunt4.SelectedItem.Text;
                ddlCunt4.SelectedIndex = 0;
                selcunid4.Visible = false;
            }
            if (ddlCunt5.Items.Count > 0 && ddlCunt5.SelectedValue != "0")
            {
                Countries += ", " + ddlCunt5.SelectedItem.Text;
                ddlCunt5.SelectedIndex = 0;
                selcunid5.Visible = false;
            }

            for (int no = 0; no < Qty; no++)
            {
                getRailPassData Data = new getRailPassData
                    {
                        OriginalPrice = Convert.ToString(listdata.OriginalPrice),
                        Id = Guid.NewGuid(),
                        PrdtId = PrdID.ToString(),
                        TravellerID = travlerId.ToString(),
                        ClassID = classid.ToString(),
                        ValidityID = validityId.ToString(),
                        CategoryID = listdata.CategoryID.ToString(),
                        IsBritrail = _BMaster.GetClassAndValidityID(listdata.CategoryID),
                        PrdtName = oManagePass.GetProductName(PrdID),
                        TravellerName = TravellerName,
                        ClassName = ClassName,
                        ValidityName = ValidityName,
                        Price = pricematch.Price,
                        SalePrice = pricematch.PriceMarkup,
                        Qty = "1",
                        CountryStartCode = cntrycode[0],
                        CountryEndCode = cntrycode[1],
                        Commission = listdata.commission.ToString(),
                        MarkUp = listdata.markup.ToString(),
                        CountryLevelIDs = cntrystring[1],
                        PassportIsVisible = IsPassport,
                        MonthValidity = MonthValidity,
                        CountryName = Countries
                    };
                list.Add(Data);
            }
            if (list.Count > 0)
            {
                ClearSelection();
                if (Session["RailPassData"] != null)
                {
                    var lstRP = Session["RailPassData"] as List<getRailPassData>;
                    if (lstRP.FirstOrDefault().IsBritrail)
                    {
                        if (list.Where(t => t.IsBritrail == true).Count() == list.Count())
                            lstRP.AddRange(list);
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "innertabActive()", true);
                            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "alert", "AlertMessage('Sorry! You can’t purchase another pass with Britrail pass.')", true);
                            return;
                        }
                    }
                    else
                    {
                        if (list.Where(t => t.IsBritrail == false).Count() == list.Count())
                            lstRP.AddRange(list);
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "innertabActive()", true);
                            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "alert", "AlertMessage('Sorry! You can’t purchase Britrail pass with this pass.')", true);
                            return;
                        }
                    }
                    Session["RailPassData"] = lstRP;
                }
                else
                    Session.Add("RailPassData", list);
                Session.Remove("CountryCodeList");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "showmsg()", true);
            }
            var oldList = Session["RailPassData"] as List<getRailPassData>;
            lblPrice.Text = oldList.Select(x => new { CPrice = Convert.ToDecimal(x.Price) }).Sum(t => t.CPrice).ToString("F");
            divGrd.Visible = true;
            grdTravellerinfo.DataSource = oldList;
            grdTravellerinfo.DataBind();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void grdTravellerinfo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Remove")
            {
                var Id = Guid.Parse(e.CommandArgument.ToString());
                var oldList = new List<getRailPassData>();
                if (HttpContext.Current.Session["RailPassData"] != null)
                {
                    oldList = Session["RailPassData"] as List<getRailPassData>;
                }
                var objRPD = oldList.FirstOrDefault(a => a.Id == Id);

                #region if free child
                if (objRPD.TravellerName.Trim().ToLower().Equals("adult") || objRPD.TravellerName.Trim().ToLower().Equals("adultsaver"))
                {
                    int totalDelAdult = oldList.Where(x => x.ValidityID == objRPD.ValidityID && x.ClassID == objRPD.ClassID && x.TravellerName.Equals(objRPD.TravellerName)).Count();
                    var delchild = oldList.Where(x => (x.Price == "0.0" || x.Price == "0") && x.ValidityID == objRPD.ValidityID && x.ClassID == objRPD.ClassID && x.TravellerName.Trim().ToLower().Equals("child")).ToList();
                    if (totalDelAdult * 2 >= delchild.Count)
                    {
                        int countx = 0;
                        foreach (var item in delchild)
                        {
                            if (countx < 2)
                            {
                                oldList.Remove(item);
                                countx++;
                            }
                        }
                    }
                }
                #endregion

                int count = 0;
                if (objRPD != null && objRPD.TravellerName.ToLower().Contains("saver"))
                {
                    var newList = (from a in oldList
                                   where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName && a.TravellerName.ToLower().Contains("saver")
                                   select a).ToList();
                    count = newList.Count();
                    if (count > 2)
                    {
                        oldList.RemoveAll(ty => ty.Id == Id);
                        _BMaster.DelpasssaleData(Id);
                        Session.Add("RailPassData", oldList);
                    }
                    else
                    {
                        foreach (getRailPassData a in newList)
                        {
                            _BMaster.DelpasssaleData(a.Id);
                            oldList.RemoveAll(ty => ty.Id == a.Id);
                        }
                        Session.Add("RailPassData", oldList);
                    }
                }
                else
                {
                    oldList.RemoveAll(ty => ty.Id == Id);
                    _BMaster.DelpasssaleData(Id);
                    Session.Add("RailPassData", oldList);
                }
                if (!(oldList.Count > 0))
                {
                    divGrd.Visible = false;
                    Session["RailPassData"] = null;
                }
                grdTravellerinfo.DataSource = oldList;
                grdTravellerinfo.DataBind();
                lblPrice.Text = oldList.Select(x => new { CPrice = Convert.ToDecimal(x.Price) }).Sum(t => t.CPrice).ToString("F");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/BookingCart");
    }

    #endregion /** Traveler information end**/

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    #region/**start country level task**/

    public void CountryLevel()
    {
        try
        {
            Guid c1 = Guid.Parse(ddlCunt1.SelectedValue);
            Guid c2 = Guid.Parse(ddlCunt2.SelectedValue);
            Guid? c3 = null;
            Guid? c4 = null;
            Guid? c5 = null;

            if (!string.IsNullOrEmpty(ddlCunt3.SelectedValue == "0" ? null : ddlCunt3.SelectedValue))
                c3 = Guid.Parse(ddlCunt3.SelectedValue);
            if (!string.IsNullOrEmpty(ddlCunt4.SelectedValue == "0" ? null : ddlCunt4.SelectedValue))
                c4 = Guid.Parse(ddlCunt4.SelectedValue);
            if (!string.IsNullOrEmpty(ddlCunt5.SelectedValue == "0" ? null : ddlCunt5.SelectedValue))
                c5 = Guid.Parse(ddlCunt5.SelectedValue);

            String StartCode = GetCountryCodeValue(c1, c2, c3, c4, c5, LevelID);
            Session["GETcountryCODE"] = StartCode + "," + null + "ñ" + c1 + "," + c2 + "," + c3 + "," + c4 + "," + c5;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void Bindcountry()
    {
        try
        {
            if (listcun != null)
            {
                ddlCunt1.DataSource = listcun.OrderBy(ty => ty.Country).ToList();
                ddlCunt1.DataTextField = "Country";
                ddlCunt1.DataValueField = "ID";
                ddlCunt1.DataBind();
                ddlCunt1.Items.Insert(0, new ListItem("-- Select the 1st country --", "0"));
                selcunid1.Visible = true;
                ddlCunt2.Items.Insert(0, new ListItem("-- Select the 2nd country --", "0"));
                ddlCunt3.Items.Insert(0, new ListItem("-- Select the 3rd country --", "0"));
                ddlCunt4.Items.Insert(0, new ListItem("-- Select the 4th country --", "0"));
                ddlCunt5.Items.Insert(0, new ListItem("-- Select the 5th country --", "0"));

            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlCunt1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), null, null, null, null, 1);
            if (listcun != null)
            {
                ddlCunt2.DataSource = listcun.OrderBy(ty => ty.Country).ToList();
                ddlCunt2.DataTextField = "Country";
                ddlCunt2.DataValueField = "ID";
                ddlCunt2.DataBind();
                ddlCunt2.Items.Insert(0, new ListItem("-- Select the 2nd country --", "0"));
                selcunid2.Visible = ddlCunt2.Enabled = true;
            }
            ddlCunt3.Enabled = ddlCunt4.Enabled = ddlCunt5.Enabled = false;
            ddlCunt2.SelectedIndex = ddlCunt3.SelectedIndex = ddlCunt4.SelectedIndex = ddlCunt5.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlCunt2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), Guid.Parse(ddlCunt2.SelectedValue), null, null, null, 2);
            if (listcun != null)
            {
                ddlCunt3.DataSource = listcun.OrderBy(ty => ty.Country).ToList();
                ddlCunt3.DataTextField = "Country";
                ddlCunt3.DataValueField = "ID";
                ddlCunt3.DataBind();
                ddlCunt3.Items.Insert(0, new ListItem("-- Select the 3rd country --", "0"));
                selcunid3.Visible = ddlCunt3.Enabled = true;
            }
            ddlCunt4.Enabled = ddlCunt5.Enabled = false;
            ddlCunt3.SelectedIndex = ddlCunt4.SelectedIndex = ddlCunt5.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlCunt3_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if ((LevelID - 3 != 0))
                GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), Guid.Parse(ddlCunt2.SelectedValue), Guid.Parse(ddlCunt3.SelectedValue), null, null, 3);
            if (listcun != null && listcun.Count > 0)
            {
                ddlCunt4.DataSource = listcun.OrderBy(ty => ty.Country).ToList();
                ddlCunt4.DataTextField = "Country";
                ddlCunt4.DataValueField = "ID";
                ddlCunt4.DataBind();
                ddlCunt4.Items.Insert(0, new ListItem("-- Select the 4th country --", "0"));
                selcunid4.Visible = ddlCunt4.Enabled = true;
            }
            ddlCunt5.Enabled = false;
            ddlCunt4.SelectedIndex = ddlCunt5.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlCunt4_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if ((LevelID - 4 != 0))
                GetCountryLevelData(Guid.Parse(ddlCunt1.SelectedValue), Guid.Parse(ddlCunt2.SelectedValue), Guid.Parse(ddlCunt3.SelectedValue), Guid.Parse(ddlCunt4.SelectedValue), null, 4);
            if (listcun != null && listcun.Count > 0)
            {
                ddlCunt5.DataSource = listcun.OrderBy(ty => ty.Country).ToList();
                ddlCunt5.DataTextField = "Country";
                ddlCunt5.DataValueField = "ID";
                ddlCunt5.DataBind();
                ddlCunt5.Items.Insert(0, new ListItem("-- Select the 5th country --", "0"));
                selcunid5.Visible = ddlCunt5.Enabled = true;
            }
            ddlCunt5.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public List<SetCountry> GetCountryLevelData(Guid? Country1, Guid? Country2, Guid? Country3, Guid? Country4, Guid? Country5, int CurrentCountryNo)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(con))
            {
                SqlCommand sqlCommand = new SqlCommand("SP_GetCountryAllLevel", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Country1", Country1);
                sqlCommand.Parameters.AddWithValue("@Country2", Country2);
                sqlCommand.Parameters.AddWithValue("@Country3", Country3);
                sqlCommand.Parameters.AddWithValue("@Country4", Country4);
                sqlCommand.Parameters.AddWithValue("@Country5", Country5);
                sqlCommand.Parameters.AddWithValue("@CurrentCountryID", CurrentCountryNo);
                sqlCommand.Parameters.AddWithValue("@Level", LevelID);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataSet ds = new DataSet();
                da.Fill(ds);
                int count = ds.Tables[0].Rows.Count;
                for (int i = 0; i < count; i++)
                {
                    SetCountry getdata = new SetCountry();
                    getdata.ID = Convert.ToString(ds.Tables[0].Rows[i]["EurailCountryID"]);
                    getdata.Country = Convert.ToString(ds.Tables[0].Rows[i]["country"]);
                    listcun.Add(getdata);
                }
                conn.Close();
                return listcun;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string GetCountryCodeValue(Guid? Country1, Guid? Country2, Guid? Country3, Guid? Country4, Guid? Country5, int productcountrycount)
    {
        try
        {
            string code = string.Empty;
            using (SqlConnection conn = new SqlConnection(con))
            {
                SqlCommand sqlCommand = new SqlCommand("SP_GetEurailPassCountryCode", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Country1", Country1);
                sqlCommand.Parameters.AddWithValue("@Country2", Country2);
                sqlCommand.Parameters.AddWithValue("@Country3", Country3);
                sqlCommand.Parameters.AddWithValue("@Country4", Country4);
                sqlCommand.Parameters.AddWithValue("@Country5", Country5);
                sqlCommand.Parameters.AddWithValue("@productcountrycount", productcountrycount);
                conn.Open();
                SqlDataReader dr = sqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    code = dr.GetValue(0).ToString();
                }
                conn.Close();
                return code;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion/**end country level task**/

    #region Class
    public class SetCountry
    {
        public string Country { get; set; }
        public string ID { get; set; }
    }
    #endregion
}