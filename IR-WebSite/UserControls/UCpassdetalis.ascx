﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UCpassdetalis.ascx.cs"
    Inherits="UserControls_passdetalis" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style>
    .input
    {
        text-transform: capitalize;
    }
    .pass .hd-title
    {
        -moz-box-sizing: border-box;
        border-radius: 0 0 5px 5px;
        height: auto !important;
        line-height: 20px;
        width: 100%;
        padding-right: 10%;
    }
    .valid
    {
        float: left;
    }
    .valid3
    {
        color: red;
    }
    .clsdob:before
    {
        content: "Date of Birth";
        font-size: 14px;
        padding-left: 95px;
        color: #5e5e5e;
        font-weight: bold;
    }
    .valid2
    {
        color: red;
        position: absolute;
        top: 33px;
        margin-left: 7px;
    }
    .pass-coloum-forth
    {
        width: 200px;
    }
    .pass-coloum-three
    {
        width: 74px;
    }
    .MonthValidity
    {
        display: none;
    }
    #MainContent_rptPassDetail_lblInvalidDate_0
    {
        width: 297px;
        position: absolute;
    }
    
    .pass .discription-block
    {
        border: 7px solid #5E5E5E;
    }
</style>
<script type="text/javascript">
    function show() { $('.discription-block').show(); }
    $(document).ready(function () {
        $('.imgOpen').click(function () {
            if ("images/arr-down.png" == $(this).attr('src').toString()) {
                $(this).attr('src', 'images/arr-right.png');
            }
            else {
                $(this).attr('src', 'images/arr-down.png');
            }
            $(this).parent().parent().parent().find('.discription-block').slideToggle();
        });
        $("[id*=lnkDelete]").click(function () {
            var countsaver = 0;
            var result = false;
            var saver = $(this).attr('saver');
            if (saver.match(/saver/i)) {
                result = confirm('You are now deleting the whole saverpass booking, as min 2 pax is required.');
                saver = 'yes';
            }
            else
                result = confirm('Are you sure? Do you want to delete this item?');
            if (result) {
                var Id = $(this).attr('commandargument');
                $("a[saver]").each(function (key, value) {
                    if ($(this).attr('saver').match(/saver/i)) {
                        countsaver++;
                    }
                });
                JSONcall2(Id, $(this), saver, countsaver);
            }
        });
    });
    function JSONcall2(Id, obj, hassaver, countsaver) {
        var hostUrl = window.location + '.aspx/del_pass';
        $(document).ready(function () {
            $.ajax({
                url: hostUrl,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: "{'Id':'" + Id + "'}",
                success: function (Pdata) {
                    if (Pdata.d == 'BookingCart') {
                        if (hassaver == 'yes' && countsaver < 3) {
                            $("a[saver]").each(function (key, value) {
                                if ($(this).attr('saver').match(/saver/i)) {
                                    $(this).parent().parent().remove();
                                }
                            });
                        }
                        else
                            obj.parent().parent().remove();
                        $(".customheader").each(function (key, value) {
                            $(this).html('Pass ' + (key + 1));
                        });
                    }
                    else
                        window.location = Pdata.d;
                    getdata();
                },
                error: function () {
                }
            });
        });
    }
    function chkVal(id, e) {
        if (e == 0)
            e = 3;
        var txtStartDateId = '#' + $(id).find('input[type=text]').attr('id');
        $(txtStartDateId).datepicker({
            numberOfMonths: 2,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            firstDay: 1,
            minDate: 0,
            maxDate: '+' + e + 'm'
        });
        $("#ui-datepicker-div").removeClass("clsdob");
        $(txtStartDateId).datepicker('show');
    }
    function chkDob(id, e) {
        var currentYear = (new Date).getFullYear();
        var txtDob = '#' + $(id).find('input[type=text]').attr('id');
        $(txtDob).datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1901:' + currentYear,
            onChangeMonthYear: function (year, month, inst) {
                var selectedDate = $(this).datepicker("getDate"); //Date object
                var day = selectedDate.getDate()
                selectedDate.setDate(day); //set first day of the month
                selectedDate.setMonth(month - 1); //month is 1-12, setMonth is 0-11
                selectedDate.setFullYear(year);
                $(this).datepicker("setDate", selectedDate);
            }
        });
        $("#ui-datepicker-div").addClass("clsdob");
        $(txtDob).datepicker('show');
    }
    function customselect() {
        var sta = $("span").hasClass("customSelect");
        if (sta) {
            $(".customSelect").remove();
            $('select').customSelect();
        }
    }
    function showmessagecountry() {
        $(".customSelect").remove();
        $(".chkcheckbox,.uncheckbox").remove();
        $(".uncheckradiobox,.chkcheckradiobox").remove();
    }
    function Productval(e) {
        var d = $(e).val().split('/');
        var startdate = new Date(d[1] + "," + d[0] + "," + d[2]);
        if (startdate == "Invalid Date") {
            alert("Pass Start Date is not valid format");
        }
        else {
            var sdate = new Date($(e).parent().find("#hdnfromdate").val());
            var edate = new Date($(e).parent().find("#hdntodate").val());
            var msg = $(e).parent().find("#hdnmsg").val();
            var valdationmsg = "#" + $(e).attr('id').replace("txtStartDate", "lblInvalidDate");
            if (sdate <= startdate && edate >= startdate) {
                $(valdationmsg).text('').hide();
            }
            else {
                $(valdationmsg).text(msg).show();
            }
        }
    }
    function chkDate(event) {
        var key = event.keyCode;
        var trgid = event.target.id ? event.target.id : event.srcElement.id;
        var id = "#" + trgid;
        var value = $(id).val();
        var strmon = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var m = value.split("/");
        var mon = parseInt(m[1]) - 1;
        if ($.isNumeric(mon))
            $(id).val(m[0] + "/" + strmon[mon] + "/" + m[2]);
    }
    function showthis() {
        document.getElementById('popupDiv').style.display = 'block';
    }
</script>
<div class="left-content" style="width: 1003px; padding-left: 0px;">
    <p>
        Please enter your details below carefully and ensure they are exactly as per your
        passport.
    </p>
    <p>
        &nbsp;
    </p>
    <div class="clear">
        &nbsp;</div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Repeater ID="rptPassDetail" runat="server" OnItemDataBound="rptPassDetail_ItemDataBound"
                OnItemCommand="rptPassDetail_ItemCommand">
                <ItemTemplate>
                    <div class="privacy-block-outer pass" id="DivRow" runat="server">
                        <div class="red-title customheader">
                            Pass&nbsp;<%# Container.ItemIndex + 1 %></div>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div class="hd-title">
                                    <div style="float: left;">
                                        <asp:HiddenField ID="hdnPassSale" runat="server" Value='<%#Eval("PrdtId")+","+Eval("Price")+","+Eval("TravellerID")+","+Eval("ClassID")+","+Eval("ValidityID")+","+Eval("CategoryID")+","+Eval("commission")+","+Eval("MarkUp")+","+Eval("CountryStartCode")+","+Eval("CountryEndCode")+","+Eval("CountryLevelIDs")%>' />
                                        <asp:HiddenField ID="HdnOriginalPrice" runat="server" Value='<%#Eval("OriginalPrice")%>' />
                                        <asp:HiddenField ID="hdnPrdtId" runat="server" Value='<%#Eval("PrdtId")%>' />
                                        <asp:HiddenField ID="hdnSalePrice" runat="server" Value='<%#Eval("SalePrice")%>' />
                                        <asp:HiddenField ID="hdnCountryLevelIDs" runat="server" Value='<%#Eval("CountryLevelIDs")%>' />
                                        <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryID")%>' />
                                        <input type="hidden" class="passprice" value='<%#Eval("Price")%>' />
                                        <asp:Label ID="lblLID" runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                                        <%#Eval("PrdtName")%>
                                        <asp:Label ID="lblCountryName" runat="server" />,
                                        <%#Eval("ValidityName")%>,
                                        <%#Eval("TravellerName")%>,
                                        <%#Eval("ClassName")%>
                                        -
                                        <%=currency.ToString()%>
                                        <%#Eval("Price")%>
                                    </div>
                                    <div id="divTckProt" <%=TicketProtection %>>
                                        <%=currency.ToString()%>
                                        <label class="lbltkpprice" style="font-size: 13px !important; color: White;">
                                            <%=TkpAmount %></label>
                                        <asp:CheckBox ID="chkTicketProtection" CssClass="calculateTotal" runat="server" onclick="getdata()" />
                                        <a onclick="showthis()" href="#" title="Ticket Protection" style="position: absolute;
                                            float: right;">
                                            <img src="images/info.png" width="20" />
                                        </a>
                                        <br>
                                        <label class="lbltkpprice" style="font-size: 11px !important; color: White;">
                                            Ticket Protection</label>
                                    </div>
                                    <asp:HiddenField runat="server" ID="hdnSaver" Value='<%#Eval("TravellerName")%>' />
                                    <asp:LinkButton ID="lnkRemovePass" runat="server" CommandName="RemovePass" CommandArgument='<%#Eval("Id") %>'
                                        class="icon-close scale-with-grid" Style="background-image: url('images/btn-cross.png')"
                                        CausesValidation="false" Width="25" Height="25" />
                                    <%--<a id="lnkDelete" commandargument='<%#Eval("Id")%>' saver="<%#Eval("TravellerName")%>">
                                <img src='images/btn-cross.png' class="icon-close scale-with-grid" alt="" border="0" /></a>--%>
                                    <a href="#">
                                        <img src='images/arr-down.png' class="icon scale-with-grid imgOpen" alt="" border="0" /></a>
                                </div>
                                <div class="discription-block row">
                                    <div class="pass-coloum-three bigslbox">
                                        Title
                                        <asp:DropDownList ID="ddlTitle" class="input" runat="server" Style="margin-left: -2px;
                                            width: 70px;">
                                            <asp:ListItem>Mr.</asp:ListItem>
                                            <asp:ListItem>Mrs.</asp:ListItem>
                                            <asp:ListItem>Ms.</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="pass-coloum-forth">
                                        First Name<span class="valid3">*</span>
                                        <asp:TextBox ID="txtFirstName" runat="server" class="input chkvalid" Text='<%#Eval("FirstName") %>'
                                            Width="190px" />
                                        <asp:RequiredFieldValidator ID="passdetailreqtxtFirstName" runat="server" ErrorMessage='Please enter first name'
                                            CssClass="valid" Text="Please enter First Name" Display="Dynamic" ForeColor="Red"
                                            ValidationGroup="vgs1" ControlToValidate="txtFirstName" />
                                        <br />
                                        <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                            InvalidChars="0123456789" FilterMode="InvalidChars" />
                                    </div>
                                    <div class="pass-coloum-forth">
                                        Last Name<span class="valid3">*</span>
                                        <asp:TextBox ID="txtLastName" runat="server" class="input chkvalid" Text='<%#Eval("LastName") %>'
                                            Width="190px" />
                                        <asp:RequiredFieldValidator ID="passdetailreqtxtLastName" runat="server" CssClass="valid"
                                            Text="Please enter Last Name" ValidationGroup="vgs1" Display="Dynamic" ForeColor="Red"
                                            ErrorMessage="Please enter Last Name" ControlToValidate="txtLastName" />
                                        <br />
                                        <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtLastName"
                                            InvalidChars="0123456789" FilterMode="InvalidChars" />
                                    </div>
                                    <div class="pass-coloum-forth">
                                        Country of residence
                                        <asp:HiddenField ID="hdnproductid" runat="server" Value='<%#Eval("PrdtId") %>' />
                                        <asp:DropDownList ID="ddlCountry" AutoPostBack="true" runat="server" class="chkcountry countryinputddlCountry"
                                            Width="198px" Style="height: 29px; border-radius: 5px;" />
                                        <asp:RequiredFieldValidator ID="passdetailreq" runat="server" ValidationGroup="vgs1"
                                            CssClass="valid" Text='Please select Country' ErrorMessage='Please select Country'
                                            ForeColor="Red" Display="Dynamic" ControlToValidate="ddlCountry" InitialValue="0" />
                                        <asp:Label runat="server" ID="errormsg" CssClass="erroronrailpass" ForeColor="red"></asp:Label>
                                    </div>
                                    <div id="Div1" class="pass-coloum-forth" style="margin-left: -1px; width: 150px;"
                                        visible='<%#Eval("PassportIsVisible")%>' runat="server">
                                        Passport Number<span class="valid3">*</span>
                                        <asp:TextBox ID="txtPassportNumber" Text='<%#Eval("PassportNo") %>' class="input chkvalid"
                                            runat="server" Style="margin-left: -3px;" Width="190px" />
                                        <asp:RequiredFieldValidator ID="passdetailreqtxtPassportNumber" ValidationGroup="vgs1"
                                            runat="server" CssClass="valid" Text="Please enter Passport No." ErrorMessage="Please enter Passport No."
                                            Display="Dynamic" ForeColor="Red" ControlToValidate="txtPassportNumber" />
                                    </div>
                                    <div style="float: left; padding-top: 7px;">
                                        <div class="pass-coloum-forth" style="padding-left: 87px;">
                                            Date Of Birth<span class="valid3">*</span>
                                            <div id="Div2" onclick="chkDob(this,0)" onload="chkDob(this,0)">
                                                <asp:TextBox ID="txtDob" MaxLength="11" runat="server" Text='<%# (Eval("DOB") != null ? Eval("DOB") : "DD/MMM/YYYY") %>'
                                                    ToolTip="ex: 01/Jan/2014" class="input chkDobValid calDate" Width="160px" onfocusout="chkDate(event)" />
                                                <span class="imgCal calIcon" style="float: left; margin-left: 4px;" title="Select Date of birth.">
                                                </span>
                                                <div style="clear: both;">
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfDob" ValidationGroup="vgs1" runat="server" CssClass="valid"
                                                    ErrorMessage="Please enter Date of birth" Text="Please enter Date of birth" Display="Dynamic"
                                                    InitialValue="DD/MMM/YYYY" ForeColor="Red" ControlToValidate="txtDob" />
                                            </div>
                                        </div>
                                        <div class="pass-coloum-forth">
                                            Pass Start Date<span class="valid3">*</span>
                                            <div id="test" onclick="chkVal(this,'<%#Eval("MonthValidity")%>')" style="width: 200px;">
                                                <asp:TextBox ID="txtStartDate" MaxLength="11" runat="server" Text='<%# (Eval("PassStartDate") != null ? Eval("PassStartDate","{0:dd/MMM/yyyy}") : "DD/MMM/YYYY") %>'
                                                    class="input chkvalid calDate GetMinPassDate" Width="160px" onfocusout="chkDate(event)"
                                                    onchange="Productval(this)" />
                                                <span class="imgCal calIcon" style="float: left; margin-left: 4px;" title="Select Pass Start Date.">
                                                </span>
                                                <label class="MonthValidity">
                                                    <%#Eval("MonthValidity")%></label>
                                                <asp:HiddenField ID="hdnfromdate" ClientIDMode="Static" runat="server" />
                                                <asp:HiddenField ID="hdntodate" ClientIDMode="Static" runat="server" />
                                                <asp:HiddenField ID="hdnmsg" ClientIDMode="Static" runat="server" />
                                                <div style="clear: both;">
                                                </div>
                                                <asp:RequiredFieldValidator ID="passdetailreqtxtStartDate" ValidationGroup="vgs1"
                                                    runat="server" CssClass="valid" Text="Please enter Pass Start Date" ErrorMessage="Please enter Pass Start Date"
                                                    Display="Dynamic" InitialValue="DD/MMM/YYYY" ForeColor="Red" ControlToValidate="txtStartDate" />
                                            </div>
                                        </div>
                                        <div class="pass-coloum-forth" id="divNationlity" runat="server" visible='<%#Eval("NationalityIsVisible")%>'>
                                            Nationality <span class="valid3">*</span>
                                            <div style="width: 200px;">
                                                <asp:DropDownList ID="ddlNationality" runat="server" class="input chkvalid" Width="190px" />
                                                <asp:RequiredFieldValidator ID="passdetailreqtxtNationality" ValidationGroup="vgs1"
                                                    runat="server" CssClass="valid" Text="Please select Nationality." ErrorMessage="Please select Nationality."
                                                    Display="Dynamic" ForeColor="Red" ControlToValidate="ddlNationality" InitialValue="0" />
                                            </div>
                                        </div>
                                        <div style="clear: both!important; width: 80%">
                                            <asp:Label ID="lblInvalidDate" Style="display: none;" runat="server" ForeColor="red" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCountry" />
                                <asp:AsyncPostBackTrigger ControlID="lnkRemovePass" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div id="popupDiv">
    <div class="modalBackground progessposition">
    </div>
    <div class="privacy-block-outer pass progess-inner">
        <div class="red-title" style="text-align: left;">
            <span>Ticket Protection</span> <a id="btnOK" href="#" onclick="searchEvent()" style="margin-left: 711px;">
                X</a>
        </div>
        <div id="divpopupdata" runat="server" class="discription-block" style="text-align: left;
            overflow-x: scroll; height: 300px; font-size: 13px;">
        </div>
    </div>
</div>
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            Shovelling coal into the server...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    function saverhas() {
        var result = confirm('You are now deleting the whole saverpass booking, as min 2 pax is required.');
        if (result) {
            $("#MainContent_UcPassDetail_UpdateProgress1").show();
        }
        return result;
    }
    function savernothas() {
        var result = confirm('Are you sure? Do you want to delete this item?');
        if (result) {
            $("#MainContent_UcPassDetail_UpdateProgress1").show();
        }
        return result;
    }

    function pophide() {
        $("#MainContent_UcPassDetail_UpdateProgress1").hide();
    }
</script>
