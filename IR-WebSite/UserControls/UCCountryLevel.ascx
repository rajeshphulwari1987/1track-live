﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UCCountryLevel.ascx.cs"
    Inherits="UserControls_UCCountryLevel" %>
<style>
    .floteleftdiv
    {
        margin: 8px;
        font-size: 14px;
        float: left;
        width: 530px;
        line-height: 33px;
    }
 
    .floteleftdiv select
    {
        height: 25px;
        width: 250px;
        margin: 5px 20px 5px 0;
    }
    .divleft
    {
        float: left;
        width: 200px;
    }
    .divright
    {
        float: left;
        width: 250px;
    }
    .warningmsg
    {
        float: left;
        font-size: 13px;
        text-align: center;
        width: 549px;
        background: #ccc;
        line-height: 28px;
    }
</style>
<asp:UpdatePanel ID="upCULVL" runat="server">
    <ContentTemplate>
        <div style="width: 600px;">
            <div class="warningmsg">
                <span style="color: Red; font-weight: bold;">*</span> Please select the country
                you wish your pass to cover</div>
            <div id="selcunid1" runat="server" class="floteleftdiv">
                <div class="divleft">
                    Select first country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt1_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid2" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select second country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt2_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid3" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select third country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt3_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid4" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select fourth country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt4_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid5" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select fifth country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt5_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid6" class="floteleftdiv" style="text-align:center" runat="server">
                 
               
                    <asp:Button ID="btnContinue" runat="server" Text="Continue..." Width="100" OnClick="btnContinue_Click" />
              
            </div>
            <div class="floteleftdiv" runat="server">
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnContinue" />
    </Triggers>
</asp:UpdatePanel>
