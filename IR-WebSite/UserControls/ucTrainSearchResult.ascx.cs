﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI;
#endregion

public partial class UserControls_ucTrainSearchResult : UserControl
{
    #region Global Variable
    private string _tr = "";
    private int _index;
    private string _depHeading = "";
    private string _returnHeading = "";

    public string TrInbound = "";
    public string TrOutbound = "";
    private bool _isOutboundTI = false;
    private bool _isInbound;
    private bool _isOutbound;
    BookingRequestUserControl objBRUC;
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    ManageTrainDetails _master = new ManageTrainDetails();
    readonly Masters _objMaster = new Masters();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static DateTime curentDateTime;
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public string _FromDate;
    public string _ToDate;
    List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
    List<BookingRequestInfo> listBookingReqInfo = new List<BookingRequestInfo>();
    List<TrainPrice> listTrainPriceInfo = new List<TrainPrice>();
    Guid id = Guid.NewGuid();
    #endregion

    #region PageEvents
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdnReq.Value = Request.QueryString["req"] != null ? Request.QueryString["req"].Trim() : "";
            ShowMessage(0, null);
            GetCurrencyCode();
            NotIsPostBack();

            if (Session["ErrorMessage"] != null)
            {
                if (Session["ErrorMessage"].ToString().Trim() != "ErrorMaxDate")
                {
                    ShowMessage(2, Session["ErrorMessage"].ToString());
                    Session.Remove("ErrorMessage");
                }
            }
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "calenable()", true);


        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
    #endregion


    #region Empty and Postback
    void NotIsPostBack()
    {
        if (!IsPostBack)
        {
            try
            {
                hdnsiteURL.Value = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                Session["SHOPPINGCART"] = null;
                Session["BOOKING-REQUEST"] = null;
                if (Session["BookingUCRerq"] == null)
                    return;
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];

                hdnDateDepart.Value = objBRUC.depdt.ToString("dd/MMM/yyyy");
                hdnDateArr.Value = objBRUC.ReturnDate.Trim();

                ddlCountry.DataSource = _master.GetCountryDetail();
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataBind();


                if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
                {
                    var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
                    Guid CoutryID = Guid.Parse(cookie.Values["_cuntryId"]);
                    ddlCountry.SelectedValue = CoutryID.ToString().Trim();
                }

                BindResult();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            hdnCurrID.Value = currency;
        }
    }

    void BindResponseWithNull()
    {
        rptTrainResult.DataSource = null;
        rptTrainResult.DataBind();

        rptTcv.DataSource = null;
        rptTcv.DataBind();

        rptBene.DataSource = null;
        rptTrainResult.DataBind();
    }
    #endregion

    #region Bind Search Result
    int GetEurostarBookingDays(BookingRequestUserControl request)
    {
        List<string> listStCode = new List<string> { "GBSPX", "BEBMI", "FRPNO", "FRPAR", "GBASI", "FRLIL", "FRLLE", "FRMLV", "GBEBF" };
        if (listStCode.Contains(request.depstCode) && listStCode.Contains(request.arrstCode))
            return 180;
        else
            return 0;
    }
    public void BindResult()
    {
        try
        {
            BindResponseWithNull();
            if (Session["BookingUCRerq"] != null)
            {
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                var currDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));


                int daysLimit = GetEurostarBookingDays(objBRUC);
                if (daysLimit == 0)
                    daysLimit = new ManageBooking().getOneHubServiceDayCount(objBRUC.OneHubServiceName);

                var maxDate = currDate.AddDays(daysLimit - 1);

                if (objBRUC.depdt > maxDate)
                {
                    return;
                }
            }
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;

            if (pInfoSolutionsResponse == null)
                return;

            if (pInfoSolutionsResponse.ErrorMessage != null)
                ShowMessage(2, pInfoSolutionsResponse.ErrorMessage.MessageCode + ": " + pInfoSolutionsResponse.ErrorMessage.MessageDescription);

            else if (pInfoSolutionsResponse.TrainInformationList == null)
            {
                BindResponseWithNull();
                return;
            }

            else if (pInfoSolutionsResponse.TrainInformationList.Any(x => !String.IsNullOrEmpty(x.Route)))
            {

                rptTcv.DataSource = pInfoSolutionsResponse.TrainInformationList;
                rptTcv.DataBind();
            }
            else
            {
                List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.Count() > 0 ? pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList() : null;
                if (list != null)
                {
                    var trainInfoSegment = list.FirstOrDefault();
                    if (trainInfoSegment != null && (list.Count > 0 && String.IsNullOrEmpty(trainInfoSegment.JourneySolutionCode)))
                    {

                        if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
                            pInfoSolutionsResponse = new TrainInformationResponse
                            {
                                ErrorMessage = pInfoSolutionsResponse.ErrorMessage,
                                IsInternationl = pInfoSolutionsResponse.IsInternationl,
                                TrainInformationList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.PriceInfo.Any(y => y.TrainPriceList.Any())).ToArray()
                            };


                        List<Taco> listtaco = pInfoSolutionsResponse.TrainInformationList.SelectMany(a => a.PriceInfo.SelectMany(x => x.TrainPriceList.Select(t => new Taco { Domain = t.Domain, Tariffgroup = t.ServiceCode, Tic = t.ServiceTypeCode, Value = t.ServiceName }).ToList())).ToList();
                        Session["farerequest"] = GetFareRequest(listtaco);

                        ScriptManager.RegisterStartupScript(Page, GetType(), "getfare", " getfare();", true);
                        rptBene.DataSource = pInfoSolutionsResponse.TrainInformationList;
                        rptBene.DataBind();

                    }
                    else
                    {
                        rptTrainResult.DataSource = pInfoSolutionsResponse.TrainInformationList.ToList();
                        rptTrainResult.DataBind();
                    }
                }
                else
                {
                    rptBene.DataSource = null;
                    rptBene.DataBind();
                    rptTrainResult.DataSource = null;
                    rptTrainResult.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
    #endregion

    #region Make Booking Request

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            Session["IsFullPrice"] = null;
            Session["IsCheckout"] = null;
            Session["BOOKING-REQUEST"] = null;
            Session["P2POrderID"] = new ManageBooking().CreateOrder(string.Empty, AgentuserInfo.UserID, Guid.Empty, siteId, "P2P", "0", "OldITTR");
            Session["ProductType"] = "P2P";

            Boolean res = false;
            bool BeNeSearch = false;
            if (String.IsNullOrEmpty(hdnFromjsCode.Value) && String.IsNullOrEmpty(hdnTojsCode.Value) && String.IsNullOrEmpty(hdnFrTcvRoute.Value))
            {
                if (!String.IsNullOrEmpty(hdnFromRid.Value))
                    res = BookingRequestBeNe(hdnFromRid.Value, hdnFromsvcTyp.Value, hdnFromPriceId.Value, hdnFromBeNeClass.Value, "Inbound");
                if (!String.IsNullOrEmpty(hdnToRid.Value))
                    res = BookingRequestBeNe(hdnToRid.Value, hdnTosvcTyp.Value, hdnToPriceId.Value, hdnToBeNeClass.Value, "Outbound");

                BookingRequestBeNeInboundOutbound();
                BeNeSearch = true;
            }
            else
            {
                if (!String.IsNullOrEmpty(hdnFromjsCode.Value))
                    res = BookingRequestTi(hdnFromjsCode.Value, hdnFromoCd.Value, hdnFromoTypCd.Value, hdnFromoSubCd.Value, hdnFromsvCode.Value, hdnFromsvTypCode.Value, hdnFromtrainNo.Value, hdnFromtripType.Value, hdnFromagre.Value, hdnFromClass.Value, "Inbound");
                if (!String.IsNullOrEmpty(hdnTojsCode.Value))
                    res = BookingRequestTi(hdnTojsCode.Value, hdnTooCd.Value, hdnTooTypCd.Value, hdnTooSubCd.Value, hdnTosvCode.Value, hdnTosvTypCode.Value, hdnTotrainNo.Value, hdnTotripType.Value, hdnToagre.Value, hdnToClass.Value, "Outbound");
            }

            Session["P2PIdInfo"] = lstP2PIdInfo;

            if (res == true)
                Response.Redirect(BeNeSearch == true ? "~/P2PBookingCart.aspx?req=BE" : "~/P2PBookingCart.aspx?req=IT", true);
            else
                Response.Redirect("~/TrainResults", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public Boolean BookingRequestBeNe(string rid, string serviceTyoeId, string ProductGroup, string clas, string TypeOfJ)
    {
        try
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            List<BookingRequest> listBookingRequest = new List<BookingRequest>();

            if (pInfoSolutionsResponse != null)
            {
                TrainInformation trainInfo = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(x => x.Routesummaryid == rid);
                if (trainInfo != null)
                {
                    listBookingReqInfo.AddRange(trainInfo.BookingRequestInfo.Where(x => x.Productfeature.ProductGroup.Contains(ProductGroup)).Select(y => y).ToList());
                    List<PriceResponse> priceRes = trainInfo.PriceInfo.ToList();

                    if (priceRes != null)
                    {
                        List<TrainPrice> trainPricelist = new List<TrainPrice>();

                        foreach (var item in priceRes)
                        {
                            if (item.TrainPriceList != null)
                                trainPricelist.AddRange(item.TrainPriceList);
                        }
                        TrainPrice trainPriceInfo = trainPricelist.FirstOrDefault(x => x.ServiceTypeCode == serviceTyoeId && x.ProductGroup == ProductGroup);
                        listTrainPriceInfo.Add(trainPriceInfo);
                        var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");

                        if (!trainPriceInfo.IsFullPrice && Session["IsFullPrice"] == null)
                            Session["IsFullPrice"] = trainPriceInfo.IsFullPrice;


                        #region Cart Information
                        List<ShoppingCartDetails> listCart = new List<ShoppingCartDetails> {new ShoppingCartDetails
                                {
                                    Id = id,
                                    DepartureStation = trainInfo.DepartureStationName,
                                    DepartureDate = trainInfo.DepartureDate,
                                    DepartureTime = trainInfo.DepartureTime,
                                    ArrivalStation = trainInfo.ArrivalStationName,
                                    ArrivalDate = trainInfo.ArrivalDate,
                                    ArrivalTime = trainInfo.ArrivalTime,
                                    TrainNo = trainInfo.TrainNumber,
                                    
                                    Title = ddlTitle.SelectedItem.Text ,
                                    FirstName = txtFirstname.Text ,
                                    LastName = txtLastname.Text,
                                    Passenger = trainPriceInfo!=null? trainPriceInfo.NumAdults + " Adults, " + trainPriceInfo.NumBoys + " Child," + trainPriceInfo.NumYouths + " Youth," + trainPriceInfo.NumSeniors + " Senior" :"",
                                    ServiceName = trainPriceInfo!=null?  trainPriceInfo.ServiceName :"",
                                    Fare = trainPriceInfo!=null? trainPriceInfo.TravelType :"",
                                    Price =  trainPriceInfo!=null ?( BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? GetTotalPriceBeNe(trainPriceInfo,false).ToString("F") : trainPriceInfo.MinPrice):"N/A",  
                                    netPrice = trainPriceInfo!=null?  Convert.ToDecimal(trainPriceInfo.MinPrice):0                                    
                                }};

                        long POrderID = Convert.ToInt64(Session["P2POrderID"]);
                        tblP2PSale objP2P = new tblP2PSale();
                        objP2P.From = trainInfo.DepartureStationName;
                        objP2P.DateTimeDepature = Convert.ToDateTime(trainInfo.DepartureDate);
                        objP2P.To = trainInfo.ArrivalStationName;
                        objP2P.DateTimeArrival = Convert.ToDateTime(trainInfo.ArrivalDate);
                        objP2P.TrainNo = trainInfo.TrainNumber;
                        objP2P.Passenger = trainPriceInfo != null ? trainPriceInfo.NumAdults + " Adults, " + trainPriceInfo.NumBoys + " Child," + trainPriceInfo.NumYouths + " Youth," + trainPriceInfo.NumSeniors + " Senior" : "";

                        objP2P.Adult = objBRUC.Adults.ToString();
                        objP2P.Children = objBRUC.Boys.ToString();
                        objP2P.Youth = objBRUC.Youths.ToString();
                        objP2P.Senior = objBRUC.Seniors.ToString();

                        objP2P.SeviceName = trainPriceInfo != null ? trainPriceInfo.ServiceName : "";
                        objP2P.FareName = trainPriceInfo != null ? trainPriceInfo.TravelType : "";
                        objP2P.Price = trainPriceInfo != null ? (BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? GetTotalPriceBeNe(trainPriceInfo, false) : 0) : 0;
                        objP2P.DepartureTime = trainInfo.DepartureTime;
                        objP2P.ArrivalTime = trainInfo.ArrivalTime;
                        objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(objP2P.Price.ToString()));
                        objP2P.Class = clas;
                        objP2P.CurrencyId = currencyID;
                        objP2P.Terms = trainPriceInfo.FareDescription;
                        objP2P.ApiPrice = BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? GetTotalOriginalPrice(trainPriceInfo) : 0;
                        objP2P.ApiName = "BENE";

                        //site currency//
                        objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
                        objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
                        objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
                        objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
                        objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
                        objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
                        objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
                        objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
                        objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
                        objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
                        objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
                        objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
                        objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
                        objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
                        objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
                        objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
                        objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

                        objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
                        objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);

                        Guid P2PID;
                        Guid LookupID = new ManageBooking().AddP2PBookingOrder(objP2P, Convert.ToInt64(Session["P2POrderID"]), out P2PID);
                        tblP2PSale objP2PRow = new ManageBooking().getP2PSingleRowData(P2PID);

                        P2PReservationIDInfo objP2PIdInfo = new P2PReservationIDInfo();
                        objP2PIdInfo.ID = P2PID;
                        objP2PIdInfo.P2PID = objP2PRow.P2PId;
                        objP2PIdInfo.JourneyType = TypeOfJ;
                        lstP2PIdInfo.Add(objP2PIdInfo);

                        tblOrderTraveller objTraveller = new tblOrderTraveller();
                        objTraveller.ID = Guid.NewGuid();
                        objTraveller.Title = ddlTitle.SelectedItem.Text;
                        objTraveller.FirstName = txtFirstname.Text;
                        objTraveller.LastName = txtLastname.Text;
                        objTraveller.Country = (ddlCountry.SelectedValue != "0" ? Guid.Parse(ddlCountry.SelectedValue) : Guid.Empty);

                        new ManageBooking().AddTraveller(objTraveller, LookupID);


                        if (Session["SHOPPINGCART"] != null)
                        {
                            List<ShoppingCartDetails> oldList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                            if (oldList != null) listCart.AddRange(oldList);
                        }
                        Session["SHOPPINGCART"] = listCart;
                        #endregion
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BookingRequestBeNeInboundOutbound()
    {
        List<Proposedprice> listPrpose = listBookingReqInfo.Select(item => new Proposedprice
        {
            ViaStations = item.ViaStations,
            DepartureStationCode = item.DepartureStationCode,
            ArrivalStationCode = item.ArrivalStationCode,
            DepartureStationName = item.DepartureStationName,
            ArrivalStationName = item.ArrivalStationName,
            Departuredate = Convert.ToDateTime(item.DepartureDate),
            Departuretime = Convert.ToDateTime(item.DepartureTime),
            Availability = item.Availability,
            Oneticketindicator = item.Oneticketindicator,
            TravelTimeDuration = item.TravelTimeDuration,
            Productfeature = new Productfeature
            {
                Comfortclass = item.Productfeature.Comfortclass,
                Ebs = item.Productfeature.Ebs,
                Esn = item.Productfeature.Esn,
                Ischangeable = item.Productfeature.Ischangeable,
                Isrefundable = item.Productfeature.Isrefundable,
                ProductGroup = item.Productfeature.ProductGroup,
                Singleindication = item.Productfeature.Singleindication,
                Supplier = item.Productfeature.Supplier,
                Value = item.Productfeature.Value
            },
            PossibleReservation = item.PossibleReservation,
            Proposedpriceid = item.Proposedpriceid,
            Routesummaryid = item.Routesummaryid,
            Segmentid = item.Segmentid,
            Segmenttype = item.Segmenttype,
            TrainPrice = listTrainPriceInfo != null ? listTrainPriceInfo.Select(trainPriceInfo => new TrainPrice
            {
                NumAdults = trainPriceInfo.NumAdults,
                NumBoys = trainPriceInfo.NumBoys,
                NumSeniors = trainPriceInfo.NumSeniors,
                NumYouths = trainPriceInfo.NumYouths,
                CurrencyCode = trainPriceInfo.CurrencyCode,
                Domain = trainPriceInfo.Domain,
                MinPrice = trainPriceInfo.MinPrice,
                ServiceCode = trainPriceInfo.ServiceCode,
                ServiceName = trainPriceInfo.ServiceName,
                ServiceTypeCode = trainPriceInfo.ServiceTypeCode,
                ProductionModeList = trainPriceInfo.ProductionModeList
            }).ToArray() : null,
            Trainnumber = item.TrainNumber,
            PriceOffer = item.PriceOffer
        }).ToList();

        List<Passengerreply> listPreply = new List<Passengerreply>();
        foreach (var item2 in listBookingReqInfo.FirstOrDefault().PassengerListReply)
        {
            listPreply.Add(new Passengerreply
            {
                Loyaltycard = objBRUC.lstLoyalty != null ? objBRUC.lstLoyalty.ToArray() : null,
                Passengerid = item2.Passengerid,
                SeatPreference = SeatPreference.S,
                FirstName = txtFirstname.Text,
                LastName = txtLastname.Text,
            });
        }
        var request = new PurchasingForServiceRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            PassengerId = listTrainPriceInfo != null ? listTrainPriceInfo.FirstOrDefault().PassengerId : null,
            PassengerListReply = listPreply.Distinct().ToArray(),
            Ticketlanguage = new BookinginfoTicketlanguage
            {
                Language = Language.nl_BE
            },
            BookingRequestList = listPrpose.ToArray()
        };

        BookingRequest bkrequest = new BookingRequest { PurchasingForServiceRequest = request, Id = id };

        List<BookingRequest> listBookingRequest = new List<BookingRequest>();
        listBookingRequest.Add(bkrequest);
        Session["BOOKING-REQUEST"] = listBookingRequest;

        //bkrequest.DepartureStationName = trainInfo.DepartureStationName;

        //listBookingRequest.Add(bkrequest);
        //if (Session["BOOKING-REQUEST"] != null)
        //{
        //    var oldList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
        //    if (oldList != null) listBookingRequest.AddRange(oldList);
        //}

        //Session["BOOKING-REQUEST"] = listBookingRequest;

    }

    public Boolean BookingRequestTi(string jsCode, string oCd, string oTypCd, string oSubCd, string svCode, string svTypCode, string trainNo, string tripType, string agre, string clas, string TypeOfJ)
    {
        try
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null)
            {
                TrainInformation trainInfo = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(tp => tp.JourneySolutionCode == jsCode);
                if (trainInfo != null)
                {
                    TrainInfoSegment[] trainSeglist = trainInfo.TrainInfoSegment.Where(x => x.JourneySolutionCode == jsCode).ToArray();
                    TrainInfoSegment trainSegInfo = trainInfo.TrainInfoSegment.FirstOrDefault(x => x.JourneySolutionCode == jsCode);
                    TrainInfoSegment trainSegInfolast = trainInfo.TrainInfoSegment.LastOrDefault(x => x.JourneySolutionCode == jsCode);
                    List<TrainPrice> trainPriceInfoList = trainInfo.PriceInfo.SelectMany(x => x.TrainPriceList.Select(t => t)).ToList();
                    TrainPrice trainPriceInfo = trainPriceInfoList.FirstOrDefault(o => o.Offer.OfferCode == Convert.ToInt32(oCd) && o.Offer.OfferTypeCode == Convert.ToInt32(oTypCd) && o.Offer.OfferSubgroupCode == Convert.ToInt32(oSubCd) && o.ServiceCode == svCode && o.ServiceTypeCode == svTypCode);
                    Guid id = Guid.NewGuid();

                    var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                    #region Cart Information
                    List<ShoppingCartDetails> listCart = trainSegInfo != null ? new List<ShoppingCartDetails> {new ShoppingCartDetails
                            {
                                Id = id,
                                DepartureStation = trainSegInfo.DepartureStationName,
                                DepartureDate = trainSegInfo.DepartureDate,
                                DepartureTime = trainSegInfo.DepartureTime,

                                ArrivalStation = trainSegInfo.ArrivalStationName,
                                ArrivalDate = trainSegInfo.ArrivalDate,
                                ArrivalTime = trainSegInfo.ArrivalTime,

                                TrainNo = trainSegInfo.TrainNumber,
                                Title = ddlTitle.SelectedItem.Text ,
                                FirstName = txtFirstname.Text ,
                                LastName= txtLastname.Text,
                                Passenger = trainPriceInfo != null ? trainPriceInfo.NumAdults + " Adults, " + trainPriceInfo.NumBoys + " Child," + trainPriceInfo.NumYouths + " Youths," + trainPriceInfo.NumSeniors + " Seniors":"",
                                ServiceName = trainPriceInfo != null ?  trainPriceInfo.ServiceName :"",
                                Fare = trainPriceInfo != null ? trainPriceInfo.TravelType :"",
                                Price =  trainPriceInfo!=null ?( BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPriceInfo.MinPrice), siteId, srcCurId, currencyID).ToString("F") : trainPriceInfo.MinPrice):"N/A",  
                                netPrice = trainPriceInfo!=null?  Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPriceInfo.MinPrice), siteId, srcCurId, currencyID).ToString("F")):0       
                                
                            }} : null;

                    long POrderID = Convert.ToInt64(Session["P2POrderID"]);


                    tblP2PSale objP2P = new tblP2PSale();

                    objP2P.From = trainSegInfo.DepartureStationName;
                    objP2P.DateTimeDepature = Convert.ToDateTime(trainSegInfo.DepartureDate);

                    objP2P.To = trainSegInfolast.ArrivalStationName;
                    objP2P.DateTimeArrival = Convert.ToDateTime(trainSegInfolast.ArrivalDate);
                    objP2P.TrainNo = trainSegInfo.TrainNumber;

                    objP2P.Passenger = trainPriceInfo != null ? objBRUC.Adults + " Adults, " + objBRUC.Boys + " Child," + objBRUC.Youths + " Youth," + objBRUC.Seniors + " Senior" : "";

                    objP2P.Adult = objBRUC.Adults.ToString();
                    objP2P.Children = objBRUC.Boys.ToString();
                    objP2P.Youth = objBRUC.Youths.ToString();
                    objP2P.Senior = objBRUC.Seniors.ToString();

                    objP2P.SeviceName = trainPriceInfo != null ? trainPriceInfo.ServiceName : "";
                    objP2P.FareName = trainPriceInfo != null ? trainPriceInfo.TravelType : "";
                    objP2P.Price = trainPriceInfo != null ? (BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPriceInfo.MinPrice), siteId, srcCurId, currencyID) : 0) : 0;
                    objP2P.DepartureTime = trainSegInfo.DepartureTime;
                    objP2P.ArrivalTime = trainSegInfo.ArrivalTime;
                    objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(objP2P.Price.ToString()));
                    objP2P.Class = clas;
                    objP2P.CurrencyId = currencyID;
                    objP2P.Terms = trainPriceInfo.FareDescription;
                    objP2P.ApiPrice = BusinessOneHub.IsNumeric(trainPriceInfo.MinPrice) ? Convert.ToDecimal(trainPriceInfo.MinPrice) : 0;
                    objP2P.ApiName = "ITALIA";

                    //site currency//
                    objP2P.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Empty, POrderID);
                    objP2P.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Empty, POrderID);
                    objP2P.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Empty, POrderID);
                    objP2P.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Empty, POrderID);
                    objP2P.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Empty, POrderID);
                    objP2P.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Empty, POrderID);
                    objP2P.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Empty, POrderID);
                    objP2P.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Empty, POrderID);
                    objP2P.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Empty, POrderID);
                    objP2P.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Empty, POrderID);
                    objP2P.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Empty, POrderID);
                    objP2P.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Empty, POrderID);
                    objP2P.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Empty, POrderID);
                    objP2P.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Empty, POrderID);
                    objP2P.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Empty, POrderID);
                    objP2P.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Empty, POrderID);
                    objP2P.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Empty, POrderID);

                    objP2P.EUR_EUT = _masterBooking.GetCurrencyMultiplier("E_EUT", Guid.Empty, POrderID);
                    objP2P.EUR_EUB = _masterBooking.GetCurrencyMultiplier("E_EUB", Guid.Empty, POrderID);
                    if (objP2P.Price == 0)
                        return false;


                    Guid P2PID;
                    Guid LookupID = new ManageBooking().AddP2PBookingOrder(objP2P, Convert.ToInt64(Session["P2POrderID"]), out P2PID);
                    tblP2PSale objP2PRow = new ManageBooking().getP2PSingleRowData(P2PID);

                    P2PReservationIDInfo objP2PIdInfo = new P2PReservationIDInfo();
                    objP2PIdInfo.ID = P2PID;
                    objP2PIdInfo.P2PID = objP2PRow.P2PId;
                    objP2PIdInfo.JourneyType = TypeOfJ;
                    lstP2PIdInfo.Add(objP2PIdInfo);

                    tblOrderTraveller objTraveller = new tblOrderTraveller();
                    objTraveller.ID = Guid.NewGuid();
                    objTraveller.Title = ddlTitle.SelectedItem.Text;
                    objTraveller.FirstName = txtFirstname.Text;
                    objTraveller.LastName = txtLastname.Text;
                    objTraveller.Country = (ddlCountry.SelectedValue != "0" ? Guid.Parse(ddlCountry.SelectedValue) : Guid.Empty);

                    new ManageBooking().AddTraveller(objTraveller, LookupID);

                    if (Session["SHOPPINGCART"] != null)
                    {
                        List<ShoppingCartDetails> oldList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
                        if (oldList != null) if (listCart != null) listCart.AddRange(oldList);
                    }
                    Session["SHOPPINGCART"] = listCart;
                    #endregion

                    #region Request Information

                    if (trainPriceInfo == null)
                        return false;

                    List<BookingRequest> listBookingRequest = new List<BookingRequest>();
                    BookingRequest bkrequest = new BookingRequest();
                    bkrequest.DepartureStationName = trainInfo.DepartureStationName;
                    bkrequest.IsInternational = pInfoSolutionsResponse.IsInternationl;

                    bool isTicketLess = trainInfo.TrainInfoSegment.Any(x => x.JourneySolutionCode == jsCode && x.IsTicketLessSale);

                    Session["SegmentType"] = isTicketLess ? "M" : trainInfo.SegmentType;
                    List<Owner> listowner = new List<Owner>();
                    listowner.Add(new Owner
                      {
                          Lastname = txtFirstname.Text + " " + txtLastname.Text,
                          FirstName = ddlTitle.SelectedItem.Text,
                          DocumentNumber = "",
                          DocumentType = DocumentType.None
                      });
                    PurchasingForServiceOwnerRequest request = new PurchasingForServiceOwnerRequest
                    {
                        Header = new Header
                        {
                            onehubusername = "#@dots!squares",
                            onehubpassword = "#@dots!squares",
                            unitofwork = 0,
                            language = Language.nl_BE,
                        },

                        IssuanceMode = isTicketLess ? IssuanceMode.T : IssuanceMode.D,

                        //S= Standard , T=Ticketless, P=Postoclick ticketless , Q=Postoclick con ritiro alla SS, D= Differed Print , O= Hold on, C=change
                        //IssuanceMode = svTypCode=="1" ? IssuanceMode.D : IssuanceMode.T,
                        PurchaseOption = false,

                        PurchasingForServiceOwnerGroup = new PurchasingForServiceOwner[] 
                            { 
                                new PurchasingForServiceOwner
                                    { 
                                    Class = trainPriceInfo.Class,
                                    NumAdults = objBRUC.Adults,
                                    NumBoys = objBRUC.Boys,
                                    NumSeniors = objBRUC.Seniors,
                                    NumYouths = objBRUC.Youths,
                                    Owners = listowner.ToArray(),
                                    SeparatedTicketsInUse = true,
                                    ServicePreferences =trainPriceInfo.ServicePreferenceList.Select(x=>new ServicePreference
                                    {
                                        Bed=x.Bed,
                                        Service=x.Service,
                                        TrainNumber=x.TrainNumber                                    
                                    }).ToArray(),                                  
                                   
                                    ServiceSetting = new ServiceSetting
                                    {
                                        transportadultpassengersnumber = 0,
                                        transportboypassengersnumber = 0,
                                        traveltype = tripType
                                    },
                                    SolutionRate = new SolutionRate
                                    {
                                        JourneySolutionCode = jsCode,
                                        Offer = new Offer
                                        {
                                            Agreement = agre,
                                            OfferCode = int.Parse(oCd),
                                            OfferSubgroupCode = int.Parse(oSubCd),
                                            OfferTypeCode = int.Parse(oTypCd),
                                        }
                                    }
                                 }
                            }
                    };
                    bkrequest.PurchasingForServiceOwnerRequest = request;
                    bkrequest.Id = id;
                    listBookingRequest.Add(bkrequest);

                    if (Session["BOOKING-REQUEST"] != null)
                    {
                        List<BookingRequest> oldList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                        if (oldList != null) listBookingRequest.AddRange(oldList);
                    }
                    Session["BOOKING-REQUEST"] = listBookingRequest;

                    #endregion
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            return false;
        }
    }


    #endregion

    #region Bind Heading ROUTE Details
    public void BindHeading()
    {
        TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
        objBRUC = Session["BookingUCRerq"] as BookingRequestUserControl;
        _FromDate = objBRUC.depdt.ToString("dd-MM-yyyy");
        _ToDate = string.IsNullOrEmpty(objBRUC.ReturnDate) ? objBRUC.depdt.ToString("dd-MM-yyyy") : Convert.ToDateTime(objBRUC.ReturnDate).ToString("dd-MM-yyyy");

        if (!_isOutbound)
        {
            if (string.IsNullOrEmpty(pInfoSolutionsResponse.TrainInformationList[_index].JourneySolutionCode) && pInfoSolutionsResponse.TrainInformationList[0].DepartureStationCode != pInfoSolutionsResponse.TrainInformationList[_index].DepartureStationCode && pInfoSolutionsResponse.TrainInformationList[0].ArrivalStationCode != pInfoSolutionsResponse.TrainInformationList[_index].ArrivalStationCode)
                OutBondHeading();
            if (pInfoSolutionsResponse.TrainInformationList[_index].IsReturn && !string.IsNullOrEmpty(pInfoSolutionsResponse.TrainInformationList[_index].JourneySolutionCode))
                OutBondHeading();
        }
        else
        {
            _returnHeading = string.Empty;
            TrOutbound = string.Empty;
        }

        if (!_isInbound)
        {

            if (!pInfoSolutionsResponse.TrainInformationList[_index].IsReturn)
            {

                _depHeading = "<div class='colum-one'><b>From : </b>" + objBRUC.FromDetail + " </div><div class='colum-one'><b>To : </b> " + objBRUC.ToDetail + " </div><div class='colum-one'><b>Date : </b>" + objBRUC.depdt.ToString("dd/MMM/yyyy") + " </div><div class='colum-one'><b>Passenger : </b>" + objBRUC.Adults.ToString() + " Adult  --  " + objBRUC.Boys.ToString() + " Child  --  " + objBRUC.Youths.ToString() + " Youth  --  " + objBRUC.Seniors.ToString() + " Senior</div> <div class='colum-one'><h2 style='font-size:20px!important;color:#FFF!important;'>Outbound</h2></div>";
                TrInbound = "<div class='train-search-result-topblock'>" + _depHeading + "</div>";
                _isInbound = true;
            }
        }
        else
        {
            _depHeading = string.Empty;
            TrInbound = string.Empty;
        }

    }
    #endregion

    void OutBondHeading()
    {

        _returnHeading = "<tr>" +
                            "<td colspan='5' style='background:#4A4A4A'>" +
                             "<table cellpadding='0' cellspacing='0' width='100%' class='detail-table' >" +
                            "<tr>" +
                            "<td colspan='2'> <h2 class='InPosition'>Inbound</h2></td>" +
                            "</tr>" +
                             "</table>" +
                            "</td>" +
                            "</tr>";
        _returnHeading = _returnHeading + "<tr>" +
                            "<td colspan='5' style='background:#4A4A4A'>" +
                            "<table cellpadding='0' cellspacing='0' width='100%' class='detail-table' >" +
                            "<tr>" +
                            "<td> <strong> From</strong> : " + objBRUC.ToDetail + "  </td>" +
                            "<td><strong> To</strong> :  " + objBRUC.FromDetail +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td> <strong> Date</strong> : " + objBRUC.ReturnDate + "  </td>" +
                            "<td><strong> Passenger </strong> : "
                            + objBRUC.Adults.ToString() + " Adult </b> -- <b> " + objBRUC.Boys.ToString() + " Child </b> -- <b>"
                            + objBRUC.Youths.ToString() + " Youth </b> -- <b> " + objBRUC.Seniors.ToString() + " Senior" +
                            "</td>" +
                            "</tr>" +
                            "</table>" +
                            "</td>" +
                            "</tr>";

        TrOutbound = _returnHeading;
        _isOutbound = true;

    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                hdnDisplaySendJourney.Value = "true";
                //btnNext.Visible = false;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rptBene_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;

            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
                pInfoSolutionsResponse = new TrainInformationResponse
                {
                    ErrorMessage = pInfoSolutionsResponse.ErrorMessage,
                    IsInternationl = pInfoSolutionsResponse.IsInternationl,
                    TrainInformationList = pInfoSolutionsResponse.TrainInformationList.Where(x => x.PriceInfo.Any(y => y.TrainPriceList.Any())).ToArray()
                };


            if (pInfoSolutionsResponse != null && _index == pInfoSolutionsResponse.TrainInformationList.Count())
                return;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string trnew = "";
                string DisplayResult = "";

                Label lblOutBound = e.Item.FindControl("lblOutBound") as Label;
                if (lblOutBound != null && pInfoSolutionsResponse.TrainInformationList[_index].IsReturn)
                {
                    BindHeading();
                    lblOutBound.Text = TrOutbound;
                }
                Literal ltrDepDate = e.Item.FindControl("ltrDepDate") as Literal;
                Literal ltrArvDate = e.Item.FindControl("ltrArrDate") as Literal;
                if (!_isOutbound && _isInbound)
                {
                    ltrDepDate.Text = ltrDepDate.Text == _FromDate ? string.Empty : ltrDepDate.Text;
                    ltrArvDate.Text = ltrArvDate.Text == _ToDate || ltrArvDate.Text == _FromDate ? string.Empty : ltrArvDate.Text;
                }
                else
                {
                    ltrDepDate.Text = ltrDepDate.Text == _FromDate || ltrDepDate.Text == _ToDate ? string.Empty : ltrDepDate.Text;
                    ltrArvDate.Text = ltrArvDate.Text == _ToDate ? string.Empty : ltrArvDate.Text;
                }

                if (pInfoSolutionsResponse != null)
                {
                    var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                    Repeater view = e.Item.FindControl("GrdRouteInfo") as Repeater;
                    if (view != null)
                    {
                        view.DataSource = listTrainInfo.TrainInfoSegment.ToList();
                        view.DataBind();
                    }
                    Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;

                    if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                    {
                        lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");
                    }
                    if (pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo != null)
                    {

                        List<TrainPrice> list = new List<TrainPrice>();
                        foreach (var item in pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo.ToList())
                            if (item.TrainPriceList != null)
                                list.AddRange(item.TrainPriceList);
                        list.Where(x => x != null).ToList();
                        string str1Class = "";
                        var lstnewCls = list.Where(a => a.Class == "1");
                        if (lstnewCls != null && (lstnewCls.Count() > 0))
                        {
                            foreach (var item in lstnewCls)
                            {
                                str1Class = str1Class + "<td class='clsServiceName'>" + item.ServiceName + "</td>";
                            }

                            str1Class = "<td style='width:30%;'></td>" + str1Class + "</tr>";
                            _tr = str1Class;
                            trnew = string.Empty;
                            trnew = trnew + "<tr>" + "<td style='width:28%;padding-left:2%; font-weight:bold;'>1st Class</td>";
                            foreach (var itemp in lstnewCls)
                            {
                                string td = string.Empty;
                                TrainPrice trainPrice = lstnewCls.FirstOrDefault(x => x.ProductGroup == itemp.ProductGroup);

                                if (trainPrice != null)
                                {
                                    var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
                                    if (trainPrice.MinPrice.Contains("Sold"))
                                        td = td + "<td style='height:27px;'>Sold out</td>";
                                    else
                                    {

                                        if (!_isOutbound && _isInbound)
                                            td = td + "<td class='tresult-parent'><label class='tooltip'><input type='radio' class='priceB' name='rdoFrom' value='" +
                                                     itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "'/><span class='tresult'><img class='callOut' src='images/blank.gif' />" + trainPrice.FareDescription + "<div class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></div>" + GetPriceTable(trainPrice, false) + "</span>" + currency + "" +
                                                   "<span class='bprice'>" + GetTotalPriceBeNe(trainPrice, false) + "</span><span class='bprice1'>" + itemp.ServiceName + "," + GetBeneUserPrice(trainPrice, 1) + "," + GetBeneUserPrice(trainPrice, 2) + "," + GetBeneUserPrice(trainPrice, 3) + "," + GetBeneUserPrice(trainPrice, 4) + "</span></label><label>" + (trainPrice.IsFullPrice == true ? "" : "<div class='info-block masterTooltip' title='This is not valid price, the valid price will only be shown at the next step.' >?</div>") + "<label></td>";
                                        else
                                            td = td + "<td class='tresult-parent'><label class='tooltip'><input type='radio' class='priceB' name='rdoTo' value='" +
                                                 itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "'/> <span  class='tresult'><img class='callOut' src='images/blank.gif'/>" + trainPrice.FareDescription + "<div class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></div>" + GetPriceTable(trainPrice, false) + "</span>" + currency + "" +
                                                "<span class='bprice'>" + GetTotalPriceBeNe(trainPrice, false) + "</span><span class='bprice1'>" + itemp.ServiceName + "," + GetBeneUserPrice(trainPrice, 1) + "," + GetBeneUserPrice(trainPrice, 2) + "," + GetBeneUserPrice(trainPrice, 3) + "," + GetBeneUserPrice(trainPrice, 4) + "</span></label><label>" + (trainPrice.IsFullPrice == true ? "" : "<div class='info-block masterTooltip' title='This is not valid price, the valid price will only be shown at the next step.' >?</div>") + "<label></td>";
                                    }
                                }
                                else
                                    td = td + "<td style='height:27px;color:#ffffff'>. </td>";

                                trnew = trnew + td;
                            }
                            _tr = _tr + trnew + "</tr>";
                            DisplayResult = DisplayResult + _tr;
                        }
                        else if (list.Count() == 0)
                        {
                            DisplayResult = "<tr><td class='clsSoldOut'>Sold out</td></tr>";
                            HtmlGenericControl divTrn = e.Item.FindControl("DivTr") as HtmlGenericControl;
                            divTrn.InnerHtml = DisplayResult;
                        }
                        string str2Class = ""; _tr = "";
                        var lstnewCls2 = list.Where(a => a.Class == "2");
                        if (lstnewCls2 != null && (lstnewCls2.Count() > 0))
                        {
                            foreach (var item in lstnewCls2)
                            {
                                str2Class = str2Class + "<td class='clsServiceName'>" + item.ServiceName + "</td>";
                            }

                            str2Class = "<td style='width:30%;'></td>" + str2Class + "</tr>";
                            _tr = _tr + str2Class;
                            trnew = string.Empty;
                            trnew = trnew + "<tr>" + "<td style='width:28%;padding-left:2%; font-weight:bold;'>2nd Class</td>";
                            foreach (var itemp in lstnewCls2)
                            {
                                string td = string.Empty;
                                TrainPrice trainPrice = lstnewCls2.FirstOrDefault(x => x.ProductGroup == itemp.ProductGroup);

                                if (trainPrice != null)
                                {
                                    var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
                                    if (BusinessOneHub.IsNumeric(trainPrice.MinPrice))
                                    {
                                        if (trainPrice.MinPrice.Contains("Sold"))
                                            td = td + "<td class='clsSoldOut'>Sold out</td>";
                                        else
                                        {

                                            if (!_isOutbound && _isInbound)
                                                td = td + "<td class='tresult-parent'><label class='tooltip'><input type='radio' class='priceB' name='rdoFrom' value='" +
                                                     itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "'/><span  class='tresult'> <img class='callOut' src='images/blank.gif'/>" + trainPrice.FareDescription + "<div class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></div>" + GetPriceTable(trainPrice, false) + "</span>" + currency + " " +
                                                     "<span class='bprice'>" + GetTotalPriceBeNe(trainPrice, false) + "</span>" + "<span class='bprice1'>" + itemp.ServiceName + "," + GetBeneUserPrice(trainPrice, 1) + "," + GetBeneUserPrice(trainPrice, 2) + "," + GetBeneUserPrice(trainPrice, 3) + "," + GetBeneUserPrice(trainPrice, 4) + "</span></label><label>" + (trainPrice.IsFullPrice == true ? "" : "<div class='info-block masterTooltip' title='This is not valid price, the valid price will only be shown at the next step.' >?</div>") + "<label></td>";
                                            else
                                                td = td + "<td class='tresult-parent'><label class='tooltip'><input type='radio' class='priceB' name='rdoTo' value='" +
                                                     itemp.Routesummaryid + "," + itemp.ServiceTypeCode + "," + itemp.ProductGroup + "," + itemp.Class + "'/><span  class='tresult'> <img class='callOut' src='images/blank.gif' />" + trainPrice.FareDescription + "<div class='" + itemp.ServiceTypeCode + "'><img src='images/fareloading.gif' style='position: static;padding-left: 32%'/></div>" + GetPriceTable(trainPrice, false) + "</span>" + currency + " " +
                                                      "<span class='bprice'>" + GetTotalPriceBeNe(trainPrice, false) + "</span><span class='bprice1'>" + itemp.ServiceName + "," + GetBeneUserPrice(trainPrice, 1) + "," + GetBeneUserPrice(trainPrice, 2) + "," + GetBeneUserPrice(trainPrice, 3) + "," + GetBeneUserPrice(trainPrice, 4) + "</span></label><label>" + (trainPrice.IsFullPrice == true ? "" : " <div class='info-block masterTooltip' title='This is not valid price, the valid price will only be shown at the next step.' >?</div>") + "<label></td>";
                                        }
                                    }

                                    else
                                        td = td + "<td style='height:27px;'>" + trainPrice.MinPrice + "</td>";

                                }
                                else
                                    td = td + "<td style='height:27px;'>Sold out</td>";

                                trnew = trnew + td;
                            }
                            _tr = _tr + trnew + "</tr>";
                            DisplayResult = DisplayResult + _tr;
                        }
                        HtmlGenericControl divTr = e.Item.FindControl("DivTr") as HtmlGenericControl;
                        divTr.InnerHtml = DisplayResult;
                    }
                    else
                    {
                        DisplayResult = "<tr><td class='clsSoldOut''>Sold out</td></tr>";
                        HtmlGenericControl divTr = e.Item.FindControl("DivTr") as HtmlGenericControl;
                        divTr.InnerHtml = DisplayResult;
                    }
                }

                _index++;
            }
            if (e.Item.ItemType == ListItemType.Header)
            {
                Label lblHeaderInfo = e.Item.FindControl("lblHeaderInfo") as Label;
                if (lblHeaderInfo != null)
                {
                    BindHeading();
                    lblHeaderInfo.Text = TrInbound;
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public string GetPriceTable(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        string priceTable = "<table width='100%' class='tariffPriceTable' cellpadding='0' cellspacing='0'> <tbody><tr> <th colspan='3'>Price Table</th> </tr>";
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Adults </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Childs </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Youths </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
            priceTable += "<tr class='cellWhite'><td>Seniors </td> <td> <span class='pound'> " + currency + " " + GetRoundPrice(price) + " each </span>   </td> </tr>";
        }

        if (!isTrenItalia)
            foreach (var item in tp.SeatReservationDetail)
            {
                priceTable += "<tr class='cellWhite' style='background:#dfdfdf'><td><b>" + item.Supplier + "</b>  " + item.Journey + " </td> <td> <span class='pound'>" + item.ReservationStatus + "  </span>   </td> </tr>";
            }
        priceTable += "</tbody></table>";
        return priceTable;
    }

    public Decimal GetTotalPriceBeNe(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (Convert.ToDecimal(tp.MinPrice) > 0)
        {
            string MinPrice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.MinPrice), siteId, srcCurId, currencyID).ToString("F");
            price = Convert.ToDecimal(GetRoundPrice(MinPrice));
        }
        return price;
    }

    public Decimal GetTotalPrice(TrainPrice tp, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
        }
        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
        }
        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
        }
        return price;
    }

    public Decimal GetTotalOriginalPrice(TrainPrice tp)
    {
        Decimal price = 0;
        if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitAdultPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
        }
        if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitChildPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
        }
        if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitSeniorPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
        }
        if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
        {
            string Aprice = Convert.ToDecimal(tp.PerUnitYouthPrice).ToString("F");
            price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
        }
        return price;
    }

    public string GetBeneUserPrice(TrainPrice tp, int Type)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
        Decimal price = 0;
        switch (Type)
        {
            case 1:
                if (Convert.ToDecimal(tp.PerUnitAdultPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitAdultPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumAdults;
                }
                break;
            case 2:
                if (Convert.ToDecimal(tp.PerUnitChildPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitChildPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumBoys;
                }
                break;
            case 3:
                if (Convert.ToDecimal(tp.PerUnitSeniorPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitSeniorPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumSeniors;
                }
                break;
            case 4:
                if (Convert.ToDecimal(tp.PerUnitYouthPrice) > 0)
                {
                    string Aprice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(tp.PerUnitYouthPrice), siteId, srcCurId, currencyID).ToString("F");
                    price += Convert.ToDecimal(GetRoundPrice(Aprice)) * tp.NumYouths;
                }
                break;
        }
        return price.ToString();
    }

    string GetRoundPrice(string price)
    {
        //Please round up the prices to nearest above 0.50
        //If he price is 100.23, mark it as 100.50, 
        //If the price is 100.65 mark it as 102.00

        string[] strPrice = price.ToString().Split('.');

        if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
            return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
        else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
            return strPrice[0].Trim() + ".50";
        else
            return strPrice[0].Trim() + ".00";

    }

    protected void rptTrainResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null && _index == pInfoSolutionsResponse.TrainInformationList.Count())
                return;


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                Repeater view = e.Item.FindControl("GrdRouteInfo") as Repeater;
                if (view != null)
                {
                    view.DataSource = listTrainInfo.TrainInfoSegment.Select(y => y);
                    view.DataBind();
                }
                Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;

                Label lblOutBound = e.Item.FindControl("lblOutBound") as Label;

                if (lblOutBound != null && pInfoSolutionsResponse.TrainInformationList[_index].IsReturn && !_isOutboundTI && pInfoSolutionsResponse.TrainInformationList[0].DepartureStationCode != pInfoSolutionsResponse.TrainInformationList[_index].DepartureStationCode && pInfoSolutionsResponse.TrainInformationList[0].ArrivalStationCode != pInfoSolutionsResponse.TrainInformationList[_index].ArrivalStationCode)
                {
                    OutBondHeading();
                    lblOutBound.Text = TrOutbound;
                    _isOutboundTI = true;
                }

                Literal ltrDepDate = e.Item.FindControl("ltrDepDate") as Literal;
                Literal ltrArvDate = e.Item.FindControl("ltrArrDate") as Literal;
                if (!_isOutboundTI && _isInbound)
                {
                    ltrDepDate.Text = ltrDepDate.Text == _FromDate ? string.Empty : ltrDepDate.Text;
                    ltrArvDate.Text = ltrArvDate.Text == _ToDate || ltrArvDate.Text == _FromDate ? string.Empty : ltrArvDate.Text;
                }
                else
                {
                    ltrDepDate.Text = ltrDepDate.Text == _FromDate || ltrDepDate.Text == _ToDate ? string.Empty : ltrDepDate.Text;
                    ltrArvDate.Text = ltrArvDate.Text == _ToDate ? string.Empty : ltrArvDate.Text;
                }

                if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                {
                    lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");
                }
                string jsCode = listTrainInfo.JourneySolutionCode.Trim().ToUpper();
                string tripType = listTrainInfo.TripType;
                if (listTrainInfo.PriceInfo != null && !listTrainInfo.TrainInfoSegment.Any(x => x.TrainDescr == "RV" || x.TrainDescr == "RE"))
                {
                    var lPrice = listTrainInfo.PriceInfo.Where(a => a.TrainPriceList != null).ToList();

                    List<TrainPrice> list = listTrainInfo.PriceInfo.SelectMany(x => x.TrainPriceList.Select(y => y)).ToList();
                    list = list.OrderByDescending(t => Convert.ToDecimal(t.MaxPrice)).Select(tt => tt).ToList();
                    if (lPrice.Count() > 0 && list.Count > 0)
                    {

                        string trainNo = string.Empty;
                        var trainInfoSegment = listTrainInfo.TrainInfoSegment.FirstOrDefault(x => x.JourneySolutionCode == jsCode);
                        if (trainInfoSegment != null)
                        {
                            trainNo = trainInfoSegment.TrainNumber;
                        }

                        List<string> offer = list.Select(x => x.TravelType).Distinct().ToList();
                        List<string> serviceName = list.Select(x => x.ServiceName).Distinct().ToList();

                        _tr = "";
                        string th = "<th style='width:30%;'></th>";
                        _tr = _tr + "<tr>";
                        foreach (var item in offer)
                        {
                            th = th + "<th>" + item + "</th>";
                        }
                        _tr = _tr + th;
                        _tr = _tr + "</tr>";

                        foreach (var sItem in serviceName)
                        {
                            string trnew = string.Empty;
                            trnew = trnew + "<tr>" + "<td style='width:28%;padding-left:2%'>" + sItem + "</td>";

                            string td = string.Empty;
                            foreach (var item in offer)
                            {
                                var firstOrDefault = list.FirstOrDefault(x => x.TravelType == item && x.ServiceName == sItem);
                                if (firstOrDefault != null)
                                {

                                    var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");
                                    if (BusinessOneHub.IsNumeric(firstOrDefault.MinPrice))
                                    {
                                        string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(firstOrDefault.MinPrice), siteId, srcCurId, currencyID).ToString("F");
                                        if (!_isOutboundTI)
                                        {
                                            td = td +
                                                 "<td class='tresult-parent'><label class='tooltip'><input type='radio' class='priceB' name='rdoFromTI' value='" +
                                                 jsCode + "," + trainNo + "," + tripType + "," +
                                                 firstOrDefault.ServiceCode + "," + firstOrDefault.ServiceTypeCode + "," +
                                                 firstOrDefault.Offer.OfferCode + "," +
                                                 firstOrDefault.Offer.OfferTypeCode + "," +
                                                 firstOrDefault.Offer.OfferSubgroupCode + "," +
                                                 firstOrDefault.Offer.Agreement.Trim() + "," +
                                                 firstOrDefault.Class + "' ><span  class='tresult'><img class='callOut' src='images/blank.gif'/> " + Server.HtmlDecode(firstOrDefault.FareDescription) + " </span>" + currency + " <span class='bprice'>" + GetRoundPrice(price) + "</span>" + "<span class='bprice1'>" + firstOrDefault.TravelType + "</span>" + "</label></td>";
                                        }
                                        else
                                        {
                                            td = td +
                                                 "<td class='tresult-parent'><label class='tooltip'><input type='radio' class='priceB' name='rdoToTI' value='" +
                                                 jsCode + "," + trainNo + "," + tripType + "," +
                                                 firstOrDefault.ServiceCode + "," + firstOrDefault.ServiceTypeCode + "," +
                                                 firstOrDefault.Offer.OfferCode + "," +
                                                 firstOrDefault.Offer.OfferTypeCode + "," +
                                                 firstOrDefault.Offer.OfferSubgroupCode + "," +
                                                 firstOrDefault.Offer.Agreement.Trim() + "," +
                                                 firstOrDefault.Class + "' ><span  class='tresult'><img class='callOut' src='images/blank.gif'/>" + Server.HtmlDecode(firstOrDefault.FareDescription) + "  </span>" + currency + " <span class='bprice'>" + GetRoundPrice(price) + "</span>" + "<span class='bprice1'>" + firstOrDefault.TravelType + "</span>" + "</label></td>";
                                        }
                                    }
                                    else
                                        td = td + "<td style='height:27px; color:#9E0B0F;'>" + firstOrDefault.MinPrice + "</td>";
                                }
                                else
                                    td = td + "<td class='clsSoldOut'>Sold out</td>";
                            }
                            trnew = trnew + td;
                            _tr = _tr + trnew + "</tr>";
                            HtmlGenericControl divTr = e.Item.FindControl("DivTr") as HtmlGenericControl;
                            divTr.InnerHtml = _tr;
                        }
                    }

                    else
                    {
                        string trnew = string.Empty;
                        trnew = trnew + "<tr>" + "<td style='width:100%;padding-left:2%'>Sold out</td></tr>";
                        HtmlGenericControl divTr = e.Item.FindControl("DivTr") as HtmlGenericControl;
                        divTr.InnerHtml = trnew;
                    }
                    _index++;
                }
                else if (listTrainInfo.TrainInfoSegment.Any(x => x.TrainDescr == "RV" || x.TrainDescr == "RE"))
                {
                    Button btnGetPrice = e.Item.FindControl("btnGetPrice") as Button;
                    btnGetPrice.Visible = true;
                    _index++;
                }

            }
            if (e.Item.ItemType == ListItemType.Header)
            {
                Label lblHeaderInfo = e.Item.FindControl("lblHeaderInfo") as Label;
                if (lblHeaderInfo != null)
                {
                    _depHeading = "<div class='colum-one'><b>From : </b>" + objBRUC.FromDetail + " </div><div class='colum-one'><b>To : </b> " + objBRUC.ToDetail + " </div><div class='colum-one'><b>Date : </b>" + objBRUC.depdt.ToString("dd/MMM/yyyy") + " </div><div class='colum-one'><b>Passenger : </b>" + objBRUC.Adults.ToString() + " Adult  --  " + objBRUC.Boys.ToString() + " Child  --  " + objBRUC.Youths.ToString() + " Youth  --  " + objBRUC.Seniors.ToString() + " Senior</div> <div class='colum-one'><h2 style='font-size:20px!important;color:#FFF!important;'>Outbound</h2></div>";
                    TrInbound = "<div class='train-search-result-topblock'>" + _depHeading + "</div>";
                    lblHeaderInfo.Text = TrInbound;
                }
            }

        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void rptTcv_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null && _index == pInfoSolutionsResponse.TrainInformationList.Count())
                return;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblOutBound = e.Item.FindControl("lblOutBound") as Label;
                if (lblOutBound != null)
                {
                    BindHeading();
                    lblOutBound.Text = TrOutbound;
                }
                Literal ltrDepDate = e.Item.FindControl("ltrDepDate") as Literal;
                Literal ltrArvDate = e.Item.FindControl("ltrArrDate") as Literal;
                if (!_isOutbound && _isInbound)
                {
                    ltrDepDate.Text = ltrDepDate.Text == _FromDate ? string.Empty : ltrDepDate.Text;
                    ltrArvDate.Text = ltrArvDate.Text == _ToDate || ltrArvDate.Text == _FromDate ? string.Empty : ltrArvDate.Text;
                }
                else
                {
                    ltrDepDate.Text = ltrDepDate.Text == _FromDate || ltrDepDate.Text == _ToDate ? string.Empty : ltrDepDate.Text;
                    ltrArvDate.Text = ltrArvDate.Text == _ToDate ? string.Empty : ltrArvDate.Text;
                }
                if (pInfoSolutionsResponse != null)
                {
                    var listTrainInfo = pInfoSolutionsResponse.TrainInformationList[_index];
                    Repeater view = e.Item.FindControl("GrdRouteInfo") as Repeater;
                    if (view != null)
                    {
                        view.DataSource = listTrainInfo.TrainInfoSegment.ToList();
                        view.DataBind();
                    }
                    Label lblTrainChanges = e.Item.FindControl("lblTrainChanges") as Label;
                    if (listTrainInfo.TrainInfoSegment.ToList().Count() > 0)
                    {
                        lblTrainChanges.Text = listTrainInfo.TrainInfoSegment.ToList().Count() == 1 ? "Direct" : ((listTrainInfo.TrainInfoSegment.ToList().Count() - 1).ToString() + " Change");
                    }
                    string route = pInfoSolutionsResponse.TrainInformationList[_index].Route;
                    if (pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo != null)
                    {
                        List<TrainPrice> list = pInfoSolutionsResponse.TrainInformationList[_index].PriceInfo.SelectMany(x => x.TrainPriceList.Select(y => y)).ToList();
                        _tr = "";
                        string th = "<th style='width:30%;'></th>";

                        _tr = _tr + "<tr>";
                        foreach (var item in list)
                        {
                            th = th + "<th>" + item.ServiceName + "</th>";
                        }
                        _tr = _tr + th;
                        _tr = _tr + "</tr>";

                        string trnew = string.Empty;
                        var firstOrDefault = list.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            string cls = firstOrDefault.Class == "2" ? "2nd Class" : "1st Class";
                            trnew = trnew + "<tr>" + "<td style='width:28%;padding-left:2%'>" + cls + "</td>";
                        }
                        foreach (var itemp in list)
                        {
                            string td = string.Empty;
                            TrainPrice trainPrice = list.FirstOrDefault(x => x.ServiceName == itemp.ServiceName);

                            if (trainPrice != null)
                            {
                                var srcCurId = FrontEndManagePass.GetCurrencyID("EUT");

                                if (BusinessOneHub.IsNumeric(trainPrice.MinPrice))
                                {
                                    string price = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(trainPrice.MinPrice), siteId, srcCurId, currencyID).ToString("F");
                                    if (!_isOutbound && _isInbound)
                                        td = td + "<td class='tresult-parent'><a href='javascript:void(0)' class='tooltip'><input type='radio' class='priceB' name='rdoFromTiTcv' value='" + route + "," + itemp.Offer.OfferCode + "," + itemp.Offer.OfferSubgroupCode + "," + itemp.Offer.OfferTypeCode + "," + itemp.ServiceCode + "," + itemp.ServiceTypeCode + ",TCV," + itemp.Class + "><span  class='tresult'> <img class='callOut' src='' />" + trainPrice.FareDescription + "</span></a><span style=''>  " + currency + " " + price + "</span></td>";
                                    else
                                        td = td + "<td class='tresult-parent'><a href='javascript:void(0)' class='tooltip'><input type='radio' class='priceB' name='rdoToTiTcv' value='" + route + "," + itemp.Offer.OfferCode + "," + itemp.Offer.OfferSubgroupCode + "," + itemp.Offer.OfferTypeCode + "," + itemp.ServiceCode + "," + itemp.ServiceTypeCode + ",TCV," + itemp.Class + "><span  class='tresult'> <img class='callOut'  src=''/>" + trainPrice.FareDescription + "</span></a><span style=''>  <span style=''> " + currency + " " + price + "</span></td>";
                                }
                                else
                                {
                                    td = td + "<td style='height:27px; color:#9E0B0F;'>" + trainPrice.MinPrice + "</td>";
                                }
                            }
                            else
                                td = td + "<td style='height:27px;color:#ffffff'>. </td>";

                            trnew = trnew + td;
                        }

                        _tr = _tr + trnew + "</tr>";
                        HtmlGenericControl divTr = e.Item.FindControl("DivTr") as HtmlGenericControl;
                        divTr.InnerHtml = _tr;
                    }
                    else
                    {
                        string trnew = string.Empty;
                        trnew = trnew + "<tr>" + "<td style='width:100%;padding-left:2%'>Sold out</td></tr>";
                        HtmlGenericControl divTr = e.Item.FindControl("DivTr") as HtmlGenericControl;
                        divTr.InnerHtml = trnew;
                    }
                }

                _index++;
            }
            if (e.Item.ItemType == ListItemType.Header)
            {
                Label lblHeaderInfo = e.Item.FindControl("lblHeaderInfo") as Label;
                if (lblHeaderInfo != null)
                {
                    BindHeading();
                    lblHeaderInfo.Text = TrInbound;
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public string getNewStationsName(string prefixText)
    {

        GetNewStaionName_Result obj = new ManageBooking().GetNewStationNameFromOld(prefixText);
        if (obj != null)
            return obj.NewStationName;
        return null;
    }

    protected void btnFullFare_Click(object sender, EventArgs e)
    {
        BindResult();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void rptTrainResult_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "GetPrice")
            {
                HiddenField hdnInboundOrOutBound = e.Item.FindControl("hdnInboundOrOutBound") as HiddenField;
                HiddenField hdnJournyTypeReturn = e.Item.FindControl("hdnJournyTypeReturn") as HiddenField;
                if (Convert.ToBoolean(hdnJournyTypeReturn.Value.Trim()))
                {
                    Session["InbountOrOutBound"] = "Inbound";
                }
                else
                {
                    Session["InbountOrOutBound"] = "Outbound";
                }
                string[] str = e.CommandArgument.ToString().Split(',');
                string JsCode = str[0];
                string TripType = str[1];

                if (Session["TrainSearch"] == null) return;
                TrainInformationResponse response = Session["TrainSearch"] as TrainInformationResponse;
                RegionalTrainPrice RegionalTrainPrice = response.TrainInformationList.FirstOrDefault(x => x.JourneySolutionCode == JsCode).RegionalTrainPrice;

                Session["PriceInfoResponse"] = RegionalTrainPrice;
                Response.Redirect("TrainSegmentSearchResult.aspx");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    private FareRequest GetFareRequest(List<Taco> tacos)
    {
        var objrequest = new FareRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            taco = tacos.ToArray()
        };
        ApiLogin(objrequest);
        var client = new OneHubRailOneHubClient();

        return objrequest;
    }
    private void ApiLogin(FareRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
}


