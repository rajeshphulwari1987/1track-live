﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;

public partial class UserControls_passdetalis : System.Web.UI.UserControl
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    ManageTrainDetails _master = new ManageTrainDetails();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    ManageBooking _masterBooking = new ManageBooking();
    readonly Masters _masterPage = new Masters();
    public List<getRailPassData> list = new List<getRailPassData>();
    public string currency = "$";
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid siteId;
    Guid countryID;
    public string TkpAmount = "0";
    public string TicketProtection = "style='display:none;float:right;width:120px;margin-right: -27px; margin-top: -7px;  margin-bottom: -14px;line-height: 16px; '";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    public void PageconditionBind()
    {
        try
        {
            var data = _db.tblTicketProtections.FirstOrDefault(t => t.SiteID == siteId);
            if (data != null)
                TkpAmount = Convert.ToString(data.Amount);
            var istckProt = _masterPage.IsTicketProtection(siteId);
            if (istckProt != null && istckProt)
                TicketProtection = "style='display:block;float:right;width:120px;margin-right: -27px;margin-top: -7px;  margin-bottom: -14px;line-height: 16px;'";

            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
                divpopupdata.InnerHtml = list.Description;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PageconditionBind();
        long POrderID = Convert.ToInt64(Session["OrderID"]);
        GetCurrencyCode();
        if (!IsPostBack)
        {
            FetchRailPassData();
        }
    }

    public bool getvalidcountryid(Guid pid, Guid cid)
    {
        try
        {
            return _db.tblProductPermittedLookups.Any(ty => ty.ProductID == pid && ty.CountryID == cid);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ddlCountry_chkcounty(object sender, EventArgs e)
    {
        var ddlcntry = (DropDownList)sender;
        var lablemsg = ddlcntry.Parent.FindControl("errormsg") as Label;
        var hdnproductid = (HiddenField)ddlcntry.Parent.FindControl("hdnproductid");
        if (ddlcntry.SelectedIndex != 0)
        {
            var countryid = Guid.Parse(ddlcntry.SelectedValue);
            var productid = string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
            bool result = getvalidcountryid(productid, countryid);
            if (!result)
            {
                ddlcntry.SelectedIndex = 0;
                lablemsg.Text = "Country not valid for this pass.";
            }
            else
                lablemsg.Text = string.Empty;
        }
        ScriptManager.RegisterStartupScript(Page, GetType(), "temp", "showmessagecountry()", true);
        ScriptManager.RegisterStartupScript(Page, GetType(), "customselect", "customselect()", true);
        ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "Validationxx()", true);
    }

    public void FetchRailPassData()
    {
        try
        {
            if (Session["AgentUsername"] == null)
            {
                Session["USERUsername"] = "Guest";
                Session["USERUserID"] = Guid.NewGuid();
            }
            if (Session["RailPassData"] != null)
            {
                list = Session["RailPassData"] as List<getRailPassData>;
                rptPassDetail.DataSource = list;
                rptPassDetail.DataBind();
            }
            else
            {
                Response.Redirect("rail-passes");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void rptPassDetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var txtDob = e.Item.FindControl("txtDob") as TextBox;
            var txtStartDate = e.Item.FindControl("txtStartDate") as TextBox;
            var ddlTitle = e.Item.FindControl("ddlTitle") as DropDownList; ;
            var txtFirstName = e.Item.FindControl("txtFirstName") as TextBox;
            var txtLastName = e.Item.FindControl("txtLastName") as TextBox;
            var txtPassportNumber = e.Item.FindControl("txtPassportNumber") as TextBox;
            var hdnproductid = e.Item.FindControl("hdnproductid") as HiddenField;
            var ddlCountry = e.Item.FindControl("ddlCountry") as DropDownList;
            var ddlNationality = e.Item.FindControl("ddlNationality") as DropDownList;
            var lnkRemovePass = e.Item.FindControl("lnkRemovePass") as LinkButton;
            var hdnSaver = e.Item.FindControl("hdnSaver") as HiddenField;
            var hdnfromdate = e.Item.FindControl("hdnfromdate") as HiddenField;
            var hdntodate = e.Item.FindControl("hdntodate") as HiddenField;
            var hdnmsg = e.Item.FindControl("hdnmsg") as HiddenField;
            var lblLID = e.Item.FindControl("lblLID") as Label;
            var hdnCountryLevelIDs = e.Item.FindControl("hdnCountryLevelIDs") as HiddenField;
            var lblCountryName = e.Item.FindControl("lblCountryName") as Label;
            var TicketProtection = e.Item.FindControl("chkTicketProtection") as CheckBox;
           
            txtStartDate.Attributes.Add("readonly", "readonly");

            var hdnCategoryID = e.Item.FindControl("hdnCategoryID") as HiddenField;
            Guid CategoryId = Guid.Parse(hdnCategoryID.Value);
            var CatData = _db.tblCategories.FirstOrDefault(t => t.ID == CategoryId);
            bool Britrail = (CatData != null ? CatData.IsBritRailPass : false);
            if (hdnSaver.Value.Contains("Saver"))
            {
                if (Britrail)
                    lnkRemovePass.OnClientClick = "if(!saverhas('3')) return false;";
                else
                    lnkRemovePass.OnClientClick = "if(!saverhas('2')) return false;";
            }
            /*Daterange for startdate*/
            Guid product = Guid.Empty;
            if (!string.IsNullOrEmpty(hdnproductid.Value))
            {
                product = Guid.Parse(hdnproductid.Value);
                var data = _db.tblProducts.FirstOrDefault(x => x.ID == product);
                if (data != null)
                {
                    hdnfromdate.Value = data.ProductValidFromDate.ToString("MM/dd/yyyy");
                    hdntodate.Value = data.ProductValidToDate.ToString("MM/dd/yyyy");
                    hdnmsg.Value = "You can only purchase this pass for travel dates from " + data.ProductValidFromDate.ToString("dd/MMM/yyyy") + " to " + data.ProductValidToDate.ToString("dd/MMM/yyyy") + ".";
                }
            }
            /*end startdate*/
            ddlNationality.DataSource = _master.GetNationality();
            ddlNationality.DataValueField = "Adjective";
            ddlNationality.DataTextField = "Adjective";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "0"));
            ddlCountry.DataSource = _master.GetCountryDetail();
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            var productid = string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
            bool result = getvalidcountryid(productid, countryID);
            if (!String.IsNullOrEmpty(txtLastName.Text))
            {
                Guid IdSession = Guid.Parse(lblLID.Text);
                if (list != null && list.Count > 0)
                {
                    ddlCountry.SelectedValue = list.FirstOrDefault(t => t.Id == IdSession).Country.ToString();
                }
            }
            else if (result && String.IsNullOrEmpty(txtLastName.Text))
                ddlCountry.SelectedValue = countryID.ToString();
            else if (String.IsNullOrEmpty(txtLastName.Text))
                ddlCountry.SelectedIndex = 0;

            if (Session["DetailRailPass"] != null)
            {
                var list2 = Session["DetailRailPass"] as List<getpreviousShoppingData>;
                Guid ID = Guid.Parse(lnkRemovePass.CommandArgument);
                var data2 = list2==null?null:list2.FirstOrDefault(ty => ty.Id == ID);
                if (data2 != null)
                {
                    txtDob.Text = data2.DOB;
                    txtStartDate.Text = data2.Date;
                    ddlTitle.SelectedValue = data2.Title;
                    txtFirstName.Text = data2.FirstName;
                    txtLastName.Text = data2.LastName;
                    ddlCountry.SelectedValue = data2.Country;
                    txtPassportNumber.Text = data2.Passoprt;
                    ddlNationality.SelectedValue = data2.Nationality;
                    TicketProtection.Checked = data2.TicketProtection;
                }
            }
            string counrty = _master.GetEurailCountryNames(hdnCountryLevelIDs.Value);
            if (!string.IsNullOrEmpty(counrty))
                lblCountryName.Text = "(" + counrty + ")";
            var Div_Eurail = e.Item.FindControl("Div_IsEurailPass") as HtmlGenericControl;
            var rfDob = e.Item.FindControl("rfDob") as RequiredFieldValidator;
            bool DOBVisible = !_masterBooking.IsDOB(Guid.Parse(hdnproductid.Value));
            if (DOBVisible)
            {
                Div_Eurail.Attributes.Add("style", "display:none;");
                rfDob.Enabled = false;
            }
        }
    }

    protected void rptPassDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "RemovePass")
            {
                int no = 2;
                ManageBooking _masterBookingx = new ManageBooking();
                var list = new List<getRailPassData>();
                var detaillst = new List<getpreviousShoppingData>();
                Guid Id = Guid.Parse(e.CommandArgument.ToString());
                detaillst = HoldDataDetail();
                if (Session["RailPassData"] != null)
                    list = Session["RailPassData"] as List<getRailPassData>;
                var objRPD = list.FirstOrDefault(a => a.Id == Id);
                int count = 0;
                if (objRPD != null && objRPD.TravellerName.ToLower().Contains("saver"))
                {
                    Guid CategoryId = Guid.Parse(objRPD.CategoryID);
                    var CatData = _db.tblCategories.FirstOrDefault(t => t.ID == CategoryId);
                    bool Britrail = (CatData != null ? CatData.IsBritRailPass : false);
                    if (Britrail)
                        no = 3;
                    var newList = (from a in list
                                   where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName 
                                   && a.TravellerName.ToLower().Contains("saver") select a).ToList();
                    count = newList.Count();
                    if (count > no)
                    {
                        list.RemoveAll(ty => ty.Id == Id);
                        detaillst.RemoveAll(ty => ty.Id == Id);
                        _masterBookingx.DelpasssaleData(Id);
                    }
                    else
                        foreach (getRailPassData a in newList)
                        {
                            _masterBookingx.DelpasssaleData(a.Id);
                            list.RemoveAll(ty => ty.Id == a.Id);
                            detaillst.RemoveAll(ty => ty.Id == a.Id);
                        }
                }
                else
                {
                    list.RemoveAll(ty => ty.Id == Id);
                    detaillst.RemoveAll(ty => ty.Id == Id);
                    _masterBookingx.DelpasssaleData(Id);
                }
                if (list.Count == 0)
                    Response.Redirect("rail-passes");
                else
                {
                    Session.Add("RailPassData", list);
                    Session.Add("DetailRailPass", detaillst);
                    rptPassDetail.DataSource = list;
                    rptPassDetail.DataBind();
                    ScriptManager.RegisterStartupScript(Page, GetType(), "getdatacall", "getdata();pophide();", true);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public List<getpreviousShoppingData> HoldDataDetail()
    {
        var detaillst = new List<getpreviousShoppingData>();
        foreach (RepeaterItem item in rptPassDetail.Items)
        {
            var ddlTitle = item.FindControl("ddlTitle") as DropDownList;
            var ddlCountry = item.FindControl("ddlCountry") as DropDownList;
            var ddlNationality = item.FindControl("ddlNationality") as DropDownList;
            var txtFirstName = item.FindControl("txtFirstName") as TextBox;
            var txtLastName = item.FindControl("txtLastName") as TextBox;
            var txtPassportNumber = item.FindControl("txtPassportNumber") as TextBox;
            var txtDob = item.FindControl("txtDob") as TextBox;
            var txtStartDate = item.FindControl("txtStartDate") as TextBox;
            var ID = item.FindControl("lblLID") as Label;
            var TicketProtection = item.FindControl("chkTicketProtection") as CheckBox;

            Guid Ids = Guid.Parse(ID.Text);

            getpreviousShoppingData obj = new getpreviousShoppingData();
            obj.Id = Ids;
            obj.Title = ddlTitle.SelectedValue;
            obj.Country = ddlCountry.SelectedValue;
            obj.Nationality = ddlNationality.SelectedValue;
            obj.FirstName = txtFirstName.Text;
            obj.LastName = txtLastName.Text;
            obj.Passoprt = txtPassportNumber.Text;
            obj.DOB = txtDob.Text;
            obj.Date = txtStartDate.Text;
            obj.TicketProtection = TicketProtection.Checked;
            detaillst.Add(obj);
        }
        return detaillst;
    }

    public void GetCurrencyCode()
    {

        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            countryID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCountryID;
            currency = oManageClass.GetCurrency((Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID);

        }
    }

    public bool btnChkOut_Click()
    {
        bool returnresult = true;
        try
        {
            Guid SaverCountry = Guid.Empty;
            var textInfo = cultureInfo.TextInfo;
            bool flagInvalidDt = false;
            if (rptPassDetail.Items.Count > 0)
            {
                long POrderID = 0;
                string AffiliateCode = string.Empty;
                var AgentID = Guid.Empty;
                var UserID = Guid.Empty;
                if (Session["AffCode"] != null)
                    AffiliateCode = Session["AffCode"].ToString();
                if (AgentuserInfo.UserID != Guid.Empty && AgentuserInfo.IsAffliate)
                    AffiliateCode = _masterBooking.GetAffiliateCode(AgentuserInfo.UserID);
                else if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                    AgentID = AgentuserInfo.UserID;
                else if (USERuserInfo.ID != Guid.Empty)
                    UserID = USERuserInfo.ID;
                if (Session["OrderID"] == null)
                {
                    POrderID = _masterBooking.CreateOrder(AffiliateCode, AgentID, UserID, siteId, "OldUCPass");
                    Session["OrderID"] = POrderID;
                }
                else
                {
                    POrderID = Convert.ToInt64(Session["OrderID"]);
                    var result = _masterBooking.DeleteOldOrderPassSales(POrderID);
                    POrderID = _masterBooking.updateExistsOrder(AffiliateCode, AgentID, UserID, siteId, POrderID);
                }
                var lstRPData = new List<getRailPassData>();
                list = Session["RailPassData"] as List<getRailPassData>;
                #region foreach
                foreach (RepeaterItem it in rptPassDetail.Items)
                {
                    Guid productid = Guid.Empty;
                    decimal ticketprotectonFee = 0;
                    var IdInsert = Guid.NewGuid();
                    var HdnOriginalPrice = it.FindControl("HdnOriginalPrice") as HiddenField;
                    var hdnPrdtId = it.FindControl("hdnPrdtId") as HiddenField;
                    var hdnPassSale = it.FindControl("hdnPassSale") as HiddenField;
                    var hdnSalePrice = it.FindControl("hdnSalePrice") as HiddenField;
                    string[] str = hdnPassSale.Value.Split(',');
                    var LID = it.FindControl("lblLID") as Label;
                    var txtStartDate = it.FindControl("txtStartDate") as TextBox;
                    var chkTicketProtection = it.FindControl("chkTicketProtection") as CheckBox;
                    var txtDob = it.FindControl("txtDob") as TextBox;
                    var lblInvalidDate = it.FindControl("lblInvalidDate") as Label;
                    var ddlTitle = it.FindControl("ddlTitle") as DropDownList;
                    var txtFirstName = it.FindControl("txtFirstName") as TextBox;
                    var txtLastName = it.FindControl("txtLastName") as TextBox;
                    var ddlCountry = it.FindControl("ddlCountry") as DropDownList;
                    var ddlNationality = it.FindControl("ddlNationality") as DropDownList;
                    var txtPassportNumber = it.FindControl("txtPassportNumber") as TextBox;
                    if (chkTicketProtection.Checked)
                        ticketprotectonFee = Convert.ToDecimal(TkpAmount);
                    /*Validate Age*/
                    var hdnCategoryID = it.FindControl("hdnCategoryID") as HiddenField;
                    var hdnSaver = it.FindControl("hdnSaver") as HiddenField;
                    var travellerID = Guid.Parse(str[2]);
                    var validateAge = _db.tblTravelerMsts.FirstOrDefault(x => x.ID == travellerID && x.IsActive);
                    Guid CategoryId = Guid.Parse(hdnCategoryID.Value);
                    var CatData = _db.tblCategories.FirstOrDefault(t => t.ID == CategoryId);
                    bool Britrail = (CatData != null ? CatData.IsBritRailPass : false);
                    if (validateAge != null)
                    {
                        if (hdnSaver.Value.Contains("Saver")) //&& Britrail
                            validateAge.FromAge = 17;
                        if (!string.IsNullOrEmpty(txtDob.Text) && txtDob.Text != "DD/MMM/YYYY")
                        {
                            var dob = Convert.ToDateTime(txtDob.Text);
                            var tm = (DateTime.Now - dob);
                            var age = (tm.Days / 365);
                            if (age >= validateAge.FromAge && age <= validateAge.ToAge || validateAge.ToAge == 0)
                                lblInvalidDate.Style.Add("display", "none");
                            else
                            {
                                GetCurrencyCode();
                                flagInvalidDt = true;
                                lblInvalidDate.Style.Add("display", "block");
                                lblInvalidDate.Text = "Please make sure the passenger date of birth is correct for the pass you have selected.";
                                returnresult = false;
                                break;
                            }
                        }
                    }
                    /*end dob*/
                    #region update data details
                    var objTraveller = new tblOrderTraveller();
                    if (LID.Text.Trim() == string.Empty)
                        objTraveller.ID = IdInsert;
                    else
                        objTraveller.ID = Guid.Parse(LID.Text);
                    objTraveller.Title = ddlTitle.SelectedItem.Text;
                    objTraveller.LastName = textInfo.ToTitleCase(txtLastName.Text);
                    objTraveller.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                    objTraveller.Country = Guid.Parse(ddlCountry.SelectedValue);
                    objTraveller.PassportNo = txtPassportNumber.Text;
                    objTraveller.Nationality = ddlNationality.SelectedValue;
                    objTraveller.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                    bool DOBVisible = _masterBooking.IsDOB(Guid.Parse(hdnproductid.Value));
                    if (DOBVisible)
                        objTraveller.DOB = Convert.ToDateTime(txtDob.Text);
                    else
                        objTraveller.DOB = null;
                    var Gtps = new tblPassSale();
                    Gtps.ID = IdInsert;
                    Gtps.OrderID = POrderID;
                    Gtps.ProductID = Guid.Parse(str[0]);
                    Gtps.Price = Convert.ToDecimal(str[1]);
                    Gtps.TravellerID = Guid.Parse(str[2]);
                    Gtps.ClassID = Guid.Parse(str[3]);
                    Gtps.ValidityID = Guid.Parse(str[4]);
                    Gtps.CategoryID = Guid.Parse(str[5]);
                    Gtps.Commition = Convert.ToDecimal(str[6]);
                    Gtps.MarkUp = Convert.ToDecimal(str[7]);
                    Gtps.TicketProtection = ticketprotectonFee;
                    Gtps.CountryStartCode = Convert.ToInt32((!string.IsNullOrEmpty(str[8]) ? str[8] : null));
                    Gtps.CountryEndCode = Convert.ToInt32((!string.IsNullOrEmpty(str[9]) ? str[9] : null));
                    Gtps.Country1 = (!string.IsNullOrEmpty(str[10]) ? Guid.Parse(str[10]) : Guid.Empty);
                    Gtps.Country2 = (!string.IsNullOrEmpty(str[11]) ? Guid.Parse(str[11]) : Guid.Empty);
                    Gtps.Country3 = (!string.IsNullOrEmpty(str[12]) ? Guid.Parse(str[12]) : Guid.Empty);
                    Gtps.Country4 = (!string.IsNullOrEmpty(str[13]) ? Guid.Parse(str[13]) : Guid.Empty);
                    Gtps.Country5 = (!string.IsNullOrEmpty(str[14]) ? Guid.Parse(str[14]) : Guid.Empty);
                    //product currency//
                    Gtps.Site_MP = _masterBooking.GetCurrencyMultiplier("SITE", Guid.Parse(str[0]), POrderID);
                    Gtps.USD_MP = _masterBooking.GetCurrencyMultiplier("USD", Guid.Parse(str[0]), POrderID);
                    Gtps.SEU_MP = _masterBooking.GetCurrencyMultiplier("SEU", Guid.Parse(str[0]), POrderID);
                    Gtps.SBD_MP = _masterBooking.GetCurrencyMultiplier("SBD", Guid.Parse(str[0]), POrderID);
                    Gtps.GBP_MP = _masterBooking.GetCurrencyMultiplier("GBP", Guid.Parse(str[0]), POrderID);
                    Gtps.EUR_MP = _masterBooking.GetCurrencyMultiplier("EUR", Guid.Parse(str[0]), POrderID);
                    Gtps.INR_MP = _masterBooking.GetCurrencyMultiplier("INR", Guid.Parse(str[0]), POrderID);
                    Gtps.SEK_MP = _masterBooking.GetCurrencyMultiplier("SEK", Guid.Parse(str[0]), POrderID);
                    Gtps.NZD_MP = _masterBooking.GetCurrencyMultiplier("NZD", Guid.Parse(str[0]), POrderID);
                    Gtps.CAD_MP = _masterBooking.GetCurrencyMultiplier("CAD", Guid.Parse(str[0]), POrderID);
                    Gtps.JPY_MP = _masterBooking.GetCurrencyMultiplier("JPY", Guid.Parse(str[0]), POrderID);
                    Gtps.AUD_MP = _masterBooking.GetCurrencyMultiplier("AUD", Guid.Parse(str[0]), POrderID);
                    Gtps.CHF_MP = _masterBooking.GetCurrencyMultiplier("CHF", Guid.Parse(str[0]), POrderID);
                    Gtps.EUB_MP = _masterBooking.GetCurrencyMultiplier("EUB", Guid.Parse(str[0]), POrderID);
                    Gtps.EUT_MP = _masterBooking.GetCurrencyMultiplier("EUT", Guid.Parse(str[0]), POrderID);
                    Gtps.GBB_MP = _masterBooking.GetCurrencyMultiplier("GBB", Guid.Parse(str[0]), POrderID);
                    Gtps.THB_MP = _masterBooking.GetCurrencyMultiplier("THB", Guid.Parse(str[0]), POrderID);
                    Gtps.SGD_MP = _masterBooking.GetCurrencyMultiplier("SGD", Guid.Parse(str[0]), POrderID);
                    //site currency//
                    Gtps.Site_USD = _masterBooking.GetCurrencyMultiplier("SUSD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SEU = _masterBooking.GetCurrencyMultiplier("SSEU", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SBD = _masterBooking.GetCurrencyMultiplier("SSBD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_GBP = _masterBooking.GetCurrencyMultiplier("SGBP", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_EUR = _masterBooking.GetCurrencyMultiplier("SEUR", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_INR = _masterBooking.GetCurrencyMultiplier("SINR", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SEK = _masterBooking.GetCurrencyMultiplier("SSEK", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_NZD = _masterBooking.GetCurrencyMultiplier("SNZD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_CAD = _masterBooking.GetCurrencyMultiplier("SCAD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_JPY = _masterBooking.GetCurrencyMultiplier("SJPY", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_AUD = _masterBooking.GetCurrencyMultiplier("SAUD", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_CHF = _masterBooking.GetCurrencyMultiplier("SCHF", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_EUB = _masterBooking.GetCurrencyMultiplier("SEUB", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_EUT = _masterBooking.GetCurrencyMultiplier("SEUT", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_GBB = _masterBooking.GetCurrencyMultiplier("SGBB", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_THB = _masterBooking.GetCurrencyMultiplier("STHB", Guid.Parse(str[0]), POrderID);
                    Gtps.Site_SGD = _masterBooking.GetCurrencyMultiplier("SSGD", Guid.Parse(str[0]), POrderID);

                    _masterBooking.AddOrderTraveller(objTraveller, _masterBooking.AddPassSale(Gtps, IdInsert, hdnSalePrice.Value, HdnOriginalPrice.Value));

                    var objUpdates = new getRailPassData();
                    objUpdates = list != null ? list.Where(a => a.Id == objTraveller.ID).FirstOrDefault() : null;
                    if (objUpdates != null)
                    {
                        objUpdates.Title = ddlTitle.SelectedItem.Text;
                        objUpdates.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                        objUpdates.LastName = textInfo.ToTitleCase(txtLastName.Text);
                        objUpdates.Country = Guid.Parse(ddlCountry.SelectedValue);
                        objUpdates.PassportNo = txtPassportNumber.Text;
                        if (DOBVisible)
                            objUpdates.DOB = txtDob.Text;
                        else
                            objUpdates.DOB = null;
                        objUpdates.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                        lstRPData.Add(objUpdates);
                    }
                    #endregion
                }
                #endregion
                if (!flagInvalidDt)
                    Session["RailPassData"] = lstRPData;
            }
            return returnresult;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public class getpreviousShoppingData
    {
        public Guid Id { get; set; }
        public String DOB { get; set; }
        public String Date { get; set; }
        public String Title { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Country { get; set; }
        public String Passoprt { get; set; }
        public String Nationality { get; set; }
        public bool TicketProtection { get; set; }
    }
}