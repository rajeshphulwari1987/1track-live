﻿using System;
using Business;
using System.Linq;
using System.Web;
using System.Configuration;

public partial class UserControls_ucRightContent : System.Web.UI.UserControl
{
    readonly Masters _master = new Masters();
    private Guid _siteId;
    public string siteURL;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
            divJRF.Visible = _master.IsVisibleJRF(_siteId);
            trainresults.HRef = _db.tblSites.Any(x=>x.ID==_siteId && x.IsTrainTickets) ? siteURL + "traintickets" : siteURL + "trainresults";
            divGeneralInfo.Visible = _master.IsVisibleGeneralInfo(_siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx"));
            if (pageId != null)
                PageContent(pageId.ID, _siteId);
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);
                rtPannel.InnerHtml = oPage.RightPanel1.Replace("CMSImages", adminSiteUrl + "CMSImages");
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }
}