﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCountryPass.ascx.cs"
    Inherits="UserControls_ucCountryPass" %>
<style type="text/css">
    .btnadd
    {
        color: White;
        width: 137px;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    .leftDiv
    {
        padding-top: 6px;
        margin-bottom: 10px;
        color: #686868;
        background-color: #F0F0F0;
        float: left;
        border-radius: 5px;
        border: 1px solid #b3b3b3;
    }
    .floteleftdiv
    {
        font-size: 14px;
        float: left;
        width: 680px;
        line-height: 33px;
    }
    .countries
    {
        line-height: 18px;
    }
    .floteleftdiv select
    {
        height: 25px;
        width: 250px;
        margin: 5px 20px 5px 0;
    }
    .divleft
    {
        margin-left: 74px;
        float: left;
        width: 290px;
    }
    .divright
    {
        float: left;
        width: 300px;
    }
    .dvcalc
    {
        width: 288px;
        float: right;
        margin: -2px 3px 0 0;
        background-color: #ECECEC;
        padding: 10px 10px 10px 10px;
        -webkit-border-radius: 0 0 5px 5px !important;
    }
    .firstchild
    {
        padding-left: 5px;
    }
</style>
<asp:UpdatePanel ID="upCULVL" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="leftDiv">
            <div class="floteleftdiv" id="savermsg" style="display: none; color: Red; padding-left: 3px;
                border: 1px solid Red; margin-left: 5px; background: #FFD2D0;">
            </div>
            <div class="floteleftdiv" runat="server">
                <div class="divleft">
                    Choose class of travel:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged" />
                </div>
            </div>
            <div class="floteleftdiv" runat="server">
                <div class="divleft">
                    Choose your days of travel:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlValidityDays" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlValidityDays_SelectedIndexChanged" />
                </div>
            </div>
            <div class="floteleftdiv" runat="server">
                <div class="divleft">
                    Choose type of travellers:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlTraveller" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlTraveller_SelectedIndexChanged"/>
                </div>
            </div>
            <div class="floteleftdiv" runat="server">
                <div class="divleft">
                    Choose number of travellers:
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddlSelectno" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="selcunid1" runat="server" class="floteleftdiv">
                <div class="divleft">
                    Select your 1st country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt1_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid2" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select your 2nd country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt2_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid3" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select your 3rd country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt3_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid4" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select your 4th country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCunt4_SelectedIndexChanged" />
                </div>
            </div>
            <div id="selcunid5" class="floteleftdiv" runat="server">
                <div class="divleft">
                    Select your 5th country:</div>
                <div class="divright">
                    <asp:DropDownList ID="ddlCunt5" runat="server" />
                </div>
            </div>
            <div id="Div1" class="floteleftdiv" runat="server">
                <div class="divleft">
                    &nbsp;
                </div>
                <div class="divright">
                    <asp:Button ID="lnlAdd" Text="Show me prices " runat="server" OnClientClick="return SelectCal('in')"
                        OnClick="lnlAdd_Click" class="newbutton btnadd" />
                </div>
            </div>
        </div>
        <div id="divGrd" class="left-content country-block-outer" style="width: 675px; margin-bottom: 10px;"
            runat="server" visible="False">
            <div class="floteleftdiv" runat="server">
                <asp:GridView ID="grdTravellerinfo" runat="server" CellPadding="4" CssClass="grid-head2"
                    ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Width="671px"
                    OnRowCommand="grdTravellerinfo_RowCommand">
                    <AlternatingRowStyle BackColor="#FBDEE6" />
                    <HeaderStyle Font-Size="13px" ForeColor="White" HorizontalAlign="Left" BackColor="#4D4D4D" />
                    <RowStyle HorizontalAlign="Left" ForeColor="Black" BackColor="#ECECEC" BorderStyle="Solid"
                        BorderColor="#FFFFFF" BorderWidth="1px" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <Columns>
                        <asp:TemplateField HeaderText="Class" HeaderStyle-CssClass="firstchild" ItemStyle-CssClass="firstchild">
                            <ItemTemplate>
                                <%#Eval("ClassName")%>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Validity">
                            <ItemTemplate>
                                <%#Eval("ValidityName")%>
                            </ItemTemplate>
                            <ItemStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Countries">
                            <ItemTemplate>
                                <%#Eval("CountryName")%>
                            </ItemTemplate>
                            <ItemStyle Width="35%" CssClass="countries" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Traveller">
                            <ItemTemplate>
                                <%#Eval("TravellerName")%>
                            </ItemTemplate>
                            <ItemStyle Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Price">
                            <ItemTemplate>
                                <%=currency %><%#Eval("Price")%>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgDelete" AlternateText="Remove" ToolTip="Delete"
                                    CommandArgument='<%#Eval("ID")%>' CommandName="Remove" ImageUrl="~/images/DeleteRed.png"
                                    Style="margin-left: 5px;" OnClientClick="return confirm('Are you sure you want to delete this item?')" />
                                </div>
                            </ItemTemplate>
                            <ItemStyle Width="7%"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="dvcalc">
                <div style="float: left;">
                    Total
                </div>
                <div style="float: right">
                    <%=currency %>
                    <asp:Label ID="lblPrice" runat="server" Text="0.00" />
                </div>
            </div>
            <div>
                <div class="btnBookkNow pos-rel" style="clear: both">
                    <asp:Button ID="btnBookNow" runat="server" class="newbutton f-right w105 mar-t" Text="Book Now"
                        OnClick="btnBookNow_Click" />
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnBookNow" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upCULVL"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            Shovelling coal into the server...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    function SelectCal(e) {
        if (e == 'in') {
            var result = true;
            $('select').each(function () {
                var data = $(this).val();
                if (data == '0') {
                    if ($(this).attr('disabled') == undefined) {
                        $(this).css("border", "1px solid Red");
                    }
                    result = false;
                }
                else {
                    $(this).css("border", "1px solid #ccc");
                }
            });
            return result;
        }
        $(document).ready(function () {
            $("[id*=ddlTraveller],[id*=ddlSelectno]").change(function () {
                if (($("[id*=ddlTraveller]").find('option:selected').text()).match(/saver/i)) {
                    var noofsaver = parseInt($("[id*=ddlSelectno]").val());
                    if (noofsaver < 2 || noofsaver > 5) {
                        $("#savermsg").show();
                        $("#savermsg").html('You can select minimum 2 or maximum 5 number of people for ' + $("[id*=ddlTraveller]").find('option:selected').text() + ' ticket on same travelling class.');
                        $("[id*=ddlSelectno]").val('0');
                    }
                    else {
                        $("#savermsg").hide();
                    }
                }
            });
        });
    }
   
</script>
