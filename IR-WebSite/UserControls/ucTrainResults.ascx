﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTrainResults.ascx.cs"
    Inherits="P2P_UserControls_ucTrainResults" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .JournyMsg
    {
        font-size: 12px !important;
        background: #FAC08F;
        padding: 12px;
        border: 3px solid #FF8000;
        border-radius: 25px;
        color: #AC5D1D !important;
        margin-bottom: 7px;
    }
    .ipRed
    {
        color: #ededed;
    }
    .clsNews
    {
        width: 300px;
        font-size: 13px;
    }
    .clsfont
    {
        font-size: 11px;
    }
    .newsletter-outer .newsletter-inner input[type="text"]
    {
        height: 25px;
        line-height: 25px;
        width: 92%;
        margin-left: 6px;
    }
    .trNews table td img
    {
        display: none;
    }
    .clsError
    {
        color: #D8000C !important;
        border: 1px solid red;
        background-color: #FFBABA;
        -webkit-border-radius: 5px 5px 5px 5px;
        -ms-border-radius: 5px 5px 5px 5px;
    }
    .float-rt
    {
        float: right;
    }
    .clsHeadColor
    {
        color: #931b31;
        font-size: 15px !important;
    }
    .clsAbs
    {
        display: none;
    }
    .btnInactive
    {
        background: #f6f8f9 !important; /* Old browsers */
        background: -moz-linear-gradient(top,  #f6f8f9 0%, #f2f5f6 49%, #dde4e7 50%, #edeeee 100%) !important; /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f6f8f9), color-stop(49%,#f2f5f6), color-stop(50%,#dde4e7), color-stop(100%,#edeeee)) !important; /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* IE10+ */
        background: linear-gradient(to bottom,  #f6f8f9 0%,#f2f5f6 49%,#dde4e7 50%,#edeeee 100%) !important; /* W3C */
        -pie-background: linear-gradient(#f6f8f9, #edeeee) !important;
        font-size: 13px !important;
    }
    .btnactive
    {
        border-radius: 5px 5px 0 0 !important;
        -moz-border-radius: 5px 5px 0 0 !important;
        -webkit-border-radius: 5px 5px 0 0 !important;
        -ms-border-radius: 5px 5px 0 0;
        behavior: url(PIE.htc);
        position: relative;
        cursor: pointer;
        border: 1px solid #b3b3b3 !important;
        border-bottom: 0px;
        background: #b1085e !important; /* Old browsers */
        background: -moz-linear-gradient(top,  #b1085e 0%, #75043d 100%) !important; /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#b1085e), color-stop(100%,#75043d)) !important; /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  #b1085e 0%,#75043d 100%) !important; /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  #b1085e 0%,#75043d 100%) !important; /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  #b1085e 0%,#75043d 100%) !important; /* IE10+ */
        background: linear-gradient(to bottom,  #b1085e 0%,#75043d 100%) !important; /* W3C */
        -pie-background: linear-gradient(#b1085e, #75043d) !important;
        font-size: 13px !important;
    }
    .clsNews
    {
        width: 300px;
        font-size: 13px;
    }
    .clsfont
    {
        font-size: 11px;
    }
    .newsletter-outer .newsletter-inner input[type="text"]
    {
        height: 25px;
        line-height: 25px;
        width: 92%;
        margin-left: 6px;
    }
    .trNews table td img
    {
        display: none;
    }
    .banner
    {
        display: none;
    }
    .d-opt
    {
        opacity: 0.5;
        cursor: default;
    }
    .m-none
    {
        margin-left: 0 !important;
    }
    .input
    {
        margin-left: 0px !important;
    }
    
    
    .full-row
    {
        clear: both;
        width: 100%;
    }
    .colum03
    {
        color: #424242;
        float: left;
        font-size: 13px;
        height: 30px;
        line-height: 30px;
        padding: 5px 0;
        width: 45%;
    }
    .round-titles
    {
        width: 98%;
        float: left;
        height: 20px;
        line-height: 20px;
        border-radius: 5px 5px 0 0;
        -moz-border-radius: 5px 5px 0 0;
        -webkit-border-radius: 5px 5px 0 0;
        -ms-border-radius: 5px 5px 0 0;
        behavior: url(PIE.htc);
        background: #4a4a4a;
        padding: 1%;
        color: #FFFFFF;
        font-size: 14px;
        font-weight: bold;
    }
    
    .booking-detail-in
    {
        width: 98%;
        padding: 1%;
        border-radius: 0 0 5px 5px;
        -moz-border-radius: 0 0 5px 5px;
        -webkit-border-radius: 0 0 5px 5px;
        -ms-border-radius: 0 0 5px 5px;
        behavior: url(PIE.htc);
        margin-bottom: 20px;
        background: #ededed;
        float: left;
    }
    
    .booking-detail-in table.grid
    {
        width: 100%;
        border: 4px solid #ededed;
        border-collapse: collapse;
    }
    .booking-detail-in table.grid tr th
    {
        border-bottom: 2px solid #ededed;
        text-align: left;
        font-size: 13px;
        color: #666666;
        height: 25px;
        line-height: 25px;
        padding-left: 5px;
    }
    
    .booking-detail-in table.grid tr td
    {
        border-bottom: 2px solid #ededed;
        background: #FFF;
        font-size: 13px;
        color: #666666;
        height: 25px;
        line-height: 25px;
        padding-left: 5px;
    }
    
    .booking-detail-in table.grid tr td a
    {
        color: #951F35;
    }
    
    .booking-detail-in .total
    {
        border-top: 1px dashed #951F35;
        width: 27%;
        float: right;
        color: #4A4A4A;
        font-size: 14px;
        font-weight: bold;
        margin-top: 10px;
        padding-top: 10px;
    }
    
    .booking-detail-in .colum-label
    {
        float: left;
        width: 30%;
        float: left;
        color: #424242;
        font-size: 13px;
        line-height: 30px;
        height: 30px;
        padding: 5px 0;
    }
    .booking-detail-in .colum-input
    {
        float: left;
        width: 70%;
        float: left;
        line-height: 30px;
        height: 30px;
        padding: 5px 0;
        font-size: 13px;
        color: #424242;
        position: relative;
    }
    .booking-detail-in .colum-input .input
    {
        border: 1px solid #ADB9C2;
        border-radius: 5px 5px 5px 5px;
        color: #424242;
        font-size: 13px;
        height: 25px;
        line-height: 25px;
        width: 95%;
        padding: 0.5%;
    }
    .aspNetDisabled
    {
        border: 1px solid #ADB9C2;
        border-radius: 5px 5px 5px 5px;
        color: #424242;
        font-size: 13px;
        height: 24px;
        line-height: 25px;
        width: 95%;
        padding: 0.5%;
        margin-right: 5px;
    }
    select.aspNetDisabled
    {
        border: 1px solid #ADB9C2;
        border-radius: 5px 5px 5px 5px;
        color: #424242;
        font-size: 13px;
        height: 30px;
        line-height: 25px;
        width: 95%; /*padding: 0.5%;*/
        padding: 5px 3px !important;
    }
    .booking-detail-in .colum-input .inputsl
    {
        border: 1px solid #ADB9C2;
        border-radius: 5px 5px 5px 5px;
        color: #424242;
        font-size: 13px;
        height: 30px;
        line-height: 30px;
        width: 40%; /*padding: 0.5%;*/
        padding: 5px 3px !important;
        margin-left: 5px;
    }
    .booking-detail-in .full-row .colum03 .inputsl
    {
        border: 1px solid #ADB9C2;
        border-radius: 5px 5px 5px 5px;
        color: #424242;
        font-size: 13px;
        height: 30px;
        line-height: 30px;
        width: 50%; /*padding: 0.5%;*/
        padding: 5px 3px !important;
    }
    .lblock
    {
        background-color: #FFF;
        color: #424242;
        font-size: 13px;
        padding: 10px;
    }
    
    .divLoy
    {
        float: left;
        margin: 5px;
        width: 250px;
    }
    .divLoyleft
    {
        float: left;
        margin-bottom: 5px;
        width: 95px;
    }
    .divLoyright
    {
        float: left;
        margin-bottom: 5px;
        width: 150px;
    }
    .divLoyleft
    {
        float: left;
        margin-bottom: 5px;
        width: 95px;
    }
    .divLoyright
    {
        float: left;
        margin-bottom: 5px;
        width: 150px;
    }
    .divLoy input[type="text"]
    {
        background: none repeat scroll 0 0 #D5D4D4;
        border: 0 none;
        color: #333333;
        float: left;
        height: 25px !important;
        line-height: 25px !important;
        text-indent: 5px;
        width: 150px !important;
    }
    .txtLoyleft
    {
        background: none repeat scroll 0 0 #696969 !important;
        border: 0 none;
        color: #000000;
        float: left;
        height: 25px !important;
        line-height: 25px !important;
        text-indent: 5px;
        width: 90px !important;
    }
    .loading
    {
        background-image: url(images/loading3.gif);
        background-position: right;
        background-repeat: no-repeat;
    }
    .modalBackground
    {
        position: fixed;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        overflow: hidden;
        padding: 0;
        margin: 0;
        background-color: #000;
        filter: alpha(opacity=50);
        opacity: 0.5;
        text-align: center;
        float: left;
    }
    .progessposition
    {
        padding-top: 20%;
    }
    .clsFont
    {
        font-size: 13px;
        color: #4D4D4D !important;
    }
    .clsInput
    {
        border-radius: 5px 5px 5px 5px;
        line-height: 25px !important;
        border: 1px solid #ADB9C2;
    }
</style>
<%=script%>
<div class="banner">
    <div id="dvBanner" class="slider-wrapper theme-default">
        <div id="slider" class="nivoSlider">
            <asp:Repeater ID="rptBanner" runat="server">
                <ItemTemplate>
                    <img src='<%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<section class="content">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
        <Scripts>
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.core.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.datepicker.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.widget.js" />
        </Scripts>
    </asp:ToolkitScriptManager>
<asp:UpdatePanel ID="updLoyalty" runat="server">
                <ContentTemplate> 
<div class="left-content" id="DivLeftOne"  runat="server">
    <h1> <asp:Label ID="lblHeading" runat="server" Text="Train Search Result"/> </h1>
<p id="pCls" runat="server">
 <asp:Label ID="lblMessageEarlierTrain" runat="server" Text="" Visible="false"></asp:Label>
</p>

 <asp:Panel ID="pnlJourneyInfo" Visible="true" runat="server">
 
<p id="errorMsg" runat="server" class="JournyMsg" >
Your requested train cannot be booked online. It may require multiple changes or be outside of the railway booking date range. Please complete the form below and we will respond with times and costs within 24 business hours.
</p>
                    <div class="round-titles" style="height:40px;font-weight:normal">
                       <%--Send Journey Details--%>
                       If your booking is not available online please send us the details of your journey and we will come back to you with your options.
                    </div>
                    <div class="booking-detail-in">  
                     <div class="colum-label">
                        <asp:Label runat="server" ID="lblNm" Text="User Name"></asp:Label><span style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                        <div class="colum-input">
                            <asp:TextBox ID="txtName" runat="server" class="input ipRed" autocomplete="off"/>
                            <asp:RequiredFieldValidator ID="rfName" runat="server" Text="*" ErrorMessage="Please enter user name."
                        ControlToValidate="txtName" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic"/>                        
                        </div>
                      <div class="colum-label">
                            Email<span style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                        <div class="colum-input">
                            <asp:TextBox ID="txtEmailAddress"  runat="server" class="input ipRed"  autocomplete="off"  />
                              <asp:RequiredFieldValidator ID="rfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                        ControlToValidate="txtEmailAddress" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic"/>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                            ControlToValidate="txtEmailAddress" ForeColor="#ededed" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgsTR" />
                        </div>
                         <div class="colum-label">
                            Contact telephone number<span style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                         <div class="colum-input">
                             <asp:TextBox ID="txtPhone" runat="server" class="input ipRed" autocomplete="off" MaxLength="15" onkeypress="return isNumberKey(event)"/>
                             <asp:RequiredFieldValidator ID="rfPhone" runat="server" Text="*" ErrorMessage="Please enter contact phone."
                                ControlToValidate="txtPhone" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic"/>
                         </div>
                        <div class="colum-label">
                            From<span style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                        <div class="colum-input">
                            <asp:TextBox ID="txtFrom" onkeyup="selectpopup(this)" runat="server" class="input ipRed" autocomplete="off" ClientIDMode="Static"/>
                            <span id="spantxtFrom" style="display:none"></span>
                            <asp:HiddenField ID="hdnFrm" runat="server" />
                            <asp:RequiredFieldValidator ID="rfFrom" runat="server" ForeColor="#ededed" ValidationGroup="vgsTR"
                                Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                                Text="*"/>
                        </div>                         
                        <div class="colum-label">
                            To<span style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                        <div class="colum-input">
                             <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" class="input ipRed" autocomplete="off" ClientIDMode="Static"/>
                            <span id="spantxtTo" style="display:none"></span>
                            <asp:RequiredFieldValidator ID="rfTo" runat="server" ForeColor="#ededed"
                                ValidationGroup="vgsTR" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                                Text="*" CssClass="font14"/>
                        </div>
                        <div class="colum-label">
                            Journey
                        </div>
                        <div class="colum-input">
                            <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vg" RepeatDirection="Horizontal"
                                AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                                <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                                <asp:ListItem Value="1">Return</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="colum-label">
                            Depart<span style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                        <div class="colum-input col-calender">
                            <asp:TextBox ID="txtDepartureDate" runat="server" Text="DD/MM/YYYY" class="input ipRed" Style="width: 50%!important;
                                margin-right: 4px; float: left;" />
                            <span class="imgCalSendBox calIcon" style="float: left;margin-right: 5px;"
                                  title="Select DepartureDate."></span>
                            <asp:RequiredFieldValidator ID="rfDepartureDate" runat="server" ForeColor="#ededed" InitialValue="DD/MM/YYYY"
                                ValidationGroup="vgsTR" Display="Dynamic" ControlToValidate="txtDepartureDate"
                                ErrorMessage="Please enter departure date." Text="*" />
                            <asp:RegularExpressionValidator ID="regDepartureDate" runat="server" ControlToValidate="txtDepartureDate"
                                ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                Display="Dynamic" ForeColor="#ededed" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                                ValidationGroup="vgsTR">*</asp:RegularExpressionValidator>  
                                <asp:DropDownList ID="ddldepTime" runat="server" class="slbox01 clsInput" Style="width: 179px !important;
                                margin-left: 0px; float: left;">
                                <asp:ListItem>00:00</asp:ListItem>
                                <asp:ListItem>01:00</asp:ListItem>
                                <asp:ListItem>02:00</asp:ListItem>
                                <asp:ListItem>03:00</asp:ListItem>
                                <asp:ListItem>04:00</asp:ListItem>
                                <asp:ListItem>05:00</asp:ListItem>
                                <asp:ListItem>06:00</asp:ListItem>
                                <asp:ListItem>07:00</asp:ListItem>
                                <asp:ListItem>08:00</asp:ListItem>
                                <asp:ListItem Selected="True">09:00</asp:ListItem>
                                <asp:ListItem>10:00</asp:ListItem>
                                <asp:ListItem>11:00</asp:ListItem>
                                <asp:ListItem>12:00</asp:ListItem>
                                <asp:ListItem>13:00</asp:ListItem>
                                <asp:ListItem>14:00</asp:ListItem>
                                <asp:ListItem>15:00</asp:ListItem>
                                <asp:ListItem>16:00</asp:ListItem>
                                <asp:ListItem>17:00</asp:ListItem>
                                <asp:ListItem>18:00</asp:ListItem>
                                <asp:ListItem>19:00</asp:ListItem>
                                <asp:ListItem>20:00</asp:ListItem>
                                <asp:ListItem>21:00</asp:ListItem>
                                <asp:ListItem>22:00</asp:ListItem>
                                <asp:ListItem>23:00</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            Return<span id="returnspan" runat="server" visible="false" style="color:Red;padding-left: 5px;font-weight: bold;">*</span>
                        </div>
                        <div class="colum-input col-calender">
                            <asp:TextBox ID="txtTrainReturnDate" runat="server" Text="DD/MM/YYYY" class="input ipRed" Style="width: 50%!important;
                                margin-right: 4px; float: left;" Enabled="False" />
                            <span class="imgCalSendBox1 calIcon" style="float: left; margin-right: 5px;"
                                title="Select Return Date."></span>
                            <asp:RequiredFieldValidator ID="rfReturnDate" runat="server" ForeColor="#ededed" ValidationGroup="vgsTR" InitialValue="DD/MM/YYYY"
                                Enabled="false" Display="Dynamic" ControlToValidate="txtTrainReturnDate" ErrorMessage="Please enter return date."
                                Text="*" />
                            <asp:RegularExpressionValidator ID="regReturnDate" runat="server" ControlToValidate="txtTrainReturnDate"
                                ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                Display="Dynamic" ForeColor="#ededed" SetFocusOnError="true" ErrorMessage="Invalid return date"
                                ValidationGroup="vgsTR" Enabled="False">*</asp:RegularExpressionValidator>
                                  <asp:DropDownList ID="ddlReturnTime" runat="server" class="slbox01 clsInput" Enabled="false"
                                Style="width: 179px !important; margin-left: 0px; float: left;">
                                <asp:ListItem>00:00</asp:ListItem>
                                <asp:ListItem>01:00</asp:ListItem>
                                <asp:ListItem>02:00</asp:ListItem>
                                <asp:ListItem>03:00</asp:ListItem>
                                <asp:ListItem>04:00</asp:ListItem>
                                <asp:ListItem>05:00</asp:ListItem>
                                <asp:ListItem>06:00</asp:ListItem>
                                <asp:ListItem>07:00</asp:ListItem>
                                <asp:ListItem>08:00</asp:ListItem>
                                <asp:ListItem Selected="True">09:00</asp:ListItem>
                                <asp:ListItem>10:00</asp:ListItem>
                                <asp:ListItem>11:00</asp:ListItem>
                                <asp:ListItem>12:00</asp:ListItem>
                                <asp:ListItem>13:00</asp:ListItem>
                                <asp:ListItem>14:00</asp:ListItem>
                                <asp:ListItem>15:00</asp:ListItem>
                                <asp:ListItem>16:00</asp:ListItem>
                                <asp:ListItem>17:00</asp:ListItem>
                                <asp:ListItem>18:00</asp:ListItem>
                                <asp:ListItem>19:00</asp:ListItem>
                                <asp:ListItem>20:00</asp:ListItem>
                                <asp:ListItem>21:00</asp:ListItem>
                                <asp:ListItem>22:00</asp:ListItem>
                                <asp:ListItem>23:00</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            Adult&nbsp;<span style="font-size: 10px">(12+ yrs)</span>
                        </div>
                        <div class="colum-input adult">
                            <asp:DropDownList ID="ddlAdult" runat="server" class="slbox01 clsInput m-none" Style="width: 35%!important;">
                            </asp:DropDownList>
                          <span class="inline-txt" style="padding-left:10px;"> Children <a target=' _blank'  href="http://www.internationalrail.com.au/information/age-requirements-for-discount-fares.aspx" style=" color: #f68428;font-size: 11px; font-weight: normal;">(Child Ages?)</a> </span>
                            <asp:DropDownList ID="ddlChild" runat="server" class="slbox01 clsInput"
                                Style="float: right!important; margin-right: 18px;width: 35%!important;">
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            Youth&nbsp;<span style="font-size: 10px;">(12-25 yrs)</span>
                        </div>
                        <div class="colum-input adult">
                            <asp:DropDownList ID="ddlYouth" runat="server" class="slbox01 clsInput m-none" Style="width: 35%!important;">
                            </asp:DropDownList>
                           <span class="inline-txt" style="padding-left:10px"> Senior<span style="font-size: 10px;">(60+ yrs)</span> </span>
                            <asp:DropDownList ID="ddlSenior" runat="server" class="slbox01 clsInput"
                                Style="float: right!important; margin-right: 18px;width: 35%!important;">
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            Class Preference
                        </div>
                        <div class="colum-input adult" style="height: auto;">
                            <asp:DropDownList ID="ddlClass" runat="server" class="slbox01 clsInput m-none">
                                <asp:ListItem Value="0">All</asp:ListItem>
                                <asp:ListItem Value="1">1st</asp:ListItem>
                                <asp:ListItem Selected="True" Value="2">2nd</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label" style="display:none;">
                            Max Transfers
                        </div>
                        <div class="colum-input adult" style="display:none;">
                            <asp:DropDownList ID="ddlTransfer" runat="server" class="inputsl m-none">
                                <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                                <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                                <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                                <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label" style="display:none;">
                          &nbsp;
                        </div>
                        <div class="colum-input" style="display:none;">
                            <asp:CheckBox ID="chkLoyalty" runat="server"/>
                           <strong class="disable-label f-left">Loyalty cards</strong> 
                            <span id="divRailPass" runat="server" Visible="False">
                            <asp:CheckBox ID="chkIhaveRailPass" runat="server" AutoPostBack="true" OnCheckedChanged="chkIhaveRailPass_CheckedChanged" />
                           <strong class="disable-label f-left">  I have a rail pass </strong> 
                            </span>
                        </div>
                        <div style="margin-top: 10px; margin-right:18px;">
                            <div class="colum-label float-lt" style="width: 70%;color: red">*Please complete all mandatory fields.</div>
                            <div>
                            <asp:Button ID="btnSendInfo" runat="server" Text="Send Info" CssClass="btn-red-cart btn-sml f-right margr"
                                ValidationGroup="vgsTR" Style="width: 15%!important;" OnClick="btnSendInfo_Click"/>
                            </div>
                        </div>
                        <asp:Label runat="server" ID="lblmsg" />
                    </div>
            </asp:Panel>
<div class="clear"> &nbsp;</div>
<div class="country-block-outer" style="display:none!important;;">
    <div class="country-block-inner">
        <div id="footerBlock" runat="server"></div>
    </div>
</div>
</div>
<div class="left-content" id="DivLeftSecond" runat="server" style="display:none;">
<div class="success">
 <asp:Label runat="server" ID="succmessage"></asp:Label>
</div>
</div>  
</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updLoyalty"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
           Shovelling coal into the server...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
</section>
<asp:HiddenField runat="server" ID="hdnSiteid" ClientIDMode="Static" />
<asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
<script type="text/javascript"> 
     $(document).ready(function () { 
      $("#transrch_txtTrainReturnDate").val("DD/MM/YYYY");
       $(window).click(function (event) {
            $('#_bindDivData').remove();
        });
      if ($("#txtFrom").val() != '') {
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            LoadCal1();
        });
       function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        
         function selectpopup(e) {
         var count = e.keyCode;
            if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
            var StationType;
            if ($(e).attr('id') == "txtFrom")
                StationType = "from";
            else
                StationType = "to";
            var $this = $(e);
            var data = $this.val();

            var station = '';
            var hostName = window.location.host;
            var url = "http://" + hostName;

            if (window.location.toString().indexOf("https:") >= 0)
                url = "https://" + hostName;

            if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                $('#spantxtFrom').text('');
                $('#spantxtTo').text('');
            }

            var filter = $("#span" + $this.attr('id') + "").text();
            if (filter == "" && $this.val() != "")
                filter = $("#hdnFilter").val();
            $("#hdnFilter").val(filter);

            if ($this.attr('id') == 'txtTo') {
                station = $("#txtFrom").val();
            }
            else {
                station = $("#txtTo").val();
            }

            if (hostName == "localhost") {
                url = url + "/InterRail/P2P";
            }

            var hostUrl = url + "/StationList.asmx/getStationsXList";
            data = data.replace(/[']/g, "♥");
            var datalength=data;
            data = data + '*' + StationType+'*'+$("#hdnSiteid").val();
            var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
            $.ajax({
                type: "POST",
                url: hostUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                success: function (msg) {
                    $('#_bindDivData').remove();
                    var lentxt = datalength.length;
                    $.each(msg.d, function (key, value) {
                        var splitdata = value.split('ñ');
                        var lenfull = splitdata[0].length; ;
                        var txtupper = splitdata[0].substring(0, lentxt);
                        var txtlover = splitdata[0].substring(lentxt, lenfull);
                        $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                    });
                    $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                },
                error: function () {
                    //                    alert("Wait...");
                }
            });
        }
        }
           function _removehoverclass(e) {
        $(".popupselect").hover(function () {
            $(".popupselect").removeAttr('style');
            $(this).attr('style', 'background-color: #ccc !important');
            countKey = $(this).index();
        });
    }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
           if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
             $('#_bindDivData').remove();
        }
        function _hideThisDiv(e) {
            $('#_bindDivData').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }
        function CheckCardVal(sender, args) {
            if (document.getElementById('transrch_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
      
        function LoadCal1() {
            $("#transrch_txtDepartureDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+3m',
            });
            $("#transrch_txtTrainReturnDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+3m'
            });

            $(".imgCalSendBox").click(function () {
                $("#transrch_txtDepartureDate").datepicker('show');
            });

            $(".imgCalSendBox1").click(function () {
                if ($('#transrch_rdBookingType_1').is(':checked')) {
                    $("#transrch_txtTrainReturnDate").datepicker('show');
                }
            });

            if ($('#transrch_ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('enable');
            }
            else {
                $("#txtReturnDate").datepicker('disable');
            }
            $("#transrch_txtDepartureDate, #transrch_txtTrainReturnDate").keypress(function (event) { event.preventDefault(); });
        }

        function calenableT() {
            LoadCal1();
            $("#transrch_txtTrainReturnDate").datepicker('enable').val($("#transrch_txtDepartureDate").val());
        }
        function caldisableT() {
            LoadCal1();
            $("#transrch_txtTrainReturnDate").datepicker('disable').val("DD/MM/YYYY");
        }
</script>
