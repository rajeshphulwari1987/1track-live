﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class railpass_two : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    readonly private ManageProduct _master = new ManageProduct();
    public static bool ErrorIn;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public Guid siteId;
    public string script = "<script></script>";
    public string siteURL = string.Empty;
    readonly ManageAffiliateUser _AffManage = new ManageAffiliateUser();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["af"] != null)
            {
                bool Result = _AffManage.GetAffURLIsActiveByAffCode(Request.QueryString["af"], Request.Url.ToString());
                if (Result)
                    Session["AffCode"] = Request.QueryString["af"];
                else
                    Response.Redirect(siteURL + "home");
            }
            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["cid"] != null)
                {
                    Guid id = (Guid)Page.RouteData.Values["cid"];
                    ViewState["cid"] = id;
                }
                if (Request.QueryString["cid"] != null)
                {
                    ViewState["cid"] = Request.QueryString["cid"].ToString();
                    string RD_URl = PageUrls.GetRedirectURL(Convert.ToString(ViewState["cid"]));
                    Response.Redirect(RD_URl);
                }
                if (ViewState["cid"] != null)
                {
                    /*********if Product Foc-AD75**********/
                    var cid = Guid.Parse(ViewState["cid"].ToString());
                    var isCatFoc = _db.tblCategories.FirstOrDefault(x => x.ID == cid);
                    if (isCatFoc != null)
                        if ((bool)isCatFoc.IsFOCAD75)
                        {
                            var isAgentSite = false;
                            var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                            if (objsite != null)
                            {
                                if (objsite.IsAgent != null) isAgentSite = (bool)objsite.IsAgent;
                            }

                            if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                            {
                                var agentId = AgentuserInfo.UserID;
                                var isFoc = _oWebsitePage.IsFocAD75(agentId);
                                if (!isFoc || !isAgentSite)
                                    Response.Redirect("home");
                            }
                            else
                                Response.Redirect("home");
                        }
                    /*************************/

                    var catActive = _master.IsActiveCategory(Guid.Parse(ViewState["cid"].ToString()));
                    var siteCat = _master.siteHasCat(Guid.Parse(ViewState["cid"].ToString()), siteId);

                    if (!catActive || !siteCat)
                        Response.Redirect("home");
                    else
                        BindRailPasses(siteId, Guid.Parse(ViewState["cid"].ToString()));
                }

                QubitOperationLoad();
                GetRailPassData();
            }
        }
        catch
        {
            return;
        }
    }
    public void GetRailPassData()
    {
        var railpass1 = _db.tblRailPassSec1.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass1 != null)
        {
            imgRail1.Src = adminSiteUrl + railpass1.Imagepath;
            lblRailTitle1.Text = railpass1.Name;
            lblRailDesp1.Text = (railpass1.Description.Length > 50) ? (Server.HtmlDecode(railpass1.Description)).Substring(0, 50) + "..." : Server.HtmlDecode(railpass1.Description);
        }
        var railpass2 = _db.tblRailPassSec2.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass2 != null)
        {
            imgRail2.Src = adminSiteUrl + railpass2.Imagepath;
            lblRailTitle2.Text = railpass2.Name;
            lblRailDesp2.Text = (railpass2.Description.Length > 50) ? (Server.HtmlDecode(railpass2.Description)).Substring(0, 50) + "..." : Server.HtmlDecode(railpass2.Description);
        }
        var railpass3 = _db.tblRailPassSec3.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass3 != null)
        {
            imgRail3.Src = adminSiteUrl + railpass3.Imagepath;
            lblRailTitle3.Text = railpass3.Name;
            lblRailDesp3.Text = (railpass3.Description.Length > 50) ? (Server.HtmlDecode(railpass3.Description)).Substring(0, 50) + "..." : Server.HtmlDecode(railpass3.Description);
        }
    }

    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    public void BindRailPasses(Guid siteId, Guid catId)
    {
        ErrorIn = false;
        var result = _master.GetProductListByCatID(siteId, catId);
        rptPasses.DataSource = result;
        rptPasses.DataBind();

        ScriptManager.RegisterStartupScript(Page, GetType(), "ie", "loadIE()", true);
    }
    protected void rptPasses_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (ErrorIn == false)
        {
            ErrorIn = true;
            if (e.Item.DataItem == null)
            {
                var lblerrmsg = e.Item.FindControl("lblerrmsg") as Label;
                var lnkGotoback = e.Item.FindControl("lnkGotoback") as LinkButton;
                lblerrmsg.Visible = true;
                lnkGotoback.Visible = true;
            }
        }
    }
    protected void rptPasses_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Redirect")
            Response.Redirect("rail-passes");
    }
    void ShowP2PWidget(Guid siteID)
    {
        var enableP2P = _oWebsitePage.EnableP2P(siteID);
        if (!enableP2P)
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);
    }

}