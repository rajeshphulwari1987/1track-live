﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="Feedback.aspx.cs"
    Inherits="Feedback" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
                return true;
            return false;
        }
    </script>
    <style type="text/css">
        .in-box
        {
            width: 60% !important;
        }
        .clsFb
        {
            clear: both;
        }
        .clsVal
        {
            padding: 20px 0 0 10px;
            float: left;
        }
    </style>
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Feedback Form</div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Feedback Form</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>

<h1>Feedback Form</h1>

<div class="formblock" style="width:65%;">
    <p>
    Thank you for visiting our site.
    While we strive to bring you the best service and rail offering in the industry, we recognise there is always room for improvement.
    <br/>Sent us your feedback, you can help us improve your experience.
    <br/><br/>
</p>
    <div class="clsFb">
      <asp:TextBox ID="txtName" class="in-box" runat="server"/>
      <asp:TextBoxWatermarkExtender runat="server" ID="txtWFN" WatermarkText="Your Name"
        TargetControlID="txtName" />
        <div class="clsVal">
        <asp:RequiredFieldValidator ID="rfNm" Display="Dynamic" runat="server" ErrorMessage="Enter your name" ForeColor="Red" Font-Size="14px" ControlToValidate="txtName" ValidationGroup="vs"/>
        </div>
    </div>
    <div class="clsFb">
      <asp:TextBox ID="txtPhn" class="in-box" runat="server" MaxLength="15"/>
      <asp:TextBoxWatermarkExtender runat="server" ID="txtWFP" WatermarkText="Phone No." TargetControlID="txtPhn" />
       <div class="clsVal">
        <asp:RequiredFieldValidator ID="rfPhone" Display="Dynamic" runat="server" ErrorMessage="Enter Phone no." ForeColor="Red" Font-Size="14px" ControlToValidate="txtPhn" ValidationGroup="vs"/>
        </div>
    </div>
    <div class="clsFb">
        <asp:DropDownList ID="ddlTopic" runat="server" CssClass="in-box" style="width:61%!important" />
         <div class="clsVal">
        <asp:RequiredFieldValidator ID="rfTopic" Display="Dynamic" runat="server" ErrorMessage="Select Topic" ForeColor="Red" Font-Size="14px" ControlToValidate="ddlTopic"
         ValidationGroup="vs" InitialValue="-1"/></div>
    </div>
    <div class="clsFb">
      <asp:TextBox ID="txtEmail" class="in-box" runat="server"/>
      <asp:TextBoxWatermarkExtender runat="server" ID="txtWFE" WatermarkText="Email Address" TargetControlID="txtEmail" />
      <div class="clsVal">
      <asp:RequiredFieldValidator ID="rfEmail" runat="server" Display="Dynamic" ErrorMessage="Enter Email" ForeColor="Red" Font-Size="14px" ControlToValidate="txtEmail" ValidationGroup="vs"/>
     <asp:RegularExpressionValidator ID="reEmail" Display="Dynamic" runat="server" ControlToValidate="txtEmail" ForeColor="Red" Font-Size="14px"
        ErrorMessage="Invalid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vs"/>
      </div>
    </div>
    <div class="clsFb">
        <asp:TextBox ID="txtDesp" class="txtarea-box" runat="server" TextMode="MultiLine" Width="500px" Rows="10" />
    </div>
    <div class="clear"></div>
    <div class="clsFb" >
    <asp:Button id="btnSubmit" class="w184" runat="server" Text="Send Feedback" ValidationGroup="vs" OnClick="btnSubmit_Click" />
     </div>
</div>

<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
