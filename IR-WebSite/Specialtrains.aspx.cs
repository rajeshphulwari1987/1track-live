﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;

public partial class Specialtrains : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly Masters _master = new Masters();
    private Guid _siteId;
    public string script = "<script></script>";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.RouteData.Values["sid"] != null)
            {
                Guid id = (Guid)Page.RouteData.Values["sid"];
                ViewState["id"] = id;
            }
            if (Request.QueryString["id"] != null)
            {
                ViewState["id"] = Request.QueryString["id"].ToString();
                string RD_URl = PageUrls.GetRedirectURL(Convert.ToString(ViewState["id"]));
                Response.Redirect(RD_URl);
            }
            BindContinent(_siteId);
            if (ViewState["id"] != null)
            {
                try
                {
                    var trainId = Guid.Parse(ViewState["id"].ToString());
                    BindSpecialTrainById(trainId);
                    BindTrainDtlImage(trainId);
                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
            }
            else
                divTrain.Visible = false;
            QubitOperationLoad();
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindContinent(Guid siteId)
    {
        //var res = _db.tblContinents.OrderBy(x => x.Name).ToList().Take(4);
        var res = from cnt in _db.tblContinents
                  join sp in _db.tblSpecialTrains on cnt.ID equals sp.ContinentID
                  join spLook in _db.tblSpecialTrainSiteLookUps on sp.ID equals spLook.SpecialTrainID
                  where spLook.SiteID == siteId && sp.IsActive == true
                  select new { ID = cnt.ID, Name = cnt.Name };
        rptContinent.DataSource = res.Distinct().ToList().Take(4);
        rptContinent.DataBind();
    }

    public void BindSpecialTrainById(Guid trainId)
    {
        dvMain.Visible = false;
        var result = from sp in _db.tblSpecialTrains
                     where sp.ID == trainId
                     select sp;
        rptSpecTrains.DataSource = result;
        rptSpecTrains.DataBind();
    }

    public void BindTrainDtlImage(Guid trainId)
    {
        var train = _db.tblSpecialTrains.FirstOrDefault(x => x.ID == trainId);
        if (train != null)
        {
            var ctryId = train.CountryID;
            tptTrainList.DataSource = _cMaster.GetTrainDtlByCId(ctryId, _siteId);
            tptTrainList.DataBind();
        }
    }

    public void BindSpecialTrainByCountry(string country)
    {
        dvMain.Visible = false;
        var countryId = from cm in _db.tblCountriesMsts
                        where cm.CountryName == country
                        select cm;

        var tblCountriesMst = countryId.FirstOrDefault();
        if (tblCountriesMst != null)
        {
            var cid = Guid.Parse(tblCountriesMst.CountryID.ToString());
            var result = _db.tblSpecialTrains.Where(sp => sp.CountryID == cid);
            rptSpecTrains.DataSource = result;
        }
        rptSpecTrains.DataBind();
    }

    protected void rptContinent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var rptTrain = (Repeater)e.Item.FindControl("rptTrain");
        var hdnConId = (HiddenField)e.Item.FindControl("hdnConId");
        if (hdnConId != null)
        {
            var conId = Guid.Parse(hdnConId.Value);
            var result = (from sp in _db.tblSpecialTrains
                          join slook in _db.tblSpecialTrainSiteLookUps
                              on sp.ID equals slook.SpecialTrainID
                          where sp.ContinentID == conId
                                && sp.IsActive == true
                                && slook.SiteID == _siteId
                          orderby sp.Name
                          select new
                              {
                                  Name = sp.Name,
                                  Url = sp.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "").ToLower()
                              } ).ToList();
            rptTrain.DataSource = result;
            rptTrain.DataBind();
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}