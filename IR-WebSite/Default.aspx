﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" ValidateRequest="false" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .slider-wrapper, .nivoSlider img
        {
            height: auto;
        }
        .clsHt
        {
            max-height: 70px;
        }
        #siteFooter
        {
            font-style: normal !important;
        }
        .right-content
        {
            font-style: normal !important;
        }
        strong, b #siteFooter
        {
            font-weight: normal !important;
        }
        .clsRpDesp strong, b
        {
            font-weight: bold !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () 
         
            $("#MainContent_TabTopJourney").click(function () {
                $("#MainContent_divTopJourney").show();
                $("#MainContent_divTopRail").hide();
                $("#MainContent_TabTopJourney").addClass('btnactive');
                $("#MainContent_TabTopJourney").removeClass('btnInactive');
                $("#MainContent_TabTopRail").addClass('btnInactive');
                $("#MainContent_TabTopRail").removeClass('btnactive');
            });
            $("#MainContent_TabTopRail").click(function () {
                $("#MainContent_divTopRail").show();
                $("#MainContent_divTopJourney").hide();
                $("#MainContent_TabTopRail").addClass('btnactive');
                $("#MainContent_TabTopRail").removeClass('btnInactive');
                $("#MainContent_TabTopJourney").addClass('btnInactive');
                $("#MainContent_TabTopJourney").removeClass('btnactive');
            });
        });
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
        <Scripts>
            <asp:ScriptReference Path="~/Scripts/DatePicker/jquery.ui.core.js" />
            <asp:ScriptReference Path="~/Scripts/DatePicker/jquery.ui.datepicker.js" />
            <asp:ScriptReference Path="~/Scripts/DatePicker/jquery.ui.widget.js" />
        </Scripts>
    </asp:ToolkitScriptManager>
    <section class="content">
<div class="left-content">
    <%--Banner section--%>
    <div class="banner" style="margin-bottom: 10px;">
        <div id="dvBanner" class="slider-wrapper theme-default" runat="server">
            <div id="slider" class="nivoSlider">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%=SiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>

    <div class="cms">
        <div id="ContentHead" runat="server"></div>
    </div>
    <div class="cms">
        <div id="ContentText" runat="server"></div>
    </div>
    <div class="clear"> &nbsp;</div>
    <div class="rail-detail-block" style="display:none"> 
    <div class="list-tab"> 
    <ul> 
        <li>
            <input value="Top Journeys" BorderStyle="None" ID="TabTopJourney" type="button"
                 style="width: 150px;height: 32px" runat="server"/> 
        </li>
        <li>
          <input value="Top Rail Passes" BorderStyle="None" ID="TabTopRail" type="button"
                style="width: 150px;height: 32px" runat="server"/>
        </li>
    </ul>
</div>

 
<div class="tab-detail-in" id="divTopJourney" runat="server">
    <asp:Repeater ID="rptJourney" runat="server" OnItemCommand="rptJourney_ItemCommand">
   <ItemTemplate>
     <div class="booking-block brbotm">
          <div class="booking-detail">
            <div class="colum01 colum011">
            <div style=" height:26px">
             <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("FromFlag")%>' alt="" border="0" height="26px" /> 
             </div>
             <h3><%#(Eval("From").ToString().Length > 15) ? (Eval("From").ToString().Substring(0, 15) + "...") : (Eval("From").ToString())%> 
              &nbsp;> <br />
             <span> Duration  </span> </h3>
             <p> 
                <%#(Eval("Description").ToString().Length > 20) ? (Eval("Description").ToString().Substring(0, 20) + "...") : (Eval("Description").ToString())%>
             </p>
            </div>
            <div class="colum01" style="float:right;width:110px">
            <%--<div class="price"><span>from</span> <br /> <%=currency%> <%#Eval("Price")%></div>--%>
            <div style="height:26px">
             <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("ToFlag")%>' alt="" border="0" height="26px"/> 
             </div>
             <h3><%#(Eval("To").ToString().Length > 15) ? (Eval("To").ToString().Substring(0, 15) + "...") : (Eval("To").ToString())%>   <br />
             <span> <%#Eval("DurationHr")%>hr <%#Eval("DurationMin")%> min</span> </h3>
            </div>            
          </div>
          <%--<div class="imgblock">
             <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImg")=="" ? "images/NoproductImage.png":Eval("BannerImg") %>' alt="" border="0" width="106px" class="clsHt"/>
             <div class="clear"></div>
          </div>--%>
          <div class="clear"></div>
          <div style="width:85px; margin:auto;">   
            <asp:Button ID="btnBook" Text="Book Now" runat="server" CommandName="Book" CommandArgument='<%#Eval("ID") %>' OnClientClick="showBookNow()"/>
          </div>
     </div>
   </ItemTemplate>
</asp:Repeater>
</div>
 
<div class="tab-detail-in" id="divTopRail" runat="server">
    <div class="left-content ">
        <h1>
            European &amp; international rail passes &amp; train tickets
        </h1>
        <p>
            To book or enquire further about any train journey or rail pass please <a href="contact-us" class="txt-red"><u>contact us </u></a>
        </p>
        <br>
        <ul class="countrylist countrylist02">
            <asp:Repeater ID="rptPasses" runat="server">
                <ItemTemplate>
                    <li><a href="<%#Eval("Url") %>">
                        <%#Eval("Name")%>
                        <strong class="txt-red"> <%#Eval("CountryName")%> »</strong> </a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <div class="clear">
            &nbsp;</div>
    </div>
</div>
 
</div>
    <div class="clear"> &nbsp;</div>
    <div class="country-block-outer bg-white">
        <h1 id="FooterHeader" runat="server">Recommended Favourites</h1>
    <div class="country-block-inner">
        <div id="footerBlock" class="row" runat="server"></div>
    </div>
</div>
<div class="clear"> &nbsp;</div>
<%--Homepage footer section--%>
    <asp:Repeater ID="rptEurailPromotion" runat="server">
        <ItemTemplate>
        <div class="addbanner-box row" style="height:auto">
            <div class="imgblock">
                <img src='<%=ConfigurationSettings.AppSettings["HttpAdminHost"]%><%#Eval("ImageUrl")=="" ? "images/NoproductImage.png":Eval("ImageUrl") %>' alt="" border="0" width="168"   />
            </div>
            <div class="discription-block">
                <h3><%#Eval("Title")%></h3>
                <p><%# (Server.HtmlDecode(Eval("Description").ToString()).Length > 450) ? Server.HtmlDecode(Eval("Description").ToString()).Substring(0, 300) + "..." : Server.HtmlDecode(Eval("Description").ToString())%></p>
            </div>
             <div style="float:right;">  <a class="read" href='<%# (Eval("NavUrl") != "" && Eval("NavUrl") != null) ? Eval("NavUrl") : frontSiteUrl + "PromotionDetails.aspx?id=" + Eval("ID") %>'> Read more </a> </div>
        </div>
        
        </ItemTemplate>
    </asp:Repeater>
</div>

<div class="right-content">
        <div class="ticketbooking" style="padding-top:0px">
            <div class="list-tab divEnableP2P">
                <ul>
                    <li><a href="#" class="active">Rail Tickets </a><li>
                    <li><a href="rail-passes">Rail Passes </a></li>
                </ul>
            </div>
            <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
            <img id="imgShadow" runat="server" src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
        </div>
<uc:Newsletter ID="Newsletter1" runat="server" />
<%--Right image section--%>
  <div class="right">
        <div id="dvRight" class="slider-wrapper theme-default" style="height:auto!important;" runat="server">
            <a id="aRightPanelUrl" runat="server">
                <div id="sliderRt">
                <asp:Repeater ID="rptRtImg" runat="server">
                    <ItemTemplate>
                        <img src='<%=SiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div></a>
        </div>
    </div>
<%--Right image section end--%>
</div>
</section>
    <div id="showBookNow" style="display: none;">
        <progresstemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2"> 
               Shovelling coal into the server...
            </div>
        </progresstemplate>
    </div>
    <script>
        function showBookNow() {
            $("#showBookNow").css("display", "block");
        }
        
    </script>
</asp:Content>
