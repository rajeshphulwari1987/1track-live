﻿using System;
using Business;
using System.Web.UI;
using System.Linq;
using System.Configuration;

public partial class ConditionsofUse : Page
{
    readonly ManageBooking _master = new ManageBooking();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private Guid _siteId;
    public string siteURL;
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Page.RouteData.Values["PageId"] != null)
                BindConditions(_siteId);
        }
    }

    public void BindConditions(Guid siteId)
    {
        var result = _master.GetConditionsBySiteid(siteId).Where(x => x.IsActive == true);
        rptConditions.DataSource = result;
        rptConditions.DataBind();
    }
}