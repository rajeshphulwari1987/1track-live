﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class P2PBookingCart : Page
{
    #region Global Variables
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private double _total = 0;
    readonly ManageUser _ManageUser = new ManageUser();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    ManageTrainDetails _master = new ManageTrainDetails();
    ManageBooking _masterBooking = new ManageBooking();
    public static string currency = "$";
    public static Guid currencyID = new Guid();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid pageID, siteId;
    public string script = "<script></script>";
    public long P2POrderId = 0;
    #endregion

    public string siteURL;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["P2POrderID"] != null)
            P2POrderId = Convert.ToInt64(Session["P2POrderID"]);

        if (Request.QueryString["req"] != null)
        {
            if (Request.QueryString["req"].Trim() == "IT")
            {
                Session["TrainType"] = "IT";
                pnlShipping.Visible = false;
                ucTicketDelivery.Visible = false;
            }
            else
                Session["TrainType"] = "BE";
        }
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "restirctCopy", "restrictCopy();", true);
        txtZip.Attributes.Add("onkeypress", "return keycheck()");
        ShowMessage(0, string.Empty);
        if (!IsPostBack)
        {
            BindPageMandatoryFields();
            GetCurrencyCode();
            GetTheme(siteId);

            if (Session["AgentUserID"] != null)
            {
                Guid IDuser = Guid.Parse(Session["AgentUserID"].ToString());
                var Agentlist = _ManageUser.AgentDetailsById(IDuser);
                var AgentNameAndEmail = _ManageUser.AgentNameEmailById(IDuser);
                if (Agentlist != null)
                {
                    ddlMr.SelectedValue = Convert.ToInt32(AgentNameAndEmail.Salutation).ToString();
                    txtFirst.Text = AgentNameAndEmail.Forename;
                    txtLast.Text = AgentNameAndEmail.Surname;
                    txtEmail.Text = AgentNameAndEmail.EmailAddress;
                    txtConfirmEmail.Text = AgentNameAndEmail.EmailAddress;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;
                    txtBillPhone.Text = Agentlist.Telephone;

                    txtShpPhone.Text = Agentlist.Telephone;
                    txtshpfname.Text = Agentlist.FirstName;
                    txtshpLast.Text = Agentlist.LastName;
                    txtshpEmail.Text = Agentlist.Email;
                    txtshpConfirmEmail.Text = Agentlist.Email;
                    ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;
                }
            }
            else if (Session["USERUserID"] != null)
            {
                var IDuser = Guid.Parse(Session["USERUserID"].ToString());
                var userlist = _ManageUser.GetUserbyID(IDuser);
                if (userlist != null)
                {
                    txtFirst.Text = userlist.FirstName;
                    txtLast.Text = userlist.LastName;
                    txtEmail.Text = userlist.Email;
                    txtConfirmEmail.Text = userlist.Email;
                    ddlCountry.SelectedValue = Convert.ToString(userlist.Country);
                    txtCity.Text = userlist.City;
                    txtAdd.Text = userlist.Address;
                    txtZip.Text = userlist.PostCode;

                    txtshpfname.Text = userlist.FirstName;
                    txtshpLast.Text = userlist.LastName;
                    txtshpEmail.Text = userlist.Email;
                    txtshpConfirmEmail.Text = userlist.Email;
                    ddlshpCountry.SelectedValue = Convert.ToString(userlist.Country);
                    txtshpCity.Text = userlist.City;
                    txtshpAdd.Text = userlist.Address;
                    txtshpZip.Text = userlist.PostCode;
                }
            }
            Session["BOOKING-REPLY"] = null;
            PageLoadEvent();
            QubitOperationLoad();
        }
    }

    public void BindPageMandatoryFields()
    {
        try
        {
            var pid = "AFC79F98-9D7B-4FC2-AA1C-2BAFD83A43A7";
            pageID = (Guid.Parse(pid));
            var list = _masterPage.GetMandatoryVal(siteId, pageID);

            foreach (var item in list)
            {
                if (item.ControlField.Trim() == "rfFirst")
                    BookingpassrfFirst.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfLast")
                    BookingpassrfLast.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfEmail")
                    BookingpassrfEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfvEmail2")
                    BookingpassrfvEmail2.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfPhone")
                    BookingpassrfPhone.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfAdd")
                    BookingpassrfAdd.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfZip")
                    BookingpassrfZip.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfCountry")
                    BookingpassrfCountry.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfDateOfDepature")
                    BookingpassrfDateOfDepature.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipFirstName")
                    BookingpassrfshipFirstName.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipLastName")
                    BookingpassrfshipLastName.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshipEmail")
                    BookingpassrfshipEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfconfirmShipEmail")
                    BookingpassrfconfirmShipEmail.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpPhone")
                    BookingpassrfshpPhone.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpAdd")
                    BookingpassrfshpAdd.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpZip")
                    BookingpassrfshpZip.ValidationGroup = item.ValGrp.Trim();

                if (item.ControlField.Trim() == "rfshpCountry")
                    BookingpassrfshpCountry.ValidationGroup = item.ValGrp.Trim();
            }

        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetTheme(Guid SiteId)
    {
        var blueThemeID = Guid.Parse("4FC1F398-5901-439B-A1E0-27B1CBE2BBEB");
        var theme = _db.tblSiteThemes.FirstOrDefault(x => x.SiteID == SiteId);
        if (theme != null)
            if (lnkBookStyle != null)
            {
                if (theme.ThemeID == blueThemeID)
                {
                    string css = "Styles/" + theme.CssFolderName + "/BookingCart.css";
                    lnkBookStyle.Attributes.Add("href", css);
                }
            }
    }

    public void QubitOperationLoad()
    {
        if (Session["siteId"] != null)
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));

            if (lstQbit != null && lstQbit.Count() > 0)
            {
                var res = lstQbit.FirstOrDefault();
                if (res != null)
                    script = res.Script;
            }
        }
    }

    void PageLoadEvent()
    {
        ShowHideTicketPurchaseDiv();

        ddlCountry.DataSource = _master.GetCountryDetail();
        ddlCountry.DataValueField = "CountryID";
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        ddlshpCountry.DataSource = _master.GetCountryDetail();
        ddlshpCountry.DataValueField = "CountryID";
        ddlshpCountry.DataTextField = "CountryName";
        ddlshpCountry.DataBind();
        ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        if (Session["siteId"] != null && Session["P2POrderID"] != null)
            new ManageBooking().UpdateSiteToOrder(Convert.ToInt64(Session["P2POrderID"]), Guid.Parse(Session["siteId"].ToString()));

        AddItemInShoppingCart();
        GetCurrencyCode();
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            string countryID = cookie.Values["_cuntryId"];
            ddlCountry.SelectedValue = countryID;
            ddlshpCountry.SelectedValue = countryID;
        }
        FillShippingData();
    }

    public void FillShippingData()
    {
        if (ddlCountry.SelectedValue != "0")
        {
            var countryid = Guid.Parse(ddlCountry.SelectedValue);
            var lstShip = new ManageBooking().getAllShippingDetail(siteId, countryid);
            if (lstShip.Any())
                rptShippings.DataSource = lstShip;
            else
            {
                var lstDefaultShip = new ManageBooking().getDefaultShippingDetail(siteId);
                if (lstDefaultShip.Any())
                    rptShippings.DataSource = lstDefaultShip;
                else
                    rptShippings.DataSource = null;
            }
            rptShippings.DataBind();
        }
    }

    public void AddItemInShoppingCart()
    {
        try
        {
            if (Session["P2POrderID"] == null)
                return;
            List<getbookingcartdata> lstPassDetail = _masterBooking.GetAllPassSale(Convert.ToInt64(Session["P2POrderID"]), "", getsitetickprotection()).OrderBy(x => x.OrderIdentity).ToList();
            lstPassDetail = lstPassDetail.ToList();

            if (lstPassDetail.Count() > 0)
            {
                rptTrain.DataSource = lstPassDetail.Count > 0 ? lstPassDetail : null;
                rptTrain.DataBind();
            }
            else
            {
                rptTrainTcv.DataSource = null;
                rptTrainTcv.DataBind();
                rptTrain.DataSource = null;
                rptTrain.DataBind();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void bindUsersession(string Email, Guid ID, string FirstName, string LastName, Guid SiteId, string Password)
    {
        Session.Remove("USERUsername");
        Session.Remove("USERRoleId");
        Session.Remove("USERUserID");
        Session.Remove("USERSiteID");
        USERuserInfo.UserEmail = Email;
        USERuserInfo.ID = ID;
        USERuserInfo.Username = FirstName;
        USERuserInfo.SiteID = SiteId;
    }

    protected BillingAddress GetBillingInfo()
    {
        return new BillingAddress
        {
            Address = txtAdd.Text,
            City = txtCity.Text,
            Country = ddlCountry.SelectedItem.Text,
            Email = txtEmail.Text.Trim(),
            FirstName = txtFirst.Text.Trim(),
            Lastname = txtLast.Text.Trim(),
            ZipCode = txtZip.Text.Trim(),
            State = txtState.Text.Trim()
        };
    }

    protected void DeleteTicketInfo(string id)
    {
        try
        {
            List<ShoppingCartDetails> cartList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
            List<BookingRequest> bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;

            if (cartList.Count > 0 && bookingList.Count > 0)
            {
                cartList.RemoveAll(x => x.Id.ToString() == id);
                bookingList.RemoveAll(x => x.Id.ToString() == id);
                Session["SHOPPINGCART"] = cartList;
                Session["BOOKING-REQUEST"] = bookingList;
                ShowMessage(1, "You have successfully deleted ticket from list view");
            }
            else
            {
                Session["SHOPPINGCART"] = null;
                Session["BOOKING-REQUEST"] = null;
            }
            AddItemInShoppingCart();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            CheckBox chk = e.Item.FindControl("chkTicketProtection") as CheckBox;
            Label lblPrice = e.Item.FindControl("lblPrice") as Label;
            Label lbltpPrice = e.Item.FindControl("lbltpPrice") as Label;
            Label currsyb = e.Item.FindControl("currsyb") as Label;
            var lblTckProc = e.Item.FindControl("lblTckProc") as Label;
            var divTckProt = e.Item.FindControl("divTckProt") as HtmlGenericControl;
            var istckProt = _masterPage.IsTicketProtection(siteId);
            if (divTckProt != null)
                divTckProt.Visible = istckProt;

            if (lblTckProc != null)
                lblTckProc.Visible = istckProt;

            if (chk.Checked)
            {
                lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                _total = _total + Convert.ToDouble(lblPrice.Text);
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            }
            else
            {
                _total = _total + Convert.ToDouble(lblPrice.Text);
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            Int64 OrderId = Convert.ToInt64(Session["P2POrderID"] != null ? Session["P2POrderID"] : "0");
            String ApiName = Request.QueryString["req"] != null ? (Request.QueryString["req"].Trim() == "IT" ? "ITALIA" : (Request.QueryString["req"].Trim() == "BE" ? "BENE" : string.Empty)) : string.Empty;

            Label lblTotal = e.Item.FindControl("lblTotal") as Label;
            Label lblBookingFee = e.Item.FindControl("lblBookingFee") as Label;
            double totalAmount;
            decimal BookingFeeAmount = new ManageBooking().P2PGetBooingFees(OrderId, ApiName);
            if (BookingFeeAmount > 0)
            {
                lblBookingFee.Text = BookingFeeAmount.ToString("F2");
                hdnBookingFee.Value = BookingFeeAmount.ToString("F2");
            }
            else
            {
                lblBookingFee.Text = "0.00";
                hdnBookingFee.Value = "0.00";
            }
            totalAmount = _total + Convert.ToDouble(hdnBookingFee.Value.Trim());
            lblTotal.Text = totalAmount.ToString("F2");
            lblTotalCost.Text = totalAmount.ToString("F2");
            /*update CommissionFeeAmount*/
            new ManageBooking().P2PGetCommissionFee(Convert.ToInt64(Session["P2POrderID"] != null ? Session["P2POrderID"] : "0"));
        }
    }

    protected void rptTrainTcv_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                string id = e.CommandArgument.ToString();
                if (e.CommandName == "Remove")
                    DeleteTicketInfo(id);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void rptTrainTcv_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Label lblp = e.Item.FindControl("lblPrice") as Label;
            string prc = lblp.Text.Replace("$", "");
            _total = _total + Convert.ToDouble(prc);
        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotal = e.Item.FindControl("lblTotal") as Label;
            lblTotal.Text = "Total:" + currency + _total.ToString("F2");
        }
    }

    public decimal getsitetickprotection()
    {
        try
        {
            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
            {
                divpopupdata.InnerHtml = list.Description;
                return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
            }
            else
                return 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["IsCheckout"] == null)
            {
                Session["IsCheckout"] = true;

                Session.Add("P2BookURL", HttpContext.Current.Request.Url.AbsoluteUri);

                #region Agent login and Guest login
                if (Session["USERUserID"] == null && Session["AgentUsername"] == null)
                {
                    Guid UserID = Guid.NewGuid();
                    var listUser = _ManageUser.CheckEmailUser(txtEmail.Text);
                    if (listUser != null && listUser.IsActive == true)
                    {
                        //Login USER User Information
                        bindUsersession(listUser.Email, listUser.ID, listUser.FirstName, listUser.LastName, listUser.SiteId, listUser.Password);
                        UserID = listUser.ID;
                        if (Session["P2POrderID"] != null)
                        {
                            long orderID = Convert.ToInt64(Session["P2POrderID"]);
                            _masterBooking.SetorderUserid(orderID, UserID);
                        }
                    }
                    else
                    {
                        //add Login USER User Information
                        if (Session["USERUserID"] != null)//For Gust Only
                        {
                            UserID = Guid.Parse(Session["USERUserID"].ToString());
                        }
                        string password = Membership.GeneratePassword(10, 3);
                        bool result = _masterBooking.AddLoginUSer(new tblUserLogin
                        {
                            ID = UserID,
                            FirstName = txtFirst.Text,
                            LastName = txtLast.Text,
                            Email = txtEmail.Text,
                            Password = password,
                            Country = Guid.Parse(ddlCountry.SelectedValue),
                            SiteId = siteId,
                            IsActive = true
                        });
                        if (result == false)
                            bindUsersession(txtEmail.Text, UserID, txtFirst.Text, txtLast.Text, siteId, password);
                    }
                }
                #endregion

                #region Booking Request

                var hdnDMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
                if (hdnDMethod != null && (hdnDMethod.Value == "DH" || hdnDMethod.Value == "ST" || hdnDMethod.Value == "TL"))
                    hdnShipMethod.Value = string.Empty;

                Session["ShipMethod"] = hdnShipMethod.Value != "" ? hdnShipMethod.Value : null;
                Session["ShipDesc"] = hdnShipDesc.Value.Trim();

                bool BookingResult = false;
                int postcodelen = txtZip.Text.Trim().Replace(" ", "").Length;
                lblpmsg.Visible = postcodelen > 7;
                if (postcodelen > 7)
                    return;

                #region Booking request check from Services
                if (Session["BOOKING-REQUEST"] != null)
                {
                    var mngUser = new ManageUser();
                    var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                    var address = GetBillingInfo();
                    if (bookingList != null)
                        foreach (var item in bookingList)
                        {
                            if (item.PurchasingForServiceOwnerRequest != null)
                            {
                                var client = new OneHubRailOneHubClient();
                                PurchasingForServiceOwnerRequest req = item.PurchasingForServiceOwnerRequest;
                                req.BillingAddress = address;
                                ApiLoginOwner(req);
                                PurchasingForServiceOwnerResponse response = item.IsInternational ? client.PurchasingForServicesInternational(req) : client.PurchasingForServiceOwner(req);
                                if (response != null)
                                {
                                    if (response.TicketBookingDetailList != null && response.TicketIssueResponse != null && response.ErrorMessage == null)
                                    {
                                        GetBookingResponse(response, null, address, item.DepartureStationName);
                                        string DeliveryMathod = GetDilveryName(string.Empty);
                                        AddP2PBookingInLocalDB(DeliveryMathod, 0);
                                        BookingResult = true;
                                    }
                                    else
                                    {
                                        ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                        return;
                                    }
                                }
                                else
                                    ShowMessage(2, "There has been an error with your booking.");
                            }

                            if (item.PurchasingForServiceRequest != null)
                            {
                                var client = new OneHubRailOneHubClient();
                                PurchasingForServiceRequest req = item.PurchasingForServiceRequest;

                                #region API Account
                                /*var api = mngUser.GetApiLogingBySiteId(siteId);
                            if (api != null)
                            {
                                var trainName = Request.QueryString["req"].Trim() == "BE" ? "BENE" : "ITALIA";
                                var result = api.FirstOrDefault(x => x.TrainName.Trim() == trainName);
                                if (result != null)
                                {
                                    req.Header.ApiLoginDetail = new ApiLoginDetail
                                    {
                                        AccountType = result.AccountType.ToString() == "1" ? AccountType.Primary : AccountType.Secondary,
                                        isProductionAccount = result.IsProductionAccnt
                                    };
                                }
                            }*/
                                #endregion
                                req.BillingAddress = address;

                                #region Delivery Option
                                HiddenField hiddenDileveryMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
                                req.BookingType = hiddenDileveryMethod.Value == "TL" ? BookingType.confirm : BookingType.provisional;
                                switch (hiddenDileveryMethod.Value)
                                {
                                    case "TL":
                                        req.DeliveryMethod = DeliveryMethod.TL;
                                        break;
                                    case "TA":
                                        req.DeliveryMethod = DeliveryMethod.TA;
                                        break;
                                    case "ST":
                                        req.DeliveryMethod = DeliveryMethod.ST;
                                        break;
                                    case "DH":
                                        req.DeliveryMethod = DeliveryMethod.DH;
                                        break;
                                    case "HP":
                                        req.DeliveryMethod = DeliveryMethod.HP;
                                        break;
                                }
                                #endregion

                                #region Ticket Collection Location
                                var ddlCollectStation = ucTicketDelivery.FindControl("ddlCollectStation") as DropDownList;
                                if (ddlCollectStation != null)
                                    if (ddlCollectStation.SelectedValue != "0")
                                        Session["CollectStation"] = ddlCollectStation.SelectedItem.Text;
                                    else
                                        Session["CollectStation"] = null;
                                #endregion

                                #region Passanger Info
                                int start = 0;
                                //DataList dtlPassngerDetails = hiddenDileveryMethod.Value == "TA" ? ucTicketDelivery.FindControl("dtlPassngerDetails2") as DataList : ucTicketDelivery.FindControl("dtlPassngerDetails") as DataList;
                                var dtlPassngerDetails = hiddenDileveryMethod.Value == "TA" ? ucTicketDelivery.FindControl("dtlPassngerDelivery") as DataList : ucTicketDelivery.FindControl("dtlPassngerDetails") as DataList;
                                var oPassnger = req.PassengerListReply.ToList();
                                foreach (DataListItem li in dtlPassngerDetails.Items)
                                {
                                    var txtFirstName = li.FindControl("txtFirstName") as TextBox;
                                    var txtLastName = li.FindControl("txtLastName") as TextBox;
                                    var txtEmailAddress = li.FindControl("txtEmailAddress") as TextBox;
                                    oPassnger[start].FirstName = string.IsNullOrEmpty(txtFirstName.Text) ? address.FirstName : txtFirstName.Text;
                                    oPassnger[start].LastName = string.IsNullOrEmpty(txtLastName.Text) ? address.Lastname : txtLastName.Text;
                                    oPassnger[start].EmailId = string.IsNullOrEmpty(txtEmailAddress.Text) ? address.Email : txtEmailAddress.Text;

                                    DropDownList ddlDay = li.FindControl("ddlDay") as DropDownList;
                                    DropDownList ddlMonth = li.FindControl("ddlMonth") as DropDownList;
                                    DropDownList ddlYear = li.FindControl("ddlYear") as DropDownList;
                                    List<string> TariffgroupList = new List<string> { "THA", "TGV", "SVI", "TGI", "TGS", "TPL", "RHE" };
                                    bool isThaylo = req.BookingRequestList.Any(t => t.PriceOffer.Any(p => TariffgroupList.Contains(p.Taco.Tariffgroup)));
                                    if (isThaylo && ddlDay != null)
                                    {
                                        var dob = ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
                                        oPassnger[start].BirthDate = Convert.ToDateTime(dob);
                                    }

                                    start++;
                                }
                                req.PassengerListReply = oPassnger.ToArray();
                                #endregion

                                #region Delivery by mail
                                if (hiddenDileveryMethod.Value == "TA")
                                {
                                    var txtMailFName = ucTicketDelivery.FindControl("txtMailFName") as TextBox;
                                    var txtMailLName = ucTicketDelivery.FindControl("txtMailLName") as TextBox;
                                    var txtDepartment = ucTicketDelivery.FindControl("txtDepartment") as TextBox;
                                    var txtStreet = ucTicketDelivery.FindControl("txtStreet") as TextBox;
                                    var txtPostalCode = ucTicketDelivery.FindControl("txtPostalCode") as TextBox;
                                    var txtMailCity = ucTicketDelivery.FindControl("txtMailCity") as TextBox;
                                    var ddlCountryMail = ucTicketDelivery.FindControl("ddlCountryMail") as DropDownList;
                                    var ddlStateMail = ucTicketDelivery.FindControl("ddlStateMail") as DropDownList;
                                    var ordID = Convert.ToInt64(Session["P2POrderID"]);
                                    var details = new tblP2PDeliveryDetails
                                    {
                                        ID = Guid.NewGuid(),
                                        OrderID = ordID,
                                        FirstName = txtMailFName.Text,
                                        LastName = txtMailLName.Text,
                                        Department = txtDepartment.Text,
                                        Address = txtStreet.Text,
                                        Postcode = txtPostalCode.Text,
                                        City = txtCity.Text,
                                        CountryID = Guid.Parse(ddlCountryMail.SelectedValue),
                                        StateID = ddlStateMail.SelectedIndex != -1 ? Convert.ToInt32(ddlStateMail.SelectedValue) : 0,
                                        CreatedDate = DateTime.Now
                                    };

                                    _masterBooking.AddDeliveryDetails(details);
                                }
                                #endregion

                                #region Loyaltycard
                                if (hiddenDileveryMethod.Value == "TL")
                                {
                                    start = 0;
                                    DataList dtlLoayalty = ucTicketDelivery.FindControl("dtlLoayalty") as DataList;
                                    if (dtlLoayalty.Items.Count > 0)
                                    {
                                        foreach (DataListItem li in dtlLoayalty.Items)
                                        {
                                            TextBox txtThalysCardNumber = li.FindControl("txtThalysCardNumber") as TextBox;
                                            if (!string.IsNullOrEmpty(txtThalysCardNumber.Text))
                                            {
                                                List<Loyaltycard> loyCard = new List<Loyaltycard>();
                                                loyCard.Add(new Loyaltycard
                                                {
                                                    cardnumber = txtThalysCardNumber.Text,
                                                    carrier = new Carrier
                                                    {
                                                        code = "THA"
                                                    }
                                                });
                                                oPassnger[start].Loyaltycard = loyCard.ToArray();
                                            }
                                            start++;
                                        }
                                        req.PassengerListReply = oPassnger.ToArray();
                                    }


                                    #region Validate Card Numbers
                                    var list = oPassnger.SelectMany(x => x.Loyaltycard).ToList();
                                    var chkduplicate = list.GroupBy(x => x.cardnumber).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
                                    if (chkduplicate.Any(x => x.Count > 1))
                                    {
                                        list = null;
                                        ShowMessage(2, "Similar (Thalys or Eurostar) card number is not allowed for another traveller.");
                                        return;
                                    }
                                    else if (list.Any(x => x.cardnumber.Length < 16))
                                    {
                                        list = null;
                                        ShowMessage(2, "Invalid card number.");
                                        return;
                                    }
                                    else if (list.Where(z => z.carrier.code == "EUR").Any(x => x.cardnumber.Substring(0, 6) != "308381"))
                                    {
                                        list = null;
                                        ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
                                        return;
                                    }

                                    else if (list.Where(z => z.carrier.code == "THA").Any(x => x.cardnumber.Substring(0, 6) != "308406"))
                                    {
                                        list = null;
                                        ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
                                        return;
                                    }

                                    #endregion
                                }
                                #endregion
                                ApiLogin(req);
                                #region Request And Save Data In DB
                                PurchasingForServiceResponse response = client.PurchasingForService(req);
                                if (response != null)
                                {
                                    if (!String.IsNullOrEmpty(response.ReservationCode) && response.ErrorMessage == null)
                                    {
                                        GetBookingResponse(null, response, null, item.DepartureStationName);
                                        string DeliveryMathod = GetDilveryName(hdnDMethod.Value);
                                        AddP2PBookingInLocalDB(DeliveryMathod, response.Amount);
                                        BookingResult = true;
                                    }
                                    else
                                    {
                                        ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                        return;
                                    }
                                }
                                else
                                    ShowMessage(2, "There has been an error with your booking.");
                                #endregion
                            }
                        }
                }
                #endregion

                if (Session["BOOKING-REQUEST"] == null)
                    Response.Redirect(siteURL);

                if (BookingResult)
                {
                    if (P2POrderId != 0)
                    {
                        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                        if (Session["AgentUsername"] == null && result != null && (result.IsWholeSale || (bool)result.IsAgent))
                        {
                            Session["redirectpage"] = "?req=" + P2POrderId;
                            Response.Redirect("~/Agent/login", false);
                        }
                        else
                            Response.Redirect("~/PaymentProcess?req=" + P2POrderId, false);
                    }

                }
                else
                    Session["IsCheckout"] = null;

                #endregion
            }
            else
                Response.Redirect("~/PaymentProcess?req=" + P2POrderId, false);
        }
        catch (Exception ex)
        {
            Session["IsCheckout"] = null;
            ShowMessage(2, ex.Message);
        }
    }

    public string GetDilveryName(string value)
    {
        switch (value)
        {
            case "TL":
                return "Thayls Ticketless";
            case "TA":
                return "Delivery by mail";
            case "ST":
                return "Collect at ticket desk";
            case "DH":
                return "Print at Home";
            case "HP":
                return "Self print train tickets";
            default:
                return "TrenItaila Printing";
        }
    }

    void AddP2PBookingInLocalDB(string delivery, float respAmt)
    {
        if (Session["P2PIdInfo"] == null)
            return;
        foreach (RepeaterItem it in rptTrain.Items)
        {
            CheckBox chkTicketProtection = it.FindControl("chkTicketProtection") as CheckBox;
            Label lbltpPrice = it.FindControl("lbltpPrice") as Label;
            Label lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
            if (chkTicketProtection.Checked)
                new ManageBooking().UpdateTicketProtection(Guid.Parse(lblPassSaleID.Text.Trim()), Convert.ToDecimal(lbltpPrice.Text.Trim()));

            if (Session["IsFullPrice"] != null && respAmt > 0)
            {
                bool IsFullPrice = Convert.ToBoolean(Session["IsFullPrice"].ToString());
                if (!IsFullPrice)
                    UpdatePrice(Convert.ToDecimal(GetTotalPriceBeNe((Decimal)respAmt, false) / rptTrain.Items.Count), Guid.Parse(lblPassSaleID.Text.Trim()));
            }

        }
        Session["IsFullPrice"] = null;
        bool isReg = false;
        if (Session["IsRegional"] != null)
            isReg = Convert.ToBoolean(Session["IsRegional"]);

        Int64 orderID = Convert.ToInt64(Session["P2POrderID"]);
        List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
        if (Session["P2PIdInfo"] != null)
            lstP2PIdInfo = (List<P2PReservationIDInfo>)Session["P2PIdInfo"];

        foreach (RepeaterItem it in rptTrain.Items)
        {
            Label lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
            ManageBooking objBooking = new ManageBooking();

            if (Session["BOOKING-REPLY"] != null)
            {
                var oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                foreach (var item in oldList)
                {
                    string pinNumber = string.IsNullOrEmpty(item.PinCode) ? item.UnitOfWork.ToString() : item.PinCode;

                    if (lstP2PIdInfo.Count() > 0)
                    {
                        List<P2PReservationIDInfo> NlstP2PIdInfo = lstP2PIdInfo.Where(a => a.JourneyType == "").ToList();
                        if (NlstP2PIdInfo.Count() > 0)
                            foreach (P2PReservationIDInfo o in NlstP2PIdInfo)
                            {
                                bool result = objBooking.UpdateReservationCodebyP2PID(o.ID, item.ReservationCode, pinNumber, delivery, false);
                            }
                        else
                            objBooking.UpdateReservationCodebyP2PID(Guid.Parse(lblPassSaleID.Text.Trim()), orderID, item.ReservationCode, pinNumber, delivery, false, item.DepStationName, isReg);
                    }
                    else
                        objBooking.UpdateReservationCodebyP2PID(Guid.Parse(lblPassSaleID.Text.Trim()), orderID, item.ReservationCode, pinNumber, delivery, false, item.DepStationName, isReg);
                }
            }
        }

        tblOrderBillingAddress objBillingAddress = new tblOrderBillingAddress();
        objBillingAddress.ID = Guid.NewGuid();
        objBillingAddress.OrderID = orderID;
        objBillingAddress.Title = ddlMr.SelectedItem.Text;
        objBillingAddress.FirstName = txtFirst.Text;
        objBillingAddress.LastName = txtLast.Text;
        objBillingAddress.Phone = txtBillPhone.Text;
        objBillingAddress.Address1 = txtAdd.Text;
        objBillingAddress.Address2 = txtAdd2.Text;
        objBillingAddress.EmailAddress = txtEmail.Text;
        objBillingAddress.City = txtCity.Text;
        objBillingAddress.State = txtState.Text;
        objBillingAddress.Country = ddlCountry.SelectedItem.Text;
        objBillingAddress.Postcode = txtZip.Text;

        if (!chkShippingfill.Checked)
        {
            objBillingAddress.TitleShpg = ddlMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtFirst.Text;
            objBillingAddress.LastNameShpg = txtLast.Text;
            objBillingAddress.EmailAddressShpg = txtEmail.Text;
            objBillingAddress.PhoneShpg = txtBillPhone.Text;
            objBillingAddress.Address1Shpg = txtAdd.Text;
            objBillingAddress.Address2Shpg = txtAdd2.Text;
            objBillingAddress.CityShpg = txtCity.Text;
            objBillingAddress.StateShpg = txtState.Text;
            objBillingAddress.CountryShpg = ddlCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtZip.Text;
        }
        else
        {
            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;
        }
        if (txtDateOfDepature.Text.Trim() != "DD/MM/YYYY")
            new ManageBooking().UpdateDepatureDate(Convert.ToInt64(Session["P2POrderID"]), Convert.ToDateTime(txtDateOfDepature.Text));

        new ManageBooking().AddOrderBillingAddress(objBillingAddress);
        if (Request.QueryString["req"] != null && Request.QueryString["req"].Trim() == "IT")
            hdnShippingCost.Value = "0";

        HiddenField hiddenDileveryMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
        string ShipMethod = "";
        string ShipDesc = "";
        string CollectStation = "";
        if (hiddenDileveryMethod.Value == "TA" && Request.QueryString["req"] == "BE")
        {
            ShipMethod = hdnShipMethod.Value;
            ShipDesc = hdnShipDesc.Value;
        }
        else
            hdnShippingCost.Value = "0";

        if (Session["CollectStation"] != null)
            CollectStation = Session["CollectStation"].ToString();
        _masterBooking.UpdateOrderData(Convert.ToDecimal(hdnShippingCost.Value), ShipMethod, ShipDesc, CollectStation, Convert.ToInt64(Session["P2POrderID"]));
        /*Update IsRegional*/
        if (Session["IsRegional"] != null)
            new ManageBooking().UpdateIsRegional(Convert.ToBoolean(Session["IsRegional"].ToString()), Convert.ToInt64(Session["P2POrderID"]));

        new ManageBooking().UpdateOrderBookingFee(Convert.ToDecimal(hdnBookingFee.Value.Trim()), Convert.ToInt64(Session["P2POrderID"]));
    }

    public Decimal GetTotalPriceBeNe(Decimal snoPrice, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (snoPrice > 0)
        {
            string MinPrice = FrontEndManagePass.GetPriceAfterConversion(snoPrice, siteId, srcCurId, currencyID).ToString("F");
            price = Convert.ToDecimal(GetRoundPrice(MinPrice));
        }
        return price;
    }

    protected void GetBookingResponse(PurchasingForServiceOwnerResponse responseown, PurchasingForServiceResponse response, BillingAddress address, string DepStName)
    {
        try
        {
            #region TI response
            List<BookingResponse> listbookingReply = new List<BookingResponse>();
            if (responseown != null)
            {
                var ticketBookingDetail = responseown.TicketBookingDetailList.FirstOrDefault(x => x.ReservationCode == responseown.TicketIssueResponse.ReservationCode);
                listbookingReply = responseown.TicketIssueResponse != null
                                       ? new List<BookingResponse>
                                                   {
                                                       new BookingResponse
                                                           {
                                                               Issued = responseown.TicketIssueResponse.Issued,
                                                              // Coupons= responseown.TicketIssueResponse.Coupons,
                                                               ReservationCode = responseown.TicketIssueResponse.ReservationCode,
                                                               ChangeReservationCode =ticketBookingDetail!=null? ticketBookingDetail.ChangeReservationCode:string.Empty,
                                                               UnitOfWork = responseown.TicketIssueResponse.UnitOfWork,
                                                               BillingAddress = address,
                                                               DepStationName = DepStName,
                                                                PdfUrl=TicketUrl(responseown.TicketIssueResponse.ReceiptPDF.ToList(),responseown.TicketIssueResponse.ReservationCode)
                                                         
                                                           }
                                                   }
                                       : null;
            }
            #endregion

            #region BENE response
            if (response != null)
                listbookingReply = new List<BookingResponse>
                    {
                        new BookingResponse
                            {
                                Issued = true,
                                ReservationCode = response.ReservationCode,
                                PinCode = response.Pincode,
                                Pnr = response.Pnr,
                                DepStationName = DepStName,
                               
                            }
                    };
            #endregion

            if (Session["BOOKING-REPLY"] != null)
            {
                List<BookingResponse> oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                if (oldList != null) if (listbookingReply != null) listbookingReply.AddRange(oldList);
            }
            Session["BOOKING-REPLY"] = listbookingReply;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }


    public List<string> TicketUrl(List<byte[]> base64StrList, string fileName)
    {
        try
        {
            List<string> urls = new List<string>();
            string siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            foreach (var itm in base64StrList)
            {
                string pdfName = string.IsNullOrEmpty(fileName) ? Session["P2POrderID"].ToString() + "-" + Guid.NewGuid().ToString() : fileName + "-" + Guid.NewGuid().ToString();
                byte[] bytes = itm;
                string path = "pdfService/" + pdfName + ".pdf";
                FileStream stream = new FileStream(Server.MapPath("~/" + path), FileMode.CreateNew);
                System.IO.BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
                urls.Add(siteURL + path);
            }
            return urls;
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(Request.Url.ToString(), ex.Message);
            return null;
        }
    }



    public void ShowHideTicketPurchaseDiv()
    {
        if (Request.QueryString["req"] != null)
        {
            if (Request.QueryString["req"].Trim() == "BE")
                ucTicketDelivery.Visible = true;
        }
        else
            ucTicketDelivery.Visible = false;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["req"] != null)
        {
            Response.Redirect("TrainResults.aspx?req=" + Request.QueryString["req"], true);
        }
        else
            Response.Redirect("TrainResults.aspx", true);

    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillShippingData();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "customselect", "customselect()", true);
    }

    #region Regional BeNe Booking
    void UpdatePrice(decimal price, Guid passSaleId)
    {
        try
        {
            tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == passSaleId).FirstOrDefault();
            objP2P.ApiPrice = price;
            objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(price.ToString()));
            objP2P.Price = BusinessOneHub.IsNumeric(price) ? GetTotalPriceBeNe(price) : 0;
            _db.SaveChanges();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public Decimal GetTotalPriceBeNe(decimal price)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
        string MinPrice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(price), siteId, srcCurId, currencyID).ToString("F");
        Decimal NewPrice = Convert.ToDecimal(GetRoundPrice(MinPrice));
        return NewPrice;
    }

    string GetRoundPrice(string price)
    {
        //Please round up the prices to nearest above 0.50
        //If he price is 100.23, mark it as 100.50, 
        //If the price is 100.65 mark it as 102.00

        string[] strPrice = price.ToString().Split('.');

        if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
            return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
        else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
            return strPrice[0].Trim() + ".50";
        else
            return strPrice[0].Trim() + ".00";

    }
    #endregion

    #region Api Login
    private void ApiLoginOwner(PurchasingForServiceOwnerRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }

    private void ApiLogin(PurchasingForServiceRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    #endregion
}