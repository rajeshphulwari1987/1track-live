﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="RailPassSection3.aspx.cs"
    Inherits="RailPassSection3" %>

<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#rptMainMenu_HyperLink1_1").addClass("active");
        });
    </script>
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsAbs
        {
            display: none;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
      <div class="breadcrumb">
        <a href="#">Home </a>>> Rail Pass Section
    </div>
    <div class="innner-banner" style="padding-bottom:3px;">
        <div class="float-lt">
            <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
            <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
        </div>
        </div>
    </div>
   
<div class="left-content">
    <div id="footerBlock" runat="server">
        <h1> <asp:Label ID="lblTitle" runat="server"/> </h1>
    <div style="float:right;width:288px;padding-left:10px;">
        <img ID="imgRt" runat="server" border="0" />
    </div>
    <div runat="server" id="divDescription" style="padding:3px;font-size:13px;"></div>
</div>
 
</div>
<div class="right-content">
<div id="divJRF" runat="server">  
<a  runat="server" id="trainresults">
    <div id="rtPannel" runat="server"></div>
    <img src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />
</a>
</div>
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
