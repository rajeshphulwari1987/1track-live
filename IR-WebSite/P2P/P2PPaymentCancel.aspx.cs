﻿using System;
using Business;
using System.Web.UI;

public partial class P2P_P2PPaymentCancel : System.Web.UI.Page
{
    #region Global Variables
    private Guid _siteId;
    public string OrderID = string.Empty;
    private tblP2PSetting _tblP2PSetting = new tblP2PSetting();
    string DirectoryUrl = string.Empty;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageP2PSettings _manageP2PSettings = new ManageP2PSettings();
    public string Url = string.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["homeUrl"] == null)
        {
            _tblP2PSetting = _manageP2PSettings.GetP2PSettingBySiteId(_siteId);
            if (_tblP2PSetting != null)
            {
                DirectoryUrl = _tblP2PSetting.DirectoryPath;
                var v = _manageP2PSettings.GetP2PSettingBySiteId(_siteId);
                if (v != null)
                {
                    ViewState["homeUrl"] = DirectoryUrl + "" + v.P2PWidgetPage;
                }
            }
        }
        Session["OrderID"] = null;
        Session["P2POrderID"] = null;
        Session["RailPassData"] = null;
        Session["key"] = null;
    }
    protected void btnGoBack_Click(object sender, EventArgs e)
    {
        Url = ViewState["homeUrl"].ToString();
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "msg", "RedirectPage('" + Url + "');", true);
    }
}