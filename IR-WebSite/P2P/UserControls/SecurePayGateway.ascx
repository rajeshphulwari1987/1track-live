﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecurePayGateway.ascx.cs"
    Inherits="P2P_UserControls_SecurePayGateway" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .form-row input[type="submit"]
    {
        padding-bottom: 3px;
        border: 1px solid #000;
        background: url(..../images/gray-btn-bg.jpg) repeat-x;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        font-size: 17px;
        behavior: url(PIE.htc);
        color: #FFF;
        cursor: pointer;
    }
    .form-row.buttons
    {
        text-align: right;
        margin: 10px 0;
        padding-right: 2px;
    }
    
    .secureTable
    {
        width: 100%;
    }
    .secureTable tr td
    {
        line-height: 25px;
    }
    .required
    {
        color: Red;
    }
</style>
<script type="text/javascript" src='../Scripts/jquery-1.9.0.min.js'></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#ContentPlaceHolder1_SecurePayGateway1_btnSubmit").click(function () {
            if ($("#ContentPlaceHolder1_SecurePayGateway1_chkActive").is(":checked")) {
                return true;
            }
            else {
                alert("To continue, you must accept our Terms & Conditions");
                return false;
            }
        });
    });
</script>
<style type="text/css">
    .modalBackground
    {
        position: fixed;
        top: 480px;
        bottom: 10px;
        left: 10px;
        right: 10px;
        overflow: hidden;
        padding: 0;
        margin: 0;
        background-color: #000;
        filter: alpha(opacity=50);
        opacity: 0.5;
        text-align: center;
        float: left;
    }
    .progessposition
    {
        padding-top: 20%;
    }
</style>
<script type="text/javascript">
    function GetPriceclick() {
        $("#hdnPrice").val($("#ContentPlaceHolder1_SecurePayGateway1_lblTotalPrice").text());
    }
</script>
<asp:HiddenField runat="server" ID="hdnPrice" ClientIDMode="Static" />
<asp:UpdatePanel ID="updSearchUC" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnAmexCharge" runat="server" Value="0" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:Panel ID="pnlErrSuccess" runat="server" Width="99%">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <table class="grid" width="80%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 48%">
                    Order Ref:
                </td>
                <td>
                    IRWEB<%=OrderNo%>
                </td>
            </tr>
            <tr>
                <td>
                    Amount:
                </td>
                <td>
                    <%=Session["currencyCode"].ToString()%>
                    &nbsp;<asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Pay with
                </td>
                <td>
                    <img src="../images/VISA_choice.gif" />
                    <img src="../images/icon_mastercard.png" />
                    <img src="../images/amex.png" />
                </td>
            </tr>
            <tr>
                <td>
                    Card Number (long 16 digit number)<span class="required">*</span>
                </td>
                <td>
                    <asp:TextBox ID="txtCardNumber" runat="server" MaxLength="16" Width="165px" autocomplete="off" />
                    <asp:RequiredFieldValidator ID="reqtxtCardNumber" ControlToValidate="txtCardNumber"
                        ValidationGroup="vg" runat="server" Display="Dynamic" ErrorMessage="*" CssClass="required" />
                </td>
            </tr>
            <tr>
                <td>
                    Card Type<span class="required">*</span>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCardType" runat="server" Width="172px">
                        <asp:ListItem Value="0">Select One</asp:ListItem>
                        <asp:ListItem Value="2">MasterCard</asp:ListItem>
                        <asp:ListItem Value="3">Visa</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="reqddlCardType" ControlToValidate="ddlCardType" ValidationGroup="vg"
                        runat="server" Display="Dynamic" ErrorMessage="*" CssClass="required" InitialValue="0" />
                </td>
            </tr>
            <tr>
                <td>
                    Cardholder's name <span class="required">*</span>
                </td>
                <td>
                    <asp:TextBox ID="txtCardholder" runat="server" MaxLength="50" Width="165px" autocomplete="off" />
                    <asp:RequiredFieldValidator ID="reqtxtCardholder" ControlToValidate="txtCardholder"
                        ValidationGroup="vg" runat="server" Display="Dynamic" ErrorMessage="*" CssClass="required" />
                </td>
            </tr>
            <tr>
                <td>
                    Expiry date (mm/yyyy) <span class="required">*</span>
                </td>
                <td>
                    Month
                    <asp:DropDownList ID="ddlMonth" runat="server">
                        <asp:ListItem Value="01">01</asp:ListItem>
                        <asp:ListItem Value="02">02</asp:ListItem>
                        <asp:ListItem Value="03">03</asp:ListItem>
                        <asp:ListItem Value="04">04</asp:ListItem>
                        <asp:ListItem Value="05">05</asp:ListItem>
                        <asp:ListItem Value="06">06</asp:ListItem>
                        <asp:ListItem Value="07">07</asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                    </asp:DropDownList>
                    Year
                    <asp:DropDownList ID="ddlYear" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Card verification code <span class="required">*</span>
                </td>
                <td>
                    <asp:TextBox ID="txtCardVarificationCode" runat="server" MaxLength="4" Width="165px"
                        autocomplete="off" />
                    <asp:RequiredFieldValidator ID="reqtxtCardVarificationCode" ControlToValidate="txtCardVarificationCode"
                        ValidationGroup="vg" runat="server" Display="Dynamic" ErrorMessage="*" CssClass="required" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Style="width: 120px;
                        float: left;" Text="Back" />
                </td>
                <td style="text-align: right">
                    <asp:CheckBox ID="chkActive" CssClass=".check" runat="server" />
                    <i style="font-size: 12px; line-height: 28px;">By continuing you agree to and accept
                        our <a href="http://www.internationalrail.com.au/information/terms-and-conditions.aspx"
                            target="_blank">Terms and Conditions</a></i>
                    <br />
                    <span class="submitButton">
                        <asp:Button ID="btnSubmit" runat="server" Text="Pay &amp; complete order" OnClientClick="GetPriceclick();"
                            CssClass="clsOrderPayBtn contourButton contourNext contourSubmit" ValidationGroup="vg"
                            OnClick="btnSubmit_Click" />
                    </span>
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updSearchUC"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="overlaybg" style="z-index: 999999; top: 64% !important;">
            &nbsp;</div>
        <div class="progess-inner2" style="left: 41%; top: 74%">
            <strong>One Moment Please </strong>
            <img src="../images/dots32.gif" alt="progress bar" />
            <span>We are processing your payment. Please wait..</span>
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
