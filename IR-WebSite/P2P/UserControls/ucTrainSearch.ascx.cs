﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;

public partial class OtherSiteP2PBooking_ucTrainSearch : UserControl
{
    #region Global Variables
    public static string UnavailableDates1 = "";
    Guid _siteId;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageP2PSettings _manageP2PSettings = new ManageP2PSettings();
    string _directoryUrl = string.Empty;
    public string SiteURL, _siteURL;
    public int MinDate = 0;
    private tblP2PSetting _tblP2PSetting = new tblP2PSetting();
    public string UrlParent = string.Empty;
    public int MaxTransfer = 0;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        _siteId = Guid.Parse(System.Configuration.ConfigurationManager.AppSettings["siteId"]);
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            _siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            hdnSiteid.Value = _siteId.ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdnSiteid.Value = _siteId.ToString();
            if (ViewState["UrlP2PResult"] == null && ViewState["UrlLoyaltyCard"] == null && Request.UrlReferrer != null)
            {
                _tblP2PSetting = _manageP2PSettings.GetP2PSettingBySiteId(_siteId);
                if (_tblP2PSetting != null)
                {
                    _directoryUrl = _tblP2PSetting.DirectoryPath;
                    var v = _manageP2PSettings.GetP2PSettingBySiteId(_siteId);
                    if (v != null)
                    {
                        ViewState["UrlLoyaltyCard"] = _directoryUrl + "" + v.P2PLoyaltyCardInfoPage;
                        ViewState["UrlP2PResult"] = _directoryUrl + "" + v.P2pTrainResultsPage;
                    }
                }
            }

            #region

            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                SiteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
                //ShowHaveRailPass(_siteId);
                var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
                UnavailableDates1 = "[";
                if (siteDDates.Count() > 0)
                {
                    foreach (var it in siteDDates)
                    {
                        UnavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    UnavailableDates1 = UnavailableDates1.Substring(0, UnavailableDates1.Length - 1);
                }
                UnavailableDates1 += "]";
            }

            if (!IsPostBack)
            {
                bool isUseSite = (bool)_oWebsitePage.IsUsSite(_siteId);
                bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(_siteId);
                divSearch.Visible = IsVisibleP2PWidget;
                if (!IsVisibleP2PWidget)
                    ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('.ticketbooking img').hide();</script>", false);

                MinDate = _oWebsitePage.GetBookingDayLimitBySiteId(_siteId);
                rdBookingType.Items[1].Text = isUseSite ? "Round Trip" : "Return";
                for (int i = 10; i >= 0; i--)
                {
                    ddlAdult.Items.Insert(0, new ListItem(i.ToString(CultureInfo.InvariantCulture), i.ToString(CultureInfo.InvariantCulture)));
                    ddlAdult.SelectedValue = "1";
                }

                for (int j = 10; j >= 0; j--)
                {
                    ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                    ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                }
            }
            #endregion
            if (Request.UrlReferrer != null)
            {
                if (Session["RedirectToHome"] == null && !Request.UrlReferrer.AbsolutePath.ToString().Contains("p2p-search-results.aspx"))
                {
                    Session["RedirectToHome"] = Request.UrlReferrer.AbsolutePath.ToString();
                }
                else if (Session["RedirectToHome"] != null && !Request.UrlReferrer.AbsolutePath.ToString().Contains("P2PWidget.aspx ") && !Request.UrlReferrer.AbsolutePath.ToString().Contains("p2p-search-results.aspx") && !Request.UrlReferrer.AbsoluteUri.ToString().Contains(Session["RedirectToHome"].ToString()) && Request.UrlReferrer.AbsoluteUri.ToString().Contains("www.internationalrail.com.au"))
                {
                    Session["RedirectToHome"] = Request.UrlReferrer.AbsolutePath.ToString();
                }

                if (Session["BookingUCRerq"] != null)
                {
                    if (Request.UrlReferrer != null)
                    {
                        if (Request.UrlReferrer.AbsoluteUri.ToString().Contains(ViewState["UrlP2PResult"].ToString()))
                            SearchTrainInfoForFill();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        divRailPass.Visible = railPass;
    }

    void SetCallander()
    {
        try
        {
            if (ViewState["Rdo"] == "1")
            {
                txtReturnDate.Enabled = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "calenable()", true);
            }
            else
            {
                txtReturnDate.Enabled = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "caldisable()", true);
            }
            rdBookingType.SelectedValue = ViewState["Rdo"] == null ? "0" : ViewState["Rdo"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void SearchTrainInfo()
    {
        try
        {
            var objBruc = new BookingRequestUserControl();
            var objBooking = new ManageBooking();

            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(txtFrom.Text.Trim());
            objBruc.depstCode = objStationDeptDetail != null ? objStationDeptDetail.StationCode : string.Empty;

            objBruc.depRCode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
            ViewState["depRCode"] = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";

            GetStationDetailsByStationName objStationArrDetail = objBooking.GetStationDetailsByStationName(txtTo.Text.Trim());
            objBruc.arrstCode = objStationArrDetail != null ? objStationArrDetail.StationCode : string.Empty;

            objBruc.arrRCode = objStationArrDetail != null ? objStationArrDetail.RailwayCode : "0";
            if (objStationDeptDetail != null)
                objBruc.OneHubServiceName = objStationDeptDetail.RailName.Trim() == "BENE" ? "BeNe" : "Trenitalia";

            objBruc.FromDetail = txtFrom.Text.Trim();
            objBruc.ToDetail = txtTo.Text.Trim();
            objBruc.depdt = DateTime.Parse(txtDepartureDate.Text, new CultureInfo("en-CA"));
            objBruc.depTime = Convert.ToDateTime(ddldepTime.SelectedValue);

            objBruc.ClassValue = Convert.ToInt32(ddlClass.SelectedValue);
            objBruc.Adults = Convert.ToInt32(ddlAdult.SelectedValue);
            objBruc.Boys = Convert.ToInt32(ddlChild.SelectedValue);
            objBruc.Seniors = Convert.ToInt32(ddlSenior.SelectedValue);
            objBruc.Youths = Convert.ToInt32(ddlYouth.SelectedValue);
            objBruc.Transfare = MaxTransfer;
            objBruc.ReturnDate = String.IsNullOrEmpty(txtReturnDate.Text) || txtReturnDate.Text.Trim() == "DD/MM/YYYY" ? string.Empty : txtReturnDate.Text;
            objBruc.ReturnTime = ddlReturnTime.SelectedValue;
            objBruc.Loyalty = chkIsLoyaltyActive.Checked;
            objBruc.isIhaveRailPass = chkIhaveRailPass.Checked;
            objBruc.Journeytype = rdBookingType.SelectedValue;

            if (rdBookingType.SelectedValue == "1")
                objBruc.IsReturnJurney = true;

            Session["BookingUCRerq"] = objBruc;
            var currDate = DateTime.Now;

            int daysLimit = GetEurostarBookingDays(objBruc);
            if (daysLimit == 0)
                daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);

            var maxDate = currDate.AddDays(daysLimit - 1);

            if (objBruc.depdt > maxDate && objStationDeptDetail != null)
            {
                Session["ErrorMessage"] = "ErrorMaxDate";
                Session["TrainSearch"] = null;
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "xxx", "ParentResult();", true);
            }
            else
                Session["ErrorMessage"] = null;

            if (!chkIhaveRailPass.Checked)
            {

                var client = new OneHubRailOneHubClient();
                TrainInformationRequest request = TrainInformation(objBruc, 1);


                #region Allow Trenitalia search for specifc site
                if (objBruc.OneHubServiceName == "Trenitalia")
                {
                    List<string> listHostForTI = new List<string> { "www.1tracktest.com", "demo.internationalrail.net", "localhost" };
                    bool IsTrenItaliaSearchAllow = listHostForTI.Contains(Request.Url.Host.Trim().ToLower());
                    if (!IsTrenItaliaSearchAllow)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "xxx", "ParentResult();", true);
                        return;
                    }
                }
                #endregion

                if (Convert.ToInt32(rdBookingType.SelectedValue) > 0 && Convert.ToInt32(objBruc.depRCode) > 0)
                    request.IsReturnJourney = false;
                else if (Convert.ToInt32(rdBookingType.SelectedValue) > 0)
                    request.IsReturnJourney = true;

                ApiLogin(request);
                TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);

                if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
                {
                    //--TreniItalia Search return request                
                    if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                    {
                        List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                        request = TrainInformation(objBruc, 2);
                        request.IsReturnJourney = true;
                        ApiLogin(request);
                        TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                        List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                        list.AddRange(listReturn);
                        if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                        {
                            List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                            List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                            List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                            pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();
                        }
                    }
                }

                Session["TrainSearch"] = pInfoSolutionsResponse;
            }
            else
                Session["TrainSearch"] = null;
        }
        catch (Exception ex)
        {
            if (!chkIsLoyaltyActive.Checked && !ex.Message.Contains("Thread"))
            {
                Session["ErrorMessage"] = ex.Message;
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "msg", "ParentResult();", true);
            }
        }
    }

    private void ApiLogin(TrainInformationRequest request)
    {
        #region API Account
        try
        {
            var mngUser = new ManageUser();
            var api = mngUser.GetApiLogingDetail(_siteId).Where(x => x.IsActive == 1).ToList();
            if (api != null)
            {
                var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
                var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

                request.Header.ApiAccount = new ApiAccountDetail
                    {
                        BeNeApiId = result != null ? result.ID : 0,
                        TiApiId = resultTI != null ? resultTI.ID : 0,
                    };
            }
        }
        catch
        {
            throw;
        }

        #endregion
    }

    int GetEurostarBookingDays(BookingRequestUserControl request)
    {
        try
        {
            List<string> listStCode = new List<string> { "GBSPX", "BEBMI", "FRPNO", "FRPAR", "GBASI", "FRLIL", "FRLLE", "FRMLV", "GBEBF" };
            if (listStCode.Contains(request.depstCode) && listStCode.Contains(request.arrstCode))
                return 180;
            else
                return 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void GetRequest(BookingRequestUserControl request)
    {
        try
        {
            var objrequest = new TrainInformationRequest
            {
                Header = new Header
                {
                    onehubusername = "#@dots!squares",
                    onehubpassword = "#@dots!squares",
                    unitofwork = 0,
                    language = Language.nl_BE,
                },
                DepartureRailwayCode = request.depRCode,
                DepartureStationCode = request.depstCode,
                ArrivalRailwayCode = request.arrRCode,
                ArrivalStationCode = request.arrstCode,
                Class = request.ClassValue,
                NumAdults = request.Adults,
                NumBoys = request.Boys,
                NumSeniors = request.Seniors,
                NumYouths = request.Youths,
                NumberOfTransfare = request.Transfare,
                IsHaveRailPass = request.isIhaveRailPass,
                IsReturnJourney = request.IsReturnJurney,
                DepartureDate = DateTime.Parse(txtDepartureDate.Text, new CultureInfo("en-CA")),
                DepartureTime = Convert.ToDateTime(ddldepTime.SelectedValue),
                RailName = request.OneHubServiceName == "Trenitalia" ? "ITALIA" : request.OneHubServiceName
            };

            if (!string.IsNullOrEmpty(txtReturnDate.Text) && !txtReturnDate.Text.Contains("DD"))
            {
                objrequest.ArrivalDate = DateTime.Parse(txtReturnDate.Text, new CultureInfo("en-CA"));
                objrequest.ArrivalTime = Convert.ToDateTime(ddlReturnTime.SelectedValue);
            }
            Session["TrainSearchRequest"] = objrequest;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public TrainInformationRequest TrainInformation(BookingRequestUserControl request, int flag)
    {
        try
        {
            GetRequest(request);
            var objrequest = new TrainInformationRequest
              {
                  Header = new Header
                  {
                      onehubusername = "#@dots!squares",
                      onehubpassword = "#@dots!squares",
                      unitofwork = 0,
                      language = Language.nl_BE,
                  },
                  DepartureRailwayCode = flag == 1 ? request.depRCode : request.arrRCode,
                  DepartureStationCode = flag == 1 ? request.depstCode : request.arrstCode,
                  ArrivalRailwayCode = flag == 1 ? request.arrRCode : request.depRCode,
                  ArrivalStationCode = flag == 1 ? request.arrstCode : request.depstCode,

                  Class = request.ClassValue,
                  NumAdults = request.Adults,
                  NumBoys = request.Boys,
                  NumSeniors = request.Seniors,
                  NumYouths = request.Youths,
                  NumberOfTransfare = request.Transfare,
                  IsHaveRailPass = request.isIhaveRailPass,
                  IsReturnJourney = request.IsReturnJurney,
                  RailName = request.OneHubServiceName == "Trenitalia" ? "ITALIA" : request.OneHubServiceName
              };

            //--Departure IF BENE
            objrequest.DepartureDate = request.depdt;
            objrequest.DepartureTime = request.depTime;
            if (request.ReturnDate != string.Empty && flag == 1)
            {
                objrequest.IsReturnJourney = true;
                objrequest.ArrivalDate = DateTime.Parse(request.ReturnDate, new CultureInfo("en-CA"));
                objrequest.ArrivalTime = Convert.ToDateTime(request.ReturnTime);
            }

            //-Return For TI
            if (request.ReturnDate != string.Empty && flag == 2)
            {
                objrequest.IsReturnJourney = true;
                objrequest.DepartureDate = DateTime.Parse(request.ReturnDate, new CultureInfo("en-CA"));
                objrequest.DepartureTime = Convert.ToDateTime(request.ReturnTime);
            }
            return objrequest;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            _siteId = Guid.Parse(hdnSiteid.Value);
            Session["siteId"] = _siteId;
            #region
            #region If P2P Widget is not allowed for this site then searching will not happing
            bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(_siteId);
            if (!IsVisibleP2PWidget)
                return;
            #endregion

            if (!string.IsNullOrEmpty(txtReturnDate.Text))
                if (!txtReturnDate.Text.Contains("DD/MM/YYYY"))
                    if (Convert.ToDateTime(txtDepartureDate.Text).Date > Convert.ToDateTime(txtReturnDate.Text).Date)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "callalert", "alert('Return date should be greater than the Depart date')", true);
                        rdBookingType_SelectedIndexChanged(sender, e);
                        return;
                    }

            var stList = _db.StationNameLists.Where(x => (x.StationEnglishName == txtFrom.Text.Trim() || x.StationEnglishName == txtTo.Text.Trim()) && x.IsUK).FirstOrDefault();
            var isUK = stList != null && stList.IsUK;

            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue == "0")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCa1l", "alert('Please enter at least 1 adult, senior or junior(youth) passenger.')", true);
                return;
            }
            if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue != "0" && isUK)
            {
                mdPassengerUK.Show();
                return;
            }
            else if (ddlAdult.SelectedValue == "0" && ddlYouth.SelectedValue == "0" && ddlSenior.SelectedValue == "0" && ddlChild.SelectedValue != "0" && !isUK)
            {
                mdPassenger.Show();
                return;
            }

            int totalAdult = Convert.ToInt32(ddlAdult.SelectedValue) * 4;
            int totalYouth = Convert.ToInt32(ddlYouth.SelectedValue) * 4;
            int totalSenior = Convert.ToInt32(ddlSenior.SelectedValue) * 4;
            int totalChilden = totalAdult + totalYouth + totalSenior;
            if (Convert.ToInt32(ddlChild.SelectedValue) > totalChilden && isUK)
            {
                mdPassengerUK.Show();
                return;
            }
            else if (Convert.ToInt32(ddlChild.SelectedValue) > totalChilden && !isUK)
            {
                mdPassenger.Show();
                return;
            }
            SearchTrainInfo();
            string str = "BE";
            if (ViewState["depRCode"] != null)
                str = ViewState["depRCode"].ToString() == "0" ? "BE" : "TI";

            #endregion

            if (chkIsLoyaltyActive.Checked)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "msg", "ParentLoyalty();", true);
            }
            else if (chkIhaveRailPass.Checked)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "xxx", "ParentResult();", true);
            }
            else
            {
                ViewState["UrlP2PResult"] = ViewState["UrlP2PResult"].ToString() + "?req=" + str;
                string result = ViewState["UrlP2PResult"].ToString();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "msg", "ParentSearchResult('" + result + "');", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (rdBookingType.SelectedValue == "0")
            {
                txtReturnDate.Enabled = false;
                txtReturnDate.Text = "";
                ddlReturnTime.Enabled = false;
                reqReturnDate.Enabled = false;
                regReturnDate.Enabled = false;

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "caldisable()", true);
            }
            else
            {
                txtReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                reqReturnDate.Enabled = true;
                regReturnDate.Enabled = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "cal", "calenable()", true);
            }
            ViewState["Rdo"] = rdBookingType.SelectedValue;
            SetCallander();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void SearchTrainInfoForFill()
    {
        try
        {
            BookingRequestUserControl objBRUC;
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            rdBookingType.SelectedValue = objBRUC.Journeytype == null ? "0" : objBRUC.Journeytype;
            if (rdBookingType.SelectedValue == "0")
            {
                txtReturnDate.Text = "";
                txtReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                reqReturnDate.Enabled = false;
            }
            else
            {
                txtReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                reqReturnDate.Enabled = true;
                txtReturnDate.Text = objBRUC.ReturnDate;
            }
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlYouth.SelectedValue = objBRUC.Youths.ToString();
            ddlSenior.SelectedValue = objBRUC.Seniors.ToString();
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            chkIsLoyaltyActive.Checked = objBRUC.Loyalty;
            MaxTransfer = objBRUC.Transfare;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}