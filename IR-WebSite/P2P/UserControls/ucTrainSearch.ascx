﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTrainSearch.ascx.cs"
    Inherits="OtherSiteP2PBooking_ucTrainSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    form
    {
        margin: 0px 0 5px 0;
    }
    #ui-datepicker-div
    {
        width: 250px !important;
        left: 10px !important;
    }
    .tooltip
    {
        display: block !important;
    }
    .loading
    {
        background-image: url(images/loading3.gif);
        background-position: right;
        background-repeat: no-repeat;
    }
    .modalBackground
    {
        position: fixed;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        overflow: hidden;
        padding: 0;
        margin: 0;
        background-color: #000;
        filter: alpha(opacity=50);
        opacity: 0.5;
        text-align: center;
        float: left;
    }
    .progessposition
    {
        padding-top: 20%;
    }
    .clsFont
    {
        font-size: 13px;
        color: #4D4D4D !important;
    }
    
    .modal-popup
    {
        left: 21px !important;
    }
    #Div_mdPassengerUK
    {
        left: -1px !important;
        top: -210px !important;
        width: 250px !important;
    }
    #Div_mdPassenger
    {
        left: -1px !important;
        top: -50px !important;
        width: 250px !important;
    }
</style>
<script type="text/javascript">
    function ParentResult() {
        var dataurl = ('<%=ViewState["UrlP2PResult"] %>');
        window.parent.location = dataurl;
    }
    function ParentLoyalty() {
        var dataurl = ('<%=ViewState["UrlLoyaltyCard"] %>');
        window.parent.location = dataurl;
    }
    function ParentSearchResult(url) {
        var dataurl = url;
        window.parent.location = dataurl;
    }
</script>
<asp:UpdatePanel ID="updSearchUC" runat="server">
    <ContentTemplate>
        <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="List" ValidationGroup="vgs"
            ShowMessageBox="true" ShowSummary="false" runat="server" />
        <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
        <div id="divSearch" runat="server" class="tab-detail-in" style="position: relative;">
            <div class="full-row">
                <div class="colum01">
                    <strong>From</strong>
                    <asp:RequiredFieldValidator ID="reqFrom" runat="server" ForeColor="red" ValidationGroup="vgs"
                        Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                        Text="*" CssClass="font14" />
                </div>
                <div class="colum02">
                    <asp:TextBox ID="txtFrom" runat="server" ClientIDMode="Static" onkeyup="selectpopup(this)"
                        autocomplete="off" /><span id="spantxtFrom" style="display: none"></span>
                    <asp:HiddenField ID="hdnFrm" runat="server" />
                </div>
            </div>
            <div class="full-row">
                <div class="colum01">
                    <strong>To</strong>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="red"
                        ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                        Text="*" CssClass="font14" />
                </div>
                <div class="colum02">
                    <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" ClientIDMode="Static"
                        autocomplete="off" /><span id="spantxtTo" style="display: none"></span>
                </div>
            </div>
            <div class="full-row">
                <div class="colum01">
                    <strong>Journey</strong>
                </div>
                <div class="colum02" style="font-weight: normal;">
                    <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vg" RepeatDirection="Horizontal"
                        AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                        <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                        <asp:ListItem Value="1">Return</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <%-- <div class="gray-row">--%>
            <div class="full-row depart-cal">
                <div class="colum01">
                    <strong>Depart</strong>
                    <asp:RequiredFieldValidator ID="reqDepartureDate" runat="server" ForeColor="red"
                        ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtDepartureDate"
                        ErrorMessage="Please enter departure date." Text="*" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="red"
                        ErrorMessage="Please enter departure date." Text="*" ValidationGroup="vgs" Display="Dynamic"
                        InitialValue="DD/MM/YYYY" ControlToValidate="txtDepartureDate" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDepartureDate"
                        ValidationExpression="^((([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4})|(DD/MM/YYYY)$"
                        Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                        ValidationGroup="vgs">*</asp:RegularExpressionValidator>
                </div>
                <div class="float-lt" style="width: 165px; font-weight: normal;">
                    <asp:TextBox ID="txtDepartureDate" ClientIDMode="Static" Text="DD/MM/YYYY" runat="server"
                        class="inbox01 mar-right" />
                    <span class="imgCal calIconSmall" title="Select DepartureDate." style="float: left;
                        margin-right: 3px; margin-top: 2px; cursor: default; width: 20px; height: 21px;">
                    </span>
                    <asp:DropDownList ID="ddldepTime" runat="server" class="slbox01 mar-right-n">
                        <asp:ListItem>00:00</asp:ListItem>
                        <asp:ListItem>01:00</asp:ListItem>
                        <asp:ListItem>02:00</asp:ListItem>
                        <asp:ListItem>03:00</asp:ListItem>
                        <asp:ListItem>04:00</asp:ListItem>
                        <asp:ListItem>05:00</asp:ListItem>
                        <asp:ListItem>06:00</asp:ListItem>
                        <asp:ListItem>07:00</asp:ListItem>
                        <asp:ListItem>08:00</asp:ListItem>
                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                        <asp:ListItem>10:00</asp:ListItem>
                        <asp:ListItem>11:00</asp:ListItem>
                        <asp:ListItem>12:00</asp:ListItem>
                        <asp:ListItem>13:00</asp:ListItem>
                        <asp:ListItem>14:00</asp:ListItem>
                        <asp:ListItem>15:00</asp:ListItem>
                        <asp:ListItem>16:00</asp:ListItem>
                        <asp:ListItem>17:00</asp:ListItem>
                        <asp:ListItem>18:00</asp:ListItem>
                        <asp:ListItem>19:00</asp:ListItem>
                        <asp:ListItem>20:00</asp:ListItem>
                        <asp:ListItem>21:00</asp:ListItem>
                        <asp:ListItem>22:00</asp:ListItem>
                        <asp:ListItem>23:00</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="returndiv" runat="server" class="full-row depart-cal">
                <div class="colum01">
                    <strong>Return</strong>
                    <asp:RequiredFieldValidator ID="reqReturnDate" runat="server" ForeColor="red" ValidationGroup="vgs"
                        Display="Dynamic" ControlToValidate="txtReturnDate" ErrorMessage="Please enter return date."
                        Text="*" Enabled="False" />
                    <asp:RegularExpressionValidator ID="regReturnDate" runat="server" ControlToValidate="txtReturnDate"
                        ValidationExpression="^((([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4})|(DD/MM/YYYY)$"
                        Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid return date"
                        ValidationGroup="vgs" Enabled="False">*</asp:RegularExpressionValidator>
                </div>
                <div class="float-lt tdisabled" style="width: 165px; font-weight: normal;">
                    <asp:TextBox ID="txtReturnDate" runat="server" Text="DD/MM/YYYY" class="mar-right"
                        ClientIDMode="Static" Enabled="false" />
                    <span class="imgCal1 calIconSmall" title="Select Return Date." style="float: left;
                        margin-right: 3px; margin-top: 2px; cursor: default; width: 20px; height: 21px;">
                    </span>
                    <asp:DropDownList ID="ddlReturnTime" runat="server" class="slbox01 mar-right-n" Enabled="false">
                        <asp:ListItem>00:00</asp:ListItem>
                        <asp:ListItem>01:00</asp:ListItem>
                        <asp:ListItem>02:00</asp:ListItem>
                        <asp:ListItem>03:00</asp:ListItem>
                        <asp:ListItem>04:00</asp:ListItem>
                        <asp:ListItem>05:00</asp:ListItem>
                        <asp:ListItem>06:00</asp:ListItem>
                        <asp:ListItem>07:00</asp:ListItem>
                        <asp:ListItem>08:00</asp:ListItem>
                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                        <asp:ListItem>10:00</asp:ListItem>
                        <asp:ListItem>11:00</asp:ListItem>
                        <asp:ListItem>12:00</asp:ListItem>
                        <asp:ListItem>13:00</asp:ListItem>
                        <asp:ListItem>14:00</asp:ListItem>
                        <asp:ListItem>15:00</asp:ListItem>
                        <asp:ListItem>16:00</asp:ListItem>
                        <asp:ListItem>17:00</asp:ListItem>
                        <asp:ListItem>18:00</asp:ListItem>
                        <asp:ListItem>19:00</asp:ListItem>
                        <asp:ListItem>20:00</asp:ListItem>
                        <asp:ListItem>21:00</asp:ListItem>
                        <asp:ListItem>22:00</asp:ListItem>
                        <asp:ListItem>23:00</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <%-- </div>--%>
            <div class="full-row" style="height: 80px; padding-left: 6px;">
                <div class="colum02" style="width: 256px;">
                    <div class="float-lt" style="width: 64px; float: left;">
                        <strong>Adults </strong>
                        <br />
                        <div style="width: 50px; font-weight: normal;">
                            <asp:DropDownList CssClass="slbox-sml" ID="ddlAdult" runat="server">
                            </asp:DropDownList>
                            <br />
                            <span style="font-size: 10px; font-weight: normal;">(12+ yrs)</span></div>
                        <span>&nbsp; </span>
                    </div>
                    <div class="float-lt" style="width: 64px; float: left; font-weight: normal;">
                        <strong>Children </strong>
                        <br />
                        <div style="width: 50px; font-weight: normal;">
                            <asp:DropDownList CssClass="slbox-sml" ID="ddlChild" runat="server">
                            </asp:DropDownList>
                            <br />
                            <span style="font-size: 8.5px; font-weight: normal;"><a target=' _blank' href="http://www.internationalrail.com.au/information/age-requirements-for-discount-fares.aspx"
                                style="color: #f68428;">(Child Ages?)</a></span>
                        </div>
                        <span>&nbsp; </span>
                    </div>
                    <div class="float-lt" style="width: 64px; float: left; font-weight: normal;">
                        <strong>Youths </strong>
                        <div style="width: 55px; font-weight: normal;">
                            <asp:DropDownList CssClass="slbox-sml" ID="ddlYouth" runat="server">
                            </asp:DropDownList>
                            <br />
                            <span style="font-size: 10px; font-weight: normal;">(12-25 yrs)</span>
                        </div>
                        <span>&nbsp; </span>
                    </div>
                    <div class="float-lt" style="width: 64px; float: left; font-weight: normal;">
                        <strong>Seniors </strong>
                        <div style="width: 50px;">
                            <asp:DropDownList CssClass="slbox-sml" ID="ddlSenior" runat="server">
                            </asp:DropDownList>
                            <br />
                            <span style="font-size: 10px; font-weight: normal;">(60+ yrs)</span></div>
                        <span>&nbsp; </span>
                    </div>
                </div>
            </div>
            <div class="full-row">
                <div class="colum01" style="line-height: 12px; width: 88px; padding-top: 5px;">
                    <strong>Class of service </strong>
                </div>
                <div class="colum02" style="width: 145px; font-weight: normal;">
                    <asp:RadioButtonList ID="ddlClass" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                        <asp:ListItem Value="1">1st</asp:ListItem>
                        <asp:ListItem Value="2">2nd</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="full-row">
                <div class="colum01" style="width: 120px!important; display: none;">
                    <asp:CheckBox ID="chkIsLoyaltyActive" runat="server" />
                    <strong>Loyalty cards </strong>
                </div>
              
            </div>
           
            <div class="full-row">
              <div id="divRailPass" class="colum01" style="width: 125px!important;" runat="server">
                    <asp:CheckBox ID="chkIhaveRailPass" runat="server" />
                    <strong>I have a rail pass </strong>
                </div>
                <asp:Button ID="btnSubmit" Style="display: block;" runat="server" class="w92 f-right"
                    ValidationGroup="vgs" Text="SEARCH" OnClick="btnSubmit_Click" />
            </div>
            <div class="swap-arr" onclick="btnswapstation()" title="Swap">
                <div class="swap-img">
                    <img src="../images/swap-arr.png" alt="" title="Swap" width="10" height="45" />
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <asp:HiddenField ID="hdnForModelUK" runat="server" />
        <asp:HiddenField ID="hdnForModel" runat="server" />
        <asp:ModalPopupExtender ID="mdPassengerUK" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlPassengerUK" BackgroundCssClass="modalBackground" TargetControlID="hdnForModelUK" />
        <asp:Panel ID="pnlPassengerUK" runat="server" CssClass="modal-popup" Style="display: none;">
            <div class="popup-inner" id="Div_mdPassengerUK">
                <div class="title">
                    Please select passenger type
                </div>
                <p>
                    Please enter at least 1 adult, senior or junior(youth) passenger.</p>
                <p>
                    Children cannot travel unattended.</p>
                <p>
                    There is a limitation for accompanied children:</p>
                <p>
                    - Max. 4 children per passenger allowed.</p>
                <p>
                    - On Eurostar, the number of children under 4 travelling for free is limited to
                    1 child per accompanying adult.</p>
                <div>
                    <asp:Button ID="btnCloseWin" runat="server" Text="Close" CssClass="btn-black newbutton"
                        Style="float: right; padding-right: 46px !important; width: 20% !important;" />
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <asp:ModalPopupExtender ID="mdPassenger" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlPassenger" BackgroundCssClass="modalBackground" TargetControlID="hdnForModel" />
        <asp:Panel ID="pnlPassenger" runat="server" CssClass="modal-popup" Style="display: none;">
            <div class="popup-inner" id="Div_mdPassenger">
                <div class="title">
                    Please select passenger type
                </div>
                <p>
                    Please enter at least 1 adult, senior or junior(youth) passenger.</p>
                <p>
                    Children cannot travel unattended.</p>
                <p>
                    There is a limitation for accompanied children:</p>
                <p>
                    - Max. 4 children per passenger allowed.</p>
                <div>
                    <asp:Button ID="Button1" runat="server" Text="Close" CssClass="btn-black newbutton"
                        Style="float: right; padding-right: 46px !important; width: 20% !important;" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updSearchUC"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="overlaybg">
            &nbsp;</div>
        <div class="progess-inner2">
            <strong>One Moment Please </strong>
            <img src="../images/dots32.gif" alt="progress bar" />
            <span>We are searching for your trains</span>
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
<asp:HiddenField runat="server" ID="hdnSiteid" ClientIDMode="Static" />
<%--<asp:HiddenField ID="hdnParentUrl" runat="server" Value="http://localhost/P2P/HTMLPage2.htm?req=BE" />--%>
<asp:HiddenField ID="hdnParentUrl" runat="server" Value="http://www.internationalrail.com.au/p2p-search-results.aspx?req=BE" />
<script type="text/jscript">
    $(document).ready(function () {
        $("#ContentPlaceHolder1_ucTrainSearch_chkIsLoyaltyActive").change(function () {
            if ($("#ContentPlaceHolder1_ucTrainSearch_chkIsLoyaltyActive").prop("checked")) {
                $("#ucTrainSearch_chkIhaveRailPass").prop("checked", false);
               
            }
            
        });

        $("#ucTrainSearch_chkIhaveRailPass").change(function () {
            if ($("#ucTrainSearch_chkIhaveRailPass").prop("checked")) {
             
                $("#ContentPlaceHolder1_ucTrainSearch_chkIsLoyaltyActive").prop("checked", false);
            }
        });
    });

    function searchEvent() {


    }
    function ShowWarrningPopup() {
        $(document).ready(function () {
            var txtfrom = $("#ContentPlaceHolder1_ucTrainSearch_ddldepTime option:selected").text();
            var txtTo = $("#ContentPlaceHolder1_ucTrainSearch_ddlReturnTime option:selected").text();
            var datefrm = $("#ucTrainSearch_txtDepartureDate").val();
            var datearray = datefrm.split('/');
            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            var FromDate = new Date(newdate);
            $("#ucTrainSearch_lblFrom").text("Departure " + txtfrom);
            $("#ucTrainSearch_lblTo").text("Arrival " + txtTo);
            $("#ucTrainSearch_lblDateTime").text(FromDate.customFormat("#DDDD# #MMMM# #DD#, #YYYY#"));
        });
    }

    Date.prototype.customFormat = function (formatString) {
        var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
        var dateObject = this;
        YY = ((YYYY = dateObject.getFullYear()) + "").slice(-2);
        MM = (M = dateObject.getMonth() + 1) < 10 ? ('0' + M) : M;
        MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
        DD = (D = dateObject.getDate()) < 10 ? ('0' + D) : D;
        DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dateObject.getDay()]).substring(0, 3);
        th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st' : (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
        formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY).replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM).replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD).replace("#DD#", DD).replace("#D#", D).replace("#th#", th);

        h = (hhh = dateObject.getHours());
        if (h == 0) h = 24;
        if (h > 12) h -= 12;
        hh = h < 10 ? ('0' + h) : h;
        AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
        mm = (m = dateObject.getMinutes()) < 10 ? ('0' + m) : m;
        ss = (s = dateObject.getSeconds()) < 10 ? ('0' + s) : s;
        return formatString.replace("#hhh#", hhh).replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m).replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm).replace("#AMPM#", AMPM);
    }
</script>
<script type="text/javascript">
    function btnswapstation() {
        var spantxtFrom = '';
        var txtFrom = '';
        spantxtFrom = $("#spantxtFrom").text();
        $("#spantxtFrom").text($("#spantxtTo").text());
        $("#spantxtTo").text(spantxtFrom);
        txtFrom = $("#txtFrom").val();
        $("#txtFrom").val($("#txtTo").val());
        $("#txtTo").val(txtFrom);
    }
    
</script>
<script type="text/javascript">

    var count = 0;
    var countKey = 1;
    var conditionone = 0;
    var conditiontwo = 0;
    var tabkey = 0;

    $(function () {
        $("#txtFrom").focus();
        if ($("#txtFrom").val() != '') {
            $('#spantxtTo').text($("#spantxtTo"));
        }
        if ($("#txtTo").val() != '') {
            $('#spantxtFrom').text($("#spantxtFrom"));
        }

        $(window).click(function (event) {
            $('#_bindDivData').remove();
        });
        $(window).keydown(function (event) {
            if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                tabkey = 0;
                event.preventDefault();
                return false;
            }
            tabkey = 1;
        });
        $("#txtFrom , #txtTo").on('keydown', function (event) {
            //40,38
            var $id = $(this);
            var maxno = 0;
            count = event.keyCode;

            $(".popupselect").each(function () { maxno++; });
            if (count == 13 || count == 9) {
                $(".popupselect").each(function () {
                    if ($(this).attr('style') != undefined) {
                        $(this).trigger('onclick');
                        $id.val($.trim($(this).find('span').text()));
                    }
                });
                $('#_bindDivData').remove();

                countKey = 1;
                conditionone = 0;
                conditiontwo = 0;
            }
            else if (count == 40 && maxno > 1) {
                conditionone = 1;
                if (countKey == maxno)
                    countKey = 0;
                if (conditiontwo == 1) {
                    countKey++;
                    conditiontwo = 0;
                }
                $(".popupselect").removeAttr('style');
                $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                countKey++;
            }
            else if (count == 38 && maxno > 1) {
                conditiontwo = 1;
                if (countKey == 0)
                    countKey = maxno;
                if (conditionone == 1) {
                    countKey--;
                    conditionone = 0;
                }
                countKey--;
                $(".popupselect").removeAttr('style');
                $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
            }
            else {
                countKey = 1;
                conditionone = 0;
                conditiontwo = 0;
            }
            $id.focus();
        });
    });
    function selectpopup(e) {
        if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
            var StationType;
            if ($(e).attr('id') == "txtFrom")
                StationType = "from";
            else
                StationType = "to";
            var $this = $(e);
            var data = $this.val();

            var station = '';
            var hostName = window.location.host;
            var url = "http://" + hostName;

            if (window.location.toString().indexOf("https:") >= 0)
                url = "https://" + hostName;

            if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                $('#spantxtFrom').text('');
                $('#spantxtTo').text('');
            }

            var filter = $("#span" + $this.attr('id') + "").text();
            if (filter == "" && $this.val() != "")
                filter = $("#hdnFilter").val();
            $("#hdnFilter").val(filter);

            if ($this.attr('id') == 'txtTo') {
                station = $("#txtFrom").val();
            }
            else {
                station = $("#txtTo").val();
            }

            if (hostName == "localhost") {
                url = url + "/InterRail/P2P";
            }

            var hostUrl = url + "/StationList.asmx/getStationsXList";
            data = data.replace(/[']/g, "♥");
            var datalength = data;
            data = data + '*' + StationType + '*' + $("#hdnSiteid").val();
            var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
            $.ajax({
                type: "POST",
                url: hostUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                success: function (msg) {
                    $('#_bindDivData').remove();
                    var lentxt = datalength.length;
                    $.each(msg.d, function (key, value) {
                        var splitdata = value.split('ñ');
                        var lenfull = splitdata[0].length; ;
                        var txtupper = splitdata[0].substring(0, lentxt);
                        var txtlover = splitdata[0].substring(lentxt, lenfull);
                        $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                    });
                    $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                },
                error: function () {
                    //                    alert("Wait...");
                }
            });
        }
    }
    function _removehoverclass(e) {
        $(".popupselect").hover(function () {
            $(".popupselect").removeAttr('style');
            $(this).attr('style', 'background-color: #ccc !important');
            countKey = $(this).index();
        });
    }
    function _Bindthisvalue(e) {
        var idtxtbox = $('#_bindDivData').prev("input").attr('id');
        $("#" + idtxtbox + "").val($(e).find('span').text());
        if (idtxtbox == 'txtTo') {
            $('#spantxtFrom').text($(e).find('div').text());
            $("#spantxtFrom", $(e).find('div').text());
        }
        else {
            $('#spantxtTo').text($(e).find('div').text());
            $("#spantxtTo", $(e).find('div').text());
        }
        $('#_bindDivData').remove();
    }

    function checkDate(sender) {
        var selectedDate = new Date(sender._selectedDate);
        var today = new Date();
        today.setHours(0, 0, 0, 0);

        if (selectedDate < today) {
            alert('Select a date sometime in the future!');
            sender._selectedDate = new Date();
            sender._textbox.set_Value(sender._selectedDate.format(sender._format));
        }
    }
    function CheckDateVal(sender, args) {

        if ($.trim(document.getElementById("txtDepartureDate").value).toString() != "DD/MM/YYYY") {
            var startDate = new Date(document.getElementById("txtDepartureDate").value);
            var finishDate = new Date(document.getElementById("txtReturnDate").value);

            if (startDate > finishDate) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
        else {
            args.IsValid = true;
        }
    }
    function OnClientPopulating(sender, e) {
        sender._element.className = "loading";
    }
    var unavailableDates = '<%=UnavailableDates1 %>';

    function nationalDays(date) {
        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

        if ($.inArray(dmy, unavailableDates) > -1) {
            return [false, "", "Unavailable"];
        }
        return [true, ""];
    }
    $(document).ready(function () {
        LoadCal();
    });
    function LoadCal() {
        $("#txtDepartureDate, #txtReturnDate").bind("contextmenu cut copy paste", function (e) {
            return false;
        });

        $("#txtDepartureDate").datepicker(
        {
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: '<%=MinDate %>',
            onClose: function (selectedDate) {
                $("#txtReturnDate").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtReturnDate").datepicker({
            numberOfMonths: 1,
            dateFormat: 'dd/M/yy',
            beforeShowDay: nationalDays,
            showButtonPanel: true,
            firstDay: 1,
            minDate: 0
        });


        $("#txtReturnDate").datepicker("option", "minDate", $("#txtDepartureDate").datepicker('getDate'));

        $("#txtDepartureDate, #txtReturnDate").keypress(function (event) { event.preventDefault(); });

        $(".imgCal").click(function () {
            $("#txtDepartureDate").datepicker('show');
        });

        $(".imgCal1").click(function () {
            if ($('#ContentPlaceHolder1_ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('show');
            }
        });
    }
    function calenable() {
        LoadCal();
    }
    function caldisable() {
        LoadCal();
    }

</script>
