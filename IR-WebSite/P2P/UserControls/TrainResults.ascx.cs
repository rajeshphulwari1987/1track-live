﻿#region Using
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Business;
using OneHubServiceRef;
#endregion

public partial class P2P_UserControls_TrainResults : System.Web.UI.UserControl
{
    private readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static string unavailableDates1 = "";
    private Guid _siteId;
    BookingRequestUserControl objBRUC;
    public string script = "<script></script>";
    public string siteURL;
    private string htmfile = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }
    #region PageLoad Events
    protected void Page_Load(object sender, EventArgs e)
    {
        _siteId = Guid.Parse(Session["siteId"].ToString());
        hdnSiteid.Value = _siteId.ToString();
        siteURL = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        ShowHaveRailPass(_siteId);
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal1", "LoadCal1();", true);
        if (!Page.IsPostBack)
        {
            errorMsg.Visible = Request.QueryString["req"] != null;
            for (int j = 10; j >= 0; j--)
            {
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }

            FillPageInfo();
            if (Page.RouteData.Values["PageId"] != null)
            {
                var pageID = (Guid)Page.RouteData.Values["PageId"];
                PageContent(pageID, _siteId);
                Page.Header.DataBind();
            }
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
            unavailableDates1 = "[";
            if (siteDDates.Any())
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
            QubitOperationLoad();
            ucSResult.Visible = Session["TrainSearch"] != null;
            IfAgentUserIsLoging();
            IfSiteIsAgent(_siteId);
        }
    }
    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        divRailPass.Visible = railPass;
    }
    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    #endregion

    void IfSiteIsAgent(Guid siteID)
    {
        var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteID);
        if (objsite != null && objsite.IsAgent == false)
            lblNm.Text = "Name";
    }
    void IfAgentUserIsLoging()
    {
        if (Session["AgentUserID"] == null)
            return;

        ManageUser _ManageUser = new ManageUser();
        Guid IDuser = Guid.Parse(Session["AgentUserID"].ToString());
        tblAdminUser objUser = _ManageUser.AgentNameEmailById(IDuser);
        if (objUser != null)
        {
            txtName.Text = objUser.Forename + " " + objUser.Surname;
            txtEmailAddress.Text = objUser.EmailAddress;
        }
    }

    #region Control Events
    protected void Tab1_Click(object sender, EventArgs e)
    {
        //MainView.ActiveViewIndex = 0;
    }

    protected void Tab2_Click(object sender, EventArgs e)
    {
        //MainView.ActiveViewIndex = 1;
    }

    #endregion

    #region UserDefined function
    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _master.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');

                List<int> idList =
                    (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _master.GetBannerImgByID(idList);
                rptBanner.DataSource = list;
                rptBanner.DataBind();
                footerBlock.InnerHtml = oPage.FooterImg;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri,
                                 ex.Message + "; Inner Exception:" +
                                 (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    #endregion

    protected void btnSendInfo_Click(object sender, EventArgs e)
    {
        try
        {
            var obj = new TrainResultField
            {
                adult = Convert.ToInt32(ddlAdult.SelectedValue),
                apiName = Request.QueryString["req"] == "BE" ? "BENE" : "ITALIA",
                child = Convert.ToInt32(ddlChild.SelectedValue),
                Class = ddlClass.SelectedValue,
                dateOfDepart = Convert.ToDateTime(txtDepartureDate.Text),
                departureTime = ddldepTime.SelectedValue,

                email = txtEmailAddress.Text,
                from = txtFrom.Text,
                ipAddress = Request.ServerVariables["REMOTE_ADDR"],
                FName = txtName.Text,
                phone = txtPhone.Text,
                senior = Convert.ToInt32(ddlSenior.SelectedValue),
                siteID = _siteId,
                to = txtTo.Text,
                youth = Convert.ToInt32(ddlYouth.SelectedValue),
                FIPNumber = string.Empty,
                DeptTNo = string.Empty,
                ReturnTNo = string.Empty,
                Notes = string.Empty,
                IsReturn = rdBookingType.SelectedValue == "1"
            };
            if (txtTrainReturnDate.Text.Trim() != "" && txtTrainReturnDate.Text.Trim() != "DD/MM/YYYY")
            {
                obj.dateOfArrival = Convert.ToDateTime(txtTrainReturnDate.Text);
                obj.arrivalTime = ddlReturnTime.SelectedValue;
            }
            else
            {
                obj.dateOfArrival = Convert.ToDateTime(txtDepartureDate.Text);
                obj.arrivalTime = ddldepTime.SelectedValue;
            }
            var resul = new ManageJourneyRequest().AddTrainResultInfo(obj);
            succmessage.Text = "Thank you, your request ID is " + resul + ", please make a note of this and quote it whenever you're in contact with us.  One of our rail experts will be in touch soon!";
            DivLeftOne.Style.Add("display", "none");
            DivLeftSecond.Style.Add("display", "block");
        }
        catch (Exception ex)
        {
        }

    }

    protected void chkLoyalty_CheckedChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            chkIhaveRailPass.Checked = false;
            ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "Validationxx()", true);
        }
    }

    protected void chkIhaveRailPass_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIhaveRailPass.Checked)
        {
            chkLoyalty.Checked = false;
            ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "Validationxx()", true);
        }
    }

    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdBookingType.SelectedValue == "0")
        {
            txtTrainReturnDate.Enabled = ddlReturnTime.Enabled = rfReturnDate.Enabled = returnspan.Visible = false;
            txtTrainReturnDate.Text = "";
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "caldisableT()", true);
            ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "Validationxx()", true);
        }
        else
        {
            txtTrainReturnDate.Enabled = ddlReturnTime.Enabled = rfReturnDate.Enabled = returnspan.Visible = true;
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "calenableT()", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
        }
    }

    public void FillPageInfo()
    {
        Boolean showBox = true;
        if (Session["ErrorMessage"] != null)
        {
            if (Session["ErrorMessage"].ToString().Trim() == "ErrorMaxDate")
            {
                showBox = false;
            }
        }

        /*Future dates msg end*/
        var pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
        if (pInfoSolutionsResponse == null || pInfoSolutionsResponse.ErrorMessage != null)
        {
            if (showBox)
            {
                lblHeading.Text = "Journey Request Form";
                pnlJourneyInfo.Visible = true;
                lblMessageEarlierTrain.Visible = false;
            }
            else
            {
                pnlJourneyInfo.Visible = false;
                lblMessageEarlierTrain.Text = "Online booking for the date you requested has not yet been opened by the operating train company, please try an earlier date(s).";
                lblMessageEarlierTrain.Visible = true;
                pCls.Attributes.Add("class", "clsError");
            }
        }
        else if (pInfoSolutionsResponse.TrainInformationList == null)
        {
            if (showBox)
            {
                pnlJourneyInfo.Visible = true;
                lblMessageEarlierTrain.Visible = false;
            }
            else
            {
                pnlJourneyInfo.Visible = false;
                lblMessageEarlierTrain.Text = "Online booking for the date you requested has not yet been opened by the operating train company, please try an earlier date(s).";
                lblMessageEarlierTrain.Visible = true;
                pCls.Attributes.Add("class", "clsError");
            }
        }
        else
        {
            //List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.Count()>0 ? pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList():null;
            if (pInfoSolutionsResponse.TrainInformationList.Count() > 0)
            {
                pnlJourneyInfo.Visible = false;
                lblMessageEarlierTrain.Visible = false;
            }
            else
            {
                if (showBox)
                {
                    pnlJourneyInfo.Visible = true;
                }
                else
                {
                    pnlJourneyInfo.Visible = false;
                    lblMessageEarlierTrain.Text = "Online booking for the date you requested has not yet been opened by the operating train company, please try an earlier date(s).";
                    lblMessageEarlierTrain.Visible = true;
                }
            }
        }

        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlYouth.SelectedValue = objBRUC.Youths.ToString();
            ddlSenior.SelectedValue = objBRUC.Seniors.ToString();
            rdBookingType.SelectedValue = objBRUC.Journeytype;
            if (objBRUC.ReturnDate != string.Empty)
            {
                txtTrainReturnDate.Enabled = true;
                rfReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                txtTrainReturnDate.Text = objBRUC.ReturnDate;
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                rfReturnDate.Enabled = true;
            }
            else
            {
                txtTrainReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                txtTrainReturnDate.Text = "";
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                rfReturnDate.Enabled = false;
            }

            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            chkLoyalty.Checked = objBRUC.Loyalty;
            chkIhaveRailPass.Checked = objBRUC.isIhaveRailPass;
            chkLoyalty_CheckedChanged(null, null);

            hdnPassenger.Value = objBRUC.Adults.ToString() + " x Adults," + objBRUC.Boys.ToString() + " x Children," + objBRUC.Seniors.ToString() + " x Seniors," +
                objBRUC.Youths.ToString() + " x Youths";

            lblSDetail.Text = "<div class='hd'> <span> >> Your Detail </span> <i>&nbsp;</i></div><div class='booking-status'>" +
                "<p>" + objBRUC.FromDetail + " to " + objBRUC.ToDetail + "<br>Departs " + objBRUC.depdt.ToString("dd/MMM/yyyy") + "<br>" +
                objBRUC.Adults.ToString() + " x Adults <br>" +
                objBRUC.Seniors.ToString() + " x Seniors <br>" +
                objBRUC.Boys.ToString() + " x Children <br>" +
                objBRUC.Youths.ToString() + " x Youths <br>" +
                "Please select train</p></div>";
        }
    }

    #region Old functionlaity up to : 31-March-2014
    public void AddJourneyRequests()
    {
        try
        {

            string Message = "<b>A user has requested a journey, please check & respond.</b><br /><br />" +
                      "<span style='font-size: 14px;font-weight:bold'>User Details</span>" +
                      "<tr><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'>" +
                      "<strong>User Name</strong></font></td><td style='font-size: 12px'>" +
                      "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + txtName.Text + " </font></td></tr>" +
                      "<tr><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'>" +
                      "<strong>User Email</strong></font></td><td style='font-size: 12px'>" +
                      "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + txtEmailAddress.Text + " </font></td></tr>" +
                      "<tr><td style='font-size: 12px'><font face='Arial, Helvetica, sans-serif' color='#000000'>" +
                      "<strong>User Contact Phone</strong></font></td><td style='font-size: 12px'>" +
                      "<font face='Arial, Helvetica, sans-serif' color='#000000'>" + txtPhone.Text + " </font></td></tr>";

            // SendMailToUser(Message, false, null);

            /*Send Email to Journey Request form email delivery address*/
            var journeyEmail = _oWebsitePage.GetJourneyEmail(_siteId);
            if (!string.IsNullOrEmpty(journeyEmail))
            {
                SendMailToUser(Message, false, journeyEmail);
            }
            else
            {
                var obj = new tblJourneyRequest();
                obj.UserName = txtName.Text;
                obj.Email = txtEmailAddress.Text;
                obj.Phone = txtPhone.Text;
                obj.From = txtFrom.Text;
                obj.To = txtTo.Text;
                obj.DepartDate = Convert.ToDateTime(txtDepartureDate.Text);
                obj.DepartTime = ddldepTime.SelectedValue;
                if (txtTrainReturnDate.Text.Trim() != "" && txtTrainReturnDate.Text.Trim() != "DD/MM/YYYY")
                {
                    obj.ReturnDate = Convert.ToDateTime(txtTrainReturnDate.Text);
                    obj.ReturnTime = ddlReturnTime.SelectedValue;
                }

                obj.Adult = ddlAdult.SelectedValue;
                obj.Children = ddlChild.SelectedValue;
                obj.Youth = ddlYouth.SelectedValue;
                obj.Senior = ddlSenior.SelectedValue;

                obj.ClassPreference = ddlClass.SelectedValue;
                obj.MaxTransfers = ddlTransfer.SelectedValue;
                obj.LoyaltyCards = chkLoyalty.Checked;
                obj.HaveARailPass = chkIhaveRailPass.Checked;
                obj.DateOfRequest = DateTime.Now;
                new ManageJourneyRequest().AddJourneyRequests(obj);

            }
            DivLeftOne.Style.Add("display", "none");
            DivLeftSecond.Style.Add("display", "block");
        }
        catch (Exception ex)
        {
        }
    }
    public bool SendMailToUser(string Message, bool isUser, string jouneyEmail)
    {
        bool retVal = false;
        try
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
            if (st != null)
            {
                string SiteName = st.SiteURL + "Home";
                string Subject = "Journey Request!";
                string dir = HttpContext.Current.Request.PhysicalApplicationPath;
                if (st.IsSTA ?? false)
                    htmfile = Server.MapPath("~/MailTemplate/StaEmail.htm");
                else
                    htmfile = Server.MapPath("~/MailTemplate/email.html");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");

                string body = list[0].InnerXml.ToString();
                body = body.Replace("##ExtraColumn##", "");
                body = body.Replace("##Message##", Message);
                body = body.Replace("##From##", txtFrom.Text);
                body = body.Replace("##To##", txtTo.Text);
                body = body.Replace("##JourneyType##", rdBookingType.SelectedItem.Text);
                body = body.Replace("##DepartureDate##", txtDepartureDate.Text + " " + ddldepTime.SelectedValue);
                body = body.Replace("##ReturnDate##", txtTrainReturnDate.Text + " " + (txtTrainReturnDate.Text.Trim() != "" && txtTrainReturnDate.Text.Trim() != "DD/MM/YYYY" ? ddlReturnTime.SelectedValue : ""));
                body = body.Replace("##NumberOfAdults##", ddlAdult.SelectedValue);
                body = body.Replace("##NumberOfChildren##", ddlChild.SelectedValue);
                body = body.Replace("##NumberOfYouths##", ddlYouth.SelectedValue);
                body = body.Replace("##NumberOfSeniors##", ddlSenior.SelectedValue);
                body = body.Replace("##ClassPreference##", ddlClass.SelectedItem.Text);
                body = body.Replace("##MaxTransfers##", ddlTransfer.SelectedItem.Text);
                body = body.Replace("##LastChecked##", chkLoyalty.Checked ? "Journey with loyalty card." : (chkIhaveRailPass.Checked ? "Journey with rail pass." : ""));
                body = body.Replace("#Blanck#", "&nbsp;");

                /*Get smtp details*/
                var result = _master.GetEmailSettingDetail(_siteId);
                if (result != null)
                {
                    // to address
                    string ToEmail;
                    if (!string.IsNullOrEmpty(jouneyEmail))
                    {
                        ToEmail = jouneyEmail;
                    }
                    else
                    {
                        if (isUser)
                            ToEmail = txtEmailAddress.Text;
                        else
                            ToEmail = result.Email;
                    }
                    //var fromAddres = new MailAddress(result.Email, result.Email);
                    var fromAddres = new MailAddress(txtEmailAddress.Text, txtEmailAddress.Text);
                    smtpClient.Host = result.SmtpHost;
                    smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                    smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                    message.From = fromAddres;
                    message.To.Add(ToEmail);

                    message.Subject = Subject;
                    message.IsBodyHtml = true;
                    message.Body = body;

                    smtpClient.Send(message);
                    retVal = true;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return retVal;
    }
    #endregion
}