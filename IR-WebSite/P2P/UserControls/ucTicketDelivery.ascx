﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTicketDelivery.ascx.cs"
    Inherits="OtherSiteP2PBooking_ucTicketDelivery" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="round-titles">
            Ticket delivery
        </div>
        <div class="delivery-detail">
            <h1>
                Select your delivery method</h1>
            <p>
                Depending on the carrier, you can choose how to receive your tickets:</p>
            <p>
                &nbsp;</p>
            <div class="deliveryOptions">
                <div class="float-lt">
                    <asp:HiddenField ID="hiddenDileveryMethod" runat="server" />
                    <asp:RadioButtonList ID="rdoBkkoingList" runat="server" AutoPostBack="true" CssClass="clsdeliveryMethod options"
                        OnSelectedIndexChanged="rdoBkkoingList_SelectedIndexChanged" />
                </div>
                <div class="dvRtBlock">
                    <div id="deliverymethodDH" runat="server" style="display: block;">
                        <div class="clsTitle">
                            <asp:Literal ID="ltrDilverName" runat="server" /></div>
                        <div id="HeaderDhMsg" runat="server" style="display: none;">
                            <p>
                                <asp:Image ID="Image11" runat="server" ImageUrl="../images/icoHomePrint.jpg" CssClass="float-rt" />
                                Click the link in the confirmation email that you receive to download your ticket
                                as a PDF. Print it on a plain sheet of A4 paper and keep it to hand during your
                                journey.
                            </p>
                            <div style="width: 440px; font-size: 13px; color: #000;">
                                Please ensure you have a printed copy of your PDF booking confirmation as trains
                                will not always accept the booking from your tablet or mobile screen.
                            </div>
                        </div>
                        <div id="HeaderStMsg" runat="server" style="display: none;">
                            <p>
                                You can collect your tickets at the international ticket desks. You will need the
                                booking reference (DNR) mentioned in the confirmation e-mail we'll send you. We
                                advise you to take this e-mail with you to the station.
                                <br />
                                <br />
                                <b>Please choose your collection station:</b>
                                <br />
                                <asp:DropDownList ID="ddlCollectStation" runat="server" Width="200px" CssClass="chkcountry">
                                    <asp:ListItem Value="0">--Choose your station--</asp:ListItem>
                                    <asp:ListItem Value="BEAAL">Aalst (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEALT">Aalter (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEAAR">Aarschot (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLASC">Amsterdam (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEANS">Ans (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBHM">Antwerpen-Berchem (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEABC">Antwerpen-Centraal (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEARL">Arlon (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLAHM">Arnhem (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEATH">Ath (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBLA">Blankenberge (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBRL">Braine-l Alleud (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBLC">Braine-le-Comte (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLBDA">Breda (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEBRG">Brugge (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEABT">Brussel Nat Luchthaven (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBCE">Bruxelles-Central (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBQL">Bruxelles-Luxembourg (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBMI">Bruxelles-Midi (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEBNO">Bruxelles-Nord (Be)</asp:ListItem>
                                    <asp:ListItem Value="BESHU">Bruxelles-Schuman (Be)</asp:ListItem>
                                    <asp:ListItem Value="BECHS">Charleroi-Sud (Be)</asp:ListItem>
                                    <asp:ListItem Value="BECIN">Ciney (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDPA">De Panne (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDPI">De Pinte (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDEI">Deinze (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLDHC">Den Haag (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEDEW">Denderleeuw (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDMD">Dendermonde (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEDIS">Diest (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEENG">Enghien (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEEUP">Eupen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEE">Geel (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEM">Gembloux (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEN">Genk (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGDA">Gent Dampoort (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGNT">Gent-Sint-Pieters (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEGEL">Genval (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHAL">Halle (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHAS">Hasselt (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHES">Herentals (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEHUY">Huy (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEIEP">Ieper (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEIZE">Izegem (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEKAP">Kapellen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEKNO">Knokke (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEKOR">Kortrijk (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELHP">La Hulpe (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELAS">La Louviere Sud (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELEU">Leuven (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELIB">Libramont (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELGG">Liege-Guillemins (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELPA">Liege-Palais (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELIR">Lier (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELOK">Lokeren (Be)</asp:ListItem>
                                    <asp:ListItem Value="BELOU">Louvain La Neuve Univ (Be)</asp:ListItem>
                                    <asp:ListItem Value="LULUX">Luxembourg (Lu)</asp:ListItem>
                                    <asp:ListItem Value="BEMAL">Marloie (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEMEC">Mechelen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEMOL">Mol (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEQMO">Mons (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEMOU">Mouscron (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEQNM">Namur (Be)</asp:ListItem>
                                    <asp:ListItem Value="BENIV">Nivelles (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEOST">Oostende (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEOTT">Ottignies (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEOUD">Oudenaarde (Be)</asp:ListItem>
                                    <asp:ListItem Value="BERIX">Rixensart (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEROE">Roeselare (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLRTC">Rotterdam (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BESIN">Sint-Niklaas (Be)</asp:ListItem>
                                    <asp:ListItem Value="BESIT">Sint-Truiden (Be)</asp:ListItem>
                                    <asp:ListItem Value="BESOI">Soignies (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETIT">Tielt (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETIE">Tienen (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETOU">Tournai (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETUB">Tubize (Be)</asp:ListItem>
                                    <asp:ListItem Value="BETUR">Turnhout (Be)</asp:ListItem>
                                    <asp:ListItem Value="NLUTC">Utrecht Cs (Nl)</asp:ListItem>
                                    <asp:ListItem Value="BEVEC">Verviers-Central (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEVIL">Vilvoorde (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEVIS">Vise (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWAR">Waregem (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWAM">Waremme (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWTL">Waterloo (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWAV">Wavre (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEWET">Wetteren (Be)</asp:ListItem>
                                    <asp:ListItem Value="BEZOT">Zottegem (Be)</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqCollectStation" runat="server" Text="*" ErrorMessage="Please choose station name."
                                    InitialValue="0" ControlToValidate="ddlCollectStation" ForeColor="#eaeaea" ValidationGroup="vgs1"
                                    Enabled="false" />
                            </p>
                        </div>
                        <div class="clear">
                        </div>
                        <div runat="server" id="divLoyalty" visible="false">
                            <p>
                                <strong>Loyalty cards:</strong> Eurostar:
                                <asp:Literal ID="lblEuCardNumber" runat="server" />, Thalys:
                                <asp:Literal ID="lblThCardNumber" runat="server" />
                            </p>
                        </div>
                        <div>
                            <p>
                                Your personal details are processed in accordance with the <a href="http://www.internationalrail.com.au/information/privacy-policy.aspx"
                                    target="_blank">privacy policy of InternationalRail.</a></p>
                            <div id="FooterStMsg" runat="server" style="display: none;">
                                <p>
                                    Important:
                                    <br />
                                    if you pay by credit card, the card holder will be asked to produce the card and
                                    identity document (such as passport) at the time of ticket collection. Virtual credit
                                    cards are not accepted. Please enquire about the opening times of the international
                                    ticket desks at the selected station.
                                </p>
                            </div>
                            <asp:DataList ID="dtlPassngerDetails" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                OnItemDataBound="dtlPassngerDetails_ItemDataBound">
                                <ItemTemplate>
                                    <div style="width: 100%; float: left;">
                                        <div class="divLoyHeader">
                                            <b>
                                                <%#Eval("PassangerType")%></b>
                                        </div>
                                        <div class="divThy" style="float: left;">
                                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="15" Width="160px" onblur="checklength(this.id);" />
                                            <asp:RequiredFieldValidator ID="reqFirstName" runat="server" Text="*" ErrorMessage="Please enter First Name."
                                                ControlToValidate="txtFirstName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                            <asp:TextBoxWatermarkExtender runat="server" ID="wtrFirstName" WatermarkText="First Name"
                                                TargetControlID="txtFirstName" WatermarkCssClass="WmaxCss" />
                                            <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                                ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                        </div>
                                        <div class="divThy" style="float: left;">
                                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" Width="160px" onblur="checklength(this.id);" />
                                            <asp:RequiredFieldValidator ID="reqLastName" runat="server" Text="*" ErrorMessage="Please enter Last Name."
                                                ControlToValidate="txtLastName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                            <asp:TextBoxWatermarkExtender runat="server" ID="wtrLastName" WatermarkText="Last Name"
                                                TargetControlID="txtLastName" WatermarkCssClass="WmaxCss" />
                                            <asp:FilteredTextBoxExtender ID="ftex" runat="server" TargetControlID="txtLastName"
                                                ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                        </div>
                                        <div class="divThy" style="float: left;">
                                            <asp:TextBox ID="txtEmailAddress" runat="server" Width="160px" />
                                            <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
                                                ErrorMessage="Please enter email." ValidationGroup="vgs1" Text="*" Enabled="false"
                                                ForeColor="#eaeaea" />
                                            <asp:RegularExpressionValidator ID="reqcustvalerrorEmailAddress" runat="server" ForeColor="#eaeaea"
                                                ErrorMessage="Please enter valid email." ControlToValidate="txtEmailAddress"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="*"
                                                ValidationGroup="vgs1" Enabled="false" />
                                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtEmailAddress"
                                                WatermarkText="Email Address" WatermarkCssClass="WmaxCss" />
                                        </div>
                                    </div>
                                    <div style="width: 100%; float: left; margin-top: 5px;">
                                        <div id="DivDOB" runat="server">
                                            <div class="divLoyHeader">
                                                D.O.B
                                            </div>
                                            <div class="divThy" style="float: left;">
                                                <asp:DropDownList ID="ddlDay" runat="server" Width="116px" Height="24px">
                                                    <asp:ListItem Value="DD" Text="DD"></asp:ListItem>
                                                    <asp:ListItem Value="01" Text="1"></asp:ListItem>
                                                    <asp:ListItem Value="02" Text="2"></asp:ListItem>
                                                    <asp:ListItem Value="03" Text="3"></asp:ListItem>
                                                    <asp:ListItem Value="04" Text="4"></asp:ListItem>
                                                    <asp:ListItem Value="05" Text="5"></asp:ListItem>
                                                    <asp:ListItem Value="06" Text="6"></asp:ListItem>
                                                    <asp:ListItem Value="07" Text="7"></asp:ListItem>
                                                    <asp:ListItem Value="08" Text="8"></asp:ListItem>
                                                    <asp:ListItem Value="09" Text="9"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                    <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                                    <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                                    <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                                    <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                                    <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                                    <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                                    <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                                    <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                                    <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                                    <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                                    <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                                    <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                                    <asp:ListItem Value="26" Text="26"></asp:ListItem>
                                                    <asp:ListItem Value="27" Text="27"></asp:ListItem>
                                                    <asp:ListItem Value="28" Text="28"></asp:ListItem>
                                                    <asp:ListItem Value="29" Text="29"></asp:ListItem>
                                                    <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                                    <asp:ListItem Value="31" Text="31"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqvalerrorDay" runat="server" CssClass="absolute" ForeColor="#eaeaea" 
                                                    Text="*" ErrorMessage="Please select dob day." ValidationGroup="vgs1" ControlToValidate="ddlDay"
                                                    InitialValue="DD" />
                                            </div>
                                            <div class="divThy" style="float: left;">
                                                <asp:DropDownList ID="ddlMonth" runat="server" Width="115px" Height="24px">
                                                    <asp:ListItem Value="MM" Text="MM"></asp:ListItem>
                                                    <asp:ListItem Value="Jan" Text="Jan"></asp:ListItem>
                                                    <asp:ListItem Value="Feb" Text="Feb"></asp:ListItem>
                                                    <asp:ListItem Value="Mar" Text="Mar"></asp:ListItem>
                                                    <asp:ListItem Value="Apr" Text="Apr"></asp:ListItem>
                                                    <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                                    <asp:ListItem Value="Jun" Text="Jun"></asp:ListItem>
                                                    <asp:ListItem Value="Jul" Text="Jul"></asp:ListItem>
                                                    <asp:ListItem Value="Aug" Text="Aug"></asp:ListItem>
                                                    <asp:ListItem Value="Sep" Text="Sep"></asp:ListItem>
                                                    <asp:ListItem Value="Oct" Text="Oct"></asp:ListItem>
                                                    <asp:ListItem Value="Nov" Text="Nov"></asp:ListItem>
                                                    <asp:ListItem Value="Dec" Text="Dec"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqvalerrorMonth" runat="server" CssClass="absolute" ForeColor="#eaeaea" 
                                                    Text="*" ErrorMessage="Please select dob month." ValidationGroup="vgs1" ControlToValidate="ddlMonth"
                                                    InitialValue="MM" />
                                                <asp:DropDownList ID="ddlYear" runat="server" Width="115px" Height="24px">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqvalerrorYear" runat="server" CssClass="absolute" ForeColor="#eaeaea" 
                                                    Text="*" ErrorMessage="Please select dob year." ValidationGroup="vgs1" ControlToValidate="ddlYear"
                                                    InitialValue="YYYY" />
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="spacer20">
                        </div>
                        <div class="clsbckColor" id="FooterDhMsg2" runat="server" style="display: none;">
                            <p>
                                Self print tickets are strictly personal and non-transferable. Enter your names
                                above as they appear in your passport, if possible.
                                <br>
                                Note: the first name field is limited to 15 characters.</p>
                            <div class="reset">
                            </div>
                        </div>
                    </div>
                    <div id="deliverymethodTL" runat="server" style="display: none;">
                        <div class="clsTitle">
                            Thalys Ticketless</div>
                        <p>
                            This is for Thalys Card holders only.<br />
                            Are you a member of the Thalys The Card&nbsp;loyalty programme? Enter the&nbsp;card
                            number for each passenger wanting to travel Ticketless and earn points for this
                            trip.
                        </p>
                        <br>
                        <asp:Image ID="Image1" runat="server" ImageUrl="../images/ticketless.jpg" Width="370"
                            Height="145" />
                        <div>
                            <div class="clear">
                            </div>
                            <asp:DataList ID="dtlLoayalty" runat="server" RepeatColumns="1" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <div class="divLoyHeader" style="float: none;">
                                        <%#Eval("PassangerType")%>
                                    </div>
                                    <div class="divLoyleft">
                                        <asp:TextBox ID="txtloyCardThy" runat="server" CssClass="txtThyleft" Style="color: #FFF;"
                                            Text="Thalys" ReadOnly="True" />
                                    </div>
                                    <div class="divThy">
                                        <asp:TextBox ID="txtThalysCardNumber" runat="server" MaxLength="20" Width="180px"
                                            Text='<%#Eval("cardnumber") %>' />
                                        <asp:RequiredFieldValidator ID="reqCardNumber" runat="server" Text="*" ErrorMessage="This option is available only for Thalys Card holders."
                                            ControlToValidate="txtThalysCardNumber" ForeColor="#eaeaea" ValidationGroup="vgs1"
                                            Enabled="false" />
                                        <asp:TextBoxWatermarkExtender runat="server" ID="txtThyP" WatermarkText="(card number)"
                                            TargetControlID="txtThalysCardNumber" WatermarkCssClass="WmaxCss" />
                                        <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtThalysCardNumber"
                                            ValidChars="0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="spacer20">
                        </div>
                    </div>
                    <div id="deliverymethodMail" runat="server" style="display: none;">
                        <div class="clsTitle">
                            Delivery by mail</div>
                        <div id="Div2" runat="server">
                            <p>
                                “Standard Delivery (allow 5 business days) by Toll Priority Courier”
                            </p>
                        </div>
                        <div class="spacer20">
                        </div>
                        <div class="clsbckColor" id="Div1" runat="server">
                            <p>
                                <strong>Note:</strong> For this delivery method there is a
                                <asp:Label ID="lblCurrncy" runat="server" /><asp:Label ID="lblAmount" runat="server"
                                    Text="0" />
                                shipping charge added to the order.
                                <div class="reset">
                                </div>
                        </div>
                        <div class="spacer20">
                        </div>
                        <div class="clsbckColor">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        FirstName:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left;">
                                        <asp:TextBox ID="txtMailFName" runat="server" MaxLength="50" Width="99%" CssClass="input" />
                                        <asp:RequiredFieldValidator ID="reqMailFName" runat="server" Text="*" ErrorMessage="Please enter first name"
                                            ControlToValidate="txtMailFName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        LastName:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:TextBox ID="txtMailLName" runat="server" MaxLength="50" Width="99%" CssClass="input" />
                                        <asp:RequiredFieldValidator ID="reqMailLName" runat="server" Text="*" ErrorMessage="Please enter last name"
                                            ControlToValidate="txtMailLName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        Department:
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:TextBox ID="txtDepartment" runat="server" MaxLength="50" Width="99%" CssClass="input" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        Address:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:TextBox ID="txtStreet" runat="server" MaxLength="50" Width="99%" CssClass="input" />
                                        <asp:RequiredFieldValidator ID="reqStreet" runat="server" Text="*" ErrorMessage="Please enter address"
                                            ControlToValidate="txtStreet" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        Town/City:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:TextBox ID="txtMailCity" runat="server" MaxLength="50" Width="99%" CssClass="input" />
                                        <asp:RequiredFieldValidator ID="reqMailCity" runat="server" Text="*" ErrorMessage="Please enter town/city"
                                            ControlToValidate="txtMailCity" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        Country:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:DropDownList ID="ddlCountryMail" runat="server" Style="margin-top: 6px; width: 100%;"
                                            CssClass="inputsl" OnSelectedIndexChanged="ddlCountryMail_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqCountryMail" runat="server" Text="*" ErrorMessage="Please select country"
                                            ControlToValidate="ddlCountryMail" ForeColor="#eaeaea" ValidationGroup="vgs1"
                                            Enabled="false" InitialValue="-1" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        State:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:DropDownList ID="ddlStateMail" runat="server" Style="margin-top: 6px; width: 100%;"
                                            CssClass="inputsl">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqStateMail" runat="server" Text="*" ErrorMessage="Please select state"
                                            ControlToValidate="ddlStateMail" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false"
                                            InitialValue="-1" />
                                    </td>
                                </tr>
                                <tr style="line-height: 5px;">
                                    <td style="width: 30%; float: left; line-height: 25px;">
                                        Postal/Zip Code:<span style="color: Red">*</span>
                                    </td>
                                    <td style="width: 69%; float: left; margin-top: 5px;">
                                        <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="10" Width="99%" CssClass="input" />
                                        <asp:RequiredFieldValidator ID="reqPostalCode" runat="server" Text="*" ErrorMessage="Please enter postal/Zip Code"
                                            ControlToValidate="txtPostalCode" ForeColor="#eaeaea" ValidationGroup="vgs1"
                                            Enabled="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="spacer20">
                        </div>
                        <div class="clsTitle">
                            <b>Travel Party: Names must be as per passport</b>
                        </div>
                        <asp:DataList ID="dtlPassngerDelivery" runat="server" RepeatColumns="1" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <div class="divLoyHeader">
                                    <b>
                                        <%#Eval("PassangerType")%></b>
                                </div>
                                <div class="divThy" style="float: left;">
                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="15" Width="160px" onblur="checklength(this.id);" />
                                    <asp:RequiredFieldValidator ID="reqFirstName" runat="server" Text="*" ErrorMessage="Please enter First Name."
                                        ControlToValidate="txtFirstName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                    <asp:TextBoxWatermarkExtender runat="server" ID="wtrFirstName" WatermarkText="First Name"
                                        TargetControlID="txtFirstName" WatermarkCssClass="WmaxCss" />
                                    <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                        ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                </div>
                                <div class="divThy" style="float: left;">
                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" Width="160px" onblur="checklength(this.id);" />
                                    <asp:RequiredFieldValidator ID="reqLastName" runat="server" Text="*" ErrorMessage="Please enter Last Name."
                                        ControlToValidate="txtLastName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                    <asp:TextBoxWatermarkExtender runat="server" ID="wtrLastName" WatermarkText="Last Name"
                                        TargetControlID="txtLastName" WatermarkCssClass="WmaxCss" />
                                    <asp:FilteredTextBoxExtender ID="ftex" runat="server" TargetControlID="txtLastName"
                                        ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                </div>
                                <div class="divThy" style="float: left;">
                                    <asp:TextBox ID="txtEmailAddress" runat="server" Width="160px" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
                                        ErrorMessage="Please enter email." ValidationGroup="vgs1" Text="*" Enabled="false"
                                        ForeColor="#eaeaea" />
                                    <asp:RegularExpressionValidator ID="reqcustvalerrorEmailAddress" runat="server" ForeColor="#eaeaea"
                                        ErrorMessage="Please enter valid email." ControlToValidate="txtEmailAddress"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="*"
                                        ValidationGroup="vgs1" Enabled="false" />
                                    <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtEmailAddress"
                                        WatermarkText="Email Address" WatermarkCssClass="WmaxCss" />
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <asp:DataList ID="dtlPassngerDetails2" runat="server" RepeatColumns="1" RepeatDirection="Horizontal">
                        <ItemTemplate>
                            <div class="divLoyHeader">
                                <b>
                                    <%#Eval("PassangerType")%></b>
                            </div>
                            <div class="divThy" style="float: left;">
                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="15" Width="180px" />
                                <asp:RequiredFieldValidator ID="reqFirstName" runat="server" Text="*" ErrorMessage="Please enter First Name."
                                    ControlToValidate="txtFirstName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                <asp:TextBoxWatermarkExtender runat="server" ID="wtrFirstName" WatermarkText="First Name"
                                    TargetControlID="txtFirstName" WatermarkCssClass="WmaxCss" />
                                <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                    ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                            </div>
                            <div class="divThy" style="float: left;">
                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" Width="180px" />
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" Text="*" ErrorMessage="Please enter Last Name."
                                    ControlToValidate="txtLastName" ForeColor="#eaeaea" ValidationGroup="vgs1" Enabled="false" />
                                <asp:TextBoxWatermarkExtender runat="server" ID="wtrLastName" WatermarkText="Last Name"
                                    TargetControlID="txtLastName" WatermarkCssClass="WmaxCss" />
                                <asp:FilteredTextBoxExtender ID="ftex" runat="server" TargetControlID="txtLastName"
                                    ValidChars="-qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
