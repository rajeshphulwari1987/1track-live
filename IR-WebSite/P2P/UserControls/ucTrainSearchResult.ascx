﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTrainSearchResult.ascx.cs"
    Inherits="UserControls_ucTrainSearchResult" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/P2P/UserControls/ucTrainSearch.ascx" TagName="ucTrainSearch"
    TagPrefix="uc1" %>
<style type="text/css">
    .tooltip {
        display: block !important;
    }

    #ContentPlaceHolder1_ucSResult_btnEarlier, #ContentPlaceHolder1_ucSResult_btnLater, #ContentPlaceHolder1_ucSResult_btnChangeSearch {
        margin-bottom: 10px;
    }
</style>
<script type="text/javascript">
    function checkDate(sender) {
        var selectedDate = new Date(sender._selectedDate);
        var today = new Date();
        today.setHours(0, 0, 0, 0);

        if (selectedDate < today) {
            alert('Select a date sometime in the future!');
            sender._selectedDate = new Date();
            sender._textbox.set_Value(sender._selectedDate.format(sender._format));
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".tresult").each(function () {
            $(this).find(".taco :eq(1)").find("div").show();
        });
        $(".taco").click(function () {
            $(this).find("div").slideToggle();
        });
    });
    function getfare() {
        var hostName = window.location.host;
        var url = "http://" + hostName;
        if (window.location.toString().indexOf("https:") >= 0)
            url = "https://" + hostName;
        if (hostName == "localhost") {
            url = url + "/interrail";
        }
        var hostUrl = url + "/StationList.asmx/GetFare";
        $.ajax({
            type: "POST",
            url: hostUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data.d, function (key, value) {
                    var scode = $.trim(value.ServiceTypeCode)
                    $('.' + scode).html(value.fareCondition);
                });
            },
            error: function () {
                console.log('An error occurred');
            }
        });
    }

</script>
<script type="text/javascript">

    function SendJCodeAndServiceID() {
        if ($("#ContentPlaceHolder1_ucSResult_hdnReq").val() == "TI") {
            var SDataVal = 'The cancellation penalty breakdown is as follows:20% penalty to Trenitalia, and 10% processing fee to ItaliaRail for handling and processing the cancellation.';
            $("#ShowCurrentDetail").html(SDataVal.toString());
        }
        var isSelect = false;
        //--TI
        if (typeof ($('input[name=rdoFromTI]:checked').val()) != "undefined") {
            var strFrom = $('input[name=rdoFromTI]:checked').val();

            var strFromTi = strFrom.split(",");
            var jsCodeFrom = strFromTi[0]; var trainNoFrom = strFromTi[1]; var tripTypeFrom = strFromTi[2];
            var svCodeFrom = strFromTi[3]; var svTypCodeFrom = strFromTi[4]; var oCdFrom = strFromTi[5]; var oTypCdFrom = strFromTi[6];
            var oSubCdFrom = strFromTi[7]; var agreFrom = strFromTi[8]; var classFrom = strFromTi[9];

            $("#ContentPlaceHolder1_ucSResult_hdnFromjsCode").val(jsCodeFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromtrainNo").val(trainNoFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromtripType").val(tripTypeFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromsvCode").val(svCodeFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromsvTypCode").val(svTypCodeFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromoCd").val(oCdFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromoTypCd").val(oTypCdFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromoSubCd").val(oSubCdFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromagre").val(agreFrom);
            $("#ContentPlaceHolder1_ucSResult_hdnFromClass").val(classFrom);
            isSelect = true;
        }

        if (typeof ($('input[name=rdoToTI]:checked').val()) != "undefined") {
            var strTo = $('input[name=rdoToTI]:checked').val();

            var strToTi = strTo.split(",");
            var jsCodeTo = strToTi[0]; var trainNoTo = strToTi[1]; var tripTypeTo = strToTi[2];
            var svCodeTo = strToTi[3]; var svTypCodeTo = strToTi[4]; var oCdTo = strToTi[5]; var oTypCdTo = strToTi[6];
            var oSubCdTo = strToTi[7]; var agreTo = strToTi[8]; var classTo = strToTi[9];

            $("#ContentPlaceHolder1_ucSResult_hdnTojsCode").val(jsCodeTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTotrainNo").val(trainNoTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTotripType").val(tripTypeTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTosvCode").val(svCodeTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTosvTypCode").val(svTypCodeTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTooCd").val(oCdTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTooTypCd").val(oTypCdTo);
            $("#ContentPlaceHolder1_ucSResult_hdnTooSubCd").val(oSubCdTo);
            $("#ContentPlaceHolder1_ucSResult_hdnToagre").val(agreTo);
            $("#ContentPlaceHolder1_ucSResult_hdnToClass").val(classTo);
            isSelect = true;
        }

        //BENE
        if (typeof ($('input[name=rdoFrom]:checked').val()) != "undefined") {
            var strf = $('input[name=rdoFrom]:checked').val();
            var arrayf = strf.split(",");
            $("#ContentPlaceHolder1_ucSResult_hdnFromRid").val(arrayf[0]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromsvcTyp").val(arrayf[1]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromPriceId").val(arrayf[2]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromBeNeClass").val(arrayf[3]);

            isSelect = true;
        }

        if (typeof ($('input[name=rdoTo]:checked').val()) != "undefined") {


            var strT = $('input[name=rdoTo]:checked').val();
            var arrayT = strT.split(",");
            $("#ContentPlaceHolder1_ucSResult_hdnToRid").val(arrayT[0]);
            $("#ContentPlaceHolder1_ucSResult_hdnTosvcTyp").val(arrayT[1]);
            $("#ContentPlaceHolder1_ucSResult_hdnToPriceId").val(arrayT[2]);
            $("#ContentPlaceHolder1_ucSResult_hdnToBeNeClass").val(arrayT[3]);
            isSelect = true;

        }

        // TI TCV

        if (typeof ($('input[name=rdoFromTiTcv]:checked').val()) != "undefined") {
            var strftcv = $('input[name=rdoFromTiTcv]:checked').val();
            var arrayftcv = strftcv.split(",");
            $("#ContentPlaceHolder1_ucSResult_hdnFrTcvRoute").val(arrayftcv[0]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromoCd").val(arrayftcv[1]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromoSubCd").val(arrayftcv[2]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromoTypCd").val(arrayftcv[3]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromsvCode").val(arrayftcv[4]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromsvTypCode").val(arrayftcv[5]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromagre").val(arrayftcv[6]);
            $("#ContentPlaceHolder1_ucSResult_hdnFromClass").val(arrayftcv[7]);
            isSelect = true;
        }

        if (typeof ($('input[name=rdoToTiTcv]:checked').val()) != "undefined") {
            var strTtcv = $('input[name=rdoToTiTcv]:checked').val();
            var arrayTtcv = strTtcv.split(",");
            $("#ContentPlaceHolder1_ucSResult_hdnToSvcRoute").val(arrayTtcv[0]);
            $("#ContentPlaceHolder1_ucSResult_hdnTooCd").val(arrayTtcv[1]);
            $("#ContentPlaceHolder1_ucSResult_hdnTooSubCd").val(arrayTtcv[2]);
            $("#ContentPlaceHolder1_ucSResult_hdnTooTypCd").val(arrayTtcv[3]);
            $("#ContentPlaceHolder1_ucSResult_hdnTosvCode").val(arrayTtcv[4]);
            $("#ContentPlaceHolder1_ucSResult_hdnTosvTypCode").val(arrayTtcv[5]);
            $("#ContentPlaceHolder1_ucSResult_hdnToagre").val(arrayTtcv[6]);
            $("#ContentPlaceHolder1_ucSResult_hdnToClass").val(arrayTtcv[7]);
            isSelect = true;
        }
        if (isSelect) {
            $find('ContentPlaceHolder1_ucSResult_mdpexQuickLoad').show();
        } else {
            alert("Pelase select at least one price");
        }
        return false;
    }

</script>
<script type="text/javascript">
    $(document).ready(function () {
        LoadCal();
    });
    function LoadCal() {
        $("#txtDepartureDate, #txtReturnDate").datepicker({
            numberOfMonths: 2,
            dateFormat: 'dd/M/yy',
            showButtonPanel: true,
            firstDay: 1,
            minDate: 0,
            maxDate: '+3m'
        });
        $(".imgCal").click(function () {
            $("#txtDepartureDate").datepicker('show');
        });
        $(".imgCal1").click(function () {
            if ($('#ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('show');
            }
        });
        $("#txtDepartureDate, #txtReturnDate").keypress(function (event) { event.preventDefault(); });
    }
</script>
<script type="text/javascript">
    function pageLoad(sender, args) {
        hide();
        $('.lnkCont').hide();
        $('.imgOpen').click(function () {
            $(this).parent().parent().next().find('.extra-detail').toggle();
        });
        $('.priceB').click(function () {
            var CurrID = $.trim($("#ContentPlaceHolder1_ucSResult_hdnCurrID").val());
            /*BE Case*/
            if ($.trim($("#ContentPlaceHolder1_ucSResult_hdnReq").val()) == "BE") {
                var SummaryTotal = parseFloat('0.00');
                var arrContentForShow = [];
                /*loop if radio is checked*/
                $('.priceB').each(function () {
                    if ($(this).is(':checked')) {
                        var strtrClass = $(this).parent().parent().parent().parent().parent().parent().parent().parent().attr('class');
                        var strtrClassSplit = strtrClass.split(' ');
                        if (strtrClassSplit.length == 2) {
                            var strMainTr = strtrClassSplit[1].split('-');
                            var strTrHead = ".trHead" + $.trim(strMainTr[1]);
                            var DepTime = $.trim($(strTrHead + " .DepTime").text());
                            var DepStName = $.trim($(strTrHead + " .DepStName").text());
                            var DepDate = $.trim($(strTrHead + " .DepDate").text());
                            var ArrTime = $.trim($(strTrHead + " .ArrTime").text());
                            var ArrStName = $.trim($(strTrHead + " .ArrStName").text());
                            var ArrDate = $.trim($(strTrHead + " .ArrDate").text());

                            var siteURL = $.trim($("#ContentPlaceHolder1_ucSResult_hdnsiteURL").val());
                            var ActDateDepart = $.trim($("#ContentPlaceHolder1_ucSResult_hdnDateDepart").val());
                            var ActDateArr = $.trim($("#ContentPlaceHolder1_ucSResult_hdnDateArr").val());

                            if (DepDate != "") {
                                ActDateDepart = (DepDate != ActDateDepart ? DepDate : ActDateDepart)
                            }
                            if (ArrDate != "") {
                                ActDateArr = (ArrDate != ActDateArr ? ArrDate : ActDateArr)
                            }
                            var PassengerVal = $("#hdnPassenger").val();

                            var STotalPrice = $(this).parent().find(".bprice");
                            var FareAndDetail = $(this).parent().find(".bprice1");
                            var ValTotal = STotalPrice.text();

                            var arrFareAndDetail = FareAndDetail.text().split(',');
                        }
                    }
                });
                /*end radio loop*/
                var i = 0;
                var strCt = '';
                for (i; i < arrContentForShow.length; i++) {
                    strCt = strCt + arrContentForShow[i];
                }

                strCt = strCt + "<div class='clear'></div><div class='dvtotalSummay'><p class='total'> <span>Total : </span>     <strong>" + CurrID + SummaryTotal.toFixed(2).toString() + "</strong> </p></div>";

                $("#lblSDetail").html(strCt);

                if ($('.booking-detail-in :radio[name=rdoTo]').length) {
                    if (arrContentForShow.length == 2) {
                        $("#btnSummarySubmit").css("display", "block");
                    }
                }
                else {
                    if (arrContentForShow.length == 1) {
                        $("#btnSummarySubmit").css("display", "block");
                    }
                }
            }
            /***/
            /*TI Case*/
            if ($.trim($("#ContentPlaceHolder1_ucSResult_hdnReq").val()) == "TI") {
                var SummaryTotal = parseFloat('0.00');
                var arrContentForShow = [];
                /*loop if radio is checked*/
                $('.priceB').each(function () {
                    if ($(this).is(':checked')) {
                        var strtrClass = $(this).parent().parent().parent().parent().parent().parent().parent().parent().attr('class');
                        var strtrClassSplit = strtrClass.split(' ');
                        if (strtrClassSplit.length == 2) {
                            var strMainTr = strtrClassSplit[1].split('-');
                            var strTrHead = ".trHead" + $.trim(strMainTr[1]);
                            var DepTime = $.trim($(strTrHead + " .DepTime").text());
                            var DepStName = $.trim($(strTrHead + " .DepStName").text());
                            var DepDate = $.trim($(strTrHead + " .DepDate").text());
                            var ArrTime = $.trim($(strTrHead + " .ArrTime").text());
                            var ArrStName = $.trim($(strTrHead + " .ArrStName").text());
                            var ArrDate = $.trim($(strTrHead + " .ArrDate").text());

                            var siteURL = $.trim($("#ContentPlaceHolder1_ucSResult_hdnsiteURL").val());
                            var ActDateDepart = $.trim($("#ContentPlaceHolder1_ucSResult_hdnDateDepart").val());
                            var ActDateArr = $.trim($("#ContentPlaceHolder1_ucSResult_hdnDateArr").val());

                            if (DepDate != "") {
                                ActDateDepart = (DepDate != ActDateDepart ? DepDate : ActDateDepart)
                            }
                            if (ArrDate != "") {
                                ActDateArr = (ArrDate != ActDateArr ? ArrDate : ActDateArr)
                            }



                            var PassengerVal = $("#hdnPassenger").val();

                            var STotalPrice = $(this).parent().find(".bprice");
                            var ValTotal = STotalPrice.text();
                            var SpServiceName = $(this).parent().find(".bprice1");
                            var VServiceName = SpServiceName.text();
                        }
                    }
                });

                var i = 0;
                var strCt = '';
                for (i; i < arrContentForShow.length; i++) {
                    strCt = strCt + arrContentForShow[i];
                }

                strCt = strCt + "<div class='clear'></div><div class='dvtotalSummay'><p class='total'> <span>Total : </span>     <strong>" + CurrID + SummaryTotal.toFixed(2).toString() + "</strong> </p></div>";

                $("#lblSDetail").html(strCt);

                if ($('.booking-detail-in :radio[name=rdoToTI]').length) {
                    if (arrContentForShow.length == 2) {
                        $("#btnSummarySubmit").css("display", "block");
                    }
                }
                else {
                    if (arrContentForShow.length == 1) {
                        $("#btnSummarySubmit").css("display", "block");
                    }
                }
            }

            var DataVal = $(this).next("span").find("div.taco:eq(1)").text();
            $("#ShowCurrentDetail").html(DataVal.toString());

            if ($('.booking-detail-in :radio[name=rdoTo]').length > 0 || $('.booking-detail-in :radio[name=rdoToTI]').length > 0) {
                if ($(this).attr('name').toString() == "rdoTo" || $(this).attr('name').toString() == "rdoToTI") {
                    $('.lnkCont').hide();
                    $('.priceB').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).parent().parent().parent().parent().parent().parent().next().find('.lnkCont').show();
                        }
                    });
                }
                else if ($('.booking-detail-in :radio[name=rdoTo]:checked').length > 0 || $('.booking-detail-in :radio[name=rdoToTI]:checked').length > 0) {
                    $('.lnkCont').hide();
                    $('.priceB').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).parent().parent().parent().parent().parent().parent().next().find('.lnkCont').show();
                        }
                    });
                    if ($(this).attr('name').toString() == "rdoFrom" || $(this).attr('name').toString() == "rdoFromTI") {
                        if ($(".InPosition").length > 0) {
                            var my = $("h2.InPosition");
                            var myposition = my.position();
                            var sPos = myposition.top + 130;
                            $('html, body').animate({ scrollTop: sPos }, 'fast');
                        }
                    }
                }
                else {
                    if ($(this).attr('name').toString() == "rdoFrom" || $(this).attr('name').toString() == "rdoFromTI") {
                        if ($(".InPosition").length > 0) {
                            var my = $("h2.InPosition");
                            var myposition = my.position();
                            var sPos = myposition.top + 130;
                            $('html, body').animate({ scrollTop: sPos }, 'fast');

                        }
                    }
                }
            }
            else {

                $('.lnkCont').hide();
                $(this).parent().parent().parent().parent().parent().parent().next().find('.lnkCont').show();
            }
        });

    }
    function hide() { $('.extra-detail').hide(); }
    function show() { $('.extra-detail').show(); }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnSummarySubmit").click(function () {
            $(".grid2").each(function () {
                var fare = $("input:checked").next("span").clone().find('p');
                $("#divShowFareRules").html(fare);
            });
        });

        $(".lnkCont").click(function () {
            $(".grid2").each(function () {
                var fare = $("input:checked").next("span").clone().find('p');
                $("#divShowFareRules").html(fare);
                $('#divShowFareRules').find('p').first().remove();
            });
        });
    });
</script>
<style type="text/css">
    .clsInput {
        border-radius: 5px 5px 5px 5px;
        line-height: 25px !important;
        border: 1px solid #ADB9C2;
    }

    .bprice1 {
        display: none;
    }
</style>
<style type="text/css">
    .loading {
        background-image: url(../images/loading3.gif);
        background-position: right;
        background-repeat: no-repeat;
    }

    .modalBackground {
        position: fixed;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        overflow: hidden;
        padding: 0;
        margin: 0;
        background-color: #000;
        filter: alpha(opacity=50);
        opacity: 0.5;
        text-align: center;
        float: left;
        z-index: 99999999;
    }

    .progessposition {
        padding-top: 20%;
    }

    .clsFont {
        font-size: 13px;
        color: #4D4D4D !important;
    }

    .popup-inner p {
        color: #424242;
        font-size: 13px;
        padding: 0px 0px 0px 0px !important;
    }

    #divShowFareRules p {
        line-height: 20px;
    }

    #divShowFareRules strong {
        line-height: 35px;
    }

    .popup-inner .btn-right {
        width: 608px !important;
        padding-top: 10px !important;
    }
</style>
<script type="text/javascript">
    function ParentResult() {
        var dataurl = ('<%=ViewState["UrlP2PResult"] %>');
        window.parent.location = dataurl;
    }
    function ParentP2PBookingCartUrl(url) {
        var dataurl = url;
        window.parent.location = dataurl;
    }
    function GoInternationRailForm() {
        window.parent.location = "http://www.internationalrail.com.au/?form=offline";
    }
</script>
<asp:HiddenField ID="hdnDisplaySendJourney" runat="server" Value="false" />
<asp:UpdatePanel ID="upnlTrainSearch" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
        <asp:HiddenField ID="hdnDateDepart" runat="server" Value="" />
        <asp:HiddenField ID="hdnDateArr" runat="server" Value="" />
        <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
        <asp:HiddenField ID="hdnReq" runat="server" Value="" />
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" />
            </div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
            <div id="DivWarning" runat="server" class="warning" style="display: none;">
                <asp:Label ID="lblWarningMsg" runat="server" />
            </div>
        </asp:Panel>
        <div id="ParentDivPrice" style="display: none;">
            <span class="clsLoader" style="margin-top: 7px;" id="PriceProgress"></span><span>loading
                more prices...</span>
        </div>
        <div id="divPrices" style="display: none;">
            <asp:Button ID="btnFullFare" runat="server" CssClass="f-right w184 mar-t" Text="Get Full Fare Details"
                Style="margin-top: 7px; font-size: 14px; padding: 5px;" OnCommand="btnFullFare_Click" />
        </div>
        <asp:Repeater ID="rptTrainResult" runat="server" OnItemDataBound="rptTrainResult_ItemDataBound"
            OnItemCommand="rptTrainResult_ItemCommand">
            <HeaderTemplate>
                <asp:Label ID="lblHeaderInfo" runat="server" Text=""></asp:Label>
                <div class="booking-detail-in">
                    <table class="grid2" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th width="20%">Train#
                            </th>
                            <th width="25%">Departure
                            </th>
                            <th width="25%">Arrival
                            </th>
                            <th width="20%">Duration times
                            </th>
                            <th width="10%">Changes
                            </th>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblInBound" runat="server" Text="" />
                <asp:Label ID="lblOutBound" runat="server" Text="" />
                <tr class="trHead<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>">
                    <td>
                        <span class="imgOpen">Details</span>
                        <%#Eval("TrainNumber")%><br />
                        <%#Eval("TrainDescr")%>
                        <asp:Literal runat="server" Text='<%#Eval("TrainCategory")%>' ID="ltrTrainName" />
                        <asp:Literal runat="server" Text='<%#Eval("TripType")%>' ID="ltrTripType"
                            Visible="false" />
                    </td>
                    <td>
                        <span class="DepTime">
                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%>
                        </span>
                        <br />
                        <strong><span class="DepStName">
                            <%# GetNewStationsName(Eval("DepartureStationName").ToString())??Eval("DepartureStationName")%></span></strong><br />
                        <span class="time DepDate">
                            <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                        </span>
                    </td>
                    <td>
                        <span class="ArrTime">
                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span>
                        <br />
                        <strong><span class="ArrStName">
                            <%# GetNewStationsName(Eval("ArrivalStationName").ToString()) ?? Eval("ArrivalStationName")%>
                        </span></strong>
                        <br />
                        <span class="time ArrDate">
                            <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                        </span>
                    </td>
                    <td>
                        <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%>
                    </td>
                    <td>
                        <asp:Label ID="lblTrainChanges" runat="server" Text="" />
                    </td>
                </tr>
                <tr class="alt tr-<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>">
                    <td colspan="5">
                        <div class="extra-detail">
                            <asp:Repeater ID="GrdRouteInfo" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" class="detailtrain" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <th width="20%">Train#
                                            </th>
                                            <th width="30%">Departure
                                            </th>
                                            <th width="30%">Arrival
                                            </th>
                                            <th width="20%"></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("TrainNumber")%><br />
                                            <%#Eval("TrainCategory")%>
                                        </td>
                                        <td>
                                            <%#Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%><br />
                                            <%#Eval("DepartureStationName")%>
                                            <br />
                                            <%#Eval("DepartureDate")%>
                                        </td>
                                        <td>
                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%><br />
                                            <%#Eval("ArrivalStationName")%><br />
                                            <%#Eval("ArrivalDate")%>
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="detail-class">
                            <asp:HiddenField ID="hdnJournyTypeReturn" runat="server" Value='<%#Eval("IsReturn") %>' />
                            <asp:Button ID="btnGetPrice" runat="server" CssClass="lnkCont1" Text="Select" Visible="false"
                                CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                            <table cellpadding="0" cellspacing="0">
                                <div id="DivTr" runat="server" style="width: 100%;">
                                </div>
                            </table>
                        </div>
                        <div class="detail-class" style="border: none!important; background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;">
                            <asp:LinkButton ID="lnkContinue" runat="server" class="lnkCont" OnClientClick="return SendJCodeAndServiceID();">Continue</asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table> </div>
            </FooterTemplate>
        </asp:Repeater>
        <div style="float: left; width: 100%">
            <div style="float: left">
                <asp:Button ID="btnEarlier" runat="server" Text="Earlier Train" OnClick="btnEarlier_Click" />
                <asp:Button ID="btnLater" runat="server" Text="Later Train" OnClick="btnLater_Click" />
            </div>
            <asp:Button ID="btnChangeSearch" runat="server" Text="Change Search" Style="float: right;"
                OnClick="btnChangeSearch_Click" />
        </div>
        <asp:Repeater ID="rptBene" runat="server" OnItemDataBound="rptBene_ItemDataBound">
            <HeaderTemplate>
                <asp:Label ID="lblHeaderInfo" runat="server" Text=""></asp:Label>
                <div class="booking-detail-in">
                    <table class="grid2" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th width="20%">Train#
                            </th>
                            <th width="30%">Departure
                            </th>
                            <th width="20%">Duration times
                            </th>
                            <th width="20%">Changes
                            </th>
                            <th width="10%">Arrival
                            </th>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblInBound" runat="server" Text="" />
                <asp:Label ID="lblOutBound" runat="server" Text="" />
                <tr class="trHead<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>">
                    <td>
                        <span class="imgOpen">Details</span>
                        <br />
                        <asp:Literal runat="server" Text='<%#Eval("TrainCategory")%>' ID="ltrTrainName" />
                    </td>
                    <td>
                        <br />
                        <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                    </td>
                    <td>
                        <br />
                        <%# Convert.ToDateTime(Eval("TravelTimeDuration")).ToString("HH:mm")%>
                    </td>
                    <td>
                        <br />
                        <asp:Label ID="lblTrainChanges" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                        <br />
                        <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%>
                        &nbsp; &nbsp; &nbsp;
                        <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                    </td>
                </tr>
                <tr class="alt tr-<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>">
                    <td colspan="5">
                        <div class="extra-detail">
                            <asp:Repeater ID="GrdRouteInfo" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" class="detailtrain" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <th width="20%">Train#
                                            </th>
                                            <th width="30%">Departure
                                            </th>
                                            <th width="30%">Arrival
                                            </th>
                                            <th width="20%"></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("TrainNumber")%><br />
                                            <%#Eval("TrainCategory")%>
                                        </td>
                                        <td>
                                            <%#Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%><br />
                                            <%#Eval("DepartureStationName") ==null ?"": Eval("DepartureStationName") %><br />
                                            <%#Eval("DepartureDate")%>
                                        </td>
                                        <td>
                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm") %><br />
                                            <%#Eval("ArrivalStationName") == null ? "" : Eval("ArrivalStationName")%><br />
                                            <%#Eval("ArrivalDate")%>
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="detail-class">
                            <table cellpadding="0" cellspacing="0">
                                <div id="DivTr" runat="server" style="width: 100%;">
                                </div>
                            </table>
                        </div>
                        <div class="detail-class" style="border: none!important; background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;">
                            <asp:LinkButton ID="lnkContinue" runat="server" class="lnkCont" OnClientClick="return SendJCodeAndServiceID();">Continue</asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table> </div>
            </FooterTemplate>
        </asp:Repeater>
        <asp:Button ID="btnInEarlier" runat="server" Text="Earlier Train" OnClick="btnInEarlier_Click"
            Visible="false" />
        <asp:Button ID="btnOutLater" runat="server" Text="Later Train" OnClick="btnOutLater_Click"
            Visible="false" />
        <asp:Repeater ID="rptTcv" runat="server" OnItemDataBound="rptTcv_ItemDataBound">
            <HeaderTemplate>
                <asp:Label ID="lblHeaderInfo" runat="server" Text=""></asp:Label>
                <div class="booking-detail-in">
                    <table class="grid2" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th width="10%"></th>
                            <th width="30%">Departure
                            </th>
                            <th width="30%">Arrival
                            </th>
                            <th width="20%">Route
                            </th>
                            <th width="10%">Train Changes
                            </th>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblInBound" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblOutBound" runat="server" Text=""></asp:Label>
                <tr>
                    <td>
                        <%--<img src="images/arr-right02.png" alt="" border="0" class="imgOpen" />--%>
                        <span class="imgOpen">Details</span>
                    </td>
                    <td>
                        <strong>
                            <%#Eval("DepartureStationName")%>
                        </strong>
                    </td>
                    <td>
                        <strong>
                            <%#Eval("ArrivalStationName")%>
                        </strong>
                    </td>
                    <td>
                        <%#Eval("Route")%>
                    </td>
                    <td>
                        <asp:Label ID="lblTrainChanges" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr class="alt">
                    <td colspan="4">
                        <div class="extra-detail">
                            <asp:Repeater ID="GrdRouteInfo" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" class="detailtrain" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <th width="30%">Departure
                                            </th>
                                            <th width="30%">Arrival
                                            </th>
                                            <th width="40%">Via
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("DepartureStationName")%>
                                            <br />
                                        </td>
                                        <td>
                                            <%#Eval("ArrivalStationName")%>
                                            <br />
                                        </td>
                                        <td>
                                            <%#Eval("Via")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="detail-class">
                            <table cellpadding="0" cellspacing="0">
                                <div id="DivTr" runat="server" style="width: 100%;">
                                </div>
                            </table>
                        </div>
                        <div class="detail-class" style="border: none!important; background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;">
                            <asp:LinkButton ID="lnkContinue" runat="server" class="lnkCont" OnClientClick="return SendJCodeAndServiceID();">Continue</asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table> </div>
            </FooterTemplate>
        </asp:Repeater>
        <div style="float: left; padding-right: 5px; padding-top: 5px; text-align: right; width: 100%;">
            <asp:Button ID="btnNext" runat="server" CssClass="btn-red" Style="width: 15%!important; display: none;"
                Text="Continue" OnClientClick="return SendJCodeAndServiceID();" />
            <asp:Label ID="lblMsg" runat="server" Visible="False" CssClass="clsMsg"></asp:Label>
        </div>
        <div style="display: none;">
            <uc1:ucTrainSearch ID="ucTrainSearch" runat="server" />
        </div>
        <div style="display: none;">
            <asp:HyperLink ID="HyperLink2" runat="server" />
        </div>
        <asp:ModalPopupExtender ID="mdpexQuickLoad" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="HyperLink2" />
        <asp:Panel ID="pnlQuckLoad" runat="server" CssClass="modal-popup" Style="left: 32.5px !important; top: 155px !important;">
            <div class="popup-inner">
                <div class="title">
                    Please Enter Following Information
                </div>
                <div class="lt">
                    Lead Passenger Name :
                </div>
                <div class="rt">
                    <div style="width: 105px; float: left">
                        <asp:DropDownList ID="ddlTitle" runat="server" class="slbox01 clsInput" Style="width: 100px !important;"
                            TabIndex="95">
                            <asp:ListItem Value="0">--Title--</asp:ListItem>
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqTitle" ForeColor="Red" runat="server" ErrorMessage="*"
                            ControlToValidate="ddlTitle" InitialValue="0" Display="Dynamic" CssClass="errMsg"
                            ValidationGroup="cntnu" />
                    </div>
                    <div style="width: 145px; float: left">
                        <asp:TextBox ID="txtFirstname" runat="server" MaxLength="15" class="input" Width="130"
                            TabIndex="96" />
                        <asp:RequiredFieldValidator ID="reqFirstname" ForeColor="Red" runat="server" ErrorMessage="*"
                            ControlToValidate="txtFirstname" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                    </div>
                    <div style="width: 145px; float: left">
                        <asp:TextBox ID="txtLastname" runat="server" MaxLength="25" class="input" Width="130"
                            TabIndex="96" />
                        <asp:RequiredFieldValidator ID="reqLastname" ForeColor="Red" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLastname" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                    </div>
                </div>
                <div class="lt sibl">
                    Country of Residence :
                </div>
                <div class="rt  big-dropdown">
                    <asp:DropDownList ID="ddlCountry" runat="server" class="slbox01 clsInput" Style="width: 200px !important;"
                        TabIndex="97">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="ddlCountry" InitialValue="0" Display="Dynamic" CssClass="errMsg"
                        ValidationGroup="cntnu" />
                </div>
                <div class="clear">
                </div>
                <div id="divShowFareRules" class="fare-rules-list">
                </div>
                <div id="ShowCurrentDetail" style="display: none">
                </div>
                <div class="btn-right" style="background-color: #eee; padding: 5px 0px;">
                    <a href="http://www.internationalrail.com.au/information/terms-and-conditions.aspx"
                        target="_blank">International Rail’s conditions </a>
                    <asp:Button ID="btnContinue" runat="server" CssClass="btn-red newbutton" Style="float: right; margin-right: 7px; width: 100px !important;"
                        TabIndex="98" Text="Continue" ValidationGroup="cntnu"
                        OnClick="btnContinue_Click" />
                    <asp:Button ID="btnCloseWin" runat="server" Text="Cancel" CssClass="btn-black newbutton"
                        TabIndex="99" Style="width: 17% !important; float: right; margin-right: 7px;" />
                </div>
            </div>
        </asp:Panel>
        <%--Ti--%>
        <asp:HiddenField ID="hdnFromjsCode" runat="server" />
        <asp:HiddenField ID="hdnFromtrainNo" runat="server" />
        <asp:HiddenField ID="hdnFromtripType" runat="server" />
        <asp:HiddenField ID="hdnFromsvCode" runat="server" />
        <asp:HiddenField ID="hdnFromsvTypCode" runat="server" />
        <asp:HiddenField ID="hdnFromoCd" runat="server" />
        <asp:HiddenField ID="hdnFromoTypCd" runat="server" />
        <asp:HiddenField ID="hdnFromoSubCd" runat="server" />
        <asp:HiddenField ID="hdnFromagre" runat="server" />
        <asp:HiddenField ID="hdnFromClass" runat="server" />
        <asp:HiddenField ID="hdnTojsCode" runat="server" />
        <asp:HiddenField ID="hdnTotrainNo" runat="server" />
        <asp:HiddenField ID="hdnTotripType" runat="server" />
        <asp:HiddenField ID="hdnTosvCode" runat="server" />
        <asp:HiddenField ID="hdnTosvTypCode" runat="server" />
        <asp:HiddenField ID="hdnTooCd" runat="server" />
        <asp:HiddenField ID="hdnTooTypCd" runat="server" />
        <asp:HiddenField ID="hdnTooSubCd" runat="server" />
        <asp:HiddenField ID="hdnToagre" runat="server" />
        <asp:HiddenField ID="hdnToClass" runat="server" />
        <%--   Bene--%>
        <asp:HiddenField ID="hdnFromRid" runat="server" />
        <asp:HiddenField ID="hdnFromsvcTyp" runat="server" />
        <asp:HiddenField ID="hdnFromPriceId" runat="server" />
        <asp:HiddenField ID="hdnFromBeNeClass" runat="server" />
        <asp:HiddenField ID="hdnToRid" runat="server" />
        <asp:HiddenField ID="hdnTosvcTyp" runat="server" />
        <asp:HiddenField ID="hdnToPriceId" runat="server" />
        <asp:HiddenField ID="hdnToBeNeClass" runat="server" />
        <%--Ti TCV--%>
        <asp:HiddenField ID="hdnFrTcvRoute" runat="server" />
        <asp:HiddenField ID="hdnToTcvRoute" runat="server" />
        <%--Below radio buttons for the done selction while rdo buttons are not rendring from code side--%>
        <input type='radio' class='priceB' name='rdoFrom' style="display: none;" />
        <input type='radio' class='priceB' name='rdoTo' style="display: none;" />
        <input type='radio' class='priceB' name='rdoFromTI' style="display: none;" />
        <input type='radio' class='priceB' name='rdoToTI' style="display: none;" />
        <input type='radio' class='priceB' name='rdoFromTiTcv' style="display: none;" />
        <input type='radio' class='priceB' name='rdoToTiTcv' style="display: none;" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnFullFare" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnEarlier" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnLater" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnInEarlier" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnOutLater" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upnlTrainSearch"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="overlaybg" style="z-index: 999999;">
            &nbsp;
        </div>
        <div class="progess-inner2" style="left: 32%;">
            <strong>One Moment Please </strong>
            <img src="../images/dots32.gif" alt="progress bar" />
            <span>We are searching for your trains</span>
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    $(document).ready(function () {
        // Tooltip only Text
        $('.masterTooltip').hover(function () {
            // Hover over code
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip2"></p>')
        .text(title)
        .appendTo('body')
        .fadeIn('slow');
        }, function () {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip2').remove();
        }).mousemove(function (e) {
            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip2')
        .css({ top: mousey, left: mousex })
        });
    });
</script>
