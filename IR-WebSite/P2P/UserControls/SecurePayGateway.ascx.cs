﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI;

public partial class P2P_UserControls_SecurePayGateway : System.Web.UI.UserControl
{
    public string OrderNo;
    public string Amount = "";
    public int TotalAmount = 0;
    public int amount = 0;
    public string AmexPrices = "";
    private readonly db_1TrackEntities _db = new db_1TrackEntities();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params["req"] != null)
            {
                OrderNo = Request.Params["req"];
            }
            else if (Session["OrderID"] != null)
                OrderNo = Session["OrderID"].ToString();
            setOrderData();

            if (ddlCardType.Items.Count == 3)
            {
                ddlCardType.Items.Insert(1, new ListItem("American Express (" + Session["currencyCode"].ToString() + " " + AmexPrices + ")", "1"));
            }

            int startYear = DateTime.Now.Year;
            for (int i = startYear; i <= startYear + 10; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "GetPriceclick", "GetPriceclick();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params["req"] != null && Session["P2BookURL"] != null)
            {
                Response.Redirect(Session["P2BookURL"].ToString(), true);
                Session.Remove("P2BookURL");
            }
            else
                Response.Redirect("P2PBookingCart.aspx", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void setOrderData()
    {
        try
        {
            ManageBooking objB = new ManageBooking();
            List<GetAllCartData_Result> lst = objB.GetAllCartData(Convert.ToInt64(OrderNo));
            if (lst.Count() > 0)
            {
                PaymentGateWayTransffer objPT = Session["PayObj"] as PaymentGateWayTransffer;

                string count = lst.FirstOrDefault().DCountry;
                var tblcountry = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName == count);
                string IsoCode = tblcountry != null ? tblcountry.IsoCode : lst.FirstOrDefault().DCountry;
                Session["CustomerData"] = lst.FirstOrDefault().Address1 + " " + lst.FirstOrDefault().Address2 + ";" + lst.FirstOrDefault().City + ";" + lst.FirstOrDefault().Postcode + ";" + IsoCode + ";" + lst.FirstOrDefault().EmailAddress;
                tblSite objSite = new ManageJourney().GetSiteList().Where(a => a.ID == lst.FirstOrDefault().SiteID).FirstOrDefault();
                tblCurrencyMst objCurrency = new Masters().GetCurrencyList().Where(a => a.ID == objSite.DefaultCurrencyID).FirstOrDefault();
                string StTkProtnAmt = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(objPT.Amount), objSite.ID, objCurrency.ID, objSite.DefaultCurrencyID.HasValue ? objSite.DefaultCurrencyID.Value : new Guid()).ToString("F");
                //Session["Amount"] = Convert.ToDouble(StTkProtnAmt);
                AmexPrices = (Convert.ToSingle(StTkProtnAmt) * 3 / 100).ToString();
                Session["currencyCode"] = objCurrency.ShortCode;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void AmexPaymentExchange()
    {
        try
        {
            ManageBooking objB = new ManageBooking();
            List<GetAllCartData_Result> lst = objB.GetAllCartData(Convert.ToInt64(OrderNo));
            if (lst.Count() > 0)
            {
                PaymentGateWayTransffer objPT = Session["PayObj"] as PaymentGateWayTransffer;
                tblSite objSite = new ManageJourney().GetSiteList().Where(a => a.ID == lst.FirstOrDefault().SiteID).FirstOrDefault();
                tblCurrencyMst objCurrency = new Masters().GetCurrencyList().Where(a => a.ID == objSite.DefaultCurrencyID).FirstOrDefault();
                string StTkProtnAmt = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(lblTotalPrice.Text), objSite.ID, objCurrency.ID, objSite.DefaultCurrencyID.HasValue ? objSite.DefaultCurrencyID.Value : new Guid()).ToString("F");
                //Session["Amount"] = Convert.ToDouble(StTkProtnAmt); 
                Session["Amount"] = Math.Round(Convert.ToDecimal(StTkProtnAmt), 0, MidpointRounding.AwayFromZero);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            ClsErrorLog.AddError(Request.Url.ToString(), ex.Message + " :: " + ex.InnerException + " :: " + ex.StackTrace);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                Guid _siteId = Guid.Parse(Session["siteId"].ToString());
                if (new ManageBooking().IsFraudIPorCard(_siteId,txtCardNumber.Text, txtCardholder.Text))
                    Response.Redirect("P2PPaymentCancel.aspx");
            }
            lblTotalPrice.Text = hdnPrice.Value;
            AmexPaymentExchange();

            if (Session["Amount"] != null)
            {
                Amount = Session["Amount"].ToString();
            }
            amount = Convert.ToInt32(Amount) * 100;

            SecurePayGeatway obj = new SecurePayGeatway();
            SecurePayRequest request = new SecurePayRequest();
            string messageId = Guid.NewGuid().ToString().Replace("-", "");
            messageId = messageId.Substring(0, 30);
            request.MerchantInfo = new MerchantInfo
            {
                MerchantID = "INA0060",
                password = "abc123"
            };
            DateTime dt = DateTime.Now;
            string messageTimestamp = dt.Year.ToString()
                  + (dt.Day.ToString().Length == 1 ? "0" + dt.Day.ToString() : dt.Day.ToString())
                  + (dt.Month.ToString().Length == 1 ? "0" + dt.Month.ToString() : dt.Month.ToString())
                  + (dt.Hour.ToString().Length == 1 ? "0" + dt.Hour.ToString() : dt.Hour.ToString())
                  + (dt.Minute.ToString().Length == 1 ? "0" + dt.Minute.ToString() : dt.Minute.ToString())
                  + (dt.Second.ToString().Length == 1 ? "0" + dt.Second.ToString() : dt.Second.ToString())
                  + (dt.Millisecond.ToString().Length == 2 ? "0" + dt.Millisecond.ToString() : dt.Millisecond.ToString())
                  + "000+600";
            request.MessageInfo = new MessageInfo
            {
                apiVersion = "xml-4.2",
                messageID = messageId,
                messageTimestamp = messageTimestamp,
                timeoutValue = 60
            };
            request.Payment = new Payment
            {
                TxnList = new Txn
                {
                    currency = Session["currencyCode"].ToString(),
                    amount = amount,
                    ID = 1,
                    purchaseOrderNo = "IRWEB" + OrderNo,
                    CreditCardInfo = new CreditCardInfo
                    {
                        cardNumber = txtCardNumber.Text,
                        cvv = txtCardVarificationCode.Text,
                        expiryDate = ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue.Substring(2, 2) //05/15  
                    },
                    txnType = 0,
                    txnSource = 23,
                }
            };
            var BookingDetails = new ManageBooking().GetBookingDetailsByOrderId(Convert.ToInt64(OrderNo));
            string countrycode = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName.Contains(BookingDetails.Country)).CountryCode;
            request.ByerInfo = new ByerInfo
            {
                firstName = BookingDetails.FirstName,
                lastName = BookingDetails.LastName,
                billingCountry = countrycode,
                deliveryCountry = countrycode,
                emailAddress = BookingDetails.EmailAddress,
                ip = Request.ServerVariables["REMOTE_ADDR"].ToString(),
                zipCode = BookingDetails.Postcode,
                town = string.IsNullOrEmpty(BookingDetails.City) ? "" : BookingDetails.City
            };
            request.RequestType = "Payment";
            string payment = "https://api.securepay.com.au/xmlapi/payment";//XMLAPI Production 
            //string payment = "https://test.api.securepay.com.au/xmlapi/payment"; //XMLAPI Test

            string ctype = "";
            if (ddlCardType.SelectedValue == "3")
                ctype = "VISA";
            else if (ddlCardType.SelectedValue == "2")
                ctype = "Master Card";
            else if (ddlCardType.SelectedValue == "1")
                ctype = "American Express";
            string expCardDate = ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue.Substring(2, 2);

            Boolean newresult = new ManageBooking().UpdateOrderPaymentDetail(Convert.ToInt64(OrderNo), txtCardholder.Text, txtCardNumber.Text, messageId, expCardDate, ctype);
            SecurePayResponse result = obj.PaymentProcess(request, payment, Convert.ToInt64(OrderNo));
            if (result.approved != null)
            {
                if (result.statusCode == 0 && result.approved.ToLower() == "yes" && newresult)
                {
                    Session["key"] = Guid.NewGuid().ToString();
                    Session["OrderID"] = OrderNo;
                    Session["AmexPrice"] = hdnAmexCharge.Value.Trim();
                    new ManageBooking().UpdateOrderStatus(3, Convert.ToInt64(OrderNo));
                    new ManageBooking().UpdateOrderAmexPrice(Convert.ToInt64(OrderNo), Convert.ToDecimal(hdnAmexCharge.Value.Trim()));
                    Response.Redirect("OrderSuccessPage.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "btnSubmit_ClickHandler", "GetPriceclick();GetcardDetails();", true);
                    ShowMessage(2, "Transaction has been Declined or Failed, Please try again.");
                }
            }
            else
            {
                //-- redirct on cancel/declien page
                Response.Redirect("P2PPaymentCancel.aspx");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            ClsErrorLog.AddError("Securepay"+Request.Url.ToString(), ex.Message + " :: " + ex.InnerException + " :: " + ex.StackTrace);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}



