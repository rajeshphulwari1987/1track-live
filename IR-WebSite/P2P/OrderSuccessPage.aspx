﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderSuccessPage.aspx.cs"
    MasterPageFile="~/P2P/SiteMaster.master" Inherits="P2P_OrderSuccessPage" UICulture="en"
    Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <meta name="WT.si_cs" content="1" />
    <meta name="WT.tx_e" content="p" />
    <%=WTpnsku%>
    <%=WTtxu%>
    <%=WTtxs%>
    <%=WTtxI%>
    <%=WTtxid%>
    <%=WTtxit%>
    <style type="text/css">
        /*booking detail*/
        .toogle
        {
            display: block !important;
        }
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
    </style>
    <script type="text/javascript">
        window.history.forward();
        window.history.forward();
    </script>
    <script type="text/javascript">
        window.universal_variable = 
        {           
            version: "1.1.0"
        }; 
        window.universal_variable.transaction =  <%=products %>;
 
    </script>
    <script type="text/javascript">
        function ParentHomeUrl(url) {
            var dataurl = url;
            window.parent.location = dataurl;
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="content">
        <div class="left-content">
            <h1>
                Thanks for your order,&nbsp;<asp:Label ID="lblClientName1" runat="server" Text=""></asp:Label>
            </h1>
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div id="divPrintGrid" runat="server">
                <div class="round-titles">
                    Order Information
                </div>
                <div class="booking-detail-in">
                    <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" width="47%">
                                Name :
                            </td>
                            <td width="50%">
                                <asp:Label ID="lblClientName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Email Address :
                            </td>
                            <td>
                                <asp:Label ID="lblEmailAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Delivery Address</b>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblDeliveryAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Order Summary:</b>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Order Date:
                            </td>
                            <td>
                                <asp:Label ID="lblOrderDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Order No.:
                            </td>
                            <td>
                                <asp:Label ID="lblOrderNumber" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptOrderInfo" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td align="left">
                                        Journey
                                        <%# Container.ItemIndex + 1 %>:
                                    </td>
                                    <td>
                                        <%#Eval("ProductDesc")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Price :
                                    </td>
                                    <td>
                                        <%=currency %>
                                        <%#Eval("Price").ToString()%>
                                    </td>
                                </tr>
                                <%#(Eval("TktPrtCharge")) != "" ? "<tr><td>Ticket Protection: </td><td>" + currency +" "+ Eval("TktPrtCharge")+"</td></tr>" : ""%>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td align="left">
                                Gross Price:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Shipping:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblShippingAmount" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Booking Fee:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblBookingFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Net Total :</b>
                            </td>
                            <td>
                                <b>
                                    <%=currency %>
                                    <asp:Label ID="lblNetTotal" runat="server" Text=""></asp:Label></b>
                            </td>
                        </tr>
                        <tr id="trDiscount" runat="server">
                            <td align="left">
                                Discount:
                            </td>
                            <td>
                                <%=currency %>
                                <asp:Label ID="lblDiscount" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Order Total :</b>
                            </td>
                            <td>
                                <b>
                                    <%=currency %>
                                    <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label></b>
                            </td>
                        </tr>
                        <tr id="trTrenItalia" runat="server" visible="false">
                            <td align="left" colspan="2" style="color: Red">
                                <b>IMPORTANT:</b> This PNR is issued as a ticket on depart and must be printed from
                                one of the self-service ticket machines located at all main Trenitalia stations
                                (which are green, white and red). Once you have printed your ticket you must validate
                                it in one of the validation machines located near to the entrance of each platform
                                (they are green and white with Trenitalia written on the front and usually mounted
                                on a wall), THIS MUST BE DONE BEFORE BOARDING YOUR TRAIN, failure to do so will
                                result in a fine.
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:Button ID="btnpdf" OnClick="btnPdf_Click" runat="server" Text="Print Ticket"
                CssClass="btn-red-cart w184 f-right" Visible="false" />
            <asp:Button ID="btnContinue" Text="Continue Shopping" CssClass="btn-red-cart w184 f-right"
                Visible="false" Width="160px" runat="server" OnClick="btnContinue_Click" />
        </div>
    </div>
    <asp:HiddenField ID="hdnUserName" runat="server" />
</asp:Content>
