﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="P2pTrainResults.aspx.cs"
    MasterPageFile="~/P2P/SiteMaster.master" Inherits="P2P_P2pTrainResults" UICulture="en"
    Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/P2P/UserControls/ucTrainSearchResult.ascx" TagName="TrainSearchResult"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function ParentHomeUrl(url) {
            var dataurl = url;
            //window.parent.location = "http://localhost" + dataurl;
            window.parent.location = "http://www.internationalrail.com.au" + dataurl;
        }
    </script>
    <style type="text/css">
        #btnChangeSearch
        {
            float: right;
            margin-bottom: 5px;
            margin-right: 253px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
        <Scripts>
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.core.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.datepicker.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.widget.js" />
        </Scripts>
    </asp:ToolkitScriptManager>
    <h1 style="padding-left: 15px !important; padding-top: 10px !important;">
        <asp:Label ID="lblHeading" runat="server" Text="Train Search Result" />
    </h1>
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="updLoyalty" runat="server">
        <ContentTemplate>
            <div class="left-content" id="DivLeftOne" runat="server">
                <uc2:TrainSearchResult runat="server" ID="ucSResult" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
