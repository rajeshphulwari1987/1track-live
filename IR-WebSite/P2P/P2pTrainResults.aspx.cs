﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using OneHubServiceRef;

public partial class P2P_P2pTrainResults : System.Web.UI.Page
{
    private tblP2PSetting _tblP2PSetting = new tblP2PSetting();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageP2PSettings _manageP2PSettings = new ManageP2PSettings();
    public string AdminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private string _directoryUrl, _url;
    public string SiteURL;
    private Guid _siteId;
    public string Script;

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                SiteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {

                Uri parenturl = new Uri(Request.UrlReferrer.OriginalString.ToString());
                string qyr = parenturl.Query;
                System.Collections.Specialized.NameValueCollection col = HttpUtility.ParseQueryString(qyr);

                if (col["key"] != null && col["status"] != null && col["OrderNo"] != null)
                {
                    string newUrl = "OrderSuccessPage.aspx?OrderNo=" + col["OrderNo"] + "&status=" + col["status"] + "&key=" + col["key"] + "";
                    Response.Redirect(newUrl);
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}