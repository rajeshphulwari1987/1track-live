﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using OneHubServiceRef;

public partial class P2P_Agent_PrintTicket : System.Web.UI.Page
{
    #region Global Variables
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageP2PSettings _manageP2PSettings = new ManageP2PSettings();
    public string AdminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string SiteURL;
    private Guid _siteId;
    public string Script;
    private tblP2PSetting _tblP2PSetting = new tblP2PSetting();
    private string _directoryUrl, _url;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                SiteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Script = new Masters().GetQubitScriptBySId(_siteId);
                
                if (Session["TicketBooking-REPLY"] != null && hdnExec.Value != "1")
                {
                    List<PrintResponse> list = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                    if (list.Count != 0)
                    {
                        ltrTicketMsg.Visible = !string.IsNullOrEmpty(list.FirstOrDefault().URL);

                        if (list.Any(x => x.URL.Contains("#")))
                        {
                            List<PrintResponse> newlist = new List<PrintResponse>();
                            foreach (var item in list)
                            {
                                string[] urls = item.URL.Split('#');
                                foreach (var url in urls)
                                {
                                    if (url.Length > 32)
                                        newlist.Add(new PrintResponse
                                        {
                                            URL = url
                                        });
                                }
                            }
                            list = newlist;
                        }

                        rptBeNePrint.DataSource = list;
                        rptBeNePrint.DataBind();
                        hdnExec.Value = "1";
                    }
                }
            }
            if (ViewState["homePageUrl"] == null)
            {
                _tblP2PSetting = _manageP2PSettings.GetP2PSettingBySiteId(_siteId);
                if (_tblP2PSetting != null)
                {
                    _directoryUrl = _tblP2PSetting.DirectoryPath;
                    var v = _manageP2PSettings.GetP2PSettingBySiteId(_siteId);
                    if (v != null)
                    {
                        ViewState["homePageUrl"] = _directoryUrl + "" + v.P2PWidgetPage;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void rptBeNePrint_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var divPrintAtHome = e.Item.FindControl("divPrintAtHome") as HtmlControl;
                if (Session["showPrintAtHome"] != null)
                {
                    if (Session["showPrintAtHome"].ToString() == "1")
                        if (divPrintAtHome != null)
                            divPrintAtHome.Visible = true;
                }
                Session.Remove("divPrintAtHome");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            _url = ViewState["homePageUrl"].ToString();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "msg", "ParentHomeUrl('" + _url + "');", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
}