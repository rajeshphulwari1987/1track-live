﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="P2PTrainTickets.aspx.cs"
    Inherits="P2P_P2PTrainResult" MasterPageFile="~/P2P/SiteMaster.master" %>

<%@ Register Src="~/P2P/UserControls/ucTrainResults.ascx" TagName="TrainResult" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc2:TrainResult runat="server" ID="transrch"></uc2:TrainResult>
</asp:Content>
