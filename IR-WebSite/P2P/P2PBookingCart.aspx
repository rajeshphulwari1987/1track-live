﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="P2PBookingCart.aspx.cs" Inherits="P2P_P2PBookingDetails"
    MasterPageFile="~/P2P/SiteMaster.master" UICulture="en" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucTicketDelivery" Src="~/P2P/UserControls/ucTicketDelivery.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <link id="lnkBookStyle" runat="server" href="Styles/BookingCart.css" rel="stylesheet"
        type="text/css" />
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.widget.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipCity]')[0], false);
            ValidatorEnable($('[id*=BookingpassrfshipState]')[0], false);

            $("#ContentPlaceHolder1_txtDateOfDepature").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            showHideShipping();
            $("#radioChkShp").attr("checked", "checked");
            ShowCall();
            getdata();
            restrictCopy();

            $(".imgCal").click(function () {
                $("#ContentPlaceHolder1_txtDateOfDepature").datepicker('show');
            });
        });

        function callshipping() {
            getdata();
            alert("Please choose shipping amount.");
        }
        function restrictCopy() {


            $(function () {
                $('#ContentPlaceHolder1_txtConfirmEmail').bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
                $('#ContentPlaceHolder1_txtshpConfirmEmail').bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
            });
        }
        function ShowCall() {

            $(".calDate, #calImg").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0
            });
            $(".calDate").keypress(function (event) { event.preventDefault(); });
            var date = $(".dateofDeparture:eq(0)").text();
            date = date.split('/');
            var d = new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0]));
            if ($("#ContentPlaceHolder1_ucTicketDelivery_rdoBkkoingList_0").val() == "DH") {
                $(".calDate, #calImg").datepicker("option", "minDate", '<%=PrintathomeDate %>');
            }
            else {
                $(".calDate, #calImg").datepicker("option", "minDate", '<%=TodayDate %>');
            }
            $(".calDate, #calImg").datepicker("option", "maxDate", d);
            $(".calDate, #calImg").val("DD/MM/YYYY");
        }
        function getdata() {
            $(document).ready(function () {
                var countPrice = 0.00;
                var bookingFee = 0.00;
                $(".calculatePrice").each(function () {
                    var price = $(this).text() == '' ? 0 : $(this).text();
                    countPrice = countPrice + parseFloat(price);
                });
                bookingFee = parseFloat($.trim($("#ContentPlaceHolder1_rptTrain_lblBookingFee").text()));
                countPrice = bookingFee + countPrice;
                $("#lblTotal").text(countPrice.toFixed(2));
                $(".shipping").each(function () {
                    if ($(this).is(":checked")) {
                        var str = $(this).val().split('^');

                        var price = str[0];
                        var smethod = str[1];
                        var sdesc = str[2];
                        sdesc = $('<div/>').html(sdesc).text();

                        $("#ContentPlaceHolder1_hdnShippingCost").attr('value', price.toString());
                        $("#ContentPlaceHolder1_hdnShipMethod").attr('value', smethod.toString());
                        $("#ContentPlaceHolder1_hdnShipDesc").attr('value', sdesc.toString());

                        var deliveryMethod = ($("#ContentPlaceHolder1_ucTicketDelivery_hiddenDileveryMethod").attr('value'));
                        if (deliveryMethod == "TA") {
                            countPrice = countPrice + parseFloat(price);
                        }
                    }
                });
                $("#ContentPlaceHolder1_lblTotalCost").text(countPrice.toFixed(2));
            });
        }
    </script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function showHideShipping() {
            if ($("#ContentPlaceHolder1_ucTicketDelivery_hiddenDileveryMethod").val() == "DH" || $("#ContentPlaceHolder1_ucTicketDelivery_hiddenDileveryMethod").val() == "ST" || $("#ContentPlaceHolder1_ucTicketDelivery_hiddenDileveryMethod").val() == "TL") {
                $("#ContentPlaceHolder1_pnlShipping").hide();
                $("#ContentPlaceHolder1_hdnShippingCost").val("0");
                $("#ContentPlaceHolder1_lblTotalCost").text($("#lblTotal").text());
            }
            else {
                getdata();
                $("#ContentPlaceHolder1_pnlShipping").show();
                var shipping = $("#ContentPlaceHolder1_hdnShippingCost").val();
                var subTotal = $("#lblTotal").text();
                var total = parseFloat(subTotal) + parseFloat(shipping);
                $("#ContentPlaceHolder1_lblTotalCost").text(total.toFixed(2));
            }
        }

        function showmessagecountry() {
            $(".chkcheckbox,.uncheckbox").remove();
            $(".uncheckradiobox,.chkcheckradiobox").remove();
        }

        $(function () {
            /*shipping details*/
            $("#ContentPlaceHolder1_chkShippingfill").click(function () {
                if ($(this).is(':checked')) {
                    $("#ContentPlaceHolder1_pnlbindshippping").show();
                    ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipCity]')[0], true);
                    ValidatorEnable($('[id*=BookingpassrfshipState]')[0], true);

                    $("#ContentPlaceHolder1_ddlshpMr").prop("selectedIndex", $("#ContentPlaceHolder1_ddlMr").prop("selectedIndex"));
                    $("#ContentPlaceHolder1_txtshpfname").val($("#txtFirst").val());
                    $("#ContentPlaceHolder1_txtshpLast").val($("#ContentPlaceHolder1_txtLast").val());
                    $("#ContentPlaceHolder1_txtshpEmail").val($("#ContentPlaceHolder1_txtEmail").val());
                    $("#ContentPlaceHolder1_txtshpConfirmEmail").val($("#ContentPlaceHolder1_txtConfirmEmail").val());
                    $("#ContentPlaceHolder1_txtShpPhone").val($("#ContentPlaceHolder1_txtBillPhone").val());
                    $("#ContentPlaceHolder1_txtshpAdd").val($("#ContentPlaceHolder1_txtAdd").val());
                    $("#ContentPlaceHolder1_txtshpAdd2").val($("#ContentPlaceHolder1_txtAdd2").val());
                    $("#ContentPlaceHolder1_txtshpCity").val($("#ContentPlaceHolder1_txtCity").val());
                    $("#ContentPlaceHolder1_txtshpState").val($("#ContentPlaceHolder1_txtState").val());
                    $("#ContentPlaceHolder1_txtshpZip").val($("#ContentPlaceHolder1_txtZip").val());
                    $("#ContentPlaceHolder1_ddlshpCountry").prop("selectedIndex", $("#ContentPlaceHolder1_ddlCountry").prop("selectedIndex"));
                }
                else {
                    $("#ContentPlaceHolder1_pnlbindshippping").hide();
                    ValidatorEnable($('[id*=BookingpassrfshipFirstName]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipLastName]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipEmail]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfconfirmShipEmail]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpPhone]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpAdd]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpZip]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshpCountry]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipCity]')[0], false);
                    ValidatorEnable($('[id*=BookingpassrfshipState]')[0], false);
                }
            });

            /*tricketprotecton*/
            $('.calculateTotal').click(function () {
                var pricewithTicketprotection = Priceupdate = 0;
                var chktrue = $(this).find('input[id*=chkTicketProtection]').prop('checked');
                var currsyb = $(this).parent().find('span[id*=currsyb]');
                var priceTP = $(this).parent().find('span[id*=lbltpPrice]');
                var price = $(this).parent().next().find('span[id*=lblPrice]');
                var PassSaleID = $(this).parent().next().find('span[id*=lblPassSaleID]');
                var HidPriceValue = $(this).parent().next().find('span[id*=lblHidPriceValue]');
                var passID = (PassSaleID.text() == '' ? '00000000-0000-0000-0000-000000000000' : PassSaleID.text());
                if (chktrue) {
                    pricewithTicketprotection = parseFloat(priceTP.text()) + parseFloat(price.text());
                    Priceupdate = (parseFloat(priceTP.text() == '' ? 0 : priceTP.text()).toFixed(2));
                    currsyb.css('color', '#000');
                    priceTP.css('color', '#000');
                }
                else {
                    pricewithTicketprotection = parseFloat(HidPriceValue.text());
                    Priceupdate = 0;
                    currsyb.css('color', '#ccc');
                    priceTP.css('color', '#ccc');
                }
                price.text(pricewithTicketprotection.toFixed(2));
                getdata();
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var State1 = $("#ContentPlaceHolder1_ucTicketDelivery_ddlStateMail option:selected").text();
            $("#ContentPlaceHolder1_txtState").val(State1);

            $("#ContentPlaceHolder1_ucTicketDelivery_txtMailFName").blur(function () {
                var FName = $("#ContentPlaceHolder1_ucTicketDelivery_txtMailFName").val();
                $("#txtFirst").val(FName);
            });
            $("#ContentPlaceHolder1_ucTicketDelivery_txtMailLName").blur(function () {
                var LName = $("#ContentPlaceHolder1_ucTicketDelivery_txtMailLName").val();
                $("#ContentPlaceHolder1_txtLast").val(LName);
            });
            $("#ContentPlaceHolder1_ucTicketDelivery_txtStreet").blur(function () {
                var Address = $("#ContentPlaceHolder1_ucTicketDelivery_txtStreet").val();
                $("#ContentPlaceHolder1_txtAdd").val(Address);
            });
            $("#ContentPlaceHolder1_ucTicketDelivery_txtMailCity").blur(function () {
                var City = $("#ContentPlaceHolder1_ucTicketDelivery_txtMailCity").val();
                $("#ContentPlaceHolder1_txtCity").val(City);
            });
            $("#ContentPlaceHolder1_ucTicketDelivery_txtPostalCode").blur(function () {
                var PostalCode = $("#ContentPlaceHolder1_ucTicketDelivery_txtPostalCode").val();
                $("#ContentPlaceHolder1_txtZip").val(PostalCode);
            });
            $("#ContentPlaceHolder1_ucTicketDelivery_ddlStateMail").change(function () {
                var State = $("#ContentPlaceHolder1_ucTicketDelivery_ddlStateMail option:selected").text();
                $("#ContentPlaceHolder1_txtState").val(State);
            });
            $("#ContentPlaceHolder1_ucTicketDelivery_ddlCountryMail").change(function () {
                var Country = $("#ContentPlaceHolder1_ucTicketDelivery_ddlCountryMail option:selected").text();
                $('#ContentPlaceHolder1_ddlCountry').val(Country).attr("selected", "selected");
            });

        });
    </script>
    <script type="text/javascript">
        function ErrorMessageShow(Message) {
            var value = Message.toString();
            if (value != '' || value != null) {
                $('#ContentPlaceHolder1_DivError').removeAttr('style');
                $('#ContentPlaceHolder1_DivError').attr('style', 'display:block;');
                $('#ContentPlaceHolder1_DivSuccess').removeAttr('style');
                $('#ContentPlaceHolder1_DivSuccess').attr('style', 'display:none;');
                $('#ContentPlaceHolder1_lblErrorMsg').text(value);
                $('#ContentPlaceHolder1_lblSuccessMsg').text('');
            }
        }

        function checklength(id) {
            if ($("#" + id).val().length < 2) {
                $("#" + id).val('');
                alert('Please enter atleast 2 character of name');
                return false;
            }
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnBookingFee" runat="server" Value="0.00" />
    <asp:HiddenField ID="hdnShipMethod" runat="server" Value="" />
    <asp:HiddenField ID="hdnShipDesc" runat="server" Value="" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="content">
        <div class="left-content" style="width: 100%">
            <h1>
                Booking Details
            </h1>
            <asp:Panel ID="pnlErrSuccess" runat="server" Style="width: 1000px;">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="round-titles" style="width: 900px;">
                Review
            </div>
            <div class="booking-detail-in" style="padding: 0; width: 905px;">
                <asp:Repeater ID="rptTrain" runat="server" OnItemDataBound="rptTrain_ItemDataBound">
                    <HeaderTemplate>
                        <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="100%" colspan="3" style="background-color: #5e5e5e; color: White;">
                                <%#Eval("PassDesc")%>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%">
                                <%#Eval("Traveller")%>
                            </td>
                            <td width="20%" style="vertical-align: bottom">
                                <asp:Label ID="lblTckProc" runat="server" Text="Ticket Protection" />
                            </td>
                            <td width="40%" style="vertical-align: bottom">
                                <asp:UpdatePanel ID="updTrain" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        &nbsp;&nbsp;&nbsp;
                                        <div id="divTckProt" runat="server" style="float: left">
                                            <asp:Label runat="server" ID="currsyb" Text='<%#currency%>'></asp:Label>
                                            <asp:Label ID="lbltpPrice" runat="server" Text='<%#Eval("TicketProtectionPrice") %>' />
                                            <asp:CheckBox ID="chkTicketProtection" CssClass="calculateTotal" runat="server" Checked='<%#Eval("TicketProtection")%>' />
                                            <a onclick="showthis()" href="#">
                                                <img src="../images/info.png" width="20" /></a>
                                        </div>
                                        <div style="float: right; line-height: 8px;">
                                            <br />
                                            <%=currency%>
                                            <asp:Label ID="lblPrice" CssClass="calculatePrice" runat="server" Text='<%#Eval("Price")%>'
                                                Style="margin-right: 8px;" />
                                            <asp:Label ID="lblPassSaleID" runat="server" Text='<%#Eval("ProductID")%>' Style="display: none;" />
                                            <asp:Label ID="lblHidPriceValue" runat="server" Text='<%#Eval("Price")%>' Style="display: none;" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="chkTicketProtection" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" colspan="3" style="background-color: #5e5e5e; color: White;">
                                    Booking Fee
                                </td>
                            </tr>
                            <tr>
                                <td width="40%">
                                    &nbsp;
                                </td>
                                <td width="20%">
                                    &nbsp;
                                </td>
                                <td width="40%">
                                    <div style="float: right">
                                        <%=currency%>
                                        <asp:Label ID="lblBookingFee" runat="server" Text="" Style="margin-right: 8px;"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="total">
                            Sub Total:
                            <%=currency%>
                            <asp:Label ID="lblTotal" ClientIDMode="Static" runat="server"></asp:Label>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:Repeater ID="rptTrainTcv" runat="server" OnItemCommand="rptTrainTcv_ItemCommand"
                    OnItemDataBound="rptTrainTcv_ItemDataBound">
                    <HeaderTemplate>
                        <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr style="background-color: #5e5e5e;">
                                <th width="50%">
                                    &nbsp;
                                </th>
                                <th width="15%">
                                    &nbsp;
                                </th>
                                <th width="10%">
                                    &nbsp;
                                </th>
                                <th width="25%" valign="middle" colspan="2">
                                    Ticket Protection <a onclick="showthis()" href="#">
                                        <img src="../images/info.png" width="17" /></a>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                Ticket (TCV):
                                <asp:Label ID="lblFrm1" runat="server" Text='<%#Eval("DepartureStation")%>' />
                                -
                                <asp:Label ID="lblTo1" runat="server" Text='<%#Eval("ArrivalStation")%>' />
                                <br />
                                From:
                                <asp:Label ID="lblFrm" runat="server" Text='<%#Eval("DepartureStation")%>' />
                                <br />
                                To:
                                <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ArrivalStation")%>' />
                                <br />
                                <asp:Label ID="lblService" runat="server" Text='<%#Eval("ServiceName")%>' />
                                <asp:Label ID="lblPngrType" runat="server" Text='<%#Eval("Passenger")%>' />
                                <br />
                                <asp:Label ID="lblFare" runat="server" Text='<%#Eval("Fare")%>' />
                            </td>
                            <td>
                                <%#Eval("Title")%>
                                <%#Eval("FirstName")%>
                                <%#Eval("LastName")%>
                            </td>
                            <td>
                                <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("Price")%>' />
                            </td>
                            <td width="10%" valign="middle">
                                <input type="checkbox" />
                            </td>
                            <td width="15%" valign="middle">
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%#Eval("Id")%>' CommandName="Remove"><img src='../images/icon-delete-new.png' /></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        <div class="total">
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc:ucTicketDelivery ID="ucTicketDelivery" runat="server" />
            <div class="round-titles">
                Billing Address
                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="vgs1" DisplayMode="List"
                    ShowSummary="false" ShowMessageBox="true" runat="server" Enabled="true" />
            </div>
            <div class="booking-detail-in delvery" style="height: 383px; width: 886px; height: 425px">
                <div class="colum-label-first">
                    Title
                </div>
                <div class="colum-label-second">
                    <asp:DropDownList ID="ddlMr" runat="server" class="inputsl">
                        <asp:ListItem Text="Mr." Value="1">
                        </asp:ListItem>
                        <asp:ListItem Text="Miss" Value="2">
                        </asp:ListItem>
                        <asp:ListItem Text="Mrs." Value="3">
                        </asp:ListItem>
                        <asp:ListItem Text="Ms" Value="4">
                        </asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="colum-label-third">
                    First Name<span class="readerror">*</span></div>
                <div class="colum-label-forth">
                    <asp:TextBox ID="txtFirst" runat="server" CssClass="input validcheck" ClientIDMode="Static" />
                    <asp:RequiredFieldValidator ID="BookingpassrfFirst" runat="server" Text="*" ErrorMessage="Please enter first name."
                        ControlToValidate="txtFirst" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-fifth">
                    Last name<span class="readerror">*</span></div>
                <div class="colum-label-sixth">
                    <asp:TextBox ID="txtLast" runat="server" CssClass="input validcheck" />
                    <asp:RequiredFieldValidator ID="BookingpassrfLast" runat="server" Text="*" ErrorMessage="Please enter last name."
                        ControlToValidate="txtLast" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-first">
                    Email<span class="readerror">*</span></div>
                <div class="colum-label-second">
                    &nbsp;
                </div>
                <div class="colum-label-third">
                    &nbsp;
                </div>
                <div class="colum-label-forth">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input validcheck" autocomplete="off"
                        Style='text-transform: lowercase'>
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="BookingpassrfEmail" runat="server" Text="*" ErrorMessage="Please enter email address."
                        ControlToValidate="txtEmail" ForeColor="#eaeaea" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="BookingpassrevEmail" runat="server" Text="*"
                        ErrorMessage="Please enter a valid email." ControlToValidate="txtEmail" ForeColor="#eaeaea"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                        ValidationGroup="vgs1" />
                </div>
                <div class="colum-label-fifth">
                    Confirm Email<span class="readerror">*</span></div>
                <div class="colum-label-sixth">
                    <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="input validcheck" autocomplete="off"
                        Style='text-transform: lowercase' />
                    <asp:RequiredFieldValidator ID="BookingpassrfvEmail2" runat="server" Text="*" ErrorMessage="Please enter confirm email."
                        ControlToValidate="txtConfirmEmail" ForeColor="#eaeaea" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="BookingpassregEmail2" runat="server" Text="*"
                        ErrorMessage="Please enter valid confirm email." ControlToValidate="txtConfirmEmail"
                        ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic" ValidationGroup="vgs1" />
                    <asp:CompareValidator ID="cmpEmailCompare" ForeColor="#eaeaea" ControlToCompare="txtEmail"
                        Display="Dynamic" ControlToValidate="txtConfirmEmail" runat="server" Text="*"
                        ValidationGroup="vgs1" ErrorMessage="Email and confirm email not matched.">
                    </asp:CompareValidator>
                </div>
                <div class="colum-label-seven">
                    Phone Number<span class="readerror">*</span>
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtBillPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                        MaxLength="20" onkeypress="return isNumberKey(event)" />
                    <asp:RequiredFieldValidator ID="BookingpassrfPhone" runat="server" Text="*" ErrorMessage="Please enter phone number."
                        ControlToValidate="txtBillPhone" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Address Line 1 Or Company Name<span class="readerror">*</span>
                </div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtAdd" runat="server" CssClass="input validcheck" MaxLength="50" />
                    <asp:RequiredFieldValidator ID="BookingpassrfAdd" runat="server" Text="*" ErrorMessage="Please enter address."
                        ControlToValidate="txtAdd" ForeColor="#eaeaea" />
                </div>
                <div class="colum-label-seven">
                    Address Line 2
                </div>
                <div class="colum-label-eight" style="height: auto;" maxlength="50">
                    <asp:TextBox ID="txtAdd2" runat="server" CssClass="input" />
                </div>
                <div class="colum-label-seven">
                    Town / City<span class="readerror">*</span></div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="input" />
                    <asp:RequiredFieldValidator ID="reqCity" runat="server" Text="*" ErrorMessage="Please enter town/city"
                        ControlToValidate="txtCity" ForeColor="#eaeaea" ValidationGroup="vgs1" />
                </div>
                <div class="colum-label-seven">
                    County / State<span class="readerror">*</span></div>
                <div class="colum-label-eight ">
                    <asp:TextBox ID="txtState" runat="server" CssClass="input" />
                    <asp:RequiredFieldValidator ID="reqState" runat="server" Text="*" ErrorMessage="Please enter state"
                        ControlToValidate="txtState" ForeColor="#eaeaea" ValidationGroup="vgs1" />
                </div>
                <div class="colum-label-seven">
                    Postal/Zip Code<span class="readerror">*</span></div>
                <div class="colum-label-eight">
                    <asp:TextBox ID="txtZip" runat="server" CssClass="input validcheck" Width="44%" MaxLength="10" />
                    <asp:RequiredFieldValidator ID="BookingpassrfZip" runat="server" Text="*" ErrorMessage="Please enter postal/zip code."
                        ControlToValidate="txtZip" ForeColor="#eaeaea" />
                    <asp:Label ID="lblpmsg" runat="server" Visible="false" ForeColor="Red" Text="Please enter maximum 7 character postal/zip code." />
                </div>
                <div class="colum-label-seven">
                    Country<span class="readerror">*</span></div>
                <div class="colum-label-eight countory">
                    <asp:DropDownList ID="ddlCountry" runat="server" class="inputsl" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <asp:RequiredFieldValidator ID="BookingpassrfCountry" runat="server" Text="*" ErrorMessage="Please select country."
                        ControlToValidate="ddlCountry" InitialValue="0" ForeColor="#eaeaea" />
                    <asp:Label ID="lblcountryMsg" runat="server" ForeColor="Red" Visible="false">Please select Australia country.</asp:Label>
                </div>
                <div class="colum-label-seven">
                    Amend Shipping Details if different to billing address
                </div>
                <div class="colum-label-eight">
                    <asp:CheckBox ID="chkShippingfill" runat="server" />
                </div>
                <asp:Label runat="server" ID="lblmsg" />
            </div>
            <%--Shipping Address Start--%>
            <asp:Panel ID="pnlbindshippping" runat="server" Style="display: none;">
                <div class="round-titles">
                    Shipping Address
                </div>
                <div class="booking-detail-in delvery" style="width: 886px; pointer-events: none;">
                    <div class="colum-label-first">
                        Title
                    </div>
                    <div class="colum-label-second">
                        <asp:DropDownList ID="ddlshpMr" runat="server" class="inputsl">
                            <asp:ListItem Text="Mr." Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="Miss" Value="2">
                            </asp:ListItem>
                            <asp:ListItem Text="Mrs." Value="3">
                            </asp:ListItem>
                            <asp:ListItem Text="Ms" Value="4">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="colum-label-third">
                        First Name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-forth">
                        <asp:TextBox ID="txtshpfname" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipFirstName" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping first name." ControlToValidate="txtshpfname"
                            ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-fifth">
                        Last name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-sixth">
                        <asp:TextBox ID="txtshpLast" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipLastName" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping last name." ControlToValidate="txtshpLast"
                            ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-first">
                        Email<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-second">
                        &nbsp;
                    </div>
                    <div class="colum-label-third">
                        &nbsp;
                    </div>
                    <div class="colum-label-forth">
                        <asp:TextBox ID="txtshpEmail" runat="server" CssClass="input validcheck" autocomplete="off"
                            Style='text-transform: lowercase' />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipEmail" runat="server" Text="*" ErrorMessage="Please enter Shipping email address."
                            ControlToValidate="txtshpEmail" ForeColor="#eaeaea" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="regx1" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                            ControlToValidate="txtshpEmail" ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                    </div>
                    <div class="colum-label-fifth">
                        Confirm Email<span class="readerror">*</span></div>
                    <div class="colum-label-sixth">
                        <asp:TextBox ID="txtshpConfirmEmail" runat="server" CssClass="input validcheck" autocomplete="off"
                            Style='text-transform: lowercase' />
                        <asp:RequiredFieldValidator ID="BookingpassrfconfirmShipEmail" runat="server" Text="*"
                            ErrorMessage="Please enter Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                            ForeColor="#eaeaea" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="*"
                            ErrorMessage="Please enter valid Shipping confirm email." ControlToValidate="txtshpConfirmEmail"
                            ForeColor="#eaeaea" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" ValidationGroup="vgs1" />
                        <asp:CompareValidator ID="CompareValidator1" ForeColor="#eaeaea" ControlToCompare="txtshpEmail"
                            Display="Dynamic" ControlToValidate="txtshpConfirmEmail" runat="server" Text="*"
                            ErrorMessage="Shipping Email and confirm email not matched." ValidationGroup="vgs1">
                        </asp:CompareValidator>
                    </div>
                    <div class="colum-label-seven">
                        Phone Number<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtShpPhone" runat="server" CssClass="input validcheck" autocomplete="off"
                            MaxLength="20" onkeypress="return isNumberKey(event)" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpPhone" runat="server" Text="*" ErrorMessage="Please enter Shipping phone number."
                            ControlToValidate="txtShpPhone" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Address Line 1 Or Company Name<span class="readerror">*</span>
                    </div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtshpAdd" runat="server" CssClass="input validcheck" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpAdd" runat="server" Text="*" ErrorMessage="Please enter Shipping address."
                            ControlToValidate="txtshpAdd" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Address Line 2</div>
                    <div class="colum-label-eight" style="height: auto;">
                        <asp:TextBox ID="txtshpAdd2" runat="server" CssClass="input" />
                    </div>
                    <div class="colum-label-seven">
                        Town / City<span class="readerror">*</span></div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpCity" runat="server" CssClass="input" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipCity" runat="server" Text="*" ErrorMessage="Please enter Shipping town/city."
                            ControlToValidate="txtshpCity" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        County / State<span class="readerror">*</span></div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpState" runat="server" CssClass="input" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshipState" runat="server" Text="*" ErrorMessage="Please enter Shipping state."
                            ControlToValidate="txtshpState" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Postal/Zip Code<span class="readerror">*</span></div>
                    <div class="colum-label-eight">
                        <asp:TextBox ID="txtshpZip" runat="server" CssClass="input validcheck" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpZip" runat="server" Text="*" ErrorMessage="Please enter Shipping postal/zip code."
                            ControlToValidate="txtshpZip" ForeColor="#eaeaea" />
                    </div>
                    <div class="colum-label-seven">
                        Country<span class="readerror">*</span></div>
                    <div class="colum-label-eight countory">
                        <asp:DropDownList ID="ddlshpCountry" runat="server" class="inputsl" />
                        <asp:RequiredFieldValidator ID="BookingpassrfshpCountry" runat="server" Text="*"
                            ErrorMessage="Please select Shipping country." ControlToValidate="ddlshpCountry"
                            InitialValue="0" ForeColor="#eaeaea" />
                    </div>
                    <asp:Label runat="server" ID="lblshpmsg" />
                </div>
            </asp:Panel>
            <%--Shipping Address End--%>
            <div class="round-titles">
                Date for Departure
            </div>
            <div class="booking-detail-in delvery" style="width: 886px">
                <div class="colum-label-seven">
                    Departure Date from Australia
                    <br />
                    (for fulfillment purposes)</div>
                <div class="colum-label-eight" style="height: auto;">
                    <asp:TextBox ID="txtDateOfDepature" runat="server" class="input validcheck calDate"
                        Text="DD/MM/YYYY" Style="margin-right: 4px;" />
                    <asp:RequiredFieldValidator ID="BookingpassrfDateOfDepature" runat="server" Text="*"
                        ErrorMessage="Please enter Date Of Depature." ControlToValidate="txtDateOfDepature"
                        ForeColor="#eaeaea" InitialValue="DD/MM/YYYY" />
                    <span class="imgCal calIcon" id="imgcal2" style="float: left;" title="Select Date of Departure.">
                    </span>
                    <br />
                    <br />
                    &nbsp;If left empty the ticket start date will be used as delivery date.
                </div>
            </div>
            <asp:Panel ID="pnlShipping" runat="server">
                <div class="round-titles">
                    Shipping Option
                </div>
                <div class="booking-detail-in" style="width: 886px">
                    <asp:Repeater ID="rptShippings" runat="server">
                        <ItemTemplate>
                            <div class="colum-input">
                                <input type="radio" id="radioChkShp" class="shipping" value='<%#Eval("Price")+"^"+Eval("ShippingName")+"^"+ Server.HtmlDecode(Eval("description").ToString())%>'
                                    name="chkshiping" onclick="getdata()" />
                                <span>
                                    <%#Eval("ShippingName") %></span>
                            </div>
                            <div class="colum-label t-right">
                                <strong>
                                    <%=currency%>
                                    <%#Eval("Price")%></strong></div>
                            <div style="background-color: #eaeaea; padding: 7px; width: 98%; float: left; line-height: 30px;
                                color: Black; font-size: 13px;">
                                <%#Eval("description")%></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
            <div class="round-titles">
                Total
            </div>
            <div class="booking-detail-in" style="width: 886px">
                <div class="colum-input" style="width: 500px;">
                    &nbsp;</div>
                <div class="colum-label t-right" style="width: 885px;">
                    <strong style="float: right">
                        <%=currency%>
                        <asp:Label ID="lblTotalCost" runat="server" /></strong>
                </div>
            </div>
            <div style="width: 920px; padding-bottom: 10px;">
                <asp:Button ID="btnBack" ClientIDMode="Static" runat="server" Text="Back" CssClass="btn-red-cart f-left"
                    OnClick="btnBack_Click" Width="120px" CausesValidation="False" />
                <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Proceed To Payment"
                    CssClass="btn-red-cart w184 f-right" OnClick="btnCheckout_Click" ValidationGroup="vgs1" />
            </div>
        </div>
        <div id="popupDiv">
            <div class="modalBackground progessposition">
            </div>
            <div class="privacy-block-outer pass progess-inner">
                <div class="red-title" style="text-align: left;">
                    <span>Ticket Protection</span> <a id="btnOK" href="#" onclick="searchEvent()">X</a>
                </div>
                <div id="divpopupdata" runat="server" class="discription-block" style="text-align: left;
                    overflow-x: scroll; height: 300px; font-size: 13px;">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnShippingCost" runat="server" Value="0" />
    <script type="text/javascript">
        function showthis() {
            document.getElementById('popupDiv').style.display = 'block';
        }

        function searchEvent() {
            document.getElementById('popupDiv').style.display = 'none';
        } 
    </script>
</asp:Content>
