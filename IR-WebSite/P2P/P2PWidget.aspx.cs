﻿using System;
using System.Net;
using System.Net.Mail;
using Business;

public partial class P2P_P2PWidget : System.Web.UI.Page
{
    #region Global Variables
    Guid _siteId;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string SiteURL, _siteURL;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                _siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (new ManageBooking().IsFraudIPorCard(_siteId, null, null))
        {
            ucTrainSearch.Visible = false;

        }
    }

    public void SendEmail()
    {
        try
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var fromAddres = new MailAddress("admin@InternationalRail.com", "admin@InternationalRail.com");
            smtpClient.Host = "mail.dotsquares.com";
            smtpClient.Port = 587;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
            smtpClient.EnableSsl = false;
            message.From = fromAddres;
            message.To.Add("dipu.bharti@dotsquares.com");
            message.Subject = "Fraud booking on Australia p2p site";
            message.IsBodyHtml = true;
            message.Body = "user try to placed fraud booking on P2P iframe site.<br/>IP Address: "+new ManageBooking().GetIpAddress();
            smtpClient.Send(message);
        }
        catch(Exception ex)
        {
        }
    }
}
