﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="P2PWidget.aspx.cs" Inherits="P2P_P2PWidget"
    MasterPageFile="~/P2P/SiteMaster.master" UICulture="en" Culture="en-GB" %>

<%@ Register Src="~/P2P/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
        <Scripts>
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery-1.9.1.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.core.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.datepicker.js" />
            <asp:ScriptReference Path="../Scripts/DatePicker/jquery.ui.widget.js" />
        </Scripts>
    </asp:ToolkitScriptManager>
    <div style="padding-left: 5px;">
        <div class="ticketbooking" style="padding-top: 0px">
            <uc1:TrainSearch ID="ucTrainSearch" runat="server"></uc1:TrainSearch>
        </div>
    </div>

</asp:Content>
