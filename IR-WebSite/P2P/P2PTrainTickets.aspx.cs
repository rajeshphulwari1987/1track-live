﻿#region Using
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Business;
using OneHubServiceRef;
#endregion

public partial class P2P_P2PTrainResult : System.Web.UI.Page
{
    public string _siteURL;
    private Guid _siteId;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                _siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}