﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentProcess.aspx.cs" Inherits="P2P_PaymentProcess"
    MasterPageFile="~/P2P/SiteMaster.master" UICulture="en" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControls/SecurePayGateway.ascx" TagName="SecurePayGateway"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery.ui.theme.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Scripts/DatePicker/jquery.ui.widget.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function RedirectPageUrl(url) {
            var dataurl = url;
            window.parent.location = dataurl;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetcardDetails();
        });

        function GetcardDetails() {
            $("#ContentPlaceHolder1_SecurePayGateway1_lblTotalPrice").text($("#ContentPlaceHolder1_lblGrandTotal").text());
            CardTypeChange();
            GetcardType();
            GetcardTypeOnKeyPress();
        }

        function CardTypeChange() {
            $("#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").change(function () {
                Getvalue();
            });
        }

        function Getvalue() {
            var total = $("#ContentPlaceHolder1_hdnGrandTotal").val();
            var value = $("#ContentPlaceHolder1_SecurePayGateway1_ddlCardType option:selected").text();

            if (value == $("#ContentPlaceHolder1_SecurePayGateway1_ddlCardType option").eq(1).text()) {
                var charge = parseFloat(total) * parseInt("3") / 100;
                total = parseFloat(total) + parseFloat(charge);
                $("#ContentPlaceHolder1_lblGrandTotal").text(total.toFixed(2));
                $("#ContentPlaceHolder1_SecurePayGateway1_hdnAmexCharge").val(charge);
            }
            else {
                total = parseFloat(total) + parseFloat("0");
                $("#ContentPlaceHolder1_lblGrandTotal").text(total.toFixed(2));
                $("#ContentPlaceHolder1_SecurePayGateway1_hdnAmexCharge").val("0");
            }
            $("#ContentPlaceHolder1_SecurePayGateway1_lblTotalPrice").text($("#ContentPlaceHolder1_lblGrandTotal").text());
        }

        function GetcardType() {
            $("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").blur(function () {
                if ($("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().substring(0, 1) == '4') {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 3);
                    Getvalue();
                }
                else if ($("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().substring(0, 1) == '5') {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 2);
                    Getvalue();
                }
                else if ($("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().substring(0, 1) == '3') {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 1);
                    Getvalue();
                }
                else {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 0);
                    Getvalue();
                }
            });
        }

        function GetcardTypeOnKeyPress() {
            $("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").keypress(function () {
                if ($("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().substring(0, 1) == '4' && $("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().length >= 4) {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 3);
                    Getvalue();
                }
                else if ($("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().substring(0, 1) == '5' && $("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().length >= 4) {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 2);
                    Getvalue();
                }
                else if ($("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().substring(0, 1) == '3' && $("#ContentPlaceHolder1_SecurePayGateway1_txtCardNumber").val().length >= 4) {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 1);
                    Getvalue();
                }
                else {
                    $("select#ContentPlaceHolder1_SecurePayGateway1_ddlCardType").prop('selectedIndex', 0);
                    Getvalue();
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:HiddenField ID="hdnGrandTotal" runat="server" />
    <div class="content">
        <div class="left-content" style="width: 100%; margin: 0px;">
            <h1>
                Review Your Order
            </h1>
            <br />
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="round-titles">
                Order Information
            </div>
            <div class="booking-detail-in" style="width: 95%">
                <table class="grid" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" style="width: 424px">
                            Email Address :
                        </td>
                        <td>
                            <asp:Label ID="lblEmailAddress" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td>
                            <b>Delivery Address</b>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblDeliveryAddress" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b>Order Summary:</b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Order Date:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderDate" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Order No.:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderNumber" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptOrderInfo" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td align="left">
                                    Journey
                                    <%# Container.ItemIndex + 1 %>:
                                </td>
                                <td>
                                    <%#Eval("ProductDesc")%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Name:
                                </td>
                                <td>
                                    <%#Eval("Title")%>
                                    <%#Eval("Name")%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Price:
                                </td>
                                <td>
                                    <%=currency %>
                                    <%#Eval("Price").ToString()%>
                                </td>
                            </tr>
                            <%#(Eval("TktPrtCharge")) != "" ? "<tr><td>Ticket Protection: </td><td>" + currency +" "+ Eval("TktPrtCharge")+"</td></tr>" : ""%>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td align="left">
                            Booking Fee:
                        </td>
                        <td>
                            <%=currency %>
                            <asp:Label ID="lblBookingFee" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Shipping:
                        </td>
                        <td>
                            <%=currency %>
                            <asp:Label ID="lblShippingAmount" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="OrderDiscountNet" runat="server">
                        <td align="left">
                            <b>Net Price:</b>
                        </td>
                        <td>
                            <b>
                                <%=currency %>
                                <asp:Label ID="lblNetTotal" runat="server" Text=""></asp:Label></b>
                        </td>
                    </tr>
                    <tr id="OrderDiscount" runat="server">
                        <td align="left">
                            Discount:
                        </td>
                        <td>
                            <%=currency %>
                            <asp:Label ID="lblDiscount" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b>Total Price:</b>
                        </td>
                        <td>
                            <b>
                                <%=currency %>
                                <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label></b>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="booking-detail-in" style="display: none;">
                <div class="colum-label">
                    Email address*
                </div>
                <div class="colum-input">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input" />
                </div>
                <div style="float: left; padding-left: 200px; width: 100%; height: 15px; font-size: 12px !important;">
                    <asp:RequiredFieldValidator ID="rfEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail"
                        ForeColor="red" ValidationGroup="vgs" />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid Email."
                        ValidationGroup="vgs" ControlToValidate="txtEmail" ForeColor="red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic" />
                </div>
                <div class="colum-label">
                    Password*
                </div>
                <div class="colum-input">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="input"
                        MaxLength="10" />
                    <asp:RequiredFieldValidator ID="rfPhn" runat="server" ErrorMessage="*" ControlToValidate="txtPassword"
                        ValidationGroup="vgs" ForeColor="red" />
                </div>
                <div style="clear: both;">
                </div>
                <asp:Label runat="server" ID="lblmsg" />
            </div>
            <div class="booking-detail-in" id="PaymentDiv" runat="server" style="width: 95% !important">
                <uc1:SecurePayGateway ID="SecurePayGateway1" runat="server" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnUserName" runat="server" />
    <script type="text/javascript">
        function newPopup(url) {
            popupWindow = window.open(
        url, 'popUpWindow', 'height=400,width=800,left=25%,top=25%,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
        }
    </script>
</asp:Content>
