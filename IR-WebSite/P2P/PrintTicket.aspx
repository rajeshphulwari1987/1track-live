﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintTicket.aspx.cs" Inherits="P2P_Agent_PrintTicket"
    MasterPageFile="~/P2P/SiteMaster.master" UICulture="en" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <link href="Styles/base.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function ParentHomeUrl(url) {
            var dataurl = url;
            window.parent.location = dataurl;
        }
    </script>
    <script type="text/javascript">
        $("#lnkUrl").click(function () {
            $("#lnkUrl").next().find();
        });
    </script>
    <style type="text/css">
        /*booking detail*/
        
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        .booking-detail-in-print
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
        }
        
        .booking-detail-in-print table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in-print table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in-print table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        .booking-detail-in-print .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in-print .colum-input
        {
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in-print .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        
        .booking-detail-in-print .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 96.5%;
            padding: 0.5%;
        }
        .clsTicket
        {
            text-align: center;
            font-size: 15px;
            color: #4d4d4d;
        }
        .left-content
        {
            width: 686px;
            padding: 10px;
        }
    </style>
    <%=Script %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">
        <div class="f-left">
            <div class="left-content">
                <h1>
                    Your ticket(s)
                </h1>
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="clsTicket">
                    <asp:Literal Text="Please click on the button below to download your PDF ticket(s)."
                        runat="server" ID="ltrTicketMsg" Visible="true"></asp:Literal>
                    <p style="padding: 50px 0 50px 0">
                        <asp:Repeater ID="rptBeNePrint" runat="server" OnItemDataBound="rptBeNePrint_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <a href='<%# Eval("URL").ToString()%>' target="_blank" id="lnkUrl" runat="server">
                                            <asp:Image ID="Image1" CssClass="scale-with-grid" alt="" border="0" runat="server"
                                                ImageUrl="../images/download-ticket.png" /></a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </p>
                    Thanks for you order - have a great trip!<br />
                    <br />
                    If we can help you with anything else please do not hesitate to contact us.<br />
                    <br />
                    <br />
                    <br />
                    <i style="font-size: 13px">You'll need Adobe Reader to view your ticket, if you don't
                        already have it click below to download it.</i>
                    <p style="padding: 50px 0 50px 0">
                        <a href="http://www.adobe.com/go/getreader" target="_blank" style="text-decoration: none;">
                            <img id="Image2" class="scale-with-grid" alt="" border="0" src="../images/get_adobe_reader.gif" />
                        </a>
                    </p>
                </div>
                <div>
                    <asp:Button ID="btnContinue" Text="Continue Shopping" class="f-right" Width="160px"
                        runat="server" OnClick="btnContinue_Click" />
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hdnExec" Value="0" />
        </div>
    </div>
</asp:Content>
