﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="P2PPaymentCancel.aspx.cs"
    MasterPageFile="~/P2P/SiteMaster.master" Inherits="P2P_P2PPaymentCancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="Styles/layout.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .shadow
        {
            width: 500px;
            height: 300px;
            border: solid 1px #555;
            background-color: #eed;
            box-shadow: 10px -10px 10px rgba(0,0,0,0.6);
            -moz-box-shadow: 0 0 10px rgba(0,0,0,0.6);
            -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.6);
            -o-box-shadow: 0 0 10px rgba(0,0,0,0.6);
        }
    </style>
    <script type="text/javascript">
        function RedirectPage(url) {
            var dataurl = url;
            window.parent.location = dataurl;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="shadow">
            <table>
                <tr>
                    <td>
                        <p style="text-align: center; padding-top: 20px;">
                            <strong>Payment Cancel Confirmation </strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="height: 380px;">
                        <asp:Button ID="btnGoBack" runat="server" Text="Go Back" class="w92 f-right" OnClick="btnGoBack_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
