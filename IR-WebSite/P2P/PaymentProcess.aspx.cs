﻿using System;
using System.Linq;
using Business;

public partial class P2P_PaymentProcess : System.Web.UI.Page
{
    #region Local Variables
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly private ManageOrder _master = new ManageOrder();

    private readonly ManageP2PSettings objP2PSettings = new ManageP2PSettings();
    public string currency = "$";
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static Guid currencyID = new Guid();
    Guid siteId;

    public string products = "";
    public string siteURL = "";
    public string OrderNo;
    public string linkURL = string.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params["req"] != null)
            {
                OrderNo = Request.Params["req"];
                linkURL = "?req=" + OrderNo;
            }
            else if (Session["OrderID"] != null)
                OrderNo = Session["OrderID"].ToString();

            OrderDiscountNet.Visible = OrderDiscount.Visible = _master.GetOrderDiscountVisibleBySiteId(siteId);

            #region If user changed query sitring data then redirct at home

            if (!IsPostBack)
            {
                if (Session["OrderID"] != null)
                    ViewState["tempOrder"] = Session["OrderID"].ToString();
                else if (Session["P2POrderID"] != null)
                    ViewState["tempOrder"] = Session["P2POrderID"].ToString();
            }

            if (ViewState["tempOrder"] != null && Request.Params["req"] != null && ViewState["tempOrder"].ToString() != Request.Params["req"])
                Response.Redirect("P2PWidget.aspx", false);

            #endregion

            #region
            if (Session["siteId"] != null)
            {
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
            GetCurrencyCode();
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(OrderNo))
                {
                    long OrderId = Convert.ToInt64(OrderNo);
                    var UpdateAgent = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId);
                    if (UpdateAgent != null && !AgentuserInfo.IsAffliate)
                        UpdateAgent.AgentID = AgentuserInfo.UserID;
                    _db.SaveChanges();
                }
                new ManageProduct().UpdateOrderCommitionPublic(siteId, Convert.ToInt64(OrderNo));
                var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (objsite != null && objsite.IsAgent.HasValue ? objsite.IsAgent.Value : false)
                {
                    if (AgentuserInfo.UserID == Guid.Empty)
                    {
                        Session.Add("redirectpage", linkURL);
                    }
                    else
                    {
                        if (Session["redirectpage"] != null)
                            Session.Remove("redirectpage");
                    }
                }
                ShowPaymentDetails();
                nextStepGoto();
            }
            #endregion
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void nextStepGoto()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var objPT = new PaymentGateWayTransffer();
                objPT.Amount = Convert.ToDouble(lblGrandTotal.Text.Trim());
                objPT.customerEmail = lblEmailAddress.Text.Trim();
                objPT.orderReference = OrderNo;
                Session["PayObj"] = objPT;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowPaymentDetails()
    {
        try
        {
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = new ManageBooking().GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = from a in lst
                             select new { Title = a.Title, Name = a.FirstName + " " + a.LastName, Price = (new ManageBooking().getPrice(a.PassSaleID, a.ProductType)), ProductDesc = (new ManageBooking().getPassDescType(a.PassSaleID, a.ProductType, (string)(Session["TrainType"]))), TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0) };
                if (lst.Count > 0)
                {
                    rptOrderInfo.DataSource = lstNew;
                    lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                    lblDeliveryAddress.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName + ", " + lst.FirstOrDefault().Address1 + ", " + (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2) ? lst.FirstOrDefault().Address2 + ", " : string.Empty) + "<br>" + (!string.IsNullOrEmpty(lst.FirstOrDefault().City) ? lst.FirstOrDefault().City + ", " : string.Empty) + (!string.IsNullOrEmpty(lst.FirstOrDefault().State) ? lst.FirstOrDefault().State + ", " : string.Empty) + lst.FirstOrDefault().DCountry + ", " + lst.FirstOrDefault().Postcode;
                    lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();

                    lblShippingAmount.Text = lst.FirstOrDefault().ShippingAmount.ToString();
                    lblDiscount.Text = _master.GetOrderDiscountByOrderId(Convert.ToInt64(OrderNo));
                    lblNetTotal.Text = ((lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TktPrtCharge)) + lst.FirstOrDefault().ShippingAmount + lst.FirstOrDefault().BookingFee).ToString();
                    lblGrandTotal.Text = (Convert.ToDecimal(lblNetTotal.Text) - Convert.ToDecimal(lblDiscount.Text)).ToString("F2");
                    hdnGrandTotal.Value = lblGrandTotal.Text;
                    lblBookingFee.Text = Convert.ToDecimal(lst.FirstOrDefault().BookingFee).ToString("F2");
                    hdnUserName.Value = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName + " " + lst.FirstOrDefault().DLastName;
                }
                else
                    rptOrderInfo.DataSource = null;
                rptOrderInfo.DataBind();
            }
            else
            {
                Response.Redirect("P2PWidget.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCurrencyCode()
    {
        try
        {
            if (Session["siteId"] != null)
            {
                siteId = Guid.Parse(Session["siteId"].ToString());
                currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
                currency = oManageClass.GetCurrency(currencyID);
                currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }   
}
