﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Specialtrains.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="Specialtrains" %>

<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControls/map.ascx" TagName="map" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="SliderGallery/thumb-scroller.css" rel="stylesheet" type="text/css" />
    <script src="SliderGallery/preview.js" type="text/javascript"></script>
    <script src="SliderGallery/jquery.thumb-scroller.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#aSpTrain").addClass("active");
        });
    </script>
    <style type="text/css">
        .floatLt
        {
            float: left;
        }
        .floatRt
        {
            float: right;
        }
        .clsBanner
        {
            float: left;
            width: 460px;
            height: 162px;
        }
        .clsImg
        {
            float: right;
            width: 220px;
            height: 162px;
        }
        iframe
        {
            width: 100%;
            height: 665px !important;
        }
        #quickPick
        {
            display: none;
        }
        .contory-block
        {
            width: 150px;
            height: 170px;
            float: left;
            border: 1px solid #b3b3b3;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -ms-border-radius: 5px;
            behavior: url(PIE.htc);
            margin: 6px;
            padding: 8px;
        }
        div.trainname
        {
            white-space: nowrap;
            width: 170px;
            overflow: hidden;
        }
        .left-content p a
        {
            display: inline !important;
            text-decoration: underline !important;
        }
    </style>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <section class="content">
<div class="left-content">
<asp:Panel ID="pnlErrSuccess" runat="server">
    <div id="DivSuccess" runat="server" class="success" style="display: none;">
        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
    <div id="DivError" runat="server" class="error" style="display: none;">
        <asp:Label ID="lblErrorMsg" runat="server" />
    </div>
</asp:Panel>
<div id="dvMain" runat="server">
 <uc2:map ID="map1" runat="server" />
</div>

<asp:Repeater ID="rptSpecTrains" runat="server">
    <ItemTemplate>
        <div class="floatLt" style="width: 680px;">
          <div class="clsBanner">
            <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImg")%>' alt="" width="458px" height="162px"/>
          </div>
          <div class="clsImg">
            <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("CountryImg")%>' alt="" width="220px" height="162px"/>
            </div>
            </div>
        <div class="floatLt" style="width:100%">
            <h1><%#Eval("Name")%></h1>
            <p><%#Eval("Title")%></p>
            </div>
        <div class="clear"></div>
        <p><%#HttpContext.Current.Server.HtmlDecode((string)Eval("Description"))%></p>
    </ItemTemplate>
</asp:Repeater> 
<div class="clear"></div>
<asp:Repeater ID="rptContinent" runat="server" OnItemDataBound="rptContinent_ItemDataBound">
    <ItemTemplate>
        <asp:HiddenField ID="hdnConId" runat="server" Value='<%#Eval("ID")%>'/>
        <div class="count">
        <div class="hd-ttl" style="cursor:default">
    	    <strong><%#Eval("Name")%></strong>
        </div>
        <div class="discription-block">
            <asp:Repeater ID="rptTrain" runat="server">
                <ItemTemplate>
                <ul>
                    <li>
                        <a href="<%# Eval("Url") %>"><%# Eval("Name") %></a>
                    </li>
                </ul>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        </div>
</ItemTemplate>
</asp:Repeater>
<div class="clear"> &nbsp;</div>

<%--Train detail--%>
<div id="divTrain" runat="server">
<strong class="txt-red">Trains</strong>
<div style="width: 695px">
    <div style="width: 700px; float:left; position:relative;">
    <div id="banner-slide">
<!-- start Basic Jquery Slider -->
    <div id="ca-container" class="ca-container" style="background-color:White;">
        <div class="ca-wrapper">
            <asp:Repeater ID="tptTrainList" runat="server">
            <HeaderTemplate>
                <div id="ts" class="thumb-scroller" style="height:195px !important;">
                    <ul class="ts-list">
            </HeaderTemplate>
                <ItemTemplate>
                        <li><a href="<%#Eval("NavUrl") %>" data-lightbox-group="gallery1" style="text-decoration:none;">
                            <img src="<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImage") %>" alt="" style="height:135px !important; width:99% !important;" />
                            <h3 style="padding-top: 5px; text-align:center;"> <div class="trainname" style="text-overflow:ellipsis;">
                           
                            <%#Eval("Name") %> 
                         
                            </div>
                               </h3>
                           </a>
                        </li>
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    </div>
    </div>
</div>
</div>
</div>
<div class="right-content">
        <div class="ticketbooking" style="padding-top:0px">
            <div class="list-tab divEnableP2P">
                <ul>
                    <li><a href="#" class="active">Rail Tickets </a><li>
                    <li><a href="rail-passes">Rail Passes </a></li>
                </ul>
            </div>
            <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
            <img src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
        </div>
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
