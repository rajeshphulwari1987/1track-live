﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Xml;
using Business;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Configuration;

public partial class RailPasses : Page
{
    readonly private ManageProduct _master = new ManageProduct();
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static bool ErrorIn;
    public Guid siteId;
    public string script = "<script></script>";
    readonly ManageAffiliateUser _AffManage = new ManageAffiliateUser();
    public string siteURL;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["af"] != null)
        {
            bool Result = _AffManage.GetAffURLIsActiveByAffCode(Request.QueryString["af"], Request.Url.ToString());
            if (Result)
                Session["AffCode"] = Request.QueryString["af"];
            else
                Response.Redirect(siteURL + "home");
        }
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["cid"] == null)
            {
                BindCategory(siteId);
                rptCat.Visible = true;
            }
            else
                rptCat.Visible = false;
            QubitOperationLoad();
            GetRailPassData();
        }
    }

    public void GetRailPassData()
    {
        var railpass1 = _db.tblRailPassSec1.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass1 != null)
        {
            imgRail1.Src = adminSiteUrl + railpass1.Imagepath;
            lblRailTitle1.Text = railpass1.Name;
            lblRailDesp1.Text = (railpass1.Description.Length > 50) ? (Server.HtmlDecode(railpass1.Description)).Substring(0, 50) + "..." : Server.HtmlDecode(railpass1.Description);
        }
        var railpass2 = _db.tblRailPassSec2.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass2 != null)
        {
            imgRail2.Src = adminSiteUrl + railpass2.Imagepath;
            lblRailTitle2.Text = railpass2.Name;
            lblRailDesp2.Text = (railpass2.Description.Length > 50) ? (Server.HtmlDecode(railpass2.Description)).Substring(0, 50) + "..." : Server.HtmlDecode(railpass2.Description);
        }
        var railpass3 = _db.tblRailPassSec3.FirstOrDefault(ty => ty.SiteId == siteId);
        if (railpass3 != null)
        {
            imgRail3.Src = adminSiteUrl + railpass3.Imagepath;
            lblRailTitle3.Text = railpass3.Name;
            lblRailDesp3.Text = (railpass3.Description.Length > 50) ? (Server.HtmlDecode(railpass3.Description)).Substring(0, 50) + "..." : Server.HtmlDecode(railpass3.Description);
        }
    }

    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindCategory(Guid siteId)
    {
        var isAgentSite = false;
        var _product = new ManageProduct();
        var result = new List<ProductCategory>();
        if (Request.QueryString["category"] != null)
        {
            /*Check if site is agent*/
            var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            if (objsite != null)
            {
                if (objsite.IsAgent != null) isAgentSite = (bool)objsite.IsAgent;
            }

            /*Check if agent user role is allowed Foc-AD75*/
            if (Request.QueryString["category"] == "FOC-AD75")
            {
                if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                {
                    var agentId = AgentuserInfo.UserID;
                    var isFoc = _oWebsitePage.IsFocAD75(agentId);
                    if (isFoc && isAgentSite)
                        result = _product.GetAssociateCategoryListRailPass(3, siteId).Where(x => x.IsActive && x.IsAD75).ToList();
                    else
                        Response.Redirect("home");
                }
                else
                    Response.Redirect("home");
            }
        }
        else
            result = _product.GetAssociateCategoryListRailPass(3, siteId).Where(x => x.IsActive && !x.IsAD75).ToList();

        var results = from p in _db.tblCategoriesNames
                      group p.Name by p.Name into g
                      where g.Count() > 1
                      select new
                      {
                          CatName = g.Key
                      };

        var list = new List<Categories>();
        foreach (var item in result)
        {
            if (_db.tblProductCategoriesLookUps.Any(x => x.CategoryID == item.ID))
                list.Add(new Categories
                {
                    ID = item.ID,
                    Name = item.Name,
                    Url = results.Any(y => y.CatName == item.Name) ? item.ID.ToString().Substring(0, 4) + "-" + item.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").ToLower() : item.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").ToLower()                
                });
        }

        rptCat.DataSource = list.OrderBy(x => x.Name);
        rptCat.DataBind();
    }

 

    protected void rptCat_OnItemCreated(object sender, RepeaterItemEventArgs args)
    {
        var lnkEdit = (LinkButton)args.Item.FindControl("lnkCat");
        if (lnkEdit != null)
        {
            ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(lnkEdit);
            ShowP2PWidget(siteId);
        }
    }

    void ShowP2PWidget(Guid siteID)
    {
        var enableP2P = _oWebsitePage.EnableP2P(siteID);
        if (!enableP2P)
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);
    }

    public class Categories
    {
        public Guid ID { get; set; }
        public String Name { get; set; }
        public String Url { get; set; }
    }
}