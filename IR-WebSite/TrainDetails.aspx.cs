﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI;

public partial class TrainDetails : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();

    private Guid _siteId;
    readonly Masters _master = new Masters();
    public string script = "<script></script>";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
    }

    #region PageLoad Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["trainId"] != null)
            {
                try
                {
                    var trainId = Guid.Parse(Request.QueryString["trainId"]);
                    BindTrainDetail(trainId, _siteId);
                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
            }
            QubitOperationLoad();
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    #endregion

    #region UserDefined function
    public void BindTrainDetail(Guid trainId, Guid siteId)
    {

        var result = GetTrainById(trainId, siteId);
        rptTrain.DataSource = result.Take(1);
        rptTrain.DataBind();
    }
    #endregion

    protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var rptGallery = (Repeater)e.Item.FindControl("rptGallery");
            var hdnTrainId = (HiddenField)e.Item.FindControl("hdnTrainId");
            if (hdnTrainId != null)
            {
                var trainid = Guid.Parse(hdnTrainId.Value);
                var res = from trnDtl in _db.tblTrainDetails
                          join trnGalryDtl in _db.tblTrainDetailGalleries
                          on trnDtl.ID equals trnGalryDtl.TrainDetailID
                          where trnDtl.IsActive && trnGalryDtl.TrainDetailID == trainid
                          select new { trnGalryDtl.ID, trnGalryDtl.GalleryImage };
                rptGallery.DataSource = res.ToList();
                rptGallery.DataBind();
            }
        }
    }

    public List<tblTrainDetail> GetTrainById(Guid trainId, Guid siteId)
    {
        try
        {
            var result = _db.tblTrainDetails.Where(x => x.ID == trainId).ToList();
            return result.ToList();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}