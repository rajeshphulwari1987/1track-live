﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConditionsofUse.aspx.cs"
    MasterPageFile="~/Site.master" Inherits="ConditionsofUse" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".privacy-block-outer").find(".discription-block").hide();
            $("#divCmsWl").find(".discription-block").show();
            $("#divCmsWl").find(".aDwn").show();
            $("#divCmsWl").find(".aRt").hide();

            $("#divCmsWl").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWl").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWl").find(".aDwn").show();
                $("#divCmsWl").find(".aRt").hide();
            });

            $("#divCmsWl").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWl").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWl").find(".aDwn").show();
                $("#divCmsWl").find(".aRt").hide();
            });

            $("#divCmsPrd").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPrd").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPrd").find(".aDwn").show();
                $("#divCmsPrd").find(".aRt").hide();
            });

            $("#divCmsPrd").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPrd").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPrd").find(".aDwn").show();
                $("#divCmsPrd").find(".aRt").hide();
            });

            $("#divCmsWeb").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWeb").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWeb").find(".aDwn").show();
                $("#divCmsWeb").find(".aRt").hide();
            });

            $("#divCmsWeb").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsWeb").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsWeb").find(".aDwn").show();
                $("#divCmsWeb").find(".aRt").hide();
            });

            $("#divCmsAnti").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsAnti").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsAnti").find(".aDwn").show();
                $("#divCmsAnti").find(".aRt").hide();
            });

            $("#divCmsAnti").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsAnti").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsAnti").find(".aDwn").show();
                $("#divCmsAnti").find(".aRt").hide();
            });

            $("#divCmsCopy").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCopy").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCopy").find(".aDwn").show();
                $("#divCmsCopy").find(".aRt").hide();
            });

            $("#divCmsCopy").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCopy").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCopy").find(".aDwn").show();
                $("#divCmsCopy").find(".aRt").hide();
            });
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Conditions of Use </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Conditions of Use</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content">
<asp:Repeater ID="rptConditions" runat="server">
    <ItemTemplate>
    <h1><%#Eval("Title")%></h1>
    <p><%#Eval("Description")%></p>
    <p>&nbsp; </p>
    <div class="clear"> &nbsp;</div>
    <div id="divBlock">
        <div id="divCmsWl" class="privacy-block-outer">
            <div class="hd-title">
                <strong> 1. Warranty and Liability </strong> 
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("WarrantyLiability"))%></div>
        </div>
        <div id="divCmsPrd" class="privacy-block-outer">
            <div class="hd-title">
                <strong>  2. Products </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("Products"))%></div>
        </div>
        <div id="divCmsWeb" class="privacy-block-outer">
            <div class="hd-title">
            <strong>  3. Use of our website </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
        <div class="discription-block">
            <%#HttpContext.Current.Server.HtmlDecode((string)Eval("UseWebsite"))%>
        </div>
        </div>
        <div id="divCmsAnti" class="privacy-block-outer">
            <div class="hd-title">
                <strong>  4. Anti Viral Software </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("AntiViralSoftware"))%></div>
        </div>
        <div id="divCmsCopy" class="privacy-block-outer">
            <div class="hd-title">
                <strong> 5. Copyrights </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("Copyright"))%>
            </div>
        </div>
    </div>
    </ItemTemplate>
</asp:Repeater>
 </div>
<div class="right-content">
    <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
