﻿using System;
using System.Linq;
using Business;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using OneHubServiceRef;
using System.Collections.Generic;

public partial class TextTemplate : System.Web.UI.Page
{
    readonly ManageBooking _masterTxt = new ManageBooking();
    private Guid _siteId;
    public string script = "<script></script>";
    readonly Masters _master = new Masters();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
        Newsletter1.Visible = _master.IsVisibleNewsLetter(_siteId);
    }

    #region PageLoad Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.RouteData.Values["PageId"] != null)
                BindTextTemplate(_siteId);

            QubitOperationLoad();
        }
    }
    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    #endregion

    #region Control Events

    #endregion

    #region UserDefined function
    public void BindTextTemplate(Guid siteId)
    {
        var result = _masterTxt.GetTextTemplateBySiteid(siteId);
        rptTemplate.DataSource = result.Take(1);
        rptTemplate.DataBind();
    }
    #endregion

    #region  One Hub Implementation
    public string[] CreateStationXml()
    {
        var clientCode = new OneHubRailOneHubClient();
        var stationList = clientCode.GetStationInfo().ToList();
        return BindXml(ReadObjectAsString(stationList));
    }
    public string ReadObjectAsString(object o)
    {
        var xmlS = new XmlSerializer(o.GetType());
        var sw = new StringWriter();
        var tw = new XmlTextWriter(sw);
        xmlS.Serialize(tw, o);
        return sw.ToString();
    }

    public string[] BindXml(string strXml)
    {
        try
        {
            if (strXml != "")
            {
                var doc = new XmlDocument();
                doc.LoadXml(strXml);
                string filePath = Server.MapPath(@"StationList.xml");
                doc.Save(filePath);
                return null;
            }

            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion
}