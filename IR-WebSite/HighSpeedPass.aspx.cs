﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using System.Threading;

public partial class HighSpeedPass : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    ManageTrainDetails _master = new ManageTrainDetails();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    ManageBooking _masterBooking = new ManageBooking();
    public static List<getRailPassData> list = new List<getRailPassData>();
    public string currency = "$";
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid siteId;
    Guid countryID;
    public string script = "<script></script>";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetCurrencyCode();
            FetchRailPassData();
            QubitOperationLoad();
        }
    }
    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }
    public bool getvalidcountryid(Guid pid, Guid cid)
    {
        try
        {
            return _db.tblProductPermittedLookups.Any(ty => ty.ProductID == pid && ty.CountryID == cid);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ddlCountry_chkcounty(object sender, EventArgs e)
    {
        var ddlcntry = (DropDownList)sender;
        var hdnproductid = (HiddenField)ddlcntry.Parent.FindControl("hdnproductid");
        if (ddlcntry.SelectedIndex != 0)
        {
            var countryid = Guid.Parse(ddlcntry.SelectedValue);
            var productid = string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
            bool result = getvalidcountryid(productid, countryid);
            if (!result)
            {
                ddlcntry.SelectedIndex = 0;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "showmessagecountry()", true);
            }
        }
    }

    public void FetchRailPassData()
    {
        try
        {
            Session["ProductType"] = null;
            if (Session["AgentUsername"] == null)
            {
                Session["USERUsername"] = "Guest";
                Session["USERUserID"] = Guid.NewGuid();
            }
            if (Session["RailPassData"] != null)
            {
                list = Session["RailPassData"] as List<getRailPassData>;
                rptPassDetail.DataSource = list;
                rptPassDetail.DataBind();
            }
            else
            {
                Response.Redirect("rail-passes");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void rptPassDetail_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Delete")
            {
                var Id = Guid.Parse(e.CommandArgument.ToString().Trim());
                var objRPD = list.Where(a => a.Id == Id).FirstOrDefault();
                int count = 0;
                if (objRPD.TravellerName.ToLower().Contains("saver"))
                {
                    var newList = (from a in list
                                   where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName && a.TravellerName.ToLower().Contains("saver")
                                   select a).ToList();
                    count = newList.Count();
                    if (count > 2)
                    {
                        list.RemoveAll(ty => ty.Id == Id);
                        Session.Add("RailPassData", list);
                    }
                    else
                    {
                        foreach (getRailPassData a in newList)
                        {
                            list.RemoveAll(ty => ty.Id == a.Id);
                        }
                        Session.Add("RailPassData", list);
                    }
                }
                else
                {
                    list.RemoveAll(ty => ty.Id == Id);
                    Session.Add("RailPassData", list);
                }
                GetCurrencyCode();
                FetchRailPassData();
            }
        }
        catch (Exception ex) { }
    }

    protected void rptPassDetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var txtStartDate = e.Item.FindControl("txtStartDate") as TextBox;
            var ddlTitle = e.Item.FindControl("ddlTitle") as DropDownList; ;
            var txtFirstName = e.Item.FindControl("txtFirstName") as TextBox;
            var txtLastName = e.Item.FindControl("txtLastName") as TextBox;
            var txtPassportNumber = e.Item.FindControl("txtPassportNumber") as TextBox;
            var hdnproductid = e.Item.FindControl("hdnproductid") as HiddenField;
            var ddlCountry = e.Item.FindControl("ddlCountry") as DropDownList;

            ddlCountry.DataSource = _master.GetCountryDetail();
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

            var productid = string.IsNullOrEmpty(hdnproductid.Value) ? new Guid() : Guid.Parse(hdnproductid.Value);
            bool result = getvalidcountryid(productid, countryID);

            if (result)
                ddlCountry.SelectedValue = countryID.ToString();
            else
                ddlCountry.SelectedIndex = 0;

            if (Session["DetailRailPass"] != null)
            {
                var list2 = Session["DetailRailPass"] as List<getpreviousShoppingData>;
                var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
                var ID = (!string.IsNullOrEmpty(lnkDelete.CommandArgument) ? Guid.Parse(lnkDelete.CommandArgument) : Guid.Empty);
                if (list2 != null)
                {
                    var data2 = list2.FirstOrDefault(ty => ty.Id == ID);
                    if (data2 != null)
                    {
                        txtStartDate.Text = data2.Date;
                        ddlTitle.SelectedValue = data2.Title;
                        txtFirstName.Text = data2.FirstName;
                        txtLastName.Text = data2.LastName;
                        ddlCountry.SelectedValue = data2.Country;
                        txtPassportNumber.Text = data2.Passoprt;
                    }
                }
            }
            var hdnCountryLevelIDs = e.Item.FindControl("hdnCountryLevelIDs") as HiddenField;
            var lblCountryName = e.Item.FindControl("lblCountryName") as Label;
            string counrty = _master.GetEurailCountryNames(hdnCountryLevelIDs.Value);
            if (!string.IsNullOrEmpty(counrty))
                lblCountryName.Text = "(" + counrty + ")";

            var lblLID = e.Item.FindControl("lblLID") as Label;
            getRailPassData objRPD = list.Where(a => a.Id == Guid.Parse(lblLID.Text.Trim())).FirstOrDefault();
            int count = 0;
            if (objRPD.TravellerName.ToLower().Contains("saver"))
            {
                var newList = (from a in list
                               where a.PrdtName == objRPD.PrdtName && a.ValidityName == objRPD.ValidityName && a.TravellerName.ToLower().Contains("saver")
                               select a).ToList();

                count = newList.Count();
                var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
                if (count == 2)
                {
                    lnkDelete.OnClientClick = "return window.confirm('You are now deleting the whole saverpass booking, as min 2 pax is required.')";
                }
                else
                {
                    lnkDelete.OnClientClick = "return window.confirm('Are you sure? Do you want to delete this item?')";
                }
            }
        }
    }


    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            countryID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCountryID;
            currency = oManageClass.GetCurrency((Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID);
        }

    }
    protected void btnChkOut_Click(object sender, EventArgs e)
    {
        try
        {
            var textInfo = cultureInfo.TextInfo;
            if (rptPassDetail.Items.Count > 0)
            {
                var AgentID = new Guid();
                var UserID = new Guid();
                string AffiliateCode = string.Empty;
               
                if (Session["AffCode"] != null)
                    AffiliateCode = Session["AffCode"].ToString();
                if (AgentuserInfo.UserID != Guid.Empty && AgentuserInfo.IsAffliate)
                    AffiliateCode = _masterBooking.GetAffiliateCode(AgentuserInfo.UserID);
                if (Session["USERUserID"] != null)
                    UserID = Guid.Parse(Session["USERUserID"].ToString());
                else if (Session["AgentUserID"] != null)
                    AgentID = Guid.Parse(Session["AgentUserID"].ToString());

                long POrderID = _masterBooking.CreateOrder(AffiliateCode, AgentID, UserID, siteId, "OldSPass");
                Session["OrderID"] = POrderID;
                var lstRPData = new List<getRailPassData>();
                list = Session["RailPassData"] as List<getRailPassData>;
                foreach (RepeaterItem it in rptPassDetail.Items)
                {
                    var IdInsert = Guid.NewGuid();
                    var HdnOriginalPrice = it.FindControl("HdnOriginalPrice") as HiddenField;
                    var hdnPassSale = it.FindControl("hdnPassSale") as HiddenField;
                    var hdnSalePrice = it.FindControl("hdnSalePrice") as HiddenField;
                    string[] str = hdnPassSale.Value.Split(',');
                    var LID = it.FindControl("lblLID") as Label;
                    var txtStartDate = it.FindControl("txtStartDate") as TextBox;
                    var ddlTitle = it.FindControl("ddlTitle") as DropDownList;
                    var txtFirstName = it.FindControl("txtFirstName") as TextBox;
                    var txtLastName = it.FindControl("txtLastName") as TextBox;
                    var ddlCountry = it.FindControl("ddlCountry") as DropDownList;
                    var txtPassportNumber = it.FindControl("txtPassportNumber") as TextBox;
                    var objTraveller = new tblOrderTraveller();

                    if (LID.Text.Trim() == string.Empty)
                        objTraveller.ID = IdInsert;
                    else
                        objTraveller.ID = Guid.Parse(LID.Text);

                    objTraveller.Title = ddlTitle.SelectedItem.Text;
                    objTraveller.LastName = textInfo.ToTitleCase(txtLastName.Text);
                    objTraveller.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                    objTraveller.Country = Guid.Parse(ddlCountry.SelectedValue);
                    objTraveller.PassportNo = txtPassportNumber.Text;
                    objTraveller.PassStartDate = Convert.ToDateTime(txtStartDate.Text);

                    var Gtps = new tblPassSale();
                    Gtps.ID = IdInsert;
                    Gtps.OrderID = POrderID;
                    Gtps.ProductID = Guid.Parse(str[0]);
                    Gtps.Price = Convert.ToDecimal(str[1]);
                    Gtps.TravellerID = Guid.Parse(str[2]);
                    Gtps.ClassID = Guid.Parse(str[3]);
                    Gtps.ValidityID = Guid.Parse(str[4]);
                    Gtps.CategoryID = Guid.Parse(str[5]);
                    Gtps.Commition = Convert.ToDecimal(str[6]);
                    Gtps.MarkUp = Convert.ToDecimal(str[7]);
                    Gtps.TicketProtection = 0;
                    Gtps.CountryStartCode = Convert.ToInt32((!string.IsNullOrEmpty(str[8]) ? str[8] : null));
                    Gtps.CountryEndCode = Convert.ToInt32((!string.IsNullOrEmpty(str[9]) ? str[9] : null));
                    Gtps.Country1 = (!string.IsNullOrEmpty(str[10]) ? Guid.Parse(str[10]) : Guid.Empty);
                    Gtps.Country2 = (!string.IsNullOrEmpty(str[11]) ? Guid.Parse(str[11]) : Guid.Empty);
                    Gtps.Country3 = (!string.IsNullOrEmpty(str[12]) ? Guid.Parse(str[12]) : Guid.Empty);
                    Gtps.Country4 = (!string.IsNullOrEmpty(str[13]) ? Guid.Parse(str[13]) : Guid.Empty);
                    Gtps.Country5 = (!string.IsNullOrEmpty(str[14]) ? Guid.Parse(str[14]) : Guid.Empty);
                    Gtps.Site_MP = _masterBooking.GetCurrencyMultiplier("SITE", Guid.Parse(str[0]), POrderID);
                    Gtps.USD_MP = _masterBooking.GetCurrencyMultiplier("USD", Guid.Parse(str[0]), POrderID);
                    Gtps.SEU_MP = _masterBooking.GetCurrencyMultiplier("SEU", Guid.Parse(str[0]), POrderID);
                    Gtps.SBD_MP = _masterBooking.GetCurrencyMultiplier("SBD", Guid.Parse(str[0]), POrderID);
                    Gtps.GBP_MP = _masterBooking.GetCurrencyMultiplier("GBP", Guid.Parse(str[0]), POrderID);
                    Gtps.EUR_MP = _masterBooking.GetCurrencyMultiplier("EUR", Guid.Parse(str[0]), POrderID);
                    _masterBooking.AddOrderTraveller(objTraveller, _masterBooking.AddPassSale(Gtps, IdInsert, hdnSalePrice.Value, HdnOriginalPrice.Value));

                    var objUpdates = new getRailPassData();
                    objUpdates = list.Where(a => a.Id == objTraveller.ID).FirstOrDefault();
                    if (objUpdates != null)
                    {
                        objUpdates.FirstName = textInfo.ToTitleCase(txtFirstName.Text);
                        objUpdates.LastName = textInfo.ToTitleCase(txtLastName.Text);
                        objUpdates.PassportNo = txtPassportNumber.Text;
                        objUpdates.PassStartDate = Convert.ToDateTime(txtStartDate.Text);
                        lstRPData.Add(objUpdates);
                    }
                }
                Session["RailPassData"] = lstRPData;
                Response.Redirect("~/BookingCart");
            }
        }
        catch (Exception ex) { }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (Session["RailPassData"] != null)
        {
            Session.Remove("DetailRailPass");
            var detailRP = new List<getpreviousShoppingData>();
            foreach (RepeaterItem item in rptPassDetail.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var lnkDelete = item.FindControl("lnkDelete") as LinkButton;
                    var txtStartDate = item.FindControl("txtStartDate") as TextBox;
                    var ddlTitle = item.FindControl("ddlTitle") as DropDownList; ;
                    var txtFirstName = item.FindControl("txtFirstName") as TextBox;
                    var txtLastName = item.FindControl("txtLastName") as TextBox;
                    var ddlCountry = item.FindControl("ddlCountry") as DropDownList;
                    var txtPassportNumber = item.FindControl("txtPassportNumber") as TextBox;

                    var ID = (!string.IsNullOrEmpty(lnkDelete.CommandArgument) ? Guid.Parse(lnkDelete.CommandArgument) : Guid.Empty);
                    string date = txtStartDate.Text;
                    string Title = ddlTitle.SelectedValue;
                    string FirstName = txtFirstName.Text;
                    string LastName = txtLastName.Text;
                    string Country = ddlCountry.SelectedValue;
                    string PassportNumber = txtPassportNumber.Text;

                    if (ID != Guid.Empty)
                    {
                        var datadetail = new getpreviousShoppingData();
                        datadetail.Id = ID;
                        datadetail.Date = date;
                        datadetail.Title = Title;
                        datadetail.FirstName = FirstName;
                        datadetail.LastName = LastName;
                        datadetail.Country = Country;
                        datadetail.Passoprt = PassportNumber;
                        detailRP.Add(datadetail);
                    }
                }
            }
            if (detailRP != null)
            {
                Session["DetailRailPass"] = detailRP;
            }
        }
        Response.Redirect("~/rail-passes");
    }

    public class getpreviousShoppingData
    {
        public Guid Id { get; set; }
        public String Date { get; set; }
        public String Title { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Country { get; set; }
        public String Passoprt { get; set; }
    }
}