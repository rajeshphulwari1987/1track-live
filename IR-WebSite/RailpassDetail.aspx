﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RailpassDetail.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="RailpassDetail" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControls/map.ascx" TagName="map" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register TagPrefix="uc" TagName="ucCountryLevel" Src="~/UserControls/ucCountryPass.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" media="all" href="maps/css/jquery.modalbox.css" />
    <script type="text/javascript" src="maps/cssmap.js"></script>
    <script type="text/javascript" src="maps/js/jquery.modalbox-1.5.0-min.js"></script>
    <style type="text/css">
        .modalboxIsDraggable
        {
            width: 867px !important;
            left: 215px !important;
        }
        .clsToolTip {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #666666;
            font-size: 14px;
            font-weight: bold;
            left: 160px;
            padding: 5px;
            position: absolute;
            top: 0;
            width: 400px;
        }
        .progess-inner
        {
            border-radius: 5px;
            background-color: White;
            z-index: 102;
            position: fixed;
            left: -16%;
            top: 26%;
            width: 550px;
            margin-left: 46%;
        }
        
        .modalBackground 
        {
            position: fixed;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            overflow: hidden;
            padding: 0;
            margin: 0;
            background-color: #000;
            filter: alpha(opacity=50);
            opacity: 0.5;
            text-align: center;
            float: left;
        }
        .progessposition
        {
            padding-top: 20%;
        }
        
        .btnBookkNow
        {
            background: #fff;
            padding: 0px 4px 4px 4px;
            float: right;
            border: 1px solid #fff;
            border-radius: 3px;
            margin-top: 5px;
            *width:105px;
        }
        
        .SortBtnStyle:hover
        {
            background-color: #ccc;
        }
        .innertab [title]{
	    position:relative;
        }
        .innertab [title]:after{
	        content:attr(title);
	        color:#000;
	        background:#999;
            border:2px solid #333;
	        background:rgba(51,51,51,0.75);
	        padding:5px;
	        position:absolute;
	        left:-9999px;
	        opacity:0;
	        bottom:100%;
	        white-space:nowrap;
	        -webkit-transition:0.25s linear opacity;
        }
        .innertab [title]:hover:after{
	        left:5px;
	        opacity:1;
        }
        .nivoSlider img{ height: 390px!important;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("ul.innertab li a:first").addClass("active").show();
            $("#MainContent_UpdatePanel1 div:first").css("display", "block");
            onload();
            $('.innertab li').mouseover(function () {
                var traveller = $(this).text();
                if (traveller.indexOf('Saver') > "-1") {
                    $("#ToolTip").css("display", "block");
                }
            }).mouseout(function () {
                $("#ToolTip").css("display", "none");
            });
        });
        function innertabActive() {
            $('#innertab').find('.active').trigger('click');
        }
        function getcurrentLinkPosition() {
            $(".innertab li").find('a').each(function () {
                var classmatch = $(this).attr('class').replace(/ /g, '');
                if (classmatch == 'linkButtonactive' || classmatch == 'linkButtonpie_first-childactive' || classmatch == 'linkButtonactivepie_first-child') {
                    $(this).trigger('click');
                }
            });
        }
        function AlertMessage(data) {
            alert(data);
        }
        function showmsg() {
            alert('Please select at least one Travel Validity.');
            window.location = window.location;
        }
        function onload() {
            $(".privacy-block-outer").find(".discription-block").hide();
            $("#divDesc").find(".discription-block").show();
            $("#divDesc").find(".aDwn").show();
            $("#divDesc").find(".aRt").hide();

            $("#divDesc").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divDesc").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divDesc").find(".aDwn").show();
                $("#divDesc").find(".aRt").hide();
            });

            $("#divCmsCon").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCon").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCon").find(".aDwn").show();
                $("#divCmsCon").find(".aRt").hide();
            });

            $("#divCmsCon").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCon").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCon").find(".aDwn").show();
                $("#divCmsCon").find(".aRt").hide();
            });

            $("#divEligforSaver").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divEligforSaver").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divEligforSaver").find(".aDwn").show();
                $("#divEligforSaver").find(".aRt").hide();
            });

            $("#divEligforSaver").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divEligforSaver").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divEligforSaver").find(".aDwn").show();
                $("#divEligforSaver").find(".aRt").hide();
            });

            $("#divCmsBook").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBook").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBook").find(".aDwn").show();
                $("#divCmsBook").find(".aRt").hide();
            });

            $("#divCmsBook").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsBook").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsBook").find(".aDwn").show();
                $("#divCmsBook").find(".aRt").hide();
            });

            $("#divCmsValid").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsValid").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsValid").find(".aDwn").show();
                $("#divCmsValid").find(".aRt").hide();
            });

            $("#divCmsValid").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsValid").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsValid").find(".aDwn").show();
                $("#divCmsValid").find(".aRt").hide();
            });

            $("#divCmsFare").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsFare").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsFare").find(".aDwn").show();
                $("#divCmsFare").find(".aRt").hide();
            });

            $("#divCmsFare").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsFare").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsFare").find(".aDwn").show();
                $("#divCmsFare").find(".aRt").hide();
            });
        };

        function showmessagecountry() {
            $(".customSelect").remove();
            $(".chkcheckbox,.uncheckbox").remove();
            $(".uncheckradiobox,.chkcheckradiobox").remove();
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hdnIsBritral" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIstwinpass" runat="server" ClientIDMode="Static" />
    <asp:ScriptManager ID="scrpMng" runat="server">
    </asp:ScriptManager>
    <section class="content">
        <div class="breadcrumb"> <a href="#">Home </a> >> <a href="<%=siteURL%>Rail-Passes">Rail Passes </a >>> <%--<asp:Label ID="lblCn" runat="server"/>  >>--%> <asp:Label ID="lblPrductName" runat="server"/>  </div>
<div class="innner-banner">
        <div class="bannerLft"> <div class="banner90"><asp:Label ID="lblCntyNm" runat="server" Text="Rail Passes"/></div></div>
        <div>
            <div style="float:left;width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" /></div>
            <div style="float:right;width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" />
        </div>
        </div>
    </div>
    <div class="headingclass" runat="server" id="getproductname"> </div>
    
<div class="left-content">
    <%--3,4,5 country pass--%>
    <div>
        <uc:ucCountryLevel ID="ucCountryLevel" runat="server" Visible="False" />
    </div>
    <%--end--%>
    
    <%--other counrtry pass--%>
<div id="dvOtherCountry" runat="server">
<asp:Repeater ID="rptPassDetail" runat="server">
    <ItemTemplate>
        <h1><%#Eval("Name")%></h1>
        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Description"))%>
</ItemTemplate>
</asp:Repeater> 
 <div id="divTool" runat="server" style="margin-bottom:20px">
    <span id="ToolTip" class="clsToolTip" style="display: none">Travelling together? See below point 3, Eligibility for Saving</span>
 </div>  
 <div class="clear"></div>
<div class="country-block-outer marg-b">
    <ul id="innertab" class="innertab">
        <asp:HiddenField ID="hdnGlobelTrName" runat="server" Value=''/>
        <asp:Repeater ID="rptTraveller" runat="server" >
            <ItemTemplate>
                <li>
                    <asp:LinkButton ID="lnkTraveller" INumber='<%# Container.ItemIndex + 1 %>' CssClass="linkButton" runat="server"  CommandName="PriceList"  CommandArgument='<%#Eval("ID")%>' Text='<%#Eval("Name")%>' OnClientClick="JavaScript:ChangeLinkPosition(this); return false;" >
                    </asp:LinkButton>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Repeater ID="rptTravellerGrid" runat="server" OnItemDataBound="rptTravellerGrid_ItemDataBound">
            <ItemTemplate>
             <div class="country-block-inner radius-block" style="display:none;" id="GV<%# Container.ItemIndex + 1 %>">
             <asp:HiddenField ID="hdnT" ClientIDMode="Static" Value='<%#Eval("Name")%>' runat="server"></asp:HiddenField>
             <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID")%>' Visible="false" ></asp:Label>
                <asp:GridView ID="grdPrice" runat="server" AutoGenerateColumns="true" OnRowDataBound="grdPrice_RowDataBound"  >
                </asp:GridView>
                </div>
            </ItemTemplate>
    </asp:Repeater>
    <div class="calculation-detail">
      <div id="divsubtotal">
        </div>
        <div class="colum-name"> <strong>Total</strong></div>
        <div class="colum-price"> <strong>  <div id="divTotal">         
        </div> </strong> </div>
    </div>
    <div class="detail-hints-main">
        <div class="detail-hints" id="divTraveller" runat="server">
        </div>
        <div class="btnBookkNow pos-rel">
             <asp:Button ID="btnBookNow" runat="server" class="newbutton f-right w105 mar-t" Text="Book Now" onclick="btnBookNow_Click" OnClientClick="return checkSaverSelect();"></asp:Button>
        </div>
    </div>
    <div class="clear"></div> 
</ContentTemplate>
</asp:UpdatePanel>
<div class="country-clear">&nbsp;</div>
</div>
<div class="clear">&nbsp;</div>
</div>
 <%--end--%>

 
<div id="divBlock">
    <div id="divDesc" class="privacy-block-outer">
        <div class="hd-title">
            <strong> 1. Description </strong> 
            <a class="aDwn"><img src="../images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="../images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block" id="divDescription" runat="server"> </div>
    </div>
    <div id="divCmsCon" class="privacy-block-outer">
        <div class="hd-title">
            <strong>2. Eligibility </strong> 
            <a class="aDwn"><img src="../images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="../images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block" id="divEligibility" runat="server" > </div>
    </div>
    <div id="divEligforSaver" class="privacy-block-outer">
        <div class="hd-title">
            <strong>3. Eligibility for Saver </strong> 
            <a class="aDwn"><img src="../images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="../images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block" id="divEligibilityforSaver" runat="server" > </div>
    </div>
    <div id="divCmsBook" class="privacy-block-outer">
        <div class="hd-title">
            <strong> 4. Discount </strong>
            <a class="aDwn"><img src="../images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="../images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block" id="divDiscount" runat="server"> </div>
    </div>
    <div id="divCmsValid" class="privacy-block-outer">
        <div class="hd-title">
            <strong>  5. Validity </strong>
            <a class="aDwn"><img src="../images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="../images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
    <div class="discription-block" id="divValidity" runat="server">        
    </div>
    </div>
    <div id="divCmsFare" class="privacy-block-outer">
        <div class="hd-title">
            <strong>  6. Terms and Conditions </strong>
            <a class="aDwn"><img src="../images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="../images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
        </div>
        <div class="discription-block" id="divTerm" runat="server"> </div>
    </div>
</div>
  

<div class="clear"> &nbsp;</div>
<div class="map-container-title">
    <h1> <a class="viewMap" href="#" id="lnkViewMap" runat="server"><img src='../images/icon-locate.png' alt="" border="0" /> View map</a> </h1>
   
</div>
</div>
<asp:HiddenField id="hdnCurrencySign" runat="server" />
<div class="right-content">
<div id="divJRF" runat="server" Visible="False">
  <a runat="server" id="trainresults">
    <div id="rtPannel1" runat="server"></div>
  </a>
<img id="imgRightPanel" runat="server" src='images/block-shadow.jpg' width="272" height="25"  class="scale-with-grid" alt="" border="0" />
</div>
<div id="divGeneralInfo" runat="server" Visible="False">
 <a href="<%=siteURL%>GeneralInformation.aspx">
    <uc:GeneralInformation ID="GInfo" runat="server" />
 </a>    
<img src='../images/block-shadow.jpg' width="272" height="25" class="scale-with-grid" alt="" border="0" />
</div>
<uc:Newsletter ID="Newsletter1" runat="server" />
<%--Right image section--%>
  <div class="right">
        <div id="dvRight" class="slider-wrapper theme-default" style="height:auto!important;" runat="server">
            <a id="aRightPanelUrl" runat="server">
                <div id="sliderRt">
                <asp:Repeater ID="rptRtImg" runat="server">
                    <ItemTemplate>
                        <img src='<%=SiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div></a>
        </div>
    </div>
<%--Right image section end--%>
</div>
</section>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                Shovelling coal into the server...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div style="clear: both;">
    </div>
    <script type="text/javascript">
        var ArryAdultSaver = new Array();
        $(document).ready(function () {
            callabc();
        });
        /* Check min and max select of saver in class */
        function checkSaverSelect() {
            var CountSaverMax = 0;
            var resultflag = false;
            var SaverName = "";
            var SaverPassesArray = [];
            $(".linkButton").each(function () {
                var strPass = $(this).text();
                var res = strPass.match(/saver/i);
                if (res) {
                    SaverName = $(this).text() + " and " + SaverName;
                    $("#GV" + $(this).attr("inumber").toString()).find("select").each(function () {
                        var i = parseInt($(this).val());
                        if (i > 0) {
                            var datavalc = $.trim($(this).attr('id'));
                            var SplitAc = datavalc.split('ddlID');
                            var SplitBc = SplitAc[1].split('_');
                            var strClassIDNumber = SplitBc[0];
                            SaverPassesArray.push($.trim($(this).parent().parent().find("td:first").text()) + strClassIDNumber + "-" + i.toString());
                        }
                    });
                }
            });
            var i = 0;
            var j = SaverPassesArray.length;
            if (j == 0) {
                resultflag = true;
            }
            else if (j == 1) {
                var SaverName1 = SaverPassesArray[0].split('-');
                var Fnum1 = parseInt($.trim(SaverName1[1]));
                if ((Fnum1 >= 2 && Fnum1 <= 5) || Fnum1 == 0) {
                    resultflag = true;
                }
                else {
                    resultflag = false;
                }
            }
            else {
                for (i = 0; i < j; i++) {
                    var k = 0;
                    var sameOther = false;
                    for (k = 0; k < j; k++) {
                        if (i != k) {
                            var strFN = SaverPassesArray[i].split('-');
                            var strSN = SaverPassesArray[k].split('-');
                            if (strFN[0] == strSN[0]) {
                                sameOther = true;
                                var num1 = parseInt($.trim(strFN[1]));
                                var num2 = parseInt($.trim(strSN[1]));
                                var total = num1 + num2;
                                if ((total >= 2 && total <= 5) || total == 0) {
                                    resultflag = true;
                                }
                                else {
                                    resultflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (sameOther == false) {
                        var SaverName2 = SaverPassesArray[i].split('-');
                        var Fnum2 = parseInt($.trim(SaverName2[1]));
                        if (!((Fnum2 >= 2 && Fnum2 <= 5) || Fnum2 == 0)) {
                            resultflag = false;
                            break;
                        }
                        else {
                            resultflag = true;
                        }
                    }
                }
            }
            if (resultflag == true) {
                return true;
            }
            else {
                alert('You can select minimum 2 or maximum 5 number of people for ' + SaverName.substring(0, SaverName.length - 4) + ' ticket on same travelling class.');
                return false;
            }
        }
        /* Total and saver condition Rajesh */
        function callabc(e) {
            var FreeChildCount = 2;//Default
            var IfSenior=false;
            var IsBritrail=$("#hdnIsBritral").val()=="True";
            var array = new Array();
            var arrayAdult = new Array();
            var idx = 0;
            var strShow = "";
            var str = '';
            var Gtotal = 0;
            var currSign = $("#MainContent_hdnCurrencySign").val();
            var CountSaverMax = 0;
            var SaverName = "";
            var selID = e;
            var flag = false;
            if (selID != null) {
                var data = $(e).attr("id");
                var SplitAA = data.split('ddlID');
                var SplitBB = SplitAA[1].split('_');
                var strClassIDNumber = SplitBB[0];
                var selIDSiblingFirstColumn = $("#" + $.trim(selID.id.toString())).parent().parent().find("td:first").text() + strClassIDNumber;
                /*default*/
                $(".linkButton").each(function () {
                    var strPass = $(this).text();
                    var resAdult = strPass.match(/Adult/i);
                    var res = strPass.match(/saver/i);
                    if (IsBritrail) {
                        FreeChildCount = 1;
                        IfSenior = (strPass.match(/Senior/i));
                        if (resAdult)
                            resAdult = !strPass.match(/saver/i);
                    }
                    if (res) {
                        SaverName = $(this).text() + " and " + SaverName;
                        $("#GV" + $(this).attr("inumber").toString()).find("select").each(function () {
                            var i = parseInt($(this).val());
                            if (i > 0) {
                                var dataval = $.trim($(this).attr('id'));
                                var SplitA = dataval.split('ddlID');
                                var SplitB = SplitA[1].split('_');
                                var strClassIDNumber1 = SplitB[0];
                                var casd = $(this).parent().parent().find("td:first").text() + strClassIDNumber1;
                                if ($.trim(casd) == $.trim(selIDSiblingFirstColumn)) {
                                    CountSaverMax = CountSaverMax + i;
                                }
                            }
                        });
                    }
                    /* ##Start## Only for free child 1(Eurail). adult,adult saver 2(Britrail). adult,Senior */
                    if (resAdult || IfSenior) {
                        $("#GV" + $(this).attr("inumber")).find("select").each(function () {
                            var i = parseInt($(this).val());
                            var dataval = $.trim($(this).attr('id'));
                            var SplitA = dataval.split('ddlID');
                            var SplitB = SplitA[1].split('_');
                            var strClassIDNumber1 = SplitB[0];
                            var casd = $(this).parent().parent().find("td:first").text() + strClassIDNumber1;
                            if ($.trim(casd) == $.trim(selIDSiblingFirstColumn)) {
                                var GRPPass = ($("#hdnIstwinpass").val() === 'True');
                                if (strPass.length == 5 && GRPPass && (i > 0) && (i % 2 == 1)) {
                                    alert("You must select a minimum number of 2 adults for a twin pass in the same class and validity. Twin Passes can only be sold in multiples of 2.");
                                    $(this).val('0');
                                }
                                var countselval = 0;
                                for (var G = 0; G < ArryAdultSaver.length; G++) {
                                    var sptarr = ArryAdultSaver[G].split('ñ');
                                    if (sptarr[0] == casd) {
                                        var spltid = $(this).attr('id').split("_");
                                        var idsadult = '_' + spltid[4] + '_' + spltid[5];
                                        $("[id*=" + idsadult + "]").each(function () {
                                            var spltid = $(this).attr('id').split("_");
                                            var hdrtxt = $("#MainContent_rptTraveller_lnkTraveller_" + spltid[3]).text();
                                            resAdult = hdrtxt.match(/Adult/i);
                                            if (IsBritrail) {
                                                IfSenior = (hdrtxt.match(/Senior/i));
                                                if (resAdult)
                                                    resAdult = !hdrtxt.match(/saver/i);
                                            }
                                            if ((resAdult || IfSenior) && $(this).val() != '') {
                                                countselval += parseInt($(this).val());
                                            }
                                        });
                                        ArryAdultSaver.splice(G, 1);
                                    }
                                }
                                ArryAdultSaver[ArryAdultSaver.length] = casd + 'ñ' + parseInt(countselval);
                            }
                        });
                        /*
                        for (var ko = 0; ko < ArryAdultSaver.length; ko++) {
                        console.log(ArryAdultSaver[ko]);
                        }
                        console.log("||||||||||||||||******************************************||||||||||||||||");
                        */
                        $.each(($('select[child=child]')), function () {
                            var isvalid = true;
                            var i = parseInt($(this).val());
                            if (i > 0) {
                                var dataval = $.trim($(this).attr('id'));
                                var SplitA = dataval.split('ddlID');
                                var SplitB = SplitA[1].split('_');
                                var strClassIDNumber1 = SplitB[0];
                                var tdtxt = $(this).parent().parent().find("td:first").text() + strClassIDNumber1;
                                if (ArryAdultSaver.length > 0) {
                                    for (var G = 0; G < ArryAdultSaver.length; G++) {
                                        var arrmatch = ArryAdultSaver[G].split('ñ');
                                        if (arrmatch[0] == tdtxt) {
                                            var adultsaverno = parseInt(arrmatch[1]) * FreeChildCount;
                                            if (i > adultsaverno) {
                                                $(this).val('0');
                                                var GRPPass = ($("#hdnIstwinpass").val() === 'True');
                                                if (GRPPass) {
                                                    alert("A maximum of 2 free child passes are allowed per adult, for additional children please purchase a normal German Rail Pass i.e. non-twin pass, in the Youth category.");
                                                }
                                                else {
                                                    if (IsBritrail)
                                                        alert('With each Adult, or Senior pass (not Adult Saver, Child or Youth) you are allowed x1 Child for free pass.');
                                                    else
                                                        alert("A maximum of 2 child passes is allowed per adult, for additional children please purchase a Youth pass.");
                                                }
                                            }
                                            isvalid = false;
                                        }
                                    }
                                    if (isvalid) {
                                        $(this).val('0'); 
                                        childalert(IsBritrail);

                                    }
                                }
                                else {
                                    $(this).val('0');
                                    childalert(IsBritrail);
                                }
                            }
                        });
                    }
                    /*##End##*/
                });
                var string = $("#" + $.trim(selID.id.toString())).parent().parent().parent().parent().parent().parent().find("#hdnT").val();
                var result = string.match(/saver/i);
                if (result) {
                    if (CountSaverMax > 5) {
                        $("#" + $.trim(selID.id.toString())).val('0');
                        alert('Maximum number of people for ' + SaverName.substring(0, SaverName.length - 4) + ' ticket on same travelling class is 5.');
                        flag = true;
                    } else {
                        flag = true;
                    }
                } else {
                    flag = true;
                }
                if (flag) {
                    $(".country-block-inner").each(function (index, value) {
                        var iCount = 0;
                        $(this).find("select").each(function () {
                            var i = parseInt($(this).val());
                            if (i > 0) {
                                iCount = iCount + i;
                            }
                        });
                        if (iCount > 0) {
                            var Amt = 0;
                            $(this).find("select").each(function () {
                                var i = parseInt($(this).val());
                                var PAmt = parseFloat($(this).parent().find(".ltrName").text());
                                var subT = 0;
                                if (i > 0) {
                                    subT = parseFloat(i) * PAmt;
                                    Amt = Amt + subT;
                                }
                            });
                            str = '<div class="CLASS-010"><div class="colum-name">' + $(this).find("#hdnT").val() + ' x ' + iCount.toString() + '</div>' + '<div class="colum-price" style="line-height:28px;">' + currSign + ' ' + Amt.toFixed(0).toString() + '</div></div>';
                            Gtotal = parseFloat(Amt) + Gtotal;
                            array[idx] = str;
                            idx++;
                        }
                    });
                    $('#divsubtotal').html(array.sort());
                    $('#divTotal').html(currSign + ' ' + Gtotal.toFixed(0).toString());
                }
            }
        }


        function childalert(IsBritrail) {
            if (IsBritrail)
                alert('Child passes can only be purchased with Adult or Senior passes with same travel validity and class.');
            else
                alert("Child passes can only be purchased with Adult or Adult Saver passes with same travel validity and class.");
        }

        /*Added for saver alert*/
        function callabcSaver(e) {
            var strShow = "";
            var str = '';
            var Gtotal = 0;
            var currSign = $("#MainContent_hdnCurrencySign").val();
            $(".country-block-inner").each(function (index, value) {
                var iCount = 0;
                $(this).find("select").each(function () {
                    var i = parseInt($(this).val());
                    if (i > 0) {
                        iCount = iCount + i;
                    }
                });
                if (iCount > 0) {
                    if (iCount > 1 && iCount < 6) {
                        str = str + '<div class="CLASS-010"><div class="colum-name">' + iCount.toString() + ' x ' + $(this).find("#hdnT").val() + '</div>';
                        var Amt = 0;
                        $(this).find("select").each(function () {
                            var i = parseInt($(this).val());
                            var PAmt = parseFloat($(this).parent().find(".ltrName").text());
                            var subT = 0;
                            if (i > 0) {
                                subT = parseFloat(i) * PAmt;
                                Amt = Amt + subT;
                            }
                        });
                        str = str + '<div class="colum-price">' + currSign + ' ' + Amt.toFixed(2).toString() + '</div></div>';
                        Gtotal = parseFloat(Amt) + Gtotal;
                    } else {
                        alert("Select passenger between 2 to 5");
                    }
                }
            });
            $('#divsubtotal').html(str);
            $('#divTotal').html(currSign + ' ' + Gtotal.toFixed(2).toString());
        }

        function ChangeLinkPosition(obj) {
            $(obj).addClass("linkButton active");
            $(obj).parents().siblings().children().removeClass('active');
            var item = $(obj).attr('inumber');
            $("#GV" + item).css("display", "block");
            $("#GV" + item).siblings('.country-block-inner').css("display", "none");
            var sta = $("span").hasClass("customSelect");
            if (sta) {
                $(".customSelect").remove();
                $('select').customSelect();
            }
        }
    </script>
    <script type="text/javascript">
        function map_Data() {
            jQuery.fn.modalBox({
                positionTop: 20,
                directCall: {
                    data: ' <div class="popuHeading"><div style="width:400px;float:left;padding-left:50px;"> International Rail Map</div></div><iframe src="maps/map.aspx" width="770" height="480"></iframe>'
                },
                setWidthOfModalLayer: 740
            });
        }
        jQuery(document).ready(function () {
            jQuery("a.viewMap").click(function () {
                map_Data();
            });
        });
    </script>
</asp:Content>
