﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="map.aspx.cs" Inherits="maps_map" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" id="css-continents">
<head>
    <meta charset="utf-8" />
    <style>
        html, body, header, hgroup, article, section, aside, footer, nav, div, p, span, ul, ol, dl, li, dt, dd, abbr, a, h1, h2, h3, hr, form, input
        {
            background: transparent;
            border: 0 none;
            margin: 0;
            outline: 0 none;
            padding: 0;
            vertical-align: baseline;
        }
        ul
        {
            list-style: none;
        }
        ol
        {
            line-height: 1.5em;
            margin-left: 2em;
        }
        small
        {
            font-size: 1em;
        }
        hr
        {
            display: none;
        }
        a
        {
            color: #000;
            text-decoration: underline;
        }
        a:active, a:visited
        {
            color: #666;
        }
        a:hover, a:focus
        {
            color: #c00;
        }
        p
        {
            margin: 1em 0;
        }
        h2, h3, h4
        {
            font-family: 'Lucida Grande' , Arial, sans-serif;
            font-weight: normal;
            margin: 1em 0;
        }
        h2
        {
            color: #666;
            font-size: 2em;
            text-transform: uppercase;
        }
        h3
        {
            color: #037fbb;
            font-size: 1.5em;
            margin-top: 2em;
        }
        h3 i
        {
            color: #999;
            font-style: normal;
        }
        h4
        {
            font-size: 1em;
            font-weight: bold;
            text-transform: uppercase;
        }
        article, aside, footer, header, hgroup, section, nav
        {
            display: block;
        }
        body
        {
            background: #f5f1ee;
            font: normal .75em 'Lucida Grande' , Arial, sans-serif;
            text-shadow: 0 1px 0 #fff;
            width: 100%;
        }
        header
        {
            background: #e2dedb;
            border-bottom: 1px solid rgba(0,0,0,.2);
            left: 0;
            padding: 1em 0;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 110;
            box-shadow: 0 1px 10px rgba(0,0,0,.3);
        }
        hgroup
        {
            margin: -.5em 0 0 -489px;
            left: 50%;
            position: absolute;
            top: 50%;
            width: 489px;
        }
        hgroup h1, hgroup h2
        {
            display: inline;
            font: normal bold 1.167em 'Lucida Grande' , Arial, sans-serif;
        }
        hgroup h1 a
        {
            color: #037fbb;
            margin: 0 .5em 0 0;
            text-decoration: none;
            text-transform: uppercase;
        }
        hgroup h2
        {
            color: #827e79;
            font-weight: normal;
        }
        hgroup h2 b
        {
            color: #5f5b57;
        }
        hgroup h1 a:hover, hgroup h1 a:focus
        {
            color: #004f80;
        }
        header nav
        {
            left: 50%;
            overflow: hidden;
            position: relative;
            width: 498px;
        }
        header ul
        {
            float: left;
            line-height: 1.5em;
            width: 33%;
        }
        header nav a
        {
            color: #555;
            font-size: .834em;
            text-decoration: none;
            text-transform: uppercase;
        }
        header nav a:hover, header nav a:focus
        {
            color: #037fbb;
        }
        article
        {
            margin: auto;
            max-width: 978px;
        }
        section
        {
            margin: 0 auto -3em auto;
            padding-top: 7em;
            width: 908px;
        }
        div.col
        {
            display: inline-block;
            vertical-align: top;
            width: 47.75%;
        }
        div.col.left
        {
            padding-right: 2%;
        }
        div.col.right
        {
            padding-left: 2%;
        }
        div.col h3
        {
            margin-top: 0;
        }
        div.info-bar
        {
            margin-bottom: 2em;
            text-align: center;
            width: 100%;
        }
        div.info-bar b
        {
            background: #e092aa;
            border: 1px solid #ce8199;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            box-shadow: inset 0 1px 2px rgba(255,255,255,.7);
            color: #600;
            display: inline-block;
            margin: 0 auto;
            padding: 1em 2em;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            text-transform: uppercase;
        }
        pre, code
        {
            background: #2c2a29;
            color: #ddd;
            font: normal 100 1em/1.5em 'M+ 1m' , Consolas, Monaco, 'Andale Mono' , Courier, monospace;
            text-shadow: 0 1px 0 #000;
            -webkit-text-stroke: .35px;
        }
        pre
        {
            -moz-border-radius: 10px;
            -ms-border-radius: 10px;
            -o-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            font-size: 1.1em;
            margin-bottom: 2em;
            overflow-y: auto;
            padding: 1em 2em;
            position: relative;
        }
        p code, li code, dd code, dt code
        {
            background: #e4e1de;
            color: #333;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            padding: .25em .5em;
            white-space: nowrap;
        }
        code[class*="language-"], pre[class*="language-"]
        {
            direction: ltr;
            text-align: left;
            white-space: pre;
            word-spacing: normal;
            -moz-tab-size: 4;
            -o-tab-size: 4;
            tab-size: 4;
            -webkit-hyphens: none;
            -moz-hyphens: none;
            -ms-hyphens: none;
            hyphens: none;
        }
        .token.comment, .token.prolog, .token.doctype, .token.cdata
        {
            color: #aaa;
        }
        .namespace
        {
            opacity: .7;
        }
        .token.property, .token.tag
        {
            color: #d5a514;
        }
        .token.selector, .token.string
        {
            color: #c00;
        }
        .language-css .token.selector, .token.attr-name
        {
            color: #c60;
        }
        .token.operator, .token.entity, .token.url, .language-css .token.string, .style .token.string
        {
            color: #add353;
        }
        .token.atrule, .token.attr-value, .token.keyword
        {
            color: #add353;
        }
        .token.number, .token.boolean
        {
            color: #99cee4;
        }
        .token.regex, .token.important
        {
            color: #e90;
        }
        .token.important
        {
            font-weight: bold;
        }
        .token.entity
        {
            cursor: help;
        }
        pre code span.ln
        {
            background: #555;
            color: #999;
            display: inline-block;
            font: normal .834em Arial, sans-serif;
            margin-left: -2.5em;
            margin-right: 1em;
            padding: .4em .5em;
            text-align: right;
            width: 2.5em;
        }
        code em
        {
            font-style: normal;
        }
        #list-code
        {
            max-height: 30em;
        }
        /*#samples {
	background:#3b3735;
	border:1px solid #fff;
	-moz-border-radius:1em;
	-webkit-border-radius:1em;
	-o-border-radius:1em;
	-ms-border-radius:1em;
	border-radius:1em;
	padding-top:0;
	text-shadow:0 1px 0 #000;
	width:908px;
}*/
        #samples
        {
            background: url("cssmap-continents/bg.jpg") repeat scroll 0 0 #686554;
            border: 1px solid #FFFFFF;
            border-radius: 1em;
            padding-top: 0;
            text-shadow: 0 1px 0 #000000;
            width: 1024px;
        }
        
        #maps-switcher
        {
            font-size: .834em;
            position: relative;
            text-align: center;
            top: -1.5em;
        }
        #maps-switcher li
        {
            display: inline-block;
            margin: 0 -2px;
        }
        #maps-switcher a
        {
            background: #333;
            border-top: 1px solid #fff;
            border-bottom: 1px solid rgba(0,0,0,.3);
            color: #ccc;
            display: block;
            padding: 1em 2em;
            text-decoration: none;
            text-transform: uppercase;
        }
        #maps-switcher a:hover, #maps-switcher a:focus
        {
            background: #222;
            color: #fff;
        }
        #maps-switcher a:active, #maps-switcher a.active
        {
            background: #222;
            color: #fd0;
        }
        #maps-switcher li:first-child a
        {
            -moz-border-radius: 6px 0 0 6px;
            -ms-border-radius: 6px 0 0 6px;
            -o-border-radius: 6px 0 0 6px;
            -webkit-border-radius: 6px 0 0 6px;
            border-radius: 6px 0 0 6px;
        }
        #maps-switcher li:last-child a
        {
            -moz-border-radius: 0 6px 6px 0;
            -ms-border-radius: 0 6px 6px 0;
            -o-border-radius: 0 6px 6px 0;
            -webkit-border-radius: 0 6px 6px 0;
            border-radius: 0 6px 6px 0;
        }
        .sample
        {
            color: #eee;
            padding: 30px 0;
            width: 100%;
        }
        .sample.hidden
        {
            left: -9999em;
            position: absolute;
        }
        .sample.active
        {
            left: 0;
            position: relative;
        }
        .sample a, .sample .css-map li a, .sample .map-visible-list a
        {
            color: #eee;
            text-decoration: none;
        }
        .sample a:hover, .sample a:focus, .sample .map-visible-list a:hover, .sample .map-visible-list a:focus, .sample .map-visible-list li.focus a
        {
            color: #fc0;
            text-decoration: none;
        }
        .sample a:active, .sample .map-visible-list a:active, .sample .map-visible-list li.active-region a
        {
            color: #cc3;
        }
        .sample .map-search-link
        {
            background: #111;
            background: rgba(0,0,0,.8);
            color: #eee;
            font: normal 14px 'Lucida Grande' , Arial, sans-serif;
            float: right;
            margin-right: 1em;
            overflow: hidden;
            padding: .8em 1.6em;
            text-shadow: 0 1px 0 #000;
            -moz-border-radius: .6em;
            -ms-border-radius: .6em;
            -o-border-radius: .6em;
            -webkit-border-radius: .6em;
            border-radius: .6em;
        }
        .sample .map-search-link:hover, a.sample .map-search-link:focus
        {
            color: #fd0;
            text-decoration: none;
        }
        .m430 .sample .map-search-link
        {
            font-size: 1em;
        }
        .sample .m430 .map-search-link
        {
            font-size: 1em;
        }
        .sample pre
        {
            border-top: 2px solid #7a7672;
            margin: 2em 40px 0 40px;
            overflow: visible;
        }
        .sample pre:before, .sample pre:after
        {
            border: solid;
            content: '';
            display: block;
            height: 0;
            left: 50%;
            position: absolute;
            width: 0;
        }
        .sample pre:before
        {
            border-color: #7a7672 transparent;
            border-width: 0 9px 9px 9px;
            margin-left: -9px;
            top: -9px;
        }
        .sample pre:after
        {
            border-color: #2c2a29 transparent;
            border-width: 0 6px 6px 6px;
            margin-left: -6px;
            top: -6px;
        }
        .map-visible-list
        {
            font-size: .9167em;
        }
        .map-visible-list li
        {
            display: inline-block;
            line-height: 1.5em;
        }
        .m960 .map-visible-list li, .m750 .map-visible-list li
        {
            width: 20%;
        }
        .m540 .map-visible-list li
        {
            width: 25%;
        }
        .m430 .map-visible-list li
        {
            width: 33%;
        }
        .css-map-container.m1110, .css-map-container.m1280, .css-map-container.m1450, .css-map-container.m2050
        {
            overflow-x: visible;
            width: 100%;
        }
        #example4 .css-map, #example4 .map-visible-list
        {
            display: inline-block;
            vertical-align: top;
        }
        #example4 .css-map-container
        {
            width: 635px;
        }
        #example4 .map-visible-list
        {
            margin-left: 20px;
            margin-top: 50px;
            width: 180px;
        }
        #example4 .m430 .map-visible-list li
        {
            width: 100%;
        }
        #example4 .m430 .map-visible-list li small
        {
            display: none;
        }
        footer
        {
            background: #e2dedb;
            border-top: 1px solid rgba(0,0,0,.2);
            bottom: 0;
            color: #666;
            font-size: .834em;
            left: 0;
            position: fixed;
            width: 100%;
            z-index: 110;
        }
        footer ul
        {
            border-top: 1px solid rgba(255,255,255,.7);
            margin: 0 auto;
            overflow: hidden;
            padding: 1em 0 1.2em 0;
            width: 978px;
        }
        footer li
        {
            display: block;
            margin: 0 1em;
            text-transform: uppercase;
        }
        footer .fl
        {
            float: left;
        }
        footer .fr
        {
            float: right;
        }
        footer a
        {
            color: #555;
            text-decoration: none;
        }
        footer a:hover, footer a:focus
        {
            color: #037fbb;
        }
        #top-link
        {
            background: #e2dedb;
            border-top: 1px solid rgba(0,0,0,.4);
            display: block;
            height: 6em;
            left: 50%;
            margin-left: -3em;
            position: absolute;
            text-decoration: none;
            top: -3em;
            width: 6em;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }
        #top-link b
        {
            font-size: 1.5em;
            left: 0;
            margin-top: -.6em;
            position: absolute;
            text-align: center;
            top: 50%;
            width: 100%;
        }
        #top-link:hover, #top-link:focus
        {
            top: -3.1em;
        }
        #raw-file
        {
            position: relative;
            width: 100%;
        }
        #raw-file a
        {
            background: #9fc73f;
            bottom: -3em;
            color: #030;
            font-weight: bold;
            padding: 1em;
            position: absolute;
            right: 3em;
            text-decoration: none;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            text-transform: uppercase;
            z-index: 99;
            -moz-box-shadow: 0 2px 4px rgba(0,0,0,.8), inset 0 5px 7px rgba(255,255,255,.5);
            -ms-box-shadow: 0 2px 4px rgba(0,0,0,.8), inset 0 5px 7px rgba(255,255,255,.5);
            box-shadow: 0 2px 4px rgba(0,0,0,.8), inset 0 5px 7px rgba(255,255,255,.5);
            -moz-border-radius: 2px;
            -ms-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
        }
        #raw-file a:hover, #raw-file a:focus
        {
            background: #add353;
            color: #030;
        }
    </style>
    <link rel="alternate" type="application/rss+xml" title="CSS &amp; jQuery clickable maps plugin"
        href="http://cssmapsplugin.com/feed" />
    <link rel="stylesheet" media="screen,projection" href="cssmap-continents/cssmap-continents.css" />
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" media="all" href="css/jquery.modalbox.css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!--<script src="js/jquery.cssmap.js"></script>-->
    <script type="text/javascript" src="cssmap.js"></script>
    <script type="text/javascript" src="cssmap_script.js"></script>
    <script type="text/javascript" src="js/jquery.modalbox-1.5.0-min.js"></script>
</head>
<body>
    <form runat="server">
    <article>
    <hr/>
      <!-- sample maps -->
      <section id="samples">
    <hr/>
    
	<div id="example1" class="sample active">
		<div class="backbutton" style="background:#eee;color:black;display:none;float:right;padding:3px;cursor:pointer;">Back</div>
        <div id="map-continents">
			<ul class="continents">
              <li class="c1"><a href="#map-africa">Africa</a></li>
              <li class="c2"><a href="#map-asia">Asia</a></li>
              <li class="c3"><a href="#map-oceana">Australia and Southern Pacific</a></li>
              <li class="c4"><a href="#map-europe">Europe</a></li>
              <li class="c5"><a href="#map-north-america">North America</a></li>
              <!--<li class="c6"><a href="#south-america">South America</a></li>-->
            </ul>
		</div>
	  
		<div id="map-asia" class="submap" style="display:none;">
			<ul class="asia">
                <asp:Repeater id="rptAsia" runat="server" OnItemDataBound="rptAsia_ItemDataBound">
                    <ItemTemplate>
                        <li id="lnkAsia" runat="server"><a id="aID" runat="server" href='<%# "../CountryDetail?Id=" + Eval("CountryID") %>' target="_top" title='<%#Eval("CountryName")%>'><%#Eval("CountryName")%></a></li>
                    </ItemTemplate>
                </asp:Repeater>
			</ul>
		</div>
		
		<div id="map-africa" class="submap" style="display:none;">
			<ul class="south-africa">
            <asp:Repeater ID="rptAfr" runat="server" OnItemDataBound="rptAfr_ItemDataBound">
                <ItemTemplate>
                    <li id="lnkAfr" runat="server"><a id="aID" runat="server" href='<%# "../CountryDetail?Id=" + Eval("CountryID") %>' target="_top" title='<%#Eval("CountryName")%>'><%#Eval("CountryName")%></a></li>
                </ItemTemplate>
            </asp:Repeater>
			</ul>
		</div>
		
		<div id="map-oceana" class="submap" style="display:none;">
			<ul class="oceana">
            <asp:Repeater ID="rptOceana" runat="server" OnItemDataBound="rptOceana_ItemDataBound">
                <ItemTemplate>
                     <li id="lnkOcn" runat="server"><a id="aID" runat="server" href='<%# "../CountryDetail?Id=" + Eval("CountryID") %>' target="_top" title='<%#Eval("CountryName")%>'><%#Eval("CountryName")%></a></li>
                </ItemTemplate>
            </asp:Repeater>
			</ul>
		</div>
		
		<div id="map-north-america" class="submap" style="display:none;">
			<ul class="north-america">
              <asp:Repeater ID="rptAm" runat="server" OnItemDataBound="rptAm_ItemDataBound">
                <ItemTemplate>
                    <li id="lnkAm" runat="server"><a id="aID" runat="server" href='<%# "../CountryDetail?Id=" + Eval("CountryID") %>' target="_top" title='<%#Eval("CountryName")%>'><%#Eval("CountryName")%></a></li>
                </ItemTemplate>
              </asp:Repeater>   
			</ul>
		</div>
		
		<div id="map-europe" class="submap"  style="display:none;">
			<ul class="europe">
            <asp:Repeater ID="rptEur" runat="server" onitemdatabound="rptEur_ItemDataBound">
                <ItemTemplate>
                    <li id="lnkEur" runat="server"><a id="aID" runat="server" href='<%# "../CountryDetail?Id=" + Eval("CountryID") %>' target="_top" title='<%#Eval("CountryName")%>'><%#Eval("CountryName")%></a></li>
                </ItemTemplate>
              </asp:Repeater>  
			</ul>
		</div>
    </div>
  </section>
      <!-- samples end -->
      
    </article>
    <script type="text/javascript">
	/* <![CDATA[ */
		jQuery(document).ready(function(){
			jQuery(".backbutton").click(function(){
				//$(".submap").hide();
				//$("#map-continents").show();
				$(".submap").hide();
				$("#map-continents").fadeIn(2000);
				$(".backbutton").hide();
			});
		});
	/* ]]> */
    </script>
    </form>
</body>
</html>
