﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI.HtmlControls;

public partial class maps_map : System.Web.UI.Page
{
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindMaps();
        }
    }

    public void BindMaps()
    {
        var asiaID = _db.tblContinents.FirstOrDefault(x => x.Name.Contains("Asia")).ID;
        var asiaList = _db.tblCountriesMsts.Where(x => x.ContinentID == asiaID && x.IsActive == true).ToList();
        rptAsia.DataSource = asiaList;
        rptAsia.DataBind();

        var afrID = _db.tblContinents.FirstOrDefault(x => x.Name.Contains("Africa")).ID;
        var afrList = _db.tblCountriesMsts.Where(x => x.ContinentID == afrID && x.IsActive == true).ToList();
        rptAfr.DataSource = afrList;
        rptAfr.DataBind();

        var ocID = _db.tblContinents.FirstOrDefault(x => x.Name.Contains("Australia/New Zealand")).ID;
        var ocList = _db.tblCountriesMsts.Where(x => x.ContinentID == ocID && x.IsActive == true).ToList();
        rptOceana.DataSource = ocList;
        rptOceana.DataBind();

        var amID = _db.tblContinents.FirstOrDefault(x => x.Name.Contains("North America")).ID;
        var amList = _db.tblCountriesMsts.Where(x => x.ContinentID == amID && x.IsActive == true).ToList();
        rptAm.DataSource = amList;
        rptAm.DataBind();

        var eurID = _db.tblContinents.FirstOrDefault(x => x.Name.Contains("Europe")).ID;
        var eurList = _db.tblCountriesMsts.Where(x => x.ContinentID == eurID && x.IsActive == true).ToList();
        rptEur.DataSource = eurList;
        rptEur.DataBind();
    }

    protected void rptAsia_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var aID = e.Item.FindControl("aID") as HtmlAnchor;
            var lnkAsia = e.Item.FindControl("lnkAsia") as HtmlGenericControl;

            if (aID != null)
                if (aID.Title == "India")
                {
                    if (lnkAsia != null)
                        lnkAsia.Attributes.Add("class", "a1");
                }
                else if (aID.Title == "Russia")
                {
                    if (lnkAsia != null)
                        lnkAsia.Attributes.Add("class", "a2");
                }
                else if (aID.Title == "Japan")
                {
                    if (lnkAsia != null)
                        lnkAsia.Attributes.Add("class", "a3");
                }
                else if (aID.Title == "South Korea")
                {
                    if (lnkAsia != null)
                        lnkAsia.Attributes.Add("class", "a4");
                }
                else if (aID.Title == "Vietnam")
                {
                    if (lnkAsia != null)
                        lnkAsia.Attributes.Add("class", "a5");
                }
                else
                {
                    if (lnkAsia != null)
                        lnkAsia.Attributes.Add("class", "a6");
                }
        }
    }

    protected void rptAfr_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var aID = e.Item.FindControl("aID") as HtmlAnchor;
            var lnkAfr = e.Item.FindControl("lnkAfr") as HtmlGenericControl;

            if (aID != null)
                if (aID.Title == "South Africa")
                {
                    if (lnkAfr != null)
                        lnkAfr.Attributes.Add("class", "za1");
                }
                else
                {
                    if (lnkAfr != null)
                        lnkAfr.Attributes.Add("class", "za2");
                }
        }
    }

    protected void rptAm_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var aID = e.Item.FindControl("aID") as HtmlAnchor;
            var lnkAm = e.Item.FindControl("lnkAm") as HtmlGenericControl;

            if (aID != null)
                if (aID.Title == "United States")
                {
                    if (lnkAm != null)
                        lnkAm.Attributes.Add("class", "nam1");
                }
                else if (aID.Title == "Canada")
                {
                    if (lnkAm != null)
                        lnkAm.Attributes.Add("class", "nam2");
                }
                else
                {
                    if (lnkAm != null)
                        lnkAm.Attributes.Add("class", "nam3");
                }
        }
    }

    protected void rptEur_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var aID = e.Item.FindControl("aID") as HtmlAnchor;
            var lnkEur = e.Item.FindControl("lnkEur") as HtmlGenericControl;

            if (aID != null)
                if (aID.Title == "Turkey")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu1");
                }
                else if (aID.Title == "Romania")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu2");
                }
                else if (aID.Title == "Greece")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu3");
                }
                else if (aID.Title == "Bulgaria")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu4");
                }
                else if (aID.Title == "Poland")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu5");
                }
                else if (aID.Title == "Germany")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu6");
                }
                else if (aID.Title == "Belgium")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu7");
                }
                else if (aID.Title == "France")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu8");
                }
                else if (aID.Title == "Spain")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu9");
                }
                else if (aID.Title == "Portugal")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu10");
                }
                else if (aID.Title == "Italy")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu11");
                }
                else if (aID.Title == "Ireland")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu12");
                }
                else if (aID.Title == "United Kingdom")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu13");
                }
                else if (aID.Title == "Norway")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu14");
                }
                else if (aID.Title == "Finland")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu15");
                }
                else if (aID.Title == "Sweden")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu16");
                }
                else if (aID.Title == "Czech Republic")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu17");
                }
                else if (aID.Title == "Austria")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu18");
                }
                else if (aID.Title == "Slovakia")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu19");
                }
                else if (aID.Title == "Switzerland")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu20");
                }
                else if (aID.Title == "Slovenia")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu21");
                }
                else if (aID.Title == "Croatia")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu22");
                }
                else if (aID.Title == "Hungary")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu23");
                }
                else if (aID.Title == "Serbia")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu24");
                }
                else if (aID.Title == "FYR Macedonia")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu25");
                }
                else if (aID.Title == "Netherlands")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu26");
                }
                else if (aID.Title == "Denmark")
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu27");
                }
                else
                {
                    if (lnkEur != null)
                        lnkEur.Attributes.Add("class", "eu28");
                }
        }
    }

    protected void rptOceana_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var aID = e.Item.FindControl("aID") as HtmlAnchor;
            var lnkOcn = e.Item.FindControl("lnkOcn") as HtmlGenericControl;

            if (aID != null)
                if (aID.Title == "Australia")
                {
                    if (lnkOcn != null)
                        lnkOcn.Attributes.Add("class", "oc1");
                }
                else if (aID.Title == "New Zealand")
                {
                    if (lnkOcn != null)
                        lnkOcn.Attributes.Add("class", "oc2");
                }
                else
                {
                    if (lnkOcn != null)
                        lnkOcn.Attributes.Add("class", "oc3");
                }
        }
    }
}