﻿#region Using
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
#endregion

public partial class RailpassDetail : Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static string ClassName = string.Empty;
    public static string SortcolumnName = string.Empty;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public ManageProduct _master = new ManageProduct();
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly ManageBooking _BMaster = new ManageBooking();
    readonly ManageAffiliateUser _AffManage = new ManageAffiliateUser();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    public static List<getCuntryCode> Levellist = new List<getCuntryCode>();
    public bool passportValid = false;
    public bool nationalityValid = false;
    string currency;
    string curID;
    Guid siteId;
    Guid productCurID;
    static int startDrp = 0;
    static int endDrp = 10;
    static bool IsSaverTraveller = false;
    public string script = "<script></script>";
    public string siteURL;
    bool IsReadonly = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
            Newsletter1.Visible = _masterPage.IsVisibleNewsLetter(siteId);
            trainresults.HRef = _db.tblSites.Any(x => x.ID == siteId && x.IsTrainTickets) ? siteURL + "traintickets" : siteURL + "trainresults";
            divJRF.Visible = _masterPage.IsVisibleJRF(siteId);
            divGeneralInfo.Visible = _masterPage.IsVisibleGeneralInfo(siteId);
            dvRight.Visible = _masterPage.IsVisibleRightPanelImages(siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["af"] != null)
        {
            bool Result = _AffManage.GetAffURLIsActiveByAffCode(Request.QueryString["af"], Request.Url.ToString());
            if (Result)
                Session["AffCode"] = Request.QueryString["af"];
            else
                Response.Redirect(siteURL+"home");
        }
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["PrdId"] != null)
            {
                Guid prdID = (Guid)Page.RouteData.Values["PrdId"];
                ViewState["prdID"] = prdID;
            }
            if (Request.QueryString["Id"] != null)
            {
                ViewState["prdID"] = Request.QueryString["Id"].ToString();
                string RD_URl = PageUrls.GetRedirectURL(Convert.ToString(ViewState["prdID"]));
                Response.Redirect(RD_URl);
            }
            if (ViewState["prdID"] != null)
            {
                /*********if Product Foc-AD75**********/
                var isPrdFoc = _master.PrdIsFoc(Guid.Parse(ViewState["prdID"].ToString()));
                if (isPrdFoc)
                {
                    var isAgentSite = false;
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                    if (objsite != null)
                    {
                        if (objsite.IsAgent != null) isAgentSite = (bool)objsite.IsAgent;
                    }
                    if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                    {
                        var agentId = AgentuserInfo.UserID;
                        var isFoc = _oWebsitePage.IsFocAD75(agentId);
                        if (!isFoc || !isAgentSite)
                            Response.Redirect("home");
                    }
                    else
                        Response.Redirect("home");
                }
                /*************************************/
                var prdCatActive = _master.IsActivePrdCategory(Guid.Parse(ViewState["prdID"].ToString()));
                var prdActive = _master.IsActiveProduct(Guid.Parse(ViewState["prdID"].ToString()));
                var sitePrd = _master.siteHasPrd(Guid.Parse(ViewState["prdID"].ToString()), siteId);
                if (!prdActive || !prdCatActive || !sitePrd)
                    Response.Redirect("home");
            }
            Response.Cache.SetNoStore();
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx"));
            if (pageId != null)
                PageContent(pageId.ID, siteId);
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.Contains("home"));
            if (pid != null)
                GetRightPanelImage(pid.ID, siteId);
            string QStr = (ViewState["prdID"] == null ? "" : ViewState["prdID"].ToString());
            Guid productCatID;
            if (Guid.TryParse(QStr, out productCatID))
                Session["getproductCatID"] = productCatID;
            else
                Session["getproductCatID"] = Guid.Empty;
            SortcolumnName = string.Empty;
            ClassName = "Asc";
            Session.Remove("CountryCodeList");
            Session.Remove("GETcountryCODE");
            QubitOperationLoad();
        }
        PageLoadEvent();
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _masterPage.GetPageDetailsByUrl(url);
                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                rtPannel1.InnerHtml = oPage.RightPanel1.Replace("CMSImages", SiteUrl + "CMSImages");
            }
            else
                imgRightPanel.Visible = false;
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetRightPanelImage(Guid pageId, Guid siteId)
    {
        var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
        if (result != null)
        {
            var url = result.Url;
            var oPage = _masterPage.GetPageDetailsByUrl(url);
            string[] arrRtListId = oPage.RightPanelImgID.Split(',');
            var idRtList = (from item in arrRtListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();

            var listRt = _masterPage.GetRightImgByID(idRtList);
            if (listRt.Count > 0)
            {
                aRightPanelUrl.HRef = oPage.RightPanelNavUrl;
                rptRtImg.DataSource = listRt;
                rptRtImg.DataBind();
            }
            else
                dvRight.Visible = false;
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    void PageLoadEvent()
    {
        if (ViewState["prdID"] == null)
            return;
       try
        {
            GetCurrencyCode();
            var pId = new Guid(ViewState["prdID"].ToString());
            tblProduct oProd = oManageClass.GetProductById(pId);
            hdnIstwinpass.Value = oProd.Istwinpass.ToString();
            passportValid = oProd.PassportValid.HasValue ? oProd.PassportValid.Value : false;
            nationalityValid = oProd.NationalityValid.HasValue ? oProd.NationalityValid.Value : false;
            int CULVL_Code = oProd.CountryStartCode;
            GetCountryLevel(CULVL_Code);
            if (Session["GetcunLvl"] != null && Session["GetcunLvl"].ToString() == "0")
            {
                ViewState["setget"] = oProd.CountryStartCode + "," + oProd.CountryEndCode;
            }
            else
            {
                /*check for 3,4,5 country pass*/
                ucCountryLevel.Visible = true;
                dvOtherCountry.Visible = false;
            }
            getproductname.InnerText = oManageClass.GetProductName(oProd.ID);
            productCurID = oProd.CurrencyID;
            imgBanner.ImageUrl = !string.IsNullOrEmpty(oProd.BannerImage) ? SiteUrl + oProd.BannerImage : "images/img_inner-banner.jpg";
            imgMap.ImageUrl = !string.IsNullOrEmpty(oProd.CounrtyImage) ? SiteUrl + oProd.CounrtyImage : "images/innerMap.gif";

            var pDetails = _master.GetProductTermsById(pId);
            if (pDetails != null)
            {
                divDescription.InnerHtml = Server.HtmlDecode(pDetails.Description);
                divDiscount.InnerHtml = Server.HtmlDecode(pDetails.Discount);
                divEligibility.InnerHtml = Server.HtmlDecode(pDetails.Eligibility);
                divEligibilityforSaver.InnerHtml = Server.HtmlDecode(pDetails.EligibilityforSaver);
                divTerm.InnerHtml = Server.HtmlDecode(pDetails.TermCondition);
                divValidity.InnerHtml = Server.HtmlDecode(pDetails.Validity);
            }

            var travller = oManageClass.GetProductTraveller(pId);
            rptTraveller.DataSource = travller;
            rptTraveller.DataBind();
            string str = string.Empty;
            int i = 1;
            foreach (var item in travller)
            {
                String after = item.Name.Substring(0, 1).ToUpper() + item.Name.Substring(1).ToLower();
                str = str + "<p> <span> " + (i++) + " </span> " + after + " " + item.AgeFrom;
                if (item.AgeTo <= 60)
                    str = str + " -" + item.AgeTo + " </p> ";
                else
                    str = str + "+ </p> ";
            }
            divTraveller.InnerHtml = str;
            hdnIsBritral.Value = _BMaster.GetIsBritrailByProductID(pId).ToString();
            rptTravellerGrid.DataSource = oManageClass.GetProductTraveller(pId);
            rptTravellerGrid.DataBind();
            lblPrductName.Text = oManageClass.GetProductName(pId);
            var countryId = oManageClass.GetCountryIdOfProduct(pId);
            BindCountryName(countryId);
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetCountryLevel(int CulevelID)
    {
        switch (CulevelID)
        {
            case 3001:
                Session["GetcunLvl"] = 3;
                break;
            case 4001:
                Session["GetcunLvl"] = 4;
                break;
            case 5001:
                Session["GetcunLvl"] = 5;
                break;
            default:
                Session["GetcunLvl"] = 0;
                break;
        }
    }

    public void GetProductListByTraveler(Guid id, Guid pid, Guid sid, GridView gv)
    {
        try
        {
            var AgentId = new Guid();
            if (Session["AgentUserID"] == null)
            {
                AgentId = Guid.NewGuid();
            }
            else
                AgentId = Guid.Parse(Session["AgentUserID"].ToString());

            gv.DataSource = null;
            gv.DataBind();
            using (var conn = new SqlConnection(con))
            {
                var sqlCommand = new SqlCommand("spGetProductListByTravellerId", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TravelerID", id);
                sqlCommand.Parameters.AddWithValue("@ProductID", pid);
                sqlCommand.Parameters.AddWithValue("@SiteID", sid);
                sqlCommand.Parameters.AddWithValue("@AgentID", AgentId.ToString());
                conn.Open();
                var da = new SqlDataAdapter(sqlCommand);
                var ds = new DataSet();
                da.Fill(ds); 
                var dv = ds.Tables[0].DefaultView;
                dv.Sort = SortcolumnName;

                gv.DataSource = dv;
                gv.DataBind();
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void grdPrice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Visible = false;
        e.Row.Cells[1].Visible = false;
        int countCell = e.Row.Cells.Count;

        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            if (countCell > 3)
            {
                for (int i = 3; i < countCell; i++)
                {
                    var lblname = new Label();
                    lblname.ID = "lblname" + i.ToString();
                    lblname.Text = e.Row.Cells[i].Text;
                    var LnkSortID = new LinkButton();//Get C0untry StartCode EndCpode
                    LnkSortID.ID = "LnkSortID" + i.ToString();
                    LnkSortID.CausesValidation = false;
                    LnkSortID.Attributes.Add("class", "SortBtnStyle " + ClassName);
                    LnkSortID.Width = 20;
                    LnkSortID.Height = 20;
                    LnkSortID.Click += new EventHandler(LnkSortID_Click);
                    if (ClassName == "Asc")
                    {
                        string name = SortcolumnName.Replace(" Desc", string.Empty);
                        if (e.Row.Cells[i].Text == name)
                        {
                            LnkSortID.Text = "▲";
                            LnkSortID.ToolTip = "Desc";
                        }
                        else
                        {
                            LnkSortID.Text = "▼";
                            LnkSortID.ToolTip = "Asce";
                        }
                    }
                    else
                    {
                        LnkSortID.Text = "▼";
                        LnkSortID.ToolTip = "Asce";
                    }
                    e.Row.Cells[i].Controls.Add(lblname);
                    e.Row.Cells[i].Controls.Add(LnkSortID);
                    ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(LnkSortID);
                }
            }
        }
        else
        {
            if (countCell > 3)
            {
                for (int i = 3; i < countCell; i++)
                {
                    var ltrName = new Label();
                    ltrName.ID = "ltrName" + i.ToString();
                    var hdnActualPrice = new HiddenField();
                    hdnActualPrice.ID = "hdnActualPrice" + i.ToString();
                    var ltrCurrency = new Label { ID = "ltrCurr" + i.ToString(), Text = currency };
                    ltrCurrency.Attributes.Add("Style", "padding-right:4px; font-weight:bold;");
                    ltrCurrency.Attributes.Add("class", "currency-value");
                    var hdnClass = new Label();
                    hdnClass.Text = "CLASS-01" + e.Row.RowIndex.ToString();
                    hdnClass.Style.Add("display", "none");
                    ltrName.Attributes.Add("class", "ltrName");
                    ltrName.Attributes.Add("Style", "padding-right:4px;");
                    ltrName.Text = string.IsNullOrEmpty(e.Row.Cells[i].Text) || e.Row.Cells[i].Text == "&nbsp;" ? "-" : e.Row.Cells[i].Text;
                    e.Row.Cells[i].Controls.Add(hdnClass);
                    if (ltrName.Text != "-")
                    {
                        e.Row.Cells[i].Controls.Add(ltrCurrency);
                        hdnActualPrice.Value = ltrName.Text;
                        ltrName.Text = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(ltrName.Text), siteId, productCurID, Guid.Parse(curID))));
                    }
                    e.Row.Cells[i].Controls.Add(hdnActualPrice);
                    e.Row.Cells[i].Controls.Add(ltrName);
                    if (!string.IsNullOrEmpty(e.Row.Cells[i].Text) && e.Row.Cells[i].Text != "&nbsp;")
                    {
                        var hdnCLvl = new Label();//get Country Level
                        hdnCLvl.ID = "hdnCLvl" + i.ToString();
                        hdnCLvl.Text = "0";
                        hdnCLvl.Attributes.Add("Style", "display:none;");
                        var hdnCLvlID = new LinkButton();//Get C0untry StartCode EndCpode
                        hdnCLvlID.ID = "hdnCLvlID" + i.ToString();
                        hdnCLvlID.CausesValidation = false;
                        hdnCLvlID.Click += new EventHandler(LnkGetCountryCode_Click);
                        hdnCLvlID.Attributes.Add("Style", "display:none;");
                        var ddl = new DropDownList();
                        ddl.ID = "ddlID" + i.ToString();
                        ddl.Attributes.Add("onchange", "callabc(this);");
                        if (IsReadonly && Convert.ToDecimal(hdnActualPrice.Value) < 1)
                            ddl.Attributes.Add("child", "child");

                        if (IsSaverTraveller)
                            endDrp = 5;
                        for (int j = startDrp; j <= endDrp; j++)
                            ddl.Items.Add(new ListItem(j.ToString(), j.ToString()));
                        if (IsSaverTraveller)
                        {
                            ddl.Items.RemoveAt(1);
                            if (Convert.ToBoolean(hdnIsBritral.Value))
                                ddl.Items.RemoveAt(1);
                        } 
                        endDrp = 10;

                        e.Row.Cells[i].Controls.Add(hdnCLvl);
                        e.Row.Cells[i].Controls.Add(ddl);
                        e.Row.Cells[i].Controls.Add(hdnCLvlID);
                        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(hdnCLvlID);
                        string namdsd = e.Row.Cells[0].Text + ", " + e.Row.Cells[1].Text + ", " + e.Row.Cells[i].Text;
                        if (Session["CountryCodeList"] != null)
                        {
                            Levellist = Session["CountryCodeList"] as List<getCuntryCode>;//get previous countrycode
                            if (Levellist != null)
                            {
                                var dd = Levellist.SingleOrDefault(ty => ty.Row == e.Row.Cells[0].Text && ty.ID == e.Row.Cells[1].Text);
                                if (dd != null)
                                {
                                    hdnCLvlID.Text = dd.CuntryCode;
                                    ddl.SelectedValue = dd.selectValue;
                                }
                            }
                        }
                        else
                        {
                            if (ViewState["setget"] != null)
                            {
                                string[] ViewStateData = ViewState["setget"].ToString().Split(',');
                                hdnCLvlID.Text = ViewStateData[0] + "," + ViewStateData[1] + "ñ,,,,";
                            }
                        }
                    }
                }
            }
        }
    }

    protected void LnkSortID_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        string id = btn.ID.Replace("LnkSortID", "lblname");
        var lbl = (Label)btn.Parent.FindControl(id);
        if (ClassName == "Asc")
        {
            SortcolumnName = lbl.Text + " " + ClassName;
            ClassName = "Desc";
        }
        else
        {
            SortcolumnName = lbl.Text + " " + ClassName;
            ClassName = "Asc";
        }
        PageLoadEvent();
        UpdatePanel1.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "getcurrentLinkPosition();callabc();", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "Validationxx()", true);
    }

    protected void LnkGetCountryCode_Click(object sender, EventArgs e)
    {
        var Levellist1 = new List<getCuntryCode>();
        var btn = (LinkButton)sender;
        string selid = btn.ID.Replace("hdnCLvlID", "ddlID");
        var selvalue = (DropDownList)btn.Parent.FindControl(selid);
        var row = (GridViewRow)(btn.Parent.Parent);

        if (Session["CountryCodeList"] != null)
        {
            Levellist1 = Session["CountryCodeList"] as List<getCuntryCode>;//get previous countrycode
            Session.Remove("CountryCodeList");
            if (Levellist1 != null)
                Levellist1.RemoveAll(ty => ty.Row == row.Cells[0].Text && ty.ID == row.Cells[1].Text);
        }
        btn.Text = (Session["GETcountryCODE"] != null ? Session["GETcountryCODE"].ToString() : "");
        if (!string.IsNullOrEmpty(btn.Text))
        {
            var datacode = new getCuntryCode();
            datacode.CuntryCode = btn.Text.Trim();
            datacode.Row = row.Cells[0].Text;
            datacode.ID = row.Cells[1].Text;
            datacode.selectValue = selvalue.SelectedValue;

            Levellist1.Add(datacode);
            Session["CountryCodeList"] = Levellist1;
            Session["GETcountryCODE"] = null;
        }
    }

    protected void rptTravellerGrid_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var grdPrice = e.Item.FindControl("grdPrice") as GridView;
            var lblID = e.Item.FindControl("lblID") as Label;
            var hdnName = e.Item.FindControl("hdnT") as HiddenField;
            if (lblID != null)
            {
                ViewState["tID"] = lblID.Text; /*added for saver alert*/
                if (ViewState["prdID"] != null)
                {
                    IsReadonly = false;
                    if (hdnName.Value.ToLower().Contains("child"))
                        IsReadonly = true;
                    if (hdnName.Value.ToLower().Contains("saver"))
                        IsSaverTraveller = true;
                    else
                        IsSaverTraveller = false;
                    GetProductListByTraveler(Guid.Parse(lblID.Text), Guid.Parse(ViewState["prdID"].ToString()), Guid.Parse(Session["siteId"].ToString()), grdPrice);
                }
            }
        }
    }
    
    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        try
        {
            var list = new List<getRailPassData>();
            string PrdtId = string.Empty;
            string PrdtName = string.Empty;
            string TravellerID = string.Empty;
            string TravellerName = string.Empty;
            string ValidityName = string.Empty;
            string Price = string.Empty;
            string Actualprice = string.Empty;
            string Qty = string.Empty;
            string CountryLevelCode = string.Empty;
            string MonthValidity = string.Empty;

            if (ViewState["prdID"] != null)
            {
                PrdtId = Convert.ToString(ViewState["prdID"]);
                PrdtName = _master.GetProductName(Guid.Parse(PrdtId));
                MonthValidity = _master.GetMonthValidity(Guid.Parse(PrdtId));
            }
            foreach (RepeaterItem item in rptTravellerGrid.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var grdPrice = item.FindControl("grdPrice") as GridView;
                    var lblTabId = item.FindControl("lblID") as Label;
                    if (grdPrice != null)
                    {
                        foreach (GridViewRow row in grdPrice.Rows)
                        {
                            TravellerID = lblTabId.Text;
                            foreach (RepeaterItem _TabName in rptTraveller.Items)
                            {
                                var lnkTabName = _TabName.FindControl("lnkTraveller") as LinkButton;
                                if (lnkTabName != null && lnkTabName.CommandArgument == TravellerID)
                                {
                                    TravellerName = lnkTabName.Text;
                                    break;
                                }
                            }
                            ValidityName = row.Cells[2].Text;
                            int countCell = row.Cells.Count;
                            var AgentId = new Guid();
                            if (Session["AgentUserID"] == null)
                            {
                                AgentId = Guid.NewGuid();
                            }
                            else
                                AgentId = Guid.Parse(Session["AgentUserID"].ToString());
                            for (int i = countCell; i > 0; i--)
                            {
                                string ltrName = "ltrName" + i.ToString();
                                string ddlIDs = "ddlID" + i.ToString();
                                string hdnCLvlIDs = "hdnCLvlID" + i.ToString();
                                string uppnls = "upd" + i.ToString();
                                string hdnPrice = "hdnActualPrice" + i.ToString();

                                var hdnActualPrice = row.FindControl(hdnPrice) as HiddenField;
                                var lblPrce = row.FindControl(ltrName) as Label;
                                var ddlID = row.FindControl(ddlIDs) as DropDownList;
                                var hdnCLvlID = row.FindControl(hdnCLvlIDs) as LinkButton;

                                if (ddlID != null && !ddlID.SelectedValue.Equals("0"))
                                {
                                    string ClassName = grdPrice.HeaderRow.Cells[i].Text;
                                    Qty = ddlID.SelectedValue;
                                    CountryLevelCode = hdnCLvlID.Text;
                                    Actualprice = hdnActualPrice.Value;
                                    Price = lblPrce.Text;

                                    string[] strSplit_dIDandCode = CountryLevelCode.Split('ñ');
                                    string[] strCode = strSplit_dIDandCode[0].Split(',');

                                    var listdata = _BMaster.GetClassAndValidityID((Actualprice.Trim()), ValidityName, ClassName, TravellerName, Guid.Parse(PrdtId), Guid.Parse(TravellerID), siteId, AgentId);
                                    for (int j = 0; j < Convert.ToInt32(Qty); j++)
                                    {
                                        var Id1 = Guid.NewGuid();
                                        if (listdata != null)
                                            list.Add(new getRailPassData
                                            {
                                                OriginalPrice = Convert.ToString(listdata.OriginalPrice),
                                                Id = Id1,
                                                PrdtId = PrdtId,
                                                TravellerID = TravellerID,
                                                ClassID = listdata.classid.ToString(),
                                                ValidityID = listdata.Validityid.ToString(),
                                                CategoryID = listdata.CategoryID.ToString(),
                                                IsBritrail = _BMaster.GetClassAndValidityID(listdata.CategoryID),
                                                PrdtName = PrdtName,
                                                TravellerName = TravellerName,
                                                ClassName = ClassName,
                                                ValidityName = ValidityName,
                                                Price = Price,
                                                SalePrice = Actualprice.Trim(),
                                                Qty = Qty,
                                                CountryStartCode = strCode[0],
                                                CountryEndCode = strCode[1],
                                                Commission = listdata.commission.ToString(),
                                                MarkUp = listdata.markup.ToString(),
                                                CountryLevelIDs = strSplit_dIDandCode[1],
                                                PassportIsVisible = passportValid,
                                                NationalityIsVisible = nationalityValid,
                                                MonthValidity = MonthValidity
                                            });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (list.Count > 0)
            {
                if (Session["RailPassData"] != null)
                {
                    var lstRP = Session["RailPassData"] as List<getRailPassData>;
                    if (lstRP.Count > 0)
                    {
                        if (lstRP.FirstOrDefault().IsBritrail)
                        {
                            if (list.Where(t => t.IsBritrail == true).Count() == list.Count())
                                lstRP.AddRange(list);
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "innertabActive()", true);
                                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "alert", "AlertMessage('Sorry! You can’t purchase another pass with Britrail pass.')", true);
                                return;
                            }
                        }
                        else
                        {
                            if (list.Where(t => t.IsBritrail == false).Count() == list.Count())
                                lstRP.AddRange(list);
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "innertabActive()", true);
                                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "alert", "AlertMessage('Sorry! You can’t purchase Britrail pass with this pass.')", true);
                                return;
                            }
                        }
                        Session["RailPassData"] = lstRP;
                    }
                    else
                        Session.Add("RailPassData", list);
                }
                else
                    Session.Add("RailPassData", list);
                Levellist.Clear();
                Session.Remove("CountryCodeList");
                Response.Redirect("~/BookingCart");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "runScript", "showmsg()", true);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void BindCountryName(Guid cuId)
    {
        try
        {
            if (cuId != new Guid())
            {
                var cuName = _cMaster.GetCountryNameById(cuId);
                //lblCn.Text = cuName.CountryName + " Passes";
            }
            else
            {
                //lblCn.Text = "Country";
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetCurrencyCode()
    {
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            curID = cookie.Values["_curId"];
            siteId = Guid.Parse(cookie.Values["_siteId"]);
            currency = oManageClass.GetCurrency(Guid.Parse(curID));
            hdnCurrencySign.Value = currency;
        }
    }

    public class getCuntryCode
    {
        public string CuntryCode { get; set; }
        public string Row { get; set; }
        public string ID { get; set; }
        public string selectValue { get; set; }
    }
}
