﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TextTemplate.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="TextTemplate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
    </style>
     <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <%--Banner section--%>
   <%-- <div class="banner">
        <img src="images/banner.jpg" class="scale-with-grid" alt="" border="0" style="margin-bottom:-5px!important"/>
    </div>--%>
    <%--Banner section end--%>
    <section class="content">
    <asp:Repeater ID="rptTemplate" runat="server">
        <ItemTemplate>
            <div class="left-content">
                <h1><%#Eval("Title")%></h1>
                <p><%#Eval("Description")%></p>
            </div>
        </ItemTemplate>
    </asp:Repeater>
<div class="right-content">
<div class="ticketbooking" style="padding-top:0px">
        <div class="list-tab divEnableP2P">
            <ul>
                <li><a href="#" class="active">Rail Tickets </a></li>
                <li><a href="rail-passes">Rail Passes </a></li>
            </ul>
        </div>
        <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
        <img src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
    </div>
<uc:Newsletter ID="Newsletter1" runat="server" />
</div>
</section>
</asp:Content>
