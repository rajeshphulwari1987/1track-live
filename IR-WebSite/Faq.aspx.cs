﻿using System;
using Business;
using System.Configuration;

public partial class Faq : System.Web.UI.Page
{
    readonly private ManageFaq _master = new ManageFaq();
    private Guid _siteId;
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    public string script;
    public string FaqDescription = "";
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            BindFaqQA(_siteId);
        }
    }

    public void BindFaqQA(Guid siteId)
    {
        var result = _master.GetFaqListonFront(siteId);
        if (result != null)
        {

            FaqDescription = _master.GetFaqDescriptionBySiteId(siteId); 
            rptFaq.DataSource = result;
            rptFaq.DataBind();
        }
    }
}