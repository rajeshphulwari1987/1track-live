﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="ContactUs.aspx.cs"
    Inherits="ContactUs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#aContact").addClass("active");

            var eMail = ($('#contactEmailTxt').html());
            function extractEmails(text) {
                return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
            }

            if (eMail != null) {
                if (eMail.indexOf('@') > "-1") {
                    document.getElementById("MainContent_hdnEmail").value = extractEmails(eMail).join('\n');
                }
            }
        });
    </script>
    <style type="text/css">
        .clsAbs
        {
            display: none;
        }
        .download
        {
            padding: 6px 9px;
            background: #e7f7d3;
            border: 1px solid #6c3;
            font-size: 13px !important;
        }
    </style>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function disablePhn(e) {
            if ($("#MainContent_txtPhn").val() != '') {
                document.getElementById("MainContent_hdnPhn").value = $("#MainContent_txtPhn").val();
                $("#MainContent_txtPhn").attr("disabled", "disabled");
            }
        }

        function disableName(e) {
            if ($("#MainContent_txtName").val().trim().length > 0) {
                document.getElementById("MainContent_hdnName").value = $("#MainContent_txtName").val();
                $("#MainContent_txtName").attr("disabled", "disabled");
            }
        }

        function disableEmail(e) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var emailaddressVal = $("#MainContent_txtEmail").val();

            if (emailReg.test(emailaddressVal) && ($("#MainContent_txtEmail").val().trim().length > 0)) {
                document.getElementById("MainContent_hdnFrmEmail").value = $("#MainContent_txtEmail").val();
                $("#MainContent_txtEmail").attr("disabled", "disabled");
            }
        }

        function disableOrder(e) {
            if ($("#MainContent_txtOrderNo").val() != '') {
                document.getElementById("MainContent_hdnOrderNo").value = $("#MainContent_txtOrderNo").val();
                $("#MainContent_txtOrderNo").attr("disabled", "disabled");
            }
        }

        function disableDesp(e) {
            if ($("#MainContent_txtDesp").val().trim().length > 0) {
                document.getElementById("MainContent_hdnDesp").value = $("#MainContent_txtDesp").val();
                $("#MainContent_txtDesp").attr("disabled", "disabled");
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".in-box").click(function () {
                $(this).val("");
            });

            $(".in-box").blur(function () {
                $(this).val(this.value);
            });
        });       
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%--Banner section--%>
    <div id="dvBanner" runat="server" class="banner">
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%=SiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content-inner">
        
<div style="float: left;width:658px">
<div id="divSta" style="float:left;width:658px" runat="server">
   <div id="ContentHead" runat="server"></div>
   <div id="ContentText" runat="server"></div>
  
<div class="formblock" style="width:40%;">
    <div>
        <asp:TextBox ID="txtName" class="in-box" runat="server" Text="Your Name" onblur="disableName(this)"/>
        <asp:HiddenField ID="hdnName" runat="server"/>
        <asp:RequiredFieldValidator ID="rfNm" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtName" ValidationGroup="vs"/>
        <asp:RequiredFieldValidator ID="rfNm1" Display="Dynamic" runat="server" ErrorMessage="*" InitialValue="Your Name" ForeColor="Red" ControlToValidate="txtName" ValidationGroup="vs"/>
    </div>
    <div>
        <asp:TextBox ID="txtEmail" class="in-box" runat="server" value="Email Address" onblur="disableEmail(this)"/>
        <asp:HiddenField ID="hdnFrmEmail" runat="server"/>
        <asp:RequiredFieldValidator ID="rfEmail" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtEmail" ValidationGroup="vs"/>
        <asp:RegularExpressionValidator ID="reEmail" Display="Dynamic" runat="server" ControlToValidate="txtEmail" ForeColor="Red" Font-Size="12px"
        ErrorMessage="Invalid Email Address" ValidationExpression="^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(Email Address)$" ValidationGroup="vs"/>
   </div>
    <div>
        <asp:TextBox ID="txtPhn" class="in-box" runat="server" value="Phone No" MaxLength="15" onkeypress="return isNumberKey(event)" onblur="disablePhn(this)"/>
        <asp:HiddenField ID="hdnPhn" runat="server"/>
        <asp:RequiredFieldValidator ID="rfPhone" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPhn" ValidationGroup="vs"/>
        <asp:RequiredFieldValidator ID="rfPhone1" Display="Dynamic" InitialValue="Phone No" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPhn" ValidationGroup="vs"/>
    </div>
    <div>
        <asp:TextBox ID="txtOrderNo" class="in-box" runat="server" value="Order number (if applicable)" onkeypress="return isNumberKey(event)" onblur="disableOrder(this)"/>
        <asp:HiddenField ID="hdnOrderNo" runat="server"/>
        <asp:RequiredFieldValidator ID="rfOrder" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtOrderNo" ValidationGroup="vs"/>
    </div>
    <div>
    <asp:TextBox ID="txtDesp" class="txtarea-box" runat="server" TextMode="MultiLine" onblur="disableDesp(this)"/>
    <asp:HiddenField ID="hdnDesp" runat="server"/>
    <asp:HiddenField ID="hdnEmail" runat="server"/>
    <asp:Button id="btnSubmit" class="f-left w184" runat="server" Text="SUBMIT" 
            ValidationGroup="vs" OnClick="btnSubmit_Click" />
    </div>
</div>
<div id="ContactPanel" class="address-block" runat="server">
</div>
</div>
<div id="divLanding" style="float:left;width:650px" runat="server" Visible="False">
    <h1>Thank you</h1>
    
    <p class="download">Thank you for your interest in International Rail. Your comment/query has been sent successfully. This information will enable us to route your request to the appropriate person. You should receive a response soon.</p>

</div>
</div> 
<div id="callBlock" class="map-block" style="float:right;" runat="server">
</div>
</section>
</asp:Content>
