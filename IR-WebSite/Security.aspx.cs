﻿using System;
using Business;
using System.Linq;
using System.Web;
using System.Configuration;

public partial class Security : System.Web.UI.Page
{
    readonly Masters _master = new Masters();
    private Guid _siteId;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            BindSecurity(_siteId);
        }
    }

    public void BindSecurity(Guid siteId)
    {
        var result = _master.GetSecurityBySiteid(siteId);
        if (result != null)
        {
            lblTitle.Text = result.Title;
            lblDesp.Text = Server.HtmlDecode(result.Description);
        }
    }
}