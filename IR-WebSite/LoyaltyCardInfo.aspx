﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="LoyaltyCardInfo.aspx.cs" Inherits="LoyaltyCardInfo" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        #MainContent_rdBookingType label
        {
            float: none !important;
            padding-top: 6px !important;
        }
        #MainContent_ddlClass label
        {
            float: none !important;
            padding-top: 6px;
        }
        .d-opt
        {
            opacity: 0.5;
            cursor: default;
        }
        .m-none
        {
            margin-left: 0 !important;
        }
        .input
        {
            margin-left: 0 !important;
        }
        .loading
        {
            background-image: url(images/loading3.gif);
            background-position: right;
            background-repeat: no-repeat;
        }
        .modalBackground
        {
            position: fixed;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            overflow: hidden;
            padding: 0;
            margin: 0;
            background-color: #000;
            filter: alpha(opacity=50);
            opacity: 0.5;
            text-align: center;
            float: left;
            z-index: 99;
        }
        .progessposition
        {
            padding-top: 20%;
        }
        
        
        .full-row
        {
            clear: both;
            width: 100%;
        }
        .colum03
        {
            color: #424242;
            float: left;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            padding: 5px 0;
            width: 45%;
        }
        .round-titles
        {
            width: 98%;
            float: left;
            height: 20px;
            line-height: 20px;
            border-radius: 5px 5px 0 0;
            -moz-border-radius: 5px 5px 0 0;
            -webkit-border-radius: 5px 5px 0 0;
            -ms-border-radius: 5px 5px 0 0;
            behavior: url(PIE.htc);
            background: #4a4a4a;
            padding: 1%;
            color: #FFFFFF;
            font-size: 14px;
            font-weight: bold;
        }
        
        .booking-detail-in
        {
            width: 98%;
            padding: 1%;
            border-radius: 0 0 5px 5px;
            -moz-border-radius: 0 0 5px 5px;
            -webkit-border-radius: 0 0 5px 5px;
            -ms-border-radius: 0 0 5px 5px;
            behavior: url(PIE.htc);
            margin-bottom: 20px;
            background: #ededed;
            float: left;
            position: relative;
        }
        
        .booking-detail-in table.grid
        {
            width: 100%;
            border: 4px solid #ededed;
            border-collapse: collapse;
        }
        .booking-detail-in table.grid tr th
        {
            border-bottom: 2px solid #ededed;
            text-align: left;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td
        {
            border-bottom: 2px solid #ededed;
            background: #FFF;
            font-size: 13px;
            color: #666666;
            height: 25px;
            line-height: 25px;
            padding-left: 5px;
        }
        
        .booking-detail-in table.grid tr td a
        {
            color: #951F35;
        }
        
        .booking-detail-in .total
        {
            border-top: 1px dashed #951F35;
            width: 27%;
            float: right;
            color: #4A4A4A;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding-top: 10px;
        }
        
        .booking-detail-in .colum-label
        {
            float: left;
            width: 30%;
            float: left;
            color: #424242;
            font-size: 13px;
            line-height: 30px;
            height: 30px;
            padding: 5px 0;
        }
        .booking-detail-in .colum-input
        {
            position: relative;
            float: left;
            width: 70%;
            float: left;
            line-height: 30px;
            height: 30px !important;
            padding: 5px 0;
            font-size: 13px;
            color: #424242;
        }
        .booking-detail-in .colum-input .input
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 25px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        .aspNetDisabled
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 24px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
            margin-right: 5px;
        }
        select.aspNetDisabled
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 25px;
            width: 95%;
            padding: 0.5%;
        }
        .booking-detail-in .colum-input .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 40%;
            padding: 0.5%;
            margin-left: 5px;
        }
        .booking-detail-in .full-row .colum03 .inputsl
        {
            border: 1px solid #ADB9C2;
            border-radius: 5px 5px 5px 5px;
            color: #424242;
            font-size: 13px;
            height: 30px;
            line-height: 30px;
            width: 50%;
            padding: 0.5%;
        }
        .lblock
        {
            background-color: #FFF;
            color: #424242;
            font-size: 13px;
            padding: 10px;
        }
        .divLoy
        {
            float: left;
            margin: 5px;
            width: 250px;
        }
        
        .divLoyleft
        {
            float: left;
            margin-bottom: 5px;
            width: 95px;
        }
        .divLoyright
        {
            float: left;
            margin-bottom: 5px;
            width: 150px;
        }
        .divLoyleft
        {
            float: left;
            margin-bottom: 5px;
            width: 95px;
        }
        .divLoyright
        {
            float: left;
            margin-bottom: 5px;
            width: 150px;
        }
        .divLoy input[type="text"]
        {
            background: none repeat scroll 0 0 #D5D4D4;
            border: 0 none;
            color: #333333;
            float: left;
            height: 25px !important;
            line-height: 25px !important;
            text-indent: 5px;
            width: 150px !important;
        }
        .txtLoyleft
        {
            background: none repeat scroll 0 0 #696969 !important;
            border: 0 none;
            color: #000000;
            float: left;
            height: 25px !important;
            line-height: 25px !important;
            text-indent: 5px;
            width: 90px !important;
        }
        .clsInput
        {
            border-radius: 5px 5px 5px 5px;
            line-height: 25px !important;
            border: 1px solid #ADB9C2;
        }
        #_bindDivData, .popupselect, .popupselectRemove
        {
            width: 433px;
        }
    </style>
    <script type="text/javascript">
        var count = 0;
        var countKey = 1;
        var conditionone = 0;
        var conditiontwo = 0;
        var tabkey = 0;

        $(function () {
            $("#txtFrom").focus();
            if ($("#txtFrom").val() != '') {
                //                alert(localStorage.getItem("spantxtTo"));
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                //                alert(localStorage.getItem("spantxtFrom"));
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            $(window).click(function (event) {
                $('#_bindDivData').remove();
            });
            $(window).keydown(function (event) {
                if (event.keyCode == 13 || (event.keyCode == 9 && tabkey == 1)) {
                    tabkey = 0;
                    event.preventDefault();
                    return false;
                }
                tabkey = 1;
            });
            $("#txtFrom , #txtTo").on('keydown', function (event) {
                //40,38
                var $id = $(this);
                var maxno = 0;
                count = event.keyCode;

                $(".popupselect").each(function () { maxno++; });
                if (count == 13 || count == 9) {
                    $(".popupselect").each(function () {
                        if ($(this).attr('style') != undefined) {
                            $(this).trigger('onclick');
                            $id.val($.trim($(this).find('span').text()));
                        }
                    });
                    $('#_bindDivData').remove();

                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                else if (count == 40 && maxno > 1) {
                    conditionone = 1;
                    if (countKey == maxno)
                        countKey = 0;
                    if (conditiontwo == 1) {
                        countKey++;
                        conditiontwo = 0;
                    }
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                    countKey++;
                }
                else if (count == 38 && maxno > 1) {
                    conditiontwo = 1;
                    if (countKey == 0)
                        countKey = maxno;
                    if (conditionone == 1) {
                        countKey--;
                        conditionone = 0;
                    }
                    countKey--;
                    $(".popupselect").removeAttr('style');
                    $(".popupselect:eq(" + countKey + ")").attr('style', 'background-color: #ccc !important');
                }
                else {
                    countKey = 1;
                    conditionone = 0;
                    conditiontwo = 0;
                }
                $id.focus();
            });
        });
        function selectpopup(e) {
            if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                var $this = $(e);
                var data = $this.val();
                var station = '';
                var hostName = window.location.host;
                var url = "http://" + hostName;

                if (window.location.toString().indexOf("https:") >= 0)
                    url = "https://" + hostName;
 

                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }
                var filter = $("#span" + $this.attr('id') + "").text();
                if (filter == "" && $this.val() != "")
                    filter = $("#hdnFilter").val();
                $("#hdnFilter").val(filter);

                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }

                if (hostName == "localhost") {
                    url = url + "/InterRail";
                }
                var hostUrl = url + "/StationList.asmx/getStationsXList";
                data = data.replace(/[']/g, "♥");

                var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {
                        $('#_bindDivData').remove();
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length; ;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);
                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                        });
                        $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                    },
                    error: function () {
                        //                    alert("Wait...");
                    }
                });
            }
        }
        function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
            $('#_bindDivData').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }

        function CheckCardVal(sender, args) {
            if (document.getElementById('MainContent_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
        $(document).ready(function () {
            LoadCal();
        });
        function LoadCal() {
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: '<%=minDate %>',
                onClose: function (selectedDate) {
                    $("#MainContent_txtReturnDate").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#MainContent_txtReturnDate").datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0
            });

            $("#MainContent_txtReturnDate").datepicker("option", "minDate", $("#MainContent_txtDepartureDate").datepicker('getDate'));

            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });

            $(".imgCal").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });

            $(".imgCal1, #MainContent_txtReturnDate").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked'))
                    $("#MainContent_txtReturnDate").datepicker('show');
            });
        }

        function calenable() {
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('enable');
        }
        function caldisable() {
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('disable');
        }
    </script>
    <%=script %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
        ValidationGroup="vgs" ShowSummary="false" ShowMessageBox="true" />
    <div class="content">
        <div class="breadcrumb">
            <a href="Home">Home </a>>>Rail Tickets
        </div>
        <div class="innner-banner">
            <div class="bannerLft">
                <div class="banner90">
                    Rail Tickets</div>
            </div>
            <div>
                <div class="float-lt" style="width: 73%">
                    <asp:Image ID="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="901px" Height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
                <div class="float-rt" style="width: 27%; text-align: right;">
                    <asp:Image ID="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Width="100%" Height="190px" ImageUrl="images/innerMap.gif" />
                </div>
            </div>
        </div>
        <div class="left-content" id="divSearch" runat="server" >
            <h1>
                Travel selection
            </h1>
            <asp:UpdatePanel ID="updLoyalty" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="warning" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" />
                        </div>
                    </asp:Panel>
                    <div class="round-titles">
                        Journey Details
                    </div>
                    <div class="booking-detail-in">
                        <div class="colum-label">
                            From
                        </div>
                        <div class="colum-input">
                            <asp:TextBox ID="txtFrom" ClientIDMode="Static" onkeyup="selectpopup(this)" runat="server"
                                placeholder="From" class="input" autocomplete="off" />
                            <span id="spantxtFrom" style="display: none"></span>
                            <asp:HiddenField ID="hdnFrm" runat="server" />
                            <asp:RequiredFieldValidator ID="reqLoyFrom" runat="server" ForeColor="red" ValidationGroup="vgs"
                                Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                                Text="*" />
                        </div>
                        <div class="colum-label">
                            To
                        </div>
                        <div class="colum-input">
                            <asp:TextBox ClientIDMode="Static" ID="txtTo" runat="server" onkeyup="selectpopup(this)"
                                placeholder="To" class="input" autocomplete="off" />
                            <span id="spantxtTo" style="display: none"></span>
                            <asp:RequiredFieldValidator ID="reqLoyTo" runat="server" ForeColor="red" ValidationGroup="vgs"
                                Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                                Text="*" CssClass="font14" />
                        </div>
                        <div class="colum-label">
                            Journey
                        </div>
                        <div class="colum-input">
                            <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vg" RepeatDirection="Horizontal"
                                AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                                <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                                <asp:ListItem Value="1">Return</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="colum-label">
                            Depart
                        </div>
                        <div class="colum-input col-calender">
                            <asp:TextBox ID="txtDepartureDate" runat="server" class="input" Text="DD/MM/YYYY"
                                Style="width: 50%!important; margin-right: 4px; float: left;" />
                            <span class="imgCal calIcon" style="float: left; margin-right: 5px;" title="Select DepartureDate.">
                            </span>
                            <asp:RequiredFieldValidator ID="reqLoyDepartureDate" runat="server" ForeColor="red"
                                ValidationGroup="vgs" Display="Dynamic" ControlToValidate="txtDepartureDate"
                                ErrorMessage="Please enter departure date." Text="*" />
                            <asp:RegularExpressionValidator ID="reqLoyregx" runat="server" ControlToValidate="txtDepartureDate"
                                ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                                ValidationGroup="vgs">*</asp:RegularExpressionValidator>
                            <asp:DropDownList ID="ddldepTime" runat="server" class="slbox01 clsInput" Style="width: 179px !important;
                                margin-left: 0px; float: left;">
                                <asp:ListItem>00:00</asp:ListItem>
                                <asp:ListItem>01:00</asp:ListItem>
                                <asp:ListItem>02:00</asp:ListItem>
                                <asp:ListItem>03:00</asp:ListItem>
                                <asp:ListItem>04:00</asp:ListItem>
                                <asp:ListItem>05:00</asp:ListItem>
                                <asp:ListItem>06:00</asp:ListItem>
                                <asp:ListItem>07:00</asp:ListItem>
                                <asp:ListItem>08:00</asp:ListItem>
                                <asp:ListItem Selected="True">09:00</asp:ListItem>
                                <asp:ListItem>10:00</asp:ListItem>
                                <asp:ListItem>11:00</asp:ListItem>
                                <asp:ListItem>12:00</asp:ListItem>
                                <asp:ListItem>13:00</asp:ListItem>
                                <asp:ListItem>14:00</asp:ListItem>
                                <asp:ListItem>15:00</asp:ListItem>
                                <asp:ListItem>16:00</asp:ListItem>
                                <asp:ListItem>17:00</asp:ListItem>
                                <asp:ListItem>18:00</asp:ListItem>
                                <asp:ListItem>19:00</asp:ListItem>
                                <asp:ListItem>20:00</asp:ListItem>
                                <asp:ListItem>21:00</asp:ListItem>
                                <asp:ListItem>22:00</asp:ListItem>
                                <asp:ListItem>23:00</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div id="returndiv" runat="server">
                            <div class="colum-label">
                                Return
                            </div>
                            <div class="colum-input col-calender">
                                <asp:TextBox ID="txtReturnDate" runat="server" class="input" Text="DD/MM/YYYY" Style="width: 50%!important;
                                    margin-right: 4px; float: left;" Enabled="False" />
                                <span class="imgCal1 calIcon" style="float: left; margin-right: 5px;" title="Select Return Date.">
                                </span>
                                <asp:RequiredFieldValidator ID="reqLoyReturnDate" runat="server" ForeColor="red"
                                    ValidationGroup="vgs" Enabled="false" Display="Dynamic" ControlToValidate="txtReturnDate"
                                    ErrorMessage="Please enter return date." Text="*" />
                                <asp:RegularExpressionValidator ID="reqLoyReturnDateregx" runat="server" ControlToValidate="txtReturnDate"
                                    ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                    Display="Dynamic" ForeColor="Red" SetFocusOnError="true" ErrorMessage="Invalid return date"
                                    ValidationGroup="vgs" Enabled="False">*</asp:RegularExpressionValidator>
                                <asp:DropDownList ID="ddlReturnTime" runat="server" class="slbox01 clsInput" Enabled="false"
                                    Style="width: 179px !important; margin-left: 0px; float: left;">
                                    <asp:ListItem>00:00</asp:ListItem>
                                    <asp:ListItem>01:00</asp:ListItem>
                                    <asp:ListItem>02:00</asp:ListItem>
                                    <asp:ListItem>03:00</asp:ListItem>
                                    <asp:ListItem>04:00</asp:ListItem>
                                    <asp:ListItem>05:00</asp:ListItem>
                                    <asp:ListItem>06:00</asp:ListItem>
                                    <asp:ListItem>07:00</asp:ListItem>
                                    <asp:ListItem>08:00</asp:ListItem>
                                    <asp:ListItem Selected="True">09:00</asp:ListItem>
                                    <asp:ListItem>10:00</asp:ListItem>
                                    <asp:ListItem>11:00</asp:ListItem>
                                    <asp:ListItem>12:00</asp:ListItem>
                                    <asp:ListItem>13:00</asp:ListItem>
                                    <asp:ListItem>14:00</asp:ListItem>
                                    <asp:ListItem>15:00</asp:ListItem>
                                    <asp:ListItem>16:00</asp:ListItem>
                                    <asp:ListItem>17:00</asp:ListItem>
                                    <asp:ListItem>18:00</asp:ListItem>
                                    <asp:ListItem>19:00</asp:ListItem>
                                    <asp:ListItem>20:00</asp:ListItem>
                                    <asp:ListItem>21:00</asp:ListItem>
                                    <asp:ListItem>22:00</asp:ListItem>
                                    <asp:ListItem>23:00</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colum-label">
                            Adult&nbsp;<span class="sml-txt">(12+ yrs)</span>
                        </div>
                        <div class="colum-input adult">
                            <asp:DropDownList ID="ddlAdult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAdult_SelectedIndexChanged"
                                class="slbox01 clsInput" Style="width: 35%!important;">
                            </asp:DropDownList>
                            <span class="inline-txt">Children <span class="sml-txt">(4-11 yrs)</span></span>
                            <asp:DropDownList ID="ddlChild" runat="server" class="slbox01 clsInput" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" Style="float: right; margin-right: 18px;
                                width: 35%!important;">
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            Youth&nbsp;<span class="sml-txt">(12-25 yrs)</span>
                        </div>
                        <div class="colum-input adult">
                            <asp:DropDownList ID="ddlYouth" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYouth_SelectedIndexChanged"
                                class="slbox01 clsInput" Style="width: 35%!important;">
                            </asp:DropDownList>
                            <span class="inline-txt">Senior <span class="sml-txt">(60+ yrs)</span> </span>
                            <asp:DropDownList ID="ddlSenior" runat="server" class="slbox01 clsInput" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlSenior_SelectedIndexChanged" Style="float: right;
                                margin-right: 18px; width: 35%!important;">
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            Class Preference
                        </div>
                        <div class="colum-input" style="height: auto;">
                            <asp:RadioButtonList ID="ddlClass" runat="server" RepeatDirection="Horizontal" Width="150px">
                                <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                <asp:ListItem Value="1">1st</asp:ListItem>
                                <asp:ListItem Value="2">2nd</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="colum-label">
                            Max Transfers
                        </div>
                        <div class="colum-input">
                            <asp:DropDownList ID="ddlTransfer" runat="server" CssClass="slbox01 clsInput" Style="width: 40%!important;
                                margin-left: 0 !important;">
                                <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                                <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                                <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                                <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colum-label">
                            &nbsp;
                        </div>
                        <div class="colum-input">
                            <div class="check-colum1">
                                <asp:CheckBox ID="chkLoyalty" runat="server" AutoPostBack="true" OnCheckedChanged="chkLoyalty_CheckedChanged" />
                                <strong>Loyalty cards</strong>
                                <asp:CustomValidator ID="cusCNE" ClientValidationFunction="CheckCardVal" runat="server"
                                    ValidationGroup="vgs" ErrorMessage="Eurostar/Thalys card number missing." Text="*"
                                    Display="Dynamic" ControlToValidate="ddlChild"></asp:CustomValidator>
                                <span id="divRailPass" class="check-colum2" runat="server" visible="False">
                                    <asp:CheckBox ID="chkIhaveRailPass" runat="server" AutoPostBack="true" OnCheckedChanged="chkIhaveRailPass_CheckedChanged" />
                                    <strong>I have a rail pass</strong></span>
                            </div>
                        </div>
                        <div class="colum-input">
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div id="FtpCardsDescriptionContent" class="ptop lblock" runat="server" visible="false">
                            Do you have a Thalys TheCard or Eurostar Frequent traveller loyalty card for journeys
                            on these trains?
                            <br />
                            If yes, Please input your card number.
                            <br />
                            <asp:DataList ID="dtlLoayalty" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <div class="divLoy">
                                        <div class="divLoyHeader">
                                            <%#Eval("PassangerType")%>
                                        </div>
                                        <div class="divLoyleft">
                                            <asp:TextBox ID="txtloyCardEur" runat="server" CssClass="txtLoyleft" Style="color: #FFF;"
                                                Text="Eurostar" ReadOnly="True" />
                                        </div>
                                        <div class="divLoyright">
                                            <asp:TextBox ID="txtloyCardNoEur" runat="server" CssClass="txtLoy LCE" MaxLength="25"
                                                Text="308381" />
                                            <asp:TextBoxWatermarkExtender runat="server" ID="txtWEuro" WatermarkText="(card number)"
                                                TargetControlID="txtloyCardNoEur" WatermarkCssClass="WmaxCss" />
                                            <asp:FilteredTextBoxExtender ID="tr1" runat="server" TargetControlID="txtloyCardNoEur"
                                                ValidChars="0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                        </div>
                                        <div class="divLoyleft">
                                            <asp:TextBox ID="txtloyCardThy" runat="server" CssClass="txtLoyleft" Style="color: #FFF;"
                                                Text="Thalys" ReadOnly="True" />
                                        </div>
                                        <div class="divLoyright">
                                            <asp:TextBox ID="txtloyCardNoThy" runat="server" CssClass="txtLoy LCT" MaxLength="25"
                                                Text="308406" />
                                            <asp:TextBoxWatermarkExtender runat="server" ID="txtWThy" WatermarkText="(card number)"
                                                TargetControlID="txtloyCardNoThy" WatermarkCssClass="WmaxCss" />
                                            <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtloyCardNoThy"
                                                ValidChars="0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-top: 20px;">
                            <asp:Button ID="btnCheckout" runat="server" Text="SEARCH" CssClass="btn-red-cart btn-sml f-right margr"
                                ValidationGroup="vgs" Style="width: 15%!important;" OnClick="btnCheckout_Click" />
                        </div>
                        <asp:Label runat="server" ID="lblmsg" />
                        <div class="swap-arr2" onclick="btnswapstation()" title="Swap">
                            <div class="swap-img">
                                <img src="images/swap-arr.png" alt="" title="Swap" /></div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlAdult" />
                    <asp:AsyncPostBackTrigger ControlID="ddlChild" />
                    <asp:AsyncPostBackTrigger ControlID="ddlYouth" />
                    <asp:AsyncPostBackTrigger ControlID="ddlSenior" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
         <div class="right-content">
            <uc:ucRightContent ID="ucRightContent" runat="server" />
        </div>
    </div>
    <asp:HiddenField ID="hdnForModelUK" runat="server" />
    <asp:HiddenField ID="hdnForModel" runat="server" />
    <asp:ModalPopupExtender ID="mdPassengerUK" runat="server" CancelControlID="btnCloseWin"
        PopupControlID="pnlPassengerUK" BackgroundCssClass="modalBackground" TargetControlID="hdnForModelUK" />
    <asp:Panel ID="pnlPassengerUK" runat="server" CssClass="modal-popup">
        <div class="popup-inner">
            <div class="title">
                Please select passenger type
            </div>
            <p>
                Please enter at least 1 adult, senior or junior(youth) passenger.</p>
            <p>
                Children cannot travel unattended.</p>
            <p>
                There is a limitation for accompanied children:</p>
            <p>
                - Max. 4 children per passenger allowed.</p>
            <p>
                - On Eurostar, the number of children under 4 travelling for free is limited to
                1 child per accompanying adult.</p>
            <div>
                <asp:Button ID="btnCloseWin" runat="server" Text="Close" CssClass="btn-black newbutton"
                    Style="width: 20%!important; float: right" />
            </div>
        </div>
    </asp:Panel>
    <div class="clear">
    </div>
    <asp:ModalPopupExtender ID="mdPassenger" runat="server" CancelControlID="btnCloseWin"
        PopupControlID="pnlPassenger" BackgroundCssClass="modalBackground" TargetControlID="hdnForModel" />
    <asp:Panel ID="pnlPassenger" runat="server" CssClass="modal-popup" >
        <div class="popup-inner">
            <div class="title">
                Please select passenger type
            </div>
            <p>
                Please enter at least 1 adult, senior or junior(youth) passenger.</p>
            <p>
                Children cannot travel unattended.</p>
            <p>
                There is a limitation for accompanied children:</p>
            <p>
                - Max. 4 children per passenger allowed.</p>
            <div>
                <asp:Button ID="Button1" runat="server" Text="Close" CssClass="btn-black newbutton"
                    Style="width: 20%!important; float: right" />
            </div>
        </div>
    </asp:Panel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updLoyalty"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                Shovelling coal into the server...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        function btnswapstation() {
            var spantxtFrom = '';
            var txtFrom = '';
            spantxtFrom = $("#spantxtFrom").text();
            $("#spantxtFrom").text($("#spantxtTo").text());
            $("#spantxtTo").text(spantxtFrom);
            txtFrom = $("#txtFrom").val();
            $("#txtFrom").val($("#txtTo").val());
            $("#txtTo").val(txtFrom);
        }
    
    </script>
</asp:Content>
