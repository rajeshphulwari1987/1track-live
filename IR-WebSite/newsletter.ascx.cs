﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

public partial class newsletter : UserControl
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private Guid _siteId;
    public string siteURL;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindNewsArea();
        }
    }

    protected void btnNews_Click(object sender, EventArgs e)
    {
        try
        {
            var siteId = Session["SiteId"];
            var newsUser = new tblNewsLetterUser
            {
                ID = Guid.NewGuid(),
                Email = txtEmail.Text.Trim(),
                Name = txtName.Text.Trim(),
                SiteID = Guid.Parse(siteId.ToString()),
                IsActive = true,
                IsSubscribed = true,
                CreatedOn = DateTime.Now
            };

            var isExist = _db.tblNewsLetterUsers.FirstOrDefault(x => x.Email == txtEmail.Text.Trim());
            if (isExist != null)
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('Email is already subscribed.')", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "N1", "alert('Email is already subscribed.')", true);
                txtEmail.Text = string.Empty;
                txtName.Text = string.Empty;
                foreach (TreeNode node in trNews.Nodes)
                {
                    node.Checked = false;
                }
            }
            else
            {
                var userID = _master.AddNewsletterUser(newsUser);
                foreach (TreeNode node in trNews.Nodes)
                {
                    if (node.Checked)
                    {
                        var newsLookSites = new tblNewsLetterUserAreaLookup { ID = Guid.NewGuid(), UserID = userID, AreaID = Guid.Parse(node.Value) };
                        _master.AddNewsAreaLookupSites(newsLookSites);
                    }
                }

                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "", "alert('You have been subscribed successfully.')", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('You have been subscribed successfully.')", true);
                txtEmail.Text = string.Empty;
                txtName.Text = string.Empty;
                foreach (TreeNode node in trNews.Nodes)
                {
                    node.Checked = false;
                }
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri,
                                 ex.Message + "; Inner Exception:" +
                                 (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void BindNewsArea()
    {
        IEnumerable<tblNewsArea> objNewsArea = _master.GetNewsArea().Where(x => x.IsActive).ToList();
        foreach (var oNews in objNewsArea)
        {
            var trNArea = new TreeNode { Text = oNews.Name, Value = oNews.ID.ToString(), SelectAction = TreeNodeSelectAction.None };
            trNews.Nodes.Add(trNArea);
        }
    }
}