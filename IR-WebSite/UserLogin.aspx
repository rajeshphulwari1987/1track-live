﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="UserLogin.aspx.cs" Inherits="UserLogin" %>

<%@ Register TagPrefix="uc" TagName="GeneralInformation" Src="~/UserControls/UCGeneralInformation.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/jscript">
        $(document).ready(function () {
            if ($("#lblerrorReg").text() != '') {
                $("#Signupclick").trigger("click");
            }
            $("#forgotpass").click(function () {
                $("#divforgot").show();
                $("#divLogin").hide();
                $("#login-sec").show();
                $("#divRegistration").hide();
            });
            $("#Signupclick").click(function () {
                $("#loginforgot").hide();
                $("#login-sec").hide();
                $("#divRegistration").show();
            });

            $("#Signin").click(function () {
                $("#divLogin").hide();
                $("#divforgot").hide();
                $("#loginforgot").show();
                $("#divLogin").show();
            });

            $("#Loginclick").click(function () {
                $("#Signin").trigger("click");
            });
        });
        window.setTimeout("closeDiv();", 20000);
        function closeDiv() {
            $("#MainContent_DivSuccess").fadeOut("slow", null);
            $("#MainContent_DivError").fadeOut("slow", null);
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> User Login </div>
<div class="innner-banner">
<div class="btn-austrailia"> <a href="#"><img src='images/australia.jpg' alt="" border="0" /></a> </div>
<div class="btn-conect"> <a href="#"><img src='images/btn-wewill-conected.png' alt="" border="0" /></a> </div>
<img src='images/inner-banner.jpg' class="scale-with-grid" alt="" border="0" />
</div>
 <div class="left-content">
     <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="Label1" runat="server" /></div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
        </asp:Panel>
 <div id="login-sec">
                <div class="full">
                    <span class="clsUserLogin"></span></div>
                <div class="full mr-tp2">
                    <div class="full corner">
                        <img src="images/login-top-bar.jpg" alt="" /><asp:ScriptManager 
                            ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                    </div>
                    <div class="login-data">
                        <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="loginform" DisplayMode="List" ShowSummary="false" ShowMessageBox="true" runat="server" />
                        <div id="divLogin"   class="login">
                            <div class="full">
                                <div class="login-col1" style="width:90px">
                                    Email Address</div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtUsername" class="login" runat="server"  ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtUsername" ControlToValidate="txtUsername" CssClass="validreq1" Display="Dynamic"
                                        runat="server" ErrorMessage="Please enter user name." ValidationGroup="loginform" Text="*"/>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col1" style="width:90px">
                                    Password</div>
                                <div class="login-col2">
                                    <label for="textfield">
                                    </label>
                                    <asp:TextBox ID="txtPassword" runat="server" class="login" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtPassword" ControlToValidate="txtPassword" CssClass="validreq1" Display="Dynamic"
                                        runat="server" ErrorMessage="Please enter password." Text="*" ValidationGroup="loginform"/>
                                </div>
                            </div>
                            <div class="full mr-tp3">
                                <div class="login-col1">
                                    &nbsp;</div>
                                <div class="login-col2">
                                    <asp:CheckBox ID="chkRememberMe" runat="server" />
                                    <label for="checkbox">
                                    </label>
                                    Remember my login details</div>
                            </div>
                            <div class="full mr-tp3">
                                     <div class="login-col2 shade5"  style=" margin:10px 0;">
                                    <a href="#" id="forgotpass">Forgot your password?</a>
                                   <a href="javascript:return(0)" id="Signupclick" class="Linkbtnheader" style="color:#000;padding-left:5px;">New User Registration</a>
                                    </div>
                            </div>
                            <div class="full mr-tp3"  >
                                    <div class="login-col2" style=" margin:5px 50px 0 0; float:right;">
                                    <asp:ImageButton ID="BtnSubmit" ImageUrl="images/btn-login.jpg" OnClick="BtnSubmit_Click"
                                        runat="server" ValidationGroup="loginform" />
                                    <asp:ImageButton ID="btnCancel" ImageUrl="images/btn-cancel.jpg" runat="server"
                                        OnClick="btnCancel_Click" />
                                        <br/>
                                          <asp:Label ID="lblSuccessMsg" runat="server" CssClass="valdreq"/>
                                </div>
                            </div>
                        </div>
                                             
                        <div style="display: none;" id="divforgot"   class="login-row1">
                            <div class="full mr-tp1">
                                <div class="login-col1">
                                    Email Id</div>
                                <div class="login-col2" >
                                    <asp:TextBox ID="txtEmail" class="login" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtEmail" ControlToValidate="txtEmail" CssClass="validreq"
                                        runat="server" ErrorMessage="Please enter registered email address." ValidationGroup="FLoginForm"/>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatortxtEmail" runat="server"
                                        ErrorMessage="Invalid Email" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="FLoginForm"/>
                                </div>
                            </div>
                            <div class="full mr-tp1">
                                <div class="login-col1">
                                    &nbsp;</div>
                                <div class="login-col2 light3 shade1">
                                    Click here <span style="font-size: 14px;">»</span> <a id="Signin" href="#">Sign In
                                    </a>
                                </div>
                            </div>
                            <div class="full mr-tp1">
                                     <div class="login-col2" style=" margin:5px 50px 0 0; float:right;">
                                    <asp:ImageButton ID="btnFSubmit" ValidationGroup="FLoginForm" ImageUrl="images/btn-request.jpg"
                                        OnClick="btnFSubmit_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                             
                </div>

                <div class="full corner">
                        <img src="images/login-bot-bar.jpg" alt="" /></div>
                         </div>
            </div>

            <div id="divRegistration" style="display:none;">
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="regstration" DisplayMode="List" ShowSummary="false" ShowMessageBox="true" runat="server" />
                      <h1><span id="lblHead">New User Registration</span></h1>
                       <p> 	All Users must first get authorization before purchasing rail passes.
                      <div class="register-in">
    <asp:textbox id="txtfname" runat="server" maxlength="200" class="in-box" />
    <asp:textboxwatermarkextender id="wtertxtfname" runat="server" watermarktext="Enter First Name"
        targetcontrolid="txtfname" />          
    <asp:requiredfieldvalidator id="RequiredFieldValidator2" controltovalidate="txtfname"
        cssclass="validreq" runat="server" errormessage="Please enter first name." Display="Dynamic" validationgroup="regstration" />
    <div id="div2" runat="server" style="display: block;">
        <asp:textbox id="txtlname" runat="server" maxlength="200" class="in-box" />
        <asp:textboxwatermarkextender id="wtertxtlname" runat="server" watermarktext="Enter Last Name"
            targetcontrolid="txtlname" />
        <div class="clear">
        </div>
        <div class="full">
            <asp:textbox id="txtdob" runat="server" maxlength="10" class="in-box" />
            <asp:textboxwatermarkextender id="wtertxtdob" runat="server" watermarktext="Enter D.O.B. "
                targetcontrolid="txtdob" />
            <asp:calendarextender id="cextStartDate" runat="server" targetcontrolid="txtdob"
                popupbuttonid="imgCalender" format="dd/MMM/yyyy" popupposition="BottomLeft">
                        </asp:calendarextender>
            <span id="imgCalender" clientidmode="Static" style="float: left; margin: 12px  0 0 5px;"
                runat="server" class="calIcon" borderwidth="0" alternatetext="Calendar" />
                  <asp:requiredfieldvalidator id="RequiredFieldValidator4" controltovalidate="txtdob" cssclass="validreq" 
            runat="server" errormessage="Please enter D.O.B." validationgroup="regstration" display="Dynamic" />
        <asp:regularexpressionvalidator id="regdate" runat="server" errormessage="Date is not valid format." controltovalidate="txtdob" cssclass="validreq" 
            validationexpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$" display="Dynamic" validationgroup="regstration"/>
        </div>
        <div class="clear">
        </div>
      
        <asp:textbox id="txtremail" runat="server" maxlength="200" class="in-box" />
        <asp:textboxwatermarkextender id="twmskEmail" runat="server" watermarktext="Enter e-Mail Address"
            targetcontrolid="txtremail" />
        <asp:requiredfieldvalidator id="RequiredFieldValidator3" controltovalidate="txtremail" cssclass="validreq"  
            runat="server" errormessage="Please enter email address" validationgroup="regstration" display="Dynamic" />
        <asp:regularexpressionvalidator id="regemail" runat="server" errormessage="Invalid email address" controltovalidate="txtremail" cssclass="validreq" 
            validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" display="Dynamic"/>
        <asp:textbox id="txtpass" runat="server" textmode="Password" maxlength="50" class="in-box" />
        <asp:textboxwatermarkextender id="wtertxtpass" runat="server" watermarktext="Enter Password"
            targetcontrolid="txtpass" />
        <asp:requiredfieldvalidator id="RequiredFieldValidator8" controltovalidate="txtpass" cssclass="validreq" 
            runat="server" errormessage="Please enter password" validationgroup="regstration" />
        <asp:textbox id="txtcnpass" runat="server" textmode="Password" maxlength="50" class="in-box" />
        <asp:textboxwatermarkextender id="wtertxtcnpass" runat="server" watermarktext="Confirm Password"
            targetcontrolid="txtcnpass" />
        <asp:comparevalidator id="cmpvdt" runat="server" errormessage="Password does not matched" controltocompare="txtpass" cssclass="validreq" 
            controltovalidate="txtcnpass" validationgroup="regstration"></asp:comparevalidator>
        <asp:textbox id="txtAddress" runat="server" maxlength="500" textmode="MultiLine"
            class="txtarea-box" />
        <asp:textboxwatermarkextender id="wtertxtAddress" runat="server" watermarktext="Enter Address"
            targetcontrolid="txtAddress" />
        <asp:textbox id="txtphone" runat="server" maxlength="15" class="in-box" />
        <asp:textboxwatermarkextender id="wtertxtphone" runat="server" watermarktext="Enter Telephone No."
            targetcontrolid="txtphone" />
        <asp:regularexpressionvalidator id="regphone" runat="server" errormessage="Please enter valid phone no." controltovalidate="txtphone" Display="Dynamic" cssclass="validreq" 
            validationexpression="\d{10,15}" validationgroup="regstration"/>
        <asp:textbox id="txtcity" runat="server" maxlength="50" class="in-box" />
        <asp:textboxwatermarkextender id="wtertxtcity" runat="server" watermarktext="Enter City Name"
            targetcontrolid="txtcity" />
        <asp:textbox id="txtpost" runat="server" maxlength="15" class="in-box" />
        <asp:textboxwatermarkextender id="wtertxtpost" runat="server" watermarktext="Enter PostCode/ZipCode"
            targetcontrolid="txtpost" />
        <asp:requiredfieldvalidator id="RequiredFieldValidator7" controltovalidate="txtpost"  cssclass="validreq" Display="Dynamic"
            runat="server" errormessage="Please enter postcode" validationgroup="regstration" />
        <asp:regularexpressionvalidator id="regpost" runat="server" errormessage="Please enter valid-postcode" controltovalidate="txtpost" cssclass="validreq" Display="Dynamic"
            validationexpression="\d{6,15}" validationgroup="regstration"/>
        <asp:dropdownlist id="ddlCountry" class="in-box padd5" runat="server" />
               <asp:requiredfieldvalidator id="RequiredFieldValidator5" controltovalidate="ddlCountry"  cssclass="validreq"  Display="Dynamic"
            runat="server" errormessage="Please select country" InitialValue="0" validationgroup="regstration" />         
        <div class="clear">
        </div>
        <div class="full" style="margin-top: 20px;">
            <asp:button text="Cancel" class="w184" id="Button1" runat="server" onclick="btnCancel_Click" />
            &nbsp;<asp:button text="Submit" class="w184" id="btnregsub" runat="server" validationgroup="regstration"
                onclick="btnregsubSubmit_Click" />
            <br />
            <asp:label id="lblerrorReg" clientidmode="Static" runat="server" 
                cssclass="valdreq" ForeColor="Red" />
        </div>
    </div>
</div>
</div>

  
 </div>
<div class="right-content">
 <a href='TrainResults.aspx' id="trainresults">
    <div id="rtPannel1" runat="server">
    </div>
</a>
<img src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />
<a href="GeneralInformation.aspx">
    <uc:GeneralInformation ID="GInfo" runat="server" />
</a>
<img src='images/block-shadow.jpg'  width="272" height="25"  class="scale-with-grid" alt="" border="0" />

</div>

</section>
</asp:Content>
