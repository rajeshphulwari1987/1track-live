﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrivacyPolicy.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="PrivacyPolicy" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            onload();
        });

        function onload() {
            $(".privacy-block-outer").find(".discription-block").hide();
            $("#divCmsCon").find(".discription-block").show();
            $("#divCmsCon").find(".aDwn").show();
            $("#divCmsCon").find(".aRt").hide();

            $("#divCmsCon").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCon").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCon").find(".aDwn").show();
                $("#divCmsCon").find(".aRt").hide();
            });

            $("#divCmsCon").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCon").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCon").find(".aDwn").show();
                $("#divCmsCon").find(".aRt").hide();
            });

            $("#divCmsInfo").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsInfo").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsInfo").find(".aDwn").show();
                $("#divCmsInfo").find(".aRt").hide();
            });

            $("#divCmsInfo").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsInfo").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsInfo").find(".aDwn").show();
                $("#divCmsInfo").find(".aRt").hide();
            });

            $("#divCmsUse").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsUse").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsUse").find(".aDwn").show();
                $("#divCmsUse").find(".aRt").hide();
            });

            $("#divCmsUse").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsUse").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsUse").find(".aDwn").show();
                $("#divCmsUse").find(".aRt").hide();
            });

            $("#divCmsDisc").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsDisc").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsDisc").find(".aDwn").show();
                $("#divCmsDisc").find(".aRt").hide();
            });

            $("#divCmsDisc").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsDisc").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsDisc").find(".aDwn").show();
                $("#divCmsDisc").find(".aRt").hide();
            });

            $("#divCmsPers").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPers").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPers").find(".aDwn").show();
                $("#divCmsPers").find(".aRt").hide();
            });

            $("#divCmsPers").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPers").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPers").find(".aDwn").show();
                $("#divCmsPers").find(".aRt").hide();
            });

            $("#divCmsCk").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCk").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCk").find(".aDwn").show();
                $("#divCmsCk").find(".aRt").hide();
            });

            $("#divCmsCk").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsCk").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsCk").find(".aDwn").show();
                $("#divCmsCk").find(".aRt").hide();
            });

            $("#divCmsSc").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsSc").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsSc").find(".aDwn").show();
                $("#divCmsSc").find(".aRt").hide();
            });

            $("#divCmsSc").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsSc").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsSc").find(".aDwn").show();
                $("#divCmsSc").find(".aRt").hide();
            });

            $("#divCmsPd").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPd").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPd").find(".aDwn").show();
                $("#divCmsPd").find(".aRt").hide();
            });

            $("#divCmsPd").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsPd").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsPd").find(".aDwn").show();
                $("#divCmsPd").find(".aRt").hide();
            });

            $("#divCmsAlt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsAlt").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsAlt").find(".aDwn").show();
                $("#divCmsAlt").find(".aRt").hide();
            });

            $("#divCmsAlt").find(".aRt").click(function () {
                $("#divBlock").find(".discription-block").hide();
                $("#divCmsAlt").find(".discription-block").show();
                $(".privacy-block-outer").find(".aDwn").hide();
                $(".privacy-block-outer").find(".aRt").show();
                $("#divCmsAlt").find(".aDwn").show();
                $("#divCmsAlt").find(".aRt").hide();
            });
        }
    </script>
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
<div class="breadcrumb"> <a href="#">Home </a> >> Privacy Policy </div>
<div class="innner-banner">
    <div class="bannerLft"> <div class="banner90">Privacy Policy</div></div>
    <div>
        <div class="float-lt" style="width: 73%"><asp:Image id="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="901px" height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
        <div class="float-rt" style="width:27%; text-align:right;"><asp:Image id="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server" Width="100%" height="190px" ImageUrl="images/innerMap.gif" />
    </div>
</div>
</div>
<div class="left-content">
<asp:Repeater ID="rptPriv" runat="server">
    <ItemTemplate>
    <h1><%#Eval("Title")%></h1>
    <p><%#Eval("Description")%></p>
    <p>&nbsp; </p>
    <div class="clear"> &nbsp;</div>
    <div id="divBlock">
        <div id="divCmsCon" class="privacy-block-outer">
            <div class="hd-title">
                <strong> 1. Your Consent </strong> 
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("YourConsent"))%></div>
        </div>
        <div id="divCmsInfo" class="privacy-block-outer">
            <div class="hd-title">
                <strong>  2. Information Collected </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("InfoCollected"))%></div>
        </div>
        <div id="divCmsUse" class="privacy-block-outer">
            <div class="hd-title">
            <strong>  3. Use of Your Information </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
        <div class="discription-block">
            <%#HttpContext.Current.Server.HtmlDecode((string)Eval("UseInfo"))%>
        </div>
        </div>
        <div id="divCmsDisc" class="privacy-block-outer">
            <div class="hd-title">
                <strong>  4. Disclosure of Your Information </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("DisclosureInfo"))%></div>
        </div>
        <div id="divCmsPers" class="privacy-block-outer">
            <div class="hd-title">
                <strong>     5. Person under 18 years </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("Person18"))%>
            </div>
        </div>
        <div id="divCmsCk" class="privacy-block-outer">
            <div class="hd-title">
                <strong>    6. Cookies </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("Cookies"))%>
            </div>
        </div>
        <div id="divCmsSc" class="privacy-block-outer">
            <div class="hd-title">
                <strong>    7. Security of your information </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block">
                <%#HttpContext.Current.Server.HtmlDecode((string)Eval("SecurityInfo"))%>
            </div>
        </div>
        <div id="divCmsPd" class="privacy-block-outer">
                <div class="hd-title">
                    <strong>   8. Your Personal Data</strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
                </div>
                <div class="discription-block">
                    <%#HttpContext.Current.Server.HtmlDecode((string)Eval("PersonalData"))%>
                </div>
        </div>
        <div id="divCmsAlt" class="privacy-block-outer">
            <div class="hd-title">
                <strong>  9. Alterations to privacy statement </strong>
            <a class="aDwn"><img src="images/arr-right.png" class="icon scale-with-grid" alt="" border="0" /></a>
            <a class="aRt"><img src="images/arr-down.png" class="icon scale-with-grid" alt="" border="0"/></a>
            </div>
            <div class="discription-block"><%#HttpContext.Current.Server.HtmlDecode((string)Eval("AlterationsPrivacy"))%></div>
        </div>
    </div>
    </ItemTemplate>
</asp:Repeater>
</div>

<div class="right-content">
   <uc:ucRightContent ID="ucRightContent" runat="server" />
</div>
</section>
</asp:Content>
