﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegionalTrainsUc.ascx.cs"
    Inherits="RegionalTrainsUc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:HiddenField ID="hdnDisplaySendJourney" runat="server" Value="false" />
<asp:UpdatePanel ID="upnlTrainSearch" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
        <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
        <asp:HiddenField ID="hdnInbountOrOutBound" runat="server" Value="" />
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <asp:Repeater ID="rptTrainResult" runat="server" OnItemDataBound="rptTrainResult_ItemDataBound">
            <ItemTemplate>
                <div class="booking-detail-in">
                    <table class="grid2" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <asp:Label ID="lblTrainInfo" runat="server" Style="font-size: 13px; font-weight: bold;"
                            Text='<%#Eval("TrainDescr")%>' />
                        <asp:HiddenField ID="hdnTrainNo" runat="server" Value='<%#Eval("TrainNumber")%>' />
                        <tr class="alt">
                            <td colspan="5">
                                <div class="detail-class">
                                    <asp:HiddenField ID="hdnSelectedInfo" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnJourneySummary" runat="server" Value='<%# Eval("DepartureStationName")+","+Eval("DepartureDate")+","+Eval("DepartureTime")+","+Eval("ArrivalStationName")+","+Eval("ArrivalDate")+","+Eval("ArrivalTime") %>' />
                                    <table cellpadding="0" cellspacing="0">
                                        <div id="DivTr" runat="server" style="width: 100%;">
                                        </div>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="detail-class" style="border: none!important; background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;">
            <asp:LinkButton ID="lnkContinue" runat="server" class="lnkCont" Style="margin-right: 5px;">Continue</asp:LinkButton>
            <asp:Button ID="btnPopUpshow" runat="server" Text="Popup" Style="display: none;"
                OnClick="btnPopUpshow_Click" />
        </div>
        <div class="f-left w100">
            <asp:Label ID="lblMsg" runat="server" Visible="False" CssClass="clsMsg"></asp:Label>
        </div>
        <asp:ModalPopupExtender ID="mdpexQuickLoad" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="lnkContinue" />
        <asp:Panel ID="pnlQuckLoad" runat="server" CssClass="modal-popup">
            <div class="popup-inner">
                <div class="title">
                    Please Enter Following Information
                </div>
                <div class="lt">
                    Lead Passenger Name :
                </div>
                <div class="rt">
                    <div style="width: 105px; float: left">
                        <asp:DropDownList ID="ddlTitle" runat="server" class="slbox01 clsInput" Style="width: 100px !important;">
                            <asp:ListItem Value="0">--Title--</asp:ListItem>
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqTitle" ForeColor="Red" runat="server" ErrorMessage="*"
                            ControlToValidate="ddlTitle" InitialValue="0" Display="Dynamic" CssClass="errMsg"
                            ValidationGroup="cntnu" />
                    </div>
                    <div style="width: 145px; float: left">
                        <asp:TextBox ID="txtFirstname" runat="server" MaxLength="50" class="input" Width="130" />
                        <asp:RequiredFieldValidator ID="reqFirstname" ForeColor="Red" runat="server" ErrorMessage="*"
                            ControlToValidate="txtFirstname" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                    </div>
                    <div style="width: 145px; float: left">
                        <asp:TextBox ID="txtLastname" runat="server" MaxLength="50" class="input" Width="130" />
                        <asp:RequiredFieldValidator ID="reqLastname" ForeColor="Red" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLastname" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                    </div>
                </div>
                <div class="lt">
                    Country of Residence :
                </div>
                <div class="rt big-dropdown">
                    <asp:DropDownList ID="ddlCountry" runat="server" class="slbox01 clsInput" Style="width: 200px !important;">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="ddlCountry" InitialValue="0" Display="Dynamic" CssClass="errMsg"
                        ValidationGroup="cntnu" />
                </div>
                <div class="clear">
                </div>
                <p>
                    <strong>Fare Rules</strong>
                    <br />
                    <ul id="ulTI" runat="server" class="fare-rules-list">
                        <li>
                            <div id="divFareRulesTI" runat="server">
                                <span class="tresult">                                  
                                    <p>
                                        <strong>Travel Conditions: </strong>
                                    </p>
                                    
                                    <p>
                                        The standard full fare for all classes of seating. Overnight trains will also offer
                                        an option to upgrade to sleeper carriages.<br>
                                    </p>
                                    <p>
                                        Cancellation requests are subject to a 30% penalty when received 24 hours prior
                                        to the trains departure.<br>
                                    </p>
                                    <p>
                                        The cancellation penalty breakdown is as follows: 20% penalty to Trenitalia, and
                                        10% processing fee to International Rail for handling and processing the cancellation.</p>
                                    <p>
                                        The ticket is non-refundable if cancelled within 24 hours of the trains departure.<br>
                                    </p>
                                    <p>
                                        Changes to your ticket are allowed at a fee of £10.00 per change.<br>
                                    </p>
                                    <p>
                                        Changes are also subject to a price increase if the new fare is more expensive.
                                        There is NO refund if the new fare is less expensive.<br>
                                    </p>
                                    <p>
                                        No changes are allowed within 24hours prior to the trains departure.<br>
                                    </p>
                                    <p>
                                        Once your PNR has been modified (trains departure date or time) then cancellation
                                        of PNR and refunds can not be issued.<br>
                                    </p>
                                    <br>
                                    
                                </span>
                            </div>
                        </li>
                    </ul>
                    <p>
                    </p>
                    <div class="btn-right">
                        <asp:Button ID="btnCloseWin" runat="server" CssClass="btn-black newbutton" Style="width: 40%!important;"
                            Text="Cancel" />
                        <asp:Button ID="btnContinue" runat="server" CssClass="btn-red newbutton" OnClick="btnContinue_Click"
                            Style="width: 100px !important;" Text="Continue" ValidationGroup="cntnu" />
                    </div>
                </p>
            </div>
        </asp:Panel>
        <script type="text/javascript">

            $(document).ready(function () {

                BindJourneySummary();
                $('.priceB').change(function () {
                    var i = $(this).parent().parent().parent().parent().parent().parent().find('input[type="hidden"]:nth-child(1)');
                    i.val($(this).val());
                    BindJourneySummary();
                });

                $("#btnSummarySubmit").click(function () {
                    $('#<%=btnPopUpshow.ClientID%>').click();
                    return false;
                });
            });

            function BindJourneySummary() {

                var CurrID = $.trim($("#MainContent_RegionalTrainsUc1_hdnCurrID").val());
                var SummaryTotal = parseFloat('0.00');
                var arrContentForShow = [];

                $('input[class=priceB]:radio:checked').each(function () {

                    var strJaourneyInfoDetial = $(this).parent().parent().parent().parent().parent().parent().find('input[type="hidden"]:nth-child(2)');
                    var strJaourneyInfoVal = strJaourneyInfoDetial.val();

                    var strTseg = $('input[class=priceB]:checked').val();

                    var strTsegTi = strTseg.split(";");
                    var oFareRules = strTsegTi[10];

                    var strJaourneyInfoValSplit = strJaourneyInfoVal.toString().split(',');
                    var DepTime = $.trim(strJaourneyInfoValSplit[2]);
                    var DepStName = $.trim(strJaourneyInfoValSplit[0]);
                    var ActDateDepart = $.trim(strJaourneyInfoValSplit[1]);
                    var ArrTime = $.trim(strJaourneyInfoValSplit[5]);
                    var ArrStName = $.trim(strJaourneyInfoValSplit[3]);
                    var ActDateArr = $.trim(strJaourneyInfoValSplit[4]);
                    $("#MainContent_RegionalTrainsUc1_ulTI").html(oFareRules);
                    var siteURL = $.trim($("#MainContent_RegionalTrainsUc1_hdnsiteURL").val());
                    var PassengerVal = $("#MainContent_hdnPassenger").val();
                    var InOrOut = $.trim($("#MainContent_RegionalTrainsUc1_hdnInbountOrOutBound").val());

                    var STotalPrice = $(this).parent().find(".bprice");
                    var ValTotal = STotalPrice.text();
                    var SpServiceName = $(this).parent().find(".bprice1");
                    var VServiceName = SpServiceName.text();

                    var arrPassengerVal = PassengerVal.split(',');

                    var StrDisplaySummary = "<div class='hd'> <span>" + (InOrOut != "" && InOrOut == "Inbound" ? " << Inbound" : (InOrOut != "" ? " >> Outbound" : "")) + "</span> <i>&nbsp;</i></div><div class='booking-status'>" +
                                    "<h3>" + DepStName + " to " + ArrStName + "</h3>" +
                                    "<p class='detail'>Departs " + DepTime + " on " + ActDateDepart + "</p>" +
                                    "<p class='detail'>Arrives " + ArrTime + " on " + ActDateDepart + "</p>" +
                                     "<p class='detail'><span>" + VServiceName + "</a></span>" +
                                   (arrPassengerVal[0].indexOf("0") != 0 ? ("<p class='price'> <span> " + arrPassengerVal[0] + " </span></p>") : "") +
                                   (arrPassengerVal[1].indexOf("0") != 0 ? ("<p class='price'> <span> " + arrPassengerVal[1] + " </span></p>") : "") +
                                    (arrPassengerVal[2].indexOf("0") != 0 ? ("<p class='price'> <span> " + arrPassengerVal[2] + " </span></p>") : "") +
                                    (arrPassengerVal[3].indexOf("0") != 0 ? ("<p class='price'> <span> " + arrPassengerVal[3] + " </span></p>") : "") +
                                   "<div class='clear'></div>" +
                                   "<p class='total'> <span>&nbsp; </span>     <strong>" + CurrID + ValTotal + "</strong> </p></div>";
                    arrContentForShow.push(StrDisplaySummary);
                    SummaryTotal = SummaryTotal + parseFloat($.trim(ValTotal));

                });

                var i = 0;
                var strCt = '';
                for (i; i < arrContentForShow.length; i++) {
                    strCt = strCt + arrContentForShow[i];
                }

                strCt = strCt + "<div class='clear'></div><div class='dvtotalSummay'><p class='total'> <span>Total : </span>     <strong>" + CurrID + SummaryTotal.toFixed(2).toString() + "</strong> </p></div>";

                $("#MainContent_lblSDetail").html(strCt);
                if (arrContentForShow.length > 0) {
                    $("#btnSummarySubmit").css("display", "block");
                }

            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upnlTrainSearch"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            Shovelling coal into the server...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
