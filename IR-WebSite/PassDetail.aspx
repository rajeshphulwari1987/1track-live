﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PassDetail.aspx.cs" Inherits="PassDetail" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .input
        {
            text-transform: capitalize;
        }
        .pass .hd-title
        {
            -moz-box-sizing: border-box;
            border-radius: 0 0 5px 5px;
            height: auto !important;
            line-height: 20px;
            width: 100%;
            padding-right: 10%;
        }
        .valid
        {
            float: left;
        }
        .valid3
        {
            color: red;
        }
        .valid2
        {
            color: red;
            position: absolute;
            top: 33px;
            margin-left: 7px;
        }
        .pass-coloum-forth
        {
            width: 200px;
        }
        .pass-coloum-three
        {
            width: 74px;
        }
        .MonthValidity
        {
            display: none;
        }
        #MainContent_rptPassDetail_lblInvalidDate_0
        {
            width: 297px;
            position: absolute;
        }
        .clsdob:before {
            content: "Date of Birth";
            font-size: 14px;
            padding-left: 95px;
            color: #5e5e5e;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        function hide() { $('.discription-block').hide(); }
        function show() { $('.discription-block').show(); }

        $(document).ready(function () {
            show();
            $('.imgOpen').click(function () {
                if ("images/arr-down.png" == $(this).attr('src').toString()) {
                    $(this).attr('src', 'images/arr-right.png');
                }
                else {
                    $(this).attr('src', 'images/arr-down.png');
                }
                $(this).parent().parent().parent().find('.discription-block').slideToggle();
            });
            $(".imgCal").click(function () {
                $("#MainContent_txtStartDate").datepicker('show');
            });

            $(".imgCal1").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked')) {
                    $("#MainContent_txtReturnDate").datepicker('show');
                }
            });
        });

        function chkVal(id, e) {
            if (e == 0)
                e = 3;
            var txtStartDateId = '#' + $(id).find('input[type=text]').attr('id');
            $(txtStartDateId).datepicker({
                numberOfMonths: 2,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+' + e + 'm'
            });
            $("#ui-datepicker-div").removeClass("clsdob");
            $(txtStartDateId).datepicker('show');
        }

        function chkDob(id, e) {
            var currentYear = (new Date).getFullYear();
            var txtDob = '#' + $(id).find('input[type=text]').attr('id');
            $(txtDob).datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1901:' + currentYear,
                onChangeMonthYear: function (year, month, inst) {
                    var selectedDate = $(this).datepicker("getDate"); //Date object
                    var day = selectedDate.getDate()
                    selectedDate.setDate(day); //set first day of the month
                    selectedDate.setMonth(month - 1); //month is 1-12, setMonth is 0-11
                    selectedDate.setFullYear(year);
                    $(this).datepicker("setDate", selectedDate);
                }
            });
            $("#ui-datepicker-div").addClass("clsdob");
            $(txtDob).datepicker('show');
        }
        function showmessagecountry() {
            $(".pass-coloum-three .customSelect").remove();
        }
        function chkdatevalid() {
            $("input[id*=rptPassDetail_txtDob],input[id*=rptPassDetail_txtStartDate]").each(function () {
                var value = $(this).val();
                if (value.length == 11) {
                    try {
                        var dateTxt = $.datepicker.parseDate('dd/M/yy', value);
                        var Todate = new Date(((new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + new Date().getFullYear()));
                        var MonthValidity = $(this).parent().find('.MonthValidity').text();
                        return false;
                        if (MonthValidity != '') {
                            var dateValidity = new Date(((Todate.getMonth() + 1 + parseInt(MonthValidity)) + "/" + Todate.getDate() + "/" + Todate.getFullYear()));
                            if (!(Todate <= dateTxt && dateTxt <= dateValidity)) {
                                $(this).val("DD/MMM/YYYY");
                                $(this).focus().select();
                                return false;
                            }
                        }
                    }
                    catch (e) {
                        $(this).val("DD/MMM/YYYY");
                        $(this).focus().select();
                        return false;
                    }
                }
                else {
                    $(this).val("DD/MMM/YYYY");
                    $(this).focus().select();
                    return false;
                }
            });
        }
        function chkDate(event) {
            var key = event.keyCode;
            var trgid = event.target.id ? event.target.id : event.srcElement.id;
            var id = "#" + trgid;
            var value = $(id).val();
            var strmon = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var m = value.split("/");
            var mon = parseInt(m[1]) - 1;
            if ($.isNumeric(mon))
                $(id).val(m[0] + "/" + strmon[mon] + "/" + m[2]);
        }
        
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="breadcrumb" style="margin-top: 15px;">
        <a href="#">Home </a>>> Pass Detail
    </div>
    <div class="left-content" style="width: 100%">
        <h1>
            Pass Detail
        </h1>
        <p>
            Please enter your details below carefully and ensure they are exactly as per your
            passport.
        </p>
        <p>
            &nbsp;
        </p>
        <div class="clear">
            &nbsp;</div>
        <asp:Repeater ID="rptPassDetail" runat="server" OnItemCommand="rptPassDetail_ItemCommand"
            OnItemDataBound="rptPassDetail_ItemDataBound">
            <ItemTemplate>
                <div class="privacy-block-outer pass">
                    <div class="red-title">
                        Pass&nbsp;<%# Container.ItemIndex + 1 %></div>
                    <div class="hd-title">
                        <asp:HiddenField ID="hdnPassSale" runat="server" Value='<%#Eval("PrdtId")+","+Eval("Price")+","+Eval("TravellerID")+","+Eval("ClassID")+","+Eval("ValidityID")+","+Eval("CategoryID")+","+Eval("commission")+","+Eval("MarkUp")+","+Eval("CountryStartCode")+","+Eval("CountryEndCode")+","+Eval("CountryLevelIDs")%>' />
                        <asp:HiddenField ID="HdnOriginalPrice" runat="server" Value='<%#Eval("OriginalPrice")%>' />
                        <asp:HiddenField ID="hdnPrdtId" runat="server" Value='<%#Eval("PrdtId")%>' />
                        <asp:HiddenField ID="hdnSalePrice" runat="server" Value='<%#Eval("SalePrice")%>' />
                        <asp:HiddenField ID="hdnCountryLevelIDs" runat="server" Value='<%#Eval("CountryLevelIDs")%>' />
                        <asp:Label ID="lblLID" runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                        <%#Eval("PrdtName")%>
                        <asp:Label ID="lblCountryName" runat="server" />,
                        <%#Eval("ValidityName")%>,
                        <%#Eval("TravellerName")%>,
                        <%#Eval("ClassName")%>
                        -
                        <%=currency.ToString()%>
                        <%#Eval("Price")%>
                        <asp:LinkButton ID="lnkDelete" CommandName="Delete" CommandArgument='<%#Eval("Id")%>'
                            runat="server" OnClientClick="return window.confirm('Are you sure? Do you want to delete this item?')"><img src='images/btn-cross.png' class="icon-close scale-with-grid" alt="" border="0" /></asp:LinkButton>
                        <a href="javascript:void(0);">
                            <img src='images/arr-down.png' class="icon scale-with-grid imgOpen" alt="" border="0" /></a>
                    </div>
                    <div class="discription-block row">
                        <div class="pass-coloum-three bigslbox">
                            Title
                            <asp:DropDownList ID="ddlTitle" class="input" runat="server" Style="margin-left: -2px;
                                width: 70px;">
                                <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                                <asp:ListItem>Ms.</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="pass-coloum-forth">
                            First Name<span class="valid3">*</span>
                            <asp:TextBox ID="txtFirstName" runat="server" class="input chkvalid" Text='<%#Eval("FirstName") %>'
                                Width="190px" />
                            <asp:RequiredFieldValidator ID="passdetailreqtxtFirstName" runat="server" ErrorMessage='Please enter first name'
                                CssClass="valid" Text="Please enter First Name" Display="Dynamic" ForeColor="Red"
                                ValidationGroup="vg1" ControlToValidate="txtFirstName" />
                            <br />
                            <asp:FilteredTextBoxExtender ID="ftr1" runat="server" TargetControlID="txtFirstName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-!@#$%^&*()_" />
                        </div>
                        <div class="pass-coloum-forth">
                            Last Name<span class="valid3">*</span>
                            <asp:TextBox ID="txtLastName" runat="server" class="input chkvalid" Text='<%#Eval("LastName") %>'
                                Width="190px" />
                            <asp:RequiredFieldValidator ID="passdetailreqtxtLastName" runat="server" CssClass="valid"
                                Text="Please enter Last Name" ValidationGroup="vg1" Display="Dynamic" ForeColor="Red"
                                ControlToValidate="txtLastName" />
                            <br />
                            <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtLastName"
                                ValidChars=" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-!@#$%^&*()_" />
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="pass-coloum-forth">
                                    Country of residence
                                    <asp:HiddenField ID="hdnproductid" runat="server" Value='<%#Eval("PrdtId") %>' />
                                    <asp:DropDownList ID="ddlCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_chkcounty"
                                        runat="server" class="chkcountry countryinputddlCountry" Width="198px" Style="height: 29px;
                                        border-radius: 5px;" />
                                    <asp:RequiredFieldValidator ID="passdetailreq" runat="server" ValidationGroup="vg1"
                                        CssClass="valid" Text='Please select Country' ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="ddlCountry" InitialValue="0" />
                                    <asp:Label runat="server" ID="errormsg" CssClass="erroronrailpass" ForeColor="red"></asp:Label>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCountry" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div id="Div1" class="pass-coloum-fifth" style="margin-left: -1px; width: 150px;"
                            visible='<%#Eval("PassportIsVisible")%>' runat="server">
                            Passport Number<span class="valid3">*</span>
                            <asp:TextBox ID="txtPassportNumber" Text='<%#Eval("PassportNo") %>' class="input chkvalid"
                                runat="server" Style="margin-left: -3px;" Width="190px" />
                            <asp:RequiredFieldValidator ID="passdetailreqtxtPassportNumber" ValidationGroup="vg1"
                                runat="server" CssClass="valid" Text="Please enter Passport No." Display="Dynamic"
                                ForeColor="Red" ControlToValidate="txtPassportNumber" />
                        </div>
                        <div style="float: left;">
                            <div style="float: left; width: 340px">
                                <div class="pass-coloum-one" style="clear: both!important; width: 25%; padding-left: 0px;">
                                    Date Of Birth<span class="valid3">*</span>
                                </div>
                                <div class="pass-coloum-two" id="Div2" onclick="chkDob(this,0)" style="width: 205px"
                                    onload="chkDob(this,0)">
                                    <asp:TextBox ID="txtDob" MaxLength="11" runat="server" Text='<%# (Eval("DOB") != null ? Eval("DOB") : "DD/MMM/YYYY") %>'
                                        ToolTip="ex: 01/Jan/2014" class="input chkDobValid calDate" Width="160px" onfocusout="chkDate(event)" />
                                    <span class="imgCal calIcon" style="float: left;" title="Select Date of birth.">
                                    </span>
                                    <div style="clear: both;">
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfDob" ValidationGroup="vg1" runat="server" CssClass="valid"
                                        Text="Please enter Date of birth" Display="Dynamic" InitialValue="DD/MMM/YYYY"
                                        ForeColor="Red" ControlToValidate="txtDob" />
                                </div>
                            </div>
                            <div class="pass-startdate">
                                <div class="pass-coloum-one" style="clear: both!important; width: 20%">
                                    Pass Start Date<span class="valid3">*</span>
                                </div>
                                <div class="pass-coloum-two" id="test" onclick="chkVal(this,'<%#Eval("MonthValidity")%>')"
                                    style="width: 42%; padding-left: 12px;">
                                    <asp:TextBox ID="txtStartDate" MaxLength="11" runat="server" Text='<%# (Eval("PassStartDate") != null ? Eval("PassStartDate","{0:dd/MMM/yyyy}") : "DD/MMM/YYYY") %>'
                                        class="input chkvalid calDate" Width="160px" onfocusout="chkDate(event)" />
                                    <span class="imgCal calIcon" style="float: left;" title="Select Pass Start Date.">
                                    </span>
                                    <label class="MonthValidity">
                                        <%#Eval("MonthValidity")%></label>
                                    <div style="clear: both;">
                                    </div>
                                    <asp:RequiredFieldValidator ID="passdetailreqtxtStartDate" ValidationGroup="vg1"
                                        runat="server" CssClass="valid" Text="Please enter Date" Display="Dynamic" InitialValue="DD/MMM/YYYY"
                                        ForeColor="Red" ControlToValidate="txtStartDate" />
                                </div>
                            </div>
                            <div style="clear: both!important; width: 80%">
                                <asp:Label ID="lblInvalidDate" Visible="False" runat="server" ForeColor="red" />
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Button ID="btnChkOut" runat="server" Text="CheckOut" ValidationGroup="vg1" CssClass="btn-red-cart newbutton w184 f-right"
            OnClientClick="chkdatevalid()" OnClick="btnChkOut_Click" />
        <asp:Button ID="btnContinue" runat="server" Text="Continue Shopping" Style="margin-right: 10px;"
            CausesValidation="false" CssClass="btn-red-cart newbutton btn-gray w184 f-right"
            OnClick="btnContinue_Click" />
    </div>
</asp:Content>
