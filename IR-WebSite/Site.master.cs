﻿using System;
using Business;
using System.Web;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class SiteMaster : MasterPage
{
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageSeo _master = new ManageSeo();
    public string AgentName = "Agent Login ";
    db_1TrackEntities db = new db_1TrackEntities();
    Guid pageID, _siteId;
    public string siteURL = "";
    public string NumOfProducts = "";
    public string siteHeaderHtml;
    public string siteFooterHtml;
    public string staStyledata = string.Empty;
    public bool chkBookingCond = false;
    public bool chkConditions = false;
    public bool chkPrivacy = false;
    public string SocialMediaScript = "";
    public string SocialMediaScriptSTA = "";
    public string GoogleAnalyticCode = "";
    public string GoogleManagerTag = "";
    public string MetaKeyTitile = "International Rail";
    public string CurrentPagePath, MetaKeywords, MetaDescription;
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public bool IsHourVisible = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            LoadSite();
            Boolean SiteIsDeleted = _oWebsitePage.GetSiteIsDeleted(_siteId);
            hour1.Visible = IsHourVisible = _oWebsitePage.IsVisibleHourOfSite(_siteId);
            if (SiteIsDeleted)
            {
                Response.Redirect("SiteBolcked.aspx");
            }
            Session["siteId"] = _siteId;
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
            hrefLogo.HRef = siteURL;
            imglogo.ImageUrl = siteURL + "images/logo.png";
            SetCookies();
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            Response.Redirect("InvalidUrl.aspx?q=" + Request.Url.Host);
        }
    }

    private void LoadSite()
    {

        if (Request.Url.Host == "localhost")
        {
            string url = Request.Url.ToString().ToLower();
            if (url.Contains("liveusagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/LiveUsAgent/");
            else if (url.Contains("liveus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/liveus/");
            if (url.Contains("liveausagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/LiveAusAgent/");
            else if (url.Contains("liveaus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/LiveAus/");
            else if (url.Contains("liveuk"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/LiveUk/");
            else if (url.Contains("liveukagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/LiveUkAgent/");
            else if (url.Contains("1track"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1track/");
            else if (url.Contains("bookmyrst"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/bookmyrst/");
        }
        else if (Request.Url.Host.Contains("1tracktest.com"))
        {
            string url = Request.Url.ToString().ToLower();
            if (url.Contains("/usagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/usagent/");
            else if (url.Contains("/us"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/us/");
            else if (url.Contains("/auagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/auagent/");
            else if (url.Contains("/au"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/au/");
            else if (url.Contains("/ukagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/ukagent/");
            else if (url.Contains("/uk"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/uk/");
            else if (url.Contains("/nzagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/nzagent/");
            else if (url.Contains("/nz"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/nz/");
            else if (url.Contains("/idivd"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/IDIVD/");
            else if (url.Contains("/idivdw"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/IDIVW/");
            else if (url.Contains("/idive"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/IDIVE/");
            else if (url.Contains("/rps"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/rps/");
            else if (url.Contains("/rp"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/sta/rp/");
            else
                _siteId = _oWebsitePage.GetSiteIdByURL("http://www.1tracktest.com/");
        }
        else
        {


            siteURL = "http://" + Request.Url.Host + "/";
            _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);

            if (_siteId == new Guid())
            {
                if (Request.Url.Host.ToLower() == "internationalrail.com")
                    siteURL = "https://www." + Request.Url.Host + "/";
                else

                    siteURL = "https://" + Request.Url.Host + "/";
                _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region P2P integration site will not run publicaly
        if (Request.Url.ToString().ToLower().Contains("https://au.internationalrail.net"))
            Response.Redirect("https://au.internationalrail.net/p-to-p.aspx");
        #endregion

        if (Session["AgentUsername"] != null)
            AgentPageLoad();
        else
            AgentName = "Agent Login";

        string url = Request.Url.ToString().ToLower();
        if (siteURL + "home" == url)
        {
            string str = url.Replace("/home", ""); 
            Response.Redirect(str);
        }

        if (!Page.IsPostBack)
        {
            PageNotPostBackAction();
            BindSEOdetail();
        }

        if (Session["siteId"] != null)
            FooterSection();

        SetItemOrderCount();
    }

    public bool MatchASPXurl(string pagename)
    {
        try
        {
            string surl = (System.IO.Path.GetFileName(pagename)).ToLower();
            List<string> ASPXList = new List<string>();
            ASPXList.Add("trainresults.aspx");
            ASPXList.Add("generalinformation.aspx");
            ASPXList.Add("railpasssection1.aspx");
            ASPXList.Add("railpasssection2.aspx");
            ASPXList.Add("railpasssection3.aspx");
            ASPXList.Add("map.aspx");
            ASPXList.Add("feedback.aspx");
            ASPXList.Add("sitemap.aspx");
            ASPXList.Add("security.aspx");
            ASPXList.Add("faq.aspx");
            return ASPXList.Any(t => t.Contains(surl));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void BindSEOdetail()
    {
        var seoType = "page";
        var res = db.tblPages.FirstOrDefault(x => x.SiteID == _siteId && x.PageName.ToUpper().Trim() == "HOME");
        if (res == null)
            return;
        var pageId = (Guid)res.NavigationID;

        string url = Request.Url.ToString();
        if (url.LastIndexOf(".aspx") > -1)
        {
            bool IsAspxPage = MatchASPXurl(url);
            url = url.Substring(0, url.LastIndexOf(".aspx"));
            if (IsAspxPage)
                Response.Redirect(url);
        }
        else
            url = url.Split('?')[0];

        if (Request.Url.ToString().Contains("default"))
            CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", url.Contains("default") ? url.Replace("default", "") : url);
        else if (Request.Url.ToString().Contains("?"))
        {
            string[] urls = Request.Url.ToString().Split('?');
            CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", urls[0]);
        }
        else
            CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", url);

        if (Page.RouteData.Values["PageId"] != null)
        {
            seoType = "page";
            pageId = Guid.Parse(Page.RouteData.Values["PageId"].ToString());
        }
        else if (Page.RouteData.Values["sid"] != null)
        {
            pageId = Guid.Parse(Page.RouteData.Values["sid"].ToString());
            seoType = "specialTrain";
        }
        else if (Page.RouteData.Values["Id"] != null)
        {
            pageId = Guid.Parse(Page.RouteData.Values["Id"].ToString());
            seoType = "country";
        }
        else if (Page.RouteData.Values["cid"] != null)/*category*/
        {
            pageId = Guid.Parse(Page.RouteData.Values["cid"].ToString());
            seoType = "category";
        }
        else if (Page.RouteData.Values["PrdId"] != null)/*product*/
        {
            pageId = Guid.Parse(Page.RouteData.Values["PrdId"].ToString());
            seoType = "product";
        }


        var result = _master.GetSeoDetails(seoType, _siteId, pageId);
        if (result == null && seoType == "page")
        {
            var absolutepage = Path.GetFileName(Request.Url.AbsolutePath);
            tblWebMenu firstOrDefault;
            if (absolutepage != null && absolutepage.Contains("aspx"))
                firstOrDefault = db.tblWebMenus.FirstOrDefault(x => x.PageName.Contains(absolutepage.Replace("-", "")) && x.IsCorporate == false);
            else
                firstOrDefault = db.tblWebMenus.FirstOrDefault(x => x.Name.Contains(absolutepage.Replace(" ", "").Replace("-", " ")) && x.IsCorporate == false);
            if (firstOrDefault != null)
                pageId = firstOrDefault.ID;
            seoType = "other";
            result = _master.GetSeoDetails(seoType, _siteId, pageId);
        }
        if (result == null)
        {
            tblPagesCM pagCm = db.tblPagesCMS.FirstOrDefault(x => x.ID == pageId);
            if (pagCm != null)
            {
                result = new SeoDetail
                {
                    MetaTitle = pagCm.Title,
                    MetaKeyword = pagCm.Keyword
                };
            }
        }
        if (result != null)
        {
            MetaDescription = string.Format("<meta name=\"description\" content=\"{0}\" />", result.MetaDescription);
            MetaKeywords = string.Format("<meta name=\"keywords \" content=\"{0}\" />", result.MetaKeyword);
            MetaKeyTitile = result.MetaTitle;
        }
    }

    private void FooterSection()
    {
        _siteId = Guid.Parse(Session["siteId"].ToString());
        var data = _oWebsitePage.GetSiteDetailsById(_siteId);
        if (data != null)
        {
            ltrSitePhoneNumber.Text = data.PhoneNumber;
            SiteHeadingTitle.InnerText = data.SiteHeadingTitle;
        }

        GetTheme(_siteId);

        chkPrivacy = db.tblPrivacyPolicies.Any(x => x.SiteID == _siteId && x.IsActive == true);
        chkConditions = db.tblConditionsofUses.Any(x => x.SiteID == _siteId && x.IsActive == true);
        chkBookingCond = db.tblBookingConditions.Any(x => x.SiteID == _siteId && x.IsActive == true);


        var footerMenu = _oWebsitePage.GetFooterMenu(_siteId);
        if (chkBookingCond == false)
            footerMenu = footerMenu.Where(x => x.Name != "Booking Conditions").ToList();
        if (chkConditions == false)
        {
            footerMenu = footerMenu.Where(x => x.Name != "Conditions of Use").ToList();
        }
        if (chkPrivacy == false)
        {
            footerMenu = footerMenu.Where(x => x.Name != "PrivacyPolicy").ToList();
        }

        var chkContact = db.tblPages.FirstOrDefault(x => x.SiteID == _siteId && x.PageName.Contains("contact"));
        if (chkContact == null)
        {
            footerMenu = footerMenu.Where(x => x.Name != "Contact Us").ToList();
        }
        var chkAbout = db.tblPages.FirstOrDefault(x => x.SiteID == _siteId && x.PageName.Contains("about"));
        if (chkAbout == null)
        {
            footerMenu = footerMenu.Where(x => x.Name != "About Us").ToList();
        }

        rptFooterMenu.DataSource = footerMenu.ToList();
        rptFooterMenu.DataBind();

        rptFooter.DataSource = _oWebsitePage.GetActiveFooterMeuList(_siteId).OrderBy(x => x.SortOrder);
        rptFooter.DataBind();
    }

    private void PageNotPostBackAction()
    {
        agentalertbox.Visible = false;
        AlertAgent();

        var data = _oWebsitePage.GetSiteDatabySiteId(_siteId);
        if (data != null)
        {
            #region Attention Section
            string AttentionTitle, AttentionMsg, AttentionImg, style;
            style = "style='background: " + (string.IsNullOrEmpty(data.BGColor) ? "#cccccc" : data.BGColor) + "; border: 2px solid " + (string.IsNullOrEmpty(data.BDRColor) ? "#E8E8E8" : data.BDRColor) + " !important';";
            AttentionImg = string.IsNullOrEmpty(data.AttentionImg) ? siteURL + "images/Seasonal.jpg" : AdminSiteUrl + data.AttentionImg;
            AttentionTitle = string.IsNullOrEmpty(data.AttentionTitle) ? string.Empty : (data.AttentionTitle.Replace("\"", "'"));
            AttentionMsg = string.IsNullOrEmpty(data.AttentionMsg) ? string.Empty : (data.AttentionMsg.Replace("\"", "'"));
            string HtmlData = "<div class='seasonal' onmouseover='seasonalshow()'><img  " + style + " class='seasonalimg' src='" + AttentionImg + "' /><div class='inline' " + style + ">" + AttentionTitle + "</div><div class='divs' onmouseover='seasonalshow()'></div><div class='seasonalmessage' " + style + " onmouseover='seasonalshow()'>" + AttentionMsg + "</div></div>";
            string json = JsonConvert.SerializeObject(HtmlData);
            Session.Add("AttentionBody", json);
            #endregion
        }

        ShowAd75Menu();
        ShowP2PWidget(_siteId);

        rptFooter.DataSource = _oWebsitePage.GetActiveFooterMeuList(_siteId).OrderBy(x => x.SortOrder);
        rptFooter.DataBind();

        rptMainMenu.DataSource = _oWebsitePage.GetActiveParentWebNavigationList(_siteId);
        rptMainMenu.DataBind();

        rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
        rptFooterMenu.DataBind();

        rptCountry.DataSource = _oWebsitePage.GetCountryNavigationList(_siteId);
        rptCountry.DataBind();
        rptSpecialTrain.DataSource = _oWebsitePage.GetSpecialTrainList(_siteId);
        rptSpecialTrain.DataBind();
        //--Bind social meadia script
        ManageSocialMediaLink objsocial = new ManageSocialMediaLink();
        SocialMediaScriptSTA = SocialMediaScript = objsocial.GetScriptBySiteID(_siteId);
    }

    private void AgentPageLoad()
    {
        string agentUser = AgentuserInfo.Username;
        var rec = db.tblAdminUsers.FirstOrDefault(x => x.UserName == agentUser);
        string name = rec == null ? "" : rec.Forename;

        if (name == string.Empty)
            name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((Session["AgentUsername"] != null ? Session["AgentUsername"] : "Agent").ToString().ToLower());

        AgentName = name + " Logout";
        lblAgent.Text = "Hello " + name;
        if (rec != null)
        {
            Guid InJob = Guid.Parse("5D3E09AB-AFDB-400D-A7C7-8DBF6F6A5F87");
            printingAgent.Visible = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == rec.RoleID).IsPrintingAllow;
        }
        printingAgent.HRef = siteURL + "Printing/PrintingOrders.aspx";
    }

    void ShowP2PWidget(Guid siteID)
    {
        var enableP2P = _oWebsitePage.EnableP2P(siteID);
        if (!enableP2P)
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);
    }

    protected void ShowAd75Menu()
    {
        var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId);

        if (objsite == null)
            return;

        ltrSiteLogoText.Text = objsite.SiteHeading;
        if ((bool)objsite.IsAgent)
        {
            if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
            {
                var AgentId = AgentuserInfo.UserID;
                aFoclink.Visible = _oWebsitePage.IsFocAD75(AgentId);
                aFoclink.HRef = siteURL + "rail-passes?category=FOC-AD75";
            }
        }

    }

    protected void AlertAgent()
    {
        var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        liTrainTicket.Visible = objsite.IsTrainTickets;
        if (objsite != null)
        {
            GoogleAnalyticCode = Server.HtmlDecode(objsite.GoogleAnalytics);
            GoogleManagerTag = Server.HtmlDecode(objsite.GoogleManager);
            bool IsAgent = (objsite.IsAgent.HasValue ? objsite.IsAgent.Value : false);
            if (IsAgent || objsite.IsWholeSale)
            {
                if (Session["AgentUsername"] == null)
                    if (!Request.Url.LocalPath.Contains("/Agent/SendPassword") && !Request.Url.LocalPath.Contains("/Agent/") && !Request.Url.LocalPath.Contains("/Agent/Login") && !Request.Url.LocalPath.Contains("/Agent/ResetPassword"))
                    {

                        agentalertbox.Visible = true;

                    }
            }
        }
    }

    public void GetTheme(Guid SiteId)
    {
        try
        {


            //SiteId = Guid.Parse("79139CF1-8243-453E-849B-C3F7EB6004D6");
            var blueThemeID = Guid.Parse("4FC1F398-5901-439B-A1E0-27B1CBE2BBEB");
            var theme = db.tblSiteThemes.FirstOrDefault(x => x.SiteID == SiteId);
            if (theme != null)
            {
                if (theme.ThemeID == blueThemeID)
                {


                    /*sta style jquery on page      */
                    //staStyledata = " $('select').customSelect(); $('input:radio').after('<div class=uncheckradiobox />'); $('input:checkbox').after('<div class= uncheckbox />'); $('input:checkbox').each(function () { if ($(this).is(':checked')) { $(this).next().addClass('chkcheckbox'); $(this).parent().find('strong').removeClass('disable-label'); } else { $(this).parent().find('strong').addClass('disable-label'); } }); $('input:radio').each(function () {var prop = $(this).css('display');$(this).next().css('display', prop);$(this).addClass('displaynone'); if ($(this).is(':checked')) { $(this).next().addClass('chkcheckradiobox'); } }); $('.chkcheckbox,.uncheckbox').bind('click', function () { var valdata = $(this).attr('class'); $('.chkcheckbox').each(function () { $(this).removeClass('chkcheckbox').addClass('uncheckbox'); $(this).parent().find('strong').addClass('disable-label'); }); if (valdata == 'uncheckbox') { $(this).removeClass('uncheckbox').addClass('chkcheckbox'); $(this).parent().find('strong').removeClass('disable-label'); } else { $(this).removeClass('chkcheckbox').addClass('uncheckbox'); $(this).parent().find('strong').addClass('disable-label'); } $(this).prev().trigger('click'); }); $('.uncheckradiobox,.chkcheckradiobox').click(function () { var valdata = $(this).attr('class'); $('.chkcheckradiobox').each(function () { $(this).removeClass('chkcheckradiobox').addClass('uncheckradiobox'); }); if (valdata == 'uncheckradiobox') { $(this).removeClass('uncheckradiobox').addClass('chkcheckradiobox'); } $(this).prev().trigger('click'); $('input:radio').each(function () { if ($(this).is(':checked')) { $(this).next().addClass('chkcheckradiobox'); } }); }); $('.progess-inner2').text('UPDATING RESULTS');";
                    staStyledata = "$('.progess-inner2').text('UPDATING RESULTS');";
                    /* end sta style*/
                    lnkStyle.Href = siteURL + "Styles/" + theme.CssFolderName + "/layout.css";
                    lnkStyleBase.Href = siteURL + "Styles/" + theme.CssFolderName + "/base.css";
                    loginimg.ImageUrl = "~/images/btn_please_login_blue.png";
                }
                if (!string.IsNullOrEmpty(theme.CssHeaderFooterName))
                    lnkHeadFtStyle.Href = Request.Url.ToString().Contains("https") ? theme.CssHeaderFooterName.Replace("http://", "https://") : theme.CssHeaderFooterName;

                if (!string.IsNullOrEmpty(theme.FooterHtml))
                    siteFooter.InnerHtml = GetHtml(theme.FooterHtml);
                else
                    SocialMediaScriptSTA = "";

                if (!string.IsNullOrEmpty(theme.HeaderHtml))
                    siteHeader.InnerHtml = GetHtml(theme.HeaderHtml);
                else
                    imglogo.ImageUrl = "images/mainLogo.png";

                if (siteURL.ToLower() == "https://www.bookmyrst.co.uk/")
                {
                    imglogo.ImageUrl = "images/logo.png";
                    imgRailStaff.Visible = true;
                }

            }
            else
            {
                SocialMediaScriptSTA = "";
                loginimg.ImageUrl = "~/images/btn_please_login.png";
            }

        }
        catch (Exception ex)
        {
        }
    }

    public string GetHtml(string url)
    {
        var wc = new WebClient();
        Stream resStream = wc.OpenRead(url);
        var sr = new StreamReader(resStream, System.Text.Encoding.UTF8);
        string siteHtml = sr.ReadToEnd();
        return siteHtml;
    }

    public void SetItemOrderCount()
    {
        if (Session["RailPassData"] == null && AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
            GetOrderinBookingCart();

        string linkURL = siteURL;
        int countpass = 0;
        if (Session["RailPassData"] != null)
        {
            var lstRP = Session["RailPassData"] as List<getRailPassData>;
            countpass = lstRP.Count;
            linkURL = siteURL + "BookingCart";
        }
        ltrlmypasses.Text = "<a href='" + linkURL + "'><img src='" + siteURL + "images/icon-basket.png' border='0' width='27' height='25' /> My Passes (" + countpass + ")</a>";
    }

    public void GetOrderinBookingCart()
    {
        try
        {
            if (Session["RailPassData"] == null && AgentuserInfo.UserID != Guid.Empty && Session["OrderID"] == null)
            {
                var list = new ManageBooking().GetLastAgentOrderInCreateStatus(AgentuserInfo.UserID);
                if (list != null && list.Count() > 0)
                {
                    Session.Add("RailPassData", list);
                    Session["OrderID"] = list.FirstOrDefault().OrderIdentity;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected new string GetRouteUrl(object name)
    {
        if (name.ToString() == "Home")
            return "";

        string urlName = name.ToString();
        return urlName.ToLower().Replace(" ", "-");
    }

    public void SetCookies()
    {
        if (Session["siteId"] == null)
            return;

        var cookie = new HttpCookie("CookieCompliance_IR");
        _siteId = Guid.Parse(Session["siteId"].ToString());
        CookiesCollectionValue ckvalue = _oWebsitePage.GetCookiesValue(_siteId);
        cookie.Values["_siteId"] = _siteId.ToString();
        cookie.Values["_curId"] = ckvalue.CurrencyId.ToString();
        cookie.Values["_cuntryId"] = ckvalue.CountryId.ToString();
        cookie.Values["_langId"] = ckvalue.LangId.ToString();
        Response.Cookies.Add(cookie);
    }

    protected void rptMainMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            var hyp = e.Item.FindControl("HyperLink1") as HyperLink;
            var hdnMenu = e.Item.FindControl("hdnMenu") as HiddenField;
            if (Page.RouteData.Values["PageId"] != null)
            {
                pageID = (Guid)Page.RouteData.Values["PageId"];
                var grpNm = db.tblWebMenus.FirstOrDefault(x => x.ID == pageID).GroupName;
                if (hdnMenu != null && hyp != null)
                    if (hdnMenu.Value == grpNm && !Request.RawUrl.Contains("FOC-AD75"))
                    {
                        hyp.Attributes.Add("class", "active");
                    }
                    else if (hdnMenu.Value == grpNm && Request.RawUrl.Contains("FOC-AD75"))
                    {
                        aFoclink.Attributes.Add("class", "active");
                    }
            }
            else
            {
                if (Request.RawUrl.Contains("RailpassDetail"))
                {
                    var grpNm = "RP";
                    if (hdnMenu != null && hyp != null)
                        if (hdnMenu.Value == grpNm)
                        {
                            var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                            if (objsite != null && objsite.IsAgent == true)
                            {
                                var prdid = Guid.Parse(Request.QueryString["Id"]);
                                var result = _oWebsitePage.ShowIsFoc(prdid);
                                if (result != null)
                                {
                                    if (result)
                                    {
                                        aFoclink.Attributes.Add("class", "active");
                                    }
                                    else
                                        hyp.Attributes.Add("class", "active");
                                }
                                else
                                    hyp.Attributes.Add("class", "active");
                            }
                            else
                                hyp.Attributes.Add("class", "active");
                        }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void rptFooter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

}
