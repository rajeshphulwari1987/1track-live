﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Pages.aspx.cs" Inherits="Pages" %>

<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="TrainSearch" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc" TagName="Newsletter" Src="newsletter.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#rptMainMenu_HyperLink1_1").addClass("active");
        });
    </script>
    <style type="text/css">
        .float-rt
        {
            float: right;
        }
        .clsHeadColor
        {
            color: #931b31;
            font-size: 15px !important;
        }
        .clsAbs
        {
            display: none;
        }
        .clsNews
        {
            width: 300px;
            font-size: 13px;
        }
        .clsfont
        {
            font-size: 11px;
        }
        .newsletter-outer .newsletter-inner input[type="text"]
        {
            height: 25px;
            line-height: 25px;
            width: 92%;
            margin-left: 6px;
        }
        .trNews table td img
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".clsEmail").keyup(function () {
                document.getElementById("divNews").style.display = 'block';
            });
        });
    </script>
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%--Banner section--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="banner" style="height: 253px">
        <div class="slider-wrapper theme-default" style="height: 253px">
            <div id="slider" class="nivoSlider" style="height: 253px">
                <asp:Repeater ID="rptBanner" runat="server">
                    <ItemTemplate>
                        <img src='<%=adminSiteUrl%><%#Eval("ImgUrl")%>' class="scale-with-grid" alt="" border="0"
                            style="height: 253px" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <%--Banner section end--%>
    <section class="content">
      <div class="left-content">
      <h1 runat="server" id="headerTitle"></h1>
      <div style="float:right;width:236px;padding-left:10px; " id="dvContent"  runat="server" visible="false">
         <img runat="server" id="imgGinfo" />
      </div>
      <div runat="server" id="divDescription" style="padding:3px;font-size:13px;">
      </div>
      </div>
      <div class="right-content">
          <div class="ticketbooking" style="padding-top:0px" id="Widget" runat="server">
            <div class="list-tab divEnableP2P">
                <ul>
                    <li><a href="#" class="active">Rail Tickets </a><li>
                    <li><a href="rail-passes">Rail Passes </a></li>
                </ul>
            </div>
            <uc1:TrainSearch ID="ucTrainSearch" runat="server" />
            <img id="imgShadow" runat="server" src='images/block-shadow.jpg'  width="272" height="25"  alt="" class="scale-with-grid" border="0" />
        </div>
         <uc:Newsletter ID="Newsletter1" runat="server" />
      </div>
   </section>
</asp:Content>
