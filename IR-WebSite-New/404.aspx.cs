﻿using System;
using System.Net.Mail;
using System.Net;
using System.Web.Routing;
public partial class _404 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["URL"] != null)
        {
            InvalidURL.Style.Add("display", "none");
            errorTicktURL.Style.Add("display", "block");
        }
        else
        {
            try
            {
                if (!Request.UrlReferrer.ToString().Contains("/Scripts/") || !Request.UrlReferrer.ToString().Contains("/assets/") || !Request.UrlReferrer.ToString().Contains("/images/") || !Request.UrlReferrer.ToString().Contains("/StationList.asmx"))
                {

                    Business.ManageRoutes objroute = new Business.ManageRoutes();
                    objroute.UpdateRouteCount(Business.Route.CategoryRoute, 0);
                    objroute.UpdateRouteCount(Business.Route.CountryRoute, 0);
                    objroute.UpdateRouteCount(Business.Route.ProductRoute, 0);
                    objroute.UpdateRouteCount(Business.Route.SpecialTrainRoute, 0);
                    RegisterRoutes(RouteTable.Routes);
                    objroute.DoneRouteProductUpdateUrl();
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
            }
            catch
            {

                InvalidURL.Style.Add("display", "block");
                errorTicktURL.Style.Add("display", "none");
            }
        }
        try
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            var fromAddres = new MailAddress("admin@InternationalRail.com", "admin@InternationalRail.com");
            smtpClient.Host = "mail.dotsquares.com";
            smtpClient.Port = 587;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
            smtpClient.EnableSsl = false;
            message.From = fromAddres;
            message.To.Add("rahul.verma@dotsquares.com");
            message.Bcc.Add("dipu.bharti@dotsquares.com");
            message.Subject = "404 Error";
            message.IsBodyHtml = true;
            message.Body = "<h3>URL</h3><br/><p>" + Request.UrlReferrer.ToString() + "</p>";
            smtpClient.Send(message);
        }
        catch (Exception ex)
        {
        }
        SetStatus404();
    }
    void SetStatus404()
    {
        Response.ClearHeaders();
        Response.ClearContent();
        Response.TrySkipIisCustomErrors = true;
        Response.StatusCode = 404;
        Response.Flush();
    }
    void RegisterRoutes(RouteCollection routes)
    {
        PageUrls.MasterSiteUrl(routes);
        PageUrls.DefaultSiteUrl(routes);
        PageUrls.RailPassCatUrl(routes);
        PageUrls.RailPassDetailUrl(routes);
        PageUrls.CountryDetailUrl(routes);
        PageUrls.SpecialTrainUrl(routes);
        PageUrls.PagesUrl(routes);
    }

}