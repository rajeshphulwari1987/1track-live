﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web.UI.HtmlControls;
using System.Web.Routing;
using System.Web;
using System.Collections.Generic;

public partial class CountryDetail : System.Web.UI.Page
{
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly Masters _masterPage = new Masters();
    static string _countryId = string.Empty;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private Guid _siteId;
    public string script, CountryImage;
    public static string currency;
    private readonly ManageInterRailNew _ManageIRNew = new ManageInterRailNew();
    private ManageScriptingTag _ManageScriptingTag = new ManageScriptingTag();
    public string IRScriptingTagJs, siteURL = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);

            if (Page.RouteData.Values["Id"] != null)
            {
                Guid Id = (Guid)Page.RouteData.Values["Id"];
                ViewState["Id"] = Id;

                #region Seobreadcrumbs
                var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(Id, _siteId);
                if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                    litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
                #endregion
            }

            if (Request.QueryString["Id"] != null)
            {
                ViewState["Id"] = Request.QueryString["Id"].ToString();
                string RD_URl = PageUrls.GetRedirectURL(Convert.ToString(ViewState["Id"]));
                Response.Redirect(RD_URl);
            }

            if (ViewState["Id"] != null)
            {
                _countryId = ViewState["Id"].ToString();
                BindCountryName(_countryId);
                GetCurrency();
                GetScriptingTag();
            }
        }
    }

    public void BindCountryName(string cuId)
    {
        try
        {
            var cId = Guid.Parse(cuId);
            var cuName = _cMaster.GetCountryNameById(cId);
            lblcn.Value = cuName.CountryName;
            if (!string.IsNullOrEmpty(cuName.CountryName) && cuName.CountryName.ToLower().Trim() == "japan" || cuName.CountryName.ToLower().Trim() == "south korea")
            {
                lblCntyNm.InnerHtml = imgBanner.AlternateText = cuName.CountryName + " Train Tickets";
                imgBanner.ToolTip = cuName.CountryName + " Train Tickets";
                imgCountry.AlternateText = "Book " + cuName.CountryName + " Train Tickets Online";
                imgCountry.ToolTip = "Train Tickets in " + cuName.CountryName;
            }
            else
            {
                lblCntyNm.InnerHtml = imgBanner.AlternateText = cuName.CountryName + " Rail Passes";
                imgBanner.ToolTip = cuName.CountryName + " Train Tickets";
                imgCountry.AlternateText = "Book " + cuName.CountryName + " Rail Passes Online";
                imgCountry.ToolTip = "Train Tickets in " + cuName.CountryName;
                if (cuName.CountryName.ToLower().Trim() == "united states")
                {
                    lblCntyNm.InnerHtml = "Train Tickets USA";
                    imgBanner.AlternateText = "Train Tickets USA, Rail Passes USA";
                    imgBanner.ToolTip = "Train Booking USA";
                    imgCountry.AlternateText = "Rail Tickets USA, Train Passes USA";
                    imgCountry.ToolTip = "USA Rail Passes";
                }
                if (cuName.CountryName.ToLower().Trim() == "united kingdom")
                {
                    lblCntyNm.InnerHtml = "Rail Passes UK";
                    imgBanner.AlternateText = "Train Tickets UK, Rail Passes UK";
                    imgBanner.ToolTip = "Train Booking UK";
                    imgCountry.AlternateText = "Rail Tickets UK, Train Passes UK";
                    imgCountry.ToolTip = "UK Rail Passes";
                }
            }

            imgBanner.ImageUrl = !string.IsNullOrEmpty(cuName.BannerImg)
                                     ? SiteUrl + cuName.BannerImg
                                     : "images/img_inner-banner.jpg";
            imgCountry.ImageUrl = !string.IsNullOrEmpty(cuName.CountryImg)
                                      ? SiteUrl + cuName.CountryImg
                                      : "images/innerMap.gif";

            var cuData = _db.tblCountriesInfoes.FirstOrDefault(x => x.CountryID == cId);
            if (cuData != null) p_countryInfo.InnerHtml = Server.UrlDecode(cuData.CountryInfo);

            h_heading.InnerHtml = "Train Tickets in " + cuName.CountryName;
            if (_siteId == Guid.Parse("5858f611-6726-4227-a417-52ccdea36b3d") && cuData != null)
            {
                p_countryInfo.InnerHtml = !string.IsNullOrEmpty(Server.HtmlDecode(cuData.IrCountryInfo)) ? Server.HtmlDecode(cuData.IrCountryInfo) : Server.HtmlDecode(cuData.CountryInfo);
                h_heading.InnerHtml = string.IsNullOrEmpty(cuData.Title) ? "Train Tickets in " + cuName.CountryName : cuData.Title;
            }

            BindProductByCountry(cuName.CountryName);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCurrency()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (result != null)
        {
            var currId = result.DefaultCurrencyID;
            if (currId != null)
                currency = oManageClass.GetCurrency(Guid.Parse(currId.ToString()));
            else
                currency = "£";
        }
        else
            currency = "£";
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public void BindProductByCountry(string CountryName)
    {
        try
        {
            var list = new List<ProductPass>();
            if (!string.IsNullOrEmpty(CountryName))
            {
                Session["GaTaggingProductlist"] = list = _ManageIRNew.GetAllProductPassNew(_siteId, CountryName,1).ToList();
                rptProduct.DataSource = list;
                rptProduct.DataBind();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetScriptingTag()
    {
        try
        {
            IRScriptingTagJs = "";
            var data = _ManageScriptingTag.GetListScriptingTagBySiteId(_siteId);
            if (data != null && data.Where(x => x.IsActive == true).ToList().Count > 0)
            {
                if (_siteId == Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D"))
                {
                    var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Pass Inner");
                    if (firstDefault != null)
                    {
                        IRScriptingTagJs = firstDefault.Script;
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##SiteUrl##", siteURL).Replace("##PageUrl1##", siteURL + "countries").Replace("##PageName1##", "Countries");
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##CurrentPageName##", lblcn.Value);
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }
}