﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Security.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="Security" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
 <br />
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top:5px">
        <h2>
            <asp:Label ID="lblTitle" runat="server" /></h2>
        <div class="starail-Section starail-Section--nopadding">
            <p>
                <asp:Label ID="lblDesp" runat="server" />
                <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Description"))%></p>
        </div>
    </div>
</asp:Content>
