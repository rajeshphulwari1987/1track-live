﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TrainResults.aspx.cs" Inherits="TrainResults" Culture="en-GB" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControls/BeNeTrainSearchResult.ascx" TagName="BeNeTrainSearchResult"
    TagPrefix="uc2" %>
<%@ Register Src="UserControls/TiTrainSearchResult.ascx" TagName="TiTrainSearchResult"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/EvolviTrainSearchResult.ascx" TagName="EvolviTrainSearchResult"
    TagPrefix="uc4" %>
<asp:Content ID="HeaderContenzt" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .chkwidth {
            width: 1%;
        }

        .p-full-text {
            text-align: justify;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
        var count = 0;
        var countKey = 1;
        var conditionone = 0;
        var conditiontwo = 0;
        var tabkey = 0;

        $(document).ready(function () {
            $("#MainContent_txtTrainReturnDate").val("DD/MM/YYYY");

            if ($("#txtFrom").val() != '') {
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            LoadCal1();
        });
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function selectpopup(e) {
            if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                var $this = $(e);
                var data = $this.val();
                var station = '';
                var hostName = window.location.host;
                var url = "http://" + hostName;

                if (window.location.toString().match(/https:/i))
                    url = "https://" + hostName;


                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }
                var filter = $("#span" + $this.attr('id') + "").text();
                if (filter == "" && $this.val() != "")
                    filter = $("#hdnFilter").val();
                $("#hdnFilter").val(filter);


                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }

                if (hostName == "localhost") {
                    url = url + "/1tracknew";
                }
                var hostUrl = url + "/StationList.asmx/getStationsXList";
                data = data.replace(/[']/g, "♥");
                var $div = $("<div id='_bindDivDataTrainResult' onmousemove='_removehoverclass(this)'/>");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {
                        $('#_bindDivDataTrainResult').remove();
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length;;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);
                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                        });
                        $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                    },
                    error: function () {
                        //                    alert("Wait...");
                    }
                });
            }
        }
        function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }

        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivDataTrainResult').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
            if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
            $('#_bindDivDataTrainResult').remove();
        }
        function _hideThisDiv(e) {
            $('#_bindDivDataTrainResult').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }

        function CheckCardVal(sender, args) {
            if (document.getElementById('MainContent_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }

        function LoadCal1() {

            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+2y',
            });
            $("#MainContent_txtTrainReturnDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+2y'
            });

            $(".imgCalSendBox").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });

            $(".imgCalSendBox1").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked')) {
                    $("#MainContent_txtTrainReturnDate").datepicker('show');
                }
            });

            if ($('#MainContent_ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('enable');
            }
            else {
                $("#txtReturnDate").datepicker('disable');
            }
            $("#MainContent_txtDepartureDate, #MainContent_txtTrainReturnDate").keypress(function (event) { event.preventDefault(); });
        }

        function calenableT() {
            LoadCal1();
            $("#MainContent_txtTrainReturnDate").datepicker('enable').val($("#MainContent_txtDepartureDate").val());
        }
        function caldisableT() {
            LoadCal1();
            $("#MainContent_txtTrainReturnDate").datepicker('disable').val("DD/MM/YYYY");
        }
    </script>
    <%=script%>
    <script type="text/javascript">
        $(document).ready(function () {
            outbondCollapsible();
        });
        function outbondCollapsible() {
            $('.journey-type').each(function (pkey, pvalue) {
                $(pvalue).find('.starail-JourneyBlock').each(function (key, value) {
                    if ($(value).find('[id*=lblCheapPrice]').text() == '') {
                        $(value).find('.from-price').attr('style', 'display:none;');
                    }
                    if (key == "0") {
                        $(value).find('.top-arrow').attr('style', 'display:block;');
                        $(value).find('.bottom-arrow').attr('style', 'display:none;');
                        $(value).find('.collapsiblediv').attr('style', 'display:block;');
                    }

                    $(value).find('.pricecheap').click(function () {
                        $(value).find('.top-arrow').attr('style', 'display:block;');
                        $(value).find('.bottom-arrow').attr('style', 'display:none;');
                        $(value).find('.collapsiblediv').attr('style', 'display:block;');
                    });
                    $(value).find('.bottom-arrow').click(function () {
                        $(value).find('.top-arrow').attr('style', 'display:block;');
                        $(value).find('.bottom-arrow').attr('style', 'display:none;');
                        $(value).find('.collapsiblediv').attr('style', 'display:block;');
                    });
                    $(value).find('.top-arrow').click(function () {
                        $(value).find('.top-arrow').attr('style', 'display:none;');
                        $(value).find('.bottom-arrow').attr('style', 'display:block;');
                        $(value).find('.collapsiblediv').attr('style', 'display:none;');
                    });
                });
            });
        }
    </script>
    <%=omegaCss %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server" />
    <asp:UpdatePanel ID="updLoyalty" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnIsStaSite" runat="server" Value="False" />
            <div id="DivLeftOne" runat="server">
                <asp:Panel ID="pnlJourneyInfo" Visible="true" runat="server">
                    <div class="starail-BookingDetails-form">
                        <div class="starail-Form-row">
                            <h2>
                                <asp:Label ID="lblHeading" runat="server" Text="Train Search Result" /></h2>
                        </div>
                        <div class="starail-Form-row">
                            <%if (!string.IsNullOrEmpty(Request.QueryString["ct"]))
                              {%>
                            <div class="starail-Alert starail-Alert--info">
                                Rail tickets/pass for this country are not bookable online please send us the details
                                of your journey and we will come back to you with your options
                            </div>
                            <%}
                              else
                              { %>
                            <div class="starail-Alert starail-Alert--info" id="DivElse" runat="server">
                                If your booking is not available online please send us the details of your journey
                                and we will come back to you with your options.
                            </div>
                            <% } %>
                        </div>
                        <div class="starail-Form-row">
                            <p id="pCls" runat="server">
                                <asp:Label ID="lblMessageEarlierTrain" runat="server" Text="" Visible="false" />
                            </p>
                            <p id="errorMsg" runat="server" class="starail-Alert starail-Alert--error">
                                I'm sorry we don't appear to be able to do that journey online at the moment, the
                                chances are we're able to do it offline though. Please complete the form below and
                                we'll get back to you with a quote.
                            </p>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-name">
                                <asp:Label runat="server" ID="lblNm" Text="User Name" />
                                <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtName" runat="server" class="starail-Form-input" autocomplete="off" />
                                    <asp:RequiredFieldValidator ID="reqvalerror0" runat="server" ControlToValidate="txtName"
                                        ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-passportno">
                                Email <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtEmailAddress" runat="server" class="starail-Form-input" autocomplete="off" />
                                    <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ControlToValidate="txtEmailAddress"
                                        ValidationGroup="vgsTR" Display="Dynamic" />
                                    <asp:RegularExpressionValidator ID="reqcustvalerror3" runat="server" ErrorMessage="Please enter a valid email."
                                        CssClass="starail-Form-required" ControlToValidate="txtEmailAddress" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Display="Dynamic" ValidationGroup="vgsTR" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-passportno">
                                Telephone <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtPhone" runat="server" class="starail-Form-input" autocomplete="off"
                                        MaxLength="15" onkeypress="return isNumberKey(event)" />
                                    <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ControlToValidate="txtPhone"
                                        ValidationGroup="vgsTR" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-email">
                                From <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer  train-result-journyFromTo">
                                <div class="starail-Form-inputContainer-col">
                                    <%--Bug ID:project/5/issue/514--%>
                                    <%--<asp:TextBox ID="txtFrom" onkeyup="selectpopup(this)" runat="server" class="starail-Form-input" autocomplete="off" ClientIDMode="Static" />
                                    <span id="spantxtFrom" style="display:none"></span>
                                    <asp:HiddenField ID="hdnFrm" runat="server" />--%>
                                    <asp:TextBox ID="txtFrom" runat="server" class="starail-Form-input" autocomplete="off"
                                        MaxLength="50" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="reqvalerror4" runat="server" ValidationGroup="vgsTR"
                                        Display="Dynamic" ControlToValidate="txtFrom" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-email">
                                To<span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer train-result-journyFromTo">
                                <div class="starail-Form-inputContainer-col">
                                    <%--      <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" class="starail-Form-input" autocomplete="off" ClientIDMode="Static"/>
                                    <span id="spantxtTo" style="display:none"></span>--%>
                                    <asp:TextBox ID="txtTo" runat="server" class="starail-Form-input" autocomplete="off"
                                        MaxLength="50" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="reqvalerror5" runat="server" ValidationGroup="vgsTR"
                                        Display="Dynamic" ControlToValidate="txtTo" CssClass="font14" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-email">
                                Journey</label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-u-cf starail-Form-switchRadioGroup train-result-journy">
                                    <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vg" RepeatDirection="Horizontal"
                                        CssClass="switchRadioGroup" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                                        <asp:ListItem Value="1">Return</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-email">
                                Leaving<span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <div class="starail-Form-datePicker">
                                    <asp:TextBox ID="txtDepartureDate" runat="server" class="starail-Form-input" placeholder="DD/MM/YYYY"
                                        autocomplete="off" />
                                    <asp:RequiredFieldValidator ID="reqvalerror6" runat="server" ValidationGroup="vgs"
                                        Display="Dynamic" ControlToValidate="txtDepartureDate" />
                                    <i class="starail-Icon-datepicker"></i>
                                </div>
                                <asp:DropDownList ID="ddldepTime" runat="server" CssClass="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                                    <asp:ListItem>00:00</asp:ListItem>
                                    <asp:ListItem>01:00</asp:ListItem>
                                    <asp:ListItem>02:00</asp:ListItem>
                                    <asp:ListItem>03:00</asp:ListItem>
                                    <asp:ListItem>04:00</asp:ListItem>
                                    <asp:ListItem>05:00</asp:ListItem>
                                    <asp:ListItem>06:00</asp:ListItem>
                                    <asp:ListItem>07:00</asp:ListItem>
                                    <asp:ListItem>08:00</asp:ListItem>
                                    <asp:ListItem Selected="True">09:00</asp:ListItem>
                                    <asp:ListItem>10:00</asp:ListItem>
                                    <asp:ListItem>11:00</asp:ListItem>
                                    <asp:ListItem>12:00</asp:ListItem>
                                    <asp:ListItem>13:00</asp:ListItem>
                                    <asp:ListItem>14:00</asp:ListItem>
                                    <asp:ListItem>15:00</asp:ListItem>
                                    <asp:ListItem>16:00</asp:ListItem>
                                    <asp:ListItem>17:00</asp:ListItem>
                                    <asp:ListItem>18:00</asp:ListItem>
                                    <asp:ListItem>19:00</asp:ListItem>
                                    <asp:ListItem>20:00</asp:ListItem>
                                    <asp:ListItem>21:00</asp:ListItem>
                                    <asp:ListItem>22:00</asp:ListItem>
                                    <asp:ListItem>23:00</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label for="returning" class="starail-Form-label">
                                Returning<span id="returnspan" runat="server" visible="false" class="starail-Form-required">*</span>
                            </label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <div class="starail-Form-datePicker loyreturntxt">
                                    <asp:TextBox ID="txtTrainReturnDate" runat="server" class="starail-Form-input" placeholder="DD/MM/YYYY"
                                        autocomplete="off" Enabled="False" />
                                    <asp:RequiredFieldValidator ID="rfReturnDate" runat="server" ValidationGroup="vgs"
                                        Enabled="false" Display="Dynamic" ControlToValidate="txtTrainReturnDate" />
                                    <i class="starail-Icon-datepicker"></i>
                                </div>
                                <asp:DropDownList ID="ddlReturnTime" runat="server" CssClass="loyreturnselect" Enabled="false">
                                    <asp:ListItem>00:00</asp:ListItem>
                                    <asp:ListItem>01:00</asp:ListItem>
                                    <asp:ListItem>02:00</asp:ListItem>
                                    <asp:ListItem>03:00</asp:ListItem>
                                    <asp:ListItem>04:00</asp:ListItem>
                                    <asp:ListItem>05:00</asp:ListItem>
                                    <asp:ListItem>06:00</asp:ListItem>
                                    <asp:ListItem>07:00</asp:ListItem>
                                    <asp:ListItem>08:00</asp:ListItem>
                                    <asp:ListItem Selected="True">09:00</asp:ListItem>
                                    <asp:ListItem>10:00</asp:ListItem>
                                    <asp:ListItem>11:00</asp:ListItem>
                                    <asp:ListItem>12:00</asp:ListItem>
                                    <asp:ListItem>13:00</asp:ListItem>
                                    <asp:ListItem>14:00</asp:ListItem>
                                    <asp:ListItem>15:00</asp:ListItem>
                                    <asp:ListItem>16:00</asp:ListItem>
                                    <asp:ListItem>17:00</asp:ListItem>
                                    <asp:ListItem>18:00</asp:ListItem>
                                    <asp:ListItem>19:00</asp:ListItem>
                                    <asp:ListItem>20:00</asp:ListItem>
                                    <asp:ListItem>21:00</asp:ListItem>
                                    <asp:ListItem>22:00</asp:ListItem>
                                    <asp:ListItem>23:00</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                            <label for="" class="starail-Form-label">
                                Who's going?</label>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                                <div>
                                </div>
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList ID="ddlAdult" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                    </asp:DropDownList>
                                    <label for="starail-adult">
                                        Adults (26-65 at time of travel)</label>
                                </div>
                                <div class="starail-Form-inputContainer-col train-result-col2">
                                    <asp:DropDownList ID="ddlChild" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                    </asp:DropDownList>
                                    <label for="starail-children">
                                        Children (under 17 at time of travel)</label>
                                </div>
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList ID="ddlYouth" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                    </asp:DropDownList>
                                    <label for="starail-adult">
                                        Youths (17-25 at time of travel)</label>
                                </div>
                                <div class="starail-Form-inputContainer-col train-result-col2">
                                    <asp:DropDownList ID="ddlSenior" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                    </asp:DropDownList>
                                    <label for="starail-adult">
                                        Seniors (over 66 at time of travel)</label>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-country">
                                Class Preference
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList ID="ddlClass" runat="server" class="starail-Form-select">
                                        <asp:ListItem Value="0">All</asp:ListItem>
                                        <asp:ListItem Value="1">1st</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="2">2nd</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-country">
                                Max Transfers
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:DropDownList ID="ddlTransfer" runat="server" class="starail-Form-select">
                                        <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                                        <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                                        <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                                        <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                            <label class="starail-Form-label" for="starail-country">
                                Journey Type
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <label class="starail-BookingDetails-form-fancyCheckbox">
                                        <span class="starail-Form-fancyCheckbox">
                                            <input type="checkbox" name="starail-protect" id="chkLoyalty" runat="server">
                                            Loyalty cards <span><i class="starail-Icon-tick"></i></span></span>&nbsp;&nbsp;&nbsp;</label>
                                </div>
                            </div>
                            <div class="starail-Form-inputContainer" id="divRailPass" runat="server" visible="False">
                                <div class="starail-Form-inputContainer-col">
                                    <label class="starail-BookingDetails-form-fancyCheckbox">
                                        <span class="starail-Form-fancyCheckbox">
                                            <input type="checkbox" name="starail-protect" id="chkIhaveRailPass" runat="server">
                                            I have a rail pass <span><i class="starail-Icon-tick"></i></span></span>&nbsp;&nbsp;&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-country">
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:Button ID="btnSendInfo" runat="server" Text="Send Info" ValidationGroup="vgsTR"
                                        OnClick="btnSendInfo_Click" CssClass="starail-Button starail-Form-button starail-Form-button--primary" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <div class="starail-Form-inputContainer p-desc-mad" id="div_Mandatory" runat="server" visible="false">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                                    <asp:CheckBox ID="chkMandatory" runat="server" />
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                                    <span class="starail-Form-required">*</span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                    <p class="p-full-text" id="p_MandatoryText" runat="server"></p>
                                </div>
                            </div>
                            <div class="starail-Form-inputContainer p-desc-not-mad" id="div_NotMandatory" runat="server" visible="false">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                                    <asp:CheckBox ID="chkNotMandatory" runat="server" />
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                    <p class="p-full-text" id="p_NotMandatoryText" runat="server"></p>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <asp:Label runat="server" ID="lblmsg" />
                        </div>
                        <ul class="starail-Checklist">
                            <li><span class="starail-Form-required">*</span>Please complete all mandatory fields.</li>
                        </ul>
                    </div>
                </asp:Panel>
                <uc2:BeNeTrainSearchResult runat="server" ID="ucSResult" Visible="false" />
                <uc1:TiTrainSearchResult runat="server" ID="ucSResultTi" Visible="false" />
                <uc4:EvolviTrainSearchResult runat="server" ID="ucSResultEvolvi" Visible="false" />
            </div>
            <div class="starail-BookingDetails-form" id="DivLeftSecond" runat="server" style="display: none;">
                <div class="starail-Form-row">
                    <h2>Journey Request Form</h2>
                </div>
                <div class="starail-Alert starail-Alert--success">
                    <p>
                        <asp:Label runat="server" ID="succmessage" />
                    </p>
                </div>
            </div>
            <div class="country-block-outer" style="display: none!important;">
                <div class="country-block-inner">
                    <div id="footerBlock" runat="server">
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlAdult" />
            <asp:AsyncPostBackTrigger ControlID="ddlChild" />
            <asp:AsyncPostBackTrigger ControlID="ddlYouth" />
            <asp:AsyncPostBackTrigger ControlID="ddlSenior" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updLoyalty"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script type="text/javascript">
        $(document).ready(function () {
            showhideterms();
        });

        function showhideterms() {
            if ('<%=IsStaSite %>' == 'False') {
                var mandatoryText = '<%=MandatoryTextFirst %>';
                if (mandatoryText.length > 130) {
                    $('[id*=p_MandatoryText]').text(mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...');
                    mandatoryText = mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...';
                }
                var notMandatoryText = '<%=MandatoryTextFirstSecond %>';
            if (notMandatoryText.length > 130) {
                $('[id*=p_NotMandatoryText]').text(notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...');
                notMandatoryText = notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...';
            }

            $('.p-desc-mad').mouseover(function () {
                $(this).find('.p-full-text').text('<%=MandatoryTextFirst %>');
            }).mouseout(function () {

                $(this).find('.p-full-text').text(mandatoryText);
            });
            ;
            $('.p-desc-not-mad').mouseover(function () {
                $(this).find('.p-full-text').text('<%=MandatoryTextFirstSecond %>');
            }).mouseout(function () {
                $(this).find('.p-full-text').text(notMandatoryText);
            });
        }
    }

    function checkMandatory() {
        alert('Please accept the privacy statement, by clicking the tick box at the bottom of the page.');
    }
    </script>
</asp:Content>
