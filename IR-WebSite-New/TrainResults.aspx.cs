﻿#region Using
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Business;
using OneHubServiceRef;
#endregion

public partial class TrainResults : Page
{
    private readonly Masters _master = new Masters();
    ManageUser _ManageUser = new ManageUser();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static string unavailableDates1 = "";
    private Guid _siteId;
    BookingRequestUserControl objBRUC;
    public string script = "<script></script>";
    public string siteURL;
    private string htmfile = string.Empty;
    public string omegaCss = string.Empty;
    public Guid IrAgentSiteID = Guid.Parse("57ab589f-8549-46ca-82e8-4d835c2ff0ac"); //Guid.Parse("98bb7831-068a-4963-9eb8-d7ccab265b4e");
    public bool IsStaSite = false;
    public string MandatoryTextFirst = "", MandatoryTextFirstSecond = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        Session["TIRegionalBooking"] = null;
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());

        if (Session["siteId"] != null)
            if (_db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 8))
                omegaCss = "<style type='text/css'>.switchRadioGroup tr td input[type='radio']:checked + label{background-color: #29166f!important;}</style>";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _siteId = Guid.Parse(Session["siteId"].ToString());
        siteURL = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        ShowHaveRailPass(_siteId);
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "LoadCal1", "LoadCal1();", true);
        if (!Page.IsPostBack)
        {
            Session["ManageAgentData"] = null;
            errorMsg.Visible = Request.QueryString["req"] != null;
            DivElse.Visible = Request.QueryString["req"] == null;
            for (int j = 10; j >= 0; j--)
            {
                ddlAdult.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }

            FillPageInfo();
            if (Page.RouteData.Values["PageId"] != null)
            {
                var pageID = (Guid)Page.RouteData.Values["PageId"];
                PageContent(pageID, _siteId);
                Page.Header.DataBind();
            }
            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
            unavailableDates1 = "[";
            if (siteDDates.Any())
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
            QubitOperationLoad();

            if (Session["TrainSearch"] != null && Request.QueryString["req"] != null)
            {
                TrainInformationResponse response = Session["TrainSearch"] as TrainInformationResponse;
                if (response.TrainInformationList != null && response.TrainInformationList.Count() > 0)
                {
                    ucSResult.Visible = Request.QueryString["req"] == "BE";
                    ucSResultTi.Visible = Request.QueryString["req"] == "TI";
                    ucSResultEvolvi.Visible = Request.QueryString["req"] == "EV";
                }
            }

            IfAgentUserIsLoging();
            IfSiteIsAgent(_siteId);
            ShowHideTermsMandatoryText(_siteId);
        }
    }

    void ShowHaveRailPass(Guid siteID)
    {
        var railPass = _oWebsitePage.HavRailPass(siteID);
        divRailPass.Visible = railPass;
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    void IfSiteIsAgent(Guid siteID)
    {
        var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteID);
        if (objsite != null && objsite.IsAgent == false)
            lblNm.Text = "Name";
    }

    void IfAgentUserIsLoging()
    {
        if (Session["AgentUserID"] == null)
            return;

        ManageUser _ManageUser = new ManageUser();
        Guid IDuser = Guid.Parse(Session["AgentUserID"].ToString());
        tblAdminUser objUser = _ManageUser.AgentNameEmailById(IDuser);
        if (objUser != null)
        {
            txtName.Text = objUser.Forename + " " + objUser.Surname;
            txtEmailAddress.Text = objUser.EmailAddress;
        }
    }

    protected void Tab1_Click(object sender, EventArgs e)
    {
        //MainView.ActiveViewIndex = 0;
    }

    protected void Tab2_Click(object sender, EventArgs e)
    {
        //MainView.ActiveViewIndex = 1;
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _master.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');


                footerBlock.InnerHtml = oPage.FooterImg;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri,
                                 ex.Message + "; Inner Exception:" +
                                 (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    protected void btnSendInfo_Click(object sender, EventArgs e)
    {
        try
        {
            if (!chkMandatory.Checked)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "checkMandatory", "LoadCal1();checkMandatory();showhideterms();", true);
                return;
            }

            var obj = new TrainResultField
            {
                adult = Convert.ToInt32(ddlAdult.SelectedValue),
                child = Convert.ToInt32(ddlChild.SelectedValue),
                Class = ddlClass.SelectedValue,
                dateOfDepart = Convert.ToDateTime(txtDepartureDate.Text),
                departureTime = ddldepTime.SelectedValue,
                email = txtEmailAddress.Text,
                from = txtFrom.Text,
                ipAddress = Request.ServerVariables["REMOTE_ADDR"],
                FName = txtName.Text,
                phone = txtPhone.Text,
                senior = Convert.ToInt32(ddlSenior.SelectedValue),
                siteID = _siteId,
                to = txtTo.Text,
                youth = Convert.ToInt32(ddlYouth.SelectedValue),
                FIPNumber = string.Empty,
                DeptTNo = string.Empty,
                ReturnTNo = string.Empty,
                Notes = string.Empty,
                IsReturn = rdBookingType.SelectedValue == "1"
            };
            if (txtTrainReturnDate.Text.Trim() != "" && txtTrainReturnDate.Text.Trim() != "DD/MM/YYYY")
            {
                obj.dateOfArrival = Convert.ToDateTime(txtTrainReturnDate.Text);
                obj.arrivalTime = ddlReturnTime.SelectedValue;
            }
            else
            {
                obj.dateOfArrival = Convert.ToDateTime(txtDepartureDate.Text);
                obj.arrivalTime = ddldepTime.SelectedValue;
            }
            if (Request.QueryString["req"] != null)
            {
                if (Request.QueryString["req"] == "BE")
                    obj.apiName = "BENE";
                else if (Request.QueryString["req"] == "TI")
                    obj.apiName = "ITALIA";
                else if (Request.QueryString["req"] == "EV")
                    obj.apiName = "Evolvi";
            }
            else
                obj.apiName = "";

            long OrderId = new ManageJourneyRequest().AddTrainResultInfo(obj);

            string subject = "Quote - Dep: " + txtDepartureDate.Text + " Pax: " + txtName.Text;
            string body = GetEmailBody(OrderId, _siteId);
            SendMail(_siteId, subject, body, txtEmailAddress.Text, "");
            
            succmessage.Text = "Thank you, your request ID is " + OrderId + ", please make a note of this and quote it whenever you're in contact with us.  One of our rail experts will be in touch soon!";
            DivLeftOne.Style.Add("display", "none");
            DivLeftSecond.Style.Add("display", "block");
        }
        catch (Exception ex) { }
    }

    public bool SendMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail)
    {
        try
        {
            var message = new MailMessage();
            var smtpClient = new SmtpClient();
            var result = _master.GetEmailSettingDetail(siteId);
            var SiteData = _master.GetSiteListEdit(siteId);
            if (result != null)
            {
                smtpClient.Host = result.SmtpHost;
                smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                message.From = new MailAddress(FromEmail, FromEmail);
                if (!string.IsNullOrEmpty(SiteData.JourneyEmail))
                    ToMail = SiteData.JourneyEmail.ToLower() == ToMail ? ToMail : SiteData.JourneyEmail;
                message.To.Add(ToMail);
                message.Subject = Subject;
                message.IsBodyHtml = true;
                message.Body = Body;
                message.CC.Add("dipu.bharti@dotsquares.com");
                smtpClient.Send(message);
                return true;
            }
            else
                return false;
        }
        catch (Exception ee)
        {
            return false;
        }
    }

    public string GetEmailBody(long OrderID, Guid SiteId)
    {
        try
        {
            string siteURLS = _master.GetSiteListEdit(SiteId).SiteURL;
            string body = string.Empty; string htmfile = string.Empty;
            string bodyHtml = "";
            //ManageEmailTemplate objMailTemp = new ManageEmailTemplate();
            //htmfile = Server.MapPath("~/MailTemplate/TrainSearchJRFTemplate.html");
            //var xmlDoc = new XmlDocument();
            //xmlDoc.Load(htmfile);
            //var list = xmlDoc.SelectNodes("html");
            //bodyHtml = list[0].InnerXml.ToString();
            //bodyHtml = bodyHtml.Replace("##QuoteID##", OrderID.ToString());
            //bodyHtml = bodyHtml.Replace("##QuoteRequestedBy##", txtName.Text);
            //bodyHtml = bodyHtml.Replace("##EmailAddress##", txtEmailAddress.Text);
            //bodyHtml = bodyHtml.Replace("##PhoneNumber##", txtPhone.Text);
            //bodyHtml = bodyHtml.Replace("##NoOfAdult##", ddlAdult.SelectedValue);
            //bodyHtml = bodyHtml.Replace("##NoOfSenior##", ddlSenior.SelectedValue);
            //bodyHtml = bodyHtml.Replace("##NoOfYouth##", ddlYouth.SelectedValue);
            //bodyHtml = bodyHtml.Replace("##NoOfChild##", ddlChild.SelectedValue);

            //body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td colspan='2' style='padding: 5px 0px 5px 10px;'><strong>" + txtFrom.Text + " - " + txtTo.Text + "</strong></td></tr>";
            //body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Departs:</td><td>" + ddldepTime.Text + " " + txtDepartureDate.Text + "</td></tr>";
            //if (txtTrainReturnDate.Text.Trim() != "" && txtTrainReturnDate.Text.Trim() != "DD/MM/YYYY")
            //{
            //    body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Arrives:</td><td>" + ddlReturnTime.Text + " " + txtTrainReturnDate.Text + "</td></tr>";
            //}
            //else
            //{
            //    body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Arrives:</td><td>" + ddldepTime.Text + " " + txtDepartureDate.Text + "</td></tr>";
            //}
            //body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Class of travel:</td><td>" + ddlClass.SelectedItem.Text + "</td></tr>";
            //body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Max Transfers:</td><td>" + ddlTransfer.SelectedItem.Text + "</td></tr>";
            //if (chkLoyalty.Checked)
            //    body += "<tr style='background-color: #eeeeee; font-size: 14px;'><td width='30%' style='padding: 5px 0px 5px 10px;'>Journey Type:</td><td>Loyalty cards  </td></tr>";

            //bodyHtml = bodyHtml.Replace("##JourneyDetails##", body);
            //bodyHtml = bodyHtml.Replace("##SpecialInstruction##", string.Empty);
            //bodyHtml = bodyHtml.Replace("##SiteLogoLeft##", siteURLS + "images/newbookmyrst-logo.png");
            //bodyHtml = _siteId == IrAgentSiteID ? bodyHtml.Replace("##SiteLogoRight##", "") : bodyHtml.Replace("##SiteLogoRight##", siteURLS + "images/newbookmyrst-logo-right.png");

            bodyHtml = "<table  border='0' cellspacing='0' cellpadding='5' align='left' style='width:80%'><tbody>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Order ID</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + OrderID + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Your name</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtName.Text + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Your email</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + txtEmailAddress.Text + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Number of adults</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + ddlAdult.SelectedValue + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Number of senior</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + ddlSenior.SelectedValue + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Number of youth</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + ddlYouth.SelectedValue + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Number of child</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + ddlChild.SelectedValue + "</td></tr>";

            bodyHtml += "<tr><td width='1000px' colspan='2' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Journey Details</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>From</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtFrom.Text + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>To</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + txtTo.Text + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Date of departure</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtDepartureDate.Text + "</td></tr>";
            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Time of departure (24 hour)</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ddldepTime.Text + "</td></tr>";
            if (txtTrainReturnDate.Text.Trim() != "" && txtTrainReturnDate.Text.Trim() != "DD/MM/YYYY")
            {
                bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Date of arrival</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtTrainReturnDate.Text + "</td></tr>";

                bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Time of arrival (24 hour)</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ddlReturnTime.Text + "</td></tr>";
            }

            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Class of service</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + ddlClass.SelectedItem.Text + "</td></tr>";

            bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Max Transfers</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ddlTransfer.SelectedItem.Text + "</td></tr>";
            if (chkLoyalty.Checked)
                bodyHtml += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Journey Type</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Loyalty cards</td></tr>";

            bodyHtml += "</tbody></table>";
            bodyHtml = bodyHtml.Replace("##TableData##", bodyHtml);
            return bodyHtml;
        }
        catch (Exception ex) { throw ex; }
    }

    protected void chkLoyalty_CheckedChanged(object sender, EventArgs e)
    {
        if (chkLoyalty.Checked)
        {
            chkIhaveRailPass.Checked = false;
            ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "showhideterms();", true);
        }
    }

    protected void chkIhaveRailPass_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIhaveRailPass.Checked)
        {
            chkLoyalty.Checked = false;
            ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "showhideterms();", true);
        }
    }

    protected void rdBookingType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdBookingType.SelectedValue == "0")
        {
            txtTrainReturnDate.Enabled = ddlReturnTime.Enabled = rfReturnDate.Enabled = returnspan.Visible = false;
            txtTrainReturnDate.Text = "";
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "caldisableT()", true);
            ScriptManager.RegisterStartupScript(Page, GetType(), "Validationxx", "showhideterms();", true);
        }
        else
        {
            txtTrainReturnDate.Enabled = ddlReturnTime.Enabled = rfReturnDate.Enabled = returnspan.Visible = true;
            ScriptManager.RegisterStartupScript(Page, GetType(), "cal", "calenableT()", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Validationxx", "showhideterms();", true);
        }
    }

    public void FillPageInfo()
    {
        Boolean showBox = true;
        if (Session["ErrorMessage"] != null)
        {
            if (Session["ErrorMessage"].ToString().Trim() == "ErrorMaxDate")
                showBox = false;
        }

        /*Future dates msg end*/
        var pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
        var data = _oWebsitePage.GetSiteDatabySiteId(_siteId);
        if (pInfoSolutionsResponse == null || pInfoSolutionsResponse.ErrorMessage != null)
        {
            if (showBox)
            {
                if (data.IsStaP2PJourneyRequest)
                    Response.Redirect("JourneyRequestForm");
                else if (_siteId == IrAgentSiteID)
                    Response.Redirect("jrf");

                lblHeading.Text = "Journey Request Form";
                pnlJourneyInfo.Visible = true;
                lblMessageEarlierTrain.Visible = false;
            }
            else
            {
                if (data.IsStaP2PJourneyRequest)
                    Response.Redirect("JourneyRequestForm.aspx");
                else if (_siteId == IrAgentSiteID)
                    Response.Redirect("jrf");

                pnlJourneyInfo.Visible = true;
                lblMessageEarlierTrain.Text = "Online booking for the date you requested has not yet been opened by the operating train company, please try an earlier date(s).";
                lblMessageEarlierTrain.Visible = true;
                pCls.Attributes.Add("class", "clsError");
            }
        }
        else if (pInfoSolutionsResponse.TrainInformationList == null)
        {
            if (showBox)
            {
                if (data.IsStaP2PJourneyRequest)
                    Response.Redirect("JourneyRequestForm.aspx");
                else if (_siteId == IrAgentSiteID)
                    Response.Redirect("jrf");

                pnlJourneyInfo.Visible = true;
                lblMessageEarlierTrain.Visible = false;
            }
            else
            {
                pnlJourneyInfo.Visible = false;
                lblMessageEarlierTrain.Text = "Online booking for the date you requested has not yet been opened by the operating train company, please try an earlier date(s).";
                lblMessageEarlierTrain.Visible = true;
                pCls.Attributes.Add("class", "clsError");
            }
        }
        else
        {
            if (pInfoSolutionsResponse.TrainInformationList.Count() > 0)
            {
                pnlJourneyInfo.Visible = false;
                lblMessageEarlierTrain.Visible = false;
            }
            else
            {
                if (showBox)
                {
                    if (data.IsStaP2PJourneyRequest)
                        Response.Redirect("JourneyRequestForm.aspx");
                    else if (_siteId == IrAgentSiteID)
                        Response.Redirect("jrf");

                    pnlJourneyInfo.Visible = true;
                }
                else
                {
                    pnlJourneyInfo.Visible = false;
                    lblMessageEarlierTrain.Text = "Online booking for the date you requested has not yet been opened by the operating train company, please try an earlier date(s).";
                    lblMessageEarlierTrain.Visible = true;
                }
            }
        }

        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlYouth.SelectedValue = objBRUC.Youths.ToString();
            ddlSenior.SelectedValue = objBRUC.Seniors.ToString();
            rdBookingType.SelectedValue = objBRUC.Journeytype;
            if (objBRUC.ReturnDate != string.Empty)
            {
                txtTrainReturnDate.Enabled = true;
                rfReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                txtTrainReturnDate.Text = objBRUC.ReturnDate;
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                rfReturnDate.Enabled = true;
            }
            else
            {
                txtTrainReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                txtTrainReturnDate.Text = "";
                ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
                rfReturnDate.Enabled = false;
            }

            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            chkLoyalty.Checked = objBRUC.Loyalty;
            chkIhaveRailPass.Checked = objBRUC.isIhaveRailPass;
            chkLoyalty_CheckedChanged(null, null);
        }
    }

    public void ShowHideTermsMandatoryText(Guid SiteId)
    {
        try
        {
            var data = _oWebsitePage.GetSiteDatabySiteId(SiteId);
            if (data != null)
            {
                IsStaSite = data.IsSTA.HasValue ? (data.IsSTA.Value == true ? true : false) : false;
                div_Mandatory.Visible = data.IsMandatoryTerm;
                div_NotMandatory.Visible = data.IsMandatoryTermSecond;
                p_MandatoryText.InnerHtml = MandatoryTextFirst = !string.IsNullOrEmpty(data.MandatoryTermText) ? data.MandatoryTermText : "";
                p_NotMandatoryText.InnerHtml = MandatoryTextFirstSecond = !string.IsNullOrEmpty(data.MandatoryTermTextSecond) ? data.MandatoryTermTextSecond : "";
            }
        }
        catch (Exception ex) { }
    }
}