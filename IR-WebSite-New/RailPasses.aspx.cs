﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
#endregion

public partial class RailPasses : System.Web.UI.Page
{
    #region Local Variables
    //private readonly Masters _master = new Masters();
    //private readonly ManageJourney _objJourney = new ManageJourney();
    //private readonly db_1TrackEntities _db = new db_1TrackEntities();
    //public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    //private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    //private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();


    Guid siteId;
    public string Url = "";
    public string siteURL = "";
    //private readonly ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            GetAllContryContinent();
            Url = Request.Url.Host.ToString();
        }
        if (Page.RouteData.Values["PageId"] != null)
        {
            #region Seobreadcrumbs
            Guid id = (Guid)Page.RouteData.Values["PageId"];
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(id, siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion
        }
    }

    public void GetAllContryContinent()
    {
        RptRailPasses.DataSource = _oWebsitePage.GetRailPassesList(siteId, AgentuserInfo.UserID).SelectMany(x => x.MenuList).ToList();
        RptRailPasses.DataBind();
    }
}