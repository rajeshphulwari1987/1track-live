﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="errorpage.aspx.cs" Inherits="errorpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" >
      <title>Error page</title>
    <style type="text/css">
                *{padding:0;outline:none;margin:0;}
                .header{height:90px;}
                body{font:11px arial,helvetica,sans-serif;background:#FFF;}
                #errorPage{background:#f0f0f0;}
                #errorPage .header{border:none;margin-left:auto;margin-right:auto;}
                #errorMessage{width:550px;background:#FFF;margin:20px auto 0;border:1px solid #d60111;padding-bottom:10px;-moz-border-radius:7px;-webkit-border-radius:7px;-moz-box-shadow:0 2px 6px 1px #d6d6d6;-webkit-box-shadow:0 2px 6px 1px #d6d6d6;box-shadow:0 2px 6px 1px #d6d6d6;}
                #errorMessage h1{background:#d60111;height:36px;margin:2px;margin-bottom:10px;line-height:32px;font-size:18px;font-weight:bold;padding-left:5px;display:block;color:#FFF;overflow:hidden;-moz-border-radius-topleft:4px;-webkit-border-top-left-radius:4px;-moz-border-radius-topright:4px;-webkit-border-top-right-radius:4px;}
                #errorMessage p{color:#666;line-height:18px;padding:0 0 0 10px;margin-left:-1px;}
                #errorMessage a:visited,#errorMessage a{color:#d60111;}
                #errorMessage a:hover{color:#000;}
    </style>

</head>
<body>
    <form id="form1" runat="server">
   <div class="container">
        <div id="errorMessage"><h1>Unexpected error</h1><p>An unexpected error occured, We apologize for the inconvenience</p><p>Go back to <a href="/">home page</a></p></div>
    </div>
    </form>
</body>
</html>
