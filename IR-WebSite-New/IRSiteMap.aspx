﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IRSiteMap.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="IRSiteMap" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        [id*="TreeView1"] table td span
        {
            color: #941e34 !important;
            font-size: 16px;
        }
    </style>
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server"
                    src="https://1track.internationalrail.net/CMSImages/banner04.jpg" />
            </div>
        </div>
    </div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px;">
        <h2>
            Sitemap
        </h2>
        <p>
            Find your way around our site.
        </p>
        <div class="starail-Box starail-Box--whiteMobile starail-ContactForm" id="siteMapWrap">
            <asp:SiteMapDataSource ID="siteMapdtSrc" runat="server" ShowStartingNode="False" />
            <asp:TreeView ID="TreeView1" runat="server" CssClass="clsSMap" DataSourceID="siteMapdtSrc"
                ShowExpandCollapse="False">
                <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                    VerticalPadding="0px" />
            </asp:TreeView>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $("#siteMapWrap a:first-child img").remove();
        })</script>
</asp:Content>
