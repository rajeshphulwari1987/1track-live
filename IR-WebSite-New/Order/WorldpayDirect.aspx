﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorldpayDirect.aspx.cs" Inherits="Order_WorldpayDirect" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/jquery-1.9.0.min.js"></script>

    <style type="text/css">
        .form-row {
            padding: 10px;
            width: 100%;
        }

            .form-row div {
                width: 32%;
                display: inline-block;
            }

            .form-row input[type=text] {
                border: 1px solid #ccc;
                padding: 10px;
                width: 55%;
                font-size: 16px;
                color: #444;
            }

            .form-row select {
                border: 1px solid #ccc;
                padding: 10px;
                font-size: 16px;
                color: #444;
            }

        .success {
            color: #4F8A10;
            background-color: #DFF2BF;
            font-size: 14px;
            background-image: url(../images/success.png);
        }

        .error, .info, .success, .warning {
            border: 1px solid;
            margin: 10px 0;
            padding: 15px 10px 15px 50px !important;
            background-repeat: no-repeat;
            background-position: 10px center;
        }

        .error {
            color: #D8000C;
            font-size: 14px;
            background-color: #FFBABA;
            background-image: url(../images/error.png);
        }

        .main {
            font-family: Tahoma,Verdana,Segoe,sans-serif;
            /*background-color: #edeff1;*/
            padding: 10px;
            font-weight: 400;
            font-size: 16px;
            color: #444;
        }

        .starail-Button {
            position: relative;
            color: #0073ae;
            font-weight: 700;
            background-color: #c9da00;
            padding: 8px 10px;
            cursor: pointer;
            vertical-align: middle;
            white-space: normal;
            text-transform: uppercase;
            border: 1px solid #bdcd00;
            border-radius: 1px;
            text-align: center;
            font-size: 17px;
            box-shadow: 1px 1px 2px rgba(0,0,0,0.3);
            transition: background-color 300ms ease-in-out,color 300ms ease-in-out;
        }

        .inner-box {
            width: 50%;
            margin: auto;
            text-align: left;
        }

        .min-txt {
            font-size: 14px;
        }

        .starail-Form-error {
            background-color: #ffedee !important;
            border-color: #e6462e !important;
            border: 1px solid;
        }

        .wait-msg {
            color: #0073ae;
            margin-left: 132px;
            margin-top: -20px;
        }

        .msg-load {
            padding: 20px;
            font-size: 16px;
        }
    </style>
    <script type="text/javascript">
        function Hidewait() {
            $("#msgonload", window.parent.document).hide();
        }

        function pleaswait() {
            $("[id*=Name],[id*=CVCNo],[id*=CardNo]").removeClass("starail-Form-error");
            var HasError = false;
            if ($("[id*=CardNo]").val().length < 14 || !$.isNumeric($("[id*=CardNo]").val())) {
                $("[id*=CardNo]").addClass("starail-Form-error");
                HasError = true;
            }
            if ($("[id*=CVCNo]").val().length < 3 || !$.isNumeric($("[id*=CVCNo]").val())) {
                $("[id*=CVCNo]").addClass("starail-Form-error");
                HasError = true;
            }
            if ($("[id*=Name]").val().match("^[a-zA-Z \(\)]+$") == null) {
                $("[id*=Name]").addClass("starail-Form-error");
                HasError = true;
            }
            if (HasError)
                return false;
            $("#msg").show();
            return true;
        }

        function resetmsg() {
            $("#msg").hide();
            $("[id*=BtnSubmit]").removeAttr("disabled");
        }

        function Showwait() {
            $("#msgonload", window.parent.document).show().text("Please do not refresh the page and wait while we are processing your payment.");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main" runat="server" id="maindiv">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" />
                </div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                </div>
            </asp:Panel>
            <br />
            <h1 style="text-align: center;">
                <asp:Label runat="server" ID="lblDescription" Text="Happy shopping with us"></asp:Label>

            </h1>
            <div class="inner-box">
                <div class="form-row">
                    <div>Name</div>
                    <asp:TextBox runat="server" ID="Name" Text="" MaxLength="40"></asp:TextBox>
                </div>

                <div class="form-row">
                    <div>Card Number</div>
                    <asp:TextBox runat="server" ID="CardNo" size="20" Text="" MaxLength="16"></asp:TextBox>
                </div>


                <div class="form-row">
                    <div>CVC</div>
                    <asp:TextBox runat="server" ID="CVCNo" MaxLength="4" Text=""></asp:TextBox>
                </div>

                <div class="form-row">
                    <div>Expired<span class="min-txt"> (MM/YYYY)</span></div>
                    <asp:DropDownList runat="server" ID="ExpMonth">
                    </asp:DropDownList>
                    <span>/ </span>
                    <asp:DropDownList runat="server" ID="ExpYear">
                    </asp:DropDownList>
                </div>
                <div class="form-row" style="text-align: center;">
                    <asp:Button ID="BtnSubmit" runat="server" Text="Confirm payment" OnClick="BtnSubmit_Click" OnClientClick="return pleaswait();" CssClass="starail-Button " />
                </div>
                <div class="form-row" style="display: none;" id="msg">
                    <%--<span class="wait-msg">Please Wait...</span>--%>
                    <img class="wait-msg" src="../images/giphy.gif" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
