﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business;
using System.Configuration;

public partial class Order_WorldPayPaymentConfirmation : System.Web.UI.UserControl
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private Guid _siteId;
    public string OrderNo = string.Empty;
    public string linkURL = string.Empty;
    public int amount = 0;
    public string Amount = "";
    public string SiteUrl = string.Empty;
    public string ReferenceId = string.Empty;
    public string MerchantCode, UserName, Password, BaseUrl = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            _siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var objsite = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
            if (objsite.PaymentType != 3)
                return;

            SiteUrl = objsite.SiteURL;

            if (Request.Params["req"] != null)
                OrderNo = Request.Params["req"];
            else if (Session["OrderID"] != null)
                OrderNo = Session["OrderID"].ToString();
            else if (Session["P2POrderID"] != null)
                OrderNo = Session["P2POrderID"].ToString();

            if (string.IsNullOrEmpty(OrderNo))
            {
                Response.Redirect("~/Home");
            }
            setOrderData();
            LoadWorldPay();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void setOrderData()
    {
        try
        {
            ManageBooking objB = new ManageBooking();
            List<GetAllCartData_Result> lst = objB.GetAllCartData(Convert.ToInt64(OrderNo));
            if (lst.Count() > 0)
            {
                PaymentGateWayTransffer objPT = Session["PayObj"] as PaymentGateWayTransffer;
                if (objPT == null)
                    return;
                string count = lst.FirstOrDefault().DCountry;
                var tblcountry = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName == count);
                string IsoCode = tblcountry != null ? tblcountry.IsoCode : lst.FirstOrDefault().DCountry;
                Session["CustomerData"] = lst.FirstOrDefault().Address1 + " " + lst.FirstOrDefault().Address2 + ";" + lst.FirstOrDefault().City + ";" + lst.FirstOrDefault().Postcode + ";" + IsoCode + ";" + lst.FirstOrDefault().EmailAddress;
                tblSite objSite = new ManageJourney().GetSiteList().Where(a => a.ID == lst.FirstOrDefault().SiteID).FirstOrDefault();
                tblCurrencyMst objCurrency = new Masters().GetCurrencyList().Where(a => a.ID == objSite.DefaultCurrencyID).FirstOrDefault();
                string StTkProtnAmt = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(objPT.Amount), objSite.ID, objCurrency.ID, objSite.DefaultCurrencyID.HasValue ? objSite.DefaultCurrencyID.Value : new Guid()).ToString("F");
                Session["Amount"] = Convert.ToDouble(StTkProtnAmt);
                Session["currencyCode"] = objCurrency.ShortCode;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void LoadWorldPay()
    {
        try
        {
            WorldPayGateway gateway = new WorldPayGateway();
            WorldPayGatewayRequest request = new WorldPayGatewayRequest();
            if (Session["Amount"] != null)
            {
                Amount = Session["Amount"].ToString();
            }
            amount = Convert.ToInt32(Convert.ToDecimal(Amount) * 100);

            var BookingDetails = new ManageBooking().GetBookingDetailsByOrderId(Convert.ToInt64(OrderNo));
            string countrycode = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName.Contains(BookingDetails.Country)).IsoCode;

            request.Order = new Order
            {
                amount = amount,
                CurrencyCode = Session["currencyCode"].ToString(),
                exponent = "2",//which specifies where the decimal point or comma should be placed in the value
                orderCode = OrderNo,
                description = "Happy shopping with us",
                paymentMethodMask = new paymentMethodMask
                {
                    Code = "All"
                },
                shopper = new shopper
                {
                    shopperEmailAddress = BookingDetails.EmailAddress
                },
                shippingAddress = new shippingAddress
                {
                    City = string.IsNullOrEmpty(BookingDetails.City) ? "" : BookingDetails.City,
                    countryCode = countrycode,
                    firstName = BookingDetails.FirstName,
                    houseNumber = 123,
                    lastName = BookingDetails.LastName,
                    postalCode = BookingDetails.Postcode,
                    street = BookingDetails.Address1
                }
            };

            var objWorld = _masterPage.GetWorldPayDetails(_siteId);
            if (objWorld != null)
            {
                BaseUrl = objWorld.WorldPayURL;
                MerchantCode = objWorld.MerchantCode;
                UserName = objWorld.UserName;
                Password = objWorld.Password;
            }
            else
            {
                ShowMessage(2, "Payment credential not found.");
                return;
            }

            var response = gateway.PaymentProcess(request, SiteUrl, BaseUrl, MerchantCode, UserName, Password);
            if (response != null)
            {
                if (!string.IsNullOrEmpty(response.ReferenceId))
                {
                    ReferenceId = response.ReferenceId.ToString();
                    new ManageBooking().UpdatePaymentReferenceId(Convert.ToInt64(OrderNo), ReferenceId);

                    Session["WorldPayResponseUrl"] = response.Url;
                    PaymentFrame.Attributes["src"] = response.Url;
                }
                else
                    ShowMessage(2, response.ErrorMessage);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}