﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication;
using System.Xml;
using System.Net.Mail;
using Business;
using System.Net;

public partial class Order_PaymentCancelled : System.Web.UI.Page
{
    private Guid _siteId;
    public string OrderID = string.Empty;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Emailsend();
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());

        if (Request.Params["req"] != null)
            OrderID = Request.Params["req"];
        else if (Session["OrderID"] != null)
            OrderID = Session["OrderID"].ToString();
        else if (Session["P2POrderID"] != null)
            OrderID = Session["P2POrderID"].ToString();

        var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);

        if (!string.IsNullOrEmpty(OrderID))
        {
            ManageBooking obj = new ManageBooking();
            new ManageBooking().UpdateOrderStatus(6, Convert.ToInt64(OrderID));
        }
        else
        {
            string rSiteName = st.SiteURL + "home";
            ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + rSiteName + "'; </script>");
        }
        Session["OrderID"] = null;
        Session["P2POrderID"] = null;
        Session["RailPassData"] = null;

        if (st != null)
        {
            string SiteName = SiteName = st.SiteURL + "OrderCancel";
            ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + SiteName + "'; </script>");
        }
    }
    public void Emailsend()
    {
        var smtpClient = new SmtpClient();
        var message = new MailMessage();
        var fromAddres = new MailAddress("admin@1tracktest.com", "admin@1tracktest.com");
        smtpClient.Host = "mail.dotsquares.com";
        smtpClient.Port = 587;
        smtpClient.UseDefaultCredentials = true;
        smtpClient.Credentials = new NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
        smtpClient.EnableSsl = false;
        message.From = fromAddres;
        message.To.Add("rajeshkumar.phulwari@dotsquares.com");
        message.Bcc.Add("dipu.bharti@dotsquares.com");
        message.Subject = "Demo: Payment status 46 feedback on accept";
        message.IsBodyHtml = true;
        message.Body = "Live: Payment status 46 feedback in [cancelled] on accept stirng::::::::\n " + Request.QueryString;
        smtpClient.Send(message);
    }
}