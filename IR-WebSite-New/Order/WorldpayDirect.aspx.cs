﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Net.Http;
using Business;

public partial class Order_WorldpayDirect : System.Web.UI.Page
{
    Masters MasterPage = new Masters();
    ManageBooking MBooking = new ManageBooking();
    ManageOrder MOrder = new ManageOrder();
    String BaseUrl = "";
    String MerchantCode = "";
    String Password = "";
    String InstallationID = "";
    Boolean Is3D = false;
    Guid SiteId = Guid.Empty;
    String OrderNo = "";
    Int32 Amount = 0;
    String SalCurrency = "";
    String Address = "";
    String Address2 = "";
    String PostCode = "";
    String City = "";
    String IsoCode = "";
    String ShopperEmailAddress = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            SiteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null)
            OrderNo = Request.Params["req"];
        else if (Session["OrderID"] != null)
            OrderNo = Session["OrderID"].ToString();
        else if (Session["P2POrderID"] != null)
            OrderNo = Session["P2POrderID"].ToString();

        if (string.IsNullOrEmpty(OrderNo))
        {
            Response.Redirect("~/Home");
        }
        if (Request.Params["PaRes"] != null)
        {
            maindiv.Visible = false;
            SecountRequest(Session["ShopperSessionId"].ToString(), Session["ShopperOrderNo"].ToString());
        }
        if (!IsPostBack)
        {
            if (Session["Amount"] != null)
            {
                Guid DefaultCurrency = MasterPage.GetSiteListEdit(SiteId).DefaultCurrencyID.Value;
                var currency = MasterPage.GetCurrencyListEdit(DefaultCurrency).Symbol;
                BtnSubmit.Text = "Confirm payment of " + currency + " " + Session["Amount"];
            }
            for (int i = 1; i <= 12; i++)
                ExpMonth.Items.Add(new ListItem { Value = i < 10 ? "0" + i : i.ToString(), Text = i < 10 ? "0" + i : i.ToString() });
            for (int i = DateTime.Now.Year; i < DateTime.Now.Year + 10; i++)
                ExpYear.Items.Add(new ListItem { Value = i.ToString(), Text = i.ToString() });
        }
        ScriptManager.RegisterStartupScript(this, GetType(), "yy", "Hidewait();", true);
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        ShowMessage(0, "");
        PaymentProcess();
    }

    private void Handle3DSResponse(string oneTime3DsToken, string redirectURL)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Load", "Showwait();", true);
            Response.Clear();
            maindiv.Visible = false;
            StringBuilder sb = new StringBuilder();
            sb.Append("<html>");
            sb.AppendFormat(@"<body onload='document.forms[""submitForm""].submit();'>");
            sb.AppendFormat("<form name='submitForm' action='{0}' method='post'>", redirectURL);
            sb.AppendFormat("<input type='hidden' name='PaReq' value='{0}'>", oneTime3DsToken);
            sb.AppendFormat("<input type='hidden' name='TermUrl' id='termUrl' value='{0}'>", redirectURL);
            sb.Append("<script>\ndocument.getElementById('termUrl').value =window.location.href;\n</script>");
            sb.Append("</form>");
            sb.Append("</body>");
            sb.Append("</html>");
            Response.Write(sb.ToString());
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    private bool GetWorldpayCredential()
    {
        try
        {
            var objWorld = MasterPage.GetWorldPayDetails(SiteId);
            if (objWorld != null)
            {
                BaseUrl = objWorld.WorldPayURL;
                MerchantCode = objWorld.MerchantCode;
                Password = objWorld.Password;
                InstallationID = objWorld.InstallationId;
                Is3D = objWorld.IsEnableThreeD;
            }
            else
            {
                ShowMessage(2, "Payment credential not found.");
                return true;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
        return false;
    }

    public bool SetOrderData()
    {
        try
        {

            if (Session["Amount"] == null)
            {
                ShowMessage(2, "Payment is not valid.");
                return true;
            }

            List<GetAllCartData_Result> lst = MBooking.GetAllCartData(Convert.ToInt64(OrderNo));
            if (lst.Count() > 0)
            {
                var FristData = lst.FirstOrDefault();
                IsoCode = MasterPage.GetCountryList().FirstOrDefault(x => x.CountryName == FristData.DCountry).IsoCode;
                Address = FristData.Address1;
                Address2 = FristData.Address2;
                City = FristData.City;
                PostCode = FristData.Postcode;
                ShopperEmailAddress = FristData.EmailAddress;
                Guid DefaultCurrency = MasterPage.GetSiteListEdit(SiteId).DefaultCurrencyID.Value;
                SalCurrency = MasterPage.GetCurrencyListEdit(DefaultCurrency).ShortCode;
                Amount = Convert.ToInt32(Convert.ToDecimal(Session["Amount"]) * 100);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
        return false;
    }

    public void PaymentProcess()
    {
        try
        {
            if (SetOrderData() || GetWorldpayCredential())
                return;
            string ShopperIpAddress = HttpContext.Current.Request.UserHostAddress;
            string ShopperSessionId = HttpContext.Current.Session.SessionID;
            string ShopperUserAgent = HttpContext.Current.Request.UserAgent;
            string ShopperAcceptHeader = String.Join(";", HttpContext.Current.Request.AcceptTypes);

            Session["ShopperOrderNo"] = OrderNo;
            Session["ShopperSessionId"] = ShopperSessionId;
            Session["ShopperName"] = Name.Text;
            Session["ShopperCardNo"] = CardNo.Text;
            Session["ShopperExpiredDate"] = (ExpMonth.SelectedValue + "/" + ExpYear.SelectedValue);

            string xmldata = @"<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE paymentService PUBLIC '-//WorldPay//DTD WorldPay PaymentService v1//EN' 'http://dtd.worldpay.com/paymentService_v1.dtd'>" +
            "<paymentService version='1.4' merchantCode='" + MerchantCode + "'>" +
             "<submit>" +
                "<order orderCode='" + OrderNo + "'>" +// installationId='" + InstallationID + "'
                    "<description>" + lblDescription.Text + "</description>" +
                    "<amount value='" + Amount + "' currencyCode='" + SalCurrency + "' exponent='2' debitCreditIndicator='credit' />" +
                    "<paymentDetails>" +
                        "<CARD-SSL>" +
                            "<cardNumber>" + CardNo.Text + "</cardNumber>" +
                            "<expiryDate><date month='" + ExpMonth.SelectedValue + "' year='" + ExpYear.SelectedValue + "'/></expiryDate>" +
                            "<cardHolderName>" + (Name.Text) + "</cardHolderName>" +/*(Is3D ? "3D" : Name.Text)*/
                            "<cvc>" + CVCNo.Text + "</cvc>" +
                            "<cardAddress>" +
                            "<address>" +
                                "<address1>" + Address + "</address1>" +
                                 "<address2>" + Address2 + "</address2>" +
                                "<postalCode>" + PostCode + "</postalCode>" +
                                "<city>" + City + "</city>" +
                                "<countryCode>" + IsoCode + "</countryCode>" +
                            "</address>" +
                            "</cardAddress>" +
                        "</CARD-SSL>" +
                        "<session shopperIPAddress='" + ShopperIpAddress + "' id='" + ShopperSessionId + "' />" +
                    "</paymentDetails>" +
                    "<shopper>" +
                        "<shopperEmailAddress>" + ShopperEmailAddress + "</shopperEmailAddress>" +
                        "<browser>" +
                            "<acceptHeader>" + ShopperAcceptHeader + "</acceptHeader>" +
                            "<userAgentHeader>" + ShopperUserAgent + "</userAgentHeader>" +
                        "</browser>" +
                    "</shopper>" +
                "</order>" +
             "</submit>" +   
            "</paymentService>";

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)SPType.Tls | (SecurityProtocolType)SPType.Tls11 | (SecurityProtocolType)SPType.Tls12;
            var credential = new NetworkCredential(MerchantCode, Password);
            HttpClientHandler handler = new HttpClientHandler { Credentials = credential };
            HttpClient client = new HttpClient(handler);
            client.BaseAddress = new System.Uri(BaseUrl);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/xml"));
            System.Net.Http.HttpContent content = new StringContent(xmldata, UTF8Encoding.UTF8, "text/xml");
            HttpResponseMessage ResponseMessage = client.PostAsync(BaseUrl, content).Result;

            if (ResponseMessage.IsSuccessStatusCode)
            {
                /*http://support.worldpay.com/support/kb/gg/corporate-gateway-guide-beta/content/directintegration/authentication.htm*/
                /*pass 1st 3D response header machine cookie in 2nd 3D request header*/
                Session["FirstResponseHeaderCookies"] = ResponseMessage.Headers.GetValues("Set-Cookie").FirstOrDefault();
                string Result = ResponseMessage.Content.ReadAsStringAsync().Result;
                ClsErrorLog.AddError("Request (1) " + OrderNo + ": : " + Request.Url.ToString(), xmldata + "   <br/>   " + Result);
                ClsErrorLog.AddError("Request (1) Header ResponseMessage " + OrderNo + ": : " + Request.Url.ToString(), ResponseMessage.Headers.ToString());
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(Result);
                XmlNode NodeElement = null;
                string datamsg = "";
                string oneTime3DsToken = "";
                string redirectURL = "";

                NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/error");
                if (NodeElement != null)
                    datamsg = NodeElement.InnerText;
                else
                {
                    NodeElement = xDoc.SelectSingleNode("paymentService/reply/error");
                    if (NodeElement != null)
                        datamsg = NodeElement.InnerText;
                }
                if (datamsg != "")
                {
                    ClsErrorLog.AddError(Request.Url.ToString(), Result);
                    ShowMessage(2, datamsg);//"Payment has been declined or invalid details. " + 
                    ScriptManager.RegisterStartupScript(this, GetType(), "Load", "resetmsg();", true);
                }
                else
                {
                    if (Is3D)
                    {
                        NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/payment/lastEvent");
                        if (NodeElement != null)
                        {
                            datamsg = NodeElement.InnerText;
                            PaymentSuccess(NodeElement.InnerText, string.Empty, false, Result, xDoc);
                        }
                        else
                        {
                            NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/requestInfo/request3DSecure/paRequest");
                            if (NodeElement != null)
                                oneTime3DsToken = NodeElement.InnerText;
                            NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/requestInfo/request3DSecure/issuerURL");
                            if (NodeElement != null)
                                redirectURL = NodeElement.InnerText;
                            Handle3DSResponse(oneTime3DsToken, redirectURL);
                        }
                    }
                    else
                    {
                        NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/payment/lastEvent");
                        if (NodeElement != null)
                            PaymentSuccess(NodeElement.InnerText, string.Empty, false, Result, xDoc);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    private void SecountRequest(string shopperSessionId, string orderCode)
    {
        try
        {
            GetWorldpayCredential();
            string xmldata = @"<?xml version='1.0' encoding='UTF-8'?>" +
                 "<!DOCTYPE paymentService PUBLIC '-//Worldpay//DTD Worldpay PaymentService v1//EN' 'http://dtd.worldpay.com/paymentService_v1.dtd'>" +
                 "<paymentService version='1.4' merchantCode='" + MerchantCode + "'>" +
                   "<submit>" +
                     "<order orderCode='" + orderCode + "'>" +
                       "<info3DSecure>" +
                         "<paResponse>" + Request.Params["PaRes"] + "</paResponse>" +
                       "</info3DSecure>" +
                       "<session id='" + shopperSessionId + "'/>" +
                     "</order>" +
                   "</submit>" +
                 "</paymentService>";

            var credential = new NetworkCredential(MerchantCode, Password);
            HttpClientHandler handler = new HttpClientHandler { Credentials = credential };
            HttpClient client = new HttpClient(handler);
            client.DefaultRequestHeaders.Add("Cookie", Convert.ToString(Session["FirstResponseHeaderCookies"]));
            client.BaseAddress = new System.Uri(BaseUrl);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/xml"));
            System.Net.Http.HttpContent content = new StringContent(xmldata, UTF8Encoding.UTF8, "text/xml");
            HttpResponseMessage ResponseMessage = client.PostAsync(BaseUrl, content).Result;
            if (ResponseMessage.IsSuccessStatusCode)
            {
                string Result = ResponseMessage.Content.ReadAsStringAsync().Result;
                ClsErrorLog.AddError("Request 3D (2) " + orderCode + ": : " + Request.Url.ToString(), xmldata + "   <br/>   " + Result);
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(Result);
                XmlNode NodeElement = null;
                NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/error");
                string datamsg = "";
                if (NodeElement != null)
                    datamsg = NodeElement.InnerText;
                else
                {
                    NodeElement = xDoc.SelectSingleNode("paymentService/reply/error");
                    if (NodeElement != null)
                        datamsg = NodeElement.InnerText;
                }
                if (datamsg != "")
                {
                    ClsErrorLog.AddError(Request.Url.ToString(), Result);
                    ShowMessage(2, "Operation failed. Please try after some time. " + datamsg);
                    new ManageBooking().UpdateOrderStatus(23, Convert.ToInt64(orderCode));
                    maindiv.Visible = true;
                }
                else
                {
                    NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/payment/ThreeDSecureResult");
                    if (NodeElement != null)
                        datamsg = NodeElement.Attributes["description"].Value;
                    NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/payment/lastEvent");
                    if (NodeElement != null)
                        PaymentSuccess(NodeElement.InnerText, datamsg, true, Result, xDoc);
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    private void PaymentSuccess(string paymentStatus, string datamsg, bool IS3DPAYMENTSecountRequest, string Result, XmlDocument xDoc)
    {
        try
        {
            String SiteUrl = MasterPage.GetSiteListEdit(SiteId).SiteURL;
            var data = MOrder.GetOrderbyOrdId(Convert.ToInt64(OrderNo));
            if (data != null && data.TrvType == "P2P")
                SiteUrl = SiteUrl + "OrderSuccessPage?req=" + OrderNo;
            else if (Session["OrderId"] != null)
                SiteUrl = SiteUrl + "OrderSuccessPage";
            if (paymentStatus == "AUTHORISED")
            {
                Session["IsPaymentSuccess"] = "PAID";
                new ManageBooking().UpdateOrderStatus(3, Convert.ToInt64(OrderNo));
                ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + SiteUrl + "'; </script>");
            }
            else
            {
                ClsErrorLog.AddError(Request.Url.ToString(), Result);
                ShowMessage(2, "Operation failed. Please try after some time. " + paymentStatus);
                new ManageBooking().UpdateOrderStatus(23, Convert.ToInt64(OrderNo));
            }
            XmlNode NodeElement = xDoc.SelectSingleNode("paymentService/reply/orderStatus/payment/paymentMethod");
            if (NodeElement != null)
                UpdatePaymentCardDetails(NodeElement.InnerText, OrderNo);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    private void SentMail(string orderno, string result, string types)
    {
        try
        {
            var db = new db_1TrackEntities();
            db.tblPaymentGatwayLogs.AddObject(new tblPaymentGatwayLog
            {
                GateWayName = "Live: WorldPay Payment Accepted " + types + " Response #" + orderno,
                OrderId = Convert.ToInt64(orderno),
                Request = "",
                Response = result,
                CratedeOn = DateTime.Now
            });
            db.SaveChanges();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void UpdatePaymentCardDetails(string Brand, string OrderNo)
    {
        try
        {
            new ManageBooking().UpdateOrderPaymentDetail(Convert.ToInt64(OrderNo), Session["ShopperName"].ToString(), Session["ShopperCardNo"].ToString(), OrderNo, Session["ShopperExpiredDate"].ToString(), Brand);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
}