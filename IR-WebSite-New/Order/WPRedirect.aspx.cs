﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

public partial class Order_WPRedirect : System.Web.UI.Page
{
    Masters MPage = new Masters();
    Guid SiteId = Guid.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            SiteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                string OrderNo = "0";
                bool IsPass = false;
                if (Session["OrderID"] != null)
                {
                    OrderNo = Session["OrderID"].ToString();
                    IsPass = true;
                }
                else if (Session["P2POrderID"] != null)
                    OrderNo = Session["P2POrderID"].ToString();

                string RequestDate = "";
                foreach (var cookieKey in Request.QueryString.AllKeys)
                    RequestDate += cookieKey + ": " + Request.QueryString[cookieKey] + ",  ";

                ClsErrorLog.AddError("Worldpay post URL Response(2) QueryString:: " + OrderNo + ": : " + Request.Url.ToString(), RequestDate);
                ClsErrorLog.AddError("Worldpay post URL Response(2):: " + OrderNo + ": : " + Request.Url.ToString(), Request.Params.ToString());

                String SiteUrl = MPage.GetSiteListEdit(SiteId).SiteURL;
                if (Request.Params["paymentStatus"] == "AUTHORISED" || Request.Params["status"] == "Success")
                {
                    Session["IsPaymentSuccess"] = "PAID";
                    new ManageBooking().UpdateOrderStatus(3, Convert.ToInt64(OrderNo));
                    new ManageBooking().UpdateOrderPaymentDetail(Convert.ToInt64(OrderNo), "", "XXXX-XXXX-XXXX-XXXX", OrderNo, "XXXX", "Card");
                    if (IsPass)
                        Response.Redirect(SiteUrl + "OrderSuccessPage");
                    else
                        Response.Redirect(SiteUrl + "OrderSuccessPage?req=" + OrderNo);
                }
                else
                {
                    if (Request.Params["status"] == "Cancel")
                    {
                        if (IsPass)
                        {
                            if (Session["RailPassData"] != null)
                                Session["NewRailPassData"] = Session["RailPassData"] as List<getRailPassData>;
                            Session.Remove("OrderID");
                            Session.Remove("RailPassData");
                        }
                        else
                        {
                            Session.Remove("P2POrderID");
                            Session.Remove("IsRegional");
                            Session.Remove("SegmentType");
                            Session.Remove("holdTodStationList");
                            Session.Remove("HoldJourneyDetailsList");
                            Session.Remove("HoldOldJourneyResponse");
                            Session.Remove("HoldReserVationRequest");
                            Session.Remove("OutTrainTimeDateEvolviList");
                            Session.Remove("InTrainTimeDateEvolviList");
                        }
                        new ManageBooking().UpdateOrderStatus(6, Convert.ToInt64(OrderNo));
                        Cancelpayment.Visible = true;
                        Continuepayment.Visible = false;
                    }
                    else
                    {
                        new ManageBooking().UpdateOrderStatus(23, Convert.ToInt64(OrderNo));
                        Cancelpayment.Attributes.Add("class", "bordered-btn margin-btn");
                        Continuepayment.Visible = Cancelpayment.Visible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Cancelpayment.Visible = true;
            Continuepayment.Visible = false;
        }
        lblmsg.InnerHtml = Request.Params["status"] + " due to any of these reasons:";
    }
}