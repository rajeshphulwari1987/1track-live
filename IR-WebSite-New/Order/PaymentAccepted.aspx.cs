﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication;
using System.Xml;
using System.Net.Mail;
using Business;
using OgoneIR;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Text.RegularExpressions;

public partial class Order_PaymentAccepted : System.Web.UI.Page
{
    private Guid _siteId;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string currency = "$";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static Guid currencyID = new Guid();
    public string OrderNo = string.Empty;
    public string linkURL = string.Empty;
    public bool IsP2PPass = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            GetCurrencyCode();
        if (Session["OgoneOrderResponse"] != null)
        {
            OgoneOrderResponse response = Session["OgoneOrderResponse"] as OgoneOrderResponse;
            OrderNo = Regex.Replace(response.ORDERID, "[A-Za-z ]", "");
        }
        else if (Request.Params["orderID"] != null)
            OrderNo = Regex.Replace(Request.Params["orderID"], "[A-Za-z ]", "");
        else if (Request.Params["orderKey"] != null)
            OrderNo = Regex.Replace(Request.Params["orderKey"], "[A-Za-z ^]", "");
        else if (Request.Params["req"] != null && Session["P2POrderID"] != null)
            OrderNo = Session["P2POrderID"].ToString();
        else if (Session["OrderID"] != null)
            OrderNo = Session["OrderID"].ToString();

        long orderid = Convert.ToInt64(OrderNo);
        var data = _db.tblOrders.FirstOrDefault(x => x.OrderID == orderid);
        if (data != null)
        {
            IsP2PPass = data.TrvType == "P2P";
            data.Brand = Request.QueryString["BRAND"];
            data.PaymentId = Request.QueryString["PAYID"];
            _db.SaveChanges();
        }

        //Ogone Gateway response
        if (Session["OgoneOrderResponse"] != null)
        {
            CreateLog(OrderNo, Request.QueryString.ToString(), "Ogone");
            bool removesession = false;
            bool checkpayment = false;
            OgoneOrderResponse response = Session["OgoneOrderResponse"] as OgoneOrderResponse;
            if (Session["OgoneOrderResponse"] != null && response.STATUS == TransactionResponseStatus.Payment_requested)
                checkpayment = true;
            else if (Request.QueryString["STATUS"] != null)
                checkpayment = Request.QueryString["STATUS"] == "9";

            if (!checkpayment)
            {
                if (IsP2PPass)
                    linkURL = "PaymentProcess?req=" + OrderNo;
                else
                    linkURL = "PaymentProcess";
                if (Request.QueryString["STATUS"] == "2")
                {
                    if (Session["counterOTP"] == null)
                        Session["counterOTP"] = 1;
                    else
                        Session["counterOTP"] = Convert.ToInt16(Session["counterOTP"]) + 1;
                    if (Convert.ToInt16(Session["counterOTP"]) < 4)
                        Response.Write(response.HTML_ANSWER);
                    else
                        removesession = true;
                }
            }
            else
            {
                linkURL = Success();
                removesession = true;
            }

            if (removesession)
            {
                Session["counterOTP"] = null;
                Session["OgoneOrderResponse"] = null;
                Session.Remove("OgoneOrderResponse");
                Session["IsPaymentSuccess"] = "PAID";
            }
        }
        else
            linkURL = string.Empty;

        var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (st != null)
        {
            string SiteName = st.SiteURL + linkURL;
            ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + SiteName + "'; </script>");
        }
    }

    private string Success()
    {
        if (IsP2PPass)
            linkURL = "OrderSuccessPage?req=" + OrderNo;
        else if (Session["OrderID"] != null)
            linkURL = "OrderSuccessPage";
        else
            linkURL = string.Empty;

        if (!string.IsNullOrEmpty(OrderNo))
            new ManageBooking().UpdateOrderStatus(3, Convert.ToInt64(OrderNo));
        return linkURL;
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(_siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    #region Mail Me Order Status
    public static void CreateLog(string orderno, string result, string types)
    {
        try
        {
            db_1TrackEntities db = new db_1TrackEntities();
            db.tblPaymentGatwayLogs.AddObject(new tblPaymentGatwayLog
            {
                GateWayName = "Live: Payment Accepted " + types + " Response #" + orderno,
                OrderId = Convert.ToInt64(orderno),
                Request = "",
                Response = result,
                CratedeOn = DateTime.Now
            });
            db.SaveChanges();
        }
        catch (Exception ex)
        {
        }
    }
    #endregion
}