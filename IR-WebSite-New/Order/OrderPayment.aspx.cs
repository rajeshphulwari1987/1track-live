﻿using System;
using Business;
using System.Net.Mail;
using System.Net;
using OgoneIR;
using System.Web.Script.Serialization;
using System.Linq;

public partial class Order_OrderPayment : System.Web.UI.Page
{
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string currency = string.Empty;
    public string OrderID = string.Empty;
    public string linkURL = string.Empty;
    public string FormURL = string.Empty;
    public string CurrencyAmount = string.Empty;
    public string cssUrl = string.Empty;
    private Guid _siteId;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    private void HideimgAmx(string SiteId)
    {
        try
        {
            switch (SiteId)
            {
                case "79139cf1-8243-453e-849b-c3f7eb6004d6":
                    imgAmx.Visible = false;
                    break;
                case "5858f611-6726-4227-a417-52ccdea36b3d":
                    imgAmx.Visible = false;
                    break;
                case "79139CF1-8243-453E-849B-C3F7EB6004D6":
                    imgAmx.Visible = false;
                    break;
                case "F06549A9-139A-44F9-8BEE-A60DC5CC2E05":
                    imgAmx.Visible = false;
                    break;
                case "2E6D9824-1A7F-405E-9295-95ECA3937650":
                    imgAmx.Visible = false;
                    break;
                case "302D426F-6C40-4B71-A565-5D89D91C037E":
                    imgAmx.Visible = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HideimgAmx(_siteId.ToString());
        GetCurrencyCode();
        if (Request.Params["req"] != null)
        {
            OrderID = Request.Params["req"];
            linkURL = "PaymentAccepted.aspx?req=" + Request.Params["req"];
            FormURL = "OrderPayment.aspx?req=" + Request.Params["req"];
        }
        else if (Session["OrderID"] != null)
        {
            OrderID = Session["OrderID"].ToString();
            linkURL = "PaymentAccepted.aspx";
            FormURL = "OrderPayment.aspx";
        }

        if (Session["Amount"] != null)
            CurrencyAmount = currency + (Convert.ToDecimal(Session["Amount"].ToString()).ToString("F"));

        if (!IsPostBack)
            ShowMessage(0, null);

        #region for new IR & travel cuts
        ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();

        string siteURL = "../";
        if (Session["siteId"] != null)
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));

        if (siteURL.Contains("travelcuts") || _oWebsitePage.GetSiteLayoutType(_siteId) == 4)
            cssUrl = "<link href='" + siteURL + "assets/css/travelcuts.css' rel='stylesheet' type='text/css' />";
        else if (siteURL.Contains("travelcuts") || _oWebsitePage.GetSiteLayoutType(_siteId) == 8)
            cssUrl = "<link href='" + siteURL + "assets/css/omegatravel.css' rel='stylesheet' type='text/css' />";
        else if (siteURL.ToLower().Contains("irrail") || siteURL.ToLower().Contains("new.internationalrail") || _oWebsitePage.GetSiteLayoutType(_siteId) == 2 || _oWebsitePage.GetSiteLayoutType(_siteId) == 6)
            cssUrl = "<link href='" + siteURL + "assets/css/internationalrail.css' rel='stylesheet' type='text/css' />";
        else
            cssUrl = "<link href='" + siteURL + "assets/css/main.css' rel='stylesheet' type='text/css' />";

        #endregion

        if (Request.Url.AbsoluteUri.ToLower().Contains("au-public") || Request.Url.AbsoluteUri.ToLower().Contains("rail.statravel.com.au"))
            p_additionalCharges.Visible = true;
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            Guid siteId = Guid.Parse(Session["siteId"].ToString());
            Guid currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    public void GetOgoneResponse(OgoneOrderRequest OgoneRequest, string orderno, OgoneOrderResponse OgoneResponse)
    {
        try
        {
            string reqs = "";
            string ress = "";
            if (OgoneRequest != null)
                reqs = new JavaScriptSerializer().Serialize(OgoneRequest);
            if (OgoneResponse != null)
                ress = new JavaScriptSerializer().Serialize(OgoneResponse);

            db_1TrackEntities db = new db_1TrackEntities();
            if (OgoneResponse != null && (OgoneResponse.STATUS == TransactionResponseStatus.Incomplete_or_invalid || OgoneResponse.STATUS == TransactionResponseStatus.Cancelled_by_client || OgoneResponse.STATUS == TransactionResponseStatus.Authorization_refused))
            {
                long OrderId = Convert.ToInt64(orderno);
                var orderDetails = db.tblOrders.FirstOrDefault(x => x.OrderID == OrderId);
                if (orderDetails != null && orderDetails.Status != 3)
                {
                    orderDetails.Status = GetStatus(OgoneResponse);
                    db.SaveChanges();
                }
            }

            db.tblPaymentGatwayLogs.AddObject(new tblPaymentGatwayLog
            {
                GateWayName = "Live: Payment Query String for " + orderno,
                OrderId = Convert.ToInt64(orderno),
                Request = reqs,
                Response = ress,
                CratedeOn = DateTime.Now
            });
            db.SaveChanges();
        }
        catch (Exception ex)
        {
        }
    }

    public int GetStatus(OgoneOrderResponse res)
    {
        int newStatus = 1;
        if (res.STATUS == TransactionResponseStatus.Incomplete_or_invalid || res.STATUS == TransactionResponseStatus.Cancelled_by_client)
            newStatus = 23;  //Incomplete
        else if (res.STATUS == TransactionResponseStatus.Authorization_refused)
            newStatus = 18;  //Declined
        return newStatus;
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}