﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorldPayPaymentConfirmation.ascx.cs"
    Inherits="Order_WorldPayPaymentConfirmation" %>
<asp:Panel ID="pnlErrSuccess" runat="server">
    <div id="DivSuccess" runat="server" class="success" style="display: none;">
        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
    <div id="DivError" runat="server" class="error" style="display: none;">
        <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
    </div>
</asp:Panel>
<iframe id="PaymentFrame" style="width: 100%; height: 605px !important;" frameborder="0"
    scrolling="no" runat="server" clientidmode="Static"></iframe>
