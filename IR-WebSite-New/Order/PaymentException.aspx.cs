﻿using System;
using System.Linq;
using Business;
using System.Net.Mail;
using System.Net;

public partial class Order_PaymentException : System.Web.UI.Page
{
    private Guid _siteId;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string OrderNo = string.Empty;
    string SiteName = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Emailsend();
        if (Request.Params["req"] != null)
            OrderNo = Request.Params["req"];
        else if (Session["OrderID"] != null)
            OrderNo = Session["OrderID"].ToString();
        else if (Session["P2POrderID"] != null)
            OrderNo = Session["P2POrderID"].ToString();

        new ManageBooking().UpdateOrderStatus(6, Convert.ToInt64(OrderNo));
        var st = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (st != null)
        {
            if (Request.QueryString["paymentStatus"] != null)
            {
                SiteName = st.SiteURL + "PaymentProcess?req=" + OrderNo;
                Response.Redirect(Session["WorldPayResponseUrl"].ToString());
                ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + SiteName + "'; </script>");
            }
            else
            {
                SiteName = st.SiteURL + "Order/PaymentConfirmation.aspx";
                ClientScript.RegisterStartupScript(GetType(), "Loadnew", "<script type='text/javascript'>window.parent.location.href = '" + SiteName + "'; </script>");
            }
        }
    }
    public void Emailsend()
    {
        var smtpClient = new SmtpClient();
        var message = new MailMessage();
        var fromAddres = new MailAddress("admin@1tracktest.com", "admin@1tracktest.com");
        smtpClient.Host = "mail.dotsquares.com";
        smtpClient.Port = 587;
        smtpClient.UseDefaultCredentials = true;
        smtpClient.Credentials = new NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
        smtpClient.EnableSsl = false;
        message.From = fromAddres;
        message.To.Add("rajeshkumar.phulwari@dotsquares.com");
        message.Bcc.Add("dipu.bharti@dotsquares.com");
        message.Subject = "Demo: Payment status 46 feedback on accept";
        message.IsBodyHtml = true;
        message.Body = "Live: Payment status 46 feedback in [exception] on accept stirng::::::::\n " + Request.QueryString;
        smtpClient.Send(message);
    }
}