﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

public partial class Order_PaymentConfirmation : System.Web.UI.UserControl
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private Guid _siteId;
    public string OrderNo = string.Empty;
    public string linkURL = string.Empty;
    public string sid = string.Empty;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            _siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var objsite = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (objsite != null)
        {
            if (objsite.PaymentType != 1)
                return;
        }

        if (Request.Params["req"] != null)
        {
            OrderNo = Request.Params["req"];
            linkURL = "Order/OrderPayment.aspx?req=" + OrderNo;
        }
        else if (Session["OrderID"] != null)
        {
            OrderNo = Session["OrderID"].ToString();
            linkURL = "Order/OrderPayment.aspx";
        }

        #region OgoneSID
        var OgoneSetting = _db.tblOgoneMsts.Where(x => x.IsActive == true).FirstOrDefault(x => x.SiteID == _siteId);
        if (OgoneSetting != null)
            sid = CalculateMD5Hash(OgoneSetting.PSPID + OrderNo.ToString());
        #endregion

        if (string.IsNullOrEmpty(OrderNo))
        {
            Response.Redirect("~/Home");
        }
        setOrderData();
    }

    public void setOrderData()
    {
        ManageBooking objB = new ManageBooking();
        List<GetAllCartData_Result> lst = objB.GetAllCartData(Convert.ToInt64(OrderNo));
        if (lst.Count() > 0)
        {
            PaymentGateWayTransffer objPT = Session["PayObj"] as PaymentGateWayTransffer;
            if (objPT == null)
                return;
            var data = lst.FirstOrDefault(); 
            var tblcountry = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName == data.DCountry);
            string IsoCode = tblcountry != null ? tblcountry.IsoCode : data.DCountry;
            Session["CustomerData"] = data.Address1 + " " + data.Address2 + ";" + data.City + ";" + data.Postcode + ";" + IsoCode + ";" + data.EmailAddress;
            Guid SiteCurr = _db.tblSites.FirstOrDefault(x=>x.ID==data.SiteID).DefaultCurrencyID.Value;
            Session["currencyCode"] =   _db.tblCurrencyMsts.FirstOrDefault(a => a.ID == SiteCurr).ShortCode;
        }
    }

    public string CalculateMD5Hash(string input)
    {
        MD5 md5 = new MD5CryptoServiceProvider();

        //compute hash from the bytes of text
        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(input));

        //get hash result after compute it
        byte[] result = md5.Hash;

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < result.Length; i++)
        {
            //change it into 2 hexadecimal digits
            //for each byte
            strBuilder.Append(result[i].ToString("x2"));
        }

        return strBuilder.ToString();
    }
}
