﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business;
using System.Configuration;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Xml;
using System.Web.UI;

public partial class Order_WorldPayPaymentConfirmation : System.Web.UI.UserControl
{
    Masters MPage = new Masters();
    ManageBooking MBooking = new ManageBooking();
    ManageOrder MOrder = new ManageOrder();
    String BaseUrl = "";
    String MerchantCode = "";
    String Password = "";
    String InstallationID = "";
    Guid SiteId = Guid.Empty;
    String OrderNo = "";
    Int32 Amount = 0;
    String SalCurrency = "";
    String Address1 = "";
    String Address2 = "";
    String PostCode = "";
    String City = "";
    String IsoCode = "";
    String ShopperEmailAddress = "";
    String State = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            SiteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["OrderID"] != null)
            OrderNo = Session["OrderID"].ToString();
        else if (Session["P2POrderID"] != null)
            OrderNo = Session["P2POrderID"].ToString();
        if (string.IsNullOrEmpty(OrderNo))
            Response.Redirect("~/Home");

        if (!IsPostBack)
            if (Session["Amount"] != null)
            {
                Guid DefaultCurrency = MPage.GetSiteListEdit(SiteId).DefaultCurrencyID.Value;
                var currency = MPage.GetCurrencyListEdit(DefaultCurrency).Symbol;
            }
        PaymentProcess();
    }

    private void GetWorldpayCredential()
    {
        try
        {
            var objWorld = MPage.GetWorldPayDetails(SiteId);
            if (objWorld != null)
            {
                BaseUrl = objWorld.WorldPayURL;
                MerchantCode = objWorld.MerchantCode;
                Password = objWorld.Password;
                InstallationID = objWorld.InstallationId;
            }
            else
            {
                ShowMessage(2, "Payment credential not found.");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void SetOrderData()
    {
        try
        {
            if (Session["Amount"] == null)
                ShowMessage(2, "Payment is not valid.");

            List<GetAllCartData_Result> lst = MBooking.GetAllCartData(Convert.ToInt64(OrderNo));
            if (lst.Count() > 0)
            {
                var FristData = lst.FirstOrDefault();
                IsoCode = MPage.GetCountryList().FirstOrDefault(x => x.CountryName == FristData.DCountry).IsoCode;
                Address1 = FristData.Address1;
                Address2 = FristData.Address2;
                City = FristData.City;
                PostCode = FristData.Postcode;
                ShopperEmailAddress = FristData.EmailAddress;
                Guid DefaultCurrency = MPage.GetSiteListEdit(SiteId).DefaultCurrencyID.Value;
                SalCurrency = MPage.GetCurrencyListEdit(DefaultCurrency).ShortCode;
                Amount = Convert.ToInt32(Convert.ToDecimal(Session["Amount"]) * 100);
                State = FristData.State;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void PaymentProcess()
    {
        try
        {
            String DescriptionText = "Rail pass(es) or ticket(s)";
            SetOrderData();
            GetWorldpayCredential();
            string xmldata = @"<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE paymentService PUBLIC '-//WorldPay//DTD WorldPay PaymentService v1//EN' 'http://dtd.worldpay.com/paymentService_v1.dtd'>" +
            "<paymentService version='1.4' merchantCode='" + MerchantCode + "'>" +
                "<submit>" +
                    "<order orderCode='" + OrderNo + "' installationId='" + InstallationID + "'>" +
                        "<description>" + DescriptionText + "</description>" +
                        "<amount value='" + Amount + "' currencyCode='" + SalCurrency + "' exponent='2' debitCreditIndicator='credit' />" +
                        "<paymentMethodMask>" +
                            "<include code='All' />" +
                        "</paymentMethodMask>" +
                        "<shopper>" +
                            "<shopperEmailAddress>" + ShopperEmailAddress + "</shopperEmailAddress>" +
                        "</shopper>" +
                        "<shippingAddress>" +
                            "<address> " +
                                "<address1>" + Address1 + "</address1>" +
                                "<address2>" + Address2 + "</address2>" +
                                "<postalCode>" + PostCode + "</postalCode>" +
                                "<city>" + City + "</city>" +
                                "<state>" + State + "</state>" +
                                "<countryCode>" + IsoCode + "</countryCode>" +
                            "</address>" +
                        "</shippingAddress>" +
                    "</order>" +
                "</submit>" +
            "</paymentService>";

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)SPType.Tls | (SecurityProtocolType)SPType.Tls11 | (SecurityProtocolType)SPType.Tls12;
            var credential = new NetworkCredential(MerchantCode, Password);
            HttpClientHandler handler = new HttpClientHandler { Credentials = credential };
            HttpClient client = new HttpClient(handler);
            client.BaseAddress = new System.Uri(BaseUrl);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/xml"));
            System.Net.Http.HttpContent content = new StringContent(xmldata, UTF8Encoding.UTF8, "text/xml");
            HttpResponseMessage ResponseMessage = client.PostAsync(BaseUrl, content).Result;

            ClsErrorLog.AddError("Worldpay Request(1):: " + OrderNo + ": : " + Request.Url.ToString(), xmldata);
            if (ResponseMessage.IsSuccessStatusCode)
            {
                string Result = ResponseMessage.Content.ReadAsStringAsync().Result;
                ClsErrorLog.AddError("Worldpay Response(1):: " + OrderNo + ": : " + Request.Url.ToString(), Result);
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(Result);
                XmlNode node = null;
                XmlNode error = null;
                node = xDoc.SelectSingleNode("paymentService/reply/orderStatus");
                error = xDoc.SelectSingleNode("paymentService/reply/error");

                String SiteUrl = MPage.GetSiteListEdit(SiteId).SiteURL;
                string failureURL = SiteUrl + "Order/WPRedirect.aspx?status=Failure";
                string cancelURL = SiteUrl + "Order/WPRedirect.aspx?status=Cancel";
                string successURL = SiteUrl + "Order/WPRedirect.aspx?status=Success";
                string errorURL = SiteUrl + "Order/WPRedirect.aspx?status=Error";
                string pendingURL = SiteUrl + "Order/WPRedirect.aspx?status=Pending";
                string URL = "&country=" + IsoCode + "&language=en" + "&successURL=" + successURL + "&failureURL=" + failureURL + "&cancelURL=" + cancelURL + "&errorURL=" + errorURL + "&pendingURL=" + pendingURL;
                if (node != null && !string.IsNullOrEmpty(node.InnerText))
                    URL = node.InnerText + URL;
                ClsErrorLog.AddError("Worldpay post URL Request(2):: " + OrderNo + ": : " + Request.Url.ToString(), URL);
                if (error != null && !string.IsNullOrEmpty(error.InnerText))
                    ShowMessage(2, error.InnerText);
                else
                    Response.Redirect(URL);
            }
            else
            {
                ShowMessage(2, "Sorry! payment gateway not working");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

}