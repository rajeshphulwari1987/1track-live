﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WPRedirect.aspx.cs" Inherits="Order_WPRedirect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="OopsPage/css/oops-style.css" rel="stylesheet" />

    <title>:: Oops! ::</title>

</head>
<body>
    <div class="oops-page">
        <div class="table-view">
            <div class="table-cell-view">
                <div class="op-wrapper">
                    <img src="OopsPage/images/Icon.svg" alt="Logo">

                    <h1 class="red-text">Sorry, unable to process payment</h1>
                    <p class="red-text" runat="server" id="lblmsg"></p>
                    <span class="red-text">Session expired due to inactivity</span>
                    <span class="red-text">You double-clicked on a payment button</span>
                    <span class="red-text">Our system encountered an obstacle</span>

                    <div class="ctc-btn">
                        <a href="/" class="bordered-btn" runat="server" id="Cancelpayment" visible="false">Back to home</a>
                        <a href="/PaymentProcess" class="green-btn" runat="server" visible="false" id="Continuepayment">Retry payment</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
