﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderPayment.aspx.cs" Inherits="Order_OrderPayment" %>

<%@ Import Namespace="OgoneIR" %>
<%@ Import Namespace="Business" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Net.Mail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.hammer.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.mmenu-partner.js" type="text/javascript"></script>
    <script src="../assets/js/main.min.js" type="text/javascript"></script>
    <script src="../assets/js/main.min.js.map" type="text/javascript"></script>
    <script src="../assets/js/partner.js" type="text/javascript"></script>
    <link href="../assets/css/partner.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/layout.css" rel="stylesheet" type="text/css" />
    <%=cssUrl%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#pleasewait").hide();
        });
        function redirect() {
            window.parent.location.href = '<%=linkURL %>';
        }
        function redirectdirect() {
            window.location.href = '<%=linkURL %>';
        }
        function checkValidattions() {
            var flag = 1;
            if ($("#cardnumber").val() == 0) {
                alert('Please enter card number.');
                flag = 0;
                return false;
            }
            else {
                if ($("#cardnumber").val().length < 15) {
                    alert('Card number not valid.');
                    flag = 0;
                    return false;
                }
                if (isNaN($("#cardnumber").val())) {
                    alert('Card number not valid.');
                    flag = 0;
                    return false;
                }
            }
            if ($("#cardname").val() == 0) {
                alert('Please enter card holder name.');
                flag = 0;
                return false;
            }

            if ($("#cvc").val() == 0) {
                alert('Please enter card verification code.');
                flag = 0;
                return false;
            }
            var currentYear = (new Date).getFullYear();
            var currentMonth = (new Date).getMonth() + 1;
            var sm = $("#expiremonth").val();
            var sy = $("#expireyear").val();
            if (sm == 0 || sy == 0) {
                alert('Please select expiry date.');
                flag = 0;
                return false;
            }
            if (sy == currentYear) {
                if (parseInt(sm) < parseInt(currentMonth)) {
                    alert('Invalid expiry date.');
                    flag = 0;
                    return false;
                }
            }
            if (!document.getElementById("chkTerms").checked) {
                alert('Please select Terms and Conditions.');
                flag = 0;
                return false;
            }
            if (flag == 0) {
                return false;
            }
            else {
                $("#pleasewait").show().addClass("showpleasewait");
            }
        }
    </script>
    <style type="text/css">
        .date-select {
            line-height: 34px;
            width: 44%;
        }

        .showpleasewait {
            display: inline-table !important;
            width: 100%;
            line-height: 50px;
            font-size: 16px;
            position: absolute;
            z-index: 999;
        }

        @media (max-width: 375px) {
            .iphone6 {
                width: 62%;
            }

            .manage_select {
                width: 100% !important;
            }
        }
    </style>
</head>
<body>
    <%
        try
        {
            if (!string.IsNullOrEmpty(OrderID))
                OrderID = OrderID;
            else
                Response.Write("<script type='text/javascript'>redirect();</script>");
            string cardtype = !string.IsNullOrEmpty(Request["cardtype"]) ? Request["cardtype"] : "";
            string cardnumber = !string.IsNullOrEmpty(Request["cardnumber"]) ? Request["cardnumber"] : "";
            string cardname = !string.IsNullOrEmpty(Request["cardname"]) ? Request["cardname"] : "";
            string expiremonth = !string.IsNullOrEmpty(Request["expiremonth"]) ? Request["expiremonth"] : "";
            string expireyear = !string.IsNullOrEmpty(Request["expireyear"]) ? Request["expireyear"] : "";
            string cvc = !string.IsNullOrEmpty(Request["cvc"]) ? Request["cvc"] : "";
            string address = "";
            string address2 = "";
            string town = "";
            string postcode = "";
            string country = "";
            string email = "";
            int amount = 0;
            if (Session["CustomerData"] != null)
            {
                string sessionStr = Session["CustomerData"].ToString();
                string[] str = sessionStr.Split(';');
                address = str[0];
                town = str[1];
                postcode = str[2];
                country = str[3];
                email = str[4];
            }
            if (Session["Amount"] != null)
                amount = Convert.ToInt32(Convert.ToDecimal(Session["Amount"].ToString()) * 100);
            if (!string.IsNullOrEmpty(Request["submitBtn"]))
            {
                string ProductName = "Rail Pass";
                string expirydate = ("0" + expiremonth).Substring(expiremonth.Length - 1, 2) + '/' + expireyear.Substring(2, 2);
                string fulladdressline = address;
                if (address2 != "")
                {
                    fulladdressline = fulladdressline + " " + address2;
                }

                string OgoneID = "";
                string OgoneUserName = "";
                string OgonePassword = "";
                string OgoneURL = "";
                string OgoneShaInPassPhrase = "";
                string currency = "";
                string OgoneReturnURL = "";
                string OgoneDeclineURL = "";
                string OgoneCancelURL = "";
                string OgoneLanguage = "";
                bool EnabaleThreeDPayment = false;
                db_1TrackEntities _db = new db_1TrackEntities();
                if (Session["siteId"] != null)
                {
                    Guid siteid = Guid.Parse(Session["siteId"].ToString());
                    var OgoneSetting = _db.tblOgoneMsts.FirstOrDefault(x => x.IsActive == true && x.SiteID == siteid);
                    if (OgoneSetting != null)
                    {
                        EnabaleThreeDPayment = OgoneSetting.IsEnableThreeD;
                        OgoneID = OgoneSetting.OgoneID.Trim();
                        OgoneUserName = OgoneSetting.UserName.Trim();
                        OgonePassword = OgoneSetting.Password.Trim();
                        OgoneURL = OgoneSetting.OgoneURL.Trim();
                        OgoneDeclineURL = OgoneSetting.ReturnURLDecline.Trim();
                        OgoneCancelURL = OgoneSetting.ReturnURLException.Trim();
                        OgoneReturnURL = OgoneSetting.ReturnURLAccept.Trim();
                        OgoneShaInPassPhrase = OgoneSetting.OgoneShaInPassPhrase.Trim();
                        OgoneLanguage = OgoneSetting.Language.Trim();
                        currency = OgoneSetting.Currency.Trim();
                    }
                }
                OgoneOrderRequest objOrderRequest = new OgoneOrderRequest(OgoneID, OgoneUserName, OgonePassword, OgoneURL, OgoneShaInPassPhrase);
                OgoneOrderResponse objOrderResponse = new OgoneOrderResponse();

                //Mandatory
                objOrderRequest.ORDERID = "InternationalRail" + OrderID;
                objOrderRequest.AMOUNT = amount;
                objOrderRequest.CURRENCY = currency;
                objOrderRequest.CARDNO = cardnumber.Trim();
                objOrderRequest.ED = expirydate;
                objOrderRequest.CVC = cvc;
                objOrderRequest.OPERATION = TransactionType.SAL;

                //Optional
                objOrderRequest.COM = ProductName;
                objOrderRequest.CN = cardname;
                objOrderRequest.EMAIL = email;
                objOrderRequest.ECOM_PAYMENT_CARD_VERIFICATION = "";
                objOrderRequest.OWNERADDRESS = fulladdressline;
                objOrderRequest.OWNERZIP = postcode;
                objOrderRequest.OWNERTOWN = town;
                objOrderRequest.OWNERCTY = country;
                objOrderRequest.OWNERTELNO = "";
                objOrderRequest.GLOBORDERID = "";
                objOrderRequest.WITHROOT = "";
                objOrderRequest.REMOTE_ADDR = Request.ServerVariables["REMOTE_ADDR"];
                objOrderRequest.RTIMEOUT = 30;
                objOrderRequest.ECI = ElectronicCommerceIndicator.ECommerceWithSSLencryption;
                string strParams = "SessionID=" + this.Session.SessionID;
                string strComPlus = objOrderRequest.ORDERID;
                string returnURLAccept = OgoneReturnURL;
                objOrderRequest.Set3DSecureParameters(Window3DPageType.MAINW, Request.ServerVariables["HTTP_ACCEPT"], Request.ServerVariables["HTTP_USER_AGENT"], returnURLAccept, OgoneReturnURL, OgoneCancelURL, strParams, strComPlus, OgoneLanguage, EnabaleThreeDPayment);
                objOrderResponse = objOrderRequest.GenerateOrderRequest();
                Session["OgoneOrderResponse"] = objOrderResponse;

                long OId = Convert.ToInt64(OrderID);
                var updateorderpayment = _db.tblOrders.FirstOrDefault(x => x.OrderID == OId);
                if (updateorderpayment != null)
                {
                    updateorderpayment.CardNumber = cardnumber;
                    updateorderpayment.ExpDate = expirydate;
                    updateorderpayment.CardholderName = cardname;
                    _db.SaveChanges();
                }

                /*3D payment enable*/
                if (objOrderResponse.RequiresCVVCheck)
                    Response.Write(objOrderResponse.HTML_ANSWER);

                if (!string.IsNullOrEmpty(cardnumber))
                {
                    GetOgoneResponse(objOrderRequest, OrderID, objOrderResponse);
                    /*Driect payment method*/
                    if (objOrderResponse.STATUS == TransactionResponseStatus.Payment_requested)
                    {
                        var orderDetails = _db.tblOrders.FirstOrDefault(x => x.OrderID == OId);
                        if (orderDetails != null)
                        {
                            new ManageBooking().UpdateOrderStatusLog(3, OId);
                            orderDetails.PaymentId = objOrderResponse.PAYID;
                            orderDetails.Status = 3;
                            _db.SaveChanges();
                        }
                        if (!objOrderResponse.RequiresCVVCheck)
                            Response.Write("<script type='text/javascript'>redirectdirect();</script>");
                        Response.Write("<script type='text/javascript'>redirect();</script>");
                    }
                    else if (!objOrderResponse.RequiresCVVCheck)
                    {
                        ShowMessage(2, "Payment has been declined or invalid details.");
                        if (objOrderResponse.STATUS == TransactionResponseStatus.Incomplete_or_invalid || objOrderResponse.STATUS == TransactionResponseStatus.Cancelled_by_client || objOrderResponse.STATUS == TransactionResponseStatus.Authorization_refused)
                        {
                            var orderDetails = _db.tblOrders.FirstOrDefault(x => x.OrderID == OId);
                            if (orderDetails != null && orderDetails.Status != 3)
                            {
                                orderDetails.Status = GetStatus(objOrderResponse);
                                new ManageBooking().UpdateOrderStatusLog(orderDetails.Status, OId);
                                _db.SaveChanges();
                            }
                        }
                    }
                }
            } 
    %>
    <form action='<%=FormURL %>' method="post" id="payment" name="payment" class="starail-Form--paymentConfirmation starail-Wrapper">
        <input type="hidden" name="id" value="<%=OrderID%>" />
        <% if (!string.IsNullOrEmpty(OrderID))
           { %>
        <input type="hidden" name="OrderId" id="OrderId" value="<%=OrderID%>" />
        <%}
           else
           {
               Response.Write("<script type='text/javascript'>redirect();</script>");
           } %>
        <div class="starail-BookingDetails-form  width-98-payment">
            <div class="starail-Form-row">
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
            </div>
            <h2>Payment Details</h2>
            <div class="starail-Form-row starail-Form-row--card">
                <label for="starail-cardnumber" class="starail-Form-label">
                    Credit card number <span class="starail-Form-required">*</span> <span class="starail-Form-note starail-Form-note--block">(the long one across the front)</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid car_number">
                    <div>
                        <input name="cardnumber" type="text" id="cardnumber" maxlength="16" autocomplete="off"
                            value="<%= cardnumber %>" class="starail-Form-input starail-Form-input--small iphone6" />
                        <img src="../assets/img/icons/visa.png" alt="Visa Logo" class="starail-Form-row--card-first" />
                        <img src="../assets/img/icons/mastercard.png" alt="Mastercard Logo" class="starail-Form-row--card-second" />
                        <img src="../assets/img/icons/amx.png" alt="Mastercard Logo" class="starail-Form-row--card-second"
                            runat="server" id="imgAmx" visible="false" style="height: 25px; padding-left: 50px;" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-cardname" class="starail-Form-label">
                    Cardholder name <span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid card_holder">
                    <div>
                        <input name="cardname" type="text" id="cardname" class="starail-Form-input starail-Form-input--small"
                            maxlength="100" value="<%= cardname %>" autocomplete="off" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-expiryfirst" class="starail-Form-label">
                    Expiry date (MM/YYYY) <span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer">
                    <div class="starail-Form-inputGroup starail-Form-inputGroup--twoMobile manage_select">
                        <select name="expiremonth" id="expiremonth" class="starail-Form-input date-select">
                            <option selected="selected" value="0">MM</option>
                            <%
            for (int count = 1; count < 13; count++)
            {
                            %><option <%= (count.ToString()==expiremonth)?"selected=\"selected\"":"" %> value="<%= count %>">
                                <%= count%></option>
                            <%
            }
                            %>
                        </select>
                        <select name="expireyear" id="expireyear" class="starail-Form-input date-select">
                            <option selected="selected" value="0">YYYY</option>
                            <%
            for (int count = DateTime.Today.Year; count < (DateTime.Today.Year + 8); count++)
            {
                            %><option <%= (count.ToString()==expireyear)?"selected=\"selected\"":"" %> value="<%= count %>">
                                <%= count%></option>
                            <%
            }
                            %>
                        </select>
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-verificationcode" class="starail-Form-label">
                    Card verification code <span class="starail-Form-required">*</span> <span class="starail-Form-note starail-Form-note--block">(the last three digits on the back)</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <div>
                        <input name="cvc" type="text" id="cvc" class="starail-Form-input starail-Form-input--small"
                            maxlength="4" autocomplete="off" />
                    </div>
                </div>
            </div>
            <div class="starail-Form-row">
                <div class="starail-Form-label">
                    &nbsp;
                </div>
                <div class="starail-Form-inputContainer-col" style="display: inline;">
                    <label>
                        <span class="starail-Form-fancyCheckbox">
                            <input type="checkbox" id="chkTerms" name="starail-tc" />
                            <span><i class="starail-Icon-tick"></i></span><a onclick="ShowTermAndCondition()"
                                class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">I accept the terms
                            and conditions and confirm all traveller(s) are eligible for pass(es) purchased.</a>
                            <p id="p_additionalCharges" runat="server" visible="false">
                                Your card issuer may levy additional charges when making purchases through this
                            site, STA Travel are not liable for any charges levied by your card issuer and these
                            cannot be refunded by STA Travel. If you would prefer to complete your purchase
                            through one of our Travel Experts please call 134 782
                            </p>
                        </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="mobile-button-div starail-u-cf paddingnone">
            <div id="pleasewait" style="display: none;">
                <span style="padding-left: 28%;">Please Wait...</span>
            </div>
            <input type="submit" name="submitBtn" value="Confirm payment of <%=CurrencyAmount %>"
                id="submitBtn" class="starail-Button starail-Button--fullMobile starail-Button--rightSubmit fontnone"
                onclick="return checkValidattions();" />
        </div>
    </form>
    <%  }
        catch (Exception ex)
        {
            ShowMessage(2, "Operation failed. Please try after some time");
        } %>
    <script type="text/javascript">
        function ShowTermAndCondition() {
            parent.$(parent.document).trigger('eventhandler');
        }
    </script>
</body>
</html>
