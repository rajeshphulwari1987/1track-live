﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Xml;

public partial class P2PBookingCart : Page
{
    #region Global Variables
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    readonly ManageAdminFee ManageAdmfee = new ManageAdminFee();
    private readonly Common _Common = new Common();
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private double _total = 0;
    readonly ManageUser _ManageUser = new ManageUser();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    ManageTrainDetails _master = new ManageTrainDetails();
    ManageBooking _masterBooking = new ManageBooking();
    public static string currency = "$";
    public static Guid currencyID = new Guid();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
    Guid pageID, siteId;
    public string script = "<script></script>";
    public long P2POrderId = 0;
    public string siteURL;
    public string ChkOutActionsTags = string.Empty;
    public string ChkOutSetActionsTags = string.Empty;
    public string SiteUrl = string.Empty;
    public bool isEvolviBooking = false;
    BookingRequestUserControl objBRUC;
    ManageOneHub _ManageOneHub = new ManageOneHub();
    public bool isRegionalBooking = false;
    public bool IsTIBookingPassHolder = false;
    public bool IsStaSite = false;
    public string MandatoryTextFirst = "", MandatoryTextFirstSecond = "";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GetCurrencyCode();
        ucTicketDelivery.ddlCountryMailHandler += new OtherSiteP2PBooking_ucTicketDelivery.OnSelectedIndexChanged(DeliveryByMailCountry);
        ucTicketDelivery.rdoBkkoingListHandler += new OtherSiteP2PBooking_ucTicketDelivery.OnSelectedIndexChanged(DeliveryByMailRadioButton);

        if (Session["BookingUCRerq"] != null)
        {
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            if (objBRUC != null)
                isEvolviBooking = !string.IsNullOrEmpty(objBRUC.OneHubServiceName) ? objBRUC.OneHubServiceName == "Evolvi" ? true : false : false;
        }

        isRegionalBooking = Session["TIRegionalBooking"] != null;
        if (Session["P2POrderID"] != null)
            P2POrderId = Convert.ToInt64(Session["P2POrderID"]);

        if (Request.QueryString["req"] != null)
        {
            if (Request.QueryString["req"] == "IT")
            {
                Session["TrainType"] = "IT";
                pnlShipping.Visible = false;
                ucTicketDelivery.Visible = false;
                div_DeliverByDate_TI.Visible = true;
                div_DeliverByDate_Bene.Visible = false;
            }
            else if (Request.QueryString["req"] == "EV")
            {
                Session["TrainType"] = "EV";
                pnlShipping.Visible = false;
                ucTicketDelivery.Visible = false;
                div_DeliverByDate_TI.Visible = false;
                div_DeliverByDate_Bene.Visible = false;
            }
            else if (Request.QueryString["req"] == "NTV")
            {
                Session["TrainType"] = "NTV";
                pnlShipping.Visible = false;
                ucTicketDelivery.Visible = false;
            }
            else
                Session["TrainType"] = "BE";
        }
        ShowMessage(0, string.Empty);
        if (!IsPostBack)
        {
            if (Request.QueryString["req"] == "IT")
            {
                ShowHideTrenItaliaPassengerDiv();
                div_chkShippingfill.Visible = true;
                hdnIsTI.Value = "True";
            }
            else if (Request.QueryString["req"] == "EV")
                div_chkShippingfill.Visible = false;
            else if (Request.QueryString["req"] == "NTV")
                div_chkShippingfill.Visible = true;
            else
                div_chkShippingfill.Visible = false;

            BindPageMandatoryFields();
            Session["BOOKING-REPLY"] = null;
            PageLoadEvent();
            QubitOperationLoad();
            GetDetailsAnalyticTags();
            GetAdminFeeData();
            EvolviDefaultCountryShow();
            if (isEvolviBooking)
                GetEvolviFareInfoList();
            if (hdnDeliveryMethodValue.Value != "TA" && !string.IsNullOrEmpty(hdnDeliveryMethodValue.Value))
                FillShippingData();
            if (AgentuserInfo.UserID != Guid.Empty)
            {
                var Agentlist = _ManageUser.AgentDetailsById(AgentuserInfo.UserID);
                var AgentNameAndEmail = _ManageUser.AgentNameEmailById(AgentuserInfo.UserID);
                if (Agentlist != null && AgentNameAndEmail != null)
                {
                    ddlMr.SelectedValue = Convert.ToInt32(AgentNameAndEmail.Salutation).ToString();
                    txtFirst.Text = AgentNameAndEmail.Forename;
                    txtLast.Text = AgentNameAndEmail.Surname;
                    txtEmail.Text = AgentNameAndEmail.EmailAddress;
                    //ddlCountry.SelectedValue = Convert.ToString(Agentlist.Country);
                    txtCity.Text = Agentlist.Town;
                    txtAdd.Text = Agentlist.Address1;
                    txtAdd2.Text = Agentlist.Address2;
                    txtZip.Text = Agentlist.Postcode;
                    txtBillPhone.Text = Agentlist.Telephone;
                }
            }
            else if (USERuserInfo.ID != Guid.Empty)
            {
                var UserData = _db.tblUserLogins.FirstOrDefault(x => x.ID == USERuserInfo.ID);
                if (UserData != null)
                {
                    txtFirst.Text = UserData.FirstName;
                    txtLast.Text = UserData.LastName;
                    txtEmail.Text = UserData.Email;
                    txtEmail.Attributes.Add("disabled", "disabled");
                    txtEmail.Attributes.Add("style", "text-transform: lowercase;background: #ccc;");
                    //ddlCountry.SelectedValue = UserData.Country.ToString();
                    txtCity.Text = UserData.City;
                    txtState.Text = UserData.State;
                    txtAdd.Text = UserData.Address;
                    txtAdd2.Text = UserData.Address2;
                    txtZip.Text = UserData.PostCode;
                    txtBillPhone.Text = UserData.Phone;
                }
            }
            ShowHideTermsMandatoryText(siteId);
            FillP2PDefaultCountry(siteId);
        }
    }

    public void GetAdminFeeData()
    {
        try
        {
            hdnAdminFee.Value = ManageAdmfee.GetAdminFeeBySiteID(siteId);
            if (string.IsNullOrEmpty(hdnAdminFee.Value))
                pnladminfee.Attributes.Add("style", "display:none");
            else
                pnladminfee.Attributes.Add("style", "display:block");
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetDetailsAnalyticTags()
    {
        ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
        var data = _masterAnalyticTag.GetTagBySiteId(siteId);
        if (data != null)
        {
            if (Session["GaTaggingProductlist"] != null)
            {
                var list = Session["GaTaggingProductlist"] as List<ProductPass>;
                string obj = "";
                #region /*ChkOutActions*/
                string[] SplitData = data.ChkOutActions.Split(';');
                string ChkOut = "";
                foreach (var item in list)
                {
                    ChkOut = SplitData[0].Replace("<Product Code>", "" + item.ProductCode + "");
                    ChkOut = ChkOut.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                    ChkOut = ChkOut.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                    ChkOut = ChkOut.Replace("<Price>", "" + item.Price + "");
                    ChkOut = ChkOut.Replace("<Quantity>", "1");
                    obj += ChkOut + ";\n";
                    ChkOut = "";
                }
                if (SplitData.Count() > 1)
                    foreach (var item in list)
                    {
                        ChkOut = SplitData[1].Replace("<Product Code>", "" + item.ProductCode + "");
                        ChkOut = ChkOut.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                        ChkOut = ChkOut.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                        ChkOut = ChkOut.Replace("<Price>", "" + item.Price + "");
                        ChkOut = ChkOut.Replace("<Quantity>", "1");
                        obj += ChkOut + ";\n";
                        ChkOut = "";
                    }
                ChkOutActionsTags = "\n<script type='text/javascript'>\n/*ChkOutActions*/\n" + obj + "\n</script>\n";
                ChkOutSetActionsTags = "\n<script type='text/javascript'>\n/*ChkOutSetActions*/\n" + data.ChkOutSetActions.Replace("<Step Number>", "'1'") + "\n</script>\n";
                #endregion
            }
        }
    }

    public void BindPageMandatoryFields()
    {
        try
        {
            var pid = "AFC79F98-9D7B-4FC2-AA1C-2BAFD83A43A7";
            pageID = (Guid.Parse(pid));
            var list = _masterPage.GetMandatoryVal(siteId, pageID);

            foreach (var item in list)
            {
                if (item.ControlField == "rfFirst")
                    reqvalerrorBA0.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfLast")
                    reqvalerrorBA1.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfEmail")
                    reqvalerrorBA2.ValidationGroup = item.ValGrp;


                if (item.ControlField == "rfPhone")
                    reqvalerrorBA7.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfAdd")
                    reqvalerrorBA8.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfZip")
                    reqvalerrorBA9.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfCountry")
                    reqvalerrorBA10.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfDateOfDepature")
                    reqvalerrorDPdATE.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshipFirstName")
                    reqvalerrorSA0.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshipLastName")
                    reqvalerrorSA1.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshipEmail")
                    reqvalerrorSA2.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpPhone")
                    reqvalerrorSA7.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpAdd")
                    reqvalerrorSA8.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpZip")
                    reqvalerrorSA9.ValidationGroup = item.ValGrp;

                if (item.ControlField == "rfshpCountry")
                    reqvalerrorSA10.ValidationGroup = item.ValGrp;

                reqvalerrorBA8.ValidationGroup = item.ValGrp;   //Billing address1
                reqvalerrorBA11.ValidationGroup = item.ValGrp;  //Billing city
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        if (Session["siteId"] != null)
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));

            if (lstQbit != null && lstQbit.Count() > 0)
            {
                var res = lstQbit.FirstOrDefault();
                if (res != null)
                    script = res.Script;
            }
        }
    }

    void PageLoadEvent()
    {
        ShowHideTicketPurchaseDiv();

        ddlCountry.DataSource = ddlshpCountry.DataSource = (siteId == Guid.Parse("107F17AC-580C-4D94-A7F7-E858ADADAB4F")) ? _master.GetCountryDetail().ToList().Where(x => !GetNotShowCountryList().Contains(x.CountryName)).ToList() : _master.GetCountryDetail().ToList();
        ddlCountry.DataValueField = ddlshpCountry.DataValueField = "CountryID";
        ddlCountry.DataTextField = ddlshpCountry.DataTextField = "CountryName";
        ddlCountry.DataBind();
        ddlshpCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        ddlshpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));

        if (Session["siteId"] != null && Session["P2POrderID"] != null)
            _masterBooking.UpdateSiteToOrder(Convert.ToInt64(Session["P2POrderID"]), Guid.Parse(Session["siteId"].ToString()));

        AddItemInShoppingCart();
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            string countryID = cookie.Values["_cuntryId"];
            ddlCountry.SelectedValue = countryID;
            ddlshpCountry.SelectedValue = countryID;
        }
        FillShippingData();
    }

    public void FillShippingData()
    {
        try
        {
            if (Request.QueryString["req"] == "BE")
            {
                Guid countryid = Guid.Empty;
                HiddenField hiddenDileveryMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
                if (hiddenDileveryMethod.Value != "TA")
                {
                    if (ddlshpCountry.SelectedValue != "0" && chkShippingfill.Checked)
                        countryid = Guid.Parse(ddlshpCountry.SelectedValue);
                    else if (ddlCountry.SelectedValue != "0")
                        countryid = Guid.Parse(ddlCountry.SelectedValue);

                    if (countryid != Guid.Empty)
                    {
                        var lstShip = _masterBooking.getAllShippingDetail(siteId, countryid);
                        if (lstShip.Any())
                        {
                            pnlShipping.Visible = true;
                            rptShippings.DataSource = lstShip;
                        }
                        else
                        {
                            var lstDefaultShip = _masterBooking.getDefaultShippingDetail(siteId);
                            if (lstDefaultShip.Any())
                            {
                                pnlShipping.Visible = true;
                                rptShippings.DataSource = lstDefaultShip;
                            }
                            else
                                rptShippings.DataSource = null;
                        }
                        rptShippings.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void AddItemInShoppingCart()
    {
        try
        {
            if (Session["P2POrderID"] == null)
                return;
            Int64 OrderId = Convert.ToInt64(Session["P2POrderID"]);
            List<getbookingcartdata> lstPassDetail = _masterBooking.GetP2PSale(OrderId, "", getsitetickprotection()).OrderBy(x => x.OrderIdentity).ToList();

            if (lstPassDetail.Count > 0 && Session["AgentUserID"] == null)
            {
                Guid travellerid = (Guid)lstPassDetail.FirstOrDefault().TravellerID;
                var tblOrderTraveller = _db.tblOrderTravellers.FirstOrDefault(x => x.ID == travellerid);
                txtFirst.Text = tblOrderTraveller.FirstName;
                txtLast.Text = tblOrderTraveller.LastName;
                ddlMr.SelectedValue = GetSalutationIndex(tblOrderTraveller.Title).ToString();
            }

            Session["GaTaggingProductlist"] = lstPassDetail.Select(item => new ProductPass
            {
                Price = item.Price.ToString(),
                ProductCode = item.ProductID.ToString(),//this is passsale id not productid
                CategoryName = item.P2PPassDetail.From + " - " + item.P2PPassDetail.To,
                Name = item.P2PPassDetail.From + " - " + item.P2PPassDetail.To,
            }).ToList();

            lstPassDetail = lstPassDetail.ToList();

            if (lstPassDetail.Count() > 0)
            {
                rptTrain.DataSource = lstPassDetail.Count > 0 ? lstPassDetail : null;
                rptTrain.DataBind();
            }
            else
            {
                rptTrain.DataSource = null;
                rptTrain.DataBind();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    int GetSalutationIndex(string str)
    {
        switch (str)
        {
            case "Dr.":
                return 1;
            case "Mr.":
                return 2;
            case "Miss.":
                return 3;
            case "Mrs.":
                return 4;
            case "Ms.":
                return 5;
            default:
                return 0;
        }

    }

    protected BillingAddress GetBillingInfo()
    {
        return new BillingAddress
        {
            Address = txtAdd.Text,
            City = txtCity.Text,
            Country = ddlCountry.SelectedItem.Text,
            Email = txtEmail.Text,
            FirstName = txtFirst.Text,
            Lastname = txtLast.Text,
            ZipCode = txtZip.Text,
            State = txtState.Text
        };
    }

    protected void DeleteTicketInfo(string id)
    {
        try
        {
            List<ShoppingCartDetails> cartList = Session["SHOPPINGCART"] as List<ShoppingCartDetails>;
            List<BookingRequest> bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;

            if (cartList.Count > 0 && bookingList.Count > 0)
            {
                cartList.RemoveAll(x => x.Id.ToString() == id);
                bookingList.RemoveAll(x => x.Id.ToString() == id);
                Session["SHOPPINGCART"] = cartList;
                Session["BOOKING-REQUEST"] = bookingList;
                ShowMessage(1, "You have successfully deleted ticket from list view");
            }
            else
            {
                Session["SHOPPINGCART"] = null;
                Session["BOOKING-REQUEST"] = null;
            }
            AddItemInShoppingCart();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var istckProt = _masterPage.IsTicketProtection(siteId);
        if (e.Item.ItemType == ListItemType.Header)
        {
            var lblTckProc = e.Item.FindControl("divHeaderTckProt") as HtmlGenericControl;
            lblTckProc.Attributes.Add("style", istckProt ? "display:none" : string.Empty);
        }
        else if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            var lblTckProcRow = e.Item.FindControl("divTckProt") as HtmlGenericControl;
            lblTckProcRow.Attributes.Add("style", istckProt ? "display:none" : string.Empty);
            CheckBox chk = e.Item.FindControl("chkTicketProtection") as CheckBox;
            Label lblPrice = e.Item.FindControl("lblPrice") as Label;
            Label lbltpPrice = e.Item.FindControl("lbltpPrice") as Label;
            Label currsyb = e.Item.FindControl("currsyb") as Label;

            if (chk.Checked)
            {
                lblPrice.Text = (Convert.ToDecimal(lblPrice.Text) + Convert.ToDecimal(lbltpPrice.Text)).ToString("F2");
                _total = _total + Convert.ToDouble(lblPrice.Text);
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000");
            }
            else
            {
                _total = _total + Convert.ToDouble(lblPrice.Text);
                lbltpPrice.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
                currsyb.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ccc");
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            Int64 OrderId = Convert.ToInt64(Session["P2POrderID"] != null ? Session["P2POrderID"] : "0");
            String ApiName = Request.QueryString["req"] != null ? (Request.QueryString["req"] == "IT" ? "ITALIA" : (Request.QueryString["req"] == "BE" ? "BENE" : string.Empty)) : string.Empty;
            Label lblTotal = e.Item.FindControl("lblTotal") as Label;
            Label lblBookingFee = e.Item.FindControl("lblBookingFee") as Label;
            double totalAmount;
            decimal BookingFeeAmount = _masterBooking.P2PGetBooingFees(OrderId, ApiName);
            if (BookingFeeAmount > 0)
            {
                lblBookingFee.Text = BookingFeeAmount.ToString("F2");
                hdnBookingFee.Value = BookingFeeAmount.ToString("F2");
            }
            else
            {
                lblBookingFee.Text = "0.00";
                hdnBookingFee.Value = "0.00";
            }
            totalAmount = _total + Convert.ToDouble(hdnBookingFee.Value);
            lblTotal.Text = totalAmount.ToString("F2");
            lblTotalCost.Text = totalAmount.ToString("F2");
            /*update CommissionFeeAmount*/
            _masterBooking.P2PGetCommissionFee(Convert.ToInt64(Session["P2POrderID"] != null ? Session["P2POrderID"] : "0"));
        }
    }

    public decimal getsitetickprotection()
    {
        try
        {
            var list = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (list != null)
            {
                divpopupdata.InnerHtml = list.Description;
                return Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(list.Amount, siteId, list.CurrencyID, currencyID).ToString("F"));
            }
            else
                return 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
        }
    }

    public string GetBrowserData()
    {
        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        string BrowserData = "Browser Capabilities;\n"
            + "Type = " + browser.Type + ";\n"
            + "Name = " + browser.Browser + ";\n"
            + "Version = " + browser.Version + ";\n"
            + "Major Version = " + browser.MajorVersion + ";\n"
            + "Minor Version = " + browser.MinorVersion + ";\n"
            + "Platform = " + browser.Platform + ";\n"
            + "Is Beta = " + browser.Beta + ";\n"
            + "Is Crawler = " + browser.Crawler + ";\n"
            + "Is AOL = " + browser.AOL + ";\n"
            + "Is Win16 = " + browser.Win16 + ";\n"
            + "Is Win32 = " + browser.Win32 + ";\n"
            + "Supports Frames = " + browser.Frames + ";\n"
            + "Supports Tables = " + browser.Tables + ";\n"
            + "Supports Cookies = " + browser.Cookies + ";\n"
            + "Supports VBScript = " + browser.VBScript + ";\n"
            + "Supports JavaScript = " +
                browser.EcmaScriptVersion.ToString() + ";\n"
            + "Supports Java Applets = " + browser.JavaApplets + ";\n"
            + "Supports ActiveX Controls = " + browser.ActiveXControls
                  + ";\n"
            + "Supports JavaScript Version = " +
                browser["JavaScriptVersion"] + ";\n";
        return BrowserData;
    }

    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        try
        {
            if (!chkMandatory.Checked)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "checkMandatory", "shipingchk();callvalerror();checkMandatory();showhideterms();", true);
                return;
            }

            if (Session["IsCheckout"] == null)
            {
                _masterBooking.UpdateBrowserDetails(P2POrderId, GetBrowserData());
                UpdateAgentAddress(P2POrderId);
                Session["IsCheckout"] = true;
                Session.Add("P2BookURL", HttpContext.Current.Request.Url.AbsoluteUri);

                #region user login for public site
                if (AgentuserInfo.UserID == Guid.Empty && USERuserInfo.ID == Guid.Empty)
                {
                    bool isRegistered = false;
                    tblUserLogin oreg = new tblUserLogin();
                    var usesdetail = _ManageUser.GetUserByEmailAndSiteID(txtEmail.Text, siteId);
                    if (usesdetail != null)
                    {
                        oreg.ID = usesdetail.ID;
                        oreg.FirstName = usesdetail.FirstName;
                        oreg.LastName = usesdetail.LastName;
                        oreg.Email = usesdetail.Email;
                        oreg.SiteId = usesdetail.SiteId;
                        oreg.Password = usesdetail.Password;
                        isRegistered = true;
                    }
                    else
                    {
                        oreg.ID = Guid.NewGuid();
                        oreg.FirstName = txtFirst.Text;
                        oreg.LastName = txtLast.Text;
                        oreg.Email = txtEmail.Text;
                        oreg.Password = _Common.CreateRandomPassword(10);
                        oreg.Address = txtAdd.Text;
                        oreg.Phone = txtBillPhone.Text;
                        oreg.City = txtCity.Text;
                        oreg.PostCode = txtZip.Text;
                        oreg.Country = Guid.Parse(ddlCountry.SelectedValue);
                        oreg.SiteId = siteId;
                        oreg.IsActive = true;
                        _ManageUser.SaveUser(oreg);
                    }
                    USERuserInfo.UserEmail = oreg.Email;
                    USERuserInfo.ID = oreg.ID;
                    USERuserInfo.Username = oreg.FirstName + (string.IsNullOrEmpty(oreg.LastName) ? "" : " " + oreg.LastName);
                    USERuserInfo.SiteID = oreg.SiteId;
                    if (Session["P2POrderID"] != null)
                    {
                        long orderID = Convert.ToInt64(Session["P2POrderID"]);
                        _masterBooking.SetorderUserid(orderID, USERuserInfo.ID);
                    }

                    #region email sent user
                    //var siteName = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                    //string htmfile = Server.MapPath("~/MailTemplate/CRMEmailTemplate.htm");
                    //var xmlDoc = new XmlDocument();
                    //xmlDoc.Load(htmfile);
                    //var list = xmlDoc.SelectNodes("html");
                    //string body = list[0].InnerXml.ToString();
                    //if (siteName.IsSTA.HasValue ? siteName.IsSTA.Value : false)
                    //    body = body.Replace("##headercolor##", "#0c6ab8");
                    //else
                    //    body = body.Replace("##headercolor##", "#941e34");
                    //body = body.Replace("##siteimagepath##", LoadSiteLogo());
                    //body = body.Replace("##Heading##", (isRegistered ? "You have already registered at " : "User account has been successfully registered at ") + (siteName != null ? siteName.DisplayName : "Internationalrail"));
                    //body = body.Replace("##UserName##", oreg.Email);
                    //body = body.Replace("##Password##", oreg.Password);
                    //body = body.Replace("##Description##", "Please login your account by clicking this link :- <a href='" + siteURL + "User/Login'>Click here</a>");
                    //body = body.Replace("#Blank#", "&nbsp;");

                    //string Subject = "User Registration";
                    //var emailSetting = _masterPage.GetEmailSettingDetail(siteId);
                    //if (emailSetting != null)
                    //    _masterPage.SendContactUsMail(siteId, Subject, body, emailSetting.Email, txtEmail.Text);
                    #endregion
                }
                #endregion

                #region Booking Request

                var hdnDMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
                if (hdnDMethod != null && (hdnDMethod.Value == "DH" || hdnDMethod.Value == "ST" || hdnDMethod.Value == "TL"))
                    hdnShipMethod.Value = string.Empty;

                Session["ShipMethod"] = hdnShipMethod.Value != "" ? hdnShipMethod.Value : null;
                Session["ShipDesc"] = hdnShipDesc.Value;

                bool BookingResult = false;
                int postcodelen = txtZip.Text.Replace(" ", "").Length;
                lblpmsg.Visible = postcodelen > 7;
                if (postcodelen > 7)
                    return;

                #region Booking request check from Services
                try
                {
                    if (Session["BOOKING-REQUEST"] != null)
                    {
                        var mngUser = new ManageUser();
                        var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
                        var address = GetBillingInfo();
                        if (bookingList != null)
                            foreach (var item in bookingList)
                            {
                                #region TI Booking
                                if (item.PurchasingForServiceOwnerRequest != null)
                                {
                                    var client = new OneHubRailOneHubClient();
                                    PurchasingForServiceOwnerRequest req = item.PurchasingForServiceOwnerRequest;

                                    //--Modify request if more than one passengers
                                    if (DivItaliaPassengers.Visible)
                                    {
                                        List<Owner> owners = new List<Owner>();
                                        OwnerType type;
                                        int SkipFirstPassenger = 0;
                                        foreach (DataListItem li in dtlTrenItaliaPassngerDetails.Items)
                                        {
                                            SkipFirstPassenger++;
                                            var txtFirstName = li.FindControl("txtFirstName") as TextBox;
                                            var txtLastName = li.FindControl("txtLastName") as TextBox;
                                            var txtPassNumber = li.FindControl("txtPassNumber") as TextBox;
                                            var ltrPassengerType = li.FindControl("ltrPassengerType") as Literal;

                                            if (ltrPassengerType.Text.Contains("Adult"))
                                                type = OwnerType.Adult;
                                            else if (ltrPassengerType.Text.Contains("Child"))
                                                type = OwnerType.Boy;
                                            else if (ltrPassengerType.Text.Contains("Senior"))
                                                type = OwnerType.Senior;
                                            else
                                                type = OwnerType.Youth;

                                            KeyValidation keyvalid = new KeyValidation();
                                            if (ViewState["ParameterRegistry"] != null)
                                            {
                                                keyvalid = ViewState["ParameterRegistry"] as KeyValidation;
                                                Regex regex = new Regex(keyvalid.ValidationPattern);
                                                Match match = regex.Match(txtPassNumber.Text);
                                                if (!match.Success || match.Value != txtPassNumber.Text)
                                                {
                                                    ShowMessage(2, "Please enter valid Pass Number.");
                                                    Session["IsCheckout"] = null;
                                                    return;
                                                }
                                            }

                                            var owner = new Owner
                                            {
                                                Lastname = txtLastName.Text,
                                                FirstName = txtFirstName.Text,
                                                DocumentNumber = "",
                                                DocumentType = DocumentType.None,
                                                OwnerType = type,
                                            };

                                            if (ViewState["ParameterRegistry"] != null)
                                                owner.ParameterPair = ViewState["ParameterRegistry"] != null ? new ParameterPair { Key = keyvalid.Key, Value = txtPassNumber.Text } : null;
                                            owners.Add(owner);

                                            if (Session["P2PID"] != null)
                                            {
                                                Guid TravellerId = Guid.Parse(Session["objTravellerID"].ToString());
                                                var res = _db.tblOrderTravellers.FirstOrDefault(x => x.ID == TravellerId);
                                                if (SkipFirstPassenger == 1)
                                                {
                                                    res.FirstName = txtFirstName.Text;
                                                    res.LastName = txtLastName.Text;
                                                }
                                                else
                                                {
                                                    TravellerId = Guid.NewGuid();
                                                    var objTraveller = new tblOrderTraveller();
                                                    objTraveller.ID = TravellerId;
                                                    objTraveller.Title = res.Title;
                                                    objTraveller.FirstName = txtFirstName.Text;
                                                    objTraveller.LastName = txtLastName.Text;
                                                    objTraveller.Country = res.Country;
                                                    _db.tblOrderTravellers.AddObject(objTraveller);
                                                }

                                                tblP2POtherTravellerLookup objP2PLookup = new tblP2POtherTravellerLookup();
                                                objP2PLookup.ID = Guid.NewGuid();
                                                objP2PLookup.OrderID = Convert.ToInt64(Session["P2POrderID"]);
                                                objP2PLookup.PassSaleID = Guid.Parse(Session["P2PID"].ToString());
                                                objP2PLookup.OrderTravellerID = TravellerId;
                                                _db.tblP2POtherTravellerLookup.AddObject(objP2PLookup);
                                                _db.SaveChanges();
                                            }
                                        }
                                        //Session["P2PID"] = null;
                                        int count = 0;
                                        foreach (var request in req.PurchasingForServiceOwnerGroup)
                                        {
                                            req.PurchasingForServiceOwnerGroup[count++].Owners = owners.ToArray();
                                        }
                                    }
                                    //--End modification

                                    req.BillingAddress = address;
                                    _ManageOneHub.ApiLogin(req, siteId);
                                    PurchasingForServiceOwnerResponse response = item.IsInternational ? client.PurchasingForServicesInternational(req) : client.PurchasingForServiceOwner(req);
                                    if (response != null)
                                    {
                                        if (response.TicketBookingDetailList != null && response.TicketIssueResponse != null && response.ErrorMessage == null)
                                        {
                                            GetBookingResponse(response, null, null, address, item.DepartureStationName);
                                            string DeliveryMathod = GetDilveryName(string.Empty);
                                            AddP2PBookingInLocalDB(DeliveryMathod, 0);
                                            BookingResult = true;
                                        }
                                        else
                                        {
                                            ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                            Session["IsCheckout"] = null;
                                            return;
                                        }
                                    }
                                    else
                                        ShowMessage(2, "There has been an error with your booking.");
                                }
                                #endregion

                                #region BENE Booking
                                if (item.PurchasingForServiceRequest != null)
                                {
                                    var client = new OneHubRailOneHubClient();
                                    PurchasingForServiceRequest req = item.PurchasingForServiceRequest;
                                    req.BillingAddress = address;

                                    #region Delivery Option
                                    HiddenField hiddenDileveryMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
                                    req.BookingType = hiddenDileveryMethod.Value == "TL" ? BookingType.confirm : BookingType.provisional;
                                    switch (hiddenDileveryMethod.Value)
                                    {
                                        case "TL":
                                            req.DeliveryMethod = DeliveryMethod.TL;
                                            break;
                                        case "TA":
                                            req.DeliveryMethod = DeliveryMethod.TA;
                                            break;
                                        case "ST":
                                            req.DeliveryMethod = DeliveryMethod.ST;
                                            break;
                                        case "DH":
                                            req.DeliveryMethod = DeliveryMethod.DH;
                                            break;
                                        case "HP":
                                            req.DeliveryMethod = DeliveryMethod.HP;
                                            break;
                                    }
                                    #endregion

                                    #region Ticket Collection Location
                                    var ddlCollectStation = ucTicketDelivery.FindControl("ddlCollectStation") as DropDownList;
                                    if (ddlCollectStation != null)
                                        if (ddlCollectStation.SelectedValue != "0")
                                            Session["CollectStation"] = ddlCollectStation.SelectedItem.Text;
                                        else
                                            Session["CollectStation"] = null;
                                    #endregion

                                    #region Passanger Info
                                    int start = 0;
                                    //DataList dtlPassngerDetails = hiddenDileveryMethod.Value == "TA" ? ucTicketDelivery.FindControl("dtlPassngerDetails2") as DataList : ucTicketDelivery.FindControl("dtlPassngerDetails") as DataList;
                                    var dtlPassngerDetails = hiddenDileveryMethod.Value == "TA" ? ucTicketDelivery.FindControl("dtlPassngerDelivery") as DataList : ucTicketDelivery.FindControl("dtlPassngerDetails") as DataList;
                                    var oPassnger = req.PassengerListReply.ToList();
                                    foreach (DataListItem li in dtlPassngerDetails.Items)
                                    {
                                        var txtFirstName = li.FindControl("txtFirstName") as TextBox;
                                        var txtLastName = li.FindControl("txtLastName") as TextBox;
                                        var txtEmailAddress = li.FindControl("txtEmailAddress") as TextBox;
                                        oPassnger[start].FirstName = string.IsNullOrEmpty(txtFirstName.Text) ? address.FirstName : txtFirstName.Text;
                                        oPassnger[start].LastName = string.IsNullOrEmpty(txtLastName.Text) ? address.Lastname : txtLastName.Text;
                                        oPassnger[start].EmailId = string.IsNullOrEmpty(txtEmailAddress.Text) ? address.Email : txtEmailAddress.Text;

                                        DropDownList ddlDay = li.FindControl("ddlDay") as DropDownList;
                                        DropDownList ddlMonth = li.FindControl("ddlMonth") as DropDownList;
                                        DropDownList ddlYear = li.FindControl("ddlYear") as DropDownList;
                                        List<string> TariffgroupList = new List<string> { "THA", "TGV", "SVI", "TGI", "TGS", "TPL", "RHE" };
                                        bool isThaylo = req.BookingRequestList.Any(t => t.PriceOffer.Any(p => TariffgroupList.Contains(p.Taco.Tariffgroup)));
                                        if (isThaylo && ddlDay != null)
                                        {
                                            var dob = ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
                                            oPassnger[start].BirthDate = Convert.ToDateTime(dob);
                                        }

                                        #region TGV Train
                                        List<string> TariffgroupTGVList = new List<string> { "TGV" };
                                        var txtLoyaltyCard = li.FindControl("txtLoyaltyCard") as TextBox;
                                        bool IsTGVTrain = req.BookingRequestList.Any(t => t.PriceOffer.Any(p => TariffgroupTGVList.Contains(p.Taco.Tariffgroup)));
                                        if (IsTGVTrain && !string.IsNullOrEmpty(txtLoyaltyCard.Text))
                                        {
                                            List<Loyaltycard> loyaltryList = new List<Loyaltycard>();
                                            Loyaltycard loyaltyInfo = new Loyaltycard
                                            {
                                                cardnumber = txtLoyaltyCard.Text,
                                                carrier = new Carrier { code = "GV", value = "SNCF Voyageur Card" }
                                            };
                                            loyaltryList.Add(loyaltyInfo);
                                            oPassnger[start].Loyaltycard = loyaltryList.ToArray();
                                        }
                                        else
                                            oPassnger[start].Loyaltycard = null;
                                        #endregion

                                        #region Save Travellers
                                        if (Session["P2PID"] != null)
                                        {
                                            Guid TravellerId = Guid.Parse(Session["objTravellerID"].ToString());
                                            var res = _db.tblOrderTravellers.FirstOrDefault(x => x.ID == TravellerId);
                                            if (start == 0)
                                            {
                                                res.FirstName = txtFirstName.Text;
                                                res.LastName = txtLastName.Text;
                                            }
                                            else
                                            {
                                                TravellerId = Guid.NewGuid();
                                                var objTraveller = new tblOrderTraveller();
                                                objTraveller.ID = TravellerId;
                                                objTraveller.Title = res.Title;
                                                objTraveller.FirstName = txtFirstName.Text;
                                                objTraveller.LastName = txtLastName.Text;
                                                objTraveller.Country = res.Country;
                                                _db.tblOrderTravellers.AddObject(objTraveller);
                                            }

                                            tblP2POtherTravellerLookup objP2PLookup = new tblP2POtherTravellerLookup();
                                            objP2PLookup.ID = Guid.NewGuid();
                                            objP2PLookup.OrderID = Convert.ToInt64(Session["P2POrderID"]);
                                            objP2PLookup.PassSaleID = Guid.Parse(Session["P2PID"].ToString());
                                            objP2PLookup.OrderTravellerID = TravellerId;
                                            _db.tblP2POtherTravellerLookup.AddObject(objP2PLookup);
                                            _db.SaveChanges();
                                        }
                                        #endregion
                                        start++;
                                    }
                                    req.PassengerListReply = oPassnger.ToArray();
                                    #endregion

                                    #region Delivery by mail
                                    if (hiddenDileveryMethod.Value == "TA")
                                    {
                                        var txtMailFName = ucTicketDelivery.FindControl("txtMailFName") as TextBox;
                                        var txtMailLName = ucTicketDelivery.FindControl("txtMailLName") as TextBox;
                                        var txtDepartment = ucTicketDelivery.FindControl("txtDepartment") as TextBox;
                                        var txtStreet = ucTicketDelivery.FindControl("txtStreet") as TextBox;
                                        var txtPostalCode = ucTicketDelivery.FindControl("txtPostalCode") as TextBox;
                                        var txtMailCity = ucTicketDelivery.FindControl("txtMailCity") as TextBox;
                                        var ddlCountryMail = ucTicketDelivery.FindControl("ddlCountryMail") as DropDownList;
                                        var ddlStateMail = ucTicketDelivery.FindControl("ddlStateMail") as DropDownList;
                                        var ordID = Convert.ToInt64(Session["P2POrderID"]);
                                        var details = new tblP2PDeliveryDetails
                                        {
                                            ID = Guid.NewGuid(),
                                            OrderID = ordID,
                                            FirstName = txtMailFName.Text,
                                            LastName = txtMailLName.Text,
                                            Department = txtDepartment.Text,
                                            Address = txtStreet.Text,
                                            Postcode = txtPostalCode.Text,
                                            City = txtCity.Text,
                                            CountryID = Guid.Parse(ddlCountryMail.SelectedValue),
                                            StateID = ddlStateMail.SelectedIndex != -1 ? Convert.ToInt32(ddlStateMail.SelectedValue) : 0,
                                            CreatedDate = DateTime.Now
                                        };

                                        _masterBooking.AddDeliveryDetails(details);
                                    }
                                    #endregion

                                    #region Loyaltycard
                                    if (hiddenDileveryMethod.Value == "TL")
                                    {
                                        start = 0;
                                        DataList dtlLoayalty = ucTicketDelivery.FindControl("dtlLoayalty") as DataList;
                                        if (dtlLoayalty.Items.Count > 0)
                                        {
                                            foreach (DataListItem li in dtlLoayalty.Items)
                                            {
                                                TextBox txtThalysCardNumber = li.FindControl("txtThalysCardNumber") as TextBox;
                                                if (!string.IsNullOrEmpty(txtThalysCardNumber.Text))
                                                {
                                                    List<Loyaltycard> loyCard = new List<Loyaltycard>();
                                                    loyCard.Add(new Loyaltycard
                                                    {
                                                        cardnumber = txtThalysCardNumber.Text,
                                                        carrier = new Carrier
                                                        {
                                                            code = "THA"
                                                        }
                                                    });
                                                    oPassnger[start].Loyaltycard = loyCard.ToArray();
                                                }
                                                start++;
                                            }
                                            req.PassengerListReply = oPassnger.ToArray();
                                        }


                                        #region Validate Card Numbers
                                        var list = oPassnger.SelectMany(x => x.Loyaltycard).ToList();
                                        var chkduplicate = list.GroupBy(x => x.cardnumber).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
                                        if (chkduplicate.Any(x => x.Count > 1))
                                        {
                                            list = null;
                                            ShowMessage(2, "Similar (Thalys or Eurostar) card number is not allowed for another traveller.");
                                            return;
                                        }
                                        else if (list.Any(x => x.cardnumber.Length < 16))
                                        {
                                            list = null;
                                            ShowMessage(2, "Invalid card number.");
                                            return;
                                        }
                                        else if (list.Where(z => z.carrier.code == "EUR").Any(x => x.cardnumber.Substring(0, 6) != "308381"))
                                        {
                                            list = null;
                                            ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
                                            return;
                                        }

                                        else if (list.Where(z => z.carrier.code == "THA").Any(x => x.cardnumber.Substring(0, 6) != "308406"))
                                        {
                                            list = null;
                                            ShowMessage(2, "The number of your frequent traveller programme (Thalys The Card or Eurostar Frequent Traveller) is not correct. Please check the number of your frequent traveller card");
                                            return;
                                        }

                                        #endregion
                                    }
                                    #endregion
                                    _ManageOneHub.ApiLogin(req, siteId);
                                    #region Request And Save Data In DB
                                    PurchasingForServiceResponse response = client.PurchasingForService(req);
                                    if (response != null)
                                    {
                                        if (!String.IsNullOrEmpty(response.ReservationCode) && response.ErrorMessage == null)
                                        {
                                            GetBookingResponse(null, response, null, null, item.DepartureStationName);
                                            string DeliveryMathod = GetDilveryName(hdnDMethod.Value);
                                            AddP2PBookingInLocalDB(DeliveryMathod, response.Amount);
                                            BookingResult = true;
                                        }
                                        else
                                        {
                                            ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                            return;
                                        }
                                    }
                                    else
                                        ShowMessage(2, "There has been an error with your booking.");
                                    #endregion
                                }
                                #endregion

                                #region EVOLVI Booking
                                if (item.EvolviBookingRequest != null)
                                {
                                    var Client = new OneHubRailOneHubClient();
                                    RailBookingRequest request = item.EvolviBookingRequest;
                                    request.Result = false;
                                    long orderID = Convert.ToInt64(Session["P2POrderID"]);
                                    request.EvDeliveryAddressInfo = EvolviBillingAddress();
                                    _ManageOneHub.EmptyEvolviBasket(orderID, siteId, true);
                                    _ManageOneHub.ApiLogin(request, siteId);
                                    RailBookingResponse response = Client.EvolviRailBooking(request);
                                    if (response != null)
                                    {
                                        if (response.ErrorMessage == null)
                                        {
                                            GetBookingResponse(null, null, response, null, item.DepartureStationName);
                                            string DeliveryMathod = GetDilveryName("EV");
                                            AddP2PBookingInLocalDB(DeliveryMathod, 0);
                                            AddUpdateEvolviBookingChargesInDb(response);
                                            BookingResult = true;
                                        }
                                        else
                                        {
                                            ShowMessage(2, response.ErrorMessage.MessageCode + ": " + response.ErrorMessage.MessageDescription);
                                            return;
                                        }
                                    }
                                    else
                                        ShowMessage(2, "There has been an error with your booking.");
                                }
                                #endregion
                            }
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(2, ex.Message);
                }
                #endregion

                if (BookingResult)
                {
                    if (P2POrderId != 0)
                    {
                        ManageAdmfee.UpdateOrderAdminFeeByOrderID(P2POrderId, Convert.ToDecimal(hdnAdminFeeAmount.Value));
                        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                        if (AgentuserInfo.UserID == Guid.Empty && result != null && (result.IsWholeSale || (bool)result.IsAgent))
                        {
                            Session["redirectpage"] = "?req=" + P2POrderId;
                            Response.Redirect("~/Agent/login", false);
                        }
                        else
                            Response.Redirect("~/PaymentProcess?req=" + P2POrderId, false);
                    }
                }
                else
                    Session["IsCheckout"] = null;
                #endregion
            }
            else
            {
                if (Session["BOOKING-REPLY"] != null)
                    Response.Redirect("~/PaymentProcess?req=" + P2POrderId, false);
                else
                    Session["IsCheckout"] = null;
            }
        }
        catch (Exception ex)
        {
            Session["IsCheckout"] = null;
            ShowMessage(2, ex.Message);
        }
    }

    public string LoadSiteLogo()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }

    void ShowHideTrenItaliaPassengerDiv()
    {
        if (Session["BOOKING-REQUEST"] != null)
        {
            var bookingList = Session["BOOKING-REQUEST"] as List<BookingRequest>;
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            if (bookingList != null)
                foreach (var item in bookingList)
                {
                    if (item.PurchasingForServiceOwnerRequest != null)
                    {
                        PurchasingForServiceOwnerRequest req = item.PurchasingForServiceOwnerRequest;
                        int adult = objBRUC.Adults;
                        int senior = objBRUC.Seniors;
                        int youth = objBRUC.Youths;
                        int boys = objBRUC.Boys;
                        DivItaliaPassengers.Visible = true;// (adult + senior + youth + boys) > 1;

                        #region enable disable pass holder for TI booking
                        ApiLoginOwner(req);
                        var client = new OneHubRailOneHubClient();
                        var response = client.GetParameterRegistry(new ParameterRegistryRequest
                        {
                            Header = req.Header,
                            offers = req.PurchasingForServiceOwnerGroup.Select(x => x.SolutionRate.Offer).ToArray()
                        });
                        ViewState["ParameterRegistry"] = response != null ? response.FirstOrDefault() : null;
                        IsTIBookingPassHolder = response != null && response.Count() > 0 ? true : false;

                        #endregion

                        var list = new List<Passanger>();
                        for (int i = 0; i < adult; i++)
                        {
                            list.Add(new Passanger
                            {
                                PassangerType = "Adult" + (i + 1).ToString()
                            });
                        }

                        for (int i = 0; i < boys; i++)
                        {
                            list.Add(new Passanger
                            {
                                PassangerType = "Child" + (i + 1).ToString()
                            });
                        }

                        for (int i = 0; i < youth; i++)
                        {
                            list.Add(new Passanger
                            {
                                PassangerType = "Youth" + (i + 1).ToString()
                            });
                        }

                        for (int i = 0; i < senior; i++)
                        {
                            list.Add(new Passanger
                            {
                                PassangerType = "Senior" + (i + 1).ToString()
                            });
                        }

                        dtlTrenItaliaPassngerDetails.DataSource = list;
                        dtlTrenItaliaPassngerDetails.DataBind();

                        foreach (DataListItem li in dtlTrenItaliaPassngerDetails.Items)
                        {
                            var txtFirstName = li.FindControl("txtFirstName") as TextBox;
                            var txtLastName = li.FindControl("txtLastName") as TextBox;
                            var rec = req.PurchasingForServiceOwnerGroup.FirstOrDefault().Owners[0];
                            if (txtFirstName != null) txtFirstName.Text = rec.FirstName;
                            if (txtLastName != null) txtLastName.Text = rec.Lastname;
                            break;
                        }
                    }
                }
        }
    }

    public string GetDilveryName(string value)
    {
        switch (value)
        {
            case "TL":
                return "Thayls Ticketless";
            case "TA":
                return "Delivery by mail";
            case "ST":
                return "Collect at ticket desk";
            case "DH":
                return "Print at Home";
            case "HP":
                return "Self print train tickets";
            default:
                return "TrenItaila Printing";
        }
    }

    void AddP2PBookingInLocalDB(string delivery, float respAmt)
    {
        if (Session["P2PIdInfo"] == null)
            return;
        foreach (RepeaterItem it in rptTrain.Items)
        {
            CheckBox chkTicketProtection = it.FindControl("chkTicketProtection") as CheckBox;
            Label lbltpPrice = it.FindControl("lbltpPrice") as Label;
            Label lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
            if (chkTicketProtection.Checked)
                _masterBooking.UpdateTicketProtection(Guid.Parse(lblPassSaleID.Text), Convert.ToDecimal(lbltpPrice.Text));

            if (Session["IsFullPrice"] != null && respAmt > 0)
            {
                bool IsFullPrice = Convert.ToBoolean(Session["IsFullPrice"].ToString());
                if (!IsFullPrice)
                    UpdatePrice(Convert.ToDecimal(GetTotalPriceBeNe((Decimal)respAmt, false) / rptTrain.Items.Count), Guid.Parse(lblPassSaleID.Text));
            }

        }
        Session["IsFullPrice"] = null;
        bool isReg = false;
        if (Session["IsRegional"] != null)
            isReg = Convert.ToBoolean(Session["IsRegional"]);

        Int64 orderID = Convert.ToInt64(Session["P2POrderID"]);
        List<P2PReservationIDInfo> lstP2PIdInfo = new List<P2PReservationIDInfo>();
        if (Session["P2PIdInfo"] != null)
            lstP2PIdInfo = (List<P2PReservationIDInfo>)Session["P2PIdInfo"];

        foreach (RepeaterItem it in rptTrain.Items)
        {
            Label lblPassSaleID = it.FindControl("lblPassSaleID") as Label;
            ManageBooking objBooking = _masterBooking;

            if (Session["BOOKING-REPLY"] != null)
            {
                var oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                foreach (var item in oldList)
                {
                    if (Request.QueryString["req"] == "IT" || Request.QueryString["req"] == "BE")
                    {
                        string pinNumber = string.IsNullOrEmpty(item.PinCode) ? item.UnitOfWork.ToString() : item.PinCode;
                        if (lstP2PIdInfo.Count() > 0)
                        {
                            List<P2PReservationIDInfo> NlstP2PIdInfo = lstP2PIdInfo.Where(a => a.JourneyType == "").ToList();
                            if (NlstP2PIdInfo.Count() > 0)
                                foreach (P2PReservationIDInfo o in NlstP2PIdInfo)
                                {
                                    bool result = objBooking.UpdateReservationCodebyP2PID(o.ID, item.ReservationCode, pinNumber, delivery, false);
                                }
                            else
                                objBooking.UpdateReservationCodebyP2PID(Guid.Parse(lblPassSaleID.Text), orderID, item.ReservationCode, pinNumber, delivery, false, item.DepStationName, isReg);
                        }
                        else

                            objBooking.UpdateReservationCodebyP2PID(Guid.Parse(lblPassSaleID.Text), orderID, item.ReservationCode, pinNumber, delivery, false, item.DepStationName, isReg);
                    }
                    else if (Request.QueryString["req"] == "EV")
                    {
                        if (Session["EvolviReservationRef"] != null)
                        {
                            var EvReservationRefList2 = Session["EvolviReservationRef"] as List<EvolviReservationRef>;
                            foreach (var Evitem in EvReservationRefList2.ToList())
                            {
                                bool result = objBooking.UpdateReservationCodebyP2PID(Evitem.P2PId, Evitem.ReservationCode, null, null, false);
                            }
                        }
                    }
                }
            }
        }

        tblOrderBillingAddress objBillingAddress = new tblOrderBillingAddress();
        objBillingAddress.ID = Guid.NewGuid();
        objBillingAddress.OrderID = orderID;
        objBillingAddress.Title = ddlMr.SelectedItem.Text;
        objBillingAddress.FirstName = txtFirst.Text;
        objBillingAddress.LastName = txtLast.Text;
        objBillingAddress.Phone = txtBillPhone.Text;
        objBillingAddress.Address1 = txtAdd.Text;
        objBillingAddress.Address2 = txtAdd2.Text;
        objBillingAddress.EmailAddress = txtEmail.Text;
        objBillingAddress.City = txtCity.Text;
        objBillingAddress.State = txtState.Text;
        objBillingAddress.Country = ddlCountry.SelectedItem.Text;
        objBillingAddress.Postcode = txtZip.Text;

        if (!chkShippingfill.Checked)
        {
            objBillingAddress.TitleShpg = ddlMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtFirst.Text;
            objBillingAddress.LastNameShpg = txtLast.Text;
            objBillingAddress.EmailAddressShpg = txtEmail.Text;
            objBillingAddress.PhoneShpg = txtBillPhone.Text;
            objBillingAddress.Address1Shpg = txtAdd.Text;
            objBillingAddress.Address2Shpg = txtAdd2.Text;
            objBillingAddress.CityShpg = txtCity.Text;
            objBillingAddress.StateShpg = txtState.Text;
            objBillingAddress.CountryShpg = ddlCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtZip.Text;
        }
        else
        {
            objBillingAddress.TitleShpg = ddlshpMr.SelectedItem.Text;
            objBillingAddress.FirstNameShpg = txtshpfname.Text;
            objBillingAddress.LastNameShpg = txtshpLast.Text;
            objBillingAddress.EmailAddressShpg = txtshpEmail.Text;
            objBillingAddress.PhoneShpg = txtShpPhone.Text;
            objBillingAddress.Address1Shpg = txtshpAdd.Text;
            objBillingAddress.Address2Shpg = txtshpAdd2.Text;
            objBillingAddress.CityShpg = txtshpCity.Text;
            objBillingAddress.StateShpg = txtshpState.Text;
            objBillingAddress.CountryShpg = ddlshpCountry.SelectedItem.Text;
            objBillingAddress.PostcodeShpg = txtshpZip.Text;
        }
        if (txtDateOfDepature.Text != "DD/MM/YYYY")
            _masterBooking.UpdateDepatureDate(Convert.ToInt64(Session["P2POrderID"]), Convert.ToDateTime(txtDateOfDepature.Text));

        _masterBooking.AddOrderBillingAddress(objBillingAddress);

        var hiddenDileveryMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
        string ShipMethod = "";
        string ShipDesc = "";
        string CollectStation = "";
        if (hiddenDileveryMethod.Value == "TA" && Request.QueryString["req"] == "BE")
        {
            ShipMethod = hdnShipMethod.Value;
            ShipDesc = hdnShipDesc.Value;
        }
        else
            hdnShippingCost.Value = "0";

        if (Session["CollectStation"] != null)
            CollectStation = Session["CollectStation"].ToString();
        _masterBooking.UpdateOrderData(Convert.ToDecimal(hdnShippingCost.Value), ShipMethod, ShipDesc, CollectStation, Convert.ToInt64(Session["P2POrderID"]));

        /*Update IsRegional*/
        if (Session["IsRegional"] != null)
            _masterBooking.UpdateIsRegional(Convert.ToBoolean(Session["IsRegional"].ToString()), Convert.ToInt64(Session["P2POrderID"]));

        _masterBooking.UpdateOrderBookingFee(Convert.ToDecimal(hdnBookingFee.Value), Convert.ToInt64(Session["P2POrderID"]));
    }

    public Decimal GetTotalPriceBeNe(Decimal snoPrice, bool isTrenItalia)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID(isTrenItalia ? "EUT" : "EUB");
        Decimal price = 0;
        if (snoPrice > 0)
        {
            string MinPrice = FrontEndManagePass.GetPriceAfterConversion(snoPrice, siteId, srcCurId, currencyID).ToString("F");
            price = Convert.ToDecimal(GetRoundPrice(MinPrice));
        }
        return price;
    }

    protected void GetBookingResponse(PurchasingForServiceOwnerResponse responseown, PurchasingForServiceResponse response, RailBookingResponse evolviResponse, BillingAddress address, string DepStName)
    {
        try
        {
            #region TI response
            List<BookingResponse> listbookingReply = new List<BookingResponse>();
            if (responseown != null)
            {
                var ticketBookingDetail = responseown.TicketBookingDetailList.FirstOrDefault(x => x.ReservationCode == responseown.TicketIssueResponse.ReservationCode);
                listbookingReply = responseown.TicketIssueResponse != null
                                       ? new List<BookingResponse>
                                                   {
                                                       new BookingResponse
                                                           {
                                                               Issued = responseown.TicketIssueResponse.Issued,
                                                              // Coupons= responseown.TicketIssueResponse.Coupons,
                                                               ReservationCode = responseown.TicketIssueResponse.ReservationCode,
                                                               ChangeReservationCode =ticketBookingDetail!=null? ticketBookingDetail.ChangeReservationCode:string.Empty,
                                                               UnitOfWork = responseown.TicketIssueResponse.UnitOfWork,
                                                               BillingAddress = address,
                                                               DepStationName = DepStName,
                                                               PdfUrl=TicketUrl(responseown.TicketIssueResponse.ReceiptPDF.ToList(),responseown.TicketIssueResponse.ReservationCode)
                                                           }
                                                   }
                                       : null;
            }
            #endregion

            #region BENE response
            if (response != null)
                listbookingReply = new List<BookingResponse>
                    {
                        new BookingResponse
                            {
                                Issued = true,
                                ReservationCode = response.ReservationCode,
                                PinCode = response.Pincode,
                                Pnr = response.Pnr,
                                DepStationName = DepStName,
                               
                            }
                    };
            #endregion

            #region EVOLVI Response
            if (evolviResponse != null)
            {
                listbookingReply = new List<BookingResponse>
                    {
                        new BookingResponse
                            {
                                Status=evolviResponse.OrderSummaryInfos!=null?evolviResponse.OrderSummaryInfos.FirstOrDefault().Status:"",
                                OrderId=evolviResponse.OrderSummaryInfos!=null?evolviResponse.OrderSummaryInfos.FirstOrDefault().BookingNo:"",
                                OrderRefId=evolviResponse.OrderSummaryInfos!=null?evolviResponse.OrderSummaryInfos.FirstOrDefault().BookingReferenceNo:""
                            }
                    };
            }
            #endregion

            if (Session["BOOKING-REPLY"] != null)
            {
                List<BookingResponse> oldList = Session["BOOKING-REPLY"] as List<BookingResponse>;
                if (oldList != null) if (listbookingReply != null) listbookingReply.AddRange(oldList);
            }
            Session["BOOKING-REPLY"] = listbookingReply;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public List<string> TicketUrl(List<byte[]> base64StrList, string fileName)
    {
        try
        {
            List<string> urls = new List<string>();
            string siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);
            foreach (var itm in base64StrList)
            {
                string pdfName = string.IsNullOrEmpty(fileName) ? Session["P2POrderID"].ToString() + "-" + Guid.NewGuid().ToString() : fileName + "-" + Guid.NewGuid().ToString();
                byte[] bytes = itm;
                string path = "pdfService/" + pdfName + ".pdf";
                FileStream stream = new FileStream(Server.MapPath("~/" + path), FileMode.CreateNew);
                System.IO.BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
                urls.Add(siteURL + path);
            }
            return urls;
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(Request.Url.ToString(), ex.Message);
            return null;
        }
    }

    public void ShowHideTicketPurchaseDiv()
    {
        if (Request.QueryString["req"] != null)
        {
            if (Request.QueryString["req"] == "BE")
                ucTicketDelivery.Visible = true;
        }
        else
            ucTicketDelivery.Visible = false;
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            FillShippingData();
            ScriptManager.RegisterStartupScript(this, GetType(), "callvalerror", "shipingchk();callvalerror();showhideterms();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    #region Regional BeNe Booking
    void UpdatePrice(decimal price, Guid passSaleId)
    {
        try
        {
            tblP2PSale objP2P = _db.tblP2PSale.Where(a => a.ID == passSaleId).FirstOrDefault();
            objP2P.ApiPrice = price;
            objP2P.NetPrice = Convert.ToDecimal(GetRoundPrice(price.ToString()));
            objP2P.Price = BusinessOneHub.IsNumeric(price) ? GetTotalPriceBeNe(price) : 0;
            _db.SaveChanges();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public Decimal GetTotalPriceBeNe(decimal price)
    {
        var srcCurId = FrontEndManagePass.GetCurrencyID("EUB");
        string MinPrice = FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(price), siteId, srcCurId, currencyID).ToString("F");
        Decimal NewPrice = Convert.ToDecimal(GetRoundPrice(MinPrice));
        return NewPrice;
    }

    string GetRoundPrice(string price)
    {
        //Please round up the prices to nearest above 0.50
        //If he price is 100.23, mark it as 100.50, 
        //If the price is 100.65 mark it as 102.00

        string[] strPrice = price.ToString().Split('.');

        if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 50)
            return (Convert.ToDecimal(strPrice[0]) + 1).ToString() + ".00";
        else if (strPrice[1] != null && Convert.ToDecimal(strPrice[1]) > 0 && Convert.ToDecimal(strPrice[1]) <= 50)
            return strPrice[0] + ".50";
        else
            return strPrice[0] + ".00";

    }
    #endregion

    #region Api Login
    private void ApiLoginOwner(PurchasingForServiceOwnerRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }

    private void ApiLogin(PurchasingForServiceRequest request)
    {
        #region API Account
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
            };
        }
        #endregion
    }
    #endregion

    public void GetDeliveryOptionByCountry()
    {
        try
        {
            if (Request.QueryString["req"] == "BE")
            {
                Guid countryId = Guid.Empty;
                HiddenField hiddenDileveryMethod = ucTicketDelivery.FindControl("hiddenDileveryMethod") as HiddenField;
                DropDownList ddlCountryMail = ucTicketDelivery.FindControl("ddlCountryMail") as DropDownList;
                if (hiddenDileveryMethod.Value == "TA")
                {
                    if (!string.IsNullOrEmpty(ddlCountryMail.SelectedValue) && ddlCountryMail.SelectedValue != "-1")
                        countryId = Guid.Parse(ddlCountryMail.SelectedValue);
                    else if (!string.IsNullOrEmpty(ddlshpCountry.SelectedValue) && ddlshpCountry.SelectedValue != "0")
                        countryId = Guid.Parse(ddlshpCountry.SelectedValue);

                    var lstShip = _masterBooking.getAllShippingDetail(siteId, countryId);
                    if (lstShip.Any())
                    {
                        rptShippings.DataSource = null;
                        pnlShipping.Visible = true;
                        rptShippings.DataSource = lstShip;
                    }
                    else
                    {
                        rptShippings.DataSource = null;
                        pnlShipping.Visible = false;
                    }
                    rptShippings.DataBind();
                }
                else
                {
                    ShowDefaultCountry();
                    if (!string.IsNullOrEmpty(ddlCountryMail.SelectedValue) && ddlCountryMail.SelectedValue != "-1")
                        countryId = Guid.Parse(ddlCountryMail.SelectedValue);
                    var lstShip = _masterBooking.getAllShippingDetail(siteId, countryId);
                    if (lstShip.Any())
                    {
                        rptShippings.DataSource = null;
                        pnlShipping.Visible = true;
                        rptShippings.DataSource = lstShip;
                    }
                    else
                    {
                        rptShippings.DataSource = null;
                        pnlShipping.Visible = false;
                    }
                    rptShippings.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void DeliveryByMailCountry(string strValue)
    {
        try
        {
            GetDeliveryOptionByCountry();
            ScriptManager.RegisterStartupScript(this, GetType(), "callvalerror", "shipingchk();callvalerror();showhideterms();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlshpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            FillShippingData();
            ScriptManager.RegisterStartupScript(this, GetType(), "callvalerror", "shipingchk();callvalerror();showhideterms();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void DeliveryByMailRadioButton(string strValue)
    {
        try
        {
            GetDeliveryOptionByCountry();
            ScriptManager.RegisterStartupScript(this, GetType(), "callvalerror", "shipingchk();callvalerror();showhideterms();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void DeliveryByMailDDLCountry(string strValue)
    {
        try
        {
            GetDeliveryOptionByCountry();
            ScriptManager.RegisterStartupScript(this, GetType(), "deliverybymailddlcountry", "shipingchk();callvalerror();showhideterms();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowDefaultCountry()
    {
        try
        {
            DropDownList ddlCountryMail = ucTicketDelivery.FindControl("ddlCountryMail") as DropDownList;
            var obMaster = new Masters();
            var CountryID = obMaster.GetDefaultCountryBySiteId(siteId);
            var data = obMaster.GetSiteListEdit(siteId);
            if (data != null)
                SiteUrl = data.SiteURL;

            if (SiteUrl.ToLower().Contains("idiv"))
                ddlCountryMail.SelectedValue = "-1";
            else if (CountryID != Guid.Empty)
            {
                ddlCountryMail.SelectedValue = CountryID.ToString();
                ucTicketDelivery.FillStates(Guid.Parse(ddlCountryMail.SelectedValue));
            }
            ddlCountry.SelectedValue = CountryID.ToString();
            ddlshpCountry.SelectedValue = CountryID.ToString();
        }
        catch (Exception ex) { throw ex; }
    }

    public void UpdateAgentAddress(long OrderID)
    {
        try
        {
            Session["ManageAgentData"] = null;
            if (Session["AgentUserID"] != null)
            {
                ManageAgentData _ManageAgentData = new ManageAgentData();
                _ManageAgentData.OrderId = OrderID;
                List<AgentAddress> _AgentAddress = new List<AgentAddress>();

                _AgentAddress.Add(new AgentAddress
                {
                    Title = ddlMr.SelectedValue,
                    FName = txtFirst.Text,
                    LName = txtLast.Text,
                    EmailAddress = txtEmail.Text,
                    Phone = txtBillPhone.Text,
                    Address1 = txtAdd.Text,
                    Address2 = txtAdd2.Text,
                    City = txtCity.Text,
                    State = txtState.Text,
                    ZipCode = txtZip.Text,
                    Country = ddlCountry.SelectedValue
                });

                if (chkShippingfill.Checked)
                {
                    _AgentAddress.Add(new AgentAddress
                    {
                        Title = ddlshpMr.SelectedValue,
                        FName = txtshpfname.Text,
                        LName = txtshpLast.Text,
                        EmailAddress = txtshpEmail.Text,
                        Phone = txtShpPhone.Text,
                        Address1 = txtshpAdd.Text,
                        Address2 = txtshpAdd2.Text,
                        City = txtshpCity.Text,
                        State = txtshpState.Text,
                        ZipCode = txtshpZip.Text,
                        Country = ddlshpCountry.SelectedValue
                    });
                }
                _ManageAgentData.AgentAddress = _AgentAddress;
                Session["ManageAgentData"] = _ManageAgentData;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillAgentAddress(long OrderID)
    {
        try
        {
            if (Session["ManageAgentData"] != null && Session["AgentUserID"] != null)
            {
                ManageAgentData ManageAgentDataList = Session["ManageAgentData"] as ManageAgentData;
                var _billingAddress = ManageAgentDataList.AgentAddress.FirstOrDefault();
                ddlMr.SelectedValue = _billingAddress.Title;
                txtFirst.Text = _billingAddress.FName;
                txtLast.Text = _billingAddress.LName;
                txtEmail.Text = _billingAddress.EmailAddress;
                txtBillPhone.Text = _billingAddress.Phone;
                txtAdd.Text = _billingAddress.Address1;
                txtAdd2.Text = _billingAddress.Address2;
                txtCity.Text = _billingAddress.City;
                txtState.Text = _billingAddress.State;
                txtZip.Text = _billingAddress.ZipCode;
                ddlCountry.SelectedValue = _billingAddress.Country;
                if (ManageAgentDataList.AgentAddress.Count == 2)
                {
                    var _shippingAddress = ManageAgentDataList.AgentAddress.Skip(1).FirstOrDefault();
                    ddlshpMr.SelectedValue = _shippingAddress.Title;
                    txtshpfname.Text = _shippingAddress.FName;
                    txtshpLast.Text = _shippingAddress.LName;
                    txtshpEmail.Text = _shippingAddress.EmailAddress;
                    txtShpPhone.Text = _shippingAddress.Phone;
                    txtshpAdd.Text = _shippingAddress.Address1;
                    txtshpAdd2.Text = _shippingAddress.Address2;
                    txtshpCity.Text = _shippingAddress.City;
                    txtshpState.Text = _shippingAddress.State;
                    txtshpZip.Text = _shippingAddress.ZipCode;
                    ddlshpCountry.SelectedValue = _shippingAddress.Country;
                    chkShippingfill.Checked = true;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "FillAgentAddress", "fillagentdata();showhideterms();", true);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetEvolviFareInfoList()
    {
        try
        {
            List<ReservationInfo> ReservationInfoList = Session["ReservationInfoList"] as List<ReservationInfo>;
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse.TrainInformationList == null || ReservationInfoList == null)
                return;

            string TicketDetails, tod = string.Empty;
            EvolviFareInformationRequest request = new EvolviFareInformationRequest();
            EvolviFareInformationResponse response = new EvolviFareInformationResponse();
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            request.TicketTypeCode = string.Empty;
            request.TocCode = string.Empty;
            _ManageOneHub.ApiLogin(request, siteId);
            response = client.EvolviFareInformation(request);
            div_EvolviTermCondition.Attributes.Add("style", "display:block");
            if (response != null)
                if (response.EvFareInformationList != null)
                {
                    string ticketTypeCode, ticketTypeCode2, trainNo1, trainNo2, fareId1, fareId2 = string.Empty;
                    trainNo1 = ReservationInfoList.FirstOrDefault(x => x.JourneyType == "Outbound").JourneyIdentifier;
                    trainNo2 = ReservationInfoList.Any(x => x.JourneyType == "Inbound") ? ReservationInfoList.FirstOrDefault(x => x.JourneyType == "Inbound").JourneyIdentifier : string.Empty;
                    fareId1 = ReservationInfoList.FirstOrDefault(x => x.JourneyType == "Outbound").FareId;
                    fareId2 = ReservationInfoList.Any(x => x.JourneyType == "Inbound") ? ReservationInfoList.FirstOrDefault(x => x.JourneyType == "Inbound").FareId : string.Empty;

                    #region OutBound
                    ticketTypeCode = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(x => x.IsReturn == false && x.TrainNumber == trainNo1).PriceInfo.FirstOrDefault().TrainPriceList.FirstOrDefault(x => x.FareId == fareId1).TicketTypeCode;
                    var fareInfoList = response.EvFareInformationList.Where(x => x.TicketTypeCode == ticketTypeCode).Count() > 1 ? response.EvFareInformationList.Where(x => x.TicketTypeCode == ticketTypeCode).Skip(1).ToList() : response.EvFareInformationList.Where(x => x.TicketTypeCode == ticketTypeCode).ToList();
                    tod = fareInfoList.FirstOrDefault().TodAvailable == "1" ? "Available" : "Not Available";
                    TicketDetails = "<h4>Order Item 1 (Outbound): " + fareInfoList.FirstOrDefault().TicketTypeAltName + "</h4>";
                    TicketDetails += "<p><strong>Ticket Code: </strong>" + fareInfoList.FirstOrDefault().TicketTypeCode + "</p>";
                    TicketDetails += "<p><strong>Class: </strong>" + fareInfoList.FirstOrDefault().TicketTypeClass + "</p>";
                    TicketDetails += "<p><strong>Reservation: </strong>" + fareInfoList.FirstOrDefault().ReservationRule + "</p>";
                    TicketDetails += "<p><strong>Journey: </strong>" + fareInfoList.FirstOrDefault().JourneyType + "</p>";

                    TicketDetails += "<p><strong>Refunds & Cancellations: </strong>" + fareInfoList.FirstOrDefault().RefundCancellationRule + "</p>";
                    TicketDetails += "<p><strong>Changes to travel plans: </strong>" + fareInfoList.FirstOrDefault().ChangeRule + "</p>";
                    TicketDetails += "<p><strong>Ticket Conditions: </strong>" + fareInfoList.FirstOrDefault().TicketTypeConditionsDescription + "</p>";
                    TicketDetails += "<p><strong>Break of Journey: </strong>" + fareInfoList.FirstOrDefault().JourneyBreakRule + "</p>";
                    TicketDetails += "<p><strong>Availability: </strong>" + fareInfoList.FirstOrDefault().AvailabilityRule + "</p>";
                    TicketDetails += "<p><strong>Ticket on Departure: </strong>" + tod + "</p>";
                    TicketDetails += "<p><strong>Pre-Booking requirement: </strong>" + fareInfoList.FirstOrDefault().PreBookingRequirement + "</p>";
                    TicketDetails += "<p><strong>Valid out: </strong>" + fareInfoList.FirstOrDefault().ValidOutRule + "</p>";
                    TicketDetails += "<p><strong>Valid return: </strong>" + fareInfoList.FirstOrDefault().ValidReturnRule + "</p>";
                    TicketDetails += "<p><strong>Child discount: </strong>" + fareInfoList.FirstOrDefault().ChildDiscount + "</p>";
                    TicketDetails += "<p><strong>Railcard discounts: </strong>" + fareInfoList.FirstOrDefault().RailcardDiscount + "</p>";
                    #endregion

                    #region InBound
                    if (ReservationInfoList.Any(x => x.JourneyType == "Inbound"))
                    {
                        ticketTypeCode2 = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(x => x.IsReturn == true && x.TrainNumber == trainNo2).PriceInfo.FirstOrDefault().TrainPriceList.FirstOrDefault(x => x.FareId == fareId2).TicketTypeCode;
                        var fareInfoList2 = response.EvFareInformationList.Where(x => x.TicketTypeCode == ticketTypeCode2).Count() > 1 ? response.EvFareInformationList.Where(x => x.TicketTypeCode == ticketTypeCode2).Skip(1).ToList() : response.EvFareInformationList.Where(x => x.TicketTypeCode == ticketTypeCode2).ToList();
                        tod = fareInfoList2.FirstOrDefault().TodAvailable == "1" ? "Available" : "Not Available";
                        TicketDetails += "<br/>";
                        TicketDetails += "<h4>Order Item 1 (Inbound): " + fareInfoList2.FirstOrDefault().TicketTypeAltName + "</h4>";
                        TicketDetails += "<p><strong>Ticket Code: </strong>" + fareInfoList2.FirstOrDefault().TicketTypeCode + "</p>";
                        TicketDetails += "<p><strong>Class: </strong>" + fareInfoList2.FirstOrDefault().TicketTypeClass + "</p>";
                        TicketDetails += "<p><strong>Reservation: </strong>" + fareInfoList2.FirstOrDefault().ReservationRule + "</p>";
                        TicketDetails += "<p><strong>Journey: </strong>" + fareInfoList2.FirstOrDefault().JourneyType + "</p>";

                        TicketDetails += "<p><strong>Refunds & Cancellations: </strong>" + fareInfoList2.FirstOrDefault().RefundCancellationRule + "</p>";
                        TicketDetails += "<p><strong>Changes to travel plans: </strong>" + fareInfoList2.FirstOrDefault().ChangeRule + "</p>";
                        TicketDetails += "<p><strong>Ticket Conditions: </strong>" + fareInfoList2.FirstOrDefault().TicketTypeConditionsDescription + "</p>";
                        TicketDetails += "<p><strong>Break of Journey: </strong>" + fareInfoList2.FirstOrDefault().JourneyBreakRule + "</p>";
                        TicketDetails += "<p><strong>Availability: </strong>" + fareInfoList2.FirstOrDefault().AvailabilityRule + "</p>";
                        TicketDetails += "<p><strong>Ticket on Departure: </strong>" + tod + "</p>";
                        TicketDetails += "<p><strong>Pre-Booking requirement: </strong>" + fareInfoList2.FirstOrDefault().PreBookingRequirement + "</p>";
                        TicketDetails += "<p><strong>Valid out: </strong>" + fareInfoList2.FirstOrDefault().ValidOutRule + "</p>";
                        TicketDetails += "<p><strong>Valid return: </strong>" + fareInfoList2.FirstOrDefault().ValidReturnRule + "</p>";
                        TicketDetails += "<p><strong>Child discount: </strong>" + fareInfoList2.FirstOrDefault().ChildDiscount + "</p>";
                        TicketDetails += "<p><strong>Railcard discounts: </strong>" + fareInfoList2.FirstOrDefault().RailcardDiscount + "</p>";
                    }
                    #endregion
                    div_TermCondition.InnerHtml = TicketDetails;
                }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public EvDeliveryAddressInfo EvolviBillingAddress()
    {
        try
        {
            string address = (!string.IsNullOrEmpty(txtAdd.Text) && txtAdd.Text.Length > 45) ? txtAdd.Text.Substring(0, 45) : txtAdd.Text;
            Guid countryId = Guid.Parse(ddlCountry.SelectedValue.ToString());
            EvDeliveryAddressInfo evAddress = new EvDeliveryAddressInfo
            {
                Address = new string[] { address },
                City = txtCity.Text,
                PostalCode = txtZip.Text,
                State = txtState.Text,
                CountryName = ddlCountry.SelectedItem.ToString(),
                ISOCountryCode = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == countryId).IsoCode,
                Title = new string[] { ddlMr.SelectedItem.ToString().Replace(".", string.Empty) },
                FName = new string[] { txtFirst.Text },
                LName = txtLast.Text
            };
            return evAddress;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return null; }
    }

    public void AddUpdateEvolviBookingChargesInDb(RailBookingResponse response)
    {
        try
        {
            if (Session["P2POrderID"] != null)
            {
                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                P2POrderId = Convert.ToInt64(Session["P2POrderID"]);
                var chargesList = response.BookingChargesInfos != null ? response.BookingChargesInfos.FirstOrDefault().ChargesInfoList != null ? response.BookingChargesInfos.FirstOrDefault().ChargesInfoList.ToList() : null : null;
                var evBookingInfoList = response.BookingReservationInfos != null ? response.BookingReservationInfos.Select(x => x.TPA_ExtensionsBookingInfoList != null ? x.TPA_ExtensionsBookingInfoList : null).FirstOrDefault() : null;
                if (chargesList != null && chargesList.Count > 3 && evBookingInfoList != null)
                {
                    _masterBooking.AddUpdateEvolviCharges(new tblEvolviOtherChargesLookUp
                    {
                        ID = Guid.NewGuid(),
                        SiteId = siteId,
                        OrderId = P2POrderId,
                        TicketCost = chargesList.FirstOrDefault(x => x.ChargeDescription == "Ticket Cost") != null ? Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(chargesList.FirstOrDefault(x => x.ChargeDescription == "Ticket Cost").Amount, siteId, srcCurId, currencyID).ToString("F2")) : 0,
                        Discount = 0, // chargesList.FirstOrDefault(x => x.ChargeDescription == "Discount") != null ? Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(chargesList.FirstOrDefault(x => x.ChargeDescription == "Discount").Amount, siteId, srcCurId, currencyID).ToString("F2")) : 0,
                        TicketChange = 0, // chargesList.FirstOrDefault(x => x.ChargeDescription == "Ticket Transaction Change") != null ? Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(chargesList.FirstOrDefault(x => x.ChargeDescription == "Ticket Transaction Change").Amount, siteId, srcCurId, currencyID).ToString("F2")) : 0,
                        CreditCardCharge = 0, // chargesList.FirstOrDefault(x => x.ChargeDescription == "Credit Card Transaction Charge") != null ? Convert.ToDecimal(FrontEndManagePass.GetPriceAfterConversion(chargesList.FirstOrDefault(x => x.ChargeDescription == "Credit Card Transaction Charge").Amount, siteId, srcCurId, currencyID).ToString("F2")) : 0,
                        BookingRef = evBookingInfoList.OrderRef,
                        BookingItemRef = evBookingInfoList.OrderItemRef
                    });
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowHideTermsMandatoryText(Guid SiteId)
    {
        try
        {
            var data = _oWebsitePage.GetSiteDatabySiteId(SiteId);
            if (data != null)
            {
                IsStaSite = data.IsSTA.HasValue ? (data.IsSTA.Value == true ? true : false) : false;
                div_Mandatory.Visible = data.IsMandatoryTerm;
                div_NotMandatory.Visible = data.IsMandatoryTermSecond;
                p_MandatoryText.InnerHtml = MandatoryTextFirst = !string.IsNullOrEmpty(data.MandatoryTermText) ? data.MandatoryTermText : "";
                p_NotMandatoryText.InnerHtml = MandatoryTextFirstSecond = !string.IsNullOrEmpty(data.MandatoryTermTextSecond) ? data.MandatoryTermTextSecond : "";
            }
        }
        catch (Exception ex) { }
    }

    public void FillP2PDefaultCountry(Guid SiteId)
    {
        try
        {
            var data = _oWebsitePage.GetSiteDatabySiteId(SiteId);
            if (data != null && data.DefaultP2PCountry.HasValue)
                ddlCountry.SelectedValue = data.DefaultP2PCountry.Value.ToString();
            else
                ddlCountry.SelectedValue = "0";
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void EvolviDefaultCountryShow()
    {
        try
        {
            if (isEvolviBooking)//For evoliv block UK because api return error: “The Country Name of United Kingdom is not allowed for this Agency”
            {
                ListItem itemToSelect = ddlCountry.Items.FindByText("United Kingdom");
                if (itemToSelect != null)
                    ddlCountry.Items.Remove(itemToSelect);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public List<string> GetNotShowCountryList()
    {
        List<string> countryList = new List<string> { "Benin", "Botswana", "Burkina Faso", "Burundi", "Cape Verde", "Comoros", "Djibouti", "Equatorial Guinea", "Eritrea", "Ethiopia", "French Guyana", "French Southern Territoires", "Gabon", "Gambia", "Guinea", "Mauritania", "Mayotte", "Niger", "Saint Helena", "Western Sahara", "Afghanistan", "Azerbaijan", "Bangladesh", "Bhutan", "British Indian Ocean Territory", "Christmas Island", "Cocos (Keeling) Islands", "East Timor", "Korea Democratic People's Republic of", "Kyrgyzstan", "Maldives", "Tajikistan", "Bouvet Island", "Cook Islands", "Guam", "Heard Island & McDonald Island", "Kiribati", "Marshall Islands", "Micronesia", "Nauru", "Niue", "Norfolk Island", "Northern Mariana Islands", "Palau", "Pitcairn Island", "Polynesia (French)", "S. Georgia & S. Sandwich Isls", "Solomon Islands", "Tokelau", "Vanuatu", "Wallis and Futuna", "Aland Islands", "Bosnia", "Faroe Islands", "Gibraltar", "Greece Plus", "Greek Island", "Guernsey", "Isle of Man", "Italy Plus", "Italy Premium", "Jersey", "Russia", "Spain Premium", "Vatican City State", "American Samoa", "Anguilla", "Antigua and Barbuda", "Aruba", "Bahamas", "Belize", "Bermuda", "Bonaire, Sint Eustatius and Saba", "British Virgin Islands", "Cayman Islands", "Dominica", "Grenada", "Guatemala", "Netherlands Antilles", "Saint Kitts and Nevis", "Sin Maarten (Dutch part)", "Turks & Caicos Islands", "Virgin Islands (British)", "Virgin Islands (US)", "Antarctica", "Bolivia", "Falkland Islands", "Guyana", "Svalbard and Jan Mayen", "Cao Tome and Principe", "Reunion", "Saint Barthelemy", "Timor-Leste", "South Georgia and the South Sandwich Islands", "Lesotho", "Saint Martin (Fr)" };
        return countryList;
    }
}