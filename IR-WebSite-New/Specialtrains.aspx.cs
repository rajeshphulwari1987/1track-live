﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

public partial class Specialtrains : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly Masters _master = new Masters();
    private Guid _siteId;
    public string script = "<script></script>";
    public string NextBookingScriptIR, NextBooking;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL = string.Empty;
    public string litSeobreadcrumbs = "";
    public string litSeobreadcrumbsTwo = "";
    private ManageScriptingTag _ManageScriptingTag = new ManageScriptingTag();
    public string IRScriptingTagJs, SpecialTrainName = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
            FillAllSpeciailTrain(_siteId);
            banner.Visible = true;
            if (Page.RouteData.Values["sid"] != null)
            {
                rptSpecialTrain.Visible = false;
                banner.Visible = false;
                Guid id = (Guid)Page.RouteData.Values["sid"];
                ViewState["id"] = id;

                #region Seobreadcrumbs
                var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(id, _siteId);
                if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                    litSeobreadcrumbs = dataSeobreadcrumbs.SourceCode;
                #endregion
            }
            else if (Page.RouteData.Values["PageId"] != null)
            {
                #region Seobreadcrumbs
                Guid id = (Guid)Page.RouteData.Values["PageId"];
                var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(id, _siteId);
                if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                    litSeobreadcrumbsTwo = dataSeobreadcrumbs.SourceCode;
                #endregion
            }

            if (ViewState["id"] != null)
            {
                try
                {
                    var trainId = Guid.Parse(ViewState["id"].ToString());
                    BindSpecialTrainById(trainId);

                }
                catch (Exception ex) { ShowMessage(2, ex.Message); }
            }
            else
                QubitOperationLoad();

            GetScriptingTag();
        }
        FillDayMonthYear();
        PageBanner(_siteId);
        GetTellMeMoreButton();
        if (_siteId == Guid.Parse("302d426f-6c40-4b71-a565-5d89d91c037e"))
           headertext.Visible= chkboxpanel.Visible = DOBpanel.Visible = false;
    }

    public void GetTellMeMoreButton()
    {
        try
        {
            Guid IR_ComSiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
            if (_siteId == IR_ComSiteId)
            {
                NextBookingScriptIR = "<script type='text/javascript'>\n function addgacode()\n{\nga('send', 'event', 'Special Trains', 'Tell me more', '" + Request.Url.ToString().Split('?')[0].Split('#')[0] + "');\n}\n</script>";
                btnSubmit.OnClientClick = "addgacode()";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "RemoveIRcode", "RemoveIRcode();", true);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void PageBanner(Guid siteID)
    {
        try
        {
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
            {

                var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pid.ID && x.SiteID == siteID);
                if (result != null)
                {
                    var url = result.Url;
                    var oPage = _master.GetPageDetailsByUrl(url);

                    //Banner
                    string[] arrListId = oPage.BannerIDs.Split(',');
                    var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                    var list = _master.GetBannerImgByID(idList);
                    int countdata = list.Count();
                    int index = new Random().Next(countdata);
                    var newlist = list.Skip(index).FirstOrDefault();

                    string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
                    if (newlist != null && newlist.ImgUrl != null)
                        img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                    else
                        img_Banner.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            bool Result = _oManageInterRailNew.AddPassQuery(new tblPassQuery
            {
                Id = Guid.NewGuid(),
                Name = txtFName.Text.Trim() + " " + txtLName.Text.Trim(),
                Email = txtEmailAddress.Text.Trim(),
                Phone = txtPhone.Text.Trim(),
                Dob = ddlDay.SelectedItem.Text.Trim() + "-" + ddlMonth.SelectedItem.Text.Trim() + "-" + ddlYear.SelectedItem.Text.Trim(),
                TellUs = txtTellUs.Text.Trim(),
                IsEmail = chkIsActive.Checked ? true : false,
                SiteId = _siteId,
                CreatedOn = DateTime.Now
            });

            if (_siteId == Guid.Parse("302d426f-6c40-4b71-a565-5d89d91c037e"))
            {
                string ToEmail = _oManageInterRailNew.GetEmailAddress(_siteId);
                if (!string.IsNullOrEmpty(ToEmail))
                    SendEmail(_siteId, ToEmail);
            }

            ClearData();
            GetAdsenceUrl();
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ent", "loadevent()", true);
            //ScriptManager.RegisterStartupScript(Page, GetType(), "callvalerror", "callvalerror()", true);
            ShowMessage(1, "Query has been sent successsfully");
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public bool SendEmail(Guid SiteId, string ToEmailAddress)
    {
        try
        {
            const string Subject = "User Comment/Query";
            var body = "<html><head><title></title></head><body><p> User Comment/Query " +
                          "<br /> <br /> User has send comment/query. <br />" +
                          "<br /> User Name : " + txtFName.Text + " " + txtLName.Text + "<br />	Email : " + txtEmailAddress.Text + "" +
                          "<br /> Phone : " + txtPhone.Text.Trim() + "<br />" +
                          "<br/> Comment/Query : " + txtTellUs.Text +
                          "<br /><br />Thanks </p></body></html>";

            if (!String.IsNullOrEmpty(ToEmailAddress))
            {
                var sendEmail = _master.SendContactUsMail(SiteId, Subject, body, txtEmailAddress.Text, ToEmailAddress);
                return sendEmail;
            }
            return false;
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            ShowMessage(2, ex.Message);
            return false;
        }
    }

    public void FillDayMonthYear()
    {
        ddlDay.Items.Insert(0, new ListItem("DD", "0"));
        ddlMonth.Items.Insert(0, new ListItem("MM", "0"));
        ddlYear.Items.Insert(0, new ListItem("YYYY", "0"));
        for (int i = 01; i <= 31; i++)
        {
            ddlDay.Items.Add(i.ToString());
        }
        for (int i = 01; i <= 12; i++)
        {
            ddlMonth.Items.Add(i.ToString());
        }
        for (int i = 1950; i <= 2015; i++)
        {
            ddlYear.Items.Add(i.ToString());
        }
    }

    void ClearData()
    {
        txtFName.Text = string.Empty;
        txtLName.Text = string.Empty;
        txtEmailAddress.Text = string.Empty;
        txtPhone.Text = string.Empty;
        chkIsActive.Checked = false;
        txtTellUs.Text = string.Empty;
        ddlDay.SelectedIndex = 0;
        ddlMonth.SelectedIndex = 0;
        ddlYear.SelectedIndex = 0;
    }

    public void GetAdsenceUrl()
    {
        var Call = _oManageInterRailNew.GetManageAdsenceLinkBySiteId(_siteId, 1);
        if (Call != null)
        {
            Anchor_Call.HRef = Call.Url.ToString();
        }

        var Book = _oManageInterRailNew.GetManageAdsenceLinkBySiteId(_siteId, 2);
        if (Book != null)
        {
            Anchor_Book.HRef = Book.Url.ToString();
        }

        var Pin = _oManageInterRailNew.GetManageAdsenceLinkBySiteId(_siteId, 3);
        if (Pin != null)
        {
            Anchor_Pin.HRef = Pin.Url.ToString();
        }

        var Email = _oManageInterRailNew.GetManageAdsenceLinkBySiteId(_siteId, 4);
        if (Email != null)
        {
            Anchor_Email.HRef = Email.Url.ToString();
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindSpecialTrainById(Guid trainId)
    {
        var result = from sp in _db.tblSpecialTrains
                     where sp.ID == trainId
                     select sp;
        rptSpecTrains.DataSource = result;
        rptSpecTrains.DataBind();
        if (result != null)
            SpecialTrainName = result.FirstOrDefault().Name;
    }

    public void BindSpecialTrainByCountry(string country)
    {
        var countryId = from cm in _db.tblCountriesMsts
                        where cm.CountryName == country
                        select cm;

        var tblCountriesMst = countryId.FirstOrDefault();
        if (tblCountriesMst != null)
        {
            var cid = Guid.Parse(tblCountriesMst.CountryID.ToString());
            var result = _db.tblSpecialTrains.Where(sp => sp.CountryID == cid);
            rptSpecTrains.DataSource = result;
        }
        rptSpecTrains.DataBind();
    }

    public void FillAllSpeciailTrain(Guid SiteID)
    {
        try
        {
            rptSpecialTrain.DataSource = _oWebsitePage.GetSpecialTrainList(SiteID).FirstOrDefault().MenuList.ToList();
            rptSpecialTrain.DataBind();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetScriptingTag()
    {
        try
        {
            IRScriptingTagJs = "";
            var data = _ManageScriptingTag.GetListScriptingTagBySiteId(_siteId);
            if (data != null && data.Where(x => x.IsActive == true).ToList().Count > 0)
            {
                if (_siteId == Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D") && !Request.Url.AbsoluteUri.ToLower().Contains("specialtrains"))
                {
                    var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Pass Inner");
                    if (firstDefault != null)
                    {
                        IRScriptingTagJs = firstDefault.Script;
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##SiteUrl##", siteURL).Replace("##PageUrl1##", siteURL + "specialtrains").Replace("##PageName1##", "Special Trains");
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##CurrentPageName##", SpecialTrainName);
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}