﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Specialtrains.aspx.cs" Inherits="Specialtrains" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=NextBookingScriptIR%>
    <%=IRScriptingTagJs%>
    <style type="text/css">
        a {
            color: #0645ad;
        }

            a:visited {
                color: #0b0080;
            }

        .starail-Banner {
            height: 190px !important;
        }

        .starail-Grid-col {
            display: block !important;
        }

        .starail-Grid--medium {
            max-width: 100% !important;
        }

        .left-banner {
            float: left !important;
            width: 732px !important;
            height: 190px !important;
        }

        .right-banner {
            float: right !important;
            width: 272px !important;
            height: 190px !important;
        }

        .starail-Country-listItem {
            width: 33% !important;
        }

        .starail-Grid {
            padding: 0 !important;
        }

        @media only screen and (min-width: 640px) {
            .starail-Form-row--spaced {
                clear: left;
                margin: 1rem 8rem 1rem 2rem;
            }
        }

        @media only screen and (max-width: 640px) {
            .starail-Country-listItem {
                width: 50% !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .starail-Country-listItem {
                width: 100% !important;
            }
        }
    </style>
    <script type="text/javascript">
        $(function () { setMenuActive("ST"); });</script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlErrSuccess" runat="server">
        <div id="DivSuccess" runat="server" class="success" style="display: none;">
            <asp:Label ID="lblSuccessMsg" runat="server" />
        </div>
        <div id="DivError" runat="server" class="error" style="display: none;">
            <asp:Label ID="lblErrorMsg" runat="server" />
        </div>
    </asp:Panel>
    <asp:Repeater ID="rptSpecTrains" runat="server">
        <ItemTemplate>
            <div class="starail-Grid starail-Grid--mobileFull starail-Grid-col--nopadding">
                <asp:HiddenField ID="lblcn" runat="server" />
                <div class="starail-Grid-col starail-Grid-col--nopadding">
                    <div class="innerbaner starail-Banner">
                        <div class="left-banner">
                            <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("BannerImg")%>'
                                alt='<%#Eval("Alt_Tag")%>' title='<%#Eval("Image_Title")%>' style="height: 190px; width: 100%;" />
                        </div>
                        <div class="right-banner">
                            <img src='<%= ConfigurationManager.AppSettings["HttpAdminHost"] %><%#Eval("CountryImg")%>'
                                alt='<%#Eval("Alt_Tag")%>' title='<%#Eval("Image_Title")%>' style="height: 190px; width: 100%;" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="Breadcrumb">
                <%=litSeobreadcrumbs %>
            </div>
            <div class="starail-Grid starail-Grid--medium starail-u-relative">
                <div class="starail-Grid-col starail-Grid-col--nopadding">
                    <div class="starail-Back starail-u-hideMobile" style="left: 0px;">
                        <a href="/" class="starail-Back-link"><i class="starail-Icon-chevron-left"></i>Back
                            to search results</a>
                    </div>
                    <div class="starail-CalloutBox starail-CalloutBox--mobileFull" style="background-color: transparent;">
                        <h2>Experience the adventure of train travel</h2>
                        <div class="starail-ImageScroller">
                            <div class="starail-ImageScroller-inner starail-u-cf">
                            </div>
                        </div>
                        <h1>
                            <%#Eval("SEOName")==null ? Eval("Name") : Eval("SEOName")%></h1>
                        <p>
                            <%#Eval("Title")%>
                        </p>
                        <div class="clear">
                        </div>
                        <p>
                            <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Description"))%>
                        </p>
                    </div>
                    <hr class="starail-Type-hr--large " />
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="starail-Grid--mobileFull" id="banner" runat="server" visible="false"
        style="padding-bottom: 10px">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="Special Train Journeys" title="Special Train Journeys"
                    id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="Breadcrumb">
                <%=litSeobreadcrumbsTwo%>
            </div>
            <asp:Repeater ID="rptSpecialTrain" runat="server">
                <ItemTemplate>
                    <section class="starail-Country starail-u-cf">
                        <header class="starail-Country-header">
                            <h2 class="starail-Country-title"><%#Eval("Name")%></h2>
                        </header>
                        <ul class="starail-Country-list starail-u-cf">
                        <asp:Repeater ID="rptSubSpecialTrain" runat="server" DataSource='<%#Eval("SubMenuList") %>'>
                              <ItemTemplate>
                                        <li class="starail-Country-listItem"> 
                                         <a href='<%=siteURL%><%#Eval("Url") %>' title='<%#Eval("Name")%>'>
                                                        <%#(Eval("Name").ToString().Length > 25) ? (Eval("Name").ToString().Substring(0, 25) + "...") : (Eval("Name").ToString())%>
                                                        (<%#Eval("CountryCode")%>) </a>
                                             </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                    </section>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="starail-Box starail-Box--whiteMobile">
        <div class="starail-Grid-col">
            <ul class="starail-CtaList starail-u-cf" style="display: none;">
                <li class="starail-CtaList-item"><a href="#" class="starail-CtaList-link starail-CtaList-link--call"
                    id="Anchor_Call" runat="server" target="_blank"><span class="starail-CtaList-iconContainer">
                        <i class="starail-Icon-phone"></i></span><span class="starail-CtaList-content">Give
                            us a call</span> </a></li>
                <li class="starail-CtaList-item"><a href="#" class="starail-CtaList-link starail-CtaList-link--book"
                    id="Anchor_Book" runat="server" target="_blank"><span class="starail-CtaList-iconContainer">
                        <i class="starail-Icon-calendar-tick"></i></span><span class="starail-CtaList-content">Book an appointment</span> </a></li>
                <li class="starail-CtaList-item"><a href="#" class="starail-CtaList-link starail-CtaList-link--pin"
                    id="Anchor_Pin" runat="server" target="_blank"><span class="starail-CtaList-iconContainer">
                        <i class="starail-Icon-map-pin"></i></span><span class="starail-CtaList-content">Find
                            a store</span> </a></li>
                <li class="starail-CtaList-item"><a href="#" class="starail-CtaList-link starail-CtaList-link--email"
                    id="Anchor_Email" runat="server" target="_blank"><span class="starail-CtaList-iconContainer">
                        <i class="starail-Icon-email"></i></span><span class="starail-CtaList-content">Email
                            us</span> </a></li>
            </ul>


            <div class="starail-Box starail-Box--fullMobile starail-ContactForm" id="div_contactus">
                <div class="starail-ContactForm starail-ContactForm--header">
                    <h3 style="padding-top: 10px">Ready for your next adventure?</h3>
                    <p>
                       <span runat="server" id="headertext"> Call us now on <strong>0871 984 7783</strong> for more information, or</span> please give us some details and we'll get in touch:
                    </p>
                </div>
                <div class="starail-Form starail-Form--onBoxNarrow starail-Form--passContact">
                    <div class="starail-Form-row">
                        <label for="starail-firstname" class="starail-Form-label">
                            Name <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtFName" runat="server" CssClass="starail-Form-input" required="required"
                                    placeholder="First name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ValidationGroup="Contact"
                                    Display="Dynamic" ControlToValidate="txtFName" />
                            </div>
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtLName" runat="server" placeholder="Last name" CssClass="starail-Form-input"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label for="starail-email" class="starail-Form-label">
                            Email <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer">
                            <asp:TextBox ID="txtEmailAddress" runat="server" AutoComplete="off" CssClass="starail-Form-input"
                                placeholder="Email" required="required"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ValidationGroup="Contact"
                                Display="Dynamic" ControlToValidate="txtEmailAddress" />
                            <asp:RegularExpressionValidator ID="reqcustvalerror3" Display="Dynamic" runat="server"
                                ControlToValidate="txtEmailAddress" ValidationExpression="^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)|(Email Address)$"
                                ValidationGroup="Contact" />
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label for="starail-phone" class="starail-Form-label">
                            Phone <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div>
                                <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone" MaxLength="15" CssClass="starail-Form-input"
                                    required="required"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqvalerror4" runat="server" ValidationGroup="Contact"
                                    Display="Dynamic" ControlToValidate="txtPhone" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row" runat="server" id="DOBpanel">
                        <label for="starail-dob" class="starail-Form-label">
                            Date of birth <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-selectGroup starail-u-cf">
                                <div>
                                    <asp:DropDownList ID="ddlDay" runat="server" CssClass="starail-Form-select starail-Form-select--small"
                                        required="required">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerror5" runat="server" ControlToValidate="ddlDay"
                                        ValidationGroup="Contact" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="starail-Form-select starail-Form-select--small"
                                        required="required">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerror6" runat="server" ControlToValidate="ddlMonth"
                                        ValidationGroup="Contact" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlYear" runat="server" CssClass="starail-Form-select" required="required">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerror7" runat="server" ControlToValidate="ddlYear"
                                        ValidationGroup="Contact" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label for="starail-comment" class="starail-Form-label">
                            Tell us about your trip <span class="starail-Form-note--block">(include your travel
                                dates if you know them)</span></label>
                        <div class="starail-Form-inputContainer">
                            <asp:TextBox ID="txtTellUs" runat="server" placeholder="Tell Us About" TextMode="MultiLine"
                                Rows="6" CssClass="starail-Form-textarea"></asp:TextBox>
                        </div>
                    </div>
                    <div class="starail-Form-row starail-Form-row--spaced" runat="server" id="chkboxpanel">
                        <div class="starail-Form-spacer starail-u-hideMobile">
                            &nbsp;
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <label>
                                    <span class="starail-Form-fancyCheckbox">
                                        <asp:CheckBox ID="chkIsActive" runat="server" />
                                        I'd like to receive emails.<span><i class="starail-Icon-tick"></i></span> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <asp:Button ID="btnSubmit" runat="server" Text="Tell me more" OnClick="btnSubmit_Click"
                            ValidationGroup="Contact" CssClass="starail-Button starail-Form-button starail-Button--blue starail-Button--fullMobile" />
                        <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="List" ValidationGroup="Contact"
                            ShowMessageBox="false" ShowSummary="false" runat="server" />
                    </div>
                </div>
            </div>
    </div>
    </div>
    <script type="text/javascript">
        function RemoveIRcode() {
            $(window).load(function () {
                $("#MainContent_btnSubmit").removeAttr('data-ga-category data-ga-action data-ga-label');
            });
        }
    </script>
</asp:Content>
