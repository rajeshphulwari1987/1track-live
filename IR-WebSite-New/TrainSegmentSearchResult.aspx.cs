﻿using System;
using Business;

public partial class TrainSegmentSearchResult : System.Web.UI.Page
{
    public string siteURL;
    private Guid _siteId;
    
    readonly Masters _master = new Masters();
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
      
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _siteId = Guid.Parse(Session["siteId"].ToString());
        siteURL = new ManageFrontWebsitePage().GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
    }
     
}