﻿using System;
using Business;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Globalization;
using Newtonsoft.Json;
using System.Text;
using System.Net;

public partial class Site : System.Web.UI.MasterPage
{
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageSeo _master = new ManageSeo();
    readonly db_1TrackEntities db = new db_1TrackEntities();
    readonly ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
    public Guid pageID, _siteId;
    public string AgentName = "Agent Login ", SiteLogoUrl = string.Empty;
    public string siteURL, NumOfProducts, AttentionTitle, AttentionMsg, AttentionImg = string.Empty;
    public bool chkBookingCond = false;
    public bool chkConditions = false;
    public bool chkPrivacy = false;
    public string MetaKeyTitile = "International Rail";
    public string CurrentPagePath = string.Empty;
    public string MetaKeywords = string.Empty;
    public string MetaDescription = string.Empty;
    public string mobileHeader, header, headerCss, footer = string.Empty;
    public string cssUrl = string.Empty;
    public string js = string.Empty;
    public string MyCart = string.Empty;
    public string b2bSitePhone = string.Empty;
    public string b2bSiteHeading = string.Empty;
    public string b2bSiteTitle = string.Empty;
    public string FaviconIcon = string.Empty;
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public string IrCss = string.Empty;
    public int BucketItem = 0;
    public bool IsLoggedIn = false;
    public bool IsTravelcut = false;
    public bool IsTravelAgentLi = true;
    public bool IsAttentionVisible = false;
    public string HrefLang = string.Empty;
    public string partnerjs = string.Empty;
    public string UserLoginHtml = string.Empty;
    public string STAUserLoginHtml = string.Empty;
    public string IRScriptingTagJs = string.Empty;
    public string IRInnerScriptingTagJs = string.Empty;
    private ManageScriptingTag _ManageScriptingTag = new ManageScriptingTag();
    public Guid IrAgentSiteID = Guid.Parse("57ab589f-8549-46ca-82e8-4d835c2ff0ac"); // Guid.Parse("98bb7831-068a-4963-9eb8-d7ccab265b4e");

    #region GTM
    public bool HaveGTMCode = false;
    public string VisitTracking = string.Empty;
    public StringBuilder DataGTM = new StringBuilder();
    public tblStaHeaderFooter StaHeaderFooters = new tblStaHeaderFooter();
    public string GoogleTagManager = "";
    public bool IsSTAGTM = false;
    #endregion

    private string[] _RequiredRoles = null;

    public string[] RequiredRoles
    {
        get { return _RequiredRoles; }
        set { _RequiredRoles = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            //Response.Headers.Remove("Cache-Control");
            //Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            //Response.AppendHeader("Pragma", "no-cache");
            //Response.AppendHeader("Expires", "0");
            LoadSite();
            Boolean SiteIsDeleted = _oWebsitePage.GetSiteIsDeleted(_siteId);
            hour1.Visible = hour3.Visible = hour4.Visible = IsAttentionVisible = _oWebsitePage.IsVisibleHourOfSite(_siteId);
           
            if (SiteIsDeleted)
                Response.Redirect("SiteBolcked.aspx");

            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);

            #region for new IR & travel cuts & thefreebirdclub
            if (_oWebsitePage.GetSiteLayoutType(_siteId) == 4)
                cssUrl = "<link href='" + siteURL + "assets/css/travelcuts.css' rel='stylesheet' type='text/css' />";
            else if (_oWebsitePage.GetSiteLayoutType(_siteId) == 9)
                cssUrl = "<link href='" + siteURL + "assets/css/freebirdclub.css' rel='stylesheet' type='text/css' />";
            else if (siteURL.ToLower().Contains("railpass") || siteURL.ToLower().Contains("interrailagentnew") || siteURL.ToLower().Contains("internationalrail") || siteURL.ToLower().Contains("interrailnew") || _oWebsitePage.GetSiteLayoutType(_siteId) == 2 || _oWebsitePage.GetSiteLayoutType(_siteId) == 6)
                cssUrl = "<link href='" + siteURL + "assets/css/internationalrail.css' rel='stylesheet' type='text/css'  />";
            else
                cssUrl = "<link href='" + siteURL + "assets/css/main.css' rel='stylesheet' type='text/css' />";

            #endregion
            SetCookies();
        }
        catch (Exception ex)
        {
            string sUrl = "InvalidUrl.aspx";
            if (!Request.Url.ToString().Contains("www") && Request.Url.Host.ToLower() == "internationalrail.com")
                sUrl = "https://www." + Request.Url.Host + "/";
            Response.Redirect(sUrl);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (AgentuserInfo.UserID != Guid.Empty)
                AgentPageLoad();
            else
                AgentName = "Agent Login";
            // HrefLang = _master.GetHrefLangTag(_siteId);
            if (!Page.IsPostBack)
            {
                var isActiveSite = _oWebsitePage.GetSiteActiveInactiveBySiteId(_siteId);
                if (!isActiveSite)
                    Response.Redirect("UnderConstruction.htm");
                var data = _oWebsitePage.GetSiteDatabySiteId(_siteId);
                if (data != null)
                {
                    if (data.IsAttentionVisible)
                    {
                        #region Attention Section
                        string AttentionTitle, AttentionMsg, AttentionImg, style;
                        style = "style='background: " + (string.IsNullOrEmpty(data.BGColor) ? "#cccccc" : data.BGColor) + "; border: 2px solid " + (string.IsNullOrEmpty(data.BDRColor) ? "#E8E8E8" : data.BDRColor) + "';";
                        AttentionImg = string.IsNullOrEmpty(data.AttentionImg) ? siteURL + "images/Seasonal.jpg" : AdminSiteUrl + data.AttentionImg;
                        AttentionTitle = string.IsNullOrEmpty(data.AttentionTitle) ? string.Empty : data.AttentionTitle.Replace("\"", "'");
                        AttentionMsg = string.IsNullOrEmpty(data.AttentionMsg) ? string.Empty : (data.AttentionMsg.Replace("\"", "'"));
                        string HtmlData = "<div class='seasonal' onmouseover='seasonalshow()'><img  " + style + " class='seasonalimg' src='" + AttentionImg + "' /><span  " + style + ">" + AttentionTitle + "</span><div class='divs' onmouseover='seasonalshow()'></div><div class='seasonalmessage' " + style + " onmouseover='seasonalshow()'>" + AttentionMsg + "</div></div>";
                        string json = JsonConvert.SerializeObject(HtmlData);
                        Session.Add("AttentionBody", json);
                        #endregion
                    }
                    else
                        Session["AttentionBody"] = null;

                    hdnIsSta.Value = data.IsSTA.Value ? "True" : "False";
                }
                ShowAd75Menu();
                BindSEOdetail();
                GetDetailsAnalyticTags();
                GetScriptingTag();

                if (_siteId == Guid.Parse("302d426f-6c40-4b71-a565-5d89d91c037e"))
                {
                    div_SocialLink.Visible = false;
                    Copyright.InnerHtml = "© Copyright International Rail Ltd. 2018 - All rights reserved. A company registered in England and Wales, company number: 3060803 with registered offices at International Rail Ltd, Highland House, Mayflower Close, Chandlers Ford, Eastleigh, Hampshire. SO53 4AR.";
                }
            }
            LoadSiteLogo();
            loadAgentfunctionality();
            SetItemOrderCount();
            GetNoOfBucketItem();
            ShowGeneralInfo();
            var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsAgent == false);
            if (objsite != null)
                UserLogin();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UserLogin()
    {
        try
        { 
            Guid IRCOM = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
            //(IRCOM == _siteId?"<a class='userloging' href='EQO/Enquiry'>Enquiry Form</a>|":string.Empty)+
            UserLoginHtml = USERuserInfo.ID == Guid.Empty ? "<a class='userloging' href='" + siteURL + "user/Registration'>Register</a>|<a class='userloging' href='" + siteURL + "user/login'>Sign in</a>" : "<a onmouseover='showusermenu()' onmouseout='hideusermenu()' class='userloging'>My Account</a>|<a class='userloging' href='" + siteURL + "user/login'>Sign Out</a>";
            UserLoginHtml += "<div class='usermenu' onmouseover='showusermenu()' onmouseout='hideusermenu()'><a href='" + siteURL + "user/Orders'>My Orders</a><a href='" + siteURL + "user/UserProfile'>Edit Profile</a> </div>";//<a href='" + siteURL + "user/Orders'>My Orders</a> 
            Session["UserLoginHtml"] = UserLoginHtml;

            STAUserLoginHtml = USERuserInfo.ID == Guid.Empty ? "<a class='userloging' href='" + siteURL + "user/Registration' style='border-right: 1px solid #ccc;'>Register</a> <a class='userloging' href='" + siteURL + "user/login' style='margin-left: -4px;'>Sign in</a>" : "<a onmouseover='showusermenu()' onmouseout='hideusermenu()' class='userloging' style='border-right: 1px solid #ccc;'>My Account</a><a class='userloging' href='" + siteURL + "user/login'>Sign Out</a>";
            STAUserLoginHtml += "<div class='usermenu' onmouseover='showusermenu()' onmouseout='hideusermenu()'><a href='" + siteURL + "user/Orders'>My Orders</a><a href='" + siteURL + "user/UserProfile'>Edit Profile</a> </div>";//<a href='" + siteURL + "user/Orders'>My Orders</a> 
        }
        catch (Exception ex) { throw ex; }
    }

    public void GetDetailsAnalyticTags()
    {
        try
        {
            StaHeaderFooters = db.tblStaHeaderFooters.FirstOrDefault(x => x.SiteID == _siteId);
            if (StaHeaderFooters != null)
            {
                IsSTAGTM = true;
                HaveGTMCode = true;
                Session["GaTaggingProductlist"] = null;
                VisitTracking = GetNewGTM();
                GoogleTagManager = GetGoogleTagManager();
            }
            ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
            var data = _masterAnalyticTag.GetTagBySiteId(_siteId);
            if (data != null)
            {
                HaveGTMCode = true;
                Guid IR_ComSiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
                if (_siteId == IR_ComSiteId && Request.RawUrl.ToLower().Contains("ordersuccesspage"))
                {
                    VisitTracking = "\n<script type='text/javascript'>\n/*VisitTracking*/\n" + data.VisitTracking + Session["PurchaseTransactionTags"] + "\n</script>\n";
                    Session.Remove("PurchaseTransactionTags");
                }
                else
                    VisitTracking = "\n<script type='text/javascript'>\n/*VisitTracking*/\n" + data.VisitTracking + "\n</script>\n";
            }
        }
        catch (Exception ex) { throw ex; }
    }

    public void SetItemOrderCount()
    {
        if (Session["RailPassData"] == null && AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
            GetOrderinBookingCart();
        string linkURL = siteURL;
        int countpass = 0;
        if (Session["RailPassData"] != null)
        {
            var lstRP = Session["RailPassData"] as List<getRailPassData>;
            countpass = lstRP.Count;
            linkURL = siteURL + "BookingCart";
        }
        if (countpass < 1)
            MyCart = "<a href='" + linkURL + "' style='padding: 2px 17px;padding-left:39px;'><lable style='background:url(" + siteURL + "images/STACartEmp.png)no-repeat;background-size: 25px 22px;position: absolute;width: 25px;height: 25px;margin-left: -25px;'></lable>&nbsp;Cart (" + countpass + ")</a>";
        else
            MyCart = "<a href='" + linkURL + "' style='padding: 2px 17px;padding-left:39px;'><lable style='background:url(" + siteURL + "images/STACart.jpg)no-repeat;background-size: 25px 25px;position: absolute;width: 25px;height: 25px;margin-left: -25px;'></lable>&nbsp;Cart (" + countpass + ")</a>";
    }

    public void GetOrderinBookingCart()
    {
        try
        {
            if (Session["RailPassData"] == null && AgentuserInfo.UserID != Guid.Empty && Session["OrderID"] == null)
            {
                var list = new ManageBooking().GetLastAgentOrderInCreateStatus(AgentuserInfo.UserID);
                if (list != null && list.Count() > 0)
                {
                    Session.Add("RailPassData", list);
                    Session["OrderID"] = list.FirstOrDefault().OrderIdentity;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ShowAd75Menu()
    {
        var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId);
        if (objsite == null)
            return;

        if ((bool)objsite.IsAgent)
        {
            if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
            {
                var AgentId = AgentuserInfo.UserID;
                IrFoc.Visible = aFoclink.Visible = _oWebsitePage.IsFocAD75(AgentId);
                IrFoc.HRef = aFoclink.HRef = siteURL + "?category=FOC-AD75";

                if (objsite.ID == IrAgentSiteID)
                    a_JRF.Visible = true;
            }
        }
    }

    void loadAgentfunctionality()
    {
        try
        {
            partnerjs = "<script src='" + siteURL + "assets/js/jquery.mmenu-partner.js' type='text/javascript'></script>\n<script src='" + siteURL + "assets/js/partner.js' type='text/javascript'></script>";
            notstaaub2b.Visible = staaub2b.Visible = false;
            pnlAgentBottom.Visible = pnlAgentTop.Visible = pnlIrHeader.Visible = pnlIrFooter.Visible = pnlSingaporeHeader.Visible = pnlThailandHeader.Visible = pnlOmegaHeader.Visible = false;
            pnlTravellCutsHeader.Visible = pnlMeritHeader.Visible = false;

            js = "<script src='" + siteURL + "assets/js/main.min.js' type='text/javascript'/>";
            if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 1))  //Sta Agent
            {
                cssUrl = "<link href='" + siteURL + "assets/css/main-agent.css' rel='stylesheet' type='text/css' />";
                js = "<script src='" + siteURL + "assets/js/main.min-agent.js' type='text/javascript'/>";

                AgentRedirect();

                pnlAgentBottom.Visible = pnlAgentTop.Visible = true;
                rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
                rptFooterMenu.DataBind();
            }
            else if (db.tblSites.Any(x => x.ID == _siteId && x.IsWholeSale))  //Sta Wholesale
            {
                cssUrl = "<link href='" + siteURL + "assets/css/main-agent.css' rel='stylesheet' type='text/css' />";
                js = "<script src='" + siteURL + "assets/js/main.min-agent.js' type='text/javascript'/>";
                pnlAgentBottom.Visible = pnlAgentTop.Visible = true;
                rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
                rptFooterMenu.DataBind();

                DivAgentGereet.Visible = !(AgentuserInfo.UserID == Guid.Empty);
            }
            if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 9))  //freebirdclub
            {
                var dd = db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                IrCss = "<link href='" + siteURL + "assets/css/bootstrap.min.css' rel='stylesheet' type='text/css' />";
                IrCss += "<link href='" + siteURL + "assets/css/font-awesome.min.css' rel='stylesheet' type='text/css' />";
                IrHeaderSection();
                IRFooterSection();
                pnlIrHeader.Visible = pnlIrFooter.Visible = true;
                if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 2))
                {
                    AgentRedirect();
                }
                DivIRAgentGereet.Visible = !(AgentuserInfo.UserID == Guid.Empty);
            }
            else if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 2))   //Internaton Rail
            {
                var dd = db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                IrCss = "<link href='" + siteURL + "assets/css/bootstrap.min.css' rel='stylesheet' type='text/css' />";
                IrCss += "<link href='" + siteURL + "assets/css/font-awesome.min.css' rel='stylesheet' type='text/css' />";

                IrHeaderSection();
                IRFooterSection();
                pnlIrHeader.Visible = pnlIrFooter.Visible = true;
                if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 2))
                {
                    AgentRedirect();
                }
                DivIRAgentGereet.Visible = !(AgentuserInfo.UserID == Guid.Empty);
            }
            else if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 4))// travel cuts
            {
                js = "<script src='" + siteURL + "assets/js/main.min-agent.js' type='text/javascript'/>";
                pnlAgentBottom.Visible = pnlTravellCutsHeader.Visible = true;
                rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
                rptFooterMenu.DataBind();
                if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 4))
                {
                    AgentRedirect();
                }
            }
            else if (db.tblSites.Any(x => x.ID == _siteId && (x.LayoutType == 1 || x.LayoutType == 7))) //sta public //Singapore public
            {
                partnerjs = "";
                var headerfooter = db.tblStaHeaderFooters.FirstOrDefault(x => x.SiteID == _siteId);
                #region /**Static Headers for STA**/
                //if (headerfooter != null)
                //{
                //    mobileHeader = headerfooter.MobileHeader;
                //    header = headerfooter.Header.Replace("http://www.statravel.com/static/", "https://www.statravel.com/static/");
                //    footer = headerfooter.Footer.Replace("http://www.statravel.com/static/", "https://www.statravel.com/static/");
                //}
                #endregion

                #region /**MegaNav API**/
                if (headerfooter != null)
                {
                    mobileHeader = headerfooter.MobileHeader;
                    IrCss += "<link href='" + siteURL + "assets/css/font-awesome.min.css' rel='stylesheet' type='text/css'  />";
                    string headerCssUrl = "http://www.statravel.com/meganav/get-top-css-js?division=" + mobileHeader;
                    string headers = "http://www.statravel.com/meganav/get-header?division=" + mobileHeader + "&textSearch=true&liveChat=true&isSecured=false";
                    string footers = "http://www.statravel.com/meganav/get-footer?division=" + mobileHeader + "&affiliationIcons=true";
                    string jquery = "http://www.statravel.com/meganav/get-bottom-js?division=" + mobileHeader + "&validateBrowser=true&jQueryDefault=false";

                    headerCss = GetHttpWebRequest(headerCssUrl);
                    header = GetHttpWebRequest(headers);

                    footer = GetHttpWebRequest(footers);
                    footer += GetHttpWebRequest(jquery);
                }
                #endregion
            }
            else if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 6))  //Thailand
            {
                var dd = db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                IrCss = "<link href='" + siteURL + "assets/css/bootstrap.min.css' rel='stylesheet' type='text/css'  />";
                IrCss += "<link href='" + siteURL + "assets/css/font-awesome.min.css' rel='stylesheet' type='text/css' />";

                IRFooterSection();
                pnlThailandHeader.Visible = pnlIrFooter.Visible = true;
                if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 6))
                {
                    AgentRedirect();
                }
            }
            /*else if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 7))  //Singapore public
       {
           cssUrl = "<link href='" + siteURL + "assets/css/main-agent.css' rel='stylesheet' type='text/css'  />";
           js = "<script src='" + siteURL + "assets/js/main.min-agent.js' type='text/javascript'/>";

           pnlAgentBottom.Visible = pnlSingaporeHeader.Visible = true;
           rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
           rptFooterMenu.DataBind();

           if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 7))
           {
               AgentRedirect();
           }
       }*/
            else if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 5))  //Merit agent
            {
                cssUrl = "<link href='" + siteURL + "assets/css/main-agent.css' rel='stylesheet' type='text/css' />";
                js = "<script src='" + siteURL + "assets/js/main.min-agent.js' type='text/javascript'/>";

                pnlAgentBottom.Visible = pnlMeritHeader.Visible = true;
                rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
                rptFooterMenu.DataBind();

                if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 5))
                {
                    AgentRedirect();
                }
            }
            else if (db.tblSites.Any(x => x.ID == _siteId && x.LayoutType == 8))  //Omega agent
            {
                cssUrl = "<link href='" + siteURL + "assets/css/omegatravel.css' rel='stylesheet' type='text/css' />";
                js = "<script src='" + siteURL + "assets/js/main.min-agent.js' type='text/javascript'/>";

                pnlAgentBottom.Visible = pnlOmegaHeader.Visible = true;
                rptFooterMenu.DataSource = _oWebsitePage.GetFooterMenu(_siteId);
                rptFooterMenu.DataBind();

                if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 8))
                {
                    AgentRedirect();
                }
            }
            var data = _oWebsitePage.GetSiteDetailsById(_siteId);
            if (data != null)
            {
                b2bSitePhone = data.PhoneNumber;
                b2bSiteTitle = data.SiteHeadingTitle;
                b2bSiteHeading = data.SiteHeading;
            }
            if (siteURL.ToLower().Contains("staau.internationalrail.net"))
                staaub2b.Visible = true;
            else
                notstaaub2b.Visible = true;
        }
        catch (Exception ex) { throw ex; }
    }

    public void AgentRedirect()
    {
        Session["deeplinkurl"] = Request.RawUrl;
        if (AgentuserInfo.UserID == Guid.Empty)
            Response.Redirect("~/agent");
    }

    public string GetHttpWebRequest(string url)
    {

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = null;

            if (response.CharacterSet == null)
            {
                readStream = new StreamReader(receiveStream);
            }
            else
            {
                readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
            }

            string data = readStream.ReadToEnd();

            response.Close();
            readStream.Close();
            return data;
        }
        return "";
    }

    private void AgentPageLoad()
    {
        string agentUser = AgentuserInfo.Username;
        var rec = db.tblAdminUsers.FirstOrDefault(x => x.UserName == agentUser);
        string name = rec == null ? "" : rec.Forename;

        if (name == string.Empty)
            name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((AgentuserInfo.UserID == Guid.Empty ? AgentuserInfo.Username : "Agent").ToString().ToLower());
        IsLoggedIn = true;
        AgentName = "Logout";
        lblAgent.Text = name;
        lblIRAgent.Text = name;
        if (rec != null)
        {
            Guid InJob = Guid.Parse("5D3E09AB-AFDB-400D-A7C7-8DBF6F6A5F87");
            IrPrintQueue.Visible = printingAgent.Visible = db.aspnet_Roles.FirstOrDefault(x => x.RoleId == rec.RoleID).IsPrintingAllow;
        }
        IrPrintQueue.HRef = printingAgent.HRef = siteURL + "Printing/PrintingOrders.aspx";
    }

    void LoadSite()
    {
        if (Request.Url.Host == "localhost")
        {
            string url = Request.Url.ToString().ToLower();
            if (url.Contains("1tracknew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1TrackNew/");
            if (url.Contains("uk-public"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/UK-Public/");
            if (url.Contains("fclub-public"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/fclub-Public/");
            else if (url.Contains("omegatravel"))
                _siteId = _oWebsitePage.GetSiteIdByURL(" http://localhost/omegatravel/");
            else if (url.Contains("1trackdemo"))
                _siteId = _oWebsitePage.GetSiteIdByURL(" http://localhost/1TrackDemo/");
            else if (url.Contains("1trackagentnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1trackagentnew/");
            else if (url.Contains("demousagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoUsAgent/");
            else if (url.Contains("demous"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoUs/");
            else if (url.Contains("1trackagentdemo"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1trackagentdemo/");
            else if (url.Contains("demoaus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoAus/");
            else if (url.Contains("interrailnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrailnew/");
            else if (url.Contains("interrailagentnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrailagentnew/");
            else if (url.Contains("interrail"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrail/");
            else if (url.Contains("amex"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/amex/");
            else if (url.Contains("australiap2p"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/australiap2p/");
            else if (url.Contains("travelcuts"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/travelcuts/");
            else if (url.Contains("irrailagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/irrailagent/");
            else if (url.Contains("irrail"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/irrail/");
            else if (url.Contains("sta-singapore"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/sta-singapore/");
            else if (url.Contains("au-public"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/AU-Public/");
            else if (url.Contains("au-agent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/au-agent/");
            else if (url.Contains("stath.internationalrail.net"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/stath.internationalrail.net/");
        }

        else if (Request.Url.Host.Contains("staagent"))
            _siteId = _oWebsitePage.GetSiteIdByURL("http://staagent.projectstatus.in/");
        else if (Request.Url.Host.Contains("sta.projectstatus"))
            _siteId = _oWebsitePage.GetSiteIdByURL("http://sta.projectstatus.in/");
        else if (Request.Url.Host.Contains("travelcuts.projectstatus"))
            _siteId = _oWebsitePage.GetSiteIdByURL("http://travelcuts.projectstatus.in/");
        else if (Request.Url.Host.Contains("travelcutsb2b.projectstatus"))
            _siteId = _oWebsitePage.GetSiteIdByURL("http://travelcutsb2b.projectstatus.in/");
        else
        {
            siteURL = "http://" + Request.Url.Host + "/";
            _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);
            if (_siteId == new Guid())
            {
                siteURL = "https://" + Request.Url.Host + "/";
                _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);
            }
        }
        Session["siteId"] = _siteId;
    }

    public void SetCookies()
    {
        if (Session["siteId"] == null)
            return;

        var cookie = new HttpCookie("CookieCompliance_IR");
        _siteId = Guid.Parse(Session["siteId"].ToString());
        CookiesCollectionValue ckvalue = _oWebsitePage.GetCookiesValue(_siteId);
        cookie.Values["_siteId"] = _siteId.ToString();
        cookie.Values["_curId"] = ckvalue.CurrencyId.ToString();
        cookie.Values["_cuntryId"] = ckvalue.CountryId.ToString();
        cookie.Values["_langId"] = ckvalue.LangId.ToString();
        Response.Cookies.Add(cookie);
    }

    public bool MatchASPXurl(string pagename)
    {
        try
        {
            string surl = (System.IO.Path.GetFileName(pagename)).ToLower();
            List<string> ASPXList = new List<string>();
            ASPXList.Add("trainresults.aspx");
            ASPXList.Add("generalinformation.aspx");
            ASPXList.Add("railpasssection1.aspx");
            ASPXList.Add("railpasssection2.aspx");
            ASPXList.Add("railpasssection3.aspx");
            ASPXList.Add("map.aspx");
            ASPXList.Add("feedback.aspx");
            ASPXList.Add("sitemap.aspx");
            ASPXList.Add("security.aspx");
            ASPXList.Add("faq.aspx");
            ASPXList.Add("railpasses.aspx");
            return ASPXList.Any(t => t.Contains(surl));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void BindSEOdetail()
    {
        var seoType = "page";
        var res = db.tblPages.FirstOrDefault(x => x.SiteID == _siteId && x.PageName.ToUpper().Trim() == "HOME");
        if (res == null)
            return;

        var pageId = (Guid)res.NavigationID;

        string url = Request.Url.ToString().ToLower();
        if (url.LastIndexOf(".aspx") > -1)
        {
            bool IsAspxPage = MatchASPXurl(url);
            url = url.Substring(0, url.LastIndexOf(".aspx"));
            if (IsAspxPage)
                Response.Redirect(url);
        }
        else
            url = url.Split('?')[0];

        if (Request.Url.ToString().Contains("default"))
            CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", url.Contains("default") ? url.Replace("default", "") : url);
        else if (Request.Url.ToString().Contains("?"))
        {
            string[] urls = Request.Url.ToString().ToLower().Split('?');
            /*RailpassDetail page redirction*/
            if (urls[0].Contains("railpassdetail") && Request.QueryString["Id"] != null)
            {
                Guid ProductId = Guid.Parse(Request.QueryString["Id"].ToString());
                var list = _oManageInterRailNew.GetProductByProductId(ProductId, _siteId);
                if (list != null)
                    CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", siteURL + list.Url.ToLower());
            }
            else
                CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", urls[0]);
        }
        else
            CurrentPagePath = string.Format("<link rel=\"canonical\" href=\"{0}\" />", url);

        if (Page.RouteData.Values["PageId"] != null)
        {
            seoType = "page";
            pageId = Guid.Parse(Page.RouteData.Values["PageId"].ToString());
        }
        else if (Page.RouteData.Values["sid"] != null)
        {
            pageId = Guid.Parse(Page.RouteData.Values["sid"].ToString());
            seoType = "specialTrain";
        }
        else if (Page.RouteData.Values["Id"] != null)
        {
            pageId = Guid.Parse(Page.RouteData.Values["Id"].ToString());
            if (db.tblPagesCMS.Any(x => x.ID == pageId))
                seoType = "cms";
            else
                seoType = "country";
        }
        else if (Page.RouteData.Values["cid"] != null)/*category*/
        {
            pageId = Guid.Parse(Page.RouteData.Values["cid"].ToString());
            seoType = "category";
        }
        else if (Page.RouteData.Values["ProductId"] != null)/*product*/
        {
            pageId = Guid.Parse(Page.RouteData.Values["ProductId"].ToString());
            seoType = "product";
        }
        else if (Page.RouteData.Values["PrdId"] != null)/*product deails*/
        {
            pageId = Guid.Parse(Page.RouteData.Values["PrdId"].ToString());
            seoType = "productdetails";
        }
        else if (Request.Url.ToString().ToLower().Contains("countries"))
        {
            var data = db.tblWebMenus.FirstOrDefault(x => x.PageName.ToLower() == "countries.aspx");
            if (data != null)
            {
                pageId = data.ID;
                seoType = "page";
            }
        }
        var result = _master.GetSeoDetails(seoType, _siteId, pageId);
        if (result == null && seoType == "page")
        {
            var absolutepage = Path.GetFileName(Request.Url.AbsolutePath);
            tblWebMenu firstOrDefault;
            if (absolutepage != null && absolutepage.Contains("aspx"))
                firstOrDefault = db.tblWebMenus.FirstOrDefault(x => x.PageName.Contains(absolutepage.Replace("-", "")) && x.IsCorporate == false);
            else
                firstOrDefault = db.tblWebMenus.FirstOrDefault(x => x.Name.Contains(absolutepage.Replace(" ", "").Replace("-", " ")) && x.IsCorporate == false);
            if (firstOrDefault != null)
                pageId = firstOrDefault.ID;
            seoType = "other";
            result = _master.GetSeoDetails(seoType, _siteId, pageId);
        }
        if (result != null)
        {
            MetaDescription = string.Format("<meta name=\"description\" content=\"{0}\" />", result.MetaDescription);
            MetaKeywords = string.Format("<meta name=\"keywords \" content=\"{0}\" />", result.MetaKeyword);
            MetaKeyTitile = result.MetaTitle;
        }
    }

    public void LoadSiteLogo()
    {
        var data = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.FaviconPath))
                FaviconIcon = "<link href='" + AdminSiteUrl + data.FaviconPath + "' rel='shortcut icon' type='image/x-icon' />";

            if (!string.IsNullOrEmpty(data.LogoPath))
            {
                SiteLogoUrl = AdminSiteUrl + data.LogoPath;
                div_HideAgent.Visible = false;
            }
            else
            {
                SiteLogoUrl = siteURL + "assets/img/branding/logo.png";
                div_HideAgent.Visible = true;
            }
        }
    }

    private void IRFooterSection()
    {
        try
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            chkPrivacy = db.tblPrivacyPolicies.Any(x => x.SiteID == _siteId && x.IsActive == true);
            chkConditions = db.tblConditionsofUses.Any(x => x.SiteID == _siteId && x.IsActive == true);
            chkBookingCond = db.tblBookingConditions.Any(x => x.SiteID == _siteId && x.IsActive == true);

            var footerMenu = _oWebsitePage.GetFooterMenu(_siteId);
            if (chkBookingCond == false)
                footerMenu = footerMenu.Where(x => x.Name != "Booking Conditions").ToList();
            if (chkConditions == false)
            {
                footerMenu = footerMenu.Where(x => x.Name != "Conditions of Use").ToList();
            }
            if (chkPrivacy == false)
            {
                footerMenu = footerMenu.Where(x => x.Name != "PrivacyPolicy").ToList();
            }

            var chkContact = db.tblPages.FirstOrDefault(x => x.SiteID == _siteId && x.PageName.Contains("contact"));
            if (chkContact == null)
            {
                footerMenu = footerMenu.Where(x => x.Name != "Contact Us").ToList();
            }
            var chkAbout = db.tblPages.FirstOrDefault(x => x.SiteID == _siteId && x.PageName.Contains("about"));
            if (chkAbout == null)
            {
                footerMenu = footerMenu.Where(x => x.Name != "About Us").ToList();
            }

            if (_siteId == Guid.Parse("302d426f-6c40-4b71-a565-5d89d91c037e"))
            {
                div_SocialLink.Visible = false;
                footerMenu = footerMenu.Where(x => x.Name != "visitengland").ToList();
            }

            rptIrFooterMenu.DataSource = footerMenu.ToList();
            rptIrFooterMenu.DataBind();

            rptFooter.DataSource = _oWebsitePage.GetActiveFooterMeuList(_siteId).OrderBy(x => x.SortOrder);
            rptFooter.DataBind();
        }
        catch (Exception ex) { }
    }

    private void IrHeaderSection()
    {
        try
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            var data = _oWebsitePage.GetSiteDetailsById(_siteId);
            if (data != null)
            {
                ltrSitePhoneNumber.Text = data.PhoneNumber;
                SiteHeadingTitle.InnerText = data.SiteHeadingTitle;
            }
            RptRailPasses.DataSource = _oWebsitePage.GetRailPassesList(_siteId, AgentuserInfo.UserID);
            RptRailPasses.DataBind();
            rptCountry.DataSource = _oWebsitePage.GetCountryNavigationList(_siteId);
            rptCountry.DataBind();
            rptSpecialTrain.DataSource = _oWebsitePage.GetSpecialTrainList(_siteId);
            rptSpecialTrain.DataBind();
        }
        catch (Exception ex) { }
    }

    private void GetNoOfBucketItem()
    {
        List<getRailPassData> list = Session["RailPassData"] as List<getRailPassData>;
        if (list != null && list.Count > 0)
        {
            BucketItem = list.Count();
        }
    }

    #region New Public Site GTM Code Section

    protected string GetNewGTM()
    {
        int OrderID = 0;
        int count = 0;
        bool isvalid = false;
        bool Error404 = false;
        string Step = "";
        string PassType = "";
        string Startdate = DateTime.Now.ToString("dd/MM/yyyy");
        var datax = new tblOrder();
        var datay = new tblOrderDiscount();
        var dataz = new tblOrderBillingAddress();
        string PageName = "Home";
        var P2Plist = GetP2PBookingData();
        switch (StaHeaderFooters.MobileHeader)
        {
            case "uk":
                DataGTM.Append("\n<script src='https://dd6zx4ibq538k.cloudfront.net/smartserve-4593.js'></script>\n");
                break;
            case "us":
                DataGTM.Append("\n<script src='https://dd6zx4ibq538k.cloudfront.net/smartserve-4597.js'></script>\n");
                break;
            case "nz":
                DataGTM.Append("\n<script src='https://dd6zx4ibq538k.cloudfront.net/smartserve-4594.js'></script>\n");
                break;
            case "au":
                DataGTM.Append("\n<script src='https://dd6zx4ibq538k.cloudfront.net/smartserve-4596.js'></script>\n");
                break;
            case "sg":
                DataGTM.Append("");
                break;
        }
        if (Request.Params["req"] != null)
            PassType = "Rail P2P";
        else
            PassType = "Rail Pass";
        if (Session["GTPassSearch"] != null)
        {
            PageName = "Search Results";
        }
        else if (Page.RouteData.Values["ProductId"] != null)
        {
            Guid ProductID = Guid.Parse(Page.RouteData.Values["ProductId"].ToString());
            PageName = db.tblProductNames.FirstOrDefault(x => x.ProductID == ProductID).Name;
        }
        else if (Page.RouteData.Values["PrdId"] != null)
        {
            Guid ProductID = Guid.Parse(Page.RouteData.Values["PrdId"].ToString());
            PageName = "Pass Selection";
        }
        if (Request.Url.AbsolutePath.ToLower().Contains("trainresults.aspx") || Request.Url.AbsolutePath.ToLower().Contains("reservation") || Request.Url.AbsolutePath.ToLower().Contains("seatallocation"))
        {
            PageName = "Ticket Selection";
        }
        else if (Request.Url.AbsolutePath.ToLower().Contains("p2pbookingcart"))
        {
            Step = "1";
            PageName = "TravellerDetails";
            isvalid = true;
        }
        else if (Request.Url.AbsolutePath.ToLower().Contains("bookingcart"))
        {
            Step = "1";
            PageName = "TravellerDetails";
            isvalid = true;
        }
        else if (Request.Url.AbsolutePath.ToLower().Contains("paymentprocess"))
        {
            Step = "2";
            PageName = "PaymentDetails";
            isvalid = true;
        }
        else if (Request.Url.AbsolutePath.ToLower().Contains("ordersuccesspage"))
        {
            Step = "confirm";
            PageName = "Confirmation";
            isvalid = true;
        }
        else if (Request.Url.AbsolutePath.ToLower().Contains("404"))
        {
            Error404 = true;
        }
        if (Session["GTMOrder"] != null && (Step == "2" || Step == "confirm"))
        {
            if (Session["GTMOrder"] != null)
                OrderID = Convert.ToInt32(Session["GTMOrder"]);
            datax = db.tblOrders.FirstOrDefault(x => x.OrderID == OrderID);
            datay = db.tblOrderDiscounts.FirstOrDefault(x => x.OrderId == OrderID);
            dataz = db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == OrderID);
        }
        String ShortCode = db.tblCurrencyMsts.FirstOrDefault(o => o.ID == (db.tblSites.FirstOrDefault(x => x.ID == _siteId).DefaultCurrencyID)).ShortCode;
        DataGTM.Append("<script>\n/*GTM New Code*/\n");
        DataGTM.Append("dataLayer = [{");
        DataGTM.Append("'country':'" + StaHeaderFooters.MobileHeader + "',");
        DataGTM.Append("'currency':'" + ShortCode + "',");
        DataGTM.Append("'environment':'Development',");//'Production', 'Development', 'Staging'
        if (Error404)
            DataGTM.Append("'error': [{'type': 'Page Not Found','code': '404'}]");
        else
            DataGTM.Append("'error':[],");
        DataGTM.Append("'pageType':'Rail',");
        DataGTM.Append("'pageType2':'" + PageName + "',");
        DataGTM.Append("'scenario':'" + PassType + "'");
        bool nocomma = true;
        if (Step == "confirm" && PassType == "Rail Pass")
        {
            if (nocomma)
            {
                DataGTM.Append(",");
                nocomma = false;
            }
            DataGTM.Append("'email':'" + dataz.EmailAddress + "',");
            DataGTM.Append("'coupon':'" + (datay != null ? datay.DiscountCode : "") + "',");
            DataGTM.Append("'couponValue':'" + (datay != null ? (datay.IsPercentage ? datay.OrderDiscountAmount + "" : datay.DiscountAmount + "") : "") + "',");
            DataGTM.Append("'paymentType':'" + datax.Brand + "',");
        }

        #region Pass Type
        if (PassType == "Rail Pass")
        {
            if (Session["GTPassSearch"] != null)
            {
                if (nocomma)
                {
                    DataGTM.Append(",");
                    nocomma = false;
                }
                var searchdata = Session["GTPassSearch"] as Business.ManageAnalyticTag.Pass;
                DataGTM.Append("'searchTxt':'" + searchdata.searchTxt + "',");
                DataGTM.Append("'searchResNo':'" + searchdata.searchResNo + "',");
                Session.Remove("GTPassSearch");
            }

            //Pass Type step 1. BookingCart 2. PaymentProcess 3.Ordersuccesspage
            if (Session["NewRailPassData"] != null && isvalid)
            {
                if (nocomma)
                {
                    DataGTM.Append(",");
                    nocomma = false;
                }
                var list = Session["NewRailPassData"] as List<getRailPassData>;
                DataGTM.Append("'ecommerce':{");
                if (Step == "confirm")
                {
                    Session.Remove("NewRailPassData");
                    var dataO = (from tppsl in db.tblPassP2PSalelookup
                                 join tps in db.tblPassSales on tppsl.PassSaleID equals tps.ID
                                 where tppsl.OrderID == OrderID
                                 select tps).ToList();
                    decimal NetAmount = (dataO.Sum(x => x.Price) + dataO.Sum(x => x.TicketProtection) + (datax.ShippingAmount.HasValue ? datax.ShippingAmount.Value : (dynamic)0) +
                         (datax.BookingFee.HasValue ? datax.BookingFee.Value : (dynamic)0) + datax.AdminFee);
                    DataGTM.Append("'purchase':{");
                    DataGTM.Append("'actionField':{");
                    DataGTM.Append("'id':'" + OrderID + "',");
                    DataGTM.Append("'affiliation':'Rail-" + StaHeaderFooters.MobileHeader + "',");
                    DataGTM.Append("'revenue':'" + NetAmount + "',");
                    DataGTM.Append("'tax':'0.00',");
                    DataGTM.Append("'shipping':'" + datax.ShippingAmount + "',");
                    DataGTM.Append("'coupon':'" + (datay != null ? datay.DiscountCode : "") + "'},");
                }
                else
                {
                    DataGTM.Append("'checkout':{");
                    DataGTM.Append("'actionField':{'step': " + Step + "},");
                }
                DataGTM.Append("'products':[");
                var Newlist = list.GroupBy(t => new { t.PrdtName, t.PrdtId, t.Price, t.ClassName, t.ValidityName, t.TravellerName, t.PassStartDate }).
                    Select(t => new
                    {
                        PrdtName = t.Key.PrdtName,
                        Name = t.Key.PrdtName,
                        PrdtId = t.Key.PrdtId,
                        Price = t.Key.Price,
                        ClassName = t.Key.ClassName,
                        ValidityName = t.Key.ValidityName,
                        Travelername = t.Key.TravellerName,
                        PassStartDate = t.Key.PassStartDate,
                        Count = t.Count()
                    }).ToList();
                foreach (var item in Newlist)
                {
                    count++;
                    DataGTM.Append("{ 'name':'" + item.PrdtName + "',");
                    DataGTM.Append("'id':'" + item.PrdtId + "',");
                    DataGTM.Append("'price':'" + item.Price + "',");
                    DataGTM.Append("'brand':'" + item.ClassName + "',");
                    DataGTM.Append("'category':'Rail/Pass',");
                    DataGTM.Append("'variant':'" + (item.Travelername + "; " + item.ValidityName) + "',");
                    DataGTM.Append("'quantity':" + item.Count + ",");
                    DataGTM.Append("'dimension111':'" + (item.PassStartDate.HasValue ? item.PassStartDate.Value.ToString("dd/MM/yyyy") : "") + "' }");
                    if (Newlist.Count > count)
                        DataGTM.Append(",");
                }
                DataGTM.Append("]}}");
            }
        }
        #endregion

        #region P2P Type
        if (PassType == "Rail P2P")
        {
            var objBruc = new BookingRequestUserControl();
            if (Session["BookingUCRerq"] != null)
                objBruc = Session["BookingUCRerq"] as BookingRequestUserControl;
            if (!string.IsNullOrEmpty(objBruc.FromDetail))// && (Step == "2" || Step == "confirm")
            {
                if (nocomma)
                {
                    DataGTM.Append(",");
                    nocomma = false;
                }
                string classn = (objBruc.ClassValue == 0 ? "All" : objBruc.ClassValue == 1 ? "1st Class" : objBruc.ClassValue == 2 ? "2nd Class" : "");
                string xx = (objBruc.Transfare == 0 ? "Direct Trains only" : objBruc.Transfare == 1 ? "Max. 1 transfer" : objBruc.Transfare == 2 ? "Max. 2 transfers" : objBruc.Transfare == 3 ? "Show all" : "");
                DataGTM.Append("'origin':'" + objBruc.FromDetail + "',");
                DataGTM.Append("'destination':'" + objBruc.ToDetail + "',");
                DataGTM.Append("'outboundDate':'" + objBruc.depdt.ToString("dd/MM/yyyy") + "',");
                DataGTM.Append("'pax':'" + (objBruc.Adults + objBruc.Youths + objBruc.Seniors + objBruc.Boys) + "',");
                DataGTM.Append("'paxAdult':'" + objBruc.Adults + "',");
                DataGTM.Append("'paxU26':'" + objBruc.Youths + "',");
                DataGTM.Append("'paxChild':'" + objBruc.Boys + "',");
                DataGTM.Append("'paxSenior':'" + objBruc.Seniors + "',");
                DataGTM.Append("'class':'" + classn + "',");
                DataGTM.Append("'maxTransfers':'" + xx + "',");
                if (Step == "confirm")
                {
                    DataGTM.Append("'email':'" + dataz.EmailAddress + "',");
                    DataGTM.Append("'coupon':'" + (datay != null ? datay.DiscountCode : "") + "',");
                    DataGTM.Append("'couponValue':'" + (datay != null ? (datay.IsPercentage ? datay.OrderDiscountAmount + "" : datay.DiscountAmount + "") : "") + "',");
                    DataGTM.Append("'paymentType':'" + datax.Brand + "',");
                }
            }
            //P2P Type step 1. BookingCart 2. PaymentProcess 3.Ordersuccesspage
            if (P2Plist != null && P2Plist.Count > 0 && isvalid)
            {
                DataGTM.Append("'ecommerce':{");
                if (Step == "confirm")
                {
                    var dataO = (from tppsl in db.tblPassP2PSalelookup
                                 join tps in db.tblP2PSale on tppsl.PassSaleID equals tps.ID
                                 where tppsl.OrderID == OrderID
                                 select tps).ToList();
                    decimal NetAmount = (dataO.Sum(x => x.Price) + dataO.Sum(x => x.TicketProtection) + (datax.ShippingAmount.HasValue ? datax.ShippingAmount.Value : (dynamic)0) +
                         (datax.BookingFee.HasValue ? datax.BookingFee.Value : (dynamic)0) + datax.AdminFee);
                    DataGTM.Append("'purchase':{");
                    DataGTM.Append("'actionField':{");
                    DataGTM.Append("'id':'" + OrderID + "',");
                    DataGTM.Append("'affiliation':'Rail-" + StaHeaderFooters.MobileHeader + "',");
                    DataGTM.Append("'revenue':'" + NetAmount + "',");
                    DataGTM.Append("'tax':'',");
                    DataGTM.Append("'shipping':'" + datax.ShippingAmount + "',");
                    DataGTM.Append("'coupon':'" + (datay != null ? datay.DiscountCode : "") + "'},");
                }
                else
                {
                    DataGTM.Append("'checkout':{");
                    DataGTM.Append("'actionField':{'step': " + Step + "},");
                }
                DataGTM.Append("'products':[");
                count = 0;
                foreach (var item in P2Plist)
                {
                    count++;
                    string ss = "";
                    if (item.Adult != "0")
                        ss = "Adult";
                    if (item.Youth != "0")
                        ss = ss != "" ? ss + ";Youth" : "Youth";
                    if (item.Children != "0")
                        ss = ss != "" ? ss + ";Children" : "Children";
                    if (item.Senior != "0")
                        ss = ss != "" ? ss + ";Senior" : "Senior";
                    string sname = item.From + " to " + item.To;
                    DataGTM.Append("{ 'name':'" + sname + "',");
                    DataGTM.Append("'id':'" + item.ID + "',");
                    DataGTM.Append("'price':'" + item.Price + "',");
                    DataGTM.Append("'brand':'" + item.Class + "',");
                    DataGTM.Append("'category':'Rail/P2P',");
                    DataGTM.Append("'variant':'" + ss + "',");
                    DataGTM.Append("'quantity':1,");
                    DataGTM.Append("'dimension111':'" + (objBruc != null ? objBruc.depdt.ToString("dd/MM/yyyy") : "") + "' }");
                    if (P2Plist.Count > count)
                        DataGTM.Append(",");
                }
                DataGTM.Append("]}}");
            }
        }
        #endregion
        DataGTM.Append("}];");
        DataGTM.Append("\n</script>\n");
        return DataGTM.ToString();
    }

    public List<tblP2PSale> GetP2PBookingData()
    {
        int P2POrderId = 0;
        if (Session["P2POrderID"] != null)
            P2POrderId = Convert.ToInt32(Session["P2POrderID"]);
        var datax = (from tppsl in db.tblPassP2PSalelookup
                     join tps in db.tblP2PSale on tppsl.PassSaleID equals tps.ID
                     where tppsl.OrderID == P2POrderId
                     select tps).ToList();
        return datax;

    }

    public string GetGoogleTagManager()
    {
        var GTM = new StringBuilder();
        GTM.Append("<!-- Google Tag Mnaager -->");
        GTM.Append("<noscript>\n ");
        GTM.Append("<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MNR6HV' height='0' width='0' style='display: none; visibility: hidden'></iframe>");
        GTM.Append("\n </noscript>");
        GTM.Append("<script type='text/javascript'>\n");
        GTM.Append("(function (w, d, s, l, i) {");
        GTM.Append("w[l] = w[l] || [];");
        GTM.Append("w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });");
        GTM.Append("var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';");
        GTM.Append("j.async = true; j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);");
        GTM.Append("})(window, document, 'script', 'dataLayer', 'GTM-MNR6HV');</script>");
        GTM.Append("<!-- End Google Tag Manager -->");
        return GTM.ToString();
    }

    #endregion

    public void GetScriptingTag()
    {
        try
        {
            IRScriptingTagJs = IRInnerScriptingTagJs = "";
            var data = _ManageScriptingTag.GetListScriptingTagBySiteId(_siteId);
            if (data != null && data.Where(x => x.IsActive == true).ToList().Count > 0)
            {
                if (_siteId == Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D"))
                {
                    string currentUrl = Request.RawUrl.ToLower();
                    if ((currentUrl.Contains("home") || currentUrl == "/1tracknew/" || currentUrl == "/"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Home");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL) : "";
                    }
                    else if (currentUrl.Contains("contact"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Contact Us");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script : "";

                        var firstDefault2 = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRInnerScriptingTagJs = firstDefault2 != null ? firstDefault2.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Contact Us") : "";
                    }
                    else if (currentUrl.Contains("railpasses"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Rail Passes") : "";
                    }
                    else if (currentUrl.Contains("about"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "About Us") : "";
                    }
                    else if (currentUrl.Contains("rail-ticket"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Rail Tickets") : "";
                    }
                    else if (currentUrl.Contains("booking-conditions"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Booking Conditions") : "";
                    }
                    else if (currentUrl.Contains("privacy-policy"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Privacy Policy") : "";
                    }
                    else if (currentUrl.Contains("conditions-of-use"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Conditions Of Use") : "";
                    }
                    else if (currentUrl.Contains("countries"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Countries") : "";
                    }
                    else if (currentUrl.Contains("specialtrains"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Special Trains") : "";
                    }
                    else if (currentUrl.Contains("faq"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Faq") : "";
                    }
                    else if (currentUrl.Contains("feedback"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Feedback") : "";
                    }
                    else if (currentUrl.Contains("security"))
                    {
                        var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Passes");
                        IRScriptingTagJs = firstDefault != null ? firstDefault.Script.Replace("##SiteUrl##", siteURL).Replace("##CurrentPageName##", "Security") : "";
                    }
                }
            }
        }
        catch (Exception ex) { }
    }

    public void ShowGeneralInfo()
    {
        if (Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst.co.uk") || Request.Url.AbsoluteUri.ToLower().Contains("1tracknew"))
        {
            anchor_GeneralInfo.Visible = true;
            anchor_GeneralInfo.HRef = siteURL + "generalinformation";
        }
    }

}
