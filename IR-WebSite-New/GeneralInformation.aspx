﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="GeneralInformation.aspx.cs" Inherits="GeneralInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
    <style type="text/css">
        .starail-Grid--mobileFull
        {
            padding-top: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding">
                <img src='https://1track.internationalrail.net/CMSImages/Homepage-slider-train-1.jpg'
                    class="starail-HomeHero-img dots-header" alt="." border="0" />
            </div>
        </div>
    </div>
    <br />
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
        <h2 id="ContentHead" runat="server">
        </h2>
        <div class="starail-Section starail-Section--nopadding">
            <p>
                <div id="ContentText" runat="server" style="text-align: justify;">
                </div>
            </p>
        </div>
    </div>
</asp:Content>
