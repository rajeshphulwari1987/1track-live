﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="RailPassSection2.aspx.cs"
    Inherits="RailPassSection2" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid-col starail-Grid-col--nopadding" style="padding:0px">
        <div class="innner-banner  starail-u-hideMobile" style="padding-bottom: 3px;">
            <div class="float-lt">
                <div class="float-lt" style="width: 73%">
                    <asp:Image ID="imgBanner" CssClass="scale-with-grid" alt="" border="0" runat="server" 
                        Width="901px" Height="190px" ImageUrl="images/img_inner-banner.jpg" /></div>
                <div class="float-rt" style="width: 27%; text-align: right;">
                    <asp:Image ID="imgMap" CssClass="scale-with-grid" alt="" border="0" runat="server"
                        Height="190px" Width="260px" ImageUrl="images/innerMap.gif" />
                </div>
            </div>
        </div>
        <div class="left-content">
            <div id="footerBlock" runat="server">
                <h1>
                    <asp:Label ID="lblTitle" runat="server" />
                </h1>
                <div style="float: right; width: 288px; padding-left: 10px;">
                    <img id="imgRt" runat="server" border="0" />
                </div>
                <div runat="server" id="divDescription" style="padding: 3px; font-size: 13px;">
                </div>
            </div>
        </div>
        <div class="right-content">
            <div id="divJRF" runat="server">
                <a runat="server" id="trainresults">
                    <div id="rtPannel" runat="server">
                    </div>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
