﻿using System;
using Business;

public partial class PromotionDetails : System.Web.UI.Page
{
    readonly Masters _master = new Masters();
    private Guid _siteId;
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public string script;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    #region PageLoad Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            if (Request.QueryString["id"] != null)
            {
                try
                {
                    BindPromotion(Guid.Parse(Request.QueryString["id"]));
                }
                catch (Exception ex)
                {
                    
                }
            }
        }
    }
    #endregion

    #region UserDefined function
    public void BindPromotion(Guid id)
    {
        var result = _master.GetPromotionByID(id);
        lblTitle.Text = result.Title;
        var imgUrl = result.ImageUrl != "" ? result.ImageUrl : "images/NoproductImage.png";
        imgPromo.ImageUrl = SiteUrl + imgUrl;
        lblDesp.Text = Server.HtmlDecode(result.Description);
    }
    #endregion

     
}