﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PromotionDetails.aspx.cs" Inherits="PromotionDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="innner-banner">
        <div class="bannerLft">
            <div class="banner90">
                Promotion</div>
        </div>
        <div>
            <div class="float-lt" style="width: 73%">
                <img id="MainContent_imgBanner" class="scale-with-grid" alt="" border="0" src="images/img_inner-banner.jpg"
                    style="height: 190px; width: 727px;"></div>
            <div class="float-rt" style="width: 27%; text-align: right;">
                <asp:Image ID="imgPromo" runat="server" Style="background-color: #eee; padding: 53px;
                    margin-top: -2px;" />
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <div class="starail-BookingDetails-form">
        <div class="starail-Form-row">
            <h2>
                <asp:Label ID="lblTitle" runat="server" />
            </h2>
        </div>
        <asp:Label ID="lblDesp" runat="server" />
    </div>
</asp:Content>
