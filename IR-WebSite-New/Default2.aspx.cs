﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http.Formatting;
using System.Net.Http;


public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        subscribeAddress();
    }

    private void subscribeAddress()
    {
        string apiKey = "e36d48b2a685a188975656294392af39-us14"; //your API KEY created by you.
        string dataCenter = "us14";
        string listID = "2b0f424d8f"; //The ListID of the list you want to use.

        SubscribeClassCreatedByMe subscribeRequest = new SubscribeClassCreatedByMe
        {
            email_address = "rajeshphulwari1987@gmail.com",
            status = "subscribed"
        };
        subscribeRequest.merge_fields = new MergeFieldClassCreatedByMe();
        subscribeRequest.merge_fields.FNAME = "Rajesh";
        subscribeRequest.merge_fields.LNAME = "phulwari";

        using (HttpClient client = new HttpClient())
        {
            var uri = "https://" + dataCenter + ".api.mailchimp.com/";
            var endpoint = "3.0/lists/" + listID + "/members";

            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", apiKey);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(uri);

            //NOTE: To avoid the method being 'async' we access to '.Result'
            HttpResponseMessage response = client.PostAsJsonAsync(endpoint, subscribeRequest).Result;//PostAsJsonAsync method serializes an object to 
            //JSON and then sends the JSON payload in a POST request
            //StatusCode == 200
            // -> Status == "subscribed" -> Is on the list now
            // -> Status == "unsubscribed" -> this address used to be on the list, but is not anymore
            // -> Status == "pending" -> This address requested to be added with double-opt-in but hasn't confirmed yet
            // -> Status == "cleaned" -> This address bounced and has been removed from the list

            //System.Net.Http.HttpContent content = new StringContent(xmldata, UTF8Encoding.UTF8, "application/xml");

            //StatusCode == 404
            if ((response.IsSuccessStatusCode))
            {
                //Your code here
            }
        }
    }
    public class SubscribeClassCreatedByMe
    {
        public string email_address { get; set; }
        public string status { get; set; }
        public MergeFieldClassCreatedByMe merge_fields { get; set; }
    }
    public class MergeFieldClassCreatedByMe
    {
        public string FNAME { get; set; }
        public string LNAME { get; set; }
    }
}