﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs"
    MasterPageFile="Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
    <style>
        a{color: #0645ad;}
        a:visited{color: #0b0080;}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img src='<%=adminSiteUrl%><%#Eval("ImgUrl")%>' class="starail-HomeHero-img dots-header"
                    id="img_Banner" runat="server" alt="." border="0" />
            </div>
        </div>
    </div>
    <br />
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
        <h2>
            <div id="ContentHead" runat="server" />
        </h2>
        <div class="starail-Section starail-Section--nopadding">
            <p>
                <div id="ContentText" runat="server" style="text-align: justify;" />
            </p>
        </div>
    </div>
</asp:Content>
