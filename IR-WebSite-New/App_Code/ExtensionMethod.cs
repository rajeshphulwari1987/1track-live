﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtensionMethods
{
    public static class ExtensionMethod
    {
        public static string ToMyLowerCase(this string str)
        {
            return str[0] + str.Substring(1, str.Length - 1).ToLower();
        }
    }
}