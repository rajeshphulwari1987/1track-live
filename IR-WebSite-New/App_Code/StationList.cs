﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Business;
using OneHubServiceRef;
using System.Web;

/// <summary>
/// Summary description for StationList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]

public class StationList : System.Web.Services.WebService
{
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static DateTime curentDateTime;
    public static int CountryCount = 0;
    public ManageBooking _booking = new ManageBooking();
    public static List<getStationList_Result> list = new List<getStationList_Result>();
    public static string[] listCountry = { };
    public static DateTime EvolvicurentDateTime;
    public static List<EvFareInformation> EvFareInformationList = new List<EvFareInformation>();

    public StationList()
    {
        // curentDateTime = DateTime.Now.AddDays(-10);
        if (DateTime.Now > EvolvicurentDateTime)
        {
            EvolvicurentDateTime = DateTime.Now.AddDays(1);
            GetFareInfoList();
        }
        if (DateTime.Now > curentDateTime)
        {
            curentDateTime = DateTime.Now.AddDays(10);
            getStaionList();
        }
        if (_db.GetAllCountryOrContinentBySiteId(System.Web.HttpContext.Current.Session["siteId"].ToString()).Max(t => t.Total) != CountryCount)
        {
            var data = _db.GetAllCountryOrContinentBySiteId(System.Web.HttpContext.Current.Session["siteId"].ToString()).ToList();
            CountryCount = (int)data.Max(t => t.Total);
            listCountry = (String[])data.Select(t => t.Name).ToArray();
        }
    }

    public void getStaionList()
    {
        list = _booking.GetAllStaionList().ToList();
    }

    [WebMethod(EnableSession = true)]
    public string[] getStationsXList(string prefixText, string filter, string station)
    {
        string SearchType = (prefixText.Split('*').Count() > 1) ? prefixText.Split('*')[1] : "";
        prefixText = prefixText.Split('*')[0];
        List<getStationList_Result> listStation = list;
        prefixText = prefixText.Replace("♥", "'");
        List<getStationList_Result> listStation2 = new List<getStationList_Result>();
        string[] resultdata = { };
        if (System.Web.HttpContext.Current.Session != null)
            listStation = CheckActiveAPI(listStation);

        if (listStation != null)
        {
            Guid siteId = Guid.Parse(System.Web.HttpContext.Current.Session["siteId"].ToString());
            if ("668de883-c0e4-47df-bc04-4c4e999a7f5f" == siteId.ToString())
            {
                //Australia P2P site has blocked TI search and below Brussel stations  
                List<string> listBlockedStation = new List<string> { "BEMEI", "BEMER", "BESHU", "BEABT", "BEBCE", "BEBCO", "BEBNO", "BEBQL", "BEBXW", "BECON", "BEDEL" };
                resultdata = listStation.Where(x => x.RailName.Contains("BENE") && !listBlockedStation.Contains(x.StationCode) && x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.StationEnglishName).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
            }
            else
            {
                // Blocked station for public site
                if (_db.tblSites.Any(x => x.IsAgent == false && x.ID == siteId))
                {
                    List<string> listBlockedStation = new List<string> { "GBEBF", "GBASI", "GBLWB", "GBSPX" };
                    resultdata = listStation.Where(x => !listBlockedStation.Contains(x.StationCode) && x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.StationEnglishName).OrderByDescending(x => x.IsFirst).ThenBy(x => x.ShortOrder).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
                }
                else
                    resultdata = listStation.Where(x => x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.StationEnglishName).OrderByDescending(x => x.IsFirst).ThenBy(x => x.ShortOrder).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
            }
            if (resultdata.Length == 0)
                return new string[] { };
            return resultdata;
        }
        else
            return new string[] { };
    }

    //Use For quote Request
    [WebMethod(EnableSession = true)]
    public string[] getStationsQuoteXList(string prefixText, string filter, string station)
    {
        string SearchType = (prefixText.Split('*').Count() > 1) ? prefixText.Split('*')[1] : "";
        prefixText = prefixText.Split('*')[0];
        List<getStationList_Result> listStation = list.Where(x => x.RailName == "BENE").ToList();
        prefixText = prefixText.Replace("♥", "'");
        List<getStationList_Result> listStation2 = new List<getStationList_Result>();
        string[] resultdata = { };

        if (!string.IsNullOrEmpty(filter))
        {
            listStation2.Clear();
            string[] code = filter.Split(',');
            for (int i = 0; i < code.Length; i++)
            {
                var data = list.Where(x => x.StationFilterCode == code[i]).ToList();
                foreach (var item in data)
                {
                    getStationList_Result obj = new getStationList_Result();
                    obj.StationEnglishName = item.StationEnglishName;
                    obj.StationCode = item.StationCode;
                    obj.StationName = item.StationName;
                    obj.StationCodeList = item.StationCodeList;
                    listStation2.Add(obj);
                }
            }
            listStation = listStation2;
        }

        if (System.Web.HttpContext.Current.Session != null)
            listStation = CheckActiveAPI(listStation);

        if (listStation != null)
        {
            if ("8fc709ec-3b2c-4d5a-9401-e70247ce7bb7" == System.Web.HttpContext.Current.Session["siteId"].ToString())//for site station restraction
            {
                var NotShowStation = GetNotShowStationList(SearchType);
                resultdata = listStation.Where(x => x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase) && !NotShowStation.Any(t => x.StationEnglishName.ToLower().Contains(t.Name))).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
            }
            else if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst"))
            {
                List<string> RestrictListToStation = new List<string>();
                if (SearchType == "from")
                {
                    bool HasIn = true;
                    if (station == "London St Pancras Int. (eurostar) (Gb)" || station == "Ashford International (Gb)" || station == "Ebbsfleet  (Gb)")
                    {
                        RestrictListToStation = new List<string> { "Paris Nord (Fr)", "Brussels Midi (International) (Be)", "Lille Europe (Fr)", "Calais Frethun (Fr)" };
                        HasIn = false;
                    }
                    if (station == "Paris Nord (Fr)" || station == "Brussels Midi (International) (Be)" || station == "Lille Europe (Fr)" || station == "Calais Frethun (Fr)")
                    {
                        RestrictListToStation = new List<string> { "Ashford International (Gb)", "Ebbsfleet  (Gb)", "London St Pancras Int. (eurostar) (Gb)" };
                        HasIn = false;
                    }
                    resultdata = listStation.Where(x => RestrictListToStation.Contains(x.StationEnglishName) && x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
                    if (HasIn)
                        resultdata = listStation.Where(x => x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
                }
                else
                    resultdata = listStation.Where(x => x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList).Take(10).ToArray();
            }
            else
            {
                List<string> listBlockedStation = new List<string> { "GBEBF", "GBASI", "GBLWB", "GBSPX" };
                resultdata = listStation.Where(x => !listBlockedStation.Contains(x.StationCode) && x.StationEnglishName.StartsWith(prefixText, StringComparison.InvariantCultureIgnoreCase)).Select(ty => ty.StationEnglishName + "ñ" + ty.StationCodeList + "ñ" + ty.RailName).Take(10).ToArray();
            }
            if (resultdata.Length == 0)
                return new string[] { };
            return resultdata;
        }
        else
            return new string[] { };
    }

    private List<StationName> GetNotShowStationList(string SearchType)
    {
        List<StationName> list = new List<StationName>();
        if (SearchType == "from")
        {
            list.Add(new StationName() { Name = "london" });
            list.Add(new StationName() { Name = "ashford" });
            list.Add(new StationName() { Name = "ebbsflett" });
        }
        if (SearchType == "to")
        {
            list.Add(new StationName() { Name = "ashford" });
            list.Add(new StationName() { Name = "lille" });
            list.Add(new StationName() { Name = "calais" });
            list.Add(new StationName() { Name = "bruxelles" });
            list.Add(new StationName() { Name = "paris" });
            list.Add(new StationName() { Name = "avignon" });
        }
        return list;
    }

    private List<getStationList_Result> CheckActiveAPI(List<getStationList_Result> list)
    {
        try
        {

            if (System.Web.HttpContext.Current.Session.Keys.Count == 0)
                return null;

            db_1TrackEntities db = new db_1TrackEntities();
            Guid siteId = Guid.Parse(System.Web.HttpContext.Current.Session["siteId"].ToString());
            var listApiLogin = db.tblApiLoginDetailSiteLookups.Where(x => x.SiteID == siteId).ToList();


            if (listApiLogin.Count < 1)
                return null;

            bool isBeNe = false;
            bool isTi = false;
            foreach (var item in listApiLogin)
            {
                if (item.TrainName.Trim() == "TrenItalia")
                    isTi = true;

                else if (item.TrainName.Trim() == "BeNe")
                    isBeNe = true;
            }

            if (isTi && !isBeNe)
                return list.Where(x => x.StationCodeList == null).ToList();
            else if (!isTi && isBeNe)
                return list.Where(x => x.StationCodeList != null).ToList();
            else if (isTi && isBeNe)

                return list;



            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [WebMethod(EnableSession = true)]
    public string[] GetCountryList()
    {
        return listCountry;
    }

    [WebMethod(EnableSession = true)]
    public FareDetailsBene[] GetFare()
    {
        FareRequest objrequest = System.Web.HttpContext.Current.Session["farerequest"] as FareRequest;
        var client = new OneHubRailOneHubClient();
        FareDetailsBene[] response = client.GetFare(objrequest);
        System.Web.HttpContext.Current.Session["fareresponse"] = response;
        return response;
    }

    [WebMethod(EnableSession = true)]
    public string GetValidityCodeHtml(string validityCode)
    {
        try
        {
            EvolviValidityCodeRequest request = new EvolviValidityCodeRequest();
            EvolviValidityCodeResponse response = new EvolviValidityCodeResponse();
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            if (!string.IsNullOrEmpty(validityCode))
            {
                request.ValidityCode = validityCode;
                response = client.EvolviValidityCodeInformation(request);
                if (response != null)
                    if (response.ErrorMessage == null)
                        return response.Details;
            }
            return "";
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    public List<EvolviRailCard> GetRailCardList()
    {
        List<EvolviRailCard> RailCardList = new List<EvolviRailCard>();
        RailCardList.AddRange(new ManageBooking().GetRailCardList().ToList().Select(x => new EvolviRailCard { RailCardCode = x.RailCardCode, RailCardName = x.RailCardName }).ToList());
        return RailCardList;
    }

    [WebMethod(EnableSession = true)]
    public void GetFareInfoList()
    {
        EvolviFareInformationRequest request = new EvolviFareInformationRequest();
        EvolviFareInformationResponse response = new EvolviFareInformationResponse();
        OneHubRailOneHubClient client = new OneHubRailOneHubClient();
        request.TicketTypeCode = string.Empty;
        request.TocCode = string.Empty;
        new ManageOneHub().EvolviApiLogin(request, Guid.Parse(System.Web.HttpContext.Current.Session["siteId"].ToString()));
        response = client.EvolviFareInformation(request);
        if (response != null)
            if (response.EvFareInformationList != null && response.EvFareInformationList.Length > 0)
                EvFareInformationList = response.EvFareInformationList.ToList();
    }

    public List<EvFareInformation> EvolviFareInformationList()
    {
        List<EvFareInformation> newEvolviFareList = EvFareInformationList;
        return newEvolviFareList;
    }

    [WebMethod(EnableSession = true)]
    public bool GetTrenitaliaTime()
    {
        if (Session["TiTimer"] != null)
        {
            TimeSpan time1 = new TimeSpan();
            time1 = (DateTime)Session["TiTimer"] - DateTime.Now;
            if (time1.Minutes <= 0 && time1.Seconds <= 0)
            {
                Session["TiTimer"] = null;
                return true;
            }
        }
        else
            Session["TiTimer"] = DateTime.Now.AddMinutes(15);
        return false;
    }

    [WebMethod(EnableSession = true)]
    public string[] GetTrenitaliaApiTimer()
    {
        string[] data = new string[3];
        TimeSpan time1 = new TimeSpan();
        data[0] = "yes";
        if (Session["TiApiTimer"] != null)
        {
            time1 = (DateTime)Session["TiApiTimer"] - DateTime.Now;
            if (!(time1.Minutes <= 0 && time1.Seconds <= 0))
                data[0] = "no";
        }
        else
        {
            Session["TiApiTimer"] = DateTime.Now.AddMinutes(17);
            time1 = (DateTime)Session["TiApiTimer"] - DateTime.Now;
            data[0] = "no";
        }
        data[1] = time1.Minutes.ToString(); data[2] = time1.Seconds.ToString();
        return data;
    }
}

class StationName
{
    public string Name { get; set; }
}

public class EvolviRailCard
{
    public string RailCardCode { get; set; }
    public string RailCardName { get; set; }
}
