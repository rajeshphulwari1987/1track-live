﻿using System;
using System.Web;
using System.IO;
using Business;
namespace OneTrack
{
    public class TxtHandler : IHttpHandler
    {
        private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();

        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }
        /// <summary>
        /// Request to handle txt file for specific site
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            Guid _siteId = _oWebsitePage.GetSiteIdByURL(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/");
            if (_siteId != Guid.Empty)
            {
                var FileName = _oWebsitePage.GetRobotsFilePathBySiteId(_siteId);
                //if siteid not found in tblRobotsFiles
                if (string.IsNullOrEmpty(FileName))
                    FileName = "~/RobotsFile/STA-Robots.txt";

                using (FileStream fileStream = File.OpenRead(HttpContext.Current.Server.MapPath(FileName)))
                {
                    HttpResponse response = context.Response;
                    byte[] buffer;
                    using (BinaryReader br = new BinaryReader(fileStream))
                    {
                        buffer = br.ReadBytes((int)fileStream.Length);
                    }
                    response.Clear();
                    response.Buffer = true;
                    response.ContentType = "text/plain";
                    response.BinaryWrite(buffer);
                    response.Flush();
                    response.End();
                }
            }
        }

        #endregion
    }

    public class XmlHandler : IHttpHandler
    {
        private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();

        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }
        /// <summary>
        /// Request to handle txt file for specific site
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            Guid _siteId = _oWebsitePage.GetSiteIdByURL(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/");
            if (_siteId != Guid.Empty)
            {
                var FileName = _oWebsitePage.GetSitemapFilePathBySiteId(_siteId);
                //if siteid not found in tblRobotsFiles
                if (string.IsNullOrEmpty(FileName))
                    FileName = "~/SiteMapFile/ir-sitemap.xml";

                using (FileStream fileStream = File.OpenRead(HttpContext.Current.Server.MapPath(FileName)))
                {
                    HttpResponse response = context.Response;
                    byte[] buffer;
                    using (BinaryReader br = new BinaryReader(fileStream))
                    {
                        buffer = br.ReadBytes((int)fileStream.Length);
                    }
                    response.Clear();
                    response.Buffer = true;
                    response.ContentType = "text/xml";
                    response.BinaryWrite(buffer);
                    response.Flush();
                    response.End();
                }
            }
        }

        #endregion
    }

}