﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Security.Cryptography;

namespace OgoneIR
{


	public class OgoneOrderRequest : OgoneBase
	{
		private string _ORDERID; //* Your unique order number (merchant reference).
		private int _AMOUNT; //* Amount to be paid MULTIPLIED BY 100
		private string _CURRENCY; //* ISO alpha order currency code
		private string _CARDNO; //* Card/account number.
		private string _ED; //* Expiry date (MM/YY or MMYY).
		private string _COM; // Order description.
		private string _CN; // Customer name.
		private string _EMAIL; // Customer’s email address.
		private string _CVC; //* Card Verification Code
		private string _ECOM_PAYMENT_CARD_VERIFICATION; //Same as CVC.
		private string _OWNERADDRESS; // Customer’s street name and number
		private string _OWNERZIP; // Customer’s ZIP code
		private string _OWNERTOWN; // Customer’s town/city name.
		private string _OWNERCTY; // Customer’s country, e.g. BE, NL, FR, ...
		private string _OWNERTELNO; // Customer’s telephone number
		private TransactionType _OPERATION; //* Defines the type of requested transaction.
		private string _GLOBORDERID; // Reference grouping several orders together
		private string _WITHROOT; // Adds a root element to our XML response. Possible values: ‘Y’ or empty.
		private string _REMOTE_ADDR; // IP address of the customer
		private int _RTIMEOUT; // Request timeout for the transaction (in seconds, value between 30 and 90)
		private ElectronicCommerceIndicator _ECI; // Electronic Commerce Indicator

		//Secure 3D Parameters
		private string _FLAG3D;
		private string _HTTP_ACCEPT;
		private string _HTTP_USER_AGENT;
		private Window3DPageType _WIN3DS;
		private string _ACCEPTURL;
		private string _DECLINEURL;
		private string _EXCEPTIONURL;
		private string _PARAMPLUS;
		private string _COMPLUS;
		private string _LANGUAGE;
        private string _ALIAS;

		public OgoneOrderRequest(string ID, string UserName, string Password, string URL, string OgoneShaInPassPhrase)
			: base(ID, UserName, Password, URL, OgoneShaInPassPhrase)
		{
			SetDefaults();
		}

		public OgoneOrderResponse GenerateOrderRequest()
		{
			string s = IsValid();
			if (s != "")
				throw new Exception(s);
			NameValueCollection NVC = new NameValueCollection();
			NVC.Add("ACCEPTURL", _ACCEPTURL);
            NVC.Add("ALIAS", _ALIAS);
			NVC.Add("AMOUNT", _AMOUNT.ToString());
			NVC.Add("CARDNO", _CARDNO);
			NVC.Add("CN", _CN);
			NVC.Add("COM", _COM);
			NVC.Add("COMPLUS", _COMPLUS);
			NVC.Add("CURRENCY", _CURRENCY);
			NVC.Add("CVC", _CVC);
			NVC.Add("DECLINEURL", _DECLINEURL);
			NVC.Add("ECI", ((int)_ECI).ToString());
			NVC.Add("ECOM_PAYMENT_CARD_VERIFICATION", _ECOM_PAYMENT_CARD_VERIFICATION);
			NVC.Add("HTTP_USER_AGENT", _HTTP_USER_AGENT);
			NVC.Add("OPERATION", _OPERATION.ToString());
			NVC.Add("ORDERID", _ORDERID);
			NVC.Add("OWNERADDRESS", _OWNERADDRESS);
			NVC.Add("OWNERCTY", _OWNERCTY);
			NVC.Add("OWNERTELNO", _OWNERTELNO);
			NVC.Add("OWNERTOWN", _OWNERTOWN);
			NVC.Add("ED", _ED);
			NVC.Add("EMAIL", _EMAIL);
			NVC.Add("EXCEPTIONURL", _EXCEPTIONURL);
			NVC.Add("FLAG3D", _FLAG3D);
			NVC.Add("GLOBORDERID", _GLOBORDERID);
			NVC.Add("HTTP_ACCEPT", _HTTP_ACCEPT);
			NVC.Add("OWNERZIP", _OWNERZIP);
			NVC.Add("PARAMPLUS", _PARAMPLUS);
			NVC.Add("PSPID", base.PSPID);
			NVC.Add("PSWD", base.PSWD);
			NVC.Add("REMOTE_ADDR", _REMOTE_ADDR);
			NVC.Add("RTIMEOUT", _RTIMEOUT.ToString());
			NVC.Add("USERID", base.USERID);
			NVC.Add("WIN3DS", _WIN3DS.ToString());
			NVC.Add("WITHROOT", _WITHROOT);
			string strReturn = base.GenerateRequest(NVC);
			return new OgoneOrderResponse(strReturn);
		}

		public string IsValid()
		{
			return "";
		}

		private void SetDefaults()
		{
			_ORDERID = "";
			_AMOUNT = 0;
			_CURRENCY = "";
			_CARDNO = "";
			_ED = "";
			_COM = "";
			_CN = "";
			_EMAIL = "";
			_CVC = "";
			_ECOM_PAYMENT_CARD_VERIFICATION = "";
			_OWNERADDRESS = "";
			_OWNERZIP = "";
			_OWNERTOWN = "";
			_OWNERCTY = "";
			_OWNERTELNO = "";
			_OPERATION = TransactionType.SAL;
			_GLOBORDERID = "";
			_WITHROOT = "";
			_REMOTE_ADDR = "";
			_RTIMEOUT = 30;
			_ECI = ElectronicCommerceIndicator.ECommerceWithSSLencryption;
            _ALIAS = "";
			//Secure 3D Parameters
			_FLAG3D = "";
			_HTTP_ACCEPT = "";
			_HTTP_USER_AGENT = "";
			_WIN3DS = Window3DPageType.MAINW;
			_ACCEPTURL = "";
			_DECLINEURL = "";
			_EXCEPTIONURL = "";
			_PARAMPLUS = "";
			_COMPLUS = "";
			_LANGUAGE = "";
		}

        public string ALIAS { get { return _ALIAS; } set { _ALIAS = value.Trim(); } }
		public string ORDERID { get { return _ORDERID; } set { _ORDERID = value.Trim(); } }
		public int AMOUNT { get { return _AMOUNT; } set { _AMOUNT = value; } }
		public string CURRENCY { get { return _CURRENCY; } set { _CURRENCY = value.Trim(); } }
		public string CARDNO { get { return _CARDNO; } set { _CARDNO = value.Trim(); } }
		public string ED { get { return _ED; } set { SetExpiryDate(value.Trim()); } }
		public string COM { get { return _COM; } set { _COM = value.Trim(); } }
		public string CN { get { return _CN; } set { _CN = value.Trim(); } }
		public string EMAIL { get { return _EMAIL; } set { _EMAIL = value.Trim(); } }
		public string CVC { get { return _CVC; } set { _CVC = value.Trim(); } }
		public string ECOM_PAYMENT_CARD_VERIFICATION { get { return _ECOM_PAYMENT_CARD_VERIFICATION; } set { _ECOM_PAYMENT_CARD_VERIFICATION = value.Trim(); } }
		public string OWNERADDRESS { get { return _OWNERADDRESS; } set { _OWNERADDRESS = value.Trim(); } }
		public string OWNERZIP { get { return _OWNERZIP; } set { _OWNERZIP = value.Trim(); } }
		public string OWNERTOWN { get { return _OWNERTOWN; } set { _OWNERTOWN = value.Trim(); } }
		public string OWNERCTY { get { return _OWNERCTY; } set { _OWNERCTY = value.Trim(); } }
		public string OWNERTELNO { get { return _OWNERTELNO; } set { _OWNERTELNO = value.Trim(); } }
		public TransactionType OPERATION { get { return _OPERATION; } set { _OPERATION = value; } }
		public string GLOBORDERID { get { return _GLOBORDERID; } set { _GLOBORDERID = value.Trim(); } }
		public string WITHROOT { get { return _WITHROOT; } set { _WITHROOT = value.Trim(); } }
		public string REMOTE_ADDR { get { return _REMOTE_ADDR; } set { _REMOTE_ADDR = value.Trim(); } }
		public int RTIMEOUT { get { return _RTIMEOUT; } set { if (value < 30 || value > 90) throw new Exception("Timeout must be between 30 and 90 seconds"); _RTIMEOUT = value; } }
		public ElectronicCommerceIndicator ECI { get { return _ECI; } set { _ECI = value; } }

		//Secure 3D Parameters
		public string FLAG3D { get { return _FLAG3D; } set { _FLAG3D = value.Trim(); } }
		public string HTTP_ACCEPT { get { return _HTTP_ACCEPT; } set { _HTTP_ACCEPT = value.Trim(); } }
		public string HTTP_USER_AGENT { get { return _HTTP_USER_AGENT; } set { _HTTP_USER_AGENT = value.Trim(); } }
		public Window3DPageType WIN3DS { get { return _WIN3DS; } set { _WIN3DS = value; } }
		public string ACCEPTURL { get { return _ACCEPTURL; } set { _ACCEPTURL = value.Trim(); } }
		public string DECLINEURL { get { return _DECLINEURL; } set { _DECLINEURL = value.Trim(); } }
		public string EXCEPTIONURL { get { return _EXCEPTIONURL; } set { _EXCEPTIONURL = value.Trim(); } }
		public string PARAMPLUS { get { return _PARAMPLUS; } set { _PARAMPLUS = value.Trim(); } }
		public string COMPLUS { get { return _COMPLUS; } set { _COMPLUS = value.Trim(); } }
		public string LANGUAGE { get { return _LANGUAGE; } set { _LANGUAGE = value.Trim(); } }


		public void Set3DSecureParameters(Window3DPageType WindowPageType, string HTTP_ACCEPT, string HTTP_USER_AGENT, string ACCEPTURL, string DECLINEURL, string EXCEPTIONURL, string PARAMPLUS, string COMPLUS, string LANGUAGE, bool Use3DSecure)
		{
			_FLAG3D = "N";// Fixed value to instruct the sysetem to perform 3-D Secure identification if necessary.
			if (Use3DSecure)
				_FLAG3D = "Y";
			_WIN3DS = WindowPageType;
			_HTTP_ACCEPT = HTTP_ACCEPT;
			_HTTP_USER_AGENT = HTTP_USER_AGENT;
			_ACCEPTURL = ACCEPTURL;
			_DECLINEURL = DECLINEURL;
			_EXCEPTIONURL = EXCEPTIONURL;
			_PARAMPLUS = PARAMPLUS;
			_COMPLUS = COMPLUS;
			_LANGUAGE = LANGUAGE;
		}

		private void SetExpiryDate(string ExDate)
		{
			//The expiry date must take the value MMYY or MM/YY
			bool bolValid = false;
			try
			{
				if (ExDate.Length == 4)
				{
					int intMonth = Convert.ToInt16(ExDate.Substring(0, 2));
					int intYear = Convert.ToInt16(ExDate.Substring(2, 2));
					if (intMonth < 13 && intMonth > 0)
						bolValid = true;
				}
				if (ExDate.Length == 5)
				{
					if (ExDate.Substring(2, 1) == "/")
					{
						int intMonth = Convert.ToInt16(ExDate.Substring(0, 2));
						int intYear = Convert.ToInt16(ExDate.Substring(3, 2));
						if (intMonth < 13 && intMonth > 0)
							bolValid = true;
					}
				}
				if (bolValid == false)
					throw new Exception();
				_ED = ExDate;
			}
			catch
			{
				throw new Exception("Invalid Expiry Date for " + ExDate);
			}
		}

	}

	public enum TransactionType
	{
		/// <summary>
		/// request for authorization
		/// </summary>
		RES,
		/// <summary>
		/// request for direct sale
		/// </summary>
		SAL,
		/// <summary>
		/// refund, not linked to a previous
		/// </summary>
		RFD
	}

	public enum ElectronicCommerceIndicator
	{
		Swiped = 0,
		ManuallyKeyedCardNotPresent = 1,
		RecurringFromMOTO = 2,
		InstalmentPayments = 3,
		ManuallyKeyedCardPresent = 4,
		ECommerceWithSSLencryption = 7,
		RecurringFromECommerce = 9
	}

	public enum Window3DPageType
	{
		/// <summary>
		/// display the identification page in the main window (default value).
		/// </summary>
		MAINW,
		/// <summary>
		/// display the identification page in a pop-up window and return to the main window at the end.
		/// </summary>
		POPUP,
		/// <summary>
		/// display the identification page in a pop-up window and remain in the pop-up window.
		/// </summary>
		POPIX
	}

}