﻿using System.Collections.Specialized;

namespace OgoneIR
{


	public class OgoneQueryRequest : OgoneBase
	{
		public OgoneQueryRequest(string ID, string UserName, string Password, string URL, string OgoneShaInPassPhrase)
			: base(ID, UserName, Password, URL, OgoneShaInPassPhrase)
		{

		}

		/// <summary>
		/// </summary>
		/// <param name="MO">Operation Type</param>
		/// <param name="OgonePayId">You can send the PAYID or the orderID to identify the original order. We recommend the use of the PAYID.</param>
		/// <param name="OrderId">You can send the PAYID or the orderID to identify the original order. We recommend the use of the PAYID.</param>
		/// <param name="OgonePayIdSub">You can indicate the history level ID if you use the PAYID to identify the original order (optional).</param>
		public OgoneQueryResponse GenerateQueryRequest(string OgonePayId, string OgonePayIdSub, string OrderId)
		{
			NameValueCollection NVC = new NameValueCollection();
			//N.B. These MUST be in alphabetical order
			NVC.Add("ORDERID", OrderId);
			NVC.Add("PAYID", OgonePayId);
			NVC.Add("PAYIDSUB", OgonePayIdSub);
			NVC.Add("PSPID", base.PSPID);
			NVC.Add("PSWD", base.PSWD);
			NVC.Add("USERID", base.USERID);
			string strReturn = base.GenerateRequest(NVC);
			return new OgoneQueryResponse(strReturn);
		}
	}

}