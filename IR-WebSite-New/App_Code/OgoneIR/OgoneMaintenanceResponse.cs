﻿using System;
using System.Xml;

namespace OgoneIR
{

	/// </summary>
	public class OgoneMaintenanceResponse
	{
		private string _ORDERID;
		private string _PAYID;
		private string _PAYIDSUB;
		private string _ACCEPTANCE;
		private string _STATUS;
		private string _NCERROR;
		private string _NCSTATUS;
		private string _NCERRORPLUS;
		private string _AMOUNT;
		private string _currency;
		private string _PM;
		private string _BRAND;

		private string _SCO_CATEGORY;

		private XmlDocument xDoc;

		public OgoneMaintenanceResponse()
		{
			SetDefaults();
		}

		public OgoneMaintenanceResponse(string Response)
		{
			LoadFromXMLString(Response);
		}

		private void SetDefaults()
		{
			_ORDERID = "";
			_PAYID = "";
			_PAYIDSUB = "";
			_NCSTATUS = "";
			_NCERROR = "";
			_NCERRORPLUS = "";
			_ACCEPTANCE = "";
			_STATUS = "0";
			_SCO_CATEGORY = "";
			_AMOUNT = "";
			_currency = "";
			_PM = "";
			_BRAND = "";
		}

		public void LoadFromXMLString(string Response)
		{
			xDoc = new XmlDocument();
			xDoc.LoadXml(Response);
			_ORDERID = GetValue("orderID");
			_PAYID = GetValue("PAYID");
			_PAYIDSUB = GetValue("PAYIDSUB");
			_ACCEPTANCE = GetValue("ACCEPTANCE");
			_STATUS = GetValue("STATUS");
			_NCERROR = GetValue("NCERROR");
			_NCSTATUS = GetValue("NCSTATUS");
			_NCERRORPLUS = GetValue("NCERRORPLUS");
			_AMOUNT = GetValue("amount");
			_currency = GetValue("currency");
			_PM = GetValue("PM");
			_BRAND = GetValue("BRAND");

			_SCO_CATEGORY = GetValue("SCO_CATEGORY");
		}

		private string GetValue(string Field)
		{
			if (xDoc.DocumentElement.Attributes[Field] != null)
				return xDoc.DocumentElement.Attributes[Field].Value;
			return "";
		}

		/// <summary>
		/// Your order reference
		/// </summary>
		public string ORDERID { get { return _ORDERID; } }
		/// <summary>
		/// Payment reference in Ogone system.
		/// </summary>
		public string PAYID { get { return _PAYID; } }
		/// <summary>
		/// The history level ID of the maintenance operation on the PAYID.
		/// </summary>
		public string PAYIDSUB { get { return _PAYIDSUB; } }
		/// <summary>
		/// First digit of NCERROR.
		/// </summary>
		public string NCSTATUS { get { return _NCSTATUS; } }
		/// <summary>
		/// Error code.
		/// </summary>
		public string NCERROR { get { return _NCERROR; } }
		/// <summary>
		/// Explanation of the error code.
		/// </summary>
		public string NCERRORPLUS { get { return _NCERRORPLUS; } }
		/// <summary>
		/// Acceptance code returned by acquirer.
		/// </summary>
		public string ACCEPTANCE { get { return _ACCEPTANCE; } }
		/// <summary>
		/// Transaction status.
		/// </summary>
		public TransactionResponseStatus STATUS { get { return (TransactionResponseStatus)Convert.ToInt32(_STATUS); } }
		/// <summary>
		/// 
		/// </summary>
		public string SCO_CATEGORY { get { return _SCO_CATEGORY; } }
		/// <summary>
		/// Order amount (not multiplied by 100).
		/// </summary>
		public string Amount { get { return _AMOUNT; } }
		/// <summary>
		/// Order currency.
		/// </summary>
		public string Currency { get { return _currency; } }
		/// <summary>
		/// Payment method.
		/// </summary>
		public string PM { get { return _PM; } }
		/// <summary>
		/// Card brand or similar information for other payment methods.
		/// </summary>
		public string BRAND { get { return _BRAND; } }

		/// <summary>
		/// If a maintenance is requested twice for the same order, the second one will theoretically be declined with an error “50001127” (this order is not authorized), because the initial successful transaction will have changed the order status.
		/// </summary>
		public bool IsDuplicateRequest { get { if (_NCERROR == "50001127")return true; return false; } }
	}

}