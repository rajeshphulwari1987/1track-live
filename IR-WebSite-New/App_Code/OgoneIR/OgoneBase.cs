﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace OgoneIR
{

	/// <summary>
	/// Summary description for OgoneBase
	/// </summary>
	public abstract class OgoneBase
	{
		private string strURL;
		private string _PSPID; //* Your affiliation name in our system.
		private string _USERID; //* Name of your application (API) user.
		private string _PSWD; //* Password of the API user (USERID).
		private string strOgoneShaInPassPhrase;

        public enum SPType
        {
            Ssl3 = 48,
            Tls = 192,
            Tls11 = 768,
            Tls12 = 3072,
        }

		public OgoneBase(string ID, string UserName, string Password, string URL, string OgoneShaInPassPhrase)
		{
			_PSPID = ID;
			_USERID = UserName;
			_PSWD = Password;
			strURL = URL;
			strOgoneShaInPassPhrase = OgoneShaInPassPhrase;
		}

		protected string GenerateRequest(NameValueCollection NVC)
		{
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)SPType.Ssl3 | (SecurityProtocolType)SPType.Tls | (SecurityProtocolType)SPType.Tls11 | (SecurityProtocolType)SPType.Tls12;
			NVC.Add("SHASIGN", OgoneBase.GetShaHash(NVC, strOgoneShaInPassPhrase, true, true));
			WebClient client = new WebClient();
			Uri uri = new Uri(strURL);
			byte[] b = client.UploadValues(uri, "POST", NVC);
			client.Dispose();
			System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
			return enc.GetString(b);
		}

		public static string GetShaHash(NameValueCollection NVC, string OgoneShaPassPhrase, bool SortCollectionAlphabetically, bool RemoveEmptyEntries)
		{
			string strPassPhraseList = ""; // for testing
			NameValueCollection arrSorted = new NameValueCollection();

			//First sort alphabetically and remove empty values
			string[] arrKeys = NVC.AllKeys;
			if (SortCollectionAlphabetically)
				Array.Sort(arrKeys);
			foreach (string s in arrKeys)
			{
				arrSorted.Add(s, NVC[s]);
			}

			NameValueCollection arrRemoved = new NameValueCollection();
			string[] arrKeysTwo = arrSorted.AllKeys;
			foreach (string s in arrKeysTwo)
			{
				if (arrSorted[s] != "")
					arrRemoved.Add(s, arrSorted[s]);
				else if (RemoveEmptyEntries == false)
					arrRemoved.Add(s, arrSorted[s]);
			}

			string strReturn = "";
			foreach (string s in arrRemoved.AllKeys)
			{
				strReturn += s.ToUpper() + "=" + arrRemoved[s] + OgoneShaPassPhrase;
				strPassPhraseList += s.ToUpper() + "=" + arrRemoved[s] + "\r\n";
			}
			SHA1CryptoServiceProvider shash = new SHA1CryptoServiceProvider();
			byte[] buffer = Encoding.UTF8.GetBytes(strReturn);
			strReturn = BitConverter.ToString(shash.ComputeHash(buffer)).Replace("-", "");
			return strReturn;
		}

		public static void WriteNVCToFile(NameValueCollection NVC, string Path)
		{
			string strWrite = "";
			foreach (string s in NVC)
			{
				strWrite += s + "|$|$|" + NVC[s] + "|%|%|";
			}
			File.WriteAllText(Path, strWrite);
		}

		public static NameValueCollection ReadNVCFromFile(string Path)
		{
			string strRead = File.ReadAllText(Path);
			string[] arrValues = strRead.Split(new string[] { "|%|%|" }, StringSplitOptions.RemoveEmptyEntries);
			NameValueCollection NVC = new NameValueCollection();
			foreach (string s in arrValues)
			{
				string[] vals = s.Split(new string[] { "|$|$|" }, StringSplitOptions.None);
				NVC.Add(vals[0], vals[1]);
			}
			return NVC;
		}

		public string URL { get { return strURL; } set { strURL = value; } }
		public string PSPID { get { return _PSPID; } set { _PSPID = value; } }
		public string USERID { get { return _USERID; } set { _USERID = value; } }
		public string PSWD { get { return _PSWD; } set { _PSWD = value; } }
	}

}
