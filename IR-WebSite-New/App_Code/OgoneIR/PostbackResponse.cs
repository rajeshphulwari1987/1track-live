﻿using System;
using System.Collections.Specialized;
using System.Web;

namespace OgoneIR
{

	public class PostbackResponse
	{
		private NameValueCollection _NVC;
		private NameValueCollection _StrippedNVC;
		private string strHashKey;
		private string strHashOutKeyword;
		private bool bolHashMatch;
		private PostbackType enmType;


		private string _orderID;
		private string _currency;
		private string _amount;
		private string _PM;
		private string _ACCEPTANCE;
		private string _STATUS;
		private string _PAYID;
		private string _NCERROR;
		private string _BRAND;
		private string _IPCTY;
		private string _CCCTY;
		private string _ECI;
		private string _CVCCheck;
		private string _AAVCheck;
		private string _VC;

		private bool bolCVVCheck;

		private string _SessionID;
		private string _CustomerId;

		public PostbackResponse(PostbackType pType, string ShaOutKey, NameValueCollection NVC)
		{
			strHashOutKeyword = ShaOutKey;
			_NVC = NVC;
			strHashKey = "";
			bolHashMatch = false;
			SetFormValues(_NVC);
			enmType = pType;
		}

		public PostbackResponse(string ShaOutKey, NameValueCollection NVC)
		{
			strHashOutKeyword = ShaOutKey;
			_NVC = NVC;
			strHashKey = "";
			bolHashMatch = false;
			SetFormValues(_NVC);
			SetPostbackType();
		}

		private void SetPostbackType()
		{

		}

		private void SetDefaults()
		{
			_orderID = "";
			_PAYID = "";
			_NCERROR = "";
			_ACCEPTANCE = "";
			_STATUS = "0";
			_amount = "";
			_currency = "";
			_PM = "";
			_BRAND = "";
			_IPCTY = "";
			_CCCTY = "";
			_ECI = "";
			_CVCCheck = "";
			_AAVCheck = "";
			_VC = "";
			bolCVVCheck = false;
			_SessionID = "";
			_CustomerId = "";
		}

		private void SetFormValues(NameValueCollection NVC)
		{
            try
            {
                _orderID = GetValue("orderID");
                _PAYID = GetValue("PAYID");
                _NCERROR = GetValue("NCERROR");
                _ACCEPTANCE = GetValue("ACCEPTANCE");
                _STATUS = GetValue("STATUS");
                _amount = GetValue("amount");
                _currency = GetValue("currency");
                _PM = GetValue("PM");
                _BRAND = GetValue("BRAND");
                _IPCTY = GetValue("IPCTY");
                _CCCTY = GetValue("CCCTY");
                _ECI = GetValue("ECI");
                _CVCCheck = GetValue("CVCCheck");
                _AAVCheck = GetValue("AAVCheck");
                _VC = GetValue("VC");
                bolCVVCheck = true;
                if (_CVCCheck.ToLower() == "no")
                    bolCVVCheck = false;
                _SessionID = GetValue("SessionID");
                _CustomerId = GetValue("CustomerId");

                switch (this.STATUS)
                {
                    case TransactionResponseStatus.Authorized:
                    case TransactionResponseStatus.Payment_requested:
                    case TransactionResponseStatus.Authorization_waiting:
                        enmType = OgoneIR.PostbackType.Accepted;
                        break;
                    default:
                        enmType = OgoneIR.PostbackType.Declined;
                        break;
                }

                //check hash
                _StrippedNVC = new NameValueCollection();

                //first remove the SHAIGN - we cannot rely on this always being the last value in the collection, so we will iterate through to remove it
                string vals = "";
                foreach (string s in NVC)
                {
                    if (s != "SHASIGN")
                    {
                        _StrippedNVC.Add(s, NVC[s]);
                        vals += s + "=" + NVC[s] + "\r\n";
                    }
                    else
                        strHashKey = NVC[s];
                }

                string strOurHashKey = OgoneBase.GetShaHash(_StrippedNVC, strHashOutKeyword, true, true);
                if (strOurHashKey == strHashKey)
                    bolHashMatch = true;
            }
            catch (Exception expr)
            { 
            
            }
		}

		public string GetView()
		{
			string strReturn = "<table border=\"1\">";
			foreach (string s in _StrippedNVC)
			{
				strReturn += "<tr><td>" + s + "</td><td>" + _StrippedNVC[s] + "</td></tr>";
			}
			strReturn += "</table>";
			return strReturn;
		}

		public string GetValue(string Key)
		{
			foreach (string s in _NVC)
			{
				if (s.ToLower() == Key.ToLower())
				{
					return _NVC[s];
				}
			}
			return "";
		}

		public NameValueCollection NVC { get { return _NVC; } set { _NVC = value; } }
		public bool IsHashOkay { get { return bolHashMatch; } }
		public PostbackType PostbackType { get { return enmType; } }

		public string ORDERID { get { return _orderID; } }
		public string PAYID { get { return _PAYID; } }
		public string NCERROR { get { return _NCERROR; } }
		public string ACCEPTANCE { get { return _ACCEPTANCE; } }
		public TransactionResponseStatus STATUS { get { return (TransactionResponseStatus)Convert.ToInt32(_STATUS); } }
		public string amount { get { return _amount; } }
		public string currency { get { return _currency; } }
		public string PM { get { return _PM; } }
		public string BRAND { get { return _BRAND; } }
		public string IPCTY { get { return _IPCTY; } }
		public string CCCTY { get { return _CCCTY; } }
		public string ECI { get { return _ECI; } }
		public string CVCCheck { get { return _CVCCheck; } }
		public string AAVCheck { get { return _AAVCheck; } }
		public string VC { get { return _VC; } }
		public bool RequiresCVVCheck { get { return bolCVVCheck; } }

		public string SessionID { get { return _SessionID; } }
		public string CustomerId { get { return _CustomerId; } }

	}

	public enum PostbackType
	{
		Accepted,
		Declined,
		Error
	}

}