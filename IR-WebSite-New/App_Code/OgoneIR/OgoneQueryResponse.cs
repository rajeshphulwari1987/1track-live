﻿using System;
using System.Xml;

namespace OgoneIR
{

	public class OgoneQueryResponse
	{
		private string _ORDERID;
		private string _PAYID;
		private string _PAYIDSUB;
		private string _NCSTATUS;
		private string _NCERROR;
		private string _NCERRORPLUS;
		private string _ACCEPTANCE;
		private string _STATUS;
		private string _ECI;
		private string _AMOUNT;
		private string _currency;
		private string _PM;
		private string _BRAND;
		private string _CARDNO;
		private string _IP;

		private XmlDocument xDoc;

		public OgoneQueryResponse()
		{
			SetDefaults();
		}

		public OgoneQueryResponse(string Response)
		{
			LoadFromXMLString(Response);
		}

		private void SetDefaults()
		{
			_ORDERID = "";
			_PAYID = "";
			_PAYIDSUB = "";
			_NCSTATUS = "";
			_NCERROR = "";
			_NCERRORPLUS = "";
			_ACCEPTANCE = "";
			_STATUS = "0";
			_ECI = "";
			_AMOUNT = "";
			_currency = "";
			_PM = "";
			_BRAND = "";
			_CARDNO = "";
			_IP = "";
		}

		public void LoadFromXMLString(string Response)
		{
			xDoc = new XmlDocument();
			xDoc.LoadXml(Response);
			_ORDERID = GetValue("orderID");
			_PAYID = GetValue("PAYID");
			_PAYIDSUB = GetValue("PAYIDSUB");
			_ACCEPTANCE = GetValue("ACCEPTANCE");
			_STATUS = GetValue("STATUS");
			_NCERROR = GetValue("NCERROR");
			_NCSTATUS = GetValue("NCSTATUS");
			_NCERRORPLUS = GetValue("NCERRORPLUS");
			_AMOUNT = GetValue("amount");
			_currency = GetValue("currency");
			_PM = GetValue("PM");
			_BRAND = GetValue("BRAND");
			_ECI = GetValue("ECI");
			_CARDNO = GetValue("CARDNO");
			_IP = GetValue("IP");
		}

		private string GetValue(string Field)
		{
			if (xDoc.DocumentElement.Attributes[Field] != null)
				return xDoc.DocumentElement.Attributes[Field].Value;
			return "";
		}


		/// <summary>
		/// Your order reference
		/// </summary>
		public string ORDERID { get { return _ORDERID; } }
		/// <summary>
		/// Payment reference in Ogone system.
		/// </summary>
		public string PAYID { get { return _PAYID; } }
		/// <summary>
		/// The history level ID of the maintenance operation on the PAYID.
		/// </summary>
		public string PAYIDSUB { get { return _PAYIDSUB; } }
		/// <summary>
		/// First digit of NCERROR.
		/// </summary>
		public string NCSTATUS { get { return _NCSTATUS; } }
		/// <summary>
		/// Error code.
		/// </summary>
		public string NCERROR { get { return _NCERROR; } }
		/// <summary>
		/// Explanation of the error code.
		/// </summary>
		public string NCERRORPLUS { get { return _NCERRORPLUS; } }
		/// <summary>
		/// Acceptance code returned by acquirer.
		/// </summary>
		public string ACCEPTANCE { get { return _ACCEPTANCE; } }
		/// <summary>
		/// Transaction status.
		/// </summary>
		public TransactionResponseStatus STATUS { get { return (TransactionResponseStatus)Convert.ToInt32(_STATUS); } }
		/// <summary>
		/// Electronic Commerce Indicator
		/// </summary>
		public string ECI { get { return _ECI; } }
		/// <summary>
		/// Order amount (not multiplied by 100).
		/// </summary>
		public string Amount { get { return _AMOUNT; } }
		/// <summary>
		/// Order currency.
		/// </summary>
		public string Currency { get { return _currency; } }
		/// <summary>
		/// Payment method.
		/// </summary>
		public string PM { get { return _PM; } }
		/// <summary>
		/// Card brand or similar information for other payment methods.
		/// </summary>
		public string BRAND { get { return _BRAND; } }

		/// <summary>
		/// The masked credit card number.
		/// </summary>
		public string CARDNO { get { return _CARDNO; } }
		/// <summary>
		/// Customer’s IP address, as detected by the Ogone system in a 3-tier integration, or sent to Ogone by the merchant in a 2-tier integration.
		/// </summary>
		public string IP { get { return _IP; } }
	}


}