﻿using System;
using System.Web.Routing;
using Business;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary Description for PageUrls
/// </summary>
public static class PageUrls
{
    #region Page redirect url by ids
    public static string GetRedirectURL(String Id)
    {
        try
        {
            string Result = string.Empty;
            List<System.Web.Routing.RouteBase> list = RouteTable.Routes.ToList();
            foreach (var item in list)
            {
                foreach (var itm in (((System.Web.Routing.Route)(item)).Defaults))
                {
                    var xa = Convert.ToString(((System.Collections.Generic.KeyValuePair<string, object>)itm).Value);
                    if (xa.Contains(Id))
                        Result = (((System.Web.Routing.Route)(item)).Url);
                }
            }
            return Result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    public static string DefaultProductUrl(string lasturl)
    {
        using (var db = new db_1TrackEntities())
        {
            string Newurl = string.Empty;
            string Host = string.Empty;
            string QureryString = (lasturl.Split('?').Count() > 1 ? "?" + lasturl.Split('?')[1] : "");
            Uri uri = new Uri(lasturl);
            if (lasturl.Contains("localhost"))
                Host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + "/1TrackNew/";
            else
                Host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + "/";
            string Name = lasturl.Substring(lasturl.LastIndexOf("/") + 1, lasturl.Length - (lasturl.LastIndexOf("/")) - 1).Split('?')[0].ToLower();
            var PrdtName = db.tblProductNames.FirstOrDefault(x => x.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower() == Name && x.tblProduct.IsActive == true);
            if (PrdtName != null)
            {
                Newurl = Host + PrdtName.ProductID.ToString().Substring(0, 4) + "-" + PrdtName.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower() + QureryString;
            }
            return Newurl;
        }
    }


    public static void DefaultSiteUrl(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            var siteUrl = db.tblSites.ToList();
            var page = db.tblWebMenus.FirstOrDefault(x => x.Name.Contains("home"));
            foreach (var item in siteUrl)
            {
                try
                {
                    if (!String.IsNullOrEmpty(item.SiteURL))
                    {
                        if (RouteTable.Routes[item.SiteURL] == null)
                            if (page != null)
                                routes.MapPageRoute(item.SiteURL, item.SiteURL, "~/" + page.PageName, false, new RouteValueDictionary { { "PageId", page.ID } });
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }
    }

    public static void MasterSiteUrl(RouteCollection routes)
    {
        try
        {
            using (var db = new db_1TrackEntities())
            {
                var oMenu = (from web in db.tblWebMenus
                             join lkp in db.tblWebMenuSiteLookups
                                 on web.ID equals lkp.PageID
                             join st in db.tblSites
                                on lkp.SiteID equals st.ID
                             select new { web, lkp, st }).ToList();

                foreach (var oM in oMenu)
                {
                    try
                    {
                        if (!String.IsNullOrEmpty(oM.web.PageName))
                        {
                            if (RouteTable.Routes[oM.web.PageName] == null)
                                routes.MapPageRoute(oM.lkp.SiteID.ToString() + StringReplace(oM.web.Name), StringReplace(oM.web.Name), "~/" + oM.web.PageName, false, new RouteValueDictionary { { "PageId", oM.web.ID }, { "SiteId", oM.lkp.SiteID } });
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// 301 redirection for old passess
    /// </summary>
    /// <param name="passpatter">old pass name</param>
    /// <returns></returns>
    public static string IsProductExist(string passpatter, string siteurl)
    {
        using (var db = new db_1TrackEntities())
        {
            Guid siteid = Guid.Empty;
            var site = db.tblSites.FirstOrDefault(x => x.SiteURL == siteurl);
            if (site != null)
                siteid = site.ID;

            var resultset = (from tc in db.tblCategories
                             join tcp in db.tblProductCategoriesLookUps on tc.ID equals tcp.CategoryID
                             join tcslkp in db.tblCategorySiteLookUps on tc.ID equals tcslkp.CategoryID
                             join tp in db.tblProducts on tcp.ProductID equals tp.ID
                             join tpn in db.tblProductNames on tp.ID equals tpn.ProductID
                             where tcslkp.SiteID == siteid && tp.IsActive == true && tc.IsActive == true
                             && tp.ProductEnableFromDate <= DateTime.Now && tp.ProductEnableToDate >= DateTime.Now
                             select new
                             {
                                 ProductID = tpn.ProductID,
                                 Name = tpn.Name
                             }).ToList();


            var result = resultset.AsEnumerable().Select(itm => new
            {
                url = itm.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower(),
                urlwithid = itm.ProductID.ToString().Substring(0, 4) + "-" + itm.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower(),
            }).OrderBy(x => x.url).ToList();

            if (result.Any(x => x.urlwithid == passpatter))
                return passpatter; //if same pattern then return
            else
            {
                //else make pattern for pass name and then return runing url
                string newurl = "";
                string passpatternwithoutid = passpatter.Substring(5, passpatter.Length - 5);
                if (result.Any(x => x.url == passpatternwithoutid) || result.Any(x => x.url == passpatter))
                {
                    var record = result.FirstOrDefault(x => x.url == passpatter || x.url == passpatternwithoutid);
                    newurl = record.urlwithid;
                }
                return newurl;
            }
        }
    }


    public static void RailPassCatUrl(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            int catcount = db.tblProductNames.Count();
            ManageRoutes objroute = new ManageRoutes();
            if (objroute.GetRouteCount(Business.Route.CategoryRoute) != catcount)
            {

                var result = db.tblProductNames.ToList();
                foreach (var itm in result)
                {
                    try
                    {
                        string NewpageName = itm.ProductID.ToString().Substring(0, 4) + "-" + itm.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower();
                        routes.MapPageRoute(NewpageName + itm.ProductID.ToString(), NewpageName, "~/railpass-two.aspx", false, new RouteValueDictionary { { "ProductId", itm.ProductID } });
                    }
                    catch
                    {
                    }
                }
                objroute.UpdateRouteCount(Business.Route.CategoryRoute, catcount);
            }//--end top if
        }
    }
    public static void RailPassDetailUrl(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            int prodcount = db.tblProductNames.Count();
            ManageRoutes objroute = new ManageRoutes();

            if (objroute.GetRouteCount(Business.Route.ProductRoute) != prodcount)
            {
                #region Caetgories
                Guid langid = Guid.Parse("72d990df-63b9-4fdf-8701-095fdeb5bbf6");
                var results = from p in db.tblCategoriesNames
                              group p.Name by p.Name into g
                              where g.Count() > 1
                              select new
                              {
                                  CatName = g.Key
                              };



                var result = (from tpn in db.tblProductNames
                              join tp in db.tblProducts on tpn.ProductID equals tp.ID
                              join tc in db.tblProductCategoriesLookUps on tpn.ProductID equals tc.ProductID
                              where tpn.LanguageID == langid
                              select new
                              {
                                  ProductID = tpn.ProductID,
                                  CategoryID = tc.CategoryID,
                                  Category = db.tblCategoriesNames.FirstOrDefault(x => x.CategoryID == tc.CategoryID && x.LanguageID == langid).Name,
                                  Name = tpn.Name
                              }).OrderBy(x => x.Name).AsEnumerable().Select(t => new
                              {
                                  ProductID = t.ProductID,
                                  Name = results.Any(y => y.CatName == t.Category) ? t.CategoryID.ToString().Substring(0, 4) + "-" + t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "") + "/" +
                                  (t.ProductID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower())
                                  : t.Category.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "") + "/" +
                                  (t.ProductID.ToString().Substring(0, 4) + "-" + t.Name.Replace(" - ", "-").Replace("/", "-").Replace("'", "").Replace(" ", "-").Replace("&", "and").ToLower())

                              }).ToList();


                foreach (var itm in result)
                {
                    try
                    {

                        routes.MapPageRoute(itm.Name + itm.ProductID.ToString(), itm.Name, "~/RailpassDetail.aspx", false, new RouteValueDictionary { { "PrdId", itm.ProductID } });
                    }
                    catch
                    {

                    }
                }
                #endregion
                objroute.UpdateRouteCount(Business.Route.ProductRoute, prodcount);
            }
        }
    }
    public static void CountryDetailUrl(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            int catcount = db.tblCountriesMsts.Count();
            ManageRoutes objroute = new ManageRoutes();


            if (objroute.GetRouteCount(Business.Route.CountryRoute) != catcount)
            {

                var result = db.tblCountriesMsts.ToList();
                foreach (var itm in result)
                {
                    try
                    {
                        string NewpageName = itm.CountryID.ToString().Substring(0, 4) + "-" + itm.CountryName.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower();
                        routes.MapPageRoute(NewpageName + itm.CountryID.ToString(), NewpageName, "~/CountryDetail.aspx", false, new RouteValueDictionary { { "Id", itm.CountryID } });
                    }
                    catch
                    { }
                }
                objroute.UpdateRouteCount(Business.Route.CountryRoute, catcount);
            }
        }
    }
    public static void SpecialTrainUrl(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            var result = db.tblSpecialTrains.ToList();
            ManageRoutes objroute = new ManageRoutes();
            if (objroute.GetRouteCount(Business.Route.SpecialTrainRoute) != result.Count())
            {
                foreach (var itm in result)
                {

                    try
                    {
                        string NewpageName = itm.ID.ToString().Substring(0, 4) + "-" + itm.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("&", "and").Replace("'", "").ToLower();
                        routes.MapPageRoute(NewpageName + itm.ID.ToString(), NewpageName, "~/SpecialTrains.aspx", false, new RouteValueDictionary { { "sid", itm.ID } });
                    }
                    catch
                    {

                    }

                }
                objroute.UpdateRouteCount(Business.Route.SpecialTrainRoute, result.Count());
            }
        }
    }


    public static void PagesUrl(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            var result = db.tblPagesCMS.ToList();
            int count = 1;
            foreach (var itm in result)
            {
                try
                {
                    string NewpageName = itm.ID.ToString().Substring(0, 2) + "-" + itm.Name.Replace(" - ", "-").Replace("&", "and").Replace("/", "-").Replace(" ", "-").Replace("'", "").ToLower();
                    routes.MapPageRoute("Pages"+NewpageName, NewpageName, "~/Pages.aspx", false, new RouteValueDictionary { { "id", itm.ID } });
                }
                catch (Exception e)
                {

                }
            }
        }
    }
    public static void TopRailPass(RouteCollection routes)
    {
        using (var db = new db_1TrackEntities())
        {
            var result = db.tblSpecialTrains.ToList();
            foreach (var itm in result)
            {
                try
                {
                    string NewpageName = itm.Name.Replace(" - ", "-").Replace("/", "-").Replace(" ", "-").Replace("'", "").ToLower();

                    routes.MapPageRoute("SpecialTrain" + itm.ID.ToString(), NewpageName, "~/SpecialTrains.aspx", false, new RouteValueDictionary { { "sid", itm.ID } });
                }
                catch
                {

                }
            }
        }
    }
    private static string StringReplace(string strUrl)
    {
        strUrl = strUrl.Replace(" ", "-");
        strUrl = strUrl.Replace("&", "and");
        return strUrl;
    }
    public static List<clsURl> GetOldNewUrl()
    {
        db_1TrackEntities db = new db_1TrackEntities();
        return (from U in db.tblUrlMapings
                join S in db.tblSites on U.SiteId equals S.ID
                where U.IsActive == true// && U.SiteId==SiteId
                select new clsURl { SiteID = S.ID, SiteName = S.DisplayName, OldUrl = U.OldURL, NewURL = U.NewURL, DefaultURL = U.DefaultURL }).ToList();
    }
    public class clsURl
    {
        public Guid SiteID { get; set; }
        public string SiteName { get; set; }
        public string OldUrl { get; set; }
        public string NewURL { get; set; }
        public string DefaultURL { get; set; }
    }
}
