﻿using System;
using OneHubServiceRef;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Business;
using System.Linq;

using System.Web;
using System.Web.UI.WebControls;

public static class BusinessOneHub
{
    public static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;

        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        return isNum;
    }

    public static string CurrencyConverterRate()
    {
        try
        {
            return "";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static string ReadObjectAsString(object o)
    {
        if (o == null)
            return string.Empty;

        XmlSerializer xmlS = new XmlSerializer(o.GetType());
        StringWriter sw = new StringWriter();
        XmlTextWriter tw = new XmlTextWriter(sw);
        xmlS.Serialize(tw, o);
        return sw.ToString();
    }
}

public class ManageOneHub
{
    public void ApiLogin(dynamic request, Guid siteId)
    {
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");
            var resultNTV = api.FirstOrDefault(x => x.TrainName == "NTV");
            var resultEVOLVI = api.FirstOrDefault(x => x.TrainName == "Evolvi");

            request.Header = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,

                ApiAccount = new ApiAccountDetail
                {
                    BeNeApiId = result != null ? result.ID : 0,
                    TiApiId = resultTI != null ? resultTI.ID : 0,
                    NTVApiId = resultNTV != null ? resultNTV.ID : 0,
                    EvolviApiId = resultEVOLVI != null ? resultEVOLVI.ID : 0
                }
            };
        }
    }

    public void TiBeneApiLogin(dynamic request, Guid siteId)
    {
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1).ToList();
        if (api != null)
        {
            var result = api.FirstOrDefault(x => x.TrainName == "BeNe");
            var resultTI = api.FirstOrDefault(x => x.TrainName == "TrenItalia");
            var resultNTV = api.FirstOrDefault(x => x.TrainName == "NTV");
            var resultEVOLVI = api.FirstOrDefault(x => x.TrainName == "Evolvi");

            request.Header.ApiAccount = new ApiAccountDetail
            {
                BeNeApiId = result != null ? result.ID : 0,
                TiApiId = resultTI != null ? resultTI.ID : 0,
                NTVApiId = resultNTV != null ? resultNTV.ID : 0,
                EvolviApiId = resultEVOLVI != null ? resultEVOLVI.ID : 0
            };
        }
    }

    public void GetRequest(BookingRequestUserControl request, string txtDepartureDate, string ddldepTime, string txtReturnDate, string ddlReturnTime)
    {
        var objrequest = new TrainInformationRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            DepartureRailwayCode = request.depRCode,
            DepartureStationCode = request.depstCode,
            ArrivalRailwayCode = request.arrRCode,
            ArrivalStationCode = request.arrstCode,
            Class = request.ClassValue,
            NumAdults = request.Adults,
            NumBoys = request.Boys,
            NumSeniors = request.Seniors,
            NumYouths = request.Youths,
            NumberOfTransfare = request.Transfare,
            IsHaveRailPass = request.isIhaveRailPass,
            IsReturnJourney = request.IsReturnJurney,
            DepartureDate = DateTime.ParseExact(txtDepartureDate, "dd/MMM/yyyy", null),
            DepartureTime = Convert.ToDateTime(ddldepTime),
            RailName = request.OneHubServiceName == "Trenitalia" ? "ITALIA" : request.OneHubServiceName,

            //Evolvi
            TargetType = request.Target,
            SequenceNo = request.SequenceNo,
            MaxResponse = request.MaxResponse,
            ISOCountry = !string.IsNullOrEmpty(request.ISOCountry) ? request.ISOCountry : "",
            ISOCurrency = !string.IsNullOrEmpty(request.ISOCurrency) ? request.ISOCurrency : "",
            EvolviRailCardRequestInfo = request.EvolviRailCardRequestInfoList != null ? request.EvolviRailCardRequestInfoList.ToArray() : null
        };

        if (!string.IsNullOrEmpty(txtReturnDate) && !txtReturnDate.Contains("DD"))
        {
            objrequest.ArrivalDate = DateTime.ParseExact(txtReturnDate, "dd/MMM/yyyy", null);
            objrequest.ArrivalTime = Convert.ToDateTime(ddlReturnTime);
        }
        HttpContext.Current.Session["TrainSearchRequest"] = objrequest;
    }

    public TrainInformationRequest TrainInformation(BookingRequestUserControl request, int flag, string txtDepartureDate, string ddldepTime, string txtReturnDate, string ddlReturnTime)
    {
        GetRequest(request, txtDepartureDate, ddldepTime, txtReturnDate, ddlReturnTime);
        var objrequest = new TrainInformationRequest
        {
            Header = new Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
            },
            DepartureRailwayCode = flag == 1 ? request.depRCode : request.arrRCode,
            DepartureStationCode = flag == 1 ? request.depstCode : request.arrstCode,
            ArrivalRailwayCode = flag == 1 ? request.arrRCode : request.depRCode,
            ArrivalStationCode = flag == 1 ? request.arrstCode : request.depstCode,

            Class = request.ClassValue,
            NumAdults = request.Adults,
            NumBoys = request.Boys,
            NumSeniors = request.Seniors,
            NumYouths = request.Youths,
            NumberOfTransfare = request.Transfare,
            IsHaveRailPass = request.isIhaveRailPass,
            IsReturnJourney = request.IsReturnJurney,
            RailName = request.OneHubServiceName == "Trenitalia" ? "ITALIA" : request.OneHubServiceName,
        };

        //--Departure IF BENE
        objrequest.DepartureDate = request.depdt;
        objrequest.DepartureTime = request.depTime;
        if (request.ReturnDate != string.Empty && flag == 1)
        {
            objrequest.IsReturnJourney = true;
            objrequest.ArrivalDate = DateTime.ParseExact(request.ReturnDate, "dd/MMM/yyyy", null);
            objrequest.ArrivalTime = Convert.ToDateTime(request.ReturnTime);
        }

        //-Return For TI
        if (request.ReturnDate != string.Empty && flag == 2)
        {
            objrequest.IsReturnJourney = true;
            objrequest.DepartureDate = DateTime.ParseExact(request.ReturnDate, "dd/MMM/yyyy", null);
            objrequest.DepartureTime = Convert.ToDateTime(request.ReturnTime);
        }
        return objrequest;
    }

    public int GetEurostarBookingDays(BookingRequestUserControl request)
    {
        List<string> listStCode = new List<string> { "GBSPX", "BEBMI", "FRPNO", "FRPAR", "GBASI", "FRLIL", "FRLLE", "FRMLV", "GBEBF" };
        if (listStCode.Contains(request.depstCode) && listStCode.Contains(request.arrstCode))
            return 180;
        else
            return 0;
    }

    public bool EmptyEvolviBasket(long OrderID, Guid SiteID, bool IsRemoveBasket)
    {
        try
        {
            EvCancelBookingRequest request = new EvCancelBookingRequest();
            EvCancelBookingResponse response = new EvCancelBookingResponse();
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();

            TrainInformationResponse pInfoSolutionsResponse = System.Web.HttpContext.Current.Session["TrainSearch"] as TrainInformationResponse;
            var evChargesList = new ManageBooking().GetEvolviChargesByOrderId(Convert.ToInt64(OrderID));
            if (pInfoSolutionsResponse.EvolviPayloadAttributesInfo != null && evChargesList != null)
            {
                if (!string.IsNullOrEmpty(evChargesList.BookingRef))
                {
                    request = new EvCancelBookingRequest
                    {
                        Version = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.Version,
                        TimeStamp = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TimeStamp,
                        SequenceNmbr = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.SequenceNmbr,
                        TargetType = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TargetType,
                        ISOCountry = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCountry,
                        ISOCurrency = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCurrency,
                        IsRemoveBasket = IsRemoveBasket,
                        OrderId = evChargesList.BookingRef
                    };
                    ApiLogin(request, SiteID);
                    response = client.EvolviCancelBooking(request);
                }
            }
            return false;
        }
        catch (Exception ex) { throw ex; }
    }

    public void EvolviApiLogin(dynamic request, Guid siteId)
    {
        var mngUser = new ManageUser();
        var api = mngUser.GetApiLogingDetail(siteId).Where(x => x.IsActive == 1 && x.TrainName == "Evolvi").ToList();
        if (api != null)
        {
            var resultEVOLVI = api.FirstOrDefault(x => x.TrainName == "Evolvi");
            request.Header = new OneHubServiceRef.Header
            {
                onehubusername = "#@dots!squares",
                onehubpassword = "#@dots!squares",
                unitofwork = 0,
                language = Language.nl_BE,
                ApiAccount = new ApiAccountDetail
                {
                    BeNeApiId = 0,
                    TiApiId = 0,
                    NTVApiId = 0,
                    EvolviApiId = resultEVOLVI != null ? resultEVOLVI.ID : 0
                }
            };
        }
    }
}

public class OutTrainTimeDateBENEList
{
    public string DeptDate { get; set; }
    public string DeptTme { get; set; }
    public int Type { get; set; }
}

public class InTrainTimeDateBENEList
{
    public string ReturnDate { get; set; }
    public string ReturnTme { get; set; }
    public int Type { get; set; }
}

public class Passanger
{
    public string PassangerType { get; set; }
    public string cardnumber { get; set; }
}

public class ShoppingCartDetails
{
    public Guid Id { get; set; }

    public string DepartureStation { get; set; }
    public string DepartureDate { get; set; }
    public string DepartureTime { get; set; }

    public string ArrivalStation { get; set; }
    public string ArrivalDate { get; set; }
    public string ArrivalTime { get; set; }

    public string TrainNo { get; set; }
    public string ServiceName { get; set; } //premium
    public string Passenger { get; set; }
    public string Fare { get; set; } //offer 
    public string Title { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Price { get; set; }
    public decimal netPrice { get; set; }
}

public class BookingRequest
{
    public PurchasingForServiceRequest PurchasingForServiceRequest { get; set; }
    public PurchasingForServiceOwnerRequest PurchasingForServiceOwnerRequest { get; set; }
    public bool IsInternational { get; set; }
    public Guid Id { get; set; }
    public string DepartureStationName { get; set; }
    public RailBookingRequest EvolviBookingRequest { get; set; }
}

public class StationInfo
{
    public Stationst Station { get; set; }
}

public class DBStation
{
    public string OldStationName { get; set; }
    public string NewStationName { get; set; }
}

public class Stationst
{
    public string StationName { get; set; }
    public BCode BCode { get; set; }
    public ICode ICode { get; set; }
    public bool IsActive { get; set; }
    public int Preference { get; set; }
}

public class BCode
{
    public string StationName { get; set; }
    public string RailwayCode { get; set; }
    public string StationCode { get; set; }
}

public class ICode
{
    public string StationName { get; set; }
    public string RailwayCode { get; set; }
    public string StationCode { get; set; }
}

public class BookingResponse
{
    public string ReservationCode { get; set; }
    public string ChangeReservationCode { get; set; }

    public bool Issued { get; set; }//TI
    public long UnitOfWork { get; set; }//TI
    public List<string> PdfUrl { get; set; }//TI
    public OneHubServiceRef.Coupon[] Coupons { get; set; }//TI

    public string Pnr { get; set; }//BE
    public string PinCode { get; set; }//BE
    public string DepStationName { get; set; }//BE

    public BillingAddress BillingAddress { get; set; }

    public string Status { get; set; }//EV
    public string OrderId { get; set; }//EV
    public string OrderRefId { get; set; }//EV
}

public class BookingRequestUserControl
{
    public bool isIhaveRailPass { get; set; }

    public string FromDetail { get; set; }
    public string ToDetail { get; set; }
    public DateTime depdt { get; set; }
    public DateTime depTime { get; set; }
    public string depRCode { get; set; }
    public string depstCode { get; set; }
    public string arrRCode { get; set; }
    public string arrstCode { get; set; }
    public int ClassValue { get; set; }
    public int Adults { get; set; }
    public int Boys { get; set; }
    public int Seniors { get; set; }
    public int Youths { get; set; }
    public int Transfare { get; set; }
    public string ReturnDate { get; set; }
    public string ReturnTime { get; set; }
    public Boolean Loyalty { get; set; }
    public string Journeytype { get; set; }
    public Guid ClassId { get; set; }
    public Guid CurrencyId { get; set; }
    public List<Loyaltycard> lstLoyalty { get; set; }
    public string OneHubServiceName { get; set; }
    public bool IsReturnJurney { get; set; }

    public string Target { get; set; } //EV
    public string SequenceNo { get; set; } //EV
    public string MaxResponse { get; set; } //EV
    public string ISOCountry { get; set; } //EV
    public string ISOCurrency { get; set; } //EV
    public List<EvolviRailCardRequestInfo> EvolviRailCardRequestInfoList { get; set; } //EV
    public bool isRailCard { get; set; } //EV
}

public class PaymentGateWayTransffer
{
    public double Amount { get; set; }
    public string customerEmail { get; set; }
    public string orderReference { get; set; }
    public string currencyCode { get; set; }
}

public class TrainInformationListNew
{
    [XmlElement]
    public string TrainNumber { get; set; }
    [XmlElement]
    public string TrainDescr { get; set; }
    [XmlElement]
    public string TrainCategoryCode { get; set; }
    [XmlElement]
    public string TrainCategory { get; set; }

    [XmlElement]
    public string DepartureStationCode { get; set; }
    [XmlElement]
    public string DepartureStationName { get; set; }
    [XmlElement]
    public string DepartureRailwayCode { get; set; }
    [XmlElement]
    public string DepartureDate { get; set; }
    [XmlElement]
    public string DepartureTime { get; set; }

    [XmlElement]
    public string ArrivalStationCode { get; set; }
    [XmlElement]
    public string ArrivalStationName { get; set; }
    [XmlElement]
    public string ArrivalRailwayCode { get; set; }
    [XmlElement]
    public string ArrivalDate { get; set; }
    [XmlElement]
    public string ArrivalTime { get; set; }

    [XmlElement]
    public string TravelTimeDuration { get; set; }
    [XmlElement]
    public string JourneySolutionCode { get; set; }
    [XmlElement]
    public string TripType { get; set; }
    [XmlElement]
    public string Route { get; set; }
    [XmlElement]
    public string NumberOfTransfer { get; set; }
    [XmlElement]
    public string Routesummaryid { get; set; }

    [XmlElement]
    public List<PriceResponse> PriceInfo { get; set; }
    [XmlElement]
    public List<TrainInfoSegment> TrainInfoSegment { get; set; }
    [XmlElement]
    public List<BookingRequestInfo> BookingRequestInfo { get; set; } //--BeNe
}

public class ManageAgentData
{
    public long OrderId { get; set; }
    public List<AgentAddress> AgentAddress { get; set; }
}

public class AgentAddress
{
    public string Title { get; set; }
    public string FName { get; set; }
    public string LName { get; set; }
    public string EmailAddress { get; set; }
    public string Phone { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Country { get; set; }
    public string ZipCode { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
}

public class TravellersInfo
{
    public string Title { get; set; }
    public string FName { get; set; }
    public string LName { get; set; }
    public string Country { get; set; }
    public string RailCard { get; set; }
    public string Type { get; set; }
    public bool Lead { get; set; }
    public string TitleValue { get; set; }
}

public class ReservationInfo
{
    public string JourneyIdentifier { get; set; }
    public string FareId { get; set; }
    public string JourneyType { get; set; }
    public string Price { get; set; }
    public string ReservationStatus { get; set; }
    public string FareSetterCode { get; set; }
}

public class ReservationCompanyInfo
{
    public string CompanyName { get; set; }
    public string TravellBy { get; set; }
    public string LegId { get; set; }
}

public class EvSeatAllocation
{
    public string SeatDescription { get; set; }
    public bool isReserable { get; set; }
}

public class EvolviReservationRef
{
    public string TrainNo { get; set; }
    public Guid P2PId { get; set; }
    public string ReservationCode { get; set; }
}

public class NTVJourneySegmentDetails
{
    public string JourneySellKey { get; set; } //NTV
}

public class JourneyReservationInfo
{
    public int Index { get; set; }
    public string DepartureStation { get; set; }
    public string ArrivalStation { get; set; }
    public string DepartureDate { get; set; }
    public string DepartureDateTime { get; set; }
    public string CompanyName { get; set; }
    public string TravellBy { get; set; }
    public string Reservable { get; set; }
    public string LegId { get; set; }
    public string ReservationStatus { get; set; }
    public List<EvSeatAllocation> EvSeatAllocations { get; set; }
    public string MatchLegID { get; set; }
    public string AccommodationType { get; set; }
    public bool IsSleeper { get; set; }
    public string fareSetterCode { get; set; }
    public bool isBerthSleeper { get; set; }
    public bool isSeatSleeper { get; set; }
}