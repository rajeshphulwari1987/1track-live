﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="BookingCart.aspx.cs" Inherits="BookingCart"
    UICulture="en" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucPassDetails" Src="~/UserControls/UCpassdetalis.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--PCA--%>
    <%-- <script>        (function (n, t, i, r) { var u, f; n[i] = n[i] || {}, n[i].initial = { accountCode: "INTER11223", host: "INTER11223.pcapredict.com" }, n[i].on = n[i].on || function () { (n[i].onq = n[i].onq || []).push(arguments) }, u = t.createElement("script"), u.async = !0, u.src = r, f = t.getElementsByTagName("script")[0], f.parentNode.insertBefore(u, f) })(window, document, "pca", "//INTER11223.pcapredict.com/js/sensor.js")</script>--%>
    <%--PCA--%>
    <%=ChkOutActionsTags%>
    <%=ChkOutSetActionsTags%>
    <%=RemoveProductTags%>
    <style type="text/css">
        .starail-Icon-tick-yes {
            position: absolute;
            color: #0c6ab8;
            z-index: 1;
            padding-left: 3px;
        }

        .starail-BookingDetails-form {
            overflow: visible;
        }

        #MainContent_pnlbindshippping {
            width: 960px;
        }

        .inputwidthTxt {
            width: 90% !important;
            margin-left: 12px;
        }

        .selectTitlewidth {
            width: 6rem !important;
        }

        .chkwidth {
            width: 1%;
        }

        .p-full-text {
            text-align: justify;
            margin: 0px;
        }

        .p-desc-not-mad, .p-desc-mad {
            padding-bottom: 1%;
        }

        .sta-height {
            height: 220px;
        }

        @media screen and (min-device-width: 360px) and (max-device-width: 768px) {
            .starail-BookingDetails-form {
                overflow: visible;
                width: 98% !important;
                float: left;
            }

            .starail-BookingDetails-form {
                overflow: hidden !important;
                width: auto;
                float: none;
            }

            #MainContent_pnlbindshippping {
                width: auto;
            }
        }
    </style>
    <script type="text/javascript">
        var dshiop = '';
        var IsAgentSite = '<%=IsAgent %>';
        $(document).ready(function () {
            $("#MainContent_txtDateOfDepature").bind("contextmenu cut copy paste keydown keyup keypress", function (e) {
                return false;
            });
            $('.calculateTotal').click(function () {
                getdata();
            });
            shipingchk();
            if (IsAgentSite != 'True') {
                $("#MainContent_UcPassDetail_rptPassDetail_txtFirstName_0").keyup(function () {
                    $("#txtFirst").val($("#MainContent_UcPassDetail_rptPassDetail_txtFirstName_0").val());
                });
                $("#MainContent_UcPassDetail_rptPassDetail_txtLastName_0").keyup(function () {
                    $("#MainContent_txtLast").val($("#MainContent_UcPassDetail_rptPassDetail_txtLastName_0").val());
                });
                $("#MainContent_UcPassDetail_rptPassDetail_ddlTitle_0").change(function () {
                    $("#MainContent_ddlMr").prop("selectedIndex", $("#MainContent_UcPassDetail_rptPassDetail_ddlTitle_0").prop("selectedIndex"));
                });
            }

        });
        /*shipping details*/
        function shipingchk() {
            $(".bookingcheckbox").find('label').before("<span class='bookingcheckboxSpan'><i class='starail-Icon-tick'></i></span>");

            $("#MainContent_txtDateOfDepature").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+12m'
            });
            if ($("#MainContent_chkShippingfill").is(':checked')) {
                $("#MainContent_pnlbindshippping").show();
                ValidatorEnable($('[id*=reqvalerrorSA0]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA1]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA2]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA7]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA8]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA9]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA10]')[0], true);
                if ($("#MainContent_ddlshpMr").val() == 0) {
                    $("#MainContent_ddlshpMr").prop("selectedIndex", $("#MainContent_ddlMr").prop("selectedIndex"));
                }
                if ($("#MainContent_txtshpfname").val() == '') {
                    $("#MainContent_txtshpfname").val($("#txtFirst").val());
                }
                if ($("#MainContent_txtshpLast").val() == '') {
                    $("#MainContent_txtshpLast").val($("#MainContent_txtLast").val());
                }
                if ($("#MainContent_txtshpEmail").val() == '') {
                    $("#MainContent_txtshpEmail").val($("#MainContent_txtEmail").val());
                }
                if ($("#MainContent_txtShpPhone").val() == '') {
                    $("#MainContent_txtShpPhone").val($("#MainContent_txtBillPhone").val());
                }
                if ($("#MainContent_txtshpAdd").val() == '') {
                    $("#MainContent_txtshpAdd").val($("#MainContent_txtAdd").val());
                }
                if ($("#MainContent_txtshpAdd2").val() == '') {
                    $("#MainContent_txtshpAdd2").val($("#MainContent_txtAdd2").val());
                }
                if ($("#MainContent_txtshpCity").val() == '') {
                    $("#MainContent_txtshpCity").val($("#MainContent_txtCity").val());
                }
                if ($("#MainContent_txtshpState").val() == '') {
                    $("#MainContent_txtshpState").val($("#MainContent_txtState").val());
                }
                if ($("#MainContent_txtshpZip").val() == '') {
                    $("#MainContent_txtshpZip").val($("#MainContent_txtZip").val());
                }
                if ($("#MainContent_ddlshpCountry").val() == '0') {
                    $("#MainContent_ddlshpCountry").prop("selectedIndex", $("#MainContent_ddlCountry").prop("selectedIndex"));
                }
            }
            else {
                $("#MainContent_pnlbindshippping").hide();
                ValidatorEnable($('[id*=reqvalerrorSA0]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA1]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA2]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA7]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA8]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA9]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA10]')[0], false);
            }
            getdata();
        }
        function lastshipping(obj) {
            if (obj != undefined && obj == 'first')
                dshiop = '';
            if (dshiop != '')
                $("#" + dshiop).attr("checked", "checked");
            else {
                $("#radioChkShp1").attr("checked", "checked");
                if ($("#radioChkShp1").val() != undefined && $("#radioChkShp1").val() != '') {
                    $('#MainContent_hdnShippingCost').val($("#radioChkShp1").val().split('^')[0]);
                    getdata();
                }
            }
        }
        function getdata() {
            $(document).ready(function () {
                resetchk($('#MainContent_chkShippingfill').is(':checked'));
                lastshipping();
                var countPrice = 0.00;
                $('.calculateTotal').each(function () {
                    var tkamount = parseFloat($(this).parent().find('.hdntkprs').val());
                    var price = parseFloat($(this).parent().find('.hdnprs').val());
                    if (!$(this).find('input').prop("checked")) {
                        tkamount = 0;
                    }
                    countPrice = countPrice + (tkamount + price);
                });
                $("#lblTotal").text(countPrice.toFixed(2));
                $(".shipping").each(function () {
                    if ($(this).is(":checked")) {
                        dshiop = $(this).attr('id');
                        var str = $(this).val().split('^');
                        var price = str[0];
                        var smethod = str[1];
                        var sdesc = str[2];
                        sdesc = $('<div/>').html(sdesc).text();
                        $("#MainContent_hdnShippingCost").attr('value', price.toString());
                        $("#MainContent_hdnShipMethod").attr('value', smethod.toString());
                        $("#MainContent_hdnShipDesc").attr('value', sdesc.toString());
                    }
                });
                var shipingamount = parseFloat($("#MainContent_hdnShippingCost").val());
                var bookingFee = parseFloat($.trim($("#MainContent_lblBookingFee").text()));
                countPrice = bookingFee + countPrice + shipingamount;

                var Adminfeeval = 0;
                if ($("#hdnAdminFee").val().match(/sum/i)) {
                    Adminfeeval = parseFloat($("#hdnAdminFee").val().replace("sum", ''));
                }
                else if ($("#hdnAdminFee").val().match(/mult/i)) {
                    Adminfeeval = parseFloat($("#hdnAdminFee").val().replace("mult", '')) * countPrice;
                }
                $("#hdnAdminFeeAmount").val(Adminfeeval.toFixed(2));
                $("#MainContent_lblAdminFee").text(Adminfeeval.toFixed(2));
                $("#MainContent_lblTotalCost").text((Adminfeeval + countPrice).toFixed(2));
            });
        }
        function shippingclick(obj) {
            getdata();
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function GoRailPassDetails() {
            window.history.back();
        }
        function GoPaymentProcessPage() {
            $("#btnCheckout").trigger('click');
        }
        function updateCartBucket(cartitem) {
            $("#dropdownMenu1").find('span.badge').html(cartitem);
            $("#dropdownMenuHover").html("You have " + cartitem + " item added.");
        }

        function resetchk(obj) {
            if (obj)
                $("#shipdelchk").show();
            else
                $("#shipdelchk").hide();
        }

        function changedeptdateonbookingfee() {
            var hostUrl = window.location.href + '.aspx/AjaxFillBookingFeeOnDepartureDate'; //window.location.origin + window.location.pathname
            var DeptDate = $("#MainContent_txtDateOfDepature").val();
            $.ajax({
                url: hostUrl,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: "{'DeptDate':'" + DeptDate + "','Price':'" + $("#hdnBookingFee").val() + "'}",
                success: function (Pdata) {
                    console.log(Pdata);
                    if (Pdata.d != 'no' || Pdata.d != '' || Pdata.d != "0.00") {
                        $("#MainContent_lblBookingFee").text(Pdata.d);
                        $("#MainContent_pnlbooking").show();
                    }
                    else {
                        $("#MainContent_lblBookingFee").text("0.00");
                        $("#MainContent_pnlbooking").hide();
                    }
                    getdata();
                },
                error: function () {
                }
            });
        }

    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdntotalSaleamount" runat="server" Value="0.00" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnBookingFee" runat="server" Value="0.00" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnShipMethod" runat="server" Value="" />
            <asp:HiddenField ID="hdnShipDesc" runat="server" Value="" />
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <div class="starail-Grid-col starail-Grid-col--nopadding">
                <div class="starail-ProgressBar starail-ProgressBar-stage--stage2 starail-u-hideMobile starail-ProgressBar-stage--stage1">
                    <div class="starail-ProgressBar-line">
                        <div class="starail-ProgressBar-progress">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Ticket Selection
                        </p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Booking Details
                        </p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Checkout
                        </p>
                    </div>
                </div>
                <div class="starail-Section starail-Section--nopadding">
                    <a href='<%=siteURL %>' class="starail-u-hideDesktop starail-Button--mobileBackButton">
                        <i class="starail-Icon-chevron-left"></i>Choose another pass</a> <a href='<%=siteURL %>'
                            class="starail-Button starail-Button--blue starail-Button--fullCallout starail-u-hideMobile">
                            <i class="starail-Icon-chevron-left"></i>Choose another pass</a>
                </div>
                <div class="starail-Section starail-Section--nopadding">
                    <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                        <div>
                            <a class="starail-Button starail-Button--blue" style="font-size: 15px;" onclick="GoRailPassDetails();">
                                <i class="starail-Icon-chevron-left"></i>Pass Selection</a> <a class="starail-Button starail-Button--blue starail-checkout-trigger"
                                    style="font-size: 15px;" id="btnCheckoutTop" runat="server" onclick="GoPaymentProcessPage();">Checkout <i class="starail-Icon-chevron-right"></i></a>
                            <!-- will submit form -->
                        </div>
                    </div>
                    <h1 class="starail-BookingDetails-title">Booking Details</h1>
                    <div class="starail-Form-row">
                        <asp:Panel ID="pnlErrSuccess" runat="server">
                            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                                <asp:Label ID="lblSuccessMsg" runat="server" />
                            </div>
                            <div id="DivError" runat="server" class="error" style="display: none;">
                                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                            </div>
                        </asp:Panel>
                        <uc:ucPassDetails runat="server" ID="UcPassDetail" />
                    </div>
                    <!-- Tickets -->
                    <div class="starail-BookingDetails-form">
                        <h2>Billing Address</h2>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-firstname">
                                Full name <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--ddInput">
                                    <asp:DropDownList ID="ddlMr" runat="server" class="starail-Form-select">
                                        <asp:ListItem Text="Title" Value="0" />
                                        <asp:ListItem Text="Dr." Value="1" />
                                        <asp:ListItem Text="Mr." Value="2" />
                                        <asp:ListItem Text="Miss" Value="3" />
                                        <asp:ListItem Text="Mrs." Value="4" />
                                        <asp:ListItem Text="Ms" Value="5" />
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtFirst" runat="server" CssClass="starail-Form-input" ClientIDMode="Static"
                                        placeholder="First Name" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA0" runat="server" ControlToValidate="txtFirst"
                                        ErrorMessage="Please enter first name." CssClass="absolute" Display="Static"
                                        Text="*" />
                                </div>
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtLast" runat="server" CssClass="starail-Form-input" placeholder="Last Name" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA1" runat="server" CssClass="absolute"
                                        Display="Static" Text="*" ErrorMessage="Please enter last name." ControlToValidate="txtLast" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-email">
                                Email address <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="starail-Form-input" Style='text-transform: lowercase' />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA2" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="Please enter email." CssClass="absolute" Display="Static" Text="*" />
                                    <asp:RegularExpressionValidator ID="reqcustvalerrorBA3" runat="server" CssClass="starail-Form-required absolute"
                                        ErrorMessage="Please enter a valid email." ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Display="Static" Text="*" ValidationGroup="vgs1" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row" style="display: none;">
                            <label class="starail-Form-label" for="starail-billing-email">
                                Confirm Email address
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="starail-Form-input" Style='text-transform: lowercase' />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-address1">
                                Phone Number <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtBillPhone" runat="server" CssClass="starail-Form-input" MaxLength="20"
                                        onkeypress="return isNumberKey(event)" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA7" runat="server" ControlToValidate="txtBillPhone"
                                        ErrorMessage="Please enter phone number." CssClass="absolute" Display="Static"
                                        Text="*" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row" style="display: none;">
                            <label class="starail-Form-label" for="starail-billing-address1">
                                Search for an address</label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtSearchAddress" runat="server" placeholder="Search Address" CssClass="starail-Form-input"
                                        MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-address2">
                                Address Line 1 Or Company Name<span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtAdd" runat="server" CssClass="starail-Form-input" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA8" runat="server" ControlToValidate="txtAdd"
                                        ErrorMessage="Please enter address." CssClass="absolute" Display="Static" Text="*" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-address2">
                                Address Line 2</label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtAdd2" runat="server" CssClass="starail-Form-input" MaxLength="50" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-town">
                                Town / City<span class="starail-Form-required">*</span>
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="starail-Form-input" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA11" runat="server" ControlToValidate="txtCity"
                                        ErrorMessage="Please enter city." CssClass="starail-Form-required absolute" Display="Static"
                                        Text="*" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-state">
                                County / State</label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtState" runat="server" CssClass="starail-Form-input" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-postcode">
                                Postal / Zip code <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txtZip" runat="server" CssClass="starail-Form-input" MaxLength="10" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA9" runat="server" ControlToValidate="txtZip"
                                        ErrorMessage="Please enter postal/zip code." CssClass="absolute" Display="Static"
                                        Text="*" />
                                    <asp:Label ID="lblpmsg" runat="server" Visible="false" CssClass="starail-Form-required"
                                        Text="Please enter maximum 7 character postal/zip code." />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <label class="starail-Form-label" for="starail-billing-country">
                                Country <span class="starail-Form-required">*</span></label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <%-- <asp:DropDownList ID="ddlCountry" runat="server" class="starail-Form-select" onchange="GetShipping()" />--%>
                                    <asp:DropDownList ID="ddlCountry" runat="server" class="starail-Form-select" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorBA10" runat="server" CssClass="starail-Form-required absolute"
                                        Display="Static" Text="*" ErrorMessage="Please select Country." ControlToValidate="ddlCountry"
                                        InitialValue="0" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <div class="starail-Form-spacer starail-u-hideMobile">
                                &nbsp;
                            </div>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-inputContainer-col">
                                    <label class="starail-BookingDetails-form-fancyCheckbox">
                                        <span id="shipdelchk" class="starail-Icon-tick-yes"><i class="starail-Icon-tick"></i>
                                        </span><span class="starail-Form-fancyCheckbox">
                                            <%--<asp:CheckBox ID="chkShippingfill" runat="server" class="starail-sendtoaddress" />--%>
                                            <asp:CheckBox ID="chkShippingfill" runat="server" class="starail-sendtoaddress" AutoPostBack="true"
                                                OnCheckedChanged="chkShippingfill_CheckedChanged" />
                                            Send my tickets to a different address </span>
                                    </label>
                                    <asp:Label runat="server" ID="lblmsg" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <p>
                            Your personal details are processed in accordance with the privacy policy of InternationalRail.
                        </p>
                    </div>
                    <asp:Panel ID="pnlbindshippping" runat="server" Style="display: none;">
                        <div class="starail-BookingDetails-form">
                            <h2>Delivery Address</h2>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-firstname">
                                    Full name <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--ddInput">
                                        <asp:DropDownList ID="ddlshpMr" runat="server" class="starail-Form-select" Style="height: auto !important;">
                                            <asp:ListItem Text="Title" Value="0" />
                                            <asp:ListItem Text="Dr." Value="1" />
                                            <asp:ListItem Text="Mr." Value="2" />
                                            <asp:ListItem Text="Miss" Value="3" />
                                            <asp:ListItem Text="Mrs." Value="4" />
                                            <asp:ListItem Text="Ms" Value="5" />
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtshpfname" runat="server" CssClass="starail-Form-input" placeholder="First name" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA0" runat="server" ControlToValidate="txtshpfname" />
                                    </div>
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpLast" runat="server" CssClass="starail-Form-input" placeholder="Last name" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA1" runat="server" ControlToValidate="txtshpLast" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-email">
                                    Email address <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpEmail" runat="server" CssClass="starail-Form-input" Style='text-transform: lowercase' />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA2" runat="server" ControlToValidate="txtshpEmail"
                                            CssClass="absolute" Display="Static" Text="*" ErrorMessage="Please enter delivery email." />
                                        <asp:RegularExpressionValidator ID="reqcustvalerrorSA3" runat="server" CssClass="absolute starail-Form-required"
                                            ControlToValidate="txtshpEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ErrorMessage="Please enter valid delivery email." Display="Static" Text="*" ValidationGroup="vgs1" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row" style="display: none;">
                                <label class="starail-Form-label" for="starail-billing-email">
                                    Confirm Email address
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpConfirmEmail" runat="server" CssClass="starail-Form-input"
                                            Style='text-transform: lowercase' />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-address1">
                                    Phone Number <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtShpPhone" runat="server" CssClass="starail-Form-input" MaxLength="20"
                                            onkeypress="return isNumberKey(event)" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA7" runat="server" ControlToValidate="txtShpPhone"
                                            ErrorMessage="Please enter delivery phone no." CssClass="absolute" Display="Static"
                                            Text="*" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-billing-address1">
                                    Search for an address</label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpSearch" runat="server" placeholder="Search Address" CssClass="starail-Form-input"
                                            MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-address1">
                                    Address Line 1 Or Company Name <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpAdd" runat="server" CssClass="starail-Form-input" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA8" runat="server" CssClass="absolute"
                                            Display="Static" Text="*" ErrorMessage="Please enter delivery address." ControlToValidate="txtshpAdd" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-address2">
                                    Address line 2</label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpAdd2" runat="server" CssClass="starail-Form-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-town">
                                    Town / City
                                </label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpCity" runat="server" CssClass="starail-Form-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-state">
                                    County / State</label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpState" runat="server" CssClass="starail-Form-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-postcode">
                                    Postal / Zip code <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <asp:TextBox ID="txtshpZip" runat="server" MaxLength="10" CssClass="starail-Form-input" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA9" runat="server" CssClass="absolute"
                                            Display="Static" Text="*" ErrorMessage="Please enter delivery postal/zip code."
                                            ControlToValidate="txtshpZip" />
                                    </div>
                                </div>
                            </div>
                            <div class="starail-Form-row">
                                <label class="starail-Form-label" for="starail-delivery-country">
                                    Country <span class="starail-Form-required">*</span></label>
                                <div class="starail-Form-inputContainer">
                                    <div class="starail-Form-inputContainer-col">
                                        <%--<asp:DropDownList ID="ddlshpCountry" runat="server" class="starail-Form-select" onchange="GetShipping()" />--%>
                                        <asp:DropDownList ID="ddlshpCountry" runat="server" class="starail-Form-select" OnSelectedIndexChanged="ddlshpCountry_SelectedIndexChanged"
                                            AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="reqvalerrorSA10" runat="server" ErrorMessage="Please select delivery country."
                                            ControlToValidate="ddlshpCountry" Display="Static" Text="*" CssClass="absolute starail-Form-required"
                                            InitialValue="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Tickets End--%>
                    <div class="starail-BookingDetails-form">
                        <h2>Date for Departure</h2>
                        <div class="starail-Form-row">
                            <label for="starail-firstname" class="starail-Form-label">
                                Date for Departure<span class="starail-Form-required">*</span>
                                <br />
                                (for fulfillment purposes)
                            </label>
                            <div class="starail-Form-inputContainer">
                                <div class="starail-Form-datePicker booking-detail-p2p">
                                    <asp:TextBox ID="txtDateOfDepature" runat="server" class="starail-Form-input starail-Form-datePicker"
                                        Text="DD/MM/YYYY" onchange="changedeptdateonbookingfee()" />
                                    <asp:RequiredFieldValidator ID="reqvalerrorDPdATE" runat="server" Text="*" ErrorMessage="Please enter Date Of Depature."
                                        CssClass="absolute" Display="Static" ControlToValidate="txtDateOfDepature" ForeColor="#eaeaea"
                                        InitialValue="DD/MM/YYYY" />
                                    <i class="starail-Icon-datepicker"></i>
                                </div>
                            </div>
                        </div>
                        <p>
                            If left empty the ticket start date will be used as delivery date.
                        </p>
                    </div>
                    <asp:Panel ID="pnlShipping" runat="server">
                        <div class="starail-BookingDetails-form">
                            <h2>Shipping Option</h2>
                            <%--<div id="shipping">
                            </div>--%>
                            <asp:Repeater ID="rptShippings" runat="server">
                                <ItemTemplate>
                                    <div class="starail-Form-row">
                                        <label>
                                            <span class="starail-Form-fancyRadioGroup">
                                                <input type="radio" id="radioChkShp<%# Container.ItemIndex + 1 %>" class="shipping"
                                                    value='<%#Eval("Price")+"^"+Eval("ShippingName")+"^"+ Server.HtmlDecode(Eval("description").ToString())%>'
                                                    name="shipping" onclick="shippingclick(this)" />
                                                <%#Eval("ShippingName")%>
                                                <%=currency%>
                                                <%#Eval("Price")%>
                                                <span><i></i></span></span>
                                        </label>
                                        <div class="starail-Form-inputContainer paddingleft">
                                            <%#Eval("description")%>
                                        </div>
                                    </div>
                                    <hr />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlbooking" runat="server">
                        <div class="starail-BookingDetails-form">
                            <h2>Booking Fee</h2>
                            <div class="starail-Form-row">
                                <label>
                                    <%=currency%><asp:Label ID="lblBookingFee" runat="server" Text="0.00" Style="margin-right: 8px;"></asp:Label>
                                </label>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnladminfee" runat="server">
                        <div class="starail-BookingDetails-form">
                            <h2>Admin Fee</h2>
                            <div class="starail-Form-row">
                                <label>
                                    <%=currency%>
                                    <asp:Label ID="lblAdminFee" runat="server" Text="0.00" Style="margin-right: 8px;"></asp:Label>
                                    <asp:HiddenField ID="hdnAdminFeeAmount" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hdnAdminFee" runat="server" ClientIDMode="Static" />
                                </label>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="starail-BookingDetails-form">
                        <h2>TOTAL PRICE</h2>
                        <div class="starail-Form-row">
                            <div class="starail-Form-inputContainer-col" style="width: 100%">
                                <p class="starail-YourBooking-totalPrice">
                                    Total Price: <span class="starail-YourBooking-totalPrice-amount">
                                        <%=currency%>
                                        <asp:Label ID="lblTotalCost" runat="server" /></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="starail-BookingDetails-form sta-height">
                        <div class="starail-Form-row p-desc-mad" id="div_Mandatory" runat="server" visible="false">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                                <asp:CheckBox ID="chkMandatory" runat="server" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                                <span class="starail-Form-required">*</span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                <p class="p-full-text" id="p_MandatoryText" runat="server"></p>
                            </div>
                        </div>
                        <div class="starail-Form-row p-desc-not-mad" id="div_NotMandatory" runat="server" visible="false">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth">
                                <asp:CheckBox ID="chkNotMandatory" runat="server" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 chkwidth" style="padding: 0px;">
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
                                <p class="p-full-text" id="p_NotMandatoryText" runat="server"></p>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-button-div paddingnone">
                        <asp:Button ID="btnCheckout" ClientIDMode="Static" runat="server" Text="Proceed to Checkout"
                            CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit IR-GaCode"
                            OnClick="btnCheckout_Click" ValidationGroup="vgs1" OnClientClick="shipingchk();Focusonerrorcontrol();" />
                        <asp:ValidationSummary ID="vs1" runat="server" ShowMessageBox="true" ShowSummary="false"
                            DisplayMode="List" ValidationGroup="vgs1" />
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnShippingCost" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        loadevent();
        function loadevent() {
            $(".loyradioSpan").remove();
            $(".loyspanradio").find('label').before("<span class='loyradioSpan' style='float:left;'><i class='radioSpancenter'></i></span>");
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            showhideterms();
            $('#MainContent_chkShippingfill').click(function () {
                //                GetShipping();
                shipingchk();
                getdata();
                lastshipping('first');
            });
            //            GetShipping();
        });
        function fillagentdata() {
            if ($("#MainContent_chkShippingfill").is(':checked')) {
                $("#MainContent_pnlbindshippping").show();
                ValidatorEnable($('[id*=reqvalerrorSA0]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA1]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA2]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA7]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA8]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA9]')[0], true);
                ValidatorEnable($('[id*=reqvalerrorSA10]')[0], true);
            }
            else {
                $("#MainContent_pnlbindshippping").hide();
                ValidatorEnable($('[id*=reqvalerrorSA0]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA1]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA2]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA7]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA8]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA9]')[0], false);
                ValidatorEnable($('[id*=reqvalerrorSA10]')[0], false);
            }
        }

        function showhideterms() {
            if ('<%=IsStaSite %>' == 'False') {
                var mandatoryText = '<%=MandatoryTextFirst %>';
                if (mandatoryText.length > 130) {
                    $('[id*=p_MandatoryText]').text(mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...');
                    mandatoryText = mandatoryText.substr(0, mandatoryText.lastIndexOf(' ', 127)) + '...';
                }
                var notMandatoryText = '<%=MandatoryTextFirstSecond %>';
                if (notMandatoryText.length > 130) {
                    $('[id*=p_NotMandatoryText]').text(notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...');
                    notMandatoryText = notMandatoryText.substr(0, notMandatoryText.lastIndexOf(' ', 127)) + '...';
                }

                $('.p-desc-mad').mouseover(function () {
                    $(this).find('.p-full-text').text('<%=MandatoryTextFirst %>');
                }).mouseout(function () {

                    $(this).find('.p-full-text').text(mandatoryText);
                });
                ;
                $('.p-desc-not-mad').mouseover(function () {
                    $(this).find('.p-full-text').text('<%=MandatoryTextFirstSecond %>');
                }).mouseout(function () {
                    $(this).find('.p-full-text').text(notMandatoryText);
                });
            }
        }

        function checkMandatory() {
            alert('Please accept the privacy statement, by clicking the tick box at the bottom of the page.');
        }
    </script>
    <%-- <script type="text/javascript">
        function GetShipping() {
            var hostUrl = window.location.href + '.aspx/AjaxFillShippingData'; //window.location.origin + window.location.pathname
            console.log(hostUrl);
            var HaveShipping = $("[id*=chkShippingfill]").is(":checked");
            var ShipCountry = $("#MainContent_ddlshpCountry").val();
            var Country = $("#MainContent_ddlCountry").val();
            console.log("{'HaveShipping':'" + HaveShipping + "','ShipCountry':'" + ShipCountry + "','Country':'" + Country + "'}");
            $.ajax({
                url: hostUrl,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: "{'HaveShipping':'" + HaveShipping + "','ShipCountry':'" + ShipCountry + "','Country':'" + Country + "'}",
                success: function (Pdata) {
                    console.log(Pdata);
                    console.log(Pdata.d);
                    if (Pdata.d == "no" || Pdata.d == 'null') {
                        $("#MainContent_pnlShipping").hide();
                        console.log("error on response method");
                    }
                    else {
                        $("#MainContent_pnlShipping").show();
                        var shippinghtml = "";
                        $.each($.parseJSON(Pdata.d), function (key, value) {
                            var valuedata = value["Price"] + "^" + value["ShippingName"] + "^" + value["Description"];
                            shippinghtml += '<div class="starail-Form-row">' +
                                '<label>' +
                                    '<span class="starail-Form-fancyRadioGroup">' +
                                        '<input type="radio" id="radioChkShp' + (key + 1) + '" class="shipping"' +
                                            ' value="' + valuedata + '" ' +
                                            'name="shipping" onclick="shippingclick(this)" />' +
                                            value["ShippingName"] +
                                            '  <%=currency%> ' +
                                            value["Price"] +
                                            '<span><i></i></span></span>' +
                                        '</label>' +
                                        '<div class="starail-Form-inputContainer paddingleft">' +
                                         value["Description"] +
                                    '</div>' +
                                '</div>';
                            $("#shipping").html(shippinghtml);
                            lastshipping('first');
                            getdata();
                        });
                    }
                },
                error: function () {
                    console.log("error on request method");
                }
            });
        }
    </script>--%>
</asp:Content>
