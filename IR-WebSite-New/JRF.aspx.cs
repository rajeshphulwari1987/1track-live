﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Net;
using System.Net.Mail;
using System.Xml;

public partial class JRF : System.Web.UI.Page
{
    private Guid _siteId;
    public Guid IrAgentSiteID = Guid.Parse("57ab589f-8549-46ca-82e8-4d835c2ff0ac"); // Guid.Parse("98bb7831-068a-4963-9eb8-d7ccab265b4e");
    private ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURLS;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURLS = _oWebsitePage.GetSiteURLbySiteId(_siteId);
        }
        if (_siteId != IrAgentSiteID)
            Response.Redirect(siteURLS);
    }
}