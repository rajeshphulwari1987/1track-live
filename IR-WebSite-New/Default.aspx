﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" ValidateRequest="false" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/UcLoyaltyCard.ascx" TagName="LoyaltyCart" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucIrBanner.ascx" TagName="IrBanner" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucStaBanner.ascx" TagName="StaBanner" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/EurostarRequestQuote.ascx" TagName="Eurostar" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/JourneyRequestForm.ascx" TagName="StaJRF" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=omegaCss%>
    <style type="text/css">
        #cookiemessage {
            display: none;
        }

        .cookie-message a {
            color: #ffffff;
            text-decoration: underline;
            margin-right: 10px;
        }

            .cookie-message a:hover {
                text-decoration: none;
                color: #ffffff;
            }

        .cookie-message {
            background: rgba(0, 0, 0, 0.76);
            color: #ffffff;
            position: fixed;
            bottom: 61px;
            left: 0;
            font-size: 14px;
            -webkit-transform: translateY(100%);
            transform: translateY(100%);
            -webkit-transition: -webkit-transform 0.3s ease-in-out;
            transition: transform 0.3s ease-in-out;
            z-index: 50;
        }

        .fw-container {
            width: 100%;
            max-width: 2560px;
            margin: auto;
        }

        .cookie-message__inner {
            padding: 10px 0px 10px 0px;
            text-align: center;
        }

        .constrain {
            width: 90%;
            max-width: 1200px;
            margin: auto;
        }

        .cookie-message a {
            color: #ffffff;
            text-decoration: underline;
            margin-right: 10px;
        }

        .btn--primary, .btn--secondary {
            display: inline-block;
            vertical-align: middle;
            color: #565759;
            text-decoration: none;
            height: auto;
            text-align: center;
            width: 100%;
            min-height: 0;
            font-size: 18px;
            line-height: 22px;
            background: transparent;
            border: none;
            cursor: pointer;
        }

        .btn--primary {
            padding: 11px 24px;
            font-weight: 600;
            background-color: #F10000;
            color: #fff;
            border-radius: 5px;
        }

            .btn--primary:hover {
                background-color: #D10101;
                color: #fff;
            }

        .btn--secondary {
            padding: 10px 24px;
            font-weight: 600;
            background-color: #009D5C;
            color: #fff;
            border-radius: 5px;
        }

            .btn--secondary:hover {
                background-color: #007D49;
                color: #fff;
            }

        .btn--md {
            width: auto;
            padding: 10px 22px;
            font-size: 16px;
        }

        .btn--search {
            border-bottom-right-radius: 15px;
        }
    </style>
    <script type="text/javascript">
        function filterDetail(name){
            dataLayer.push({ "event": "railFilterAdded", "filterDetail": name});
        }

        function starail_Switcher_tab_width(){
            $(".is-eurostar").css("width","33.3%");
            $("#tab-menu-section").addClass("threetab");
        }

        function starail_Switcher_tab_width_fifty(){
            $(".is-eurostar").css("width","50%");
            $("#tab-menu-section").addClass("threetab");
        }
        var countryList = <%=datalist%>;
    var ispostback = false;
    $(document).ready(function () {
        setMenuActive("H");
        initcallSearch('<%=ispostbacksearch %>');
            SetUrlForHastag(); //#region hashtag 
        });
        function LoadCal1(){
            activeCrousal();
        }
        function LoadCal2(){
            activeCrousal();
        }
        function showhideJourney(){
            activeCrousal();
        } 
        function SetUrlForHastag() {
            $("#MainContent_ucLoyaltyCart_updProgress,#MainContent_updProgress").show();
            var url = document.location;
            var strippedUrl = url.toString().split("#");
            if (strippedUrl.length > 1) {
                var anchorValue = strippedUrl[1].toLowerCase().replace("passes", "");
                if (anchorValue != '') {
                    if (anchorValue == "railtickets") {
                        TabRailTicket();
                    } else {
                        var countriesArray = countryList;
                        var isCountry = false;
                        for (var i = 0; i < countriesArray.length; i++) {
                            if (countriesArray[i].replace(" ", "").toLowerCase() == anchorValue) {
                                $("#txtSearch").val(countriesArray[i]);
                                $('#hdnHashtag').val(countriesArray[i]);
                                i = countriesArray.length;
                                isCountry = true;
                                break;
                            }
                        }
                        if (!isCountry) {
                            $('#hdnHashtag').val(anchorValue);
                        }
                        $('#dopostback').click();
                        //$("#MainContent_btnSearch").trigger('click');//__doPostBack("dopostback","");
                        TabRailPass();
                    }
                }
            } 
            else if($('#hdnNewOrderID').val()!='') {
                TabRailTicket();
                $("#MainContent_ucLoyaltyCart_updProgress,#MainContent_updProgress").hide();
                $("#newproduct").show();
            }
            else{
                TabRailPass(); //default
                $("#newproduct").show();
                $("#MainContent_ucLoyaltyCart_updProgress,#MainContent_updProgress").hide();

            }
        }

        function calltest(){
            $("#newproduct").show();
            $("#MainContent_ucLoyaltyCart_updProgress,#MainContent_updProgress").hide();
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function TabRailTicket() {
            $("#starail-tickets").removeClass('starail-Switcher-content--hidden');
            $("#starail-passes,#starail-eurostar-tickets").addClass('starail-Switcher-content--hidden');
            $("#TabRailPass,#MainContent_TabEurostartickets").removeClass('starail-Switcher-tab--active');
            $("#TabRailTickets").addClass('starail-Switcher-tab--active');
            DatePickerCall2();
        }
        function TabRailPass() {
            $("#starail-tickets,#starail-eurostar-tickets").addClass('starail-Switcher-content--hidden');
            $("#starail-passes").removeClass('starail-Switcher-content--hidden');
            $("#TabRailPass").addClass('starail-Switcher-tab--active');
            $("#TabRailTickets,#MainContent_TabEurostartickets").removeClass('starail-Switcher-tab--active');
            DatePickerCall2();
        }
        function TabEurostarTicket() {
            $("#starail-eurostar-tickets").removeClass('starail-Switcher-content--hidden');
            $("#starail-tickets,#starail-passes").addClass('starail-Switcher-content--hidden');
            $("#TabRailPass,#TabRailTickets").removeClass('starail-Switcher-tab--active');
            $("#MainContent_TabEurostartickets").addClass('starail-Switcher-tab--active');
            DatePickerCall2();
        }
        function LoadRadioButton() {
            $(".loyspanradio").find('label').before("<span class='loyradioSpan' style='float:left;'><i class='radioSpancenter'></i></span>");
        }

        
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        #MainContent_div_HomePageHeading1 h1, #MainContent_div_HomePageHeading2 h2 {
            font-size: 22px;
            font-weight: 700;
            text-align: center;
            color: #fff;
            margin: 0;
            padding: 10px;
        }

    </style>
    <asp:HiddenField ID="hdnHashtag" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSearchText" runat="server" ClientIDMode="Static" />
    <asp:ScriptManager ID="scrpMng" runat="server" ScriptMode="Release" EnableCdn="true">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="assets/searchabletext/jquery.searchabletext.min.js" />
                <asp:ScriptReference Name="MicrosoftAjax.js" />
                <asp:ScriptReference Name="MicrosoftAjaxWebForms.js" />
                <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="DynamicPopulate.DynamicPopulateBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="RoundedCorners.RoundedCornersBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="DropShadow.DropShadowBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="Compat.DragDrop.DragDropScripts.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="DragPanel.FloatingBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="ModalPopup.ModalPopupBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="FilteredTextBox.FilteredTextBoxBehavior.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
                <asp:ScriptReference Name="TextboxWatermark.TextboxWatermark.js" Assembly="AjaxControlToolkit, Version=4.1.60919.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updSearchContinent" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="dopostback" runat="server" OnClick="dopostback_Click" ClientIDMode="Static"
                CssClass="displaynone" />
            <asp:Panel ID="pnlIrBanner" runat="server" Style="display: none;">
                <asp:TextBox ID="txtSearch" runat="server" autocomplete="off" CssClass="form-control"
                    placeholder="Enter a continent, country or pass name" ClientIDMode="Static" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn btn-default IR-GaCode"
                    Text="Go"></asp:Button>
            </asp:Panel>
            <uc1:IrBanner ID="IrBanner" runat="server" Visible="false" />
            <uc1:StaBanner ID="StaBanner" runat="server" />
            <div class="starai8l-Section starail-Section--nopadding">
                <div class="newboxmian-data heading1">
                    <div class="newboxdata-heading" id="div_HomePageHeading1" runat="server">
                    </div>
                    <div id="div_HomePageDescription1" runat="server">
                    </div>
                </div>
                <div class="starail-Switcher tabtwo-box">
                    <ul class="starail-Switcher-tabs threetab" id="tab-menu-section">
                        <li class="starail-Switcher-tab is-eurostar starail-Switcher-tab--active" id="TabRailPass">
                            <a href="#starail-passes" class="js-starail-Switcher-trigger" onclick="TabRailPass();">Rail Passes</a> </li>
                        <li class="starail-Switcher-tab is-eurostar" id="TabEurostartickets" visible="false"
                            runat="server"><a href="#starail-eurostar-tickets" class="js-starail-Switcher-trigger"
                                onclick="TabEurostarTicket();" id="anchor_JRF" runat="server">EUROSTAR TICKETS</a>
                        </li>
                        <li class="starail-Switcher-tab is-eurostar" id="TabRailTickets" runat="server"><a
                            href="#starail-tickets" class="js-starail-Switcher-trigger" id="Railtickets"
                            runat="server" onclick="TabRailTicket();">Rail Tickets</a> </li>
                    </ul>
                    <ul class="starail-Switcher-tabs twoli twotab not-active">
                        <li class="starail-Switcher-tab starail-Switcher-tab--active is-eurostar" id="Tab_RailPass">
                            <a href="#starail-passes" class="js-starail-Switcher-trigger">Rail Passes</a>
                        </li>
                        <li class="starail-Switcher-tab starail-Switcher-tab--active is-eurostar" id="Tab_RailTickets">
                            <a href="#starail-tickets" class="js-starail-Switcher-trigger">Rail Tickets</a>
                        </li>
                    </ul>
                    <%-- Rail Passess Start--%>
                    <div id="starail-passes" class="starail-Switcher-content">
                        <div class="starail-Filter" id="div_Filer" runat="server" visible="false">
                            <h1 class="starail-Filter-title starail-u-alpha">Rail Passes And Routes
                                <%=String.IsNullOrEmpty(ContinentName) ? "" : "in " + ContinentName%></h1>
                            <div class="starail-Filter-toggle starail-u-hideDesktop">
                                <a class="starail-Filter-toggle-trigger" href="#">Filter <span class="starail-Filter-chevron"></span></a>
                            </div>
                            <div class="starail-Filter-filterBar
                starail-u-cf"
                                style="max-height: auto;">
                                <asp:CheckBoxList ID="chkCountryList" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="chkCountryList_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </div>
                            <div class=" starail-u-hideMobile">
                                <asp:LinkButton ID="btnClearFilter" runat="server" class="starail-Filter-clear-trigger1 starail-Button starail-Button--blue1"
                                    OnClick="btnClearFilter_Click"><i class="starail-Icon starail-Icon-timescircle"></i>
                Clear filters</asp:LinkButton>
                            </div>
                            <div class="starail-Filter-more starail-u-hideMobile">
                                <a class="starail-Filter-more-trigger" onclick="ShowCountry()"><i class="starail-Icon
                starail-Icon-pluscircle"></i>Select More Countries</a> <a class="starail-Filter-less-trigger"
                    onclick="HideCountry()"><i class="starail-Icon starail-Icon-minuscircle"></i>Hide
                    countries</a>
                            </div>
                        </div>
                        <%-- Product Start --%>
                        <div runat="server" id="nosearchresultfound" visible="false" class="warning" style="background-image: none;">
                            No search result found.
                        </div>
                       <div id="newproduct" style="display:none;">
                        <asp:Repeater ID="rptProduct" runat="server">
                            <ItemTemplate>
                                <a class="starail-ImageLink linkbox-product IR-GaCode" irgacodetext='<%#Eval("Name")%>'
                                    href='<%#Eval("Url")
                %>'>
                                    <div class="starail-Tag starail-Tag--specialOffer" id="div_SpecialOffer" runat="server"
                                        visible='<%#Eval("IsSpecialOffer")%>'>
                                    </div>
                                    <div class="starail-Tag starail-Tag--bestSeller" id="div_BestSeller" runat="server"
                                        visible='<%#Eval("BestSeller")%>'>
                                    </div>
                                    <img src='<%# SiteUrl+""+Eval("ProductImage") %>' alt='<%# Eval("Name")+" Start From "+currency+" "+Eval("Price") %>'
                                        title='<%# Eval("Name")+" Start From "+currency+" "+Eval("Price") %>' width="242" height="242" />
                                    <div class="starail-ImageLink-titleContainer">
                                        <p class="starail-ImageLink-title">
                                            <span class="highlight">
                                                <%#Eval("Name")%></span>
                                        </p>
                                    </div>
                                    <div class="starail-ImageLink-overlay">
                                        <div class="starail-ImageLink-overlay-content">
                                            <p>
                                                <%#Eval("Desciption")%>
                                            </p>
                                            <div class="starail-Button" <%#Convert.ToBoolean(Eval("IsFlag"))?"style='display:none;'":""%>>
                                                <%#Convert.ToDecimal(Eval("Price").ToString())>0?"From "+currency+" "+Eval("Price")+"<br/>":""%>
                                                Find out more
                                            </div>
                                            <div class="starail-Button" <%#Convert.ToBoolean(Eval("IsFlag"))?"":"style='display:none;'"%>>
                                                Find out more
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>

                       </div>
                        <%-- Product End --%>
                        <div class="clearfix">
                        </div>
                        <hr id="hr_home" runat="server" />
                        <span class="viewmorebox" id="anchor_showMore" runat="server" onclick="showIR8Products();">View More</span>
                        <div class="clearfix">
                        </div>
                    </div>
                    <%-- Rail Passess End --%>
                    <%-- Rail Tickets Start --%>
                    <div id="starail-tickets" class="starail-Switcher-content starail-Switcher-content--hidden">
                        <div class="starail-SearchTickets">
                            <h1 class="starail-SearchTickets-title starail-u-alpha">Tell us <span class="starail-u-hideMobile">more </span>about your trip</h1>
                            <div class="starail-Box starail-Box--whiteMobile">
                                <uc1:LoyaltyCart ID="ucLoyaltyCart" runat="server" />
                                <!-- .starail-Form -->
                            </div>
                            <asp:Panel ID="pnlErrSuccess" runat="server">
                                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                                    <asp:Label ID="lblSuccessMsg" runat="server" />
                                </div>
                                <div id="DivError" runat="server" class="error" style="display: none;">
                                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <%-- Rail Tickets End --%>
                    <%-- Eurostar Tickets End --%>
                    <div id="starail-eurostar-tickets" class="starail-Switcher-content starail-Switcher-content--hidden">
                        <uc1:Eurostar ID="eurostar" runat="server" Visible="false" />
                        <uc1:StaJRF ID="StaJRF" runat="server" Visible="false" />
                    </div>
                    <div class="newboxmian-data heading2">
                        <div class="newboxdata-heading" id="div_HomePageHeading2" runat="server">
                        </div>
                        <div id="div_HomePageDescription2" runat="server">
                            .
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                    <%-- Eurostar Tickets End --%>
                </div>
            </div>
            <%--Cookies--%>
            <div id="cookiemessage" class="cookie-message fw-container" runat="server" clientidmode="Static">
                <div class="cookie-message__inner constrain">
                    <span>This website uses cookies to ensure you get the best experience on our website.Click
                        here to see our </span><a href="/cookies" target="_blank">Cookie Policy</a>
                    <a id="cookiesgotit" class="btn btn--md btn--secondary cookie-button">Got it!</a>
                </div>
            </div>
            <%--Cookies END--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updSearchContinent"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                UPDATING RESULTS...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField runat="server" ID="hdnCountryChkbox" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdncountrylistshow" ClientIDMode="Static" />
    <script src="Scripts/js.cookie.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(window).load(function () {
            if (!Cookies.get('EtchCookieConsent')) {
                jQuery('#cookiemessage').show();
            }
            else
                jQuery('#cookiemessage').hide();

            jQuery("#cookiesgotit").click(function () {
                if (!Cookies.get('EtchCookieConsent')) {
                    Cookies.set('EtchCookieConsent', true, { expires: 365 });
                }
                jQuery('#cookiemessage').hide();
            });
        });
    
    </script>
    <script type="text/javascript">
        function ShowCountry() {
            $('.starail-Filter-more-trigger').parents().find('.starail-Filter').addClass('starail-Filter--open');
            $("#hdncountrylistshow").val("ok");
        }
        function HideCountry() {
            $('.starail-Filter-less-trigger').parents().find('.starail-Filter').removeClass('starail-Filter--open');
            $("#hdncountrylistshow").val("");
        }
        function FilterCall() {
            $('.starail-Filter-toggle-trigger').on('click', function (e) {
                $(this).parents().find('.starail-Filter').toggleClass('starail-Filter--open');
                e.preventDefault();
            });
            $(".loycheckboxSpan").remove();
            if ($(".loycheckbox").parent('label').length < 1) {
                $(".loycheckbox").wrap("<label class='starail-Form-label starail-Form-label--checkbox starail-Filter-filter width_25p'>");
                $(".loycheckbox").find('label').before("<span><i class='starail-Icon-tick'></i></span>");
            }
        }
        function chkcountry(obj) {
            var ss = $(obj).parent().find("input[type='checkbox']").val();
            $("#hdnCountryChkbox").val(ss);
        }
    </script>
    <script type="text/javascript">
        size_a = $("#starail-passes a.linkbox-product").size();
        var x = 8;
        if ($('.starail-Switcher').hasClass('tabtwo-box')) {
            x = 9;
        }
        $(document).ready(function () {
            $(".loycheckboxSpan").remove();

            $('div.item').eq(0).addClass('active').end();
            if ($('.viewmorebox').is(':visible') == true) {
                if ($('.starail-Switcher').hasClass('tabtwo-box')) {
                    if (size_a > 9) {
                        $('#starail-passes a.linkbox-product:gt(8)').hide();
                    }
                    else {
                        $('.viewmorebox').hide();
                    }
                }
                else {
                    if (size_a > 8) {
                        $('#starail-passes a.linkbox-product:gt(7)').hide();
                    }
                    else {
                        $('.viewmorebox').hide();
                    }
                }

                if ($('#MainContent_div_HomePageDescription1').text() == "" || $('#MainContent_div_HomePageHeading1').text() == "") {
                    $('.heading1').hide();
                }
                if ($('#MainContent_div_HomePageDescription2').text() == "" || $('#MainContent_div_HomePageHeading2').text() == "") {
                    $('.heading2').hide();
                }
            }
        });
        function activeCrousal() {
            $(".carousel-inner div").removeClass("active");
            $(".carousel-indicators li").removeClass("active");

            $(".carousel-indicators li:first-child").addClass("active");
            $(".carousel-inner div:first-child").addClass("active");
        }

        function DatePickerCall2() {
            $(document).ready(function () {
                IrTextChanges();
                activeCrousal();
                callvalerror();
                showhideJourney();
                activeJRFTab();
                $(".loycheckboxSpan").remove();
                if ($(".loycheckbox").parent('label').length < 1) {
                    $(".loycheckbox").wrap("<label class='starail-Form-label starail-Form-label--checkbox starail-Filter-filter width_25p'>");
                    $(".loycheckbox").find('label').before("<span><i class='starail-Icon-tick'></i></span>");
                }
                $(".loyradioSpan").remove();
                $(".loyspanradio").find('label').before("<span class='loyradioSpan' style='float:left;'><i class='radioSpancenter'></i></span>");

                $("[id*=UcQuotationForm2_txtDepartureDate],[id*=UcQuotationForm1_txtDepartureDate]").datepicker({
                    numberOfMonths: 1,
                    dateFormat: 'dd/M/yy',
                    beforeShowDay: nationalDays,
                    showButtonPanel: true,
                    firstDay: 1,
                    minDate: '<%=minDate %>'
                });

                $("#MainContent_ucLoyaltyCart_txtDepartureDate").datepicker({
                    numberOfMonths: 1,
                    dateFormat: 'dd/M/yy',
                    beforeShowDay: nationalDays,
                    showButtonPanel: true,
                    firstDay: 1,
                    minDate: '<%=minDate %>',
                    onClose: function (selectedDate) {
                        $("#MainContent_ucLoyaltyCart_txtReturnDate").datepicker("option", "minDate", selectedDate);
                    }
                });

                $("#MainContent_ucLoyaltyCart_txtReturnDate").datepicker({
                    numberOfMonths: 1,
                    dateFormat: 'dd/M/yy',
                    beforeShowDay: nationalDays,
                    showButtonPanel: true,
                    firstDay: 1,
                    minDate: $("#MainContent_ucLoyaltyCart_txtDepartureDate").val()
                });


                $("#MainContent_ucLoyaltyCart_rdBookingType").find('td').each(function (key, value) {
                    if ($(this).find('label input').val() != undefined) {
                        $("#MainContent_ucLoyaltyCart_rdBookingType").find('td').each(function (key, value) {
                            var aa = $(this).find('label').text();
                            var bb = $(this).find('label').attr('for');
                            var xx = $(this).find('label input').first();
                            $(this).find('label').remove(); $(this).append(xx).append("<label for=" + bb + " >" + aa + "</label>");
                        });
                        $("#MainContent_ucLoyaltyCart_ddlClass").find('td').each(function (key, value) {
                            var aa = $(this).find('label').text();
                            var bb = $(this).find('label').attr('for');
                            var xx = $(this).find('label input').first();
                            $(this).find('label').remove();
                            $(this).find('span .loyradioSpan').before(xx);
                            $(this).find('span .loyradioSpan').after("<label for=" + bb + " >" + aa + "</label>");
                        });
                    }

                });

                $("[id*=UcQuotationForm1_rdBookingType] tr td").each(function (key, value) {
                    if ($(this).find('label input').val() == undefined) {
                        $("[id*=UcQuotationForm1_rdBookingType],[id*=UcQuotationForm1_ddlClass],[id*=UcQuotationForm2_ddlClass]").addClass("new-table-section");
                    }
                    else {
                        $("[id*=UcQuotationForm1_rdBookingType],[id*=UcQuotationForm1_ddlClass],[id*=UcQuotationForm2_ddlClass]").removeClass("new-table-section");
                    }
                });
            });
        }
        function showIR8Products() {
            $('#starail-passes a:lt(' + size_a + ')').show();
            $('.viewmorebox').hide();

            $('.starail-Filter-more-trigger').removeAttr("style");
            $('.starail-Filter-less-trigger').removeAttr("style");
        }

        function threeTabVisible() {
            $('.threetab').css({ 'display': 'block' });
            $('.twotab').css({ 'display': 'none' });
            $('.starail-Switcher').removeClass('tabtwo-box');
        }

        function twoTabVisible() {
            $('.threetab').css({ 'display': 'none' });
            $('.twotab').css({ 'display': 'block' });
            if ($(window).width() < 768) {
                $('.starail-Switcher').removeClass('tabtwo-box');
                $('#Tab_RailTickets').removeClass('starail-Switcher-tab--active');
                $('#MainContent_ucLoyaltyCart_lblShowIR').hide();
                $('#MainContent_ucLoyaltyCart_lblNotShowIR').show();
                $('#MainContent_ucLoyaltyCart_lblAdult').show();
                $('#MainContent_ucLoyaltyCart_lblChild').show();
                $('#MainContent_ucLoyaltyCart_lblYouth').show();
                $('#MainContent_ucLoyaltyCart_lblSenior').show();
                $('#MainContent_ucLoyaltyCart_lblIRAdult').hide();
                $('#MainContent_ucLoyaltyCart_lblIRChild').hide();
                $('#MainContent_ucLoyaltyCart_lblIRYouth').hide();
                $('#MainContent_ucLoyaltyCart_lblIRSenior').hide();
                $('#MainContent_ucLoyaltyCart_pnlTravellerPopUP').hide();
                removeIrClasses();
            }
        }
        function activeJRFTab() {
            if ('<%=isBookMyRst %>' == 'True') {
                $('#TabRailPass').removeClass('starail-Switcher-tab--active');
                $('#MainContent_TabEurostartickets').addClass('starail-Switcher-tab--active');
                $('#starail-passes').addClass('starail-Switcher-content--hidden');
                $('#starail-eurostar-tickets').removeClass('starail-Switcher-content--hidden');
            }
        }
    </script>
</asp:Content>
