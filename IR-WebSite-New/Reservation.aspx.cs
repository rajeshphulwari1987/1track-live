﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using OneHubServiceRef;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


public partial class Reservation : Page
{
    BookingRequestUserControl objBRUC;
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    ManageTrainDetails _master = new ManageTrainDetails();
    readonly Masters _objMaster = new Masters();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string _FromDate;
    public string _ToDate;
    ManageOneHub _ManageOneHub = new ManageOneHub();
    List<EvolviReservationRef> EvReservationRefList = new List<EvolviReservationRef>();
    public bool isBerthOutBound, isBerthInBound = false;
    List<BerthPassenger> BerthoutassengerList = new List<BerthPassenger>();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            GetCurrencyCode();
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["req"] != null)
                    GetJourneyDetails();
                else
                    Response.Redirect("TrainResults.aspx?req=EV", true);

                LoadTraveller();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetJourneyDetails()
    {
        try
        {
            List<ReservationCompanyInfo> objReservationCompInfo = new List<ReservationCompanyInfo>();
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            List<ReservationInfo> ReservationInfoList = Session["ReservationInfoList"] as List<ReservationInfo>;

            if (ReservationInfoList != null && ReservationInfoList.Count > 0)
            {
                lblTotal.Text = currency + ReservationInfoList.Sum(x => Convert.ToDecimal(x.Price)).ToString("F2");
                hdnTotal.Value = ReservationInfoList.Sum(x => Convert.ToDecimal(x.Price)).ToString("F2");
            }

            if (pInfoSolutionsResponse != null)
            {
                if (ReservationInfoList != null && ReservationInfoList.Count > 0)
                {
                    var outJourney = ReservationInfoList.FirstOrDefault(x => x.JourneyType == "Outbound");
                    if (outJourney != null)
                    {
                        var OutBoundList = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(x => x.IsReturn == false && x.TrainNumber == outJourney.JourneyIdentifier);
                        if (OutBoundList != null)
                        {
                            #region OutBound
                            var journeyResponse = new JourneyDetailsResponse();
                            if (Session["HoldOldJourneyResponse"] != null)
                            {
                                var res = Session["HoldOldJourneyResponse"] as List<HoldOldJourneyResponse>;
                                if (res.Any(x => x.isOutBound && x.Res != null))
                                    journeyResponse = res.FirstOrDefault(x => x.isOutBound).Res;
                                else
                                    journeyResponse = JourneyDetailsRequest(OutBoundList, "Outbound");
                            }
                            else
                                journeyResponse = JourneyDetailsRequest(OutBoundList, "Outbound");

                            if (journeyResponse != null && journeyResponse.TrainFareSegmentList != null)
                            {
                                var JourneyReserveList = journeyResponse.TrainFareSegmentList.SelectMany(x => x.EvLegInfoList != null ? x.EvLegInfoList.Select((y, i) => new JourneyReservationInfo
                                {
                                    Index = i + 1,
                                    DepartureStation = y.DepartureStation,
                                    ArrivalStation = y.ArrivalStation,
                                    DepartureDate = y.DepartureDate,
                                    DepartureDateTime = y.DepartureDate + " " + y.DepartureTime,
                                    CompanyName = string.IsNullOrEmpty(y.CompanyName) ? "(UNDERGROUND)" : "(" + y.CompanyName + ")",
                                    TravellBy = string.IsNullOrEmpty(y.TravellBy) ? "" : "(" + y.TravellBy + ")",
                                    Reservable = y.Reservable.ToString(),
                                    LegId = y.LegId,
                                    ReservationStatus = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().ReservableStatus.ToString()
                                    : string.Empty : string.Empty,
                                    MatchLegID = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().LegID
                                    : string.Empty : string.Empty,
                                    AccommodationType = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().AccommodationType.ToString()
                                    : string.Empty : string.Empty,
                                    IsSleeper = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().IsSleeperService
                                    : false : false,
                                    fareSetterCode = outJourney.FareSetterCode,

                                    isBerthSleeper = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString() && z.EvBirthSupplementList != null && z.EvBirthSupplementList.Length > 0)
                                    : false : false,

                                    isSeatSleeper = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString() && z.EvSeatSupplementList != null && z.EvSeatSupplementList.Length > 0)
                                    : false : false,
                                }).ToList() : null).ToList();

                                if (JourneyReserveList != null && JourneyReserveList.Count > 0)
                                {
                                    objReservationCompInfo = JourneyReserveList.Select(x => new ReservationCompanyInfo
                                    {
                                        CompanyName = x.CompanyName,
                                        TravellBy = x.TravellBy,
                                        LegId = x.LegId
                                    }).ToList();
                                }
                                rptOutBound.DataSource = JourneyReserveList.ToList();
                                rptOutBound.DataBind();

                                if (JourneyReserveList.Any(x => x.IsSleeper == true))
                                {
                                    var berthInfoList = journeyResponse.TrainFareSegmentList.FirstOrDefault().EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.FirstOrDefault().EvBirthSupplementList != null
                                       ? journeyResponse.TrainFareSegmentList.FirstOrDefault().EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.FirstOrDefault().EvBirthSupplementList.ToList()
                                       : null;

                                    string ReservationStatus = JourneyReserveList.FirstOrDefault().ReservationStatus;
                                    string adultPrice = "0";
                                    string childPrice = "0";
                                    string adultChildPrice = "0";
                                    string twoChildPrice = "0";
                                    string soloAdultOrChildPrice = "0";
                                    string OneBerthAdultMF = "0";

                                    bool isSolo = berthInfoList != null && berthInfoList.Count > 0 ? berthInfoList.Any(x => x.Type == BerthTypeEnum.SOLO) ? true : false : false;
                                    bool isMaleFemale = berthInfoList != null && berthInfoList.Count > 0 ? berthInfoList.Any(x => x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML) ? true : false : false;
                                    bool isTogr = berthInfoList != null && berthInfoList.Count > 0 ? berthInfoList.Any(x => x.Type == BerthTypeEnum.TOGR) ? true : false : false;
                                    if (berthInfoList != null && berthInfoList.Count > 0)
                                    {
                                        if (isTogr && isSolo)
                                        {
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Adult" || x.AdultOrChild.ToString() == "Child"))
                                            {
                                                adultChildPrice = adultPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Adult" && x.Type == BerthTypeEnum.TOGR).Amount;
                                                twoChildPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Child" && x.Type == BerthTypeEnum.TOGR).Amount;
                                            }
                                            else if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both"))
                                                adultChildPrice = twoChildPrice = adultPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.TOGR).Amount;

                                            if (isSolo)
                                                soloAdultOrChildPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.SOLO).Amount;

                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                        else if (isTogr)
                                        {
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Adult" || x.AdultOrChild.ToString() == "Child"))
                                            {
                                                adultChildPrice = adultPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Adult" && x.Type == BerthTypeEnum.TOGR).Amount;
                                                twoChildPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Child" && x.Type == BerthTypeEnum.TOGR).Amount;
                                            }
                                            else if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both"))
                                                adultChildPrice = twoChildPrice = adultPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.TOGR).Amount;

                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                        else if (isSolo)
                                        {
                                            soloAdultOrChildPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.SOLO).Amount;
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                        else
                                        {
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                    }
                                    FillBerthByTraveller(true, isSolo, isMaleFemale, isTogr, adultPrice, childPrice, adultChildPrice, twoChildPrice, soloAdultOrChildPrice, OneBerthAdultMF, ReservationStatus);
                                }
                            }
                            #endregion
                        }
                    }

                    var inJourney = ReservationInfoList.FirstOrDefault(x => x.JourneyType == "Inbound");
                    if (inJourney != null)
                    {
                        div_Inbound.Attributes.Add("style", "display:block;");
                        var InBoundList = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault(x => x.IsReturn && x.TrainNumber == inJourney.JourneyIdentifier);
                        if (InBoundList != null)
                        {
                            #region InBound
                            var journeyResponse = new JourneyDetailsResponse();
                            if (Session["HoldOldJourneyResponse"] != null)
                            {
                                var res = Session["HoldOldJourneyResponse"] as List<HoldOldJourneyResponse>;
                                if (res.Any(x => x.isOutBound == false && x.Res != null))
                                    journeyResponse = res.FirstOrDefault(x => x.isOutBound == false).Res;
                                else
                                    journeyResponse = JourneyDetailsRequest(InBoundList, "Inbound");
                            }
                            else
                                journeyResponse = JourneyDetailsRequest(InBoundList, "Inbound");

                            if (journeyResponse != null && journeyResponse.TrainFareSegmentList != null)
                            {
                                var JourneyReserveList = journeyResponse.TrainFareSegmentList.SelectMany(x => x.EvLegInfoList != null ? x.EvLegInfoList.Select((y, i) => new JourneyReservationInfo
                                {
                                    Index = i + 1,
                                    DepartureStation = y.DepartureStation,
                                    ArrivalStation = y.ArrivalStation,
                                    DepartureDate = y.DepartureDate,
                                    DepartureDateTime = y.DepartureDate + " " + y.DepartureTime,
                                    CompanyName = string.IsNullOrEmpty(y.CompanyName) ? "(UNDERGROUND)" : "(" + y.CompanyName + ")",
                                    TravellBy = string.IsNullOrEmpty(y.TravellBy) ? "" : "(" + y.TravellBy + ")",
                                    Reservable = y.Reservable.ToString(),
                                    LegId = y.LegId,
                                    ReservationStatus = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().ReservableStatus.ToString()
                                    : string.Empty : string.Empty,
                                    MatchLegID = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().LegID
                                    : string.Empty : string.Empty,
                                    AccommodationType = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().AccommodationType.ToString()
                                    : string.Empty : string.Empty,
                                    IsSleeper = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Where(z => z.LegID == (i + 1).ToString()).FirstOrDefault().IsSleeperService
                                    : false : false,
                                    fareSetterCode = inJourney.FareSetterCode,

                                    isBerthSleeper = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString() && z.EvBirthSupplementList != null && z.EvBirthSupplementList.Length > 0)
                                    : false : false,

                                    isSeatSleeper = x.EvJourneyFareCollectionList != null ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString())
                                    ? x.EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.Any(z => z.LegID == (i + 1).ToString() && z.EvSeatSupplementList != null && z.EvSeatSupplementList.Length > 0)
                                    : false : false,
                                }).ToList() : null).ToList();

                                if (JourneyReserveList != null && JourneyReserveList.Count > 0)
                                {
                                    objReservationCompInfo.AddRange(JourneyReserveList.Select(x => new ReservationCompanyInfo
                                    {
                                        CompanyName = x.CompanyName,
                                        TravellBy = x.TravellBy,
                                        LegId = x.LegId
                                    }).ToList());
                                }
                                rptInBound.DataSource = JourneyReserveList.ToList();
                                rptInBound.DataBind();

                                if (JourneyReserveList.Any(x => x.IsSleeper == true))
                                {
                                    var berthInfoList = journeyResponse.TrainFareSegmentList.FirstOrDefault().EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.FirstOrDefault().EvBirthSupplementList != null
                                        ? journeyResponse.TrainFareSegmentList.FirstOrDefault().EvJourneyFareCollectionList.EvJourneyFareLegCollectionList.FirstOrDefault().EvBirthSupplementList.ToList()
                                        : null;
                                    string ReservationStatus = JourneyReserveList.FirstOrDefault().ReservationStatus;
                                    string adultPrice = "0";
                                    string childPrice = "0";
                                    string adultChildPrice = "0";
                                    string twoChildPrice = "0";
                                    string soloAdultOrChildPrice = "0";
                                    string OneBerthAdultMF = "0";

                                    bool isSolo = berthInfoList != null && berthInfoList.Count > 0 ? berthInfoList.Any(x => x.Type == BerthTypeEnum.SOLO) ? true : false : false;
                                    bool isMaleFemale = berthInfoList != null && berthInfoList.Count > 0 ? berthInfoList.Any(x => x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML) ? true : false : false;
                                    bool isTogr = berthInfoList != null && berthInfoList.Count > 0 ? berthInfoList.Any(x => x.Type == BerthTypeEnum.TOGR) ? true : false : false;
                                    if (berthInfoList != null && berthInfoList.Count > 0)
                                    {
                                        if (isTogr && isSolo)
                                        {
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Adult" || x.AdultOrChild.ToString() == "Child"))
                                            {
                                                adultChildPrice = adultPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Adult" && x.Type == BerthTypeEnum.TOGR).Amount;
                                                twoChildPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Child" && x.Type == BerthTypeEnum.TOGR).Amount;
                                            }
                                            else if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both"))
                                                adultChildPrice = twoChildPrice = adultPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.TOGR).Amount;

                                            if (isSolo)
                                                soloAdultOrChildPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.SOLO).Amount;

                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                        else if (isTogr)
                                        {
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Adult" || x.AdultOrChild.ToString() == "Child"))
                                            {
                                                adultChildPrice = adultPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Adult" && x.Type == BerthTypeEnum.TOGR).Amount;
                                                twoChildPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Child" && x.Type == BerthTypeEnum.TOGR).Amount;
                                            }
                                            else if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both"))
                                                adultChildPrice = twoChildPrice = adultPrice = childPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.TOGR).Amount;

                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                        else if (isSolo)
                                        {
                                            soloAdultOrChildPrice = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && x.Type == BerthTypeEnum.SOLO).Amount;
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                        else
                                        {
                                            if (berthInfoList.Any(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)))
                                                OneBerthAdultMF = berthInfoList.FirstOrDefault(x => x.AdultOrChild.ToString() == "Both" && (x.Type == BerthTypeEnum.MALE || x.Type == BerthTypeEnum.FEML)).Amount;
                                        }
                                    }
                                    FillBerthByTraveller(false, isSolo, isMaleFemale, isTogr, adultPrice, childPrice, adultChildPrice, twoChildPrice, soloAdultOrChildPrice, OneBerthAdultMF, ReservationStatus);
                                }
                            }
                            #endregion
                        }
                    }
                    else
                        div_Inbound.Attributes.Add("style", "display:none;");
                    Session["ReservationCompInfo"] = objReservationCompInfo;
                }
                else
                    Response.Redirect("TrainResults.aspx?req=EV", true);
            }
            else
                Response.Redirect("TrainResults.aspx?req=EV", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public JourneyDetailsResponse JourneyDetailsRequest(TrainInformation objTrainInfo, string JourneyType)
    {
        try
        {
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            JourneyDetailsRequest request = new JourneyDetailsRequest();
            JourneyDetailsResponse response = new JourneyDetailsResponse();
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            List<ReservationInfo> ReservationInfoList = Session["ReservationInfoList"] as List<ReservationInfo>;

            if (objTrainInfo != null)
            {
                string TrainNo = objTrainInfo.TrainNumber.ToString();
                if (pInfoSolutionsResponse.EvolviPayloadAttributesInfo != null)
                {
                    request = new JourneyDetailsRequest
                    {
                        EchoToken = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.EchoToken,
                        Version = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.Version,
                        TransactionIdentifier = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TransactionIdentifier,
                        TimeStamp = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TimeStamp,
                        SequenceNmbr = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.SequenceNmbr,
                        TargetType = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TargetType,
                        ISOCountry = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCountry,
                        ISOCurrency = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCurrency,

                        EvJourneyInfo = new List<EvJourneyInfo> 
                        {
                            new EvJourneyInfo
                            {
                                JourneyIdentifier=TrainNo,
                                JourneyFareInfo=objTrainInfo.PriceInfo != null ? objTrainInfo.PriceInfo.SelectMany(x => x.TrainPriceList != null ? x.TrainPriceList.Where(m => m.FareId == ReservationInfoList.FirstOrDefault(n => n.JourneyType == JourneyType).FareId).Select(y =>new JourneyFareInfo
                                {
                                    FareId=y.FareId
                                }) : null).ToArray() : null
                            }
                        }.ToArray()
                    };
                    _ManageOneHub.ApiLogin(request, siteId);

                    response = client.EvolviJourneyDetails(request);
                    if (response != null && response.TrainFareSegmentList != null && response.TrainFareSegmentList.Length > 0)
                    {
                        var res = new List<HoldOldJourneyResponse>();
                        if (Session["HoldOldJourneyResponse"] != null)
                        {
                            res = Session["HoldOldJourneyResponse"] as List<HoldOldJourneyResponse>;
                            res.Add(new HoldOldJourneyResponse { Res = response, isOutBound = (JourneyType == "Outbound" ? true : false) });
                        }
                        else
                            res.Add(new HoldOldJourneyResponse { Res = response, isOutBound = (JourneyType == "Outbound" ? true : false) });
                        Session["HoldOldJourneyResponse"] = res;
                    }
                    return response.TrainFareSegmentList != null ? response : null;
                }
                return null;
            }
            return null;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return null; }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rptOutBound_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            try
            {
                DropDownList ddlCoach = e.Item.FindControl("ddlCoach") as DropDownList;
                DropDownList ddlSeat = e.Item.FindControl("ddlSeat") as DropDownList;
                var coachList = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                ddlCoach.DataSource = coachList;
                ddlCoach.DataBind();
                ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
                for (int i = 1; i <= 99; i++)
                {
                    ddlSeat.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlSeat.Items.Insert(0, new ListItem("Select", "0"));

                HiddenField hdnIndex = e.Item.FindControl("hdnIndex") as HiddenField;
                HiddenField hdnMatchLegId = e.Item.FindControl("hdnMatchLegId") as HiddenField;
                HiddenField hdnReservationStatus = e.Item.FindControl("hdnReservationStatus") as HiddenField;
                HiddenField hdnAccommodationType = e.Item.FindControl("hdnAccommodationType") as HiddenField;
                HiddenField hdnIsSleeper = e.Item.FindControl("hdnIsSleeper") as HiddenField;
                HtmlTable div_Reservation1 = e.Item.FindControl("div_Reservation1") as HtmlTable;
                HtmlTable div_Reservation2 = e.Item.FindControl("div_Reservation2") as HtmlTable;

                RadioButton rdnNoSeat = e.Item.FindControl("rdnNoSeat") as RadioButton;
                RadioButton rdnSeat = e.Item.FindControl("rdnSeat") as RadioButton;
                RadioButton rdnSpecificSeat = e.Item.FindControl("rdnSpecificSeat") as RadioButton;
                RadioButton rdnBerth = e.Item.FindControl("rdnBerth") as RadioButton;
                RadioButton rdnBerthExtra = e.Item.FindControl("rdnBerthExtra") as RadioButton;

                HiddenField hdnisBerthSleeper = e.Item.FindControl("hdnisBerthSleeper") as HiddenField;
                HiddenField hdnisSeatSleeper = e.Item.FindControl("hdnisSeatSleeper") as HiddenField;
                if (hdnIndex.Value == hdnMatchLegId.Value)
                {
                    if (hdnReservationStatus.Value == "SeatsMandatory")
                    {
                        if (Convert.ToBoolean(hdnIsSleeper.Value))
                        {
                            if (Convert.ToBoolean(hdnisBerthSleeper.Value) == false && Convert.ToBoolean(hdnisSeatSleeper.Value))
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "RecliningSeat")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "BerthAtExtraCost")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "Seat")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                            }
                        }
                        else
                        {
                            rdnSeat.Checked = true;
                            rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                        }
                    }
                    else if (hdnReservationStatus.Value == "SeatsRecommended")
                    {
                        rdnNoSeat.Checked = true;
                        rdnBerth.Visible = rdnBerthExtra.Visible = false;
                    }
                    else if (hdnReservationStatus.Value == "SeatsOptional")
                    {
                        rdnNoSeat.Checked = true;
                        rdnBerth.Visible = rdnBerthExtra.Visible = false;
                    }
                    else if (hdnReservationStatus.Value == "BerthsMandatory")
                    {
                        if (Convert.ToBoolean(hdnIsSleeper.Value))
                        {
                            if (hdnAccommodationType.Value == "BerthInclusive")
                            {
                                rdnBerth.Checked = true;
                                rdnNoSeat.Visible = rdnSeat.Visible = rdnSpecificSeat.Visible = rdnBerthExtra.Visible = false;
                                div_Berth_Traveller.Attributes.Add("style", "display:block;");
                            }
                            else if (hdnAccommodationType.Value == "BerthAtExtraCost")
                            {
                                rdnBerthExtra.Checked = true;
                                rdnNoSeat.Visible = rdnSeat.Visible = rdnSpecificSeat.Visible = rdnBerth.Visible = false;
                                div_Berth_Traveller.Attributes.Add("style", "display:block;");
                            }
                        }
                    }
                    else if (hdnReservationStatus.Value == "BerthsOptional")
                    {
                        if (Convert.ToBoolean(hdnIsSleeper.Value))
                        {
                            if (hdnAccommodationType.Value == "BerthAtExtraCost")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = false;
                            }
                            else
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        rdnNoSeat.Checked = true;
                        rdnSeat.Visible = rdnSpecificSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                    }

                    if (Convert.ToBoolean(hdnIsSleeper.Value))
                        div_Berth_Traveller.Attributes.Add("style", "display:block;");

                    div_Reservation1.Attributes.Add("style", "display:block;");
                    div_Reservation2.Attributes.Add("style", "display:none;");
                }
                else
                {
                    div_Reservation1.Attributes.Add("style", "display:none;");
                    div_Reservation2.Attributes.Add("style", "display:block;");
                }

                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                if (objBRUC != null)
                {
                    hdnNoOfPassenger.Value = (objBRUC.Adults + objBRUC.Boys + objBRUC.Youths + objBRUC.Seniors).ToString();
                    if ((objBRUC.Adults + objBRUC.Boys + objBRUC.Youths + objBRUC.Seniors) > 1)
                        rdnSpecificSeat.Visible = false;
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }

    protected void rptInBound_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            try
            {
                DropDownList ddlCoach = e.Item.FindControl("ddlCoach") as DropDownList;
                DropDownList ddlSeat = e.Item.FindControl("ddlSeat") as DropDownList;
                var coachList = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                ddlCoach.DataSource = coachList;
                ddlCoach.DataBind();

                ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
                for (int i = 1; i <= 99; i++)
                {
                    ddlSeat.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlSeat.Items.Insert(0, new ListItem("Select", "0"));

                HiddenField hdnIndex = e.Item.FindControl("hdnIndex") as HiddenField;
                HiddenField hdnMatchLegId = e.Item.FindControl("hdnMatchLegId") as HiddenField;
                HiddenField hdnReservationStatus = e.Item.FindControl("hdnReservationStatus") as HiddenField;
                HiddenField hdnAccommodationType = e.Item.FindControl("hdnAccommodationType") as HiddenField;
                HiddenField hdnIsSleeper = e.Item.FindControl("hdnIsSleeper") as HiddenField;

                HtmlTable div_Reservation1 = e.Item.FindControl("div_Reservation1") as HtmlTable;
                HtmlTable div_Reservation2 = e.Item.FindControl("div_Reservation2") as HtmlTable;

                RadioButton rdnNoSeat = e.Item.FindControl("rdnNoSeat") as RadioButton;
                RadioButton rdnSeat = e.Item.FindControl("rdnSeat") as RadioButton;
                RadioButton rdnSpecificSeat = e.Item.FindControl("rdnSpecificSeat") as RadioButton;
                RadioButton rdnBerth = e.Item.FindControl("rdnBerth") as RadioButton;
                RadioButton rdnBerthExtra = e.Item.FindControl("rdnBerthExtra") as RadioButton;

                HiddenField hdnisBerthSleeper = e.Item.FindControl("hdnisBerthSleeper") as HiddenField;
                HiddenField hdnisSeatSleeper = e.Item.FindControl("hdnisSeatSleeper") as HiddenField;
                if (hdnIndex.Value == hdnMatchLegId.Value)
                {
                    if (hdnReservationStatus.Value == "SeatsMandatory")
                    {
                        if (Convert.ToBoolean(hdnIsSleeper.Value))
                        {
                            if (Convert.ToBoolean(hdnisBerthSleeper.Value) == false && Convert.ToBoolean(hdnisSeatSleeper.Value))
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "RecliningSeat")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "BerthAtExtraCost")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "Seat")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                            }
                        }
                        else
                        {
                            rdnSeat.Checked = true;
                            rdnNoSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                        }
                    }
                    else if (hdnReservationStatus.Value == "SeatsRecommended")
                    {
                        rdnNoSeat.Checked = true;
                        rdnBerth.Visible = rdnBerthExtra.Visible = false;
                    }
                    else if (hdnReservationStatus.Value == "SeatsOptional")
                    {
                        rdnNoSeat.Checked = true;
                        rdnBerth.Visible = rdnBerthExtra.Visible = false;
                    }
                    else if (hdnReservationStatus.Value == "BerthsMandatory")
                    {
                        if (Convert.ToBoolean(hdnIsSleeper.Value))
                        {
                            if (hdnAccommodationType.Value == "BerthInclusive")
                            {
                                rdnBerth.Checked = true;
                                rdnNoSeat.Visible = rdnSeat.Visible = rdnSpecificSeat.Visible = rdnBerthExtra.Visible = false;
                            }
                            else if (hdnAccommodationType.Value == "BerthAtExtraCost")
                            {
                                rdnBerthExtra.Checked = true;
                                rdnNoSeat.Visible = rdnSeat.Visible = rdnSpecificSeat.Visible = rdnBerth.Visible = false;
                            }
                        }
                    }
                    else if (hdnReservationStatus.Value == "BerthsOptional")
                    {
                        if (Convert.ToBoolean(hdnIsSleeper.Value))
                        {
                            if (hdnAccommodationType.Value == "BerthAtExtraCost")
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = false;
                            }
                            else
                            {
                                rdnSeat.Checked = true;
                                rdnNoSeat.Visible = rdnBerth.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        rdnNoSeat.Checked = true;
                        rdnSeat.Visible = rdnSpecificSeat.Visible = rdnBerth.Visible = rdnBerthExtra.Visible = false;
                    }

                    if (Convert.ToBoolean(hdnIsSleeper.Value))
                        div_Berth_Traveller.Attributes.Add("style", "display:block;");

                    div_Reservation1.Attributes.Add("style", "display:block;");
                    div_Reservation2.Attributes.Add("style", "display:none;");
                }
                else
                {
                    div_Reservation1.Attributes.Add("style", "display:none;");
                    div_Reservation2.Attributes.Add("style", "display:block;");
                }

                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                if (objBRUC != null)
                {
                    hdnNoOfPassenger.Value = (objBRUC.Adults + objBRUC.Boys + objBRUC.Youths + objBRUC.Seniors).ToString();
                    if ((objBRUC.Adults + objBRUC.Boys + objBRUC.Youths + objBRUC.Seniors) > 1)
                        rdnSpecificSeat.Visible = false;
                }
            }
            catch (Exception ex) { ShowMessage(2, ex.Message); }
        }
    }

    void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            hdnCurrID.Value = currency;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            SeatReservationRequest request = new SeatReservationRequest();
            SeatReservationResponse response = new SeatReservationResponse();
            OneHubRailOneHubClient client = new OneHubRailOneHubClient();
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            List<TravellersInfo> travellerList = Session["TravellerList"] as List<TravellersInfo>;

            if (pInfoSolutionsResponse.EvolviPayloadAttributesInfo != null)
            {
                request = new SeatReservationRequest()
                {
                    EchoToken = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.EchoToken,
                    Version = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.Version,
                    TransactionIdentifier = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TransactionIdentifier,
                    TimeStamp = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TimeStamp,
                    SequenceNmbr = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.SequenceNmbr,
                    TargetType = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TargetType,
                    ISOCountry = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCountry,
                    ISOCurrency = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCurrency,

                    TravellerInfo = travellerList != null && travellerList.Count > 0 ? new TravellerInfo
                    {
                        FirstName = new string[] { txtFirstName.Text },
                        LastName = txtLastName.Text,
                        Title = new string[] { ddlTitle.SelectedItem.Text.Replace(".", string.Empty) },
                        TravellerType = new string[] { "Adult" },
                        NoOfPassenger = "1",
                        PhoneNo = !string.IsNullOrEmpty(txtPhoneNo.Text) ? txtPhoneNo.Text : ""
                    } : null,
                    EvJourneyFareInfo = GetSegmentInfoList().ToArray()
                };
                long orderID = Convert.ToInt64(Session["P2POrderID"]);
                _ManageOneHub.EmptyEvolviBasket(orderID, siteId, true);
                _ManageOneHub.ApiLogin(request, siteId);
                bool isMatch = false;

                if (Session["HoldReserVationRequest"] != null)
                    isMatch = IsReservationExist(request);

                Session["HoldReserVationRequest"] = request;
                if (isMatch)
                    response = Session["ReserveList"] as SeatReservationResponse;
                else
                    response = client.EvolviSeatReservation(request);

                if (response != null)
                {
                    if (response.ErrorMessage != null)
                    {
                        if (Session["BookingUCRerq"] != null)
                        {
                            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                            ShowMessage(2, response.ErrorMessage.MessageCode == "Could not reserve a seat/berth" ? "The system was unable to reserve your sleeper berth.Please try again.There is no availability for requested product on leg from " + objBRUC.FromDetail + " to " + objBRUC.ToDetail : response.ErrorMessage.MessageCode);
                        }
                        else
                            ShowMessage(2, response.ErrorMessage.MessageCode == "Could not reserve a seat/berth" ? "The system was unable to reserve your sleeper berth.Please try again." : response.ErrorMessage.MessageCode);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "selectRadio1", "callvalerror();callAllFunctions();", true);
                        return;
                    }
                    else
                    {
                        if (response.EvJourneySeatInfo != null)
                        {
                            if (Session["EvolviReservationRef"] != null)
                            {
                                EvReservationRefList = Session["EvolviReservationRef"] as List<EvolviReservationRef>;
                                List<EvolviReservationRef> EvReservationRefList2 = new List<EvolviReservationRef>();
                                foreach (var item in response.EvJourneySeatInfo.ToList())
                                {
                                    foreach (var newitem in EvReservationRefList.ToList())
                                    {
                                        if (newitem.TrainNo == item.JourneyIdentifier)
                                            EvReservationRefList2.Add(new EvolviReservationRef { P2PId = newitem.P2PId, TrainNo = newitem.TrainNo, ReservationCode = item.BookingRef });
                                    }
                                }
                                Session["EvolviReservationRef"] = EvReservationRefList2;
                                Session["holdTodStationList"] = null;
                            }
                        }
                        Session["ReserveList"] = response;
                        AddEvolviSleeperCharges();
                        Response.Redirect("SeatAllocation.aspx?req=EV", true);
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "selectRadio2", "callvalerror();callAllFunctions();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "selectRadio3", "callvalerror();callAllFunctions();", true);
        }
    }

    protected void btnChange_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("TrainResults.aspx?req=EV", true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public List<EvJourneyFareInfo> GetSegmentInfoList()
    {
        try
        {
            List<EvJourneyFareInfo> EvJourneyFareInfoList = new List<EvJourneyFareInfo>();
            List<ReservationInfo> ReservationList = Session["ReservationInfoList"] as List<ReservationInfo>;
            List<EvTrainSegmentInfo> OutEvTrainSegmentInfo = new List<EvTrainSegmentInfo>();
            List<EvTrainSegmentInfo> inEvTrainSegmentInfo = new List<EvTrainSegmentInfo>();

            List<SeatPreferenceInfo> SeatPreferenceInfoList = new List<SeatPreferenceInfo>();
            for (int i = 0; i < chkAdditionalInfo.Items.Count; i++)
            {
                if (chkAdditionalInfo.Items[i].Selected)
                    SeatPreferenceInfoList.Add(new SeatPreferenceInfo { SeatOptionCodes = (SeatOptionCode)Enum.Parse(typeof(SeatOptionCode), chkAdditionalInfo.Items[i].Value, true) });
            }
            if (ReservationList != null && ReservationList.Count > 0)
            {
                #region OutBound
                for (int i = 0; i < rptOutBound.Items.Count; i++)
                {
                    HiddenField hdnLegId = rptOutBound.Items[i].FindControl("hdnLegId") as HiddenField;
                    HiddenField hdnReservationStatus = rptOutBound.Items[i].FindControl("hdnReservationStatus") as HiddenField;
                    HiddenField hdnDeptDateTime = rptOutBound.Items[i].FindControl("hdnDeptDateTime") as HiddenField;
                    HiddenField hdnIsSleeper = rptOutBound.Items[i].FindControl("hdnIsSleeper") as HiddenField;
                    RadioButton rdnNoSeat = rptOutBound.Items[i].FindControl("rdnNoSeat") as RadioButton;
                    RadioButton rdnSeat = rptOutBound.Items[i].FindControl("rdnSeat") as RadioButton;
                    RadioButton rdnSpecificSeat = rptOutBound.Items[i].FindControl("rdnSpecificSeat") as RadioButton;

                    DropDownList ddlDirection = rptOutBound.Items[i].FindControl("ddlDirection") as DropDownList;
                    DropDownList ddlLocation = rptOutBound.Items[i].FindControl("ddlLocation") as DropDownList;
                    DropDownList ddlPosition = rptOutBound.Items[i].FindControl("ddlPosition") as DropDownList;
                    DropDownList ddlCoach = rptOutBound.Items[i].FindControl("ddlCoach") as DropDownList;
                    DropDownList ddlSeat = rptOutBound.Items[i].FindControl("ddlSeat") as DropDownList;
                    DropDownList ddlDirections = rptOutBound.Items[i].FindControl("ddlDirections") as DropDownList;

                    if (!string.IsNullOrEmpty(hdnReservationStatus.Value))
                    {
                        SeatDirectionType rDirection = new SeatDirectionType();
                        SeatType rPosition = new SeatType();
                        SeatLocationType rLocation = new SeatLocationType();
                        EvolviSeatType rSeatType = new EvolviSeatType();
                        if (rdnNoSeat.Checked)
                        {
                            rDirection = SeatDirectionType.Any;
                            rPosition = SeatType.Any;
                            rLocation = SeatLocationType.Any;
                            rSeatType = EvolviSeatType.NoSeat;
                        }
                        else if (rdnSeat.Checked)
                        {
                            rDirection = (SeatDirectionType)Enum.Parse(typeof(SeatDirectionType), ddlDirection.SelectedValue, true);
                            rPosition = (SeatType)Enum.Parse(typeof(SeatType), ddlPosition.SelectedValue, true);
                            rLocation = (SeatLocationType)Enum.Parse(typeof(SeatLocationType), ddlLocation.SelectedValue, true);
                            rSeatType = EvolviSeatType.Seat;
                        }
                        else if (rdnSpecificSeat.Checked)
                        {
                            rDirection = SeatDirectionType.Unspecified;
                            rSeatType = EvolviSeatType.SpecificSeat;
                        }
                        else
                            isBerthOutBound = true;

                        OutEvTrainSegmentInfo.Add(new EvTrainSegmentInfo
                        {
                            LegId = hdnLegId.Value.Trim(),
                            DepartureDateTime = DateTime.ParseExact(hdnDeptDateTime.Value, "dd-MM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture),
                            Direction = rDirection,
                            Position = rPosition,
                            Location = rLocation,
                            Coach = rdnSpecificSeat.Checked ? ddlCoach.SelectedValue : "",
                            Number = rdnSpecificSeat.Checked ? ddlSeat.SelectedValue : "",
                            Class = rdnSpecificSeat.Checked ? ddlDirections.SelectedValue : "",
                            SeatType = rSeatType,
                            SeatPreferenceInfo = SeatPreferenceInfoList.Count > 0 ? SeatPreferenceInfoList.ToArray() : null,
                            EvSleeperRequirementCollection = Convert.ToBoolean(hdnIsSleeper.Value) && isBerthOutBound ? EvSleeperRequirementCollectionList(rptOutBerth).ToArray() : null,
                            IsSleeper = Convert.ToBoolean(hdnIsSleeper.Value) ? true : false,
                            IsReturn = false,
                            IsSleeperReservation = isBerthOutBound
                        });
                    }
                }
                #endregion

                #region InBound
                for (int i = 0; i < rptInBound.Items.Count; i++)
                {
                    HiddenField hdnLegId = rptInBound.Items[i].FindControl("hdnLegId") as HiddenField;
                    HiddenField hdnReservationStatus = rptInBound.Items[i].FindControl("hdnReservationStatus") as HiddenField;
                    HiddenField hdnDeptDateTime = rptInBound.Items[i].FindControl("hdnDeptDateTime") as HiddenField;
                    HiddenField hdnIsSleeper = rptInBound.Items[i].FindControl("hdnIsSleeper") as HiddenField;
                    RadioButton rdnNoSeat = rptInBound.Items[i].FindControl("rdnNoSeat") as RadioButton;
                    RadioButton rdnSeat = rptInBound.Items[i].FindControl("rdnSeat") as RadioButton;
                    RadioButton rdnSpecificSeat = rptInBound.Items[i].FindControl("rdnSpecificSeat") as RadioButton;

                    DropDownList ddlDirection = rptInBound.Items[i].FindControl("ddlDirection") as DropDownList;
                    DropDownList ddlLocation = rptInBound.Items[i].FindControl("ddlLocation") as DropDownList;
                    DropDownList ddlPosition = rptInBound.Items[i].FindControl("ddlPosition") as DropDownList;
                    DropDownList ddlCoach = rptInBound.Items[i].FindControl("ddlCoach") as DropDownList;
                    DropDownList ddlSeat = rptInBound.Items[i].FindControl("ddlSeat") as DropDownList;
                    DropDownList ddlDirections = rptInBound.Items[i].FindControl("ddlDirections") as DropDownList;

                    if (!string.IsNullOrEmpty(hdnReservationStatus.Value))
                    {
                        SeatDirectionType rDirection = new SeatDirectionType();
                        SeatType rPosition = new SeatType();
                        SeatLocationType rLocation = new SeatLocationType();
                        EvolviSeatType rSeatType = new EvolviSeatType();
                        if (rdnNoSeat.Checked)
                        {
                            rDirection = SeatDirectionType.Any;
                            rPosition = SeatType.Any;
                            rLocation = SeatLocationType.Any;
                            rSeatType = EvolviSeatType.NoSeat;
                        }
                        else if (rdnSeat.Checked)
                        {
                            rDirection = (SeatDirectionType)Enum.Parse(typeof(SeatDirectionType), ddlDirection.SelectedValue, true);
                            rPosition = (SeatType)Enum.Parse(typeof(SeatType), ddlPosition.SelectedValue, true);
                            rLocation = (SeatLocationType)Enum.Parse(typeof(SeatLocationType), ddlLocation.SelectedValue, true);
                            rSeatType = EvolviSeatType.Seat;
                        }
                        else if (rdnSpecificSeat.Checked)
                        {
                            rDirection = SeatDirectionType.Unspecified;
                            rSeatType = EvolviSeatType.SpecificSeat;
                        }
                        else
                            isBerthInBound = true;

                        inEvTrainSegmentInfo.Add(new EvTrainSegmentInfo
                        {
                            LegId = hdnLegId.Value.Trim(),
                            DepartureDateTime = DateTime.ParseExact(hdnDeptDateTime.Value, "dd-MM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture),
                            Direction = rDirection,
                            Position = rPosition,
                            Location = rLocation,
                            Coach = rdnSpecificSeat.Checked ? ddlCoach.SelectedValue : "",
                            Number = rdnSpecificSeat.Checked ? ddlSeat.SelectedValue : "",
                            Class = rdnSpecificSeat.Checked ? ddlDirections.SelectedValue : "",
                            SeatType = rSeatType,
                            SeatPreferenceInfo = SeatPreferenceInfoList.Count > 0 ? SeatPreferenceInfoList.ToArray() : null,
                            EvSleeperRequirementCollection = Convert.ToBoolean(hdnIsSleeper.Value) && isBerthInBound ? EvSleeperRequirementCollectionList(rptInBerth).ToArray() : null,
                            IsSleeper = Convert.ToBoolean(hdnIsSleeper.Value) ? true : false,
                            IsReturn = true,
                            IsSleeperReservation = isBerthInBound
                        });
                    }
                }
                #endregion

                EvJourneyFareInfoList.Add(new EvJourneyFareInfo
                {
                    FareId = ReservationList.FirstOrDefault(x => x.JourneyType == "Outbound") != null ? ReservationList.FirstOrDefault(x => x.JourneyType == "Outbound").FareId : "",
                    JourneyIdentifier = ReservationList.FirstOrDefault(x => x.JourneyType == "Outbound") != null ? ReservationList.FirstOrDefault(x => x.JourneyType == "Outbound").JourneyIdentifier : "",
                    EvTrainSegmentInfo = OutEvTrainSegmentInfo.ToArray()
                });

                if (rptInBound.Items.Count > 0)
                {
                    EvJourneyFareInfoList.Add(new EvJourneyFareInfo
                    {
                        FareId = ReservationList.FirstOrDefault(x => x.JourneyType == "Inbound") != null ? ReservationList.FirstOrDefault(x => x.JourneyType == "Inbound").FareId : "",
                        JourneyIdentifier = ReservationList.FirstOrDefault(x => x.JourneyType == "Inbound") != null ? ReservationList.FirstOrDefault(x => x.JourneyType == "Inbound").JourneyIdentifier : "",
                        EvTrainSegmentInfo = inEvTrainSegmentInfo.ToArray()
                    });
                }
            }
            Session["ReserveSeatType"] = EvJourneyFareInfoList;
            return EvJourneyFareInfoList;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return null; }
    }

    public void LoadTraveller()
    {
        try
        {
            if (Session["TravellerList"] != null)
            {
                List<TravellersInfo> travellerList = Session["TravellerList"] as List<TravellersInfo>;
                if (travellerList != null && travellerList.Count > 0)
                {
                    var leadTraveller = travellerList.FirstOrDefault(x => x.Lead == true);
                    if (leadTraveller != null)
                    {
                        ddlTitle.SelectedValue = leadTraveller.TitleValue;
                        txtFirstName.Text = leadTraveller.FName;
                        txtLastName.Text = leadTraveller.LName;
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillBerthByTraveller(bool isOutbound, bool isSolo, bool isMaleFemale, bool isTogr, string adultPrice, string childPrice, string adultOrChildPrice, string twoChildPrice, string soloAdultOrChildPrice, string OneBerthAdultMF, string ReservationStatus)
    {
        try
        {
            if (Session["BookingUCRerq"] != null && Session["TrainSearch"] != null)
            {
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                int noOfAdult = objBRUC.Adults + objBRUC.Youths + objBRUC.Seniors;
                int noOfChild = objBRUC.Boys;
                decimal total = noOfAdult + noOfChild;

                List<BerthPassenger> BerthPassengerList = new List<BerthPassenger>();
                var srcCurId = FrontEndManagePass.GetCurrencyID("GBP");
                for (int i = 1; i <= total; i++)
                {
                    BerthPassenger objBerthPassenger = new BerthPassenger();
                    objBerthPassenger.isSolo = isSolo;
                    objBerthPassenger.isMaleFemale = isMaleFemale;
                    objBerthPassenger.isTogr = isTogr;
                    objBerthPassenger.NoOfAdult = noOfAdult;
                    objBerthPassenger.NoOfChild = noOfChild;

                    objBerthPassenger.AdultPrice = !string.IsNullOrEmpty(adultPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToInt32(adultPrice) > 0 ? (decimal)(Convert.ToInt32(adultPrice)) / 100 : 0, siteId, srcCurId, currencyID).ToString("F2") : "0";
                    objBerthPassenger.ChildPrice = !string.IsNullOrEmpty(childPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToInt32(childPrice) > 0 ? (decimal)(Convert.ToInt32(childPrice)) / 100 : 0, siteId, srcCurId, currencyID).ToString("F2") : "0";
                    objBerthPassenger.AdultOrChildSharingPrice = !string.IsNullOrEmpty(adultOrChildPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToInt32(adultOrChildPrice) > 0 ? (decimal)(Convert.ToInt32(adultOrChildPrice)) / 100 : 0, siteId, srcCurId, currencyID).ToString("F2") : "0";
                    objBerthPassenger.TwoChildSharingPrice = !string.IsNullOrEmpty(twoChildPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToInt32(twoChildPrice) > 0 ? (decimal)(Convert.ToInt32(twoChildPrice)) / 100 : 0, siteId, srcCurId, currencyID).ToString("F2") : "0";

                    objBerthPassenger.soloAdultOrChildPrice = !string.IsNullOrEmpty(soloAdultOrChildPrice) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToInt32(soloAdultOrChildPrice) > 0 ? (decimal)(Convert.ToInt32(soloAdultOrChildPrice)) / 100 : 0, siteId, srcCurId, currencyID).ToString("F2") : "0";
                    objBerthPassenger.OneBerthAdultMF = !string.IsNullOrEmpty(OneBerthAdultMF) ? FrontEndManagePass.GetPriceAfterConversion(Convert.ToInt32(OneBerthAdultMF) > 0 ? (decimal)(Convert.ToInt32(OneBerthAdultMF)) / 100 : 0, siteId, srcCurId, currencyID).ToString("F2") : "0";

                    objBerthPassenger.isfirst = i > 1;
                    objBerthPassenger.no = i;
                    BerthPassengerList.Add(objBerthPassenger);
                }
                if (isOutbound)
                {
                    rptOutBerth.DataSource = BerthPassengerList;
                    rptOutBerth.DataBind();
                    div_Out_Berth.Attributes.Add("style", "display:block;");
                }
                else
                {
                    rptInBerth.DataSource = BerthPassengerList;
                    rptInBerth.DataBind();
                    div_In_Berth.Attributes.Add("style", "display:block;");
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public List<EvSleeperRequirementCollection> EvSleeperRequirementCollectionList(Repeater rpt)
    {
        try
        {
            List<EvSleeperRequirementCollection> EvSleeperRequirementCollectionList = new List<EvSleeperRequirementCollection>();
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                var ddlNoOfBerth = rpt.Items[i].FindControl("ddlNoOfBerth") as DropDownList;
                var ddlBerth1 = rpt.Items[i].FindControl("ddlBerth1") as DropDownList;
                var ddlBerth2 = rpt.Items[i].FindControl("ddlBerth2") as DropDownList;
                var ddlCabinLocation = rpt.Items[i].FindControl("ddlCabinLocation") as DropDownList;
                var ddlPreferences = rpt.Items[i].FindControl("ddlPreferences") as DropDownList;
                var hdnBerthCost = rpt.Items[i].FindControl("hdnBerthCost") as HiddenField;
                var hdnisDelete = rpt.Items[i].FindControl("hdnisDelete") as HiddenField;

                if (hdnisDelete.Value == "no" && ddlNoOfBerth.SelectedValue != "Select ...")
                {
                    string Berth1 = ddlBerth1.SelectedValue == "Select ..." ? "Not Available" : ddlBerth1.SelectedValue;
                    string Berth2 = ddlBerth2.SelectedValue == "Select ..." ? "Not Available" : ddlBerth2.SelectedValue;
                    EvSleeperRequirementCollectionList.Add(new EvSleeperRequirementCollection
                    {
                        CabinType = (CabinTypeEnum)Enum.Parse(typeof(CabinTypeEnum), GetNoOfBirth(ddlNoOfBerth.SelectedValue), true),
                        CabinLocation = (CabinLocationEnum)Enum.Parse(typeof(CabinLocationEnum), GetCabinLocation(ddlCabinLocation.SelectedValue), true),
                        //ConnectingDoor = (ConnectingDoorEnum)Enum.Parse(typeof(ConnectingDoorEnum), ddlNoOfBerth.SelectedValue, true),
                        BerthOne = new EvBerthPreference
                        {
                            Occupancy = (BerthOccupancyEnum)Enum.Parse(typeof(BerthOccupancyEnum), GetBerth1(Berth1), true),
                            //BerthLocation = (BerthOccupancyEnum)Enum.Parse(typeof(BerthOccupancyEnum), ddlBerth1.SelectedValue, true),
                            ReservationCost = new EvCurrencyAmount
                            {
                                CurrencyCode = "GBP",
                                TaxRateType = TaxRateTypeEnum.ZeroRate
                            }
                        },
                        BerthTwo = new EvBerthPreference
                        {
                            Occupancy = (BerthOccupancyEnum)Enum.Parse(typeof(BerthOccupancyEnum), GetBerth2(Berth2), true),
                            ReservationCost = new EvCurrencyAmount
                            {
                                CurrencyCode = "GBP",
                                TaxRateType = TaxRateTypeEnum.ZeroRate
                            }
                        }
                    });
                }
            }
            return EvSleeperRequirementCollectionList;
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); return null; }
    }

    public string GetNoOfBirth(string NoOfBirths)
    {
        switch (NoOfBirths)
        {
            case "Solo occupancy cabin":
                return "SoloBerth";
            case "One berth in two berth cabin":
                return "OneBerth";
            case "Two berths in two berth cabin":
                return "TwoBerths";
        }
        return "";
    }

    public string GetBerth1(string Birth1)
    {
        switch (Birth1)
        {
            case "Adult":
                return "OneAdult";
            case "Child":
                return "OneChild";
            case "Male Adult":
                return "MaleAdult";
            case "Female Adult":
                return "FemaleAdult";
            case "Adult and child sharing berth":
                return "OneAdultOneChild";
            case "Two children sharing berth":
                return "TwoChildren";
        }
        return "";
    }

    public string GetBerth2(string Birth2)
    {
        switch (Birth2)
        {
            case "Not Available":
                return "NotSelected";
            case "Adult":
                return "OneAdult";
            case "Child":
                return "OneChild";
            case "Male Adult":
                return "MaleAdult";
            case "Female Adult":
                return "FemaleAdult";
            case "Adult and child sharing berth":
                return "OneAdultOneChild";
            case "Two children sharing berth":
                return "TwoChildren";
        }
        return "";
    }

    public string GetCabinLocation(string CabinLocation)
    {
        switch (CabinLocation)
        {
            case "No Preference":
                return "No_Preference";
            case "Centre of Carriage":
                return "Centre_of_Carriage";
            case "End of Carriage":
                return "End_of_Carriage";
        }
        return "";
    }

    public string GetPreferences(string Preferences)
    {
        switch (Preferences)
        {
            case "No Preference":
                return "No_Preference";
            case "Connecting Door":
                return "Connecting_Door";
            case "Upper Berth":
                return "Upper_Berth";
            case "Lower Berth":
                return "Lower_Berth";
        }
        return "";
    }

    public void AddEvolviSleeperCharges()
    {
        try
        {
            decimal SleeperOutCharge = 0;
            decimal SleeperInCharge = 0;
            for (int i = 0; i < rptOutBerth.Items.Count; i++)
            {
                var hdnBerthCost = rptOutBerth.Items[i].FindControl("hdnBerthCost") as HiddenField;
                SleeperOutCharge += Convert.ToDecimal(hdnBerthCost.Value);
            }
            for (int i = 0; i < rptInBerth.Items.Count; i++)
            {
                var hdnBerthCost = rptInBerth.Items[i].FindControl("hdnBerthCost") as HiddenField;
                SleeperInCharge += Convert.ToDecimal(hdnBerthCost.Value);
            }
            if (isBerthOutBound == false)
                SleeperOutCharge = 0;
            if (isBerthInBound == false)
                SleeperInCharge = 0;

            long P2POrderId = Convert.ToInt64(Session["P2POrderID"]);
            _masterBooking.UpdateSleeperPrice(P2POrderId, SleeperOutCharge, SleeperInCharge);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public bool IsReservationExist(SeatReservationRequest request)
    {
        var req = Session["HoldReserVationRequest"] as SeatReservationRequest;
        bool isMatch = true;
        if (req != null && request != null)
        {
            if (req.EvJourneyFareInfo != null && request.EvJourneyFareInfo != null && req.EvJourneyFareInfo.Length == request.EvJourneyFareInfo.Length)
            {
                for (int i = 0; i < req.EvJourneyFareInfo.Length; i++)
                {
                    for (int j = 0; j < request.EvJourneyFareInfo.Length; j++)
                    {
                        if (i == j)
                        {
                            #region
                            var data1 = req.EvJourneyFareInfo[i];
                            var data2 = request.EvJourneyFareInfo[j];
                            if (data1.JourneyIdentifier == data2.JourneyIdentifier && data1.FareId == data2.FareId)
                            {
                                if (data1.EvTrainSegmentInfo != null && data2.EvTrainSegmentInfo != null)
                                {
                                    if (data1.EvTrainSegmentInfo.Length == data2.EvTrainSegmentInfo.Length)
                                    {
                                        for (int m = 0; m < data1.EvTrainSegmentInfo.Length; m++)
                                        {
                                            for (int n = 0; n < data2.EvTrainSegmentInfo.Length; n++)
                                            {
                                                if (m == n)
                                                {

                                                    #region
                                                    var data3 = data1.EvTrainSegmentInfo[m];
                                                    var data4 = data2.EvTrainSegmentInfo[n];
                                                    if (data3.LegId == data4.LegId && data3.IsReturn == data4.IsReturn && data3.IsSleeper == data4.IsSleeper && data3.IsSleeperReservation == data4.IsSleeperReservation && data3.Direction == data4.Direction && data3.Position == data4.Position && data3.Location == data4.Location && data3.SeatType == data4.SeatType && data3.DepartureDateTime == data4.DepartureDateTime)
                                                    {
                                                        if (data3.EvSleeperRequirementCollection != null && data4.EvSleeperRequirementCollection != null)
                                                        {
                                                            if (data3.EvSleeperRequirementCollection.Length == data4.EvSleeperRequirementCollection.Length)
                                                            {
                                                                for (int x = 0; x < data3.EvSleeperRequirementCollection.Length; x++)
                                                                {
                                                                    for (int y = 0; y < data4.EvSleeperRequirementCollection.Length; y++)
                                                                    {
                                                                        if (x == y)
                                                                        {

                                                                            #region
                                                                            var data5 = data3.EvSleeperRequirementCollection[x];
                                                                            var data6 = data4.EvSleeperRequirementCollection[y];
                                                                            if (data5.CabinType == data6.CabinType && data5.CabinLocation == data6.CabinLocation && data5.ConnectingDoor == data6.ConnectingDoor)
                                                                            {
                                                                                #region
                                                                                if (data5.BerthOne != null && data6.BerthOne != null)
                                                                                {
                                                                                    if (data5.BerthOne.Occupancy == data6.BerthOne.Occupancy && data5.BerthOne.BerthLocation == data6.BerthOne.BerthLocation)
                                                                                    {
                                                                                        if (data5.BerthOne.ReservationCost != null && data6.BerthOne.ReservationCost != null)
                                                                                        {
                                                                                            if (data5.BerthOne.ReservationCost.CurrencyCode == data6.BerthOne.ReservationCost.CurrencyCode && data5.BerthOne.ReservationCost.TaxRateType == data6.BerthOne.ReservationCost.TaxRateType)
                                                                                                isMatch = true;
                                                                                            else
                                                                                                return false;
                                                                                        }
                                                                                        else if (data5.BerthOne.ReservationCost == null && data6.BerthOne.ReservationCost == null)
                                                                                            isMatch = true;
                                                                                        else
                                                                                            return false;
                                                                                    }
                                                                                    else
                                                                                        return false;
                                                                                }
                                                                                else if (data5.BerthOne == null && data6.BerthOne == null)
                                                                                    isMatch = true;
                                                                                else
                                                                                    return false;
                                                                                #endregion

                                                                                #region
                                                                                if (data5.BerthTwo != null && data6.BerthTwo != null)
                                                                                {
                                                                                    if (data5.BerthTwo.Occupancy == data6.BerthTwo.Occupancy && data5.BerthTwo.BerthLocation == data6.BerthTwo.BerthLocation)
                                                                                    {
                                                                                        if (data5.BerthTwo.ReservationCost != null && data6.BerthTwo.ReservationCost != null)
                                                                                        {
                                                                                            if (data5.BerthTwo.ReservationCost.CurrencyCode == data6.BerthTwo.ReservationCost.CurrencyCode && data5.BerthTwo.ReservationCost.TaxRateType == data6.BerthTwo.ReservationCost.TaxRateType)
                                                                                                isMatch = true;
                                                                                            else
                                                                                                return false;
                                                                                        }
                                                                                        else if (data5.BerthTwo.ReservationCost == null && data6.BerthTwo.ReservationCost == null)
                                                                                            isMatch = true;
                                                                                        else
                                                                                            return false;
                                                                                    }
                                                                                    else
                                                                                        return false;
                                                                                }
                                                                                else if (data5.BerthTwo == null && data6.BerthTwo == null)
                                                                                    isMatch = true;
                                                                                else
                                                                                    return false;
                                                                                #endregion
                                                                            }
                                                                            else
                                                                                return false;
                                                                            #endregion


                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                                return false;
                                                        }
                                                        else if (data3.EvSleeperRequirementCollection == null && data4.EvSleeperRequirementCollection == null)
                                                            isMatch = true;
                                                        else
                                                            return false;
                                                    }
                                                    else
                                                        return false;

                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                    else
                                        return false;
                                }
                                else
                                    return false;
                            }
                            else
                                return false;
                            #endregion
                        }
                    }
                }
            }
            else
                return false;

            if (req.TravellerInfo != null && request.TravellerInfo != null)
            {
                var data1 = req.TravellerInfo;
                var data2 = request.TravellerInfo;
                if (data1.FirstName.FirstOrDefault() == data2.FirstName.FirstOrDefault() && data1.LastName == data2.LastName && data1.Title.FirstOrDefault() == data2.Title.FirstOrDefault() && data1.TravellerType.FirstOrDefault() == data2.TravellerType.FirstOrDefault() && data1.NoOfPassenger == data2.NoOfPassenger && data1.PhoneNo == data2.PhoneNo)
                    isMatch = true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;

        return isMatch;
    }
}

public class BerthPassenger
{
    public int no { get; set; }
    public bool isfirst { get; set; }
    public int NoOfAdult { get; set; }
    public int NoOfChild { get; set; }
    public string AdultPrice { get; set; }
    public string ChildPrice { get; set; }
    public string AdultOrChildSharingPrice { get; set; }
    public string TwoChildSharingPrice { get; set; }
    public string soloAdultOrChildPrice { get; set; }
    public string OneBerthAdultMF { get; set; }
    public bool isSolo { get; set; }
    public bool isMaleFemale { get; set; }
    public bool isTogr { get; set; }
}

public class HoldOldJourneyResponse
{
    public JourneyDetailsResponse Res { get; set; }
    public bool isOutBound { get; set; }
}
