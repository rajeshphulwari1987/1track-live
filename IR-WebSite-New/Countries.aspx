﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Countries.aspx.cs" Inherits="Countries" ValidateRequest="false" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="Scripts/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () { setMenuActive("C"); });
        function SectionHide(objId) {
            var id = objId.toString();
            $("#" + id).attr("style", "display:none;");
        }
    </script>
    <style type="text/css">
        .btnhover:hover
        {
            color: #fb4f14;
        }
        #MainContent_countryMessage
        {
            background-color: rgb(255, 240, 240);
            font-size: 15px;
            background-position: 10px center;
            background-repeat: no-repeat;
            border: 1px solid red;
            margin: 10px 0;
            padding: 15px 10px 15px 10px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <h1 class="starail-u-alpha starail-Country-pageTitle">
                Explore More Countries With Us</h1>
            <div id="countryMessage" runat="server">
                <asp:Label ID="lblcountryMessage" runat="server" Text="Currently no passes available for your chosen country. Please try another destination." ForeColor="Red" />
            </div>
       <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
            <asp:Repeater ID="rptContinent" runat="server" OnItemDataBound="rptContinent_ItemDataBound"
                OnItemCommand="rptContinent_ItemCommand">
                <ItemTemplate>
                    <section class="starail-Country starail-u-cf" id='<%#Eval("ContinentID") %>'>
                        <header class="starail-Country-header">
                            <h2 class="starail-Country-title"><asp:Button ID="lnkContinent" runat="server" CommandName="Continent" CommandArgument='<%#Eval("ContinentName") %>' Text='<%#Eval("ContinentName") %>' style="border: none;  background: transparent;"/></h2>
                            <asp:HiddenField ID="hdfContinentId" runat="server" Value='<%#Eval("ContinentID") %>' />
                        </header>
                        <ul class="starail-Country-list starail-u-cf">
                        <asp:Repeater ID="rptCountry" runat="server"    onitemcommand="rptCountry_ItemCommand">
                              <ItemTemplate>
                                        <li class="starail-Country-listItem"> 
                                         <asp:Button ID="LinkButton1" runat="server" CssClass="btnhover" style="border: none;  background: transparent;" CommandName="COUNTRY" Text='  <%#Eval("CountryName").ToString().Length > 20 ? Eval("CountryName").ToString().Substring(0, 20) + ".." : Eval("CountryName").ToString()%>' CommandArgument=<%# String.Format("{0}~{1}", Eval("CountryName"), Eval("CountryCode")) %> />
                                      
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("CountryID") %>' />
                                        </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                    </section>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
