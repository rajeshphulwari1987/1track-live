﻿/* hoverIntent v1.8.0 // 2014.06.29 // jQuery v1.9.1+ http://cherne.net/brian/resources/jquery.hoverIntent.html You may use hoverIntent under the terms of the MIT license. Basically that means you are free to use hoverIntent as long as this header is left intact. Copyright 2007, 2014 Brian Cherne */
(function ($) { $.fn.hoverIntent = function (handlerIn, handlerOut, selector) { var cfg = { interval: 100, sensitivity: 6, timeout: 0 }; if (typeof handlerIn === "object") { cfg = $.extend(cfg, handlerIn) } else { if ($.isFunction(handlerOut)) { cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector }) } else { cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut }) } } var cX, cY, pX, pY; var track = function (ev) { cX = ev.pageX; cY = ev.pageY }; var compare = function (ev, ob) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); if (Math.sqrt((pX - cX) * (pX - cX) + (pY - cY) * (pY - cY)) < cfg.sensitivity) { $(ob).off("mousemove.hoverIntent", track); ob.hoverIntent_s = true; return cfg.over.apply(ob, [ev]) } else { pX = cX; pY = cY; ob.hoverIntent_t = setTimeout(function () { compare(ev, ob) }, cfg.interval) } }; var delay = function (ev, ob) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); ob.hoverIntent_s = false; return cfg.out.apply(ob, [ev]) }; var handleHover = function (e) { var ev = $.extend({}, e); var ob = this; if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t) } if (e.type === "mouseenter") { pX = ev.pageX; pY = ev.pageY; $(ob).on("mousemove.hoverIntent", track); if (!ob.hoverIntent_s) { ob.hoverIntent_t = setTimeout(function () { compare(ev, ob) }, cfg.interval) } } else { $(ob).off("mousemove.hoverIntent", track); if (ob.hoverIntent_s) { ob.hoverIntent_t = setTimeout(function () { delay(ev, ob) }, cfg.timeout) } } }; return this.on({ "mouseenter.hoverIntent": handleHover, "mouseleave.hoverIntent": handleHover }, cfg.selector) } })(jQuery);
var staHeaderFooter = function () {
    ; (function ($, window, document, undefined) {
        $.fn.doubleTapToGo = function (params) {
            if (!('ontouchstart' in window) &&
            !navigator.msMaxTouchPoints &&
            !navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) return false;
            this.each(function () {
                var curItem = false;
                $(this).on('click', function (e) {
                    var item = $(this);
                    if (item[0] != curItem[0]) {
                        e.preventDefault();
                        curItem = item;
                    }
                });
                $(document).on('click touchstart MSPointerDown', function (e) {
                    var resetItem = true,
                    parents = $(e.target).parents();
                    for (var i = 0; i < parents.length; i++)
                        if (parents[i] == curItem[0])
                            resetItem = false;
                    if (resetItem)
                        curItem = false;
                });
            });
            return this;
        };
    })(jQuery, window, document);
    staHeaderFooter.newsletterWidget = $("#sta-newsletter-widget");
    staHeaderFooter.searchMobileVisible = false;
    staHeaderFooter.visits = 0;
    staHeaderFooter.pageResponsive = true;
    $(document).ready(function () {
        headerPageLoad();
        footerPageLoad();
    });
    function headerPageLoad() {
        /* Hover Intent: deactivate default nav hover css */
        $("#sta-nav-top>ul>div>li").css({ 'background-color': '#eee' });
        $("#sta-nav-top ul.sta-nav-top-sub-menu").css({ 'display': 'none' });
        $("header #sta-nav>li").css({ 'background-color': '#ffffff' });
        $("header #sta-nav>li>ul.sta-mainNavSubMenu").css({ 'display': 'none' });
        /* Configure hover intent and activate */
        var hoverIntentConfigTop = { sensitivity: 7, interval: 35, timeout: 150, over: openTopnav, out: closeTopnav };
        var hoverIntentConfig = { sensitivity: 7, interval: 100, timeout: 200, over: openMeganav, out: closeMeganav };
        $("#sta-top-header #sta-nav-top>ul>div>li").hoverIntent(hoverIntentConfigTop);
        $("header #sta-nav>li").hoverIntent(hoverIntentConfig);
        if (typeof staHeaderFooter.newsletterWidget === "object") {
            if (staHeaderFooter.newsletterWidget.val() == "yes") {
                staHeaderFooter.visits = staHeaderFooter.common.getCookie("staVisits");
                if (staHeaderFooter.visits == null) {
                    staHeaderFooter.visits = 0;
                }
                staHeaderFooter.header.newsletterWidget();
            }
        }
        $("#sta-nav-top .sta-flag-dropdown").doubleTapToGo();
        $("#sta-nav-top .sta-contact-dropdown").doubleTapToGo();
        $("#sta-nav-top .sta-account").doubleTapToGo();
        $("ul#sta-nav>li:has(ul)").doubleTapToGo();
        staHeaderFooter.header.navlazy();
        staHeaderFooter.header.main();
        staHeaderFooter.header.windowResize();
        $(window).resize(function () {
            staHeaderFooter.header.windowResize();
        });
    }
    function footerPageLoad() {
        $("#sta-footer-subscribe-btn").click(function () {
            staHeaderFooter.header.newsletterSignUp('sta-footer-email', 'sta-footer-subscribe-btn', 'Subscribe to Newsletter');
            return false;
        });
        staHeaderFooter.footer.windowResize();
        $(window).resize(function () {
            staHeaderFooter.footer.windowResize();
        });
    }
};
/* Hover Delay code for Meganav and the Top Header Nav */
$(document).on('touchstart', function (ev) {
    ev.stopPropagation();
    var containerTopnav = $("#sta-top-header #sta-nav-top");
    var containerMeganav = $("header #sta-nav");
    if ((!containerTopnav.is(ev.target) && containerTopnav.has(ev.target).length === 0) && (!containerMeganav.is(ev.target) && containerMeganav.has(ev.target).length === 0)) {
        $("#sta-top-header #sta-nav-top>ul>div>li").css({ 'background-color': '#eee', 'color': '#333' });
        $("#sta-top-header #sta-nav-top>ul>div>li").children('ul').css({ 'display': 'none' });
        $("header #sta-nav>li").css({ 'background-color': '#fff' });
        $('header #sta-nav>li>ul.sta-mainNavSubMenu').css({ 'display': 'none' })
    }
});
function openMeganav() {
    $(this).css({ 'background-color': '#eee', 'color': '#333' });
    $(this).children('ul').css({ 'left': '-1px', 'display': 'block' });
}
function closeMeganav() {
    $(this).css({ 'background-color': '#fff', 'color': '#333' });
    $(this).children('ul').css({ 'display': 'none' });
}
function openTopnav() {
    $(this).css({ 'background-color': '#fff', 'color': '#333' });
    if ($(this).hasClass("sta-search-dropdown")) {          // if search box nav
        $(this).children('ul').css({ 'display': 'block', 'left': '-322px' });
    } else if ($(this).hasClass("sta-flag-dropdown")) {     // if country flag nav
        $(this).children('ul').css({ 'display': 'block', 'left': '-164px' });
    } else if ($(this).hasClass("sta-account")) {           // if account nav
        $(this).children('ul').css({ 'display': 'block', 'left': '-1px' });
    } else if ($(this).hasClass("sta-contact-dropdown")) {  // if contact us 
        $(this).children('ul').css({ 'display': 'block', 'left': '-1px' });
    }
}
function closeTopnav() {
    $(this).css({ 'background-color': '#eee', 'color': '#333' });
    $(this).children('ul').css({ 'display': 'none' });
}
staHeaderFooter();
/* staHeaderFooter Common */
staHeaderFooter.common = {
    getCookie: function (cookieName) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + cookieName + "=");
        if (c_start == -1) {
            c_start = c_value.indexOf(cookieName + "=");
        }
        if (c_start == -1) {
            c_value = null;
        }
        else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    },
    setCookie: function (cookieName, cookieValue, expireDays) {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + expireDays);
        var cookieValueS = escape(cookieValue) + ";path=/" + ((expireDays == null) ? "" : "; expires=" + expireDate.toUTCString());
        document.cookie = cookieName + "=" + cookieValueS;
    },
    getQueryValue: function (name, queryString) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        results = regex.exec(queryString);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    isValidEmailAddress: function (emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    },
    getPageResponsive: function () {
        staHeaderFooter.pageResponsive = $("#sta-page-responsive").val();
        if (staHeaderFooter.pageResponsive == null) {
            staHeaderFooter.pageResponsive = true;
        }
        else if (staHeaderFooter.pageResponsive == 'false') {
            staHeaderFooter.pageResponsive = false;
        }
        return staHeaderFooter.pageResponsive;
    }
};
/* staHeaderFooter footer code */
staHeaderFooter.footer = {
    windowResize: function () {
        staHeaderFooter.pageResponsive = staHeaderFooter.common.getPageResponsive();
        if ($(window).width() < 640 && staHeaderFooter.pageResponsive) {
            $("#sta-footer-panel1").append($("#sta-footer-newsletter"));
            $("#sta-footer-panel2").append($("#sta-footer-contact-us"), $("#sta-footer-specialist-travel"), $("#sta-footer-request-brochure"));
            $("#sta-footer-panel3").append($("#sta-footer-about-us"), $("#sta-footer-support"));
            staHeaderFooter.footer.newsletter("#sta-footer-newsletter");
            $('#sta-footer-newsletter h4').addClass('sta-btn-collapse-mini');
        } else {
            $("#sta-footer-panel2").append($("#sta-footer-contact-us"), $("#sta-footer-about-us"), $("#sta-footer-specialist-travel"), $("#sta-footer-support"), $("#sta-footer-request-brochure"));
            $("#sta-footer-panel3").append($("#sta-footer-newsletter"), $("#sta-footer-blog"));
            $('#sta-footer-newsletter h4').removeClass('sta-btn-collapse-mini');
        }
    },
    newsletter: function (activator) {
        $(activator).undelegate("h4", "click");
        $(activator).delegate("h4", "click", function () {
            if ($(this).next(".sta-mobile-extra").is(":visible")) {
                $(this).removeClass("sta-ready-to-collapse-mini")
            } else {
                $(this).addClass("sta-ready-to-collapse-mini")
            }
            $(this).next(".sta-mobile-extra").slideToggle()
        })
    }
};
/* staHeaderFooter header code */
staHeaderFooter.header = {
    windowResize: function () {
        staHeaderFooter.searchMobileVisible = $('#sta-search-form-copy').is(":visible");
        staHeaderFooter.header.toggleMobileSearch(staHeaderFooter.searchMobileVisible);
        staHeaderFooter.pageResponsive = staHeaderFooter.common.getPageResponsive();
        if ($(window).width() < 640 && staHeaderFooter.pageResponsive) {
            $("#sta-newsletter").attr('style', 'display:none !important');
            $("nav#sta-nav-wrap-copy ul.navigation").show();
            $("#sta-mobile-header").parent().addClass("mm-page");
        }
        else {
            $("#sta-search-form-copy").css("display", "none");
            $("#sta-mobile-header").parent().removeClass("mm-page");
            var isSubscribed = staHeaderFooter.common.getCookie("staNewsletterSubscribed");
            if (isSubscribed == null) {
                isSubscribed = "false";
            }
            if (isSubscribed === "true") {
                $("#sta-newsletter").attr('style', 'display:none !important');
            }
            else {
                if (staHeaderFooter.visits == 1 || staHeaderFooter.visits % 5 == 0) {
                    $("#sta-newsletter").attr('style', 'display:block !important');
                }
                else {
                    $("#sta-newsletter").attr('style', 'display:none !important');
                }
            }
        }
    },
    toggleMobileSearch: function (searchVisible) {
        if (!searchVisible) {
            $("#sta-mobile-header a.sta-search").addClass("sta-search-disable");
            $("#sta-mobile-header a.sta-search").removeClass("sta-search-enable");
            $("#sta-page-overlay").css("display", "none");
        }
        else {
            $("#sta-mobile-header a.sta-search").removeClass("sta-search-disable");
            $("#sta-mobile-header a.sta-search").addClass("sta-search-enable");
            $("#sta-page-overlay").css("display", "block");
        }
    },
    main: function () {
        staHeaderFooter.pageResponsive = staHeaderFooter.common.getPageResponsive();
        if (staHeaderFooter.pageResponsive) {
            //  Menu left
            var $navWrap = $('nav#sta-nav-wrap');
            $navWrap.clone().attr('id', 'sta-nav-wrap-copy').insertAfter("nav#sta-nav-wrap");
            $.each($("#sta-nav-wrap-copy ul.sta-mainNavSubMenu .sta-desp-column"), function (index, attr) {
                $(attr).find("a").wrap("<li class='sta-more-nav-highlight1'></li>");
                $(attr).find("li").insertAfter($(attr).parent().find(".sta-nav-li:last"));
            });
            // Menu right
            var $menuRight = $('nav#sta-nav-top');
            $menuRight.clone().attr('id', 'sta-nav-top-copy').insertAfter("nav#sta-nav-top");
            // Adding GroupTravel and My Account to right menu
            $("#sta-nav-top-copy .sta-group-travel").insertAfter($("#sta-nav-wrap-copy ul.sta-navigation > li:last-child"));
            $("#sta-nav-top-copy .sta-my-account").insertAfter($("#sta-nav-wrap-copy ul.sta-navigation > li:last-child"));
            // Menu left building
            $menuLeft = $('nav#sta-nav-wrap-copy');
            $("nav#sta-nav-wrap-copy ul.sta-shadow").removeClass("sta-shadow");
            $("nav#sta-nav-wrap-copy .sta-nav-li").unwrap().unwrap();
            $("nav#sta-nav-wrap-copy .sta-nav-li .sta-nav-heading2").contents().unwrap();
            $menuLeft.mmenu({
                classes: 'mm-white',
                dragOpen: true
            });
            // Menu right building
            var $menuRightCopy = $('nav#sta-nav-top-copy');
            $("#sta-nav-top-copy .sta-nav-left .sta-nav-dropdown:first-child").unwrap();
            $("#sta-nav-top-copy .sta-nav-right .sta-nav-dropdown:first-child").unwrap();
            $("#sta-nav-top-copy .sta-nav-dropdown:first-child .sta-nav-top-sub-menu").children().unwrap().unwrap();
            var contactText = $("#sta-nav-top-copy .sta-contact").text();
            $("#sta-nav-top-copy .sta-contact").wrap("<li>").wrap("<span class='sta-nav-heading2'>");
            $("#sta-nav-top-copy .sta-contact").remove();
            $("#sta-nav-top-copy li span.sta-nav-heading2").text(contactText);
            $("#sta-nav-top-copy #sta-search-form").attr("id", "sta-search-form-copy");
            $("#sta-nav-top-copy #sta-search-form-copy").unwrap().unwrap();
            $("#sta-nav-top-copy #sta-search-form-copy").insertAfter($("#sta-nav-top-copy"));
            $menuRightCopy.mmenu({
                position: 'right',
                classes: 'mm-white',
                dragOpen: true
            });
            // Change the right menu 
            $("#sta-nav-top-copy .sta-nav-top-sub-menu .sta-nav-heading3").text('Select Country');
            $("#sta-nav-top-copy .sta-nav-top-sub-menu .mm-subtitle .mm-subclose").addClass('mm-country-back');
        }
        $("#nav > li").hover(
            function () {
                $("#sta-page-overlay").css("display", "block");
            },
            function () {
                $("#sta-page-overlay").css("display", "none");
            }
        );
        // check if there is appointment
        if ($("#sta-nav-top .sta-nav-left li.sta-appointment").length > 0)
            staHeaderFooter.header.topHeaderBorderChange("sta-contact-dropdown", "sta-appointment");
        else
            staHeaderFooter.header.topHeaderBorderChange("sta-contact-dropdown", "sta-store");
        staHeaderFooter.header.topHeaderBorderChange("sta-appointment", "sta-store");
        // check if there is my account
        if ($("#sta-nav-top .sta-nav-right li.sta-my-account").length > 0)
            staHeaderFooter.header.topHeaderBorderChange("sta-group-travel", "sta-my-account");
        else
            staHeaderFooter.header.topHeaderBorderChange("sta-group-travel", "sta-flag-dropdown");
        staHeaderFooter.header.topHeaderBorderChange("sta-my-account", "sta-flag-dropdown");
        staHeaderFooter.header.topHeaderBorderChange("sta-flag-dropdown", "sta-search-dropdown");
        staHeaderFooter.searchMobileVisible = $('#sta-search-form-copy').is(":visible");
        $("#sta-mobile-header a.sta-search").click(function () {
            staHeaderFooter.searchMobileVisible = (staHeaderFooter.searchMobileVisible == true) ? false : true;
            staHeaderFooter.header.toggleMobileSearch(staHeaderFooter.searchMobileVisible);
            $("#sta-search-form-copy").slideToggle();
        });
        $("#sta-search-form-copy #sta-searchField").focus(function () {
            $("#sta-search-form-copy #sta-searchField").val('');
        });
        $("#sta-search-form #sta-searchField").focus(function () {
            $("#sta-search-form #sta-searchField").val('');
        });
    },
    topHeaderBorderChange: function (hoverElement, changeElement) {
        $("#sta-nav-top li." + hoverElement).hover(function () {
            $("#sta-nav-top li." + changeElement + " a").css("border-left", "1px solid #e9ebee");
        }, function () {
            $("#sta-nav-top li." + changeElement + " a").css("border-left", "1px solid #d1d1d1");
        });
    },
    newsletterSignUp: function (emailElement, subscribeButtonElement, subscribeTxt) {
        var emailAddress = $("#" + emailElement).val();
        if (staHeaderFooter.common.isValidEmailAddress(emailAddress)) {
            $.ajax({
                url: "http://www.statravel.com.au/newsletterapp/signUp",
                type: "POST",
                data: {
                    marketCode: $("#sta-marketCodeNewsletter").val(),
                    email: emailAddress
                },
                beforeSend: function () {
                    $("#" + subscribeButtonElement).val("Submitting");
                    $("#" + subscribeButtonElement).prop("disabled", true);
                },
                complete: function () {
                    $("#" + subscribeButtonElement).val(subscribeTxt);
                    $("#" + subscribeButtonElement).prop("disabled", false);
                },
                success: function (data) {
                    // console.log(data);
                    if (data != "ERROR") {
                        window.location = $("#sta-newsletterSecondPage").val() + emailAddress;
                        staHeaderFooter.common.setCookie("staNewsletterSubscribed", "true", "30");
                    }
                    else {
                        alert("Some issue subscribing. Please try again later.");
                    }
                }
            });
        }
        else {
            alert("Please enter valid email address");
        }
    },
    newsletterWidget: function () {
        var isSubscribed = staHeaderFooter.common.getCookie("staNewsletterSubscribed");
        if (isSubscribed == null) {
            isSubscribed = "false";
        }
        staHeaderFooter.visits = parseInt(staHeaderFooter.visits) + 1;
        staHeaderFooter.common.setCookie("staVisits", staHeaderFooter.visits);
        if (isSubscribed === "true") {
            $("#sta-newsletter").attr('style', 'display:none !important');
        }
        else {
            // If first visit or visit is multiple of 5
            if (staHeaderFooter.visits == 1 || staHeaderFooter.visits % 5 == 0) {
                $("#sta-newsletter").attr('style', 'display:block !important');
            }
            else {
                $("#sta-newsletter").attr('style', 'display:none !important');
            }
        }
        $("#close").click(function () {
            $("#sta-newsletter").attr('style', 'display:none !important');
            return false;
        });
        $("#sta-subscribeBtn").click(function () {
            staHeaderFooter.header.newsletterSignUp('sta-email', 'sta-subscribeBtn', 'Subscribe');
            return false;
        });
    },
    navlazy: function () {
        $("#sta-nav-wrap #sta-nav > li > a").hover(function () {
            var link = $(this);
            var image = link.parent().find("img.sta-nav-lazy");
            var dataOriginal = link.parent().find("img.sta-nav-lazy").data('original');
            image.attr("src", dataOriginal);
        });
    }
};