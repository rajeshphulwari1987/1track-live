﻿/**
 * Loads the STA CSS files and injects them into the <b>head</b> tag.
 *
 * @param division
 * division initials.
 *
 */
function getSTATopHeader(division) {

 if (division !== null && division !== "") {

 jQuery
 .ajax({
 cache : false,
 type : "GET",
 contentType : "application/json; charset=utf-8",
 async : false,
 url : "http://www.statravel.co.uk/meganav/get-json-top-css-js?division="
 + division,
 dataType : "jsonp",
 jsonpCallback : 'staTopHeader',

 success : function(data) {
 jQuery("head").append(data.html);
 },
 error : function(request, status, error) {
 console.log("STA top header loading failed. " + error);
 }
 });

 } else {
 console.log("Division not provided");
 }
}

/**
 * Loads the STA header section and injects it into the component for the id
 * given as parameter.
 *
 * @param componentIdToInject
 * component id to inject the HTML received.
 * @param division
 * division initials.
 * @param phoneNumber
 * phone number. By default displays the STA phone number.
 * <b>Optional</b>
 * @param textSearch
 * sets the search box visibility. By default display it. <b>Optional</b>
 * @param liveChat
 * sets the live chat box visibility. By default does not display.
 * <b>Optional</b>
 * @param menuItem
 * number of the menu item to highlights the menu item. By default
 * any menu item is highlighted.<b>Optional</b>
 */
function getSTAHeader(componentIdToInject, division, phoneNumber, textSearch,
 liveChat, menuItem) {

 if ((componentIdToInject !== null && componentIdToInject !== "")
 && (division !== null && division !== "")) {

 jQuery.ajax({
 cache : false,
 type : "GET",
 contentType : "application/json; charset=utf-8",
 async : false,
 url : "http://www.statravel.co.uk/meganav/get-json-header?division="
 + division + "&phoneNumber=" + phoneNumber + "&textSearch="
 + textSearch + "&liveChat=" + liveChat + "&menuItem="
 + menuItem,
 dataType : "jsonp",
 jsonpCallback : 'staHeader',

 success : function(data) {

 jQuery("#" + componentIdToInject).append(data.html);
 staHeaderFooter();
 },
 error : function(request, status, error) {
 console.log("STA top header loading failed. " + error);
 }
 });
 } else {
 console.log("Division/componentId not provided");
 }
}

/**
 * Loads the STA footer section and injects it into the component for the id
 * given as parameter.
 *
 * @param componentIdToInject
 * component id to inject the HTML received.
 * @param division
 * division initials.
 * @param phoneNumber
 * phone number. By default displays the STA phone number.
 * <b>Optional</b>
 * @param liveChat
 * sets the live chat box visibility. By default display it.
 * <b>Optional</b>
 * @param legalText
 * legal text to override the STA default one. By default display the
 * STA legal text <b>Optional</b>
 * @param displayAffiliationIcons
 * number of the menu item to highlights the menu item. By default
 * display the affiliation icons. <b>Optional</b>
 */
function getSTAFooter(componentIdToInject, division, phoneNumber,
 displayLivechat, legalText, displayAffiliationIcons) {

 if ((componentIdToInject !== null && componentIdToInject !== "")
 && (division !== null && division !== "")) {

 jQuery.ajax({
 cache : false,
 type : "GET",
 contentType : "application/json; charset=utf-8",
 async : false,
 url : "http://www.statravel.co.uk/meganav/get-json-footer?division="
 + division + "&phoneNumber=" + phoneNumber + "&liveChat="
 + displayLivechat + "&legalText" + legalText
 + "&affiliationIcons=" + displayAffiliationIcons,
 dataType : "jsonp",
 jsonpCallback : 'staFooter',

 success : function(data) {

 jQuery("#" + componentIdToInject).append(data.html);
 staHeaderFooter();

 },
 error : function(request, status, error) {
 console.log("STA footer loading failed. " + error);
 }
 });

 } else {
 console.log("Division/componentId not provided");
 }
}