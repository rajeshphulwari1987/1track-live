﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="Feedback.aspx.cs"
    Inherits="Feedback" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:content id="HeaderContent" runat="server" contentplaceholderid="HeadContent">
    <script type="text/javascript">
        function keycheck() {
            var KeyID = event.keyCode;
            if (KeyID >= 48 && KeyID <= 57 || KeyID == 46)
                return true;
            return false;
        }
        function checkval() {
            var style = $("#MainContent_reEmailreqcustvalerror").css('display');
            var visibility = $("#MainContent_reEmailreqcustvalerror").css('visibility');
            if (style == 'none' || visibility == 'hidden')
                $("#MainContent_reEmailreqcustvalerror").parent().find('input,select').removeClass('starail-Form-error');
            else
                $("#MainContent_reEmailreqcustvalerror").parent().find('input,select').addClass('starail-Form-error');
        }
    </script>
    <%=script %>
</asp:content>
<asp:content id="BodyContent" runat="server" contentplaceholderid="MainContent">
    <asp:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
    </asp:toolkitscriptmanager>
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
 <br />
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
        <h2>
            Feedback</h2>
        <div class="starail-Form-row">
            <p style="text-align: left;">
                Thank you for visiting our site. While we strive to bring you the best service and
                rail offering in the industry, we recognise there is always room for improvement. 
            </p>
        </div>
    </div>
    <div class="starail-Box starail-Box--whiteMobile starail-ContactForm">
      <div class="starail-Form-row">
      <h3>
           Your  Feedback </h3>
            <p> 
                Sent us your feedback, you can help us improve your experience.
            </p>
        </div>

        <div class="starail-Form starail-Form--onBoxNarrow starail-ContactForm-form">
            <div class="starail-Form-row">
                <label for="starail-firstname" class="starail-Form-label">
                    Name <span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <asp:textbox id="txtName" class="starail-Form-input" runat="server" placeholder="Enter Your Name" />
                    <asp:requiredfieldvalidator id="rfNmreqvalerror" runat="server" controltovalidate="txtName"
                        validationgroup="vs" />
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-firstname" class="starail-Form-label">
                    Phone No. <span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <asp:textbox id="txtPhn" class="starail-Form-input" runat="server" maxlength="15"
                        placeholder="Enter Your Phone Number" />
                    <asp:requiredfieldvalidator id="rfPhonereqvalerror" runat="server" controltovalidate="txtPhn"
                        validationgroup="vs" />
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-firstname" class="starail-Form-label">
                    Topic <span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <asp:dropdownlist id="ddlTopic" runat="server" cssclass="starail-Form-input" />
                    <asp:requiredfieldvalidator id="rfTopicreqvalerror" runat="server" controltovalidate="ddlTopic"
                        validationgroup="vs" initialvalue="-1" />
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-firstname" class="starail-Form-label">
                    Email <span class="starail-Form-required">*</span></label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <asp:textbox id="txtEmail" class="starail-Form-input" runat="server" placeholder="Enter Your Email" />
                    <asp:requiredfieldvalidator id="rfEmailreqvalerror" runat="server" controltovalidate="txtEmail"
                        validationgroup="vs" />
                    <asp:regularexpressionvalidator id="reqcustvalerrorreEmail" runat="server" controltovalidate="txtEmail"
                        validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" validationgroup="vs" />
                </div>
            </div>
            <div class="starail-Form-row">
                <label for="starail-firstname" class="starail-Form-label">
                    Feedback</label>
                <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                    <asp:textbox id="txtDesp" runat="server" textmode="MultiLine" cssclass="starail-Form-input"
                        rows="5" placeholder="Enter Your Feedback" style="width:100%" />
                </div>
            </div>
            <div class="starail-Form-row">
                <asp:button id="btnSubmit" class="starail-Button starail-Form-button" runat="server"
                    text="Send Feedback" validationgroup="vs" onclick="btnSubmit_Click" onclientclick="checkval()" />
            </div>
        </div>
    </div>
</asp:content>
