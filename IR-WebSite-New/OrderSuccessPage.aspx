﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OrderSuccessPage.aspx.cs" Inherits="OrderSuccessPage" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <meta name="WT.si_cs" content="1" />
    <meta name="WT.tx_e" content="p" />
    <%=WTpnsku%>
    <%=WTtxu%>
    <%=WTtxs%>
    <%=WTtxI%>
    <%=WTtxid%>
    <%=WTtxit%>
    <script type="text/javascript">
        window.history.forward();
        window.history.forward();
    </script>
    <%=products%>
    <%=script%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%=iframe %>
    <asp:HiddenField ID="hdnBookingFee" runat="server" Value="0.00" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Banner">
                <div class="starail-Banner-content">
                    <h1 class="starail-Banner-title starail-Banner-primaryTitle">
                        Enjoy Your Trip!</h1>
                </div>
                <img alt="Banner Alt" class="starail-Banner-image dots-header" id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
    <div class="starail-Grid">
        <div class="starail-Grid-col">
            <!-- Tickets -->
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" />
                </div>
            </asp:Panel>
            <div class="starail-YourBooking starail-YourBooking--confirmation">
                <asp:HyperLink ID="btnpdf" runat="server" Visible="false" CssClass="starail-Button starail-YourBooking-btn"
                    NavigateUrl="~/PrintTicket.aspx"><i class="starail-Icon-ticket"></i>Download Your Ticket</asp:HyperLink>
                <h2>
                    Your Journey</h2>
                <div class="starail-YourBooking-table">
                    <div class="starail-YourBooking-ticketDetails">
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Order Number:
                        </div>
                        <div class="starail-YourBooking-col--mobileFullWidth">
                            <asp:Label runat="server" ID="lblOrderNumber"></asp:Label>
                        </div>
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Order Date:
                        </div>
                        <div class="starail-YourBooking-col--mobileFullWidth">
                            <asp:Label runat="server" ID="lblOrderDate"></asp:Label>
                        </div>
                    </div>
                    <div class="starail-YourBooking-ticketDetails">
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Email Address:
                        </div>
                        <div class="starail-YourBooking-col--mobileFullWidth">
                            <asp:Label runat="server" ID="lblEmailAddress"></asp:Label>
                        </div>
                        <div class="verticaltop starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Delivery Address:
                        </div>
                        <div class="verticaltop starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <asp:Label runat="server" ID="lblDeliveryAddress"></asp:Label>
                        </div>
                    </div>

                    <div class="starail-YourBooking-ticketDetails" id="div_BookingRef" runat="server" visible="false">
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Booking Ref:
                        </div>
                        <div class="starail-YourBooking-col--mobileFullWidth">
                            <asp:label runat="server" id="lblBookingRef"></asp:label>
                        </div>
                        <div class="verticaltop starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            TOD Ref:
                        </div>
                        <div class="verticaltop starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <asp:label runat="server" id="lblTODRef"></asp:label>
                        </div>
                    </div>
                </div>
                <div class="starail-u-hideMobile">
                    <br />
                </div>
                <div class="starail-YourBooking-table" runat="server" id="DivPassSale">
                    <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                        <div>
                            Pass
                        </div>
                        <div>
                            Countries
                        </div>
                        <div>
                            Validity
                        </div>
                        <div class="starail-YourBooking-col-center">
                            Protected
                        </div>
                        <div class="starail-YourBooking-col">
                            Passenger information
                        </div>
                        <div class="starail-YourBooking-col-price">
                            Price
                        </div>
                    </div>
                    <asp:Repeater ID="RptPassDetails" runat="server">
                        <ItemTemplate>
                            <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--closed">
                                <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                    <a class="" href="#">
                                        <%#Eval("ProductDesc")%>:
                                        <br />
                                        <span class="starail-YourBooking-light">
                                            <%#Eval("Traveller")%></span> <i class="starail-Icon-chevron-up"></i></a>
                                </div>
                                <div class="starail-u-hideMobile">
                                    <p class="starail-YourBooking-title">
                                        <%#Eval("ProductDesc")%></p>
                                    <p>
                                        <%#Eval("Traveller")%></p>
                                    <asp:HiddenField runat="server" ID="hdnPass" Value='<%#Eval("ProductDesc")+"**"+Eval("Traveller")%>' />
                                </div>
                                <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                    <p class="starail-u-hideDesktop">
                                        Countries</p>
                                    <p>
                                        <%#Eval("Countries")%>
                                    </p>
                                    <asp:HiddenField runat="server" ID="hdnCountries" Value='<%#Eval("Countries")%>' />
                                </div>
                                <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                    <p class="starail-u-hideDesktop">
                                        Validity:</p>
                                    <p>
                                        <%#Eval("Validity")%></p>
                                    <asp:HiddenField runat="server" ID="hdnValidity" Value='<%#Eval("Validity")%>' />
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col-center">
                                    <p>
                                        <span class="starail-u-hideDesktop">Protected: </span>
                                        <%#Eval("Protected").ToString() == "Yes" ? "<i class='starail-Icon-shield'></i>": ""%>
                                        <%#Eval("ProtectedAmount")%>
                                        <span class="starail-u-visuallyHiddenDesktop">
                                            <%#Eval("Protected")%></span>
                                    </p>
                                    <asp:HiddenField runat="server" ID="hdnProtected" Value='<%#Eval("ProtectedAmount")+"**"+Eval("Protected")%>' />
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <p>
                                        <span class="starail-u-hideDesktop">Passenger information: </span>
                                        <span class="starail-u-hideMobile">
                                            Passenger (<%#Eval("Trvname")%>):
                                            <br />
                                        </span>
                                        <%#Eval("Passengerinfo")%><span class="starail-u-hideDesktopInline">(<%#Eval("Trvname")%>)</span></p>
                                </div>
                                <div class="starail-YourBooking-col-price">
                                    <p>
                                        <%=currency%><%#Eval("Price")%></p>
                                    <asp:HiddenField runat="server" ID="hdnPrice" Value='<%#Eval("Price")%>' />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="starail-YourBooking-table" runat="server" id="DivP2PSale">
                    <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                        <div>
                            Train</div>
                        <div>
                            Departs</div>
                        <div>
                            Arrives</div>
                        <div class="starail-YourBooking-col-center">
                            Class</div>
                        <div class="">
                            Passenger information</div>
                        <div class="starail-YourBooking-col-price">
                            Price</div>
                    </div>
                    <asp:Repeater ID="RptP2PDetails" runat="server">
                        <ItemTemplate>
                            <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--closed">
                                <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                   <a class="" href="#"><span class="starail-YourBooking-light">
                                        <%#Eval("Jolurny")%>
                                    </span>
                                        <%# isEvolviBooking.ToString() == "False" ? Eval("Train") : string.Empty%>
                                        <i class="starail-Icon-chevron-up"></i> </a>
                                </div>
                                <div class="starail-u-hideMobile">
                                    <p class="starail-YourBooking-title">
                                        <%#Eval("Jolurny")%></p>
                                     <p>
                                       <%# isEvolviBooking.ToString() == "False" ? Eval("Train") : string.Empty%>
                                       </p>
                                    <asp:HiddenField runat="server" ID="hdnTrain" Value='<%# isEvolviBooking.ToString() == "False"?Eval("Jolurny")+"**"+Eval("Train"):Eval("Jolurny")%>' />
                                </div>
                                <div class="starail-YourBooking-col">
                                    <p class="starail-u-hideDesktop">
                                        Departs:</p>
                                    <p>
                                        <%#Eval("Departs")%></p>
                                    <p>
                                        <%#Eval("DepartsStation")%></p>
                                    <asp:HiddenField runat="server" ID="hdnDeparts" Value='<%#Eval("Departs")+"**"+Eval("DepartsStation")%>' />
                                </div>
                                <div class="starail-YourBooking-col">
                                    <p class="starail-u-hideDesktop">
                                        Arrives:</p>
                                    <p>
                                        <%#Eval("Arrives")%></p>
                                    <p>
                                        <%#Eval("ArrivesStation")%></p>
                                    <asp:HiddenField runat="server" ID="hdnArrives" Value='<%#Eval("Arrives")+"**"+Eval("ArrivesStation")%>' />
                                </div>
                                <div class="starail-YourBooking-col-center">
                                    <p>
                                        <span class="starail-u-hideDesktop">Class: </span>
                                        <%#Eval("Class")%></p>
                                    <asp:HiddenField runat="server" ID="hdnClass" Value='<%#Eval("Class")%>' />
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <p>
                                        <span class="starail-u-hideDesktop">Passenger information: </span>
                                        <span class="starail-u-hideMobile">
                                            <%#Eval("Trvname")%></span>
                                        <br />
                                        <%#Eval("Passengerinfo")%>
                                        <span class="starail-u-hideDesktopInline">(<%#Eval("Trvname")%>)</span></p>
                                </div>
                                <div class="starail-YourBooking-col-price">
                                    <p>
                                        <%=currency%><%#Eval("Price")%></p>
                                    <asp:HiddenField runat="server" ID="hdnPrice" Value='<%#Eval("Price")%>' />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="starail-u-hideMobile">
                    <br />
                </div>
                <div class="starail-YourBooking-table">
                    <div class="starail-YourBooking-ticketDetails">
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Shipping Amount:
                        </div>
                        <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <%=currency%><asp:Label runat="server" ID="lblShippingAmount"></asp:Label>
                        </div>
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Booking Fee:
                        </div>
                        <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <%=currency%><asp:Label runat="server" ID="lblBookingFee"></asp:Label>
                        </div>
                    </div>
                    <div class="starail-YourBooking-ticketDetails">
                        <div id="OrderDiscount1" runat="server" class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Discount Amount:
                        </div>
                        <div id="OrderDiscount2" runat="server" class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <%=currency%><asp:Label runat="server" ID="lblDiscount"></asp:Label>
                        </div>
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            <div runat="server" id="hidelblAdminfee">
                                Admin Fee:</div>
                        </div>
                        <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <div runat="server" id="hidetxtAdminfee">
                                <%=currency%><asp:Label runat="server" ID="lblAdminFee"></asp:Label></div>
                        </div>
                    </div>
                     <div class="starail-YourBooking-ticketDetails">
                         <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth" id="otherchargesText" runat="server" visible="false">
                         Other Charges:
                         </div>
                        <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth" id="otherchargesValue" runat="server" visible="false">
                           <%=currency%><asp:label runat="server" id="lblOtherCharges"></asp:label>
                        </div>
                        <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                            Net Total:
                        </div>
                       <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                            <%=currency%><asp:Label runat="server" ID="lblNetTotal"></asp:Label>
                        </div>
                    </div>
                </div>
                <p class="starail-YourBooking-totalPrice">
                    Total Price for
                    <asp:Label ID="lbltxttraveller" runat="server"></asp:Label>: <span class="starail-YourBooking-totalPrice-amount">
                        <%=currency%></span><asp:Label CssClass="starail-YourBooking-totalPrice-amount" ID="lblGrandTotal"
                            runat="server"></asp:Label>
                </p>
                      <a href="#" id="anchor_pdf" runat="server" class="starail-YourBooking-receipt" target="_blank">
                      <i class="starail-Icon-receipt">
                </i>Download Your Receipt</a>
            </div>
            <!-- / Tickets -->
        </div>
    </div>
    <script type="text/javascript">
        function DownloadReceipt() {
            $("#btnPrint").trigger('click');
        }
    </script>
</asp:Content>
