﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CountryDetail.aspx.cs" Inherits="CountryDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControls/map.ascx" TagName="map" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=IRScriptingTagJs%>
    <style type="text/css">
        a
        {
            color: #0645ad;
        }
        a:visited
        {
            color: #0b0080;
        }
        .starail-Banner
        {
            height: 190px !important;
        }
        .starail-Grid-col
        {
            display: block !important;
        }
        .starail-Grid--medium
        {
            max-width: 100% !important;
        }
        .left-banner
        {
            float: left !important;
            width: 732px !important;
            height: 190px !important;
        }
        .right-banner
        {
            float: right !important;
            width: 272px !important;
            height: 190px !important;
        }
        .sitemaptext
        {
            background: #dfdfdf none repeat scroll 0 0;
            border-bottom: 1px dashed #757575;
            border-top: 1px dashed #757575;
            color: #951f35;
            font-size: 17px;
            width: 100%;
            margin: 10px 0;
            padding: 0 10px;
        }
        .sitemaptext h1
        {
            margin: 10px 0;
        }
        .starail-Lightbox-content-inner
        {
            max-width: 75% !important;
        }
        .banner90 h1
        {
            color: #ffffff;
            font-size: 16px;
            padding: 5px;
            font-weight: 500;
        }
        .span-heading
        {
            margin-bottom: 15px;
        }
        .span-heading span
        {
            font-family: 'Titillium Web' ,Tahoma,Verdana,Segoe,sans-serif;
            color: #444;
            font-weight: 700;
            text-transform: uppercase;
            line-height: 1;
            font-size: 1.71429rem;
            margin: 0 0 1.42857rem;
        }
    </style>
    <script type="text/javascript">
        $(function () { setMenuActive("C"); });
        function reloadmap() {
            document.getElementById('mapframe').contentWindow.location.reload();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="starail-Grid starail-Grid--mobileFull starail-Grid-col--nopadding">
        <asp:HiddenField ID="lblcn" runat="server" />
        <div class="starail-Grid-col starail-Grid-col--nopadding ">
            <div class="innerbaner starail-Banner ">
                <div class="banner90">
                    <h1 id="lblCntyNm" runat="server" style="text-transform: none !important;">
                    </h1>
                </div>
                <div class="left-banner">
                    <asp:Image ID="imgBanner" border="0" runat="server" /></div>
                <div class="right-banner">
                    <asp:Image ID="imgCountry" runat="server" /></div>
            </div>
        </div>
    </div>
    <div class="Breadcrumb">
        <asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-Grid starail-Grid--medium starail-u-relative">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" />
            </div>
        </asp:Panel>
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <h2 id="h_heading" runat="server">
                Train Tickets in
            </h2>
            <p id="p_countryInfo" runat="server">
            </p>
        </div>
        <div class="starai8l-Section starail-Section--nopadding">
            <div class="span-heading">
                <span>RAIL PASSES AND TRAINS</span></div>
            <asp:Repeater ID="rptProduct" runat="server">
                <ItemTemplate>
                    <a class="starail-ImageLink linkbox-product IR-GaCode" irgacodetext='<%#Eval("Name")%>'
                        href='<%#Eval("Url")
                %>'>
                        <div class="starail-Tag starail-Tag--specialOffer" id="div_SpecialOffer" runat="server"
                            visible='<%#Eval("IsSpecialOffer")%>'>
                        </div>
                        <div class="starail-Tag starail-Tag--bestSeller" id="div_BestSeller" runat="server"
                            visible='<%#Eval("BestSeller")%>'>
                        </div>
                        <img src='<%# SiteUrl+""+Eval("ProductImage") %>' alt='<%#Eval("Name") %>' />
                        <div class="starail-ImageLink-titleContainer">
                            <p class="starail-ImageLink-title">
                                <span class="highlight">
                                    <%#Eval("Name")%></span>
                            </p>
                        </div>
                        <div class="starail-ImageLink-overlay">
                            <div class="starail-ImageLink-overlay-content">
                                <p>
                                    <%#Eval("Desciption")%></p>
                                <div class="starail-Button" <%#Convert.ToBoolean(Eval("IsFlag"))?"style='display:none;'":""%>>
                                    From
                                    <%=currency%>
                                    <%#Eval("Price") %>
                                    <br />
                                    Find out more</div>
                                <div class="starail-Button" <%#Convert.ToBoolean(Eval("IsFlag"))?"":"style='display:none;'"%>>
                                    Find out more</div>
                            </div>
                        </div>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
