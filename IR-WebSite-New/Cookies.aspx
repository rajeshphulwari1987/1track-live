﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Cookies.aspx.cs" Inherits="Cookies" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .starail-Banner 
        {
            height: 190px !important;
        }
        .starail-Grid-col
        {
            display: block !important;
        }
        .starail-Grid--medium
        {
            max-width: 100% !important;
        }
        .left-banner
        {
            float: left !important;
            width: 732px !important;
            height: 190px !important;
        }
        .right-banner
        {
            float: right !important;
            width: 272px !important;
            height: 190px !important;
        }
        .sitemaptext
        {
            background: #dfdfdf none repeat scroll 0 0;
            border-bottom: 1px dashed #757575;
            border-top: 1px dashed #757575;
            color: #951f35;
            font-size: 17px;
            width: 100%;
            margin: 10px 0;
            padding: 0 10px;
        }
        .sitemaptext h1
        {
            margin: 10px 0;
        }
    </style>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div class="starail-Grid starail-Grid--mobileFull starail-Grid-col--nopadding">
        <asp:HiddenField ID="lblcn" runat="server" />
        <div class="starail-Grid-col starail-Grid-col--nopadding ">
            <div class="innerbaner starail-Banner ">
                <div class="banner90">
                    <asp:Label ID="lblCntyNm" runat="server" Text="Cookies" /></div>
                <div class="left-banner">
                    <asp:Image ID="imgBanner" alt="" border="0" runat="server" ImageUrl="images/img_inner-banner.jpg" /></div>
                <div class="right-banner">
                    <asp:Image ID="imgMap" runat="server" ImageUrl="images/innerMap.gif" /></div>
            </div>
        </div>
    </div>
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>

    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
        <h2>
            <asp:Literal ID="ltrTitle" runat="server" />
        </h2>
        <div class="starail-Section starail-Section--nopadding">
            <p>
                <div id="ltrDesc" runat="server" style="text-align: justify;" />
            </p>
        </div>
        <div class="clear">
            &nbsp;</div>
    </div>
</asp:Content>
