﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Uk ticket collection.aspx.cs"
    Inherits="Ukticketcollection" MasterPageFile="Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('div.item').eq(0).addClass('active').end();
        });
        function activeCrousal() {
            $(".carousel-inner div:first-child").addClass("active");
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <main class="starail-Wrapper starail-Wrapper--main dots-starail-Wrapper" role="main"> 
             
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            
                    <div class="item">
                        <img src='http://admin.1tracktest.com/Uploaded/CmsBannerImgs/b546bbfb-00f8-44f1-b470-794c4696b07a.jpg' class="scale-with-grid" alt="" border="0"
                            width="100%" style="height: 253px" /></div>
                
        </div>
    </div>
    <div class="starail-Grid-col starail-Grid-col--nopadding">
        <h1>
            UK ticket collection
        </h1>
        <div id="MainContent_dvContent" style="float: right; width: 236px; padding-left: 10px;">
            <img src="http://admin.1tracktest.com/Uploaded/CMSImage/3e4140fa-6c74-4ce5-8ad5-2925a58c8cd1.jpg" id="MainContent_imgGinfo" />
        </div>
        <div id="MainContent_divDescription" style="padding: 3px; font-size: 13px;"><p>You can collect your tickets free of charge from any National Rail   station in the UK that has self-service ticket machines - it doesn't have to be   your  journey departure station, so if another station is more   conveniently located, you can collect your tickets from there instead.</p>
<p>Your tickets will be ready 15 minutes after making the booking and can be collected any time prior to your journey.</p>
<p>Here's how to collect your tickets from a self-service machine:</p>
<ul>
  <li>
    <p>You must have any valid  credit/debit card (it does not have to be the same one you used to make your   booking) when you collect your tickets from the self-service machine.</p>
  </li>
  <li>At the self-service machine, choose the 'collect pre-paid tickets' option <strong>before</strong> inserting  your credit card. If you do  not do this,  the machine does   not  connect with  the correct system that holds your existing booking,   and  will not  therefore dispense your pre-paid tickets </li>
</ul>
<ul>
  <li>Enter the 8-digit collection reference on your order confirmation   (this will be  letters and numbers, e.g. T4X7DPL9) to receive your   tickets</li>
</ul>
<ul>
  <li>Before leaving the self-service machine, check that the correct   number of coupons have been dispensed to cover the complete booking,   including   tickets, supplements and seat reservations. The collection   receipt is the last coupon to be printed. <strong>Any   problems should be raised immediately with local station staff</strong>.</li>
</ul>
<p>If you have problems collecting your tickets from the self-service   ticket machine,  please go immediately to the station booking office.   Upon production of  your booking confirmation print-out  station staff    should be able to issue your tickets.</p>
</div>
    </div>
 
            </main>
</asp:Content>
