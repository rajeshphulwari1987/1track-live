﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PrintTicket : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    readonly Masters _masterPage = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL;
    private Guid _siteId;
    public string script;
    Guid pageID;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Page.RouteData.Values["PageId"] != null)
                {
                    pageID = (Guid)Page.RouteData.Values["PageId"];
                }
                else
                {
                    var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
                    if (pid != null)
                        pageID = pid.ID;
                }

                script = new Masters().GetQubitScriptBySId(_siteId);
                if (Session["TicketBooking-REPLY"] != null && hdnExec.Value != "1")
                {
                    List<PrintResponse> list = Session["TicketBooking-REPLY"] as List<PrintResponse>;
                    if (list.Count != 0)
                    {
                        ltrTicketMsg.Visible = !string.IsNullOrEmpty(list.FirstOrDefault().URL);

                        if (list.Any(x => x.URL.Contains("#")))
                        {
                            List<PrintResponse> newlist = new List<PrintResponse>();
                            foreach (var item in list)
                            {
                                string[] urls = item.URL.Split('#');
                                foreach (var url in urls)
                                {
                                    if (!string.IsNullOrEmpty(url) && url.Length > 32)
                                    {
                                        newlist.Add(new PrintResponse
                                        {
                                            URL = url
                                        });
                                    }
                                }
                            }
                            list = newlist;
                        }

                        rptBeNePrint.DataSource = list;
                        rptBeNePrint.DataBind();
                        hdnExec.Value = "1";
                    }
                }
                PageContent(pageID, _siteId);
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void rptBeNePrint_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var divPrintAtHome = e.Item.FindControl("divPrintAtHome") as HtmlControl;
                if (Session["showPrintAtHome"] != null)
                {
                    if (Session["showPrintAtHome"].ToString() == "1")
                        if (divPrintAtHome != null)
                            divPrintAtHome.Visible = true;
                }
                Session.Remove("divPrintAtHome");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }
}