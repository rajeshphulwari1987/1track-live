var Hashtable = (function () {
    var p = "function";
    var n = (typeof Array.prototype.splice == p) ? function (s, r) {
        s.splice(r, 1)
    } : function (u, t) {
        var s, v, r;
        if (t === u.length - 1) {
            u.length = t
        } else {
            s = u.slice(t + 1);
            u.length = t;
            for (v = 0, r = s.length; v < r; ++v) {
                u[t + v] = s[v]
            }
        }
    };

    function a(t) {
        var r;
        if (typeof t == "string") {
            return t
        } else {
            if (typeof t.hashCode == p) {
                r = t.hashCode();
                return (typeof r == "string") ? r : a(r)
            } else {
                if (typeof t.toString == p) {
                    return t.toString()
                } else {
                    try {
                        return String(t)
                    } catch (s) {
                        return Object.prototype.toString.call(t)
                    }
                }
            }
        }
    }

    function g(r, s) {
        return r.equals(s)
    }

    function e(r, s) {
        return (typeof s.equals == p) ? s.equals(r) : (r === s)
    }

    function c(r) {
        return function (s) {
            if (s === null) {
                throw new Error("null is not a valid " + r)
            } else {
                if (typeof s == "undefined") {
                    throw new Error(r + " must not be undefined")
                }
            }
        }
    }
    var q = c("key"),
        l = c("value");

    function d(u, s, t, r) {
        this[0] = u;
        this.entries = [];
        this.addEntry(s, t);
        if (r !== null) {
            this.getEqualityFunction = function () {
                return r
            }
        }
    }
    var h = 0,
        j = 1,
        f = 2;

    function o(r) {
        return function (t) {
            var s = this.entries.length,
                v, u = this.getEqualityFunction(t);
            while (s--) {
                v = this.entries[s];
                if (u(t, v[0])) {
                    switch (r) {
                        case h:
                            return true;
                        case j:
                            return v;
                        case f:
                            return [s, v[1]]
                    }
                }
            }
            return false
        }
    }

    function k(r) {
        return function (u) {
            var v = u.length;
            for (var t = 0, s = this.entries.length; t < s; ++t) {
                u[v + t] = this.entries[t][r]
            }
        }
    }
    d.prototype = {
        getEqualityFunction: function (r) {
            return (typeof r.equals == p) ? g : e
        },
        getEntryForKey: o(j),
        getEntryAndIndexForKey: o(f),
        removeEntryForKey: function (s) {
            var r = this.getEntryAndIndexForKey(s);
            if (r) {
                n(this.entries, r[0]);
                return r[1]
            }
            return null
        },
        addEntry: function (r, s) {
            this.entries[this.entries.length] = [r, s]
        },
        keys: k(0),
        values: k(1),
        getEntries: function (s) {
            var u = s.length;
            for (var t = 0, r = this.entries.length; t < r; ++t) {
                s[u + t] = this.entries[t].slice(0)
            }
        },
        containsKey: o(h),
        containsValue: function (s) {
            var r = this.entries.length;
            while (r--) {
                if (s === this.entries[r][1]) {
                    return true
                }
            }
            return false
        }
    };

    function m(s, t) {
        var r = s.length,
            u;
        while (r--) {
            u = s[r];
            if (t === u[0]) {
                return r
            }
        }
        return null
    }

    function i(r, s) {
        var t = r[s];
        return (t && (t instanceof d)) ? t : null
    }

    function b(t, r) {
        var w = this;
        var v = [];
        var u = {};
        var x = (typeof t == p) ? t : a;
        var s = (typeof r == p) ? r : null;
        this.put = function (B, C) {
            q(B);
            l(C);
            var D = x(B),
                E, A, z = null;
            E = i(u, D);
            if (E) {
                A = E.getEntryForKey(B);
                if (A) {
                    z = A[1];
                    A[1] = C
                } else {
                    E.addEntry(B, C)
                }
            } else {
                E = new d(D, B, C, s);
                v[v.length] = E;
                u[D] = E
            }
            return z
        };
        this.get = function (A) {
            q(A);
            var B = x(A);
            var C = i(u, B);
            if (C) {
                var z = C.getEntryForKey(A);
                if (z) {
                    return z[1]
                }
            }
            return null
        };
        this.containsKey = function (A) {
            q(A);
            var z = x(A);
            var B = i(u, z);
            return B ? B.containsKey(A) : false
        };
        this.containsValue = function (A) {
            l(A);
            var z = v.length;
            while (z--) {
                if (v[z].containsValue(A)) {
                    return true
                }
            }
            return false
        };
        this.clear = function () {
            v.length = 0;
            u = {}
        };
        this.isEmpty = function () {
            return !v.length
        };
        var y = function (z) {
            return function () {
                var A = [],
                    B = v.length;
                while (B--) {
                    v[B][z](A)
                }
                return A
            }
        };
        this.keys = y("keys");
        this.values = y("values");
        this.entries = y("getEntries");
        this.remove = function (B) {
            q(B);
            var C = x(B),
                z, A = null;
            var D = i(u, C);
            if (D) {
                A = D.removeEntryForKey(B);
                if (A !== null) {
                    if (!D.entries.length) {
                        z = m(v, C);
                        n(v, z);
                        delete u[C]
                    }
                }
            }
            return A
        };
        this.size = function () {
            var A = 0,
                z = v.length;
            while (z--) {
                A += v[z].entries.length
            }
            return A
        };
        this.each = function (C) {
            var z = w.entries(),
                A = z.length,
                B;
            while (A--) {
                B = z[A];
                C(B[0], B[1])
            }
        };
        this.putAll = function (H, C) {
            var B = H.entries();
            var E, F, D, z, A = B.length;
            var G = (typeof C == p);
            while (A--) {
                E = B[A];
                F = E[0];
                D = E[1];
                if (G && (z = w.get(F))) {
                    D = C(F, z, D)
                }
                w.put(F, D)
            }
        };
        this.clone = function () {
            var z = new b(t, r);
            z.putAll(w);
            return z
        }
    }
    return b
})();
(function (k) {
    var a = new Hashtable();
    var f = ["ae", "au", "ca", "cn", "eg", "gb", "hk", "il", "in", "jp", "sk", "th", "tw", "us"];
    var b = ["at", "br", "de", "dk", "es", "gr", "it", "nl", "pt", "tr", "vn"];
    var i = ["cz", "fi", "fr", "ru", "se", "pl"];
    var d = ["ch"];
    var g = [
        [".", ","],
        [",", "."],
        [",", " "],
        [".", "'"]
    ];
    var c = [f, b, i, d];

    function j(n, l, m) {
        this.dec = n;
        this.group = l;
        this.neg = m
    }

    function h() {
        for (var l = 0; l < c.length; l++) {
            localeGroup = c[l];
            for (var m = 0; m < localeGroup.length; m++) {
                a.put(localeGroup[m], l)
            }
        }
    }

    function e(l, r) {
        if (a.size() == 0) {
            h()
        }
        var q = ".";
        var o = ",";
        var p = "-";
        if (r == false) {
            if (l.indexOf("_") != -1) {
                l = l.split("_")[1].toLowerCase()
            } else {
                if (l.indexOf("-") != -1) {
                    l = l.split("-")[1].toLowerCase()
                }
            }
        }
        var n = a.get(l);
        if (n) {
            var m = g[n];
            if (m) {
                q = m[0];
                o = m[1]
            }
        }
        return new j(q, o, p)
    }
    k.fn.formatNumber = function (l, m, n) {
        return this.each(function () {
            if (m == null) {
                m = true
            }
            if (n == null) {
                n = true
            }
            var p;
            if (k(this).is(":input")) {
                p = new String(k(this).val())
            } else {
                p = new String(k(this).text())
            }
            var o = k.formatNumber(p, l);
            if (m) {
                if (k(this).is(":input")) {
                    k(this).val(o)
                } else {
                    k(this).text(o)
                }
            }
            if (n) {
                return o
            }
        })
    };
    k.formatNumber = function (q, w) {
        var w = k.extend({}, k.fn.formatNumber.defaults, w);
        var l = e(w.locale.toLowerCase(), w.isFullLocale);
        var n = l.dec;
        var u = l.group;
        var o = l.neg;
        var m = "0#-,.";
        var t = "";
        var s = false;
        for (var r = 0; r < w.format.length; r++) {
            if (m.indexOf(w.format.charAt(r)) == -1) {
                t = t + w.format.charAt(r)
            } else {
                if (r == 0 && w.format.charAt(r) == "-") {
                    s = true;
                    continue
                } else {
                    break
                }
            }
        }
        var v = "";
        for (var r = w.format.length - 1; r >= 0; r--) {
            if (m.indexOf(w.format.charAt(r)) == -1) {
                v = w.format.charAt(r) + v
            } else {
                break
            }
        }
        w.format = w.format.substring(t.length);
        w.format = w.format.substring(0, w.format.length - v.length);
        var p = new Number(q);
        return k._formatNumber(p, w, v, t, s)
    };
    k._formatNumber = function (m, q, n, I, t) {
        var q = k.extend({}, k.fn.formatNumber.defaults, q);
        var G = e(q.locale.toLowerCase(), q.isFullLocale);
        var F = G.dec;
        var w = G.group;
        var l = G.neg;
        var z = false;
        if (isNaN(m)) {
            if (q.nanForceZero == true) {
                m = 0;
                z = true
            } else {
                return null
            }
        }
        if (n == "%") {
            m = m * 100
        }
        var B = "";
        if (q.format.indexOf(".") > -1) {
            var H = F;
            var u = q.format.substring(q.format.lastIndexOf(".") + 1);
            if (q.round == true) {
                m = new Number(m.toFixed(u.length))
            } else {
                var M = m.toString();
                M = M.substring(0, M.lastIndexOf(".") + u.length + 1);
                m = new Number(M)
            }
            var A = m % 1;
            var C = new String(A.toFixed(u.length));
            C = C.substring(C.lastIndexOf(".") + 1);
            for (var J = 0; J < u.length; J++) {
                if (u.charAt(J) == "#" && C.charAt(J) != "0") {
                    H += C.charAt(J);
                    continue
                } else {
                    if (u.charAt(J) == "#" && C.charAt(J) == "0") {
                        var r = C.substring(J);
                        if (r.match("[1-9]")) {
                            H += C.charAt(J);
                            continue
                        } else {
                            break
                        }
                    } else {
                        if (u.charAt(J) == "0") {
                            H += C.charAt(J)
                        }
                    }
                }
            }
            B += H
        } else {
            m = Math.round(m)
        }
        var v = Math.floor(m);
        if (m < 0) {
            v = Math.ceil(m)
        }
        var E = "";
        if (q.format.indexOf(".") == -1) {
            E = q.format
        } else {
            E = q.format.substring(0, q.format.indexOf("."))
        }
        var L = "";
        if (!(v == 0 && E.substr(E.length - 1) == "#") || z) {
            var x = new String(Math.abs(v));
            var p = 9999;
            if (E.lastIndexOf(",") != -1) {
                p = E.length - E.lastIndexOf(",") - 1
            }
            var o = 0;
            for (var J = x.length - 1; J > -1; J--) {
                L = x.charAt(J) + L;
                o++;
                if (o == p && J != 0) {
                    L = w + L;
                    o = 0
                }
            }
            if (E.length > L.length) {
                var K = E.indexOf("0");
                if (K != -1) {
                    var D = E.length - K;
                    var s = E.length - L.length - 1;
                    while (L.length < D) {
                        var y = E.charAt(s);
                        if (y == ",") {
                            y = w
                        }
                        L = y + L;
                        s--
                    }
                }
            }
        }
        if (!L && E.indexOf("0", E.length - 1) !== -1) {
            L = "0"
        }
        B = L + B;
        if (m < 0 && t && I.length > 0) {
            I = l + I
        } else {
            if (m < 0) {
                B = l + B
            }
        }
        if (!q.decimalSeparatorAlwaysShown) {
            if (B.lastIndexOf(F) == B.length - 1) {
                B = B.substring(0, B.length - 1)
            }
        }
        B = I + B + n;
        return B
    };
    k.fn.parseNumber = function (l, m, o) {
        if (m == null) {
            m = true
        }
        if (o == null) {
            o = true
        }
        var p;
        if (k(this).is(":input")) {
            p = new String(k(this).val())
        } else {
            p = new String(k(this).text())
        }
        var n = k.parseNumber(p, l);
        if (n) {
            if (m) {
                if (k(this).is(":input")) {
                    k(this).val(n.toString())
                } else {
                    k(this).text(n.toString())
                }
            }
            if (o) {
                return n
            }
        }
    };
    k.parseNumber = function (s, x) {
        var x = k.extend({}, k.fn.parseNumber.defaults, x);
        var m = e(x.locale.toLowerCase(), x.isFullLocale);
        var p = m.dec;
        var v = m.group;
        var q = m.neg;
        var l = "1234567890.-";
        while (s.indexOf(v) > -1) {
            s = s.replace(v, "")
        }
        s = s.replace(p, ".").replace(q, "-");
        var w = "";
        var o = false;
        if (s.charAt(s.length - 1) == "%" || x.isPercentage == true) {
            o = true
        }
        for (var t = 0; t < s.length; t++) {
            if (l.indexOf(s.charAt(t)) > -1) {
                w = w + s.charAt(t)
            }
        }
        var r = new Number(w);
        if (o) {
            r = r / 100;
            var u = w.indexOf(".");
            if (u != -1) {
                var n = w.length - u - 1;
                r = r.toFixed(n + 2)
            } else {
                r = r.toFixed(w.length - 1)
            }
        }
        return r
    };
    k.fn.parseNumber.defaults = {
        locale: "us",
        decimalSeparatorAlwaysShown: false,
        isPercentage: false,
        isFullLocale: false
    };
    k.fn.formatNumber.defaults = {
        format: "#,###.00",
        locale: "us",
        decimalSeparatorAlwaysShown: false,
        nanForceZero: true,
        round: true,
        isFullLocale: false
    };
    Number.prototype.toFixed = function (l) {
        return k._roundNumber(this, l)
    };
    k._roundNumber = function (n, m) {
        var l = Math.pow(10, m || 0);
        var o = String(Math.round(n * l) / l);
        if (m > 0) {
            var p = o.indexOf(".");
            if (p == -1) {
                o += ".";
                p = 0
            } else {
                p = o.length - (p + 1)
            }
            while (p < m) {
                o += "0";
                p++
            }
        }
        return o
    }
})(jQuery);
(function () {
    var b = {};
    this.tmpl = function a(e, d) {
        var c = !/\W/.test(e) ? b[e] = b[e] || a(document.getElementById(e).innerHTML) : new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};with(obj){p.push('" + e.replace(/[\r\t\n]/g, " ").split("<%").join("\t").replace(/((^|%>)[^\t]*)'/g, "$1\r").replace(/\t=(.*?)%>/g, "',$1,'").split("\t").join("');").split("%>").join("p.push('").split("\r").join("\\'") + "');}return p.join('');");
        return d ? c(d) : c
    }
})();
(function (a) {
    a.baseClass = function (b) {
        b = a(b);
        return b.get(0).className.match(/([^ ]+)/)[1]
    };
    a.fn.addDependClass = function (d, b) {
        var c = {
            delimiter: b ? b : "-"
        };
        return this.each(function () {
            var e = a.baseClass(this);
            if (e) {
                a(this).addClass(e + c.delimiter + d)
            }
        })
    };
    a.fn.removeDependClass = function (d, b) {
        var c = {
            delimiter: b ? b : "-"
        };
        return this.each(function () {
            var e = a.baseClass(this);
            if (e) {
                a(this).removeClass(e + c.delimiter + d)
            }
        })
    };
    a.fn.toggleDependClass = function (d, b) {
        var c = {
            delimiter: b ? b : "-"
        };
        return this.each(function () {
            var e = a.baseClass(this);
            if (e) {
                if (a(this).is("." + e + c.delimiter + d)) {
                    a(this).removeClass(e + c.delimiter + d)
                } else {
                    a(this).addClass(e + c.delimiter + d)
                }
            }
        })
    }
})(jQuery);
(function (b) {
    function a() {
        this._init.apply(this, arguments)
    }
    a.prototype.oninit = function () { };
    a.prototype.events = function () { };
    a.prototype.onmousedown = function () {
        this.ptr.css({
            position: "absolute"
        })
    };
    a.prototype.onmousemove = function (d, c, e) {
        this.ptr.css({
            left: c,
            top: e
        })
    };
    a.prototype.onmouseup = function () { };
    a.prototype.isDefault = {
        drag: false,
        clicked: false,
        toclick: true,
        mouseup: false
    };
    a.prototype._init = function () {
        if (arguments.length > 0) {
            this.ptr = b(arguments[0]);
            this.outer = b(".draggable-outer");
            this.is = {};
            b.extend(this.is, this.isDefault);
            var c = this.ptr.offset();
            this.d = {
                left: c.left,
                top: c.top,
                width: this.ptr.width(),
                height: this.ptr.height()
            };
            this.oninit.apply(this, arguments);
            this._events()
        }
    };
    a.prototype._getPageCoords = function (c) {
        if (c.targetTouches && c.targetTouches[0]) {
            return {
                x: c.targetTouches[0].pageX,
                y: c.targetTouches[0].pageY
            }
        } else {
            return {
                x: c.pageX,
                y: c.pageY
            }
        }
    };
    a.prototype._bindEvent = function (f, d, e) {
        var c = this;
        if (this.supportTouches_) {
            f.get(0).addEventListener(this.events_[d], e, false)
        } else {
            f.bind(this.events_[d], e)
        }
    };
    a.prototype._events = function () {
        var c = this;
        this.supportTouches_ = (b.browser.webkit && navigator.userAgent.indexOf("Mobile") != -1);
        this.events_ = {
            click: this.supportTouches_ ? "touchstart" : "click",
            down: this.supportTouches_ ? "touchstart" : "mousedown",
            move: this.supportTouches_ ? "touchmove" : "mousemove",
            up: this.supportTouches_ ? "touchend" : "mouseup"
        };
        this._bindEvent(b(document), "move", function (d) {
            if (c.is.drag) {
                d.stopPropagation();
                d.preventDefault();
                c._mousemove(d)
            }
        });
        this._bindEvent(b(document), "down", function (d) {
            if (c.is.drag) {
                d.stopPropagation();
                d.preventDefault()
            }
        });
        this._bindEvent(b(document), "up", function (d) {
            c._mouseup(d)
        });
        this._bindEvent(this.ptr, "down", function (d) {
            c._mousedown(d);
            return false
        });
        this._bindEvent(this.ptr, "up", function (d) {
            c._mouseup(d)
        });
        this.ptr.find("a").click(function () {
            c.is.clicked = true;
            if (!c.is.toclick) {
                c.is.toclick = true;
                return false
            }
        }).mousedown(function (d) {
            c._mousedown(d);
            return false
        });
        this.events()
    };
    a.prototype._mousedown = function (c) {
        this.is.drag = true;
        this.is.clicked = false;
        this.is.mouseup = false;
        var d = this.ptr.offset();
        var e = this._getPageCoords(c);
        this.cx = e.x - d.left;
        this.cy = e.y - d.top;
        b.extend(this.d, {
            left: d.left,
            top: d.top,
            width: this.ptr.width(),
            height: this.ptr.height()
        });
        if (this.outer && this.outer.get(0)) {
            this.outer.css({
                height: Math.max(this.outer.height(), b(document.body).height()),
                overflow: "hidden"
            })
        }
        this.onmousedown(c)
    };
    a.prototype._mousemove = function (c) {
        this.is.toclick = false;
        var d = this._getPageCoords(c);
        this.onmousemove(c, d.x - this.cx, d.y - this.cy)
    };
    a.prototype._mouseup = function (c) {
        var d = this;
        if (this.is.drag) {
            this.is.drag = false;
            if (this.outer && this.outer.get(0)) {
                if (b.browser.mozilla) {
                    this.outer.css({
                        overflow: "hidden"
                    })
                } else {
                    this.outer.css({
                        overflow: "visible"
                    })
                }
                if (b.browser.msie && b.browser.version == "6.0") {
                    this.outer.css({
                        height: "100%"
                    })
                } else {
                    this.outer.css({
                        height: "auto"
                    })
                }
            }
            this.onmouseup(c)
        }
    };
    window.Draggable = a
})(jQuery);
(function (d) {
    function a(f) {
        if (typeof f == "undefined") {
            return false
        }
        if (f instanceof Array || (!(f instanceof Object) && (Object.prototype.toString.call((f)) == "[object Array]") || typeof f.length == "number" && typeof f.splice != "undefined" && typeof f.propertyIsEnumerable != "undefined" && !f.propertyIsEnumerable("splice"))) {
            return true
        }
        return false
    }
    d.slider = function (h, g) {
        var f = d(h);
        if (!f.data("jslider")) {
            f.data("jslider", new c(h, g))
        }
        return f.data("jslider")
    };
    d.fn.slider = function (j, g) {
        var i, h = arguments;

        function f(l) {
            return l !== undefined
        }

        function k(l) {
            return l != null
        }
        this.each(function () {
            var m = d.slider(this, j);
            if (typeof j == "string") {
                switch (j) {
                    case "value":
                        if (f(h[1]) && f(h[2])) {
                            var l = m.getPointers();
                            if (k(l[0]) && k(h[1])) {
                                l[0].set(h[1]);
                                l[0].setIndexOver()
                            }
                            if (k(l[1]) && k(h[2])) {
                                l[1].set(h[2]);
                                l[1].setIndexOver()
                            }
                        } else {
                            if (f(h[1])) {
                                var l = m.getPointers();
                                if (k(l[0]) && k(h[1])) {
                                    l[0].set(h[1]);
                                    l[0].setIndexOver()
                                }
                            } else {
                                i = m.getValue()
                            }
                        }
                        break;
                    case "prc":
                        if (f(h[1]) && f(h[2])) {
                            var l = m.getPointers();
                            if (k(l[0]) && k(h[1])) {
                                l[0]._set(h[1]);
                                l[0].setIndexOver()
                            }
                            if (k(l[1]) && k(h[2])) {
                                l[1]._set(h[2]);
                                l[1].setIndexOver()
                            }
                        } else {
                            if (f(h[1])) {
                                var l = m.getPointers();
                                if (k(l[0]) && k(h[1])) {
                                    l[0]._set(h[1]);
                                    l[0].setIndexOver()
                                }
                            } else {
                                i = m.getPrcValue()
                            }
                        }
                        break;
                    case "calculatedValue":
                        var o = m.getValue().split(";");
                        i = "";
                        for (var n = 0; n < o.length; n++) {
                            i += (n > 0 ? ";" : "") + m.nice(o[n])
                        }
                        break;
                    case "skin":
                        m.setSkin(h[1]);
                        break
                }
            } else {
                if (!j && !g) {
                    if (!a(i)) {
                        i = []
                    }
                    i.push(m)
                }
            }
        });
        if (a(i) && i.length == 1) {
            i = i[0]
        }
        return i || this
    };
    var e = {
        settings: {
            from: 1,
            to: 10,
            step: 1,
            smooth: true,
            limits: true,
            round: 0,
            format: {
                format: "#,##0.##"
            },
            value: "5;7",
            dimension: ""
        },
        className: "jslider",
        selector: ".jslider-",
        template: tmpl('<span class="<%=className%>"><table><tr><td><div class="<%=className%>-bg"><i class="l"></i><i class="r"></i><i class="v"></i></div><div class="<%=className%>-pointer"></div><div class="<%=className%>-pointer <%=className%>-pointer-to"></div><div class="<%=className%>-label"><span><%=settings.from%></span></div><div class="<%=className%>-label <%=className%>-label-to"><span><%=settings.to%></span><%=settings.dimension%></div><div class="<%=className%>-value"><span></span><%=settings.dimension%></div><div class="<%=className%>-value <%=className%>-value-to"><span></span><%=settings.dimension%></div><div class="<%=className%>-scale"><%=scale%></div></td></tr></table></span>')
    };

    function c() {
        return this.init.apply(this, arguments)
    }
    c.prototype.init = function (g, f) {
        this.settings = d.extend(true, {}, e.settings, f ? f : {});
        this.inputNode = d(g).hide();
        this.settings.interval = this.settings.to - this.settings.from;
        this.settings.value = this.inputNode.attr("value");
        if (this.settings.calculate && d.isFunction(this.settings.calculate)) {
            this.nice = this.settings.calculate
        }
        if (this.settings.onstatechange && d.isFunction(this.settings.onstatechange)) {
            this.onstatechange = this.settings.onstatechange
        }
        this.is = {
            init: false
        };
        this.o = {};
        this.create()
    };
    c.prototype.onstatechange = function () { };
    c.prototype.create = function () {
        var f = this;
        this.domNode = d(e.template({
            className: e.className,
            settings: {
                from: this.nice(this.settings.from),
                to: this.nice(this.settings.to),
                dimension: this.settings.dimension
            },
            scale: this.generateScale()
        }));
        this.inputNode.after(this.domNode);
        this.drawScale();
        if (this.settings.skin && this.settings.skin.length > 0) {
            this.setSkin(this.settings.skin)
        }
        this.sizes = {
            domWidth: this.domNode.width(),
            domOffset: this.domNode.offset()
        };
        d.extend(this.o, {
            pointers: {},
            labels: {
                0: {
                    o: this.domNode.find(e.selector + "value").not(e.selector + "value-to")
                },
                1: {
                    o: this.domNode.find(e.selector + "value").filter(e.selector + "value-to")
                }
            },
            limits: {
                0: this.domNode.find(e.selector + "label").not(e.selector + "label-to"),
                1: this.domNode.find(e.selector + "label").filter(e.selector + "label-to")
            }
        });
        d.extend(this.o.labels[0], {
            value: this.o.labels[0].o.find("span")
        });
        d.extend(this.o.labels[1], {
            value: this.o.labels[1].o.find("span")
        });
        if (!f.settings.value.split(";")[1]) {
            this.settings.single = true;
            this.domNode.addDependClass("single")
        }
        if (!f.settings.limits) {
            this.domNode.addDependClass("limitless")
        }
        this.domNode.find(e.selector + "pointer").each(function (g) {
            var j = f.settings.value.split(";")[g];
            if (j) {
                f.o.pointers[g] = new b(this, g, f);
                var h = f.settings.value.split(";")[g - 1];
                if (h && new Number(j) < new Number(h)) {
                    j = h
                }
                j = j < f.settings.from ? f.settings.from : j;
                j = j > f.settings.to ? f.settings.to : j;
                f.o.pointers[g].set(j, true)
            }
        });
        this.o.value = this.domNode.find(".v");
        this.is.init = true;
        d.each(this.o.pointers, function (g) {
            f.redraw(this)
        });
        (function (g) {
            d(window).resize(function () {
                g.onresize()
            })
        })(this)
    };
    c.prototype.setSkin = function (f) {
        if (this.skin_) {
            this.domNode.removeDependClass(this.skin_, "_")
        }
        this.domNode.addDependClass(this.skin_ = f, "_")
    };
    c.prototype.setPointersIndex = function (f) {
        d.each(this.getPointers(), function (g) {
            this.index(g)
        })
    };
    c.prototype.getPointers = function () {
        return this.o.pointers
    };
    c.prototype.generateScale = function () {
        if (this.settings.scale && this.settings.scale.length > 0) {
            var h = "";
            var g = this.settings.scale;
            var j = Math.round((100 / (g.length - 1)) * 10) / 10;
            for (var f = 0; f < g.length; f++) {
                h += '<span style="left: ' + f * j + '%">' + (g[f] != "|" ? "<ins>" + g[f] + "</ins>" : "") + "</span>"
            }
            return h
        }
        return ""
    };
    c.prototype.drawScale = function () {
        this.domNode.find(e.selector + "scale span ins").each(function () {
            d(this).css({
                marginLeft: -d(this).outerWidth() / 2
            })
        })
    };
    c.prototype.onresize = function () {
        var f = this;
        this.sizes = {
            domWidth: this.domNode.width(),
            domOffset: this.domNode.offset()
        };
        d.each(this.o.pointers, function (g) {
            f.redraw(this)
        })
    };
    c.prototype.update = function () {
        this.onresize();
        this.drawScale()
    };
    c.prototype.limits = function (f, i) {
        if (!this.settings.smooth) {
            var h = this.settings.step * 100 / (this.settings.interval);
            f = Math.round(f / h) * h
        }
        var g = this.o.pointers[1 - i.uid];
        if (g && i.uid && f < g.value.prc) {
            f = g.value.prc
        }
        if (g && !i.uid && f > g.value.prc) {
            f = g.value.prc
        }
        if (f < 0) {
            f = 0
        }
        if (f > 100) {
            f = 100
        }
        return Math.round(f * 10) / 10
    };
    c.prototype.redraw = function (f) {
        if (!this.is.init) {
            return false
        }
        this.setValue();
        if (this.o.pointers[0] && this.o.pointers[1]) {
            this.o.value.css({
                left: this.o.pointers[0].value.prc + "%",
                width: (this.o.pointers[1].value.prc - this.o.pointers[0].value.prc) + "%"
            })
        }
        this.o.labels[f.uid].value.html(this.nice(f.value.origin));
        this.redrawLabels(f)
    };
    c.prototype.redrawLabels = function (l) {
        function h(n, o, p) {
            o.margin = -o.label / 2;
            label_left = o.border + o.margin;
            if (label_left < 0) {
                o.margin -= label_left
            }
            if (o.border + o.label / 2 > g.sizes.domWidth) {
                o.margin = 0;
                o.right = true
            } else {
                o.right = false
            }
            n.o.css({
                left: p + "%",
                marginLeft: o.margin,
                right: "auto"
            });
            if (o.right) {
                n.o.css({
                    left: "auto",
                    right: 0
                })
            }
            return o
        }
        var g = this;
        var i = this.o.labels[l.uid];
        var m = l.value.prc;
        var j = {
            label: i.o.outerWidth(),
            right: false,
            border: (m * this.sizes.domWidth) / 100
        };
        if (!this.settings.single) {
            var f = this.o.pointers[1 - l.uid];
            var k = this.o.labels[f.uid];
            switch (l.uid) {
                case 0:
                    if (j.border + j.label / 2 > k.o.offset().left - this.sizes.domOffset.left) {
                        k.o.css({
                            visibility: "hidden"
                        });
                        k.value.html(this.nice(f.value.origin));
                        i.o.css({
                            visibility: "visible"
                        });
                        m = (f.value.prc - m) / 2 + m;
                        if (f.value.prc != l.value.prc) {
                            i.value.html(this.nice(l.value.origin) + "&nbsp;&ndash;&nbsp;" + this.nice(f.value.origin));
                            j.label = i.o.outerWidth();
                            j.border = (m * this.sizes.domWidth) / 100
                        }
                    } else {
                        k.o.css({
                            visibility: "visible"
                        })
                    }
                    break;
                case 1:
                    if (j.border - j.label / 2 < k.o.offset().left - this.sizes.domOffset.left + k.o.outerWidth()) {
                        k.o.css({
                            visibility: "hidden"
                        });
                        k.value.html(this.nice(f.value.origin));
                        i.o.css({
                            visibility: "visible"
                        });
                        m = (m - f.value.prc) / 2 + f.value.prc;
                        if (f.value.prc != l.value.prc) {
                            i.value.html(this.nice(f.value.origin) + "&nbsp;&ndash;&nbsp;" + this.nice(l.value.origin));
                            j.label = i.o.outerWidth();
                            j.border = (m * this.sizes.domWidth) / 100
                        }
                    } else {
                        k.o.css({
                            visibility: "visible"
                        })
                    }
                    break
            }
        }
        j = h(i, j, m);
        if (k) {
            var j = {
                label: k.o.outerWidth(),
                right: false,
                border: (f.value.prc * this.sizes.domWidth) / 100
            };
            j = h(k, j, f.value.prc)
        }
        this.redrawLimits()
    };
    c.prototype.redrawLimits = function () {
        if (this.settings.limits) {
            var h = [true, true];
            for (key in this.o.pointers) {
                if (!this.settings.single || key == 0) {
                    var l = this.o.pointers[key];
                    var g = this.o.labels[l.uid];
                    var k = g.o.offset().left - this.sizes.domOffset.left;
                    var f = this.o.limits[0];
                    if (k < f.outerWidth()) {
                        h[0] = false
                    }
                    var f = this.o.limits[1];
                    if (k + g.o.outerWidth() > this.sizes.domWidth - f.outerWidth()) {
                        h[1] = false
                    }
                }
            }
            for (var j = 0; j < h.length; j++) {
                if (h[j]) {
                    this.o.limits[j].fadeIn("fast")
                } else {
                    this.o.limits[j].fadeOut("fast")
                }
            }
        }
    };
    c.prototype.setValue = function () {
        var f = this.getValue();
        this.inputNode.attr("value", f);
        this.onstatechange.call(this, f)
    };
    c.prototype.getValue = function () {
        if (!this.is.init) {
            return false
        }
        var g = this;
        var f = "";
        d.each(this.o.pointers, function (h) {
            if (this.value.prc != undefined && !isNaN(this.value.prc)) {
                f += (h > 0 ? ";" : "") + g.prcToValue(this.value.prc)
            }
        });
        return f
    };
    c.prototype.getPrcValue = function () {
        if (!this.is.init) {
            return false
        }
        var g = this;
        var f = "";
        d.each(this.o.pointers, function (h) {
            if (this.value.prc != undefined && !isNaN(this.value.prc)) {
                f += (h > 0 ? ";" : "") + this.value.prc
            }
        });
        return f
    };
    c.prototype.prcToValue = function (n) {
        if (this.settings.heterogeneity && this.settings.heterogeneity.length > 0) {
            var k = this.settings.heterogeneity;
            var j = 0;
            var m = this.settings.from;
            for (var g = 0; g <= k.length; g++) {
                if (k[g]) {
                    var f = k[g].split("/")
                } else {
                    var f = [100, this.settings.to]
                }
                f[0] = new Number(f[0]);
                f[1] = new Number(f[1]);
                if (n >= j && n <= f[0]) {
                    var l = m + ((n - j) * (f[1] - m)) / (f[0] - j)
                }
                j = f[0];
                m = f[1]
            }
        } else {
            var l = this.settings.from + (n * this.settings.interval) / 100
        }
        return this.round(l)
    };
    c.prototype.valueToPrc = function (l, n) {
        if (this.settings.heterogeneity && this.settings.heterogeneity.length > 0) {
            var k = this.settings.heterogeneity;
            var j = 0;
            var m = this.settings.from;
            for (var g = 0; g <= k.length; g++) {
                if (k[g]) {
                    var f = k[g].split("/")
                } else {
                    var f = [100, this.settings.to]
                }
                f[0] = new Number(f[0]);
                f[1] = new Number(f[1]);
                if (l >= m && l <= f[1]) {
                    var o = n.limits(j + (l - m) * (f[0] - j) / (f[1] - m))
                }
                j = f[0];
                m = f[1]
            }
        } else {
            var o = n.limits((l - this.settings.from) * 100 / this.settings.interval)
        }
        return o
    };
    c.prototype.round = function (f) {
        f = Math.round(f / this.settings.step) * this.settings.step;
        if (this.settings.round) {
            f = Math.round(f * Math.pow(10, this.settings.round)) / Math.pow(10, this.settings.round)
        } else {
            f = Math.round(f)
        }
        return f
    };
    c.prototype.nice = function (f) {
        f = f.toString().replace(/,/gi, ".").replace(/ /gi, "");
        if (d.formatNumber) {
            return d.formatNumber(new Number(f), this.settings.format || {}).replace(/-/gi, "&minus;")
        } else {
            return new Number(f)
        }
    };

    function b() {
        Draggable.apply(this, arguments)
    }
    b.prototype = new Draggable();
    b.prototype.oninit = function (h, g, f) {
        this.uid = g;
        this.parent = f;
        this.value = {};
        this.settings = this.parent.settings
    };
    b.prototype.onmousedown = function (f) {
        this._parent = {
            offset: this.parent.domNode.offset(),
            width: this.parent.domNode.width()
        };
        this.ptr.addDependClass("hover");
        this.setIndexOver()
    };
    b.prototype.onmousemove = function (g, f) {
        var h = this._getPageCoords(g);
        this._set(this.calc(h.x))
    };
    b.prototype.onmouseup = function (f) {
        if (this.parent.settings.callback && d.isFunction(this.parent.settings.callback)) {
            this.parent.settings.callback.call(this.parent, this.parent.getValue())
        }
        this.ptr.removeDependClass("hover")
    };
    b.prototype.setIndexOver = function () {
        this.parent.setPointersIndex(1);
        this.index(2)
    };
    b.prototype.index = function (f) {
        this.ptr.css({
            zIndex: f
        })
    };
    b.prototype.limits = function (f) {
        return this.parent.limits(f, this)
    };
    b.prototype.calc = function (g) {
        var f = this.limits(((g - this._parent.offset.left) * 100) / this._parent.width);
        return f
    };
    b.prototype.set = function (f, g) {
        this.value.origin = this.parent.round(f);
        this._set(this.parent.valueToPrc(f, this), g)
    };
    b.prototype._set = function (g, f) {
        if (!f) {
            this.value.origin = this.parent.prcToValue(g)
        }
        this.value.prc = g;
        this.ptr.css({
            left: g + "%"
        });
        this.parent.redraw(this)
    }
})(jQuery);
(function (b) {
    var a = 0;
    b.popover = function (g, n) {
        var l = b(g),
            i = this;
        var f, o, j, h, d, k, m;
        var e = {
            trigger: "click",
            closeHTML: "",
            appearUnder: g,
            baseClass: "popover",
            extraClass: "",
            escape: true,
            clickOff: true,
            maskOpacity: 0,
            animationSpeed: 370,
            title: l.text(),
            position: "right",
            bottom: "<br/>",
            content: function () {
                var p = l.attr("href");
                return b(p).html()
            },
            onTrigger: function () { },
            onDisplay: function () { },
            onClose: function () { }
        };
        i.settings = {};
        i.init = function () {
            i.settings = b.extend({}, e, n);
            if (n.position != "right" && n.position != "left" && n.position != "top" && n.position != "bottom" && n.position != "nonetop") {
                i.settings.position = e.position
            }
            j = i.settings.baseClass;
            h = "#" + j + "-bg";
            d = b(h);
            k = j + "-active";
            m = i.settings.animationSpeed / 2.5;
            c();
            var p = "." + j + "-wrap a.close";
            p += (i.settings.clickOff) ? ", " + h : "";
            l.bind(i.settings.trigger, function (q) {
                i.settings.onTrigger.call(this);
                i.showPopover();
                q.preventDefault()
            });
            if (i.settings.escape) {
                b("body").keyup(function (r) {
                    if (r.which === 27) {
                        var q = (r.shiftKey) ? m * 7 : m;
                        i.hidePopover(q)
                    }
                })
            }
            b(p).click(function (r) {
                var q = (r.shiftKey) ? m * 7 : m;
                i.hidePopover(q);
                r.preventDefault()
            })
        };
        i.destroy = function () { };
        i.showPopover = function (u) {
            u = typeof (u) != "undefined" ? u : i.settings.animationSpeed;
            i.hidePopover();
            i.settings.onDisplay.call(this);
            d.css("opacity", i.settings.maskOpacity).show();
            l.addClass(k);
            var r = b(i.settings.appearUnder);
            var v = r.offset();
            var t = r.width();
            var p = r.height();
            var s = o.width();
            var q = o.height();
            if (i.settings.position === "right") {
                o.css({
                    top: (v.top + (p / 2) - (q / 2)) + "px",
                    left: (v.left + t + 14) + "px"
                }).fadeIn(u)
            } else {
                if (i.settings.position === "top") {
                    o.css({
                        left: (v.left + (t / 2) - (s / 2)) + "px",
                        top: (v.top - q - 12) + "px"
                    }).fadeIn(u)
                } else {
                    if (i.settings.position === "left") {
                        o.css({
                            top: (v.top + (p / 2) - (q / 2)) + "px",
                            left: (v.left - s - 12) + "px"
                        }).fadeIn(u)
                    } else {
                        if (i.settings.position === "bottom") {
                            o.css({
                                left: (v.left + (t / 2) - (s / 2)) + "px",
                                top: (v.top + p + 12) + "px"
                            }).fadeIn(u)
                        } else {
                            if (i.settings.position === "nonetop") {
                                o.css({
                                    left: (v.left + (t / 2) - (s / 2) + 7.5) + "px",
                                    top: (v.top - q - 1) + "px"
                                }).fadeIn(u)
                            }
                        }
                    }
                }
            }
        };
        i.hidePopover = function (p) {
            p = typeof (p) != "undefined" ? p : m;
            i.settings.onClose.call(this);
            d.hide();
            b("." + k).removeClass(k);
            f.fadeOut(p)
        };
        var c = function () {
            if (!d.length) {
                b("body").append('<div id="' + j + '-bg" />')
            }
            d = b(h);
            var p = '<div class="' + j + '-wrap"><div class="' + j + " " + i.settings.position + " " + i.settings.extraClass + '">';
            if (i.settings.title !== "") {
                p += '<h3 class="' + j + '-title">' + i.settings.title + "</h3>"
            }
            p += '<div class="' + j + '-content" />';
            p += '<a href="#" class="close">';
            if (i.settings.closeHTML !== "") {
                p += '<img src="http://cdn.goeuro.comimages/clear.png">'
            }
            p += "</a>";
            if (i.settings.bottom !== "") {
                p += '<br class="clear" /></div></div>'
            }
            b("body").append(p);
            f = b("." + j + "-wrap");
            o = b(f[a]);
            o.find("." + j + "-content").html(i.settings.content())
        };
        i.init()
    };
    b.fn.popover = function (c) {
        return this.each(function () {
            if (!b(this).data("popover")) {
                var d = new b.popover(this, c);
                b(this).data("popover", d);
                a++
            }
        })
    }
})(jQuery);

(function (b) {
    var a = {
        init: function (c) {
            var e = {
                fit: false,
                before: function (f) { },
                complete: function (f) { }
            },
                d = b.extend({}, e, c);
            return this.each(function () {
                var g = b(this),
                    f = g.find("img");
                if (typeof d.before === "function") {
                    d.before(g)
                }
                f.one("load", function () {
                    var j = b(this);
                    j.removeAttr("width height").css({
                        width: "",
                        height: "",
                        "margin-left": "",
                        "margin-top": ""
                    });
                    var h = j.width(),
                        o = j.height(),
                        m = g.innerWidth(),
                        p = g.innerHeight(),
                        n, k, l, i;
                    if (d.fit) {
                        if (h >= m && o >= p) {
                            n = 1 / (h / m);
                            k = 1 / (o / p)
                        } else {
                            n = m / h;
                            k = p / o
                        }
                        if (n < k) {
                            h = k * h;
                            o = k * o
                        } else {
                            h = n * h;
                            o = n * o
                        }
                        if (h > m) {
                            i = (m - h) / 2;
                            j.css("margin-left", i + "px")
                        } else {
                            l = (p - o) / 2;
                            j.css("margin-top", l + "px")
                        }
                    } else {
                        n = m / h;
                        k = p / o;
                        if (n > k) {
                            h = k * h;
                            o = k * o
                        } else {
                            h = n * h;
                            o = n * o
                        }
                        if (h < m) {
                            i = (m - h) / 2;
                            j.css("margin-left", i + "px")
                        } else {
                            l = (p - o) / 2;
                            j.css("margin-top", l + "px")
                        }
                    }
                    j.width(h);
                    j.height(o);
                    if (typeof d.complete === "function") {
                        d.complete(g)
                    }
                }).each(function () {
                    if (this.complete) {
                        f.load()
                    }
                })
            })
        }
    };
    b.fn.fitc = function (c) {
        if (a[c]) {
            return a[c].apply(this, Array.prototype.slice.call(arguments, 1))
        } else {
            if (typeof c === "object" || !c) {
                return a.init.apply(this, arguments)
            } else {
                b.error("Method " + c + " does not exist on jQuery.fitc")
            }
        }
    }
})(jQuery);

var ONE_DAY = 86400000;
var locales = {
    en: {
        verify: "Verify",
        person: "Person",
        persons: "Persons",
        noResults: {
            carrental: "Sorry! No rental cars were found. Try modifying your search options.",
            flight: "Sorry!<br/>No air results can be found. This is probably because your search was:<br />1. On a route where our air partners haven’t made seats available for these dates yet<br />2. Between two places that aren't able to be connected by air<br /><br />Think we missed a connection? Yes, it happens sometimes! Then please let us know at <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a>",
            200: "Sorry! We are not able to find a <%= travelmode %> between these two places.",
            201: "Sorry! We are not yet fully available in <%= country %>. Try <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: 'Sorry! We are not yet fully available in <%= country %>. Follow us on <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> for information on country coverage.',
            210: 'Sorry! We are not yet fully available in <%= country %>. Follow us on <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> for information on country coverage.',
            211: "Sorry! We are not able to find a <%= travelmode %> between these two places.",
            299: "Sorry! We are not yet fully available in Italy. Try <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> or <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Sorry, but we cannot find your location.",
        suggestNullJourney: "Please enter two different locations.",
        sameDayNotice: "Hey day tripper! Are you sure you want a ticket to depart and arrive on the same date?",
        compareDates: "Given Date is not valid",
        travelAgeCar: "Must be at least 20 years old to rent a car",
        resultRouteTextShow: "Open",
        resultRouteTextClose: "Close",
        "resultRouteTextShow.afterSales": "Show details",
        "resultRouteTextClose.afterSales": "Hide details",
        noConnection: {
            100: "Search not possible",
            101: "Leaving from a location outside of Europe",
            102: "Going to a location outside of Europe",
            103: "Trip is not inside Europe",
            110: "The distance between these two locations is too close to fly.",
            120: "Please note: Some trains and buses can only be booked 90 days in advance.",
            140: "Due to Christmas Holidays, no trains are available from the 25-26 of December. Please try alternative travel dates or check bus and air options.",
            "140.short": "No trains available due to christmas holidays.",
            150: "Thank you for searching on GoEuro.it! We’re currently working on integrating search results from Trenitalia. Until then, please use <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a> to book your tickets. Have a good trip!",
            "150.short": "No trains available"
        },
        share: {
            fb: {
                description: "The best travel options for Europe. Compare. Combine. Go.",
                caption: "Shared by GoEuro.com",
                name: "Found on GoEuro: <%= mode %> from <%= from %> to <%= to %> in <%= date %> for just <%= price %> <%= currency %> round-trip.",
                nameOneWay: "Found on GoEuro: <%= mode %> from <%= from %> to <%= to %> in <%= date %> for just <%= price %> <%= currency %>."
            }
        }
    },
    de: {
        verify: "Überprüfen",
        person: "Person",
        persons: "Personen",
        noResults: {
            carrental: "Verzeihung! Wir konnten für Ihre Suchanfrage keine Ergebnisse finden. Bitte ändern Sie Ihre Suchkriterien und versuchen Sie es erneut.",
            flight: "Entschuldigen Sie!<br/>Es wurden keine Flugverbindungen gefunden. Dies ist vermutlich auf eine der folgenden Ursachen zurückzuführen:<br />1. Unsere Fluganbieter bieten derzeit leider keine Buchungsoptionen für diesen Zeitraum an. <br />2. Zwischen diesen beiden Orten existiert keine Flugverbindung.<br /><br />Fehlt uns eine Verbindung? Das passiert leider. Wenn dem so ist, teilen Sie uns das bitte über <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a> mit.",
            200: "Entschuldigen Sie! Es ist uns leider nicht möglich eine <%= travelmode %>verbindung zwischen diesen zwei Orten ausfindig zu machen.",
            201: "Entschuldigen Sie! In <%= country %> haben wir noch keine volle Abdeckung. Bitte nutzen Sie das Angebot von <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: 'Entschuldigen Sie! In <%= country %> haben wir noch keine volle Abdeckung. Folgen Sie uns auf <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a>, um Informationen über unsere Länderabdeckung zu erhalten.',
            210: 'Entschuldigen Sie! In <%= country %> haben wir noch keine volle Abdeckung. Folgen Sie uns auf <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a>, um Informationen über unsere Länderabdeckung zu erhalten.',
            211: "Entschuldigen Sie! Es ist uns leider nicht möglich eine <%= travelmode %>verbindung zwischen diesen zwei Orten ausfindig zu machen.",
            299: "Entschuldigen Sie! In Italy haben wir noch keine volle Abdeckung. Bitte nutzen Sie das Angebot von <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> oder <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Leider konnten wir den eingegebenen Ort nicht finden.",
        suggestNullJourney: "Bitte wählen Sie zwei verschiedene Orte aus.",
        sameDayNotice: "Sind Sie sicher, dass Sie am selben Tag zurückfahren wollen?",
        compareDates: "Das Datum ist nicht korrekt oder liegt in der Vergangenheit",
        travelAgeCar: "Altersangabe nicht korrekt. Das Mindestalter beträgt 20 Jahre.",
        resultRouteTextShow: "Anzeigen",
        resultRouteTextClose: "Schließen",
        "resultRouteTextShow.afterSales": "Mehr Details",
        "resultRouteTextClose.afterSales": "Weniger Details",
        noConnection: {
            100: "Suche nicht möglich",
            101: "Abfahrtsort liegt außerhalb Europas",
            102: "Ankunftsort liegt außerhalb Europas",
            103: "Reise ist außerhalb Europas",
            110: "Diese beiden Orte liegen zu nah beieinander, um zu fliegen.",
            120: "Bitte beachten: Busse und Züge sind häufig nur bis zu 90 Tage im Voraus buchbar.",
            140: "Wegen Weihnachten ist der Bahnbetrieb in Großbritannien am 25. und 26. Dezember weitgehend eingestellt. Bitte ändern Sie, wenn möglich, Ihr Reisedatum oder weichen Sie auf Flugzeug oder Bus aus.",
            "140.short": "Aufgrund der Weihnachtsfeiertage verkehren leider keine Züge.",
            150: "Danke, dass Sie GoEuro.it für Ihre Suche nutzen! Wir arbeiten gerade daran, die Suchergebnisse von Trenitalia zu integrieren. Bis dahin nutzen Sie bitte <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a> um Ihre Zugtickets zu buchen. Wir wünschen Ihnen eine angenehme Reise!",
            "150.short": "Keine Zugverbindung verfügbar."
        },
        share: {
            fb: {
                description: "Die besten Verbindungen für Europa. Compare. Combine. Go.",
                caption: "Shared by GoEuro.de",
                name: "Auf GoEuro gefunden: <%= mode %> von <%= from %> nach <%= to %> im <%= date %> für <%= price %> <%= currency %> hin und zurück.",
                nameOneWay: "Auf GoEuro gefunden: <%= mode %> von <%= from %> nach <%= to %> im <%= date %> für nur <%= price %> <%= currency %>."
            }
        }
    },
    es: {
        verify: "Comprobar",
        person: "Persona",
        persons: "Personas",
        timeoutMsg: "Lo sentimos, se ha agotado el tiempo para la búsqueda. Por favor, vuelva a intentarlo",
        searchAgain: "Buscar otra vez",
        toHome: "Página de inicio",
        noResults: {
            carrental: "¡Lo sentimos! No se ha encontrado ningún coche de alquiler. Por favor prueba a modificar tus opciones de búsqueda.",
            flight: "¡Lo sentimos!<br/>No se encontraron resultados de vuelos.  Esto puede ser debido a que la búsqueda que realizó está: <br />1. En una ruta en la que las compañías ferroviarias asociadas aun no han facilitado asientos disponibles para estas fechas. <br />2. Entre dos lugares que no pueden ser conectados por aire. <br /><br />¿Crees que nos falta una conexión? Sí, pasa a veces. En ese caso, por favor háznoslo saber <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a>",
            200: "¡Lo sentimos!, no hemos podido encontrar ningún <%= travelmode %> entre estos dos destinos",
            201: "¡Lo sentimos!, nuestra cobertura en <%= country %> aún no es completa. Puedes utilizar <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: '¡Lo sentimos!, nuestra cobertura en <%= country %> aún no es completa. Síguenos en <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> para más información sobre los países que cubrimos.',
            210: '¡Lo sentimos!, nuestra cobertura en <%= country %> aún no es completa. Síguenos en <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> para más información sobre los países que cubrimos.',
            211: "¡Lo sentimos!, no hemos podido encontrar ningún <%= travelmode %> entre estos dos destinos",
            299: "¡Lo sentimos!, nuestra cobertura en Italia aún no es completa. Puedes utilizar <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> o <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Lo sentimos, pero no encontramos el destino que has introducido.",
        suggestNullJourney: "Por favor, introduce dos destinos diferentes.",
        sameDayNotice: "¡Ey, fiera! ¿Estás seguro de que quieres realizar una búsqueda de ida y vuelta para el mismo día?",
        compareDates: "Dicha fecha no es válida",
        travelAgeCar: "Deberás tener al menos 20 años para alquilar un coche",
        resultRouteTextShow: "Mostrar",
        resultRouteTextClose: "Ocultar",
        "resultRouteTextShow.afterSales": "Mostrar detalles",
        "resultRouteTextClose.afterSales": "Ocultar detalles",
        noConnection: {
            100: "Búsqueda sin resultados",
            101: "Saliendo desde una localización fuera de Europa",
            102: "Llegando a una localización fuera de Europa",
            103: "El viaje no es dentro de Europa",
            110: "La distancia comprendida entre las dos localizaciones es demasiado corta para viajar.",
            120: "Nótese que algunos trenes y autobuses sólo podrán reservarse con 90 días de antelación.",
            140: "Debido a las fiestas de Navidad no hay trenes disponibles los días 25 y 26 de Diciembre. Por favor, inténtelo de nuevo con otras fechas o bien seleccione un billete de bus o avión.",
            "140.short": "Debido a las fiestas de Navidad no hay trenes disponibles.",
            150: "Gracias por buscar en GoEuro.it! Actualmente estamos trabajando en la integración de los resultados de búsqueda de Trenitalia. Hasta entonces, por favor, utiliza <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a> para reservar tus billetes. ¡Qué tengas un buen viaje!",
            "150.short": "No hay opciones de tren disponibles"
        },
        share: {
            fb: {
                description: "Las mejores opciones de viaje para Europa. Compare. Combine. Go.",
                caption: " Compartido por GoEuro.com",
                name: "Encontrado en GoEuro <%= mode %> desde <%= from %> a <%= to %> en <%= date %> por sólo <%= price %> <%= currency %> viaje de ida y vuelta.",
                nameOneWay: "Encontrado en GoEuro: <%= mode %> desde <%= from %> a <%= to %> en <%= date %> por solo <%= price %> <%= currency %>."
            }
        }
    },
    it: {
        verify: "Verifica",
        person: "Persona",
        persons: "Persone",
        timeoutMsg: "Spiacenti, la sessione di ricerca è scaduta. Per favore, inizia una nuova ricerca",
        searchAgain: "Cerca ancora",
        toHome: "Pagina principale",
        noResults: {
            carrental: "Siamo spiacenti, la tua richiesta non ha prodotto risultati. Per favore, prova con altri criteri di ricerca.",
            flight: "Siamo spiacenti, <br/>Non è stato possibile trovare risultati corrispondenti alla ricerca.  Il problema può essere dovuto al fatto che: <br />1. Non ci sono treni disponibili per la data selezionata <br />2. Non sono disponibili collegamenti aerei tra le due località selezionate. <br /><br />Credi che manchi qualche collegamento? Si, a volte può succedere. In tal caso, per favore segnalacelo scrivendo a <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a>",
            200: "Siamo spiacenti, non abbiamo trovato nessun <%= travelmode %> tra le due localitá",
            201: "Siamo spiacenti, la nostra copertura in <%= country %> non è ancora completa. In ogni caso, puoi utilizzare <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: 'Siamo spiacenti, la nostra copertura in  <%= country %> non è ancora completa. Seguici su <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> per conoscere tutte le novità e ricevere maggiori informazioni.',
            210: 'Siamo spiacenti, la nostra copertura in  <%= country %> non è ancora completa. Seguici su <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> per conoscere tutte le novità e ricevere maggiori informazioni.',
            211: "Siamo spiacenti, non abbiamo trovato nessun <%= travelmode %> tra le due località",
            299: "Siamo spiacenti, la nostra copertura in Italia non è ancora completa. In ogni caso, puoi utilizzare <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> o <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Spiacenti, non troviamo la località che hai introdotto.",
        suggestNullJourney: "Per favore, inserisci due località differenti.",
        sameDayNotice: "Sei sicuro di voler effettuare un viaggio di andata e ritorno nello stesso giorno?",
        compareDates: "La data selezionata non è valida",
        travelAgeCar: "Il noleggio di un'auto è consentito alle persone con un'età di almeno venti anni",
        resultRouteTextShow: "Mostra",
        resultRouteTextClose: "Nascondi",
        "resultRouteTextShow.afterSales": "Mostra dettagli",
        "resultRouteTextClose.afterSales": "Nascondi dettagli",
        noConnection: {
            100: "Ricerca senza risultati",
            101: "La località di partenza è fuori dall'Europa",
            102: "La località di arrivo è fuori dall'Europa",
            103: "Il viaggio selezionato è fuori dall'Europa",
            110: "La distanza tra le due località è troppo corta.",
            120: "N.B., alcuni treni e autobus possono essere prenotati solamente 90 giorni prima della partenza.",
            140: "Causa festività natalizie, non ci sono treni disponibili il 25 e il 26 dicembre. Si prega di provare con date alternative o altre opzioni di viaggio.",
            "140.short": "Nessun treno disponibile, causa festività natalizie.",
            150: "Grazie per aver utilizzato GoEuro.it! Attualmente stiamo lavorando per integrare le soluzioni di viaggio di Trenitalia. Per il momento è possibile prenotare i biglietti ferroviari solo su <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a>. Vi auguriamo buon viaggio!",
            "150.short": "Nessun treno disponibile"
        },
        share: {
            fb: {
                description: "Tutti i modi migliori di viaggiare in Europa. Compare. Combine. Go.",
                caption: " Pubblicato da GoEuro.com",
                name: "Trovato su GoEuro <%= mode %> da <%= from %> a <%= to %> il <%= date %> per solamente <%= price %> <%= currency %>  viaggio di andata e ritorno.",
                nameOneWay: "Trovato su GoEuro: <%= mode %> da <%= from %> a <%= to %> il <%= date %> per solamente <%= price %> <%= currency %>."
            }
        }
    },
    fr: {
        verify: "Vérifier",
        person: "Personne",
        persons: "Personnes",
        noResults: {
            carrental: "Désolé ! Aucun résultat n'a été trouvé. Veuillez modifier vos critères de recherche s'il vous plait.",
            flight: "Désolé !<br/>Pas de résultat d'avion disponible. Il est probable que votre recherche soit :<br />1. Sur une route pour laquelle nos partenaires n'ont plus de place disponible pour ces dates<br />2. Entre deux locations ne pouvant pas être reliées par avion<br /><br />Vous pensez qu'il nous manque une connextion ? Oui, cela arrive parfois ! Veuillez nous le signaler à <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a>",
            200: "Désolé ! Nous ne sommes pas en mesure de trouver un <%= travelmode %> entre ces deux emplacements.",
            201: "Désolé ! Nous ne sommes pas encore complètement disponibles en <%= country %>. Essayez <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: 'Désolé ! Nous ne sommes pas encore complètement disponibles en <%= country %>. Suivez nous sur <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> pour plus d\'informations sur notre couverture de territoire.',
            210: 'Désolé ! Nous ne sommes pas encore complètement disponibles en <%= country %>. Suivez nous sur <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> pour plus d\'informations sur notre couverture de territoire.',
            211: "Désolé ! Nous ne sommes pas en mesure de trouver un <%= travelmode %> entre ces deux emplacements.",
            299: "Désolé ! Nous ne sommes pas encore complètement disponibles en Italie. Essayez <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> ou <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Désolé, nous ne trouvons pas cet emplacement.",
        suggestNullJourney: "Veuillez entrer deux villes différentes.",
        sameDayNotice: "Êtes vous sûrs de sélectionner un voyage ayant les mêmes dates d'arrivée et de départ ?",
        compareDates: "La date entrée n'est pas valide",
        travelAgeCar: "Le conducteur doit au moins avoir 20ans pour louer une voiture",
        resultRouteTextShow: "Ouvrir",
        resultRouteTextClose: "Fermer",
        "resultRouteTextShow.afterSales": "Montrer l'itinéraire",
        "resultRouteTextClose.afterSales": "Cacher l'itinéraire",
        noConnection: {
            100: "Recherche impossible",
            101: "Au départ d'un emplacement en dehors de l'Europe",
            102: "Arrivée à un emplacement en dehors de l'Europe",
            103: "Le voyage n'est pas situé en Europe",
            110: "La distance entre les deux emplacements est trop courte pour voler.",
            120: "Veuillez noter que certains trains et bus ne peuvent seulement être réservés que 90 jours en avance.",
            140: "Pour cause de vacances de Noël, aucun train n'est disponible pour les 25 et 26 Décembre. Veuillez considérer d'autres dates ou vous reporter sur les avions et les bus.",
            "140.short": "Aucun train disponible pour les fêtes de Noël.",
            150: "Merci pour vos recherches sur GoEuro.it! Nous travaillons actuellement afin d’intégrer les résultats de recherche de Trenitalia. Désormais, veuillez utiliser <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a> pour réserver vos billets. Bon voyage!",
            "150.short": "Pas de train disponible"
        },
        share: {
            fb: {
                description: "Les meilleures comparaisons de voyages en Europe. Compare. Combine. Go.",
                caption: "Partagé par GoEuro.com",
                name: "Trouvé sur GoEuro: <%= mode %> de <%= from %> à <%= to %> le <%= date %> pour juste <%= price %> <%= currency %> aller-retour.",
                nameOneWay: "Trouvé sur GoEuro: <%= mode %> de <%= from %> à <%= to %> le <%= date %> pour just <%= price %> <%= currency %>."
            }
        }
    },
    nl: {
        verify: "Controleren",
        person: "Persoon",
        persons: "Personen",
        noResults: {
            carrental: "Sorry! Er zijn geen huurauto’s gevonden.  Probeer uw zoekopties te wijzigen.",
            flight: "Sorry!<br/>Er zijn geen vluchten gevonden. Dit komt vermoedelijk doordat uw zoekopdracht:<br />1. Een route betreft waarop onze partners nog geen stoelen beschikbaar heeft voor deze data<br />2. Zich bevindt tussen twee plaatsen zonder vluchtverbindingen.<br /><br />Hebben wij een verbinding gemist? Dat kann gebeuren! Laat het ons weten op  <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a>",
            200: "Sorry! We hebben geen  <%= travelmode %>  tussen deze twee locaties kunnen vinden.",
            201: "Sorry! We zijn nog niet optimaal beschikbaar in <%= country %>. Probeer <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: 'Sorry! We zijn nog niet optimaal beschikbaar in <%= country %>. Volg ons op <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> voor informatie over onze landendekking.',
            210: 'Sorry! We zijn nog niet optimaal beschikbaar in  <%= country %>. Volg ons op <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> voor informatie over onze landendekking.',
            211: "Sorry! We hebben geen <%= travelmode %> tussen deze twee locaties kunnen vinden.",
            299: "Sorry! We zijn nog niet optimaal beschikbaar in Italië. Probeer <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> of <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Sorry, we hebben uw locatie niet kunnen vinden.",
        suggestNullJourney: "Vul twee verschillende locaties in a.u.b.",
        sameDayNotice: "Hey dagtrip-liefhebber! Weet je zeker dat vertrekken en terugkomen wilt op dezelfde dag? ",
        compareDates: "Opgegeven datum is niet geldig",
        travelAgeCar: "Bestuurder dient minstens 20 jaar te zijn voor het huren van een auto",
        resultRouteTextShow: "Toon",
        resultRouteTextClose: "Verberg",
        "resultRouteTextShow.afterSales": "Details weergeven",
        "resultRouteTextClose.afterSales": "Details verbergen",
        noConnection: {
            100: "Zoekopdracht niet mogelijk",
            101: "Vertrek vanuit een locatie buiten Europa",
            102: "Aankomst in een locatie buiten Europa",
            103: "Reis is niet binnen Europa",
            110: "De afstand tussen deze twee locaties is te klein om te vliegen.",
            120: "Houdt er rekening mee dat sommige treinen en bussen alleen 90 dagen van te voren geboekt kunnen worden.",
            140: "Als gevolg van de Kerstdagen zijn er geen treinen beschikbaar op 25-26 December. Probeer alternatieve reisdagen of controleer bus- en vliegtopties.",
            "140.short": "Geen treinen beschikbaar als gevolg van kerstdagen.",
            150: "Bedankt voor het zoeken op GoEuro.it! Op dit moment zijn wij bezig om zoekresultaten van Trenitalia te integreren. Gebruik tot die tijd alstublieft <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a> om uw treintickets te boeken. Goede reis!",
            "150.short": "Geen treinen beschikbaar"
        },
        share: {
            fb: {
                description: "De beste reisopties voor Europa. Vergelijk. Cobineer. Ga.",
                caption: "Gedeeld door GoEuro.com",
                name: "Gevonden op GoEuro: <%= mode %> van <%= from %> naar <%= to %> op <%= date %> voor maar <%= price %> <%= currency %> heen-en terugreis.",
                nameOneWay: "Gevonden op GoEuro: <%= mode %> van <%= from %> naar <%= to %> op <%= date %> voor maar <%= price %> <%= currency %>."
            }
        }
    },
    ca: {
        verify: "Comprovar",
        person: "Persona",
        persons: "Persones",
        timeoutMsg: "Ho sentim, s’ha exhaurit el temps de cerca. Si us plau, torna a provar-ho",
        searchAgain: "Cerca un altre cop",
        toHome: "Pàgina d’inici",
        noResults: {
            carrental: "Ho sentim! No s’ha trobat cap cotxe de lloguer. Si us plau, prova de modificar les teves opcions de cerca.",
            flight: "Ho sentim!<br/>No s’han trobat resultats de vols. Això pot ser perquè la cerca que has realitzat està: <br />1. En una ruta en la qual les companyies ferroviàries associades encara no han facilitat seients disponibles per a aquestes dates. <br />2. Entre dues destinacions que no poden ser conectades per aire. <br /><br />Creus que ens falta alguna connexió? Sí, de vegades passa. En aquest cas, si us plau fes-nos-ho saber a <a href='mailto:contact@goeuro.com'>contact@goeuro.com</a>",
            200: "Ho sentim!, no hem pogut trobar cap <%= travelmode %> entre ambdues destinacions",
            201: "Ho sentim!, la nostra cobertura a <%= country %> encara no és completa. Pots utilizar <a href=\"<%= link %>\" target='_blank'><%= operator %></a>.",
            202: 'Ho sentim!, la nostra cobertura a <%= country %> encara no és completa. Segueix-nos al <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> per a més informació sobre els països que cobrim.',
            210: 'Ho sentim!, la nostra cobertura a <%= country %> encara no és completa. Segueix-nos al <a href="https://www.facebook.com/GoEuro" target="_blank">Facebook</a> per a més informació sobre els països que cobrim.',
            211: "Ho sentim!, no hem pogut trobar cap <%= travelmode %> entre ambdues destinacions",
            299: "Ho sentim!, la nostra cobertura a Itàlia encara no és completa. Pots utilizar <a href=\"http://trenitalia.com\" target='_blank'>Trenitalia</a> o bé <a href=\"http://www.italotreno.it\" target='_blank'>Italo</a>."
        },
        suggestError: "Ho sentim, però no trobem la destinació que has introduït.",
        suggestNullJourney: "Si us plau, introdueix dues destinacions diferents.",
        sameDayNotice: "Ostres! Estàs segur que vols realitzar una cerca d’anada i tornada per al mateix dia?",
        compareDates: "Aquesta data no és vàlida",
        travelAgeCar: "Has de ser major de 20 anys per poder llogar un cotxe",
        resultRouteTextShow: "Mostrar ruta",
        resultRouteTextClose: "Ocultar ruta",
        "resultRouteTextShow.afterSales": "Mostrar detalls",
        "resultRouteTextClose.afterSales": "Ocultar detalls",
        noConnection: {
            100: "Cerca sense resultats",
            101: "Sortint des d’una localització fora d’Europa",
            102: "Arribant a una localització fora d’Europa",
            103: "El viatge no és a dins d’Europa",
            110: "La distància compresa entre ambdues localitzacions és massa curta per viatjar.",
            120: "Tingues en compte que alguns trens i autobusos només es podran reservar amb 90 dies d’antelació.",
            140: "A causa de les festes de Nadal no hi ha trens disponibles els dies 25 i 26 de desembre. Si us plau, prova-ho de nou amb altres dates, o bé selecciona un bitllet d’autobús o d’avió.",
            "140.short": "A causa de les festes de Nadal no hi ha trens disponibles.",
            150: "Gràcies per cercar a GoEuro.it! Actualment estem treballant en la integració dels resultats de cerca de Trenitalia. Fins llavors, si us plau, utilitza <a href=\"http://trenitalia.it\" target='_blank'>Trenitalia.it</a> per reservar els teus bitllets. Que tinguis un bon viatge!",
            "150.short": "No hi ha trens disponibles"
        },
        share: {
            fb: {
                description: "Les millors opcions de viatge per a Europa. Compare. Combine. Go.",
                caption: " Compartit per GoEuro.com",
                name: "Trobat a GoEuro <%= mode %> de <%= from %> a <%= to %> en <%= date %> per només <%= price %> <%= currency %> viatge d’anada i tornada.",
                nameOneWay: "Trobat a GoEuro: <%= mode %> de <%= from %> a <%= to %> en <%= date %> per només <%= price %> <%= currency %>."
            }
        }
    }
};
var overloadCountdownTime, overloadCounterInterval;
var isMobile = navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone|IEMobile|Opera Mobi|Symb[ian|OS]+)/i);
var isAndroid = navigator.userAgent.match(/Android/i);
var isIphone = navigator.userAgent.match(/iPhone|iPad|iPod/i);
 
var $bonusCardTrigger = $(".homepage-content #filters, #results-search-form-more");
var ref_search;
var cached_flights_results = [];
var cached_trains_results = [];
var cached_buses_results = [];
var sug_addr1 = "";
var sug_addr2 = "";
var ajaxObj;
var mapObjectsAjax;
var map;
var expandedMap;
var directionsDisplay;
var mapMarkers;
var infowindow;
var current_type = "";
var current_id = 0;
var summaryTableReq = "";
var autocompleteCache = {};
var resultsLoaded = {};
var firstTriggerBonusCards = 1;
var popupOver = {};
var resultDetails = {};
var enableFlightTabPopup = true;
var sortVal = "price";
var lastSorting = {
    flight: sortVal,
    train: sortVal,
    bus: sortVal
};
var journey = {
    icons: {
        from: 1,
        to: 2
    },
    from: null,
    to: null,
    poly: null,
    airports: {
        from: [],
        to: []
    },
    stations: {
        from: [],
        to: []
    }
};
var directCar = {
    request: true,
    load: function () {
        $.ajax({
            type: "GET",
            url: "/search/" + $("#travel-search-results").data("searchid") + "/directcar",
            success: function (a) {
                if (a) {
                    $(".bar-directcar").popover({
                        closeHTML: "",
                        extraClass: "directcar",
                        title: "",
                        position: "top",
                        bottom: "",
                        content: function () {
                            return a
                        }
                    })
                }
            }
        })
    },
    reposition: function () {
        var c = $(".bar-directcar"),
            a = $(".directcar.popover").last(),
            d = c.offset(),
            b = a.parent(".popover-wrap");
        if (undefined !== d) {
            b.css({
                left: ((d.left + c.width() / 2) - a.width() / 2 - 0.5),
                top: d.top - b.height() - 6,
                display: "block"
            });
            $("#popover-bg").css({
                display: "block"
            })
        }
    }
};
var overloadCountdownTimer = function overloadCountdownTimerFn() {
    var b = 30,
        a = $(".overload-modal__countdown"),
        d = function c(f) {
            if (f < 10) {
                a.first().text("0");
                a.last().text(f)
            } else {
                var e = f.toString().split("", -1);
                a.first().text(e[0]);
                a.last().text(e[1])
            }
        };
    overloadCountdownTime = b;
    d(overloadCountdownTime);
    clearInterval(overloadCounterInterval);
    overloadCounterInterval = setInterval(function () {
        d(--overloadCountdownTime);
        if (overloadCountdownTime === 1) {
            $.get("/request-entry", function (e) {
                $("#tour, #tour-blocker").hide();
                clearInterval(overloadCounterInterval)
            }).error(function (e) {
                if (e.status === 509) {
                    overloadCountdownTime = b + 1
                }
            })
        }
    }, 1000)
};
var showAndroidOverlay = function showAndroidOverlayFn() {
    var a = ["de", "es", "uk", "com", "fr", "nl", "lu", "be", "it"];
    if ((_.contains(a, domain) && location.pathname === "/" && location.search === "" && (isIphone || isAndroid))) {
        $(".frontpage-mobile-overlay").show();
        isAndroid && $(".overload-modal__slide-android").addClass("active");
        isIphone && $(".overload-modal__slide-ios").addClass("active");
        $(".overload-modal--dismiss").on("click", function () {
            $("#tour-blocker, #tour").hide()
        })
    }
};

function updateUrlToDomain(a, c) {
    if (!/localhost/.test(location.host)) {
        var e = location.host.split(".goeuro")[0].split("."),
            d = _.last(e),
            g = ["www", "fr"],
            b = ["search", "book", "cars"],
            f = a;
        if (_.contains(g, d)) {
            if (c) {
                e[e.length - 1] = c
            } else {
                if (d !== "www" && e.length > 1) {
                    e.pop()
                } else {
                    e[e.length - 1] = "www"
                }
            }
        } else {
            if (c) {
                e.push(c)
            }
        }
        if (/.+_.+/.test(a)) {
            f = a.split("_")[1].toLowerCase();
            f === "gb" && (f = "co.uk");
            f === "us" && (f = "com")
        }
        location.href = location.protocol + "//" + e.join(".") + ".goeuro." + f + (_.contains(b, location.pathname.split("/")[1]) ? location.pathname : "/")
    } else {
        location.href = location.href
    }
}
var focusForm = function (a) {
    if (a == "A") {
        $("#to_filter").focus()
    } else {
        if (a == "AtoB") {
            $("#departure_date").focus()
        } else {
            $("#from_filter").focus()
        }
    }
    window.scrollTo(0, 0)
};

function drawMapsForPhantom() {
    var a = $(".phantom-map");
    a.each(function (k, c) {
        if (c.getAttribute("datmap-marker-departs")) {
            var l = c.getAttribute("data-type"),
                g = JSON.parse(c.getAttribute("datmap-marker-departs")),
                e = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [{
                        featureType: "water",
                        stylers: [{
                            visibility: "on"
                        }, {
                            color: "#acbcc9"
                        }]
                    }, {
                        featureType: "landscape",
                        stylers: [{
                            color: "#f2e5d4"
                        }]
                    }, {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [{
                            color: "#c5c6c6"
                        }]
                    }, {
                        featureType: "road.arterial",
                        elementType: "geometry",
                        stylers: [{
                            color: "#e4d7c6"
                        }]
                    }, {
                        featureType: "road.local",
                        elementType: "geometry",
                        stylers: [{
                            color: "#fbfaf7"
                        }]
                    }, {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [{
                            color: "#c5dac6"
                        }]
                    }, {
                        featureType: "administrative",
                        stylers: [{
                            visibility: "on"
                        }, {
                            lightness: 33
                        }]
                    }, {
                        featureType: "road"
                    }, {
                        featureType: "poi.park",
                        elementType: "labels",
                        stylers: [{
                            visibility: "on"
                        }, {
                            lightness: 20
                        }]
                    }, {
                        featureType: "road",
                        stylers: [{
                            lightness: 20
                        }]
                    }]
                };
            if (l === "station") {
                $.extend(e, {
                    center: new google.maps.LatLng(g.geo_position.latitude, g.geo_position.longitude),
                    zoom: 15
                });
                new google.maps.Map(c, e)
            } else {
                var b = new google.maps.LatLngBounds();
                var j = [];
                if (l === "header") {
                    var h = 0;
                    _.each(g, function (i) {
                        if (i.geo_position.latitude && i.geo_position.longitude) {
                            j.push(new google.maps.LatLng(i.geo_position.latitude, i.geo_position.longitude));
                            if (h === 0 || h === g.length - 1) {
                                b.extend(j[h])
                            }
                            h++
                        }
                    })
                } else {
                    _.each(g, function (m, n) {
                        if (m.geo_position.latitude && m.geo_position.longitude) {
                            j.push(new google.maps.LatLng(m.geo_position.latitude, m.geo_position.longitude));
                            if (n === 0 || n === g.length - 1) {
                                b.extend(_.last(j))
                            }
                        }
                    })
                }
                $.extend(e, {
                    bounds: b,
                    center: b.getCenter(),
                    zoom: 4
                });
                var d = new google.maps.Map(c, e);
                for (var h = 0; h < j.length; h++) {
                    new google.maps.Marker({
                        position: j[h],
                        map: d,
                        icon: {
                            url: "data:image/gif;base64,R0lGODlhCgALAIQbAGyn0G+p0XWs03mu1H2x1X6x1o621ZK41pS/3ZjB3pzE35/F4KbK46fK46jL477Z6sDZ68Tc7MXc7Mjd7tjo8t3r9ODt9uLu9vb6/ff6/fr8/v///////////////////yH5BAEKAB8ALAAAAAAKAAsAAAVH4Cc+hvGIIjUALDBQogAk05QAwtcAi+ZrCwCDALj8NBcAIQDAHDOAQAFgOVoABQdAcVQAHJ8VAgJBuESVaatQQX0ih4MEFQIAOw==",
                            anchor: new google.maps.Point(5, 5)
                        }
                    })
                }
                if (l === "journey") {
                    var f = new google.maps.Polyline({
                        path: j,
                        geodesic: true,
                        strokeOpacity: 1,
                        strokeWeight: 2,
                        strokeColor: "#f89406"
                    });
                    f.setMap(d)
                }
            }
        }
    })
}
var scrollToElement = function (b) {
    var a = document.getElementById(b).offsetTop;
    $(window).scrollTop(a)
};

function setFromMarker(d, a, e) {
    var c = new google.maps.MarkerImage("images/map-marker-depart.png", new google.maps.Size(45, 37), new google.maps.Point(0, 0), new google.maps.Point(12, 36));
    if (null !== journey.from) {
        journey.from.setPosition(new google.maps.LatLng(d, a))
    } else {
        journey.from = new google.maps.Marker({
            position: new google.maps.LatLng(d, a),
            map: map,
            icon: c,
            title: e,
            zIndex: 10
        })
    }
    for (var b = 0; b < journey.airports.from.length; b++) {
        journey.airports.from[b].setMap(null)
    }
}

function setToMarker(d, a, e) {
    var c = new google.maps.MarkerImage("images/map-marker-arrive.png", new google.maps.Size(45, 37), new google.maps.Point(0, 0), new google.maps.Point(12, 36));
    if (null !== journey.to) {
        journey.to.setPosition(new google.maps.LatLng(d, a))
    } else {
        journey.to = new google.maps.Marker({
            position: new google.maps.LatLng(d, a),
            map: map,
            icon: c,
            title: e,
            zIndex: 11
        })
    }
    for (var b = 0; b < journey.airports.to.length; b++) {
        journey.airports.to[b].setMap(null)
    }
}

function setAirportMarker(e, b, f, d) {
    var c = new google.maps.MarkerImage("images/plane-marker_blue.png", new google.maps.Size(45, 37), new google.maps.Point(0, 0), new google.maps.Point(12, 36)),
        a = new google.maps.Marker({
            position: new google.maps.LatLng(e, b),
            map: map,
            icon: c,
            title: f,
            zIndex: 1
        });
    if (undefined !== d) {
        journey.airports[d].push(a)
    }
}

function setStationMarker(e, b, f, d) {
    var c = new google.maps.MarkerImage("images/carstation-marker.png", new google.maps.Size(45, 37), new google.maps.Point(0, 0), new google.maps.Point(12, 36)),
        a = new google.maps.Marker({
            position: new google.maps.LatLng(e, b),
            map: map,
            icon: c,
            title: f,
            zIndex: 1
        });
    if (undefined !== d) {
        journey.stations[d].push(a)
    }
}

function panMapToPositions(b, c) {
    if (b === "station") {
        map.setZoom(10);
        var a = new google.maps.LatLngBounds();
        a.extend(journey.stations[c][0].getPosition());
        a.extend(journey.stations[c][journey.stations[c].length - 1].getPosition());
        map.panTo(a.getCenter())
    }
}

function drawPolyline() {

     
    var a = new google.maps.LatLngBounds();
    a.extend(journey.from ? journey.from.position : a.getCenter());
    a.extend(journey.to ? journey.to.position : a.getCenter());
    map.panTo(a.getCenter());

    return; //--through return we have removed polyline

    if (directionsDisplay) {
        directionsDisplay.setMap(null);
        directionsDisplay = null
    }
    if (null === journey.poly) {
        journey.poly = new google.maps.Polyline()
    }
    journey.poly.setMap(null);
    if (null !== journey.from && null !== journey.to) {
        map.fitBounds(a);
        var c = new google.maps.DirectionsService();
        directionsDisplay = new google.maps.DirectionsRenderer({
            suppressMarkers: true
        });
        directionsDisplay.setMap(map);
        var b = {
            origin: journey.from.position,
            destination: journey.to.position,
            travelMode: google.maps.DirectionsTravelMode.WALKING
        };
        c.route(b, function (e, d) {
            if (d === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(e)
            } else { 
                journey.poly.setPath([journey.from.position, journey.to.position]);
                journey.poly.setOptions({
                    geosedic: false,
                    strokeWidth: 10
                });
                journey.poly.setEditable(false);
                journey.poly.setMap(map)
            }
        })
    }
}

function getUserLocale() {
    var a = "en";
    if ($.cookie("language")) {
        return $.cookie("language")
    }
    if (undefined !== document.documentElement.attributes.lang) {
        a = document.documentElement.attributes.lang.nodeValue
    }
    return a
}

function suggest(c) {
    this.request_url = "/GoEuroAPI/rest/suggest/";
    var b = this.request_url.concat(getUserLocale());
    this.min_length = 2;
    var e = this.min_length;
    this.delay = 50;
    var a = this.delay;
    this.elements = ["#from_filter", "#to_filter"];
    var d = this.elements;
    if (c === undefined) {
        var c = ["location", "station", "airport"]
    }
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (h, g) {
            var i = this,
                f = "";
            $.each(g, function (j, k) {
                if (k.type !== f) {
                    h.append("<li class='ui-autocomplete-category ui-autocomplete-category-" + k.type + "'></li>");
                    f = k.type
                }
                i._renderItemData(h, k)
            })
        }
    });
    this.init = function () {
        $("#from_filter").catcomplete({
            appendTo: ".from-box",
            minLength: e,
            delay: a,
            select: function (f, g) {
                $("#departure_fk").val(g.item.id).data("country", g.item.country);
                sug_addr1 = g.item.value;
                if (validateNullJourney(g.item.id, $("#arrival_fk").val(), "#to_filter")) {
                    _gaq.push(["_trackEvent", "Suggester", "Select", g.item.label]);
                    $bonusCardTrigger.triggerHandler("bonuscardchange", ["departure", g.item.id])
                }
            },
            source: function (h, f) {
                var g = h.term;
                if (g in autocompleteCache) {
                    f(autocompleteCache[g]);
                    return
                }
                lastXhr = $.getJSON(b, h, function (j, i, k) {
                    filter_data = new Array();
                    $.each(j, function (l, m) {
                        if ($.inArray(m.type, c) !== -1) {
                            if (j[l].value.indexOf("Space,") < 0) {
                                filter_data.push(j[l])
                            }
                        }
                    });
                    autocompleteCache[g] = filter_data;
                    if (k === lastXhr) {
                        f(filter_data)
                    }
                })
            }
        });
        $("#to_filter").catcomplete({
            appendTo: ".to-box",
            minLength: e,
            delay: a,
            select: function (f, g) {
                $("#arrival_fk").val(g.item.id).data("country", g.item.country);
                sug_addr2 = g.item.value.replace("\\'", "'");
                if (validateNullJourney($("#departure_fk").val(), g.item.id, "#from_filter")) {
                    _gaq.push(["_trackEvent", "Suggester", "Select", g.item.label]);
                    $bonusCardTrigger.triggerHandler("bonuscardchange", ["arrival", g.item.id])
                }
            },
            source: function (h, f) {
                var g = h.term;
                if (g in autocompleteCache) {
                    f(autocompleteCache[g]);
                    return
                }
                lastXhr = $.getJSON(b, h, function (j, i, k) {
                    filter_data = new Array();
                    $.each(j, function (l, m) {
                        if ($.inArray(m.type, c) !== -1) {
                            if (j[l].value.indexOf("Space,") > -1) {
                                j[l].value = "Space"
                            }
                            filter_data.push(j[l])
                        }
                    });
                    autocompleteCache[g] = filter_data;
                    if (k === lastXhr) {
                        f(filter_data)
                    }
                })
            }
        });
        $("#from_filter").bind("keydown", function (g) {
            if ($(this).val().length > 1) {
                var f = (g.keyCode ? g.keyCode : g.which);
                hideInputError("suggest", "#" + $(this).attr("id"));
                focused = "departure";
                if (f == 9 || f == 13) {
                    getFirst(focused);
                    if (f == 13 && $("#arrival_fk").val().length === 0) {
                        g.preventDefault();
                        closeDropdowns("#from_filter")
                    }
                } else {
                    $("#" + focused + "_fk").val("")
                }
            }
        }).on("blur", function () {
            var f = $(this).val().length > 1;
            var g = $("#departure_fk").val() === "";
            if ($(this).val().length > 1 && $("#departure_fk").val() === "") {
                getFirst("departure")
            }
        });
        $("#from_filter").click(function () {
            if (typeof focused !== "undefined" && focused !== null && focused != $(this).attr("id")) {
                getFirst(focused)
            }
        });
        $("#to_filter").bind("keydown", function (g) {
            if ($(this).val().length > 1) {
                var f = (g.keyCode ? g.keyCode : g.which);
                hideInputError("suggest", "#" + $(this).attr("id"));
                focused = "arrival";
                if (f == 9 || f == 13) {
                    getFirst(focused);
                    if (f == 13 && $("#departure_fk").val().length === 0) {
                        g.preventDefault();
                        closeDropdowns("#to_filter")
                    }
                } else {
                    $("#" + focused + "_fk").val("")
                }
            }
        }).on("blur", function () {
            if ($(this).val().length > 1 && $("#arrival_fk").val() === "") {
                getFirst("arrival")
            }
        });
        $("#to_filter").click(function () {
            if (typeof focused !== "undefined" && focused !== null && focused != $(this).attr("id")) {
                getFirst(focused)
            }
        })
    };
    this.closeDropdowns = function (f) {
        if (typeof f !== "undefined" || f == "all") {
            $.each(d, function (h, g) {
                $(g).catcomplete("close")
            })
        } else {
            if ($(f).length > 0) {
                $(f).catcomplete("close")
            }
        }
    };
    this.getFirst = function (g, h) {
        h = (h === undefined) ? false : h;
        focused = null;
        if (g == "departure") {
            var f = "#from_filter"
        } else {
            if (g == "arrival") {
                var f = "#to_filter"
            }
        }
        if ($("#" + g + "_fk").val().length === 0) {
            if ($(f).val().length >= e) {
                lastXhr = $.getJSON(b, {
                    term: $(f).val()
                }, function (j, i, k) {
                    if (k === lastXhr) {
                        if (undefined !== j[0]) {
                            $(f).val(j[0].value);
                            $("#" + g + "_fk").val(j[0].id);
                            if (validateNullJourney($("#departure_fk").val(), $("#arrival_fk").val(), f)) {
                                if ($("#mymap").length > 0) {
                                    if (g === "departure") {
                                        sug_addr1 = j[0].value
                                    } else {
                                        if (g === "arrival") {
                                            sug_addr2 = j[0].value
                                        }
                                    }
                                }
                                $bonusCardTrigger.triggerHandler("bonuscardchange", [g, j[0].id])
                            }
                        } else {
                            showInputError("suggest", f);
                            return false
                        }
                    }
                })
            } else {
                if ($(f).val().length !== 0 || h === true) {
                    showInputError("suggest", f)
                }
                return false
            }
        }
    };
    this.validateNullJourney = function (g, h, f) {
        if (g == h && g != "" && $("#same_dropoff").length === 0) {
            showInputError("suggestNullJourney", f);
            return false
        } else {
            return true
        }
    };
    getFirst = this.getFirst;
    closeDropdowns = this.closeDropdowns;
    validateNullJourney = this.validateNullJourney
}

function showInputError(b, a) {
    hideInputError(b, a);
    switch (b) {
        case "suggest":
            $('<ul class="errors ' + b + '"><li>' + locales[getUserLocale()].suggestError + "</li></ul>").insertAfter(a);
            break;
        case "suggestNullJourney":
            $('<ul class="errors ' + b + '"><li>' + locales[getUserLocale()].suggestNullJourney + "</li></ul>").insertAfter(a);
            break;
        case "compareDates":
            $('<ul class="errors ' + b + '"><li>' + locales[getUserLocale()].compareDates + "</li></ul>").insertAfter(a);
            break;
        case "travelAgeCar":
            $('<ul class="errors ' + b + '"><li>' + locales[getUserLocale()].travelAgeCar + "</li></ul>").insertAfter(a);
            break
    }
}

function hideInputError(b, a) {
    if ((b == "suggest")) {
        if ($(a).siblings("ul.errors").length !== 0) {
            $(a).siblings("ul.errors").remove()
        }
    } else {
        $("ul.errors." + b).remove()
    }
}

function getDate(e, f) {
    f = (f === undefined) ? "dd/mm/yy" : f;
    if (f == "dd/mm/yy") {
        var b = e.split("/");
        var a = parseInt(b[1], 10);
        var g = parseInt(b[0], 10);
        var h = parseInt(b[2], 10);
        var c = new Date(h, a - 1, g);
        if (c.getFullYear() == h && c.getMonth() + 1 == a && c.getDate() == g) {
            return c
        } else {
            return false
        }
    }
}

function compareDates(b, d, c) {
    var b = getDate(b, c);
    if ($("#trip_type").val() == "O") {
        return (b) ? true : false
    }
    var d = getDate(d, c);
    var a = new Date();
    a.setHours(0, 0, 0, 0);
    if (!b || !d) {
        return false
    }
    if ((a <= b && b <= d)) {
        return true
    } else {
        return false
    }
}

function intValidation(b) {
    var a = /^\d+$/;
    return (a.test(b)) ? true : false
}

function ageValidation(a, b) {
    if ((b === "travelAgeCar" && (parseInt(a, 10) < 20 || parseInt(a, 10) > 99)) || intValidation(a) === false) {
        return false
    } else {
        return true
    }
}

function internationalValidation(b, a) {
    if (b.data("country") === a.data("country")) {
        return false
    } else {
        return true
    }
}

function tooFarInFuture(b) {
    var b = getDate(b);
    var a = new Date();
    a.setDate(a.getDate() + 90);
    if (a < b) {
        return confirm(locales[getUserLocale()].noConnection[120])
    }
    return true
}

function searchValidation(a) {
    $(a).submit(function (f) {
        var d = [{
            selector: "#departure_fk",
            relInput: "departure",
            type: "suggest"
        }, {
            selector: "#departure_date",
            relInput: "#return_date",
            type: "compareDates"
        }];
        if ($("#same_dropoff").length > 0) {
            d.push({
                selector: "#traveler_age",
                type: "travelAgeCar"
            });
            if ($("#same_dropoff").attr("checked") === "checked") {
                $("#arrival_fk").val($("#departure_fk").val());
                $("#to_filter").val($("#from_filter").val())
            }
        } else {
            d.push({
                selector: "#arrival_fk",
                relInput: "arrival",
                type: "suggest"
            })
        }
        var c = [];
        $.each([$("#departure_time"), $("#return_time")], function () {
            if ($(this).val() === "anytime") {
                $(this).val("any")
            }
        });
        $.each(d, function (e, g) {
            switch (g.type) {
                case "suggest":
                    if ($(g.selector).val().length === 0) {
                        locationSuggest.getFirst(g.relInput, true);
                        c.push({
                            selector: g.selector,
                            type: g.type
                        })
                    }
                    break;
                case "compareDates":
                    if (false === compareDates($(g.selector).val(), $(g.relInput).val())) {
                        c.push({
                            selector: g.selector,
                            type: g.type
                        })
                    }
                    break;
                case "travelAgeCar":
                    if (false === ageValidation($(g.selector).val(), g.type)) {
                        c.push({
                            selector: g.selector,
                            type: g.type
                        })
                    }
                    break;
                default:
            }
        });
        var b = ($('input[name="trip_type"]').val() === "R") ? $("#return_date").val() : $("#departure_date").val();
        if (jQuery.isEmptyObject(c) && locationSuggest.validateNullJourney($("#arrival_fk").val(), $("#departure_fk").val(), "#to_filter")) {
            return true
        } else {
            f.preventDefault();
            $.each(c, function (e, g) {
                if (g.type !== "suggest") {
                    showInputError(g.type, g.selector)
                }
            });
            locationSuggest.closeDropdowns("all")
        }
    })
}

function triggerNewSearch() {
    $("#departure_time, #return_time").val("any");
    $("#newSearch").submit()
}

function showTimeout() {
    $(".tooltip, .popover").css("display", "none");
    $("#search-timeout").reveal({
        closeOnBackgroundClick: false,
        opened: function () {
            $("#timeout-research").click(function () {
                triggerNewSearch()
            });
            $("body,.reveal-modal-bg").off("keydown, keypress, keyup")
        }
    })
}

function searchTimeOut() {
    var b = new Date();
    b.setMinutes(b.getMinutes() + 25);
    var a = setInterval(c, 10 * 1000);

    function c() {
        var d = new Date();
        if (b.getTime() <= d.getTime()) {
            showTimeout();
            clearInterval(a)
        }
    }
}

function buildDeeplink(a, b) {
    if (b === undefined) {
        b = new Object();
        $(a + " input").each(function (d) {
            var c = $(this).attr("id");
            if (c !== undefined) {
                b[c] = $(this).val()
            }
        })
    }
    deeplink = "/?";
    _.each(b, function (e, c, d) {
        deeplink += c + "=" + e + "&"
    });
    return deeplink
}
var personCounter = {
    travelerCounter: function () {
        return parseInt((parseInt($("#nbchildren").val(), 10) || 0) + (parseInt($("#nbadults").val(), 10) || 0) + (parseInt($("#nbinfants").val(), 10) || 0), 10)
    },
    updateCounters: function () {
        if ($(".frontpage").length !== 0) {
            var b = this.travelerCounter();
            if (b === 1) {
                $("#person-counter").children("div:first-child").text("1 " + locales[getUserLocale()].person)
            } else {
                if (b <= 5) {
                    $("#person-counter").children("div:first-child").text(b + " " + locales[getUserLocale()].persons)
                }
            }
        } else {
            if ($(".travel-search-result-page").length !== 0) {
                $("#person-counter").text(this.travelerCounter());
                var a = 0;
                $.each(["adults", "children"], function (d, e) {
                    var c = parseInt($("#nb" + e).val(), 10);
                    var g = $(".bonuscard-dropdown").children('select[data-type="' + e + '"]');
                    var f = false;
                    $.each(g, function (h, i) {
                        if (c !== 0 && $(this).length !== 0) {
                            var j = $(this).siblings(".custom.dropdown").find("li.selected").is(":first-child");
                            if (!j && f == false) {
                                a += c;
                                f = true
                            }
                        }
                    })
                });
                $("#bc-counter").text(a)
            }
        }
    }
};

function checkSameDate(a) {
    $("#" + a).on("submit", function () {
        if ("R" === $("#trip_type").val() && $("#departure_date").val() === $("#return_date").val() && internationalValidation($("#departure_fk"), $("#arrival_fk"))) {
            return confirm(locales[getUserLocale()].sameDayNotice)
        }
    })
}

function prepareCarRental(a) {
    $("#filters [id^=car_form_]").each(function () {
        $(this).attr("id", $(this).attr("id").replace("car_form_", ""))
    });
    $("form#newSearch [id^=car_form_]").each(function () {
        $(this).attr("id", $(this).attr("id").replace("car_form_", ""))
    });
    if ($(".frontpage-form.carrental #same_dropoff").val() === "0") {
        $("#to_filter").removeAttr("disabled");
        $("#from_filter").parent().animate({
            width: "38%"
        }, 0, function () {
            $("#to_filter").parent().toggleClass("hidden").hide().fadeToggle(0)
        }).toggleClass("nine")
    }
    $("label[for=same_dropoff]").click(function () {
        if ($("#to_filter").attr("disabled") == "disabled") {
            $("#to_filter").removeAttr("disabled");
            if (a == "search") {
                $("#from_filter").parent().animate({
                    width: "25%",
                    "margin-right": "25%"
                }, 300, function () {
                    $("#from_filter").parent().css("margin-right", "5px");
                    $("#to_filter").parent().toggleClass("hidden").hide().fadeToggle(300)
                })
            } else {
                $("#from_filter").parent().animate({
                    width: "38%"
                }, 300, function () {
                    $("#to_filter").parent().toggleClass("hidden").hide().fadeToggle(300)
                }).toggleClass("nine")
            }
        } else {
            $("#to_filter").attr("disabled", "disabled");
            $("#to_filter").parent().fadeToggle().toggleClass("hidden");
            if (a == "search") {
                $("#from_filter").parent().css("margin-right", "25%");
                $("#from_filter").parent().animate({
                    width: "50%",
                    "margin-right": "0"
                }, 300)
            } else {
                $("#from_filter").parent().animate({
                    width: "76%"
                }, 300).toggleClass("nine")
            }
        }
    });
    $("label[for=driver_age]").ready(function () {
        if ($("#driver_age").attr("checked") !== "checked") {
            $("#traveler_age").parent().removeClass("hidden")
        }
    }).click(function () {
        if ($("#driver_age").attr("checked") === "checked") {
            $("#traveler_age").parent().removeClass("hidden");
            $("#driver_age").attr("checked", "unchecked")
        } else {
            $("#traveler_age").parent().addClass("hidden")
        }
    });
    $("#from_filter").keydown(function () {
        if (event.which === 13) {
            if ($("#same_dropoff").next(".checkbox").hasClass("checked")) {
                setTimeout(function () {
                    $("#from_filter").parents("form").submit()
                }, 500)
            }
        }
    });
    $("#to_filter").keydown(function () {
        if (event.which === 13) {
            setTimeout(function () {
                $("#to_filter").parents("form").submit()
            }, 500)
        }
    });
    autoFocusInput()
}

function prepareRouteWidget() {
    $(".result-details").on("click", ".icon-info-sign", function (a) {
        a.preventDefault();
        a.stopPropagation()
    });
    $("#results").on("click", "[rel=popover]", function (a) {
        a.preventDefault();
        a.stopPropagation()
    });
    $("#results").on("click touch", ".route-leg", {}, function (d) {
        var c = $(this),
            a = $(d.target),
            b = $(d.currentTarget);
        d.stopImmediatePropagation();
        if (a.hasClass("pubtrans_popover") || a.hasClass("icon-info-sign")) {
            d.preventDefault();
            d.stopPropagation()
        } else {
            if (b.hasClass("collapsible")) {
                if (c.hasClass("expanded")) {
                    c.removeClass("expanded").addClass("collapsed").find(".leg-summary").show().siblings(".leg-segment").hide()
                } else {
                    c.removeClass("collapsed").addClass("expanded").find(".leg-summary").hide().siblings(".leg-segment").show()
                }
            }
        }
    }).children("div:first-child").show()
}

function autoFocusInput() {
    var a;
    $(".autofocus-elements-list .auto-focus-element").each(function (b, c) {
        if (!a && $(c).val().length === 0) {
            a = true;
            setTimeout(function () {
                $(c).focus()
            }, 10)
        }
    })
}

function prepareHomepage(a) {
    showAndroidOverlay();
    if (a === 1) {
        prepareCarRental("home");
        locationSuggest = new suggest(["location"])
    } else {
        locationSuggest = new suggest();
        if (location.pathname != "/") {
            setTimeout(function () {
                var b = (new Date).getTime();
                $("#departure_date").datepicker("setDate", new Date(b + 7 * ONE_DAY));
                $("#return_date").datepicker("setDate", new Date(b + 12 * ONE_DAY))
            }, 0)
        }
        if (isMobile && location.search == "") {
            setTimeout(function () {
                var b = (new Date).getTime();
                $("#departure_date").datepicker("setDate", new Date(b + 1 * ONE_DAY));
                $("#return_date").datepicker("setDate", new Date(b + 6 * ONE_DAY))
            }, 0)
        }
        if (!(location.search.match(/arrival_fk\=(\d+)/) && document.forms.filters.arrival_fk.value === location.search.match(/arrival_fk\=(\d+)/)[1])) { }
        $("#adults, #nbadults").val(1);
        $("#children, #nbchildren, #infants, #nbinfants").val(0)
    }
    locationSuggest.init();
    searchValidation("#filters");
    $.datepicker.setDefaults($.datepicker.regional[getUserLocale()]);
    $("#departure_date").datepicker({
        buttonText: "Departure date",
        hoursField: "#departure_time",
        onSelect: function (e, c) {
            var d = $(this),
                b = $("#return_date");
            d.data("touched", "true");
            b.datepicker("option", "minDate", e);
            if (b.data("touched") === undefined) {
                b.datepicker("setDate", new Date($.datepicker.parseDate("dd/mm/yy", e).getTime() + 7 * ONE_DAY))
            }
            if ($("#return_round_trip").css("visibility") !== "hidden" && d.data("reverseTab") !== "true") {
                setTimeout(function () {
                    if ($("#trip_type").val() === "R") {
                        $("#return_date:not([type=hidden])").focus()
                    }
                }, 100)
            }
            d.data("reverseTab", "false");
            $(this).datepicker("hide");
            _gaq.push(["_trackEvent", "Datepicker", "Select", "departure-date"])
        }
    });
    $("#return_date").datepicker({
        buttonText: "Arrival date",
        hoursField: "#return_time",
        onSelect: function (c, b) {
            $(this).data("touched", "true");
            $(this).datepicker("hide");
            _gaq.push(["_trackEvent", "Datepicker", "Select", "return-date"])
        }
    });
    $("#person-counter").popover({
        closeHTML: "",
        title: $("#person-counter").data("title") || "",
        position: "nonetop",
        bottom: "",
        content: function () {
            return $("#counter-wrapper").html()
        },
        onDisplay: function () {
            $("#counter-wrapper").remove()
        }
    });
    checkSameDate("filters");
    $(document).on("click", "input[type=text]", function () {
        this.select()
    });
    autoFocusInput()
}
 
 

function prepareSearchPage(b, a) {
    var b = b || {
        searchId: 1
    };
    getSummary(b.searchId, "price");
    prepareRouteWidget();
    searchTimeOut("#newSearch");
    if (a === 1) {
        prepareCarRental("search");
        locationSuggest = new suggest(["location"]);
        resultsReady(b.searchId, true)
    } else {
        locationSuggest = new suggest()
    }
    locationSuggest.init();
    $.datepicker.setDefaults($.datepicker.regional[getUserLocale()]);
    $("#departure_date").datepicker({
        buttonText: "Departure date",
        hoursField: "#departure_time",
        onSelect: function (f, d) {
            var e = $(this),
                c = $("#return_date");
            if ($("#return_round_trip").css("visibility") === "visible") { }
            $(this).datepicker("hide");
            if ($("#return_round_trip").css("visibility") === "visible" && e.data("reverseTab") !== "true") {
                setTimeout(function () {
                    if ($("#trip_type").val() === "R") {
                        c.focus()
                    }
                }, 100)
            }
            e.data("reverseTab", "false");
            _gaq.push(["_trackEvent", "Datepicker", "Select", "departure-date"])
        }
    });
    $("#return_date").datepicker("destroy");
    $("#return_date").datepicker({
        buttonText: "Arrival date",
        hoursField: "#return_time",
        onSelect: function (d, c) {
            $(this).data("touched", "true");
            $("#departure_date").datepicker("option", "maxDate", d);
            $(this).datepicker("hide");
            _gaq.push(["_trackEvent", "Datepicker", "Select", "return-date"])
        }
    });
    $("#departure_time, #return_time").on("focus", function (d) {
        var c = (d.target.id === "departure_time") ? "#departure_date" : "#return_date";
        $(c).datepicker("show")
    });
    $("#from_filter, #to_filter").click(function () {
        this.select()
    });
    searchValidation("#newSearch");
    checkSameDate("newSearch");
    $("#results-search-form-more").triggerHandler("bonuscardchange", ["departure", $("#departure_fk").val()]);
    $("#results-search-form-more").triggerHandler("bonuscardchange", ["arrival", $("#arrival_fk").val()])
}
var timeout;

function noSearchResults() {
    headline = locales[getUserLocale()].noResults.generic;
    clearTimeout(timeout);
    $(".tabs.contained dd, #results div").removeClass("loading").addClass("no-results");
    $.each($("#results").children(), function () {
        if (!$("body").hasClass("carrental")) {
            $(this).html("<strong>" + locales[getUserLocale()].noResults.genericHeadline + "</strong><br />" + locales[getUserLocale()].noResults.generic)
        } else {
            $(this).html(locales[getUserLocale()].noResults.carrental)
        }
    });
    getSummary($("#travel-search-results").data("searchid"), "price", true);
    $("#travel-search-results dl.tabs").hide();
    $('.summary-bar[data-type!="directcar"]').unbind("click");
    $(".sort-toggle").children("li").attr("onclick", "")
}

function resultsReady(f, e) {
    var b = f;
    var d = "";
    var a = window.showTypes || ["train", "flight", "bus", "car"];
    for (var c = 0; c < a.length; c++) {
        d += (c > 0 ? "&" : "") + "type[]=" + a[c]
    }
    $.ajax({
        type: "POST",
        url: "/search/" + f + "/is-done",
        data: d,
        success: function (i) {
            var j = !_.isString(i) && _.isObject(i) ? i : $.parseJSON(i);
            var h = jQuery.inArray("directcar", j.type);
            if (h !== -1) {
                j.type.splice(h);
                delete j.resp.directcar
            }
            if ((_.reject(j.type, function (k) {
                return k == "directcar" || j.resp[k]["status"] === "error" || j.resp[k]["status"] === "notavailable" ? 1 : 0
            }).length === 0)) {
                noSearchResults()
            } else {
                var g = false;
                $.each(j.resp, function (l, o) {
                    if (!o.status || (o.status === "error" || o.status === "notavailable")) {
                        if (resultsLoaded[l] !== true) {
                            getSummary(f, "price");
                            $("dd#tab_" + l + ", #results-" + l).removeClass("loading").addClass("no-results").addClass("no-conn");
                            var k = '<div class="row centered"><strong>' + locales[getUserLocale()].noResults.genericHeadline + "</strong>";
                            if (o.returnCodes && o.returnCodes.length !== 0) {
                                $.each(o.returnCodes, function (q, p) {
                                    if (p !== 101 && p !== 102 && p !== 103) {
                                        if (typeof locales[getUserLocale()].noConnection[p] !== "undefined" && !(l !== "flight" && p === 110)) {
                                            k += "<p>" + locales[getUserLocale()].noConnection[p] + "</p>"
                                        } else {
                                            k += "<p>" + locales[getUserLocale()].noResults[l] + "</p>"
                                        }
                                    } else {
                                        if (l !== "flight") {
                                            $("dd#tab_" + l + ", #results-" + l).hide()
                                        }
                                    }
                                })
                            } else {
                                k += "<p>" + locales[getUserLocale()].noResults[l] + "</p>"
                            }
                            k += "</div>";
                            $("#results-" + l).html(k);
                            resultsLoaded[l] = true;
                            if ($("#results-" + l).hasClass("active")) {
                                $("dd#tab_" + l + ", #results-" + l).removeClass("active").next().addClass("active")
                            }
                        }
                    } else {
                        if (o.status && (o.status === "new" || o.status === "inprogress")) {
                            g = true
                        } else {
                            if (o.status === "done" && resultsLoaded[l] !== true) {
                                if ($("#results-" + l).hasClass("active") || l === "car") {
                                    getFilters(f, l, true);
                                    getResults(f, l);
                                    showTab(l, true);
                                    e = false;
                                    if (l === "flight") {
                                        $("#tab_flight").triggerHandler("click")
                                    }
                                } else {
                                    getResults(f, l)
                                }
                                resultsLoaded[l] = true;
                                var n = $("#results").children(".active").data("type");
                                var m = sortVal = $("#" + n + "_sortby").val() || "price";
                                if (resultsLoaded[l] === true && l !== "car") {
                                    getSummary(f, m)
                                }
                            }
                        }
                    }
                });
                if (g) {
                    setTimeout(function () {
                        resultsReady(f, e)
                    }, 500)
                }
            }
        }
    }).error(function () {
        noSearchResults()
    })
}

function showTab(a, b) {
    if (undefined !== b && b) {
        $("#results,.tabs.contained").children(".active").removeClass("active");
        $("#tab_" + a + ", #results-" + a).addClass("active");
        _gaq.push(["_trackEvent", "Serp", "Tabchange", a])
    }
}

function getSummary(c, a, b) {
    b = (b === undefined) ? false : b;
    if (summaryTableReq && summaryTableReq.readyState !== 4) {
        summaryTableReq.abort()
    }
    summaryTableReq = $.ajax({
        type: "GET",
        url: "/search/" + c + "/summary",
        data: "sort=" + a,
        success: function (d) {
            if (d) {
                $("#summary_table").html(d);
                $("#sortby-" + a).siblings(".active").removeClass("active");
                $("#sortby-" + a).addClass("active");
                if ($(".bar-directcar").length !== 0 && directCar.request === true) {
                    directCar.load();
                    directCar.request = false
                }
                if (b) {
                    $("#sort-toggle-container").addClass("hidden").hide();
                    $('.summary-bar[data-type!="directcar"]').unbind("click")
                }
            }
        }
    })
}

function switchSorting(b) {
    var a = $(".tabs.contained").children(".active").find("a").data("type"),
        c = $("#travel-search-results").data("searchid");
    $("#" + a + "_page").val(1);
    $("#" + a + "_sortby").val(b);
    $("#" + a + "_orderby").val(b);
    getSummary(c, b);
    if (!$("#tab_" + a).is(".no-results, .loading")) {
        refine_search(a, 1, c)
    }
    $("#sortby-" + b).siblings(".active").removeClass("active");
    $("#sortby-" + b).addClass("active");
    _gaq.push(["_trackEvent", "Serp", "Sortchange", b]);
    sortVal = b;
    lastSorting[a] = sortVal;
    return
}

function getFilters(d, a, b) {
    var c = (a === "car") ? "/cars" : "";
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: c + "/search/" + d + "/filters",
            data: "type=" + a,
            success: function (e) {
                if (e) {
                    $("#filters").append(e);
                    if (b) {
                        if (lastSorting[a] !== sortVal) {
                            $("#" + a + "_sortby").val(sortVal);
                            refine_search(a, 0, d);
                            lastSorting[a] = sortVal
                        }
                        currentType = a;
                        showFilters(a)
                    }
                }
            }
        })
    }, 200)
}

function showFilters(a) {
    $("#filters").find(".active").removeClass("active");
    $("#filters_" + a).fadeIn(500).addClass("active");
    $("#form_filters_" + a + " input[id!='travelBy_flight_" + a + "'][id!='travelBy_train_" + a + "']").change(function () {
        refine_search(a)
    })
}

function getResults(b, a) {
    ref_search = $.ajax({
        type: "POST",
        url: "/search/" + b + "/results",
        data: "type=" + a,
        success: function (c) {
            if (c) {
                $("#results-" + a).html(c);
                $("dd#tab_" + a + ", #results-" + a).removeClass("loading");
                if ($("dd#tab_" + a + ", #results-" + a).hasClass("hidden")) {
                    $("dd#tab_" + a + ", #results-" + a).removeClass("hidden")
                }
            }
        }
    })
}

function refine_search(c, b, e) {
    if (ref_search && ref_search.readyState != 4) {
        clearTimeout(timeout);
        ref_search.abort()
    }
    if (b !== 0) {
        $("#" + c + "_page").val(1)
    }
    var a = e || $("#travel-search-results").data("searchid");
    var d = $("#form_filters_" + c).serialize().replace(/\&type\=\w+/, "");
    _gaq.push(["_trackEvent", "Serp", "Filterchange", c]);
    ref_search = $.ajax({
        type: "GET",
        url: "/search/" + a + "/results?type=" + c,
        data: d,
        beforeSend: function () {
            $("#results-" + c).animate({
                opacity: 0.5
            }, 0)
        },
        success: function (f) {
            if (f) {
                $("#results-" + c).html(f)
            }
        },
        complete: function () {
            $("#results-" + c).animate({
                opacity: 1
            }, 500)
        }
    })
}

function change_result_tabs(b, a) {
    if ($("#tab_" + a).is(".no-results, .loading")) {
        $("#filters").find(".active").removeClass("active")
    } else {
        if (resultsLoaded[a]) {
            $("#" + a + "_page").val(1);
            if ($("#filters_" + a).length === 0) {
                getFilters(b, a, true)
            } else {
                $("#" + a + "_sortby").val(sortVal);
                showFilters(a);
                if (lastSorting[a] !== sortVal) {
                    refine_search(a, 0, b);
                    lastSorting[a] = sortVal
                }
            }
        }
    }
    showTab(a, true)
}

function getResultDetails(c, d) {
    var a = $("#search_id").val();
    var b = function (e) {
        if (e) {
            $target = $("#result_details_" + d + "_" + c);
            $target.html(e);
            $("*[rel=popover]", $target).each(function (f) {
                $elt = $(this);
                $elt.popover({
                    closeHTML: "",
                    title: $elt.data("title") || "",
                    position: $elt.data("position") || "",
                    content: function () {
                        return $elt.data("content") || ""
                    }
                })
            });
            $target.slideDown("slow");
            resultDetails[d][c] = e;
            $("a.pubtrans_popover", $target).each(function () {
                loadAirportInfo($(this))
            })
        }
    };
    $.get("/search/" + a + "/" + d + "/detail/" + c, b)
}

function loadTicketInformation(a) {
    $(a).popover({
        closeHTML: "",
        title: a.data("title") || "",
        position: $(a).data("position") || "",
        extraClass: $(a).data("extraclass") || "",
        content: function () {
            return $(a).next("div.popover-content-container").html() || ""
        }
    })
}

function loadAirportInfo(a) {
    $(a).popover({
        closeHTML: "",
        title: a.data("title") || "",
        position: $(a).data("position") || "",
        content: function () {
            return $(a).next("div.popover-content-container").html() || ""
        }
    })
}

function initializeGMap(lat,long) {
    var a = {
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(53, 13),
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: true,
        disableDefaultUI: true
    };
    map = new google.maps.Map(document.getElementById("mymap"), a);
    $("#mymap").on("click", "div.gmnoprint + div", function (b) {
        if ($(this).css("display") !== "none") {
            $(this).css("display", "none")
        }
    })
}

function codeAddressGMap() {
    var c = sug_addr1;
    var b = sug_addr2;
    if (directionsDisplay) {
        directionsDisplay.setMap(null);
        directionsDisplay = null
    }
    if (mapMarkers && mapMarkers.length > 0) {
        for (var a = 0; a < mapMarkers.length; a++) {
            mapMarkers[a].setMap(null);
            mapMarkers[a] = null
        }
    }
    mapMarkers = [];
    if (mapObjectsAjax && mapObjectsAjax.readyState != 4) {
        mapObjectsAjax.abort()
    }
    mapObjectsAjax = $.ajax({
        type: "POST",
        url: "/index/map-objects",
        data: "location1=" + c + "&location2=" + b,
        success: function (e) {
            var f = $.parseJSON(e);
            if (null !== f) {
                for (var d = 0; d < f.length; d++) {
                    mapMarkers.push(new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(f[d]["lat"], f[d]["lng"]),
                        information: f[d]["name"],
                        icon: "images/map-marker-depart.png"
                    }));
                    google.maps.event.addListener(mapMarkers[d], "click", function (g) {
                        this.infowindow = new google.maps.InfoWindow({
                            content: this.information
                        });
                        this.infowindow.open(map, this)
                    })
                }
            }
        }
    });
    if (c === "" || b === "" || c == b) {
        geoCodeAddressGMap(c === "" ? b : c)
    } else {
        if (c.length > 2 && b.length > 2) {
            traceRoute(c, b)
        }
    }
}

function geoCodeAddressGMap(a, b) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        address: a
    }, function (d, c) {
        if (c === google.maps.GeocoderStatus.OK) {
            map.setCenter(d[0].geometry.location);
            mapMarkers.push(new google.maps.Marker({
                map: map,
                position: d[0].geometry.location
            }))
        }
    })
}

function traceRoute(g, e, h) {
    function a(i, l, m) {
        return new google.maps.Marker({
            position: i,
            map: map,
            icon: l,
            title: m
        })
    }
    var j = new google.maps.DirectionsService();
    if (!h) {
        h = map
    }
    directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: true
    });
    directionsDisplay.setMap(h);
    var b = g;
    var c = e;
    if (sug_addr1 === "" && sug_addr2 === "") {
        sug_addr1 = b;
        sug_addr2 = c
    }
    var d = {
        origin: b,
        destination: c,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    if (mapMarkers && mapMarkers.length > 0) {
        for (var f = 0; f < mapMarkers.length; f++) {
            mapMarkers[f].setMap(null);
            mapMarkers[f] = null
        }
    }
    var k = {
        start: new google.maps.MarkerImage("images/map-marker-depart.png", new google.maps.Size(45, 37), new google.maps.Point(0, 0), new google.maps.Point(12, 36)),
        end: new google.maps.MarkerImage("images/map-marker-arrive.png", new google.maps.Size(45, 37), new google.maps.Point(0, 0), new google.maps.Point(12, 36))
    };
    j.route(d, function (l, i) {
        if (i === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(l);
            var m = l.routes[0].legs[0];
            mapMarkers.push(a(m.start_location, k.start, m.start_address));
            mapMarkers.push(a(m.end_location, k.end, m.end_address))
        }
    })
}

function toggleFilter(a) {
    $("#" + a).toggleClass("closed")
}

function initPopovers(a) {
    if (a === undefined) {
        a = "*[rel=popover]"
    }
    $(a).each(function (b) {
        $elt = $(this);
        $elt.popover({
            closeHTML: $elt.data("close") || "",
            title: $elt.data("title") || "",
            position: $elt.data("position") || "",
            content: function () {
                return $elt.data("content") || ""
            },
            extraClass: $elt.data("extraclass") || ""
        })
    })
}
$(document).ajaxStart(function () {
    $(".tooltip").css("display", "none")
});
$(document).ready(function () {
    drawMapsForPhantom();
    if (isMobile) {
        $(".btn.booking").removeClass("has-tip")
    }
    $("#mymap").on("click", ".gmnoprint", function (b) {
        return false
    });
    $(document.body).on("click", function (c) {
        var b = c.target;
        if ($(b).parents().is('li[id*="header-"]')) {
            $(".userprofile").find(".dropdown.open").removeClass("open");
            $(b).parents('li[id*="header-"]').children(".dropdown").toggleClass("open")
        } else {
            $(".userprofile").find(".dropdown.open").removeClass("open")
        }
    });
    $("#header-langswitch, #header-userprofile, #header-currencyswitch").on(" mouseenter", function () {
        $(".userprofile").find(".dropdown.open").removeClass("open")
    });
    $("#travel-search-results .tabs-content").on("click", "a.booking", function (c) {
        c.stopImmediatePropagation();
        c.preventDefault();
        var d = $(c.currentTarget).attr("href") + "&returnUrl=" + encodeURIComponent(location.pathname),
            f = "ext_" + Math.random(),
            b = window.open(d);
        d = d.replace("/movelia", "/");
        setTimeout(function () {
            var g = document.createElement("form"),
                e = document.getElementsByTagName("script")[0];
            g.method = "post";
            g.action = d;
            e.parentNode.insertBefore(g, e);
            g.submit()
        }, 250);
        _gaq.push(["_trackEvent", "Serp", "Booking", "Clicked on booking button"])
    });
    $("#search-options input[type=checkbox]:not(:disabled)").on("change", function (d) {
        var c = $(this),
            b = [];
        if (c.data("value") === "all") {
            if (!c.prop("checked")) {
                c.next().trigger("click")
            }
            $("#search-options input[type=checkbox]:checked").each(function (f, e) {
                if (e.id !== "trip_by_input_1") {
                    $(e).next("span.custom.checkbox").trigger("click")
                }
            });
            $("#search-options #trip_by_span_1").addClass("checked");
            $("#search-options #trip_by_input_1").prop("checked", "checked");
            b = ["flight", "train", "bus"];
            $("#trip_by").val("['flight', 'train', 'bus']")
        } else {
            $("#search-options #trip_by_input_1").prop("checked", false);
            $("#search-options #trip_by_span_1").removeClass("checked");
            $("#search-options input[type=checkbox]:checked").each(function (f, e) {
                b.push($(e).data("value"))
            });
            $("#trip_by").val("['" + b.join("', '") + "']")
        }
        if (b === []) { }
        $bonusCardTrigger.triggerHandler("bonuscardchange", ["travelMode", b])
    });
    initPopovers();
    $(".frontpage #bonuscard-wrapper").bind("DOMSubtreeModified", function () {
        if (firstTriggerBonusCards === 1 && $("body").scrollTop() < 100) {
            $("#departure_date, #return_date").datepicker("option", "direction", "up");
            $(".frontpage").animate({
                scrollTop: 100
            }, 800);
            firstTriggerBonusCards = 0
        }
    });
    $("#travel-search-results").on("click", "dl.tabs dd", function (d) {
        if ($(this).is(":first-child")) {
            return false
        }
        var c = $(d.currentTarget),
            b = c.find("a");
        if (c.attr("rel") === "popover") {
            c.unbind("click");
            c.attr("rel", null);
            if (c.hasClass("no-results")) {
                $(".tabflight-info").remove()
            }
        }
        b.toggleClass("disabled");
        if (!c.hasClass("active")) {
            change_result_tabs(b.data("id"), b.data("type"))
        }
    });
    $("#travel-search-results div#summary_table").on("click", ".summary-bar", function (c) {
        var b = $(this).data("type");
        if (b === "directcar") {
            if ($(".popover.directcar").length === 0) {
                directCar.load()
            }
            directCar.reposition();
            _gaq.push(["_trackEvent", "Serp", "directCar", "Click on summary bar - directcar"])
        }
        if (!$("#tab_" + b).hasClass("active") && b !== "directcar" && resultsLoaded[b]) {
            change_result_tabs($("#travel-search-results").data("searchid"), b)
        }
    });
    $("#results").on("click", ".btn-toolbar, .result-content, .result-details", function (j) {
        var m = $(j.currentTarget),
            f = $(j.target);
        if (f.hasClass("icon-info-sign") || f.hasClass("bookables-offer-popover")) {
            return false
        }
        if (f.hasClass("booking") || f.parents("a").hasClass("booking")) {
            return
        }
        if (m.hasClass("btn-toolbar")) {
            var k = $(j.currentTarget),
                h = k.parents(".result-details")
        } else {
            if (m.hasClass("result-content")) {
                var k = $(j.currentTarget),
                    h = k.siblings(".result-details")
            } else {
                var h = m,
                    k = m.prev()
            }
        }
        if (h.children().length === 0 && !m.hasClass("btn-toolbar")) {
            var g = h.prop("id").match(/result_details_([a-z]+)_(\d+)/).slice(1),
                c = g[0],
                l = g[1],
                b = $("#travel-search-results").data("searchid");
            h.html("<!--<p>Loading ... </p> -->");
            $.ajax({
                url: "/search/" + b + "/" + c + "/detail/" + l,
                cache: true,
                method: "GET",
                success: function (o, e, n) {
                    h.html(o);
                    if (/(Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone|Symb[ian|OS]+)*/i.test(navigator.userAgent)) {
                        $(".btn.booking").removeClass("has-tip")
                    }
                    $("*[rel=popover]", h).each(function (p) {
                        $elt = $(this);
                        $elt.popover({
                            closeHTML: "",
                            title: $elt.data("title") || "",
                            position: $elt.data("position") || "",
                            content: function () {
                                return $elt.data("content") || ""
                            }
                        })
                    });
                    $("a.pubtrans_popover", h).each(function () {
                        loadAirportInfo($(this))
                    });
                    $(".bookables-offer-popover").each(function () {
                        loadTicketInformation($(this))
                    })
                },
                error: function (o, e, n) {
                    h.html("<!-- <h4>Sorry, there has been an error</h4> -->")
                }
            })
        }
        if (m.hasClass("route-widget__btn--toggle")) {
            var d = $(".route-widget__btn--toggle"),
                i = $(".route-widget__holder");
            i.toggleClass("opened");
            if (i.hasClass("opened")) {
                d.text(locales[getUserLocale()]["resultRouteTextClose.afterSales"])
            } else {
                d.text(locales[getUserLocale()]["resultRouteTextShow.afterSales"])
            }
        } else {
            if (!$("body").hasClass("travel-page booking-details")) {
                h.slideToggle("0.1s", function () {
                    k.parent().toggleClass("opened");
                    k.find(".search-book-button").toggleClass("opened")
                })
            }
        }
        j.preventDefault();
        j.stopPropagation()
    });
    $(".travel-page.booking-details .bookables-offer-popover").each(function () {
        loadTicketInformation($(this))
    });
    $(".booking-details .result-content").on("mouseenter mouseleave", ".result-content, .result-details", function (b) {
        $(b.currentTarget).find(".share-dropdown").hide()
    }).on("click", function (c) {
        $target = $(c.target);
        if ($target.hasClass("share-button")) {
            $target.next("div.share-dropdown").slideToggle();
            return false
        } else {
            if ($target.hasClass("fb-share")) {
                var b = $target.parents("div.share-dropdown").data("placeholders");
                postToFBFeed(b);
                return false
            }
        }
    });
    $("#results").on("click", ".pagination a", function (g) {
        g.preventDefault();
        var f = $(this),
            c = f.attr("href").match(/page\=(\d+)/)[1],
            d = $("#filters").children(".active").attr("id").replace(/^filters_/, ""),
            b = $("#filters #" + d + "_page");
        _gaq.push(["_trackEvent", "Serp", "Pagination", "Jumping to page " + c]);
        b.val(c);
        refine_search(d, 0, $("#filters #search_id").val())
    });
    $(".search-types-bar ul li a").click(function () {
        if (!$(this).parent().hasClass("active")) {
            $(this).parent().addClass("active").siblings().removeClass("active");
            $("#trip_type").val($(this).attr("value"));
            $("#return_round_trip").toggleClass("invisible");
            $.fn.placeholder()
        }
    });
    $(".travel-search-result-page, .frontpage-form").on("click", ".right-btn", function () {
        var b = $(this).parent().children("div").children("input"),
            c = b.attr("name");
        if (personCounter.travelerCounter() < 5) {
            b.val(parseInt(b.val(), 10) + 1);
            $("#" + c).val(b.val());
            if ("infants" !== b.attr("id")) {
                $bonusCardTrigger.triggerHandler("bonuscardchange", [b.attr("id"), b.val()])
            }
            personCounter.updateCounters()
        }
        return false
    }).on("click", ".left-btn", function () {
        var b = $(this).parent().children("div").children("input"),
            c = b.attr("name");
        if ((b.attr("id") === "nbadults" && parseInt(b.val()) > 1) || (b.attr("id") !== "nbadults" && parseInt(b.val()) > 0)) {
            b.val(parseInt(b.val(), 10) - 1);
            if (b.val() === "0") {
                var d = $('select[data-type="children"]').attr("name");
                $('input[type="hidden"][name="' + d + '"]').remove()
            }
            $("#" + c).val(b.val());
            personCounter.updateCounters();
            if ("infants" !== b.attr("id")) {
                $bonusCardTrigger.triggerHandler("bonuscardchange", [b.attr("id"), b.val()])
            }
        }
        return false
    }).on("keypress", ".counter-cell input", function (g) {
        var f = parseInt($(this).val()) || 0,
            d = parseInt(String.fromCharCode(g.keyCode), 10) || 0,
            c = personCounter.travelerCounter(),
            b = g.keyCode === 0 ? g.which : g.keyCode;
        if (String.fromCharCode(b).match(/[^0-9]/g) || ((d > 5) || ((c + d - f) > 5)) || ($(this).attr("id") === "nbadults" && d === 0)) {
            return false
        } else {
            $("#" + $(this).attr("name")).val(d);
            personCounter.updateCounters()
        }
    }).on("change", ".counter-cell input", function () {
        personCounter.updateCounters();
        if ("infants" !== $(this).attr("id")) {
            $bonusCardTrigger.triggerHandler("bonuscardchange", [$(this).attr("id"), $(this).val()])
        }
    }).on("blur", ".counter-cell input", function () {
        if ((!$(this).val() && $(this).val() === "") || parseInt($(this).val()) === 0 || isNaN($(this).val())) {
            if ($(this).attr("id") === "nbadults") {
                $(this).val(1)
            } else {
                var b = $(this).attr("name");
                $(this).val(0);
                $("#" + b).val(0);
                if (b === "children") {
                    var c = $('select[data-type="children"]').attr("name");
                    $('input[type="hidden"][name="' + c + '"]').remove()
                }
            }
        }
        personCounter.updateCounters();
        $bonusCardTrigger.triggerHandler("bonuscardchange", [$(this).attr("id"), $(this).val()])
    });
    $("#bonuscard-wrapper").on("click", ".dropdown li", function () {
        var d = $(this).parents(".form-row").find("select"),
            h = d.children(":nth-child(" + ($(this).index() + 1) + ")").val(),
            b = $('input[type="hidden"][name="' + d.attr("name") + '"]');
        if (b.length === 0) {
            $(this).parents("form").append('<input type="hidden" name="' + d.attr("name") + '" value="' + h + '">')
        } else {
            b.val(h)
        }
        if ($(".search-results-page").length !== 0) {
            if (!$(this).hasClass("selected")) {
                $counter = $("#bc-counter");
                var i = parseInt($counter.text(), 10) || 0;
                var g = d.attr("data-type");
                var f = parseInt($("#nb" + g).val());
                var e = false;
                $.each($("#bonuscard-wrapper").find('select[data-type="' + g + '"][name!="' + d.attr("name") + '"]'), function (c, j) {
                    if (!$(this).siblings(".dropdown").children("ul").children(".selected").is(":first-child")) {
                        e = true
                    }
                });
                if (e == false) {
                    if (!$(this).is(":first-child") && $(this).siblings(":first-child").hasClass("selected")) {
                        $counter.text(i + f)
                    } else {
                        if ($(this).is(":first-child")) {
                            $counter.text(i - f)
                        }
                    }
                }
            }
        }
    });
    $(window).resize(function () {
        if ($(".frontpage #person-counter").length > 0 && $(".popover-wrap").length > 0 && $(".popover-wrap").css("display") === "block") {
            $(".popover-wrap").attr("style", "top : " + ($("#person-counter").offset().top - $(".popover-wrap").outerHeight() - 1) + "px;left : " + $("#person-counter").offset().left + "px;display:block;")
        } else {
            if ($(".search-results-page").length !== 0) {
                if (!$(".tabflight-info").is(":hidden")) {
                    $(".tabflight-info").remove();
                    $("#popover-bg").css({
                        display: "none"
                    })
                }
                if (!$(".directcar.popover").is(":hidden")) {
                    directCar.reposition()
                }
            }
        }
        return false
    });
    $("#filters #search .row .two.columns").on("keypress", "#traveler_age", function (c) {
        var b = c.keyCode === 0 ? c.which : c.keyCode;
        if (String.fromCharCode(b).match(/[^0-9]/g)) {
            return false
        }
    }).on("blur", "#traveler_age", function (b) {
        if (parseInt($(this).val()) < 21) {
            $(this).val(21)
        }
    });
    $(document).bind("dragstart", function () {
        return false
    });
    $("#filters").on("click", "a.extend-trigger", function (d) {
        var c = $(this).data("extend-value"),
            b = $(this).data("type");
        $.ajax({
            type: "GET",
            url: "/search/" + $("#search_id").val() + "/extendrange/" + b + "/" + c,
            success: function (e) {
                if (e) {
                    location.reload()
                } else {
                    $(d.trigger).remove()
                }
            }
        })
    });
    var a = $("#results-search-form-more");
    a.inner = $(".results-search-form-more-inner");
    a.on("transitionEnd webkitTransitionEnd transitionend oTransitionEnd msTransitionEnd", function (b) {
        if (a.hasClass("opened")) {
            a.css("overflow", "initial")
        }
    });
    $(".results-search-form").on("click", "#results-search-form-more-btn", function () {
        a.toggleClass("opened closed");
        a.contentHeight = a.outerHeight();
        if (a.hasClass("closed")) {
            a.css("max-height", a.contentHeight);
            setTimeout(function () {
                a.css({
                    "max-height": 0,
                    opacity: 0,
                    overflow: "hidden"
                })
            }, 10)
        } else {
            if (a.hasClass("opened")) {
                a.contentHeight += a.inner.outerHeight();
                a.css({
                    "max-height": a.contentHeight,
                    opacity: 1
                })
            }
        }
        $(this).toggleClass("closed opened")
    });
    $("#movelia-frame").load(function () {
        $(this).load(function () {
            $("#movelia-iframe-overlay").hide()
        })
    });
    (function (b) {
        b.fn.placeholder = function () {
            if (typeof document.createElement("input").placeholder == "undefined") {
                b("[placeholder]").focus(function () {
                    var c = b(this);
                    if (c.val() == c.attr("placeholder")) {
                        c.val("");
                        c.removeClass("placeholder")
                    }
                }).blur(function () {
                    var c = b(this);
                    if (c.val() == "" || c.val() == c.attr("placeholder")) {
                        c.addClass("placeholder");
                        c.val(c.attr("placeholder"))
                    }
                }).blur().parents("form").submit(function () {
                    b(this).find("[placeholder]").each(function () {
                        var c = b(this);
                        if (c.val() == c.attr("placeholder")) {
                            c.val("")
                        }
                    })
                })
            }
        }
    })(jQuery);
    $("#linkedin").on("click", function () {
        var b = _.last(location.host.split("."));
        var c = getUserLocale() == "en" ? (b == "co.uk" ? "co.uk" : "com") : getUserLocale();
        IN.UI.Share().params({
            url: "http://www.goeuro." + c
        }).place()
    });
    $(".travel-page.booking-details").on("click", ".hotel-holder__partner-logo , .hotel-holder__partner-image, .hotel-holder__catch-phrase ", function () {
        var b = $(this).attr("href") || $(this).find("a").attr("href"),
            c = this.className || this.getAttribute("class"),
            d = null;
        if (/image/.test(c)) {
            d = "image"
        } else {
            if (/logo/.test(c)) {
                d = "logo"
            } else {
                if (/catch-phrase/.test(c)) {
                    d = "tagline"
                }
            }
        }
        if (b.match("airbnb")) {
            _gaq.push(["_trackEvent", "hotel-partners", "airbnb", "after-sales-click-" + d])
        } else {
            if (b.match("budgetplaces")) {
                _gaq.push(["_trackEvent", "hotel-partners", "budgetplaces", "after-sales-click-" + d])
            }
        }
    });
    $(".travel-page .hotel-holder__partner").ready(function () {
        if ($(".hotel-holder__partner-api").length != 0) {
            _gaq.push(["_trackEvent", "hotel-partners", "budgetplaces", "on-page"])
        }
    })
});