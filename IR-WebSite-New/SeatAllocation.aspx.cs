﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;

public partial class SeatAllocation : System.Web.UI.Page
{
    BookingRequestUserControl objBRUC;
    public string currency;
    public Guid currencyID = new Guid();
    Guid siteId;
    ManageBooking _masterBooking = new ManageBooking();
    ManageTrainDetails _master = new ManageTrainDetails();
    readonly Masters _objMaster = new Masters();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string _FromDate;
    public string _ToDate;
    public string LocationCode = string.Empty;

    OneHubRailOneHubClient client = new OneHubRailOneHubClient();
    private readonly db_1TrackEntities db = new db_1TrackEntities();
    ManageOneHub _ManageOneHub = new ManageOneHub();
    public bool isShowReserable = false;
    public bool isShowOutbound = false;
    public bool isShowInbound = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["req"] != null)
                {
                    GetReservationDetails();
                    showTodDiv();
                }
                else
                    Response.Redirect("TrainResults.aspx?req=EV", true);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetReservationDetails()
    {
        try
        {
            string OutSleeper = string.Empty;
            string InSleeper = string.Empty;
            var objReservationCompInfo = Session["ReservationCompInfo"] as List<ReservationCompanyInfo>;
            var ReservationInfoList = Session["ReservationInfoList"] as List<ReservationInfo>;
            var objResponse = Session["ReserveList"] as SeatReservationResponse;
            var outboundList = new List<JourneyReservationInfo>();
            var inboundList = new List<JourneyReservationInfo>();

            if (objResponse != null && ReservationInfoList != null)
            {
                if (objResponse.EvJourneySeatInfo != null && objResponse.EvJourneySeatInfo.Count() > 0)
                {
                    var EvSeatAllocate = new List<EvSeatAllocation>() { new EvSeatAllocation { SeatDescription = "<span><strong>Seat Allocation:&nbsp;</strong></span><span>None reservable</span>", isReserable = false } };
                    #region outBound
                    outboundList = objResponse.EvJourneySeatInfo[0].EvTrainSegmentsInfoList != null && objResponse.EvJourneySeatInfo[0].EvTrainSegmentsInfoList.Count() > 0 ? objResponse.EvJourneySeatInfo[0].EvTrainSegmentsInfoList.Select((x, i) => new JourneyReservationInfo
                    {
                        Index = i + 1,
                        DepartureStation = x.DeptStation,
                        ArrivalStation = x.ArrivalStation,
                        DepartureDateTime = x.DeptDate + " " + x.DeptTime,
                        CompanyName = objReservationCompInfo != null && objReservationCompInfo.Count > 0 ? objReservationCompInfo.FirstOrDefault(y => y.LegId == x.LegId).CompanyName : "",
                        TravellBy = objReservationCompInfo != null && objReservationCompInfo.Count > 0 ? objReservationCompInfo.FirstOrDefault(y => y.LegId == x.LegId).TravellBy : "",
                        LegId = x.LegId,
                        DepartureDate = x.DeptDate,
                        IsSleeper = objResponse.EvJourneySeatInfo[0].IsSleeper,
                        EvSeatAllocations = x.EvSeatInfoList != null && x.EvSeatInfoList.Count() > 0 ? x.EvSeatInfoList.Select((y, j) => new EvSeatAllocation
                        {
                            SeatDescription = objResponse.EvJourneySeatInfo[0].IsSleeper
                            ? "<span><strong>Berth Allocation:&nbsp;&nbsp;</strong></span><span class='" + (j == 0 ? "span-first" : "span-last") + "'>" + y.Coach + "" + y.SeatNumber + "&nbsp;&nbsp;&nbsp;" + y.Note + "</span>"
                            : "<span><strong>Seat Allocation:&nbsp;&nbsp;</strong></span><span class='" + (j == 0 ? "span-first" : "span-last") + "'>" + y.Coach + "" + y.SeatNumber + "&nbsp;&nbsp;&nbsp;" + y.Note + "</span>",
                            isReserable = true
                        }).ToList() : EvSeatAllocate.ToList()
                    }).ToList() : null;

                    if (outboundList != null && outboundList.Count > 0)
                    {
                        rptOutBound.DataSource = outboundList;
                        rptOutBound.DataBind();
                        if (outboundList.Any(x => x.EvSeatAllocations.Any(y => y.isReserable)))
                            isShowOutbound = true;
                        if (outboundList.Any(x => x.EvSeatAllocations != null && x.EvSeatAllocations.Count > 0))
                        {
                            string outhtmldata = "";
                            foreach (var aa in outboundList)
                            {
                                if (outhtmldata != "")
                                    outhtmldata += "<br/>";
                                outhtmldata += "<div><b>Leg " + aa.Index + "</b> - " + aa.DepartureStation + " to " + aa.ArrivalStation + ", departing at " + aa.DepartureDateTime + " on " + aa.DepartureDate + " " + aa.TravellBy + "</div>";
                                foreach (var bb in aa.EvSeatAllocations.Select(z => new { z.SeatDescription }))
                                {
                                    outhtmldata += bb.SeatDescription + "@";
                                }
                            }
                            OutSleeper = outhtmldata;
                        }
                    }
                    #endregion

                    #region Inbound
                    if (objResponse.EvJourneySeatInfo.Count() > 1)
                    {
                        inboundList = objResponse.EvJourneySeatInfo[1].EvTrainSegmentsInfoList != null && objResponse.EvJourneySeatInfo[1].EvTrainSegmentsInfoList.Count() > 0 ? objResponse.EvJourneySeatInfo[1].EvTrainSegmentsInfoList.Select((x, i) => new JourneyReservationInfo
                       {
                           Index = i + 1,
                           DepartureStation = x.DeptStation,
                           ArrivalStation = x.ArrivalStation,
                           DepartureDateTime = x.DeptDate + " " + x.DeptTime,
                           CompanyName = objReservationCompInfo != null && objReservationCompInfo.Count > 0 ? objReservationCompInfo.FirstOrDefault(y => y.LegId == x.LegId).CompanyName : "",
                           TravellBy = objReservationCompInfo != null && objReservationCompInfo.Count > 0 ? objReservationCompInfo.FirstOrDefault(y => y.LegId == x.LegId).TravellBy : "",
                           LegId = x.LegId,
                           DepartureDate = x.DeptDate,
                           IsSleeper = objResponse.EvJourneySeatInfo[1].IsSleeper,
                           EvSeatAllocations = x.EvSeatInfoList != null && x.EvSeatInfoList.Count() > 0 ? x.EvSeatInfoList.Select((y, j) => new EvSeatAllocation
                           {
                               SeatDescription = objResponse.EvJourneySeatInfo[1].IsSleeper
                            ? "<span><strong>Berth Allocation:&nbsp;&nbsp;</strong></span><span class='" + (j == 0 ? "span-first" : "span-last") + "'>" + y.Coach + "" + y.SeatNumber + "&nbsp;&nbsp;&nbsp;" + y.Note + "</span>"
                            : "<span><strong>Seat Allocation:&nbsp;&nbsp;</strong></span><span class='" + (j == 0 ? "span-first" : "span-last") + "'>" + y.Coach + "" + y.SeatNumber + "&nbsp;&nbsp;&nbsp;" + y.Note + "</span>",
                               isReserable = true
                           }).ToList() : EvSeatAllocate.ToList()
                       }).ToList() : null;

                        if (inboundList != null && inboundList.Count > 0)
                        {
                            rptInBound.DataSource = inboundList;
                            rptInBound.DataBind();
                            if (inboundList.Any(x => x.EvSeatAllocations.Any(y => y.isReserable)))
                                isShowInbound = true;
                            if (inboundList.Any(x => x.EvSeatAllocations != null && x.EvSeatAllocations.Count > 0))
                            {
                                string inhtmldata = "";
                                foreach (var aa in inboundList)
                                {
                                    if (inhtmldata != "")
                                        inhtmldata += "<br/>";
                                    inhtmldata += "<div><b>Leg " + aa.Index + "</b> - " + aa.DepartureStation + " to " + aa.ArrivalStation + ", departing at " + aa.DepartureDateTime + " on " + aa.DepartureDate + " " + aa.TravellBy + "</div>";
                                    foreach (var bb in aa.EvSeatAllocations.Select(z => new { z.SeatDescription }))
                                    {
                                        inhtmldata += bb.SeatDescription + "@";
                                    }
                                }
                                InSleeper = inhtmldata;
                            }
                        }
                    }

                    #endregion

                    long P2POrderId = Convert.ToInt64(Session["P2POrderID"]);
                    _masterBooking.AddSleeperSeat(P2POrderId, OutSleeper, InSleeper);

                    #region Show Hide Journey
                    if (isShowOutbound && isShowInbound)
                    {
                        div_seatAllocation.Attributes.Add("style", "display:block");
                        div_Outbound.Attributes.Add("style", "display:block");
                        div_Inbound.Attributes.Add("style", "display:block");
                    }
                    else if (isShowOutbound)
                    {
                        div_seatAllocation.Attributes.Add("style", "display:block");
                        div_Outbound.Attributes.Add("style", "display:block");
                    }
                    else if (isShowInbound)
                    {
                        div_seatAllocation.Attributes.Add("style", "display:block");
                        div_Inbound.Attributes.Add("style", "display:block");
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MultiText", "removeMultiText();", true);
                    #endregion
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            EvBookingRequest();
            Response.Redirect("P2PBookingCart.aspx?req=EV");
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void EvBookingRequest()
    {
        RailBookingRequest request = new RailBookingRequest();
        try
        {
            List<BookingRequest> listBookingRequest = new List<BookingRequest>();
            BookingRequest bkrequest = new BookingRequest();
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            var objTravellLists = Session["TravellerList"] as List<TravellersInfo>;
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            List<ReservationInfo> ReservationInfoList = Session["ReservationInfoList"] as List<ReservationInfo>;
            List<EvJourneyFareInfo> EvJourneyFareInfoList = Session["ReserveSeatType"] as List<EvJourneyFareInfo>;
            var objResponse = Session["ReserveList"] as SeatReservationResponse;

            if (pInfoSolutionsResponse.EvolviPayloadAttributesInfo != null)
            {
                LocationCode = !string.IsNullOrEmpty(hdnStationCode.Value) ? hdnStationCode.Value.Trim() : pInfoSolutionsResponse.TrainInformationList.Where(y => ReservationInfoList.Select(m => m.JourneyIdentifier).ToList().Contains(y.TrainNumber)).FirstOrDefault().DepartureStationCode;
                request = new RailBookingRequest
                {
                    EchoToken = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.EchoToken,
                    Version = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.Version,
                    TransactionIdentifier = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TransactionIdentifier,
                    TimeStamp = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TimeStamp,
                    SequenceNmbr = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.SequenceNmbr,
                    TargetType = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.TargetType,
                    ISOCountry = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCountry,
                    ISOCurrency = pInfoSolutionsResponse.EvolviPayloadAttributesInfo.ISOCurrency,
                    AccountID = pInfoSolutionsResponse.TPAExtensionsInfo.FirstOrDefault().AccountInfo.FirstOrDefault().AccountID,
                };
                request.OriginDestinationOptionInfoList = pInfoSolutionsResponse.TrainInformationList != null ? pInfoSolutionsResponse.TrainInformationList.Where(y => ReservationInfoList.Select(m => m.JourneyIdentifier).ToList().Contains(y.TrainNumber)).Select(y => new OriginDestinationOptionInfo
                   {
                       DepartureDateTime = DateTime.ParseExact(y.DepartureDate + " " + y.DepartureTime, "dd-MM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture),
                       TrainNumber = y.TrainNumber,
                       DepartureStationCode = y.DepartureStationCode,
                       DepartureStationName = y.DepartureStationName,
                       ArrivalStationCode = y.ArrivalStationCode,
                       ArrivalStationName = y.ArrivalStationName,
                       FareReference = y.IsReturn ? ReservationInfoList.FirstOrDefault(o3 => o3.JourneyType == "Inbound").FareId : ReservationInfoList.FirstOrDefault(o3 => o3.JourneyType == "Outbound").FareId,
                       SeatDirection = EvJourneyFareInfoList != null ? EvJourneyFareInfoList.Where(o3 => o3.JourneyIdentifier == y.TrainNumber).Select(o3 => o3.EvTrainSegmentInfo != null && o3.EvTrainSegmentInfo.ToList().Count > 0 ? o3.EvTrainSegmentInfo.FirstOrDefault().Direction : SeatDirectionType.Any).FirstOrDefault() : SeatDirectionType.Any,
                       SeatPreference = EvJourneyFareInfoList != null ? EvJourneyFareInfoList.Where(o3 => o3.JourneyIdentifier == y.TrainNumber).Select(o3 => o3.EvTrainSegmentInfo != null && o3.EvTrainSegmentInfo.ToList().Count > 0 ? o3.EvTrainSegmentInfo.FirstOrDefault().Position : SeatType.Any).FirstOrDefault() : SeatType.Any,
                       IsSleeper = objResponse != null
                       ? y.IsReturn ? objResponse.EvJourneySeatInfo != null && objResponse.EvJourneySeatInfo.Length > 0 ? objResponse.EvJourneySeatInfo[1].IsSleeper : false : objResponse.EvJourneySeatInfo[0].IsSleeper
                       : false
                   }).ToArray() : null;

                #region Travellers
                request.TravellerInfosList = objTravellLists != null && objTravellLists.Count > 0 ? objTravellLists.OrderByDescending(x => x.Lead).Select((x, i) => new TravellerInfo
                {
                    Title = new string[] { x.Title.Replace(".", string.Empty) },
                    FirstName = new string[] { x.FName },
                    LastName = x.LName,
                    NoOfPassenger = (objBRUC.Adults + objBRUC.Boys + objBRUC.Youths + objBRUC.Seniors).ToString(),
                    TravellerType = new string[] { x.Type.Split(' ')[0] },
                    RailCardCode = objBRUC.EvolviRailCardRequestInfoList != null && objBRUC.EvolviRailCardRequestInfoList.Count > 0 ? objBRUC.EvolviRailCardRequestInfoList.Where((y, j) => i == j).FirstOrDefault().RailCardCode : string.Empty
                }).ToArray() : null;
                #endregion

                request.TPA_ExtensionInfo = objResponse != null ? new TPA_ExtensionInfo
                {
                    TicketQueueID = pInfoSolutionsResponse.TPAExtensionsInfo != null ? pInfoSolutionsResponse.TPAExtensionsInfo.Select(x => x.TicketQueuesInfo != null ? x.TicketQueuesInfo.FirstOrDefault().TicketQueueID : "").FirstOrDefault() : "",
                    LocationCode = LocationCode,

                    OutBoundSeatLocation = EvJourneyFareInfoList != null && EvJourneyFareInfoList.Count > 0 ? EvJourneyFareInfoList[0].EvTrainSegmentInfo != null && EvJourneyFareInfoList[0].EvTrainSegmentInfo.ToList().Count > 0 ? EvJourneyFareInfoList[0].EvTrainSegmentInfo.FirstOrDefault().Location : SeatLocationType.Any : SeatLocationType.Any,
                    IsOutBound = objResponse.EvJourneySeatInfo[0].IsSleeper
                    ? false
                    : EvJourneyFareInfoList != null && EvJourneyFareInfoList.Count > 0 ? EvJourneyFareInfoList[0].EvTrainSegmentInfo != null && EvJourneyFareInfoList[0].EvTrainSegmentInfo.ToList().Count > 0 ? true : false : false,

                    InBoundSeatLocation = EvJourneyFareInfoList != null && EvJourneyFareInfoList.Count > 1 ? EvJourneyFareInfoList[1].EvTrainSegmentInfo != null && EvJourneyFareInfoList[1].EvTrainSegmentInfo.ToList().Count > 0 ? EvJourneyFareInfoList[1].EvTrainSegmentInfo.FirstOrDefault().Location : SeatLocationType.Any : SeatLocationType.Any,
                    IsInBound = objResponse.EvJourneySeatInfo != null && objResponse.EvJourneySeatInfo.Length > 1 ? objResponse.EvJourneySeatInfo[1].IsSleeper
                    ? false
                    : EvJourneyFareInfoList != null && EvJourneyFareInfoList.Count > 1 ? EvJourneyFareInfoList[1].EvTrainSegmentInfo != null && EvJourneyFareInfoList[1].EvTrainSegmentInfo.ToList().Count > 0 ? true : false : false : false,

                    NRSBookingRef = objResponse.EvJourneySeatInfo.FirstOrDefault().BookingRef
                } : null;

                bkrequest.EvolviBookingRequest = request;
                bkrequest.Id = Guid.NewGuid();
                listBookingRequest.Add(bkrequest);
                Session["BOOKING-REQUEST"] = listBookingRequest;
                UpdateSleeperPrice();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void showTodDiv()
    {
        try
        {
            EvolviTODAvailabilityRequest request = new EvolviTODAvailabilityRequest();
            EvolviTODAvailabilityResponse response = new EvolviTODAvailabilityResponse();
            TrainInformationResponse pInfoSolutionsResponse = Session["TrainSearch"] as TrainInformationResponse;
            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null && pInfoSolutionsResponse.TrainInformationList.Length > 0)
            {
                var list = pInfoSolutionsResponse.TrainInformationList.FirstOrDefault();
                h3_StationName.InnerHtml = list.DepartureStationName + " to " + list.ArrivalStationName;
                request.StationCode = list.DepartureStationCode;
                _ManageOneHub.ApiLogin(request, siteId);

                if (Session["holdTodStationList"] != null)
                    response = Session["holdTodStationList"] as EvolviTODAvailabilityResponse;
                else
                    response = client.EvolviTODAvailability(request);

                if (response != null && response.ErrorMessage == null && response.StationList != null && response.StationList.FirstOrDefault(x => x.CRSCode == list.DepartureStationCode) != null)
                {
                    if (response.StationList.FirstOrDefault(x => x.CRSCode == list.DepartureStationCode).TodAvailable == "Y")
                    {
                        Session["holdTodStationList"] = response;
                        ddlStation.SelectedValue = list.DepartureStationName;
                        hdnStationCode.Value = list.DepartureStationCode;

                        if (response.StationList.FirstOrDefault(x => x.CRSCode == request.StationCode).EvTodAvailabilityList != null)
                        {
                            lblTODAvailable.Text = "ToD Availability - at " + ddlStation.SelectedValue;
                            var evStationList = response.StationList.FirstOrDefault(x => x.CRSCode == request.StationCode);
                            if (evStationList.EvTodAvailabilityList.EvBookingOffice != null)
                                lblBookingOffice.Text = "Booking Office: " + evStationList.EvTodAvailabilityList.EvBookingOffice.Availability + ": Mon – Sun, Jan – Dec.";
                            else
                                lblBookingOffice.Text = "No Booking Office details for this location are currently known.";

                            if (evStationList.EvTodAvailabilityList.EvTVM != null)
                                lblTVM.Text = "Ticket Vending Machine: " + evStationList.EvTodAvailabilityList.EvTVM.Availability + ": Mon – Sun, Jan – Dec.";
                            else
                                lblTVM.Text = "No Ticket Vending Details at this location are currently known.";
                        }
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void ddlStation_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            EvolviTODAvailabilityRequest request = new EvolviTODAvailabilityRequest();
            EvolviTODAvailabilityResponse response = new EvolviTODAvailabilityResponse();
            if (ddlStation.SelectedIndex > 0)
            {
                request.StationCode = hdnStationCode.Value = db.StationNameLists.FirstOrDefault(x => x.StationEnglishName == ddlStation.SelectedValue && x.IsActive == true && x.RailName == "EVOLVI").StationCode;
                _ManageOneHub.ApiLogin(request, siteId);
                response = client.EvolviTODAvailability(request);
                if (response != null && response.ErrorMessage == null && response.StationList != null)
                {
                    if (response.StationList.FirstOrDefault(x => x.CRSCode == request.StationCode).TodAvailable == "Y")
                    {
                        if (response.StationList.FirstOrDefault(x => x.CRSCode == request.StationCode).EvTodAvailabilityList != null)
                        {
                            lblTODAvailable.Text = "ToD Availability - at " + ddlStation.SelectedValue;
                            var evStationList = response.StationList.FirstOrDefault(x => x.CRSCode == request.StationCode);
                            if (evStationList.EvTodAvailabilityList.EvBookingOffice != null)
                                lblBookingOffice.Text = "Booking Office: " + evStationList.EvTodAvailabilityList.EvBookingOffice.Availability + ": Mon – Sun, Jan – Dec.";
                            else
                                lblBookingOffice.Text = "No Booking Office details for this location are currently known.";

                            if (evStationList.EvTodAvailabilityList.EvTVM != null)
                                lblTVM.Text = "Ticket Vending Machine: " + evStationList.EvTodAvailabilityList.EvTVM.Availability + ": Mon – Sun, Jan – Dec.";
                            else
                                lblTVM.Text = "No Ticket Vending Details at this location are currently known.";
                        }
                    }
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void UpdateSleeperPrice()
    {
        try
        {
            bool isOutBound = false;
            bool isInBound = false;
            var objResponse = Session["ReserveList"] as SeatReservationResponse;
            if (objResponse != null && objResponse.EvJourneySeatInfo != null && objResponse.EvJourneySeatInfo.Length > 0)
            {
                bool IsSleeper = objResponse.EvJourneySeatInfo[0].IsSleeper;
                if (IsSleeper)
                {
                    var outboundList = objResponse.EvJourneySeatInfo[0].EvTrainSegmentsInfoList != null && objResponse.EvJourneySeatInfo[0].EvTrainSegmentsInfoList.Length > 0 ? objResponse.EvJourneySeatInfo[0].EvTrainSegmentsInfoList.Any(x => x.EvSeatInfoList != null && x.EvSeatInfoList.Length > 0) : false;
                    if (outboundList == false)
                        isOutBound = true;
                }
            }
            if (objResponse != null && objResponse.EvJourneySeatInfo != null && objResponse.EvJourneySeatInfo.Length > 1)
            {
                bool IsSleeper = objResponse.EvJourneySeatInfo[1].IsSleeper;
                if (IsSleeper)
                {
                    var inboundList = objResponse.EvJourneySeatInfo[1].EvTrainSegmentsInfoList != null && objResponse.EvJourneySeatInfo[1].EvTrainSegmentsInfoList.Length > 0 ? objResponse.EvJourneySeatInfo[1].EvTrainSegmentsInfoList.Any(x => x.EvSeatInfoList != null && x.EvSeatInfoList.Length > 0) : false;
                    if (inboundList == false)
                        isInBound = true;
                }
            }
            long P2POrderId = Convert.ToInt64(Session["P2POrderID"]);
            if (isOutBound && isInBound)
                _masterBooking.UpdateSleeperPriceZero(P2POrderId, 0, 0, true, true);
            else if (isOutBound)
                _masterBooking.UpdateSleeperPriceZero(P2POrderId, 0, 0, true, false);
            else if (isInBound)
                _masterBooking.UpdateSleeperPriceZero(P2POrderId, 0, 0, false, true);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }
}