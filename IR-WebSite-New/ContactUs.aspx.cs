﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class ContactUs : Page
{
    private readonly ManageUser _ManageUser = new ManageUser();
    private readonly ManageEQO _ManageEnquiry = new ManageEQO();
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string SiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private Guid _siteId;
    public string script;
    public string toMail;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                script = new Masters().GetQubitScriptBySId(_siteId);
                if (Page.RouteData.Values["PageId"] != null)
                {
                    var pageID = (Guid)Page.RouteData.Values["PageId"];
                    PageContent(pageID, _siteId);
                    PageBanner(_siteId);

                    #region Seobreadcrumbs
                    var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageID, _siteId);
                    if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                        litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
                    #endregion
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void PageBanner(Guid siteID)
    {
        try
        {
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
            {

                var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pid.ID && x.SiteID == siteID);
                if (result != null)
                {
                    var url = result.Url;
                    var oPage = _master.GetPageDetailsByUrl(url);

                    //Banner
                    string[] arrListId = oPage.BannerIDs.Split(',');
                    var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                    var list = _master.GetBannerImgByID(idList);
                    int countdata = list.Count();
                    int index = new Random().Next(countdata);
                    var newlist = list.Skip(index).FirstOrDefault();
                    if (newlist != null && newlist.ImgUrl != null)
                        img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                    else
                        img_Banner.Visible = false;
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void PageContent(Guid pageID, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _master.GetPageDetailsByUrl(url);
                ContentHead.InnerHtml = oPage.PageHeading;
                ContentText.InnerHtml = oPage.PageContent;
                ContactPanel.InnerHtml = oPage.ContactPanel.Replace("CMSImages", SiteUrl + "CMSImages");

                db_1TrackEntities db = new db_1TrackEntities();
                bool IsStaSite = db.tblSites.Any(x => x.ID == siteId && x.IsSTA == true);
                if (IsStaSite)
                {
                    callBlock.Visible = false;
                }
                else
                    callBlock.InnerHtml = oPage.ContactCall.Replace("CMSImages", SiteUrl + "CMSImages");
            }
            else
            {
                callBlock.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            var enableEmail = _master.EnableContactEmail(_siteId);
            if (enableEmail)
                SendContactEmail(_siteId);

            Guid user = new Guid();

            tblUserLogin oreg = new tblUserLogin()
            {
                ID = Guid.NewGuid(),
                FirstName = txtName.Text,
                LastName = string.Empty,
                Email = txtEmail.Text,
                Password = "12345678",
                Phone = txtPhn.Text,
                Country = _db.tblSites.FirstOrDefault(x => x.ID == _siteId).DefaultCountryID.Value,
                SiteId = _siteId,
                IsActive = true,
                IsContactUsUser = true,
                IsEQOUser = false
            };
            user = _ManageUser.SaveEQOcontactusUser(oreg);

            _master.AddContact(new tblContact
            {
                ID = Guid.NewGuid(),
                SiteID = _siteId,
                Name = txtName.Text.Trim(),
                Email = txtEmail.Text.Trim(),
                Phone = txtPhn.Text.Trim(),
                OrderNumber = txtOrderNo.Text.Trim(),
                Description = txtDesp.Text.Trim()
            });

            ShowMessage(1, "Your comment/query has been sent successfully.");
            ClearData();
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            ShowMessage(2, ex.Message);
        }
    }

    public void SendContactEmail(Guid siteid)
    {
        try
        {
            const string Subject = "User Comment/Query";
            var body = "<html><head><title></title></head><body><p> User Comment/Query " +
                          "<br /> <br /> User has send comment/query. <br />" +
                          "<br /> User Name : " + txtName.Text.Trim() + "<br />	Email : " + txtEmail.Text.Trim() + "" +
                          "<br /> Phone : " + txtPhn.Text.Trim() + "<br /> Order Number : " + txtOrderNo.Text.Trim() +
                          "<br/> Comment/Query : " + txtDesp.Text.Trim() +
                          "<br /><br />Thanks </p></body></html>";

            if (!String.IsNullOrEmpty(hdnEmail.Value))
            {
                var sendEmail = _master.SendContactUsMail(siteid, Subject, body, txtEmail.Text.Trim(), hdnEmail.Value);
                if (sendEmail)
                {
                    ShowMessage(1, "Thank you for your interest in International Rail. Your comment/query has been sent successfully. This information will enable us to route your request to the appropriate person. You should receive a response soon.");
                }
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
            ShowMessage(2, ex.Message);
        }
    }

    private void ClearData()
    {
        txtName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhn.Text = string.Empty;
        txtOrderNo.Text = string.Empty;
        txtDesp.Text = string.Empty;
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}