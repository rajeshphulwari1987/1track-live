﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Text;
using System.Xml;
using System.Globalization;

public partial class User_Registration : System.Web.UI.Page
{
    private readonly ManageTrainDetails _master = new ManageTrainDetails();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private readonly ManageUser _ManageUser = new ManageUser();
    private readonly Masters _objmaster = new Masters();
    private readonly Common _Common = new Common();
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private string htmfile = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                FillDayMonthYear();
                ddlCountry.DataSource = _master.GetCountryDetail();
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("Select Country", "0"));
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillDayMonthYear()
    {
        var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
        ddlDay.Items.Insert(0, new ListItem("Day", "0"));
        ddlMonth.Items.Insert(0, new ListItem("Month", "0"));
        ddlYear.Items.Insert(0, new ListItem("Year", "0"));
        for (int i = 01; i <= 31; i++)
            ddlDay.Items.Add(i.ToString());
        for (int i = 0; i < months.Length-1; i++)
            ddlMonth.Items.Add(new ListItem(months[i], i.ToString()));
        for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 100; i--)
            ddlYear.Items.Add(i.ToString());
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            tblUserLogin oreg = new tblUserLogin()
            {
                ID = Guid.NewGuid(),
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Email = txtEmail.Text,
                Password = txtPassword.Text,
                Address = txtLine1.Text,
                Address2 = txtLine2.Text,
                Phone = txtPhone.Text,
                City = txtTown.Text,
                PostCode = txtPostcode.Text,
                State = txtstate.Text,
                IsNewsEmail = chkNewsEmail.Checked,
                Country = Guid.Parse(ddlCountry.SelectedValue),
                SiteId = _siteId,
                IsActive = true
            }; 
            oreg.Dob = Convert.ToDateTime(ddlDay.SelectedItem.Text.Trim() + "/" + ddlMonth.SelectedItem.Text.Trim() + "/" + ddlYear.SelectedItem.Text.Trim());

            var user = _ManageUser.SaveUser(oreg);
            if (user != new Guid())
            {
                var siteName = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                htmfile = Server.MapPath("~/MailTemplate/CRMEmailTemplate.htm");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                if (siteName.IsSTA.HasValue ? siteName.IsSTA.Value : false)
                    body = body.Replace("##headercolor##", "#0c6ab8");
                else
                    body = body.Replace("##headercolor##", "#941e34");
                body = body.Replace("##siteimagepath##", LoadSiteLogo());
                body = body.Replace("##Heading##", "User account has been successfully registered at " + (siteName != null ? siteName.DisplayName : "Internationalrail"));
                body = body.Replace("##UserName##", oreg.Email);
                body = body.Replace("##Password##", oreg.Password);
                body = body.Replace("##Description##", "Please login your account by clicking this link :- <a href='" + siteURL + "User/Login'>Click here</a>");
                body = body.Replace("#Blank#", "&nbsp;");

                string Subject = "User Registration";
                var emailSetting = _objmaster.GetEmailSettingDetail(_siteId);
                if (emailSetting != null)
                {
                    var sendEmail = _objmaster.SendContactUsMail(_siteId, Subject, body, emailSetting.Email, txtEmail.Text);
                    if (sendEmail)
                    {
                        ResetData();
                        Response.Redirect(siteURL + "User/success.aspx");
                    }
                }
            }
            else
                ShowMessage(2, "We've been unable to register an account with this email address as it appears you’re lucky enough to already have one! If you can't remember your password, you can reset you it here. <a href='ForgotPassword'> Forgotten password.</a>");
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public string LoadSiteLogo()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }

    public void ResetData()
    {
        txtFirstName.Text = txtLastName.Text = txtPhone.Text = txtLine1.Text = txtTown.Text = txtEmail.Text = txtstate.Text= txtPostcode.Text = txtLine2.Text = "";
        ddlCountry.SelectedIndex = 0;
        ddlDay.SelectedIndex = 0;
        ddlMonth.SelectedIndex = 0;
        ddlYear.SelectedIndex = 0;

    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.InnerHtml = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.InnerHtml = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.InnerHtml = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}