﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Xml;
using System.Net.Mail;
using System.Net;

public partial class User_ForgotPassword : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    readonly Masters _master = new Masters();
    private readonly ManageUser _ManageUser = new ManageUser();
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private string htmfile = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            var result = _ManageUser.GetUserByEmailAndSiteID(txtEmail.Text.Trim(), _siteId);
            if (result != null)
            {
                var siteName = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                htmfile = Server.MapPath("~/MailTemplate/CRMEmailTemplate.htm");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                if (siteName.IsSTA.HasValue ? siteName.IsSTA.Value : false)
                    body = body.Replace("##headercolor##", "#0c6ab8");
                else
                    body = body.Replace("##headercolor##", "#941e34");
                body = body.Replace("##siteimagepath##", LoadSiteLogo());
                body = body.Replace("##Heading##", "Your login details are as below ");
                body = body.Replace("##UserName##", result.Email);
                body = body.Replace("##Password##", result.Password);
                body = body.Replace("##Description##", "Please login your account by clicking this link :- <a href='" + siteURL + "User/Login'>Click here</a>");
                body = body.Replace("#Blank#", "&nbsp;");

                string Subject = "Forgotten password";
                var emailSetting = _master.GetEmailSettingDetail(_siteId);
                if (emailSetting != null)
                {
                    var sendEmail = SendForgatPasswordMail(_siteId, Subject, body, emailSetting.Email, txtEmail.Text.Trim());
                    if (sendEmail)
                    {
                        txtEmail.Text = "";
                        ShowMessage(1, "Password has been sent to your registered email address.");
                    }
                }
            }
            else
            {
                ShowMessage(2, "User is not registered! please registered first <a href='Registration'>click here</a>");
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public bool SendForgatPasswordMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail)
    {
        try
        {
            var masterPage = new Masters();
            var message = new MailMessage();
            var smtpClient = new SmtpClient();
            var result = masterPage.GetEmailSettingDetail(siteId);
            if (result != null)
            {
                var fromAddres = new MailAddress(FromEmail, FromEmail);
                smtpClient.Host = result.SmtpHost;
                smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                message.From = fromAddres;
                message.To.Add(ToMail);
                message.Subject = Subject;
                message.IsBodyHtml = true;
                message.Body = Body;
                smtpClient.Send(message);
                return true;
            }
            else
                return false;
        }
        catch (Exception ee)
        {
            return false;
        }
    }

    public string LoadSiteLogo()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}