﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;
using OneHubServiceRef;

public partial class User_Orders : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageOrder _ManageOrder = new ManageOrder();
    private Int32 TotalRecord = 1;
    public int nxtpaging = 0;
    readonly db_1TrackEntities db = new db_1TrackEntities();
    private const int pageSize = 20;
    public bool IsAgent = false;
    public int OrderNo = 0;
    public string Currency = "$";
    FrontEndManagePass oManageClass = new FrontEndManagePass();
    public Guid currencyID = new Guid();
    ManageOneHub _ManageOneHub = new ManageOneHub();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsAgent == true);
            if (objsite != null)
            {
                IsAgent = true;
                if (AgentuserInfo.UserID == Guid.Empty)
                    Response.Redirect("~/agent/login");
            }
            else if (USERuserInfo.ID == Guid.Empty)
                Response.Redirect("~/user/login");

            if (AgentuserInfo.UserID != Guid.Empty || USERuserInfo.ID != Guid.Empty)
            {
                GetCurrencyCode();
                GetOrderList(FromDate(), DateTime.Now);
                BindPager();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetOrderList(DateTime FromDate, DateTime ToDate)
    {
        try
        {
            Guid UserID = Guid.Empty;
            if (IsAgent)
            {
                var ValidUser = db.tblAdminUserLookupSites.FirstOrDefault(o => o.SiteID == _siteId);
                bool IsValid = ValidUser != null ? true : false;
                UserID = _db.tblAdminUsers.FirstOrDefault(x => x.ID == AgentuserInfo.UserID && IsValid).ID;
            }
            else
                UserID = _db.tblUserLogins.FirstOrDefault(x => x.Email == USERuserInfo.UserEmail && x.SiteId == _siteId).ID;
            if (UserID != Guid.Empty)
            {
                int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
                string Status = ddlOrders.SelectedValue != "0" ? ddlOrders.SelectedItem.Text : string.Empty;
                var list = _ManageOrder.GetCRMOrderDetailList(_siteId, UserID, FromDate, ToDate, pageIndex, pageSize);
                var DataRecord = list.FirstOrDefault();
                if (DataRecord != null)
                {
                    TotalRecord = DataRecord.TOTALRECORD.Value;
                    div_Paging.Visible = pageSize < TotalRecord ? true : false;
                }
                grdOrder.DataSource = list;
                grdOrder.DataBind();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void ddlOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["PageIndex"] = 1;
            GetOrderList(FromDate(), DateTime.Now);
            BindPager();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private DateTime FromDate()
    {
        if (ddlOrders.SelectedValue == "1")
            return DateTime.Now.AddDays(-30);
        else if (ddlOrders.SelectedValue == "2")
            return DateTime.Now.AddMonths(-6);
        else if (ddlOrders.SelectedValue == "3")
            return DateTime.Now.AddYears(-1);
        else if (ddlOrders.SelectedValue == "4")
            return DateTime.Parse("1/1/2014");
        return default(DateTime);
    }

    public void GetCurrencyCode()
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                currencyID = (Guid)oManageClass.GetCurrencyDetail(_siteId).DefaultCurrencyID;
                Currency = oManageClass.GetCurrency(currencyID);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    #region Paging

    public class ClsPageCount
    {
        public string PageCount { get; set; }
    }

    public void BindPager()
    {
        try
        {
            int no = 1;
            var oPageList = new List<ClsPageCount>();
            int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
            int newpagecount = (TotalRecord - 1) / pageSize + (((TotalRecord - 1) % pageSize) > 0 ? 1 : 0);
            if (nxtpaging > 0)
                pageIndex = no = nxtpaging;
            else
                no = pageIndex > 10 ? (((pageIndex - 1) / 10) * 10) + 1 : no;
            if (no > 10)
                lnkPrepaging.Visible = true;
            else
                lnkPrepaging.Visible = false;
            lnkPrepaging.Attributes.Add("newpage", (no > 1 ? (no - 1) : 1).ToString());
            for (int i = no; i <= newpagecount; i++)
            {
                if (i == newpagecount || newpagecount < 11)
                    lnknextpaging.Visible = false;
                else
                    lnknextpaging.Visible = true;
                var oPage = new ClsPageCount { PageCount = i.ToString() };
                if ((i % 10) == 0)
                {
                    oPageList.Add(oPage);
                    lnknextpaging.Attributes.Add("newpage", (i + 1).ToString());
                    i = newpagecount;
                }
                else
                    oPageList.Add(oPage);
            }
            if (newpagecount <= 1)
            {
                lnknextpaging.Visible = lnkPrepaging.Visible = lnkPrevious.Visible = lnkNext.Visible = false;
                litTotalPages.Text = "";
            }
            else
            {
                if (pageIndex == 1)
                    lnkPrevious.Visible = false;
                else
                    lnkPrevious.Visible = true;
                if (pageIndex == newpagecount)
                    lnkNext.Visible = false;
                else
                    lnkNext.Visible = true;
                litTotalPages.Text = "Showing page " + CurrentPage + " of " + newpagecount;
            }
            DLPageCountItem.DataSource = oPageList;
            DLPageCountItem.DataBind();
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex).ToString(CultureInfo.InvariantCulture))
                    lblPage.Attributes.Add("class", "activepaging");
                else
                    lblPage.Attributes.Remove("class");
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void lnkPrepaging_Click(object sender, EventArgs e)
    {
        CurrentPage = Convert.ToInt32(lnkPrepaging.Attributes["newpage"]);
        GetOrderList(FromDate(), DateTime.Now);
        BindPager();
    }

    protected void lnknextpaging_Click(object sender, EventArgs e)
    {
        CurrentPage = nxtpaging = Convert.ToInt32(lnknextpaging.Attributes["newpage"]);
        GetOrderList(FromDate(), DateTime.Now);
        BindPager();
    }

    protected void lnkPage_Command(object sender, CommandEventArgs e)
    {
        int pageIndex = Convert.ToInt32(e.CommandArgument);
        ViewState["PageIndex"] = pageIndex;
        GetOrderList(FromDate(), DateTime.Now);
        BindPager();
    }

    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        CurrentPage = CurrentPage - 1;
        GetOrderList(FromDate(), DateTime.Now);
        BindPager();
    }

    protected void lnkNext_Click(object sender, EventArgs e)
    {
        CurrentPage = CurrentPage + 1;
        GetOrderList(FromDate(), DateTime.Now);
        BindPager();
    }

    private int CurrentPage
    {
        get
        {
            return ((ViewState["PageIndex"] == null || ViewState["PageIndex"].ToString() == "") ? 1 : int.Parse(ViewState["PageIndex"].ToString()));
        }
        set
        {
            ViewState["PageIndex"] = value;
        }
    }
    #endregion

    protected void grdOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgBtnApproved = e.Row.FindControl("imgBtnApproved") as ImageButton;
                Image imgBtnCreaded = e.Row.FindControl("imgBtnCreaded") as Image;
                ImageButton imgBtnBucket = e.Row.FindControl("imgBtnBucket") as ImageButton;
                HiddenField hdnStatus = e.Row.FindControl("hdnStatus") as HiddenField;
                if (hdnStatus.Value == "Order Created")
                {
                    imgBtnCreaded.Visible = imgBtnBucket.Visible = true;
                    imgBtnApproved.Visible = false;
                }
                else if (hdnStatus.Value == "Approved" || hdnStatus.Value == "Completed" || hdnStatus.Value == "On Hold (After Approved)" || hdnStatus.Value == "Refund Complete" || hdnStatus.Value == "Approval Needed")
                {
                    imgBtnCreaded.Visible = imgBtnBucket.Visible = false;
                    imgBtnApproved.Visible = true;
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void imgBtnBucket_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            GridViewRow grdrow = (GridViewRow)((ImageButton)sender).NamingContainer;
            var hdnOrderId = grdrow.FindControl("hdnOrderId") as HiddenField;
            var hdnPRODUCTTYPE = grdrow.FindControl("hdnPRODUCTTYPE") as HiddenField;
            if (hdnPRODUCTTYPE.Value == "P2P")
            {
                Session["P2POrderID"] = hdnOrderId.Value;
                Session["OrderID"] = null;
                Response.Redirect("~/Home");
            }
            else
            {
                var list = new ManageBooking().GetUserOrderInCreateStatusByOrderID(Convert.ToInt32(hdnOrderId.Value));
                if (list != null && list.Count() > 0)
                {
                    Session.Add("RailPassData", list);
                    Session["OrderID"] = list.FirstOrDefault().OrderIdentity;
                    Session["P2POrderID"] = null;
                    Response.Redirect("~/bookingcart");
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void imgBtnApproved_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            GridViewRow grdrow = (GridViewRow)((ImageButton)sender).NamingContainer;
            var hdnOrderId = grdrow.FindControl("hdnOrderId") as HiddenField;
            Response.Redirect("OrderReceipt?ID=" + hdnOrderId.Value);
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }
}