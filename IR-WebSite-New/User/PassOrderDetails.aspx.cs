﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

public partial class User_PassOrderDetails : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageOrder _ManageOrder = new ManageOrder();
    public string Currency = "$";
    private readonly ManageBooking Objbooking = new ManageBooking();
    readonly db_1TrackEntities db = new db_1TrackEntities();
    FrontEndManagePass oManageClass = new FrontEndManagePass();
    public int OrderNo = 0;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsAgent == true);
                if (objsite != null)
                {
                    useronly.Visible = false;
                    if (AgentuserInfo.UserID == Guid.Empty)
                        Response.Redirect("~/agent/login");
                }
                else if (USERuserInfo.ID == Guid.Empty)
                    Response.Redirect("~/user/login");                

                GetCurrencyCode();
                if (Request["id"] != null)
                {
                    OrderNo = Convert.ToInt32(Request["id"]);
                    lnkcancelorder.HRef="OrderCancel?id="+OrderNo;
                    if (db.tblUseCancelOrders.Any(x => x.OrderId == OrderNo))
                        lnkcancelorder.InnerText = "Display cancel order status";
                    else
                        lnkcancelorder.InnerText = "Cancel Order";
                    FillPassDetails(OrderNo);
                }
            }
            
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetCurrencyCode()
    {
        try
        {
            if (Request["id"] != null)
            {
                OrderNo = Convert.ToInt32(Request["id"]);
                tblOrder objtblord = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderNo);
                if (objtblord != null)
                {
                    var currencyID = _db.tblSites.FirstOrDefault(x => x.ID == objtblord.SiteID);
                    if (currencyID != null)
                        Currency = oManageClass.GetCurrency((Guid)currencyID.DefaultCurrencyID);
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillPassDetails(long OrderID)
    {
        try
        {
            var OrderType = _db.tblOrders.FirstOrDefault(x => x.OrderID == OrderID);
            if (OrderType.Status == 6)
                useronly.Visible = false;
            if (OrderType.TrvType == "Pass")
            {
                //Pass Details
                div_AmexFee.Visible = div_Commission.Visible = fieldset_Journey.Visible = div_P2PGrandTotal.Visible = false;
                var prdPass = _ManageOrder.GetPassSale(OrderID);
                grdPass.DataSource = prdPass.OrderBy(s => s.OrderIdentity).ThenBy(x => x.RailPassName).ThenBy(ty => ty.CountryName).ThenBy(ty => ty.PassDesc).ThenBy(ty => ty.TrvClass).ThenBy(ty => ty.StockNumber);
                grdPass.DataBind();

                //Order Details
                var _order = _ManageOrder.GetOrderById(OrderID);
                if (_order != null)
                {
                    decimal netTotal = _ManageOrder.GetOrderPriceSum(OrderID);
                    decimal bookingFee = _order.BookingFee;
                    decimal TicketProtectionCharge = prdPass.Sum(ty => ty.TicketProtection);
                    decimal Extracharge = Objbooking.GetExtraChargeByOrderId(OrderID);
                    lblOrderID.Text = _order.OrderID.ToString(CultureInfo.InvariantCulture);
                    lblStatus.Text = _order.OrderStatus;
                    txtOrdCDate.Text = _order.CreatedOn.ToString("dd MMM yyyy");
                    lblShippingCost.Text = _order.ShippingAmount != null ? _order.ShippingAmount.ToString() : "0.00";
                    lblDiscount.Text = _ManageOrder.GetOrderDiscountByOrderId(OrderID);
                    lblBookindFee.Text = bookingFee.ToString();
                    lblAdminFee.Text = _order.AdminFee.ToString("F");
                    lblTckProtection.Text = TicketProtectionCharge.ToString(CultureInfo.InvariantCulture);
                    txtDepDt.Text = _order.DateOfDepart != null ? Convert.ToDateTime(_order.DateOfDepart).ToString("dd MMM yyyy") : (_order.PassStartDate != null ? Convert.ToDateTime(_order.PassStartDate).ToString("dd MMM yyyy") : null);
                    tblProductGrandTotal.Text = ((netTotal + Convert.ToDecimal(lblShippingCost.Text) + bookingFee + TicketProtectionCharge) - (Convert.ToDecimal(lblDiscount.Text) + _order.AdminFee + Extracharge)).ToString("F");
                    lblGrossTotal.Text = (netTotal).ToString("F");
                    string eurailData = _ManageOrder.GetEurailData(_order.OrderID).ToString("F");
                    string NonEurail = _ManageOrder.GetNonEurailData(_order.OrderID).ToString("F");
                    lblNetPrice.Text = (Convert.ToDecimal(eurailData) + Convert.ToDecimal(NonEurail)).ToString("F");
                }

                //Traveller Details
                var dataprdTrv = _ManageOrder.GetTraveller(OrderID);
                var prdTrv = dataprdTrv.Select(ty => new
                {
                    ID = ty.ID,
                    TicketNumber = ty.TicketNumber,
                    Country = ty.Country,
                    Title = ty.Title,
                    FirstName = ty.FirstName,
                    LastName = ty.LastName,
                    DOB = ty.DOB,
                    CountryName = ty.CountryName,
                    PassportNo = ty.PassportNo,
                    Nationality = ty.Nationality,
                    PassStartDate = ty.PassStartDate,
                    LeadPassenger = ty.LeadPassenger,
                    StockNo = prdPass.FirstOrDefault(v => v.ID == ty.PassSalesId).StockNumber,
                    OrderIdentity = ty.OrderIdentity
                }).OrderBy(ty => ty.OrderIdentity).ToList();
                grdPassTraveller.DataSource = prdTrv;
                grdPassTraveller.DataBind();
            }
            else if (OrderType.TrvType == "P2P")
            {
                //Order Details
                fieldset_Pass.Visible = div_DeptDate.Visible = div_ProductGrossTotal.Visible = div_ProductGrandTotal.Visible = false; ;
                var lst = new ManageBooking().GetP2PSaleDetail(OrderID).OrderBy(t => t.ShortOrder).ToList();
                if (lst != null && lst.Count() > 0 && lst.FirstOrDefault().TrvType == "P2P")
                {
                    var data = lst.FirstOrDefault();
                    decimal BookingFee = data.BookingFee;
                    decimal Commission = lst.Sum(t => t.CommissionFee);
                    decimal NetPriceValue = (lst.Sum(t => t.NetPrice ?? 0) - Commission);
                    decimal AmexPrice = (decimal)data.AmexPrice;
                    decimal NetPrice = NetPriceValue;
                    decimal GrossPrice = lst.Sum(t => t.NetPrice ?? 0);
                    decimal TicketProtection = lst.Sum(t => t.TicketProtection ?? 0);
                    decimal Discount = Convert.ToDecimal(_ManageOrder.GetOrderDiscountByOrderId(OrderID));
                    decimal Extracharge = Objbooking.GetExtraChargeByOrderId(OrderID);
                    decimal GrandTotal = (GrossPrice + TicketProtection + BookingFee + data.ShippingAmount + AmexPrice) - (Discount + Extracharge + data.AdminFee);
                    lblOrderID.Text = data.OrderID.ToString();
                    lblStatus.Text = data.StatusName;
                    txtOrdCDate.Text = data.CreatedOn.ToString("dd MMM yyyy");
                    lblShippingCost.Text = data.ShippingAmount.ToString("F");
                    lblAmexPrice.Text = data.AmexPrice.ToString("F");
                    lblBookindFee.Text = data.BookingFee.ToString("F");
                    lblCommission.Text = data.CommissionFee.ToString("F");
                    lblTckProtection.Text = data.TicketProtection.HasValue ? data.TicketProtection.Value.ToString("F") : "0.00";
                    lblNetPrice.Text = GrossPrice.ToString("F");
                    lblP2PGrandTotal.Text = GrandTotal.ToString("F");
                    lblDiscount.Text = Discount.ToString("F");
                    lblAdminFee.Text = data.AdminFee.ToString("F");
                }

                //Traveller Details
                grdP2PTraveller.DataSource = lst;
                grdP2PTraveller.DataBind();

                //Journey Details
                var datalist = lst.Where(t => t.ID != Guid.Empty).Select(t => new
                {
                    Id = t.ID,
                    TrainNo = t.TrainNo,
                    From = t.From,
                    To = t.To,
                    DateTimeDepature = t.DateTimeDepature,
                    DateTimeArrival = t.DateTimeArrival,
                    Passenger = t.Passenger,
                    Symbol = t.Symbol,
                    NetPrice = t.NetPrice,
                    DepartureTime = t.DepartureTime,
                    ArrivalTime = t.ArrivalTime,
                    Class = t.Class,
                    FareName = t.FareName,
                    ReservationCode = t.ReservationCode,
                    PinNumber = t.PinNumber,
                    PassP2PSaleID = t.PassP2PSaleID,
                    DeliveryOption = t.DeliveryOption,
                    IsNotRefundedFull = !_ManageOrder.IsHundradPercentRefund(t.ID, OrderID),
                    PdfURL = !string.IsNullOrEmpty(t.ReservationCode) ? (t.ReservationCode.Length == 7 ? _ManageOrder.GetBENEPdfURLByOrderId(Convert.ToInt32(OrderID.ToString())) : t.PdfURL) : string.Empty,
                    IsBENE = !string.IsNullOrEmpty(t.ReservationCode) ? (t.ReservationCode.Length == 7 ? true : false) : false,
                    Terms = Server.HtmlDecode(t.terms),
                    Fipnumber = t.Fipnumber,
                    FIPClass = t.FIPClass
                }).ToList();
                if (datalist.Count == 1 && datalist.Any(t => t.Id == Guid.Empty))
                    datalist = null;
                rptJourney.DataSource = datalist;
                rptJourney.DataBind();
            }

            //Billing And Shipping Address
            var billing = _ManageOrder.GetBillingInfo(OrderID);
            if (billing != null)
            {
                rptBilling.DataSource = rptShipping.DataSource = billing;
                rptBilling.DataBind();
                rptShipping.DataBind();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}