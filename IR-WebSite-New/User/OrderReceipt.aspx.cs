﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Business;

public partial class User_OrderReceipt : System.Web.UI.Page
{
    static long id;
    static string currency = "$";
    private readonly ManageOrder _master = new ManageOrder();
    private readonly ManageBooking _masterBooking = new ManageBooking();
    private Guid _siteId;
    public string siteURL;
    private readonly db_1TrackEntities db = new db_1TrackEntities();
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string address = "<br/>International Rail Ltd<br/> P O Box 153, Alresford, SO24 4AQ<br/><br/>Tel: +44 (0) 871 231 0790<br/>Email: sales@internationalrail.com";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["id"] != null)
            {
                if (!Int64.TryParse(Request.QueryString["id"].Trim(), out id))
                    Response.Redirect("~/User/Orders.aspx");
            }
            if (!IsPostBack)
            {
                if (Int64.TryParse(Request.QueryString["id"].Trim(), out id))
                    FillOrderInfo();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void FillOrderInfo()
    {
        try
        {
            tblOrder objO = db.tblOrders.Where(a => a.OrderID == id).FirstOrDefault();
            if (objO != null)
            {
                var st = db.tblSites.FirstOrDefault(x => x.ID == objO.SiteID);
                if (st != null)
                {
                    imgLogo.Src = LoadSiteLogo();
                    currency = new FrontEndManagePass().GetCurrency(st.DefaultCurrencyID.HasValue ? (st.DefaultCurrencyID.Value) : new Guid());
                    List<GetAllCartData_Result> lst = _masterBooking.GetAllCartData(id).ToList();
                    var lst1 = (from a in lst
                                select new
                                {
                                    Price = (_masterBooking.getPrice(a.PassSaleID, a.ProductType)),
                                    ProductDesc = (_masterBooking.getPassDesc(a.PassSaleID, a.ProductType) + "<br>" + a.FirstName + " " + a.MiddleName + " " + a.LastName),
                                    TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0)
                                }).ToList();
                    if (lst.Count > 0)
                    {
                        lblUserName.Text = lst.FirstOrDefault().DTitle + " " + lst.FirstOrDefault().DFirstName;
                        lblUserSurName.Text = lst.FirstOrDefault().DLastName;
                        lblEmailAddress.Text = lst.FirstOrDefault().EmailAddress;
                        lblDeliveryAddress.Text = lst.FirstOrDefault().Address1 + ", " +
                                                  (!string.IsNullOrEmpty(lst.FirstOrDefault().Address2)
                                                       ? lst.FirstOrDefault().Address2 + ", "
                                                       : string.Empty) + "<br>" +
                                                  (!string.IsNullOrEmpty(lst.FirstOrDefault().City)
                                                       ? lst.FirstOrDefault().City + ", " +
                                                         lst.FirstOrDefault().Postcode + ", <br>"
                                                       : string.Empty) +
                                                  (!string.IsNullOrEmpty(lst.FirstOrDefault().State)
                                                       ? lst.FirstOrDefault().State + ",<br> "
                                                       : string.Empty) + lst.FirstOrDefault().DCountry;
                        lblOrderDate.Text = lst.FirstOrDefault().CreatedOn.ToString("dd/MMM/yyyy");
                        lblOrderNumber.Text = lst.FirstOrDefault().OrderID.ToString();
                        lblTotal.Text = currency + (lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge)).ToString();
                        lblShipping.Text = currency + (lst.FirstOrDefault().ShippingAmount != null ? lst.FirstOrDefault().ShippingAmount.ToString() : "0.00");
                        lblBookingFee.Text = currency + (lst.FirstOrDefault().BookingFee != null ? lst.FirstOrDefault().BookingFee.ToString() : "0.00");

                        decimal AdminFee = _masterBooking.GetAdminFeeByOrderId(id);
                        decimal ExtraCharge = _masterBooking.GetExtraChargeByOrderId(id);
                        decimal DiscountFee = Convert.ToDecimal(_master.GetOrderDiscountByOrderId(id));
                        if (lst.FirstOrDefault().ProductType == "P2P")
                            tr_P2PRefund.Visible = true;
                        else
                            tr_P2PRefund.Visible = false;
                        lblP2PRefund.Text = currency + _masterBooking.GetP2PRefundAmount(id);
                        lblAdminFee.Text = currency + AdminFee;
                        lblDiscount.Text = currency + _master.GetOrderDiscountByOrderId(id);
                        lblOrderTotal.Text = currency + (((lst1.Sum(a => a.Price) + lst1.Sum(a => a.TktPrtCharge) + (lst.FirstOrDefault().ShippingAmount == null ? 0 : lst.FirstOrDefault().ShippingAmount) + (lst.FirstOrDefault().BookingFee == null ? 0 : lst.FirstOrDefault().BookingFee)) - (AdminFee + ExtraCharge)) - DiscountFee);
                    }

                    var phoneshpg = db.tblOrderBillingAddresses.FirstOrDefault(x => x.OrderID == objO.OrderID);
                    if (phoneshpg != null)
                        lblPhoneNumber.Text = phoneshpg.PhoneShpg;
                    else
                        lblPhoneNumber.Text = string.Empty;

                    List<GetAllCartData_Result> lstC = _masterBooking.GetAllCartData(id).ToList();
                    var lstNew = (from a in lstC
                                  select
                                      new
                                      {
                                          a,
                                          Price = (_masterBooking.getPrice(a.PassSaleID, a.ProductType)),
                                          ProductDesc =
                                  (_masterBooking.getUserP2PPassDesc(a.PassSaleID, a.ProductType)),
                                          TktPrtCharge = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                          OrderIdentity = a.OrderIdentity,
                                          AdminExtraCharge = _masterBooking.GetExtraCharge(a.PassSaleID.Value, a.ProductType),
                                          OrderRefund = _masterBooking.GetRefundAmountByID(a.PassSaleID.Value, a.LID, a.ProductType)
                                      }).OrderBy(t => t.OrderIdentity).ToList();
                    string strProductDesc = "";
                    int i = 1;
                    if (lstNew.Count() > 0)
                    {
                        foreach (var x in lstNew)
                        {
                            strProductDesc +=
                                "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px;font-weight:bold' valign='top'>Ticket" +
                                i + "</td></tr>";
                            strProductDesc +=
                                "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>" +
                                x.ProductDesc.Replace("Rail Tickets", "") + "</td></tr>";

                            /*Ticket Protection*/
                            strProductDesc +=
                                "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px;' valign='top'>Ticket Protection:</td><td width='50%' style='font-size: 12px' valign='top'>" +
                                currency + " " + x.TktPrtCharge.ToString("F") + "</td></tr>";
                            /*Cxl/Extra Charge*/
                            strProductDesc +=
                                "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px;' valign='top'>Cxl/Extra Charge:</td><td width='50%' style='font-size: 12px' valign='top'>" +
                                currency + " " + x.AdminExtraCharge.ToString("F") + "</td></tr>";
                            /*Price*/
                            strProductDesc +=
                                "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px;' valign='top'>Price:</td><td width='50%' style='font-size: 12px' valign='top'>" +
                                currency + " " + x.Price.ToString() + "</td></tr>";
                            /*pass refund*/
                            strProductDesc +=
                              "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px;' valign='top'>Refund Price:</td><td width='50%' style='font-size: 12px' valign='top'>" +
                              currency + " " + x.OrderRefund.ToString("F") + "</td></tr>";

                            if (x.a.PassStartDate != null)
                                strProductDesc +=
                                    "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Pass Start Date:</td><td width='50%' style='font-size: 12px' valign='top'>" +
                                    Convert.ToDateTime(x.a.PassStartDate).ToString("dd/MMM/yyyy") + "</td></tr>";
                            strProductDesc +=
                                "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Passenger Name:</td><td width='50%' style='font-size: 12px' valign='top'>" + x.a.FirstName + " " + x.a.MiddleName + " " + x.a.LastName + "</td></tr>";
                            if (x.a.DOB != null)
                                strProductDesc +=
                                    "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Date of Birth:</td><td width='50%' style='font-size: 12px' valign='top'>" +
                                    Convert.ToDateTime(x.a.DOB).ToString("dd/MMM/yyyy") + "</td></tr>";
                            if (x.a.Country != null && x.a.ProductType == "Pass")
                                strProductDesc += "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Country of residence:</td><td width='50%' style='font-size: 12px' valign='top'>" + GetCountry((Guid)x.a.Country) + "</td></tr>";
                            else
                            {
                                var ClassName = _masterBooking.GetP2PSaleDataById(x.a.PassSaleID.Value);
                                strProductDesc += "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Class:</td><td width='50%' style='font-size: 12px' valign='top'>" + (ClassName != null ? ClassName.Class : "") + "</td></tr>";
                            }

                            if (x.a.ProductType == "Pass")
                                strProductDesc += "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Passport Number:</td><td width='50%' style='font-size: 12px' valign='top'>" + x.a.PassportNo + "</td></tr>";
                            else
                                strProductDesc += "<tr style='font-family: sans-serif;'><td width='50%' style='font-size: 12px' valign='top'>Ticket Type:</td><td width='50%' style='font-size: 12px' valign='top'>" + x.a.P2PType + "</td></tr>";

                            strProductDesc += "<tr><td colspan='3'>&nbsp;</td></tr>";
                            i++;
                        }
                    }
                    litOrderInfo.Text = strProductDesc;
                }
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    string GetCountry(Guid cid)
    {
        tblCountriesMst cont = db.tblCountriesMsts.FirstOrDefault(x => x.CountryID == cid);
        return cont != null ? cont.CountryName : "";
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public string LoadSiteLogo()
    {
        var data = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }
}