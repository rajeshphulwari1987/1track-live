﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="Orders.aspx.cs" Inherits="User_Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .class-main .class-label
        {
            width: 20%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <fieldset class="grid-sec2">
            <legend><strong>Order Details</strong></legend>
            <div class="fieldset-chart">
                <div class="table-res">
                    <div class="class-main">
                        <div class="class-label">
                            <strong>Orders placed in the list: </strong>
                        </div>
                        <div class="class-text">
                            <asp:DropDownList ID="ddlOrders" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOrders_SelectedIndexChanged">
                                <asp:ListItem Value="1">Last 30 days</asp:ListItem>
                                <asp:ListItem Value="2">Last 6 months</asp:ListItem>
                                <asp:ListItem Value="3">Last year</asp:ListItem>
                                <asp:ListItem Value="4">All orders</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="fieldset-chart">
                <div class="table-res">
                    <asp:GridView ID="grdOrder" runat="server" CssClass="grid-head2" AutoGenerateColumns="false"
                        PageSize="10" OnRowDataBound="grdOrder_RowDataBound">
                        <EmptyDataTemplate>
                            Record not found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Order Date">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Order Date: </b>
                                    </div>
                                    <%# Eval("CREATEDON", "{0: dd/MMM/yy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order ID">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Order ID: </b>
                                    </div>
                                    <%#Eval("ORDERID")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order Total">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Order Total: </b>
                                    </div>
                                    <%=Currency%>
                                    <%#Eval("PRICE")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Status: </b>
                                    </div>
                                    <%#Eval("STATUSNAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Receipt">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%#Eval("STATUSNAME")%>' />
                                    <asp:HiddenField ID="hdnOrderId" runat="server" Value='<%#Eval("ORDERID")%>' />
                                    <asp:HiddenField ID="hdnPRODUCTTYPE" runat="server" Value='<%#Eval("PRODUCTTYPE")%>' />
                                    <asp:Image ID="imgBtnCreaded" Visible="false" runat="server" CssClass="hidemobile"
                                        Height="30px" Width="30px" ImageUrl="../images/minus-icon.png"></asp:Image>
                                    <asp:ImageButton Visible="false" runat="server" ID="imgBtnApproved" CssClass="hidemobile"
                                        Height="30px" Width="30px" CommandArgument='<%#Eval("ORDERID")%>' CommandName="Approved"
                                        ImageUrl="../images/cmsclick.png" OnClick="imgBtnApproved_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton Visible="false" ID="imgBtnBucket" runat="server" CssClass="hidemobile"
                                        Height="30px" Width="30px" CommandArgument='<%#Eval("ORDERID")%>' CommandName="Bucket"
                                        ImageUrl="../images/cart-icon.png" OnClick="imgBtnBucket_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div id="div_Paging" runat="server">
                        <table class="tablle-paging">
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkPrevious" Text='<< Previous' runat="server" OnClick="lnkPrevious_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrepaging" Text='...' runat="server" OnClick="lnkPrepaging_Click" />
                                </td>
                                <asp:Repeater ID="DLPageCountItem" runat="server">
                                    <ItemTemplate>
                                        <td class="tdpaging">
                                            <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <td>
                                    <asp:LinkButton ID="lnknextpaging" Text='...' runat="server" OnClick="lnknextpaging_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkNext" Text='Next >>' runat="server" OnClick="lnkNext_Click" />
                                </td>
                                <td>
                                    <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>
