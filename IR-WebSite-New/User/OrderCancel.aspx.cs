﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Xml;
using System.Text;
using System.Globalization;
using System.Threading;

public partial class User_OrderCancel : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    public string siteURL;
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (USERuserInfo.ID == Guid.Empty)
                Response.Redirect("~/user/login");
            if (!Page.IsPostBack && Request["id"] != null)
            {
                lblOrderNo.Text = Request.Params["id"].ToString();
                GetCommentList(Convert.ToInt64(lblOrderNo.Text));
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            int orderid = Convert.ToInt32(lblOrderNo.Text);
            tblUseCancelOrder _tblUseCancelOrder = new tblUseCancelOrder()
            {
                OrderId = orderid,
                UserId = USERuserInfo.ID,
                IsActive = true,
                Status = 6,
                CreatedOn = DateTime.Now,
                CreatedBy = USERuserInfo.ID
            };
            tblUseCancelOrderChat _tblUseCancelOrderChat = new tblUseCancelOrderChat()
            {
                OrderId = orderid,
                ChatUserId = USERuserInfo.ID,
                Commitment = txtComment.Text.Trim(),
                CreatedOn = DateTime.Now
            };

            bool result = _ManageUser.AddUserCancelOrder(_tblUseCancelOrder, _tblUseCancelOrderChat);
            if (result)
            {
                txtComment.Text = "";
                ShowMessage(1, "Order Comment has been sent successfully.");
                GetCommentList(Convert.ToInt64(lblOrderNo.Text));
                SendEmail(orderid);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    private void GetCommentList(long OrderID)
    {
        try
        {
            rptComment.DataSource = _ManageUser.GetUseCancelOrderChatList(OrderID, _siteId, USERuserInfo.ID).ToList();
            rptComment.DataBind();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("PassOrderDetails?id=" + lblOrderNo.Text);
    }

    public void SendEmail(long OrderID)
    {
        try
        {
            var CommunicationList = _ManageUser.GetUseCancelOrderChatList(OrderID, _siteId, USERuserInfo.ID).ToList();
            if (CommunicationList != null)
            {
                var siteName = _db.tblSites.FirstOrDefault(x => x.ID == _siteId);
                string htmfile = Server.MapPath("~/MailTemplate/CRMCancelEmailTemplate.htm");
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(htmfile);
                var list = xmlDoc.SelectNodes("html");
                string body = list[0].InnerXml.ToString();
                StringBuilder bodycommitment = new StringBuilder();
                if (siteName.IsSTA.HasValue ? siteName.IsSTA.Value : false)
                    body = body.Replace("##headercolor##", "#0c6ab8");
                else
                    body = body.Replace("##headercolor##", "#941e34");
                body = body.Replace("##siteimagepath##", LoadSiteLogo());
                body = body.Replace("#Blank#", "&nbsp;");
                body = body.Replace("##OrderNo##", "Order "+OrderID+" Cancel Request");
                foreach (var data in CommunicationList)
                {
                    string color = (data.IsUser == "clsUser" ? "#cccccc" : "#a1e5ff");
                    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                    TextInfo textInfo = cultureInfo.TextInfo;
                    bodycommitment.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0' style='border: 1px solid #ccc;'><tr>");
                    bodycommitment.Append("<td bgcolor='" + color + "' width='100%' valign='top' style='border: none;margin-left:-10px;padding:5px;font-family: Arial, Helvetica, sans-serif;font-size: 15px;'>");
                    bodycommitment.Append("<div style='font-weight:bold;'>");
                    bodycommitment.Append(textInfo.ToTitleCase(data.Name));
                    bodycommitment.Append("</div><div style='font-size: 11px;'>");
                    bodycommitment.Append(data.CreatedOn.ToString("dd/MMM/yyyy h:mm tt"));
                    bodycommitment.Append("</div></td></tr><tr>");
                    bodycommitment.Append("<td width='100%' valign='top' style='padding:9px; font-family: Arial, Helvetica, sans-serif;line-height: 25px; font-size: 14px; background: white;'>");
                    bodycommitment.Append(data.Commitment.Replace("\r\n", "<br />"));
                    bodycommitment.Append("</td></tr></table><br/>");
                }
                body = body.Replace("##Commitment##", Convert.ToString(bodycommitment));
                string Subject = "Order " + OrderID + " cancel Request";
                var emailSetting = _master.GetEmailSettingDetail(_siteId);
                if (emailSetting != null)
                {
                    _master.SendCancelOrderMail(_siteId, Subject, body, emailSetting.Email, USERuserInfo.UserEmail);
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public string LoadSiteLogo()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.LogoPath))
                return AdminSiteUrl + data.LogoPath;
            else
                return siteURL + "assets/img/branding/logo.png";
        }
        return "https://www.internationalrail.com/images/logo.png";
    }
}