﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OrderDetails.aspx.cs" Inherits="User_OrderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <fieldset class="grid-sec2">
            <legend><strong>Order Details</strong></legend>
            <div class="fieldset-chart">
                <div class="table-res">
                    <div class="class-main">
                    <div class="chk-cancel">
                                <asp:CheckBox runat="server" AutoPostBack="true" ID="chkCancelOrderRequest" 
                                    Text=" Order cancel request list" 
                                    oncheckedchanged="chkCancelOrderRequest_CheckedChanged" />
                            </div>
                            </div>
                    <div class="class-main">
                        <div class="class-label">
                            <strong>Order No : </strong>
                        </div>
                        <div class="class-text">
                            <asp:TextBox ID="txtOrderNO" runat="server" AutoPostBack="True" OnTextChanged="txtOrderNO_TextChanged"></asp:TextBox>
                            
                        </div>                        
                    </div>
                    <div class="class-main">
                        <div class="class-label">
                            <strong>Order Status : </strong>
                        </div>
                        <div class="class-text">
                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="fieldset-chart">
                <div class="table-res">
                    <asp:GridView ID="grdOrder" runat="server" CssClass="grid-head2" AutoGenerateColumns="false"
                        PageSize="10">
                        <EmptyDataTemplate>
                            Record not found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Order ID">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Order ID: </b>
                                    </div>
                                    <%#Eval("ORDERID")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Type: </b>
                                    </div>
                                    <%#Eval("PRODUCTTYPE")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Travellers">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Travellers: </b>
                                    </div>
                                    <%#Eval("TRAVELLERNAME").ToString().Substring(1, Eval("TRAVELLERNAME").ToString().Length - 1)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Status: </b>
                                    </div>
                                    <%#Eval("ORDERSTATUS")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Departure Date">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Departure Date: </b>
                                    </div>
                                    <%# Eval("DATEOFDEPART", "{0: dd/MMM/yy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Date">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Booking Date: </b>
                                    </div>
                                    <%# Eval("CREATEDON", "{0: dd/MMM/yy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="View Details">
                                <ItemTemplate>
                                    <a href='PassOrderDetails?id=<%#Eval("ORDERID")%>' title="">
                                        <img class="hidemobile" src="../images/view.png" title="View" /><lable class="hidedesktop">View</lable></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div id="div_Paging" runat="server">
                        <table class="tablle-paging">
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkPrevious" Text='<< Previous' runat="server" OnClick="lnkPrevious_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrepaging" Text='...' runat="server" OnClick="lnkPrepaging_Click" />
                                </td>
                                <asp:Repeater ID="DLPageCountItem" runat="server">
                                    <ItemTemplate>
                                        <td class="tdpaging">
                                            <asp:LinkButton ID="lnkPage" CommandArgument='<%#Eval("PageCount") %>' CommandName="item"
                                                Text='<%#Eval("PageCount") %>' OnCommand="lnkPage_Command" runat="server" />
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <td>
                                    <asp:LinkButton ID="lnknextpaging" Text='...' runat="server" OnClick="lnknextpaging_Click" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkNext" Text='Next >>' runat="server" OnClick="lnkNext_Click" />
                                </td>
                                <td>
                                    <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>
