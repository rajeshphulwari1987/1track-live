﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OrderReceipt.aspx.cs" Inherits="User_OrderReceipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function printItn() {
            var printContent = document.getElementById('divPrintGrid');
            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var WinPrint = window.open(windowUrl, windowName, 'left=300,top=300,right=500,bottom=500,width=1000,height=500');
            WinPrint.document.write(printContent.innerHTML);
            WinPrint.document.write('<' + '/body' + '><' + '/html' + '>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
    <style type="text/css">
        .print-receipt
        {
            float: right;
            margin-bottom: 2%;
            margin-right: 2%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <fieldset class="grid-sec2">
            <legend><strong>Order Details</strong></legend>
            <div id="divPrintGrid">
                <table style="background-color: #fff; margin-left: 4%;" width="90%" border="0" cellspacing="0"
                    cellpadding="0">
                    <tr class="header">
                        <td>
                            <table border="0" width="90%" cellspacing="0" cellpadding="0" style="border-bottom: 1px solid #999;
                                margin-left: 4%;">
                                <tr>
                                    <td height="90" valign="middle" width="40%">
                                        <img id="imgLogo" runat="server" src="http://www.internationalrail.com/images/logo.png"
                                            alt="" border="0" />
                                    </td>
                                    <td height="90" valign="middle" width="60%" style="font-size: 14px;">
                                        <font style="float: right; padding-right: 30px; font-family: Arial, Helvetica, sans-serif;"
                                            color="#00aeef"><b>Sale Receipt </b></font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-left: 5%;">
                                <tr>
                                    <td valign="top" align="left">
                                        <table width="100%" border="0" cellspacing="3" cellpadding="3" align="left">
                                            <tr>
                                                <td colspan="2" style="font-size: 14px">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Order Information</b></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Name </b></font>
                                                </td>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblUserName" runat="server" /><asp:Label ID="lblUserSurName" runat="server"
                                                            Style="font-weight: bold; margin-left: 3px;" /></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Email Address</b> </font>
                                                </td>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblEmailAddress" runat="server" /></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px; vertical-align: top" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Delivery Address</b> </font>
                                                </td>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblDeliveryAddress" runat="server" /></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Phone Number</b> </font>
                                                </td>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblPhoneNumber" runat="server" /></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Order Summary:</b> </font>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Date</b> </font>
                                                </td>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblOrderDate" runat="server" /></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif"><b>Order#</b></font>
                                                </td>
                                                <td style="font-size: 12px" width="50%">
                                                    <font face="Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblOrderNumber" runat="server" /></font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <br />
                                                    <table width="100%">
                                                        <tr>
                                                            <th align="left">
                                                                Product Description
                                                            </th>
                                                            <th align="left">
                                                                <%--Ticket Protection--%>
                                                            </th>
                                                            <th align="left">
                                                                <%--Price--%>
                                                            </th>
                                                        </tr>
                                                        <asp:Literal ID="litOrderInfo" runat="server"></asp:Literal>
                                                        <tr>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">Total :</font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">
                                                                    <asp:Label ID="lblTotal" runat="server" /></font>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_P2PRefund" runat="server">
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">Api Refund:</font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">
                                                                    <asp:Label ID="lblP2PRefund" runat="server" /></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">Shipping:</font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">
                                                                    <asp:Label ID="lblShipping" runat="server" /></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">Booking Fee:</font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">
                                                                    <asp:Label ID="lblBookingFee" runat="server" /></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">Admin Fee:</font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">
                                                                    <asp:Label ID="lblAdminFee" runat="server" /></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">Discount Amount:</font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif">
                                                                    <asp:Label ID="lblDiscount" runat="server" /></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif"><b>Order Total:</b> </font>
                                                            </td>
                                                            <td style="font-size: 12px" width="50%">
                                                                <font face="Arial, Helvetica, sans-serif"><b>
                                                                    <asp:Label ID="lblOrderTotal" runat="server" /></b> </font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="print-receipt">
                <a href="#" onclick="printItn();">Print Receipt</a>
            </div>
        </fieldset>
    </div>
</asp:Content>
