﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

public partial class User_OrderDetails : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();
    private readonly ManageOrder _ManageOrder = new ManageOrder();
    private Int32 TotalRecord = 1;
    public int nxtpaging = 0;
    readonly db_1TrackEntities db = new db_1TrackEntities();
    private const int pageSize = 20;
    public bool IsAgent = false;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                var objsite = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsAgent == true);
                if (objsite != null)
                {
                    IsAgent = true;
                    if (AgentuserInfo.UserID == Guid.Empty)
                        Response.Redirect("~/agent/login");
                }
                else if (USERuserInfo.ID == Guid.Empty)
                    Response.Redirect("~/user/login");
                BindOrderStatus();
            }
            if (AgentuserInfo.UserID != Guid.Empty || USERuserInfo.ID != Guid.Empty)
            {
                GetOrderList();
                BindPager();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetOrderList()
    {
        try
        {
            Guid UserID =Guid.Empty;
            DateTime d1 = DateTime.ParseExact(DateTime.Now.AddYears(-5).Date.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null);
            DateTime d2 = DateTime.ParseExact(DateTime.Now.Date.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null);
            if (IsAgent)
            {
                var ValidUser = db.tblAdminUserLookupSites.FirstOrDefault(o => o.SiteID == _siteId);
                bool IsValid=ValidUser!=null?true:false;
                UserID = _db.tblAdminUsers.FirstOrDefault(x => x.ID == AgentuserInfo.UserID && IsValid).ID;
            }
            else
                UserID = _db.tblUserLogins.FirstOrDefault(x => x.Email == USERuserInfo.UserEmail && x.SiteId == _siteId).ID;
            if (UserID != Guid.Empty)
            {
                int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
                string Status=ddlStatus.SelectedValue != "0" ? ddlStatus.SelectedItem.Text : string.Empty;
                var list = _ManageOrder.GetUserOrderList(_siteId, UserID, txtOrderNO.Text, pageIndex, pageSize, Status, chkCancelOrderRequest.Checked);
                var DataRecord = list.FirstOrDefault();
                if (DataRecord != null)
                {
                    TotalRecord = DataRecord.TOTALRECORD.Value;
                    div_Paging.Visible = pageSize < TotalRecord ? true : false;
                }
                grdOrder.DataSource = list;
                grdOrder.DataBind();
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void BindOrderStatus()
    {
        try
        {
            ddlStatus.DataSource = new ManageOrder().GetStatusList().OrderBy(a => a.Name).ToList();
            ddlStatus.DataValueField = "ID";
            ddlStatus.DataTextField = "Name";
            ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, new ListItem("Select Order", "0"));
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    #region Paging

    public class ClsPageCount
    {
        public string PageCount { get; set; }
    }

    public void BindPager()
    {
        try
        {
            int no = 1;
            var oPageList = new List<ClsPageCount>();
            int pageIndex = ViewState["PageIndex"] == null ? 1 : int.Parse(ViewState["PageIndex"].ToString());
            int newpagecount = (TotalRecord - 1) / pageSize + (((TotalRecord - 1) % pageSize) > 0 ? 1 : 0);
            if (nxtpaging > 0)
                pageIndex = no = nxtpaging;
            else
                no = pageIndex > 10 ? (((pageIndex - 1) / 10) * 10) + 1 : no;
            if (no > 10)
                lnkPrepaging.Visible = true;
            else
                lnkPrepaging.Visible = false;
            lnkPrepaging.Attributes.Add("newpage", (no > 1 ? (no - 1) : 1).ToString());
            for (int i = no; i <= newpagecount; i++)
            {
                if (i == newpagecount || newpagecount < 11)
                    lnknextpaging.Visible = false;
                else
                    lnknextpaging.Visible = true;
                var oPage = new ClsPageCount { PageCount = i.ToString() };
                if ((i % 10) == 0)
                {
                    oPageList.Add(oPage);
                    lnknextpaging.Attributes.Add("newpage", (i + 1).ToString());
                    i = newpagecount;
                }
                else
                    oPageList.Add(oPage);
            }
            if (newpagecount <= 1)
            {
                lnknextpaging.Visible = lnkPrepaging.Visible = lnkPrevious.Visible = lnkNext.Visible = false;
                litTotalPages.Text = "";
            }
            else
            {
                if (pageIndex == 1)
                    lnkPrevious.Visible = false;
                else
                    lnkPrevious.Visible = true;
                if (pageIndex == newpagecount)
                    lnkNext.Visible = false;
                else
                    lnkNext.Visible = true;
                litTotalPages.Text = "Showing page " + CurrentPage + " of " + newpagecount;
            }
            DLPageCountItem.DataSource = oPageList;
            DLPageCountItem.DataBind();
            foreach (RepeaterItem item1 in DLPageCountItem.Items)
            {
                var lblPage = (LinkButton)item1.FindControl("lnkPage");
                if (lblPage.Text.Trim() == (pageIndex).ToString(CultureInfo.InvariantCulture))
                    lblPage.Attributes.Add("class", "activepaging");
                else
                    lblPage.Attributes.Remove("class");
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void lnkPrepaging_Click(object sender, EventArgs e)
    {
        CurrentPage = Convert.ToInt32(lnkPrepaging.Attributes["newpage"]);
        GetOrderList();
        BindPager();
    }

    protected void lnknextpaging_Click(object sender, EventArgs e)
    {
        CurrentPage = nxtpaging = Convert.ToInt32(lnknextpaging.Attributes["newpage"]);
        GetOrderList();
        BindPager();
    }

    protected void lnkPage_Command(object sender, CommandEventArgs e)
    {
        int pageIndex = Convert.ToInt32(e.CommandArgument);
        ViewState["PageIndex"] = pageIndex;
        GetOrderList();
        BindPager();
    }

    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        CurrentPage = CurrentPage - 1;
        GetOrderList();
        BindPager();
    }

    protected void lnkNext_Click(object sender, EventArgs e)
    {
        CurrentPage = CurrentPage + 1;
        GetOrderList();
        BindPager();
    }

    private int CurrentPage
    {
        get
        {
            return ((ViewState["PageIndex"] == null || ViewState["PageIndex"].ToString() == "") ? 1 : int.Parse(ViewState["PageIndex"].ToString()));
        }
        set
        {
            ViewState["PageIndex"] = value;
        }
    }
    #endregion

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GetOrderList();
            BindPager();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void txtOrderNO_TextChanged(object sender, EventArgs e)
    {
        try
        {
            GetOrderList();
            BindPager();
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void chkCancelOrderRequest_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCancelOrderRequest.Checked)
        {
            ViewState["PageIndex"] = 1;
            GetOrderList();
            BindPager();
        }
    }
}