﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="User_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .main
        {
            padding: 2em 0;
        }
        .absolute
        {
            color: transparent;
        }
        .form-row
        {
            width: 50%;
        }
        .form-box input[type="text"], .form-box input[type="password"], .form-box input[type="email"], .form-box select
        {
            width: 100%;
        }
        .starail-Login-forgottenPw
        {
            font-size: 13px;
        }
        .form-row
        {
            margin: 0 0 15px 60px;
        }
        @media only screen and (max-width: 480px)
        {
            .form-row
            {
                margin: 0 0 15px 0px;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        .
        <div class="centerdiv">
            <div class="register-form">
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                    </div>
                </asp:Panel>
                <h3>
                    Login Form</h3>
                <div class="form-box">
                    <div class="form-row width75">
                        <i class="starail-Icon-email"></i>
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmail"
                            CssClass="absolute" ErrorMessage="Please enter email." ValidationGroup="reg"
                            Display="Dynamic" Text="*" />
                    </div>
                    <div class="form-row width75">
                        <i class="starail-Icon-password"></i>
                        <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password"
                            MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorPassword" CssClass="required" Display="Dynamic"
                            runat="server" ControlToValidate="txtPassword" ValidationGroup="reg"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-row width75 starail-Login-forgottenPw" style="text-align: left">
                        <a href="ForgotPassword">Forgotten password?</a>
                        <div>
                            <br />
                            By continuing you agree with our <a href='<%=siteURL %>Privacy-Policy'>Privacy Policy.</a>
                        </div>
                        <hr />
                        <div>
                            <a href="Registration">New customer? Sign up here</a></div>
                    </div>
                    <div class="centerrowtext">
                        <asp:Button ID="btnSubmit" CssClass="width35" runat="server" Text="Login" ValidationGroup="reg"
                            OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
