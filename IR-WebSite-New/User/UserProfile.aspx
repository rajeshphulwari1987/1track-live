﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="UserProfile.aspx.cs" Inherits="User_UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .absolute
        {
            color: transparent;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_txtDOB").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                changeYear: true,
                changeMonth: true,
            });
            $("#MainContent_txtDOB").bind("contextmenu cut copy paste", function (e) {
                return false;
            });
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <div class="register-form">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                </div>
            </asp:Panel>
            <h3>
                User Profile</h3>
            <asp:HiddenField ID="hdnID" runat="server" />
            <div class="form-box frmregister">
                <div class="row">
                    <div class="user-input-heading-ep">
                        First Name<span class="required">*</span></div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorFirstName" CssClass="absolute" Display="Dynamic"
                            runat="server" ControlToValidate="txtFirstName" ValidationGroup="reg"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Last Name</div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Password<span class="required">*</span></div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password" MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorPassword" CssClass="absolute" Display="Dynamic"
                            runat="server" ControlToValidate="txtPassword" ValidationGroup="reg"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Email</div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" MaxLength="200" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmail"
                            CssClass="absolute" ErrorMessage="Please enter email." ValidationGroup="reg"
                            Display="Dynamic" Text="*" />
                        <asp:RegularExpressionValidator ID="reqcustvalerrorEmailAddress" runat="server" ErrorMessage="Please enter valid email."
                            CssClass="absolute" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" Text="*" ValidationGroup="reg" />
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Phone Number<span class="required">*</span></div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-mobile" aria-hidden="true"></i>
                        <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone Number" MaxLength="15"
                            onkeypress="return isNumberKey(event)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorPhone" CssClass="absolute" Display="Dynamic"
                            runat="server" ControlToValidate="txtPhone" ValidationGroup="reg"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        DOB</div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <asp:TextBox ID="txtDOB" runat="server" placeholder="DOB"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Address</div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        <asp:TextBox ID="txtAddress" runat="server" placeholder="Address" Width="100%" TextMode="MultiLine"
                            MaxLength="150"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        City</div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        <asp:TextBox ID="txtCity" runat="server" placeholder="City" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Post code</div>
                    <div class="form-row form-row-ep">
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        <asp:TextBox ID="txtPostcode" runat="server" placeholder="Post code" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="user-input-heading-ep">
                        Country<span class="required">*</span></div>
                    <div class="form-row form-row-ep">
                        <asp:DropDownList ID="ddlCountry" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqvalerrorCountry" runat="server" ErrorMessage="*"
                            ControlToValidate="ddlCountry" InitialValue="0" Display="Dynamic" CssClass="absolute"
                            ValidationGroup="reg" />
                    </div>
                    <div class="centerrowtext">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="width35" Text="Change and Update"
                            ValidationGroup="reg" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
