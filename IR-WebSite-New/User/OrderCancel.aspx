﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="OrderCancel.aspx.cs" Inherits="User_OrderCancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .txt
        {
            background-color: white;
            padding: 5px;
            margin-bottom: 24px;
        }
        .headerlbl
        {
            padding: 5px;
        }
        .clsUser
        {
            background-color: #a1e5ff;
        }
        .clsAdmin
        {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">
        function cheknull() {
           return $("#MainContent_txtComment").val() != '' ? true : false;
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <fieldset class="grid-sec2">
            <legend><strong>Cancel Order</strong></legend>
            <a href="PassOrderDetails?id=<%=Request.Params["id"].ToString() %>" class="backlin">Back</a>
            <div class="grid-sec2" style="border:none;">
                <div>
                    <strong>Order No : </strong>
                    <asp:Label ID="lblOrderNo" runat="server"></asp:Label>
                </div>
                <hr />
                <div>
                    <div>
                        <strong>Comment </strong>
                    </div>
                    <div>
                        <asp:TextBox ID="txtComment" runat="server" Rows="10" Columns="70" Width="95%" placeholder="Reason for cancel"
                            TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div>
                        <asp:Button ID="btnSubmit" runat="server" style="width: 100px; display: inline;" class="starail-Button" Text="Submit" OnClientClick="return cheknull()" OnClick="btnSubmit_Click"/>
                        <asp:Button ID="btnCancel" runat="server" style="width: 100px; display: inline;" class="starail-Button" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
            </div>
        </fieldset>
        <fieldset class="grid-sec2">
            <legend><strong>Comments</strong></legend>
            <asp:Repeater ID="rptComment" runat="server">
                <ItemTemplate>
                    <div class="<%#Eval("IsUser")%>">
                        <div class="headerlbl">
                            <strong>
                                <%#Eval("Name")%>
                            </strong>
                            <div style="float: right;">
                                <%# String.Format("{0:dd/MMM/yyyy h:mm tt}", Eval("CreatedOn"))%></div>
                        </div>
                        <div class="txt">
                            <%#Eval("Commitment").ToString().Replace("\r\n","<br />")%>
                            <br />
                        </div>
                    </div>
                    <hr />
                </ItemTemplate>
            </asp:Repeater>
        </fieldset>
    </div>
</asp:Content>
