﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Globalization;

public partial class User_UserProfile : System.Web.UI.Page
{
    private readonly ManageTrainDetails _master = new ManageTrainDetails();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    private readonly ManageUser _ManageUser = new ManageUser();
    private readonly Masters _objmaster = new Masters();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                ddlCountry.DataSource = _master.GetCountryDetail();
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("Select Country", "0"));

                if (USERuserInfo.ID != Guid.Empty)
                    GetUserDetails(USERuserInfo.UserEmail);
                else
                    Response.Redirect("~/user/login");
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            tblUserLogin _userLogin = new tblUserLogin()
            {
                ID = Guid.Parse(hdnID.Value),
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Email = txtEmail.Text,
                Password = txtPassword.Text,
                Phone = txtPhone.Text,
                City = txtCity.Text,
                Country = Guid.Parse(ddlCountry.SelectedValue),
                SiteId = _siteId,
                Address = txtAddress.Text,
                PostCode = txtPostcode.Text
            };
            if (!string.IsNullOrEmpty(txtDOB.Text))
                _userLogin.Dob = Convert.ToDateTime(txtDOB.Text);
            var userID = _ManageUser.SaveUser(_userLogin);
            if (userID != new Guid())
                ShowMessage(1, "Profile has been updated successfully.");
            GetUserDetails(_userLogin.Email);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetUserDetails(string EmailAddress)
    {
        try
        {
            var result = _ManageUser.GetUserByEmailID(EmailAddress);
            if (result != null)
            {
                hdnID.Value = result.ID.ToString();
                txtFirstName.Text = result.FirstName;
                txtLastName.Text = result.LastName;
                txtEmail.Text = result.Email;
                txtCity.Text = result.City;
                ddlCountry.SelectedValue = result.Country.ToString();
                txtPhone.Text = result.Phone;
                txtPostcode.Text = result.PostCode;
                txtDOB.Text = !string.IsNullOrEmpty(result.Dob.ToString()) ? result.Dob.Value.ToString("dd/MMM/yyyy", CultureInfo.InvariantCulture) : "";
                txtPassword.Attributes.Add("value",result.Password);
                txtAddress.Text = result.Address;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}