﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Registration.aspx.cs" Inherits="User_Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
<%--PCA--%>
<script>    (function (n, t, i, r) { var u, f; n[i] = n[i] || {}, n[i].initial = { accountCode: "INTER11223", host: "INTER11223.pcapredict.com" }, n[i].on = n[i].on || function () { (n[i].onq = n[i].onq || []).push(arguments) }, u = t.createElement("script"), u.async = !0, u.src = r, f = t.getElementsByTagName("script")[0], f.parentNode.insertBefore(u, f) })(window, document, "pca", "//INTER11223.pcapredict.com/js/sensor.js")</script>
<%--PCA--%>
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <div class="register-form">
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" />
                </div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <div id="lblErrorMsg" runat="server" class="starail-Form-required">
                    </div>
                </div>
            </asp:Panel>
            <h3>
                Registration Form</h3>
            <div class="form-box regi-form">
                <div class="col-md-12 col-sm-12">
                    <h2>
                        Your Details</h2>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            First Name<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-user"></i>
                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" MaxLength="200"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorFirstName" CssClass="absolute" Display="Dynamic"
                                runat="server" ControlToValidate="txtFirstName" ValidationGroup="reg"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Last Name</div>
                        <div class="form-row">
                            <i class="fa fa-user"></i>
                            <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Email<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-envelope-o"></i>
                            <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" MaxLength="200"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmail"
                                CssClass="absolute" ErrorMessage="Please enter email." ValidationGroup="reg"
                                Display="Dynamic" Text="*" />
                            <asp:RegularExpressionValidator ID="reqcustvalerrorEmailAddress" runat="server" ErrorMessage="Please enter valid email."
                                CssClass="absolute" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Display="Dynamic" Text="*" ValidationGroup="reg" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Phone Number<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-mobile"></i>
                            <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone Number" MaxLength="15"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorPhone" CssClass="absolute" Display="Dynamic"
                                runat="server" ControlToValidate="txtPhone" ValidationGroup="reg"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Password<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-user"></i>
                            <asp:TextBox ID="txtPassword" runat="server" placeholder="password" MaxLength="10"
                                TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorpass" ControlToValidate="txtPassword"
                                runat="server" ValidationGroup="reg"></asp:RequiredFieldValidator>
                     
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Confirm Password<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-user"></i>
                            <asp:TextBox ID="txtConfirmPassword" runat="server" placeholder="password" MaxLength="10"
                                TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorconpass" ControlToValidate="txtConfirmPassword"
                                runat="server" ValidationGroup="reg"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cval" runat="server" ErrorMessage="Confirm Password do not match."
                                ControlToCompare="txtPassword" ValidationGroup="reg" ControlToValidate="txtConfirmPassword" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Your Date of birth<span class="required">*</span></div>
                        <div class="form-row">
                            <div class="row">
                                <div class="col-sm-4 left width100">
                                    <div class="padleft">
                                        <asp:DropDownList ID="ddlDay" runat="server" CssClass="starail-Form-select starail-Form-select--small"
                                            required="required">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqvalerror5" runat="server" ControlToValidate="ddlDay"
                                            ValidationGroup="reg" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator></div>
                                </div>
                                <div class="col-sm-4 left width100">
                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="starail-Form-select starail-Form-select--small"
                                        required="required">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerror6" runat="server" ControlToValidate="ddlMonth"
                                        ValidationGroup="reg" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator></div>
                                <div class="col-sm-4 left width100">
                                    <asp:DropDownList ID="ddlYear" runat="server" CssClass="starail-Form-select" required="required">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqvalerror7" runat="server" ControlToValidate="ddlYear"
                                        ValidationGroup="reg" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="col-md-12 col-sm-12" style="width: 100%;float: left;">
                    <h2>
                        BILLING ADDRESS</h2>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Search for an address</div>
                        <div class="form-row">
                            <i class="fa fa-location-arrow"></i>
                            <asp:TextBox ID="txtSearchAddress" runat="server" placeholder="Search Address" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Line1<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-location-arrow"></i>
                            <asp:TextBox ID="txtLine1" runat="server" placeholder="Line1" MaxLength="200"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalerrorLine1" CssClass="absolute" Display="Dynamic"
                                runat="server" ControlToValidate="txtLine1" ValidationGroup="reg"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Line2</div>
                        <div class="form-row">
                            <i class="fa fa-location-arrow"></i>
                            <asp:TextBox ID="txtLine2" runat="server" Width="100%" placeholder="Line2" MaxLength="150"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Town</div>
                        <div class="form-row">
                            <i class="fa fa-location-arrow"></i>
                            <asp:TextBox ID="txtTown" runat="server" placeholder="Town" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            County/state</div>
                        <div class="form-row">
                            <i class="fa fa-location-arrow"></i>
                            <asp:TextBox ID="txtstate" runat="server" placeholder="County/state" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Post code</div>
                        <div class="form-row">
                            <i class="fa fa-location-arrow"></i>
                            <asp:TextBox ID="txtPostcode" runat="server" placeholder="Post code" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 left">
                    <div class="form-group">
                        <div class="user-input-heading">
                            Country<span class="required">*</span></div>
                        <div class="form-row">
                            <i class="fa fa-flag"></i>
                            <asp:DropDownList ID="ddlCountry" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqvalerrorCountry" runat="server" ErrorMessage="*"
                                ControlToValidate="ddlCountry" InitialValue="0" Display="Dynamic" CssClass="absolute"
                                ValidationGroup="reg" />
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12 col-sm-12 left centerrowtext">
                        <div class="form-row checkbox-news">
                           <asp:CheckBox runat="server" ID="chkNewsEmail" Checked="true" Text="Send me booking & travel information, news and other special offers." />
                        </div>
                </div> 
                <div class="centerrowtext">
                    <asp:Button ID="btnSubmit" CssClass="width35" runat="server" Text="Sign Up" ValidationGroup="reg"
                        OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
