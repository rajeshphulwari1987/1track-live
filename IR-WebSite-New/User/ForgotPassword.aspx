﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ForgotPassword.aspx.cs" Inherits="User_ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .absolute
        {
            color: transparent;
        }
        .form-row
        {
            margin: 30px 0 15px 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <div class="centerdiv">
            <div class="register-form">
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                    </div>
                </asp:Panel>
                <h3>
                    Forgot Password</h3>
                <div class="form-box">
                    <div>
                        Please enter the email address associated with your account.</div>
                    <div class="form-row width75" style="margin: 10px auto;">
                        <i class="starail-Icon-email" aria-hidden="true"></i>
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalerrorEmailAddress" runat="server" ControlToValidate="txtEmail"
                            CssClass="absolute" ErrorMessage="Please enter email." ValidationGroup="reg"
                            Display="Dynamic" Text="*" />
                        <asp:RegularExpressionValidator ID="reqcustvalerrorEmailAddress" runat="server" ErrorMessage="Please enter valid email."
                            CssClass="absolute" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Display="Dynamic" Text="*" ValidationGroup="reg" />
                    </div>
                    <div class="centerrowtext">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="width35" Text="Submit" ValidationGroup="reg"
                            OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
