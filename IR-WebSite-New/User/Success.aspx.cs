﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Xml;

public partial class User_Success : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
   
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
 

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("REFRESH", "5;URL=../"); 
    }

}