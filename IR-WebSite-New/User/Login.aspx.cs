﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

public partial class User_Login : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private Guid _siteId;
    public string siteURL;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageUser _ManageUser = new ManageUser();
    readonly Masters _master = new Masters();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        USERuserInfo.UserEmail ="";
        USERuserInfo.ID = Guid.Empty;
        USERuserInfo.Username = "";
        USERuserInfo.SiteID = _siteId;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            var result = _ManageUser.GetUserLogin(txtEmail.Text.Trim(), txtPassword.Text.Trim(), _siteId);
            if (result != null)
            {
                USERuserInfo.UserEmail = result.Email;
                USERuserInfo.ID = result.ID;
                USERuserInfo.Username = result.FirstName +(string.IsNullOrEmpty(result.LastName) ? "" : " " + result.LastName);
                USERuserInfo.SiteID = result.SiteId;
                //Response.Redirect("OrderDetails");
                siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
                Response.Redirect(siteURL);
            }
            else
            {
                ShowMessage(2, "Invalid username and password.");
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}