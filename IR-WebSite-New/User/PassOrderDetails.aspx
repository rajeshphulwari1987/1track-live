﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PassOrderDetails.aspx.cs" Inherits="User_PassOrderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .main
        {
            padding: 2em 0;
        }
        #MainContent_lnkcancelorder:hover
        {
            text-decoration: underline !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main small-font">
        <asp:Panel ID="pnlErrSuccess" runat="server">
            <div id="DivSuccess" runat="server" class="success" style="display: none;">
                <asp:Label ID="lblSuccessMsg" runat="server" /></div>
            <div id="DivError" runat="server" class="error" style="display: none;">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
            </div>
        </asp:Panel>
        <fieldset class="grid-sec2">
            <legend><strong>Orders</strong></legend>
            <a href="OrderDetails" class="backlin">Back</a>
            <div class="fieldset-chart">
                <div class="div_th">
                    <p>
                        <strong>Order Number : </strong>
                        <asp:Label ID="lblOrderID" runat="server" />
                    </p>
                </div>
                <div class="div_td">
                    <strong>Status : </strong>
                    <asp:Label ID="lblStatus" runat="server" /></div>
                <div class="div_td">
                    <strong>Creation Date : </strong>
                    <asp:Label ID="txtOrdCDate" runat="server" /></div>
                <div class="div_td" id="div_DeptDate" runat="server">
                    <strong>Departure date : </strong>
                    <asp:Label ID="txtDepDt" runat="server" /></div>
                <div class="div_td" id="div_ProductGrandTotal" runat="server">
                    <strong>Grand Total : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="tblProductGrandTotal" runat="server" /></div>
                <div class="div_td" id="div_ProductGrossTotal" runat="server">
                    <strong>Product(s) Gross Total : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblGrossTotal" runat="server" /></div>
                <div class="div_td">
                    <strong>Discount Amount : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblDiscount" runat="server" /></div>
                <div class="div_td">
                    <strong>Shipping Cost : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblShippingCost" runat="server" /></div>
                <div class="div_td">
                    <strong>Booking Fee : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblBookindFee" runat="server" /></div>
                <div class="div_td" id="div_AmexFee" runat="server">
                    <strong>Amex Fee : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblAmexPrice" runat="server" /></div>
                <div class="div_td" id="div_Commission" runat="server">
                    <strong>Commission Fee : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblCommission" runat="server" /></div>
                <div class="div_td">
                    <strong>Ticket Protection : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblTckProtection" runat="server" /></div>
                <div class="div_td">
                    <strong>Admin Fee : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblAdminFee" runat="server" /></div>
                <div class="div_td">
                    <strong>Product(s) Net Total : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblNetPrice" runat="server" /></div>
                <div class="div_td" id="div_P2PGrandTotal" runat="server">
                    <strong>Grand Total : </strong>
                    <%=Currency.ToString()%>&nbsp;<asp:Label ID="lblP2PGrandTotal" runat="server" /></div>
                <div class="div_td">
                    <p id="useronly" runat="server">
                        <strong>Cancel Order : </strong><a runat="server" id="lnkcancelorder" style="color: #d04819;
                            text-decoration: none;"></a>
                    </p>
                </div>
            </div>
        </fieldset>
        <fieldset class="grid-sec2" id="fieldset_Pass" runat="server">
            <legend><strong>Pass Details</strong></legend>
            <div class="fieldset-chart">
                <div class="table-res">
                    <asp:GridView ID="grdPass" runat="server" AutoGenerateColumns="False" CssClass="grid-head2">
                        <EmptyDataTemplate>
                            Record not found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Ticket No.">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Ticket No.: </b>
                                    </div>
                                    <%# ((GridViewRow)Container).RowIndex + 1%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Name: </b>
                                    </div>
                                    <%#Eval("RailPassName")%>
                                    <%#Eval("CountryName")%>,
                                    <%#Eval("PassDesc")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Traveller Type">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Traveller Type: </b>
                                    </div>
                                    <asp:Label ID="lblTrvType" Text=' <%#Eval("TrvClass")%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Class Name">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Class Name: </b>
                                    </div>
                                    <%#Eval("ClassType")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price (NET)">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Price (NET): </b>
                                    </div>
                                    <%=Currency%>
                                    <%#Eval("Price")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ticket Protection">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Ticket Protection: </b>
                                    </div>
                                    <%=Currency.ToString()%>
                                    <%#Eval("TicketProtection")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </fieldset>
        <fieldset class="grid-sec2" id="fieldset_Journey" runat="server">
            <legend><strong>Journey Information</strong></legend>
            <asp:Repeater ID="rptJourney" runat="server">
                <ItemTemplate>
                    <div class="fieldset-chart">
                        <div class="div_td">
                            <strong>Train No : </strong>
                            <%#Eval("TrainNo")%></div>
                        <div class="div_td">
                            <strong>From : </strong>
                            <%#Eval("From")%></div>
                        <div class="div_td">
                            <strong>To : </strong>
                            <%#Eval("To")%></div>
                        <div class="div_td">
                            <strong>Depature Date : </strong>
                            <%#Eval("DateTimeDepature", "{0:dd/MMM/yyyy}")%></div>
                        <div class="div_td">
                            <strong>Arrival Date : </strong>
                            <%#Eval("DateTimeArrival", "{0:dd/MMM/yyyy}")%></div>
                        <div class="div_td">
                            <strong>Passenger : </strong>
                            <%#Eval("Passenger")%></div>
                        <div class="div_td">
                            <strong>Net Price : </strong>
                            <%# Eval("Symbol")%>&nbsp;<%# Eval("NetPrice")%></div>
                        <div class="div_td">
                            <strong>Class : </strong>
                            <%#Eval("Class")%></div>
                        <div class="div_td">
                            <strong>Fare Name : </strong>
                            <%#Eval("FareName")%></div>
                        <div class="div_td">
                            <strong>PNR No : </strong>
                            <%#Eval("ReservationCode")%></div>
                        <div class="div_td">
                            <strong>Pin No : </strong>
                            <%#Eval("PinNumber")%></div>
                        <div class="div_td">
                            <strong>Delivery Option : </strong>
                            <%#Eval("DeliveryOption")%></div>
                        <div class="div_td">
                            <strong>My FIP Card Number:</strong>
                            <%#Eval("Fipnumber")%></div>
                        <div class="div_td">
                            <strong>My FIP Card Class : </strong>
                            <%#Eval("FIPClass")%></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </fieldset>
        <fieldset class="grid-sec2">
            <legend><strong>Traveller Information</strong></legend>
            <div class="fieldset-chart">
                <div class="table-res">
                    <asp:GridView ID="grdPassTraveller" runat="server" AutoGenerateColumns="False" CssClass="grid-head2">
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Ticket No.">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Ticket No.: </b>
                                    </div>
                                    <%# ((GridViewRow)Container).RowIndex + 1%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Title: </b>
                                    </div>
                                    <%#Eval("Title")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>First Name: </b>
                                    </div>
                                    <%#Eval("FirstName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Last Name: </b>
                                    </div>
                                    <%#Eval("LastName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date of Birth">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Date of Birth: </b>
                                    </div>
                                    <%# Eval("DOB", "{0:MMM dd, yyyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country of residence">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Country of residence: </b>
                                    </div>
                                    <%#Eval("CountryName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nationality">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Nationality: </b>
                                    </div>
                                    <%# Eval("Nationality") != null ? Eval("Nationality").ToString() == "0" ? "-" : Eval("Nationality") : "-"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Passport No">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Passport No: </b>
                                    </div>
                                    <%#Eval("PassportNo")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pass Start Date">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Pass Start Date: </b>
                                    </div>
                                    <%# Eval("PassStartDate", "{0:MMM dd, yyyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="grdP2PTraveller" runat="server" AutoGenerateColumns="False" CssClass="grid-head2">
                        <EmptyDataTemplate>
                            Record not found.</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Ticket No.">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Ticket No.: </b>
                                    </div>
                                    <%# ((GridViewRow)Container).RowIndex + 1%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Title: </b>
                                    </div>
                                    <%#Eval("Title")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>First Name: </b>
                                    </div>
                                    <%#Eval("FirstName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Last Name: </b>
                                    </div>
                                    <%#Eval("LastName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country">
                                <ItemTemplate>
                                    <div class="hidedesktop">
                                        <b>Country: </b>
                                    </div>
                                    <%#Eval("CountryName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </fieldset>
        <fieldset class="grid-sec2">
            <legend><strong>Billing Information</strong></legend>
            <div class="fieldset-chart">
                <asp:Repeater ID="rptBilling" runat="server">
                    <ItemTemplate>
                        <div class="div_td">
                            <strong>Title: </strong>
                            <%#Eval("Title")%></div>
                        <div class="div_td">
                            <strong>First Name: </strong>
                            <%#Eval("FirstName")%></div>
                        <div class="div_td">
                            <strong>Last Name: </strong>
                            <%#Eval("LastName")%></div>
                        <div class="div_td">
                            <strong>Address1: </strong>
                            <%#Eval("Address1")%></div>
                        <div class="div_td">
                            <strong>Address2:</strong>
                            <%#Eval("Address2")%></div>
                        <div class="div_td">
                            <strong>City:</strong>
                            <%#Eval("City")%></div>
                        <div class="div_td">
                            <strong>State:</strong>
                            <%# Eval("State")%></div>
                        <div class="div_td">
                            <strong>Postal/Zip Code:</strong>
                            <%#Eval("Postcode")%></div>
                        <div class="div_td">
                            <strong>Country:</strong>
                            <%#Eval("Country")%></div>
                        <div class="div_td">
                            <strong>Email Address:</strong>
                            <%#Eval("EmailAddress")%></div>
                        <div class="div_td">
                            <strong>Phone Number:</strong>
                            <%#Eval("Phone")%></div>
                        <div class="div_td">
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </fieldset>
        <fieldset class="grid-sec2">
            <legend><strong>Shipping Information</strong></legend>
            <div class="fieldset-chart">
                <asp:Repeater ID="rptShipping" runat="server">
                    <ItemTemplate>
                        <div class="div_td">
                            <strong>Title: </strong>
                            <%#Eval("TitleShpg")%></div>
                        <div class="div_td">
                            <strong>First Name:</strong>
                            <%#Eval("FirstNameShpg")%></div>
                        <div class="div_td">
                            <strong>Last Name:</strong>
                            <%#Eval("LastNameShpg")%></div>
                        <div class="div_td">
                            <strong>Address1:</strong>
                            <%#Eval("Address1Shpg")%></div>
                        <div class="div_td">
                            <strong>Address2:</strong>
                            <%#Eval("Address2Shpg")%></div>
                        <div class="div_td">
                            <strong>City:</strong>
                            <%#Eval("CityShpg")%></div>
                        <div class="div_td">
                            <strong>State:</strong>
                            <%# Eval("StateShpg")%></div>
                        <div class="div_td">
                            <strong>Postal/Zip Code:</strong>
                            <%#Eval("PostcodeShpg")%></div>
                        <div class="div_td">
                            <strong>Country:</strong>
                            <%#Eval("CountryShpg")%></div>
                        <div class="div_td">
                            <strong>Email Address:</strong>
                            <%#Eval("EmailAddressShpg")%></div>
                        <div class="div_td">
                            <strong>Phone Number:</strong>
                            <%#Eval("PhoneShpg")%></div>
                        <div class="div_td">
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </fieldset>
    </div>
</asp:Content>
