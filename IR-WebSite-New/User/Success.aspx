﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Success.aspx.cs" Inherits="User_Success" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../assets/css/register-style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .absolute
        {
            color: transparent;
        }
        .parent-margin
        {
            margin-top: 20px; 
            margin-bottom: 85px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="starail-Box starail-Box--whiteMobile starail-ContactForm">
        <div class="starail-Form  starail-ContactForm-form parent-margin">
            <div class="success">
                <asp:Label ID="Label1" runat="server" Text="Your account has been successfully created. You will be redirected to the homepage
        shortly, if you're not" /><a style="margin-left: 5px; color: #4F8A10; text-decoration: underline;"
            href='<%=siteURL%>'>click here</a></div>
        </div>
    </div>
</asp:Content>
