﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="LoyaltyCardInfo.aspx.cs" Inherits="LoyaltyCardInfo" Culture="en-GB" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .Breadcrumb
        {
            padding-top: 15px;
            padding-bottom: 10px;
        }
        .starail-Form-inputContainer input[type=radio] + label
        {
            font-size: 16px;
            font-size: 1.14286rem;
        }
        
        .railsectionbg
        {
            background-color: #eee;
            border: solid 1px #ccc;
            position: relative;
            top: -160px;
            margin-bottom: -160px;
            margin-left: 10px;
        }
        
        .railsectionbg label
        {
            font-size: 15px;
            clear: both;
            width: 100%;
        }
        
        .railsectionbg
        {
            padding: 8px 15px;
        }
        
        .railsectionbg input
        {
            width: 100%;
            padding: 6px 10px;
        }
        
        .railselectformblk
        {
            margin-bottom: 10px;
        }
        .linknavblk00
        {
            margin: 0px;
            display: inline-block;
            width: 100%;
        }
        
        .linknavblk00 table label
        {
            padding-bottom: 5px;
            padding-top: 5px;
        }
        a.onewaylink
        {
            text-align: center;
            background-color: #8e3030;
            color: #fff;
            padding: 5px;
            margin-top: 0px;
            width: 98%;
            display: block;
        }
        
        a.returnlink
        {
            background-color: #ccc;
            color: #fff;
            padding: 5px;
            margin-top: 0px;
            text-align: center;
            width: 98%;
            display: block;
        }
        
        .railselectformblk a:hover
        {
            text-decoration: none;
        }
        
        .whoisgoingblk
        {
            border: solid 1px #ccc;
        }
        
        .whoisgoingblk h4
        {
            padding: 8px 10px;
            color: #fff;
            font-weight: 400;
            text-transform: none;
        }
        
        .whoisblk
        {
            padding: 0 10px;
        }
        
        .whoisblk p
        {
            font-size: 12px;
            width: 105%;
        }
        
        .whoisblk select
        {
            width: 100%;
            padding: 4px 7px;
        }
        
        
        .whoisbottom
        {
            clear: both;
        }
        
        
        .contenttitleblk h4
        {
            padding: 10px;
            color: #fff;
            margin-bottom: 10px;
        }
        
        .contenttitleblk
        {
            padding: 17px 0;
        }
        
        .whoblk
        {
            padding: 0;
            margin-top: 10px;
            line-height: 10px;
        }
        
        .whoblk p
        {
            margin-top: 9px;
        }
        
        .railsectionbg select
        {
            height: 32px;
            padding: 5px 7px;
        }
        
        .ui-datepicker-calendar
        {
            margin-left: 4px;
        }
        .mintext
        {
            font-size: 12px;
            display: inline;
        }
        #_bindDivData
        {
            width: 57%;
        }
        .pnlpopup
        {
            z-index: 111 !important;
            position: fixed !important;
            left: 23%;
            top: 25%;
        }
        .modalBackgroundnew
        {
            position: fixed !important;
            left: 0px !important;
            top: 0px !important;
            z-index: 110 !important;
            width: 100% !important;
            height: 100% !important;
        }
        .pnl-pop .starail-YourBooking-table
        {
            display: inline-block;
            max-height: 250px;
            overflow-x: auto;
        }
        .starail-Form-datePicker i
        {
            right: 25px;
            top: 8px;
        }
        .banner90 h1
        {
            color: #fff !important;
        }
        input#MainContent_rdBookingType_0
        {
            position: absolute;
            top: 20px;
            z-index: 0;
        }
        #MainContent_rdBookingType
        {
            margin-bottom: 0px;
        }
        #MainContent_rdBookingType td
        {
            position: relative;
        }
        #MainContent_rdBookingType_0 + label, #MainContent_rdBookingType_1 + label
        {
            display: inline-block;
            width: 100%;
        }
        .linknavblk00 label
        {
            position: relative;
            z-index: 2;
        }
        input#MainContent_rdBookingType_1
        {
            position: absolute;
            top: 20px;
        }
        .linknavblk00 label
        {
            padding-bottom: 0;
        }
        .inner-banner
        {
            height: 190px;
            width: 900px;
        }
        #MainContent_ddlReturnTime
        {
            width: 100% !important;
        }
        
        .img-down-show
        {
            position: absolute;
            top: 4%;
            left: 92%;
        }
        .img-up-show
        {
            position: absolute;
            top: 13%;
            left: 92%;
        }
        #_bindDivDataTrainResult
        {
            width: 91%;
        }
        .innner-banner
        {
            background: #2f2f2f;
        }
        @media only screen and (max-width: 639px)
        {
            .mintext
            {
                display: -webkit-box;
                position: inherit;
            }
            .pnl-pop .starail-YourBooking-ticketDetails
            {
                padding: 5px 11px 10px 5px;
            }
            .pnl-pop .header-width
            {
                width: 30%;
            }
            .pnl-pop .row-width
            {
                width: 70%;
            }
            .pnl-pop .starail-YourBooking-ticketDetails
            {
                margin-bottom: 5px;
            }
        }
    </style>
    <script type="text/javascript"> 
    var count = 0;
    var countKey = 1;
    var conditionone = 0;
    var conditiontwo = 0;
    var tabkey = 0;

     $(document).ready(function () { 
          setMenuActive("RT");
          initradio();
          coalapsibleh4();
          $(".closex").click(function () {
                    $(".pnlpopupbox").hide();
          });
          $("#MainContent_txtReturnDate").val("DD/MM/YYYY");

           if ($("#txtFrom").val() != '') {
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            LoadCal();
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
   
         function selectpopup(e) { 
            if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                var $this = $(e);
                var data = $this.val();
                var station = '';
                var hostName = window.location.host;
                var url = "http://" + hostName;

                if (window.location.toString().indexOf("https:") >= 0)
                    url = "https://" + hostName;
 

                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }
                var filter = $("#span" + $this.attr('id') + "").text();
                if (filter == "" && $this.val() != "")
                    filter = $("#hdnFilter").val();
                $("#hdnFilter").val(filter);


                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }

                if (hostName == "localhost") {
                    url = url + "/1TrackNew";
                }
                var hostUrl = url + "/StationList.asmx/getStationsXList";
                data = data.replace(/[']/g, "♥");
                 
               
                var $div = $("<div id='_bindDivDataTrainResult' onmousemove='_removehoverclass(this)'/>");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {  
                        $('#_bindDivDataTrainResult').remove();
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length; ;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);
                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue_y(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1]  + "</div><p style='display:none'>" + splitdata[2] + "</p></div>"));
                        });
                        $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                    },
                    error: function () {
                        //                    alert("Wait...");
                    }
                });
            }
        }
     function _removehoverclass(e) {
            $(".popupselect").hover(function () {
                $(".popupselect").removeAttr('style');
                $(this).attr('style', 'background-color: #ccc !important');
                countKey = $(this).index();
            });
        }

        function _Bindthisvalue_y(e) {
            var idtxtbox = $('#_bindDivDataTrainResult').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
           if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
                 $("#hdntxtToRailName").val($(e).find('p').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
                 $("#hdntxtFromRailName").val($(e).find('p').text());
            }
             $('#_bindDivDataTrainResult').remove();
             isEvolviStation();
             ChangeChildAgeForEvolvi()
        }

         function isEvolviStation() {
        //Hide rail card option it will be used in future
        //        if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
        //            $('[id*=div_chkAddRailCard]').show();
        //        }
        //        else {
        //            $('[id*=div_chkAddRailCard]').hide();
        //        }
        }

        function EvolviMaxPassenger(event) {
            if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
                var count = parseInt($('[id*=ddlAdult]').val()) + parseInt($('[id*=ddlChild]').val()) + parseInt($('[id*=ddlYouth]').val()) + parseInt($('[id*=ddlSenior]').val());
                if (count > 9) {
                    alert('Please select at most 9 passenger.');
                    event.preventDefault();
                }
            }
        }

        function checkPassenger(event) {
            if ($('[id*=txtReturnDate]').val() != '' && $('[id*=txtReturnDate]').val() != 'DD/MM/YYYY' && $('[id*=txtReturnDate]').val() != undefined) {
                if (Date.parse($('[id*=txtReturnDate]').val()) < Date.parse($('[id*=txtDepartureDate]').val())) {
                    alert('Return Date should be greater than the departure Date');
                    event.preventDefault();
                }
            }

            if ($('[id*=ddlAdult]').val() == '0' && $('[id*=ddlChild]').val() == '0' && $('[id*=ddlYouth]').val() == '0' && $('[id*=ddlSenior]').val() == '0') {
                alert('Please enter at least 1 adult, senior or junior(youth) passenger.');
                event.preventDefault();
            }
            else if ($('[id*=ddlAdult]').val() == '0' && $('[id*=ddlChild]').val() != '0' && $('[id*=ddlYouth]').val() == '0' && $('[id*=ddlSenior]').val() == '0') {
                $('[id*=pnlPassenger]').show();
                event.preventDefault();
            }

            var totalAdult = parseInt($('[id*=ddlAdult]').val()) * 4;
            var totalYouth = parseInt($('[id*=ddlYouth]').val()) * 4;
            var totalSenior = parseInt($('[id*=ddlSenior]').val()) * 4;
            var totalChilden = totalAdult + totalYouth + totalSenior;

            if (parseInt($('[id*=ddlChild]').val()) > totalChilden) {
                $('[id*=pnlPassenger]').show();
                event.preventDefault();
            }
        }

        function ChangeChildAgeForEvolvi() {
            if ($("#hdntxtToRailName").val() == "EVOLVI" && $("#hdntxtFromRailName").val() == "EVOLVI") {
                $('[id*=lblAdult]').text('(26-65 at time of travel)');
                $('[id*=lblChild]').text('(5-15 at time of travel)');
                $('[id*=lblYouth]').text('(16-25 at time of travel)');
                $('[id*=lblSenior]').text('(over 66 at time of travel)');
            }
            else {
                $('[id*=lblAdult]').text('(26-65 at time of travel)');
                $('[id*=lblChild]').text('(4-17 at time of travel)');
                $('[id*=lblYouth]').text('(17-25 at time of travel)');
                $('[id*=lblSenior]').text('(over 66 at time of travel)');
            }
        }


        function _hideThisDiv(e) {
            $('#_bindDivDataTrainResult').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }

        function CheckCardVal(sender, args) {
            if (document.getElementById('MainContent_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
      
        function LoadCal() {
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+3m',
            });
            $("#MainContent_txtReturnDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+3m'
            });

            $(".imgCalSendBox").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });

            $(".imgCalSendBox1").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked')) {
                    $("#MainContent_txtReturnDate").datepicker('show');
                }
            });

            if ($('#MainContent_ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('enable');
            }
            else {
                $("#txtReturnDate").datepicker('disable');
            }
            $("#MainContent_txtDepartureDate, #MainContent_txtReturnDate").keypress(function (event) { event.preventDefault(); });
        }

        function calenable() {
        callvalerror();
            LoadCal();
            $("#MainContent_txtReturnDate").datepicker('enable').val($("#MainContent_txtDepartureDate").val());
        }
        function caldisable() {
        callvalerror();
            LoadCal 
            $("#MainContent_txtReturnDate").datepicker('disable').val("DD/MM/YYYY");
        }
        function initradio(){  
          $("#MainContent_txtReturnDate").attr("disabled","disabled");
           $("#MainContent_ddlReturnTime").attr("disabled","disabled");
         $('input[type="radio"][name="ctl00$MainContent$rdBookingType"]').change(function() {
        if (this.value == '0') {
           $("#ReturningDiv").attr("style","display:none;");
           $("#MainContent_ddlReturnTime").attr("disabled","disabled"); 
        }
        else if (this.value == '1') {
           $("#ReturningDiv").attr("style","display:block;");
           $("#MainContent_ddlReturnTime").removeAttr("disabled");
        }
    });  
        }
    </script>
    <%=script %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
        ValidationGroup="vgs" ShowSummary="false" ShowMessageBox="false" />
    <div id="DivLeftOne" runat="server">
        <div style="clear: both;">
        </div>
        <div class="Breadcrumb">
            <asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
        <div class="starail-BookingDetails-form">
            <div class="starail-Form-row">
                <h1 id="h2_Heading" runat="server">
                    Travel selection
                </h1>
            </div>
            <div class="starail-Form-row">
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="warning" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
            </div>
            <div class="trainbanner01">
                <asp:Image ID="imgInnerBanner" runat="server" ImageUrl="images/img_inner-banner.jpg"
                    CssClass="scale-with-grid inner-banner" AlternateText="Rail Tickets" ToolTip="Book Rail Ticket" />
            </div>
            <asp:UpdatePanel ID="updLoyalty" runat="server">
                <ContentTemplate>
                    <div class="col-sm-12">
                        <div class="row railselection-ar">
                            <div class="col-sm-5">
                                <div class="railsectionbg">
                                    <div class="railselection-form">
                                        <div class="railselectformblk">
                                            <label>
                                                From *
                                            </label>
                                            <asp:TextBox ID="txtFrom" onkeyup="selectpopup(this)" runat="server" class="starail-Form-input"
                                                placeholder="From" autocomplete="off" ClientIDMode="Static" />
                                            <span id="spantxtFrom" style="display: none;"></span>
                                            <asp:HiddenField ID="hdnFrm" runat="server" />
                                            <asp:RequiredFieldValidator ID="reqvalerror4" runat="server" ValidationGroup="vgs"
                                                Display="Dynamic" ControlToValidate="txtFrom" />
                                        </div>
                                        <div class="railselectformblk">
                                            <label>
                                                To *
                                            </label>
                                            <asp:TextBox ID="txtTo" runat="server" onkeyup="selectpopup(this)" class="starail-Form-input"
                                                placeholder="To" autocomplete="off" ClientIDMode="Static" />
                                            <span id="spantxtTo" style="display: none"></span>
                                            <asp:RequiredFieldValidator ID="reqvalerror5" runat="server" ValidationGroup="vgs"
                                                Display="Dynamic" ControlToValidate="txtTo" CssClass="font14" />
                                        </div>
                                        <div class="railselectformblk">
                                            <div class="linknavblk00">
                                                <label>
                                                    Journey</label>
                                                <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vgs" RepeatDirection="Horizontal"
                                                    CssClass="switchRadioGroup-trainticket" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                                                    <asp:ListItem Value="0" Selected="True">One-way</asp:ListItem>
                                                    <asp:ListItem Value="1">Return</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="railselectformblk leaving">
                                            <label>
                                                Leaving <span>* </span>
                                            </label>
                                            <div class="row">
                                                <div class="col-sm-7 starail-Form-datePicker">
                                                    <asp:TextBox ID="txtDepartureDate" runat="server" class="starail-Form-input" placeholder="DD/MM/YYYY"
                                                        autocomplete="off" />
                                                    <asp:RequiredFieldValidator ID="reqvalerror6" runat="server" ValidationGroup="vgs"
                                                        Display="Dynamic" ControlToValidate="txtDepartureDate" />
                                                    <i class="starail-Icon-datepicker"></i>
                                                </div>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddldepTime" runat="server" CssClass="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                                                        <asp:ListItem>00:00</asp:ListItem>
                                                        <asp:ListItem>01:00</asp:ListItem>
                                                        <asp:ListItem>02:00</asp:ListItem>
                                                        <asp:ListItem>03:00</asp:ListItem>
                                                        <asp:ListItem>04:00</asp:ListItem>
                                                        <asp:ListItem>05:00</asp:ListItem>
                                                        <asp:ListItem>06:00</asp:ListItem>
                                                        <asp:ListItem>07:00</asp:ListItem>
                                                        <asp:ListItem>08:00</asp:ListItem>
                                                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                                                        <asp:ListItem>10:00</asp:ListItem>
                                                        <asp:ListItem>11:00</asp:ListItem>
                                                        <asp:ListItem>12:00</asp:ListItem>
                                                        <asp:ListItem>13:00</asp:ListItem>
                                                        <asp:ListItem>14:00</asp:ListItem>
                                                        <asp:ListItem>15:00</asp:ListItem>
                                                        <asp:ListItem>16:00</asp:ListItem>
                                                        <asp:ListItem>17:00</asp:ListItem>
                                                        <asp:ListItem>18:00</asp:ListItem>
                                                        <asp:ListItem>19:00</asp:ListItem>
                                                        <asp:ListItem>20:00</asp:ListItem>
                                                        <asp:ListItem>21:00</asp:ListItem>
                                                        <asp:ListItem>22:00</asp:ListItem>
                                                        <asp:ListItem>23:00</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="railselectformblk leaving" id="ReturningDiv" style="display: none;" runat="server">
                                            <label>
                                                Returning <span>* </span>
                                            </label>
                                            <div class="row">
                                                <div class="col-sm-7 starail-Form-datePicker">
                                                    <asp:TextBox ID="txtReturnDate" runat="server" class="starail-Form-input" placeholder="DD/MM/YYYY"
                                                        autocomplete="off" Enabled="False" />
                                                    <asp:RequiredFieldValidator ID="reqvalerror7" runat="server" ValidationGroup="vgs"
                                                        Enabled="false" Display="Dynamic" ControlToValidate="txtReturnDate" />
                                                    <i class="starail-Icon-datepicker"></i>
                                                </div>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlReturnTime" runat="server" CssClass="loyreturnselect" Enabled="false">
                                                        <asp:ListItem>00:00</asp:ListItem>
                                                        <asp:ListItem>01:00</asp:ListItem>
                                                        <asp:ListItem>02:00</asp:ListItem>
                                                        <asp:ListItem>03:00</asp:ListItem>
                                                        <asp:ListItem>04:00</asp:ListItem>
                                                        <asp:ListItem>05:00</asp:ListItem>
                                                        <asp:ListItem>06:00</asp:ListItem>
                                                        <asp:ListItem>07:00</asp:ListItem>
                                                        <asp:ListItem>08:00</asp:ListItem>
                                                        <asp:ListItem Selected="True">09:00</asp:ListItem>
                                                        <asp:ListItem>10:00</asp:ListItem>
                                                        <asp:ListItem>11:00</asp:ListItem>
                                                        <asp:ListItem>12:00</asp:ListItem>
                                                        <asp:ListItem>13:00</asp:ListItem>
                                                        <asp:ListItem>14:00</asp:ListItem>
                                                        <asp:ListItem>15:00</asp:ListItem>
                                                        <asp:ListItem>16:00</asp:ListItem>
                                                        <asp:ListItem>17:00</asp:ListItem>
                                                        <asp:ListItem>18:00</asp:ListItem>
                                                        <asp:ListItem>19:00</asp:ListItem>
                                                        <asp:ListItem>20:00</asp:ListItem>
                                                        <asp:ListItem>21:00</asp:ListItem>
                                                        <asp:ListItem>22:00</asp:ListItem>
                                                        <asp:ListItem>23:00</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row whoisgoingblk">
                                                <div class="show-hide-img">
                                                    <img src="images/arrow-down.png" class="img-down-show" alt="" />
                                                    <img src="images/arrow-up.png" class="img-up-show" alt="" />
                                                </div>
                                                <h4 class="coalapsible">
                                                    Who's going?
                                                </h4>
                                                <div class="coalapsible-div">
                                                    <div class="whoisblk row">
                                                        <div class="col-sm-8">
                                                            <p>
                                                                Adults <span id="lblAdult">(26-65 at time of travel)</span>
                                                            </p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlAdult" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="whoisblk row">
                                                        <div class="col-sm-8">
                                                            <p>
                                                                Childrens <span id="lblChild">(4-17 at time of travel)</span>
                                                            </p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlChild" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="whoisblk row">
                                                        <div class="col-sm-8">
                                                            <p>
                                                                Youths <span id="lblYouth">(17-25 at time of travel)</span>
                                                            </p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlYouth" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="whoisblk row">
                                                        <div class="col-sm-8">
                                                            <p>
                                                                Seniors <span id="lblSenior">(over 66 at time of travel)</span>
                                                            </p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlSenior" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="whoisbottom">
                                            <div class="col-sm-12 whoblk">
                                                <div class="row">
                                                    <div class="col-sm-7">
                                                        <p>
                                                            Class Preference
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <asp:DropDownList ID="ddlClass" runat="server" class="starail-Form-select">
                                                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                                            <asp:ListItem Value="1">1st</asp:ListItem>
                                                            <asp:ListItem Value="2">2nd</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 whoblk">
                                                <div class="row">
                                                    <div class="col-sm-7">
                                                        <p>
                                                            Max Transfers
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <asp:DropDownList ID="ddlTransfer" runat="server" class="starail-Form-select">
                                                            <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                                                            <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                                                            <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                                                            <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 starail-Form-row starail-u-cf starail-SearchTickets-quantity"
                                                style="display: none;">
                                                <label class="starail-Form-label" for="starail-country">
                                                    Journey Type
                                                </label>
                                                <div class="starail-Form-inputContainer">
                                                    <div class="starail-Form-inputContainer-col">
                                                        <label class="starail-BookingDetails-form-fancyCheckbox">
                                                            <span class="starail-Form-fancyCheckbox">
                                                                <input type="checkbox" name="starail-protect" id="chkLoyalty" runat="server">
                                                                Loyalty cards <span><i class="starail-Icon-tick"></i></span></span>&nbsp;&nbsp;&nbsp;</label></div>
                                                </div>
                                                <div class="starail-Form-inputContainer" id="div1" runat="server" visible="False">
                                                    <div class="starail-Form-inputContainer-col">
                                                        <label class="starail-BookingDetails-form-fancyCheckbox">
                                                            <span class="starail-Form-fancyCheckbox">
                                                                <input type="checkbox" name="starail-protect" id="chkIhaveRailPass" runat="server">
                                                                I have a rail pass <span><i class="starail-Icon-tick"></i></span></span>&nbsp;&nbsp;&nbsp;</label></div>
                                                </div>
                                            </div>
                                            <asp:Button ID="btnCheckout" runat="server" Text="Search" ValidationGroup="vgs" OnClientClick="checkPassenger(event);EvolviMaxPassenger(event);"
                                                OnClick="btnCheckout_Click" CssClass="starail-Button starail-Form-button starail-Form-button--primary IR-GaCode" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="contenttitleblk">
                                        <h2 id="h4_Heading" runat="server">
                                            Content Title
                                        </h2>
                                        <p id="p_Content" runat="server">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In imperdiet ligula ex,
                                            non sagittis augue egestas id. Praesent viverra turpis lectus, id accumsan nisl
                                            mattis vel. Phasellus rhoncus volutpat sollicitudin. Nulla vehicula iaculis condimentum.
                                            Phasellus rutrum mauris id dapibus maximus. Phasellus a tempor metus. Etiam odio
                                            lectus, pharetra vitae lorem eget, iaculis venenatis augue. Nam in efficitur enim.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="booking-detail-in" style="display: none;">
                            <div id="FtpCardsDescriptionContent" class="ptop lblock" runat="server" visible="false">
                                Do you have a Thalys TheCard or Eurostar Frequent traveller loyalty card for journeys
                                on these trains?
                                <br />
                                If yes, Please input your card number.
                                <br />
                                <asp:DataList ID="dtlLoayalty" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <div class="divLoy">
                                            <div class="divLoyHeader">
                                                <%#Eval("PassangerType")%>
                                            </div>
                                            <div class="divLoyleft">
                                                <asp:TextBox ID="txtloyCardEur" runat="server" CssClass="txtLoyleft" Style="color: #FFF;"
                                                    Text="Eurostar" ReadOnly="True" />
                                            </div>
                                            <div class="divLoyright">
                                                <asp:TextBox ID="txtloyCardNoEur" runat="server" CssClass="txtLoy LCE" MaxLength="25"
                                                    Text="308381" />
                                                <asp:TextBoxWatermarkExtender runat="server" ID="txtWEuro" WatermarkText="(card number)"
                                                    TargetControlID="txtloyCardNoEur" WatermarkCssClass="WmaxCss" />
                                                <asp:FilteredTextBoxExtender ID="tr1" runat="server" TargetControlID="txtloyCardNoEur"
                                                    ValidChars="0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                            </div>
                                            <div class="divLoyleft">
                                                <asp:TextBox ID="txtloyCardThy" runat="server" CssClass="txtLoyleft" Style="color: #FFF;"
                                                    Text="Thalys" ReadOnly="True" />
                                            </div>
                                            <div class="divLoyright">
                                                <asp:TextBox ID="txtloyCardNoThy" runat="server" CssClass="txtLoy LCT" MaxLength="25"
                                                    Text="308406" />
                                                <asp:TextBoxWatermarkExtender runat="server" ID="txtWThy" WatermarkText="(card number)"
                                                    TargetControlID="txtloyCardNoThy" WatermarkCssClass="WmaxCss" />
                                                <asp:FilteredTextBoxExtender ID="ftr2" runat="server" TargetControlID="txtloyCardNoThy"
                                                    ValidChars="0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div style="clear: both;">
                            </div>
                            <asp:Label runat="server" ID="lblmsg" />
                            <div class="swap-arr2" onclick="btnswapstation()" title="Swap">
                                <div class="swap-img">
                                    <img src="images/swap-arr.png" alt="" title="Swap" /></div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlAdult" />
                    <asp:AsyncPostBackTrigger ControlID="ddlChild" />
                    <asp:AsyncPostBackTrigger ControlID="ddlYouth" />
                    <asp:AsyncPostBackTrigger ControlID="ddlSenior" />
                </Triggers>
            </asp:UpdatePanel>
            <input id="hdntxtFromRailName" type="hidden" runat="server" clientidmode="Static" />
            <input id="hdntxtToRailName" type="hidden" runat="server" clientidmode="Static" />
            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updLoyalty"
                DisplayAfter="0" DynamicLayout="True">
                <ProgressTemplate>
                    <div class="modalBackground progessposition">
                    </div>
                    <div class="progess-inner2">
                        UPDATING RESULTS...
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
    <div class="clear">
    </div>
    <asp:Panel ID="pnlPassenger" runat="server" CssClass="modal-popup pnlpopupbox" Style="display: none">
        <div class="modalBackground modalBackgroundnew">
        </div>
        <div class="popup-inner pnlpopup" style="z-index: 111; position: fixed; left: 23%;
            top: 25%;">
            <div class="title">
                Please select passenger type
            </div>
            <p>
                Please enter at least 1 adult, senior or junior(youth) passenger.</p>
            <p>
                Children cannot travel unattended.</p>
            <p>
                There is a limitation for accompanied children:</p>
            <p>
                - Max. 4 children per passenger allowed.</p>
            <div>
                <button type="button" class="starail-Button btn-black newbutton closex" style="width: 20%!important;
                    float: right">
                    Cancel</button>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function btnswapstation() {
            var spantxtFrom = '';
            var txtFrom = '';
            spantxtFrom = $("#spantxtFrom").text();
            $("#spantxtFrom").text($("#spantxtTo").text());
            $("#spantxtTo").text(spantxtFrom);
            txtFrom = $("#txtFrom").val();
            $("#txtFrom").val($("#txtTo").val());
            $("#txtTo").val(txtFrom);
        }
        function coalapsibleh4() {
            $('.img-down-show').show();
            $('.img-up-show').hide();
            $('.coalapsible,.show-hide-img').click(function () {
                $(".coalapsible-div").slideToggle(function () {
                    if ($('.coalapsible-div').is(':visible') == true) {
                        $('.img-down-show').show();
                        $('.img-up-show').hide();
                    }
                    else {
                        $('.img-down-show').hide();
                        $('.img-up-show').show();
                    }
                });
            });
        }
    </script>
</asp:Content>
