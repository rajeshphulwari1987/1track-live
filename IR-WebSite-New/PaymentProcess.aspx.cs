﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Business;
using System.Web.Services;
using System.Collections.Generic;

public partial class PaymentProcess : Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageBooking MBooking = new ManageBooking();
    readonly private ManageOrder _master = new ManageOrder();
    public string currency = "$";
    public string currencyCode = "USD";
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public static Guid currencyID = new Guid();
    Guid siteId;
    public string script = "<script></script>";
    public string products = "";
    public string siteURL = "";
    public string OrderNo;
    public bool IsFirst = true;
    public string linkURL = string.Empty;
    public string ChkOutActionsTags = string.Empty;
    public string ChkOutSetActionsTags = string.Empty;
    public string EvOtherCharges = "0";
    public string EvChargesPopUp = string.Empty;
    public string EvChargeImages = string.Empty;
    public bool isEvolviBooking = false;
    BookingRequestUserControl objBRUC;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["IsCheckout"] = null;
            if (Request.Params["req"] != null)
            {
                OrderNo = Request.Params["req"];
                linkURL = "?req=" + OrderNo;
                DivPassSale.Visible = false;
                //lnkDetail.PostBackUrl = "P2PBookingCart.aspx";
            }
            else if (Session["OrderID"] != null)
            {
                //lnkDetail.PostBackUrl = "BookingCart.aspx";
                DivP2PSale.Visible = false;
                OrderNo = Session["OrderID"].ToString();
            }
            OrderDiscount1.Visible = OrderDiscount2.Visible = _master.GetOrderDiscountVisibleBySiteId(siteId);

            if (Session["BookingUCRerq"] != null)
            {
                objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
                if (objBRUC != null)
                    isEvolviBooking = !string.IsNullOrEmpty(objBRUC.OneHubServiceName) ? objBRUC.OneHubServiceName == "Evolvi" ? true : false : false;
            }

            if (Session["siteId"] != null)
            {
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
            GetCurrencyCode();
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(OrderNo))
                {
                    long OrderId = Convert.ToInt64(OrderNo);
                    var UpdateAgent = _db.tblOrders.FirstOrDefault(ty => ty.OrderID == OrderId);
                    if (UpdateAgent != null && !AgentuserInfo.IsAffliate)
                        UpdateAgent.AgentID = AgentuserInfo.UserID;
                    _db.SaveChanges();
                }

                if (AgentuserInfo.UserID != Guid.Empty)
                    new ManageProduct().UpdateOrderCommition(siteId, AgentuserInfo.UserID, Convert.ToInt64(OrderNo));
                else
                    new ManageProduct().UpdateOrderCommitionPublic(siteId, Convert.ToInt64(OrderNo));

                var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                if (objsite != null)
                {
                    if (objsite.PaymentType == 1)
                    {
                        Session.Add("redirectpage", linkURL);
                        btnContinue.Visible = false;
                    }
                    else
                    {
                        if (Session["redirectpage"] != null)
                            Session.Remove("redirectpage");
                        btnContinue.Visible = true;
                    }
                }
                GetEvolviTicketInfo();
                ShowPaymentDetails();
                QubitOperationLoad();
                GetDetailsAnalyticTags();
                OrderDiscount.Visible = new ManageOrder().GetOrderDiscountVisibleBySiteId(siteId);
                if (oManageClass.IsTiApi(Convert.ToInt64(OrderNo)))
                    hdnIsTI.Value = "True";
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void GetDetailsAnalyticTags()
    {
        ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
        var data = _masterAnalyticTag.GetTagBySiteId(siteId);
        if (data != null)
        {
            if (Session["GaTaggingProductlist"] != null)
            {
                var list = Session["GaTaggingProductlist"] as List<ProductPass>;
                string obj = "";
                string[] SplitData = data.Actions.Split(';');
                #region/*Checkout TAgging*/
                obj = "";
                SplitData = data.ChkOutActions.Split(';');
                string chkout = "";
                foreach (var item in list)
                {
                    chkout = SplitData[0].Replace("<Product Code>", "" + item.ProductCode + "");
                    chkout = chkout.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                    chkout = chkout.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                    chkout = chkout.Replace("<Price>", "" + item.Price + "");
                    chkout = chkout.Replace("<Quantity>", "" + item.Count + "");
                    obj += chkout + ";\n";
                    chkout = "";
                }
                if (SplitData.Count() > 1)
                    foreach (var item in list)
                    {
                        chkout = SplitData[1].Replace("<Product Code>", "" + item.ProductCode + "");
                        chkout = chkout.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                        chkout = chkout.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                        chkout = chkout.Replace("<Price>", "" + item.Price + "");
                        chkout = chkout.Replace("<Quantity>", "" + item.Count + "");
                        obj += chkout + ";\n";
                        chkout = "";
                    }
                ChkOutActionsTags = "\n<script type='text/javascript'>\n/*ChkOutActions*/\n" + obj + "\n</script>\n";
                ChkOutSetActionsTags = "\n<script type='text/javascript'>\n/*ChkOutSetActions*/\n" + data.ChkOutSetActions.Replace("<Step Number>", "'2'") + "\n</script>\n";
                #endregion
            }
        }
    }

    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public String GetJourney(string ProductType)
    {
        if (Session["TIRegionalBooking"] != null)
            return "";

        if (ProductType == "P2P")
        {
            if (IsFirst)
            {
                IsFirst = false;
                return "OUTBOUND:";
            }
            else
                return "INBOUND:";
        }
        return string.Empty;
    }

    public void ShowPaymentDetails()
    {
        try
        {

            if (!string.IsNullOrEmpty(OrderNo))
            {
                var lst = MBooking.GetAllCartData(Convert.ToInt64(OrderNo)).ToList();
                var lstNew = (from a in lst
                              select new
                              {
                                  PassSaleID = a.PassSaleID,
                                  ProductDesc = a.ProductType != "P2P" ? MBooking.GetProductName(a.PassSaleID) : string.Empty,
                                  Traveller = a.ProductType != "P2P" ? MBooking.GetTravellerName(a.PassSaleID) : string.Empty,
                                  Countries = MBooking.GetPassCountries(a.PassSaleID),
                                  //string.IsNullOrEmpty(MBooking.GetPassCountries(a.PassSaleID)) ? MBooking.GetCountryByIDs(a.Country) : MBooking.GetPassCountries(a.PassSaleID),
                                  Validity = a.ProductType != "P2P" ? MBooking.GetPassValidity(a.PassSaleID) : string.Empty,
                                  Protected = (a.TicketProtection.HasValue ? (a.TicketProtection.Value > 0 ? "Yes" : "No") : "No"),
                                  TKProtected = (a.TicketProtection.HasValue ? a.TicketProtection.Value : 0),
                                  ProtectedAmount = (a.TicketProtection.HasValue ? a.TicketProtection.Value > 0 ? currency + a.TicketProtection.Value : string.Empty : string.Empty),
                                  Trvname = a.ProductType != "P2P" ? a.Trvname : MBooking.GetP2PTravellerName(a.PassSaleID),
                                  Passengerinfo = a.Title + " " + a.FirstName + " " + a.MiddleName + " " + a.LastName.Split('<')[0],
                                  Price = (MBooking.getPrice(a.PassSaleID, a.ProductType)),
                                  Qty = 1,
                                  Arrives = a.ProductType == "P2P" ? MBooking.GetP2PArrives(a.PassSaleID) : string.Empty,
                                  Departs = a.ProductType == "P2P" ? MBooking.GetP2PDeparts(a.PassSaleID) : string.Empty,
                                  ArrivesStation = a.ProductType == "P2P" ? MBooking.GetP2PArrivesStation(a.PassSaleID) : string.Empty,
                                  DepartsStation = a.ProductType == "P2P" ? MBooking.GetP2PDepartsStation(a.PassSaleID) : string.Empty,
                                  Train = a.ProductType == "P2P" ? MBooking.GetP2PTrain(a.PassSaleID) : string.Empty,
                                  Class = a.ProductType == "P2P" ? MBooking.GetP2PClass(a.PassSaleID) : string.Empty,
                                  ServiceName = a.ProductType == "P2P" ? MBooking.GetP2PService(a.PassSaleID) : string.Empty,
                                  OrderIdentity = a.OrderIdentity,
                                  Jolurny = GetJourney(a.ProductType)
                              }).OrderBy(t => t.OrderIdentity).ToList();

                if (lst.Count > 0)
                {
                    lbltxttraveller.Text = string.Empty;
                    if (Request.Params["req"] == null)
                    {
                        RptPassDetails.DataSource = lstNew;
                        RptPassDetails.DataBind();
                        hdnProductPrice.Value = lstNew.Sum(t => t.Price).ToString("F");
                    }
                    else
                    {
                        hidelblnettotal.Attributes.Add("style", "display:none");
                        hidetxtnettotal.Attributes.Add("style", "display:none");
                        RptP2PDetails.DataSource = lstNew;
                        RptP2PDetails.DataBind();
                        hdnProductPrice.Value = lstNew.Sum(t => t.Price).ToString("F");
                    }
                    var lstData = lst.FirstOrDefault();
                    lblEmailAddress.Text = lstData.EmailAddress;
                    lblOrderDate.Text = lstData.CreatedOn.ToString("dd/MMM/yyyy");
                    lblOrderNumber.Text = lstData.OrderID.ToString();
                    lblShippingAmount.Text = (lstData.ShippingAmount ?? 0).ToString("F2");
                    lblDeliveryAddress.Text = lstData.DTitle + " " + lstData.DFirstName + " " + lstData.DLastName + ", " + lstData.Address1 + ", " + (!string.IsNullOrEmpty(lstData.Address2) ? lstData.Address2 + ", " : string.Empty) + "<br>" + (!string.IsNullOrEmpty(lstData.City) ? lstData.City + ", " : string.Empty) + (!string.IsNullOrEmpty(lstData.State) ? lstData.State + ", " : string.Empty) + lstData.DCountry + ", " + lstData.Postcode;
                    lblDeliveryAddress.Text = lblDeliveryAddress.Text.Replace("  , , <br>, ", string.Empty);
                    lblBookingFee.Text = Convert.ToDecimal(lstData.BookingFee).ToString("F2");
                    lblDiscount.Text = _master.GetOrderDiscountByOrderId(Convert.ToInt64(OrderNo));
                    txtDiscountCode.Text = _master.GetOrderDiscountCodeByOrderId(Convert.ToInt64(OrderNo));
                    lblAdminFee.Text = new ManageAdminFee().GetOrderAdminFeeByOrderID(Convert.ToInt32(OrderNo)).ToString();
                    if (lblAdminFee.Text == "0.00")
                    {
                        hidelblAdminfee.Attributes.Add("style", "display:none");
                        hidetxtAdminfee.Attributes.Add("style", "display:none");
                        lblAdminFee.Text = "0.00";
                    }
                    lblNetTotal.Text = (Convert.ToDecimal(lblAdminFee.Text) + (lstNew.Sum(a => a.Price) + lstNew.Sum(a => a.TKProtected)) + (lstData.ShippingAmount ?? 0) + (lstData.BookingFee ?? 0) + (!string.IsNullOrEmpty(EvOtherCharges) ? Convert.ToDecimal(EvOtherCharges) : 0)).ToString();
                    lblDiscountSum.Text = (Convert.ToDecimal(lblNetTotal.Text) - Convert.ToDecimal(lblDiscount.Text)).ToString("F2");

                    /*Start payment session*/
                    Session["Amount"] = lblDiscountSum.Text;
                    var tblcountry = _db.tblCountriesMsts.FirstOrDefault(x => x.CountryName == lstData.DCountry);
                    string IsoCode = tblcountry != null ? tblcountry.IsoCode : lstData.DCountry;
                    Session["CustomerData"] = lstData.Address1 + " " + lstData.Address2 + ";" + lstData.City + ";" + lstData.Postcode + ";" + IsoCode + ";" + lstData.EmailAddress;
                    /*End payment session*/

                    lblGrandTotal.Text = lblNetTotal.Text;
                    var sa = lstNew.GroupBy(t => new { t.Trvname }).Select(t => new
                    {
                        Qty = t.Sum(x => Convert.ToInt32(x.Qty)) + "",
                        TravellerName = t.Key.Trvname
                    });
                    foreach (var item in sa)
                    {
                        if (Request.Params["req"] == null)
                            lblDiscountTotalTxt.Text = lbltxttraveller.Text = (string.IsNullOrEmpty(lbltxttraveller.Text) ? string.Empty : lbltxttraveller.Text + "+") + (item.Qty + " " + item.TravellerName);
                        else
                            lblDiscountTotalTxt.Text = lbltxttraveller.Text = (string.IsNullOrEmpty(lbltxttraveller.Text) ? string.Empty : lbltxttraveller.Text + "+") + item.TravellerName;
                    }
                    nextStepGoto();

                    if (_master.GetOrderDiscountVisibleBySiteId(siteId) == false && lblAdminFee.Text == "0.00")
                        div_Discount_AdminFee.Visible = false;
                }
            }
            else
            {
                Response.Redirect("~/Home", false);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
            Response.Redirect("~/Agent/Agentpayment.aspx" + "?amt=" + lblDiscountSum.Text.Trim() + "&oid=" + OrderNo);
    }

    public void nextStepGoto()
    {
        if (!string.IsNullOrEmpty(OrderNo) && !string.IsNullOrEmpty(lblDiscountSum.Text))
        {
            var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
            bool IsWholeSale = (objsite != null ? objsite.IsWholeSale : false);

            if (objsite.PaymentType == 1)
            {
                PaymentDiv.Visible = true;
                btnWorldPay.Visible = false;
                btnContinue.Visible = false;
            }
            else if (objsite.PaymentType == 3)
            {
                PaymentDiv.Visible = false;
                btnWorldPay.Visible = true;
                btnContinue.Visible = false;
            }
            else if (objsite.PaymentType == 2)
            {
                PaymentDiv.Visible = false;
                btnWorldPay.Visible = false;
                btnContinue.Visible = true;
            }
        }
        else
        {
            Response.Redirect("~/Home");
        }
    }

    public void GetCurrencyCode()
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            currencyID = (Guid)oManageClass.GetCurrencyDetail(siteId).DefaultCurrencyID;
            currency = oManageClass.GetCurrency(currencyID);
            currencyCode = oManageClass.GetCurrencyShortCode(currencyID);
        }
    }

    protected void BtnDiscount_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hdnProductPrice.Value) && !string.IsNullOrEmpty(txtDiscountCode.Text) && MBooking.ValidDiscountCode(txtDiscountCode.Text, siteId, Convert.ToInt32(lblOrderNumber.Text)))
        {
            string DiscountAmount = MBooking.AddOrderDiscountData(txtDiscountCode.Text, siteId, Convert.ToInt64(lblOrderNumber.Text), Convert.ToDecimal(hdnProductPrice.Value));
            lblErrormsg.Visible = true;
            lblErrormsg.Text = "Thanks! Your code entitles you to " + (DiscountAmount.Contains("%") ? "" : currency) + DiscountAmount + " off your rail pass.";
        }
        else
        {
            MBooking.DeleteDiscountByOrderId(Convert.ToInt32(lblOrderNumber.Text));
            lblErrormsg.Visible = true;
            lblErrormsg.Text = "The promotional code you entered in not valid.";
        }
        OrderNo = lblOrderNumber.Text;
        ShowPaymentDetails();
        ScriptManager.RegisterStartupScript(this, typeof(Page), "reloadifram", "reloadifram()", true);
    }

    protected void btnWorldPay_Click(object sender, EventArgs e)
    {
        Response.Redirect("Payment.aspx");
    }

    public void GetEvolviTicketInfo()
    {
        try
        {
            var evChargesList = MBooking.GetEvolviChargesByOrderId(Convert.ToInt64(OrderNo));
            if (evChargesList != null)
            {
                EvOtherCharges = ((evChargesList.TicketChange + evChargesList.CreditCardCharge) - (evChargesList.Discount)).ToString("F2");
                lblTicketCharge.Text = EvOtherCharges;
                EvChargesPopUp += "<div class='starail-BookingDetails-form'>";

                EvChargesPopUp += "<div class='journey-header'><h3>Ticket Discount</h3></div>";
                EvChargesPopUp += "<div class='panel-heading'><p>" + currency + evChargesList.Discount.ToString("F2") + "</p></div>";

                EvChargesPopUp += "<div class='journey-header'><h3>Ticket Transaction Charge</h3></div>";
                EvChargesPopUp += "<div class='panel-heading'><p>" + currency + evChargesList.TicketChange.ToString("F2") + "</p></div>";

                EvChargesPopUp += "<div class='journey-header'><h3>Credit Card Transaction Charge</h3></div>";
                EvChargesPopUp += "<div class='panel-heading'><p>" + currency + evChargesList.CreditCardCharge.ToString("F2") + "</p></div>";

                EvChargesPopUp += "</div>";
                hideTicketText.Visible = false;   //hideTicketText.Visible = true;
                hideTicketValue.Visible = false;  //hideTicketValue.Visible = true;
                divChargesPopup.InnerHtml = EvChargesPopUp;
            }
            else
            {
                hideTicketText.Visible = false;
                hideTicketValue.Visible = false;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg2.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg2.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg2.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

}

