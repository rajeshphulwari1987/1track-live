﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TrainTickets.aspx.cs" Inherits="TrainTickets" Culture="en-GB" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContenzt" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .Form-select-100
        {
            width: 100% !important;
        }
        .Form-select-25
        {
            width: 100% !important;
        }
        .mintext
        {
            font-size: 12px;
            display: inline;
        }
        .mintext-new
        {
            font-size: 12px;
            display: -webkit-box;
        }
        .ui-datepicker-calendar
        {
            margin-left: 4px;
        }
        @media only screen and (max-width: 639px)
        {
            .starail-Form-inputContainer
            {
                overflow: visible !important;
            }
            .mintext
            {
                display: -webkit-box;
                position: inherit;
            }
        }
        .justify-align
        {
            text-align: justify;
        }
        .BookingDetails-form-white {
    background-color: White;
    padding: 9px;
    overflow: hidden;
}
    </style>
    <script type="text/javascript"> 
     $(document).ready(function () {
            $("#MainContent_txtTrainReturnDate").val("DD/MM/YYYY");
             $("#aTrainTicket").addClass("active"); 
      if ($("#txtFrom").val() != '') {
                //                alert(localStorage.getItem("spantxtTo"));
                $('#spantxtTo').text(localStorage.getItem("spantxtTo"));
            }
            if ($("#txtTo").val() != '') {
                //                alert(localStorage.getItem("spantxtFrom"));
                $('#spantxtFrom').text(localStorage.getItem("spantxtFrom"));
            }
            LoadCal1();
        });
       function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        
         function selectpopup(e) {
            if (count != 40 && count != 38 && count != 13 && count != 37 && count != 39 && count != 9) {
                var $this = $(e);
                var data = $this.val();
                var station = '';
                var hostName = window.location.host;
                var url = "http://" + hostName;

                if (window.location.toString().indexOf("https:") >= 0)
                    url = "https://" + hostName;

                if ($("#txtFrom").val() == '' && $("#txtTo").val() == '') {
                    $('#spantxtFrom').text('');
                    $('#spantxtTo').text('');
                }
                var filter = $("#span" + $this.attr('id') + "").text();
                if (filter == "" && $this.val() != "")
                    filter = $("#hdnFilter").val();
                $("#hdnFilter").val(filter);


                if ($this.attr('id') == 'txtTo') {
                    station = $("#txtFrom").val();
                }
                else {
                    station = $("#txtTo").val();
                }

                if (hostName == "localhost") {
                    url = url + "/InterRail";
                }
                var hostUrl = url + "/StationList.asmx/getStationsXList";
                data = data.replace(/[']/g, "♥");

                var $div = $("<div id='_bindDivData' onmousemove='_removehoverclass(this)'/>");
                $.ajax({
                    type: "POST",
                    url: hostUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'prefixText':'" + data + "','filter':'" + filter + "','station':'" + station + "'}",
                    success: function (msg) {
                        $('#_bindDivData').remove();
                        var lentxt = data.length;
                        $.each(msg.d, function (key, value) {
                            var splitdata = value.split('ñ');
                            var lenfull = splitdata[0].length; ;
                            var txtupper = splitdata[0].substring(0, lentxt);
                            var txtlover = splitdata[0].substring(lentxt, lenfull);
                            $this.after($div.append("<div class='popupselect' onclick='_Bindthisvalue(this)'><b class='_prifixText'>" + txtupper + "</b>" + txtlover + "<span style='display:none'>" + splitdata[0] + "</span><div style='display:none'>" + splitdata[1] + "</div>"));
                        });
                        $(".popupselect:eq(0)").attr('style', 'background-color: #ccc !important');
                    },
                    error: function () {
                        //                    alert("Wait...");
                    }
                });
            }
        }
        function _Bindthisvalue(e) {
            var idtxtbox = $('#_bindDivData').prev("input").attr('id');
            $("#" + idtxtbox + "").val($(e).find('span').text());
           if (idtxtbox == 'txtTo') {
                $('#spantxtFrom').text($(e).find('div').text());
                localStorage.setItem("spantxtFrom", $(e).find('div').text());
            }
            else {
                $('#spantxtTo').text($(e).find('div').text());
                localStorage.setItem("spantxtTo", $(e).find('div').text());
            }
             $('#_bindDivData').remove();
        }
        function _hideThisDiv(e) {
            $('#_bindDivData').remove();
        }
        function checkDate(sender) {
            var selectedDate = new Date(sender._selectedDate);
            var today = new Date();
            today.setHours(0, 0, 0, 0);

            if (selectedDate < today) {
                alert('Select a date sometime in the future!');
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }

        function CheckCardVal(sender, args) {
            if (document.getElementById('MainContent_chkLoyalty').checked) {
                var $lyltydiv = $('.divLoy');
                var countFalse = 0;
                $lyltydiv.each(function () {
                    if (($(this).find(".LCE").val() == '' || $(this).find(".LCE").val() == undefined) && ($(this).find(".LCT").val() == '' || $(this).find(".LCT").val() == undefined)) {
                        countFalse = 1;
                    }
                });

                if (countFalse == 1) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            } else {
                args.IsValid = true;
            }
        }
        function OnClientPopulating(sender, e) {
            sender._element.className = "input loading";
        }
        function OnClientCompleted(sender, e) {
            sender._element.className = "input";
        }
        var unavailableDates = '<%=unavailableDates1 %>';
        function nationalDays(date) {
            dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            if ($.inArray(dmy, unavailableDates) > -1) {
                return [false, "", "Unavailable"];
            }
            return [true, ""];
        }
      
        function LoadCal1() {
            $("#MainContent_txtDepartureDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 3,
                maxDate: '+6m',
            });
            $("#MainContent_txtTrainReturnDate").datepicker({
                numberOfMonths: 1,
                dateFormat: 'dd/M/yy',
                beforeShowDay: nationalDays,
                showButtonPanel: true,
                firstDay: 1,
                minDate: 0,
                maxDate: '+6m'
            });

            $(".imgCalSendBox").click(function () {
                $("#MainContent_txtDepartureDate").datepicker('show');
            });

            $(".imgCalSendBox1").click(function () {
                if ($('#MainContent_rdBookingType_1').is(':checked')) {
                    $("#MainContent_txtTrainReturnDate").datepicker('show');
                }
            });

            if ($('#MainContent_ucTrainSearch_rdBookingType_1').is(':checked')) {
                $("#txtReturnDate").datepicker('enable');
            }
            else {
                $("#txtReturnDate").datepicker('disable');
            }
            $("#MainContent_txtDepartureDate, #MainContent_txtTrainReturnDate").keypress(function (event) { event.preventDefault(); });
        }

        function calenableT() {
        callvalerror();
            LoadCal1();
            $("#MainContent_txtTrainReturnDate").datepicker('enable').val($("#MainContent_txtDepartureDate").val());
        }
        function caldisableT() {
        callvalerror();
            LoadCal1();
            $("#MainContent_txtTrainReturnDate").datepicker('disable').val("DD/MM/YYYY");
        }
    </script>
    <script type="text/javascript">
        function onTestChange() {
            if (window.event.keyCode == 13) {
                $("#MainContent_txtNotes").val($("#MainContent_txtNotes").val() + "\n");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="updLoyalty" runat="server">
        <ContentTemplate>
                    <div class="BookingDetails-form-white justify-align">
                        <div class="starail-Form-row warning" id="errorMsg" runat="server" visible="false">
                            <p  class="starail-Form-required">
                                I'm sorry we don't appear to be able to do that journey online at the moment, the
                                chances are we're able to do it offline though. Please complete the form below and
                                we'll get back to you with a quote.
                            </p>
                                <asp:Label ID="lblMessageEarlierTrain" runat="server" CssClass="starail-Form-required" Visible="false"></asp:Label>
                        </div>
                    </div>
            <asp:Panel ID="pnlJourneyInfo" Visible="true" runat="server">
                <div class="starail-BookingDetails-form">
                    <div class="starail-Form-row">
                        <div class="left-content" id="DivLeftOne" runat="server">
                            <h2>
                                <asp:Label ID="lblHeading" runat="server" Text="Ticket Request Form" />
                            </h2>
                        </div>
                    </div>
                    <div class="starail-Form-row justify-align">
                        Please complete the form below with as much detail as possible for your required
                        journey and one of our agents will come back to you with the details and a quotation.
                        Should there be more than one FIP card holder on the booking please add this detail
                        in the “Additional Notes” field. Likewise if you would like to request more than
                        1 journey at the same time please insert the detail in this field
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            FIP Card Number<span class="starail-Form-required">*</span> <span class="mintext-new">
                                (including prefix, if any)</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtFIP" runat="server" class="starail-Form-input" />
                                <asp:RequiredFieldValidator ID="rfFIP_reqvalerror" runat="server" Text="*" ErrorMessage="Please enter FIP number."
                                    ControlToValidate="txtFIP" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Title<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlTitle" runat="server" CssClass="starail-Form-select starail-Form-select--narrow Form-select-100">
                                    <asp:ListItem Selected="True" Value="Mr.">Mr.</asp:ListItem>
                                    <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                    <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            First Name<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtFName" runat="server" class="starail-Form-input" />
                                <asp:RequiredFieldValidator ID="rfName_reqvalerror" runat="server" Text="*" ErrorMessage="Please enter user name."
                                    ControlToValidate="txtFName" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Last Name
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtLName" runat="server" class="starail-Form-input" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Email<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtEmailAddress" runat="server" class="starail-Form-input" />
                                <asp:RequiredFieldValidator ID="rfEmail_reqvalerror" runat="server" Text="*" ErrorMessage="Please enter email address."
                                    ControlToValidate="txtEmailAddress" ForeColor="#ededed" ValidationGroup="vgsTR"
                                    Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" Text="*" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="txtEmailAddress" ForeColor="#ededed" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Display="Dynamic" ValidationGroup="vgsTR" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Contact number<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtPhone" runat="server" class="starail-Form-input" MaxLength="15"
                                    onkeypress="return isNumberKey(event)" />
                                <asp:RequiredFieldValidator ID="rfPhone_reqvalerror" runat="server" Text="*" ErrorMessage="Please enter contact phone."
                                    ControlToValidate="txtPhone" ForeColor="#ededed" ValidationGroup="vgsTR" Display="Dynamic" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Journey
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-u-cf starail-Form-switchRadioGroup train-result-journy">
                                <asp:RadioButtonList ID="rdBookingType" runat="server" ValidationGroup="vg" RepeatDirection="Horizontal"
                                    CssClass="switchRadioGroup" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="rdBookingType_SelectedIndexChanged">
                                    <asp:ListItem Value="0">One-way</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Return</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            From<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtFrom" runat="server" class="starail-Form-input" autocomplete="off" /><%-- onkeyup="selectpopup(this)"--%>
                                <span id="spantxtFrom" style="display: none"></span>
                                <asp:HiddenField ID="hdnFrm" runat="server" />
                                <asp:RequiredFieldValidator ID="rfFrom_reqvalerror" runat="server" ForeColor="#ededed"
                                    ValidationGroup="vgsTR" Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="Please enter From station."
                                    Text="*" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            To<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtTo" runat="server" class="starail-Form-input" autocomplete="off" /><%-- onkeyup="selectpopup(this)" --%>
                                <span id="spantxtTo" style="display: none"></span>
                                <asp:RequiredFieldValidator ID="rfTo_reqvalerror" runat="server" ForeColor="#ededed"
                                    ValidationGroup="vgsTR" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="Please enter To station."
                                    Text="*" CssClass="font14" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Depart<span class="starail-Form-required">*</span>
                        </div>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                            <div class="starail-Form-datePicker">
                                <asp:TextBox ID="txtDepartureDate" runat="server" Text="DD/MM/YYYY" class="starail-Form-input" />
                                <asp:RequiredFieldValidator ID="rfDepartureDate_reqvalerror" runat="server" ForeColor="#ededed"
                                    InitialValue="DD/MM/YYYY" ValidationGroup="vgsTR" Display="Dynamic" ControlToValidate="txtDepartureDate"
                                    ErrorMessage="Please enter departure date." Text="*" />
                                <asp:RegularExpressionValidator ID="regDepartureDate" runat="server" ControlToValidate="txtDepartureDate"
                                    ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                    Display="Dynamic" ForeColor="#ededed" SetFocusOnError="true" ErrorMessage="Invalid depart date"
                                    ValidationGroup="vgsTR">*</asp:RegularExpressionValidator>
                                <i class="starail-Icon-datepicker"></i>
                            </div>
                            <asp:DropDownList ID="ddldepTime" runat="server" class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                                <asp:ListItem>00:00</asp:ListItem>
                                <asp:ListItem>01:00</asp:ListItem>
                                <asp:ListItem>02:00</asp:ListItem>
                                <asp:ListItem>03:00</asp:ListItem>
                                <asp:ListItem>04:00</asp:ListItem>
                                <asp:ListItem>05:00</asp:ListItem>
                                <asp:ListItem>06:00</asp:ListItem>
                                <asp:ListItem>07:00</asp:ListItem>
                                <asp:ListItem>08:00</asp:ListItem>
                                <asp:ListItem Selected="True">09:00</asp:ListItem>
                                <asp:ListItem>10:00</asp:ListItem>
                                <asp:ListItem>11:00</asp:ListItem>
                                <asp:ListItem>12:00</asp:ListItem>
                                <asp:ListItem>13:00</asp:ListItem>
                                <asp:ListItem>14:00</asp:ListItem>
                                <asp:ListItem>15:00</asp:ListItem>
                                <asp:ListItem>16:00</asp:ListItem>
                                <asp:ListItem>17:00</asp:ListItem>
                                <asp:ListItem>18:00</asp:ListItem>
                                <asp:ListItem>19:00</asp:ListItem>
                                <asp:ListItem>20:00</asp:ListItem>
                                <asp:ListItem>21:00</asp:ListItem>
                                <asp:ListItem>22:00</asp:ListItem>
                                <asp:ListItem>23:00</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Train Number <span class="mintext">(Depart)</span>
                        </div>
                        <div class="starail-Form-inputContainer train-result-journyFromTo">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txttnodept" runat="server" class="starail-Form-input" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div id="returnspan" runat="server" visible="false">
                        <div class="starail-Form-row">
                            <div class="starail-Form-label">
                                Return<span class="starail-Form-required">*</span>
                            </div>
                            <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGroup">
                                <div class="starail-Form-datePicker">
                                    <asp:TextBox ID="txtTrainReturnDate" runat="server" Text="DD/MM/YYYY" class="starail-Form-input" />
                                    <asp:RequiredFieldValidator ID="rfReturnDate_reqvalerror" runat="server" ForeColor="#ededed"
                                        ValidationGroup="vgsTR" InitialValue="DD/MM/YYYY" Display="Dynamic" ControlToValidate="txtTrainReturnDate"
                                        ErrorMessage="Please enter return date." Text="*" />
                                    <asp:RegularExpressionValidator ID="regReturnDate" runat="server" ControlToValidate="txtTrainReturnDate"
                                        ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/\d{4}$"
                                        Display="Dynamic" ForeColor="#ededed" SetFocusOnError="true" ErrorMessage="Invalid return date"
                                        ValidationGroup="vgsTR">*</asp:RegularExpressionValidator>
                                    <i class="starail-Icon-datepicker"></i>
                                </div>
                                <asp:DropDownList ID="ddlReturnTime" runat="server" class="starail-Form-select starail-Form-inputContainer--time starail-Form-inputContainer--last">
                                    <asp:ListItem>00:00</asp:ListItem>
                                    <asp:ListItem>01:00</asp:ListItem>
                                    <asp:ListItem>02:00</asp:ListItem>
                                    <asp:ListItem>03:00</asp:ListItem>
                                    <asp:ListItem>04:00</asp:ListItem>
                                    <asp:ListItem>05:00</asp:ListItem>
                                    <asp:ListItem>06:00</asp:ListItem>
                                    <asp:ListItem>07:00</asp:ListItem>
                                    <asp:ListItem>08:00</asp:ListItem>
                                    <asp:ListItem Selected="True">09:00</asp:ListItem>
                                    <asp:ListItem>10:00</asp:ListItem>
                                    <asp:ListItem>11:00</asp:ListItem>
                                    <asp:ListItem>12:00</asp:ListItem>
                                    <asp:ListItem>13:00</asp:ListItem>
                                    <asp:ListItem>14:00</asp:ListItem>
                                    <asp:ListItem>15:00</asp:ListItem>
                                    <asp:ListItem>16:00</asp:ListItem>
                                    <asp:ListItem>17:00</asp:ListItem>
                                    <asp:ListItem>18:00</asp:ListItem>
                                    <asp:ListItem>19:00</asp:ListItem>
                                    <asp:ListItem>20:00</asp:ListItem>
                                    <asp:ListItem>21:00</asp:ListItem>
                                    <asp:ListItem>22:00</asp:ListItem>
                                    <asp:ListItem>23:00</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                        </div>
                        <div class="starail-Form-row">
                            <div class="starail-Form-label">
                                Train Number <span class="mintext">(Return)</span>
                            </div>
                            <div class="starail-Form-inputContainer train-result-journyFromTo">
                                <div class="starail-Form-inputContainer-col">
                                    <asp:TextBox ID="txttnoreturn" runat="server" class="starail-Form-input" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                        </div>
                    </div>
                    <div class="starail-Form-row starail-u-cf starail-SearchTickets-quantity">
                        <label for="" class="starail-Form-label">
                            Who's going?</label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlAdult" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                </asp:DropDownList>
                                <label for="starail-adult">
                                    Adults <span class="mintext">(26-65 at time of travel)</span></label>
                            </div>
                            <div class="starail-Form-inputContainer-col train-result-col2">
                                <asp:DropDownList ID="ddlChild" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                </asp:DropDownList>
                                <label for="starail-children">
                                    Children <span class="mintext">(under 17 at time of travel)</span></label>
                            </div>
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlYouth" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                </asp:DropDownList>
                                <label for="starail-adult">
                                    Youths <span class="mintext">(17-25 at time of travel)</span></label>
                            </div>
                            <div class="starail-Form-inputContainer-col train-result-col2">
                                <asp:DropDownList ID="ddlSenior" runat="server" CssClass="starail-Form-select starail-Form-select--narrow">
                                </asp:DropDownList>
                                <label for="starail-adult">
                                    Seniors <span class="mintext">(over 66 at time of travel)</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Class Preference
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlClass" runat="server" class="starail-Form-select starail-Form-select--narrow Form-select-100">
                                    <asp:ListItem Value="0">All</asp:ListItem>
                                    <asp:ListItem Value="1">1st</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="2">2nd</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Max Transfers
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlTransfer" runat="server" class="starail-Form-select starail-Form-select--narrow Form-select-100">
                                    <asp:ListItem Value="0">Direct Trains only</asp:ListItem>
                                    <asp:ListItem Value="1">Max. 1 transfer</asp:ListItem>
                                    <asp:ListItem Value="2">Max. 2 transfers</asp:ListItem>
                                    <asp:ListItem Value="3" Selected="True">Show all</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            &nbsp;
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:CheckBox ID="chkLoyalty" runat="server" AutoPostBack="true" OnCheckedChanged="chkLoyalty_CheckedChanged" />
                                <strong class="disable-label f-left">Loyalty cards</strong> <span id="divRailPass"
                                    runat="server" visible="False">
                                    <asp:CheckBox ID="chkIhaveRailPass" runat="server" AutoPostBack="true" OnCheckedChanged="chkIhaveRailPass_CheckedChanged" />
                                    <strong class="disable-label f-left">I have a rail pass </strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                            Additional Notes
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtNotes" runat="server" class="starail-Form-input" TextMode="MultiLine"
                                    MaxLength="2000" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                    </div>
                    <div class="starail-Form-row">
                        <div class="starail-Form-label">
                        </div>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:Button ID="btnSendInfo" runat="server" Text="Send Info" CssClass="starail-Button starail-Form-button starail-Form-button--primary"
                                    ValidationGroup="vgsTR" OnClick="btnSendInfo_Click" />
                            </div>
                        </div>
                    </div>
                    <div style="color: red">
                        * Please complete all mandatory fields.</div>
                </div>
                <asp:Label runat="server" ID="lblmsg" />
                </div>
            </asp:Panel>
            <div class="left-content" id="DivLeftSecond" runat="server" visible="false">
                <div class="success">
                    <asp:Label runat="server" ID="succmessage"></asp:Label>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlAdult" />
            <asp:AsyncPostBackTrigger ControlID="ddlChild" />
            <asp:AsyncPostBackTrigger ControlID="ddlYouth" />
            <asp:AsyncPostBackTrigger ControlID="ddlSenior" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updLoyalty"
        DisplayAfter="0" DynamicLayout="True">
        <ProgressTemplate>
            <div class="modalBackground progessposition">
            </div>
            <div class="progess-inner2">
                Shovelling coal into the server...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
