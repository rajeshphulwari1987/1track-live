﻿#region Using
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
#endregion

public partial class RailpassDetail : Page
{
    readonly Masters _masterPage = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public static string ClassName = string.Empty;
    public static string SortcolumnName = string.Empty;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public ManageProduct _master = new ManageProduct();
    readonly ManageCountry _cMaster = new ManageCountry();
    readonly ManageBooking _BMaster = new ManageBooking();
    readonly ManageAffiliateUser _AffManage = new ManageAffiliateUser();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    string con = System.Configuration.ConfigurationManager.ConnectionStrings["db_Entities"].ToString();
    public static List<getCuntryCode> Levellist = new List<getCuntryCode>();
    public bool passportValid = false;
    public string currency;
    string curID;
    Guid siteId;
    Guid productCurID;
    static int startDrp = 0;
    static int endDrp = 10;
    static bool IsSaverTraveller = false;
    public string script = "<script></script>";
    public string siteURL;
    public string ActionsTags, specialOfferText;
    public string SetActionsTags;
    bool IsReadonly = false;
    public decimal TicketProtection = 0;
    List<headername> Lstheader = new List<headername>();
    private ManageScriptingTag _ManageScriptingTag = new ManageScriptingTag();
    public string IRScriptingTagJs, ProductNames = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            siteId = Guid.Parse(Session["siteId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var Protection = FrontEndManagePass.GetTicketProtectionPrice(siteId);
        if (Protection != null)
            TPmessageDiv.Attributes.Add("style", "display:block");

        if (Request.QueryString["af"] != null)
        {
            bool Result = _AffManage.GetAffURLIsActiveByAffCode(Request.QueryString["af"], Request.Url.ToString());
            if (Result)
                Session["AffCode"] = Request.QueryString["af"];
            else
                Response.Redirect(siteURL + "home");
        }
        if (!IsPostBack)
        {
            Session["ManageAgentData"] = null;
            if (Request.Params["id"] != null)
                Page.RouteData.Values["PrdId"] = Request.Params["id"];
            if (Page.RouteData.Values["PrdId"] != null)
            {
                Guid prdIDs = Guid.Parse(Page.RouteData.Values["PrdId"].ToString());
                ViewState["prdID"] = prdIDs;

                #region Seobreadcrumbs
                var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(prdIDs, siteId, true);
                if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                    litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
                #endregion

                var isActvieP = _master.IsActiveProductWithEnableForPurchase(Guid.Parse(ViewState["prdID"].ToString()), siteId);
                if (!isActvieP)
                    Response.Redirect(siteURL);
            }

            if (ViewState["prdID"] != null)
            {
                /*********if Product Foc-AD75**********/
                var isPrdFoc = _master.PrdIsFoc(Guid.Parse(ViewState["prdID"].ToString()));
                if (isPrdFoc)
                {
                    var isAgentSite = false;
                    var objsite = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
                    if (objsite != null)
                    {
                        if (objsite.IsAgent != null) isAgentSite = (bool)objsite.IsAgent;
                    }
                    if (AgentuserInfo.UserID != Guid.Empty && !AgentuserInfo.IsAffliate)
                    {
                        var agentId = AgentuserInfo.UserID;
                        var isFoc = _oWebsitePage.IsFocAD75(agentId);
                        if (!isFoc || !isAgentSite)
                            Response.Redirect("home");
                    }
                    else
                        Response.Redirect("home");
                }
                /*************************************/
            }
            Response.Cache.SetNoStore();
            var pageId = _db.tblWebMenus.FirstOrDefault(ty => ty.PageName.Contains("AboutUs.aspx"));
            if (pageId != null)
                PageContent(pageId.ID, siteId);
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.Contains("home"));
            string QStr = (ViewState["prdID"] == null ? "" : ViewState["prdID"].ToString());
            Guid productCatID;
            if (Guid.TryParse(QStr, out productCatID))
                Session["getproductCatID"] = productCatID;
            else
                Session["getproductCatID"] = Guid.Empty;
            SortcolumnName = string.Empty;
            ClassName = "Asc";
            Session.Remove("CountryCodeList");
            Session.Remove("GETcountryCODE");
            QubitOperationLoad();
        }
        PageLoadEvent();
        GetDetailsAnalyticTags();
        GetScriptingTag();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "runScript", "innertabActive();", true);
    }

    public void GetDetailsAnalyticTags()
    {
        ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
        var data = _masterAnalyticTag.GetTagBySiteId(siteId);
        if (data != null)
        {
            if (Session["GaTaggingProductlist"] != null)
            {
                var list = Session["GaTaggingProductlist"] as List<ProductPass>;
                string obj = "";
                string[] SplitData = data.Actions.Split(';');
                #region/*Action TAgging*/
                string Actions = "";
                foreach (var item in list)
                {
                    Actions = SplitData[0].Replace("<Product Code>", "" + item.ProductCode + "");
                    Actions = Actions.Replace("<Product Name>", "" + item.Name.Replace("'", "’") + "");
                    Actions = Actions.Replace("<Product Category>", "" + item.CategoryName.Replace("'", "’") + "");
                    Actions = Actions.Replace("<Price>", "" + (string.IsNullOrEmpty(item.Price) ? "0.00" : item.Price) + "");
                    Actions = Actions.Replace("<Quantity>", "" + (item.Count == 0 ? 1 : item.Count) + "");
                    obj += Actions + ";\n";
                    Actions = "";
                }
                ActionsTags = "\n<script type='text/javascript'>\n/*Actions*/\n" + obj + "\n</script>\n";
                SetActionsTags = "\n<script type='text/javascript'>\n/*ActionsSet*/\n" + data.SetActions + "\n</script>\n";
                #endregion
            }
        }
    }

    public void PageContent(Guid pageId, Guid siteId)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageId && x.SiteID == siteId);
            if (result != null)
            {
                var url = result.Url;
                tblPage oPage = _masterPage.GetPageDetailsByUrl(url);
                string[] arrListId = oPage.BannerIDs.Split(',');
                List<int> idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
            }
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    void PageLoadEvent()
    {
        if (ViewState["prdID"] == null)
            return;
        try
        {
            GetCurrencyCode();
            var pId = new Guid(ViewState["prdID"].ToString());
            tblProduct oProd = oManageClass.GetProductById(pId);
            specialOfferText = oProd.SpecialOfferText;
            if (!string.IsNullOrEmpty(specialOfferText))
                divspecialOfferText.InnerHtml = (specialOfferText.Replace(Environment.NewLine, "<br />"));
            hdnIstwinpass.Value = oProd.Istwinpass.ToString();
            passportValid = oProd.PassportValid.HasValue ? oProd.PassportValid.Value : false;
            int CULVL_Code = oProd.CountryStartCode;
            GetCountryLevel(CULVL_Code);
            if (Session["GetcunLvl"] != null && Session["GetcunLvl"].ToString() == "0")
            {
                ViewState["setget"] = oProd.CountryStartCode + "," + oProd.CountryEndCode;
            }
            else
            {
                /*check for 3,4,5 country pass*/
                divucCountryLevel.Visible = true;
                dvOtherCountry.Visible = false;
            }

            string productName = ProductNames = oManageClass.GetProductName(oProd.ID);
            getproductname.InnerText = oManageClass.GetProductName(oProd.ID);



            productCurID = oProd.CurrencyID;
            var pDetails = _master.GetProductTermsById(pId);
            if (pDetails != null)
            {
                divDescription.InnerHtml = Server.HtmlDecode(pDetails.Description);
                divDiscount.InnerHtml = Server.HtmlDecode(pDetails.Discount);
                divEligibility.InnerHtml = Server.HtmlDecode(pDetails.Eligibility);
                divEligibilityforSaver.InnerHtml = Server.HtmlDecode(pDetails.EligibilityforSaver);
                divTerm.InnerHtml = Server.HtmlDecode(pDetails.TermCondition);
                divValidity.InnerHtml = Server.HtmlDecode(pDetails.Validity);

                if (!string.IsNullOrEmpty(Server.HtmlDecode(pDetails.GreatPassDescription)))
                    div_GreatPassDescription.InnerHtml = "<h3>This pass is great because...</h3>" + Server.HtmlDecode(pDetails.GreatPassDescription);
                else
                    div_GreatPassDescription.Attributes.Add("style", "display:none");
            }

            var travller = oManageClass.GetProductTraveller(pId);
            travller = travller.Select(t => new Traveller
                        {
                            ID = t.ID,
                            Name = t.Name,
                            Age = (t.AgeTo > 50 ? t.AgeFrom + " + " : t.AgeFrom + " - " + t.AgeTo),
                            SortOrder = (t.SortOrder == null ? 0 : t.SortOrder)
                        }).Distinct().OrderBy(x => x.SortOrder).ToList();

            hdnIsBritral.Value = _BMaster.GetIsBritrailByProductID(pId).ToString();
            rptTraveller.DataSource = travller;
            rptTraveller.DataBind();
            rptTravellerGrid.DataSource = oManageClass.GetProductTraveller(pId);
            rptTravellerGrid.DataBind();
            var countryId = oManageClass.GetCountryIdOfProduct(pId);
            var Protection = FrontEndManagePass.GetTicketProtectionPrice(siteId);
            if (Protection != null)
                TicketProtection = Protection.Amount;
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    public void GetCountryLevel(int CulevelID)
    {
        if (CulevelID > 2000 && CulevelID < 3001)
            Session["GetcunLvl"] = 2;
        else if (CulevelID > 3000 && CulevelID < 4001)
            Session["GetcunLvl"] = 3;
        else if (CulevelID > 4000 && CulevelID < 5001)
            Session["GetcunLvl"] = 4;
        else if (CulevelID > 5000 && CulevelID < 6001)
            Session["GetcunLvl"] = 5;
        else
            Session["GetcunLvl"] = 0;
    }

    public void GetProductListByTraveler(Guid id, Guid pid, Guid sid, GridView gv)
    {
        try
        {
            var AgentId = new Guid();
            if (Session["AgentUserID"] == null)
            {
                AgentId = Guid.NewGuid();
            }
            else
                AgentId = Guid.Parse(Session["AgentUserID"].ToString());

            gv.DataSource = null;
            gv.DataBind();
            using (var conn = new SqlConnection(con))
            {
                var sqlCommand = new SqlCommand("spGetProductListByTravellerId", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TravelerID", id);
                sqlCommand.Parameters.AddWithValue("@ProductID", pid);
                sqlCommand.Parameters.AddWithValue("@SiteID", sid);
                sqlCommand.Parameters.AddWithValue("@AgentID", AgentId.ToString());
                conn.Open();
                var da = new SqlDataAdapter(sqlCommand);
                var ds = new DataSet();
                da.Fill(ds);
                var dv = ds.Tables[0].DefaultView;
                dv.Sort = SortcolumnName;

                gv.DataSource = dv;
                gv.DataBind();
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;//.Message + ";;;" + ex.InnerException + ";;;" + ex.StackTrace;
        }
    }

    protected void grdPrice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Visible = false;
        e.Row.Cells[1].Visible = false;
        int countCell = e.Row.Cells.Count;
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            Lstheader.Clear();
            e.Row.CssClass = "starail-PassChoiceTable-headerRow starail-u-cf";
            e.Row.Cells[2].CssClass = "starail-PassChoiceTable-col";
            if (countCell > 3)
            {
                for (int i = 3; i < countCell; i++)
                {
                    headername obj = new headername();
                    obj.id = i;
                    obj.name = e.Row.Cells[i].Text;
                    Lstheader.Add(obj);
                    int k = i;
                    e.Row.Cells[k].CssClass = "starail-PassChoiceTable-col";
                    var lblname = new Label();
                    lblname.ID = "lblname" + i;
                    lblname.Text = e.Row.Cells[i].Text;
                    var LnkSortID = new LinkButton();//Get C0untry StartCode EndCpode
                    LnkSortID.ID = "LnkSortID" + i;
                    LnkSortID.CausesValidation = false;
                    LnkSortID.Attributes.Add("class", "SortBtnStyle " + ClassName);
                    LnkSortID.Width = 20;
                    LnkSortID.Height = 20;
                    LnkSortID.Click += new EventHandler(LnkSortID_Click);
                    if (ClassName == "Asc")
                    {
                        string name = SortcolumnName.Replace(" Desc", string.Empty);
                        if (e.Row.Cells[i].Text == name)
                        {
                            LnkSortID.Text = "▲";
                            LnkSortID.ToolTip = "Desc";
                        }
                        else
                        {
                            LnkSortID.Text = "▼";
                            LnkSortID.ToolTip = "Asce";
                        }
                    }
                    else
                    {
                        LnkSortID.Text = "▼";
                        LnkSortID.ToolTip = "Asce";
                    }
                    e.Row.Cells[k].Controls.Add(lblname);
                    e.Row.Cells[k].Controls.Add(LnkSortID);
                    ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(LnkSortID);
                }
            }
        }
        else
        {
            if (countCell > 3)
            {
                e.Row.CssClass = "starail-PassChoiceTable-row starail-u-cf js-accordonItem";
                e.Row.Cells[2].CssClass = "starail-PassChoiceTable-col starail-PassChoiceTable-header js-accordonHeader data-list-class-grid-td-first";
                for (int i = 3; i < countCell; i++)
                {
                    Literal ltrfirst = new Literal();
                    Label ltrName = new Label { ID = "ltrName" + i, Text = string.IsNullOrEmpty(e.Row.Cells[i].Text) || e.Row.Cells[i].Text == "&nbsp;" ? "-" : e.Row.Cells[i].Text };
                    HiddenField hdnActualPrice = new HiddenField { ID = "hdnActualPrice" + i };
                    Label ltrCurrency = new Label { ID = "ltrCurr" + i, Text = currency };
                    HiddenField hdnClass = new HiddenField { Value = "CLASS-01" + e.Row.RowIndex };
                    Label lblmult = new Label { Text = "X" };
                    Literal ltrsecond = new Literal();
                    Label lblpassanger = new Label { Text = "Passengers" };
                    Literal ltrthird = new Literal { Text = "</div>" };
                    int k = i;
                    e.Row.Cells[k].CssClass = "starail-PassChoiceTable-bigCol data-list-class-grid-td-secound";

                    ltrName.Attributes.Add("class", "ltrName");
                    ltrName.Attributes.Add("Style", "padding-right:4px;");
                    if (ltrName.Text != "-")
                    {
                        hdnActualPrice.Value = ltrName.Text;
                        ltrName.Text = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(ltrName.Text), siteId, productCurID, Guid.Parse(curID))));
                    }
                    if (i == 3)
                    {
                        ltrfirst.Text = "<div class='starail-PassChoiceTable-col data-list-class-grid-td-div-first'><a href='#' class='js-lightboxOpen starail-u-hideMobile' data-lightbox-id='starail-ticket-info'><i class='starail-Icon-questions'></i></a><div class='starail-PassChoiceTable-passDetails starail-u-showMobile'><p></p></div></div>";
                        e.Row.Cells[k].Controls.Add(ltrfirst);
                        ltrsecond.Text = "<div class='starail-PassChoiceTable-col starail-PassChoiceTable-inputContainer data-list-class-grid-td-div-secound-first'><span class='starail-PassChoiceTable-mobileLabel'>" + Lstheader.FirstOrDefault(tblAdminFaq => tblAdminFaq.id == i).name + "</span>";
                    }
                    else
                    {
                        ltrsecond.Text = "<div class='starail-PassChoiceTable-col starail-PassChoiceTable-inputContainer data-list-class-grid-td-div-secound'><span class='starail-PassChoiceTable-mobileLabel'>" + Lstheader.FirstOrDefault(tblAdminFaq => tblAdminFaq.id == i).name + "</span>";
                    }
                    e.Row.Cells[k].Controls.Add(ltrsecond);
                    e.Row.Cells[k].Controls.Add(hdnClass);
                    if (ltrName.Text != "-")
                        e.Row.Cells[k].Controls.Add(ltrCurrency);
                    e.Row.Cells[k].Controls.Add(hdnActualPrice);
                    e.Row.Cells[k].Controls.Add(ltrName);
                    if (ltrName.Text != "-")
                        e.Row.Cells[k].Controls.Add(lblmult);
                    if (!string.IsNullOrEmpty(e.Row.Cells[i].Text) && e.Row.Cells[i].Text != "&nbsp;")
                    {
                        HiddenField hdnCLvlID = new HiddenField { ID = "hdnCLvlID" + i };
                        DropDownList ddl = new DropDownList();
                        ddl.ID = "ddlID" + i;
                        ddl.Attributes.Add("onchange", "callabc(this);");
                        ddl.Attributes.Add("class", "js-notificationCountPart data-list-class-grid-td-div-select");
                        if (IsReadonly && Convert.ToDecimal(hdnActualPrice.Value) < 1)
                            ddl.Attributes.Add("child", "child");

                        if (IsSaverTraveller)
                            endDrp = 5;
                        for (int j = startDrp; j <= endDrp; j++)
                            ddl.Items.Add(new ListItem(j.ToString(), j.ToString()));
                        if (IsSaverTraveller)
                        {
                            ddl.Items.RemoveAt(1);
                            if (Convert.ToBoolean(hdnIsBritral.Value))
                                ddl.Items.RemoveAt(1);
                        }
                        endDrp = 10;

                        e.Row.Cells[k].Controls.Add(ddl);
                        e.Row.Cells[i].Controls.Add(hdnCLvlID);
                        e.Row.Cells[k].Controls.Add(lblpassanger);
                        e.Row.Cells[k].Controls.Add(ltrthird);
                        if (ViewState["setget"] != null)
                        {
                            string[] ViewStateData = ViewState["setget"].ToString().Split(',');
                            hdnCLvlID.Value = ViewStateData[0] + "," + ViewStateData[1] + "ñ,,,,";
                        }
                    }
                }
            }
        }
    }

    protected void LnkSortID_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        string id = btn.ID.Replace("LnkSortID", "lblname");
        var lbl = (Label)btn.Parent.FindControl(id);
        if (ClassName == "Asc")
        {
            SortcolumnName = lbl.Text + " " + ClassName;
            ClassName = "Desc";
        }
        else
        {
            SortcolumnName = lbl.Text + " " + ClassName;
            ClassName = "Asc";
        }
        PageLoadEvent();
        UpdatePanel1.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "getcurrentLinkPosition();callabc();collChangeLinkPosition();", true);
    }

    protected void rptTravellerGrid_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var grdPrice = e.Item.FindControl("grdPrice") as GridView;
            var lblID = e.Item.FindControl("lblID") as Label;
            var hdnName = e.Item.FindControl("hdnT") as HiddenField;
            if (lblID != null)
            {
                ViewState["tID"] = lblID.Text; /*added for saver alert*/
                if (ViewState["prdID"] != null)
                {
                    IsReadonly = false;
                    if (hdnName.Value.ToLower().Contains("child") || hdnName.Value.ToLower().Contains("family"))
                        IsReadonly = true;
                    if (hdnName.Value.ToLower().Contains("saver"))
                        IsSaverTraveller = true;
                    else
                        IsSaverTraveller = false;
                    GetProductListByTraveler(Guid.Parse(lblID.Text), Guid.Parse(ViewState["prdID"].ToString()), Guid.Parse(Session["siteId"].ToString()), grdPrice);
                }
            }
        }
    }

    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        try
        {
            var list = new List<getRailPassData>();
            Guid PrdtId = Guid.Empty;
            string PrdtName = string.Empty;
            string TravellerID = string.Empty;
            string TravellerName = string.Empty;
            string ValidityName = string.Empty;
            string Price = string.Empty;
            string Actualprice = string.Empty;
            string Qty = string.Empty;
            string CountryLevelCode = string.Empty;
            string MonthValidity = string.Empty;
            Decimal BookingFee = 0;
            if (ViewState["prdID"] != null)
            {
                PrdtId = Guid.Parse(ViewState["prdID"].ToString());
                PrdtName = _master.GetProductName(PrdtId);
                MonthValidity = _master.GetMonthValidity(PrdtId);
            }
            foreach (RepeaterItem item in rptTravellerGrid.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    var grdPrice = item.FindControl("grdPrice") as GridView;
                    var lblTabId = item.FindControl("lblID") as Label;
                    if (grdPrice != null)
                    {
                        foreach (GridViewRow row in grdPrice.Rows)
                        {
                            TravellerID = lblTabId.Text;
                            foreach (RepeaterItem _TabName in rptTraveller.Items)
                            {
                                var lnkTabName = _TabName.FindControl("lnkTraveller") as LinkButton;
                                if (lnkTabName != null && lnkTabName.CommandArgument == TravellerID)
                                {
                                    TravellerName = lnkTabName.Attributes["TxtName"];
                                    break;
                                }
                            }
                            ValidityName = row.Cells[2].Text;
                            int countCell = row.Cells.Count;
                            var AgentId = new Guid();
                            if (Session["AgentUserID"] == null)
                            {
                                AgentId = Guid.NewGuid();
                            }
                            else
                                AgentId = Guid.Parse(Session["AgentUserID"].ToString());
                            for (int i = countCell; i > 0; i--)
                            {
                                string ltrName = "ltrName" + i;
                                string ddlIDs = "ddlID" + i;
                                string hdnCLvlIDs = "hdnCLvlID" + i;
                                string uppnls = "upd" + i;
                                string hdnPrice = "hdnActualPrice" + i;

                                var hdnActualPrice = row.FindControl(hdnPrice) as HiddenField;
                                var lblPrce = row.FindControl(ltrName) as Label;
                                var ddlID = row.FindControl(ddlIDs) as DropDownList;
                                var hdnCLvlID = row.FindControl(hdnCLvlIDs) as HiddenField;

                                if (ddlID != null && !ddlID.SelectedValue.Equals("0"))
                                {
                                    string ClassName = grdPrice.HeaderRow.Cells[i].Text;
                                    Qty = ddlID.SelectedValue;
                                    CountryLevelCode = hdnCLvlID.Value;
                                    Actualprice = hdnActualPrice.Value.Trim();
                                    Price = lblPrce.Text;

                                    string[] strSplit_dIDandCode = CountryLevelCode.Split('ñ');
                                    string[] strCode = strSplit_dIDandCode[0].Split(',');

                                    var listdata = _BMaster.GetClassAndValidityID(Actualprice, ValidityName, ClassName, TravellerName, PrdtId, Guid.Parse(TravellerID), siteId, AgentId);
                                    for (int j = 0; j < Convert.ToInt32(Qty); j++)
                                    {
                                        BookingFee = _BMaster.GetCategoryOrProductBookingFeeById(siteId,listdata.CategoryID, PrdtId, Convert.ToDecimal(Price));
                                        var Id1 = Guid.NewGuid();
                                        if (listdata != null)
                                            list.Add(new getRailPassData
                                            {
                                                OriginalPrice = Convert.ToString(listdata.OriginalPrice),
                                                Id = Id1,
                                                PrdtId = PrdtId.ToString(),
                                                TravellerID = TravellerID,
                                                ClassID = listdata.classid.ToString(),
                                                ValidityID = listdata.Validityid.ToString(),
                                                CategoryID = listdata.CategoryID.ToString(),
                                                IsBritrail = _BMaster.GetIsBritrailPassByCatID(listdata.CategoryID),
                                                IsEurail = _BMaster.GetIsEurailPassByCatID(listdata.CategoryID),
                                                PrdtName = PrdtName,
                                                TravellerName = TravellerName,
                                                ClassName = ClassName,
                                                ValidityName = ValidityName,
                                                Price = Price,
                                                SalePrice = Actualprice,
                                                Qty = "1",
                                                CountryStartCode = strCode[0],
                                                CountryEndCode = strCode[1],
                                                Commission = listdata.commission.ToString(),
                                                MarkUp = listdata.markup.ToString(),
                                                CountryLevelIDs = strSplit_dIDandCode[1],
                                                PassportIsVisible = passportValid,
                                                MonthValidity = MonthValidity,
                                                PassTypeCode = listdata.PassTypeCode,
                                                NationalityIsVisible = _BMaster.GetIsNationaltyByProdId(PrdtId),
                                                BookingFee = BookingFee
                                            });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (list.Count > 0)
            {
                if (Session["RailPassData"] != null)
                {
                    var lstRP = Session["RailPassData"] as List<getRailPassData>;
                    if (lstRP.Count > 0)
                    {
                        if (lstRP.Any(x => x.IsBritrail) || list.Any(t => t.IsBritrail))
                        {
                            if (!list.Any(t => t.IsEurail))
                                lstRP.AddRange(list);
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "innertabActive", "innertabActive();", true);
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", "AlertMessage('Sorry! You can’t purchase another pass with Britrail pass.')", true);
                                return;
                            }
                        }
                        else
                        {
                            if (!list.Any(t => t.IsBritrail))
                                lstRP.AddRange(list);
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "innertabActive", "innertabActive();", true);
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", "AlertMessage('Sorry! You can’t purchase Britrail pass with this pass.')", true);
                                return;
                            }
                        }
                        Session["RailPassData"] = lstRP;
                        list = lstRP;
                    }
                    Session.Add("RailPassData", list);
                }
                else
                    Session.Add("RailPassData", list);

                Session["GaTaggingProductlist"] = list.GroupBy(t => new { t.PrdtId, t.PassTypeCode, t.PrdtName, _master.GetCategoriesNameById(Guid.Parse(t.CategoryID)).FirstOrDefault().Name, t.Price }).Select(t => new ProductPass
                {
                    ProductID = t.Key.PrdtId,
                    ProductCode = t.Key.PassTypeCode.ToString(),
                    Name = t.Key.PrdtName,
                    CategoryName = t.Key.Name,
                    Price = t.Key.Price,
                    Count = t.Count()
                }).ToList();

                Levellist.Clear();
                Session.Remove("CountryCodeList");
                Response.Redirect("~/bookingcart");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showmsg", "showmsg();collChangeLinkPosition();", true);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCurrencyCode()
    {
        if (HttpContext.Current.Request.Cookies["CookieCompliance_IR"] != null)
        {
            var cookie = HttpContext.Current.Request.Cookies["CookieCompliance_IR"];
            curID = cookie.Values["_curId"];
            siteId = Guid.Parse(cookie.Values["_siteId"]);
            currency = oManageClass.GetCurrency(Guid.Parse(curID));
            hdnCurrencySign.Value = currency;
        }
    }

    public void GetScriptingTag()
    {
        try
        {
            IRScriptingTagJs = "";
            var data = _ManageScriptingTag.GetListScriptingTagBySiteId(siteId);
            if (data != null && data.Where(x => x.IsActive == true).ToList().Count > 0)
            {
                if (siteId == Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D"))
                {
                    var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Pass Details");
                    if (firstDefault != null)
                    {
                        IRScriptingTagJs = firstDefault.Script;
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##SiteUrl##", siteURL).Replace("##PageUrl1##", siteURL + "railpasses").Replace("##PageName1##", "Rail Passes");
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##PageUrl2##", siteURL + Request.Url.AbsoluteUri.Split('/').Last()).Replace("##PageName2##", ProductNames);
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##CurrentPageName##", "Pass Details");
                    }
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }

    public class getCuntryCode
    {
        public string CuntryCode { get; set; }
        public string Row { get; set; }
        public string ID { get; set; }
        public string selectValue { get; set; }
    }
    public class headername
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}
