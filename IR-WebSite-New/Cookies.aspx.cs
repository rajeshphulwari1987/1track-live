﻿using System;
using System.Web.UI;
using Business;
using System.Net;
using System.Configuration;

public partial class Cookies : Page
{
    readonly ManageCookie _oMaster = new ManageCookie();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private Guid _siteId;
    public string siteURL;
    public string Description;
    public string script;

    protected void Page_Init(object sender, EventArgs e)
    { 
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            BindCookies();

            #region Seobreadcrumbs
            var pageID = (Guid)Page.RouteData.Values["PageId"];
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageID, _siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion
        }
    }

    public void BindCookies()
    {
        var result = _oMaster.GetCookiesBySiteId(_siteId);
        if (result != null)
        {
            ltrTitle.Text = result.Name;
            ltrDesc.InnerHtml = WebUtility.HtmlDecode(result.Description);
        }
    }
}