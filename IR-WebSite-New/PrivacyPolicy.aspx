﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrivacyPolicy.aspx.cs" MasterPageFile="~/Site.master"
    Inherits="PrivacyPolicy" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script %>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server"
                    src="http://localhost/IR-Admin/CMSImages/banner04.jpg" />
            </div>
        </div>
    </div>
    <br />
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
        <h2>
            <%=Server.HtmlDecode(PageTitle)%></h2>
        <p>
            <%=Server.HtmlDecode(PageDescription)%></p>
        <div class="starail-Section starail-Section--nopadding">
            <ul class="starail-Accordion js-accordon">
                <asp:Repeater ID="rptPriv" runat="server">
                    <ItemTemplate>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    1. Your Consent</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("YourConsent"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    2. Information Collected
                                </h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("InfoCollected"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    3. Use of Your Information</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("UseInfo"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    4. Disclosure of Your Information</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("DisclosureInfo"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    5. Person under 18 years</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Person18"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    6. Cookies</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Cookies"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    7. Security of your information</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("SecurityInfo"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    8. Your Personal Data</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("PersonalData"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    9. Alterations to privacy statement</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("AlterationsPrivacy"))%></p>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Content>
