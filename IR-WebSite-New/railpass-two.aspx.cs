﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Business;
using System.Configuration;

public partial class railpass_two : System.Web.UI.Page
{
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    readonly private ManageProduct _master = new ManageProduct();
    public static bool ErrorIn;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly ManageAffiliateUser _AffManage = new ManageAffiliateUser();
    private readonly tblPassQuery _otblPassQuery = new tblPassQuery();
    private readonly ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
    public Guid siteId, ProductId;
    public string siteURL, ProductImage, ProductImageSecond, ProductImageAltTag, ProductImageSecondAltTag, BannerImage, Url, Description, ProductName, Price, specialOfferText, Announcement;
    public string script = "<script></script>";
    public static string currency;
    string currId;
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    bool IsOffer = false;
    bool BestSelller = false;
    public string NextBooking;
    public string NextBookingScriptIR;
    private ManageScriptingTag _ManageScriptingTag = new ManageScriptingTag();
    public string IRScriptingTagJs = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["af"] != null)
        {
            bool Result = _AffManage.GetAffURLIsActiveByAffCode(Request.QueryString["af"], Request.Url.ToString());
            if (Result)
                Session["AffCode"] = Request.QueryString["af"];
            else
                Response.Redirect(siteURL + "home");
        }
        try
        {
            GetCurrency();
            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["ProductId"] != null)
                {
                    Guid id = Guid.Parse(Page.RouteData.Values["ProductId"].ToString());
                    ViewState["ProductId"] = id;
                    ResetGetDetailsAnalyticTags(id);

                    #region Seobreadcrumbs
                    var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(id, siteId);
                    if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                        litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
                    #endregion

                    var isActvieP = _master.IsActiveProductWithEnableForPurchase(id, siteId);
                    if (!isActvieP)
                        Response.Redirect(siteURL);

                    var data = _oManageInterRailNew.GetSpecialBestSeller(id);
                    if (data != null)
                    {
                        IsOffer = data.IsSpecialOffer;
                        BestSelller = data.BestSeller;
                        if (IsOffer)
                            div_SpecialOffer.InnerHtml = "Special Offer";
                        else if (BestSelller)
                        {
                            div_SpecialOffer.InnerHtml = "Best Seller";
                            div_SpecialOffer.Attributes.Add("style", "background-color:#fc9933 !important;");
                        }
                        else
                            div_SpecialOffer.Attributes.Add("style", "display:none;");
                    }
                }
                QubitOperationLoad();
                GetProductByProductId();
                CrossSaleAdsenseList();
                GetNextBookingButton();
                GetScriptingTag();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetNextBookingButton()
    {
        try
        {
            Guid IR_ComSiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
            if (siteId != IR_ComSiteId)
            {
                NextBooking = "<a href='" + siteURL + Url + "' class='starail-Button starail-Button--full' data-ga-category='" + Request.Url.ToString().Split('?')[0].Split('#')[0] + "' data-ga-action='click' data-ga-label='" + Request.Url.Host + "'>NEXT: SELECT YOUR PASS</a> ";
            }
            else
            {
                NextBookingScriptIR = "<script type='text/javascript'>\n function addgacode()\n{\nga('send', 'event', 'Rail Passes', 'Select your Pass', '" + Request.Url.ToString().Split('?')[0].Split('#')[0] + "');\n}\n</script>";
                NextBooking = "<a onclick='addgacode();' href='" + siteURL + Url + "' class='starail-Button starail-Button--full'>NEXT: SELECT YOUR PASS</a>";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ResetGetDetailsAnalyticTags(Guid ProductId)
    {
        if (Session["GaTaggingProductlist"] != null && (Session["GaTaggingProductlist"] as List<ProductPass>).Any(t => t.ID != Guid.Empty))
        {
            Session["GaTaggingProductlist"] = (Session["GaTaggingProductlist"] as List<ProductPass>).Where(t => t.ID == ProductId).GroupBy(t => new { t.ProductCode, t.Name, t.CategoryName }).
              Select(t => new ProductPass
              {
                  ProductCode = t.Key.ProductCode.ToString(),
                  Name = t.Key.Name,
                  CategoryName = t.Key.Name,
              }).ToList();
        }
        ScriptManager.RegisterStartupScript(Page, GetType(), "redirectpage", "clickbookonline()", false);
    }

    public void QubitOperationLoad()
    {
        var lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    void ShowP2PWidget(Guid siteID)
    {
        var enableP2P = _oWebsitePage.EnableP2P(siteID);
        if (!enableP2P)
            ScriptManager.RegisterStartupScript(Page, GetType(), "tmp", "<script type='text/javascript'>$('a[href*=rail-tickets]').hide();$('.divEnableP2P').hide();</script>", false);
    }

    public void GetCurrency()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (result != null)
        {
            currId = result.DefaultCurrencyID.ToString();
            if (currId != null)
                currency = oManageClass.GetCurrency(Guid.Parse(currId));
            else
                currency = "£";
        }
        else
            currency = "£";
    }

    bool isBritRailPromoPass(Guid productid)
    {
        try
        {
            if (productid != Guid.Empty)
            {
                bool isPromo = (_db.tblProducts.Any(x => x.ID == productid && x.IsPromoPass && x.tblProductCategoriesLookUps.Any(y => y.ProductID == productid && y.tblCategory.IsBritRailPass)));
                DateTime lastdateofPromo = Request.Url.ToString().ToLower().Contains("localhost") ? Convert.ToDateTime("09/18/2015").AddDays(1) : Convert.ToDateTime("18/09/2015").AddDays(1);
                if (isPromo && lastdateofPromo > DateTime.Now)
                    return true;
            }
            return false;
        }
        catch
        {
            return false;
        }

    }

    public void GetProductByProductId()
    {
        try
        {
            if (ViewState["ProductId"] != null)
            {
                ProductId = Guid.Parse(ViewState["ProductId"].ToString());
                divExtraDayDesc.Visible = isBritRailPromoPass(ProductId);
                var list = _oManageInterRailNew.GetProductByProductId(ProductId, siteId);
                if (list != null)
                {
                    divAnnouncement.Visible = !string.IsNullOrEmpty(list.AnnouncementTitle);
                    lblAnnouncementTitle.Text = list.AnnouncementTitle;
                    Announcement = Server.HtmlDecode(list.Announcement);
                    Description = Server.HtmlDecode(list.Description);
                    Url = list.Url;
                    ProductName = list.Name;
                    specialOfferText = list.SpecialOfferText;
                    if (!string.IsNullOrEmpty(specialOfferText))
                        divspecialOfferText.InnerHtml = (specialOfferText.Replace(Environment.NewLine, "<br />"));
                    Price = String.Format("{0:0}", Math.Ceiling(FrontEndManagePass.GetPriceAfterConversion(Convert.ToDecimal(list.Price), siteId, list.CurrencyID, Guid.Parse(currId))));
                    ProductImageAltTag = list.ProductAltTag1;
                    ProductImageSecondAltTag = list.ProductAltTag2;
                    if (!string.IsNullOrEmpty(list.ProductImage))
                        ProductImage = string.IsNullOrEmpty(Convert.ToString(list.ProductImage.Replace("ProductImages", "STAProductImages"))) ? "images/city.jpg" : Convert.ToString(list.ProductImage.Replace("ProductImages", "STAProductImages"));
                    else
                        ProductImage = "images/city.jpg";
                    if (!string.IsNullOrEmpty(list.ProductImageSecond))
                        ProductImageSecond = string.IsNullOrEmpty(Convert.ToString(list.ProductImageSecond.Replace("ProductImages", "STAProductImages"))) ? "images/city.jpg" : Convert.ToString(list.ProductImageSecond.Replace("ProductImages", "STAProductImages"));
                    else
                        ProductImageSecond = "images/city.jpg";
                    BannerImage = string.IsNullOrEmpty(Convert.ToString(list.BannerImage)) ? "images/banner-pass.jpg" : Convert.ToString(list.BannerImage);
                    var result = _master.GetProductTermsById(list.Id);
                    if (result != null)
                    {
                        if (!string.IsNullOrEmpty(Server.HtmlDecode(result.TermCondition)))
                        {
                            //termandcondition.InnerHtml = Server.HtmlDecode(result.TermCondition);  
                        }
                        else
                            div_GreatPassDescription.Attributes.Add("style", "display:none");

                        if (!string.IsNullOrEmpty(Server.HtmlDecode(result.GreatPassDescription)))
                            div_GreatPassDescription.InnerHtml = "<h2>This pass is great because...</h2>" + Server.HtmlDecode(result.GreatPassDescription);
                        else
                            div_GreatPassDescription.Attributes.Add("style", "display:none");
                    }

                    // Only for Ir Site
                    if (siteId == Guid.Parse("5858f611-6726-4227-a417-52ccdea36b3d"))
                    {
                        h_Title.InnerHtml = !string.IsNullOrEmpty(list.IrTitle) ? list.IrTitle : "Experience the adventure of train travel";
                        Description = !string.IsNullOrEmpty(list.IrDescription) ? Server.HtmlDecode(list.IrDescription) : Server.HtmlDecode(list.Description);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void CrossSaleAdsenseList()
    {
        try
        {
            rptCrossSale.DataSource = _oManageInterRailNew.GetCrossSaleAdsenseInfoList(siteId, ProductId);
            rptCrossSale.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    public void GetScriptingTag()
    {
        try
        {
            IRScriptingTagJs = "";
            var data = _ManageScriptingTag.GetListScriptingTagBySiteId(siteId);
            if (data != null && data.Where(x => x.IsActive == true).ToList().Count > 0)
            {
                if (siteId == Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D"))
                {
                    var firstDefault = data.FirstOrDefault(x => x.IsActive == true && x.PageType == "Pass Inner");
                    if (firstDefault != null)
                    {
                        IRScriptingTagJs = firstDefault.Script;
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##SiteUrl##", siteURL).Replace("##PageUrl1##", siteURL + "railpasses").Replace("##PageName1##", "Rail Passes");
                        IRScriptingTagJs = IRScriptingTagJs.Replace("##CurrentPageName##", ProductName);
                    }
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }
}