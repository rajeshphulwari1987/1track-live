﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

public partial class MagNavTest : System.Web.UI.Page
{
    public string headerCss, header, footer, jquery;
    protected void Page_Load(object sender, EventArgs e)
    {
        string mobileHeader = "us";
        string headerCssUrl = "http://www.statravel.com/meganav/get-top-css-js?division=" + mobileHeader;
        string headers = "http://www.statravel.com/meganav/get-header?division=" + mobileHeader;// +"&textSearch=true&liveChat=true&isSecured=true";
        string footers = "http://www.statravel.com/meganav/get-footer?division=" + mobileHeader + "&affiliationIcons=true";
        string jquery = "http://www.statravel.com/meganav/get-bottom-js?division=" + mobileHeader + "&jQueryDefault=false";

        headerCss = GetHttpWebRequest(headerCssUrl);
        header = GetHttpWebRequest(headers);

        footer = GetHttpWebRequest(footers);
        jquery = GetHttpWebRequest(jquery);
    }
    public string GetHttpWebRequest(string url)
    {

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = null;

            if (response.CharacterSet == null)
            {
                readStream = new StreamReader(receiveStream);
            }
            else
            {
                readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
            }

            string data = readStream.ReadToEnd();

            response.Close();
            readStream.Close();
            return data;
        }
        return "";
    }
}