﻿using System;
using Business;
using System.Web.UI;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;

public partial class Public : System.Web.UI.MasterPage
{
    readonly ManageUser _ManageUser = new ManageUser();
    readonly db_1TrackEntities db = new db_1TrackEntities();
    readonly Masters _master = new Masters();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteAdminUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    public string siteURL, SiteLogoUrl = string.Empty;
    private Guid _siteId;
    public string script = "<script></script>";
    public string FaviconIcon = string.Empty;
    public string IrRailCss = string.Empty;
    public string AgentCss = string.Empty;
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            LoadSite();
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);
        }
        catch (Exception ex)
        {
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var isActiveSite = _oWebsitePage.GetSiteActiveInactiveBySiteId(_siteId);
            if (!isActiveSite)
                Response.Redirect("UnderConstruction.htm");

            var data = _db.tblSites.FirstOrDefault(t => t.ID == _siteId && t.IsSTA == true);
            if (data != null)
                hdnSTA.Value = "1";
            else
                hdnSTA.Value = "0";

            if (Request.QueryString["UserId"] != null)
            {
                if (AgentuserInfo.UserID != Guid.Empty)
                {
                    removeAgentSession();
                    Response.Redirect(siteURL + "home");
                }
            }
        }
        LoadSiteLogo();
        QubitOperationLoad();
        IrRailFunctionality();
    }

    void LoadSite()
    {
        if (Request.Url.Host == "localhost")
        {
            string url = Request.Url.ToString().ToLower();
            if (url.Contains("1tracknew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1tracknew/");
            else if (url.Contains("1trackagentnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1trackagentnew/");
            else if (url.Contains("1trackagentdemo"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1trackagentdemo/");
            else if (url.Contains("omegatravel"))
                _siteId = _oWebsitePage.GetSiteIdByURL(" http://localhost/omegatravel/");
            else if (url.Contains("demousagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoUsAgent/");
            else if (url.Contains("demous"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoUs/");
            else if (url.Contains("demoausagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoAusAgent/");
            else if (url.Contains("demoaus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoAus/");
            else if (url.Contains("interrailnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrailnew/");
            else if (url.Contains("interrailagentnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrailagentnew/");
            else if (url.Contains("interrail"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrail/");
            else if (url.Contains("amex"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/amex/");
            else if (url.Contains("australiap2p"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/australiap2p/");
            else if (url.Contains("travelcutsagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/travelcutsagent/");
            else if (url.Contains("travelcuts"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/travelcuts/");
            else if (url.Contains("irrailagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/irrailagent/");
            else if (url.Contains("irrail"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/irrail/");
            else if (url.Contains("singapore"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/singapore/");
            else if (url.Contains("thailandagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/thailandagent/");
            else if (url.Contains("thailand"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/thailand/");
            else if (url.Contains("meritagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/meritagent/");
            else if (url.Contains("au-agent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/au-agent/");
            else if (url.Contains("stath.internationalrail.net"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/stath.internationalrail.net/");
        }
        else if (Request.Url.Host.Contains("projectstatus.in"))
        {
            if (Request.Url.Host.Contains("travelcuts.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://travelcuts.projectstatus.in/");
            else if (Request.Url.Host.Contains("travelcutsb2b.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://travelcutsb2b.projectstatus.in/");
            else if (Request.Url.Host.Contains("merit.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://merit.projectstatus.in/");
            else if (Request.Url.Host.Contains("bookmyrst.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://bookmyrst.projectstatus.in/");
            else if (Request.Url.Host.Contains("railpass.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://railpass.projectstatus.in/");
            else if (Request.Url.Host.Contains("internationalrail.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://internationalrail.projectstatus.in/");
            else if (Request.Url.Host.Contains("internationalrailuk.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://internationalrailuk.projectstatus.in/");
        }

        else if (Request.Url.Host.Contains("1tracktest.com"))
        {

            if (Request.Url.Host.Contains("staau.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://staau.1tracktest.com/");
            else if (Request.Url.Host.Contains("staaua.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://staaua.1tracktest.com/");
            else if (Request.Url.Host.Contains("stanz.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stanz.1tracktest.com/");
            else if (Request.Url.Host.Contains("stanza.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stanza.1tracktest.com/");
            else if (Request.Url.Host.Contains("stauk.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stauk.1tracktest.com/");
            else if (Request.Url.Host.Contains("stauka.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stauka.1tracktest.com/");
            else if (Request.Url.Host.Contains("staus.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://staus.1tracktest.com/");
            else if (Request.Url.Host.Contains("stausa.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stausa.1tracktest.com/");
            else if (Request.Url.Host.Contains("idivd.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://idivd.1tracktest.com/");
            else if (Request.Url.Host.Contains("idive.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://idive.1tracktest.com/");
            else if (Request.Url.Host.Contains("idivw.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://idivw.1tracktest.com/");            
        }

        else
        {
            siteURL = "http://" + Request.Url.Host + "/";
            _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);

            if (_siteId == new Guid())
            {
                siteURL = "https://" + Request.Url.Host + "/";
                _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);
            }
        }
        Session["siteId"] = _siteId;
    }

    public void LoadSiteLogo()
    {
        var data = _db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.FaviconPath))
                FaviconIcon = "<link href='" + SiteAdminUrl + data.FaviconPath + "' rel='shortcut icon' type='image/x-icon' />";

            if (!string.IsNullOrEmpty(data.LogoPath))
                SiteLogoUrl = SiteAdminUrl + data.LogoPath;
            else
                SiteLogoUrl = siteURL + "assets/img/branding/logo.png";
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void removeAgentSession()
    {
        Session.Remove("RailPassData");
        Session.Remove("DetailRailPass");
        Session.Remove("AgentUsername");
        Session.Remove("AgentRoleId");
        Session.Remove("AgentUserID");
        Session.Remove("AgentSiteID");
        Session.Remove("IsAffliate");
        Session.Remove("IsPaymentGatway");
        Session.Remove("PopupDisplayed");
        Session.Remove("OrderID");
        Session.Remove("P2POrderID");
    }

    public void IrRailFunctionality()
    {
        if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && x.LayoutType == 8))
            AgentCss = "<link href='../assets/css/omegatravel.css' rel='stylesheet' type='text/css' />";
        else
            AgentCss = "<link href='../assets/css/main-agent.css' rel='stylesheet' type='text/css' />";

        if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && (x.LayoutType == 2 || x.LayoutType == 6)))
        {
            IrRailCss = "<link href='../assets/css/internationalrail.css' rel='stylesheet' type='text/css' />";
        }
    }
}
