﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;

#endregion

public partial class _Default : Page
{
    private readonly Masters _master = new Masters();
    private readonly ManageJourney _objJourney = new ManageJourney();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    public string script = "<script></script>";
    Guid pageID, siteId;
    public static string currency;
    public string currId;
    public string frontSiteUrl, ContinentName;
    private readonly ManageInterRailNew _ManageIRNew = new ManageInterRailNew();
    private readonly tblHelp _otblHelp = new tblHelp();
    string value = string.Empty;
    public string siteURLS;
    public string datalist = "";
    public static bool SearchCntry = false;
    public static string ispostbacksearch = "no";
    public string omegaCss = string.Empty;
    public bool ISFOCAD75 = false;
    public int minDate = 0;
    public bool isBookMyRst = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.RawUrl.EndsWith("/default.aspx", StringComparison.OrdinalIgnoreCase))
        {
            var url = Request.Url.AbsoluteUri.ToLower().Replace("/default.aspx", string.Empty);
            Response.Redirect(url);
        }
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURLS = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));

            if (_db.tblSites.Any(x => x.ID == siteId && (x.LayoutType == 2 || x.LayoutType == 9 || x.LayoutType == 6)))
            {
                IrBanner.Visible = true;
                StaBanner.Visible = false;
            }
            if (_db.tblSites.Any(x => x.ID == siteId && x.LayoutType == 8))
            {
                omegaCss = "<style type='text/css'>.switchRadioGroup tr td input[type='radio']:checked + label{background-color: #29166f!important;}</style>";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            GetEnableCookieFeature();
            if (Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst.co.uk"))//|| Request.Url.AbsoluteUri.ToLower().Contains("1tracknew")
            {
                StaJRF.Visible = TabEurostartickets.Visible = true;
                TabRailTickets.Visible = Railtickets.Visible = false;
                anchor_JRF.InnerText = "Journey Request Form";
                isBookMyRst = true;
                ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "starail_Switcher_tab_width", "starail_Switcher_tab_width_fifty();threeTabVisible();", true);
            }
            else if (Request.Url.AbsoluteUri.ToLower().Contains("bookmyrst") || Request.Url.AbsoluteUri.ToLower().Contains("1tracknew"))
            {
                Railtickets.InnerText = "OTHER RAIL TICKETS";
                eurostar.Visible = TabEurostartickets.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "starail_Switcher_tab_width", "starail_Switcher_tab_width();threeTabVisible();", true);
            }
            else if (_db.tblSites.Any(x => x.ID == siteId && x.LayoutType == 2))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "starail_Switcher_tab_width", "twoTabVisible();", true);
            }
            else
            {
                eurostar.Visible = false;
                ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "threeTabVisible", "threeTabVisible();", true);
            }
            minDate = _oWebsitePage.GetBookingDayLimitBySiteId(siteId);

            GetCurrency();
            if (!Page.IsPostBack)
            {
                Session["selectedcountry"] = null;
                if (Request.Params["category"] != null)
                    ISFOCAD75 = true;
                GetCountrylist();
                if (Page.RouteData.Values["PageId"] != null)
                    pageID = (Guid)Page.RouteData.Values["PageId"];
                else
                {
                    var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
                    if (pid != null)
                        pageID = pid.ID;
                }
                Page.Header.DataBind();
                PageContent(pageID, siteId);
                QubitOperationLoad();
                frontSiteUrl = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
                ContinentName = "";
                ispostbacksearch = "no";
                if (Session["oldsearch"] != null)
                {
                    ispostbacksearch = "yes";
                    txtSearch.Text = Session["oldsearch"].ToString();
                    Session.Remove("oldsearch");
                }
                if (Session["CountryPass"] != null)
                {
                    ispostbacksearch = "yes";
                    txtSearch.Text = Session["CountryPass"].ToString();
                    Session.Remove("CountryPass");
                }
                BindAllCountryList(txtSearch.Text);
                FilterShowHide(txtSearch.Text.Trim());
            }

            #region Baner Uc Call
            if (IrBanner.Visible == true)
            {
                if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                {
                    TextBox txtIrsearch = IrBanner.FindControl("txtSearch") as TextBox;
                    txtIrsearch.Text = txtSearch.Text;
                    ispostbacksearch = "yes";
                    ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "searchnew", "initcallSearch('" + ispostbacksearch + "');", true);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                {
                    TextBox txtStasearch = StaBanner.FindControl("txtSearch") as TextBox;
                    txtStasearch.Text = txtSearch.Text;
                    ispostbacksearch = "yes";
                    ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "searchnew", "initcallSearch('" + ispostbacksearch + "');", true);
                }
            }
            #endregion
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCountrylist()
    {
        if (datalist == "")
            datalist = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(new StationList().GetCountryList());
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (siteId != null && !string.IsNullOrEmpty(txtSearch.Text))
            {
                GetAllProduct();
                ContinentName = txtSearch.Text.Trim();
            }
            else
                ContinentName = "";
            Session["oldsearch"] = ContinentName;
            if (Request.Params["category"] != null)
                Response.Redirect("~/?category=FOC-AD75");
            else
                Response.Redirect("~/");
        }
        catch (Exception ex)
        {
        }
    }

    private void FilterShowHide(string search)
    {
        try
        {
            if (!string.IsNullOrEmpty(search))
            {
                var data = _db.tblContinents.FirstOrDefault(x => x.Name == search);
                if (data != null)
                    div_Filer.Visible = true;
                else
                    div_Filer.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void BindAllCountryList(string ContinentName)
    {
        try
        {
            List<CountryList> CountryList = new List<CountryList>();
            CountryList.Add(new CountryList { Name = "All Country" });
            CountryList.AddRange(_ManageIRNew.GetHomePageCountryListFilterByContinent(ContinentName, siteId));
            if (CountryList.Count < 2)
                CountryList.AddRange(_ManageIRNew.GetHomePageCountryList(siteId));
            chkCountryList.DataSource = CountryList;
            chkCountryList.DataTextField = "Name";
            chkCountryList.DataValueField = "Name";
            chkCountryList.DataBind();
            CheckBoxSTAStyle();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void chkCountryList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            #region GTM Tracking
            string countryname = "";
            var chklist = chkCountryList.Items.Cast<ListItem>().Where(li => li.Selected).ToList();
            if (Session["selectedcountry"] == null)
            {
                Session["selectedcountry"] = chklist;
                countryname = chklist.FirstOrDefault().Text;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "filterDetail", "filterDetail('" + countryname + "');", true);
            }
            else
            {
                var ss = Session["selectedcountry"] as List<ListItem>;
                if (ss.Count < chklist.Count)
                {
                    countryname = chklist.FirstOrDefault(x => !ss.Select(o => o.Text).Contains(x.Text)).Text;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "filterDetail", "filterDetail('" + countryname + "');", true);
                }
                Session["selectedcountry"] = chklist;
            }
            #endregion
            Session.Remove("oldsearch");
            Session.Remove("CountryPass");
            txtSearch.Text = string.Empty;
            CheckBoxSTAStyle();
            ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "filter", "FilterCall();activeCrousal();initcallSearch('yes');", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void dopostback_Click(object sender, EventArgs e)
    {
        CheckBoxSTAStyle();
        ScriptManager.RegisterStartupScript(this.Page, this.pageID.GetType(), "filtxxer", " calltest();activeCrousal(); FilterCall();initcallSearch('yes');", true);
    }

    protected void CheckBoxSTAStyle()
    {
        try
        {
            nosearchresultfound.Visible = false;
            bool NoCountryFound = true;
            List<ProductPass> list = new List<ProductPass>();
            string CountryList = string.Empty;
            for (int i = 0; i < chkCountryList.Items.Count; i++)
            {
                if (hdnCountryChkbox.Value == "All Country" && i > 0)
                    chkCountryList.Items[i].Selected = false;
                else if (i == 0 && hdnCountryChkbox.Value != "All Country")
                    chkCountryList.Items[0].Selected = false;
                chkCountryList.Items[i].Attributes.Add("onclick", "chkcountry(this)");
                chkCountryList.Items[i].Attributes.Add("class", "starail-Form-fancyCheckbox loycheckbox");
                if (chkCountryList.Items[i].Selected)
                    CountryList += string.IsNullOrEmpty(CountryList) ? chkCountryList.Items[i].Text : "," + chkCountryList.Items[i].Text;
            }
            if (!string.IsNullOrEmpty(hdnHashtag.Value))
            {
                list = _ManageIRNew.GetAllProductBySiteIdHashtagNew(siteId, hdnHashtag.Value);
                rptProduct.DataSource = list;
                rptProduct.DataBind();
                NoCountryFound = true;
            }
            else if (!string.IsNullOrEmpty(CountryList))
            {
                list = _ManageIRNew.GetAllORSelectedCountryProductPass(siteId, CountryList);
                rptProduct.DataSource = list;
                rptProduct.DataBind();
                if (list.Count < 1)
                    nosearchresultfound.Visible = true;
                NoCountryFound = false;
            }
            if (list.Count < 1 && NoCountryFound)
                GetAllProduct();
            if (hdncountrylistshow.Value == "ok")
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowCountry", "ShowCountry();", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnClearFilter_Click(object sender, EventArgs e)
    {
        BindAllCountryList(txtSearch.Text);
        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "countrynotfound", "DatePickerCall2();initcallSearch('no');", true);
    }

    public void GetAllProduct()
    {
        try
        {
            var list = new List<ProductPass>();
            Session["GaTaggingProductlist"] = list = _ManageIRNew.GetAllProductPassNew(siteId, Session["CountryPass"] == null ? txtSearch.Text : Session["CountryPass"].ToString()).ToList();
            #region Google Code
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                Session["GTPassSearch"] = new Business.ManageAnalyticTag.Pass() { searchTxt = txtSearch.Text, searchResNo = list.Count.ToString() };
            }
            #endregion
            if (list.Count() > 0)
            {
                if (ISFOCAD75)
                    list = list.Where(t => t.IsFocAD75 == ISFOCAD75).ToList();
                else if (string.IsNullOrEmpty(txtSearch.Text))
                    list = _db.tblSites.Any(x => x.LayoutType == 2 && x.ID == siteId) ? list.Where(t => t.IsTopPass).ToList() : list.Where(t => t.IsTopPass).Take(8).ToList();
                rptProduct.DataSource = list;
                rptProduct.DataBind();
            }
            else if (Session["CountryPass"] != null || !string.IsNullOrEmpty(txtSearch.Text))
            {
                Session["country"] = "country";
                Session["CountryPass"] = null;
                Response.Redirect("Countries");
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void GetCurrency()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (result != null)
        {
            currId = result.DefaultCurrencyID.ToString();
            if (currId != null)
                currency = oManageClass.GetCurrency(Guid.Parse(currId));
            else
                currency = "£";
        }
        else
            currency = "£";
    }

    protected void rptCat_OnItemCreated(object sender, RepeaterItemEventArgs args)
    {
        try
        {
            var lnkEdit = (LinkButton)args.Item.FindControl("lnkCat");
            if (lnkEdit != null)
                ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(lnkEdit);

        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID && (x.SiteType == 2 || x.SiteType == 9));
            if (result != null)
            {
                div_HomePageHeading1.InnerHtml = !string.IsNullOrEmpty(result.HomePageHeading1) ? result.HomePageHeading1 : "";
                div_HomePageHeading2.InnerHtml = !string.IsNullOrEmpty(result.HomePageHeading2) ? result.HomePageHeading2 : "";
                div_HomePageDescription1.InnerHtml = !string.IsNullOrEmpty(result.HomePageDescription1) ? result.HomePageDescription1 : "";
                div_HomePageDescription2.InnerHtml = !string.IsNullOrEmpty(result.HomePageDescription2) ? result.HomePageDescription2 : "";
            }
            else
            {
                div_HomePageHeading1.Visible = div_HomePageHeading2.Visible = div_HomePageDescription1.Visible = div_HomePageDescription2.Visible = hr_home.Visible = anchor_showMore.Visible = false;
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void GetEnableCookieFeature()
    {
        var result = _db.tblSites.FirstOrDefault(x => x.ID == siteId);
        if (result != null && !result.IsEnableCookie)
        {
            cookiemessage.Visible = false;
        }
    }

    #region  We don't have that journey online
    /*
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            bool Result = _oManageInterRailNew.AddHelp(new tblHelp
            {
                Id = Guid.NewGuid(),
                Name = txtFName.Text.Trim() + " " + txtLName.Text.Trim(),
                Email = txtEmail.Text.Trim(),
                Phone = txtPhone.Text.Trim(),
                SiteId = siteId,
                Contact = rdnEmail.Checked ? rdnEmail.Text : rdnPhone.Text,
                CreatedOn = DateTime.Now
            });
            ClearData();
            ScriptManager.RegisterStartupScript(Page, GetType(), "return", "TabRailTicket()", true);
            ScriptManager.RegisterStartupScript(Page, GetType(), "LoadRadio", "LoadRadioButton()", true);
            ScriptManager.RegisterStartupScript(Page, GetType(), "callvalerror", "callvalerror()", true);
            ShowMessage(1, "Query has been sent successsfully");
        }
        catch (Exception ex) { throw ex; }
    }

    public void ClearData()
    {
        txtEmail.Text = string.Empty;
        txtFName.Text = string.Empty;
        txtLName.Text = string.Empty;
        txtPhone.Text = string.Empty;
        rdnEmail.Checked = true;
        rdnPhone.Checked = false;
    }*/
    #endregion

    #region  One Hub Implementation
    /// <summary>
    /// Create the station list XML.
    /// </summary>
    /// <returns></returns>
    public string[] CreateStationXml()
    {
        var clientCode = new OneHubRailOneHubClient();
        var stationList = clientCode.GetStationInfo().ToList();
        return BindXml(ReadObjectAsString(stationList));
    }
    public string ReadObjectAsString(object o)
    {
        var xmlS = new XmlSerializer(o.GetType());
        var sw = new StringWriter();
        var tw = new XmlTextWriter(sw);
        xmlS.Serialize(tw, o);
        return sw.ToString();
    }
    /// <summary>
    /// Binds the station XML list.
    /// </summary>
    /// <param name="strXml">The STR XML.</param>
    /// <returns></returns>
    public string[] BindXml(string strXml)
    {
        try
        {
            if (strXml != "")
            {
                var doc = new XmlDocument();
                doc.LoadXml(strXml);
                string filePath = Server.MapPath(@"StationList.xml");
                doc.Save(filePath);
                return null;
            }
            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

}

public class Categories
{
    public Guid ID { get; set; }
    public String Name { get; set; }
    public String Url { get; set; }
}