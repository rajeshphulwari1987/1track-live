﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VisitEngland.aspx.cs" Inherits="VisitEngland"
    MasterPageFile="Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700,900"
        rel="stylesheet">
    <link href='assets/css/bootstrapNew.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/styleNew.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="Scripts/js/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/js/bootstrap.js" type="text/javascript"></script>
    <style type="text/css">
        .edit-btn
        {
            display: none;
        }
        #sta-nav-wrap ul.sta-navigation .sta-mainNavSubMenu .sta-nav-column
        {
            width: 514px !important;
        }
        .show-more
        {
            display: none;
            text-align: center;
        }
        .product-inactive
        {
            display: none;
        }
        #sta-header #sta-tagline-call-info
        {
            height: 141px !important;
        }
        .tour-packages div.snip1174 a span
        {
            display: inline-block;
        }
        .book-pass
        {
            font-size: 36px;
            color: #ffffff;
            text-transform: uppercase;
        }
        .book-price
        {
            float: left;
            font-size: 23px;
            color: #f6be00;
            text-transform: uppercase;
            background-color: #ffffff;
            line-height: 42px;
        }
        .book-left
        {
            float: left;
            width: 0;
            height: 0;
            border-top: 21px solid transparent;
            border-right: 30px solid #ffffff;
            border-bottom: 21px solid transparent;
        }
        .book-right
        {
            width: 0;
            height: 0;
            border-top: 21px solid transparent;
            border-left: 39px solid #ffffff;
            border-bottom: 21px solid transparent;
        }
        .yellow-box
        {
            margin-top: 15px;
            height: 100px;
        }
        .product a
        {
            margin-top: -40px;
        }
        @media screen and (max-width: 600px)
        {
            .show-more
            {
                display: block;
                text-align: center;
            }
            #MainContent_sbtn6
            {
                text-align: center;
            }
            .yellow-box
            {
                margin-top: 40px;
            }
            .tour-packages div.snip1174:hover a, .tour-packages div.snip1174.hover a
            {
                top: 10px;
            }
        }
    </style>
    <script type="text/javascript">
        $(window).resize(function () {
            showHideProduct();
        });
        $(document).ready(function () {
            showHideProduct();
        });

        function showHideProduct() {
            if ($(window).width() <= 480) {
                var size_all = $("#pnlpass div.product").size();
                var x = 3;
                if (size_all > x) {
                    $('#pnlpass div.product:gt(2)').removeClass('product-inactive').addClass('product-inactive');
                }
                $('.show-more a').click(function () {
                    if (size_all > x) {
                        var size_inactive = $("#pnlpass div.product-inactive").size();
                        if (size_inactive > 0) {
                            $('#pnlpass div.product:gt(2)').removeClass('product-inactive');
                            $('.show-more a').text('Hide more');
                        }
                        else {
                            $('#pnlpass div.product:gt(2)').removeClass('product-inactive').addClass('product-inactive');
                            $('.show-more a').text('Show more');
                        }
                    }
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="main-wrapper pos-rel">
        <div>
            <div class="row">
                <!-- Banner Start -->
                <div class="col-md-12">
                    <div class="bannernew">
                        <div class="bannernew-text">
                            <h1 runat="server" id="sbtn1">
                                Page Title</h1>
                            <h2 runat="server" id="sbtn2">
                                SUb title would go along here</h2>
                        </div>
                        <img src="images/banner-1.jpg" id="imgbanner" alt="" class="img-responsive" runat="server"
                            style="width: 100%; height: 240px; position: initial;">
                    </div>
                </div>
                <div class="Breadcrumb">
                    <asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
                <!-- Banner End -->
                <!-- Page Text Start -->
                <div class="col-md-12">
                    <div class="contant-text">
                        <p runat="server" id="sbtn3">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Nemo enim ipsam
                            voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.
                        </p>
                    </div>
                </div>
                <!-- Page Text End -->
                <!-- Plan Start -->
                <div class="col-md-12">
                    <div class="point-to">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <h2 runat="server" id="sbtn4">
                                    Point to Point</h2>
                                <hr class="blank">
                                <p runat="server" id="sbtn5">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fermentum tellus
                                    in sodales tincidunt. Morbi odio lorem, consequat vel lacus quis, commodo imperdiet
                                    libero. Ut imperdiet pellentesque urna vel elementum. Sed fringilla non enim blandit
                                    egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam commodo
                                    varius diam eget euismod. Mauris eu libero leo.</p>
                                <hr class="blank">
                                <div id="sbtn6" runat="server">
                                    <a href="#" class="light-green-btn">plan your journey</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <img id="imgptp" src="images/plan-img.jpg" alt="" class="img-responsive" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Plan End -->
                <!-- Tour Packages Start -->
                <div class="col-md-12">
                    <div class="tour-packages">
                        <hr class="blank">
                        <h2 runat="server" id="sbtn7">
                            Choose Your Pass</h2>
                        <div class="row" id="pnlpass">
                            <asp:Repeater ID="rptproduct" runat="server">
                                <ItemTemplate>
                                    <div class="col-md-4 col-sm-4 <%#Eval("ID")%> product">
                                        <div class="snip1174 yellow">
                                            <img src="<%#adminSiteUrl+""+Eval("PRODUCTIMAGE")%>" />
                                            <div>
                                                <div class="figcaption">
                                                    <div class="yellow-box">
                                                        <h2>
                                                            <%#Eval("NAME")%></h2>
                                                        <p style='display: <%#Convert.ToBoolean(Eval("ISSPECIALOFFER"))?"":"none"%>'>
                                                            Special Offer
                                                        </p>
                                                        <p style='display: <%#Convert.ToBoolean(Eval("BESTSELLER"))?"":"none"%>'>
                                                            Best Seller
                                                        </p>
                                                    </div>
                                                    <a href="<%#Eval("URL")%>"><span class="book-pass">Book Now</span><br />
                                                        <span class="book-left"></span><span class="book-price">From
                                                            <%=currency%>
                                                            <%#Eval("PPRICE")%></span><span class="book-right"></span> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div class="show-more">
                                <a>Show more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Tour Packages End -->
                <!-- Gray Box Start -->
                <div class="col-md-12">
                    <div class="gray-text-box">
                        <p runat="server" id="sbtn8">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fermentum tellus
                            in sodales tincidunt. Morbi odio lorem, consequat vel lacus quis, commodo imperdiet
                            libero. Ut imperdiet pellentesque urna vel elementum. Sed fringilla non enim blandit
                            egestas.
                        </p>
                    </div>
                </div>
                <!-- Gray Box End -->
                <!-- Map Section Start -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="map-sec">
                                <a href="#" runat="server" id="VEmaplink">
                                    <img id="imgmap" src="images/map-img.png" alt="" class="img-responsive" runat="server">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="visit-box">
                                <a href="#" runat="server" id="VEmaps1link">
                                    <img id="imgmaps1" src="images/demo-img-1.jpg" alt="" class="img-responsive" runat="server">
                                </a>
                            </div>
                            <div class="visit-box">
                                <a href="#" runat="server" id="VEmaps2link">
                                    <img id="imgmaps2" src="images/demo-img-2.jpg" alt="" class="img-responsive" runat="server">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Map Section End -->
                <!-- Contact Us Start -->
                <div class="col-md-12">
                    <div class="contact-box">
                        <h2>
                            Contact us</h2>
                        <div class="sta-contact-box">
                            <ul class="row">
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-phone" id="stacall" runat="server">
                                        <a href="tel:134782">Call 134 782</a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-appt" id="stabook" runat="server">
                                        <a href="http://www.statravel.com.au/appointment.htm">Book an Appointment</a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-chat" id="stalive" runat="server">
                                        <a onclick="javascript:window.open('http://statravel.ehosts.net/netagent/cimlogin.aspx?questid=13FC0C61-B374-49CD-B7A3-6D6468600D2A&amp;portid=363559B1-4892-4B6B-9C45-3877462C7BA9&amp;nareferer='+escape(document.location),'_blank','resizable=no,width=600,height=400,scrollbars=no'); return false;"
                                            href="//www.statravel.com.aujavascript:void(0);">Live Chat</a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <div class="sta-contact-email" id="staemail" runat="server">
                                        <a href="http://www.statravel.com.au/email_us.htm">Email Us</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Contact Us End -->
                <!-- Adventure Tours Start -->
                <!-- Adventure Tours End -->
                <div class="col-md-12" id="pnlnewadventure" runat="server" clientidmode="Static"
                    style="padding: 0px">
                    <div class="newadventure-tours" style="padding: 15px; margin: 0px; float: left;">
                        <div class="col-xs-6 col-sm-4 col-md-4" style="padding: 0px;" id="newadone">
                            <a href="#" data-toggle="modal" data-target="#mymodelnewadventure" class="edit-btn newadventure"
                                style="margin-top: -32px;">Edit</a>
                            <div class="thumbnail" style="padding: 0px;">
                                <div class="caption" style="width: 100%; height: 200px; background: rgba(246, 190, 0, 1);
                                    text-align: center; color: #fff !important;">
                                    <h4>
                                        UK’s top Harry Potter filming locations</h4>
                                    <div>
                                        <a href="#" class="book-now-btn">Book Now</a></div>
                                </div>
                                <img src="https://1track.internationalrail.net/Uploaded/SPCountryImg/e971df46-ec54-4eff-b60b-e065790a7cc4.jpg"
                                    alt="..." style="width: 100%; height: 200px;">
                            </div>
                            <label class="hide imgid">
                                4</label>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4" style="padding: 0px;" id="newadtwo">
                            <a href="#" data-toggle="modal" data-target="#mymodelnewadventure" class="edit-btn newadventure"
                                style="margin-top: -32px;">Edit</a>
                            <div class="thumbnail" style="padding: 0px;">
                                <img src="https://1track.internationalrail.net/Uploaded/SPCountryImg/855caa7a-abb7-44c8-b1a8-8b028c0837c3.jpg"
                                    alt="..." style="width: 100%; height: 200px;">
                                <div class="caption" style="width: 100%; height: 200px; background: rgba(246, 190, 0, 1);
                                    text-align: center; color: #fff !important;">
                                    <h4>
                                        Top 10 British dishes around the UK</h4>
                                    <div>
                                        <a href="#" class="book-now-btn">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <label class="hide imgid">
                                4</label>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4" style="padding: 0px;" id="newadthree">
                            <a href="#" data-toggle="modal" data-target="#mymodelnewadventure" class="edit-btn newadventure"
                                style="margin-top: -32px;">Edit</a>
                            <div class="thumbnail" style="padding: 0px;">
                                <div class="caption" style="width: 100%; height: 200px; background: rgba(246, 190, 0, 1);
                                    text-align: center; color: #fff !important;">
                                    <h4>
                                        Alternative nights out around the UK</h4>
                                    <div>
                                        <a href="#" class="book-now-btn">Book Now</a>
                                    </div>
                                </div>
                                <img src="https://1track.internationalrail.net/Uploaded/SPCountryImg/bc1814cb-5778-4265-82be-921376aad002.jpg"
                                    alt="..." style="width: 100%; height: 200px;">
                            </div>
                            <label class="hide imgid">
                                66</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
