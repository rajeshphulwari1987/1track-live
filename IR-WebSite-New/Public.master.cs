﻿using System;
using Business;
using System.Web.UI;
using System.Linq;
using System.Configuration;

public partial class Public : System.Web.UI.MasterPage
{
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly ManageSeo _master = new ManageSeo();
    public string AgentName = "Login ";
    readonly db_1TrackEntities db = new db_1TrackEntities();
    public Guid _siteId;
    public string siteURL, NumOfProducts = string.Empty;
    public string siteHeaderHtml = string.Empty;
    public string siteFooterHtml = string.Empty;
    public string staStyledata = string.Empty;
    public bool chkBookingCond = false;
    public bool chkConditions = false;
    public bool chkPrivacy = false;
    public string MetaKeyTitile = "International Rail";
    public string CurrentPagePath = string.Empty;
    public string MetaKeywords = string.Empty;
    public string MetaDescription = string.Empty;
    public string SocialMediaScript = string.Empty;
    public string SocialMediaScriptSTA = string.Empty;
    public string GoogleAnalyticCode = string.Empty;
    public string GoogleManagerTag = string.Empty;
    public string VisitTracking = string.Empty;
    public string VisitTrackingPageView = string.Empty;
    public string EcommerceTags = string.Empty;
    public string EventTags = string.Empty;
    public string mobileHeader, header, footer = string.Empty;
    public string cssUrl = "assets/css/main.css";
    public string b2bSitePhone = "";
    public string b2bSiteHeading = "";
    public string b2bSiteTitle = "";
    public string SiteLogoUrl = string.Empty;
    public string FaviconIcon = string.Empty;
    public string AdminSiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    public string IrRailCss = string.Empty;
    public string SiteAdminUrl = ConfigurationManager.AppSettings["HttpAdminHost"];

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            LoadSite();
            Boolean SiteIsDeleted = _oWebsitePage.GetSiteIsDeleted(_siteId);
            if (SiteIsDeleted)
            {
                Response.Redirect("SiteBolcked.aspx");
            }
            siteURL = _oWebsitePage.GetSiteURLbySiteId(_siteId);

            #region for new IR & travel cuts
            if (siteURL.Contains("travelcuts") || _oWebsitePage.GetSiteLayoutType(_siteId) == 4)
                cssUrl = siteURL + "assets/css/travelcuts.css";
            else if (siteURL.ToLower().Contains("merit") || _oWebsitePage.GetSiteLayoutType(_siteId) == 5)
                cssUrl = siteURL + "assets/css/merit.css";
            else
                cssUrl = siteURL + "assets/css/main.css";
            #endregion

        }
        catch (Exception ex)
        {
            //Response.Redirect("InvalidUrl.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetDetailsAnalyticTags();
            LoadSiteFavicon();
            var data = _oWebsitePage.GetSiteDetailsById(_siteId);
            if (data != null)
            {
                b2bSitePhone = data.PhoneNumber;
                b2bSiteTitle = data.SiteHeadingTitle;
                b2bSiteHeading = data.SiteHeading;
            }
        }
        LoadSiteLogo();
        IrRailFunctionality();
    }

    public void LoadSiteLogo()
    {
        var data = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.FaviconPath))
                FaviconIcon = "<link href='" + SiteAdminUrl + data.FaviconPath + "' rel='shortcut icon' type='image/x-icon' />";

            if (!string.IsNullOrEmpty(data.LogoPath))
                SiteLogoUrl = SiteAdminUrl + data.LogoPath;
            else
                SiteLogoUrl = siteURL + "assets/img/branding/logo.png";
        }
    }

    public void GetDetailsAnalyticTags()
    {
        ManageAnalyticTag _masterAnalyticTag = new ManageAnalyticTag();
        var data = _masterAnalyticTag.GetTagBySiteId(_siteId);
        if (data != null)
        {
            VisitTracking = "\n<script type='text/javascript'>\n/*VisitTracking*/\n" + data.VisitTracking + "\n</script>\n";
            VisitTrackingPageView = "\n<script type='text/javascript'>\n/*PageViewTag*/\n" + data.VisitTrackingPageView + "\n</script>\n";
            EcommerceTags = "\n<script type='text/javascript'>\n/*Ecommerce*/\n" + data.Ecommerce + "\n</script>\n";

            string[] EventData = data.EventTracking.Split(';');
            var Eventtxt = EventData[0].Replace("<Category>", Request.Url.AbsoluteUri).Replace("<Action>", "click").Replace("<Label>", Request.Url.Host);
            Eventtxt += EventData[1].Replace("<Category>", Request.Url.AbsoluteUri).Replace("<Action>", "click").Replace("<Label>", Request.Url.Host);
            EventTags = "\n<script type='text/javascript'>\n/*EventData*/\n" + Eventtxt + "\n</script>\n";
        }
    }

    void LoadSite()
    {
        if (Request.Url.Host == "localhost")
        {
            string url = Request.Url.ToString().ToLower();

            if (url.Contains("1tracknew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1tracknew/");
            if (url.Contains("1trackagentnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1trackagentnew/");
            else if (url.Contains("1trackagentdemo"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/1trackagentdemo/");
            else if (url.Contains("demousagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoUsAgent/");
            else if (url.Contains("demous"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoUs/");
            else if (url.Contains("demoausagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoAusAgent/");
            else if (url.Contains("demoaus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/DemoAus/");
            else if (url.Contains("interrailnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrailnew/");
            else if (url.Contains("interrailagentnew"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrailagentnew/");
            else if (url.Contains("interrail"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/interrail/");
            else if (url.Contains("amex"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/amex/");
            else if (url.Contains("australiap2p"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/australiap2p/");
            else if (url.Contains("travelcutsagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/travelcutsagent/");
            else if (url.Contains("travelcuts"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/travelcuts/");
            else if (url.Contains("irrailagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/irrailagent/");
            else if (url.Contains("irrail"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/irrail/");
            else if (url.Contains("singapore"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/singapore/");
            else if (url.Contains("thailandagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/thailandagent/");
            else if (url.Contains("thailand"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/thailand/");
            else if (url.Contains("meritagent"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/meritagent/");
            else if (url.Contains("stath.internationalrail.net"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://localhost/stath.internationalrail.net/");
        }
        else if (Request.Url.Host.Contains("projectstatus.in"))
        {

            if (Request.Url.Host.Contains("travelcuts.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://travelcuts.projectstatus.in/");
            else if (Request.Url.Host.Contains("travelcutsb2b.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://travelcutsb2b.projectstatus.in/");
            else if (Request.Url.Host.Contains("merit.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://merit.projectstatus.in/");
            else if (Request.Url.Host.Contains("bookmyrst.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://bookmyrst.projectstatus.in/");
            else if (Request.Url.Host.Contains("railpass.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://railpass.projectstatus.in/");
            else if (Request.Url.Host.Contains("internationalrail.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://internationalrail.projectstatus.in/");
            else if (Request.Url.Host.Contains("internationalrailuk.projectstatus"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://internationalrailuk.projectstatus.in/");

        }

        else if (Request.Url.Host.Contains("1tracktest.com"))
        {

            if (Request.Url.Host.Contains("staau.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://staau.1tracktest.com/");
            else if (Request.Url.Host.Contains("staaua.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://staaua.1tracktest.com/");
            else if (Request.Url.Host.Contains("stanz.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stanz.1tracktest.com/");
            else if (Request.Url.Host.Contains("stanza.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stanza.1tracktest.com/");
            else if (Request.Url.Host.Contains("stauk.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stauk.1tracktest.com/");
            else if (Request.Url.Host.Contains("stauka.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stauka.1tracktest.com/");
            else if (Request.Url.Host.Contains("staus.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://staus.1tracktest.com/");
            else if (Request.Url.Host.Contains("stausa.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://stausa.1tracktest.com/");
            else if (Request.Url.Host.Contains("idivd.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://idivd.1tracktest.com/");
            else if (Request.Url.Host.Contains("idive.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://idive.1tracktest.com/");
            else if (Request.Url.Host.Contains("idivw.1tracktest"))
                _siteId = _oWebsitePage.GetSiteIdByURL("http://idivw.1tracktest.com/");
        }

        else
        {
            siteURL = "http://" + Request.Url.Host + "/";
            _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);

            if (_siteId == new Guid())
            {
                siteURL = "https://" + Request.Url.Host + "/";
                _siteId = _oWebsitePage.GetSiteIdByURL(siteURL);
            }
        }
        Session["siteId"] = _siteId;
    }

    public void LoadSiteFavicon()
    {
        var data = db.tblSites.FirstOrDefault(x => x.ID == _siteId && x.IsActive == true);
        if (data != null)
        {
            if (!string.IsNullOrEmpty(data.FaviconPath))
                FaviconIcon = "<link href='" + AdminSiteUrl + data.FaviconPath + "' rel='shortcut icon' type='image/x-icon' />";
        }
    }

    public void IrRailFunctionality()
    {
        if (db.tblSites.Any(x => x.ID == _siteId && (bool)x.IsAgent && (x.LayoutType == 2 || x.LayoutType == 6)))
        {
            IrRailCss = "<link href='../assets/css/internationalrail.css' rel='stylesheet' type='text/css' />";
        }
    }
}
