﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="RailPasses.aspx.cs" Inherits="RailPasses" ValidateRequest="false" Culture="en-GB" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () { setMenuActive("RP"); });
        function SectionHide(objId) {
            var id = objId.toString();
            $("#" + id).attr("style", "display:none;");
        }
    </script>
    <style type="text/css">
        .btnhover:hover
        {
            color: #fb4f14;
        }
        li.starail-Country-listItem
        {
            width: 50% !important;
        }
        @media only screen and (max-width: 639px)
        {
            .starail-Country .starail-Country-listItem
            {
                width: 100% !important;
                float: none;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <h1 class="starail-u-alpha starail-Country-pageTitle">
                Explore More Rail Passes With Us</h1>
       <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
            <asp:Repeater ID="RptRailPasses" runat="server">
                <ItemTemplate>
                    <section class="starail-Country starail-u-cf">
                        <header class="starail-Country-header">
                            <h2 class="starail-Country-title"><%#Eval("Name")%></h2>
                        </header>
                        <ul class="starail-Country-list starail-u-cf">
                        <asp:Repeater ID="rptPasses" runat="server" DataSource='<%#Eval("SubMenuList") %>'>
                              <ItemTemplate>
                                        <li class="starail-Country-listItem"> 
                                         <a href='<%=siteURL%><%#Eval("Url") %>' title='<%#Eval("Name")%>'><%#Eval("Name")%> </a>
                                        </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                        </section>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
