﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingCondition.aspx.cs"
    MasterPageFile="~/Site.master" Inherits="BookingCondition" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server"   />
            </div>
        </div>
    </div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
       <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
        <h2>
            <%=Server.HtmlDecode(PageTitle)%></h2>
        <p>
            <%=Server.HtmlDecode(PageDescription)%></p>
        <div class="starail-Section starail-Section--nopadding">
            <ul class="starail-Accordion js-accordon">
                <asp:Repeater ID="rptBooking" runat="server">
                    <ItemTemplate>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    1. The Contract</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Contract"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    2. Bookings & Reservations
                                </h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("BookingsReservations"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    3. Validity</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Validity"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    4. Fares and payment</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("FaresPayment"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    5. Changes to your ticket and refunds</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("TicketRefunds"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    6. Lost or Damaged Tickets</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("LostDamagedTickets"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    7. Baggage</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Baggage"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    8. Force Majeure</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("ForceMajeure"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    9. Governing Law</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("GoverningLaw"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    10. Boarding times</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("BoardingTimes"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    11. Timetable information</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("TimetableInformation"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    12. Variations</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Variations"))%></p>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Content>
