﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TrainSegmentSearchResult.aspx.cs" Inherits="TrainSegmentSearchResult"
    Culture="en-GB" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %> 
<%@ Register Src="RegionalTrainsUc.ascx" TagName="RegionalTrainsUc" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="Scripts/gmap.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var start = document.getElementById('txtFrom').value;
            start = start.trim().replace('(All stations)', '');
            start = start.trim().replace(/[^a-z0-9]+/gi, ' ');
            start = start + " italy";


            var end = document.getElementById('txtTo').value;
            end = end.trim().replace('(All stations)', '');
            end = end.trim().replace(/[^a-z0-9]+/gi, ' ');
            end = end + " italy";

            var geocoder = new google.maps.Geocoder();
            initializeGMap();
            geocoder.geocode({ 'address': start }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
            geocoder.geocode({ 'address': end }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }); 
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
    </asp:ToolkitScriptManager> 
    <uc2:RegionalTrainsUc ID="RegionalTrainsUc1" runat="server" /> 
</asp:Content>
