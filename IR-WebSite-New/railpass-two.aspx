﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="railpass-two.aspx.cs" Inherits="railpass_two" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=NextBookingScriptIR %>
    <%=IRScriptingTagJs%>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <style>
        a
        {
            color: #0645ad;
        }
        a:visited
        {
            color: #0b0080;
        }
    </style>
    <style>
        .freeday
        {
            font-size: 25px !important;
        }
        .specialoffer
        {
            border: 2px dotted #C9DA00;
            padding: 8px;
            background-color: rgba(201, 218, 0, 0.07);
        }
        .starail-Banner .starail-Banner-primaryTitle
        {
            font-size: 36px;
            padding-top: 30px;
        }
        .starail-Banner .starail-Banner-secondaryTitle
        {
            font-size: 20px;
        }
        .starail-Banner .starail-Banner-offer
        {
            font-size: 14px;
            margin-bottom: 17px;
        }
        .starail-Banner
        {
            height: 220px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="starail-Grid starail-Grid--mobileFull" style="padding: 0px">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Back starail-u-hideDesktop">
                <a href='<%=siteURL%>' class="starail-Back-link"><i class="starail-Icon-chevron-left">
                </i>Back to search results</a>
            </div>
            <div class="starail-Banner" style="background: #0065BD;">
                <div class="starail-Banner-content">
                    <h1 class="starail-Banner-title starail-Banner-primaryTitle">
                        <%=ProductName %></h1>
                    <h2 class="starail-Banner-title starail-Banner-secondaryTitle">
                        See the world from just
                        <%=currency%>
                        <%=Price%></h2>
                    <div class="starail-Banner-offer starail-u-hideMobile" id="div_SpecialOffer" runat="server">
                        Special Offer</div>
                    <h3 class="starail-Banner-title" style="text-transform: none; font-size: 15px; font-weight: normal;">
                        <div runat="server" id="divspecialOfferText">
                        </div>
                    </h3>
                </div>
            </div>
            <div class="Breadcrumb">
                <asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
        </div>
    </div>
    <div class="starail-Grid starail-Grid--medium starail-u-relative">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Back starail-u-hideMobile">
                <a href='<%=siteURL%>' class="starail-Back-link"><i class="starail-Icon-chevron-left">
                </i>Back to search results</a>
            </div>
            <p>
                <%=NextBooking%>
            </p>
            <div class="specialoffer" runat="server" id="divAnnouncement" style="margin-bottom: 15px;
                font-size: 14px;">
                <p style="margin: 0px;">
                    <b>
                        <asp:Label runat="server" ID="lblAnnouncementTitle"></asp:Label></b>
                    <br>
                    <%=Announcement%></p>
            </div>
            <div class="starail-CalloutBox starail-CalloutBox--mobileFull" id="div_GreatPassDescription"
                runat="server">
            </div>
            <h2 id="h_Title" runat="server">
                Experience the adventure of train travel</h2>
            <div class="starail-ImageScroller">
                <div class="starail-ImageScroller-inner starail-u-cf">
                    <img src='<%=adminSiteUrl+""+ProductImage %>' alt="<%=ProductImageAltTag %>" title="<%=ProductImageAltTag %>"
                        class="starail-ImageScroller-image" width="300px" height="200px" />
                    <img src='<%=adminSiteUrl+""+ProductImageSecond %>' alt="<%=ProductImageSecondAltTag %>"
                        title="<%=ProductImageSecondAltTag %>" class="starail-ImageScroller-image" width="300px"
                        height="200px" />
                </div>
            </div>
            <div id="divExtraDayDesc" runat="server">
                <div class="specialoffer">
                    <h4>
                        BritRail Free Day Promo:</h4>
                    <p>
                        Extra day with BritRail Passes, BritRail England Passes and BritRail South West
                        Passes when purchased by 18th Sep 2015. Valid for Adult, Youth, Child, Senior and
                        Saver fares. All BritRail Extra Day Promo Passes are printed on a separate ticket.
                    </p>
                </div>
            </div>
            <p>
                <%=Description%></p>
           <%-- <div class="starail-CalloutBox starail-CalloutBox--mobileFull" id="termandcondition"
                runat="server">
            </div>--%>
            <hr class="starail-Type-hr--large " />
            <p class="starail-u-marginNone">
                <%=NextBooking%>
            </p>
        </div>
    </div>
    <br />
    <div class="starail-Grid starail-Grid--medium starail-u-relative">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <asp:Repeater ID="rptCrossSale" runat="server">
                <ItemTemplate>
                    <div class="starail-u-marginNone" style="position: relative; height: 232px;">
                        <a href='<%#Eval("URLLink")%>' target="_blank" style="text-decoration: none;">
                            <div style="position: absolute; width: 100%; height: 220px;">
                                <img src='<%# adminSiteUrl+""+Eval("BannerImg") %>' alt='' style="width: 100%; height: 100%;
                                    <%#Eval("BannerImg").ToString().Length<28?"display:none;": ""%>border: none!important;" />
                            </div>
                            <div class="Link-overlay" style="height: 95%; position: relative; padding-top: 10px;
                                text-align: center;">
                                <h3 style="margin-bottom: 5px;">
                                    <%#Eval("ShortDescription")%>
                                </h3>
                                <h4>
                                    <%#((Eval("Price") != null && Convert.ToDecimal(Eval("Price"))!=0) ? "From " + currency + " " + Eval("Price").ToString() + "" : "")%></h4>
                                <p class="starail-u-hideMobile starail-Button desc-section">
                                    <%#Eval("LongDescription").ToString().Length > 350 ? Eval("LongDescription").ToString().Substring(0, 350) + ".." : Eval("LongDescription")%>
                                </p>
                                <p class="starail-u-hideDesktop starail-Button desc-section">
                                    <%#Eval("LongDescription").ToString().Length > 200 ? Eval("LongDescription").ToString().Substring(0, 200) + ".." : Eval("LongDescription")%>
                                </p>
                            </div>
                        </a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <script type="text/javascript">
        loadevent();
        function loadevent() {
            $(".loycheckboxSpan").remove();
            $(".loycheckbox").find('label').before("<span class='loycheckboxSpan'><i class='starail-Icon-tick'></i></span>");
        }
    </script>
    <style type="text/css">
        .Link-overlay:hover
        {
            background-color: rgba(0,0,0,0.65);
        }
        .Link-overlay:hover h3, .Link-overlay:hover h4
        {
            color: #fff;
        }
        .desc-section
        {
            font-weight: normal;
            font-size: 12px !important;
            margin-right: 10px;
            margin-left: 10px;
        }
    </style>
</asp:Content>
