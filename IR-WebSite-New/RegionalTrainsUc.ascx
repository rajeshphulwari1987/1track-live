﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegionalTrainsUc.ascx.cs"
    Inherits="RegionalTrainsUc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/ucTrainSearch.ascx" TagName="ucTrainSearch" TagPrefix="uc1" %>
<asp:HiddenField ID="hdnDisplaySendJourney" runat="server" Value="false" />
<asp:UpdatePanel ID="upnlTrainSearch" runat="server">
    <ContentTemplate>
        <div class="starail-Grid starail-Grid--mobileFull">
            <div class="starail-Grid-col starail-Grid-col--nopadding">
                <div class="starail-ProgressBar starail-ProgressBar-stage--stage1 starail-u-hideMobile">
                    <div class="starail-ProgressBar-line">
                        <div class="starail-ProgressBar-progress">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Ticket Selection</p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Booking Details</p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Checkout</p>
                    </div>
                </div>
                <asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
                <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
                <asp:HiddenField ID="hdnInbountOrOutBound" runat="server" Value="" />
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="starail-Section starail-Section--nopadding">
                    <div class="starail-Tabs starail-TrainOptions-mainTabs">
                        <div class="starail-Tabs-wrapper">
                            <div class="starail-Grid-col starail-Grid-col--8of12">
                                <div id="outbound" class="starail-Tabs-content">
                                    <asp:Repeater ID="rptTrainResult" runat="server" OnItemDataBound="rptTrainResult_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnTrainNo" runat="server" Value='<%#Eval("TrainNumber")%>' />
                                            <div class="starail-JourneyBlock">
                                                <div class="starail-JourneyBlock-form">
                                                    <div class="starail-JourneyBlock-header">
                                                        <div class="starail-JourneyBlock-summary">
                                                            <div class="starail-DestinationIcon">
                                                                <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p class="starail-JourneyBlock-infoRow">
                                                            <i class="starail-Icon-rail"></i><span>
                                                                <%#Eval("TrainDescr").ToString()%>
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <asp:HiddenField ID="hdnSelectedInfo" runat="server" Value="" />
                                                    <div class="starail-JourneyBlock-content starail-u-hideMobile" id="DivTr" runat="server">
                                                    </div>
                                                    <%--Mobile--%>
                                                    <div class="starail-JourneyBlock-mobileTickets">
                                                        <asp:DropDownList ID="ddlFromPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                        <asp:HyperLink ID="hlnkConfirm" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                            Text="Confirm Ticket" />
                                                    </div>
                                                    <div class="starail-JourneyBlock-footer starail-u-hideMobile" style="height: 0px;">

                                                    </div>
                                                    <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                        <p class="starail-JourneyBlock-footer-ticketSelection">
                                                            <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">
                                                                <i class="starail-Icon-question"></i></a>
                                                        </p>
                                                        <div class="starail-JourneyBlock-mobileFooter-btns" style="width: 100%">
                                                            <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:LinkButton ID="lnkContinue" runat="server" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger IR-GaCode">Next: Booking Details</asp:LinkButton>
                                </div>
                            </div>
                            <div class="starail-Grid-col starail-Grid-col--4of12 starail-u-hideMobile starail-Sidebar">
                                <a href="#" class="starail-Button starail-Button--full  starail-rtnJourney-trigger openPopUp IR-GaCode">
                                    Next: Booking Details</a>
                                <div class="starail-Tabs starail-Sidebar-tabs">
                                    <ul class="starail-Tabs-tabs">
                                        <li class="starail-Tabs-tab starail-Tabs-tab--active"><a href="#starail-routemap"
                                            class="js-starail-Tabs-trigger">
                                            <div>
                                                Route Map
                                            </div>
                                        </a></li>
                                        <li class="starail-Tabs-tab"><a href="#starail-edittickets" class="js-starail-Tabs-trigger">
                                            <div>
                                                Edit Search
                                            </div>
                                        </a></li>
                                    </ul>
                                    <div id="starail-routemap" class="starail-Tabs-content">
                                        <div class="results-map">
                                            <div id="mymap" class="results-map" style="height: 300px; width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="starail-edittickets" class="starail-Tabs-content starail-Tabs-content--hidden">
                                        <div class="starail-SearchTickets starail-SearchTickets--mini">
                                            <div class="starail-Box starail-Box--whiteMobile">
                                                <uc1:ucTrainSearch ID="ucTrainSearch" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                <div class="starail-Sidebar-bottomBtns">
                                    <a href="#" class="starail-Button starail-Button--full  starail-rtnJourney-trigger openPopUp IR-GaCode">
                                        Next: Booking Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%=pricePoup%>
        <asp:Label ID="lblMsg" runat="server" Visible="False" CssClass="clsMsg"></asp:Label>
        <asp:ModalPopupExtender ID="mdpexQuickLoad" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="lnkContinue" />
        <asp:Panel ID="pnlQuckLoad" runat="server">
            <div class="starail-popup">
                <div class="starail-BookingDetails-form">
                    <h2>
                        Lead Passenger Information</h2>
                    <div class="starail-Form-row">
                        <label class="starail-Form-label" for="starail-firstname">
                            Lead passenger name <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col starail-Form-inputContainer-col--ddInput">
                                <asp:DropDownList ID="ddlTitle" runat="server" CssClass="starail-Form-select" TabIndex="95">
                                        <asp:ListItem Value="0">Title</asp:ListItem>
                                        <asp:ListItem Value="1">Dr.</asp:ListItem>
                                        <asp:ListItem Value="2">Mr.</asp:ListItem>
                                        <asp:ListItem Value="3">Miss.</asp:ListItem>
                                        <asp:ListItem Value="4">Mrs.</asp:ListItem>
                                        <asp:ListItem Value="5">Ms.</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqvalerror0" runat="server" ControlToValidate="ddlTitle"
                                    InitialValue="0" Display="Dynamic" ValidationGroup="cntnu" />
                                <asp:TextBox ID="txtFirstname" runat="server" MaxLength="15" CssClass="starail-Form-input"
                                    TabIndex="96"/>
                                <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ControlToValidate="txtFirstname"
                                    Display="Dynamic" ValidationGroup="cntnu" />
                                     <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFirstname"
                            WatermarkText="First Name" WatermarkCssClass="watermark"/>
                            </div>
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtLastname" runat="server" MaxLength="25" CssClass="starail-Form-input"
                                    TabIndex="96"/>
                                <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ControlToValidate="txtLastname"
                                    Display="Dynamic" ValidationGroup="cntnu" />
                                     <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtLastname"
                            WatermarkText="Last Name" WatermarkCssClass="watermark"/>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label class="starail-Form-label" for="starail-country">
                            Country of residence <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="starail-Form-select" TabIndex="97">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ErrorMessage="*" ControlToValidate="ddlCountry"
                                    InitialValue="0" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <div id="ulTI" runat="server" class="fare-rules-list">
                            <div id="divFareRulesTI" runat="server">
                                <span class="tresult">
                                    <p>
                                        <strong>Travel Conditions: </strong>
                                    </p>
                                    <p>
                                        The standard full fare for all classes of seating. Overnight trains will also offer
                                        an option to upgrade to sleeper carriages.<br>
                                    </p>
                                    <p>
                                        Cancellation requests are subject to a 30% penalty when received 24 hours prior
                                        to the trains departure.<br>
                                    </p>
                                    <p>
                                        The cancellation penalty breakdown is as follows: 20% penalty to Trenitalia, and
                                        10% processing fee to International Rail for handling and processing the cancellation.</p>
                                    <p>
                                        The ticket is non-refundable if cancelled within 24 hours of the trains departure.<br>
                                    </p>
                                    <p>
                                        Changes to your ticket are allowed at a fee of £10.00 per change.<br>
                                    </p>
                                    <p>
                                        Changes are also subject to a price increase if the new fare is more expensive.
                                        There is NO refund if the new fare is less expensive.<br>
                                    </p>
                                    <p>
                                        No changes are allowed within 24hours prior to the trains departure.<br>
                                    </p>
                                    <p>
                                        Once your PNR has been modified (trains departure date or time) then cancellation
                                        of PNR and refunds can not be issued.<br>
                                    </p>
                                    <br>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label for="starail-email" class="starail-Form-label">
                        </label>
                        <div class="starail-Form-inputContainer">
                            <asp:Button ID="btnCloseWin" runat="server" Text="Cancel" CssClass="starail-Button starail-Form-button is-disabled"
                                TabIndex="99" />
                            <asp:Button ID="btnContinue" runat="server" CssClass="starail-Button starail-Form-button IR-GaCode"
                                TabIndex="98" Text="Continue" ValidationGroup="cntnu" OnClick="btnContinue_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upnlTrainSearch"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    $(document).ready(function () {
        $(".openPopUp").click(function () {
            $find('MainContent_RegionalTrainsUc1_mdpexQuickLoad').show(); ;
        });

        $('input[type=radio]').click(function () {
            var val = $(this).val();
            if (this.id.indexOf('rdo') > -1) {
                var id = this.id.replace("rdo", "");
                $('#MainContent_RegionalTrainsUc1_rptTrainResult_hdnSelectedInfo_' + id).val(val);
            }
        });
    }); 
</script>
