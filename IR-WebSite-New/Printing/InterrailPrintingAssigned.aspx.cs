using System;
using System.Web.UI;
using Business;
using System.Xml;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web.UI.HtmlControls;

public partial class Printing_InterrailPrintingAssigned : Page
{
    private readonly ManagePrintQueue _oPrint = new ManagePrintQueue();
    private readonly ManageBooking MBooking = new ManageBooking();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private Guid _siteId;
    public int CounterPass = 0;
    public string SiteUrl;
    public string script = "<script></script>";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            SiteUrl = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["siteId"] = "98BB7831-068A-4963-9EB8-D7CCAB265B4E";
            if (!Page.IsPostBack)
            {
                BindQueue();
                QubitOperationLoad();
                btnProblem.Visible = btnPrintedOK.Visible = btnNotVoidOk.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    public void BindQueue()
    {
        try
        {
            List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
            List<Guid> listIds = listCatIds != null ? listCatIds.Select(x => x.Id).Distinct().ToList() : null;
            if (Request.QueryString["qId"] != null && listIds != null)
            {
                var queueId = Guid.Parse(Request.QueryString["qId"]);
                List<spGetInJobedPrintQueueItemsInterrail_Result> list = new List<spGetInJobedPrintQueueItemsInterrail_Result>();
                string strIds = string.Empty;
                foreach (var item in listIds)
                    strIds = string.IsNullOrEmpty(strIds) ? item.ToString() : strIds + "," + item.ToString();

                list = _oPrint.GetPrintQueueInJobListInterrail(queueId, strIds, _siteId, AgentuserInfo.UserID);
                if (list != null && list.Count > 0)
                {
                    lblRangeFrm.Text = list.Min(ty => ty.STOCKNO).ToString();
                    lblRangeTo.Text = list.Max(ty => ty.STOCKNO).ToString();
                    hdnTotalPass.Value = list.Where(t => t.SAVERCODE.Contains("no")).Count().ToString();
                    rptPrint.DataSource = list;
                    rptPrint.DataBind();
                    dvPrd.Visible = true;
                }
                else
                {
                    ShowMessage(2, "Stock not available for this agent.");
                    btnPrint.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message + "::" + ex.StackTrace);
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            List<CategoryIds> listCatIds = Session["lisCategoryIds"] as List<CategoryIds>;
            List<Guid> listIds = listCatIds.Select(x => x.Id).Distinct().ToList();
            var queueId = Guid.Parse(Request.QueryString["qId"]);
            foreach (var item in listIds)
                _oPrint.UpdatePrintQStausFromAssigenToInJob(queueId, item, _siteId);
            PrintClick();
            List<long> stockno = new List<long>();
            foreach (RepeaterItem row in rptPrint.Items)
            {
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt64(stkNo.Text);
                stockno.Add(stNo);
            }
            btnConfirm_Click(stockno.Min(), stockno.Max());
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnConfirm_Click(long stockfrom, long stockto)
    {
        try
        {
            Guid queueId = new Guid();
            if (Request.QueryString["qId"] != null)
                queueId = Guid.Parse(Request.QueryString["qId"]);

            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;

            string tktXML = string.Empty;
            string pqitmIds = string.Empty;
            int start = 0;
            List<Guid> ids = new List<Guid>();
            foreach (RepeaterItem row in rptPrint.Items)
            {
                CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
                if (chkPrint.Checked)
                {
                    HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
                    Guid id = Guid.Parse(hdnID.Value);
                    ids.Add(id);
                    if (start == 0)
                        pqitmIds = id.ToString();
                    else
                        pqitmIds += "," + id.ToString();
                    start = 1;
                }
            }
            var list = _oPrint.GetPrintQueueItemsInJobListInterrail(userid, pqitmIds, queueId, stockfrom, stockto);
            foreach (var item in list)
                tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;

            if (!string.IsNullOrEmpty(tktXML))
                PrintTicket(queueId, tktXML);
            foreach (var id in ids)
                _oPrint.UpdatePrintQConfirmedStatus(id, 2); //2.printed
            UpdateInterrailStockNumberStatus(2); //2.printed
        }
        catch (Exception ex)
        {
            List<long> stockno = new List<long>();
            foreach (RepeaterItem row in rptPrint.Items)
            {
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateInterrailStockNoReuse(stno);//reuse stockno
            CleareprintingQueue(4);//4.Queued tblprintqueueitems update
            UpdateOrderStatusByStatus(false, 3);//3:Approved;
            ShowMessage(2, ex.Message);
        }
    }

    public void PrintTicket(Guid branchId, string tktXML)
    {
        try
        {
            XmlDocument coupan = new XmlDocument();
            coupan.LoadXml(tktXML);
            //--Load RCT2 Template     
            var list = _oPrint.GetRCT2Template(branchId, 1);
            if (list == null || list.Count == 0) return;
            XmlDocument template = new XmlDocument();
            template.LoadXml(list[0].Template);

            TicketPrinter.TicketPrinter TP = new TicketPrinter.TicketPrinter();
            TP.RootPath = Server.MapPath("");
            MemoryStream ms = new MemoryStream();
            ms = TP.Print(template, coupan);
            byte[] streampdf = ms.ToArray();
            string FileName = Guid.NewGuid().ToString();
            string file = "Printing/PrintedTicket/" + FileName + ".pdf";
            string path = Server.MapPath("~/" + file);
            File.WriteAllBytes(path, streampdf);
            ViewState["url"] = "../" + file;
            ViewState["FileURL"] = ConfigurationManager.AppSettings["PassTicketURl"] + FileName + ".pdf";
            PrintingView.Visible = true;

            Printingiframe.Attributes["src"] = SiteUrl + file;
            UpdateOrderStatusByStatus(true, 7);//7:Completed;
            rangeAssign.Visible = btnPrint.Visible = btnCancel.Visible = false;
            Msgtxt.InnerHtml = "Please find below a PDF of your pass(es).  In order to print, first check the correct stock (and stock numbers) are in the printer.   Next click on the printer icon in the top right corner of the PDF window, choose your printer and make sure you have the �Legal� paper size selected, then click �OK�.   <br/> </br> Once you have printed the pass(es) successfully on the correct stock (and stock numbers), click �All passes printed OK� at the bottom of the page.";
            lblHeader.Text = "Printing";
            btnProblem.Visible = btnPrintedOK.Visible = true;
        }
        catch (Exception ex) { }
    }

    public void PrintClick()
    {
        Guid PrintQueueIdNew = Guid.Empty;
        bool caseCount = true;
        Guid PQID = Guid.Empty;
        Guid queueId = new Guid();
        if (Request.QueryString["qId"] != null)
            queueId = Guid.Parse(Request.QueryString["qId"]);
        CounterPass = 0;
        foreach (RepeaterItem row in rptPrint.Items)
        {
            CheckBox chkPrint = row.FindControl("chkPrint") as CheckBox;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            HiddenField hdnCategoryID = row.FindControl("hdnCategoryID") as HiddenField;
            HiddenField hdnSaver = row.FindControl("hdnSaver") as HiddenField;
            HiddenField hdnStatus = row.FindControl("hdnStatus") as HiddenField;
            Guid catId = Guid.Parse(hdnCategoryID.Value);
            Guid id = Guid.Parse(hdnID.Value);
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            if (Convert.ToUInt64(hdnOrderNo.Value) > 0)
                PQID = id;
            if (chkPrint.Checked && !string.IsNullOrWhiteSpace(hdnOrderNo.Value))
            {
                /*if non saver and void then*/
                if (!hdnSaver.Value.ToLower().Contains("yes"))
                {
                    #region For Void and non saver pass
                    string tktXML = string.Empty;
                    Guid passSaleID = (Guid)_oPrint.GetPassSaleIDByPrintQueueID(id);
                    var list = _oPrint.GetRetrieveEurailProductXML(passSaleID, passSaleID.ToString());
                    foreach (var item in list)
                    {
                        tktXML += item.XML_F52E2B61_18A1_11d1_B105_00805F49916B;
                    }
                    if (!string.IsNullOrEmpty(tktXML))
                    {
                        CounterPass++;
                        XmlDataDocument xDoc = new XmlDataDocument();
                        xDoc.LoadXml(tktXML);
                        XmlDocument result = null;
                        result = InterrailBuildTemplate(xDoc, Server.MapPath("Interrail.xml"));
                        XmlNode xnr = result.SelectSingleNode("/Coupons");
                        if (xnr != null)
                        {
                            foreach (XmlNode xn in xnr)
                            {
                                Guid userid = AdminuserInfo.UserID;
                                if (AgentuserInfo.UserID != new Guid())
                                    userid = AgentuserInfo.UserID;
                                _oPrint.InsertStockInUsageInterrail(catId, queueId, id, stNo, xn.OuterXml, userid, false);
                            }
                        }
                    }
                    else
                    {
                        if (caseCount)
                        {
                            caseCount = false;
                            PrintQueueIdNew = Guid.NewGuid();
                            _oPrint.Insertqueueitems(PrintQueueIdNew, _oPrint.getPQdata(PQID));
                        }
                        string VOID = "<Coupon name=\"Pass\"><Element type=\"text\" name=\"void\" startcol=\"1\" endcol=\"72\" startrow=\"A\" endrow=\"R\" fontsize=\"37\" valign=\"middle\" align=\"middle\" fontface=\"Verdana\" fontbold=\"true\">VOID</Element></Coupon>";
                        _oPrint.InsertStockInUsageInterrail(catId, queueId, PrintQueueIdNew, stNo, VOID, AdminuserInfo.UserID, false);
                    }
                    #endregion
                }
            }
        }
    }

    public XmlDocument InterrailBuildTemplate(XmlDocument xDoc, string Path)
    {
        try
        {
            #region DECLARE FUNCATION VARIABLES
            decimal Cost = 0;
            int TotalPass = (!string.IsNullOrEmpty(hdnTotalPass.Value) ? Convert.ToInt32(hdnTotalPass.Value) : 4);
            int TravellerCat = 0;
            int DaysIn = 0;
            int LastDateDaysIn = 0;
            int Months = 0;
            int CountryCodeStart = 0;
            string CategoryName = string.Empty;
            string ProductName = string.Empty;
            string Traveller = string.Empty;
            string validity = string.Empty;
            string ValidIN = string.Empty;
            string ClassCode = string.Empty;
            string TemplateNote = string.Empty;
            Passenger[] pas = new Passenger[0];
            DateTime DateOfIssue = DateTime.Now;
            DateTime MustBeActivated = DateOfIssue.AddMonths(11);
            DateTime StartDate = DateTime.MinValue;
            DateTime EndDate = DateTime.MinValue;
            Boolean IsFlexi = false;
            Boolean InterRailPromo = false;
            Guid ProductID = Guid.Empty;
            String SiteCurrency = "EUR";
            String ReferenceNo = "";
            XmlNode xnr;
            XmlDocument xTemplate = new XmlDocument();
            xTemplate.Load(Path);
            #endregion

            #region Default value section
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/OrderID");
            if (xnr != null)
                ReferenceNo = xnr.InnerText;
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ID");
            if (xnr != null)
            {
                ProductID = Guid.Parse(xnr.InnerText);
                DateTime PrdtValidityDate = _oPrint.GetProductValiditydate(ProductID);
                if (MustBeActivated > PrdtValidityDate)
                {
                    MustBeActivated = PrdtValidityDate;
                }
            }
            /*****************************/
            //xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CostCurrencyID");
            //if ((xnr != null) && (xnr.InnerText != ""))
            //    SiteCurrency = _oPrint.GetCurrencySourceCode(Guid.Parse(xnr.InnerText));

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Cost");
            if ((xnr != null) && (xnr.InnerText != ""))
            {
                try
                {
                    Cost = Convert.ToDecimal(xnr.InnerText.ToString());
                }
                catch (Exception e) { }
            }
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/SupplierCurrencyID");
            if ((xnr != null) && (xnr.InnerText != ""))
            {
                Guid sourceCurId = Guid.Parse(xnr.InnerText);
                Guid targetCurrId = FrontEndManagePass.GetCurrencyID("SEU");
                if (sourceCurId != targetCurrId)
                    Cost = FrontEndManagePass.GetPriceAfterConversion(Cost, _siteId, targetCurrId, sourceCurId);
            }
            /*****************************/

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Vailidity");
            if ((xnr != null) && (xnr.InnerText != "")) validity = xnr.InnerText.ToUpper();
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/IsFlexi");
            if ((xnr != null) && (xnr.InnerText != "")) IsFlexi = Convert.ToBoolean(xnr.InnerText);
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/DaysIn");
            if ((xnr != null) && (xnr.InnerText != "")) DaysIn = (!String.IsNullOrEmpty(xnr.InnerText)) ? Convert.ToInt32(xnr.InnerText) : 0;
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/LastDateDaysIn");
            if ((xnr != null) && (xnr.InnerText != "")) LastDateDaysIn = (!String.IsNullOrEmpty(xnr.InnerText)) ? Convert.ToInt32(xnr.InnerText) : 0;
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Months");
            if ((xnr != null) && (xnr.InnerText != "")) Months = (!String.IsNullOrEmpty(xnr.InnerText)) ? Convert.ToInt32(xnr.InnerText) : 0;
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/IsPromo");
            if (xnr != null) InterRailPromo = Convert.ToBoolean(xnr.InnerText);
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/TravellerCat");
            if ((xnr != null) && (xnr.InnerText != "")) TravellerCat = Convert.ToInt32(xnr.InnerText);
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/ClassCode");
            if ((xnr != null) && (xnr.InnerText != "")) ClassCode = xnr.InnerText == "0" ? string.Empty : xnr.InnerText;
            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Traveller");
            if ((xnr != null) && (xnr.InnerText != "")) Traveller = xnr.InnerText.ToUpper();
            /*****************************/

            xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/CountryCodeStart");
            if (xnr != null && xnr.InnerText != "") CountryCodeStart = Convert.ToInt32(xnr.InnerText);
            if (CountryCodeStart > 0 && CountryCodeStart < 1000)// One Country Pass 
            {
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/Name");
                if ((xnr != null) && (xnr.InnerText != "")) ProductName = xnr.InnerText.ToUpper().Replace("INTERRAIL ", string.Empty);
                TemplateNote = "NOT VALID IN COUNTRY OF RESIDENCE";
                CategoryName = "INTERRAIL ONE COUNTRY PASS";
                ValidIN = _oPrint.GetProductsuppliercategory(ProductID);
            }
            else //other pass(Global pass)
            {
                xnr = xDoc.SelectSingleNode("/Voucher/Products/Product/PassType");
                if ((xnr != null) && (xnr.InnerText != "")) ProductName = xnr.InnerText.ToUpper().Replace("INTERRAIL ", string.Empty);
            }

            if (CountryCodeStart == 9999) //Global Pass
            {
                ValidIN = "ALL COMPANIES PARTICIPATING IN INTERRAIL";
                TemplateNote = "*ONLY 1 OUT- AND 1 INBOUND JOURNEY IN THIS COUNTRY";
                CategoryName = "INTERRAIL";
            }
            else if (CountryCodeStart == 501)// Attica (Greek) 
            {
                //TemplateNote = "CONDITIONS OF USE: WWW.EURAILGROUP.ORG/ATTICA";
                TemplateNote = _oPrint.GetProductPrintingNote(ProductID);
            }

            if (Cost == 0 && Traveller.Contains("CHILD"))//Child Pass
                TemplateNote = "MAXIMUM 2 FREE CHILDREN PER ADULT";
            /*****************************/

            xnr = xDoc.SelectSingleNode("/Voucher/Passengers");
            if (xnr != null)
            {
                XmlNodeList nl = xnr.ChildNodes;
                pas = new Passenger[nl.Count];
                int counter = 0;
                foreach (XmlNode xn in nl)
                {
                    pas[counter].Name = string.IsNullOrEmpty(xn["InterrailName"].InnerText) ? string.Empty : xn["InterrailName"].InnerText.ToUpper();
                    pas[counter].Country = string.IsNullOrEmpty(xn["Country"].InnerText) ? string.Empty : xn["Country"].InnerText.ToUpper();
                    pas[counter].PassportNumber = string.IsNullOrEmpty(xn["PassportNo"].InnerText) ? string.Empty : xn["PassportNo"].InnerText.ToUpper();
                    pas[counter].DOB = xn["DOB"] != null ? Convert.ToDateTime(xn["DOB"].InnerText) : (DateTime?)null;
                    pas[counter].PassStartDate = xn["PassStartDate"] != null ? Convert.ToDateTime(xn["PassStartDate"].InnerText) : (DateTime?)null;
                    counter++;
                }
            }
            /*****************************/
            if (pas[0].PassStartDate.HasValue)
            {
                StartDate = pas[0].PassStartDate.Value;
                if (DaysIn > 0 && Months == 0)
                    EndDate = StartDate.AddDays(DaysIn);
                else if (LastDateDaysIn > 0 && DaysIn > 0)
                    EndDate = StartDate.AddDays(LastDateDaysIn);
                else if (Months > 0 && DaysIn == 0)
                {
                    EndDate = StartDate.AddMonths(Months);
                    //// if(InterRailPromo)
                    EndDate = EndDate.AddDays(1);/*As per Vesa email  RE: Interrail Flash day promo.*/
                }
                else if (DaysIn > 0 && Months > 0)
                    EndDate = StartDate.AddMonths(Months);
                if (EndDate > DateTime.Now)
                    EndDate = EndDate.AddDays(-1);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassValidDate']");
                if (xnr != null) xnr.InnerText = StartDate.ToString("dd-MM-yyyy").Replace("-", ".").Replace("/", ".") + " - " + EndDate.ToString("dd/MM/yyyy").Replace("-", ".").Replace("/", ".");
            }
            #endregion

            #region Manage interrail section
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassName']");
            if (xnr != null) xnr.InnerText = pas[0].Name;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassCountry']");
            if (xnr != null) xnr.InnerText = pas[0].Country;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassPassport']");
            if (xnr != null) xnr.InnerText = pas[0].PassportNumber;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassDOB']");
            if (xnr != null) xnr.InnerText = pas[0].DOB.HasValue ? pas[0].DOB.Value.ToString("dd/MM/yyyy").Replace("-", ".").Replace("/", ".") : string.Empty;
            //xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassValidDate']");
            //if (xnr != null) xnr.InnerText = DateOfIssue.ToString("dd-MM-yyyy").Replace("-", ".").Replace("/", ".") + " - " + MustBeActivated.ToString("dd/MM/yyyy").Replace("-", ".").Replace("/", ".");
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassClass']");
            if (xnr != null) xnr.InnerText = ClassCode;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassCategory']");
            if (xnr != null) xnr.InnerText = CategoryName;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassProduct']");
            if (xnr != null) xnr.InnerText = ProductName;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassTravellerValid']");
            if (xnr != null) xnr.InnerText = validity;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassProductType']");
            if (IsFlexi)
            {
                if (xnr != null) xnr.InnerText = "FLEXI";
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtCalendar']");
                if (xnr != null) xnr.Attributes["numberofdays"].InnerXml = DaysIn.ToString();
            }
            else
            {
                xnr.InnerText = "CONTINUOUS";
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPromotionalText']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtCalendar']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='LblPassDay']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='LblPassMonth']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassValidityText']");
            if (xnr != null) xnr.InnerText = ValidIN;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassTravellerName']");
            if (xnr != null) xnr.InnerText = Traveller;
            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassTemplateNote']");
            if (xnr != null) xnr.InnerText = TemplateNote;
            /*InterRail PROMOTIONAL PASS*/
            if (InterRailPromo)
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='LblPassType']");
                if (xnr != null) xnr.InnerText = "PROMOTIONAL PASS";
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='InterrailRefundable']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
            }

            /* PRINTDATE FOR COMPLIMENTARY AND AD75 PASS ONLY*/
            if (TravellerCat == 31 || TravellerCat == 32 || TravellerCat == 61)
            {
                //if (pas[0].PassStartDate.HasValue)
                //{
                //    StartDate = pas[0].PassStartDate.Value;
                //    if (DaysIn > 0 && Months == 0)
                //        EndDate = StartDate.AddDays(DaysIn);
                //    else if (Months > 0 && DaysIn == 0)
                //        EndDate = StartDate.AddMonths(Months);
                //    else if (DaysIn > 0 && Months > 0)
                //        EndDate = StartDate.AddMonths(Months);
                //    if (EndDate > DateTime.Now)
                //        EndDate = EndDate.AddDays(-1);
                //    xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassValidDate']");
                //    if (xnr != null) xnr.InnerText = StartDate.ToString("dd-MM-yyyy").Replace("-", ".").Replace("/", ".") + " - " + EndDate.ToString("dd/MM/yyyy").Replace("-", ".").Replace("/", ".");
                //}
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='LblPassType']");
                if (xnr != null) xnr.InnerText = "COMPLIMENTARY PASS";
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassPrice']");
                if (xnr != null) xnr.InnerText = "PRICE " + SiteCurrency + ": ***.**";
            }
            else
            {
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassComplimentary']");
                if (xnr != null) xnr.ParentNode.RemoveChild(xnr);
                xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassPrice']");
                if (xnr != null) xnr.InnerText = "PRICE " + SiteCurrency + ": " + Cost.ToString("F2");
            }

            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtRef']");
            if (xnr != null) xnr.InnerText = ReferenceNo;

            xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtDateOfIssue']");
            if (xnr != null) xnr.InnerText = DateOfIssue.ToString("dd-MM-yyyy").Replace("-", ".").Replace("/", ".");

            //xnr = xTemplate.SelectSingleNode("/Coupons/Coupon[@name='Pass']/Element[@name='TxtPassPageNo']");
            //if (xnr != null) xnr.InnerText = "PAGE " + CounterPass + "/" + TotalPass;
            #endregion

            return xTemplate;
        }
        catch (Exception ex)
        {
            ShowMessage(2, "Interrail XML Error.");
            return null;
        }
    }

    protected void btnProblem_Click(object sender, EventArgs e)
    {
        try
        {
            PrintingView.Visible = false;
            rangeAssign.Visible = true;
            UpdateOrderStatusByStatus(false, 3);//3:Approved;
            if (ViewState["FileURL"] != null)
            {
                try
                {
                    if (File.Exists(ViewState["FileURL"].ToString()))
                        File.Delete(ViewState["FileURL"].ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Printing File in used.");
                }
            }
            List<long> stockno = new List<long>();
            foreach (RepeaterItem row in rptPrint.Items)
            {
                CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
                Label stkNo = row.FindControl("lblStockNo") as Label;
                Int64 stNo = Convert.ToInt32(stkNo.Text);
                stockno.Add(stNo);
            }
            foreach (var stno in stockno)
                _oPrint.UpdateInterrailStockNo(stno, 5, AgentuserInfo.UserID);//void
            CleareprintingQueue(4);//4.Queued tblprintqueueitems update

            btnNotVoidOk.Visible = true;
            btnProblem.Visible = btnPrintedOK.Visible = btnPrint.Visible = btnCancel.Visible = false;

            lblHeader.Text = "Void Stock";
            Msgtxt.InnerText = "Please select (tick) the stock numbers that are still usable/undamaged and press 'OK' to attempt printing again on the same stock. If the stock is unusable and cannot be used again please untick all of the stock numbers on that sheet and click 'OK' to void it.";
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Showprinting", "Showprinting()", true);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void UpdateOrderStatusByStatus(bool CaseValid, int status)
    {
        try
        {//1:Order Created;3:Approved;6:Cancelled;7:Completed;8:Refund Pending;9:Refund Complete;19:Confirm;20:Partial refund
            foreach (RepeaterItem row in rptPrint.Items)
            {
                HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
                if (CaseValid)
                {
                    if (MBooking.AllPassPrintedForThisOrder(Convert.ToInt64(hdnOrderNo.Value)))
                        MBooking.UpdateOrderStatus(status, Convert.ToInt64(hdnOrderNo.Value));
                }
                else
                    MBooking.UpdateOrderStatus(status, Convert.ToInt64(hdnOrderNo.Value));
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        List<long> stockno = new List<long>();
        foreach (RepeaterItem row in rptPrint.Items)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
        }
        foreach (var stno in stockno)
            _oPrint.UpdateInterrailStockNoReuse(stno);
        CleareprintingQueue(4);//4.Queued tblprintqueueitems update
        Response.Redirect("PrintingOrders");
    }

    void UpdateInterrailStockNumberStatus(int status)
    {
        foreach (RepeaterItem row in rptPrint.Items)
        {
            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;
            HiddenField hdnOrderNo = row.FindControl("hdnOrderNo") as HiddenField;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int32 stNo = Convert.ToInt32(stkNo.Text);
            Int32 OrderNo = Convert.ToInt32(hdnOrderNo.Value);
            if (OrderNo == 0 && status != 3)//void
                _oPrint.UpdateInterrailStockNo(stNo, 5, userid);
            else
                _oPrint.UpdateInterrailStockNo(stNo, status, userid);
        }
    }

    protected void btnNotVoidOk_Click(object sender, EventArgs e)
    {
        bool thisisVoid = false;
        List<long> stockno = new List<long>();
        foreach (RepeaterItem row in rptPrint.Items)
        {
            CheckBox chkunused = row.FindControl("chkPrint") as CheckBox;
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            stockno.Add(stNo);
            if (!chkunused.Checked)
                thisisVoid = true;
        }
        if (thisisVoid)
        {
            foreach (var stno in stockno)
                _oPrint.UpdateInterrailStockNo(stno, 5, AgentuserInfo.UserID);
        }
        else
        {
            foreach (var stno in stockno)
            {
                //_oPrint.DeleteEurailStockNo(stno);
                _oPrint.UpdateInterrailStockNoReuse(stno);
            }
        }
        CleareprintingQueue(4);//4.Queued tblprintqueueitems update
        Response.Redirect("PrintingOrders");
    }

    protected void btnPrintedOK_Click(object sender, EventArgs e)
    {
        string URlPath = string.Empty;
        if (ViewState["url"] != null)
            URlPath = ViewState["url"].ToString();
        foreach (RepeaterItem row in rptPrint.Items)
        {
            Label stkNo = row.FindControl("lblStockNo") as Label;
            Int64 stNo = Convert.ToInt32(stkNo.Text);
            _oPrint.UpdatePrintingURLInterrail(stNo, URlPath);
            break;
        }
        Response.Redirect("PrintingOrders");
    }

    public void CleareprintingQueue(int STATUS)
    {
        List<Guid> ids = new List<Guid>();
        foreach (RepeaterItem row in rptPrint.Items)
        {
            HiddenField hdnID = row.FindControl("hdnPQItemID") as HiddenField;
            Guid id = Guid.Parse(hdnID.Value);
            ids.Add(id);
        }
        foreach (var id in ids)
            _oPrint.UpdatePrintQConfirmedStatus(id, STATUS);
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }

    protected void rptPrint_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HtmlGenericControl divrpthide = e.Item.FindControl("divrpthide") as HtmlGenericControl;
                Label lblHideClass = e.Item.FindControl("lblHideClass") as Label;
                if (!string.IsNullOrEmpty(lblHideClass.Text))
                {
                    lblHideClass.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
                    divrpthide.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
                }
            }
        }
    }

    struct Passenger
    {
        public string Name;
        public string Country;
        public string PassportNumber;
        public DateTime? DOB;
        public DateTime? PassStartDate;
    }

}