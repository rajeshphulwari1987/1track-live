﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Web;
using System.Collections.Generic;

public partial class Printing_PrintingOrders : Page
{
    private readonly ManagePrintQueue oPrint = new ManagePrintQueue();
    readonly private ManageProduct _oProduct = new ManageProduct();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private Guid _siteId;
    public string siteURL;
    public string script = "<script></script>";

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                _siteId = Guid.Parse(Session["siteId"].ToString());
                siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                BindDropDown(_siteId);
                QubitOperationLoad();
            }
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void QubitOperationLoad()
    {
        try
        {
            List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
            var res = lstQbit.FirstOrDefault();
            if (res != null)
                script = res.Script;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void BindDropDown(Guid _siteId)
    {
        try
        {
            ddlCategory.DataSource = _oProduct.GetAssociateCategoryList(1, _siteId).Where(x => x.IsPrintQueueEnable == true || x.IsBritrail == true || x.IsInterRail == true).ToList().OrderBy(x => x.Name);
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();

            Guid userid = AdminuserInfo.UserID;
            if (AgentuserInfo.UserID != new Guid())
                userid = AgentuserInfo.UserID;
            var stockList = oPrint.GetStockList(userid);
            ddlStock.DataSource = stockList;
            ddlStock.DataTextField = "Name";
            ddlStock.DataValueField = "ID";
            ddlStock.DataBind();
            ddlStock.Items.Insert(0, new ListItem("Print queue name", "-1"));

            ddlPrntQ.DataSource = stockList;
            ddlPrntQ.DataTextField = "Name";
            ddlPrntQ.DataValueField = "ID";
            ddlPrntQ.DataBind();
            ddlPrntQ.Items.Insert(0, new ListItem("Print queue name", "-1"));
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            GetCategoriesId();
            List<CategoryIds> CatID = new List<CategoryIds>();
            if (ddlCategory.SelectedValue.Length > 30)
                CatID.Add(new CategoryIds { Id = Guid.Parse(ddlCategory.SelectedValue) });
            else
                CatID = Session["lisCategoryIds"] as List<CategoryIds>;
            int result = oPrint.CheckBritrailPassAny(CatID);
            /* Test Account*/
            if (result == 1)//Britrial
                Response.Redirect("BritrailPrintingAssigned.aspx?qId=" + ddlStock.SelectedValue);
            else if (result == 2)//Interrail
                Response.Redirect("InterrailPrintingAssigned.aspx?qId=" + ddlStock.SelectedValue);
            else//Eurail
                Response.Redirect("PrintingAssigned?qId=" + ddlStock.SelectedValue);
            //Response.Redirect("testInterrailPrintingAssigned.aspx?qId=" + ddlStock.SelectedValue);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void GetCategoriesId()
    {
        try
        {
            List<CategoryIds> lisCategoryIds = new List<CategoryIds>();
            if (rptPrintQueue.Items.Count > 0)
            {
                foreach (RepeaterItem item in rptPrintQueue.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        HiddenField hdnCategoryID = item.FindControl("hdnCategoryID") as HiddenField;
                        HiddenField hdnID = item.FindControl("hdnID") as HiddenField;
                        Guid Id = Guid.Parse(hdnID.Value);
                        oPrint.UpdatePrintQPandingStatus(Id);
                        Guid id = Guid.Parse(hdnCategoryID.Value);
                        lisCategoryIds.Add(new CategoryIds { Id = id });
                    }
                }
            }
            else
            {
                var list = _oProduct.GetAssociateCategoryList(3, _siteId).Where(x => x.IsPrintQueueEnable == true).ToList().OrderBy(x => x.Name);
                foreach (var item in list)
                    lisCategoryIds.Add(new CategoryIds { Id = item.ID });
            }
            Session["lisCategoryIds"] = lisCategoryIds.ToList();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlStock_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlStock_SelectedIndexChanged(sender, e);
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    void BindGrid()
    {
        try
        {
            if (ddlStock.SelectedValue == "-1" || ddlCategory.SelectedValue == "-1")
                return;
            Guid queueID = Guid.Parse(ddlStock.SelectedValue);
            Guid catID = Guid.Parse(ddlCategory.SelectedValue);
            rptPrintQueue.DataSource = oPrint.GetPrintQList(queueID, catID, _siteId, AgentuserInfo.UserID);
            rptPrintQueue.DataBind();
            if (rptPrintQueue.Items.Count > 0)
                btnPrint.Visible = dvPrd.Visible = true;
            else
                btnPrint.Visible = dvPrd.Visible = false;
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            Guid id = Guid.Parse(hdnID.Value);
            Guid stockQueueID = Guid.Parse(ddlPrntQ.SelectedValue);
            oPrint.MovePrintQItem(id, stockQueueID);
            BindGrid();
        }
        catch (Exception ex)
        {
            ClsErrorLog.AddError(HttpContext.Current.Request.Url.AbsoluteUri, ex.Message + "; Inner Exception:" + (ex.InnerException != null ? ex.InnerException.Message : "null"));
        }
    }

    protected void rptPrintQueue_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            var prntQid = Guid.Parse(e.CommandArgument.ToString());
            if (e.CommandName.Contains("QueueStatus"))
            {
                string[] str = e.CommandName.Split('ñ');
                oPrint.UpdatePrintQStatus(prntQid, Convert.ToInt64(str[1]));
            }
            if (e.CommandName.Contains("Remove"))
            {
                string[] str = e.CommandName.Split('ñ');
                oPrint.DeletePrintQItem(prntQid, Convert.ToInt64(str[1]));
            }
            if (e.CommandName == "Move")
            {
                mdpupPrntQ.Show();
                hdnID.Value = prntQid.ToString();
            }
            BindGrid();
        }
        catch (Exception ex)
        {
            ShowMessage(2, ex.Message);
        }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}


