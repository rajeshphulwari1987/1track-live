﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PrintingOrders.aspx.cs" Inherits="Printing_PrintingOrders" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
    <style type="text/css">
        .starail-Form-row
        {
            margin-bottom: 12px !important;
        }
        .marginprint
        {
            margin-left: 72px;
        }
        .starail-BookingDetails-titleAndButton
        {
            margin-bottom: 0.129rem;
            margin-top: 1.125em;
        }
        .p-margintop
        {
            margin-top: 6px;
            margin-left: 10px;
        }
        .p-margintopall
        {
            margin-top: -9px;
            margin-left: 10px;
        }
        .showorder
        {
            display: block !important;
        }
        #MainContent_pnlQuckLoad
        {
            z-index: 101 !important;
            position: absolute !important;
            width: 50%;
        }
        .starail-YourBooking
        {
            margin-bottom: 0px;
            padding: 0px;
            box-shadow: 0px;
        }
        #MainContent_mdpupPrntQ_backgroundElement
        {
            z-index: 100 !important;
        }
        .starail-Form-inputContainer-col
        {
            width: 100% !important;
        }
        .labelwidth
        {
            width: 36% !important;
        }
        .selectlist
        {
            width: 100% !important;
        }
        .starail-Form-button
        {
            width: 50% !important;
        }
        .starail-BookingDetails-form
        {
            padding: 25px !important;
        }
        .selectlistmargintwo
        {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        @media only screen and (max-width: 800px)
        {
            #MainContent_pnlQuckLoad
            {
                width: 93% !important;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <asp:HiddenField ID="hdnID" runat="server" />
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Form--paymentConfirmation">
                <div class="starail-BookingDetails-titleAndButton">
                    <h1 class="starail-BookingDetails-title">
                        Print Queue - Assignment</h1>
                    <p class="p-margintop">
                        Welcome to Print Queue.</p>
                    <p class="p-margintopall">
                        Click on the word "Queued" to assign an order(s) to your printing. Then click "print"
                        when you're ready to check the ticket stock.</p>
                </div>
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="starail-BookingDetails-form starail-BookingDetails-form--payAccount">
                    <h2>
                        Print Queue</h2>
                    <div class="starail-Form-row">
                        <label for="starail-ref" class="starail-Form-label">
                            Category
                        </label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div>
                                <asp:DropDownList ID="ddlCategory" class="starail-Form-select" runat="server" ValidationGroup="rv"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label for="starail-ref" class="starail-Form-label">
                            Stock Queue
                        </label>
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div>
                                <asp:DropDownList ID="ddlStock" class="starail-Form-select" runat="server" ValidationGroup="rv"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlStock_SelectedIndexChanged" />
                                <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ValidationGroup="st"
                                    ControlToValidate="ddlStock" InitialValue="-1" Display="Dynamic" />
                            </div>
                        </div>
                    </div>
                    <div id="dvPrd" runat="server" visible="false">
                        <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                            <div style="width: 11%;">
                                Order
                            </div>
                            <div style="width: 21%;">
                                Lead Passenger
                            </div>
                            <div style="width: 50%;">
                                Product
                            </div>
                            <div>
                                Status
                            </div>
                        </div>
                        <asp:Repeater ID="rptPrintQueue" runat="server" OnItemCommand="rptPrintQueue_ItemCommand">
                            <ItemTemplate>
                                <div class="starail-YourBooking-ticketDetails">
                                    <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                        <span class="starail-u-hideDesktop">Order </span>
                                        <%#Eval("OrderNo")%>
                                        <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryID")%>' />
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Eval("ID")%>' />
                                    </div>
                                    <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                        <span class="starail-u-hideDesktop">Lead Passenger </span>
                                        <%#Eval("LeadPassenger")%>
                                    </div>
                                    <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                        <span class="starail-u-hideDesktop">Product Name </span>
                                        <%#Eval("ProductName")%>
                                    </div>
                                    <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth" style="font-size: 13px">
                                        <span class="starail-u-hideDesktop">Status </span>
                                        <asp:LinkButton ID="lnkStatus" runat="server" Text='<%#Eval("Status")%>' CommandArgument='<%#Eval("ID")%>'
                                            CommandName='<%#"QueueStatusñ"+Eval("OrderNo")%>' />
                                        <asp:LinkButton ID="lnkMove" runat="server" Text="Move" CommandArgument='<%#Eval("ID")%>'
                                            CommandName="Move" />
                                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandArgument='<%#Eval("ID")%>'
                                            CommandName='<%#"Removeñ"+Eval("OrderNo")%>' OnClientClick="return confirm('Are you sure? Do you want to delete this item?')" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="starail-Form-row" style="margin-top: 10px">
                        <div class="starail-Form-inputContainer starail-Form-inputContainer--inputGrid">
                            <div class="marginprint">
                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit ButtonMargin"
                                    Visible="false" Width="100px" OnClick="btnPrint_Click" ValidationGroup="st" />
                            </div>
                        </div>
                    </div>
                </div>
                <div style="display: none">
                    <a id="lnkSentToQueue" runat="server" href="#"></a>
                </div>
            </div>
        </div>
    </div>
    <asp:ModalPopupExtender ID="mdpupPrntQ" runat="server" CancelControlID="btnClose"
        PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="lnkSentToQueue" />
    <asp:Panel ID="pnlQuckLoad" runat="server">
        <div class="starail-popup">
            <div class="starail-BookingDetails-form">
                <h2>
                    Send to Queue</h2>
                <div class="starail-Form-row selectlistmargintwo">
                    <label class="starail-Form-label" for="starail-country">
                        Stock Queue <span class="starail-Form-required">*</span></label>
                        <div class="clear"></div>
                    <div class="starail-Form-inputContainer selectlistwidth">
                        <div class="starail-Form-inputContainer-col">
                            <asp:DropDownList ID="ddlPrntQ" runat="server" ValidationGroup="vp" CssClass="starail-Form-select"
                                TabIndex="97" />
                            <asp:RequiredFieldValidator ID="reqvalerror0" ControlToValidate="ddlPrntQ" InitialValue="-1"
                                ValidationGroup="vp" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="starail-Form-row selectlistmargin" style="text-align: center;">
                    <asp:Button ID="btnSend" runat="server" CssClass="starail-Button starail-Form-button"
                        TabIndex="98" Text="Send" ValidationGroup="vp" OnClick="btnSend_Click" />
                </div>
                <div class="starail-Form-row  selectlistmargin" style="text-align: center;">
                    <asp:LinkButton ID="btnClose" runat="server" Text="Cancel"></asp:LinkButton></div>
            </div>
    </asp:Panel>
</asp:Content>
