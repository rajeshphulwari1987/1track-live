﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PrintingAssigned.aspx.cs" Inherits="Printing_PrintingAssigned" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src='../Scripts/jquery-1.11.3.min.js'></script>
    <%=script%>
    <style type="text/css">
        .starail-YourBooking
        {
            padding: 0px !important;
        }
        input[type="submit"]
        {
            margin-left: 7px;
        }
        .starail-BookingDetails-titleAndButton
        {
            margin-bottom: 0.129rem;
            margin-top: 1.125em;
        }
        .p-margintop
        {
            margin-top: 6px;
            margin-left: 10px;
        }
        @media only screen and (max-width: 639px)
        {
            .starail-Button--rightSubmit
            {
                margin: 0px !important;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/StationList.asmx" />
        </Services>
    </asp:ToolkitScriptManager>
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Form--paymentConfirmation">
                <div class="starail-BookingDetails-titleAndButton">
                    <h1 class="starail-BookingDetails-title">
                        Print Queue -
                        <asp:Label ID="lblHeader" ClientIDMode="Static" runat="server" Text="Printing"></asp:Label></h1>
                    <p id="Msgtxt" runat="server" clientidmode="Static" class="p-margintop">
                        Please check the stock allocations below. If the stock allocation is incorrect please
                        contact your administrator.</p>
                </div>
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="starail-BookingDetails-form starail-BookingDetails-form--payAccount">
                    <div id="rangeAssign" runat="server">
                        <h2>
                            Print Queue Assignment</h2>
                        <div class="starail-Form-row">
                            <div class="starail-YourBooking-col--mobileFullWidt">
                                <label for="starail-ref" class="starail-Form-label">
                                    Range From :</label>
                                <div class="starail-Form-label">
                                    <asp:Label ID="lblRangeFrm" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-Form-row">
                            <div class="starail-YourBooking-col--mobileFullWidt">
                                <label for="starail-ref" class="starail-Form-label">
                                    Range To:</label>
                                <div class="starail-Form-label">
                                    <asp:Label ID="lblRangeTo" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="starail-u-hideDesktop">
                            <br />
                        </div>
                    </div>
                    <div id="PrintingView" visible="false" runat="server" style="border: 1px solid #ccc;
                        padding: 3px; margin: 20px 0;">
                        <iframe id="Printingiframe" runat="server" height="500px" width="100%"></iframe>
                    </div>
                    <asp:UpdatePanel ID="up1" runat="server">
                        <ContentTemplate>
                            <div class="starail-YourBooking" id="dvPrd" runat="server" style="margin-top: 50px;"
                                visible="false">
                                <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                                    <div style="width: 15%;">
                                        Order
                                    </div>
                                    <div style="width: 25%;">
                                        Lead Passenger
                                    </div>
                                    <div style="width: 25%;">
                                        Product
                                    </div>
                                    <div style="width: 25%;">
                                        Stock No.
                                    </div>
                                    <div class="Showprinting" style="width: 10%;">
                                        Usable
                                    </div>
                                </div>
                                <asp:Repeater ID="rptPrint" runat="server" OnItemDataBound="rptPrint_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--open" runat="server" id="divrpthide">
                                            <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                                <span class="starail-u-hideDesktop">Order </span>
                                                <%#Eval("ProductName").ToString() != "VOID" ? Eval("OrderNo") : ""%>
                                                <asp:HiddenField ID="hdnSaver" runat="server" Value='<%#Eval("SaverCode")%>' />
                                                <asp:HiddenField ID="hdnPQItemID" runat="server" Value='<%#Eval("ID")%>' />
                                                <asp:HiddenField ID="hdnCategoryID" runat="server" Value='<%#Eval("CategoryID")%>' />
                                                <asp:HiddenField ID="hdnOrderNo" runat="server" Value='<%#Eval("ProductName").ToString()!="VOID" ? Eval("OrderNo") : "0"%>' />
                                            </div>
                                            <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                                <span class="starail-u-hideDesktop">Lead Passenger </span>
                                                <%#Eval("LeadPassenger")%>
                                            </div>
                                            <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                                <span class="starail-u-hideDesktop">Product Name </span>
                                                <%#Eval("ProductName")%>
                                            </div>
                                            <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                                <span class="starail-u-hideDesktop">Stock Number </span>
                                                <asp:Label ID="lblStockNo" runat="server" Text='<%#Eval("StockNo")%>' />
                                                <asp:Label ID="lblHideClass" runat="server" Text='<%#Eval("HideClass") %>' Style="display: none;" />
                                            </div>
                                            <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth Showprinting">
                                                <span class="starail-u-hideDesktop">Usable </span>
                                                <asp:CheckBox ID="chkPrint" runat="server" Checked="true" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="starail-BookingDetails-submit starail-u-cf">
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit ButtonMargin"
                    OnClick="btnPrint_Click" />
                <div class="starail-u-hideDesktop">
                    <br />
                </div>
                <asp:Button ID="btnCancel" ClientIDMode="Static" runat="server" Text="Cancel" CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit"
                    OnClick="btnCancel_Click" />
                <asp:Button ID="btnPrintedOK" runat="server" Text="All passes printed OK" ClientIDMode="Static"
                    CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit ButtonMargin"
                    OnClick="btnPrintedOK_Click" />
                <div class="starail-u-hideDesktop">
                    <br />
                </div>
                <asp:Button ID="btnProblem" runat="server" Text="There was a problem" ClientIDMode="Static"
                    CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit ButtonMargin"
                    OnClick="btnProblem_Click" />
                <asp:Button ID="btnNotVoidOk" runat="server" Text="OK" ClientIDMode="Static" CssClass="starail-Button starail-Button--fullMobile  starail-Button--rightSubmit ButtonMargin"
                    OnClick="btnNotVoidOk_Click" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.Showprinting').hide();
        function Showprinting() {
            $('.Showprinting').show();
        }
    </script>
</asp:Content>
