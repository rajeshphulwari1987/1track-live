﻿using System;
using System.Web.UI;
using Business;
using System.Linq;
using System.Web;
using System.Configuration;

public partial class IRSiteMap : Page
{
    readonly Masters _master = new Masters();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    private Guid _siteId;
    public string script;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            PageBanner(_siteId);
        }
    }

    public void PageBanner(Guid siteID)
    {
        try
        {
            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
            {
                var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pid.ID && x.SiteID == siteID);
                if (result != null)
                {
                    var url = result.Url;
                    var oPage = _master.GetPageDetailsByUrl(url);

                    //Banner
                    string[] arrListId = oPage.BannerIDs.Split(',');
                    var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                    var list = _master.GetBannerImgByID(idList);
                    int countdata = list.Count();
                    int index = new Random().Next(countdata);
                    var newlist = list.Skip(index).FirstOrDefault();
                    if (newlist != null && newlist.ImgUrl != null)
                        img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                    else
                        img_Banner.Visible = false;
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }
}