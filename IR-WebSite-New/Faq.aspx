﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Faq.aspx.cs" Inherits="Faq" %>

<%@ Register TagPrefix="uc" TagName="ucRightContent" Src="UserControls/ucRightContent.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
 <br />
            <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top:5px">
        <h2>
            FAQ</h2>
        <%=Server.HtmlDecode(FaqDescription)%>
        <div class="starail-Section starail-Section--nopadding">
            <ul class="starail-Accordion js-accordon">
                <asp:Repeater ID="rptFaq" runat="server">
                    <ItemTemplate>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                                <div class="starail-Accordion-header js-accordonHeader">
                                    <h4 class="starail-Accordion-title">
                                        <%# Container.ItemIndex + 1 %>.
                                        <%#Eval("Question") %></h4>
                                    <button class="starail-Accordion-carat js-accordonButton">
                                        <i class="starail-Icon-chevron-down"></i>
                                    </button>
                                </div>
                                <div class="starail-Accordion-body">
                                    <div class="starail-Accordion-bodyContent">
                                        <p>
                                            <%#Eval("Answer") %></p>
                                    </div>
                                </div>
                            </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Content>
