﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PaymentProcess.aspx.cs" Inherits="PaymentProcess" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Order/PaymentConfirmation.ascx" TagName="PaymentConfirmation" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%=script%>
    <%=ChkOutActionsTags%>
    <%=ChkOutSetActionsTags%>
    <script type="text/javascript">
        function GoDetails() {
            window.history.back();
        }
        function GoOrderSuccessPage() {
            $("#PaymentFrame").contents().find("#submitBtn").trigger('click');
        }
        function reloadifram() {
            document.getElementById("PaymentFrame").contentDocument.location.reload(true);
        }
        $(document).ready(function () {
            $('#span_hover').mouseenter(function () {
                $('#MainContent_divChargesPopup').show();
            })
             .mouseleave(function () {
                 $('#MainContent_divChargesPopup').hide();
             });
        });
    </script>
    <style type="text/css">
        .starail-BookingDetails-title {
            display: inline-block;
        }

        .timertopcls {
            display: inline-block;
            float: right;
            margin-top: 4px;
            padding: 4px 6px;
            border-radius: 7px;
            font-size: 12px;
            top: 49% !important;
            right: 15.5% !important;
            position: fixed;
            z-index: 100001;
            background: #fff;
            text-align: center;
            margin-right: -120px;
            box-shadow: 2px 4px 4px #d4d4d4;
            text-shadow: 1px 2px 3px #d4d4d4;
        }

            .timertopcls a {
                font-weight: bold;
                text-decoration: underline;
            }

        @media (max-width: 767px) {
            .starail-BookingDetails-title {
                display: block;
                margin-bottom: 10px;
            }

            .timertopcls {
                display: block;
                margin: 0 15px 10px 15px;
                float: left;
                right: -4% !important;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:HiddenField runat="server" ID="hdnProductPrice" />
    <asp:HiddenField ID="hdnIsTI" runat="server" Value="False" ClientIDMode="Static" />
    <div class="starail-Grid starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-ProgressBar starail-ProgressBar-stage--stage3 starail-u-hideMobile">
                <div class="starail-ProgressBar-line">
                    <div class="starail-ProgressBar-progress">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Ticket Selection
                    </p>
                    <div class="starail-ProgressBar-subStage">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Booking Details
                    </p>
                    <div class="starail-ProgressBar-subStage">
                    </div>
                </div>
                <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                    <div class="starail-ProgressBar-circle">
                    </div>
                    <p class="starail-ProgressBar-label">
                        Checkout
                    </p>
                </div>
            </div>
            <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                <div class="">
                    <a class="starail-Button starail-Button--blue" onclick="GoDetails();"><i class="starail-Icon-chevron-left"></i>Details</a> <a class="starail-Button starail-Button--blue starail-done-trigger"
                        onclick="GoOrderSuccessPage();">Done! <i class="starail-Icon-chevron-right"></i>
                    </a>
                </div>
            </div>
            <h1 class="starail-BookingDetails-title">Nearly there!
                <br />
                Review your booking and enter your billing info:</h1>
            <div class="timertopcls" style='display: none;'>
                <div class='timercls'></div>
                <a href='<%=siteURL %>' class="timerredirectcls" style='display: none;'>Click here</a>
            </div>
            <!-- Passes -->
            <asp:Panel ID="pnlErrSuccess" runat="server">
                <div id="DivSuccess" runat="server" class="success" style="display: none;">
                    <asp:Label ID="lblSuccessMsg" runat="server" />
                </div>
                <div id="DivError" runat="server" class="error" style="display: none;">
                    <asp:Label ID="lblErrorMsg2" runat="server" />
                </div>
            </asp:Panel>
            <asp:UpdatePanel ID="Upnl" runat="server">
                <ContentTemplate>
                    <div class="starail-YourBooking">
                        <h2>Your Booking</h2>
                        <div class="starail-YourBooking-table">
                            <div class="starail-YourBooking-ticketDetails" style="display: none;">
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Order Number:
                                </div>
                                <div class="starail-YourBooking-col--mobileFullWidth">
                                    <asp:Label runat="server" ID="lblOrderNumber"></asp:Label>
                                </div>
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Order Date:
                                </div>
                                <div class="starail-YourBooking-col--mobileFullWidth">
                                    <asp:Label runat="server" ID="lblOrderDate"></asp:Label>
                                </div>
                            </div>
                            <div class="starail-YourBooking-ticketDetails" style="display: none;">
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Email Address:
                                </div>
                                <div class="starail-YourBooking-col--mobileFullWidth">
                                    <asp:Label runat="server" ID="lblEmailAddress"></asp:Label>
                                </div>
                                <div class="verticaltop starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Delivery Address:
                                </div>
                                <div class="verticaltop starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <asp:Label runat="server" ID="lblDeliveryAddress"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="starail-u-hideMobile">
                            <br />
                        </div>
                        <div class="starail-YourBooking-table" runat="server" id="DivPassSale">
                            <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                                <div>
                                    Pass
                                </div>
                                <div>
                                    Countries
                                </div>
                                <div>
                                    Validity
                                </div>
                                <div class="starail-YourBooking-col-center">
                                    Protected
                                </div>
                                <div class="starail-YourBooking-col">
                                    Passenger information
                                </div>
                                <div class="starail-YourBooking-col-price">
                                    Price
                                </div>
                            </div>
                            <asp:Repeater ID="RptPassDetails" runat="server">
                                <ItemTemplate>
                                    <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--closed">
                                        <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                            <a class="" href="#">
                                                <%#Eval("ProductDesc")%>:
                                                <br />
                                                <span class="starail-YourBooking-light">
                                                    <%#Eval("Traveller")%></span> <i class="starail-Icon-chevron-up"></i></a>
                                        </div>
                                        <div class="starail-u-hideMobile">
                                            <p class="starail-YourBooking-title">
                                                <%#Eval("ProductDesc")%>
                                            </p>
                                            <p>
                                                <%#Eval("Traveller")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                            <p class="starail-u-hideDesktop">
                                                Countries
                                            </p>
                                            <p>
                                                <%#Eval("Countries")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col  starail-YourBooking-col--mobileFullWidth">
                                            <p class="starail-u-hideDesktop">
                                                Validity:
                                            </p>
                                            <p>
                                                <%#Eval("Validity")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col starail-YourBooking-col-center">
                                            <p>
                                                <span class="starail-u-hideDesktop">Protected: </span>
                                                <%#Eval("Protected").ToString() == "Yes" ? "<i class='starail-Icon-shield'></i>": ""%>
                                                <%#Eval("ProtectedAmount")%>
                                                <span class="starail-u-visuallyHiddenDesktop">
                                                    <%#Eval("Protected")%></span>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                            <p>
                                                <span class="starail-u-hideDesktop">Passenger information: </span><span class="starail-u-hideMobile">Passenger (<%#(Eval("Arrives").ToString()==""?"1 ":"")+Eval("Trvname")%>):
                                                    <br />
                                                </span>
                                                <%#Eval("Passengerinfo")%><span class="starail-u-hideDesktopInline">(<%#Eval("Trvname")%>)</span>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col-price">
                                            <p>
                                                <%=currency%><%#Eval("Price")%>
                                            </p>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="starail-YourBooking-table" runat="server" id="DivP2PSale">
                            <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--header starail-u-hideMobile">
                                <div>
                                    Train
                                </div>
                                <div>
                                    Departs
                                </div>
                                <div>
                                    Arrives
                                </div>
                                <div class="starail-YourBooking-col-center">
                                    Class
                                </div>
                                <div class="">
                                    Passenger information
                                </div>
                                <div class="starail-YourBooking-col-price">
                                    Price
                                </div>
                            </div>
                            <asp:Repeater ID="RptP2PDetails" runat="server">
                                <ItemTemplate>
                                    <div class="starail-YourBooking-ticketDetails starail-YourBooking-ticketDetails--closed">
                                        <div class="starail-YourBooking-mobileTrigger starail-u-hideDesktop">
                                            <a class="" href="#"><span class="starail-YourBooking-light">
                                                <%#Eval("Jolurny")%>
                                            </span>
                                                <%# isEvolviBooking.ToString() == "False" ? Eval("Train") : string.Empty%><i class="starail-Icon-chevron-up"></i>
                                            </a>
                                        </div>
                                        <div class="starail-u-hideMobile">
                                            <p class="starail-YourBooking-title">
                                                <%#Eval("Jolurny")%>
                                            </p>
                                            <p>
                                                <%# isEvolviBooking.ToString() == "False" ? Eval("Train") : string.Empty%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col">
                                            <p class="starail-u-hideDesktop">
                                                Departs:
                                            </p>
                                            <p>
                                                <%#Eval("Departs")%>
                                            </p>
                                            <p>
                                                <%#Eval("DepartsStation")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col">
                                            <p class="starail-u-hideDesktop">
                                                Arrives:
                                            </p>
                                            <p>
                                                <%#Eval("Arrives")%>
                                            </p>
                                            <p>
                                                <%#Eval("ArrivesStation")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col-center">
                                            <p>
                                                <span class="starail-u-hideDesktop">Class: </span>
                                                <%#Eval("Class")%>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                            <p>
                                                <span class="starail-u-hideDesktop">Passenger information: </span><span class="starail-u-hideMobile">
                                                    <%#Eval("Trvname")%></span>
                                                <br />
                                                <%#Eval("Passengerinfo")%>
                                                <span class="starail-u-hideDesktopInline">(<%#Eval("Trvname")%>)</span>
                                            </p>
                                        </div>
                                        <div class="starail-YourBooking-col-price">
                                            <p>
                                                <%=currency%><%#Eval("Price")%>
                                            </p>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="starail-u-hideMobile">
                            <br />
                        </div>
                        <div class="starail-YourBooking-table">
                            <div class="starail-YourBooking-ticketDetails" id="div_BookingShippingFee" runat="server">
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Booking Fee:
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <%=currency%><asp:Label runat="server" ID="lblBookingFee"></asp:Label>
                                </div>
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Shipping Amount:
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <%=currency%><asp:Label runat="server" ID="lblShippingAmount"></asp:Label>
                                </div>
                            </div>
                            <div class="starail-YourBooking-ticketDetails" id="div_Discount_AdminFee" runat="server">
                                <div id="OrderDiscount1" runat="server" class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Discount Amount:
                                </div>
                                <div id="OrderDiscount2" runat="server" class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <%=currency%><asp:Label runat="server" ID="lblDiscount"></asp:Label>
                                </div>
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    <div runat="server" id="hidelblAdminfee">
                                        Admin Fee:
                                    </div>
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <div runat="server" id="hidetxtAdminfee">
                                        <%=currency%><asp:Label runat="server" ID="lblAdminFee"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="starail-YourBooking-ticketDetails">
                                <div class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth"
                                    id="hideTicketText" runat="server" visible="false">
                                    Other Charges
                                </div>
                                <div class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth" id="hideTicketValue"
                                    runat="server" visible="false">
                                    <%=currency%><asp:Label ID="lblTicketCharge" runat="server" Text="0"></asp:Label><span
                                        id="span_hover"></span>
                                </div>
                                <div runat="server" id="hidelblnettotal" class="starail-YourBooking-ticketDetails--header starail-YourBooking-col--mobileFullWidth">
                                    Net Total:
                                </div>
                                <div runat="server" id="hidetxtnettotal" class="starail-YourBooking-col starail-YourBooking-col--mobileFullWidth">
                                    <%=currency%><asp:Label runat="server" ID="lblNetTotal"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <p class="starail-YourBooking-totalPrice">
                            Total Price for
                            <asp:Label ID="lbltxttraveller" runat="server"></asp:Label>: <span class="starail-YourBooking-totalPrice-amount">
                                <%=currency%></span><asp:Label CssClass="starail-YourBooking-totalPrice-amount" ID="lblGrandTotal"
                                    runat="server"></asp:Label>
                        </p>
                    </div>
                    <div class="starail-Form--promoCode" runat="server" id="OrderDiscount">
                        <div class="starail-BookingDetails-form">
                            <h2>Got a Promotional Code?</h2>
                            <div class="starail-Form-row">
                                <label for="starail-promo" class="starail-Form-label">
                                    Enter your code</label>
                                <div class="starail-Form-inputContainer ">
                                    <asp:TextBox runat="server" ID="txtDiscountCode" class="starail-Form-input starail-Form-input--medium"
                                        MaxLength="50"></asp:TextBox>
                                    <asp:Button CssClass="starail-Button starail-Button--blue" ID="BtnDiscount" runat="server"
                                        ValidationGroup="pr" Text=" Apply promo code" OnClick="BtnDiscount_Click" />
                                    <asp:RequiredFieldValidator ID="reqPromo" runat="server" ValidationGroup="pr" Display="Dynamic"
                                        CssClass="starail-Form-required starail-Form--promoCode-note" ErrorMessage="Please enter promo code."
                                        ControlToValidate="txtDiscountCode" />
                                    <asp:Label ID="lblErrormsg" Visible="true" runat="server" CssClass="starail-Form-required starail-Form--promoCode-note"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <p class="starail-BookingDetails-totalPrice starail-YourBooking-totalPrice">
                            Your <span class="starail-YourBooking-totalPrice-highlight">new</span> Total Price
                            for
                            <asp:Label ID="lblDiscountTotalTxt" runat="server"></asp:Label>: <span class="starail-YourBooking-totalPrice-amount">
                                <%=currency%></span><asp:Label CssClass="starail-YourBooking-totalPrice-amount" ID="lblDiscountSum"
                                    runat="server"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <uc1:PaymentConfirmation ID="PaymentDiv" runat="server" />
            <asp:Button ID="btnWorldPay" runat="server" Text="Proceed To Payment" OnClick="btnWorldPay_Click"
                CssClass="starail-Button starail-Button--fullMobil starail-Button--rightSubmit"
                Visible="false" />
            <div id="DivButton" runat="server">
                <asp:Button ID="btnContinue" CssClass="starail-Button starail-Button--fullMobil starail-Button--rightSubmit"
                    runat="server" OnClick="btnContinue_Click" Text="Proceed to payment" ToolTip="You will now be re-directed to our Payment Gateway, Ogone to pay for your order by credit or debit card. Once completed you will automatically return to our site to receive your order confirmation." />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on('eventhandler', function () {
            ShowTermAndCondition();
        });
        function ShowTermAndCondition() {
            $("#ShowTermAndCondition").show();
        }
        function HideTermAndCondition() {
            $("#ShowTermAndCondition").hide();
        }
    </script>
    <style type="text/css">
        #ShowBodyTermAndCondition {
            width: 65%;
            left: 18%;
        }

        @media screen and (min-device-width: 360px) and (max-device-width: 768px) {
            #ShowBodyTermAndCondition {
                width: 90% !important;
                left: 5% !important;
            }
        }
    </style>
    <div id="ShowTermAndCondition" style="display: none;">
        <div class="modalBackground progessposition">
        </div>
        <div id="ShowBodyTermAndCondition" style="z-index: 999; position: fixed; border: 3px solid #0c6ab8; padding: 10px; color: #fff; height: 90%; top: 5%; background: white; color: #333333; font-family: Verdana, Arial, helvetica; font-size: 8pt; text-align: left; margin-left: 0px;">
            <a onclick="HideTermAndCondition()" class="starail-Lightbox-close js-lightboxClose"
                style="position: absolute; cursor: pointer;"><i class="starail-Icon starail-Icon-close"></i></a>
            <div style="height: 100%; width: 100%; overflow-y: scroll;">
                <p align="center">
                    <b><font size="2">Terms and Conditions</font></b>
                </p>
                Ordering from International Rail Whether you're a new costumer placing your first
                order with International Rail or a returning customer, we want to make sure that
                ordering at International Rail is as easy as possible. Turnaround Time<br>
                <b>Turnaround Time</b> The turnaround time on our products is between 10 to 21 business
                days after your final approval.
                <br>
                <b>Shipping</b> A shipping calculator is located at the bottom of every product
                page, where you can easily type your zip code and find out the exact shipping cost
                for the product on that page.
                <br>
                <b>Payment</b> We accept almost any type of payment. You can easily pay online using
                major credit cards as Visa, MasterCard and American Express. We also are proud to
                accept PayPal transactions and at checkout you will be redirected to your PayPal
                account. You can choose to send us a check or money order, or call in for your credit
                card number in case you don’t want to type it online. You will find all above options
                after you place your products in the shopping cart and proceed to checkout.
                <br>
                <b>Wholesaler / Design studio</b> Dealer accounts are being setup after placing
                your first order with International Rail. Please contact us for more information
                on how you can receive dealer prices.
                <br>
                <b>Invoices</b> It's easy to print an invoice for any order you have placed with
                International Rail. Simply follow these instructions: * Click the Your Account link
                at the top of most pages of our site. * Log in using the e-mail address and password
                associated with your International Rail account. * Click on the order number you
                want to print the invoice for. * View the invoice and click on the “printable version”
                button to have it printed. Sales tax Companies selling over the Internet are subject
                to the same sales tax collection requirements as any other retailers. Remote sellers
                (including Internet retailers and catalog companies) are generally required to collect
                taxes where they have a physical selling presence. If they do not have any such
                presence, they are not required to collect sales taxes. Items sold by International
                Rail Printing LTD, or its subsidiaries, and shipped to destinations in the state
                of Connecticut are subject to 6% sales tax. Order status Wondering about an order
                you've already placed? Visit the order summary in Your Account. Just click on the
                order number and you will be able to see if your order is either pending, in graphic
                design, in manufacturing, finalizing order (printed but not yet ready to ship),
                shipped or complete. Artwork specifications Artwork should be created in CMYK format
                (not RGB format) from the following compatible programs from either a Mac or PC:
                Adobe Illustrator (convert all text to outlines or include fonts) Photoshop (send
                a file that is not flattened or compressed) Quark (include all files and fonts used)
                Freehand (convert all text to outlines or include fonts) You can upload the following
                types of files- PDF, PSD, EPS, TIFF, JPG, AI, PNG, and BMP. We cannot use Microsoft
                Publisher, Word, Powerpoint or Excel files. The artwork should be created at a resolution
                of 300 DPI. Allow 1/8 inch bleed to all images that extend past cut marks/die lines.
                Please ensure that no text, logos or essential images are placed within 1/8 inch
                from the edge of your magnet. Your computer monitor and hue and saturation settings
                can greatly affect the colors you see. For specific colors, please provide a Pantone®
                Matching System (PMS) color formula number. We cannot guarantee exact PMS color
                matches as we print CMYK. We will always send a free email proof for approval. We
                will not print and will not charge you before we receive your approval. Having trouble
                uploading your file? We are here to help! Contact our help center or order graphic
                design. Graphic design by International Rail Order graphic design for the product
                you choose from our experts. You can make changes as you wish until you get the
                best option for your business. We will always send a free email proof for approval.
                We will not print and will not charge you before we receive your approval. Print
                your existing business card on a magnet Please mail your existing business card
                to us. We will scan the business card and email you a free proof for approval. The
                scan is free. If the file needs editing, like changes in phone numbers or name,
                there is a $29 edit fee that may apply. We will not print and will not charge you
                before we receive your approval.
            </div>
        </div>
    </div>
    <div id="divChargesPopup" runat="server" class="chargepopup" style="display: none;">
    </div>
</asp:Content>
