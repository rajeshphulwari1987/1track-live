﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConditionsofUse.aspx.cs"
    MasterPageFile="~/Site.master" Inherits="ConditionsofUse" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%=script%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="starail-Grid--mobileFull">
        <div class="starail-Grid-col starail-Grid-col--nopadding">
            <div class="starail-Section starail-HomeHero starail-Section--nopadding" style="overflow: visible;">
                <img class="starail-HomeHero-img dots-header" alt="." id="img_Banner" runat="server" />
            </div>
        </div>
    </div>
    <div class="starail-BookingDetails-titleAndButton" style="padding-top: 5px">
       <div class="Breadcrumb"><asp:Literal runat="server" ID="litSeobreadcrumbs"></asp:Literal></div>
        <h2>
            <%=Server.HtmlDecode(PageTitle)%></h2>
        <p>
            <%=Server.HtmlDecode(PageDescription)%></p>
        <div class="starail-Section starail-Section--nopadding">
            <ul class="starail-Accordion js-accordon">
                <asp:Repeater ID="rptConditions" runat="server">
                    <ItemTemplate>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    1. Warranty and Liability</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("WarrantyLiability"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    2. Products
                                </h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Products"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    3. Use of our website</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("UseWebsite"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    4. Anti Viral Software</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("AntiViralSoftware"))%></p>
                                </div>
                            </div>
                        </li>
                        <li class="starail-Accordion-item js-accordonItem is-close">
                            <div class="starail-Accordion-header js-accordonHeader">
                                <h4 class="starail-Accordion-title">
                                    5. Copyrights</h4>
                                <button class="starail-Accordion-carat js-accordonButton">
                                    <i class="starail-Icon-chevron-down"></i>
                                </button>
                            </div>
                            <div class="starail-Accordion-body">
                                <div class="starail-Accordion-bodyContent">
                                    <p>
                                        <%#HttpContext.Current.Server.HtmlDecode((string)Eval("Copyright"))%></p>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Content>
