﻿using System;
using Business;
using System.Linq;
using System.Web;
using System.Configuration;

public partial class Security : System.Web.UI.Page
{
    readonly Masters _master = new Masters();
    private Guid _siteId;
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string adminSiteUrl = ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public string siteURL;
    public string script;
    readonly Masters _masterPage = new Masters();
    Guid pageID;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            script = new Masters().GetQubitScriptBySId(_siteId);
            BindSecurity(_siteId);

            var pid = _db.tblWebMenus.FirstOrDefault(x => x.Name.ToLower() == "home");
            if (pid != null)
                pageID = pid.ID;
            PageContent(pageID, _siteId);

            #region Seobreadcrumbs
            var pageIDs = (Guid)Page.RouteData.Values["PageId"];
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(pageIDs, _siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion
        }
    }

    public void BindSecurity(Guid siteId)
    {
        var result = _master.GetSecurityBySiteid(siteId);
        if (result != null)
        {
            lblTitle.Text = result.Title;
            lblDesp.Text = Server.HtmlDecode(result.Description);
        }
    }

    public void PageContent(Guid pageID, Guid siteID)
    {
        try
        {
            var result = _db.tblPages.FirstOrDefault(x => x.NavigationID == pageID && x.SiteID == siteID);
            if (result != null)
            {
                var url = result.Url;
                var oPage = _masterPage.GetPageDetailsByUrl(url);

                //Banner
                string[] arrListId = oPage.BannerIDs.Split(',');
                var idList = (from item in arrListId where !String.IsNullOrEmpty(item) select int.Parse(item)).ToList();
                var list = _masterPage.GetBannerImgByID(idList);
                int countdata = list.Count();
                int index = new Random().Next(countdata);
                var newlist = list.Skip(index).FirstOrDefault();
                if (newlist != null && newlist.ImgUrl != null)
                    img_Banner.Src = adminSiteUrl + "" + newlist.ImgUrl;
                else
                    img_Banner.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}