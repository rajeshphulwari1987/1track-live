﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="404.aspx.cs" Inherits="_404" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
                *{padding:0;outline:none;margin:0;}
                .header{height:90px;} 
                #errorPage{background:#f0f0f0;  font-size: 14px !important;}
                #errorPage .header{border:none;margin-left:auto;margin-right:auto;}
                #errorMessage{width:70%;min-height:100px;background:#FFF;margin:5% 15% 10% 15%;;border:1px solid #d60111;padding-bottom:10px;-moz-border-radius:7px;-webkit-border-radius:7px;-moz-box-shadow:0 2px 6px 1px #d6d6d6;-webkit-box-shadow:0 2px 6px 1px #d6d6d6;box-shadow:0 2px 6px 1px #d6d6d6;}
                #errorMessage h1{background:#d60111;height:36px;margin:2px;margin-bottom:10px;line-height:32px;font-size:18px;font-weight:bold;padding-left:5px;display:block;color:#FFF;overflow:hidden;-moz-border-radius-topleft:4px;-webkit-border-top-left-radius:4px;-moz-border-radius-topright:4px;-webkit-border-top-right-radius:4px;}
                #errorMessage p{color:#666;line-height:18px;padding:0 0 0 10px;margin-left:-1px; font-size: 14px !important;}
                #errorMessage a:visited,#errorMessage a{color:#d60111;}
                #errorMessage a:hover{color:#000;}
    </style>
</asp:Content>        
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container" id="InvalidURL" runat="server" style="display: block; margin-top: 110px;">
        <div class="error-box fullrow" style="margin: 0% 15% 10% 15%;">
            <div class="error-imgbox">
                <img src="images/error-images.jpg" alt="" />
            </div>
            <div class="error-textbox ">
                <div class="redtxt">
                    OOPS !!!
                </div>
    <div class="blu-txt">
        Something went wrong with requested URL. Please try again.</div>
</div>
        </div>
    </div>
    <div class="container" id="errorTicktURL" runat="server" style="display: none; margin-top: 110px;">
        <div id="errorMessage">
            <h1>
                Ticket Printing Failed</h1>
            <p>
                Hello Dear,<br />
                <br />
                You are not able to print your ticket due to ticket payment is fully refunded, so
                please make a new journey for the travel or contact to admin.
            </p>
            <br />
            <p>
                Go back to <a href="home">home page</a></p>
        </div>
    </div>
</asp:Content>
