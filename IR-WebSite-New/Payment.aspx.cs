﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;

public partial class Payment : System.Web.UI.Page
{
    Guid siteId;
    public string OrderNo = string.Empty;
    public string siteURL;
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["req"] != null)
            OrderNo = Request.Params["req"];
        else if (Session["OrderID"] != null)
            OrderNo = Session["OrderID"].ToString();
        else if (Session["P2POrderID"] != null)
            OrderNo = Session["P2POrderID"].ToString();

        if (string.IsNullOrEmpty(OrderNo))
            Response.Redirect("~/Home");

        //if (!IsPostBack)
        //{
        //    if (oManageClass.IsTiApi(Convert.ToInt64(OrderNo)))
        //        hdnIsTI.Value = "True";
        //}

        //if (siteId == Guid.Parse("fd93bd1f-5d1c-458e-87fd-b7c8e65fd67d"))
        //{
        //    PaymentFrame.Visible = false;
        //    div_warning.Visible = true;
        //    ScriptManager.RegisterStartupScript(this, GetType(), "Hidewaitbox", "Hidewaitbox();", true);
        //}
    }
}