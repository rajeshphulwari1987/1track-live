﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using System.Net;
using System.Net.Mail;

public partial class JourneyRequestForm : System.Web.UI.Page
{
    private Guid _siteId;
    public string script = "";
    public static string unavailableDates1 = "";
    public List<SnoClass> list = new List<SnoClass>();
    ManageUser _ManageUser = new ManageUser();
    public bool IsStaSite = false;
    public string MandatoryTextFirst = "", MandatoryTextFirstSecond = "";
    private ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public Guid AuAgentSiteID = Guid.Parse("3086C621-7493-4C5E-9C3F-A6E807BD211C");

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            _siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _siteId = Guid.Parse(Session["siteId"].ToString());
            if (AgentuserInfo.UserID == Guid.Empty)
                Response.Redirect("~/");
            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["PageId"] != null)
                {
                    var pageID = (Guid)Page.RouteData.Values["PageId"];
                    Page.Header.DataBind();
                }
                var siteDDates = new ManageHolidays().GetAllHolydaysBySite(_siteId);
                unavailableDates1 = "[";
                if (siteDDates.Any())
                {
                    foreach (var it in siteDDates)
                    {
                        unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
                }
                unavailableDates1 += "]";
                QubitOperationLoad();
                list.Add(new SnoClass { textrowno = "1st", rowno = "1" });
                repeterjourneylist.DataSource = list;
                repeterjourneylist.DataBind();
                ShowHideTermsMandatoryText(_siteId);
            }
        }
        catch (Exception ex)
        {
            errorMsg.Visible = true;
        }
    }

    public void QubitOperationLoad()
    {
        List<QubitFields> lstQbit = new Masters().GetQubitScriptList(Guid.Parse(Session["siteId"].ToString()));
        var res = lstQbit.FirstOrDefault();
        if (res != null)
            script = res.Script;
    }

    protected void repeterjourneylist_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var hdnddltimedept = e.Item.FindControl("hdnddltimedept") as HiddenField;
                var hdnddltimearrival = e.Item.FindControl("hdnddltimearrival") as HiddenField;
                var hdnddlclass = e.Item.FindControl("hdnddlclass") as HiddenField;
                var hdnddlservicetype = e.Item.FindControl("hdnddlservicetype") as HiddenField;

                var ddltimedept = e.Item.FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = e.Item.FindControl("ddltimearrival") as DropDownList;
                var ddlclass = e.Item.FindControl("ddlclass") as DropDownList;
                var ddlservicetype = e.Item.FindControl("ddlservicetype") as DropDownList;

                if (!string.IsNullOrEmpty(hdnddlclass.Value))
                    ddlclass.SelectedValue = hdnddlclass.Value;

                if (!string.IsNullOrEmpty(hdnddlservicetype.Value))
                    ddlservicetype.SelectedValue = hdnddlservicetype.Value;

                if (!string.IsNullOrEmpty(hdnddltimearrival.Value))
                    ddltimearrival.SelectedValue = hdnddltimearrival.Value;

                if (!string.IsNullOrEmpty(hdnddltimedept.Value))
                    ddltimedept.SelectedValue = hdnddltimedept.Value;
            }
        }
        catch (Exception ex)
        {
            errorMsg.Visible = true;
        }
    }

    protected void repeterjourneylist_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Addjourney")
            {
                fillrepeter();
                int item = repeterjourneylist.Items.Count;
                if (item == 0)
                    list.Add(new SnoClass { textrowno = "1st", rowno = "1" });
                if (item == 1)
                    list.Add(new SnoClass { textrowno = "2nd", rowno = "2" });
                if (item == 2)
                    list.Add(new SnoClass { textrowno = "3rd", rowno = "3" });
                if (item >= 3)
                    list.Add(new SnoClass { textrowno = (item + 1) + "th", rowno = (item + 1).ToString() });
                repeterjourneylist.DataSource = list;
                repeterjourneylist.DataBind();
            }

            if (e.CommandName == "Removejourney")
            {
                fillrepeter();
                string currentrow = e.CommandArgument.ToString();
                list.Remove(list.FirstOrDefault(x => x.rowno == currentrow));
                repeterjourneylist.DataSource = list;
                repeterjourneylist.DataBind();
            }
            ShowHideTermsMandatoryText(_siteId);
        }
        catch (Exception ex)
        {
            errorMsg.Visible = true;
        }
    }

    public void fillrepeter()
    {
        try
        {
            int i = 1;
            foreach (RepeaterItem it in repeterjourneylist.Items)
            {
                string textrowno = "";
                if (i == 1)
                    textrowno = "1st";
                if (i == 2)
                    textrowno = "2nd";
                if (i == 3)
                    textrowno = "3rd";
                if (i > 3)
                    textrowno = (i) + "th";

                var hdnrowno = it.FindControl("hdnrowno") as HiddenField;
                var txtfrom = it.FindControl("txtfrom") as TextBox;
                var txtto = it.FindControl("txtto") as TextBox;
                var txtchange = it.FindControl("txtchange") as TextBox;
                var txtdeptdate = it.FindControl("txtdeptdate") as TextBox;
                var txttrainno = it.FindControl("txttrainno") as TextBox;
                var ddltimedept = it.FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = it.FindControl("ddltimearrival") as DropDownList;
                var ddlclass = it.FindControl("ddlclass") as DropDownList;
                var ddlservicetype = it.FindControl("ddlservicetype") as DropDownList;

                var obj = new SnoClass
                {
                    ddlclass = ddlclass.SelectedValue,
                    ddlservicetype = ddlservicetype.SelectedValue,
                    ddltimearrival = ddltimearrival.SelectedValue,
                    ddltimedept = ddltimedept.SelectedValue,
                    rowno = i.ToString(),
                    textrowno = textrowno,
                    txtchange = txtchange.Text,
                    txtdeptdate = txtdeptdate.Text,
                    txtfrom = txtfrom.Text,
                    txtto = txtto.Text,
                    txttrainno = txttrainno.Text,
                };
                list.Add(obj);
                i++;
            }
        }
        catch (Exception ex)
        {
            errorMsg.Visible = true;
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!chkMandatory.Checked)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "checkMandatory", "LoadCal1();callvalerror();callvalerror2();checkMandatory();showhideterms();", true);
                return;
            }

            var userInfo = _ManageUser.AgentLoginById(AgentuserInfo.UserID);
            string subject = "";
            db_1TrackEntities db = new db_1TrackEntities();
            tblOrder objOrder = new tblOrder();
            objOrder.CreatedOn = DateTime.Now;
            objOrder.Status = 21;
            objOrder.SiteID = _siteId;
            objOrder.UserID = USERuserInfo.ID;
            objOrder.AgentID = AgentuserInfo.UserID;
            objOrder.TrvType = "P2P";
            objOrder.IpAddress = Request.ServerVariables["REMOTE_ADDR"];
            objOrder.AgentReferenceNo = string.IsNullOrEmpty(txtreference.Text) ? "0" : txtreference.Text;
            objOrder.page_accept = "created";
            objOrder.AffiliateCode = "";
            objOrder.Note = txtnote.Text;
            objOrder.P2PSTAStore = string.Empty;
            db.tblOrders.AddObject(objOrder);

            tblOrderTraveller objtot = new tblOrderTraveller();
            objtot.ID = Guid.NewGuid();
            objtot.Title = ddltitle.SelectedValue;
            objtot.FirstName = txtleadfname.Text;
            objtot.LastName = txtleadlast.Text;
            objtot.Country = Guid.Empty;
            db.tblOrderTravellers.AddObject(objtot);
            db.SaveChanges();

            int count = 0;
            foreach (RepeaterItem it in repeterjourneylist.Items)
            {
                var txtfrom = it.FindControl("txtfrom") as TextBox;
                var txtto = it.FindControl("txtto") as TextBox;
                var txtchange = it.FindControl("txtchange") as TextBox;
                var txtdeptdate = it.FindControl("txtdeptdate") as TextBox;
                var txttrainno = it.FindControl("txttrainno") as TextBox;
                var ddltimedept = it.FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = it.FindControl("ddltimearrival") as DropDownList;
                var ddlclass = it.FindControl("ddlclass") as DropDownList;
                var ddlservicetype = it.FindControl("ddlservicetype") as DropDownList;

                tblP2PSale objtps = new tblP2PSale();
                objtps.ID = Guid.NewGuid();
                objtps.From = txtfrom.Text;
                objtps.To = txtto.Text;
                objtps.Via = txtchange.Text;
                objtps.DateTimeArrival = objtps.DateTimeDepature = Convert.ToDateTime(txtdeptdate.Text);
                objtps.TrainNo = txttrainno.Text;
                objtps.FareName = tickettype.SelectedValue;
                objtps.Passenger = ddladult.SelectedValue + " Adults, " + ddlchildren.SelectedValue + " Child, " + ddlyouths.SelectedValue + " Youth, " + ddlseniors.SelectedValue + " Senior";
                objtps.CurrencyId = db.tblSites.FirstOrDefault(x => x.ID == _siteId).DefaultCurrencyID;
                objtps.Class = ddlclass.SelectedValue;
                objtps.SeviceName = ddlservicetype.SelectedValue;
                objtps.DepartureTime = ddltimedept.SelectedValue;
                objtps.ArrivalTime = ddltimearrival.SelectedValue;
                objtps.Senior = ddlseniors.SelectedValue;
                objtps.Adult = ddladult.SelectedValue;
                objtps.Children = ddlchildren.SelectedValue;
                objtps.Youth = ddlyouths.SelectedValue;
                objtps.P2PType = Requesttype.SelectedValue;
                objtps.DeliveryOption = ticketdelivered.SelectedValue;
                db.tblP2PSale.AddObject(objtps);

                tblPassP2PSalelookup objtpsl = new tblPassP2PSalelookup();
                objtpsl.ID = Guid.NewGuid();
                objtpsl.OrderID = objOrder.OrderID;
                objtpsl.PassSaleID = objtps.ID;
                objtpsl.OrderTravellerID = objtot.ID;
                objtpsl.ProductType = "P2P";
                db.tblPassP2PSalelookup.AddObject(objtpsl);
                db.SaveChanges();
                if (count == 0)
                    subject = "Quote - Dep: " + txtdeptdate.Text + " Pax: " + ddltitle.SelectedValue + " " + txtleadfname.Text + " " + txtleadlast.Text;
                count++;
            }

            tblOrderBillingAddress objbilling = new tblOrderBillingAddress();
            objbilling.ID = Guid.NewGuid();
            objbilling.OrderID = objOrder.OrderID;
            objbilling.TitleShpg = ddltitle.SelectedValue;
            objbilling.FirstNameShpg = objbilling.FirstName = userInfo.Forename;
            objbilling.LastNameShpg = objbilling.LastName = userInfo.Surname;
            objbilling.EmailAddressShpg = objbilling.EmailAddress = userInfo.EmailAddress;
            objbilling.PhoneShpg = objbilling.Phone = string.Empty;
            db.tblOrderBillingAddresses.AddObject(objbilling);
            db.SaveChanges();

            string body = GetEmailBody(objOrder.OrderID);
            SendMail(_siteId, subject, body, !string.IsNullOrEmpty(userInfo.EmailAddress) ? userInfo.EmailAddress.Trim() : "", "staau@1trackagent.com");
            //SendMail(_siteId, subject, body, !string.IsNullOrEmpty(userInfo.EmailAddress) ? userInfo.EmailAddress.Trim() : "", "dipu.bharti@dotsquares.com");
            succmessage.Text = "Thank you, your request ID is " + objOrder.OrderID + ", please make a note of this and quote it whenever you're in contact with us.  One of our rail experts will be in touch soon!";
            pnlJourneyInfo.Visible = errorMsg.Visible = false;
            sucessMsg.Visible = true;
        }
        catch (Exception ex) { errorMsg.Visible = true; }
    }

    public bool SendMail(Guid siteId, string Subject, string Body, string FromEmail, string ToMail)
    {
        try
        {
            var masterPage = new Masters();
            var message = new MailMessage();
            var smtpClient = new SmtpClient();
            var result = masterPage.GetEmailSettingDetail(siteId);
            var SiteData = masterPage.GetSiteListEdit(siteId);
            if (result != null)
            {
                smtpClient.Host = result.SmtpHost;
                smtpClient.Port = Convert.ToInt32(result.SmtpPort);
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                smtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);

                message.From = new MailAddress(FromEmail, FromEmail);
                if (!string.IsNullOrEmpty(SiteData.JourneyEmail))
                    ToMail = SiteData.JourneyEmail.ToLower() == ToMail ? ToMail : SiteData.JourneyEmail;
                message.To.Add(ToMail);
                message.To.Add(FromEmail);
                message.Subject = Subject;
                message.IsBodyHtml = true;
                message.Body = Body;
                message.CC.Add("dipu.bharti@dotsquares.com");
                smtpClient.Send(message);
                return true;
            }
            else
                return false;
        }
        catch (Exception ee)
        {
            return false;
        }
    }

    public string GetEmailBody(long OrderId)
    {
        try
        {
            var userInfo = _ManageUser.AgentLoginById(AgentuserInfo.UserID);
            string body = "<table  border='0' cellspacing='0' cellpadding='5' align='left'><tbody>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Order ID</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + OrderId + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Your name</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + userInfo.Forename + " " + userInfo.Surname + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Your email</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + userInfo.EmailAddress + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Your reference</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtreference.Text + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Type of request</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + Requesttype.SelectedValue + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Type of ticket</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + tickettype.SelectedValue + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>How would you like the ticket delivered</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ticketdelivered.SelectedValue + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Number of adults</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + (Convert.ToInt32(ddladult.SelectedValue) + Convert.ToInt32(ddlchildren.SelectedValue) + Convert.ToInt32(ddlyouths.SelectedValue) + Convert.ToInt32(ddlseniors.SelectedValue)) + "</td></tr>";
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Lead passenger name (as per passport)</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ddltitle.SelectedValue + txtleadfname.Text + txtleadlast.Text + "</td></tr>";

            int count = 2;
            for (int i = 0; i < repeterjourneylist.Items.Count; i++)
            {
                var txtfrom = repeterjourneylist.Items[i].FindControl("txtfrom") as TextBox;
                var txtto = repeterjourneylist.Items[i].FindControl("txtto") as TextBox;
                var txtchange = repeterjourneylist.Items[i].FindControl("txtchange") as TextBox;
                var txtdeptdate = repeterjourneylist.Items[i].FindControl("txtdeptdate") as TextBox;
                var txttrainno = repeterjourneylist.Items[i].FindControl("txttrainno") as TextBox;
                var ddltimedept = repeterjourneylist.Items[i].FindControl("ddltimedept") as DropDownList;
                var ddltimearrival = repeterjourneylist.Items[i].FindControl("ddltimearrival") as DropDownList;
                var ddlclass = repeterjourneylist.Items[i].FindControl("ddlclass") as DropDownList;
                var ddlservicetype = repeterjourneylist.Items[i].FindControl("ddlservicetype") as DropDownList;
                int rowCount = repeterjourneylist.Items.Count;

                if (rowCount > 1)
                    body += "<tr><td width='500px' colspan='2' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Journey " + (i + 1) + "</td></tr>";

                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>From</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtfrom.Text + "</td></tr>";
                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>To</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + txtto.Text + "</td></tr>";
                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Date of departure</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + txtdeptdate.Text + "</td></tr>";
                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Time of departure (24 hour)</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ddltimedept.Text + "</td></tr>";
                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Class of service</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + ddlclass.SelectedValue + "</td></tr>";
                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Type of accommodation</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + ddlservicetype.SelectedValue + "</td></tr>";
                body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>Add a " + (count == 2 ? count + "nd" : count + "rd") + " journey?</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>" + (rowCount > (i + 1) ? "Yes" : "No") + "</td></tr>";
                count++;
            }
            body += "<tr><td width='350px' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>Special instructions eg seat requests, extra pax, extra journeys.</td><td style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;'>" + txtnote.Text + "</td></tr>";

            if (_siteId == AuAgentSiteID)
                body += "<tr><td width='500px' colspan='2' style='font-size: 15px; color: #5c5c5c; font-family: Arial, Helvetica, sans-serif; padding:5px; border: 1px solid #dddddd;background:#f4f5f7;'>IP address: 81.149.113.28 [STA Travel Australia]</td></tr>";
            body += "</tbody></table>";
            return body;
        }
        catch (Exception ex) { throw ex; }
    }

    public void ShowHideTermsMandatoryText(Guid SiteId)
    {
        try
        {
            var data = _oWebsitePage.GetSiteDatabySiteId(SiteId);
            if (data != null)
            {
                IsStaSite = data.IsSTA.HasValue ? (data.IsSTA.Value == true ? true : false) : false;
                div_Mandatory.Visible = data.IsMandatoryTerm;
                div_NotMandatory.Visible = data.IsMandatoryTermSecond;
                p_MandatoryText.InnerHtml = MandatoryTextFirst = !string.IsNullOrEmpty(data.MandatoryTermText) ? data.MandatoryTermText : "";
                p_NotMandatoryText.InnerHtml = MandatoryTextFirstSecond = !string.IsNullOrEmpty(data.MandatoryTermTextSecond) ? data.MandatoryTermTextSecond : "";
            }
        }
        catch (Exception ex) { errorMsg.Visible = true; }
    }
}

public class SnoClass
{
    public string rowno { get; set; }
    public string textrowno { get; set; }
    public string txtfrom { get; set; }
    public string txtto { get; set; }
    public string txtchange { get; set; }
    public string txtdeptdate { get; set; }
    public string txttrainno { get; set; }
    public string ddltimedept { get; set; }
    public string ddltimearrival { get; set; }
    public string ddlclass { get; set; }
    public string ddlservicetype { get; set; }
}