﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Http;
using System.Xml;
using System.Text;
using Business;
using System.Net.Mail;

public partial class TestDemo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write("<br/>" + Request.Url);
        //Response.Write("<br/> " + Request.Url.Host);
        //Response.Write("<br/> " + Request.IsSecureConnection);
        //Response.Write("<br/> " + Request.Url.AbsolutePath);
        //Response.Write("<br/> " + Request.Url.AbsoluteUri);
        //Response.Write("<br/> " + Request.Url.LocalPath);
        Response.Write("<br/> " + DateTime.Now);
    }

    public void SendScheduleMail()
    {
        db_1TrackEntities db = new db_1TrackEntities();
        var ExpireDate = DateTime.Now.AddDays(45);
        var CurrentDate = DateTime.Now;
        var BodyMessage = new StringBuilder();
        var datalist = new List<ProductExpired>();
        BodyMessage.Append("Hi All,<br/><br/>");
        BodyMessage.Append("Please be aware that the following product(s) are set to expire in the month of " + String.Format("{0:MMMM}", DateTime.Now) + " " + DateTime.Now.Year + ":<br/><ul>");
        if (db.tblProducts.Any(x => x.ProductEnableToDate < ExpireDate && x.ProductEnableToDate > CurrentDate && x.IsActive == true && x.tblProductCategoriesLookUps.FirstOrDefault().tblCategory.IsActive == true))
        {
            datalist = db.tblProducts.Where(x => x.ProductEnableToDate < ExpireDate && x.ProductEnableToDate > CurrentDate && x.IsActive == true && x.tblProductCategoriesLookUps.FirstOrDefault().tblCategory.IsActive == true).AsEnumerable().Select(x => new ProductExpired
            {
                CategoryName = x.tblProductCategoriesLookUps.FirstOrDefault().tblCategory.tblCategoriesNames.FirstOrDefault().Name,
                Name = x.tblProductNames.FirstOrDefault().Name,
                ExpireDate = x.ProductEnableToDate.ToString("dd/MMM/yyyy hh:mm")
            }).ToList();
        }
        foreach (var item in datalist)
            BodyMessage.Append("<li>" + item.CategoryName + " => " + item.Name + ": (" + item.ExpireDate + ")</li>");
        BodyMessage.Append("</ul><br/><br/>Thanks<br/>");
        BodyMessage.Append("1track");
        if (datalist.Count > 0)
        {
            var Message = new MailMessage();
            var SmtpClient = new SmtpClient();
            Guid SiteId = Guid.Parse("5858F611-6726-4227-A417-52CCDEA36B3D");
            var result = new Masters().GetEmailSettingDetail(SiteId);
            if (result != null)
            {
                var fromAddres = new MailAddress("rajeshkumar.phulwari@dotsquares.com", "rajeshkumar.phulwari@dotsquares.com");
                Message.From = fromAddres;
                Message.To.Add("rajeshkumar.phulwari@dotsquares.com");
                Message.Subject = "1Track Product Expiry Notification";
                Message.IsBodyHtml = true;
                Message.Body = BodyMessage.ToString();
                SmtpClient.Host = result.SmtpHost;
                SmtpClient.Port = Convert.ToInt32(result.SmtpPort);
                SmtpClient.UseDefaultCredentials = true;
                SmtpClient.Credentials = new NetworkCredential(result.SmtpUser, result.SmtpPass);
                SmtpClient.EnableSsl = Convert.ToBoolean(result.EnableSsl);
                SmtpClient.Send(Message);
            }
        }
    }

    class ProductExpired
    {
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string ExpireDate { get; set; }

    }

}
