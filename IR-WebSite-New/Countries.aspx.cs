﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Business;
using OneHubServiceRef;
using System.Web.UI.HtmlControls;
#endregion

public partial class Countries : System.Web.UI.Page
{
    #region Local Variables
    private readonly Masters _master = new Masters();
    private readonly ManageJourney _objJourney = new ManageJourney();
    private readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string SiteUrl = System.Configuration.ConfigurationManager.AppSettings["HttpAdminHost"];
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    private readonly FrontEndManagePass oManageClass = new FrontEndManagePass();
    Guid siteId;
    public string Url = "";
    private readonly ManageInterRailNew _oManageInterRailNew = new ManageInterRailNew();
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
            siteId = Guid.Parse(Session["siteId"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["country"] != null)
                countryMessage.Attributes.Add("style", "display:block");
            else
                countryMessage.Attributes.Add("style", "display:none");

            GetAllContryContinent();
            Url = Request.Url.Host.ToString();
            Session["country"] = null;
        }
        if (Request.Url.ToString().ToLower().Contains("countries"))
        {
            #region Seobreadcrumbs
            //Guid id = (Guid)Page.RouteData.Values["PageId"];
            Guid id= Guid.Parse("B219ECE0-A8B4-4BC4-B782-94F977D98028");
            var dataSeobreadcrumbs = new ManageSeo().Getlinkbody(id, siteId);
            if (dataSeobreadcrumbs != null && dataSeobreadcrumbs.IsActive)
                litSeobreadcrumbs.Text = dataSeobreadcrumbs.SourceCode;
            #endregion
        }
    }

    public void GetAllContryContinent()
    {
        rptContinent.DataSource = _oManageInterRailNew.GetAllContinent(siteId);
        rptContinent.DataBind();
    }



    protected void rptContinent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            HiddenField ContinentId = (HiddenField)e.Item.FindControl("hdfContinentId");
            Repeater rptCountry = (Repeater)e.Item.FindControl("rptCountry");
            rptCountry.DataSource = _oManageInterRailNew.GetAllCountry(siteId, Guid.Parse(ContinentId.Value)).Where(x=>x.CountryCode!="IND").ToList();
            rptCountry.DataBind();
            if (_oManageInterRailNew.GetAllCountry(siteId, Guid.Parse(ContinentId.Value)).Count() < 1)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "SectionHide", "SectionHide('" + ContinentId.Value + "');", true);
            }
        }
    }
    protected void rptCountry_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "COUNTRY")
        {
            string[] strarray = e.CommandArgument.ToString().Split('~');
            Session["CountryPass"] = strarray[0];


            List<string> listcountrycode = new List<string> { "VNM", "MYS", "CHN", "IND" };
            Response.Redirect(listcountrycode.Contains(strarray[1]) ? "TrainResults?ct=" + strarray[1] : "~/");

           // Response.Redirect("~/");
        }


    }
    protected void rptContinent_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Continent")
        {
            Session["CountryPass"] = e.CommandArgument.ToString();
            Response.Redirect("~/");
        }
    }
}