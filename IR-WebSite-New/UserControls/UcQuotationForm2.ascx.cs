﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using OneHubServiceRef;
using Business;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

public partial class UserControls_UcQuotationForm2 : System.Web.UI.UserControl
{
    public int minDate = 0;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    public static string unavailableDates1 = "";
    private Guid siteId;
    public string siteURL;
    public string script;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(Guid.Parse(Session["siteId"].ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["siteId"] != null)
            {
                siteId = Guid.Parse(Session["siteId"].ToString());
                var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);
                unavailableDates1 = "[";
                if (siteDDates.Count() > 0)
                {
                    foreach (var it in siteDDates)
                    {
                        unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                    }
                    unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
                }
                unavailableDates1 += "]";
            }

            if (!IsPostBack)
            {
                bool isUseSite = (bool)_oWebsitePage.IsUsSite(siteId);
                divSearch.Visible = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
                minDate = _oWebsitePage.GetBookingDayLimitBySiteId(siteId);
                script = new Masters().GetQubitScriptBySId(siteId);
            }
        }
        catch (Exception ex) { ShowMessage(2, ex.Message); }
    }

    public void ShowMessage(int flag, string message)
    {
        //0: Display none all div
        //1: Display block success div
        //2: Display block error div
        switch (flag)
        {
            case 0:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = string.Empty;
                break;
            case 1:
                DivError.Style.Add("display", "none");
                DivSuccess.Style.Add("display", "block");
                lblErrorMsg.Text = string.Empty;
                lblSuccessMsg.Text = message;
                break;
            case 2:
                DivError.Style.Add("display", "block");
                DivSuccess.Style.Add("display", "none");
                lblErrorMsg.Text = message;
                lblSuccessMsg.Text = string.Empty;
                break;
        }
    }
}