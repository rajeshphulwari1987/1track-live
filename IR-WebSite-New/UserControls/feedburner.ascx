﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="feedburner.ascx.cs" Inherits="UserControls_feedburner" %>
<style type="text/css">
    /*Feed Barner*/
    .pnlNewsContentPanel
    {
        clear: both;
        padding: 5px;
        overflow: hidden;
        font-size: 13px;
    }
    .pnlNewsDescription
    {
        font-size: 12px;
        color: #666666;
        padding: 7px;
        display: block;
        float: right;
        width: 98%;
        border-bottom: 1px dotted #DDDDDD;
    }
    #divMainBar
    {
        overflow: auto;
        background-color: #FDFDFD;
        border: none solid #ABD2E9;
        height: 530px;
        width: 370px;
    }
    #hlNewsTitle
    {
        font-family: Lucida Grande,Lucida Sans,Lucida Sans Unicode,Verdana,Arial,Helvetica,sans-serif;
        font-weight: bold;
    }
    
    #divImageHeader
    {
        width: 370px;
        height: 40px;
    }
    .dashboard-block-outer
    {
        float: left;
    }
    .dashboard-block
    {
        width: 96%;
        position: relative;
        border-radius: 5px;
        -ms-border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border: 1px solid #b3b3b3;
        float: left;
        padding: 2%;
        margin-top: 10px;
        behavior: url(../Others/PIE.htc);
        background: #efefef;
        min-height: 403px;
        max-height: 403px;
    }
    .dashboard-block h3
    {
        font-size: 18px;
        color: #9a223a;
        padding: 0px;
        margin: 5px;
    }
    
    .dashboard-block .innner-dashboard
    {
        width: 90%;
        position: relative;
        margin: 1%;
        border-radius: 5px;
        -ms-border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border: 1px solid #CCCCCC;
        float: left;
        padding: 1% 4%;
        behavior: url(../Others/PIE.htc);
        background: #fff;
        min-height: 350px;
        max-height: 350px;
        overflow-y: scroll;
    }
    .dashboard-block .innner-dashboard-faq
    {
        width: 90%;
        position: relative;
        margin: 1%;
        border-radius: 5px;
        -ms-border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border: 1px solid #CCCCCC;
        float: left;
        padding: 1% 4%;
        behavior: url(../Others/PIE.htc);
        background: #fff;
        min-height: 330px;
        max-height: 330px; /*overflow-y: scroll;*/
    }
    .dashboard-block p.dsh
    {
        font-size: 13px;
        font-weight: bold;
        border: 1px dashed #CCCCCC;
        padding: 10px;
        color: #b43758;
        margin: 5px 0;
    }
    
    .dashboard-block h4
    {
        font-size: 13px;
        color: #313333;
        padding: 5px 0 0;
        margin: 0px;
        font-weight: bold;
    }
    .dashboard-block p
    {
        font-size: 12px;
        color: #313333;
        border: 0px;
        padding: 5px 0 0;
        margin: 0px;
    }
    
    .dashboard-block ul
    {
        margin: 5px 0;
        padding: 0px;
        float: left;
        width: 100%;
    }
    .dashboard-block ul li
    {
        float: left;
        list-style: none;
        background: url(../images/bullet_style.png) no-repeat left;
        font-size: 12px;
        color: #666666;
        margin-left: 10px;
        padding-left: 15px;
        line-height: 20px;
        width: 90%;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".more-link").attr("target", "_blank");
        $(".more-link").attr("target", "_blank");
    });
</script>
<div class="tab-detail-in">
    <div class="dashboard-block-outer">
        <div class="dashboard-block">
            <h3>
                <asp:Label ID="lblFeedTitle" runat="server" />
            </h3>
            <div class="innner-dashboard">
                <asp:ListView ID="lstViewNewsFeeds" runat="server" OnItemDataBound="lstViewNewsFeeds_ItemDataBound">
                    <LayoutTemplate>
                        <div runat="server" id="ItemPlaceHolder" style="width: 100%">
                        </div>
                        <div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <asp:Panel ID="pnlHeader" runat="server" CssClass="pnlNewsContentPanel">
                            <h4>
                                <asp:HyperLink ID="hlNewsTitle" runat="server"></asp:HyperLink><br />
                                Date:
                                <asp:Label ID="lblLastUpdateTime" runat="server"></asp:Label></h4>
                            <asp:Panel ID="panelNewsDescription" runat="server" CssClass="pnlNewsDescription">
                            </asp:Panel>
                        </asp:Panel>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <div>
                            Sorry, there are no items to display.
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
                <asp:DataPager ID="dataPagerFeeds" runat="server" PagedControlID="lstViewNewsFeeds">
                    <Fields>
                    </Fields>
                </asp:DataPager>
            </div>
        </div>
    </div>
</div>
