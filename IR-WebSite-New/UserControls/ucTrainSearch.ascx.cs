﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using OneHubServiceRef;
using System.Web.Script.Serialization;

public partial class OtherSiteP2PBooking_ucTrainSearch : UserControl
{

    public static string unavailableDates1 = "";
    Guid siteId;
    private readonly ManageFrontWebsitePage _oWebsitePage = new ManageFrontWebsitePage();
    readonly db_1TrackEntities _db = new db_1TrackEntities();
    public string siteURL;
    public int minDate = 0;
    ManageOneHub _ManageOneHub = new ManageOneHub();
    public string railCardCodeList = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["siteId"] != null)
        {
            siteId = Guid.Parse(Session["siteId"].ToString());
            siteURL = _oWebsitePage.GetSiteURLbySiteId(siteId);

            var siteDDates = new ManageHolidays().GetAllHolydaysBySite(siteId);
            unavailableDates1 = "[";
            if (siteDDates.Count() > 0)
            {
                foreach (var it in siteDDates)
                {
                    unavailableDates1 += "\"" + Convert.ToDateTime(it.DateofHoliday).ToString("dd-MM-yyyy") + "\"" + ",";
                }
                unavailableDates1 = unavailableDates1.Substring(0, unavailableDates1.Length - 1);
            }
            unavailableDates1 += "]";
        }

        if (!IsPostBack)
        {
            bool isUseSite = (bool)_oWebsitePage.IsUsSite(siteId);
            divSearch.Visible = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            minDate = _oWebsitePage.GetBookingDayLimitBySiteId(siteId);
            rdBookingType.Items[1].Text = isUseSite ? "Round Trip" : "Return";
            for (int i = 10; i >= 0; i--)
            {
                ddlAdult.Items.Insert(0, new ListItem(i.ToString(CultureInfo.InvariantCulture), i.ToString(CultureInfo.InvariantCulture)));
                ddlAdult.SelectedValue = "1";
            }

            for (int j = 10; j >= 0; j--)
            {
                ddlChild.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlYouth.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
                ddlSenior.Items.Insert(0, new ListItem(j.ToString(CultureInfo.InvariantCulture), j.ToString(CultureInfo.InvariantCulture)));
            }

            if (Session["BookingUCRerq"] != null)
                SearchTrainInfoForFill();
        }
    }

    public void SearchTrainInfo()
    {
        try
        {
            var objBruc = new BookingRequestUserControl();
            var objBooking = new ManageBooking();

            GetStationDetailsByStationName objStationDeptDetail = objBooking.GetStationDetailsByStationName(txtFrom.Text.Trim());
            objBruc.depstCode = objStationDeptDetail != null ? objStationDeptDetail.StationCode : string.Empty;
            objBruc.depRCode = objStationDeptDetail != null ? objStationDeptDetail.RailwayCode : "0";
            ViewState["DeptrailName"] = objStationDeptDetail != null ? objStationDeptDetail.RailName : "";

            GetStationDetailsByStationName objStationArrDetail = objBooking.GetStationDetailsByStationName(txtTo.Text.Trim());
            objBruc.arrstCode = objStationArrDetail != null ? objStationArrDetail.StationCode : string.Empty;
            objBruc.arrRCode = objStationArrDetail != null ? objStationArrDetail.RailwayCode : "0";
            ViewState["ArvtrailName"] = objStationArrDetail != null ? objStationArrDetail.RailName : "";

            List<EvolviRailCardRequestInfo> objRailCardList = new List<EvolviRailCardRequestInfo>();
            if (objStationDeptDetail != null)
            {
                if (objStationDeptDetail.RailName.Trim() == "BENE")
                    objBruc.OneHubServiceName = "BeNe";
                else if (objStationDeptDetail.RailName.Trim() == "NTV")
                    objBruc.OneHubServiceName = "NTV";
                else if (objStationDeptDetail.RailName.Trim() == "ITALIA")
                    objBruc.OneHubServiceName = "Trenitalia";
                else if (objStationDeptDetail.RailName.Trim() == "EVOLVI")
                {
                    objBruc.OneHubServiceName = "Evolvi";
                    objBruc.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                    objBruc.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                    objBruc.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                    objBruc.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                    objBruc.isRailCard = false;

                    #region Hide rail card option it will be used in future.
                    //int counter = Convert.ToInt32(ddlAdult.SelectedValue) + Convert.ToInt32(ddlChild.SelectedValue);
                    //for (int i = 1; i <= counter; i++)
                    //{
                    //    string ddlRailCard = Request.Form["ddlRailCard" + i + ""];
                    //    if (Convert.ToInt32(ddlYouth.SelectedValue) == 0 && Convert.ToInt32(ddlSenior.SelectedValue) == 0 && ddlRailCard != null)
                    //    {
                    //        objRailCardList.Add(new EvolviRailCardRequestInfo
                    //        {
                    //            RailCardType = ddlRailCard == "0" ? "" : "RailCard",
                    //            RailCardCode = ddlRailCard == "0" ? "" : ddlRailCard
                    //        });
                    //    }
                    //    if (ddlRailCard != null)
                    //        objBruc.isRailCard = true;
                    //}
                    #endregion
                    objBruc.EvolviRailCardRequestInfoList = objRailCardList;
                }
            }

            objBruc.FromDetail = txtFrom.Text.Trim();
            objBruc.ToDetail = txtTo.Text.Trim();
            objBruc.depdt = DateTime.ParseExact(txtDepartureDate.Text, "dd/MMM/yyyy", null);
            objBruc.depTime = Convert.ToDateTime(ddldepTime.SelectedValue);

            objBruc.ClassValue = Convert.ToInt32(ddlClass.SelectedValue);
            objBruc.Adults = Convert.ToInt32(ddlAdult.SelectedValue);
            objBruc.Boys = Convert.ToInt32(ddlChild.SelectedValue);
            objBruc.Seniors = Convert.ToInt32(ddlSenior.SelectedValue);
            objBruc.Youths = Convert.ToInt32(ddlYouth.SelectedValue);
            objBruc.Transfare = Convert.ToInt32(ddlTransfer.SelectedValue);
            objBruc.ReturnDate = String.IsNullOrEmpty(txtReturnDate.Text) || txtReturnDate.Text.Trim() == "DD/MM/YYYY" ? string.Empty : txtReturnDate.Text;
            objBruc.ReturnTime = ddlReturnTime.SelectedValue;
            objBruc.Journeytype = rdBookingType.SelectedValue;

            if (rdBookingType.SelectedValue == "1")
                objBruc.IsReturnJurney = true;

            Session["BookingUCRerq"] = objBruc;
            var currDate = DateTime.Now;

            if (objBruc.OneHubServiceName == "BeNe" || objBruc.OneHubServiceName == "Trenitalia")
            {
                int daysLimit = _ManageOneHub.GetEurostarBookingDays(objBruc);
                if (daysLimit == 0)
                    daysLimit = new ManageBooking().getOneHubServiceDayCount(objBruc.OneHubServiceName);

                var maxDate = currDate.AddDays(daysLimit - 1);
                if (objBruc.depdt > maxDate && objStationDeptDetail != null)
                {
                    Session["ErrorMessage"] = "ErrorMaxDate";
                    Session["TrainSearch"] = null;
                    if (ViewState["DeptrailName"] != null && ViewState["ArvtrailName"] != null)
                    {
                        if (ViewState["DeptrailName"].ToString() == "BENE" && ViewState["ArvtrailName"].ToString() == "BENE")
                            Response.Redirect("TrainResults.aspx?req=" + "BE");
                        else if (ViewState["DeptrailName"].ToString() == "ITALIA" && ViewState["ArvtrailName"].ToString() == "ITALIA")
                        {
                            Session["TiTimer"] = null;
                            new StationList().GetTrenitaliaTime();
                            Response.Redirect("TrainResults.aspx?req=" + "TI");
                        }
                        else if (ViewState["DeptrailName"].ToString() == "NTV" && ViewState["ArvtrailName"].ToString() == "NTV")
                            Response.Redirect("TrainResults.aspx?req=" + "NTV");
                        else if (ViewState["DeptrailName"].ToString() == "EVOLVI" && ViewState["ArvtrailName"].ToString() == "EVOLVI")
                            Response.Redirect("TrainResults.aspx?req=" + "EV");
                    }
                }
                else
                    Session["ErrorMessage"] = null;
            }

            var client = new OneHubRailOneHubClient();
            TrainInformationRequest request = _ManageOneHub.TrainInformation(objBruc, 1, txtDepartureDate.Text, ddldepTime.SelectedValue, txtReturnDate.Text, ddlReturnTime.SelectedValue);

            if (objBruc.OneHubServiceName == "Evolvi")
            {
                request.SequenceNo = System.Web.Configuration.WebConfigurationManager.AppSettings["SequenceNo"].ToString();
                request.MaxResponse = System.Web.Configuration.WebConfigurationManager.AppSettings["MaxResponse"].ToString();
                request.ISOCountry = new ManageBooking().GetCountryISOCode(siteId);
                request.ISOCurrency = new ManageBooking().GetCurrencyISOCode(siteId);
                request.EvolviRailCardRequestInfo = objRailCardList.ToArray();
            }

            if (Convert.ToInt32(rdBookingType.SelectedValue) > 0 && objBruc.OneHubServiceName == "Trenitalia")
                request.IsReturnJourney = false;
            else if (Convert.ToInt32(rdBookingType.SelectedValue) > 0)
                request.IsReturnJourney = true;


            _ManageOneHub.ApiLogin(request, siteId);
            TrainInformationResponse pInfoSolutionsResponse = client.TrainInformation(request);

            if (pInfoSolutionsResponse != null && pInfoSolutionsResponse.TrainInformationList != null)
            {
                //--TreniItalia Search return request                
                if (objBruc.ReturnDate != string.Empty && objBruc.OneHubServiceName == "Trenitalia")
                {
                    List<TrainInfoSegment> list = pInfoSolutionsResponse.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    request = _ManageOneHub.TrainInformation(objBruc, 2, txtDepartureDate.Text, ddldepTime.SelectedValue, txtReturnDate.Text, ddlReturnTime.SelectedValue);
                    request.IsReturnJourney = true;
                    _ManageOneHub.ApiLogin(request, siteId);
                    TrainInformationResponse pInfoSolutionsResponseReturn = client.TrainInformation(request);
                    List<TrainInfoSegment> listReturn = pInfoSolutionsResponseReturn.TrainInformationList.SelectMany(x => x.TrainInfoSegment.Select(y => y)).ToList();
                    list.AddRange(listReturn);
                    if (pInfoSolutionsResponseReturn.TrainInformationList != null)
                    {
                        List<TrainInformation> listResp = pInfoSolutionsResponse.TrainInformationList.ToList();
                        List<TrainInformation> listRespReturn = pInfoSolutionsResponseReturn.TrainInformationList.ToList();
                        List<TrainInformation> resultList = listResp.Concat(listRespReturn).ToList();
                        pInfoSolutionsResponse.TrainInformationList = resultList.ToArray();
                    }
                }
            }
            Session["TrainSearch"] = pInfoSolutionsResponse;
            Session["HoldJourneyDetailsList"] = null;
            Session["HoldReserVationRequest"] = null;
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("Thread"))
            {
                Session["ErrorMessage"] = ex.Message;
                Response.Redirect("TrainResults.aspx");
            }
        }
    }

    public void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            #region If P2P Widget is not allowed for this site then searching will not happing
            bool IsVisibleP2PWidget = (bool)_oWebsitePage.IsVisibleP2PWidget(siteId);
            if (!IsVisibleP2PWidget)
                return;
            #endregion

            if (!string.IsNullOrEmpty(txtReturnDate.Text))
                if (!txtReturnDate.Text.Contains("DD/MM/YYYY"))
                    if (Convert.ToDateTime(txtDepartureDate.Text).Date > Convert.ToDateTime(txtReturnDate.Text).Date)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "callalert", "LoadCal();clickaa('" + rdBookingType.SelectedValue + "');alert('Return date should be greater than the Depart date')", true);
                        return;
                    }
            SearchTrainInfo();

            string stri = "BE";
            if (ViewState["DeptrailName"] != null && ViewState["ArvtrailName"] != null)
            {
                if (ViewState["DeptrailName"].ToString() == "BENE" && ViewState["ArvtrailName"].ToString() == "BENE")
                    stri = "BE";
                else if (ViewState["DeptrailName"].ToString() == "ITALIA" && ViewState["ArvtrailName"].ToString() == "ITALIA")
                    stri = "TI";
                else if (ViewState["DeptrailName"].ToString() == "NTV" && ViewState["ArvtrailName"].ToString() == "NTV")
                    stri = "NTV";
                else if (ViewState["DeptrailName"].ToString() == "EVOLVI" && ViewState["ArvtrailName"].ToString() == "EVOLVI")
                    stri = "EV";
            }
            Response.Redirect("TrainResults.aspx?req=" + stri);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SearchTrainInfoForFill()
    {
        try
        {
            BookingRequestUserControl objBRUC;
            objBRUC = (BookingRequestUserControl)Session["BookingUCRerq"];
            txtFrom.Text = objBRUC.FromDetail;
            txtTo.Text = objBRUC.ToDetail;
            txtDepartureDate.Text = objBRUC.depdt.ToString("dd/MMM/yyyy");
            rdBookingType.SelectedValue = objBRUC.Journeytype == null ? "0" : objBRUC.Journeytype;
            if (rdBookingType.SelectedValue == "0")
            {
                txtReturnDate.Text = "";
                txtReturnDate.Enabled = false;
                ddlReturnTime.Enabled = false;
                reqvalerror4.Enabled = false;
                lblrtnerror.Visible = false;
            }
            else
            {
                lblrtnerror.Visible = true;
                txtReturnDate.Enabled = true;
                ddlReturnTime.Enabled = true;
                reqvalerror4.Enabled = true;
                txtReturnDate.Text = objBRUC.ReturnDate;
            }
            ddlAdult.SelectedValue = objBRUC.Adults.ToString();
            ddlChild.SelectedValue = objBRUC.Boys.ToString();
            ddlYouth.SelectedValue = objBRUC.Youths.ToString();
            ddlSenior.SelectedValue = objBRUC.Seniors.ToString();
            ddldepTime.SelectedValue = objBRUC.depTime.ToString("HH:mm");
            ddlReturnTime.SelectedValue = objBRUC.ReturnTime;
            ddlClass.SelectedValue = objBRUC.ClassValue.ToString();
            ddlTransfer.SelectedValue = objBRUC.Transfare.ToString();
            railCardCodeList = objBRUC.EvolviRailCardRequestInfoList != null && objBRUC.EvolviRailCardRequestInfoList.Count > 0 ? new JavaScriptSerializer().Serialize(objBRUC.EvolviRailCardRequestInfoList.Select(x => x.RailCardCode).ToList()) : string.Empty;
            hdntxtFromRailName.Value = hdntxtToRailName.Value = !string.IsNullOrEmpty(objBRUC.OneHubServiceName) ? objBRUC.OneHubServiceName.ToUpper() : "";

            #region Hide rail card option it will be used in future
            //if (objBRUC.OneHubServiceName == "Evolvi")
            //{
            //    div_chkAddRailCard.Attributes.Add("style", "display:block;");
            //    hdntxtFromRailName.Value = "EVOLVI";
            //    hdntxtToRailName.Value = "EVOLVI";
            //}
            #endregion
        }
        catch (Exception ex)
        {

        }
    }
}