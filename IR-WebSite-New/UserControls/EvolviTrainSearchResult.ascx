﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EvolviTrainSearchResult.ascx.cs"
    Inherits="UserControls_EvolviTrainSearchResult" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="ucTrainSearch.ascx" TagName="ucTrainSearch" TagPrefix="uc1" %>
<style type="text/css">
    .starail-JourneyBlock-class:nth-child(2n-1)
    {
        clear: none;
    }
    .starail-JourneyBlock-class.full-fare-width
    {
        margin-top: 10px;
    }
    .full-fare-width
    {
        width: 100%;
        float: left;
    }
    .new-inner-section
    {
        height: 250px;
        overflow-y: auto;
    }
    .new-inner-section-fare-validity
    {
        height: 525px;
        overflow-y: auto;
    }
    .hide-railcard, .starail-JourneyBlock-journeyDetails-evolvi, .showhide-railcard, .starail-JourneyBlock-less-trigger-evolvi
    {
        display: none;
    }
    .table-lable.custline
    {
        line-height: 16px !important;
    }
    #MainContent_ucSResultEvolvi_pnlQuckLoad
    {
        z-index: 101 !important;
    }
    #MainContent_ucSResultEvolvi_mdpexQuickLoad_backgroundElement
    {
        z-index: 100 !important;
    }
    .selectlist
    {
        width: 50%;
    }
    .starail-Form-inputContainer-col
    {
        width: 100% !important;
    }
    .selectlistmargintwo
    {
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .selectlistmargin
    {
        margin-bottom: 10px;
    }
    .starail-BookingDetails-form
    {
        padding: 25px !important;
    }
    .starail-JourneyBlock-journeyDetails-row .starail-JourneyBlock-journeyDetails-details
    {
        width: 33%;
    }
    /*price css*/
    .searchtop-box
    {
        overflow-x: auto;
        overflow-y: hidden;
    }
    .searchtable-box
    {
        display: table;
        width: 900px;
    }
    
    .searchtable-row
    {
        display: table-row;
    }
    .searchtable-header
    {
        display: table-header-group;
    }
    .searchtable-col
    {
        display: table-cell;
    }
    
    .searchtable-row.headrow .searchtable-col
    {
        font-weight: bold;
    }
    .searchtable-row .searchtable-col.headinner
    {
        font-weight: bold;
    }
    
    .searchtable-row .searchtable-col
    {
        padding: 5px 10px;
    }
    /*pax css*/
    .searchtablepax-box
    {
        display: table;
        width: 430px;
    }
    .searchtablepax-row
    {
        display: table-row;
    }
    .searchtablepax-header
    {
        display: table-header-group;
    }
    .searchtablepax-col
    {
        display: table-cell;
    }
    
    .searchtablepax-row.headrow .searchtablepax-col
    {
        font-weight: bold;
    }
    .searchtablepax-row .searchtablepax-col.headinner
    {
        font-weight: bold;
    }
    
    .fullrow:before, .fullrow:after
    {
        content: "";
        display: table;
    }
    .fullrow:after
    {
        clear: both;
    }
    
    .starail-JourneyBlock-class
    {
        padding-right: 8px !important;
        margin-top: 0;
    }
    
    #MainContent_ucSResultEvolvi_pnlQuckLoad
    {
        width: 1050px;
    }
    
    #MainContent_ucSResultEvolvi_pnlQuckLoad .starail-YourBooking-col
    {
        width: 14%;
    }
    #MainContent_ucSResultEvolvi_pnlQuckLoad .selectlist
    {
        width: 70%;
    }
    #MainContent_ucSResultEvolvi_pnlQuckLoad .starail-YourBooking
    {
        padding: 2.14286rem 1.14286rem 0.42857rem 1.14286rem;
    }
    .headerwidth
    {
        width: 5% !important;
    }
    .headerwidth2
    {
        width: 10% !important;
    }
    .headerwidth3
    {
        width: 1% !important;
    }
    .submitbuttons
    {
        margin-top: 10px;
    }
    .cancelbutton
    {
        text-align: center;
        display: block;
    }
    .selectlist2
    {
        width: 90%;
    }
    .starail-Form-fancyRadioGroup:not(#ie8)
    {
        font-size: 12.5px;
    }
    .table-lable
    {
        display: block;
        padding: 0 10px 10px;
        font-size: 1rem;
        border-bottom: 1px solid #d8d8ce;
        margin: 0 -10px 10px;
        font-weight: bold;
    }
    .starail-YourBooking-ticketDetails > div
    {
        vertical-align: top;
    }
    .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable
    {
        display: none;
    }
    .starail-YourBooking-table .starail-YourBooking-ticketDetails:first-child .table-lable
    {
        display: block;
    }
    .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable + span
    {
        display: block;
        padding-top: 11px;
    }
    .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable + input[type="radio"]
    {
        display: block;
        margin-top: 15px;
    }
    
    .top-arrow, .bottom-arrow
    {
        width: 25px;
        float: right;
        top: 20px;
        position: absolute;
        right: -26px;
    }
    .from-price
    {
        width: 56px;
    }
    a.bottom-arrow
    {
        margin-right: 18px;
    }
    a.top-arrow
    {
        margin-right: 18px;
    }
    .p-price-margin
    {
        margin-left: 5px;
        width: 100%;
    }
    .price-font
    {
        font-size: 20px !important;
        margin-left: 20px;
    }
    .pricemain-box
    {
        position: relative;
    }
    .pricebox-tag
    {
        position: absolute;
        width: 170px;
        text-align: center;
        right: -10px;
        top: 5px;
        height: 50px;
    }
    .pricebox-tag span
    {
        padding-left: 0px !important;
        width: 100%;
        display: inline-block;
        text-align: center !important;
        font-weight: bold;
        float: none !important;
        margin-left: 0;
    }
    .pricebox-tag img
    {
        margin: -16px auto 0;
        display: inline-block;
    }
    
    .group-discount-popup
    {
        position: fixed;
        width: 40%;
        left: 28%;
        top: 14%;
        z-index: 1001;
    }
    
    .fare-validity-popup
    {
        position: fixed;
        width: 65%;
        left: 18%;
        top: 2%;
        z-index: 10001;
    }
    
    .group-discount-popup .groput-discount
    {
        display: block;
        width: 100%;
    }
    .fare-validity-popup .groput-discount
    {
        display: block;
        width: 100%;
    }
    .spanradio
    {
        position: inherit !important;
    }
    .click-able
    {
        cursor: pointer;
        text-decoration: underline;
        font-size: 16px;
    }
    
    .starail-farevalidity-close
    {
        position: absolute;
        top: -11.2px;
        right: -11.2px;
        width: 32px;
        height: 32px;
        border-radius: 16px;
        box-shadow: 0 0 3px rgba(0,0,0,0.15);
        display: block;
        color: #FFF;
        text-decoration: none;
        text-align: center;
        transition: background-color .3s ease-in-out;
    }
    
    .starail-Icon-close
    {
        font-size: 1.28571rem;
        line-height: 2rem !important;
    }
    
    .starail-farevalidity-close:hover, .starail-farevalidity-close:focus, .starail-farevalidity-close:visited
    {
        color: #FFF;
        text-decoration: none;
    }
    
    .farecode-parent
    {
        margin-bottom: 5px;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 639px)
    {
        .starail-JourneyBlock-journeyDetails-row .starail-JourneyBlock-journeyDetails-details
        {
            width: 50%;
        }
        .starail-JourneyBlock-journeyDetails-row .p-heading
        {
            width: 100%;
            padding-bottom: 12px;
            padding-top: 12px;
        }
        .searchtablepax-box
        {
            width: 250px;
        }
        .starail-BookingDetails-form
        {
            padding: none !important;
        }
        .selectlistmargin
        {
            margin-bottom: 5px;
        }
        .starail-JourneyBlock-journeyDetails-row .starail-JourneyBlock-journeyDetails-title
        {
            width: 100%;
        }
    }
    @media only screen and (max-width: 767px)
    {
        .starail-YourBooking-ticketDetails > div
        {
            display: block;
            width: 100% !important;
            padding: 0 0 7px;
        }
        .starail-YourBooking-ticketDetails
        {
            display: block;
        }
        .starail-YourBooking-table
        {
            display: block;
        }
        #MainContent_ucSResultEvolvi_pnlQuckLoad
        {
            width: 95% !important;
            overflow: auto;
            top: 25px !important;
            bottom: 25px !important;
        }
        .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable
        {
            display: block;
            margin-left: 0;
            margin-right: 10px;
        }
        .table-lable
        {
            padding: 0 0 5px;
            margin-bottom: 0;
            border: none;
        }
        .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable + span
        {
            display: inline-block;
            padding-top: 0;
        }
        .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable + input[type="radio"]
        {
            display: inline-block;
            margin-top: 0;
        }
        .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable:first-child, .starail-YourBooking-table .starail-YourBooking-ticketDetails .table-lable:last-child
        {
            display: inline-block;
        }
        .starail-YourBooking-ticketDetails
        {
            padding-bottom: 0;
        }
        #MainContent_ucSResultEvolvi_pnlQuckLoad .starail-Form-input, #MainContent_ucSResultEvolvi_pnlQuckLoad .starail-Form-textarea, #MainContent_ucSResultEvolvi_pnlQuckLoad .starail-Form-select
        {
            padding: 3px;
        }
    }
    @media only screen and (max-width: 767px)
    {
        .group-discount-popup
        {
            width: 100%;
            left: 1%;
            top: 1%;
        }
    }
</style>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upnlTrainSearch"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upnlTrainSearch" runat="server">
    <ContentTemplate>
        <div class="starail-Grid starail-Grid--mobileFull">
            <div class="starail-Grid-col starail-Grid-col--nopadding fullrow">
                <div class="starail-ProgressBar starail-ProgressBar-stage--stage1 starail-u-hideMobile">
                    <div class="starail-ProgressBar-line">
                        <div class="starail-ProgressBar-progress">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Ticket Selection</p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Booking Details</p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Checkout</p>
                    </div>
                </div>
                <asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
                <asp:HiddenField ID="hdnDateDepart" runat="server" Value="" />
                <asp:HiddenField ID="hdnDateArr" runat="server" Value="" />
                <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
                <asp:HiddenField ID="hdnReq" runat="server" Value="" />
                <%--From Hidden Value --%>
                <asp:HiddenField ID="hdnFareIdFrom" runat="server" />
                <asp:HiddenField ID="hdnTrainNoFrom" runat="server" />
                <asp:HiddenField ID="hdnFareClassFrom" runat="server" />
                <asp:HiddenField ID="hdnSingleOrDefaultFrom" runat="server" />
                <asp:HiddenField ID="hdnRestrictionCodeFrom" runat="server" />
                <asp:HiddenField ID="hdnClassCodeFrom" runat="server" />
                <asp:HiddenField ID="hdnpriceFrom" runat="server" />
                <asp:HiddenField ID="hdnIdFrom" runat="server" />
                <asp:HiddenField ID="hdnTicketTypeCodeFrom" runat="server" />
                <asp:HiddenField ID="hdnValidityCodeFrom" runat="server" />
                <asp:HiddenField ID="hdnFareSetterCodeFrom" runat="server" />
                <asp:HiddenField ID="hdnRefundAndCancellationsFrom" runat="server" />
                <asp:HiddenField ID="hdnChangesToTravelPlansFrom" runat="server" />
                <%--To Hidden Value--%>
                <asp:HiddenField ID="hdnFareIdTo" runat="server" />
                <asp:HiddenField ID="hdnTrainNoTo" runat="server" />
                <asp:HiddenField ID="hdnFareClassTo" runat="server" />
                <asp:HiddenField ID="hdnSingleOrDefaultTo" runat="server" />
                <asp:HiddenField ID="hdnRestrictionCodeTo" runat="server" />
                <asp:HiddenField ID="hdnClassCodeTo" runat="server" />
                <asp:HiddenField ID="hdnpriceTo" runat="server" />
                <asp:HiddenField ID="hdnIdTo" runat="server" />
                <asp:HiddenField ID="hdnTicketTypeCodeTo" runat="server" />
                <asp:HiddenField ID="hdnValidityCodeTo" runat="server" />
                <asp:HiddenField ID="hdnFareSetterCodeTo" runat="server" />
                <asp:HiddenField ID="hdnRefundAndCancellationsTo" runat="server" />
                <asp:HiddenField ID="hdnChangesToTravelPlansTo" runat="server" />
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <%-- Hide rail card option it will be used in future--%>
                <div class="starail-Form-row" id="div_youthSnrMessage" runat="server" style="display: none;">
                    <p id="errorMsg" runat="server" class="starail-Alert starail-Alert--error">
                        ! You chose youth or senior passenger but did not add a railcard. Rates shown are
                        the standard adult fares. To add a railcard please <a style="text-decoration: underline;"
                            onclick="activeInactiveTab();">click here</a>
                    </p>
                </div>
                <%-- End rail card option --%>
                <div class="starail-Section starail-Section--nopadding">
                    <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                        <div class="starail-TrainOptions-mobileNav-stage1">
                            <a href="home" class="starail-Button starail-Button--blue"><i class="starail-Icon-chevron-left">
                            </i>Edit Search</a> <a class="starail-Button starail-TrainOptions-mobileNav-rtnJourney starail-rtnJourney-trigger starail-Button--blue is-disabled"
                                id="anchor_Next" runat="server">Next: Return <i class="starail-Icon-chevron-right">
                                </i></a>
                        </div>
                        <div class="starail-TrainOptions-mobileNav-stage2 starail-u-hide">
                            <a class="starail-Button starail-Button--blue starail-TrainOptions-outbound-trigger">
                                <i class="starail-Icon-chevron-left"></i>Outbound</a> <a class="starail-Button starail-TrainOptions-mobileNav-booking starail-booking-trigger starail-Button--blue is-disabled">
                                    Your Details <i class="starail-Icon-chevron-right"></i></a>
                            <!-- will submit form -->
                        </div>
                    </div>
                    <div class="starail-Tabs starail-TrainOptions-mainTabs">
                        <ul class="starail-Tabs-tabs">
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-Tabs-tab--active starail-TrainOptions-outbound csscount">
                                <a href="#outbound" class="js-starail-Tabs-trigger">
                                    <div class="starail-TrainOptions-tab-container">
                                        <p class="starail-u-hideDesktop starail-TrainOptions-mobiletab-title">
                                            Your outbound journey</p>
                                        <p class="starail-TrainOptions-tab-cost starail-u-hideMobile">
                                            <%=currency %>
                                            <asp:Label ID="lbloutPrice" runat="server" Text="0.00" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-title">
                                            <asp:Literal ID="ltroutFrom" runat="server" />
                                            <i class="starail-Icon-direction-arrow"><span class="starail-u-visuallyHidden">to </span>
                                            </i>
                                            <asp:Literal ID="ltroutTo" runat="server" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-details">
                                            <span class="starail-u-hideMobile">Outbound </span><span class="starail-u-hideDesktopInline">
                                                Date: </span><span class="starail-TrainOptions-tab-details-date">
                                                    <asp:Literal ID="ltroutDate" runat="server" />
                                                </span><span class="starail-u-hideMobile">| </span><span class="starail-TrainOptions-tab-details-time starail-TrainOptions-tab-details-time--init starail-u-hideMobile">
                                                    (Choose time)</span>
                                        </p>
                                    </div>
                                </a></li>
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-TrainOptions-inbound csscount"
                                runat="server" id="ShowInboud"><a href="#inbound" class="js-starail-Tabs-trigger">
                                    <div class="starail-TrainOptions-tab-container">
                                        <p class="starail-u-hideDesktop starail-TrainOptions-mobiletab-title">
                                            Your inbound journey</p>
                                        <p class="starail-TrainOptions-tab-cost starail-u-hideMobile">
                                            <%=currency %>
                                            <asp:Label ID="lblinPrice" runat="server" Text="0.00" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-title">
                                            <asp:Literal ID="ltrinFrom" runat="server" />
                                            <i class="starail-Icon-direction-arrow"><span class="starail-u-visuallyHidden">to </span>
                                            </i>
                                            <asp:Literal ID="ltrinTo" runat="server" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-details">
                                            <span class="starail-u-hideMobile">Inbound </span><span class="starail-u-hideDesktopInline">
                                                Date: </span><span class="starail-TrainOptions-tab-details-date">
                                                    <asp:Literal ID="ltrinDate" runat="server" />
                                                </span><span class="starail-u-hideMobile">| </span><span class="starail-TrainOptions-tab-details-time starail-TrainOptions-tab-details-time--init starail-u-hideMobile">
                                                    (Choose time)</span>
                                        </p>
                                    </div>
                                </a></li>
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-TrainOptions-subtotal">
                                <div class="starail-TrainOptions-subtotal-details">
                                    <p class="starail-u-hideDesktopInline">
                                        Price:
                                    </p>
                                    <p class="starail-TrainOptions-subtotal-title starail-u-hideMobile" data-starail-subtotal="Subtotal"
                                        data-starail-total="Total Price">
                                        Subtotal</p>
                                    <p class="starail-TrainOptions-subtotal-cost">
                                        <%=currency %>
                                        <asp:Label ID="lblTotalPrice" runat="server" Text="0.00" />
                                    </p>
                                </div>
                                <p class="starail-TrainOptions-subtotal-passengers">
                                    For
                                    <asp:Literal ID="ltrPassenger" runat="server" />
                                </p>
                            </li>
                        </ul>
                        <div class="starail-Tabs-wrapper">
                            <div class="starail-Grid starail-Grid--nopadding starail-Grid--mobileFull">
                                <div class="starail-Grid-col starail-Grid-col--8of12">
                                    <div id="outbound" class="starail-Tabs-content journey-type">
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkOutTopEarlier" CssClass="starail-Button starail-Button--blue"
                                                OnClick="lnkEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkOutTopLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later"
                                                OnClick="lnkLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                        <div class="starail-TrainOptions-outboundError starail-Alert starail-Alert--error starail-u-hide">
                                            <p>
                                                Please select an outbound journey.</p>
                                        </div>
                                        <asp:Repeater ID="rptoutEvolvi" runat="server" OnItemDataBound="rptoutEvolvi_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="starail-JourneyBlock">
                                                    <div class="starail-JourneyBlock-form">
                                                        <div class="starail-JourneyBlock-header">
                                                            <div class="starail-JourneyBlock-summary" style="width: 100%">
                                                                <div class="starail-DestinationIcon" style="height: 5px">
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow" style="min-width: 35%; float: left;
                                                                    padding-right: 1%;">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        From:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrFrom" />
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        To:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrTo" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-summary">
                                                                <div class="starail-DestinationIcon">
                                                                    <div class="starail-DestinationIcon-line">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Departs:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details
                                                starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide
                                                starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                    <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Arrives:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide
                                                starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                    <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Changes:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details
                                                starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <asp:Label ID="lblTrainChanges" runat="server" Text='<%#Eval("NoOfStops").ToString()%>' />
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Duration:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details
                                                starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-info starail-u-hideMobile pricemain-box">
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <a class="starail-JourneyBlock-more-trigger" style="line-height: 0px" href="#"><i
                                                                        class="starail-Icon-changes"></i>
                                                                        <asp:Label ID="lblTrainChangesMobile" runat="server" Text='<%#Eval("NoOfStops").ToString()%>'
                                                                            CssClass="pricecheap" />
                                                                    </a>
                                                                </p>
                                                                <div class="pricebox-tag">
                                                                    <img src="<%=siteURL %>images/from.png" class="from-price" alt="" />
                                                                    <asp:Label ID="lblCheapPrice" runat="server" CssClass="price-font"></asp:Label>
                                                                </div>
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <i class="starail-Icon-time"></i><span class="starail-infoRow-time">
                                                                        <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span> <a style="display: block;"
                                                                            class="bottom-arrow">
                                                                            <img src="<%=siteURL %>images/arrow-bottom.png" alt="" /></a> <a style="display: none;"
                                                                                class="top-arrow">
                                                                                <img src="<%=siteURL %>images/arrow-top.png" alt="" /></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-content starail-u-hideMobile collapsiblediv" id="DivTr"
                                                            runat="server" style="display: none;">
                                                        </div>
                                                        <div class="starail-JourneyBlock-journeyDetails-evolvi">
                                                            <div class="starail-JourneyBlock-content">
                                                                <asp:Repeater ID="GrdRouteInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="starail-JourneyBlock-journeyDetails-row">
                                                                            <asp:Repeater ID="rptLegInfo" runat="server" DataSource='<%#Eval("EvLegInfoList") %>'>
                                                                                <ItemTemplate>
                                                                                    <div class="starail-JourneyBlock-journeyDetails-details p-heading">
                                                                                        <p>
                                                                                            <span>
                                                                                                <%#Eval("TravellBy")%></span>
                                                                                        </p>
                                                                                        <p>
                                                                                            <span>
                                                                                                <%# string.Format("{0}", "(" + Eval("JourneyDuration").ToString().Split(':')[0] + "h:" + Eval("JourneyDuration").ToString().Split(':')[1] + "min" + ")")%></span>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                        <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                            Departs:</p>
                                                                                        <p>
                                                                                            <span>
                                                                                                <%#Eval("DepartureTime")%></span><span class="starail-u-hideMobile"> | </span>
                                                                                            <span>
                                                                                                <%#Eval("DepartureDate")%></span></p>
                                                                                        <p>
                                                                                            <%#Eval("DepartureStation")%></p>
                                                                                    </div>
                                                                                    <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                        <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                            Arrives:</p>
                                                                                        <p>
                                                                                            <span>
                                                                                                <%#Eval("ArrivalTime")%></span><span class="starail-u-hideMobile"> | </span>
                                                                                            <span>
                                                                                                <%#Eval("ArrivalDate")%></span></p>
                                                                                        <p>
                                                                                            <%#Eval("ArrivalStation")%></p>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-u-hideDesktop starail-JourneyBlock-mobileTriggers">
                                                            <a class="starail-JourneyBlock-more-trigger-evolvi" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>View journey details</a> <a class="starail-JourneyBlock-less-trigger-evolvi"
                                                                href="#"><i class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-JourneyBlock-mobileTickets">
                                                            <asp:DropDownList ID="ddlFromPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                            <asp:HyperLink ID="hlnkConfirm" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                                Text="Confirm Ticket" />
                                                            <asp:Button ID="btnGetPriceMobile" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-u-hideMobile collapsiblediv" style="display: none;">
                                                            <a class="starail-JourneyBlock-more-trigger-evolvi" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>More info</a> <a class="starail-JourneyBlock-less-trigger-evolvi" href="#"><i
                                                                class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                            <asp:HyperLink href="#" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger is-disabled"
                                                                ID="btnContinueBooking" runat="server" onclick="return SendJCodeAndServiceID();"> <%=outBoundTag%></asp:HyperLink>
                                                            <asp:HiddenField ID="hdnJournyTypeReturn" runat="server" Value='<%#Eval("IsReturn") %>' />
                                                            <asp:Button ID="btnGetPrice" runat="server" CssClass="starail-Button" Style="float: right;"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                            <p class="starail-JourneyBlock-footer-ticketSelection">
                                                                <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">
                                                                    <i class="starail-Icon-question"></i></a>
                                                            </p>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                            </div>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <asp:HyperLink runat="server" ID="hlnkContinue" class="starail-Button starail-rtnJourney-trigger"
                                                                    Text="Continue" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkOutBottomEarlier" CssClass="starail-Button starail-Button--blue"
                                                OnClick="lnkEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier
        trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkOutBottomLater" CssClass="starail-Button
        starail-Button--blue starail-TrainOptions-later" OnClick="lnkLater_Click">Later
        trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="inbound" class="starail-Tabs-content starail-Tabs-content--hidden journey-type">
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkInTopEarlier" CssClass="starail-Button starail-Button--blue"
                                                OnClick="lnkInEarlier_Click">
        <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier
        trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkInToptomLater" CssClass="starail-Button
        starail-Button--blue starail-TrainOptions-later" OnClick="lnkInLater_Click">Later
        trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                        <asp:Repeater ID="rptinEvolvi" runat="server" OnItemDataBound="rptinEvolvi_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="starail-JourneyBlock">
                                                    <div class="starail-JourneyBlock-form">
                                                        <div class="starail-JourneyBlock-header">
                                                            <div class="starail-JourneyBlock-summary" style="width: 100%">
                                                                <div class="starail-DestinationIcon" style="height: 5px">
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow" style="min-width: 35%; float: left;
                                                                    padding-right: 1%;">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        From:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrFrom" />
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        To:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrTo" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-summary">
                                                                <div class="starail-DestinationIcon">
                                                                    <div class="starail-DestinationIcon-line">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle
        starail-DestinationIcon-circle--filled">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Departs:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">
                                                                                | </span><span class="starail-JourneyBlock-date
        starail-u-hideMobile">
                                                                                    <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                                                                                </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Arrives:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide
        starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
            <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Changes:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details
        starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <asp:Label ID="lblTrainChanges" runat="server" Text='<%#Eval("NoOfStops").ToString()%>' />
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Duration:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-info starail-u-hideMobile pricemain-box">
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <a class="starail-JourneyBlock-more-trigger" style="line-height: 0px" href="#"><i
                                                                        class="starail-Icon-changes"></i>
                                                                        <asp:Label ID="lblTrainChangesMobile" runat="server" Text='<%#Eval("NoOfStops").ToString()%>'
                                                                            CssClass="pricecheap" />
                                                                    </a>
                                                                </p>
                                                                <div class="pricebox-tag">
                                                                    <img src="<%=siteURL %>images/from.png" class="from-price" alt="" />
                                                                    <asp:Label ID="lblCheapPrice" runat="server" CssClass="price-font"></asp:Label>
                                                                </div>
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <i class="starail-Icon-time"></i><span class="starail-infoRow-time">
                                                                        <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span> <a style="display: block;"
                                                                            class="bottom-arrow">
                                                                            <img src="<%=siteURL %>images/arrow-bottom.png" alt="" /></a> <a style="display: none;"
                                                                                class="top-arrow">
                                                                                <img src="<%=siteURL %>images/arrow-top.png" alt="" /></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-content starail-u-hideMobile collapsiblediv" id="DivTr"
                                                            runat="server" style="display: none;">
                                                        </div>
                                                        <div class="starail-JourneyBlock-journeyDetails-evolvi">
                                                            <div class="starail-JourneyBlock-content">
                                                                <asp:Repeater ID="GrdRouteInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="starail-JourneyBlock-journeyDetails-row">
                                                                            <asp:Repeater ID="rptLegInfo" runat="server" DataSource='<%#Eval("EvLegInfoList") %>'>
                                                                                <ItemTemplate>
                                                                                    <div class="starail-JourneyBlock-journeyDetails-details p-heading">
                                                                                        <p>
                                                                                            <span>
                                                                                                <%#Eval("TravellBy")%></span>
                                                                                        </p>
                                                                                        <p>
                                                                                            <span>
                                                                                                <%# string.Format("{0}", "(" + Eval("JourneyDuration").ToString().Split(':')[0] + "h:" + Eval("JourneyDuration").ToString().Split(':')[1] + "min" + ")")%></span>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                        <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                            Departs:</p>
                                                                                        <p>
                                                                                            <span>
                                                                                                <%#Eval("DepartureTime")%></span><span class="starail-u-hideMobile"> | </span>
                                                                                            <span>
                                                                                                <%#Eval("DepartureDate")%></span></p>
                                                                                        <p>
                                                                                            <%#Eval("DepartureStation")%></p>
                                                                                    </div>
                                                                                    <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                        <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                            Arrives:</p>
                                                                                        <p>
                                                                                            <span>
                                                                                                <%#Eval("ArrivalTime")%></span><span class="starail-u-hideMobile"> | </span>
                                                                                            <span>
                                                                                                <%#Eval("ArrivalDate")%></span></p>
                                                                                        <p>
                                                                                            <%#Eval("ArrivalStation")%></p>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-u-hideDesktop starail-JourneyBlock-mobileTriggers">
                                                            <a class="starail-JourneyBlock-more-trigger-evolvi" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>View journey details</a> <a class="starail-JourneyBlock-less-trigger-evolvi"
                                                                href="#"><i class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-JourneyBlock-mobileTickets">
                                                            <asp:DropDownList ID="ddlFromPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                            <asp:HyperLink ID="hlnkConfirm" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                                Text="Confirm Ticket" />
                                                            <asp:Button ID="btnGetPriceMobile" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-u-hideMobile collapsiblediv" style="display: none;">
                                                            <a class="starail-JourneyBlock-more-trigger-evolvi" href="#"><i class="starail-Icon starail-Icon-pluscircle">
                                                            </i>More info</a> <a class="starail-JourneyBlock-less-trigger-evolvi" href="#"><i
                                                                class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                            <asp:HyperLink href="#" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger is-disabled"
                                                                ID="btnContinueBooking" runat="server" onclick="return SendJCodeAndServiceID();">
                                                                 <%=inBoundTag%></asp:HyperLink>
                                                            <asp:HiddenField ID="hdnJournyTypeReturn" runat="server" Value='<%#Eval("IsReturn") %>' />
                                                            <asp:Button ID="btnGetPrice" runat="server" CssClass="starail-Button" Style="float: right;"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                            <p class="starail-JourneyBlock-footer-ticketSelection">
                                                                <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">
                                                                    <i class="starail-Icon-question"></i></a>
                                                            </p>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                            </div>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <asp:HyperLink runat="server" ID="hlnkContinue" class="starail-Button starail-rtnJourney-trigger"
                                                                    Text="Continue" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkInBottomEarlier" CssClass="starail-Button
        starail-Button--blue" OnClick="lnkInEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i
        class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkInBottomLater" CssClass="starail-Button starail-Button--blue
        starail-TrainOptions-later" OnClick="lnkInLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i
        class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Grid-col starail-Grid-col--4of12 starail-u-hideMobile starail-Sidebar">
                                    <a href="#" class="starail-Button starail-Button--full starail-Sidebar-rtnJourney starail-rtnJourney-trigger is-disabled"
                                        onclick="return SendJCodeAndServiceID();">Booking Details</a> <a href="#" class="starail-Button starail-Button--full starail-Sidebar-booking starail-rtnJourney-trigger starail-u-hide is-disabled"
                                            onclick="return SendJCodeAndServiceID();">Booking Details</a>
                                    <div class="starail-Tabs starail-Sidebar-tabs">
                                        <ul class="starail-Tabs-tabs active-inactive-tab">
                                            <li class="starail-Tabs-tab starail-Tabs-tab--active"><a href="#starail-routemap"
                                                class="js-starail-Tabs-trigger">
                                                <div>
                                                    Route Map
                                                </div>
                                            </a></li>
                                            <li class="starail-Tabs-tab"><a href="#starail-edittickets" class="js-starail-Tabs-trigger">
                                                <div>
                                                    Edit Search
                                                </div>
                                            </a></li>
                                        </ul>
                                        <div id="starail-routemap" class="starail-Tabs-content">
                                            <div class="results-map">
                                                <div id="mymap" class="results-map" style="height: 300px; width: 100%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="starail-edittickets" class="starail-Tabs-content starail-Tabs-content--hidden">
                                            <div class="starail-SearchTickets starail-SearchTickets--mini">
                                                <div class="starail-Box
        starail-Box--whiteMobile">
                                                    <uc1:ucTrainSearch ID="ucTrainSearch" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    &nbsp;
                                    <div class="starail-Sidebar-bottomBtns">
                                        <a href="#" class="starail-Button starail-Button--full starail-Sidebar-rtnJourney starail-rtnJourney-trigger is-disabled"
                                            onclick="return SendJCodeAndServiceID();">Booking Details</a> <a href="#" class="starail-Button starail-Button--full starail-Sidebar-booking starail-rtnJourney-trigger starail-u-hide is-disabled"
                                                onclick="return SendJCodeAndServiceID();">Booking Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="starail-TrainOptions-mobileNav
        starail-u-hideDesktop">
                        <div class="starail-TrainOptions-mobileNav-stage1">
                            <a href="default.aspx#rail-tickets" class="starail-Button starail-Button--blue"><i
                                class="starail-Icon-chevron-left"></i>Edit Search</a> <a href="#" class="starail-Button starail-TrainOptions-mobileNav-rtnJourney
        starail-rtnJourney-trigger starail-Button--blue is-disabled">Next: Return <i class="starail-Icon-chevron-right">
        </i></a>
                        </div>
                        <div class="starail-TrainOptions-mobileNav-stage2 starail-u-hide">
                            <a href="#" class="starail-Button starail-Button--blue starail-TrainOptions-outbound-trigger">
                                <i class="starail-Icon-chevron-left"></i>Outbound</a> <a href="#" class="starail-Button
        starail-TrainOptions-mobileNav-booking starail-booking-trigger starail-Button--blue
        is-disabled">Your Details <i class="starail-Icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%=pricePoup%>
        <%--   Model Popup--%>
        <div style="display: none;">
            <asp:HyperLink ID="HyperLink2" runat="server" />
        </div>
        <asp:Panel ID="pnlQuckLoad" runat="server">
            <div class="starail-popup">
                <div class="starail-YourBooking">
                    <h2>
                        Passenger Information</h2>
                    <div style="max-height: 300px; overflow-y: scroll; display: block !important;">
                        <asp:Repeater ID="rptPassengerDetails" runat="server" OnItemDataBound="rptPassengerDetails_ItemDataBound">
                            <HeaderTemplate>
                                <div class="starail-YourBooking-table">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="starail-YourBooking-ticketDetails">
                                    <div class="starail-YourBooking-col headerwidth">
                                        <lable class="table-lable">Type</lable>
                                        <asp:Label ID="lblType" runat="server" Text='<%#Eval("Type") %>'></asp:Label>
                                    </div>
                                    <div class="starail-YourBooking-col headerwidth2">
                                        <lable class="table-lable">Title</lable>
                                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="starail-Form-select selectlist"
                                            TabIndex='<%# 95 + (Container.ItemIndex)*6 %>'>
                                            <asp:ListItem Value="0">Title</asp:ListItem>
                                            <asp:ListItem Value="1">Dr.</asp:ListItem>
                                            <asp:ListItem Value="2">Mr.</asp:ListItem>
                                            <asp:ListItem Value="3">Miss</asp:ListItem>
                                            <asp:ListItem Value="4">Mrs.</asp:ListItem>
                                            <asp:ListItem Value="5">Ms.</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqvalerror0" runat="server" ControlToValidate="ddlTitle"
                                            InitialValue="0" Display="Dynamic" ValidationGroup="cntnu" />
                                    </div>
                                    <div class="starail-YourBooking-col">
                                        <lable class="table-lable">First Name</lable>
                                        <asp:TextBox ID="txtFirstname" runat="server" MaxLength="15" CssClass="starail-Form-input"
                                            TabIndex='<%# 96 + (Container.ItemIndex)*6 %>' placeholder="First Name" Text='<%#Eval("FName") %>' />
                                        <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ControlToValidate="txtFirstname"
                                            Display="Dynamic" ValidationGroup="cntnu" />
                                    </div>
                                    <div class="starail-YourBooking-col">
                                        <lable class="table-lable">Last Name</lable>
                                        <asp:TextBox ID="txtLastname" runat="server" MaxLength="25" CssClass="starail-Form-input"
                                            TabIndex='<%# 97 + (Container.ItemIndex)*6 %>' placeholder="Last Name" Text='<%#Eval("LName") %>' />
                                        <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ControlToValidate="txtLastname"
                                            Display="Dynamic" ValidationGroup="cntnu" />
                                    </div>
                                    <div class="starail-YourBooking-col" style="display: none;">
                                        <lable class="table-lable custline">Railcard <div class="showhide-railcard"><asp:RadioButton ID="rdnRailCardYes" runat="server" Text="Yes" GroupName="RdnRailCard" CssClass="rail-card" />
                                    <asp:RadioButton ID="rdnRailCardNo" runat="server" Text="No" GroupName="RdnRailCard" CssClass="rail-card" Checked="true" /></div></lable>
                                        <asp:DropDownList ID="ddlRailCard" runat="server" CssClass="starail-Form-select selectlist2 hide-railcard"
                                            TabIndex='<%# 99 + (Container.ItemIndex)*6 %>'>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="starail-YourBooking-col headerwidth3" style="display: none;">
                                        <lable class="table-lable">Lead</lable>
                                        <asp:RadioButton ID="rdnLeadPassenger" runat="server" GroupName="lead" TabIndex='<%# 100 + (Container.ItemIndex)*6 %>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <asp:Button ID="btnContinue" runat="server" CssClass="starail-Button starail-Form-button submitbuttons"
                        TabIndex="101" Text="Continue" ValidationGroup="cntnu" OnClick="btnContinue_Click"
                        OnClientClick="ShowGroupDiscountPopup(event);" />
                    <asp:LinkButton ID="btnCloseWin" runat="server" Text="Cancel" CssClass="cancelbutton"></asp:LinkButton>
                </div>
            </div>
        </asp:Panel>
        <%--Ti--%>
        <%-- Evolvi Group Saving Discout Popup --%>
        <div class="starail-popup group-discount-popup" id="group_discount_popup" style="display: none;">
            <div class="starail-YourBooking">
                <h2>
                    Group Fare Warning</h2>
                <div class="starail-YourBooking-ticketDetails groput-discount new-inner-section">
                    <div class="starail-YourBooking-col groput-discount">
                        <p>
                            The selected fare has a group discount applied. Please note that under the terms
                            and conditions of the discount, all passengers travelling on a ticket with a group
                            saving must travel together.</p>
                    </div>
                    <div class="starail-YourBooking-col groput-discount">
                        <p>
                            If the conditions of this fare are not suitable, please go to the Travellers page,
                            un-tick the “Travelling Together” box, and re-submit your fare search – the Groupsave
                            discount will not be applied.</p>
                    </div>
                    <div class="starail-YourBooking-col groput-discount">
                        <p>
                            Calendar restrictions apply to the use of Groupsave tickets. Before purchasing your
                            tickets, we advise you to check that there are no restrictions in place which would
                            invalidate them.</p>
                    </div>
                    <div class="starail-YourBooking-col">
                        Click <a href="http://www.nationalrail.co.uk/times_fares/groupsave_calendar.html"
                            target="_blank">here</a> to view the current calendar restrictions on the National
                        Rail website.
                    </div>
                </div>
                <div class="starail-YourBooking-col groput-discount">
                    <p>
                        <input type="checkbox" class="group-class" />
                        <span>I have read and accept the conditions above.</span>
                    </p>
                </div>
                <button type="button" id="btnGroupSubmit" disabled="disabled" class="starail-Button starail-Form-button"
                    style="background-color: rgba(148, 30, 52, 0.76);">
                Submit</utton></div>
        </div>
        <%-- End Evolvi Group Saving Discout Popup --%>
        <%-- Evolvi Fare Validity PopUp --%>
        <div class="starail-popup fare-validity-popup" id="fare_validity_code" style="display: none;">
            <div class="starail-YourBooking">
                <h2>
                    Fare Validity</h2>
                <div>
                    <a href='#' class='starail-farevalidity-close js-farevalidityClose'><i class='starail-Icon starail-Icon-close'>
                    </i></a>
                </div>
                <div class="starail-YourBooking-ticketDetails groput-discount new-inner-section-fare-validity fare-validity-show">
                </div>
            </div>
        </div>
        <asp:ModalPopupExtender ID="mdpexQuickLoad" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="HyperLink2" />
        <%-- End Evolvi Fare Validity PopUp --%>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    $(document).ready(function () {
        checkTermAndCondition();
        showHideRailCardDdl();
        checkInBoundJourneyFare();
        checkOutBoundJourneyFare();
        $(".starail-JourneyBlock-more-trigger-evolvi").on("click", function (t) {
            $(this).parent().parent().find(".starail-JourneyBlock-journeyDetails-evolvi").show();
            t.preventDefault();
            $(this).parent().find(".starail-JourneyBlock-more-trigger-evolvi").hide();
            $(this).parent().find(".starail-JourneyBlock-less-trigger-evolvi").show();
        });
        $(".starail-JourneyBlock-less-trigger-evolvi").on("click", function (t) {
            $(this).parent().parent().find(".starail-JourneyBlock-journeyDetails-evolvi").hide();
            t.preventDefault();
            $(this).parent().find(".starail-JourneyBlock-less-trigger-evolvi").hide();
            $(this).parent().find(".starail-JourneyBlock-more-trigger-evolvi").show();
        });

        //change ddl price value in mobile 
        $('.starail-JourneyBlock-mobileTickets').change(function () {
            getOutBoundmobilevalue();
            getInBoundmobilevalue();
        });

        $('.js-farevalidityClose').click(function () {
            $('[id*=fare_validity_code]').hide();
        });

        updateJourneyFarePrice();
        $('input[type=radio]').change(function () {
            updateJourneyFarePrice();
        });
    });

    function updateJourneyFarePrice() {
        var totalprice = 0;
        var inprice = 0;
        var outprice = 0;

        if ($('input[id=rdoFromEVOLVI]:checked').val() != undefined && $('input[id=rdoToEVOLVI]:checked').val() != undefined) {
            if ($('input[id=rdoFromEVOLVI]:checked').parent().hasClass('return') && $('input[id=rdoToEVOLVI]:checked').parent().hasClass('return')) {
                var strf = $('input[id=rdoFromEVOLVI]:checked').val();
                var arrayf = strf.split(",");
                outprice = parseFloat(arrayf[6]).toFixed(2);
                inprice = parseFloat(inprice).toFixed(2);
                $('[id*=lbloutPrice]').text(outprice);
                $('[id*=lblinPrice]').text(inprice);
                totalprice = parseFloat(outprice) + parseFloat(inprice);
                $('[id*=lblTotalPrice]').text(totalprice.toFixed(2));
            }
            else {
                var strf1 = $('input[id=rdoFromEVOLVI]:checked').val();
                var arrayf1 = strf1.split(",");
                outprice = parseFloat(arrayf1[6]).toFixed(2);

                var strf2 = $('input[id=rdoToEVOLVI]:checked').val();
                var arrayf2 = strf2.split(",");
                inprice = parseFloat(arrayf2[6]).toFixed(2);

                $('[id*=lbloutPrice]').text(outprice);
                $('[id*=lblinPrice]').text(inprice);
                totalprice = parseFloat(outprice) + parseFloat(inprice);
                $('[id*=lblTotalPrice]').text(totalprice.toFixed(2));
            }
        }
        else if ($('input[id=rdoFromEVOLVI]:checked').val() != undefined) {
            var strf = $('input[id=rdoFromEVOLVI]:checked').val();
            var arrayf = strf.split(",");
            outprice = parseFloat(arrayf[6]).toFixed(2);
            inprice = parseFloat(inprice).toFixed(2);
            $('[id*=lbloutPrice]').text(outprice);
            $('[id*=lblinPrice]').text(inprice);
            totalprice = parseFloat(outprice) + parseFloat(inprice);
            $('[id*=lblTotalPrice]').text(totalprice.toFixed(2));
        }
        else if ($('input[id=rdoToEVOLVI]:checked').val() != undefined) {
            var strf = $('input[id=rdoToEVOLVI]:checked').val();
            var arrayf = strf.split(",");
            outprice = parseFloat(outprice).toFixed(2);
            inprice = parseFloat(arrayf[6]).toFixed(2);
            $('[id*=lbloutPrice]').text(outprice);
            $('[id*=lblinPrice]').text(inprice);
            totalprice = parseFloat(outprice) + parseFloat(inprice);
            $('[id*=lblTotalPrice]').text(totalprice.toFixed(2));
        }
        else {
            $('[id*=lbloutPrice]').text(outprice);
            $('[id*=lblinPrice]').text(inprice);
            totalprice = parseFloat(outprice) + parseFloat(inprice);
            $('[id*=lblTotalPrice]').text(totalprice.toFixed(2));
        }
    }
</script>
<script type="text/javascript">
    function SendJCodeAndServiceID() {
        if ($("#MainContent_ucSResultEvolvi_hdnReq").val() == "EV") {
            var SDataVal = 'The cancellation penalty breakdown is as follows:20% penalty to Trenitalia, and 10% processing fee to ItaliaRail for handling and processing the cancellation.';
            $("#ShowCurrentDetail").html(SDataVal.toString());
        }

        //--Evolvi
        if (typeof ($('input[id=rdoFromEVOLVI]:checked').val()) != "undefined") {
            var strFrom = $('input[id=rdoFromEVOLVI]:checked').val();
            var strFromEvolvi = strFrom.split(",");

            var fareIdFrom = strFromEvolvi[0];
            var trainNoFrom = strFromEvolvi[1];
            var fareClassFrom = strFromEvolvi[2];
            var singleOrDefaultFrom = strFromEvolvi[3];

            var restrictionCodeFrom = strFromEvolvi[4];
            var classCodeFrom = strFromEvolvi[5];
            var priceFrom = strFromEvolvi[6];
            var idFrom = strFromEvolvi[7];
            var ticketTypeCodeFrom = strFromEvolvi[8];
            var validityCodeFrom = strFromEvolvi[9];
            var fareSetterCodeFrom = strFromEvolvi[10];

            var RefundAndCancellationsFrom = strFromEvolvi[11];
            var ChangesToTravelPlansFrom = strFromEvolvi[12];

            $("#MainContent_ucSResultEvolvi_hdnFareIdFrom").val(fareIdFrom);
            $("#MainContent_ucSResultEvolvi_hdnTrainNoFrom").val(trainNoFrom);
            $("#MainContent_ucSResultEvolvi_hdnFareClassFrom").val(fareClassFrom);
            $("#MainContent_ucSResultEvolvi_hdnSingleOrDefaultFrom").val(singleOrDefaultFrom);

            $("#MainContent_ucSResultEvolvi_hdnRestrictionCodeFrom").val(restrictionCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnClassCodeFrom").val(classCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnpriceFrom").val(priceFrom);
            $("#MainContent_ucSResultEvolvi_hdnIdFrom").val(idFrom);
            $("#MainContent_ucSResultEvolvi_hdnTicketTypeCodeFrom").val(ticketTypeCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnValidityCodeFrom").val(validityCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnFareSetterCodeFrom").val(fareSetterCodeFrom);

            $("#MainContent_ucSResultEvolvi_hdnRefundAndCancellationsFrom").val(RefundAndCancellationsFrom);
            $("#MainContent_ucSResultEvolvi_hdnChangesToTravelPlansFrom").val(ChangesToTravelPlansFrom);
        }

        if (typeof ($('input[id=rdoToEVOLVI]:checked').val()) != "undefined") {
            var strTo = $('input[id=rdoToEVOLVI]:checked').val();
            var strToEvolvi = strTo.split(",");

            var fareIdTo = strToEvolvi[0];
            var trainNoTo = strToEvolvi[1];
            var fareClassTo = strToEvolvi[2];
            var singleOrDefaultTo = strToEvolvi[3];

            var restrictionCodeTo = strToEvolvi[4];
            var classCodeTo = strToEvolvi[5];
            var priceTo = strToEvolvi[6];
            var idTo = strToEvolvi[7];
            var ticketTypeCodeTo = strToEvolvi[8];
            var validityCodeTo = strToEvolvi[9];
            var fareSetterCodeTo = strToEvolvi[10];

            var RefundAndCancellationsTo = strFromEvolvi[11];
            var ChangesToTravelPlansTo = strFromEvolvi[12];

            $("#MainContent_ucSResultEvolvi_hdnFareIdTo").val(fareIdTo);
            $("#MainContent_ucSResultEvolvi_hdnTrainNoTo").val(trainNoTo);
            $("#MainContent_ucSResultEvolvi_hdnFareClassTo").val(fareClassTo);
            $("#MainContent_ucSResultEvolvi_hdnSingleOrDefaultTo").val(singleOrDefaultTo);

            $("#MainContent_ucSResultEvolvi_hdnRestrictionCodeTo").val(restrictionCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnClassCodeTo").val(classCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnpriceTo").val(priceTo);
            $("#MainContent_ucSResultEvolvi_hdnIdTo").val(idTo);
            $("#MainContent_ucSResultEvolvi_hdnTicketTypeCodeTo").val(ticketTypeCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnValidityCodeTo").val(validityCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnFareSetterCodeTo").val(fareSetterCodeTo);

            $("#MainContent_ucSResultEvolvi_hdnRefundAndCancellationsTo").val(RefundAndCancellationsTo);
            $("#MainContent_ucSResultEvolvi_hdnChangesToTravelPlansTo").val(ChangesToTravelPlansTo);
        }
        checkSelectedPrice();
        return false;
    }

    function checkSelectedPrice() {
        var count = 0;
        $('.csscount').each(function () {
            count++;
        });

        if (count == 2) {
            if ($('input[id=rdoFromEVOLVI]:checked').val() && $('input[id=rdoToEVOLVI]:checked').val())
                $find('MainContent_ucSResultEvolvi_mdpexQuickLoad').show();
            else if ($('input[id=rdoFromEVOLVI]:checked').val()) {
                $('.starail-TrainOptions-outbound').removeClass('starail-Tabs-tab--active');
                $('.starail-TrainOptions-inbound').removeClass('starail-Tabs-tab--active').addClass('starail-Tabs-tab--active');
                $('#outbound').removeClass('starail-Tabs-content--hidden').addClass('starail-Tabs-content--hidden');
                $('#inbound').removeClass('starail-Tabs-content--hidden');
            }
            else if ($('input[id=rdoToEVOLVI]:checked').val()) {
                $('.starail-TrainOptions-outbound').removeClass('starail-Tabs-tab--active').addClass('starail-Tabs-tab--active');
                $('.starail-TrainOptions-inbound').removeClass('starail-Tabs-tab--active');
                $('#outbound').removeClass('starail-Tabs-content--hidden');
                $('#inbound').removeClass('starail-Tabs-content--hidden').addClass('starail-Tabs-content--hidden');
            }
        }
        if (count == 1 && $('input[id=rdoFromEVOLVI]:checked').val()) {
            $find('MainContent_ucSResultEvolvi_mdpexQuickLoad').show();
        }
        if ($('input[id=rdoToEVOLVI]:checked').val() == false && $('input[id=rdoFromEVOLVI]:checked').val() == false) {
            alert("Pelase select at least one price");
        }
    }

    function SendJCodeAndServiceIDMobile(strf) {
        var arrayf = strf.split(",");
        if (arrayf[11] == 'outbound') {

            var fareIdFrom = arrayf[0];
            var trainNoFrom = arrayf[1];
            var fareClassFrom = arrayf[2];
            var singleOrDefaultFrom = arrayf[3];

            var restrictionCodeFrom = arrayf[4];
            var classCodeFrom = arrayf[5];
            var priceFrom = arrayf[6];
            var idFrom = arrayf[7];
            var ticketTypeCodeFrom = arrayf[8];
            var validityCodeFrom = arrayf[9];
            var fareSetterCodeFrom = arrayf[10];
            var RefundAndCancellationsFrom = arrayf[11];
            var ChangesToTravelPlansFrom = arrayf[12];


            $("#MainContent_ucSResultEvolvi_hdnFareIdFrom").val(fareIdFrom);
            $("#MainContent_ucSResultEvolvi_hdnTrainNoFrom").val(trainNoFrom);
            $("#MainContent_ucSResultEvolvi_hdnFareClassFrom").val(fareClassFrom);
            $("#MainContent_ucSResultEvolvi_hdnSingleOrDefaultFrom").val(singleOrDefaultFrom);

            $("#MainContent_ucSResultEvolvi_hdnRestrictionCodeFrom").val(restrictionCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnClassCodeFrom").val(classCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnpriceFrom").val(priceFrom);
            $("#MainContent_ucSResultEvolvi_hdnIdFrom").val(idFrom);
            $("#MainContent_ucSResultEvolvi_hdnTicketTypeCodeFrom").val(ticketTypeCodeFrom);

            $("#MainContent_ucSResultEvolvi_hdnValidityCodeFrom").val(validityCodeFrom);
            $("#MainContent_ucSResultEvolvi_hdnFareSetterCodeFrom").val(fareSetterCodeFrom);

            $("#MainContent_ucSResultEvolvi_hdnRefundAndCancellationsFrom").val(RefundAndCancellationsFrom);
            $("#MainContent_ucSResultEvolvi_hdnChangesToTravelPlansFrom").val(ChangesToTravelPlansFrom);
        }
        if (arrayf[11] == 'inbound') {
            var fareIdTo = arrayf[0];
            var trainNoTo = arrayf[1];
            var fareClassTo = arrayf[2];
            var singleOrDefaultTo = arrayf[3];

            var restrictionCodeTo = arrayf[4];
            var classCodeTo = arrayf[5];
            var priceTo = arrayf[6];
            var idTo = arrayf[7];
            var ticketTypeCodeTo = arrayf[8];
            var validityCodeTo = arrayf[9];
            var fareSetterCodeTo = arrayf[10];

            var RefundAndCancellationsTo = arrayf[11];
            var ChangesToTravelPlansTo = arrayf[12];

            $("#MainContent_ucSResultEvolvi_hdnFareIdTo").val(fareIdTo);
            $("#MainContent_ucSResultEvolvi_hdnTrainNoTo").val(trainNoTo);
            $("#MainContent_ucSResultEvolvi_hdnFareClassTo").val(fareClassTo);
            $("#MainContent_ucSResultEvolvi_hdnSingleOrDefaultTo").val(singleOrDefaultTo);

            $("#MainContent_ucSResultEvolvi_hdnRestrictionCodeTo").val(restrictionCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnClassCodeTo").val(classCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnpriceTo").val(priceTo);
            $("#MainContent_ucSResultEvolvi_hdnIdTo").val(idTo);
            $("#MainContent_ucSResultEvolvi_hdnTicketTypeCodeTo").val(ticketTypeCodeTo);

            $("#MainContent_ucSResultEvolvi_hdnValidityCodeTo").val(validityCodeTo);
            $("#MainContent_ucSResultEvolvi_hdnFareSetterCodeTo").val(fareSetterCodeTo);

            $("#MainContent_ucSResultEvolvi_hdnRefundAndCancellationsTo").val(RefundAndCancellationsTo);
            $("#MainContent_ucSResultEvolvi_hdnChangesToTravelPlansTo").val(ChangesToTravelPlansTo);
        }
        checkSelectedPrice();
        return false;
    }
</script>
<script src="Scripts/gmap.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en" type="text/javascript"></script>
<script type="text/javascript">
    var geocoder;
    var maps;
    $(window).load(function () {
        $(".showhide-railcard").hide(); $(".showhide-railcard:first").css("display", " -webkit-inline-box");
        LoadMap();
    });
    function LoadMap() {
        var start = document.getElementById('txtFrom').value;
        start = start.trim().replace('(any)', '');
        start = start.trim().replace(/[^a-z0-9]+/gi, ' ');
        start = start + " uk";

        var end = document.getElementById('txtTo').value;
        end = end.trim().replace('(any)', '');
        end = end.trim().replace(/[^a-z0-9]+/gi, ' ');
        end = end + " uk";

        var geocoder = new window.google.maps.Geocoder();
        initializeGMap();

        var FromLatitude = '<%=FromLatitude%>';
        var FromLongitude = '<%=FromLongitude%>';
        var ToLatitude = '<%=ToLatitude%>';
        var ToLongitude = '<%=ToLongitude%>';

        if (FromLatitude != '0.0000000000' && FromLongitude != '0.0000000000') {
            var fromstart = new google.maps.LatLng(parseFloat(FromLatitude), parseFloat(FromLongitude));
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': fromstart }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
        }
        else {
            geocoder.geocode({ 'address': start }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                    drawPolyline();
                }
            });
        }

        if (ToLatitude != '0.0000000000' && ToLongitude != '0.0000000000') {
            var toend = new google.maps.LatLng(parseFloat(ToLatitude), parseFloat(ToLongitude));
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': toend }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }
        else {
            geocoder.geocode({ 'address': end }, function (results, status) {
                if (status == window.google.maps.GeocoderStatus.OK) {
                    setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                    drawPolyline();
                }
            });
        }
    }
</script>
<script type="text/javascript">
    function getOutBoundmobilevalue() {
        $("#outbound").find(".starail-JourneyBlock").each(function (key, value) {
            if ($(this).find("#MainContent_ucSResultEvolvi_rptoutEvolvi_ddlFromPrice_" + key + " option:selected").length > 0) {
                var pricevalue = $(this).find("#MainContent_ucSResultEvolvi_rptoutEvolvi_ddlFromPrice_" + key + " option:selected").val();
                pricevalue = pricevalue + ",outbound";
                $(this).find("#MainContent_ucSResultEvolvi_rptoutEvolvi_hlnkContinue_" + key).attr("onclick", "return SendJCodeAndServiceIDMobile('" + pricevalue + "')");
            }
        });
    }

    function getInBoundmobilevalue() {
        $("#inbound").find(".starail-JourneyBlock").each(function (key, value) {
            if ($(this).find("#MainContent_ucSResultEvolvi_rptinEvolvi_ddlFromPrice_" + key + " option:selected").length > 0) {
                var pricevalue = $(this).find("#MainContent_ucSResultEvolvi_rptinEvolvi_ddlFromPrice_" + key + " option:selected").val();
                pricevalue = pricevalue + ",inbound";
                $(this).find("#MainContent_ucSResultEvolvi_rptinEvolvi_hlnkContinue_" + key).attr("onclick", "return SendJCodeAndServiceIDMobile('" + pricevalue + "')");
            }
        });
    }

    function checkFirstRadioButton() {
        $('#MainContent_ucSResultEvolvi_pnlQuckLoad').find('[id*=rdnLeadPassenger]:first').prop("checked", true);
    }

    function SetUniqueRadioButton(nameregex, current) {
        re = new RegExp(nameregex);
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i];
            if (elm.type == 'radio') {
                if (re.test(elm.name)) {
                    elm.checked = false;
                }
            }
        }
        current.checked = true;
    }

    function showHideRailCardDdl() {
        $('[id*=rdnRailCard]').click(function () {
            if ($('#MainContent_ucSResultEvolvi_rptPassengerDetails_rdnRailCardYes_0').is(':checked')) {
                $('[id*=ddlRailCard]').removeClass('hide-railcard');
            }
            else {
                $('[id*=ddlRailCard]').addClass('hide-railcard');
            }
        });
    }

    function activeInactiveTab() {
        $('.active-inactive-tab').find('li:first').removeClass('starail-Tabs-tab--active');
        $('.active-inactive-tab').find('li:last').addClass('starail-Tabs-tab--active');
        $('#starail-routemap').addClass('starail-Tabs-content--hidden');
        $('#starail-edittickets').removeClass('starail-Tabs-content--hidden');
        $('[id*=chkAddRailCard]').attr('checked', true);
        bindRailcard('click');
    }
</script>
<script type="text/javascript">
    function checkInBoundJourneyFare() {
        var inprice = 0;
        $('#inbound').find('input[type="radio"]').change(function (key, value) {
            var inBoundFareId = $(this).parents().attr('fareid');
            var inBoundFareClass = '';
            if ($(this).parents().parents().hasClass('return')) {
                inBoundFareClass = 'return';
            }
            else if ($(this).parents().parents().hasClass('single')) {
                inBoundFareClass = 'single';
            }

            if ($('#outbound').find('input[type="radio"]').is(':checked')) {
                if ($('#outbound').find('input[type="radio"]:checked').parents().parents().hasClass('return')) {
                    var fareid = $('#outbound').find('input[type="radio"]:checked').parents().attr('fareid');
                    if (inBoundFareClass != 'return') {
                        alert('Please select return fare with same class in inbound journey.');
                        $(this).attr('checked', false);
                        $('#MainContent_ucSResultEvolvi_lblinPrice').text(inprice.toFixed(2));
                    }
                    else if (inBoundFareId != fareid && inBoundFareClass == 'return') {
                        alert('Please select return fare with same class in inbound journey.');
                        $(this).attr('checked', false);
                        $('#MainContent_ucSResultEvolvi_lblinPrice').text(inprice.toFixed(2));
                    }
                    else {
                        $('#MainContent_ucSResultEvolvi_lblinPrice').text(inprice.toFixed(2));
                    }
                }
                else if ($('#outbound').find('input[type="radio"]:checked').parents().hasClass('single')) {
                    if (inBoundFareClass != 'single') {
                        alert('Please select dual single fare with same class in inbound journey.');
                        $(this).attr('checked', false);
                        $('#MainContent_ucSResultEvolvi_lblinPrice').text(inprice.toFixed(2));
                    }
                }
            }

            if ($('#MainContent_ucSResultEvolvi_lblinPrice').length <= 0) {
                $('#MainContent_ucSResultEvolvi_lblTotalPrice').text((parseFloat($('#MainContent_ucSResultEvolvi_lbloutPrice').text())).toFixed(2));
            }
            else {
                $('#MainContent_ucSResultEvolvi_lblTotalPrice').text((parseFloat($('#MainContent_ucSResultEvolvi_lbloutPrice').text()) + parseFloat($('#MainContent_ucSResultEvolvi_lblinPrice').text())).toFixed(2));
            }

            checkuncheckRadioText();
        });
    }

    function checkOutBoundJourneyFare() {
        var inprice = 0;
        $('#outbound').find('input[type="radio"]').change(function (key, value) {
            var outBoundFareId = $(this).parents().attr('fareid');
            var outBoundFareClass = '';
            if ($(this).parents().parents().hasClass('return')) {
                outBoundFareClass = 'return';
            }
            else if ($(this).parents().parents().hasClass('single')) {
                outBoundFareClass = 'single';
            }

            if ($('#inbound').find('input[type="radio"]').is(':checked')) {
                if ($('#inbound').find('input[type="radio"]:checked').parents().parents().hasClass('return')) {
                    var fareid = $('#inbound').find('input[type="radio"]:checked').parents().attr('fareid');
                    if (outBoundFareClass != 'return') {
                        alert('Please select return fare with same class in outbound journey.');
                        $(this).attr('checked', false);
                        $('#MainContent_ucSResultEvolvi_lbloutPrice').text(inprice.toFixed(2));
                    }
                    else if (outBoundFareId != fareid && outBoundFareClass == 'return') {
                        alert('Please select return fare with same class in outbound journey.');
                        $(this).attr('checked', false);
                        $('#MainContent_ucSResultEvolvi_lbloutPrice').text(inprice.toFixed(2));
                    }
                    else {
                        $('#MainContent_ucSResultEvolvi_lbloutPrice').text(inprice.toFixed(2));
                    }
                }
                else if ($('#inbound').find('input[type="radio"]:checked').parents().hasClass('single')) {
                    if (outBoundFareClass != 'single') {
                        alert('Please select dual single fare with same class in outbound journey.');
                        $(this).attr('checked', false);
                        $('#MainContent_ucSResultEvolvi_lbloutPrice').text(inprice.toFixed(2));
                    }
                }
            }

            if ($('#MainContent_ucSResultEvolvi_lblinPrice').length <= 0) {
                $('#MainContent_ucSResultEvolvi_lblTotalPrice').text((parseFloat($('#MainContent_ucSResultEvolvi_lbloutPrice').text())).toFixed(2));
            }
            else {
                $('#MainContent_ucSResultEvolvi_lblTotalPrice').text((parseFloat($('#MainContent_ucSResultEvolvi_lbloutPrice').text()) + parseFloat($('#MainContent_ucSResultEvolvi_lblinPrice').text())).toFixed(2));
            }

            checkuncheckRadioText();
        });
    }

    function checkuncheckRadioText() {
        if (($('#outbound').find('input[type="radio"]').length > 0) && ($('#inbound').find('input[type="radio"]').length > 0)) {
            if (($('#outbound').find('input[type="radio"]').is(':checked')) && ($('#inbound').find('input[type="radio"]').is(':checked'))) {
                $('#outbound').find('[id*=btnContinueBooking]').text('Booking Details');
                $('#inbound').find('[id*=btnContinueBooking]').text('Booking Details');
            }
            else {
                $('#outbound').find('[id*=btnContinueBooking]').text('Inbound Journey');
                $('#inbound').find('[id*=btnContinueBooking]').text('Outbound Journey');
            }
        }
    }

    function ShowGroupDiscountPopup(event) {
        var isGroupSavingDiscount = false;
        if (($('#outbound').find('input[type="radio"]').length > 0) && ($('#inbound').find('input[type="radio"]').length > 0)) {
            if (($('#outbound').find('input[type="radio"]:checked').parent().parent().parent().find('#hdnGroupDiscountCode').val() == "GS3") || ($('#inbound').find('input[type="radio"]:checked').parent().parent().parent().find('#hdnGroupDiscountCode').val() == "GS3")) {
                isGroupSavingDiscount = true;
            }
        }
        else if ($('#outbound').find('input[type="radio"]').length > 0) {
            if ($('#outbound').find('input[type="radio"]:checked').parent().parent().parent().find('#hdnGroupDiscountCode').val() == "GS3") {
                isGroupSavingDiscount = true;
            }
        }
        if ((isGroupSavingDiscount) && (!$('.group-class').is(':checked'))) {
            $('#group_discount_popup').show();
            event.preventDefault();
        }
    }

    function checkTermAndCondition() {
        $('[id*=btnGroupSubmit]').click(function () {
            $('#group_discount_popup').hide();
        });

        $('.group-class').change(function () {
            if ($('.group-class').is(':checked')) {
                $('[id*=btnGroupSubmit]').removeAttr("disabled");
                $('[id*=btnGroupSubmit]').css("background-color", "rgb(148, 30, 52);");
            }
            else {
                $('[id*=btnGroupSubmit]').attr("disabled", "disabled");
                $('[id*=btnGroupSubmit]').css("background-color", "rgba(148, 30, 52, 0.76);");
            }
        });

        $('[id*=btnCloseWin]').click(function () {
            $('[id*=btnGroupSubmit]').attr("disabled", "disabled");
            $('.group-class').attr("checked", false);
            $('[id*=btnGroupSubmit]').css("background-color", "rgba(148, 30, 52, 0.76);");
        });
    }

    function getValidityCode(code) {
        var hostName = window.location.host;
        var url = "http://" + hostName;

        if (window.location.toString().indexOf("https:") >= 0)
            url = "https://" + hostName;
        if (hostName == "localhost") {
            url = url + "/1TrackNew";
        }
        var hostUrl = url + "/StationList.asmx/GetValidityCodeHtml";
        $.ajax({
            type: "POST",
            url: hostUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: "{'validityCode':'" + code + "'}",
            success: function (msg) {
                $('[id*=fare_validity_code]').find('.fare-validity-show').html(msg.d);
                $('[id*=fare_validity_code]').show();
                $('.relatedValidity').click(function () {
                    getValidityCode($(this).text());
                });
            },
            error: function () {
            }
        });
    }
</script>
