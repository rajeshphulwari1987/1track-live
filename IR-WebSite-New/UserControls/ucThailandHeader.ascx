﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucThailandHeader.ascx.cs"
    Inherits="UserControls_ucThailandHeader" %>
<div class="page-wrap">
    <div class="navbarbg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <asp:Panel ID="pnlErrSuccess" runat="server">
                        <div id="DivSuccess" runat="server" class="success" style="display: none;">
                            <asp:Label ID="lblSuccessMsg" runat="server" /></div>
                        <div id="DivError" runat="server" class="error" style="display: none;">
                            <asp:Label ID="lblErrorMsg" runat="server" CssClass="starail-Form-required" />
                        </div>
                    </asp:Panel>
                    <ul class="top_link">
                        <li><a href='<%=siteURL%>Faq'>Faq’s </a></li>
                        <li><a href='<%=siteURL%>feedback'>Feedback </a></li>
                        <li><a href='<%=siteURL%>irsitemap'>Sitemap </a></li>
                        <li><a href='<%=siteURL%>security'>Security </a></li>
                    </ul>
                    <div class="dropdown cart_link">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" onclick="cartDetail()">
                            <i class="fa fa-shopping-cart"></i><span class="badge">
                                <%=BucketItem%></span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu1" id="dropdownMenuHover">
                            You have
                            <%=BucketItem%>
                            item added.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo ">
                <a href="<%=siteURL%>">
                    <img src="<%=SiteLogoUrl %>" alt="logo"></a>
            </div>
            <div style="top:8px;position: relative;">
                <%=Session["AttentionBody"] != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(Session["AttentionBody"].ToString()) : string.Empty%>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right ">
                <span class="phone">
                    <div id="hour3" runat="server">
                    </div>
                    <div class="clearfix visible-xs">
                    </div>
                    <i>
                        <img src="<%=siteURL%>assets/img/icon_phone.png" alt=""></i>
                    <asp:Literal ID="ltrSitePhoneNumber" runat="server" />
                </span>
                <p class="callinfo" id="SiteHeadingTitle" runat="server">
                </p>
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle"
                    type="button">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span>
                </button>
            </div>
        </div>
    </div>
    <div class="navouter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                    <div class="navouter bdrbtm">
                        <div class="pull-right pos-rel agent_login" style='<%=IsLoggedIn ? "": "display: none" %>'>
                            <i class="fa fa-user fa-2x search "></i>
                            <div id="DivIRAgentGereet" runat="server" style="display: inline;">
                                <asp:Label ID="lblIRAgent" runat="server" />
                                !</div>
                            <a href="<%=siteURL%>Agent/Login">Logout</a>
                        </div>
                        <div class="clearfix visible-xs">
                        </div>
                        <div class=" collapse navbar-collapse pull-left">
                            <ul id="menuh" class="nav navbar-nav">
                                <li><a id="HMH" href='<%=siteURL %>' class="active"><span></span>HOME </a></li>
                                <li><a id="IrPrintQueue" runat="server" visible="false">PRINT QUEUE</a> </li>
                                <li><a id="IrFoc" runat="server" visible="false">FOC / AD75</a></li>
                                <li><a id="HMRT" href='<%=siteURL %>rail-tickets'><span></span>RAIL TICKETS </a>
                                </li>
                                <asp:Repeater ID="rptCountry" runat="server">
                                    <ItemTemplate>
                                        <li class="dropdown dropdown-large "><a id="HMC" href="#"><span></span>
                                            <%#Eval("Name").ToString().ToUpper() %></a>
                                            <ul class="dropdown-menu dropdown-menu-large row" style="overflow-y: auto; max-height: 350px;">
                                                <asp:Repeater ID="rptSubMenu" runat="server" DataSource='<%#Eval("MenuList") %>'>
                                                    <ItemTemplate>
                                                        <li class="col-sm-3">
                                                            <ul>
                                                                <li class="dropdown-header"><a href='<%=siteURL%><%#Eval("Url") %>'>
                                                                    <%#Eval("Name") %></a> </li>
                                                                <asp:Repeater ID="Repeater1" runat="server" DataSource='<%#Eval("SubMenuList") %>'>
                                                                    <ItemTemplate>
                                                                        <li><a href='<%=siteURL%><%#Eval("Url")%>'>
                                                                            <%#Eval("Name") %></a></li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="rptSpecialTrain" runat="server">
                                    <ItemTemplate>
                                        <li class="dropdown dropdown-large "><a id="HMST" href="<%=siteURL %>specialtrains">
                                            <span></span>
                                            <%#Eval("Name").ToString().ToUpper() %></a>
                                            <ul class="dropdown-menu dropdown-menu-large row" style="overflow-y: auto; max-height: 350px;">
                                                <asp:Repeater ID="rptSubMenu" runat="server" DataSource='<%#Eval("MenuList") %>'>
                                                    <ItemTemplate>
                                                        <li class="col-sm-3">
                                                            <ul>
                                                                <li class="dropdown-header"><a href='<%=siteURL%><%#Eval("Url") %>'>
                                                                    <%#Eval("Name") %></a> </li>
                                                                <asp:Repeater ID="Repeater1" runat="server" DataSource='<%#Eval("SubMenuList") %>'>
                                                                    <ItemTemplate>
                                                                        <li><a href='<%=siteURL%><%#Eval("Url") %>' title='<%#Eval("Name")%>'>
                                                                            <%#(Eval("Name").ToString().Length > 18) ? (Eval("Name").ToString().Substring(0, 18) + "...") : (Eval("Name").ToString())%>
                                                                            (<%#Eval("CountryCode")%>) </a></li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <li><a id="HMCU" href='<%=siteURL %>contact-us'><span></span>CONTACT US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
