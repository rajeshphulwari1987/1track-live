﻿using System;
using System.Globalization;
using System.Net;
using System.ServiceModel.Syndication;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class UserControls_feedburner : UserControl
{
    #region Declare Private Properties

    // Declare Feed Url from where data will be retrive.
    private string _feedUrl;
    public string FeedUrl
    {
        get { return _feedUrl; }
        set
        {
            _feedUrl = value;
        }
    }

    // Declare No of item to be displayed.
    private int _noOfItems;
    public int NoOfItems
    {
        get { return _noOfItems; }
        set { _noOfItems = value; }
    }

    // Declare Title Header of Feed.
    private string _feedTitleHeader;
    public string FeedTitleHeader
    {
        get { return _feedTitleHeader; }
        set { _feedTitleHeader = value; }
    }

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set Page size. 
        dataPagerFeeds.PageSize = 5;
        DisplayFeed();
    }

    #region User Define Method
    public void DisplayFeed()
    {
        try
        {
            // Read the feed using an XmlReader
            var reader = XmlReader.Create(FeedUrl);
            var feed = SyndicationFeed.Load(reader);

            if (feed != null) lstViewNewsFeeds.DataSource = feed.Items;
            lstViewNewsFeeds.DataBind();
            lblFeedTitle.Text = FeedTitleHeader;
        }
        catch (Exception ex)
        {
            if (ex is WebException || ex is XmlException)
            {
                // Handle bad url, timeout or xml error here.
            }
            else
                throw;
        }
    }

    /// <summary>
    /// Url Validation
    /// </summary>
    /// <param name="feedUrl">Feed Url</param>
    /// <returns>bool</returns>
    public bool IsValidUri(string feedUrl)
    {
        Uri validatedUri;
        return Uri.TryCreate(feedUrl, UriKind.RelativeOrAbsolute, out validatedUri);
    }

    #endregion

    /// <summary>
    /// Bound List view data item.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewNewsFeeds_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            // Instantiate listview control object.
            var lvDataItem = (ListViewDataItem)e.Item;

            //Make sure that data item is binding with listview.
            if (lvDataItem.ItemType == ListViewItemType.DataItem)
            {
                var item = e.Item.DataItem as SyndicationItem;
                //SyndicationLink feedItem = e.Item.DataItem as SyndicationLink;

                var panelNewsDescContent = (Panel)e.Item.FindControl("panelNewsDescription");
                var uriTitleLink = (HyperLink)e.Item.FindControl("hlNewsTitle");
                uriTitleLink.Target = "_blank";
                uriTitleLink.ForeColor = System.Drawing.Color.Blue;
                var lblLastUpdate = (Label)e.Item.FindControl("lblLastUpdateTime");
                if (uriTitleLink != null)
                {
                    uriTitleLink.Text = Convert.ToString(item.Title.Text);
                    uriTitleLink.NavigateUrl = item.Links[0].Uri.AbsoluteUri;
                }
                // Make sure that panel object is referenced.
                if (panelNewsDescContent != null)
                {
                    var lblBox = new Label();
                    lblBox.Text = Convert.ToString(item.Summary.Text);
                    panelNewsDescContent.Controls.Add(lblBox);
                }
                if (lblLastUpdate != null)
                {
                    lblLastUpdate.Text = item.PublishDate.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));
                }
            }
        }
        catch (Exception ee)
        {
            Response.Write(ee.Message);
        }
    }
}