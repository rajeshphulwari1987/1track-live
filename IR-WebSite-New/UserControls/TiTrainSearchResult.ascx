﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TiTrainSearchResult.ascx.cs"
    Inherits="UserControls_TiTrainSearchResult" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="ucTrainSearch.ascx" TagName="ucTrainSearch" TagPrefix="uc1" %>
<style type="text/css">
    #MainContent_ucSResult_pnlQuckLoad {
        z-index: 101 !important;
    }

    #MainContent_ucSResult_mdpexQuickLoad_backgroundElement {
        z-index: 100 !important;
    }

    .selectlist {
        width: 50%;
    }

    .selectlistmargin {
        margin-bottom: 10px;
    }

    .starail-BookingDetails-form {
        padding: 25px !important;
    }

    .top-arrow, .bottom-arrow {
        width: 25px;
        float: right;
        top: 20px;
        position: absolute;
        right: -26px;
    }

    .from-price {
        width: 56px;
    }

    a.bottom-arrow {
        margin-right: 18px;
    }

    a.top-arrow {
        margin-right: 18px;
    }

    .p-price-margin {
        margin-left: 5px;
        width: 100%;
    }

    .price-font {
        font-size: 20px !important;
        margin-left: 20px;
    }

    .pricemain-box {
        position: relative;
    }

    .pricebox-tag {
        position: absolute;
        width: 170px;
        text-align: center;
        right: -10px;
        top: 5px;
        height: 50px;
    }

        .pricebox-tag span {
            padding-left: 0px !important;
            width: 100%;
            display: inline-block;
            text-align: center !important;
            font-weight: bold;
            float: none !important;
            margin-left: 0;
        }

        .pricebox-tag img {
            margin: -16px auto 0;
            display: inline-block;
        }

    @media only screen and (max-width: 480px) {
        #MainContent_ucSResultTi_pnlQuckLoad {
            left: auto !important;
            width: 100% !important;
        }

        .starail-BookingDetails-form {
            padding: 5px !important;
        }

        .starail-Form-input, .starail-Form-textarea, .starail-Form-select {
            width: 100%;
        }
    }
</style>
<asp:UpdatePanel ID="upnlTrainSearch" runat="server">
    <ContentTemplate>
        <div class="starail-Grid starail-Grid--mobileFull">
            <div class="starail-Grid-col starail-Grid-col--nopadding">
                <div class="starail-ProgressBar starail-ProgressBar-stage--stage1 starail-u-hideMobile">
                    <div class="starail-ProgressBar-line">
                        <div class="starail-ProgressBar-progress">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage1">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Ticket Selection
                        </p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage2">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Booking Details
                        </p>
                        <div class="starail-ProgressBar-subStage">
                        </div>
                    </div>
                    <div class="starail-ProgressBar-stage starail-ProgressBar-stage3">
                        <div class="starail-ProgressBar-circle">
                        </div>
                        <p class="starail-ProgressBar-label">
                            Checkout
                        </p>
                    </div>
                </div>
                <asp:HiddenField ID="hdnsiteURL" runat="server" Value="" />
                <asp:HiddenField ID="hdnDateDepart" runat="server" Value="" />
                <asp:HiddenField ID="hdnDateArr" runat="server" Value="" />
                <asp:HiddenField ID="hdnCurrID" runat="server" Value="" />
                <asp:HiddenField ID="hdnReq" runat="server" Value="" />
                <asp:Panel ID="pnlErrSuccess" runat="server">
                    <div id="DivSuccess" runat="server" class="success" style="display: none;">
                        <asp:Label ID="lblSuccessMsg" runat="server" />
                    </div>
                    <div id="DivError" runat="server" class="error" style="display: none;">
                        <asp:Label ID="lblErrorMsg" runat="server" />
                    </div>
                </asp:Panel>
                <div class="starail-Section starail-Section--nopadding">
                    <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                        <div class="starail-TrainOptions-mobileNav-stage1">
                            <a href="home" class="starail-Button starail-Button--blue"><i class="starail-Icon-chevron-left"></i>Edit Search</a> <a class="starail-Button starail-TrainOptions-mobileNav-rtnJourney starail-rtnJourney-trigger starail-Button--blue is-disabled"
                                id="anchor_Next" runat="server">Next: Return <i class="starail-Icon-chevron-right"></i></a>
                        </div>
                        <div class="starail-TrainOptions-mobileNav-stage2 starail-u-hide">
                            <a class="starail-Button starail-Button--blue starail-TrainOptions-outbound-trigger">
                                <i class="starail-Icon-chevron-left"></i>Outbound</a> <a class="starail-Button starail-TrainOptions-mobileNav-booking starail-booking-trigger starail-Button--blue is-disabled">Your Details <i class="starail-Icon-chevron-right"></i></a>
                            <!-- will submit form -->
                        </div>
                    </div>
                    <div class="starail-Tabs starail-TrainOptions-mainTabs">
                        <ul class="starail-Tabs-tabs">
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-Tabs-tab--active starail-TrainOptions-outbound csscount">
                                <a href="#outbound" class="js-starail-Tabs-trigger">
                                    <div class="starail-TrainOptions-tab-container">
                                        <p class="starail-u-hideDesktop starail-TrainOptions-mobiletab-title">
                                            Your outbound journey
                                        </p>
                                        <p class="starail-TrainOptions-tab-cost starail-u-hideMobile">
                                            <%=currency %>
                                            <asp:Label ID="lbloutPrice" runat="server" Text="0.00" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-title">
                                            <asp:Literal ID="ltroutFrom" runat="server" />
                                            <i class="starail-Icon-direction-arrow"><span class="starail-u-visuallyHidden">to </span>
                                            </i>
                                            <asp:Literal ID="ltroutTo" runat="server" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-details">
                                            <span class="starail-u-hideMobile">Outbound </span><span class="starail-u-hideDesktopInline">Date: </span><span class="starail-TrainOptions-tab-details-date">
                                                <asp:Literal ID="ltroutDate" runat="server" />
                                            </span><span class="starail-u-hideMobile">| </span><span class="starail-TrainOptions-tab-details-time starail-TrainOptions-tab-details-time--init starail-u-hideMobile">(Choose time)</span>
                                        </p>
                                    </div>
                                </a></li>
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-TrainOptions-inbound csscount"
                                runat="server" id="ShowInboud"><a href="#inbound" class="js-starail-Tabs-trigger">
                                    <div class="starail-TrainOptions-tab-container">
                                        <p class="starail-u-hideDesktop starail-TrainOptions-mobiletab-title">
                                            Your inbound journey
                                        </p>
                                        <p class="starail-TrainOptions-tab-cost starail-u-hideMobile">
                                            <%=currency %>
                                            <asp:Label ID="lblinPrice" runat="server" Text="0.00" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-title">
                                            <asp:Literal ID="ltrinFrom" runat="server" />
                                            <i class="starail-Icon-direction-arrow"><span class="starail-u-visuallyHidden">to </span>
                                            </i>
                                            <asp:Literal ID="ltrinTo" runat="server" />
                                        </p>
                                        <p class="starail-TrainOptions-tab-details">
                                            <span class="starail-u-hideMobile">Inbound </span><span class="starail-u-hideDesktopInline">Date: </span><span class="starail-TrainOptions-tab-details-date">
                                                <asp:Literal ID="ltrinDate" runat="server" />
                                            </span><span class="starail-u-hideMobile">| </span><span class="starail-TrainOptions-tab-details-time starail-TrainOptions-tab-details-time--init starail-u-hideMobile">(Choose time)</span>
                                        </p>
                                    </div>
                                </a></li>
                            <li class="starail-Tabs-tab starail-Tabs-tab--trainOptions starail-TrainOptions-subtotal">
                                <div class="starail-TrainOptions-subtotal-details">
                                    <p class="starail-u-hideDesktopInline">
                                        Price:
                                    </p>
                                    <p class="starail-TrainOptions-subtotal-title starail-u-hideMobile" data-starail-subtotal="Subtotal"
                                        data-starail-total="Total Price">
                                        Subtotal
                                    </p>
                                    <p class="starail-TrainOptions-subtotal-cost">
                                        <%=currency %>
                                        <asp:Label ID="lblTotalPrice" runat="server" Text="0.00" />
                                    </p>
                                </div>
                                <p class="starail-TrainOptions-subtotal-passengers">
                                    For
                                    <asp:Literal ID="ltrPassenger" runat="server" />
                                </p>
                            </li>
                        </ul>
                        <div class="starail-Tabs-wrapper">
                            <div class="starail-Grid starail-Grid--nopadding starail-Grid--mobileFull">
                                <div class="starail-Grid-col starail-Grid-col--8of12">
                                    <div id="outbound" class="starail-Tabs-content journey-type">
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkOutTopEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkOutTopLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                        <div class="starail-TrainOptions-outboundError starail-Alert starail-Alert--error starail-u-hide">
                                            <p>
                                                Please select an outbound journey.
                                            </p>
                                        </div>
                                        <asp:Repeater ID="rptoutTi" runat="server" OnItemDataBound="rptoutTi_ItemDataBound"
                                            OnItemCommand="rptoutTi_ItemCommand">
                                            <ItemTemplate>
                                                <div class="starail-JourneyBlock">
                                                    <div class="starail-JourneyBlock-form">
                                                        <div class="starail-JourneyBlock-header">
                                                            <div class="starail-JourneyBlock-summary" style="width: 100%">
                                                                <div class="starail-DestinationIcon" style="height: 5px">
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow" style="min-width: 35%; float: left; padding-right: 1%;">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        From:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrFrom" />
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        To:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrTo" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-summary">
                                                                <div class="starail-DestinationIcon">
                                                                    <div class="starail-DestinationIcon-line">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Departs:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                                                                            </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Arrives:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                                                                            </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Changes:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <asp:Label ID="lblTrainChanges" runat="server" Text="" />
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Duration:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Eval("TotalJourneyTime")%></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-info starail-u-hideMobile pricemain-box">
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <a class="starail-JourneyBlock-more-trigger" style="line-height: 0px" href="#"><i
                                                                        class="starail-Icon-changes"></i>
                                                                        <asp:Label ID="lblTrainChangesMobile" runat="server" CssClass="pricecheap" />
                                                                    </a>
                                                                </p>
                                                                <div class="pricebox-tag">
                                                                    <img src="<%=siteURL %>images/from.png" class="from-price" alt="" />
                                                                    <asp:Label ID="lblCheapPrice" runat="server" CssClass="price-font"></asp:Label>
                                                                </div>
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <i class="starail-Icon-time"></i><span class="starail-infoRow-time">
                                                                        <%#Eval("TotalJourneyTime")%></span> <a style="display: block;" class="bottom-arrow">
                                                                            <img src="<%=siteURL %>images/arrow-bottom.png" alt="" /></a> <a style="display: none;"
                                                                                class="top-arrow">
                                                                                <img src="<%=siteURL %>images/arrow-top.png" alt="" /></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-journeyDetails">
                                                            <div class="starail-JourneyBlock-content">
                                                                <asp:Repeater ID="GrdRouteInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="starail-JourneyBlock-journeyDetails-row">
                                                                            <div class="starail-JourneyBlock-journeyDetails-provider">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    <%#Eval("TrainNumber").ToString()%>
                                                                                    <%#Eval("TrainDescr").ToString()%>
                                                                                </p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Departs:
                                                                                </p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("DepartureDate")%></span>
                                                                                </p>
                                                                                <p>
                                                                                    <%#Eval("DepartureStationName")%>
                                                                                </p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Arrives:
                                                                                </p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("ArrivalDate")%></span>
                                                                                </p>
                                                                                <p>
                                                                                    <%#Eval("ArrivalStationName")%>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-u-hideDesktop starail-JourneyBlock-mobileTriggers">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle"></i>View journey details</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i
                                                                class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-content starail-u-hideMobile collapsiblediv" id="DivTr"
                                                            runat="server" style="display: none;">
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-JourneyBlock-mobileTickets">
                                                            <asp:DropDownList ID="ddlFromPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                            <asp:HyperLink ID="hlnkConfirm" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                                Text="Confirm Ticket" />
                                                            <asp:Button ID="btnGetPriceMobile" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger IR-GaCode"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-u-hideMobile collapsiblediv" style="display: none;">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle"></i>More info</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                            <asp:HyperLink href="#" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger is-disabled IR-GaCode"
                                                                ID="btnContinueBooking" runat="server" onclick="return SendJCodeAndServiceID();"> Next:  <%=journyTag%></asp:HyperLink>
                                                            <asp:HiddenField ID="hdnJournyTypeReturn" runat="server" Value='<%#Eval("IsReturn") %>' />
                                                            <asp:Button ID="btnGetPrice" runat="server" CssClass="starail-Button IR-GaCode" Style="float: right;"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                            <p class="starail-JourneyBlock-footer-ticketSelection">
                                                                <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">More info <i class="starail-Icon-question"></i></a>
                                                            </p>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                            </div>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <asp:HyperLink runat="server" ID="hlnkContinue" class="starail-Button starail-rtnJourney-trigger IR-GaCode"
                                                                    Text="Continue" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkOutBottomEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkOutBottomLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="inbound" class="starail-Tabs-content starail-Tabs-content--hidden journey-type">
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkInTopEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkInEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkInToptomLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkInLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                        <asp:Repeater ID="rptinTi" runat="server" OnItemDataBound="rptinTi_ItemDataBound"
                                            OnItemCommand="rptinTi_ItemCommand">
                                            <ItemTemplate>
                                                <div class="starail-JourneyBlock">
                                                    <div class="starail-JourneyBlock-form">
                                                        <div class="starail-JourneyBlock-header">
                                                            <div class="starail-JourneyBlock-summary" style="width: 100%">
                                                                <div class="starail-DestinationIcon" style="height: 5px">
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow" style="min-width: 35%; float: left; padding-right: 1%;">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        From:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrFrom" />
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        To:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details" style="text-transform: none">
                                                                        <asp:Literal runat="server" ID="ltrTo" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-summary">
                                                                <div class="starail-DestinationIcon">
                                                                    <div class="starail-DestinationIcon-line">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle starail-DestinationIcon-circle--filled">
                                                                    </div>
                                                                    <div class="starail-DestinationIcon-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Departs:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%# Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                <asp:Literal ID="ltrDepDate" runat="server" Text='<%#Eval("DepartureDate")%>' />
                                                                            </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Arrives:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span> <span class="starail-JourneyBlock-divide starail-u-hideMobile">| </span><span class="starail-JourneyBlock-date starail-u-hideMobile">
                                                                                <asp:Literal ID="ltrArrDate" runat="server" Text='<%#Eval("ArrivalDate")%>' />
                                                                            </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Changes:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <asp:Label ID="lblTrainChanges" runat="server" Text="" />
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="starail-JourneyBlock-summaryRow starail-u-hideDesktop">
                                                                    <p class="starail-JourneyBlock-title">
                                                                        Duration:
                                                                    </p>
                                                                    <p class="starail-JourneyBlock-details starail-JourneyBlock-details--departs">
                                                                        <span class="starail-JourneyBlock-time">
                                                                            <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="starail-JourneyBlock-info starail-u-hideMobile pricemain-box">
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <a class="starail-JourneyBlock-more-trigger" style="line-height: 0px" href="#"><i
                                                                        class="starail-Icon-changes"></i>
                                                                        <asp:Label ID="lblTrainChangesMobile" runat="server" CssClass="pricecheap" />
                                                                    </a>
                                                                </p>
                                                                <div class="pricebox-tag">
                                                                    <img src="<%=siteURL %>images/from.png" class="from-price" alt="" />
                                                                    <asp:Label ID="lblCheapPrice" runat="server" CssClass="price-font"></asp:Label>
                                                                </div>
                                                                <p class="starail-JourneyBlock-infoRow p-price-margin">
                                                                    <i class="starail-Icon-time"></i><span class="starail-infoRow-time">
                                                                        <%#Convert.ToDateTime(Eval("TotalJourneyTime")).ToString("HH:mm")%></span> <a style="display: block;"
                                                                            class="bottom-arrow">
                                                                            <img src="<%=siteURL %>images/arrow-bottom.png" alt="" /></a> <a style="display: none;"
                                                                                class="top-arrow">
                                                                                <img src="<%=siteURL %>images/arrow-top.png" alt="" /></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="starail-JourneyBlock-journeyDetails">
                                                            <div class="starail-JourneyBlock-content">
                                                                <asp:Repeater ID="GrdRouteInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="starail-JourneyBlock-journeyDetails-row">
                                                                            <div class="starail-JourneyBlock-journeyDetails-provider">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    <%#Eval("TrainNumber").ToString()%>
                                                                                    <%#Eval("TrainDescr").ToString()%>
                                                                                </p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Departs:
                                                                                </p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("DepartureTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("DepartureDate")%></span>
                                                                                </p>
                                                                                <p>
                                                                                    <%#Eval("DepartureStationName")%>
                                                                                </p>
                                                                            </div>
                                                                            <div class="starail-JourneyBlock-journeyDetails-details">
                                                                                <p class="starail-JourneyBlock-journeyDetails-title">
                                                                                    Arrives:
                                                                                </p>
                                                                                <p>
                                                                                    <span>
                                                                                        <%#Convert.ToDateTime(Eval("ArrivalTime")).ToString("HH:mm")%></span><span class="starail-u-hideMobile">
                                                                                            | </span><span>
                                                                                                <%#Eval("ArrivalDate")%></span>
                                                                                </p>
                                                                                <p>
                                                                                    <%#Eval("ArrivalStationName")%>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-u-hideDesktop starail-JourneyBlock-mobileTriggers">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle"></i>View journey details</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i
                                                                class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                        </div>
                                                        <div class="starail-JourneyBlock-content starail-u-hideMobile collapsiblediv" id="DivTr"
                                                            runat="server" style="display: none;">
                                                        </div>
                                                        <%--Mobile--%>
                                                        <div class="starail-JourneyBlock-mobileTickets">
                                                            <asp:DropDownList ID="ddlFromPrice" runat="server" CssClass="starail-Form-select starail-JourneyBlock-select" />
                                                            <asp:HyperLink ID="hlnkConfirm" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger"
                                                                Text="Confirm Ticket" />
                                                            <asp:Button ID="btnGetPriceMobile" runat="server" CssClass="starail-Button starail-Button--fullMobile starail-Button--blue starail-JourneyBlock-confirm-trigger IR-GaCode"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-u-hideMobile collapsiblediv" style="display: none;">
                                                            <a class="starail-JourneyBlock-more-trigger" href="#"><i class="starail-Icon starail-Icon-pluscircle"></i>More info</a> <a class="starail-JourneyBlock-less-trigger" href="#"><i class="starail-Icon starail-Icon-minuscircle"></i>Hide journey details</a>
                                                            <asp:HyperLink href="#" class="starail-JourneyBlock-button starail-Button starail-rtnJourney-trigger is-disabled IR-GaCode"
                                                                ID="btnContinueBooking" runat="server" onclick="return SendJCodeAndServiceID();">
                                                                 Next:  <%=journyTag%></asp:HyperLink>
                                                            <asp:HiddenField ID="hdnJournyTypeReturn" runat="server" Value='<%#Eval("IsReturn") %>' />
                                                            <asp:Button ID="btnGetPrice" runat="server" CssClass="starail-Button IR-GaCode" Style="float: right;"
                                                                Text="Get Price" Visible="false" CommandName="GetPrice" CommandArgument='<%#Eval("JourneySolutionCode")+","+Eval("TripType") %>' />
                                                        </div>
                                                        <div class="starail-JourneyBlock-footer starail-JourneyBlock-mobileFooter starail-u-hide">
                                                            <p class="starail-JourneyBlock-footer-ticketSelection">
                                                                <span>Class not found</span> <a href="#" class="js-lightboxOpen" data-lightbox-id="starail-ticket-info">More info <i class="starail-Icon-question"></i></a>
                                                            </p>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <a href="#" class="starail-Button starail-undoTicket-trigger">Undo</a>
                                                            </div>
                                                            <div class="starail-JourneyBlock-mobileFooter-btns">
                                                                <asp:HyperLink runat="server" ID="hlnkContinue" class="starail-Button starail-rtnJourney-trigger IR-GaCode"
                                                                    Text="Continue" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div class="starail-TrainOptions-earlierLaterBtns">
                                            <asp:LinkButton runat="server" ID="lnkInBottomEarlier" CssClass="starail-Button starail-Button--blue IR-GaCode"
                                                OnClick="lnkInEarlier_Click"> <i class="starail-Icon-chevron-left"></i><i class="starail-Icon-double-chevron-left"></i>Earlier trains</asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkInBottomLater" CssClass="starail-Button starail-Button--blue starail-TrainOptions-later IR-GaCode"
                                                OnClick="lnkInLater_Click">Later trains <i class="starail-Icon-chevron-right"></i><i class="starail-Icon-double-chevron-right"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="starail-Grid-col starail-Grid-col--4of12 starail-u-hideMobile starail-Sidebar">
                                    <a href="#" class="starail-Button starail-Button--full starail-Sidebar-rtnJourney starail-rtnJourney-trigger is-disabled IR-GaCode"
                                        onclick="return SendJCodeAndServiceID();">Next:
                                        <%=journyTag%></a> <a href="#" class="starail-Button starail-Button--full starail-Sidebar-booking starail-booking-trigger starail-u-hide is-disabled IR-GaCode"
                                            onclick="return SendJCodeAndServiceID();">Next: Booking Details</a>
                                    <div class="starail-Tabs starail-Sidebar-tabs">
                                        <ul class="starail-Tabs-tabs">
                                            <li class="starail-Tabs-tab starail-Tabs-tab--active"><a href="#starail-routemap"
                                                class="js-starail-Tabs-trigger">
                                                <div>
                                                    Route Map
                                                </div>
                                            </a></li>
                                            <li class="starail-Tabs-tab"><a href="#starail-edittickets" class="js-starail-Tabs-trigger IR-GaCode">
                                                <div>
                                                    Edit Search
                                                </div>
                                            </a></li>
                                        </ul>
                                        <div id="starail-routemap" class="starail-Tabs-content">
                                            <div class="results-map">
                                                <div id="mymap" class="results-map" style="height: 300px; width: 100%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="starail-edittickets" class="starail-Tabs-content starail-Tabs-content--hidden">
                                            <div class="starail-SearchTickets starail-SearchTickets--mini">
                                                <div class="starail-Box starail-Box--whiteMobile">
                                                    <uc1:ucTrainSearch ID="ucTrainSearch" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    &nbsp;
                                    <div class="starail-Sidebar-bottomBtns">
                                        <a href="#" class="starail-Button starail-Button--full starail-Sidebar-rtnJourney starail-rtnJourney-trigger is-disabled IR-GaCode"
                                            onclick="return SendJCodeAndServiceID();">Next:
                                            <%=journyTag%></a> <a href="#" class="starail-Button starail-Button--full starail-Sidebar-booking starail-booking-trigger starail-u-hide is-disabled IR-GaCode"
                                                onclick="return SendJCodeAndServiceID();">Next: Booking Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="starail-TrainOptions-mobileNav starail-u-hideDesktop">
                        <div class="starail-TrainOptions-mobileNav-stage1">
                            <a href="default.aspx#rail-tickets" class="starail-Button starail-Button--blue"><i
                                class="starail-Icon-chevron-left"></i>Edit Search</a> <a href="#" class="starail-Button starail-TrainOptions-mobileNav-rtnJourney starail-rtnJourney-trigger starail-Button--blue is-disabled">Next: Return <i class="starail-Icon-chevron-right"></i></a>
                        </div>
                        <div class="starail-TrainOptions-mobileNav-stage2 starail-u-hide">
                            <a href="#" class="starail-Button starail-Button--blue starail-TrainOptions-outbound-trigger">
                                <i class="starail-Icon-chevron-left"></i>Outbound</a> <a href="#" class="starail-Button starail-TrainOptions-mobileNav-booking starail-booking-trigger starail-Button--blue is-disabled">Your Details <i class="starail-Icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%=pricePoup%>
        <%--   Model Popup--%>
        <div style="display: none;">
            <asp:HyperLink ID="HyperLink2" runat="server" />
        </div>
        <asp:ModalPopupExtender ID="mdpexQuickLoad" runat="server" CancelControlID="btnCloseWin"
            PopupControlID="pnlQuckLoad" BackgroundCssClass="modalBackground" TargetControlID="HyperLink2" />
        <asp:Panel ID="pnlQuckLoad" runat="server">
            <div class="starail-popup">
                <div class="starail-BookingDetails-form">
                    <h2>Lead Passenger Information</h2>
                    <div class="starail-Form-row selectlistmargin">
                        <label class="starail-Form-label" for="starail-country">
                            Lead passenger name <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlTitle" runat="server" CssClass="starail-Form-select selectlist"
                                    TabIndex="95">
                                    <asp:ListItem Value="0">Title</asp:ListItem>
                                    <asp:ListItem Value="1">Dr.</asp:ListItem>
                                    <asp:ListItem Value="2">Mr.</asp:ListItem>
                                    <asp:ListItem Value="3">Miss</asp:ListItem>
                                    <asp:ListItem Value="4">Mrs.</asp:ListItem>
                                    <asp:ListItem Value="5">Ms.</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqvalerror0" runat="server" ControlToValidate="ddlTitle"
                                    InitialValue="0" Display="Dynamic" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row selectlistmargin">
                        <label class="starail-Form-label" for="starail-country">
                        </label>
                        <div class="starail-Form-inputContainer selectlistwidth">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtFirstname" runat="server" MaxLength="15" CssClass="starail-Form-input"
                                    TabIndex="96" placeholder="First Name" />
                                <asp:RequiredFieldValidator ID="reqvalerror1" runat="server" ControlToValidate="txtFirstname"
                                    Display="Dynamic" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row selectlistmargin">
                        <label class="starail-Form-label" for="starail-country">
                        </label>
                        <div class="starail-Form-inputContainer selectlistwidth">
                            <div class="starail-Form-inputContainer-col">
                                <asp:TextBox ID="txtLastname" runat="server" MaxLength="25" CssClass="starail-Form-input"
                                    TabIndex="96" placeholder="Last Name" />
                                <asp:RequiredFieldValidator ID="reqvalerror2" runat="server" ControlToValidate="txtLastname"
                                    Display="Dynamic" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row">
                        <label class="starail-Form-label" for="starail-country">
                            Country of residence <span class="starail-Form-required">*</span></label>
                        <div class="starail-Form-inputContainer selectlistwidth">
                            <div class="starail-Form-inputContainer-col">
                                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="starail-Form-select" TabIndex="97">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqvalerror3" runat="server" ErrorMessage="*" ControlToValidate="ddlCountry"
                                    InitialValue="0" Display="Dynamic" CssClass="errMsg" ValidationGroup="cntnu" />
                            </div>
                        </div>
                    </div>
                    <div class="starail-Form-row selectlistmargin">
                        <div id="divShowFareRules" class="fare-rules-list">
                        </div>
                        <div id="ShowCurrentDetail" style="display: none">
                        </div>
                        <hr style="margin: 0px" />
                    </div>
                    <div class="starail-Form-row selectlistmargin" style="text-align: center;">
                        <asp:Button ID="btnContinue" runat="server" CssClass="stapopup-btn starail-Button starail-Form-button IR-GaCode"
                            TabIndex="98" Text="Continue" ValidationGroup="cntnu" OnClick="btnContinue_Click" />
                        <asp:LinkButton ID="btnCloseWin" CssClass="stapopup-btn starail-Form-button IR-GaCode"
                            runat="server" Text="Cancel"></asp:LinkButton>
                    </div>
                </div>
        </asp:Panel>
        <%--Ti--%>
        <asp:HiddenField ID="hdnFromjsCode" runat="server" />
        <asp:HiddenField ID="hdnFromtrainNo" runat="server" />
        <asp:HiddenField ID="hdnFromtripType" runat="server" />
        <asp:HiddenField ID="hdnFromsvCode" runat="server" />
        <asp:HiddenField ID="hdnFromsvTypCode" runat="server" />
        <asp:HiddenField ID="hdnFromoCd" runat="server" />
        <asp:HiddenField ID="hdnFromoTypCd" runat="server" />
        <asp:HiddenField ID="hdnFromoSubCd" runat="server" />
        <asp:HiddenField ID="hdnFromagre" runat="server" />
        <asp:HiddenField ID="hdnFromClass" runat="server" />
        <asp:HiddenField ID="hdnTojsCode" runat="server" />
        <asp:HiddenField ID="hdnTotrainNo" runat="server" />
        <asp:HiddenField ID="hdnTotripType" runat="server" />
        <asp:HiddenField ID="hdnTosvCode" runat="server" />
        <asp:HiddenField ID="hdnTosvTypCode" runat="server" />
        <asp:HiddenField ID="hdnTooCd" runat="server" />
        <asp:HiddenField ID="hdnTooTypCd" runat="server" />
        <asp:HiddenField ID="hdnTooSubCd" runat="server" />
        <asp:HiddenField ID="hdnToagre" runat="server" />
        <asp:HiddenField ID="hdnToClass" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upnlTrainSearch"
    DisplayAfter="0" DynamicLayout="True">
    <ProgressTemplate>
        <div class="modalBackground progessposition">
        </div>
        <div class="progess-inner2">
            UPDATING RESULTS...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[type=radio]').click(function () {
            var totalprice = 0;
            if ($('input[id=rdoFromTI]:checked').val() != undefined) {
                var strf = $('input[id=rdoFromTI]:checked').val();
                var arrayf = strf.split(",");
                $('#MainContent_ucSResultTi_lbloutPrice').text(parseFloat(arrayf[11]).toFixed(2));
                totalprice = parseFloat(arrayf[11]).toFixed(2);
            }

            if ($('input[id=rdoToTI]:checked').val() != undefined) {
                var strf = $('input[id=rdoToTI]:checked').val();
                var arrayf = strf.split(",");
                $('#MainContent_ucSResultTi_lblinPrice').text(parseFloat(arrayf[11]).toFixed(2));
                totalprice = parseFloat(totalprice) + parseFloat(arrayf[11]);
            }
            $('#MainContent_ucSResultTi_lblTotalPrice').text(parseFloat(totalprice).toFixed(2));
        });

        //change ddl price value in mobile 
        getOutBoundmobilevalue();
        getInBoundmobilevalue();
        $('.starail-JourneyBlock-mobileTickets').change(function () {
            getOutBoundmobilevalue();
            getInBoundmobilevalue();
        });
    });
</script>
<script type="text/javascript">
    function getOutBoundmobilevalue() {
        $("#outbound").find(".starail-JourneyBlock").each(function (key, value) {
            if ($(this).find("#MainContent_ucSResultTi_rptoutTi_ddlFromPrice_" + key + " option:selected").length > 0) {
                var pricevalue = $(this).find("#MainContent_ucSResultTi_rptoutTi_ddlFromPrice_" + key + " option:selected").val();
                pricevalue = pricevalue + ",outbound";
                $(this).find("#MainContent_ucSResultTi_rptoutTi_hlnkContinue_" + key).attr("onclick", "return SendJCodeAndServiceIDMobile('" + pricevalue + "')");
            }
        });
    }

    function getInBoundmobilevalue() {
        $("#inbound").find(".starail-JourneyBlock").each(function (key, value) {
            if ($(this).find("#MainContent_ucSResultTi_rptinTi_ddlFromPrice_" + key + " option:selected").length > 0) {
                var pricevalue = $(this).find("#MainContent_ucSResultTi_rptinTi_ddlFromPrice_" + key + " option:selected").val();
                pricevalue = pricevalue + ",inbound";
                $(this).find("#MainContent_ucSResultTi_rptinTi_hlnkContinue_" + key).attr("onclick", "return SendJCodeAndServiceIDMobile('" + pricevalue + "')");
            }
        });
    }

    function SendJCodeAndServiceID() {
        if ($("#MainContent_ucSResultTi_hdnReq").val() == "TI") {
            var SDataVal = 'The cancellation penalty breakdown is as follows:20% penalty to Trenitalia, and 10% processing fee to ItaliaRail for handling and processing the cancellation.';
            $("#ShowCurrentDetail").html(SDataVal.toString());
        }

        //--TI
        if (typeof ($('input[id=rdoFromTI]:checked').val()) != "undefined") {
            var strFrom = $('input[id=rdoFromTI]:checked').val();

            var strFromTi = strFrom.split(",");
            var jsCodeFrom = strFromTi[0]; var trainNoFrom = strFromTi[1]; var tripTypeFrom = strFromTi[2];
            var svCodeFrom = strFromTi[3]; var svTypCodeFrom = strFromTi[4]; var oCdFrom = strFromTi[5]; var oTypCdFrom = strFromTi[6];
            var oSubCdFrom = strFromTi[7]; var agreFrom = strFromTi[8]; var classFrom = strFromTi[9];

            $("#MainContent_ucSResultTi_hdnFromjsCode").val(jsCodeFrom);
            $("#MainContent_ucSResultTi_hdnFromtrainNo").val(trainNoFrom);
            $("#MainContent_ucSResultTi_hdnFromtripType").val(tripTypeFrom);
            $("#MainContent_ucSResultTi_hdnFromsvCode").val(svCodeFrom);
            $("#MainContent_ucSResultTi_hdnFromsvTypCode").val(svTypCodeFrom);
            $("#MainContent_ucSResultTi_hdnFromoCd").val(oCdFrom);
            $("#MainContent_ucSResultTi_hdnFromoTypCd").val(oTypCdFrom);
            $("#MainContent_ucSResultTi_hdnFromoSubCd").val(oSubCdFrom);
            $("#MainContent_ucSResultTi_hdnFromagre").val(agreFrom);
            $("#MainContent_ucSResultTi_hdnFromClass").val(classFrom);

            var fare = "";
            $('#starail-ticket-info' + strFromTi[10]).find('.tresult').find('p').each(function () {
                fare += $(this).html();
            });
            $('#divShowFareRules').html(fare);
        }

        if (typeof ($('input[id=rdoToTI]:checked').val()) != "undefined") {
            var strTo = $('input[id=rdoToTI]:checked').val();
            var strToTi = strTo.split(",");
            var jsCodeTo = strToTi[0]; var trainNoTo = strToTi[1]; var tripTypeTo = strToTi[2];
            var svCodeTo = strToTi[3]; var svTypCodeTo = strToTi[4]; var oCdTo = strToTi[5]; var oTypCdTo = strToTi[6];
            var oSubCdTo = strToTi[7]; var agreTo = strToTi[8]; var classTo = strToTi[9];

            $("#MainContent_ucSResultTi_hdnTojsCode").val(jsCodeTo);
            $("#MainContent_ucSResultTi_hdnTotrainNo").val(trainNoTo);
            $("#MainContent_ucSResultTi_hdnTotripType").val(tripTypeTo);
            $("#MainContent_ucSResultTi_hdnTosvCode").val(svCodeTo);
            $("#MainContent_ucSResultTi_hdnTosvTypCode").val(svTypCodeTo);
            $("#MainContent_ucSResultTi_hdnTooCd").val(oCdTo);
            $("#MainContent_ucSResultTi_hdnTooTypCd").val(oTypCdTo);
            $("#MainContent_ucSResultTi_hdnTooSubCd").val(oSubCdTo);
            $("#MainContent_ucSResultTi_hdnToagre").val(agreTo);
            $("#MainContent_ucSResultTi_hdnToClass").val(classTo);
            var fare = "";
            $('#starail-ticket-info' + strToTi[10]).find('.tresult').find('p').each(function () {
                fare += $(this).html();
            });
            $('#divShowFareRules').html(fare);
        }
        checkSelectedPrice();
        return false;
    }

    function checkSelectedPrice() {
        var count = 0;
        $('.csscount').each(function () {
            count++;
        });

        if (count == 2 && $('input[id=rdoToTI]:checked').val()) {
            $find('MainContent_ucSResultTi_mdpexQuickLoad').show();
        }
        if (count == 1 && $('input[id=rdoFromTI]:checked').val()) {
            $find('MainContent_ucSResultTi_mdpexQuickLoad').show();
        }
        if ($('input[id=rdoToTI]:checked').val() == false && $('input[id=rdoFromTI]:checked').val() == false) {
            alert("Pelase select at least one price");
        }
    }

    function SendJCodeAndServiceIDMobile(strf) {
        var arrayf = strf.split(",");
        if (arrayf[12] == 'outbound') {
            var jsCodeFrom = arrayf[0]; var trainNoFrom = arrayf[1]; var tripTypeFrom = arrayf[2];
            var svCodeFrom = arrayf[3]; var svTypCodeFrom = arrayf[4]; var oCdFrom = arrayf[5]; var oTypCdFrom = arrayf[6];
            var oSubCdFrom = arrayf[7]; var agreFrom = arrayf[8]; var classFrom = arrayf[9];
            $("#MainContent_ucSResultTi_hdnFromjsCode").val(jsCodeFrom);
            $("#MainContent_ucSResultTi_hdnFromtrainNo").val(trainNoFrom);
            $("#MainContent_ucSResultTi_hdnFromtripType").val(tripTypeFrom);
            $("#MainContent_ucSResultTi_hdnFromsvCode").val(svCodeFrom);
            $("#MainContent_ucSResultTi_hdnFromsvTypCode").val(svTypCodeFrom);
            $("#MainContent_ucSResultTi_hdnFromoCd").val(oCdFrom);
            $("#MainContent_ucSResultTi_hdnFromoTypCd").val(oTypCdFrom);
            $("#MainContent_ucSResultTi_hdnFromoSubCd").val(oSubCdFrom);
            $("#MainContent_ucSResultTi_hdnFromagre").val(agreFrom);
            $("#MainContent_ucSResultTi_hdnFromClass").val(classFrom);
            var fare = "";
            $('#starail-ticket-info' + arrayf[10]).find('.tresult').find('p').each(function () {
                fare += $(this).html();
            });
            $('#divShowFareRules').html(fare);
        }
        if (arrayf[12] == 'inbound') {
            var jsCodeTo = arrayf[0]; var trainNoTo = arrayf[1]; var tripTypeTo = arrayf[2];
            var svCodeTo = arrayf[3]; var svTypCodeTo = arrayf[4]; var oCdTo = arrayf[5]; var oTypCdTo = arrayf[6];
            var oSubCdTo = arrayf[7]; var agreTo = arrayf[8]; var classTo = arrayf[9];
            $("#MainContent_ucSResultTi_hdnTojsCode").val(jsCodeTo);
            $("#MainContent_ucSResultTi_hdnTotrainNo").val(trainNoTo);
            $("#MainContent_ucSResultTi_hdnTotripType").val(tripTypeTo);
            $("#MainContent_ucSResultTi_hdnTosvCode").val(svCodeTo);
            $("#MainContent_ucSResultTi_hdnTosvTypCode").val(svTypCodeTo);
            $("#MainContent_ucSResultTi_hdnTooCd").val(oCdTo);
            $("#MainContent_ucSResultTi_hdnTooTypCd").val(oTypCdTo);
            $("#MainContent_ucSResultTi_hdnTooSubCd").val(oSubCdTo);
            $("#MainContent_ucSResultTi_hdnToagre").val(agreTo);
            $("#MainContent_ucSResultTi_hdnToClass").val(classTo);

            var fare = "";
            $('#starail-ticket-info' + arrayf[10]).find('.tresult').find('p').each(function () {
                fare += $(this).html();
            });
            $('#divShowFareRules').html(fare);
        }
        checkSelectedPrice();
        return false;
    }
</script>
<script src="Scripts/gmap.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en" type="text/javascript"></script>
<script type="text/javascript">
    var geocoder;
    var maps;
    $(window).load(function () {
        LoadMap();
    });
    function LoadMap() {
        var start = document.getElementById('txtFrom').value;
        start = start.trim().replace('(All stations)', '');
        start = start.trim().replace(/[^a-z0-9]+/gi, ' ');
        start = start + " italy";


        var end = document.getElementById('txtTo').value;
        end = end.trim().replace('(All stations)', '');
        end = end.trim().replace(/[^a-z0-9]+/gi, ' ');
        end = end + " italy";

        var geocoder = new window.google.maps.Geocoder();
        initializeGMap();
        geocoder.geocode({ 'address': start }, function (results, status) {
            if (status == window.google.maps.GeocoderStatus.OK) {
                setFromMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), start);
                drawPolyline();
            }
        });
        geocoder.geocode({ 'address': end }, function (results, status) {
            if (status == window.google.maps.GeocoderStatus.OK) {
                setToMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), end);
                drawPolyline();
            }
        });
    }
</script>
